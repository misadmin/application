<style type="text/css">
td.head {
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	padding:4px;
}
</style>

<div style="background:#FFF; ">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" style="font-family:Arial, Helvetica, sans-serif; font-size:25px; font-weight:bold; padding-top:5px; padding-bottom:5px; color:#666;"><?php print($title); ?></td>
    </tr>
  <tr>
    <td width="53%"><table align="left" cellpadding="2" cellspacing="0" style="width:300px; border:solid; border-width:1px; border-color:#060; margin-top:5px; margin-bottom:5px;">
      <tr style="font-size:12px;">
        <td width="94" class="head">Current Term</td>
        <td width="9" class="head">:</td>
        <td width="183" class="head"><?php print($academic_terms->term." ".$academic_terms->sy); ?></td>
      </tr>
    </table></td>
   </tr>
  <tr>
    <td colspan="2"><hr style="margin-top:10px; width:100%;" color="#000033" /></td>
  </tr>
</table>
</div>
