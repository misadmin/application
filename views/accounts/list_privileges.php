<h2 class="heading">List of All Privileges</h2>
<table style = "width:70%;" id="offerings" class="table table-bordered table-condensed">
    <thead>
    <tr>
      <th width="15%" class="header" style="text-align:center; vertical-align:middle;">Code</th>
      <th width="50%" class="header" style="text-align:center; vertical-align:middle;">Privilege Name </th>
     <th width="10%" class="header" style="text-align:center; vertical-align:middle;">Max Units (1st-3rd year)</th>
     <th width="10%" class="header" style="text-align:center; vertical-align:middle;">Max Units (4th/5th year)</th>
      <th width="10%" class="header" style="text-align:center; vertical-align:middle;">Edit</th>
      <th width="10%" class="header" style="text-align:center; vertical-align:middle;">Delete</th>
    </tr>
    </thead>
    <?php
		if($scholarships) {
			foreach ($scholarships AS $scholarship) {
			?>
			<tr>
			 <td style="text-align:left; vertical-align:middle; "><?php print($scholarship->scholarship_code); ?></td>
			 <td style="text-align:left; vertical-align:middle; "><?php print($scholarship->scholarship); ?> </td>
			 <td style="text-align:center; vertical-align:middle; "><?php print($scholarship->max_unit_undergrad); ?> </td>
			 <td style="text-align:center; vertical-align:middle; "><?php print($scholarship->max_unit_grad); ?> </td>
			  <td style="text-align:center; vertical-align:middle; ">
               <a class="edit_privilege" href="<?php print($scholarship->id); ?>"><i class="icon-edit"></i></a></td>
            <td style="text-align:center; vertical-align:middle; ">
               <a class="delete_privilege" href="<?php print($scholarship->id); ?>"><i class="icon-trash"></i></a>
                    		  
		     </td>
			</tr>
   			 <?php
			}
		}
	?>
</table>
<?php
// old school search
/* 
<!-- div style="background-color:#F8F8F8;">
<form method="post" name="search" action="echo site_url("dean/{$this->uri->segment(2)}")">
  <input type="hidden" name="step" value="2" />
  <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered" >
    <tr>
      <td>
      <div class="input-append">
  <input class="span2" id="appendedInputButton" type="text" style="width:200px;" name="code">
  <button class="btn" type="submit"><i class="icon-search"></i></button>
</div>
      </td>
      </tr>
  </table>
</form>
</div> -->
*/
?>
<script>
$().ready(function() {
	$('#offerings').dataTable( {
	    "aoColumnDefs": [
	    { "bSearchable": false, "aTargets": [ 1,2,3,4,5,6 ] }
	                   ] } );
  }
)
</script>

<form id="edit_priv" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="edit_privilege_form" />
		
</form>			


<script>
	$('.edit_privilege').click(function(event){
		event.preventDefault(); 
	
		     var priv_id = $(this).attr('href');
		
	  		$('<input>').attr({
			    type: 'hidden',
			    name: 'priv_id',
			    value: priv_id,
			}).appendTo('#edit_priv');
			$('#edit_priv').submit();
		
	});
</script>

<form id="delete_priv" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="form_to_delete_stud_privilege" />
		
	</form>			

<script>
$('.delete_privilege').bind('click', function(event){
	event.preventDefault();
	
		var priv_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'priv_id',
		    value: priv_id,
		}).appendTo('#delete_priv');
		$('#delete_priv').submit();
			
});
</script>