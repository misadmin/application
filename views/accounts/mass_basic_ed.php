<?php 
$levels = array(
		11=> 'Preschool',
		1 => 'Kindergarten',
		3 => 'Grade School',		
		4 => 'HS-Day',
		12 => 'Senior High',
);
?><div class="row-fluid form-horizontal">
	<h2 class="heading"><?php echo $title; ?></h2>
	<div class="control-group formSep">
		<label class="control-label">Level</label>
		<div class="controls">
			<select name="level" id="level">
			<option value="">-- Select --</option>
			<?php foreach($levels as $key => $level): ?>
			<option value="<?php echo $key; ?>" <?php echo isset($level_selected) && $level_selected==$level ? ' selected="selected"' : ''; ?>><?php echo $level; ?></option>
			<?php endforeach; ?>		
			</select>
		</div>
	</div>
	<div class="control-group formSep">
		<label class="control-label">Year Level</label>
		<div class="controls">
			<select name="year_level" id="year_level"></select>
		</div>
	</div>
	<div class="control-group formSep">
		<label class="control-label">Section</label>
		<div class="controls">
			<select name="section" id="section">
				 <option value=""></option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<button id="submit" class="btn btn-success">Generate and Prepare to Print</button>
		</div>			
	</div>
</div>
<script>
var handler = {
	site_url: "<?php echo site_url(); ?>",
	init: function(){
		var level = <?php echo (isset($level_selected) ? $level_selected : '2'); ?>;
		var print_what = "<?php echo $print_what; ?>";
		$('#submit').addClass('disabled');
		$('#year_level').html(handler.set_options(level));
		$('#level,#year_level,#section').on('change', function(){
			var level=$('#level').val();
			var year_level=$('#year_level').val();
			var section = $('#section').val();
			if(level=='' || year_level=='' || section==''){
				$('#submit').addClass('disabled');
			} else
				$('#submit').removeClass('disabled');
		});
		$('#level').bind('change', function(){
			switch ($(this).val()){
				case '0' : $('#year_level').html('<option></option>');
						$('#section').html('<option value=""></option>');
						break;
				case '2' : $('#year_level').html(handler.set_options(1,2));
						$('#section').html('<option value=""></option>');
						break;
				case '1' : $('#year_level').html(handler.set_options(1,1));
						$('#section').html('<option value=""></option>');
						break;
				case '11' : $('#year_level').html(handler.set_options(1,1));
						$('#section').html('<option value=""></option>');
						break;						
				case '3' : $('#year_level').html(handler.set_options(1,6));
						$('#section').html('<option value=""></option>');
						break;
				case '4' : $('#year_level').html(handler.set_options(7,10));
						$('#section').html('<option value=""></option>');
						break;
				case '12' : $('#year_level').html(handler.set_options(11,12));
						$('#section').html('<option value=""></option>');
						break;
				default: $('#year_level').html('<option></option>');
						$('#section').html('<option value=""></option>');
						break;
			}
		});
		$('#year_level').on('change', function(){
			var year_level=$(this).val();
			var level = $('select[name="level"]').val();
			$.ajax({
				'url': '<?php echo site_url('ajax/sections/'); ?>?level='+level+'&year='+year_level,
				'dataType':'json',
				'success':function(data){
					var section_selector = '<option value="">-- Select --</option>';
					for(var i=0; i<data.length; i++){
						section_selector += '<option value="'+data[i].id+'">('+data[i].id+') '+data[i].section+'</option>';
					}
					$('#section').html(section_selector);
				},
				'error':function(){

				}	
			});
		});
		$('#submit').on('click', function(){
			if($(this).hasClass('disabled'))
				return false;
			var what = "<?php $exploded=explode("_", $print_what); echo $exploded[2]; ?>";

			var url = handler.site_url + "printer/basic_ed/" + what + "/"  + $('#level').val() + "/" + $('#year_level').val() + "/" + $('#section').val();
			window.open(url, 'print', "height=300,width=400");
		});
	},
	set_options: function(start, num){
		var i;
		var content='<option value="">-- Select --</option>';
		for(i=start; i<=num;i++){
			content = content + '<option value="' + i + '">' + i + '</option>';
		}
		return content;
	}	
}

function print(content) {
    var dapplet = document.jzebra;
    if (dapplet != null) {
       dapplet.append(content);
       dapplet.print();
	}
}
$().ready(function(){
	handler.init();		
});
</script>
