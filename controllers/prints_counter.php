 <?php
class prints_counter extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Prints_counter_model');
        $this->load->model('Academic_terms_model');
    }


    public function check_print_count(){
    	$counter = 0;
    	$hist_id = $this->input->post('hist_id');
    	$idnum = $this->input->post('idnum');
    	$period = $this->input->post('period');;

    	//log_message("INFO","HIST_ID =>".print_r($hist_id,true));
    	//log_message("INFO","IDNUM =>".print_r($idnum,true));
    	log_message("INFO","PERIOD   ==> ".print_r($period,true)); //Toyet 8.28.2018
		$current_term = $this->Academic_terms_model->current_academic_term();
		if($this->Prints_counter_model->exists_in_term($idnum,$current_term->id)){
		} else {
			$this->Prints_counter_model->add_prints_counter($hist_id,$period);
		}
		switch ($period) {
			case 'Pre-Final':
				$counter = $this->Prints_counter_model->increment_prefinal($hist_id);
				break;
            case 'Final':
                log_message("INFO","passing here..."); 
                $counter = $this->Prints_counter_model->increment_final($hist_id);
                break;
		}
        //log_message("INFO","$counter".print_r($counter,true)); // Toyet 9.19.2018
		echo $counter;
    }


}