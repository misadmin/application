<style>
	div#msg_header_assign {
		border-radius: 5px; 
		background-color: #D7D4D4; 
		border: solid; 
		border-width: 1px; 
		border-color: #B7B6B6; 
		text-align:left; 
		padding-left: 10px;
		margin:0 auto;
		margin-bottom:20px;
		font-family: arial;
		font-size: 14px;
		width: 98%;
		height: 35px;
		line-height: 35px;
		font-weight:bold;
		color:#B50303;
</style>

<div class="row-fluid" >
<?php
	if (!$is_enrolled and $totally_withdrawn=="N") {
?>
		<div id="msg_header_assign"> 
				Student is not enrolled yet!
		</div>
<?php 
    } elseif ($totally_withdrawn=="Y"){
?>
		<div id="msg_header_assign"> 
				Student has totally withdrawn!
		</div>
<?php

	} else {
?>
		<div id="msg_header_assign" style="display:none;">
		</div>

		<div class="form-horizontal">
			<fieldset>
				<form id="proceed_prospectus_to_use_form" class="form-horizontal">
					<div class="control-group formSep">
						<label for="grade_level" class="control-label">Prospectus Used</label>
						<div class="controls">
							<select id="prospectus_to_use" name="prospectus_to_use" class="form-control" style="width: 425px;" onchange="prospectus_select_changed();">
								<option value="0">--Select Prospectus--</option>
								<?php 
									if ($prospectus_list) {
										foreach ($prospectus_list AS $list) {
								?>
										<option value="<?php print($list->id); ?>" 
										<?php
										    if($list->id==$prospectus_assigned){
										    	print(' selected');
										    }
										?> ><?php print('('.$list->id.') '.$list->description.' - '.$list->effective_year);	?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<div class="controls" style="float:left; padding-right:10px;">
							<input type="submit" class="btn btn-danger" name="prospectus_to_use_btn" id="prospectus_to_use_btn" value="Update Prospectus!" disabled />
						</div>
						<div style="float:left; padding-right:8px; margin-top:5px;" id="show_prospectus_to_use_icon">
						</div>		
					</div>
				</form>
			</fieldset>
		</div>
<?php 
	} 
?>

</div>


<script>
	$('#proceed_prospectus_to_use_form').submit(function(e){
		e.preventDefault();		
		
		var prospectus_id = $('#prospectus_to_use').val();
		
		var confirmed = confirm('Assign/Change Prospectus Information?');

		if (confirmed){

			var students_idno      = $(this).attr('href');
			var student_history_id = "<?php print($student_history_id); ?>";

			$('#show_prospectus_to_use_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+students_idno,
				data: { student_history_id: student_history_id,
						prospectus_id: prospectus_id,
						action: 'prospectus_to_use' },
				dataType: 'text',
				success: function(response) {				
					$('#show_prospectus_to_use_icon').html("");
					$('#msg_header_assign').show();	
					$('#msg_header_assign').html("Student successfully assigned/changed a prospectus!");
					alert("Student successfully assigned/changed a prospectus!");
				},
			    error: function (request, error) {
			           console.log(arguments);
			           alert(" Can't do because: " + error);
			    },
			});
		}
	});

	function isEmpty(v) {
		if (v === '' || v === null) {
			return true;
		} else {
			return false;
		}
	}

</script>



<script>
	function prospectus_select_changed(){

		var prospectus_assigned = <?php print($prospectus_assigned); ?>;
		var prospectus_selected = $('#prospectus_to_use').val();

		if (prospectus_assigned != prospectus_selected){
			$('#prospectus_to_use_btn').prop('disabled', false);
		} else {
			$('#prospectus_to_use_btn').prop('disabled', true);
		}
	};	
</script>


