

		<div style="width:100%; border-radius:5px; background-color:#EAEAEA; border:solid 1px #D8D8D8; height:35px;">

				<!-- Added by Toyet 12.06.2017
					 as requested by Sir Charlie:Accounts -->
				<?php
					if($select_id=='assess' && $role=='accounts'){
						echo '<div style="float:left; padding-left:8px; margin-top:6px;">';
						echo '<a href="#jumpto"><button class="btn btn-small btn-primary"> Jump To Print Buttons </button></a>';
						echo '</div>';
				} ?>

				<div style="float:right;" >
					<div id="show_change_icon_<?php print($select_id); ?>" style="float:left; padding-right:8px; margin-top:6px;">
							
					</div>
					<div style="float:left; padding-top:4px; margin-right:10px;" >
						<select name="term_id" class="form-control <?php print($change_select); ?>" style="width:auto; height:auto;" id="<?php print($select_id); ?>">
							<?php 
								if ($academic_terms) {
									foreach ($academic_terms AS $term) {
							?>		
										<option value="<?php print($term->id); ?>" 
											term_text="<?php print($term->student_term." ".$term->sy); ?>"
											<?php if ($selected_term == $term->id) print("selected"); ?>>
											<?php print($term->term." ".$term->sy); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
		</div>
