
<div style="overflow:auto; height:500px; float:right; width:30%; vertical-align:top;" >
	  <table id="offered_schedules" border="0" cellspacing="0" class="table table-bordered table-hover table-condensed" 
	  		style="width:100%; padding:0px; margin:0px;">
	    <thead>
		   	<tr><th colspan=4>Recently Offered Courses:</th></tr>
	        <tr>
				<th width="30%" style="text-align:center; vertical-align:middle;">Course</th>
				<th width="10%" style="text-align:center; vertical-align:middle;">Units</th>
				<th width="40%" style="text-align:center; vertical-align:middle;" >Time</th>
				<th width="20%" style="text-align:center; vertical-align:middle;" >Room</th>
			</tr>
    	</thead>
<?php
			if ($offerings) {
				foreach ($offerings AS $offer) {
					if ($offer->status == 'ACTIVE') {
							echo '<tr>';
							echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->course_code .' ('.$offer->section_code.')';
							echo '<td style="text-align:center; vertical-align:middle; ">' . $offer->credit_units;
							$offering_slots = $this->Offerings_Model->ListOfferingSlots($offer->id);
							$offering_slots_id = 0;
							echo '<td style="text-align:left; vertical-align:middle; ">';
							if ($offering_slots) {
								foreach($offering_slots[0] AS $slots) {
									if ($offering_slots_id !== $slots->course_offerings_id){										
										echo  $slots->tym.' '.$slots->days_day_code .'<br>' ;										
									}
								}
								echo '<td style="text-align:left; vertical-align:middle; ">' ;								
								foreach($offering_slots[0] AS $slots) {								
									if ($offering_slots_id !== $slots->course_offerings_id){										
										echo $slots->room_no .'<br>';
									}																			
								}
								$offering_slots_id = $slots->course_offerings_id;
							}
					}
				}
			}
?>
	</table>
</div>
</div>
<script>
	$().ready(function(){
		$("#offered_schedules").dataTable({
				"iDisplayLength": 100,
			    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			    //"sScrollY": "200px",
	  	});
	    $("#offered_schedules").stickyTableHeaders();
	    $('#offered_schedules').stickyTableHeaders('updateWidth');
		
	});	
</script>