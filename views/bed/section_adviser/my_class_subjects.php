		
			<div style="width:100%; overflow:auto; margin-top:15px; ">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr class="shs_header" style="text-align:center; font-size:11px; font-weight:bold; height:auto;"  >
								<td class="shs_header" style="width:10%;">Course<br>Code</td>
								<td class="shs_header" style="width:30%;">Descriptive Title</td>
								<td class="shs_header" style="width:20%;">Teacher</td>
								<td class="shs_header" style="width:8%;">Midterm<br>Submitted</td>
								<td class="shs_header" style="width:8%;">Midterm<br>Confirmed</td>
								<td class="shs_header" style="width:8%;">Finals<br>Submitted</td>
								<td class="shs_header" style="width:8%;">Finals<br>Confirmed</td>
								<td class="shs_header" style="width:8%;">View<br>Grades Submission</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($courses) {
									foreach($courses  AS $course) {
							?>			
										<tr>
											<td style="vertical-align:middle;"><?php print($course->course_code); ?></td>
											<td style="vertical-align:middle;"><?php print($course->descriptive_title); ?></td>
											<td style="vertical-align:middle;"><?php print($course->teacher); ?></td>
											<td style="vertical-align:middle; text-align:center;"><?php print($course->midterm_date); ?></td>
											<td style="vertical-align:middle; text-align:center;"><?php print($course->midterm_confirm_date); ?></td>
											<td style="vertical-align:middle; text-align:center;"><?php print($course->finals_date); ?></td>
											<td style="vertical-align:middle; text-align:center;"><?php print($course->finals_confirm_date); ?></td>
											<td style="vertical-align:middle; text-align:center;">
												<a href="<?php print($course->block_sections_id); ?>" 
													courses_id="<?php print($course->courses_id); ?>"
													course_offerings_id="<?php print($course->course_offerings_id); ?>"
													teacher="<?php print("Teacher: ".$course->teacher); ?>"
													descriptive_title="<?php print($course->descriptive_title); ?>"
													class="list_enrolled_students" >
													<div id="show_zoom_icon_<?php print($course->course_offerings_id); ?>" >
														<i class="icon-list"></i>
													</div>
												</a>
											</td>
										</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>


<script>
	$('.list_enrolled_students').click(function(e){
		e.preventDefault();		
		
		var block_sections_id   = $(this).attr('href');
		var courses_id          = $(this).attr('courses_id');
		var course_offerings_id = $(this).attr('course_offerings_id');
		var teacher             = $(this).attr('teacher');
		var descriptive_title   = $(this).attr('descriptive_title');


		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/my_section'));?>",
			data: { "block_sections_id": block_sections_id,
					"courses_id": courses_id,
					"course_offerings_id": course_offerings_id,
					"teacher": teacher,
					"action": 'extract_students_for_grade_confirm'},
			dataType: 'json',
			success: function(response) {				

				$('.heading').html('Senior High School - Grades Confirmation'); 	

				$('#show_my_students').html(response.output); 	
				$('#show_my_students').show();
				$('#show_my_subjects').hide();
				$('#section_list').hide();
				$('#show_msg').hide();
				
				$('#show_class_teacher').html(teacher); 	
				$('#show_description').html(descriptive_title); 
			}
		});
		
	});

</script>
			