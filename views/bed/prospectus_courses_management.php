<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>

<div id="show_prospectus_course_management_page" >
	<h2 class="heading"><?php print($yr_level->description); ?> - Prospectus Courses Management</h2>
	<div id="rounded_header" style="width:99%; font-size:12px; margin:0 auto; margin-top:15px; padding:0px;">
		<div id="container">
			<div id="left" >
				<div id="show_error_msg" style="padding-left:10px; padding-top:10px; color:#F92424; font-weight:bold; font-size:14px;">
					
				</div>
			</div>
			<div id="right">
				<div id="show_add_term_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<div style="float:left; padding-top:4px; padding-right:4px; font-size:14px;" >
					<a href="#" class="btn btn-success add_prospectus_term" >
						<i class="icon-plus-sign" style="margin-top:2px;"></i> Add Term
					</a>
				</div>
					<div style="float:left; padding-top:5px;" class="b2">
						<select name="prospectus_id" class="form-control change_new_strand" style="width:auto; height:auto;" id="select2">
							<?php 
								if ($prospectuses) {
									foreach ($prospectuses AS $prospectus) {
							?>		
										<option value="<?php print($prospectus->id); ?>" 
											level="<?php print($prospectus->level); ?>" 
											description="<?php print($prospectus->description); ?>" 
											year_level="<?php print($prospectus->year_level); ?>"
											<?php if ($prospectus_id == $prospectus->id) print("selected"); ?>>
											<?php print($prospectus->description); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
			</div>
		</div>
	</div>

	
	<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_terms" >
		<?php
			$data = array(
					"prospectus_courses"=>$prospectus_courses,
					"subjects"=>$subjects,
			);
			
			$this->load->view('bed/prospectus/list_all_prospectus_courses',$data);					
		?>
	</div>
</div>



<script>
	$('.add_prospectus_term').click(function(event){
				
		$('#show_error_msg').html('');
		
		var level		= $("select#select2 option:selected").attr('level');
		var year_level	= $("select#select2 option:selected").attr('description');
		
		$('#level').val(level);
		$('#year_level').val(year_level);

		$('#modal_new_prospectus_term').modal('show');
		
	});
</script>				


	<div id="modal_new_prospectus_term" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_prospectus_term_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Create Prospectus Term</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$this->load->view('bed/prospectus/modal_view/new_prospectus_term_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Create Term!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<script>
	$('#add_prospectus_term_form').submit(function(e) {
		e.preventDefault();
		
		var prospectus_id 	= $("select#select2 option:selected").attr('value');
		var year_level		= $("select#select2 option:selected").attr('year_level');

		$('#modal_new_prospectus_term').modal('hide');
		
		$('#show_add_term_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/prospectus_courses_management'); ?>"+'/'+prospectus_id,
			data: {	
					"action": "add_prospectus_term",
					"year_level": year_level,	
				},
			dataType: 'json',
			success: function(response) {													
				
				if (!response.success) {
					$('#show_error_msg').html('ERROR: Term already exists!');
				}
				$('#show_extracted_terms').html(response.output); 
				
				$('#show_add_term_icon').html('');
				
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
			
		});
		
	});
</script>



	<script>
		$("select#select2").change(function(){
			
			$('#show_error_msg').html('');

			var prospectus_id = $("select#select2 option:selected").attr('value');

			$('#show_add_term_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/prospectus_courses_management'));?>"+'/'+prospectus_id,
				data: {
						"action": "change_yr_level" },
				dataType: 'json',
				success: function(response) {													

					$('#show_prospectus_course_management_page').html(response.output); 
					
					$('#show_add_term_icon').html('');
					
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
				
			});
	
		});
	
	</script> 

