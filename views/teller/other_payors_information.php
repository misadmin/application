<h3 class="heading">Account Information</h3>
<div class="row-fluid formSep">
	<div class="span8">
		<div class="row">
			<div class="span2 text-right">Account Number:</div>
			<div class="span10"><strong><?php echo $account->number; ?></strong></div>
		</div>
		<div class="row">
			<div class="span2 text-right">Account Name:</div>
			<div class="span10"><strong><?php echo $account->fullname; ?></strong></div>	
		</div>
	</div>
	<div class="span4">
		<?php if(isset($profile_sidebar)): ?>
		<div class="widgets well" style="margin-bottom:0">
		<?php foreach ($profile_sidebar as $widget): ?>
			<?php echo $widget ?>
		<?php  endforeach ?>
		</div>
		<?php endif ?>
	</div>
</div>

