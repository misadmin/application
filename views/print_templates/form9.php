<div id="to_print" style="display:none;"><?php
$content = "";
$content .= $this->print_lib->init() . $this->print_lib->condensed();
$content .= $this->print_lib->align_right('RECORDS OF CANDIDATE FOR GRADUATION', 136) . "\n";
$content .= $this->print_lib->align_right('FOR COLLEGIATE COURSES', 136) . "\n\n\n";
$content .= str_pad('', 136, '=')."\n";
$def = array(4, 19, 3, 50, 16, 3, 41);
$rows = array(
			array(
					(object)array('content'=>'I.', 'align'=>'left'),
					(object)array('content'=>'Name of Candidate', 'align'=>'left'),
					(object)array('content'=>':', 'align'=>'center'),
					(object)array('content'=>$student_info->fullname, 'align'=>'left'), 
					(object)array('content'=>'Age: ', 'align'=>'left'),
					(object)array('content'=>':', 'align'=>'center'),
					(object)array('content'=>"{$student_info->age}        Gender: {$student_info->gender}", 'align'=>'left'),
				),
			array(
					(object)array('content'=>'', 'align'=>'left'),
					(object)array('content'=>'Date of Birth', 'align'=>'left'),
					(object)array('content'=>':', 'align'=>'center'),
					(object)array('content'=>$student_info->birthdate, 'align'=>'left'), 
					(object)array('content'=>'Place of Birth', 'align'=>'left'),
					(object)array('content'=>':', 'align'=>'center'),
					(object)array('content'=>$student_info->birth_place, 'align'=>'left'),
				),
			array(
					(object)array('content'=>'', 'align'=>'left'),
					(object)array('content'=>'Parent or Guardian', 'align'=>'left'),
					(object)array('content'=>':', 'align'=>'center'),
					(object)array('content'=>$guardian_parent, 'align'=>'left'), 
					(object)array('content'=>'Address', 'align'=>'left'),
					(object)array('content'=>':', 'align'=>'center'),
					(object)array('content'=>$student_info->home_address, 'align'=>'left'),
				));
$content .= $this->print_lib->table($def, $rows);
$content .= str_pad('', 136, '=')."\n";
$content .= $this->print_lib->table(
					array(4, 132), 
					array(
							array(
									(object)array('content'=>'II.', 'align'=>'left'), 
									(object)array('content'=>'Records of Preliminary Education', 'align'=>'left')
								)
							)
		);
$content .= str_pad('', 136, '=')."\n";
$content .= $this->print_lib->prelim_education_table ($elementary, $highschool);
$content .= str_pad('', 136, '=')."\n";
$content .= $this->print_lib->table(
		array(4, 29, 3, 100),
		array(
				array(
						(object)array('content'=>'III.', 'align'=>'left'),
						(object)array('content'=>'Entrance Data', 'align'=>'left'),
						(object)array('content'=>':', 'align'=>'center'),
						(object)array('content'=>$requirements, 'align'=>'left')
				),
		)
);
$content .= str_pad('', 136, '=')."\n";
$content .= $this->print_lib->table(
		array(4, 29, 3, 100),
		array(
				array(
						(object)array('content'=>'IV.', 'align'=>'left'),
						(object)array('content'=>'Candidate for Title/Degree', 'align'=>'left'),
						(object)array('content'=>':', 'align'=>'center'),
						(object)array('content'=>"{$student_info->academic_program_abbreviated} ({$student_info->academic_program})", 'align'=>'left')
				),
				array(
						(object)array('content'=>'', 'align'=>'left'),
						(object)array('content'=>'Date of Graduation', 'align'=>'left'),
						(object)array('content'=>':', 'align'=>'center'),
						(object)array('content'=>$date_of_graduation, 'align'=>'left')
				)
		)
);
$content .= str_pad('', 136, '=')."\n";
$content .= $this->print_lib->table(
		array(4, 132),
		array(
				array(
						(object)array('content'=>'V.', 'align'=>'left'),
						(object)array('content'=>'COLLEGE RECORDS', 'align'=>'left')
				)
		)
);
$max_lines_per_page = 60;
$table_overhead = 16;
$first_page_lines = $max_lines_per_page - (count(explode("\n", $content)) - 1) - $table_overhead + 5;
$succeeding_page_lines = $max_lines_per_page - $table_overhead - 2;
$last_page = FALSE;
$page = 1;
$line_num = 0;
$total_course_lines = count($courses);
$lines_left = count($courses);
$status = 'first';
while ( ! $last_page){
	if($page==1) {
		$num_courses_to_push = $first_page_lines; 
	} else {
		$content .= str_pad($student_info->fullname, 136, " ", STR_PAD_LEFT) . "\n\n\n\n";
		$num_courses_to_push = $succeeding_page_lines;
		$status = 'succeeding';
	}
	
	if($lines_left < $num_courses_to_push){
		$num_courses_to_push = $lines_left;
		$last_page = TRUE;
		$status = 'last';
	}
	
	$course_arr=array();
	for ($count = 0; $count < $num_courses_to_push; $count++){
		$course_arr[] = $courses[$line_num];
		$line_num++;
	}
	$forwarded_units = (isset($table_content->total_credits) ? $table_content->total_credits : 0);
	$table_content = $this->print_lib->form9_table($course_arr, $page, $status, $forwarded_units, ($max_lines_per_page - 4), $student_info->fullname, $last_sem);
	$content .= $table_content->content . $this->print_lib->char_form_feed();
	$lines_left = $lines_left - $num_courses_to_push;
	$page++;
}

$content .= $this->print_lib->no_condensed();
echo $content;
?></div>