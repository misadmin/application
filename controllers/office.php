<?php

class Office extends MY_Controller {
	
	/**
	 * This method creates the instance of a faculty account.
	 */
	public function __construct(){
		parent::__construct();
		//print_r($this->userinfo);die();
			
		if ($this->session->userdata('role')=='office'){				
			$this->navbar_data['menu'] = array(
							'AV Hall Reservation' => 
										array(
												'View Calendar' =>'calendar',
												'My Bookings History' => 'booking_history',
										),
					);
		}
		
		//$this->navbar_data['menu'] = $navigation;
		$this->content_lib->set_navbar_content('', $this->navbar_data);
	}
	
	/**
	 * Description:
	 * 		This function displays the class schedule of the faculty member and provides
	 * a download capability for the class list.
	 * 
	 * 
	 */
	
	function add_booking(){
		$activity_date = $this->input->post('activity_date');
		$activity_time_s = $this->input->post('activity_time_s');
		$activity_time_e = $this->input->post('activity_time_e');
		$activity_name = $this->input->post('activity_name');
		$equipment = $this->input->post('equipment');
		$size = $this->input->post('group_size');
		$remark = $this->input->post('remark');
		$location = $this->input->post('location');
		
		$this->load->library('av_lib');
		$this->av_lib->add_booking($activity_date,$activity_time_s,$activity_time_e,$activity_name,$equipment,$size,$remark,$location);
	}
	

	function list_all_bookings_of_the_day(){
		$activity_date = $this->input->post('date');
		$this->load->library('av_lib');
		$this->av_lib->list_all_bookings_of_the_day($activity_date);
	}
		
	function calendar(){
		$this->content_lib->enqueue_header_style('ggpopover');
		$this->content_lib->enqueue_footer_script('ggpopover');
		
		$browser = getBrowser(null, true);
		if (!$browser){
			$this->content_lib->set_message('Unable to determine your browser','alert-error');
			$this->content_lib->content();
			return false;
		}else{
			if ($browser['name'] !='Google Chrome' ){
				$this->content_lib->set_message('Sorry, but you must use Google Chrome to reserve.','alert-error');
				$this->content_lib->content();
				return false;					
			}else{
				if ((int)substr($browser['version'],0,2) < 31 ){ //ref: http://caniuse.com/#feat=input-datetime				
					$this->content_lib->set_message('Sorry, but you must use Google Chrome Version 31 or higher.','alert-error');
					$this->content_lib->content();
					return false;						
				}
			}
		}

		
		$this->load->library('av_lib');
		$this->av_lib->calendar();
	}
	
	
	function booking_history($empno=NULL){
		$this->load->model('av/av_model');
		$data['data'] = $this->av_model->booking_history($this->userinfo['empno']);
		if (!$data){
			$this->content_lib->set_message("Failed to list booking history!", 'alert-error');
		}else{
			//$this->content_lib->enqueue_footer_script('data_tables');
			//$this->content_lib->enqueue_footer_script('data_tables_tools');
			$this->content_lib->enqueue_body_content('av/booking_history',$data);				
		}
					
		$this->content_lib->content();
	}
	
	function cancel_my_booking(){
		$ids_to_cancel = implode(',',$this->input->post('check'));
		$this->load->library('av_lib');
		$this->av_lib->cancel_my_booking($ids_to_cancel);
	}
}


