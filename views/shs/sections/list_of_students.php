<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>


			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; ">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:10%;">Course<br>Code</td>
								<td class="shs_header" style="width:35%;">Descriptive Title</td>
								<td class="shs_header" style="width:30%;">Schedule/Room</td>
								<td class="shs_header" style="width:20%;">Teacher</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($schedules) {
									foreach($schedules AS $sched) {
							?>			
										<tr>
											<td><?php print($sched->course_code); ?></td>
											<td><?php print($sched->descriptive_title); ?></td>
											<td>
												<div id="container">
													<div id="left" style="width:230px;">
														<?php print($sched->stime.'-'.$sched->etime.' ['.$sched->day_name.']'); ?>
													</div>
													<div id="left">
														<?php print("- Rm.".$sched->room_no); ?>
													</div>
													<div id="right">
														<a href="<?php print($section->id); ?>" 
															course_offerings_id="<?php print($sched->course_offerings_id); ?>"
															course_offerings_slots_id="<?php print($sched->course_offerings_slots_id); ?>"
															descriptive_title="<?php print($sched->descriptive_title); ?>"
															schedule="<?php print($sched->stime.'-'.$sched->etime.' ['.$sched->day_name.']'); ?>"
															days_names="<?php print($sched->days_names); ?>"
															array_days="<?php print($sched->day_name); ?>"
															start_time="<?php print($sched->start_time); ?>"
															end_time="<?php print($sched->end_time); ?>"
															offerings_slots_rooms_id="<?php print($sched->offerings_slots_rooms_id); ?>"
															class="change_schedule_offering" >
															<div id="show_change_schedule_icon_<?php print($sched->course_offerings_id); ?>" >
																<i class="icon-edit"></i>
															</div>
														</a>
													</div>
												</div>
											</td>
											<td>
												<div id="container">
													<div id="left">
														<?php print($sched->teacher); ?>
													</div>
													<div id="right">
														<a href="<?php print($section->id); ?>" 
															course_offerings_id="<?php print($sched->course_offerings_id); ?>"
															descriptive_title="<?php print($sched->descriptive_title); ?>"
															schedule="<?php print($sched->start_time.'-'.$sched->end_time.' ['.$sched->day_name.']'); ?>"
															teacher_empno="<?php print($sched->employees_empno); ?>"
															class="assign_teacher" >
															<div id="show_assign_teacher_icon_<?php print($sched->course_offerings_id); ?>" >
																<i class="icon-edit"></i>
															</div>
														</a>
													</div>
												</div>
											</td>
											<td style="text-align:center;">
												<a href="<?php print($section->id); ?>" 
													course_offerings_id="<?php print($sched->course_offerings_id); ?>"
													course_offerings_slots_id="<?php print($sched->course_offerings_slots_id); ?>"
													class="delete_schedule" >
													<div id="show_trash_icon_<?php print($sched->course_offerings_id); ?>" >
														<i class="icon-trash"></i>
													</div>
												</a>
											</td>										
										</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>

