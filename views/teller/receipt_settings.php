<div class="span12">
	<form method="post" class="form-horizontal">
	<?php action_form ('update_receipts', FALSE); ?>
	<?php $this->common->hidden_input_nonce(FALSE); ?>
		<fieldset>
			<div class="control-group formSep">
				<label for="vat-receipt" class="control-label">VAT Receipt</label>
				<div class="controls">
					<input type="text" name="vat-receipt" id="vat-receipt" maxlength="10" value="<?php echo $vat_receipt; ?>" />
				</div>
			</div>
			<div class="control-group formSep">
				<label for="nonvat-receipt" class="control-label">non-VAT Receipt</label>
				<div class="controls">
					<input type="text" name="nonvat-receipt" maxlength="10" id="nonvat-receipt" value="<?php echo $nonvat_receipt; ?>" />
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn btn-success">Update Receipt Numbers</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>