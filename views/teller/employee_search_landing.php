<?php $genders = $this->config->item('genders'); ?>
<div class="row-fluid">
	<h2 class="heading">Employee Search</h2>
	<div class="pagination">
		<ul>
<?php for($count = ord('A'); $count<=ord('Z'); $count++): ?>
			<li <?php if(isset($letter) && $letter == chr($count)) echo 'class="active"'?>><a href="<?php echo chr($count); ?>"><?php echo chr($count); ?></a></li>
<?php endfor; ?>
		</ul>
	</div>
	<div class="row-fluid clearfix">
		<table class="table table-striped table-bordered mediaTable activeMediaTable" id="MediaTable-0">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="10%">Employee ID</th>
					<th width="75%">Employee Name</th>
					<th width="10%">Gender</th>
				</tr>
			</thead>
			<tbody>
<?php if(isset($results) && count($results) > 0): $count=0; ?>
<?php foreach ($results as $result): $count++; ?>
				<tr>
					<td><?php echo $count; ?></td>
					<td><a href="<?php echo site_url("{$role}/employee/{$result->empno}"); ?>"><?php echo $result->empno; ?></a></td>
					<td><?php echo $result->fullname; ?></td>
					<td><?php if( ! empty($result->gender)) echo $genders[$result->gender];?></td>
				</tr>
<?php endforeach; ?>
<?php else: ?>
				<tr><td colspan="4">No Results Found</td></tr>
<?php endif;?>
			</tbody>
		</table>
	</div>
</div>
<form method="post" action="" id="search_by_letter">
<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
$('.pagination a').click(function(event){
	event.preventDefault(); 
	var letter = $(this).attr('href');
			
	$('<input>').attr({
	    type: 'hidden',
	    name: 'letter',
	    value: letter,
	}).appendTo('#search_by_letter');
		$('#search_by_letter').submit();
});
</script>