<?php 
/**
 * Model to handle all accounts related database queries.
 * 
 * @author HOLY NAME
 * @copyright HNU-MIS
 * @package HNU-MIS
 * 
 */
class Accounts_model extends MY_Model {
	
	/**
	 * Method to extract all fees groups and fees subgroup
	 * 
	 * @return array|boolean an array of fees group and fees subgroup on success. Returns False when error.
	 */
	function all_fees_groups_subgroups (){
		$sql = "
				SELECT 
					fg.id fees_group_id, fg.fees_group, fs.id as fees_subgroups_id, fs.description
				FROM 
					fees_groups     fg
				LEFT JOIN 
					fees_subgroups  fs 
				ON 
					fg.id = fs.fees_groups_id
				ORDER BY 
					fg.fees_group, fs.description
				";
		
		$query = $this->db->query($sql);
		
		if ($query && $query->num_rows() > 0){
			$return = array();
			foreach ($query->result() as $row){
				if (array_key_exists($row->fees_group_id, $return)) {
					$return[$row->fees_group_id]['children'][$row->fees_subgroups_id] = $row->description;
				} else{
					$return[$row->fees_group_id] = array(
						'name'=>$row->fees_group,
						'children'=>array($row->fees_subgroups_id=>$row->description)
						);
				}
			}
			return $return;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Inserts a new fees group
	 * 
	 * @param string $data represents the new fees group to add
	 * @return insertid|False when successful False on error
	 */
	function insert_new_group ($data){
		$sql = "
			INSERT into
				fees_groups (`weight`, `fees_group`)
				(SELECT (MAX(`weight`) + 1) as weight, {$this->db->escape($data)} FROM fees_groups)
			";
		
		if ($this->db->query($sql)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}
	
	/**
	 * Inserts a new fees subgroup
	 * 
	 * @param int $group_id group id
	 * @param string $data the name of the new subgroup
	 * @return boolean
	 */
	function insert_new_subgroup ($group_id, $data){
		$sql = "
			INSERT into
				fees_subgroups (`fees_groups_id`, `description`)
			VALUE
				({$this->db->escape($group_id)}, {$this->db->escape($data)})
			";
		if ($this->db->query($sql)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}
	
	/**
	 * This function updates a fees group based on a given id
	 * @param timyint $group_id (group id)
	 * @param unknown_type $fees_group
	 */
	function edit_group($group_id, $fees_group){
		$sql = "
			UPDATE
				fees_groups 
			SET
				`fees_group`={$this->db->escape($group_id)}	
			WHERE
				id={$this->db->escape($group_id)}
			";
		return $this->db->query($sql);
	}
	
	/**
	 * This function updates the fees subgroups based on a given id
	 * @param unknown_type $id
	 * @param tinyint $fees_group_id (fees group id)
	 * @param varchar $description (description of the fees subgroups)
	 */
	function edit_subgroup($id, $fees_group_id, $description){
		$sql = "
		UPDATE
			fees_subgroups	
		SET
			`description`={$this->db->escape($description)},
			`fees_group_id`={$this->db->escape($fees_group_id)},
		WHERE
			id={$this->db->escape($group_id)}
		";
		return $this->db->query($sql);
	}
	
	/**
	 * This function retrieves the fees of a given academic group
	 * @param smallint $academic_year_id (academic year)
	 * @param tinyint $academic_group_id (academic group)
	 * @return multitype:multitype:multitype:multitype:NULL multitype:multitype:NULL     NULL  |boolean
	 */
	function fetch_fees_given_group ($academic_year_id, $academic_group_id){
		$sql = "
			SELECT 
				ay.end_year, tr.term, pg.abbreviation, fg.id as fees_group_id, fg.fees_group, fs.id as fees_subgroup_id, fs.description, f.yr_level, f.rate
			from 
				fees_schedule  f
			join 
				fees_subgroups fs 
			on 
				fs.id = f.fees_subgroups_id
			Join 
				fees_groups    fg 
			on 
				fg.id = fs.fees_groups_id
			join 
				acad_program_groups pg 
			on 
				pg.id = f.acad_program_groups_id
			join 
				academic_terms tr 
			on 
				tr.id = f.academic_terms_id
			join 
				academic_years ay on ay.id = tr.academic_years_id
			where 
				ay.id = {$this->db->escape($academic_year_id)} 
			and 
				pg.id={$this->db->escape($academic_group_id)}
			order by 
				fg.weight, ay.end_year,tr.term, pg.abbreviation, fg.fees_group, fs.description
 			";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			
			$return = array();
			foreach ($query->result() as $row) {
				
				if (array_key_exists($row->fees_group_id, $return)){
					//This fees_group_id already exists...
					if (array_key_exists($row->fees_subgroup_id, $return[$row->fees_group_id]['subgroups'])) {
						//This fees_subgroup_id already exists
						if (isset($return[$row->fees_group_id]['subgroups'][$row->fees_subgroup_id]['values']) && array_key_exists($row->yr_level, $return[$row->fees_group_id]['subgroups'][$row->fees_subgroup_id]['values'])) {
							//This year_level is already there...
							if (array_key_exists($row->term, $return[$row->fees_group_id]['subgroups'][$row->fees_subgroup_id]['values'][$row->yr_level])) {
								$return[$row->fees_group_id]['subgroups'][$row->fees_subgroup_id]['values'][$row->yr_level] = array($row->term => $row->rate); 
							} else {
							//	echo "here\n";
								$return[$row->fees_group_id]['subgroups'][$row->fees_subgroup_id]['values'][$row->yr_level][$row->term] = $row->rate;
							}
						} else {
							//This year_level does not exist yet...
							$return[$row->fees_group_id]['subgroups'][$row->fees_subgroup_id]['values'][$row->yr_level] = array($row->term => $row->rate);
						}	
					} else {
						//This fees_subgroup_id did not exist yet...
						$return[$row->fees_group_id]['subgroups'][$row->fees_subgroup_id] = array(
																								'name'	=> $row->description,
																								'values' => array(
																												$row->yr_level => array($row->term => $row->rate)));
					}
				} else {
					//this fees_group_id does not exist yet...
					$return[$row->fees_group_id] = array(
													'name' => $row->fees_group,
													'subgroups' => array(
																	$row->fees_subgroup_id => array(
																								'name'	=> $row->description,
																								'values' => array(
																												$row->yr_level => array($row->term => $row->rate)))));
				}
			}
			return $return;
		} else {
			return FALSE;	
		}
	}
	
	/**
	 * This function retrieves the assessment of a particular student
	 * @param int $student_histories_id (id representing the student's enrollment in a term)
	 * @return unknown|boolean
	 */
	function student_assessment ($student_histories_id) {
	
		$sql = "SELECT
					fg.fees_group, fsg.description, fs.rate
				FROM
					fees_schedule fs
				LEFT JOIN
					fees_subgroups fsg
				ON
					fs.fees_subgroups_id=fsg.id
				LEFT JOIN
					fees_groups fg
				ON
					fg.id=fsg.fees_groups_id
				LEFT JOIN
					acad_program_groups apg
				ON
					fs.acad_program_groups_id = apg.id
				LEFT JOIN
					academic_programs ap
				ON
					ap.acad_program_groups_id=apg.id
				LEFT JOIN
					academic_terms at
				ON
					at.id=fs.academic_terms_id
				LEFT JOIN
					 prospectus p
				ON
					p.academic_programs_id=ap.id
				LEFT JOIN
					student_histories sh
				ON
					sh.prospectus_id=p.id	
				WHERE
					sh.id={$this->db->escape($student_histories_id)}
				AND
					fs.yr_level=sh.year_level
				AND
					fs.academic_terms_id = sh.academic_terms_id
				ORDER BY 
					fg.weight, fg.fees_group, fsg.description
				";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			$return = array();
			foreach($query->result() as $row){
				if (array_key_exists($row->fees_group, $return)){
					$return[$row->fees_group][$row->description] = $row->rate;
				} else {
					$return[$row->fees_group] = array($row->description => $row->rate);
				}
			}
			return $return;
		} else {
			return FALSE;
		}
		
	}
	
	/**
	 * This function retrieves the id, group name and abbreviation for a given level, initially college level
	 * @param varchar $type (the level number)
	 * @return multitype:NULL |boolean
	 */
	public function academic_programs_group ($type=6) {
		$sql = "SELECT
					id, group_name, abbreviation
				FROM
					acad_program_groups
				WHERE
					levels_id = {$this->db->escape($type)}
				ORDER BY
					abbreviation	
				";
		
		$query = $this->db->query($sql);
		
		if ($query && $query->num_rows() > 0){
			$return = array();
			foreach($query->result() as $row){
				$return[$row->id] = $row->group_name;
			}
			return $return;
		} else {
			return FALSE;
		}
	}
	
	//added: 4/1/2013
	
	/**
	 * This function retrieves the tuition fee rate of a particular term enrollment of a student
	 * @param int $history_id (id representing the student's enrollment in a term)
	 * @return NULL
	 */
	function get_tuition_fee($history_id) 
		{
			$result = null;
			
			$q = "SELECT a.rate
					FROM
						fees_schedule as a,
						fees_subgroups as b,
						fees_groups as c, 
						student_histories as d, 
						prospectus as e, 
						academic_programs as f
					WHERE
						d.id = {$this->db->escape($history_id)} 
						AND d.academic_terms_id = a.academic_terms_id
						AND d.year_level = a.yr_level
						AND a.levels_id = 6
						AND a.fees_subgroups_id = b.id
						AND b.fees_groups_id = c.id
						AND c.fees_group = 'Tuition Basic'
						AND d.prospectus_id = e.id
						AND e.academic_programs_id = f.id
						AND f.acad_program_groups_id = a.acad_program_groups_id"; 
			
		$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}	
	
		/**
		 * This function retrieves the laboratory courses and their respective rates
		 * @param int $history_id (id representing the student's enrollment in a term)
		 * @param unknown_type $cur_aca_yr
		 * @return number
		 */
	function get_lab_fees($history_id, $cur_aca_yr) 
		{
			$result = 0;
			
			$q = "SELECT d.courses_id, d.rate
					FROM
						enrollments as a,
						course_offerings as b,
						courses as c,
						laboratory_fees as d
					WHERE
						a.student_history_id = {$this->db->escape($history_id)} 
						AND a.course_offerings_id = b.id
						AND b.courses_id = c.id
						AND c.group_types_id = 11
						c.id = d.courses_id
					)
					"; 
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}	

		/**
		 * This function retrieves the total fees belonging to a fees group
		 * @param int $history_id (id representing the student's enrollment in a term)
		 * @param tinyint $fees_groups_id (represents the fees group)
		 * @return NULL
		 */
		function getFees($history_id, $fees_groups_id) 
		{
			$result = null;
			
			$q = "SELECT sum(a.rate) as total_fee, b.description
					FROM
						fees_schedule as a,
						fees_subgroups as b,
						fees_groups as c, 
						student_histories as d, 
						prospectus as e, 
						academic_programs as f
					WHERE
						d.id = {$this->db->escape($history_id)} 
						AND d.academic_terms_id = a.academic_terms_id
						AND d.year_level = a.yr_level
						AND a.levels_id = 6
						AND a.fees_subgroups_id = b.id
						AND b.fees_groups_id = c.id
						AND c.id = {$this->db->escape($fees_groups_id)} 
						AND d.prospectus_id = e.id
						AND e.academic_programs_id = f.id
						AND f.acad_program_groups_id = a.acad_program_groups_id"; 
			
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}	
	

		/**
		 * This function retrieves the fees group applicable to a particular student
		 * @param tinyint $student_history_id (id representing the student's enrollment in a term)
		 * @return NULL
		 */
		function ListFeesGroups($student_history_id) {
			$result = null;
			
			$q1 = "SELECT 
						a.id, a.fees_group 
					FROM 
						fees_groups as a
					WHERE
					    a.weight != 1
						AND a.id NOT IN (Select 
											c.fees_groups_id
										 FROM 
										 	privileges_availed_details as c, 
											privileges_availed as d 
										WHERE
										    d.id = c.privileges_availed_id
											AND d.student_histories_id = {$this->db->escape($student_history_id)})    	
 					ORDER BY
						a.weight";
	
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;	
		
		}
		
	/**
	 * This function generates all fees groups
	 * @return NULL
	 */
	function ListAllFeesGroups() {
			$result = null;
				
			$q1 = "SELECT
							a.id, a.fees_group
						FROM
							fees_groups as a
						WHERE
							a.weight != 1
						ORDER BY
							a.weight";
		
			$query = $this->db->query($q1);
		
			if($query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
		
	}
		
	/**
	 * This function generates other fees groups
	 * @return NULL
	 */
	function ListOtherFeesGroups() {
			$result = null;
			
			$q1 = "SELECT 
						a.id, a.fees_group 
					FROM 
						fees_groups as a
					WHERE
					    a.weight != 1 AND a.weight != 0	
 					ORDER BY
						a.weight";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
		return $result;	
	}
	
	
	public function add_payer ($payer_name, $type){
		
		$sql = "
			INSERT into `{$type}` (
			";
	}	
	
	//Added: 4/17/2013
	
	/**
	 * This function retrieves the rate of a laboratory course
	 * @param int $course_id (represents the laboratory course)
	 * @param smallint $academic_year_id (represents the academic year where the rate applies
	 * @return number
	 */
	function get_lab_fee($course_id, $academic_year_id) 
		{
			$result = 0;
			
			$q = "SELECT a.rate
					FROM
						
						laboratory_fees as a
					WHERE
						a.courses_id = {$this->db->escape($course_id)} 
						AND a.academic_years_id = {$this->db->escape($academic_year_id)} 
				
					"; 
			//print($q); die();
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}else 
				$result = NULL; 
			
			return $result;
		}	
	
		
	/*
	 * NOTE: this is a temporary module
	 * By: genes 12/11/13
	 */	
	function ListStudents_EnrollmentsDeleted_AfterPosting($academic_terms_id) {
		$result = null;
			
		$q1 = "SELECT
					h.students_idno,
					CONCAT(a.lname,', ',a.fname,' ',a.mname) AS neym,
					b.course_code,
					c.section_code,
					b.paying_units,
					f.rate AS lab_fee,
					e.rate,
					g.deleted_on,
					c.status
				FROM
					enrollments_deleted AS g,
					course_offerings AS c,
					courses AS b 
					LEFT JOIN laboratory_fees AS f ON b.id=f.courses_id 
						AND f.academic_years_id = 114,
					students AS a,
					student_histories AS h,
					prospectus AS i,
					academic_programs AS j, 
					fees_schedule AS e 
				WHERE
					g.student_histories_id=h.id
					AND g.course_offerings_id = c.id
					AND c.courses_id=b.id
					AND a.idno = h.students_idno
					AND e.academic_terms_id = h.academic_terms_id
					AND e.yr_level = h.year_level
					AND h.prospectus_id=i.id
					AND i.academic_programs_id=j.id
					AND e.acad_program_groups_id=j.acad_program_groups_id
					AND h.academic_terms_id = {$this->db->escape($academic_terms_id)} 
					AND DATE(g.deleted_on) >= '2013-12-02'
				GROUP BY
					h.students_idno, 
					b.course_code,
					c.section_code
				ORDER BY 
					a.lname, a.fname";
			
		$query = $this->db->query($q1);
		
		if($query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
		
	}
	
	//Added: Isah on 1/25/2014
	
	function MatriFee($history_id, $fees_groups_id)
	{
		$result = null;
			
		$q = "SELECT sum(a.rate) as total_fee, b.description
		FROM
		fees_schedule as a,
		fees_subgroups as b,
		fees_groups as c,
		student_histories as d,
		prospectus as e,
		academic_programs as f
		WHERE
		d.id = {$this->db->escape($history_id)}
		AND d.academic_terms_id = a.academic_terms_id
		AND d.year_level = a.yr_level
		AND a.fees_subgroups_id = b.id
		AND b.fees_groups_id = c.id
		AND c.id = {$this->db->escape($fees_groups_id)}
		AND d.prospectus_id = e.id
		AND e.academic_programs_id = f.id
		AND f.acad_program_groups_id = a.acad_program_groups_id";
		//print($q); die();
		$query = $this->db->query($q);
			
			
		if($query && $query->num_rows() > 0){
		$result = $query->row();
	}
		
	return $result;
	}
	//Added: 6/28/2014  by Isah
	function getFeesBed($acad_terms_id, $fees_groups_id, $level, $yr_level)
	{
		$result = null;
			
		$q = "SELECT sum(a.rate) as total_fee, c.fees_group as description
				FROM
					fees_schedule as a,
					fees_subgroups as b,
					fees_groups as c
				WHERE
					a.academic_terms_id = {$this->db->escape($acad_terms_id)}
					AND a.yr_level = {$this->db->escape($yr_level)}
					AND a.levels_id = {$this->db->escape($level)}
					AND a.fees_subgroups_id = b.id
					AND b.fees_groups_id = c.id
					AND c.id = {$this->db->escape($fees_groups_id)}	";
				//print($q); die();
				$query = $this->db->query($q);
			
			
		if($query && $query->num_rows() > 0){
		$result = $query->row();
	}
		
	return $result;
	}
		
}