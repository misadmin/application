
	<?php
		if ($prospectus_courses) {
			foreach($prospectus_courses AS $prospectus_course) {
	?>
				<div style="height:38px; background-color:#B5D4B8; border:solid 1px #9DAFA0; margin-bottom:10px;">
					<div style="float:left; font-size:16px; font-weight:bold; padding-top:10px; padding-left:10px; padding-right:10px; color:#39393A; width:200px;">
						<?php print($prospectus_course->grade_level." - ".$prospectus_course->term); ?>	
					</div>
					<div style="float:left; padding-top:4px;">
						<a href="<?php print($prospectus_course->id); ?>" 
							class="btn btn-primary add_term_course" >
							<i class="icon-plus-sign" style="margin-top:2px;"></i> Add Course
						</a>
					</div>
					<div style="float:left; padding-top:4px; padding-left:10px;">
						<a href="<?php print($prospectus_course->id); ?>" 
							class="btn btn-danger delete_term" >
							<i class="icon-trash" style="margin-top:2px;"></i> Delete Term
						</a>
					</div>
				</div>
				
				<div style="overflow:auto; margin:0 auto; width:100%;" class="show_extracted_terms_courses" >
					<?php
					
						$data['year_level'] = $prospectus_course->year_level;
						$data['pros_term']  = $prospectus_course->pros_term;
						if (isset($prospectus_course->courses)) {
							$data['detailed_courses'] = $prospectus_course->courses;
						} else {
							$data['detailed_courses'] = array();
						}
						
						$this->load->view('bed/prospectus/list_all_prospectus_detailed_courses',$data);					
					?>
				</div>
				
	<?php
			}
		}
	?>
		
	

<script>
	$('.add_term_course').click(function(event){
		event.preventDefault();
		
		$('#show_error_msg').html('');

		$('#modal_new_term_course').modal('show');
		
		$('#term_id_modal').val($(this).attr('href'));
		
	});
</script>				


	<div id="modal_new_term_course" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_term_course_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Add Term Course</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="term_id_modal"></div>
						<?php
							$data['subjects'] = $subjects;
							$this->load->view('bed/prospectus/modal_view/new_term_course_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Course!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<!-- JQ to submit an update to a volunteer -->
<script>
	$('#add_term_course_form').submit(function(e) {
		e.preventDefault();
		
		var term_id   = $('#term_id_modal').val();
		var course_id = $('#course_id_add_modal').val();
		var stype_id  = $('#stype_add_modal').val();

		$('#modal_new_term_course').modal('hide');
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/prospectus_courses_management')."/".$prospectus_id); ?>",
			data: {"term_id": term_id,
					"course_id": course_id,
					"stype_id": stype_id,
					"action": "add_prospectus_term_course" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_terms').html(response.output); 
				
			}
		});
		
	});
</script>				


<script>
	$('.del_prospectus_term_course').click(function(event){
		event.preventDefault(); 

		$('#show_error_msg').html('');

		var confirmed = confirm('Are you sure you want to delete this Course?');

		if (confirmed){
			
			var prospectus_course_id = $(this).attr('href');
			var courses_id           = $(this).attr('courses_id');

			$('#show_trash_icon_'+prospectus_course_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/prospectus_courses_management')."/".$prospectus_id); ?>",
				data: { 
						prospectus_course_id: prospectus_course_id,
						courses_id: courses_id,
						action: 'delete_prospectus_course'},
				dataType: 'json',
				success: function(response) {	
					$('#show_extracted_terms').html(response.output); 
					$('#show_error_msg').html(response.msg_error);
				}
			});
		
		}

	});
</script>


<script>
	$('.delete_term').click(function(event){
		event.preventDefault(); 

		var confirmed = confirm('Are you sure you want to delete this Term?');

		if (confirmed){
			
			var prospectus_term_id = $(this).attr('href');

			$('#show_trash_icon_'+prospectus_term_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/prospectus_courses_management')."/".$prospectus_id); ?>",
				data: { 
						prospectus_term_id: prospectus_term_id,
						action: 'delete_prospectus_term'},
				dataType: 'json',
				success: function(response) {
					if (!response.success) {
						$('#show_error_msg').html('ERROR: Term cannot be deleted because it contains courses!');
					} 
					
					$('#show_extracted_terms').html(response.output); 
				}
			});
		
		}

	});
</script>


<script>
	$('.update_type').click(function(event){
		event.preventDefault();
		
		$('#show_error_msg').html('');

		$('#modal_update_course_type').modal('show');
		
		$('#prospectus_courses_id_update_modal').val($(this).attr('href'));
		$('#descriptive_title_update_modal').val($(this).attr('descriptive_title'));
		$('#subject_types_id_update_modal').val($(this).attr('subject_types_id'));
		
	});
</script>				


	<div id="modal_update_course_type" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_term_course_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Subject Type</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="prospectus_courses_id_update_modal"></div>
						<?php
							$data['s_types'] = $s_types;
							$this->load->view('bed/prospectus/modal_view/update_course_type_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#update_term_course_form').submit(function(e) {
		e.preventDefault();
		
		var prospectus_courses_id = $('#prospectus_courses_id_update_modal').val();
		var stype_id             = $('#subject_types_id_update_modal').val();

		$('#modal_update_course_type').modal('hide');
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/prospectus_courses_management')."/".$prospectus_id); ?>",
			data: {"prospectus_courses_id": prospectus_courses_id,
					"stype_id": stype_id,
					"action": "update_prospectus_course_type" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_terms').html(response.output); 
				
			}
		});
		
	});
</script>				

