<h2 class="heading">Add New Privilege </h2>
<body onLoad="show_offering();">

<form id="new_privilege" method="post" action="" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value="new_privilege" />

	
	<div class="control-group">
					<label class="control-label" for="abbreviation">Privilege Code</label>
		   		 	<div class="controls">
		      			:&nbsp&nbsp<input type="text" id="pcode" class="span1" name="pcode">
		   		 	</div>
		</div>
    
	<div class="control-group">
					<label class="control-label" for="abbreviation">Privilege Name</label>
		   		 	<div class="controls">
		      			:&nbsp&nbsp<input type="text" id="privilege" class="span3" name="privilege">
		   		 	</div>
		</div>
		
	<div class="control-group">
					<label class="control-label" for="abbreviation">Maximum units (1st-3rd Year)</label>
		   		 	<div class="controls">
		      			:&nbsp&nbsp<input type="text" id="max_under" class="span1" name="max_under">
		   		 	</div>
	</div>
	<div class="control-group">
					<label class="control-label" for="abbreviation">Maximum units (4th/5th Year)</label>
		   		 	<div class="controls">
		      			:&nbsp&nbsp<input type="text" id="max_grad" class="span1" name="max_grad">
		   		 	</div>
	</div>
    <div class="control-group">
    	 <div class="controls">
    		<button class="btn btn-primary" type="submit">Create New Privilege</button>
    	</div>
	</div>
</form>

</body>

