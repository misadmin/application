<div class="row-fluid">
	<div class="span10" style="margin-bottom:20px;">
		<select name="show" id="limit_show" multiple="multiple">
			<optgroup label="Teller Visibility">
				<option value="visibility_yes">Visible to Teller</option>
				<option value="visibility_no">Not Visible to Teller</option>
			</optgroup>
			<optgroup label="Ledgered or Non-Ledgered">
				<option value="ledgered_yes">Ledgered</option>
				<option value="ledgered_no">Non-Ledgered</option>
			</optgroup>
			<optgroup label="VAT or Non-VAT">
				<option value="vat_VAT-Type">VAT</option>
				<option value="vat_non-VAT">Non-VAT</option>
			</optgroup>
			<optgroup label="Status">
				<option value="status_Inactive">Inactive</option>
			</optgroup>
		</select>
	</div>
</div>
<div class="row-fluid">
	<div class="span10">
		<table class="table table-bordered table-hover" id="teller_code_table">
			<thead>
				<tr>
					<th width="10%" style="text-align: center; vertical-align:middle">Teller Code</th>
					<th width="40%" style="text-align: center; vertical-align:middle">Description</th>
					<th width="10%" style="text-align: center; vertical-align:middle">Default Amount</th>
					<th width="10%" style="text-align: center; vertical-align:middle">Visible to Teller</th>
					<th width="10%" style="text-align: center; vertical-align:middle">Ledgered</th>
					<th width="10%" style="text-align: center; vertical-align:middle">Tax Type</th>
					<th width="10%" style="text-align: center; vertical-align:middle">Status</th>
				</tr>
			</thead>
			<tbody>
	<?php if (is_array($items) && count($items) > 0): ?>
	<?php foreach ($items as $item): ?>
				<tr>
					<td><?php echo $item->teller_code; ?></td>
					<td><a href="#" class="item" item_id="<?php echo $item->id; ?>" teller_code="<?php echo $item->teller_code; ?>" description="<?php echo $item->description; ?>" default="<?php echo $item->default_amount; ?>" teller_visible="<?php echo $item->teller_visible; ?>" tax_type="<?php echo $item->tax_type; ?>" status="<?php echo $item->status; ?>" is_ledger="<?php echo $item->is_ledger; ?>" applicable_levels="<?php echo $item->applicable_levels; ?>"><?php echo $item->description; ?></a></td>
					<td style="text-align:right"><?php echo $item->default_amount; ?></td>
					<td style="text-align:center"><?php echo ($item->teller_visible == 'N' ? 'No' : '<strong>Yes</strong>'); ?></td>
					<td style="text-align:center"><?php echo ($item->is_ledger == 'N' ? 'No' : '<strong>Yes</strong>'); ?></td>
					<td style="text-align:center" ><?php echo ($item->tax_type == 'NV' ? 'non-VAT' : 'VAT-Type'); ?></td>
					<td style="text-align:center"><?php echo ucfirst($item->status); ?></td>
				</tr>
	<?php endforeach; ?>
	<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="myModalLabel">Edit this Item</h3>
	</div>
	<div class="modal-body">
		<form method="post" class="form-horizontal" id="update_item_form">
			<?php action_form('update_item', FALSE);?>
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" name="item_id" id="item_id" value="" />
			<fieldset>
				<div class="control-group formSep">
					<label class="control-label">Item Code</label>
					<div class="controls control-text" id="teller_code"></div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="description">Description</label>
					<div class="controls"><input type="text" id="description" name="description" value="description" /></div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="default_amount">Default Value</label>
					<div class="controls"><input type="text" id="default_amount" name="default_amount" value="default_value" style="text-align:right" /></div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="teller_visible">Visible to Teller?</label>
					<div class="controls">
						<select name="teller_visible" id="teller_visible">
							<option value="Y">Yes</option>
							<option value="N">No</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="is_ledger">Is Ledgered?</label>
					<div class="controls">
						<select name="is_ledger" id="is_ledger">
							<option value="Y">Yes</option>
							<option value="N">No</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="tax_type">Tax Type</label>
					<div class="controls">
						<select name="tax_type" id="tax_type">
							<option value="NV">Non-VAT</option>
							<option value="V">VAT</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep" id="item_level">
					<label class="control-label" for="status">Applicable to</label>
					<div class="controls">
						<?php foreach ($this->config->item('teller_code_levels') as $key => $level) : ?>
						<label class="checkbox">
							<input type="checkbox" class="item_level" value="<?php echo $key; ?>" name="applicable_levels[]"/><?php echo $level?>
						</label>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="status">Status</label>
					<div class="controls">
						<select name="status" id="status">
							<option value="active">Active</option>
							<option value="inactive">Inactive</option>
						</select>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" id="update_item">Update Item</button>
	</div>
</div>
<style>
.paginate_button, .paginate_active{padding:10px;cursor: pointer;}
</style>
<script>
function in_array(needle, haystack) {
    for(var i in haystack) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
$().ready(function(){
	$('.item').bind('click', function(){
		$('#item_id').val($(this).attr('item_id'));
		$('#teller_code').html($(this).attr('teller_code'));
		$('#description').val($(this).attr('description'));
		$('#default_amount').val($(this).attr('default'));
		$('#teller_visible').val($(this).attr('teller_visible'));
	
		var tax_type = $(this).attr('tax_type');
		$('#tax_type option[value="' + tax_type + '"]').attr("selected","selected");
	
		var status = $(this).attr('status');
		$('#status option[value="' + status + '"]').attr("selected","selected");
	
		var is_ledger = $(this).attr('is_ledger');
		$('#is_ledger option[value="' + is_ledger + '"]').attr("selected","selected");

		var levels = $(this).attr('applicable_levels').split(',');
		$('.item_level').each(function(){
			if(in_array($(this).val(), levels)){
				$(this).prop('checked', true);
			} else {
				$(this).prop('checked', false);
			}
		});
		if($(this).attr('teller_visible')=='N'){
			$('#item_level').css('display', 'none');
		} else {
			$('#item_level').css('display', '');
		}
		$('#myModal').modal('show');					
	});
	$('#myModal').on('hide', function(){
		$('.item_level').each(function(){
			$(this).prop('checked', false);
		});
	});
	
	var oTable = $('#teller_code_table').dataTable({
		"sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aaSorting": [[0, 'asc']],
        "bAutoWidth": true,
	});
	$('#limit_show').multiselect({
		enableCaseInsensitiveFiltering:false,
		onChange:function(element, checked){
			var visibility_filter='';
			var ledgered_filter='';
			var vat_filter = '';
			var status_filter = '';
			$('#limit_show').find(":selected").each(function(index, val){
				var filter_val = $(this).val().split("_");
				var filter = filter_val[0];
				var val = filter_val[1];
				switch(filter){
					case 'visibility' :
						visibility_filter += val + "|";
						break;
					case 'ledgered' :
						ledgered_filter += val + "|";
						break;
					case 'vat' :
						vat_filter += val + "|";
						break;
					case 'status' :
						status_filter += val + "|";
						break; 
				}
			});
			visibility_filter = visibility_filter.replace(/\|+$/,'');
			ledgered_filter = ledgered_filter.replace(/\|+$/,'');
			vat_filter = vat_filter.replace(/\|+$/,'');
			status_filter = status_filter.replace(/\|+$/,'');
			oTable.fnFilter(visibility_filter,3,true);
			oTable.fnFilter(ledgered_filter,4,true);
			oTable.fnFilter(vat_filter,5,true);
			oTable.fnFilter(status_filter,6,true);
		} 
	});
	$('#update_item').on('click', function(){
		$('#update_item_form').submit();
	});
});
</script>