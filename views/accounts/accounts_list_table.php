<?php 
class accounts_list extends table_lib{
	function format_data_account_number ($data){
		
		return '<a href="payors/' . $data['account_number'] . '">' . $data['account_number'] . '</a>';
	}
	
	function format_cell_account_number ($data){
		return ' style="text-align: center;"';
	}
	
	function format_cell_type ($data){
		return ' style="text-align: center;"';
	}
	
	function format_cell_tenant ($data){
		return ' style="text-align: center;"';
	}
}

$accounts_list = new accounts_list();

$accounts_list->set_attributes(array('class'=>'table table-bordered','id'=>'accounts_list'));

$head_row = array(
		array('id'=>'account_number',
				"value"=>"Account Number",
				"attributes"=>array('id'=>'account_number', 'style'=>'width:15%; text-align:center;')
		),
		array("id"=>"account_name",
				"value"=>"Accounts Name",
				"attributes" => array('id'=>'account_name','style'=>'width:55%; text-align:center;')
		),
		array("id"=>"type",
				"value"=>"Type",
				"attributes" => array('id'=>'type', 'style'=>'width:15%; text-align:center;')
		),
		array("id"=>"tenant",
				"value"=>"Tenant/Non-Tenant",
				"attributes" => array('id'=>'tenant', 'style'=>'width:15%; text-align:center;')
		)
);

$accounts_list->insert_head_row($head_row);
$accounts_list->insert_data($data);

?>

<h2 class="heading">List of Accounts</h2>
<div class="row-fluid">
<?php echo $accounts_list->content() ?>
</div>
<script>
$().ready(function(){
	$('#accounts_list').dataTable();
});
</script>