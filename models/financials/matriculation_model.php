<?php

	class Matriculation_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function AddCollege_Matriculation($data) 
		{
			foreach($data['academic_programs'] as $program)
			{
				$query = "INSERT INTO matriculation_fees (id, academic_programs_id, academic_years_id, rate, yr_level) 
   						      VALUES ('',
									  {$this->db->escape($program->id)},
									  {$this->db->escape($data['academic_years_id'])},
									  {$this->db->escape($data['rate'])},
									  {$this->db->escape($data['yr_level'])})";
				$this->db->query($query);
									
			}						
			
			$id = @mysql_insert_id();
			
			//print($query);
		//	die();
			return $id;
			
			
		}
				
		
		function AddGraduate_Matriculation($data) 
		{
			$query = "INSERT INTO matriculation_fees (id, academic_programs_id, academic_years_id, rate, yr_level) 
   				      VALUES ('',
						  	  {$this->db->escape($data['programs_id'])},
					  	      {$this->db->escape($data['academic_years_id'])},
						      {$this->db->escape($data['rate'])},
						      {$this->db->escape($data['yr_level'])})";
				
			$this->db->query($query);					
				
			$id = @mysql_insert_id();
			
			return $id;
		}
		
		
//Added: 3/2/2013

function ListMatricFees($data) {
			$result = 0;
			$q = "SELECT 
						a.id,
						b.description, 
						b.abbreviation,
						a.yr_level,
						a.rate,
						a.posted					
					FROM
						matriculation_fees AS a, academic_programs as b
					WHERE 
					    b.colleges_id = {$this->db->escape($data[college_id])}
						AND a.academic_programs_id = b.id
						AND a.academic_years_id = {$this->db->escape($data[academic_years_id])}
					ORDER BY
					    b.description, a.yr_level";
			
			//print($q);
		//	die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}		
		
function getMatFee($fee_id) 
		{
			$result = null;
			
			$q = "SELECT a.rate, a.yr_level, a.academic_years_id, b.description 
					FROM
						matriculation_fees as a, academic_programs as b, academic_years as c
					WHERE
						a.academic_programs_id = b.id
						AND a.id = {$this->db->escape($fee_id)} "; 
			
				
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}	
		
  function UpdateMatricFee($id, $matf){
	$q = "UPDATE matriculation_fees 
			SET 
				rate={$this->db->escape($matf)} 
			WHERE
				id={$this->db->escape($id)}";
			
		if ($this->db->query($q)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
				
}

//end of college_model.php