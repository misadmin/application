<style>
.m_slip {
	font-size: 11px;
	font-weight: bold;
	color: #002460;
	width: 500px;
}
.m_slip input{
	border-radius: 0;
	font-weight: bold;
	font-size: 19px;
	margin: 0;
	height: 20px;
	background: #F3F5F8;
	color: #182C97;
}
.m_slip input:focus{
	background: #D1D5E0;
}
.m_slip input.digit{
	border: solid 2px rgb(0,36,96);
	width: 12px;
	margin-right: -5px;
}
.m_slip input.last{
	margin-right: 1px;
}
.m_slip input.unbordered{
	border: solid 0 #000;
	width: 357px;
}
.m_slip input.pieces,
.m_slip input.coins,
.m_slip input.inline-text{
	border: solid 0 #000;
	width: 87%;
	font-size: 13px;
	text-align: center;
	padding: 0;
}
.m_border {
	border: solid 2px rgb(0,36,96);
}
.m_slip table td{
	border: solid 2px rgb(0,36,96);
	text-align: center;
}
.m_slip table td:first-child{
	border-left: solid 0;
}
.m_slip table td:last-child{
	border-right: solid 0;
}
.m_label {
	line-height: 28px;
	padding: 0 30px 0 5px;
	float: left;
	margin-right: 3px;
	display: block;
}
.m_container {
	clear: both;
	margin: 0 0 5px 0;
	padding: 0;
}
.m_sub {
	border-top: solid 2px rgb(0,36,96);
}
</style>
<div class="row-fluid">
	<div class="span12">
		<?php if (isset($popup)):?>
		<a data-toggle="modal" href="#myModal" class="btn btn-primary btn-large">Metrobank Deposit Slip</a>
		
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    <h3 id="myModalLabel">Metrobank Deposit Slip</h3>
		  </div>
		  <div class="modal-body">
		<?php endif; ?>
			<center>
			<div class="m_slip">
				<form id="metro_bank_deposit_slip_form" action="<?php echo site_url('teller/deposit');?>" method="post">
					<div class="m_container m_border">
						<img alt="" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QCsRXhpZgAATU0AKgAAAAgACQEaAAUAAAABAAAAegEbAAUAAAABAAAAggEoAAMAAAABAAIAAAExAAIAAAASAAAAigMBAAUAAAABAAAAnAMDAAEAAAABAAAAAFEQAAEAAAABAQAAAFERAAQAAAABAAAOw1ESAAQAAAABAAAOwwAAAAAAAABgAAAAAQAAAGAAAAABUGFpbnQuTkVUIHYzLjUuMTAAAAGGoAAAsY//2wBDAAQDAwMDAgQDAwMEBAQFBgoGBgUFBgwICQcKDgwPDg4MDQ0PERYTDxAVEQ0NExoTFRcYGRkZDxIbHRsYHRYYGRj/2wBDAQQEBAYFBgsGBgsYEA0QGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBj/wAARCAA1AiUDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD4/owT2NXdN02+1fU7fTtMs7i7vJ3EcNtbxmSSVj/CqqCSeewJr7O+DX7GCW9oviz41XKWlrGhn/sGKcIFQDJN1MDhRjJKqfqw5WvvsTjKeHjzVH8urPmKNCdV2ij50+E3wK8e/GHV/J8Oaf5GmxvtudYugUtoe5GcfO3+wuT0zgc19KeNf2DLeHwHBJ4D8V3N54ht48zw6oqRwXjAc+WVGYT6Biw6ZI5avqjR/E3gnSPhK/iTSUttL8IadbPLBNFB5MBt4wcyRIAMocHaQPn4K5DAnW1DxRo+lDRDqVx9lGs3SWVoZMANM8bSIhPYkIQPVsDuK+br5tiZ1LwVkun+Z69PA0YxtLVn48eJvC2v+DvEl1oHibSbrS9RtjiS2uY9rj0I7Mp6hlJBHIJrEr9bvih4Q+GHxKvbbwD4+sY11G6gebSrk4inO3/WfZpe7rlS0ZyCCCVIzj4Q+Nf7LHjj4VzXGtWCt4h8LqS39o20eJLZewuIx9z03jK+u0nFexgs1hXtGfuyf3P0PPxGClTu46o8AopzjBHTkZ4ptescIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXt/7IH/J53g3/t9/9IbivEK9v/ZA/wCTzvBv/b7/AOkNxXNjf93qf4X+Rth/4sfVfmfqfRRRXwB9QFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAfF37CDfDweH9aXdYf8ACdG6J/f4FwbLYmBDn+EPv3be+3dxtr6Y+Kf/AArv/hAXPxR2f8I55yed5wmMG7Py+b5XbOMbuM474r5o/ZB+FngXxp+zvqmpa3ocbau2tSxw6vAzQ3tr5ccTI0MyndGVLE/LgHuCOK9suPEHjP4Y28tj8R7eXxj4KKMh8S21sHurSI8bb+2UfvE25BmjGMDLIM5r18alLFScW7p7bP5P+vmcOHbjRSktP63On+Ih+GQ+EZPxA+xf8Id+4znzDbbcjys+V/Dnbjtnb7U34kH4YDwdpJ+Iotf7I+3Qf2d5olP+lbW8ry/L+bfjdiruj+HfBGtfCdvDukPa6r4Q1G2khhihn86AW8gIMcTgnCDJ2gH5OAMBQBrX/hjR9UGijUrYXY0a5S8sxLztmSNo1cjuQHYj0OD2FcCnGLSbejf9eT7nS4tq9lqc98UW+GK+GNP/AOFoCzNib+L7CLhZDIbvB2eV5fz+ZjdjbzXZx+R/Z6H5vI8sf6/OduP4t3PTru59a4Lx9r3gHwd4o07xP4lSXU/EflPbaHpVtEbq8kLcuLWAdHbADScAAKCwHXKTwZ4w+Jsgvfim/wDY/h0sHh8F6fcbvOHb7fOv+tOefJQiPgZL01BOCcm0v62X67C5rSaSu/63Z+fH7SZ8AN+0XrZ+G/2P+xz5e/7Dj7MZ9v70w442bvTjO7HFd7+yP8E/Bfxfn8WN4zgvpYdMW1FuLW4MGGkMu7OBz/qxWF+134W8O+Ef2k7vTvDGjWek2UtjbTm1tIxFErspDFUHCg7RnGOa97/4J/WBi8DeNNSxgT31tAP+ARM3/tSvpsTWcMAp0272WvXoeRRpqWKcZLqzxz9rD4DaF8I9a8P6h4OivE0TU4ZIpFuZjMY7iMgn5j03IwIH+w1fN6DLZyABz61+o/7SHhiy+Kf7KWtXOkqtzcacDqtmRyQ9uWWVeO5QTJj1r8//AIF+A/8AhY/x+8OeFpoPMspLkXF7wSPs0X7yQE9gwXaD6sB3qsuxjnhnKq9Y3uTi8Py1kobPY+vPhX+xx8ONS+Dfh7U/G9nqz67e2q3l0sV48Kx+Z8yx7B0KoVB75Br4g+IWh2vhj4teKPDdiHFrpmq3VnAJG3N5ccrIuTjk4Aye9fsFDrVhJ4ruvDkLf6ZaWcN5Kg6JHK8iJ+Zhf8q/Kf8AaE082f7VPjm0iQkyaxNKqgEkmQ78ceu6uXKcVVrVpqo3textjqMKdOPIjy7vRX2Z8NP2Hzf+EYvEPxV8S3GhCaISnTbJY1lgQjIM0sgKq3PKhTjuc5xo+O/2F9L/AOEOm1j4UeLbvUrqONpY7LUmikW7xztjmjChWOMDIIJ6letd7zXDKfJzfPp95yrA1uXmsfE8MMk8qxxIXdiFCKCSTnoAOT+FdFr3gDxn4W0Oy1jxJ4a1PSLK+kaO2lvoGh84gAttVsNgAjnGPevof9kT4H2PjLxNP401TVr3Tr/wprVs62KwKRIyfOVfd8ynK4Ppz3r62+OPwO0z436Ro9hqfiC80hNMmkmVraJJPM3qFIO7pgDg+5rHE5rCjXVJ7dWa0cDKpTc+vQ/Juivoew/Z80W7/bSvPgi3iK8WxgRmXUVgTzWItVnwV+6OWx9B617RZ/sDaN/wmkq6h431B/DqQIU8m3jS7llJbeMkFVQDbg7SSSemMnoqZlh6duZ7q+3RmMMHVnflWzsfCFFe1+PfgTd2H7Vl78Hfh7He6tIph8h7woHVHgSRnkZVCqq7jlsdB0J4P0ToP7Bvg2y0OKXxz481J71wA/8AZoitoEY/wqZVct9flz6CnVzGhSjGUnvqu4QwlSbaitj4Lr6C/ZO+E/g/4tfEHXdJ8ZW93Na2enC5iW2uDCd5lVeSOowTxXcfGH9inVfCPhi68TfD3WZ/EFpaRmW4026iC3SxqMl42X5ZMDnbtU4HG48U79gVdnxg8Ug4/wCQMOn/AF3SscTjYVcLOpQlt96NKWHlCvGFRHkP7SXgDw78NPj7f+E/CsNxDpkNrbyok8xlbc8YYncecZ7dq8h7V+k3xM/ZTsfit8d9W8c+KvE8um6RJb28EFtYKpmfZGFZneQFUGcgABieuR0PnXxC/YPtLbwzcaj8M/E99eX8KGRNN1cRn7TjnakqBQrYGBuBBPUjOazw2a0FCEJy1sr+vqVWwNRylKK0Ph6iuq8KeDL/AMUfFLTfAz3FtpN9fXq2HmagGjWGUtt2uMZDbhtxj73FfaenfsJfDixsooPFHxA1qa9fhTa/Z7RGPoqSK5P5124nH0cO0qj1Zz0sLUq6xR8BUV9Q/Hv9ki++FnhKbxl4V1iXW9CgYLeRXMSpc2gYhVclfldMkAkBSMjgjJFT9nL9mvQvjZ4I1bWtT8T3+lTWF8LURW1vHIGUxhsksc9SR6cUv7QoOl7ZP3R/VanP7O2p800V94+Ff2CPDw04v448Z6i107uI4NIWOJUXJ27nkVt7YwTgAA8DOMnyj9oD9k7UPhR4bPi/w1q8ut+HY5FS6FxGEuLPcdqs235XQtgbgFIJHHORFPNMPUn7OMtSpYKrCPO0bP7J/wABfh78WvA2v6r4xtdQmuLK+jt4fs100ICmPccgdTk186/EjRLDwz8YvFXhzSlkWx03V7qztxI+9hHHMyKC3c4Aya+2P2A+PhZ4sHb+1o//AESK+fJ/hH4i+MH7afjXw3oqeTar4gvZb+/dS0dnD9pfLn1Y9FXufYEjmo4lxxVb2kvdil8jWpRTo0+Vas8B70V9N/Hv4AfDL4KeF4jJ481XU/EV4oNjpItolyoODLKQcrHwQD1Y8DoxX5lYgudvTtXpYevCvHnhsclWlKm+WW4lFFFbmQUUUUAFFFFABRRRQAV7f+yB/wAnneDf+33/ANIbivEK9v8A2QP+TzvBv/b7/wCkNxXNjf8Ad6n+F/kbYf8Aix9V+Z+p9FFFfAH1AUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB8w/sKf8m03w/wCo9cf+ioaxfAf7Z2lJ431Lwj8U7RNOEF/Pa2+tWaFodqyMqieMZK4AHzrkc8gYJrZ/YUwP2ar/AB/0H7jPt+5hrybxF+w38S9V8Yatq1r4n8KrDd3c08YlluAwV5GYA4iODgjPJ+pr3nHDTxFaOIdtdDzFKrGlTdJXPpif4fSWMx8dfAjxBp2mPfn7VPpW7zdF1jI+/tTPkyHj97F1x8ytk1APHnxd8ZMfDXhv4b3vgvU4wI9T1zxFsmtLInGfsiof9MbGSp+VBgbuuK8k+FH7P37SXwg1TzPDXj3wjNpcjbrjR7ua5e2lPchfKGxv9pcHgZyOK+g/FD/GO88KfZvCNh4N0zWJF2td31/PcRQHHJRBbrvOem4gdyD0rjqqMZWUoz7N9PXv+P6HRBtxu04+X+RzNx/wq39nzQ7rxj4z8RS3mv36kXOs6o/2jUdRYc+XEg+6g4AjQKijGema4/4IftK6j8avj7q+g2ejRaV4bstKkuraKXD3MriaNA8jg7V4Y/IvTP3m4ryDxN+xp8dfGfiKfX/FXxC8N6rqU5/eXFzc3LNjso/c4VR2UAADoBXp/wCzX+zT4w+DPxL1PxL4i1rQ723utMaySOweVn3mWN8neigDCdvWuipTwsaMpSnzTa/qxlGVZ1ElG0T52/bd5/aonHH/ACC7T+TV9DfsG2vlfs+61d4H7/X5APosEI/nmvnr9tvn9qqYf9Qy0/k1fUH7Gdqum/sjW94cAXF/e3JP+62z/wBp11Yx2y6C72MKC/2uXzGfsn/ECDxh4X8Z+Fbpg8uk67dTRxtyDbXM0kg69fn83P1FZf7MXwYHw9+LnxM1a5twiWeonRdMZiSwtjtuCxz6pJbc/wCya+af2S/Hz+Ff2o9Pju5vLsvEIfTJ8njfI26I47nzFVc9g5r7g/aM8dJ8Ov2cvEesW8og1G+j/s6yZTtYzzDZvB/vKgZ/+AVz4ylOlXlQp7VLfma4ecZ01Ul9i55v+zz8RP8AhYn7VPxd1iGfzbGRbSGwIbK/Z4HliRl9mzv+rmvNZvCllrf/AAVtuLK/hSS2guU1Uo3ILR2KSp/5ECn8KyP2B7rZ8ZfE1n2k0USAZ/uzoP8A2atf4p+LbL4Xf8FOLHxlqZK6fJBai7b+5DLbm3Z/+A43cddpHet3SdPE1adP+Sy+5GanzUYTn/N+rPof48fBLVvjXpumaRH48l8P6VaM0stnHZfaFuZTja7/ALxeFGcDB+8T6YZ8BPgZqPwQstX03/hOZte02/aOWO0ey+zrbyrkM6/vG+8CoPA+6OtYX7Rnwf8AFPxX0XRvEvw18UG21K0hZRAl88MF/A+HVldDt3Dkgnhgx5GBXzfon7L37T2q34g1LU5dEh6Ge819pFH0WJnJz6YH1FclCKqYf2cqyiuzSv8A5m9R8tXmVNt97i+LbOLQv+Co0On6Y7QW0/ifT7mSKNiFZ5VhkfIHHLOx/GvT/wBvu5uLbwb4Ke2nliY3tyCY3KnHlp6GvlL4c31zo/7UXha88TXEpuLLxDbJez3chZlK3CoxdmOflxySeg9q+6P2tPhB4x+LXgvw9B4Mtre7u9OvZHlt5Z1hJR0xuDMQOCoyOvPFd9flo4mhzvRK1/kctK9SjU5Vq3sfJX7H00tx+2F4dllleRzDeEs7Ek/6M/UnrXun7c3xI8U+HR4a8HeH9XutMtb6Ga8vXtZWiebawRELKQdo+YkZwSRkcV5R+zV4T1TwP+3pp/hTW1iXUdOW9guBDJvQMLZjwe45rpv2/wD/AJKT4Q6/8guX/wBHVdVRqZhTb1XLf8yYOUcLLvf/ACOx/YP0f+0dO8YePtUnlvdVmnh0tLq5cySrGqCRhubJwcx/98D0r50/aa+IGt+Nf2jfEcF/eTPp+kX0um2NmzExQJE2wkL0yzKWJ684zgAD2H9hP4i6bpHibXPh1qlylvJqxjvNOLsAskyKVeMf7TLtIHfY30qf9oX9kvx5rfxj1Lxh8O9Og1ew1qY3U9r9pjgltZ25kz5jAMrNlgQeNxBHAJUJwo4+braXWje3QcoyqYWKp666nof7Dnj/AFrxR8Ktb8L6zeTXo8P3EItJpmLMsEqtiLJ5IVo2xnoGAHAAGZ+z54ds/Cn7ePxa0HT40itIIGeCJBhY0knjlCAdgA+Pwr0D4DfDOy/Zy+BWr6p441SytrudzqOq3CPmK2jRdqRBurkDPTqzkDPBPkf7JfitvHX7W3xK8ZGN4l1S0e5SN8Exo1yuxT7hQo/CuKbU3iKlP4LL77r/AIJ0RTiqUZ/EcP8AtweOvEN98cE8Ef2hPDoemWcMq2cchEcs0il2ldR95sFVGc4AOMbjnuf2EfH+u3134g+H2oXk91p1rapqFisjlhanfsdFznCtuVscAEEgcmtf9qb9mTxl8RfiFH498BrbahPNbR217ps9wsEm5MhZI2fCkbSMgkEbeM7uOu/Ze+A2ofBXw/rPibxtc2VvrGoQqkkUcweOyt48u2+T7pYnk44AQcnmtalfDvL1TTXNZadbkQpVfrTk9v0Pmj9ozwbrC/t03ul+ArKZtY1Sa1v7OG0GHFw6BmcHgD50ZyxwByxIAJr1nTP2KNV12V/E/wAXvijczapP+8uhbKJip/2riY8n/gAA7cYqv8HPiH4e+If/AAUj8TeJxLE1vdadPZ6I0nV/JESBkzyC8aSuB1wW963P2qvg58Zvid8U9Ik8J2rap4ZSzSIW5v0gitptzb3eN2GSQUO4AnC4xxzc69WMqeHclC0Vdv8ALUmNKDUqqXNrse4eJfD9hp37JeveGl1e41yytfDV1ape3UiyyTqkDhSzKAGYYAz7eteH/sCHPww8Xds6pEf/ACAK9gsPA8/w8/YtvfBE1zHeXWn+GryKaWIHY8rQyO+3gEruYgcAkY4FeP8A7Af/ACS7xb/2FIj/AOQBXDFr6rWs7+8vzOl/xqd1bRnzN+0d478S+JP2mPEv2/Vbny9H1OaysIUlZUto4n2DYM4UkpuJGCSa+4NK1S68c/8ABPSbU/EErXl3eeELn7RNJ8zSukLrvJPVsoDn1r8+fjfx+0x47xwP7fvM/wDf5q++fhz/AMo3IP8AsUr3/wBFy16OYQjGhRcV1X5HLhJN1Kl+zOG/YD/5JX4sPf8AtWL/ANELXuWp6JB8I/hl4z8S+A/Cz6zrd1Nc63PBu/e3s8js5yepRAxwi87VIGWbnw39gQY+FfiwdP8Aiaxf+iFq58JP2grmH9qjxv8ACrxpqjy2t14gvI9EurmTJhkWVlFrk8BGAGwdmBHO4Y4cXSnPEVXHVKza7o6KE4xpQUt3omfCXjHxd4g8b+M7/wAT+JdRkv8AUb2QvLK3AHGAqj+FVHAUcAVz9fWH7X/wAHgzxHJ8SvCdhjw9qc3+nW8S/LYXLHqAOkchyR2VsjgMor5QYFWweK+mwlenWpKdPb8vI8evTlTm4z3EooorpMQooooAKKKKACiiigAr2/8AZA/5PO8G/wDb7/6Q3FeIV7f+yB/yed4N/wC33/0huK5sb/u9T/C/yNsP/Fj6r8z9T6KKK+APqAooooAKKKKACiiigD5a/aB+JHxS0b9qTwJ8N/Anjb/hG7LxBBDHNL/ZttebJJLh4/MxKhJwAPlDAcVY+HnxZ+JeiftFeK/gh8SNbtPEV5Zae9/puvQWUdo74iWUK8SDZgq+fUFCMsCCK/7QPw3+KWtftSeBPiR4E8E/8JJZeH4IZJov7StrPfJHcPJ5eZXBGQR8wUjmrXgH4TfEnXP2i/FPxv8AiLolp4curzTnsNN0CC9ju3QmFYgzyp8mNqH3Jcn5Qoz7S9h7Bc3L8Plfmvp57fI89+09q7X387Wt9x5f8IPi58UPiP4Wv9U8VftX+HvAtzbXX2eOy1PR9LLzpsVvMG8xnGSR0PQ811Xxs+KnxQ8F/GD4b+BdK+MNlpdhq2lWx1LxJLplk0EkjSsjXhWQFUUgBtquFA7965X4QfCP4ofDjwtf6X4q/ZQ8PeOrm5uvtEd7qesaWHgTYq+WN4kOMgnqOp4rufiX8IPHvxG/aG+FHiy5+Gllb+HrCytItc0yW8tJ4LELOzSQFSwEyqhA+RCCOAO1dFR0FXv7vLr/ACdvLX7zGCqulbXm0/m7/wBbFL4Y/tG+MbC8+Ktv4u8RWXj/AEjwhZPe2WvWFpHardsH2JH+6Gza5PB5xtYgsMVWt/H/AO0tqH7OU/7QkHjvQ4bFGe6TwiNFiaFrVJvLYmcnzQeGbbuztH3gTivo7xL8KfCer/BvxB8PNC0fTPDljq9rJERpdnHAkchHyyFEChiGCk+uMV85W/gH9pXTv2cZ/wBnuHwFodxYuz2kfi4a1EsK2zzeYwMBHmk/Mw3bc4P3SRk89KpQqNyjGKd1e9vhtrbpv217Gs4VIaNt6Pa+/T+maHxR/ad8URfAr4a+L/CaR+HbXxdLJFqetvZ/bl0kxSLHIscZ+V2yJWAYZZYjgZyRvfs/fFT4g+L/ABn4m0nUvGmh+O/DNpamex8RW8UFheb8LgSWQIkRDlxlk+8nBYEVN4h+EvxV8DfBDwR4N+GT6L4n0vRxt17w5qttbeXrAaXzZNrzqwQFmkGMggFSDkc4fwf+Bfi/T/2jNY+KmpeBdN+HOlvYSWtr4ZsdRjvC8jxqpYtH+7CEgtgYwdvy8Zpv6t7CSjbrba++nne3qu4L23tE3fpfttr5fqM/Z/8AGHxr+OXwJ1a6PxY/sLXbLXhGuq/2FaXW62FuCYPK2ogy7ht/JG3HQ1jfBrxN+0L8U/iT418Ny/HX+zY/Cl6ts0v/AAjFjN9tHmyoTjC+X/qfVvve3Pof7IHwx8cfC74W67pHjvRP7JvbrVftMMX2mGfdH5KLuzE7AcqRgnPFQ/s4/C7x14C+MPxV1zxZof8AZ9hruorPps32qGXz0E9w2cRuxXiRDhgDz7GitVpRdbk5dLcuke6vbTUIQm1T5r9b6s+jq+c/j18T/F3hv4zeGvA2m+PbD4caPqOnSXn/AAk19pqXsc9yrlRanzPkjUAKxc4xvHPY/RleK/F/SPijP4wgudG8EaJ8S/BVzY/Z7rwjqUtratBdB9wukmmQ5G3C7c8ckDoR52Dcfae9b52/XT79DrxF+TT8P+Br9xy+s/E/4r6d8PfgzeazLY6RrOv+LbPStXFg1teQXtq8hXcjr5iqJECtlGBBJwRXr3xe17VvC/wG8XeItCuvsmp6fpU9zaz+WsnlyKhKttYFTg9iCK+cH+CXxY8MfA/4f2ukeFrXWtY0TxsPFEmg2mpxwxWduCXW2SaZsYyACRuwXJ+bGT6T4g1P41/En4VeNfCGu/BD/hF2vdBuY7G5/wCEltL37RcttVINqhdmQzHexAGzB6iuupSpuUZQcbJu+qWl+zd9vUwhOaTUr3tpv27nn2v/ABR+IeofFnwn4Zb456X8OtNvvAtlrlzqOpadYyxz3jth1HnbAGYHOA2BsOF616f+zt8Q/FHja18X6V4i8Qaf4qi0DVBZWnifT7ZYItSQpuJ2p8m5eM7eMMOvU8xoPwS1LUP2gPC+seOvA2l6p4csPh7aaPN/aS213HFqEbglBExYlgu75wu3k4bmvobSdH0jQdJi0vQtKstMsYs+Xa2UCwRJnk4RQAPyqcVVoqHJBK9lqrafNajoQqc3NJ/n/wAMXaKKK8w7D8h/ht8aPiR8LLe+h8E+IWsba7IkmtpYUniZxwHCuCFbHGRjIABzgY7v/hsf4/bv+Rqsf/BXb/8AxNFFffTwtGcnKUE36Hy8a9SKtGTF/wCGx/j9jP8AwlVj/wCCu3/+Jo/4bG+P3/Q1WP8A4K7f/wCJooqPqWH/AOfa+5FfWKv8z+8T/hsf4/f9DXY/+Cu3/wDiaG/bH+PwUn/hKrHj/qF2/wD8TRRT+pYf/n2vuQfWav8AM/vPGvFHinxB4y8W3niXxPqc2pardN5k1zNgFsfKAAMBQAAAAAAAAAK9A8IftHfFjwR8O4fBfhvXLW10eESqkL2MUjASMzv8zKSclzRRW0qNOceWUU0jONSUXzJ6nltnfXenajBfWMzQXNu6TQypwyOpDKwPqCAfwrufiB8b/iZ8UdKs9O8ceI21O0s5TPDCLaKFVcrt3Hy1XJxkDOcZOMZNFFU6cJNSa1WwlOSTSejM/wCHXxL8X/C7xNN4j8GahFZahNavaSSSQJMDGWRyNrgjOUU59vc1H8QPiR4s+Jfi4eJfF97DeaiIFtRJHAkI2ISV+VABn5jzRRS9lDn57a9+oc8uXlvodX8Ov2ivi18NNMXSPDfiUvpSfMmn38S3MMfchA3KDOThSBk5xXV63+2Z8dNVtHtrfWtM0kMuGk0+wRXx7NJvx9R+FFFZSwdCUuaUFf0NI16iXKpOx4Lfaje6jqlzqV/cyXN3cytPNPM255JGYszEnqSSSfrXuvhr9sT41+GfC8eiLqmm6pFboIobjU7XzZ0UcAbwRu+rbj70UVdWhTqq04p2JhVnB3i7HAWHxj8e2Xxon+KdrqkCeJp2kke5NtGUJaPYw8vG3G3jp79ah+JPxY8bfFfU7HU/GuoQXtxZQGCBorZIcIzbiCFAzzRRTVGmpKSirrQTqSa5b6M4u3uJrW5S4t5XhliYOkkbFWUg8EEdCDyDXvnhr9sb44+H9IisJNa0/WUVNscuq2gllUD1dSrOfdiTRRSq0KdVWqRTCFWcH7rscF8Rvjf8TPii6weMvEkt3Yxtvj0+BFgt0bHB2IAGYc4ZtxGetVPhp8V/HHwr1O/v/BGpw2FxeQCGdpLaOfcituAAcHHNFFP2FNQ9nyq3boN1J83NfU7fQP2tPjbofiK/1OPxHb3q6hN581ne2qyQK5AXcijBj4HRSATkkZ5rM+Iv7Snxa+JelS6Nr+vRW2kv/rdP06EW8UuOzkZZxnnazEcDiiis1hKCkpKCv6Ddeo48rk7Hl2m6nqGla1b6rpd5PZX1tIJobm3cpJG4OQysOQc8175H+2n8dLfQRp7ano004jAF/JpyGfp14ITP/AaKKurh6VX+JFOwoVZw+F2MCz/ap+OFr4eutLfxcLy3ujM03220imd/MPzjcy5A+YgKMBRwAKwfhr8cfiR8LNIvNO8E6xb2Nte3CSzrLaRzln27QQXBxxjgUUVP1ajZx5FZ76D9tUvfmehw2va7qXifxTf+I9XlWXUNQupLq4kRAgaR2LMwUcDk9BXpOl/tGfFfRvhYvw9sNctY9AWyl08W5sYi3kuGDDft3ZO485zRRVzowmlGUU0iY1JRd09zP+Gvxz+JPws0a70vwTrFvY215cLNMslpHOS+3aCC4OOAOBXFazr+ra54yvvE2oXW7U729kvJp4l8v96z7yyhcbfmORjpRRTjSgpOSWrBzk0ot6I9W1f9qX4z6/4LuPD2ueILHUNNvLZrW4huNNgYyoRglm253d8jBB5614kzbnLetFFFKjCndQil6BOpKesncSiiitTMKKKKACiiigAooooAK9v/AGQP+TzvBv8A2+/+kNxRRXNjf93qf4X+Rth/4sfVfmfqfRRRXwB9QFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAf/Z" />
					</div>
					<div class="m_container">
						<div class="m_border m_label">ACCOUNT NUMBER</div>
						<input type="text" name="an-0" class="digit focus" maxlength="1"/>
						<input type="text" name="an-1" name="an-0" class="digit" maxlength="1"/>
						<input type="text" name="an-2" class="digit last" maxlength="1"/>
						<input type="text" name="an-3" class="digit last" maxlength="1"/>
						<input type="text" name="an-4" class="digit" maxlength="1"/>
						<input type="text" name="an-5" class="digit" maxlength="1"/>
						<input type="text" name="an-6" class="digit" maxlength="1"/>
						<input type="text" name="an-7" class="digit" maxlength="1"/>
						<input type="text" name="an-8" class="digit" maxlength="1"/>
						<input type="text" name="an-9" class="digit" maxlength="1"/>
						<input type="text" name="an-10" class="digit" maxlength="1"/>
						<input type="text" name="an-11" class="digit last" maxlength="1"/>
						<input type="text" name="an-12" class="digit" maxlength="1"/>
					</div>
					<div class="m_container m_border">
						<div class="m_label">ACCOUNT NAME</div>
						<input type="text" class="unbordered" />
					</div>
					<div class="m_container m_border">
						<div class="" style="background:rgb(208,221,230)">
							<center>CASH DENOMINATION BREAKDOWN</center>
						</div>
						<div class="">
							<table>
								<tr>
									<td style="width:36%">DENOMINATION</td>
									<td style="width:24%">NO. OF PIECES</td>
									<td>AMOUNT</td>
								</tr>
								<tr>
									<td>1000</td>
									<td><input type="text" class="pieces" data-denom="1000" data-piece="0" data-pointer="0" value="0" /></td>
									<td><div id="amount-0">0</div></td>
								</tr>
								<tr>
									<td>500</td>
									<td><input type="text" class="pieces" data-denom="500" data-piece="0" data-pointer="1" value="0" /></td>
									<td><div id="amount-1">0</div></td>
								</tr>
								<tr>
									<td>200</td>
									<td><input type="text" class="pieces" data-denom="200" data-piece="0" data-pointer="2" value="0" /></td>
									<td><div id="amount-2">0</div></td>
								</tr>
								<tr>
									<td>100</td>
									<td><input type="text" class="pieces" data-denom="100" data-piece="0" data-pointer="3" value="0" /></td>
									<td><div id="amount-3">0</div></td>
								</tr>
								<tr>
									<td>50</td>
									<td><input type="text" class="pieces" data-denom="50" data-piece="0" data-pointer="4" value="0" /></td>
									<td><div id="amount-4">0</div></td>
								</tr>
								<tr>
									<td>20</td>
									<td><input type="text" class="pieces" data-denom="20" data-piece="0" data-pointer="5" value="0" /></td>
									<td><div id="amount-5">0</div></td>
								</tr>
								<tr>
									<td>Coins</td>
									<td><input type="text" class="coins" value="0" /></td>
									<td><div id="amount-6">0</div></td>
								</tr>
								<tr>
									<td style="background:rgb(208,221,230)" colspan="2">TOTAL CASH DEPOSIT <img class="pull-right" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMAUExURQAkYAQoYgUoYwcqZAsuZw0waBQ2bRo8cSBAdCFBdSREdzRSgkFeikdjjk5qk1RvllVvl1t1mlt1m2J7n2iAo3GJqXWMq4KYs4+jvJyvxKK0yKm6za/A0bbG1bzL2b3L2sPR3tDd5v///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACyHg4wAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAAAIxJREFUKFN10G0XwTAMBeDLMKyGrUy9Te///5GynlobJh+f3pM0gZ8o/MXX14sk3W7ePBULWkjVj4wD7o2wuY4c0JJ1AVSXyB8k2yWw6QInJM8lUJ56jSTXQHFXSdKugIVOWulaOdkk9WyG+U4NyigmjwdJmZv+/LBPopDstjO1eUDv82PEjX7PPHn5N1ygKe6jqzH+AAAAAElFTkSuQmCC"></td>
									<td><div id="total" style="font-size: 19px;">0</div></td>
								</tr>
							</table>
						</div>
						<div class="" style="background:rgb(208,221,230)">
							<center>CHECK DEPOSIT</center>
						</div>
						<div class="m_sub" style="background:rgb(208,221,230);font-size:10px">
							PLEASE LIST EACH CHECK AND ENDORSE PROPERLY, KINDLY CHECK "CC/GC/DD" COLUMN IF<br>CHECK DEPOSIT MADE IS IN THE FORM CASHIER'S CHECK/GIFT CHECK OR LOCAL DEMAND DRAFT
						</div>
						<div class="">
						<table>
							<tr>
								<td>BANK/BRANCH</td>
								<td>CC/GC/<br>DD</td>
								<td>CHECK NUMBER</td>
								<td>AMOUNT</td>
							</tr>
							<?php for ($i=0;$i<10;$i++): ?>
							<tr>
								<td><input id="bank-<?php echo $i ?>" name="bank-<?php echo $i ?>" type="text" class="inline-text" /></td>
								<td><input id="cc-gc-dd-<?php echo $i ?>" name="cc-gc-dd-<?php echo $i ?>" value="1" type="checkbox" class="" /></td>
								<td><input id="check-number-<?php echo $i ?>" name="check-number-<?php echo $i ?>" type="text" class="inline-text" /></td>
								<td><input id="check-amount-<?php echo $i ?>" name="check-amount-<?php echo $i ?>" type="text" class="decimal inline-text" data-index="<?php echo $i ?>" /></td>
							</tr>
							<?php endfor; ?>
							<tr>
								<td style="background:rgb(208,221,230)" colspan="3">
									TOTAL CHECK DEPOSIT
									<img class="pull-right" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMAUExURQAkYAQoYgUoYwcqZAsuZw0waBQ2bRo8cSBAdCFBdSREdzRSgkFeikdjjk5qk1RvllVvl1t1mlt1m2J7n2iAo3GJqXWMq4KYs4+jvJyvxKK0yKm6za/A0bbG1bzL2b3L2sPR3tDd5v///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACyHg4wAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAAAIxJREFUKFN10G0XwTAMBeDLMKyGrUy9Te///5GynlobJh+f3pM0gZ8o/MXX14sk3W7ePBULWkjVj4wD7o2wuY4c0JJ1AVSXyB8k2yWw6QInJM8lUJ56jSTXQHFXSdKugIVOWulaOdkk9WyG+U4NyigmjwdJmZv+/LBPopDstjO1eUDv82PEjX7PPHn5N1ygKe6jqzH+AAAAAElFTkSuQmCC">
									<input id="number-of-check" name="number-of-check" type="text" class="inline-text" placeholder="Number of Checks" style="width: 124px;font-size: 10px;float: right;text-align: left;padding: 0 5px;margin-right: 5px" />
								</td>
								<td><div id="check-total" style="font-size: 19px;">0</div></td>
							</tr>
							<tr>
								<td style="background:rgb(208,221,230);font-size:15px" colspan="3">
									<strong>TOTAL DEPOSIT</strong>
									<img class="pull-right" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMAUExURQAkYAQoYgUoYwcqZAsuZw0waBQ2bRo8cSBAdCFBdSREdzRSgkFeikdjjk5qk1RvllVvl1t1mlt1m2J7n2iAo3GJqXWMq4KYs4+jvJyvxKK0yKm6za/A0bbG1bzL2b3L2sPR3tDd5v///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACyHg4wAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAAAIxJREFUKFN10G0XwTAMBeDLMKyGrUy9Te///5GynlobJh+f3pM0gZ8o/MXX14sk3W7ePBULWkjVj4wD7o2wuY4c0JJ1AVSXyB8k2yWw6QInJM8lUJ56jSTXQHFXSdKugIVOWulaOdkk9WyG+U4NyigmjwdJmZv+/LBPopDstjO1eUDv82PEjX7PPHn5N1ygKe6jqzH+AAAAAElFTkSuQmCC">
								</td>
								<td><div id="deposit-total" style="font-size: 22px;">0</div></td>
							</tr>
							<tr>
								<td colspan="2">Signature/Conforme of Depositor<br><br><br></td>
								<td>Date<br><br><br></td>
								<td>Approved By<br><br><br></td>
							</tr>
						</table>						
						</div>
					</div>
		    		<button class="btn" >Print</button>
				</form>
			</div>
	</center>
		<?php if (isset($popup)):?>
			</div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		    <button class="btn btn-primary" onclick="console.log($('#metro_bank_deposit_slip_form'))">Print</button>
		  </div>
		</div>
		<?php endif;?>
	</div>
</div>
<script>
var subs = [0,0,0,0,0,0,0];
var checkSubs = [0,0,0,0,0,0,0,0,0,0];
var overAllTotal = 0;

$().ready( function() {
	
	$('.focus').focus();
	$('.m_slip input.digit').keyup(
		function (e){
			//console.log(e.keyCode);
			//backspace = 8
			if (e.keyCode == 8){
				$(this).prev().focus().val('');
			}
			//48-57
			else if (e.keyCode >= 48 && e.keyCode <= 57){
				$(this).val(e.keyCode-48);
				$(this).next().focus()
			}
			//numpad
			else if (e.keyCode >= 96 && e.keyCode <= 105){
				$(this).val(e.keyCode-96);
				$(this).next().focus()
			}
			else {
				$(this).val('');
			}
		}
	);
	$('.m_slip input.pieces').keyup(
		function (e){
			var str = $(this).val();
			
			if (!str || 0 === str.length || isNaN(str)){
				piece = 0;
			}
			else {
				piece = parseInt(str);
			}
			
			$(this).val(piece);

			var subTotal = $(this).data('denom')*piece
			
			$('#amount-'+$(this).data('pointer')).text(commafy(subTotal));

			subs[$(this).data('pointer')] = subTotal;
			
			updateTotal();
		}
	);
	$('.m_slip input.coins').keyup(
		function (e){
			var str = $(this).val().replace(/[^0-9.]/g, ''); 
			var coins;
			if (!str || 0 === str.length || isNaN(str)){
				$(this).val(0);
				coins = 0;
			}
			else {
				coins = parseFloat(str);
				if (coins >= 1){
					$(this).val(str.replace(/^0+/, ''));
				}
				else {
					$(this).val(str);
				}
			}
			$('#amount-6').text(commafy(coins.toFixed(2)));
			subs[6] = coins;
			updateTotal();
		}
	);
	$('.m_slip input.decimal').keyup(
			function (e){
				var str = $(this).val().replace(/[^0-9.]/g, ''); 
				var dec;
				if (!str || 0 === str.length || isNaN(str)){
					$(this).val('');
					dec = 0;
				}
				else {
					dec = parseFloat(str);
					if (dec >= 1){
						$(this).val(str.replace(/^0+/, ''));
					}
					else {
						$(this).val(str);
					}
				}

				checkSubs[$(this).data('index')] = dec;
				updateTotal();
			}
		);
});

function updateTotal(){
	var cashTotal = 0;
	var checkTotal = 0;
	var depositTotal = 0;
	for (var i=0; i<=6; i++){
		cashTotal += subs[i];
		console.log("Cash:"+cashTotal);
	}
	$('#total').text(commafy(cashTotal.toFixed(2)));

	for (var i=0; i<10; i++){
		checkTotal += checkSubs[i];
		console.log("Check:"+checkTotal);
	}
	$('#check-total').text(commafy(checkTotal.toFixed(2)));
	
	depositTotal = cashTotal + checkTotal;
	console.log("Total:"+cashTotal);
	$('#deposit-total').text(commafy(depositTotal.toFixed(2)));
	//console.log(total);
}

function commafy(num) {
	return num.toString().split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "," );
}
</script>
<style>
@media print{
	.modal-header,
	.modal-footer,
	.btn,
	header,
	footer {
		display:none;
	}
}
</style>