<?php

	class Auth_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
			
   		}
				
		function get_user($username,$password)
		{
			$result = null;
			
			$query = $this->db->query("SELECT a.employees_empno,b.college_code,b.name AS college_name,a.colleges_id,CONCAT(e.lname,', ',e.fname) AS neym 
											FROM dean AS a, colleges AS b, col_faculty AS c, academic_employees AS d,
												employees AS e
											WHERE a.colleges_id=b.id AND c.employees_empno=a.employees_empno AND c.employees_empno=d.employees_empno
												AND d.employees_empno=e.empno AND a.employees_empno={$this->db->escape($username)} 
												AND e.password=PASSWORD({$this->db->escape($password)} ");
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}


		function get_student_user($username,$password)
		{
			$result = null;
			
			$query = $this->db->query("SELECT a.idno, CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1)) AS neym, a.student_type 
											FROM students AS a
											WHERE a.idno={$this->db->escape($username)} 
												AND a.password=PASSWORD({$this->db->escape($password)}) ");
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
	}

?>