<?php //echo site_url($this->uri->segment(1).'/get_students'); die();?>
<div style="margin-top:40px;">
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding: 2px; margin-bottom:20px;">
	
	<table style="width:100%; height:auto;">
	<tr>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:13%; padding:7px;">
	<?php 
		if ($offerings) {
	?>
	<a class="download_enroll" href="" academic_terms_id="<?php print($selected_term); ?>"><i class="icon-download-alt"></i> Download to PDF</a>
	<?php 
		}
	?>
	</td>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:13%;">
	<?php 
		if ($offerings) {
	?>
	<a href="<?php print(base_url('downloads/College Enrollment Summary '.$show_current_term.'.xls')); ?>" class="btn btn-link">
	<i class="icon-download-alt"></i> Download as Excel</a>
	<?php 
		}
	?>
	</td>
	<td style="text-align:right; vertical-align:top;">
	<form id="change_sy" method="POST">
			<select name="academic_terms_id" id="academic_terms_id">
					<?php 
						foreach($terms AS $my_term) {
					?>
							<option value="<?php print($my_term->id); ?>" <?php if ($my_term->id == $selected_term) { print("selected"); } ?>>
								<?php print($my_term->term.' '.$my_term->sy); ?> </option>
					<?php 	
						}
					?>
			</select>
	
	</form>						
	</td>
	</tr>
	</table>
</div>

<?php 
	if ($offerings) {
?>
<div style="margin-bottom: 15px; font-size:14px; color:#FF0000;">
<span style="font-weight: bold;">NOTE:</span> <span style="font-style: italic;">Expand and click Total to view List of Students Enrolled</span>
</div>

<div style="width:100%;">

 <table width="100%" border="0" cellspacing="0" cellpadding="1" class="table table-bordered table-condensed table-hover" id="table-summary" 
 	style="display: block;">
 	<thead>
  <tr style="background:#EAEAEA;">
    <td colspan="19" align="center" style="text-align:center; font-weight:bold; font-size:16px;">COLLEGE<br />ENROLLMENT SUMMARY<br /> 
	<?php
		print($show_current_term);
	 ?></td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td rowspan="2" style="width:22%; text-align:center; vertical-align:middle;">COLLEGES &amp; PROGRAMS </td>
    <td colspan="3" style="width:13%; text-align:center;">1st Year </td>
    <td colspan="3" style="width:13%; text-align:center;">2nd Year </td>
    <td colspan="3" style="width:13%; text-align:center;">3rd Year </td>
    <td colspan="3" style="width:13%; text-align:center;">4th Year </td>
    <td colspan="3" style="width:13%; text-align:center;">5th Year </td>
    <td colspan="3" style="width:13%; text-align:center;">GRAND TOTAL </td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td style="text-align:center; width:3%;">M</td>
    <td style="text-align:center; width:3%;">F</td>
    <td style="text-align:center; width:3%;">Total</td>
    <td style="text-align:center; width:3%;">M</td>
    <td style="text-align:center; width:3%;">F</td>
    <td style="text-align:center; width:3%;">Total</td>
    <td style="text-align:center; width:3%;">M</td>
    <td style="text-align:center; width:3%;">F</td>
    <td style="text-align:center; width:3%;">Total</td>
    <td style="text-align:center; width:3%;">M</td>
    <td style="text-align:center; width:3%;">F</td>
    <td style="text-align:center; width:3%;">Total</td>
    <td style="text-align:center; width:3%;">M</td>
    <td style="text-align:center; width:3%;">F</td>
    <td style="text-align:center; width:3%;">Total</td>
    <td style="text-align:center; width:3%;">M</td>
    <td style="text-align:center; width:3%;">F</td>
    <td style="text-align:center; width:3%;">Total</td>
  </tr>
	</thead>
  <?php 
		$grand_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
									'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
									'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
									'total_m'=>0,'total_f'=>0,'grand_total'=>0);
		$program_total = 0;
		
  		foreach($students AS $k=>$college) {
  ?>
  <tr style="text-align:center;" class="success">
    <td style="text-align:left; font-weight:bold;"><?php print($college['name']); ?> 
    	<span style="float:right;" id="<?php print("showSpan_".$college['id']); ?>">
    		<a style="font-weight:normal; font-style:italic;" href="#" onclick="showStuff('<?php print("overview_".$college['id']); ?>'); showStuff('<?php print("overviewSpan_".$college['id']); ?>'); hideStuff('<?php print("showSpan_".$college['id']); ?>');return false;">(Expand)</a></span>
    	<span style="float:right; display:none;" id="<?php print("overviewSpan_".$college['id']); ?>">
    		<a style="font-weight:normal; font-style:italic;" href="#" onClick="showStuff('<?php print("showSpan_".$college['id']); ?>'); hideStuff('<?php print("overview_".$college['id']); ?>'); hideStuff('<?php print("overviewSpan_".$college['id']); ?>'); return false;">(Collapse)</a></span>
    </td>
    <td colspan="18">&nbsp;</td>
    </tr>
   <tbody id="<?php print("overview_".$college['id']); ?>" style="display: none;" class="table hover">
   <?php
			//$programs = $this->Enrollments_Model->ListEnrollmentSummary($college['id'], $selected_term);
			
			$total1 = 0;
			$total2 = 0;
			$total3 = 0;
			$total4 = 0;
			$total5 = 0;			
			$total_m = 0;			
			$total_f = 0;			
			$col_total = 0;
			$college_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
									'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
									'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
									'total_m'=>0,'total_f'=>0);
			
			if (isset($college[$k])) {
				
				foreach ($college[$k] AS $program) {

				//if ($program['id']) { //$program cannot be created if no total of that program is 0
				
					$college_total['m1'] = $college_total['m1'] + $program['m1']; 
					$college_total['f1'] = $college_total['f1'] + $program['f1'];
					$total1 = $program['m1'] + $program['f1']; 
					$college_total['total1'] = $college_total['total1'] + $total1;
					$college_total['m2'] = $college_total['m2'] + $program['m2']; 
					$college_total['f2'] = $college_total['f2'] + $program['f2']; 
					$total2 = $program['m2'] + $program['f2']; 
					$college_total['total2'] = $college_total['total2'] + $total2;
					$college_total['m3'] = $college_total['m3'] + $program['m3']; 
					$college_total['f3'] = $college_total['f3'] + $program['f3'];
					$total3 = $program['m3'] + $program['f3']; 
					$college_total['total3'] = $college_total['total3'] + $total3;
					$college_total['m4'] = $college_total['m4'] + $program['m4']; 
					$college_total['f4'] = $college_total['f4'] + $program['f4']; 
					$total4 = $program['m4'] + $program['f4']; 
					$college_total['total4'] = $college_total['total4'] + $total4;
					$college_total['m5'] = $college_total['m5'] + $program['m5']; 
					$college_total['f5'] = $college_total['f5'] + $program['f5']; 
					$total5 = $program['m5'] + $program['f5']; 
					$college_total['total5'] = $college_total['total5'] + $total5;
					$total_m = $program['m1'] + $program['m2'] + $program['m3'] + $program['m4'] + $program['m5']; 
					$college_total['total_m'] = $college_total['total_m'] + $total_m;
					$total_f = $program['f1'] + $program['f2'] + $program['f3'] + $program['f4'] + $program['f5']; 
					$college_total['total_f'] = $college_total['total_f'] + $total_f;
					$program_total = $total_m + $total_f; 
					$col_total = $col_total + $program_total;
				
				
					if ($program_total > 0) {
   ?>
  <tr>
    <td style="text-align:left; padding-left:30px; width:22%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['abbreviation']); ?></td>
    <td style="text-align:center; color:#0000CC; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['m1']); ?></td>
    <td style="text-align:center; color:#FF0000; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['f1']); ?></td>
    <td style="text-align:center; font-weight:bold; width:3%;" id="<?php print("overview_".$college['id']); ?>">
    		<?php 
				if ($total1) {
			?>
				<a href="#" data-toggle="modal" data-target="#modal_id1_<?php print($program['id']); ?>" />
  			<?php print($total1); ?></a>

<div class="modal hide fade" id="modal_id1_<?php print($program['id']); ?>" 
	selected_term="<?php print($selected_term); ?>" 
	prog_id="<?php print($program['id']); ?>" 
	yr_level="1" style="width:750px; margin-left: -375px;">
   <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">�</button>
       <h3>List of Students</h3>
   </div>
	<div class="modal-body">            
	      <div id="modalContent_<?php print($program['id']); ?>" >
	       text here
		  </div>
	</div>
   <div class="modal-footer">
       <a href="#" class="btn" data-dismiss="modal">Close</a>
   </div>
</div>

<script>
$('#modal_id1_<?php print($program['id']); ?>').on('show', function(){
	  var prog_id = $(this).attr('prog_id');
	  var selected_term = $(this).attr('selected_term');
	  var yr_level = $(this).attr('yr_level');
	  $('#modalContent_<?php print($program['id']); ?>').html('loading...')

	  $.ajax({
	      cache: false,
	      type: 'GET',
	      url: '<?php echo site_url($this->uri->segment(1).'/get_students');?>',
	      data: {prog_id: prog_id,selected_term: selected_term,yr_level: yr_level},
	      success: function(data) {
	        $('#modalContent_<?php print($program['id']); ?>').html(data); //this part to pass the var
	      }
	  });
	})
</script>						


<?php
				} else {
					print($total1); 
				}
				
			?>
				</td>
    <td style="text-align:center; color:#0000CC; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['m2']); ?></td>
    <td style="text-align:center; color:#FF0000; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['f2']); ?></td>
    <td style="text-align:center; font-weight:bold; width:3%;" id="<?php print("overview_".$college['id']); ?>">
    		<?php 
    			if ($total2) {
			?>
			<a href="#" data-toggle="modal" data-target="#modal_id2_<?php print($program['id']); ?>" />
  			<?php print($total2); ?></a>

			<div class="modal hide fade" id="modal_id2_<?php print($program['id']); ?>" 
				selected_term="<?php print($selected_term); ?>" 
				prog_id="<?php print($program['id']); ?>" 
				yr_level="2" style="width:750px; margin-left: -375px;">
			   <div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   </div>
				<div class="modal-body">            
				      <div id="modalContent2_<?php print($program['id']); ?>" >
				       text here
					  </div>
				</div>
			   <div class="modal-footer">
			       <a href="#" class="btn" data-dismiss="modal">Close</a>
			   </div>
			</div>
			
			<script>
			$('#modal_id2_<?php print($program['id']); ?>').on('show', function(){
				  var prog_id = $(this).attr('prog_id');
				  var selected_term = $(this).attr('selected_term');
				  var yr_level = $(this).attr('yr_level');
				  $('#modalContent2_<?php print($program['id']); ?>').html('loading...')
			
				  $.ajax({
				      cache: false,
				      type: 'GET',
				      url: '<?php echo site_url($this->uri->segment(1).'/get_students');?>',
				      data: {prog_id: prog_id,selected_term: selected_term,yr_level: yr_level},
				      success: function(data) {
				        $('#modalContent2_<?php print($program['id']); ?>').html(data); //this part to pass the var
				      }
				  });
				})
			</script>						

			<?php 
				} else {
					print($total2); 
				}
				
			?>
	</td>
    <td style="text-align:center; color:#0000CC; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['m3']); ?></td>
    <td style="text-align:center; color:#FF0000; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['f3']); ?></td>
    <td style="text-align:center; font-weight:bold; width:3%;" id="<?php print("overview_".$college['id']); ?>">
    		<?php 
				if ($total3) {
			?>
			<a href="#" data-toggle="modal" data-target="#modal_id3_<?php print($program['id']); ?>" />
  			<?php print($total3); ?></a>

			<div class="modal hide fade" id="modal_id3_<?php print($program['id']); ?>" 
				selected_term="<?php print($selected_term); ?>" 
				prog_id="<?php print($program['id']); ?>" 
				yr_level="3" style="width:750px; margin-left: -375px;">
			   <div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   </div>
				<div class="modal-body">            
				      <div id="modalContent3_<?php print($program['id']); ?>" >
				       text here
					  </div>
				</div>
			   <div class="modal-footer">
			       <a href="#" class="btn" data-dismiss="modal">Close</a>
			   </div>
			</div>
			
			<script>
			$('#modal_id3_<?php print($program['id']); ?>').on('show', function(){
				  var prog_id = $(this).attr('prog_id');
				  var selected_term = $(this).attr('selected_term');
				  var yr_level = $(this).attr('yr_level');
				  $('#modalContent3_<?php print($program['id']); ?>').html('loading...')
			
				  $.ajax({
				      cache: false,
				      type: 'GET',
				      url: '<?php echo site_url($this->uri->segment(1).'/get_students');?>',
				      data: {prog_id: prog_id,selected_term: selected_term,yr_level: yr_level},
				      success: function(data) {
				        $('#modalContent3_<?php print($program['id']); ?>').html(data); //this part to pass the var
				      }
				  });
				})
			</script>						
			
			<?php 
				} else {
					print($total3); 
				}
				
			?>
	</td>
    <td style="text-align:center; color:#0000CC; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['m4']); ?></td>
    <td style="text-align:center; color:#FF0000; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['f4']); ?></td>
    <td style="text-align:center; font-weight:bold; width:3%;" id="<?php print("overview_".$college['id']); ?>">
    		<?php 
				if ($total4) {
   			?>
			<a href="#" data-toggle="modal" data-target="#modal_id4_<?php print($program['id']); ?>" />
  			<?php print($total4); ?></a>

			<div class="modal hide fade" id="modal_id4_<?php print($program['id']); ?>" 
				selected_term="<?php print($selected_term); ?>" 
				prog_id="<?php print($program['id']); ?>" 
				yr_level="4" style="width:750px; margin-left: -375px;">
			   <div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   </div>
				<div class="modal-body">            
				      <div id="modalContent4_<?php print($program['id']); ?>" >
				       text here
					  </div>
				</div>
			   <div class="modal-footer">
			       <a href="#" class="btn" data-dismiss="modal">Close</a>
			   </div>
			</div>
			
			<script>
			$('#modal_id4_<?php print($program['id']); ?>').on('show', function(){
				  var prog_id = $(this).attr('prog_id');
				  var selected_term = $(this).attr('selected_term');
				  var yr_level = $(this).attr('yr_level');
				  $('#modalContent4_<?php print($program['id']); ?>').html('loading...')
			
				  $.ajax({
				      cache: false,
				      type: 'GET',
				      url: '<?php echo site_url($this->uri->segment(1).'/get_students');?>',
				      data: {prog_id: prog_id,selected_term: selected_term,yr_level: yr_level},
				      success: function(data) {
				        $('#modalContent4_<?php print($program['id']); ?>').html(data); //this part to pass the var
				      }
				  });
				})
			</script>						
		
			<?php 
				} else {
					print($total4); 
				}
				
			?>
	</td>
    <td style="text-align:center; color:#0000CC; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['m5']); ?></td>
    <td style="text-align:center; color:#FF0000; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program['f5']); ?></td>
    <td style="text-align:center; font-weight:bold; width:3%;" id="<?php print("overview_".$college['id']); ?>">
    		<?php 
				if ($total5) {
			?>
			<a href="#" data-toggle="modal" data-target="#modal_id5_<?php print($program['id']); ?>" />
  			<?php print($total5); ?></a>

			<div class="modal hide fade" id="modal_id5_<?php print($program['id']); ?>" 
				selected_term="<?php print($selected_term); ?>" 
				prog_id="<?php print($program['id']); ?>" 
				yr_level="5" style="width:750px; margin-left: -375px;">
			   <div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   </div>
				<div class="modal-body">            
				      <div id="modalContent5_<?php print($program['id']); ?>" >
				       text here
					  </div>
				</div>
			   <div class="modal-footer">
			       <a href="#" class="btn" data-dismiss="modal">Close</a>
			   </div>
			</div>
			
			<script>
			$('#modal_id5_<?php print($program['id']); ?>').on('show', function(){
				  var prog_id = $(this).attr('prog_id');
				  var selected_term = $(this).attr('selected_term');
				  var yr_level = $(this).attr('yr_level');
				  $('#modalContent5_<?php print($program['id']); ?>').html('loading...')
			
				  $.ajax({
				      cache: false,
				      type: 'GET',
				      url: '<?php echo site_url($this->uri->segment(1).'/get_students');?>',
				      data: {prog_id: prog_id,selected_term: selected_term,yr_level: yr_level},
				      success: function(data) {
				        $('#modalContent5_<?php print($program['id']); ?>').html(data); //this part to pass the var
				      }
				  });
				})
			</script>						
			
			<?php 
				} else {
					print($total5); 
				}
				
			?>
	</td>
    <td style="text-align:center; color:#0000CC; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($total_m); ?></td>
    <td style="text-align:center; color:#FF0000; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($total_f); ?></td>
    <td style="text-align:center; font-weight:bold; width:3%;" id="<?php print("overview_".$college['id']); ?>"><?php print($program_total); ?></td>
</tr>

  <?php
  			}
		}
	}
  ?></tbody>
  <?php 
			$grand_total['m1'] = $grand_total['m1'] + $college_total['m1'];
			$grand_total['f1'] = $grand_total['f1'] + $college_total['f1'];
			$grand_total['m2'] = $grand_total['m2'] + $college_total['m2'];
			$grand_total['f2'] = $grand_total['f2'] + $college_total['f2'];
			$grand_total['m3'] = $grand_total['m3'] + $college_total['m3'];
			$grand_total['f3'] = $grand_total['f3'] + $college_total['f3'];
			$grand_total['m4'] = $grand_total['m4'] + $college_total['m4'];
			$grand_total['f4'] = $grand_total['f4'] + $college_total['f4'];
			$grand_total['m5'] = $grand_total['m5'] + $college_total['m5'];
			$grand_total['f5'] = $grand_total['f5'] + $college_total['f5'];
			$grand_total['total1'] = $grand_total['total1'] + $college_total['total1'];
			$grand_total['total2'] = $grand_total['total2'] + $college_total['total2'];
			$grand_total['total3'] = $grand_total['total3'] + $college_total['total3'];
			$grand_total['total4'] = $grand_total['total4'] + $college_total['total4'];
			$grand_total['total5'] = $grand_total['total5'] + $college_total['total5'];
			$grand_total['total_m'] = $grand_total['total_m'] + $college_total['total_m'];
			$grand_total['total_f'] = $grand_total['total_f'] + $college_total['total_f'];
			$grand_total['grand_total'] = $grand_total['grand_total'] + $col_total;

   ?>
  <tr style="font-weight:bold;">
    <td style="text-align:right;">COLLEGE TOTAL: </td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m1']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f1']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total1']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m2']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f2']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total2']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m3']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f3']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total3']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m4']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f4']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total4']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m5']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f5']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total5']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['total_m']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['total_f']); ?></td>
    <td style="text-align:center;"><?php print($col_total); ?></td>
  </tr>
   <?php
	}
  ?>
  
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td style="text-align:right;">GRAND TOTAL: </td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m1']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f1']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total1']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m2']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f2']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total2']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m3']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f3']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total3']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m4']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f4']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total4']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m5']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f5']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total5']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['total_m']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['total_f']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['grand_total']); ?></td>
  </tr>
</table>


<div style="width:50%;">
	<table class="table table-condensed table-bordered" style="width:100%;">
	<tr>
		<td style="width:40%;">TOTAL STUDENTS ENROLLED</td>
		<td style="width:10%; text-align:center; font-weight:bold;"><?php print($grand_total['grand_total']); ?></td>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td>TOTAL PAY UNITS</td>
		<td style="text-align:center; font-weight:bold;"><?php print(number_format($total_units->total_paying,2)); ?></td>
		<td style="width:40%;">AVERAGE PAY UNITS/STUDENT</td>
		<td style="width:10%; text-align:center; font-weight:bold;">
			<?php 
				if ($total_units->total_paying) {
					print(number_format($total_units->total_paying/$grand_total['grand_total'],2)); 
				} else {
					print("0.00"); 
				}
			?>
		</td>
	</tr>
	<tr>
		<td>TOTAL LOAD UNITS</td>
		<td style="text-align:center; font-weight:bold;"><?php print(number_format($total_units->total_load,2)); ?></td>
		<td>AVERAGE LOAD UNITS/STUDENT</td>
		<td style="text-align:center; font-weight:bold;">
			<?php 
				if ($total_units->total_load) {
					print(number_format($total_units->total_load/$grand_total['grand_total'],2)); 
				} else {
					print("0.00"); 					
				}
			?>
		</td>
	</tr>
	<tr>
		<td>TOTAL OFFERINGS</td>
		<td style="text-align:center; font-weight:bold;"><?php print($offerings); ?></td>
		<td>AVERAGE STUDENTS/CLASS</td>
		<td style="text-align:center; font-weight:bold;"><?php print(number_format($enrollees->total_enrollees/$offerings,2)); ?></td>
	</tr>
	</table>
</div>

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding: 2px; margin-bottom:20px;">
	
	<table style="width:100%; height:auto;">
	<tr>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:13%; padding:7px;">
	<a class="download_enroll" href="" academic_terms_id="<?php print($selected_term); ?>"><i class="icon-download-alt"></i> Download to PDF</a>
	</td>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:13%;">
	<a href="<?php print(base_url('downloads/College Enrollment Summary '.$show_current_term.'.xls')); ?>" class="btn btn-link" >
	<i class="icon-download-alt"></i> Download as Excel</a>
	</td>
	<td style="text-align:right; vertical-align:top;">&nbsp;</td>
	</tr>
	</table>
</div>
<?php 
	} else {
?>		
	<span style="font-weight:bold; font-size:16px; margin-left:10px; color:#FF0000;">NO Data Available!</span>
<?php 
	}
?>

</div>

</div>

  <form id="download_enroll_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="4" />
</form>
<script>
	$(document).ready(function(){
  
		$('.download_enroll').click(function(event){
			event.preventDefault(); 
			var academic_terms_id = $(this).attr('academic_terms_id');
	
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_terms_id',
				value: academic_terms_id,
			}).appendTo('#download_enroll_summary_form');
			$('#download_enroll_summary_form').submit();
			
		});

		
	});
</script>


<script type="text/javascript">
	function showStuff(id) {
		document.getElementById(id).style.display = 'table-row-group';
	}
	function hideStuff(id) {
		document.getElementById(id).style.display = 'none';
	}
</script>

<script>
	$(document).ready(function(){
		$('#academic_terms_id').bind('change', function(){
			show_loading_msg();
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 3,
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>

