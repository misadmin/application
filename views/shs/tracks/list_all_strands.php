
			<div style="padding:3px; overflow:auto;">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:7%;">Strand ID</td>
								<td class="shs_header" style="width:22%;">Track Name</td>
								<td class="shs_header" style="width:12%;">Abbreviation</td>
								<td class="shs_header" style="width:35%;">Description</td>
								<td class="shs_header" style="width:7%;">Max.<br>Years</td>
								<td class="shs_header" style="width:7%;">Status</td>
								<td class="shs_header" style="width:5%;">Update</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($strands) {
									foreach($strands AS $strand) {
										if ($strand->status == 'Offered') {
											$bg = "#FFFFFF";
										} else {
											$bg = "#FF9C9C";
										}
							?>
							<tr>
									<td style="text-align:center; background-color:<?php print($bg); ?>; "><?php print($strand->id); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($strand->track_name); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($strand->abbreviation); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($strand->description); ?></td>
									<td style="text-align:center; background-color:<?php print($bg); ?>; "><?php print($strand->max_yr_level); ?></td>
									<td style="text-align:center; background-color:<?php print($bg); ?>; ">
										<div style="float:left; padding-left:5px;">
											<?php print($strand->status); ?>
										</div>
										<div>
											<a href="<?php print($strand->id); ?>" 
												status="<?php print($strand->status); ?>"
												description="<?php print($strand->description); ?>"
												class="update_status" >
												<div id="show_edit_icon_<?php print($strand->id); ?>" >
													<i class="icon-edit"></i>
												</div>
											</a>
										</div>
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>; ">
										<a href="<?php print($strand->id); ?>" 
											acad_program_groups_id="<?php print($strand->acad_program_groups_id); ?>"
											abbreviation="<?php print($strand->abbreviation); ?>"
											description="<?php print($strand->description); ?>"
											max_yr_level="<?php print($strand->max_yr_level); ?>"
											class="update_strand" >
											<div id="show_pencil_icon_<?php print($strand->id); ?>" >
												<i class="icon-pencil"></i>
											</div>
										</a>
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>; ">
										<a href="<?php print($strand->id); ?>" class="delete_strand" >
											<div id="show_trash_icon_<?php print($strand->id); ?>" >
												<i class="icon-trash"></i>
											</div>
										</a>
									</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>

			

<!-- JQ to delete strand-->			
<script>
	$('.delete_strand').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete this strand?');

		if (confirmed){
			
			var track_id  = <?php print($track_id); ?>;
			var strand_id = $(this).attr('href');

			$('#show_trash_icon_'+strand_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/strand_management');?>',
				data: { track_id: track_id,
						strand_id: strand_id,
						action: 'delete_strand'},
				dataType: 'json',
				success: function(response) {
					$('#show_extracted_items').html(response.output); 

				}
			});
		}

	});
</script>


<!-- JQ to update status-->
<script>
	$('.update_status').click(function(event){
		event.preventDefault();

		var strand_id   = $(this).attr('href');
		var description = $(this).attr('description');
		var status      = $(this).attr('status');
		
		$('#strand_id_status').val(strand_id);
		$('#description_status').val(description);
		$('#status_status').val(status);
		
		$('#modal_update_status').modal('show');
	});
</script>				


	<div id="modal_update_status" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_status_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Status</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="strand_id_status"></div>
						<?php														
							$this->load->view('shs/tracks/modal_view/update_status_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<!-- JQ to submit an update to a status -->
<script>
	$('#update_status_form').submit(function(e) {
		e.preventDefault();
		
		var strand_id = $('#strand_id_status').val();
		var track_id  = <?php print($track_id); ?>;

		var element1  = $("option:selected", "#status_status");
		var status    = element1.attr('shortcut');
	
		$('#modal_update_status').modal('hide');

		$('#show_edit_icon_'+strand_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/strand_management'); ?>",
			data: {"strand_id": strand_id,
					"track_id": track_id,
					"status": status,
					"action": "update_status" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_items').html(response.output); 
				
				$('#show_edit_icon_'+strand_id).html("<i class='icon-edit'></i>")
				
			}
		});
		
	});
</script>				



<script>
	$('.update_strand').click(function(event){
		event.preventDefault();

		var strand_id    = $(this).attr('href');
		var track_id     = $(this).attr('acad_program_groups_id');
		var description  = $(this).attr('description');
		var abbreviation = $(this).attr('abbreviation');
		var max_yr_level = $(this).attr('max_yr_level');
		
		$('#track_id_update_modal').val(track_id);
		$('#strand_id_update').val(strand_id);
		$('#description_update_modal').val(description);
		$('#abbreviation_update_modal').val(abbreviation);
		$('#max_yr_level_update_modal').val(max_yr_level);

		$('#modal_update_strand').modal('show');
				
	});
</script>				


	<div id="modal_update_strand" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_strand_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Status</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="strand_id_update"></div>
						<?php
							$data['tracks'] = $tracks;
							$this->load->view('shs/tracks/modal_view/update_strand_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#update_strand_form').submit(function(e) {
		e.preventDefault();
		
		var track_id     = $('#track_id_update_modal').val();
		var abbreviation = $('#abbreviation_update_modal').val();
		var description  = $('#description_update_modal').val();
		var max_yr_level = $('#max_yr_level_update_modal').val();
		var strand_id    = $('#strand_id_update').val();
	
		$('#modal_update_strand').modal('hide');

		$('#show_pencil_icon_'+strand_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/strand_management'); ?>",
			data: {"strand_id": strand_id,
					"track_id": track_id,
					"abbreviation": abbreviation,
					"description": description,
					"max_yr_level": max_yr_level,
					"action": "update_strand" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_strand_management_page').html(response.output); 
				
				$('#show_pencil_icon_'+strand_id).html("<i class='icon-pencil'></i>")
				
			}
		});
		
	});
</script>				


