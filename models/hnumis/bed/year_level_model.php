<?php

	class year_level_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListLevels() {
			$result = null;
			
			$q1 = "SELECT 
							a.id,
							a.group_name,
							a.abbreviation,
							a.num_year,
							(SELECT 
									COUNT(x.id) 
								FROM 
									academic_programs AS x 
								WHERE 
									x.acad_program_groups_id = a.id 
							) AS num_yr_level,
							(SELECT 
									GROUP_CONCAT(x.abbreviation SEPARATOR ', ') 
								FROM 
									academic_programs AS x 
								WHERE 
									x.acad_program_groups_id = a.id
								ORDER BY 
									x.abbreviation
							) AS year_level
						FROM 
							acad_program_groups AS a,
							levels AS b 
						WHERE 
							a.levels_id = b.id 
							AND b.type = 'basic_ed'
						ORDER BY 
							b.rank ";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		public function ListYearLevels($acad_program_groups_id=NULL,$status=NULL) {
			$result = null;
			
			if ($acad_program_groups_id == 'All') {
				$q1 = "SELECT 
								a.id,
								a.acad_program_groups_id,
								a.abbreviation,
								a.description,
								CASE a.status
									WHEN 'F' THEN 'Frozen'
									WHEN 'O' THEN 'Offered'
								END AS status,
								b.num_year AS max_yr_level,
								b.group_name AS track_name,
								d.year_level,
								c.id AS levels_id
							FROM 
								academic_programs AS a,
								acad_program_groups AS b,
								levels AS c,
								bed_academic_programs AS d
							WHERE 
								a.acad_program_groups_id = b.id 
								AND c.id = b.levels_id 
								AND a.id = d.academic_programs_id ";
			} else {								
				$q1 = "SELECT 
								a.id,
								a.acad_program_groups_id,
								a.abbreviation,
								a.description,
								CASE a.status
									WHEN 'F' THEN 'Frozen'
									WHEN 'O' THEN 'Offered'
								END AS status,
								b.num_year AS max_yr_level,
								b.group_name AS track_name,
								d.year_level,
								c.id AS levels_id
							FROM 
								academic_programs AS a,
								acad_program_groups AS b,
								levels AS c,
								bed_academic_programs AS d
							WHERE 
								a.acad_program_groups_id = b.id 
								AND c.id = b.levels_id 
								AND a.id = d.academic_programs_id  
								AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)} ";
			}
			
			if ($status) {
				$q1 .= " AND a.status = {$this->db->escape($status)} ";
			}
					
			$q1 .= " ORDER BY 
						c.rank,
						a.abbreviation ";
			
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		

		/*function ListStrandsByLatestProspectus() {
			$result = null;
			
			$q1 = "SELECT 
							b.id, 
							a.description, 
							b.effective_year,
							c.abbreviation AS track_abbrev,
							a.abbreviation,
							a.id AS academic_programs_id
						FROM 
							academic_programs AS a,
							prospectus AS b,
							acad_program_groups AS c
						WHERE 
							a.id = b.academic_programs_id
							AND a.acad_program_groups_id = c.id
							AND a.colleges_id = 12 
							AND a.status = 'O' 
							AND b.status ='A' 
							AND b.id = (SELECT 
												x.id 
											FROM 
												prospectus AS x 
											WHERE 
												x.academic_programs_id = a.id 
											ORDER BY 
												x.effective_year DESC 
											LIMIT 1)
						ORDER BY 
							a.description,
							b.effective_year DESC";
			
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}*/

		
		function AddAcadProgramGroups($data) {
				
			$q1 = "INSERT INTO
							acad_program_groups (group_name,abbreviation,levels_id,num_year)
						VALUES (
							{$this->db->escape($data['group_name'])},
							{$this->db->escape($data['abbreviation'])},
							{$this->db->escape($data['levels_id'])},
							{$this->db->escape($data['num_year'])} ) ";
			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
		public function AddYearLevel($data=NULL) {
				
			$q1 = "INSERT INTO
							academic_programs (departments_id,colleges_id,acad_program_groups_id,abbreviation,description,status,max_yr_level,inserted_by)
						VALUES (
							{$this->db->escape($data['departments_id'])},
							{$this->db->escape($data['colleges_id'])},
							{$this->db->escape($data['acad_program_groups_id'])},
							{$this->db->escape($data['abbreviation'])},
							{$this->db->escape($data['description'])},
							{$this->db->escape($data['status'])},
							{$this->db->escape($data['max_yr_level'])},
							{$this->db->escape($data['inserted_by'])}) ";
			//return $q1;
			if ($this->db->query($q1)) {
				$data1['academic_programs_id'] 	= $this->db->insert_id();
				$data1['year_level'] 			= $data['year_level'];
				
				$this->addBED_acad_programs($data1);
				
				return $data1['academic_programs_id'];
			} else {
				return FALSE;
			}
		}


		private function addBED_acad_programs($data=NULL) {
			
			$q1 = "INSERT INTO
							bed_academic_programs (academic_programs_id,year_level)
						VALUES (
							{$this->db->escape($data['academic_programs_id'])},
							{$this->db->escape($data['year_level'])} ) ";
			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		}
		
		
		function UpdateAcadProgramGroups($data){

			$q1 = "UPDATE acad_program_groups
					SET group_name = {$this->db->escape($data['group_name'])},
					    abbreviation = {$this->db->escape($data['abbreviation'])},
						num_year = {$this->db->escape($data['num_year'])}
		 			WHERE
						id = {$this->db->escape($data['id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

		
		function UpdateYrLevelStatus($data=NULL){

			$q1 = "UPDATE academic_programs
					SET status = {$this->db->escape($data['status'])},
						update_by = {$this->db->escape($data['update_by'])}
		 			WHERE
						id = {$this->db->escape($data['id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}


		public function UpdateYearLevel($data=NULL){

			$q1 = "UPDATE 
							academic_programs
						SET 
							abbreviation = {$this->db->escape($data['abbreviation'])},
							description = {$this->db->escape($data['description'])},
							update_by = {$this->db->escape($data['update_by'])}
						WHERE
							id = {$this->db->escape($data['id'])} ";
			
			//return $q1;
			if ($this->db->query($q1)) {
				
				$this->updateBEDYearLevel($data);
				
				return TRUE;
			} else {
				return FALSE;
			}			
		}


		private function updateBEDYearLevel($data=NULL){

			$q1 = "UPDATE 
							bed_academic_programs
						SET 
							year_level = {$this->db->escape($data['year_level'])}
						WHERE
							academic_programs_id = {$this->db->escape($data['id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
	    function DeleteTrack($track_id) {
			
			$q = "DELETE FROM
						acad_program_groups 
					WHERE
						id = {$this->db->escape($track_id)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
	    function DeleteYearLevel($yr_level_id=NULL) {
			
			$q = "DELETE FROM
						academic_programs 
					WHERE
						id = {$this->db->escape($yr_level_id)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


		public function getNextBEDProgramGroups($order_no=NULL, $enrollment_type=NULL) {

			$result=null; 
			
			if ($enrollment_type == 'isped-selfcontained') {
				$q1 = "SELECT 
								a.acad_program_groups_id
							FROM 
								bed_program_groups_order AS a  
							WHERE 
								a.order_no = {$this->db->escape($order_no)} 
								AND a.group_type = 'ISPED' ";				
			} else {
				$q1 = "SELECT 
								a.acad_program_groups_id
							FROM 
								bed_program_groups_order AS a  
							WHERE 
								a.order_no = {$this->db->escape($order_no)} 
								AND a.group_type = 'Regular' ";
			}
			
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;

		}

		
		/*function ListTracksByArray($array_academic_programs_groups_id=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							a.id AS acad_program_groups_id,
							a.abbreviation,
							a.group_name,
							(SELECT 
									GROUP_CONCAT(x.abbreviation SEPARATOR ', ') 
								FROM 
									academic_programs AS x 
								WHERE 
									x.acad_program_groups_id = a.id
								ORDER BY 
									x.abbreviation
							) AS strand_description
						FROM 
							acad_program_groups AS a
						WHERE 
							a.id IN (".$array_academic_programs_groups_id.")
						ORDER BY 
							a.abbreviation ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}*/
		
	}
	
?>
