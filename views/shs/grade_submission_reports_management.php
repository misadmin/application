

		<div style="width:90%;" >
			<h2 class="heading">Grades Submission Report</h2>
			<div style="width:98%; margin:0 auto; ">
					<?php 
						$data = array(
									'academic_terms'=>$academic_terms,
									'selected_term'=>$selected_term,
									'select_id'=>'grades',
									'change_select'=>'change_grades_data');
						$this->load->view('shs/reports/pulldown_academic_terms',$data);
					?>
			</div>
			
			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; " id="display_teachers" >
				<?php 
					$data = array(
								'teachers'=>$teachers,
							);	
					$this->load->view('shs/reports/grades_submission_reports',$data);
				?>
			</div>
		</div>
		
		
			
<script>
	$(document).ready(function(){
		$('.change_grades_data').bind('change', function(){
			
			var element2    = $("option:selected", "#grades");
			var term_id     = element2.val();
			
			$('#show_change_icon_grades').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/grades_submission_report'));?>",
				data: {	"term_id": term_id,
						"action": "change_select" },
				dataType: 'json',
				success: function(response) {													

					$('#display_teachers').html(response.output); 

					$('#show_change_icon_grades').html("")
										
				}
			});

		});
	});
</script> 

