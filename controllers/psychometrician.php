<?php

class Psychometrician extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->content_lib->set_title ('Psychometrician | ' . $this->config->item('application_title'));
		$this->navbar_data['menu'] = array(
				'Student'	=>'student',
				'Exams'		=> 'exams',
				);
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		$this->load->library('form_validation');
	}
	
	public function student(){
		
		$this->content_lib->set_title('Psychometrician | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		
		if ( ! empty($query)){
			
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
			
			$this->load->library('pagination_lib');
			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				echo "No result found for that query";
			}
		}
		
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/Programs_Model');
			$this->load->model('hnumis/Courses_Model');
			$this->load->model('hnumis/Enrollments_model');
			$this->load->model('psychometrician_model');
			$this->load->model('hnumis/Student_model');
			$this->load->model('hnumis/AcademicYears_Model');
				
			$this->load->library('tab_lib');
			
			//We're going to use the datepicker plugin...
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');
			$this->content_lib->enqueue_header_style('qtip');
			$this->content_lib->enqueue_footer_script('qtip');
			//prepare contents...
			$current_term = $this->Academic_terms_model->current_academic_term();
			$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_term->id);
			$grades = $this->Enrollments_model->student_grades($idnum);
			
			$student_academic_terms = $this->Academic_terms_model->student_inclusive_academic_terms ($idnum);
			
			$all_exams = $this->psychometrician_model->all_exams(FALSE);
			$student_exams = $this->psychometrician_model->student_exams($idnum);
			
			$student_exams_id_arrays = array();
			if ($student_exams && count($student_exams) > 0) {
				foreach ($student_exams as $a){
					$student_exams_id_arrays[] = $a->id;
				}
			}
			
			//Let us perform activities first specified by $action
			//We're going to do this first before outputting the student profile
			//Note: enqueue the tab contents here... but don't feed that yet to the body.
			 
			switch ($this->input->post('action')){
				case 'encode_exam_result' :

								//Let us check the NONCE...
								if ( ! $this->common->nonce_is_valid($this->input->post('nonce'))){
									//An invalid NONCE occurred either a second sent or user is a BAD guy!
									$this->content_lib->set_message('Nonce is Invalid', 'alert-error');
									$this->content_lib->content();
									return FALSE;
								}
								
								//process the result... this has the assumption that all pushed through after browser based validation...
								//todo: server side validation...								
								$data['exam_types_id'] = $this->input->post('exam_type', TRUE);
								$data['student_histories_id'] = $this->input->post('histories_id', TRUE); 
								$data['employees_empno'] = $this->session->userdata('empno');
								$data['date_taken'] = $this->input->post('date_taken', TRUE); 
								$data['raw_score'] = $this->input->post('exam_raw_score', TRUE);
								$data['percentile'] = $this->input->post('exam_percentile', TRUE);
								$data['description'] = $this->input->post('exam_comment', TRUE);
								
								if ($this->psychometrician_model->insert_student_exam_result ($data)) {
									$this->content_lib->set_message ("Examination encoded for student.", 'alert-success');
									$student_exams = $this->psychometrician_model->student_exams($idnum);
										
									$student_exams_id_arrays = array();
									if ($student_exams && count($student_exams) > 0) {
										foreach ($student_exams as $a){
											$student_exams_id_arrays[] = $a->id;
										}
									}
								} else {
									$this->content_lib->set_message ("Examination NOT encoded for student.", 'alert-error');
								}
																
								//end process...	
								$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $current_term->id);
								$acad_term = $this->AcademicYears_Model->getAcademicTerms($current_term->id);
								
								if ($selected_history) {
									$student_units = $this->Student_model->getUnits($selected_history, $acad_term->term);
									//$student_units = $this->Student_model->getUnits($selected_history, $acad_term);
									$student_history_id=$selected_history->id;
								} else {
									$student_units['max_bracket_units']=0;
									$student_history_id=0;
								}
								
								
								$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', array('schedules'=>$courses,
										'academic_terms_id'=>$current_term->id,
										'max_units'=>$student_units['max_bracket_units'],					
										'student_histories_id'=>$student_history_id), 'schedule', FALSE);
								$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', FALSE);
								$this->tab_lib->enqueue_tab ('Exam Results', 'student/psych_exam_results_with_form', array('exams_taken'=>$student_exams), 'exam', FALSE);
								$this->tab_lib->enqueue_tab ('Add Exam', 'psychometrician/add_exam_results', array('student_exams_id_arrays'=>$student_exams_id_arrays, 'exam_types'=>$all_exams, 'inclusive_terms'=>$student_academic_terms), 'add_exam', TRUE);
								
								$tab_content = $this->tab_lib->content();
								break;

				case 'generate_class_schedule':
									$this->load->model("hnumis/Reports_model");
								
									$student['idno']= $result->idno;
									$student['fname']= $result->fname;
									$student['lname']= $result->lname;
									$student['mname']= $result->mname;
									$student['yr_level']= $result->year_level;
									$student['abbreviation']= $result->abbreviation;
									$student['max_bracket_units']= $this->input->post('max_units');
									$student['student_histories_id']= $this->input->post('student_histories_id');
								
									$selected_term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
								
									$this->Reports_model->generate_student_class_schedule_pdf($this->input->post('academic_terms_id'), $student, $selected_term);
								
									return;
												
				default:		
					
								$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $current_term->id);
								
								if ($selected_history) {
									$student_units = $this->Student_model->getUnits($selected_history, $current_term->term);
									$student_history_id=$selected_history->id;
								} else {
									$student_units['max_bracket_units']=0;
									$student_history_id=0;
								}
						
								$acad_term = $this->AcademicYears_Model->getAcademicTerms($current_term->id);
						
								$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', array('schedules'=>$courses,
										'academic_terms_id'=>$current_term->id,
										'max_units'=>$student_units['max_bracket_units'],					
										'student_histories_id'=>$student_history_id), 'schedule', TRUE);
								$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', FALSE);
								$this->tab_lib->enqueue_tab ('Exam Results', 'student/psych_exam_results_with_form', array('exams_taken'=>$student_exams), 'exam', FALSE);
								$this->tab_lib->enqueue_tab ('Add Exam', 'psychometrician/add_exam_results', array('student_exams_id_arrays'=>$student_exams_id_arrays, 'exam_types'=>$all_exams, 'inclusive_terms'=>$student_academic_terms), 'add_exam', FALSE);
								
								$tab_content = $this->tab_lib->content();
								break; 
				
			}
			
			//let's prepare to output the student's profile...
			$result = $this->student_common_model->my_information($idnum);
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->mname. " " . $result->lname,
						'course'	=> $result->abbreviation,
						'level'		=> $result->year_level,
						'full_home_address'	=> $result->full_home_address,
						'full_city_address' => $result->full_city_address,
						'phone_number' 		=> $result->phone_number,
						'section' 			=> $result->section,
				);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				
				//We feed the profile to the body...
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//Then we feed the tab content to the body...
				$this->content_lib->enqueue_body_content("", $tab_content);
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				echo "No student found with that ID Number...";
			}
		}
		
		//what will happen if there is no query... and the result is not numeric?
		$this->content_lib->content();		
	}
	
	public function exams(){
		$this->load->model('psychometrician_model');
		$this->content_lib->set_title ('Psychometrician | Exams | ' . $this->config->item('application_title'));
		$action = ($this->input->post('action') ? $this->input->post('action') : "");
		
		if ($action) {
			$nonce = $this->input->post('nonce');
			
			if ($this->common->nonce_is_valid($nonce)) {
				//nonce is valid...
				switch ($action) {
					case "new_exam"		:
									$exam_abbreviation = $this->input->post('exam_abbreviation', TRUE);
									$exam_description = $this->input->post('exam_description', TRUE);
									if ($this->psychometrician_model->insert_exam(array('abbreviation'=>$exam_abbreviation, 'description'=>$exam_description, 'status'=>'active'))) {
										$message = "Exam Type added to Database...";
										$severity = "alert-success";
									} else {
										$message = "Error adding exam type to Database...";
										$severity = "alert-error";
									}
									break;
					case "edit_exam"	:
									$id = (int)$this->input->post('id', TRUE);
									$exam_abbreviation = $this->input->post('edit_exam_abbreviation', TRUE);
									$exam_description = $this->input->post('edit_exam_description', TRUE);
									$status = $this->input->post('status', TRUE);
									
									if ($this->psychometrician_model->update_exam(array('id'=>$id, 'abbreviation'=>$exam_abbreviation, 'description'=>$exam_description, 'status'=>$status))) {
										$message = "Exam Type Edited...";
										$severity = "alert-success";
									} else {
										$message = "Error editing exam type...";
										$severity = "alert-error";
									}
									break;
					default	:
						
							break;
				}
			} else {
				//nonce is invalid
				$message = "Nonce Error.";
				$severity = "alert-error";
			}
		}
		
		if (isset($message)) {
			$this->content_lib->set_message ($message, $severity);
		}
		$all_exams = array('exams' => $this->psychometrician_model->all_exams());
		
		$this->load->library('tab_lib');
		$this->tab_lib->set_tab_container_name ("exams");
		$this->tab_lib->enqueue_tab("All Exams", 'psychometrician/all_exams', $all_exams, 'all_exams', TRUE);
		$this->tab_lib->enqueue_tab("New Exam", 'psychometrician/new_exam_form', array(), 'new_exam', FALSE);
		$tab = $this->tab_lib->content();
		$this->content_lib->enqueue_body_content('', $tab);
		$this->content_lib->content();
	}

	
	
/***********************************************************************************************/

/***********************************************************************************************/
	
}