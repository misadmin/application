<?php
$no_of_years = 5;
$no_of_terms = 3;

$sample_fees = array(
		//group
		'1'=>array(
				'name'		=>'tuition',
				'subgroups' 	=> array(
						//subgroup
						'100'=>array(
								'name'=>'tuition',
								'values'=>array(
										//years
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'130'=>array(
								'name'=>'tuition2',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						)
				)
		),
		'2'=>array(
				'name'=>'miscellaneous',
				'subgroups' => array(
						'101'=>array(
								'name'=>'misc1',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'102'=>array(
								'name'=>'misc2',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'103'=>array(
								'name'=>'misc3',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						)
				)
		),
		'3'=>array(
				'name'=>'other fees',
				'subgroups' => array(
						'101'=>array(
								'name'=>'other fees1',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'102'=>array(
								'name'=>'other fees2',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'103'=>array(
								'name'=>'other fees3',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						)
				)
		)
);

$fees = isset($fees) ? $fees : $sample_fees;
//<input type="text" name="rate[--subgroup id--][--year--][--term--]" value="" />

//print_r($fees);
?>

<table class="table">
	<thead>
		<tr>
			<th></th>
			<?php
				$year = 1;
				while($year <= $no_of_years):
			?>
			<th>Year <?php echo $year++?></th>
			<?php endwhile?>
		</tr>
	</thead>
	<tbody>
	<?php foreach($fees as $key=>$val): ?>
		<tr>
			<td colspan="<?php echo $no_of_years+1?>"><?php echo $val['name'] ?></td>
		</tr>
		<?php foreach($val['subgroups'] as $child_key => $child): ?>
		<tr>
			<td child-id="<?php echo $child_key;?>">&nbsp;&nbsp;<?php echo $child['name']; ?></td>
			<?php
				$year = 1;
				while($year <= $no_of_years):
			?>
			<td>
				<?php
				if(isset($child["values"][$year])):
					$term = 1;
					while($term <= $no_of_terms):
						if(isset($child["values"][$year][$term])){
							echo $child["values"][$year][$term];
						} else {
							echo "0";
						}
						echo "&nbsp";
					$term++;
					endwhile;
				?>
			</td>
			<?php
				else:
					echo "0&nbsp0&nbsp0";
				endif;
				$year++;
				endwhile;
			?>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>