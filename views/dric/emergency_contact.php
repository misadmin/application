<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='emergency_info') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?> 
		<form id="student_emergency_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_emergency_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<div class="control-group formSep">
					<label for="emergency_notify" class="control-label">Person to Notify:</label>
					<div class="controls" id="emergency_notify_controls">
						<input type="text" name="emergency_notify" id="emergency_notify" class="input-xlarge" value="<?php if(isset($emergency_notify)) echo $emergency_notify; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="emergency_telephone" class="control-label">Telephone/Mobile No.:</label>
					<div class="controls" id="primary_school_controls">
						<input type="text" name="emergency_telephone" id="emergency_telephone" class="input-xlarge" value="<?php if(isset($emergency_telephone)) echo $emergency_telephone; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="emergency_email_address" class="control-label">Email Address:</label>
					<div class="controls" id="emergency_email_address_controls">
						<input type="text" name="emergency_email_address" id="emergency_email_address" class="input-xlarge" value="<?php if(isset($emergency_email_address)) echo $emergency_email_address; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="emergency_address" class="control-label">Address:</label>
					<div class="controls" id="emmergency_address_controls">
						<div style="margin-top:6px" id="emergency_address_control_text"><strong><?php echo isset($emergency_address)?$emergency_address:""; ?></strong>&nbsp;<small><a href="javascript://" id="emergency_address_control"  class="edit_toggle" >Edit</a></small></div>
						<div id="emergency_address_control_controls"></div>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button id="update_educational_information" class="btn btn-primary" type="submit">Update Emergency Contact Information</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script>
$('#student_emergency_information_form').validate({
	onkeyup: false,
	errorClass: 'error',
	validClass: 'valid',
	rules: {
		emergency_notify: { required: true},
		emergency_email_address: { email: true},
	},
	highlight: function(element) {
		$(element).closest('div').addClass("f_error");
					setTimeout(function() {
						boxHeight()
					}, 200)
				},
	unhighlight: function(element) {
		$(element).closest('div').removeClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	errorPlacement: function(error, element) {
		$(element).closest('div').append(error);
	}
});
</script>

	


<script>
var philippine_id = <?php echo ltrim($philippine_id, '0'); ?>;
var city_id = <?php echo ltrim($this->config->item('city_id'), '0'); ?>;
var pbirth_control_controls = $('#pbirth_control_controls').html();
var countries = <?php echo json_encode($countries); ?>;
var provinces = <?php echo json_encode($provinces); ?>;

$(document).ready(function(){
//	$('#fathers_number').mask("9999-999-9999");  
//	$('#mothers_number').mask("9999-999-9999");  
	
	
	<?php
	$controls = array(
				'place_of_birth',
				'home_address',
				'city_address',
				'fathers_address',
				'mothers_address',
				'secondary_school_address',
				'emergency_address',
			);
	
	foreach ($controls as $control){
		if (empty($control)) {
			echo "\t$('#{$control}_control_controls').show();\n";
		} else {
			echo "\t$('#{$control}_control_controls').hide();\n";
		}
	}
	?>
	show_countries('place_of_birth_country', 'place_of_birth_control_controls', false);
	show_countries('home_address_country', 'home_address_control_controls', false);
	show_countries('fathers_address_country', 'fathers_address_control_controls', true);
	show_countries('mothers_address_country', 'mothers_address_control_controls', true);
	show_countries('secondary_school_address_country', 'secondary_school_address_control_controls', false);
	show_countries('emergency_address_country', 'emergency_address_control_controls', true);
	show_barangays(city_id, 'city_address_barangay_id', 'city_address_control_controls', true, true, 'city_address');
});

$('.edit_toggle').live('click', function(){
	var control = $(this).attr("id");
	console.log(control);
	$('#'+ control + '_text').hide();
	$('#'+ control + '_controls').show();
});

/* place of Birth */
$('#place_of_birth_country').live('change', function(){
	var country_id = $(this).val();
	
	if (country_id==philippine_id){
		$('#place_of_birth_address').remove();
		$('#place_of_birth_control_controls').append(show_provinces('place_of_birth_province'));
	} else {
		$('#place_of_birth_address').remove();
		$('#place_of_birth_province').remove();
		$('#place_of_birth_town_id').remove();
		$('#place_of_birth_control_controls').append('<input style="margin-left: 5px" type="text" class="input-xlarge" id="place_of_birth_address" placeholder="House Num, Street Name, etc." name="place_of_birth_address" />');
	}
	
});

$('#place_of_birth_province').live('change', function(){
	var province_id = $(this).val();

	$('#place_of_birth_town_id').remove();
	show_towns(province_id, 'place_of_birth_town_id', 'place_of_birth_control_controls');
});

/* home address */
$('#home_address_country').live('change', function(){
	var country_id = $(this).val();
	
	if (country_id==philippine_id){
		$('#home_address').remove();
		$('#home_address_control_controls').append(show_provinces('home_address_province'));
	} else {
		$('#home_address').remove();
		$('#home_address_province').remove();
		$('#home_address_town').remove();
		$('#home_address_barangay_id').remove();
		$('#home_address_control_controls').append('<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="home_address" name="home_address" />');
	}
	
});

$('#home_address_province').live('change', function(){
	var province_id = $(this).val();

	$('#home_address_town').remove();
	$('#home_address_barangay_id').remove();
	$('#home_address').remove();
	show_towns(province_id, 'home_address_town', 'home_address_control_controls');
});

$('#home_address_town').live('change', function(){
	var town_id = $(this).val();
	
	$('#home_address_barangay_id').remove();
	$('#home_address').remove();
	show_barangays(town_id, 'home_address_barangay_id', 'home_address_control_controls', false, false, 'home_address');
});

$('#home_address_barangay_id').live('change', function(){
	
	$('#home_address').remove();
	$('#home_address_control_controls').append('<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="home_address" name="home_address" />');
});

/* city address */
$('#city_address_barangay_id').live('change', function(){
	var barangay_id = $(this).val();
	$('#city_address').remove();

	if (isNumber(barangay_id)) {
		$('#city_address').remove();
		$('#city_address_control_controls').append('<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="city_address" name="city_address" />');
	}
});

/** Father's Address **/
$('#fathers_address_country').live('change', function(){
	var country_id = $(this).val();
	
	if (country_id==philippine_id){
		$('#fathers_address').remove();
		$('#fathers_address_control_controls').append(show_provinces('fathers_address_province'));
	} else {
		$('#fathers_address').remove();
		$('#fathers_address_province').remove();
		$('#fathers_address_town').remove();
		$('#fathers_address_barangay_id').remove();
		$('#fathers_address_control_controls').append('<input style="margin-left: 5px"  placeholder="House Num, Street Name, etc." type="text" class="input-xlarge" id="fathers_address" name="fathers_address" />');
	}

	if ( ! isNumber(country_id)){
		$('#fathers_address').remove();	
	}
});

$('#fathers_address_province').live('change', function(){
	var province_id = $(this).val();

	$('#fathers_address_town').remove();
	$('#fathers_address_barangay_id').remove();
	$('#fathers_address').remove();
	show_towns(province_id, 'fathers_address_town', 'fathers_address_control_controls');
});

$('#fathers_address_town').live('change', function(){
	var town_id = $(this).val();
	
	$('#fathers_address_barangay_id').remove();
	$('#fathers_address').remove();
	show_barangays(town_id, 'fathers_address_barangay_id', 'fathers_address_control_controls', false, false, 'fathers_address');
});

$('#fathers_address_barangay_id').live('change', function(){
	
	$('#fathers_address').remove();
	$('#fathers_address_control_controls').append('<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="fathers_address" name="fathers_address" />');
});

/** Mother's Address **/
$('#mothers_address_country').live('change', function(){
	var country_id = $(this).val();
	
	if (country_id==philippine_id){
		$('#mothers_address').remove();
		$('#mothers_address_control_controls').append(show_provinces('mothers_address_province'));
	} else {
		$('#mothers_address').remove();
		$('#mothers_address_province').remove();
		$('#mothers_address_town').remove();
		$('#mothers_address_barangay_id').remove();
		$('#mothers_address_control_controls').append('<input style="margin-left: 5px" placeholder="House Num, Street Name, etc."  type="text" class="input-xlarge" id="mothers_address" name="mothers_address" />');
	}

	if ( ! isNumber(country_id)){
		$('#mothers_address').remove();	
	}
});

$('#mothers_address_province').live('change', function(){
	var province_id = $(this).val();

	$('#mothers_address_town').remove();
	$('#mothers_address_barangay_id').remove();
	$('#mothers_address').remove();
	show_towns(province_id, 'mothers_address_town', 'mothers_address_control_controls');
});

$('#mothers_address_town').live('change', function(){
	var town_id = $(this).val();
	
	$('#mothers_address_barangay_id').remove();
	$('#mothers_address').remove();
	show_barangays(town_id, 'mothers_address_barangay_id', 'mothers_address_control_controls', false, false, 'mothers_address');
});

$('#mothers_address_barangay_id').live('change', function(){
	
	$('#mothers_address').remove();
	$('#mothers_address_control_controls').append('<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="mothers_address" name="mothers_address" />');
});

/** secondary school address **/
$('#secondary_school_address_country').live('change', function(){
	var country_id = $(this).val();
	
	if (country_id==philippine_id){
		$('#secondary_school_address').remove();
		$('#secondary_school_address_control_controls').append(show_provinces('secondary_school_address_province'));
	} else {
		$('#secondary_school_address').remove();
		$('#secondary_school_address_province').remove();
		$('#secondary_school_address_town').remove();
		$('#secondary_school_address_barangay_id').remove();
		$('#secondary_school_address_control_controls').append('<input style="margin-left: 5px"  placeholder="House Num, Street Name, etc." type="text" class="input-xlarge" id="secondary_school_address" name="secondary_school_address" />');
	}

	if ( ! isNumber(country_id)){
		$('#secondaray_school_address').remove();	
	}
});

$('#secondary_school_address_province').live('change', function(){
	var province_id = $(this).val();

	$('#secondary_school_address_town').remove();
	$('#secondary_school_address_barangay_id').remove();
	$('#secondary_school_address').remove();
	show_towns(province_id, 'secondary_school_address_town', 'secondary_school_address_control_controls');
});

$('#secondary_school_address_town').live('change', function(){
	var town_id = $(this).val();
	
	$('#secondary_school_address_barangay_id').remove();
	$('#secondary_school_address').remove();
	show_barangays(town_id, 'secondary_school_address_barangay_id', 'secondary_school_address_control_controls', false, false, 'secondary_school_address');
});

$('#secondary_school_address_barangay_id').live('change', function(){
	
	$('#secondary_school_address').remove();
	$('#secondary_school_address_control_controls').append('<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="secondary_school_address" name="secondary_school_address" />');
});

// Emergency Address:
$('#emergency_address_country').live('change', function(){
	var country_id = $(this).val();
	
	if (country_id==philippine_id){
		$('#emergency_address').remove();
		$('#emergency_address_control_controls').append(show_provinces('emergency_address_province'));
	} else {
		$('#emergency_address').remove();
		$('#emergency_address_province').remove();
		$('#emergency_address_town').remove();
		$('#emergency_address_barangay_id').remove();
		$('#emergency_address_control_controls').append('<input style="margin-left: 5px"  placeholder="House Num, Street Name, etc." type="text" class="input-xlarge" id="emergency_address" name="emergency_address" />');
	}

	if ( ! isNumber(country_id)){
		$('#emergency_address').remove();	
	}
});

$('#emergency_address_province').live('change', function(){
	var province_id = $(this).val();

	$('#emergency_address_town').remove();
	$('#emergency_address_barangay_id').remove();
	$('#emergency_address').remove();
	show_towns(province_id, 'emergency_address_town', 'emergency_address_control_controls');
});

$('#emergency_address_town').live('change', function(){
	var town_id = $(this).val();
	
	$('#emergency_address_barangay_id').remove();
	$('#emergency_address').remove();
	show_barangays(town_id, 'emergency_address_barangay_id', 'emergency_address_control_controls', false, false, 'emergency_address');
});

$('#emergency_address_barangay_id').live('change', function(){
	
	$('#emergency_address').remove();
	$('#emergency_address_control_controls').append('<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="emergency_address" name="emergency_address" />');
});
	
function show_provinces (control_id){
	var content = '<select style="margin-left: 5px" id="' + control_id + '" name="' + control_id + '"><option>--Select Province--</option>';
	$.each(provinces, function(index, value){
		content = content + '<option value="' + value.id + '">' + value.name + '</option>';
	});
	content = content + '</select>';
	return content;
}

function show_towns (province_id, control_id, caller){
	
	$.ajax({
		url: "<?php echo site_url("ajax/towns"); ?>/?id=" + province_id,
		dataType: "json",
		success: function(data){
			var content = '<select style="margin-left: 5px" id="' + control_id + '" name="' + control_id + '"><option value="0">--Select Town--</option>';
					
			$.each(data, function(index, value){
				content = content + '<option value="' + value.id + '">' + value.name + '</option>';
			});
			content = content + '</select>';
			$('#'+caller).append(content);
		}	
	});
}

function show_barangays (town_id, control_id, caller, include_same_as_home, include_none, textinput){
	
	$.ajax({
		url: "<?php echo site_url("ajax/barangays"); ?>/?id=" + town_id,
		dataType: "json",
		success: function(data){
			var content = '<select style="margin-top: 5px" id="' + control_id + '" name="' + control_id + '"><option value="0">--Select Barangay (optional)--</option>';

			if(include_none){
				content = content + '<option value="none">No City Address</option>';
			}

			if (include_same_as_home){
				content = content + '<option value="same_as_home">Same as Home Address</option>';
			}		
			$.each(data, function(index, value){
				content = content + '<option value="' + value.id + '">' + value.name + '</option>';
			});
			content = content + '</select>';

			content = content + '<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="' + textinput + '" name="' + textinput + '" />';
			$('#'+caller).append(content);
		}	
	});
}

function show_countries (control_id, caller, include_same_as_home){
	
	var content = '<select id="' + control_id + '" name="' + control_id + '"><option value="0">--Select Country--</option>';

	if (include_same_as_home){
		content = content + '<option value="same_as_home">Same as Home</option>';
	}
	
	content = content + '<option value="' + philippine_id + '">Philippines</option>';
	$.each(countries, function(index, value){
		if (value.id != philippine_id) {
			content = content + '<option value="' + value.id + '">' + value.name + '</option>';
		}
	});
	content = content + '</select>';
	$('#'+caller).append(content);
}

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
</script>

