<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function show_course1()
{
	show('course1');
	hide('course2');
}
function show_course2()
{
	hide('course1');
	show('course2');
}
</script>

<body onLoad="show_course1();">

 <div style="background:#FFF; width:40%; text-align:center; margin:auto;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head" style="text-align:center; padding:10px;">
  Advise Course</td>
  </tr>
  <tr>
    <td colspan="3">
<form action="<?php echo site_url("dean/student/{$student['idnum']}"); ?>" method="post">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value="advise_course" />
  <input type="hidden" name="student_histories_id" value="<?php print($student_histories_id); ?>" />
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
<tr>
  <td width="97" align="left" valign="middle">Source</td>
  <td width="3" align="left" valign="middle">:</td>
  <td width="401" style="text-align:left; vertical-align:middle;"><input name="course_type" type="radio" class="form" onClick="show_course1();" value="P" checked="checked"/>
    From Prospectus
      <input name="course_type" type="radio" class="form" onClick="show_course2();" value="A"/> 
    All Courses</td>
</tr>
<tr>
  <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="course1">
    <tr>
      <td width="24%" align="left">Course Code</td>
      <td width="1%" align="left">:</td>
      <td width="75%" align="left"><select name="courses_id1" class="form">
        <?php
				foreach($CourseFromProspectus AS $course1) {
					print("<option value=".$course1->courses_id.">".$course1->course_code."</option>\n");	
				}
			?>
      </select></td>
    </tr>
  </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="course2">
      <tr>
        <td width="24%" align="left">Course Code</td>
        <td width="1%" align="left">:</td>
        <td width="75%" align="left"><select name="courses_id2" class="form">
          <?php
				foreach($CourseFromCourses AS $course) {
					print("<option value=".$course->id.">".$course->course_code."</option>\n");	
				}
			?>
        </select></td>
      </tr>
    </table>
    </td>
  </tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td align="left"><input type="submit" name="button" id="button" value="Advise Course!"  class="btn btn-success" /></td>
</tr>
    </table>
</form>
    
<div style="width:95%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-hover table-condensed" style="margin:10px;">
	    <thead>
  <tr>
    <th width="40%" class="header">Code</th>
    <th width="9%" class="header">Credit Units </th>
      <th width="39%" class="header">Advised by </th>
    <th width="12%" class="header">Delete</th>
  </tr>
	</thead>
	<?php
		if ($AdvisedCourses) {
			foreach ($AdvisedCourses AS $AdvisedCourse) {
	
	?>
  <tr>
    <td><?php print($AdvisedCourse->course_code); ?></td>
    <td style="text-align:center;"><?php print($AdvisedCourse->credit_units); ?></td>
    <td style="text-align:left;"><?php print($AdvisedCourse->lname.", ".$AdvisedCourse->fname); ?></td>
    <td style="text-align:center; vertical-align:middle;">
        <a class="delete_ad_course" href="<?php print($AdvisedCourse->id);?>">
        <i class="icon-trash"></i>
        </a></td>
  </tr>
	<?php
			}
		}	
	?>
  <thead>
  <tr>
    <th colspan="4" class="header" style="text-align:left">&nbsp; &nbsp;</th>
    </tr>
  </thead>
</table>
</div>
    </td>
    </tr>
    </table>
    
    <div style="margin-top:10px; text-align:left; margin-left: 5px; font-weight: bold; color: red;">
    	Note: Advised courses are automatically considered NOT BRACKETED.
    </div>
    </div>

</body>

<form id="delete_ad_course" action="<?php echo site_url("dean/student/{$student['idnum']}"); ?>" method="post">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value="delete_advised_course" />

</form>

<script>
	$('.delete_ad_course').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to remove the advised course?');

		if (confirmed){

			var advised_course_id = $(this).attr('href');
		
			
			$('<input>').attr({
				type: 'hidden',
				name: 'advised_course_id',
				value: advised_course_id,
			}).appendTo('#delete_ad_course');
			$('#delete_ad_course').submit();
		}
		
	});
</script>


