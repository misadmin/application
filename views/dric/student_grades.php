<?php 
$headers = array(
				array('id'=>'catalog', 'name'=>'Catalog #', 'width'=>20),
				array('id'=>'section', 'name'=>"", 'width'=>5),
				array('id'=>'title', 'name'=>"Descriptive Title", 'width'=>35),
				array('id'=>'units', 'name'=>"Units", 'width'=>10),
				array('id'=>'prelim', 'name'=>"Prelim", 'width'=>10),
				array('id'=>'midterm', 'name'=>"Midterm", 'width'=>10),
				array('id'=>'finals', 'name'=>"Finals", 'width'=>10)
		);

$data = array();
$year_level 		= $grades[0]->year_level;
$course_that_term 	= $grades[0]->abbreviation;
$term_that_time 	= $grades[0]->term;
$sy_that_time 		= $grades[0]->sy;

foreach($grades as $grade){
	$data[] = array(
			'catalog'=>$grade->course_code, 
			'section'=>$grade->section_code, 
			'title'=>$grade->descriptive_title, 
			'units'=>$grade->credit_units,
			'prelim'=>$grade->prelim_grade,
			'midterm'=>$grade->midterm_grade,
			'finals'=>$grade->finals_grade,
		);
}
$this->print_lib->set_table_header ($headers);
$this->print_lib->set_data($data);
?>
<?php
//this should be placed outside the html tag or body to be accessible, maybe after $this->content_lib->content();
//echo sprintf($this->config->item('jzebra_applet'), base_url()); ?>
<div id="grades_to_print" style="display:none;"><?php
echo print_lib::init();
if($this->session->userdata('printed') == 'yes')
	echo $this->print_lib->set_page_length_in_lines(33); else {
	echo $this->print_lib->set_page_length_in_lines(34);
	$this->session->set_userdata('printed', 'yes');
} 
echo print_lib::align_right($idnumber) . "\n";?>
<?php echo print_lib::align_right($familyname . ", " . $firstname) . "\n"; ?>
<?php echo print_lib::align_right($year_level . " " . $course_that_term) . "\n"; ?>
<?php echo print_lib::align_right($term_that_time . " SY " . $sy_that_time) . "\n"; ?>
<?php echo print_lib::align_right('Student Grades') . "\n"; ?>
<?php echo $this->print_lib->draw_table(); ?>
<?php echo "\n\n\n"?>
<?php echo $this->print_lib->align_right('________________________', 78); ?>
<?php echo "\n" . $this->print_lib->align_right('Registrar      ', 78); ?>
<?php echo "\n\n\n\nStudent's Grades printed by HNUMIS. " . date(DATE_RFC822) . "\n\n"; ?>
</div>
<script>
function print(content) {
    var dapplet = document.jzebra;
    if (dapplet != null) {
    	dapplet.append('\x1B\x40');
       dapplet.append(content);
       dapplet.print();
	}
}
    
$(document).ready(function(){
	$('#print_grade').click(function(){
		var content_to_print = $('#grades_to_print').text();
		var vapplet = document.jzebra;
		if (vapplet != null) {
			// Searches for default printer
			vapplet.findPrinter();
		}
		print(content_to_print);
	});
	

	$('#print_schedule').click(function(){
		var content_to_print = $('#content_to_print').text();
		var applet = document.jzebra;
		if (applet != null) {
			// Searches for default printer
			applet.findPrinter();
		}
		print(content_to_print);
	});

	
});
</script>