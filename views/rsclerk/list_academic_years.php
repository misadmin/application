<div style="float:left; width:auto; margin-top:10px">
<div class="formSep">
	<h2>Select Academic Year</h2>
</div> 

<div class="row-fluid">
	<div class="span7">
		<form action="" method="post" class="form-horizontal">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" value="retrieve_advisers" name="step" />
			<fieldset>
				<div class="control-group">
					<label for="" class="control-label">Academic Year: </label>
						<div class="controls">
							<select id="academic_years_id" name="academic_years_id">
								<!--  <option value="">-- Select Academic Year --</option>  -->
					          	<?php
									foreach($academicyears AS $ay) {
										if ($ay) {
											print("<option value=".$ay->id);
											print(">".$ay->start_year." - ".$ay->end_year."</option>");
										} else {
											print("<option value=".$ay->id.">".$ay->start_year." ".$ay->end_year."</option>");
										}	
									}
								?>
							</select>
						</div>
				</div>
				<div class="control-group">
					<label for="" class="control-label">Levels: </label>
						<div class="controls">
							<select id="levels_id" name="levels_id">
								<!--  <option value="">-- Select Levels --</option>  -->
					          	<?php
									foreach($levels AS $level) {
										if ($level) {
											print("<option value=".$level->id);
											print(">".$level->level."</option>");
										} else {
											print("<option value=".$level->id.">".$level->level."</option>");
										}	
									}
								?>
							</select>
						</div>
				</div>
				
				
				<div class="form-actions">
						<button class="btn btn-success" type="submit">Continue >></button>
				</div>	
			</fieldset>
		</form>
	</div>
</div>
<!-- </div> -->