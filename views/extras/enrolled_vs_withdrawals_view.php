<style>
	table.balances 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
	
	tr.bold {font-weight:bold;}
	em { color: red; }
</style>

<div class="loading"></div>

<div class="span8">

<h3 class="heading">List of Enrollments vs Withdrawals</h3>

<form id="frm1" method="post" action=""  class="form-inline">
<?php echo $this->common->hidden_input_nonce(); ?>

<fieldset>
	<div>
		    <div class="controls">
		    	<label for="end_yr">SY:
					<select id="end_yr" name="end_yr">
						<?php foreach ($end_yr as $key=>$value): ?>
							<option <?php echo ($value==$this->input->post('end_yr') ? 'selected="selected"' : ''); ?> value="<?php echo $value; ?>"><?php echo $value-1 .'-' . $value; ?></option>						
						<?php endforeach; ?>
					</select>		    
				<button type="submit" class="btn">Submit</button>
				</label>
			</div>
	</div>

</fieldset>
</form>
<hr>


<?php 
	if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {

?>


<table id="withdrawals_t" class="table table-bordered table-striped table-condensed balances">
<thead>
	<tr><th colspan="11">SY <?php echo $this->input->post('end_yr') - 1 . '-' . $this->input->post('end_yr'); ?>
	<tr><th colspan="3"></th>
		<th colspan="2">1st Sem</th>
		<th colspan="2">2nd Sem</th>
		<th colspan="2">Summer</th>
		<th colspan="2">Total</th>
	</tr>		
		
	<tr>
	<th></th>
	<th>College</th>
	<th>Program</th>
	<th>Gross</th>
	<th>Net</th>
	<th>Gross</th>
	<th>Net</th>
	<th>Gross</th>
	<th>Net</th>
	<th>Gross</th>
	<th>Net</th>	
	</tr>
</thead>

<?php
	//$terms = array(1=>'1st Sem', 2=>'2nd Sem', 3=>'Summer',);
	$gross_1 = 0; $gross_2 = 0; $gross_3 = 0; $total_gross = 0 ; 
	$net_1 = 0; $net_2 = 0; $net_3 = 0; $total_net = 0;
	
	if (count($rows) < 1) {
		echo '<td class="center" colspan=11>No record found!</td>';
	}else{
		$row_num = 0;
		foreach($rows as $row){
				$row_num++;
				$gross_1 += $row->enr_1 + $row->wit_1;
				$gross_2 += $row->enr_2 + $row->wit_2;
				$gross_3 += $row->enr_3 + $row->wit_3;
				$net_1   += $row->enr_1 ;
				$net_2   += $row->enr_2 ;
				$net_3   += $row->enr_3 ;
				$total_gross += ($row->enr_1  + $row->enr_2 + $row->enr_3) + ($row->wit_1 + $row->wit_2 + $row->wit_3);    
				$total_net   += ($row->enr_1  + $row->enr_2 + $row->enr_3) ;  
				
?>
				<tr>
				<td class="center"><?php echo $row_num .'.'; ?> </td>
				<td class="left"><?php echo $row->college_code; ?></td>
				<td class="left"><?php echo $row->abbreviation; ?></td>
				<td class="center"><?php echo number_format($row->enr_1 + $row->wit_1,0); ?></td>
				<td class="center"><?php echo number_format($row->enr_1,0); ?></td>
				
				<td class="center"><?php echo number_format($row->enr_2 + $row->wit_2,0); ?></td>
				<td class="center"><?php echo number_format($row->enr_2,0); ?></td>

				<td class="center"><?php echo number_format($row->enr_3 + $row->wit_3,0); ?></td>
				<td class="center"><?php echo number_format($row->enr_3,0); ?></td>

				<td class="center"><?php echo number_format($row->enr_1 +$row->enr_2 + $row->enr_3 +  $row->wit_1 +  $row->wit_2 +  $row->wit_3,0); ?></td>
				<td class="center"><?php echo number_format($row->enr_1 + $row->enr_2 + $row->enr_3,0); ?></td>
				
				
				</tr>		

<?php
			}
?>			
			<tr class="bold">
				<td class="center" colspan="3"><strong>TOTAL...</strong></td>
				<td class="center"><?php echo number_format($gross_1,0); ?></td>
				<td class="center"><?php echo number_format($net_1,0); ?></td>
				<td class="center"><?php echo number_format($gross_2,0); ?></td>
				<td class="center"><?php echo number_format($net_2,0); ?></td>
				<td class="center"><?php echo number_format($gross_3,0); ?></td>
				<td class="center"><?php echo number_format($net_3,0); ?></td>
				<td class="center"><?php echo number_format($total_gross,0); ?></td>
				<td class="center"><?php echo number_format($total_net,0); ?></td>
				
			</tr>
<?php 			
		} 		
	}
?>

</table>

</div>


<script>
$(document).ready(function(){
	$("#withdrawals_t").stickyTableHeaders();
    $("#withdrawals_t").stickyTableHeaders('updateWidth');

	$('#frm1').validate({
		submitHandler: function(form) {
			$('.loading').delay(5).fadeIn(100);
			form.submit();
		}
	});
    
	
});

</script>