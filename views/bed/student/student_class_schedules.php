

		<div style="width:<?php 
								if (isset($schedule_width)) {
									print($schedule_width); 
								} else {
									print("100%");
								}
							?>;" >

			<?php 
				if($show_pulldown) {
			?>
					<div style="width:98%; margin:0 auto; ">
						<?php 
							$data = array(
										'academic_terms'=>$academic_terms,
										'selected_term'=>$selected_term,
										'select_id'=>'class_sched',
										'change_select'=>'change_class_sched');
							$this->load->view('bed/reports/pulldown_academic_terms',$data);
						?>
					</div>
			<?php
				}
			?>
			
			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; " id="display_schedules" >
				<?php 
					$data = array(
								'class_schedules'=>$class_schedules,
							);	
					$this->load->view('bed/student/student_class_schedules_with_teacher',$data);
				?>
			</div>
		</div>
		
		
			
<script>
	$(document).ready(function(){
		$('.change_class_sched').bind('change', function(){
			
			var element2    = $("option:selected", "#class_sched");
			var term_id     = element2.val();
			
			var student_idno = '<?php print($idnum); ?>';

			$('#show_change_icon_class_sched').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+student_idno,
				data: {	"term_id": term_id,
						"action": "change_term_for_class_sched" },
				dataType: 'json',
				success: function(response) {													

					$('#display_schedules').html(response.output); 

					$('#show_change_icon_class_sched').html("")
										
				}
			});

		});
	});
</script> 

