<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
</style>

  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" class="head1" style="width:42%;">
    <form method="post" id="delete_privilege_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value= "delete_privilege_availed"/>
  <?php
	if (isset($privilege_availed)) {
  ?>
  <input type="hidden" name="privilege_availed_id" value="<?php print($privilege_availed->id); ?>" > 
	<?php
		}
	?>
	
	
	  <tr class="head">
        <td colspan="3" class="head"> DELETE ASSIGNED PRIVILEGES</td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside" style="width:100%;">
           <tr>
            <td width="125" valign="middle">Term</td>
            <td width="6" valign="middle">:</td>
            <td width="85" valign="middle"> <?php print($term->term." ".$term->sy); ?> </td>
          </tr>	   
		  <tr>
            <td valign="middle">Privilege</td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print($privilege->scholarship); ?></td>
		  </tr>
		   <tr>
            <td valign="middle"> Tuition Percentage  </td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print(number_format($tuition_percentage,1)); ?> % </td>
          </tr>
		   <tr>
		     <td valign="middle">Units</td>
		     <td valign="middle">:</td>
		     <td valign="middle" style="font-weight:bold;"><?php print($paying_units); ?></td>
	        </tr>
			<?php
				if ($other_privileges) {
			?>
		   <tr>
		     <td colspan="3" valign="middle"><strong><u> Privileges</u></strong></td>
	        </tr>
			<?php
					$total = 0;
					foreach($other_privileges AS $other) {
			?>
		   <tr>
		     <td valign="middle" style="padding-left:15px;">
			 <?php 
					if ($other->weight != 1) {
						if ($privilege->posted == 'N') {
				?>
			<?php
						}
					}
			?>		 
			 <?php print($other->fees_group);  ?></td>
		     <td valign="middle">:</td>
		     <td align="right" valign="middle" style="padding-right:15px;">
			 		<?php 
						print(number_format($other->discount_amount,2)); 
						$total = $total + $other->discount_amount;
			 		 ?></td>
	        </tr>
			<?php
					}
				}
			?>
		   <tr style="font-weight:bold;">
		     <td align="right" valign="middle" style="padding-left:15px;">TOTAL</td>
		     <td valign="middle">:</td>
		     <td align="right" valign="middle" style="padding-right:15px;"><?php print(number_format($total,2));  ?></td>
	      </tr>
		  <tr>
  				<td align ="center"><input type="submit" name="delete" id="delete" value="Delete!" /></td>
		</tr>
        </table></td>
      </tr>
  </form>
    </table>

  </div>
  
  <script>
$('#delete').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to delete privilege of student?");

	if (confirmed){
		$('#delete_privilege_form').submit();
	}		
});
</script>

