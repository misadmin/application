<?php

	class reports_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		public function registered_not_enrolled() {
		
			$this->CI->load->model('hnumis/shs/track_strand_model');
			$this->CI->load->model('hnumis/AcademicYears_Model');
			$this->CI->load->model('hnumis/shs/shs_student_model');
			
			$terms   = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
			$strands = $this->CI->track_strand_model->ListStrands();
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':
		
					$this->CI->load->model('academic_terms_model');
					
					$academic_terms  = $this->CI->academic_terms_model->current_academic_term();

					$students        = $this->CI->shs_student_model->ListRegisteredNotEnrolled($academic_terms->id);
					$selected_term   = $academic_terms->id;
					$selected_strand = 'All';
					
					$data = array("terms"=>$terms,
									"students"=>$students,
									"strands"=>$strands,
									"selected_term"=>$selected_term,
									"selected_strand"=>$selected_strand,
								);

					break;
				
				case 'change_selection':
				
					if ($this->CI->input->post('academic_programs_id') == 'All') {
						$selected_strand = 'All';
						$students = $this->CI->shs_student_model->ListRegisteredNotEnrolled($this->CI->input->post('academic_terms_id'));						
					} else {
						$selected_strand = $this->CI->input->post('academic_programs_id');
						$students = $this->CI->shs_student_model->ListRegisteredNotEnrolled($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'));
					}
					
					$data = array("students"=>$students,
									"selected_term"=>$this->CI->input->post('academic_terms_id'),
									"selected_strand"=>$selected_strand,
								);
								
					$my_return['output'] = $this->CI->load->view('shs/reports/list_of_students', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/reports/registered_not_enrolled',$data);
			$this->CI->content_lib->content();
				
		}
		
			
		public function enrolled_not_sectioned() {
		
			$this->CI->load->model('hnumis/shs/track_strand_model');
			$this->CI->load->model('hnumis/AcademicYears_Model');
			$this->CI->load->model('hnumis/shs/shs_student_model');
			
			$terms   = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
			$strands = $this->CI->track_strand_model->ListStrands();
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':
		
					$this->CI->load->model('academic_terms_model');
					
					$academic_terms  = $this->CI->academic_terms_model->current_academic_term();

					$students        = $this->CI->shs_student_model->ListEnrolledNotSectioned($academic_terms->id);
					$selected_term   = $academic_terms->id;
					$selected_strand = 'All';
					
					$data = array("terms"=>$terms,
									"students"=>$students,
									"strands"=>$strands,
									"selected_term"=>$selected_term,
									"selected_strand"=>$selected_strand,
								);

					break;
				
				case 'change_selection':
				
					if ($this->CI->input->post('academic_programs_id') == 'All') {
						$selected_strand = 'All';
						$students = $this->CI->shs_student_model->ListEnrolledNotSectioned($this->CI->input->post('academic_terms_id'));						
					} else {
						$selected_strand = $this->CI->input->post('academic_programs_id');
						$students = $this->CI->shs_student_model->ListEnrolledNotSectioned($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'));
					}
					
					$data = array("students"=>$students,
									"selected_term"=>$this->CI->input->post('academic_terms_id'),
									"selected_strand"=>$selected_strand,
								);
								
					$my_return['output'] = $this->CI->load->view('shs/reports/list_of_students', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/reports/enrolled_not_sectioned',$data);
			$this->CI->content_lib->content();
				
		}


		public function enrollment_summary() {
		
			$this->CI->load->model('hnumis/shs/shs_enrollments_model');
			$this->CI->load->model('hnumis/shs/shs_student_model');
			$this->CI->load->model('hnumis/AcademicYears_Model');
			
			$terms   = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':
		
					$this->CI->load->model('academic_terms_model');
					
					$academic_terms = $this->CI->academic_terms_model->current_academic_term();

					$enrollees = $this->CI->shs_enrollments_model->ListEnrollments($academic_terms->id);

					$data = array("terms"=>$terms,
									"selected_term"=>$academic_terms->id,
									"enrollees"=>$enrollees,
									"term_sy"=>$academic_terms->term." ".$academic_terms->sy,
								);

					break;
				
				case 'change_selection':
				
					$enrollees = $this->CI->shs_enrollments_model->ListEnrollments($this->CI->input->post('academic_terms_id'));

					$data = array("terms"=>$terms,
									"selected_term"=>$this->CI->input->post('academic_terms_id'),
									"enrollees"=>$enrollees,
									"term_sy"=>$this->CI->input->post('term_sy'),
								);
								
					$my_return['output'] = $this->CI->load->view('shs/reports/enrollment_summary_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'extract_list_of_enrolled_students':
				
					$data['students'] = $this->CI->shs_student_model->ListEnrolled_Students($this->CI->input->post('academic_terms_id'),$this->CI->input->post('strand_id'),$this->CI->input->post('grade_level'));
					
					$my_return['output'] = $this->CI->load->view('shs/reports/list_of_enrolled_students', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'download_enrollment_summary_pdf':
				
					$data['term_sy']   = $this->CI->input->post('term_sy');
					$data['enrollees'] = json_decode($this->CI->input->post('enrollees'));

					$this->generate_enrollment_summary_pdf($data);

					return;
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/reports/enrollment_summary',$data);
			$this->CI->content_lib->content();
				
		}
		
		
		private function generate_enrollment_summary_pdf($data) {
			ob_start();

			$this->CI->load->library('my_pdf');
			$this->CI->load->library('hnumis_pdf');
		
			$this->CI->hnumis_pdf->AliasNbPages();		
			$this->CI->hnumis_pdf->set_HeaderTitle('SENIOR HIGH SCHOOL ENROLLMENT SUMMARY: '.$data['term_sy']);
			$this->CI->hnumis_pdf->AddPage('L','letter');
			
			$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->CI->hnumis_pdf->SetDrawColor(165,165,165);
			$this->CI->hnumis_pdf->Ln();

			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->SetFillColor(116,116,116);
			$this->CI->hnumis_pdf->SetTextColor(253,253,253);

			$this->CI->hnumis_pdf->Ln(3);
			$this->CI->hnumis_pdf->SetX(20);
			$this->CI->hnumis_pdf->Cell(100,15,'STRANDS',1,0,'C',true);

			$r = 1;
			$x = 120;
			$y = 38;
			$header1 = array('Grade 11','Grade 12','TOTAL');

			$w1 = array(45,45,45);

			for($i=0;$i<count($w1);$i++)
				$this->CI->hnumis_pdf->Cell($w1[$i],8,$header1[$i],1,0,'C',true);

			$header2 = array('M','F','TOTAL','M','F','TOTAL','M','F','TOTAL');

			for($i=0;$i<count($header2);$i++) {
				$this->CI->hnumis_pdf->SetXY($x,$y);
				$this->CI->hnumis_pdf->MultiCell(15,7,$header2[$i],1,'C',true);

				$x = $x + 15;
				$r++;
				if ($r > 18) {
					$r=1;
					$x=10;
					$y = $y + 28;
				}
			}

			$l = 8;
			$w = array(100,15,15,15,15,15,15,15,15,15);

			$this->CI->hnumis_pdf->SetFillColor(255,255,255);
			$this->CI->hnumis_pdf->SetTextColor(0,0,0);
			$this->CI->hnumis_pdf->SetFont('Arial','',9);

			if ($data['enrollees']) {
				$gtotal_m11=0; $gtotal_f11=0; $gtotal_11=0; $gtotal_m=0;
				$gtotal_m12=0; $gtotal_f12=0; $gtotal_12=0; $gtotal_f=0;

				foreach($data['enrollees'] AS $enrol) {
					$this->CI->hnumis_pdf->SetX(20);
					$this->CI->hnumis_pdf->Cell($w[0],$l,$enrol->description,1,0,'L',true);
					$this->CI->hnumis_pdf->Cell($w[1],$l,$enrol->m11,1,0,'C',true);            	$gtotal_m11 += $enrol->m11;
					$this->CI->hnumis_pdf->Cell($w[2],$l,$enrol->f11,1,0,'C',true); 			$gtotal_f11 += $enrol->f11;
					$this->CI->hnumis_pdf->Cell($w[3],$l,$enrol->m11+$enrol->f11,1,0,'C',true); $gtotal_11 += ($enrol->m11+$enrol->f11);
					$this->CI->hnumis_pdf->Cell($w[4],$l,$enrol->m12,1,0,'C',true); 			$gtotal_m12 += $enrol->m12;
					$this->CI->hnumis_pdf->Cell($w[5],$l,$enrol->f12,1,0,'C',true); 			$gtotal_f12 += $enrol->f12;
					$this->CI->hnumis_pdf->Cell($w[6],$l,$enrol->m12+$enrol->f12,1,0,'C',true); $gtotal_12 += ($enrol->m12+$enrol->f12);
					$this->CI->hnumis_pdf->Cell($w[7],$l,$enrol->m11+$enrol->m12,1,0,'C',true);	$gtotal_m += ($enrol->m11+$enrol->m12);
					$this->CI->hnumis_pdf->Cell($w[8],$l,$enrol->f11+$enrol->f12,1,0,'C',true);	$gtotal_f += ($enrol->f11+$enrol->f12);
					$this->CI->hnumis_pdf->Cell($w[9],$l,$enrol->m11+$enrol->f11+$enrol->m12+$enrol->f12,1,0,'C',true);
					
					$this->CI->hnumis_pdf->Ln();					
				}
				$l=10;
				$this->CI->hnumis_pdf->SetX(20);
				$this->CI->hnumis_pdf->SetFont('Arial','B',10);
				$this->CI->hnumis_pdf->SetFillColor(200,200,200);
				
				$this->CI->hnumis_pdf->Cell($w[0],$l,'GRAND TOTAL',1,0,'R',true);
				$this->CI->hnumis_pdf->Cell($w[1],$l,$gtotal_m11,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[2],$l,$gtotal_f11,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[3],$l,$gtotal_11,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[4],$l,$gtotal_m12,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[5],$l,$gtotal_f12,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[6],$l,$gtotal_12,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[4],$l,$gtotal_m,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[5],$l,$gtotal_f,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[6],$l,$gtotal_m+$gtotal_f,1,0,'C',true);				
				
			}


			$this->CI->hnumis_pdf->Output();
			
			ob_end_flush(); 	
		}
		
		
	}
	
?>