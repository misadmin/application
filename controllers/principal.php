<?php 

class Principal extends MY_Controller {
	

	public function __construct(){
		parent::__construct();		
		$this->content_lib->set_title ('BED Principal | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_footer_script('validate');
		$this->content_lib->enqueue_footer_script('autocomplete');
		$this->content_lib->enqueue_footer_script('jquery-ui');
		
		if ($this->session->userdata('role')=='principal'){
			$this->navbar_data['menu'] = array(
					'Students'=>array('Students' => 'student',
									),
					'Sections'=>array(
									'Section Management' => 'section_management'
									),				
					'Principal Management'=>array(
									'Level Management' => 'level_management',
									'Year Level Management' => 'year_level_management',
									'Subject Management' => 'subject_management',
									'Prospectus Management' => 'prospectus_management',
									'School Days Management' => 'school_days',
									),	
					'Reports'=>array('Students/Pupils Registered-Not Enrolled' => 'registered_not_enrolled',
									'Students/Pupils Enrolled-Not Sectioned' => 'enrolled_not_sectioned',
					),	

			);
		}
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		
	}
	

	public function student(){
			
		$this->content_lib->set_title('BED Principal | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		
		if ( ! empty($query)){
			
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
			
			$this->load->library('pagination_lib');
			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			}
		}
		
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->helper('student_helper');
			$this->load->model('teller/teller_model');
			$this->load->library('tab_lib');
			
			$this->content_lib->enqueue_header_style('image_area_select');
			$this->content_lib->enqueue_footer_script('image_area_select');
			$port = substr($idnum, 0, 3);
			$this->content_lib->set_title ('BED Principal | ' . $idnum);
			
			$result = $this->student_common_model->my_information($idnum);	

			if ($result !== FALSE) {
				//a user with that id number is seen...
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->mname. " " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->section_name,
						'full_home_address'	=> $result->full_home_address,
						'full_city_address' => $result->full_city_address,
						'phone_number' 		=> $result->phone_number,
						'section'			=> $result->section,
						'bed_status'		=> $result->bed_status,
				);

				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}					

				$tab_no 	= array_fill(0,26,FALSE);
				$tab_no[1] 	= TRUE;
				
				$dcontent['signature'] = base_url('assets/img/signature.png');
				
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);

				if (!is_college_student($idnum)) {

					if ($this->student_model->student_is_basic_ed($idnum)) {

						$data['students_idno'] 		= $idnum;
						$data['active_tab'] 		= 'enrollment_status';
						$data['enrollment_status'] 	= $result->enrollment_status;
	
						$this->tab_lib->enqueue_tab ('Enrollment Status', 'bed/student/tabs/enrollment_status', $data, 'assign_section', $tab_no[1]);
				
						$tab_content = $this->tab_lib->content();
						
						$this->content_lib->enqueue_body_content("", $tab_content);
	
					} else {
						$this->content_lib->set_message('Student records in Senior High School!', 'alert-error');
					}
										
				} else {
					$this->content_lib->set_message('Student records are in College! Refer to Digital-Records in Charge (DRIC)!', 'alert-error');						
				}				
				
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}

		}
				
		$this->content_lib->content();	
	}
	

	public function level_management() {
		$this->load->library('bed/year_level_lib');
	
		$this->year_level_lib->level_management();
	}


	public function year_level_management() {
		$this->load->library('bed/year_level_lib');
	
		$this->year_level_lib->year_level_management();
	}

	
	public function subject_management() {
		$this->load->library('bed/subject_lib');
	
		$this->subject_lib->subject_management();
	}

	public function prospectus_management() {
		$this->load->library('bed/prospectus_lib');
	
		$this->prospectus_lib->prospectus_management();		
	}

	public function prospectus_courses_management($prospectus_id=null) {
		$this->load->library('bed/prospectus_lib');
	
		if (!$prospectus_id) {
			redirect('principal/prospectus_management', 'refresh');
		} else {
			$this->prospectus_lib->prospectus_courses_management($prospectus_id);
		}
	}
	
	public function section_management() {
		$this->load->library('bed/section_lib');
	
		$this->section_lib->section_management();		
		
	}

	public function detailed_section_management($section_id=null) {
		$this->load->library('bed/section_lib');
	
		if (!$section_id) {
			redirect('principal/section_management', 'refresh');
		} else {
			$this->section_lib->detailed_section_management($section_id);
		}
	}

	
	//NOTE: this function handles all actions for the student tabs
	/*public function process_student_action($idnum) {
		
		if (!$this->input->post('action')) {
			redirect('shschair/student', 'refresh');
		}
		
		$this->load->library('shs/shs_student_lib');
	
		$this->shs_student_lib->process_student_action($idnum);
		
	}


	public function registered_not_enrolled() {
		$this->load->library('shs/reports_lib');
	
		$this->reports_lib->registered_not_enrolled();		
		
	}

	public function enrolled_not_sectioned() {
		$this->load->library('shs/reports_lib');
	
		$this->reports_lib->enrolled_not_sectioned();		
		
	}


	public function shs_enrollment_summary() {
		$this->load->library('shs/reports_lib');
	
		$this->reports_lib->enrollment_summary();
			
	}
	

	public function school_days() {
		$this->load->library('shs/attendance_lib');
	
		$this->attendance_lib->school_days();		
	}
	
	public function teacher_loads() {
		$this->load->library('shs/shs_faculty_lib');
	
		$this->shs_faculty_lib->teacher_loads();		
	}


	public function grades_submission_report() {
		$this->load->library('shs/shs_faculty_lib');
	
		$this->shs_faculty_lib->grades_submission_report();		
	}*/

	
	/*
	 * ADDED: 4/14/19 
	 * by genes
	 */
	public function enrolled_not_sectioned() {
		$this->load->library('bed/enrolment_lib');
	
		$this->enrolment_lib->enrolled_not_sectioned();
	
	}

	
	/*
	 * ADDED: 4/14/19 
	 * by genes
	 */
	public function registered_not_enrolled() {
		$this->load->library('bed/enrolment_lib');
	
		$this->enrolment_lib->registered_not_enrolled();
	
	}

	
}

?>