<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
</style>

  <div style="background:#FFF; ">
    <table width="30%" align="center" cellpadding="0" cellspacing="0" class="head1" style="width:42%;">
    <form method="post" id="remove_credited_course_form" action="<?php echo site_url("dean/student/{$idno}")?>">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value= "remove_credited_course"/>
  <input type="hidden" name="credited_course" value= "<?php print($id)?>"/>
  
	  <tr class="head">
        <td colspan="3" class="head"> Credited Course </td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside" style="width:100%;">
           <tr>
            <td width="112" valign="middle">Course Credited </td>
            <td width="8" valign="middle">:</td>
            <td width="59" valign="middle"> <?php print($couse_code);?> </td>
          </tr>	   
		  <tr>
            <td valign="middle">Grade</td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print($grade); ?></td>
		  </tr>
		   <tr>
            <td valign="middle">Taken in HNU </td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print($is_taken_here); ?></td>
          </tr>
		    <tr>
            <td valign="middle"> Notes </td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print($notes); ?></td>
          </tr>
		   
		   
		
		  <tr>
  				<td align ="center"><input type="submit" name="credited_course_id" id="credited_course_id" value="Remove credited course!" /></td>
		</tr>
        </table></td>
      </tr>
  </form>
    </table>

  </div>
  
 <script>
$('#credited_course_id').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to remove credited course?");

	if (confirmed){
		$('#remove_credited_course_form').submit();
	}		
});
</script>

