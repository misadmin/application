
			<div style="padding:3px; overflow:auto; width:40%;">
					<table class="table table-hover table-bordered table-striped table-condensed" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:20%;">Detail</td>
								<td class="shs_header" style="width:70%;">Assigned</td>
								<td class="shs_header" style="width:10%;">Update</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Class Adviser</td>
								<td class="show_class_adviser">
									<?php print($section->class_adviser); ?>
								</td>
								<td style="text-align:center;">
									<a href="<?php print($section->id); ?>" 
										decription="<?php print("Grade ".$section->grade_level." - ".$section->section_name); ?>"
										strand="<?php print($section->strand); ?>"
										block_adviser="<?php print($section->block_adviser); ?>"
										class="assign_section_adviser" >
										<div id="show_assign_adviser_icon" >
											<i class="icon-edit"></i>
										</div>
									</a>
								</td>
							</tr>
							<tr>
								<td>Classroom</td>
								<td><?php print($section->room_no); ?></td>
								<td style="text-align:center;">
									<a href="<?php print($section->id); ?>" 
										decription="<?php print("Grade ".$section->grade_level." - ".$section->section_name); ?>"
										class="assign_room_no" >
										<div id="show_update_room_id_icon" >
											<i class="icon-edit"></i>
										</div>
									</a>
								</td>
							</tr>
							<tr>
								<td>Section Name</td>
								<td><?php print($section->section_name); ?></td>
								<td style="text-align:center;">
									<a href="<?php print($section->id); ?>" 
										decription="<?php print("Grade ".$section->grade_level." - ".$section->section_name); ?>"
										strand="<?php print($section->strand); ?>"
										section_name="<?php print($section->section_name); ?>"
										class="update_section_name" >
										<div id="show_update_sectionname_icon" >
											<i class="icon-edit"></i>
										</div>
									</a>
								</td>
							</tr>
							<tr>
								<td>Strand</td>
								<td><?php print($section->strand); ?></td>
								<td style="text-align:center;">
									<?php 
										if (!$assessment) {
									?>			
										<a href="<?php print($section->id); ?>" 
											decription="<?php print("Grade ".$section->grade_level." - ".$section->section_name); ?>"
											strand_id="<?php print($section->strand_id); ?>"
											class="update_strand_assign" >
											<div id="show_update_strand_icon" >
												<i class="icon-edit"></i>
											</div>
										</a>
									<?php 
										} else {
											print("<i class=\"icon-ban-circle\">");
										}
									?>
								</td>
							</tr>
						</tbody>
					</table>
			</div>

			<?php 
				if ($assessment) {
			?>		
					<div style="font-size:14px; font-weight:bold; color:#E40808; margin-top:5px;">
						Strand cannot be updated anymore! Assessment already posted!
					</div>
			<?php 
				}
			?>
<!-- JQ for Assign Adviser -->
<script>
	$('.assign_section_adviser').click(function(event){
		event.preventDefault();

		var section_id = $(this).attr('href');
		
		$('#modal_assign_section_adviser').modal('show');
		
		$('#section_id_modal').val(section_id);
		$('#description_update_modal').val($(this).attr('decription'));
		$('#strand_update_modal').val($(this).attr('strand'));
		$('#adviser_id_modal').val($(this).attr('block_adviser'));
				
	});
</script>				

	<div id="modal_assign_section_adviser" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="assign_adviser_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Assign Section Adviser</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="section_id_modal"></div>
						<?php
							$data['advisers'] = $faculty;
							$this->load->view('shs/sections/modal_view/assign_section_adviser_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Assign Adviser!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script>
	$('#assign_adviser_form').submit(function(e) {
		e.preventDefault();
		
		var section_id    = $('#section_id_modal').val();
		var block_adviser = $('#adviser_id_modal').val();

		$('#modal_assign_section_adviser').modal('hide');

		$('#show_assign_adviser_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management'));?>"+'/'+section_id,
			data: {"section_id": section_id,
					"block_adviser": block_adviser,
					"action": "assign_section_adviser" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_section_management_page').html(response.output); 
				
			}
		});
		
	});
</script>				
<!-- End for JQ Assign Adviser -->



<!-- JQ for Update Strand -->
<script>
	$('.update_strand_assign').click(function(event){
		event.preventDefault();

		var section_id = $(this).attr('href');
		
		$('#modal_update_assigned_strand').modal('show');
		
		$('#section_id_strand_update_modal').val(section_id);
		$('#description_update_strand_modal').val($(this).attr('decription'));
		$('#update_strand_id_modal').val($(this).attr('strand_id'));
						
	});
</script>				

	<div id="modal_update_assigned_strand" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_assigned_strand_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Assigned Strand</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="section_id_strand_update_modal"></div>
						<?php
							$data['strands'] = $strands;
							$this->load->view('shs/sections/modal_view/update_strand_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update Strand!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script>
	$('#update_assigned_strand_form').submit(function(e) {
		e.preventDefault();
		
		var section_id = $('#section_id_strand_update_modal').val();
		var strand_id  = $('#update_strand_id_modal').val();

		$('#modal_update_assigned_strand').modal('hide');

		$('#show_update_strand_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management'));?>"+'/'+section_id,
			data: {"section_id": section_id,
					"strand_id": strand_id,
					"action": "update_assigned_strand" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_section_management_page').html(response.output); 
				
			}
		});
		
	});
</script>				
<!-- End for JQ Update Strand -->



<!-- JQ for Update Section Name -->
<script>
	$('.update_section_name').click(function(event){
		event.preventDefault();

		var section_id = $(this).attr('href');
		
		$('#modal_update_section_name').modal('show');
		
		$('#section_id_modal').val(section_id);
		$('#description_section_modal').val($(this).attr('decription'));
		$('#strand_section_modal').val($(this).attr('strand'));
		$('#section_name_modal').val($(this).attr('section_name'));
						
	});
</script>				

	<div id="modal_update_section_name" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_section_name_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Section Name</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="section_id_modal"></div>
						<?php
							$this->load->view('shs/sections/modal_view/update_section_name_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update Section Name!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script>
	$('#update_section_name_form').submit(function(e) {
		e.preventDefault();
		
		var section_id   = $('#section_id_modal').val();
		var section_name = $('#section_name_modal').val();

		$('#modal_update_section_name').modal('hide');

		$('#show_update_sectionname_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;'/>")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management'));?>"+'/'+section_id,
			data: {"section_id": section_id,
					"section_name": section_name,
					"action": "update_section_name" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_section_management_page').html(response.output); 
				
			}
		});
		
	});
</script>				
<!-- End for JQ Update Section Name -->


<!-- JQ for Assign Room No. -->
<script>
	$('.assign_room_no').click(function(event){
		event.preventDefault();

		var section_id = $(this).attr('href');

		$('#modal_assign_room_no').modal('show');

		$('#section_id_modal').val($(this).attr('href'));
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management'));?>"+'/'+section_id,
			data: {"section_id": section_id,
					"action": "extract_vacant_rooms" },
			dataType: 'json',
			success: function(response) {													
				$('#vacant_rooms').html(response.bates_building);														
				$('#description_assign_room_modal').val(response.grade_section); 			
				$('#strand_assign_room_modal').val(response.strand); 
				$("input[name=room_id]").val([parseInt(response.rooms_id)]);
				
			}
			
		});
						
	});
</script>

	<div id="modal_assign_room_no" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="assign_room_no_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Assign Classroom</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="section_id_modal"></div>
						<?php
							$this->load->view('shs/sections/modal_view/assign_room_no_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Assign Room!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#assign_room_no_form').submit(function(e) {
		e.preventDefault();
		
		var section_id   = $('#section_id_modal').val();
		var rooms_id     = $('input[name=room_id]:checked').val();

		$('#modal_assign_room_no').modal('hide');

		$('#show_update_room_id_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;'/>")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management'));?>"+'/'+section_id,
			data: {"section_id": section_id,
					"rooms_id": rooms_id,
					"action": "assign_room_no" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_section_management_page').html(response.output); 
				
			}
		});
		
	});
</script>				
	
	