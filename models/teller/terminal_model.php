<?php 

class Terminal_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}
	public function create ($terminal_name, $terminal_description, $machine_ip){
		$mic = $this->random_text(50);
		$sql = "
			INSERT into machines
				(`machine_ip`, `inserted_by`, `inserted_on`, `name`, `description`, `mic`)
			VALUE
				({$this->db->escape($machine_ip)}, 
				{$this->session->userdata('empno')}, 
				NOW(), 
				{$this->db->escape($terminal_name)}, 
				{$this->db->escape($terminal_description)}, 
				{$this->db->escape($mic)})
			";
		//print_r($sql);die();
		$this->db->query($sql);
		if($id = $this->db->insert_id()){
			return (object)array('id'=>$id, 'mic'=>$mic);
		} else {
			return FALSE;
		}
	}
	public function retrieve ($machine_ip='', $mic=''){
		if(empty($machine_ip) && empty($mic)){
			//return all the terminals...
			$this->db->order_by('name', 'asc');
			$query = $this->db->get('machines');
			return $query->result();
		} else {
			$this->db->where('machine_ip', $machine_ip);
			//FIXME: $mic should be needed... better use dual validation... 
			//$this->db->where('mic', $mic);
			$this->db->order_by('name');
			$query = $this->db->get('machines');
			return $query->row();
		}
		
	}
	
	public function delete ($id){
		return $this->db->delete('machines', array('id'=>$id));
	}
	
	public function terminal_is_marked($ip, $mic=''){
		if(!empty($mic))
			$this->db->where('mic', $mic);
		$this->db->where('machine_ip', $ip);
		$query = $this->db->get('machines');
		if ($query->num_rows() > 0)
			return TRUE; else
			return FALSE;
	}
	
	public function update ($id, $data){
		$this->db->where('id', $id);
		$this->db->update('machines', $data);
		return $this->db->affected_rows();
	}
	
	private function random_text($length=10){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
}