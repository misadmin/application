<?php

	class Scholarship_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


	    function AddScholarshipItem($data) 
		{
			$query = "INSERT INTO scholarships (id, scholarship_code, scholarship) 
   						VALUES ('',
						   	   	 {$this->db->escape($data['scholarship_code'])},
								 {$this->db->escape($data['scholarship'])})";
			$this->db->query($query);
			$id = @mysql_insert_id();
			//print($query);
			//die();
			return $id;
			
		}

	function ListPrivileges() {
			$result = null;
				
			$q1 = "SELECT
					id,
					scholarship_code,
					scholarship
				   FROM
					scholarships
				   ORDER BY 
					  scholarship";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}					


	
	function getScholarship($privilege_availed_id){
	
		$result = null;
		
		$q = "SELECT 
					a.scholarship,
					b.id
				FROM
					scholarships as a,
					privileges_availed as b
				WHERE
					b.id = {$this->db->escape($privilege_availed_id)}
					AND b.scholarships_id = a.id";
				
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
		//print_r($query);
	   // die();
		
		return $result;
			
	}		
	
	function getTuitionPrivilege($privilege_availed_id){
	
		$result = null;
		
		$q = "SELECT 
		         (a.discount_percentage * 100) as discount_percentage,
			a.discount_amount
			 FROM
				privileges_availed_details as a
			 WHERE
				a.privileges_availed_id = {$this->db->escape($privilege_availed_id)}
				AND a.fees_groups_id = 9";
				
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
		//print_r($query);
	   // die();
		
		return $result;
			
	}		
	
	//Added: March 27, 2013 by Amie
	function AddStudentPrivilege($data) {
		
		$query = "INSERT into privileges_availed 
					(id,
					 student_histories_id,
					 scholarships_id
					 ) 
					VALUES ('',
							{$this->db->escape($data['student_history_id'])},
							{$this->db->escape($data['privilege_id'])})";
		//print($query);
		//die();
			$this->db->query($query);
			
			$id = @mysql_insert_id();
			return $id;
	}		

	function AddPrivilegeDetails($privilege_availed_id, $data) {
		
		$query = "INSERT into privileges_availed_details 
					(id, 
					  privileges_availed_id,
					  fees_groups_id,
					  discount_percentage,
					  discount_amount)
					VALUES ('',
							{$this->db->escape($privilege_availed_id)},
							{$this->db->escape($data['fees_groups_id'])},
							{$this->db->escape($data['discount_percentage'])},
							{$this->db->escape($data['discount_amount'])})";
							
		$this->db->query($query);
			
		$id = @mysql_insert_id();
		return $id;					
	}
	
	
	
//Added: 3/9/2013

	
	function ListStudentsWithPrivileges($aca_term_id, $privilege_id) {
			$result = null;
				
			$q1 = "SELECT
						a.id,
						a.discount_percentage,
						a.discount_amount,
						a.students_idno,
						a.approve_status,
						b.lname,
						b.fname,
						c.year_level,
						e.abbreviation		
				  FROM
					  privileges_availed_details as a,
					  students as b,
					  student_histories as c,
					  prospectus as d,
					  academic_programs as e
				  WHERE
				  	  a.academic_terms_id = {$this->db->escape($aca_term_id)}
					  AND a.scholarships_id = {$this->db->escape($privilege_id)}
					  AND a.students_idno = c.students_idno
					  AND c.academic_terms_id = {$this->db->escape($aca_term_id)}
					  AND c.students_idno = b.idno
					  AND c.prospectus_id = d.id
					  AND d.academic_programs_id = e.id	  
				   GROUP BY
				      a.students_idno
				   ORDER BY 
					  b.lname";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}					

	function getStudentPrivilege($privilege_id){
	
		$result = null;
		
		$q = "SELECT 
				a.id,
				a.discount_percentage,
				a.discount_amount,
				a.students_idno,
				a.approve_status,
				b.lname,
				b.fname,
				c.year_level,
				e.abbreviation,	
				f.scholarship	
		
			FROM
				privileges_availed as a,
				students as b,
				student_histories as c,
				prospectus as d,
				academic_programs as e,
				scholarships as f
				
			WHERE
				a.id = {$this->db->escape($privilege_id)}
				AND a.scholarships_id = f.id
				AND a.students_idno = c.students_idno
			    AND a.academic_terms_id = c.academic_terms_id
			    AND c.students_idno = b.idno
				AND c.prospectus_id = d.id
				AND d.academic_programs_id = e.id";
				
				
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
		//print_r($query);
	   // die();
		
		return $result;
			
	}		
	
	//Added: 3/11/2013
	
	function UpdatePrivilege($id, $privilege_id, $priv_disc){
		$q = "UPDATE privileges_availed 
			SET 
				scholarships_id ={$this->db->escape($privilege_id)},
				discount_percentage = {$this->db->escape($priv_disc)} 
			WHERE
				id={$this->db->escape($id)}";
			
	//print_r($q);
	//die();
		if ($this->db->query($q)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}

	
	
	function ListPrivilegeAvailedDetails($privilege_availed_id) {
			$result = null;
				
			$q1 = "SELECT
					b.fees_group, 
					a.discount_amount					
				   FROM
					privileges_availed_details as a,
					fees_groups as b
				   WHERE
				    a.privileges_availed_id = {$this->db->escape($privilege_availed_id)} 
					AND a.fees_groups_id = b.id
				   ORDER BY 
					 b.weight";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}					
	
}

//end of scholarship_model.php