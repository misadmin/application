<form id="class_request" method="post">
</form>
<script>
$(document).ready(function(){
	$('a.crh').bind('click', function(e) {
		e.preventDefault();
		var classType = $(this).attr("data-type");
		var className = $(this).attr("data-name");
		console.log(classType + '  ' + className);
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'type',
		    value: classType
		}).appendTo('form#class_request');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'name',
		    value: className
		}).appendTo('form#class_request');
		
	    $('form#class_request').submit();
	});
});
</script>