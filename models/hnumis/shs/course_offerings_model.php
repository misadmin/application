<?php

	class Course_offerings_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

		private $course_offerings_slots_id = NULL;
		
		
		function get_Course_Offerings_Slots_Id() {
			return $this->course_offerings_slots_id;
		}
		
		
		function AddCourseOfferings($data) {

			$q1 = "INSERT INTO
							course_offerings (academic_terms_id, 
												courses_id, 
												section_code, 
												inserted_by,
												inserted_on)
						VALUES (
							{$this->db->escape($data['academic_terms_id'])},
							{$this->db->escape($data['courses_id'])},
							{$this->db->escape($data['section_code'])},
							{$this->db->escape($data['inserted_by'])},
							NOW()) ";
			//return $q1;
			if ($this->db->query($q1)) {
				$data['course_offerings_id'] = $this->db->insert_id();
				
				if ($this->AddCourseOfferingsSlots($data)) { //add to course_offerings_slots table
					if ($this->AddBlockCourseOfferings($data)) { //add to block_course_offerings table
						return TRUE;
					} else {
						return FALSE;
					}
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
			
		}

		
		function AddCourseOfferingsSlots($data) {
			
			$q1 = "INSERT INTO
						course_offerings_slots (course_offerings_id,
												rooms_id,
												start_time,
												end_time,
												inserted_by)
						VALUES (
							{$this->db->escape($data['course_offerings_id'])},
							{$this->db->escape($data['rooms_id'])},
							{$this->db->escape($data['start_time'])},
							{$this->db->escape($data['end_time'])},
							{$this->db->escape($data['inserted_by'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				$this->course_offerings_slots_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}	
			
		}
		
		
		private function AddBlockCourseOfferings($data) {
			
			$q1 = "INSERT INTO
						block_course_offerings (block_sections_id,
												course_offerings_id,
												inserted_by)
						VALUES (
							{$this->db->escape($data['block_sections_id'])},
							{$this->db->escape($data['course_offerings_id'])},
							{$this->db->escape($data['inserted_by'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

	
		function getLastSection($courses_id,$academic_terms_id) {
			$result = null;
			
			$q = "SELECT 
						a.section_code 
					FROM
					     course_offerings AS a
					WHERE 
					    a.courses_id = {$this->db->escape($courses_id)}
						AND a.academic_terms_id={$this->db->escape($academic_terms_id)} 
					ORDER 
						BY a.id DESC LIMIT 1";
			
			//return $q;
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}

		
		function DeleteOffering($course_offerings_id=NULL) {
			
			$q2 = "DELETE FROM 
							course_offerings 
						WHERE 
							id = {$this->db->escape($course_offerings_id)} ";
			//return $q2;
			if ($this->db->query($q2)) {
				return TRUE;
			} else {
				return FALSE;
			}

		}


		function DeleteOffering_Slots($course_offerings_slots_id=NULL) {

			$q2 = "DELETE FROM 
							course_offerings_slots 
						WHERE 
							id = {$this->db->escape($course_offerings_slots_id)} ";
			//return $q2;
			if ($this->db->query($q2)) {
				return TRUE;
			} else {
				return FALSE;
			}

		}

		
		function AssignTeacher($data) {
			$q1 = "UPDATE course_offerings
					SET employees_empno = {$this->db->escape($data['employees_empno'])},
						updated_by = {$this->db->escape($data['updated_by'])}
		 			WHERE
						id = {$this->db->escape($data['course_offerings_id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
			
		}


		function UpdateOfferingSlotsRoom($data) {
			
			$q1 = "UPDATE 
							course_offerings_slots
						SET 
							rooms_id = {$this->db->escape($data['rooms_id'])}
						WHERE
							course_offerings_id IN (SELECT 
															course_offerings_id 
														FROM 
															block_course_offerings 
														WHERE 
															block_sections_id = {$this->db->escape($data['block_sections_id'])}) ";
					
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		
		}


		function UpdateSchedule_CourseOfferingsSlot($data) {
			
			$q1 = "UPDATE 
							course_offerings_slots
						SET 
							rooms_id = {$this->db->escape($data['rooms_id'])},
							start_time = {$this->db->escape($data['start_time'])},
							end_time = {$this->db->escape($data['end_time'])},
							updated_by = {$this->db->escape($data['encoded_by'])}
						WHERE
							id = {$this->db->escape($data['course_offerings_slots_id'])} ";
					
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		
		}
		
		
		function CheckIfCourseOffered($data) {
			$result = null;
			
			$q = "SELECT 
						g.courses_id,
						f.year_level,
						f.term,
						d.id
					FROM
						course_offerings AS a,
						enrollments AS b,
						block_course_offerings AS c,
						student_histories AS d,
						prospectus AS e,
						prospectus_terms AS f,
						prospectus_courses AS g
					WHERE 
						a.id = b.course_offerings_id
						AND b.course_offerings_id = c.course_offerings_id 
						AND c.block_sections_id = d.block_sections_id
						AND d.prospectus_id = e.id 
						AND e.id = f.prospectus_id 
						AND f.id = g.prospectus_terms_id 
						AND d.year_level = f.year_level
						AND a.courses_id = g.courses_id
						AND g.courses_id = {$this->db->escape($data['courses_id'])} 
						AND g.id = {$this->db->escape($data['prospectus_course_id'])} 
					LIMIT 1 ";
			
			//return $q;
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
			
		}

		
		function getCourse_Offerings($block_sections_id=NULL, $courses_id=NULL, $academic_terms_id=NULL) {
			$result = null;
			
			$q = "SELECT 
						a.id AS course_offerings_id
					FROM
					    course_offerings AS a,
						block_course_offerings AS b
					WHERE 
						a.id = b.course_offerings_id
						AND b.block_sections_id = {$this->db->escape($block_sections_id)}
					    AND a.courses_id = {$this->db->escape($courses_id)}
						AND a.academic_terms_id={$this->db->escape($academic_terms_id)} ";
			
			//return $q;
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
		
		function CountOffering_Slots($course_offerings_id=NULL) {
			$result = null;
			
			$q = "SELECT 
						COUNT(*) AS cnt
					FROM
					    course_offerings_slots AS a
					WHERE 
						a.course_offerings_id = {$this->db->escape($course_offerings_id)} ";
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
		
		
	}
	
?>
