<style>
tr.wd {
	font-weight: bold;
	font-style: italic;
	color: #D66E6E;
	background: #FFFFD9;}
</style>

<?php
//print_r(get_defined_vars());die();

?>
<div class="span11" >
	<div class="pull-right">
		<form method="post" id="schedule_current_term_form">
			<input type="hidden" value="schedule_current_term" name="action" />
			<?php $this->common->hidden_input_nonce(FALSE); ?>
	<?php if (isset($academic_terms) && !empty($academic_terms)): ?>
				<select name="academic_term" id="academic_term">
	<?php foreach ($academic_terms as $academic_term):?> 
					<option value="<?php echo $academic_term->id; ?>" <?php if($academic_term->id==$academic_terms_id) echo 'selected="selected"'?>><?php echo $academic_term->term . " " . $academic_term->sy; ?></option>
	<?php endforeach; ?>
				</select>
	<?php endif; ?>
		</form>
		<script>
			$('#academic_term').bind('change', function(){
				$('#schedule_current_term_form').submit();
			});
		</script>
	</div>
	<div style="margin-bottom:10px;">
	<?php 
		if($this->uri->segment(1) != 'evaluator') {
	?>
	<a class="download_class_sched" href="" academic_terms_id="<?php print($academic_terms_id); ?>" 
			student_histories_id="<?php print($student_histories_id); ?>"  
			max_units="<?php print($max_units); ?>"  >Download Class Schedule as PDF<i class="icon-download-alt"></i></a>
	<?php 
		}
	?>
	</div>
</div>
<div class="clearfix span11" >
	<form method="POST">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
      <input type="hidden" name="action" value="delete_enrolled_course" />
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th width="5%" style="vertical-align:middle; text-align:center;">Del.</th>
				<th width="15%">Catalog [Section]</th>
				<th width="30%">Descriptive Title</th>
				<th width="30%">Schedule / [Room] Building</th>
				<th width="15%">Teacher</th>
				<th width="5%">Units</th>
			</tr>
		</thead>
	
		<tbody>
	<?php 
			if(is_array($schedules) && count($schedules) > 0): ?>
	<?php 
			$total_credit_units = 0;
			$total_paying_units = 0;
			$total_bracketed_units = 0;
			foreach($schedules as $schedule):
				
				$total_credit_units += (float)$schedule['credit_units'];
				$total_paying_units += (float)$schedule['paying_units'];
				
	?>
			<tr <?php echo ($schedule['status']=="Withdrawn" ? 'class="wd"' : "");?>> 
				<td style="vertical-align:middle; text-align:center;">
					<input type="radio" name="enrollments_id" value="<?php print($schedule['enroll_id']); ?>"></td>
				<td style="vertical-align:middle;"> <a href="#" 
			 			title="<?php if ($assessment_date) { 
			 							if (isset($assessment_date->transaction_date) && $schedule['date_enrolled'] < $assessment_date->transaction_date)
											echo "Enrolled on ";
			 							else echo "Added on";
			 						  } else { echo "Enrolled on";} 
									  echo $schedule['date_enrolled']. " by ".$schedule['enrollee_id'];   
			 						  if ($schedule['date_withdrawn']!='') 
			 							echo "; Withdrawn on ". $schedule['date_withdrawn'] . " by " .$schedule['withdrawn_by']; ?>" >  
			 					<?php print($schedule['course_code']." [".$schedule['section_code']."]"); ?><?php echo ($schedule['status']=="Withdrawn" ? ' [WD]' : "");?> </td>
				<td style="vertical-align:middle;"><?php echo $schedule['descriptive_title']; ?></td>
				<td>
				<?php 
						$offering_slots = $this->Offerings_Model->ListOfferingSlots($schedule['id']);
						foreach ($offering_slots[0] AS $offer) {
							$sched = $offer->tym." ".$offer->days_day_code." /[".$offer->room_no."] ".$offer->bldg_name;
							print($sched."<br>");
						}
				
				?>
				</td>
				<td style="vertical-align:middle;"><?php echo $schedule['instructor']; ?></td>
				<td style="vertical-align:middle; text-align:center;">
				<?php 
					if ($schedule['is_bracketed'] == 'Y') {
						print("(".$schedule['credit_units'].")");
						$total_bracketed_units += (float)$schedule['credit_units'];
					} else {
						print($schedule['credit_units']); 
					}
				?></td>
				
			</tr>
	<?php endforeach; ?>
				
				<tr><td colspan="6" style="text-align:right"><strong>TOTAL UNITS</strong>(Credit/Pay/Bracket)<strong>: <?php echo number_format($total_credit_units,2) . '/'. number_format($total_paying_units,2) .'/'. number_format($total_bracketed_units,2); ?></strong></td>
				<tr><td colspan="6" style="text-align:right"><input type="submit" name="button" id="button" value="Delete Enrolled Course!" class="btn btn-danger" /></td>
				
					
	<?php else: ?>
			<tr>
				<td colspan="6">No Courses found for this term</td>
			</tr>
	<?php endif; ?>
		</tbody>
	</table>
	</form>
</div>

  <form id="download_class_sched_form" method="post" target="_blank">
  <input type="hidden" name="step" value="2" />
  <input type="hidden" name="action" value="generate_class_schedule" />
  </form>
<script>
$(document).ready(function(){
   
   $('.download_class_sched').click(function(event){
		event.preventDefault(); 
		var academic_terms_id = $(this).attr('academic_terms_id');
		var student_histories_id = $(this).attr('student_histories_id');
		var max_units = $(this).attr('max_units');

		$('<input>').attr({
			type: 'hidden',
			name: 'academic_terms_id',
			value: academic_terms_id,
		}).appendTo('#download_class_sched_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'student_histories_id',
			value: student_histories_id,
		}).appendTo('#download_class_sched_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'max_units',
			value: max_units,
		}).appendTo('#download_class_sched_form');
	

		$('#download_class_sched_form').submit();
		
	});
	
	
});
	
</script>