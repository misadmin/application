<?php

class Faculty extends MY_Controller {
	
	/**
	 * This method creates the instance of a faculty account.
	 */
	public function __construct(){
		parent::__construct();
		//print_r($this->userinfo);die();
		
		$this->load->model('hnumis/shs/shs_faculty_model');
		
		$class_scheds = $this->shs_faculty_model->List_MySHS_ClassSchedules($this->userinfo['empno']);
		
		if ($this->session->userdata('role')=='faculty'){		
		
			if ($class_scheds) { //faculty has a shs class
				$this->navbar_data['menu'] = array(
								'Class Schedules' => array(
									'College' => 'courses',
									'Senior High School' => 'shs_class_schedules'
								),	
								'AV Hall Reservation' => 
											array(
													'View Calendar' =>'calendar',
													'My Bookings History' => 'booking_history',
											),
						);			
			} else {
		
				$this->navbar_data['menu'] = array(
								'Class Schedules' => array(
									'College' => 'courses',
								),	
								'AV Hall Reservation' => 
											array(
													'View Calendar' =>'calendar',
													'My Bookings History' => 'booking_history',
											),
						);
			}
		}
		
		//$this->navbar_data['menu'] = $navigation;
		$this->content_lib->set_navbar_content('', $this->navbar_data);
	}
	
	/**
	 * Description:
	 * 		This function displays the class schedule of the faculty member and provides
	 * a download capability for the class list.
	 * 
	 * 
	 */
	public function courses(){
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Courses_model');
		
		$course_offering_id = $this->input->post('course_offering_id');
		$this->load->model('academic_terms_model');

		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
		
		switch ($step) {
			case 1:
				break;
				
			case 2: //Download Class List as PDF
				$this->generate_class_list_pdf();
				return;
		}
		
		if (is_numeric($course_offering_id)) {
			//a course is selected... we're going to show this course...
			$this->show_course($course_offering_id);
			return;
		}
		//A course is not set to be shown... so we're to show the courses...
		
		$data['academic_terms'] = $this->academic_terms_model->faculty_inclusive_academic_term ($this->userinfo['empno']);
		
		if ($this->input->post('academic_terms_id')) {
			$academic_terms_id = $this->input->post('academic_terms_id');
		} else {			
			//$academic_terms = $this->academic_terms_model->current_academic_term();
			//$academic_terms_id = $academic_terms->id;

			$academic_terms = $this->academic_terms_model->current_academic_term();
			$academic_terms_id = (isset($data['academic_terms'][0]->id) ? $data['academic_terms'][0]->id : $academic_terms->id);
				
			
		}
		if ($result = $this->Courses_model->faculty_courses_academic_terms($this->userinfo['empno'], $academic_terms_id))
			//print_r($result); die();
			$this->content_lib->enqueue_body_content ('faculty/list_courses', array('result'=>$result, 'term'=>$result[0]->term, 'school_year'=>$result[0]->sy, 'selected_term'=>$academic_terms_id));
		
		$data['selected_term'] = $academic_terms_id;
		$this->content_lib->enqueue_sidebar_widget ('dean/list_academic_terms1',$data, 'Select Academic Terms', 'in');
		$this->content_lib->content();
	}
	
	/**
	 * Description:
	 * This method handles the uploading of grades of the students by the faculty member.
	 * @param $course_offering_id
	 */	
	private function show_course($course_offering_id){
		
		$this->load->model("hnumis/Enrollments_model");
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/Offerings_model');
		$message = array();
		
		if ($action = $this->input->post('action')) {

			switch ($action){
				
				case 'post_grade'	:
								//check nonce...
								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
									$this->load->model('hnumis/grades_model');
									$period = $this->input->post('period');
									$error_found = FALSE; //no error found yet...
									
									//data validation for the period
									if ($period && in_array($period, $this->config->item('periods_to_be_graded'))){
										$data = $this->input->post($period);
										$course_offering_id = $this->input->post('course_offering_id');
										//data validation of the grades
										$return = $this->grades_model->course_is_under_faculty($course_offering_id, $this->userinfo['empno'], $data);
										if ( ! $return){
											$error_found = TRUE;
											$message = array('message'=>'Error seen while uploading grades.', 'severity'=>'error');
										} else {
											//course and enrollments are validated... lets validate if these grades are right...
											if ($period == 'finals'){
												$regex = $this->config->item('finals_grade_regex');		
											} else {
												$regex = $this->config->item('basic_grade_regex');
											}
											foreach ($data as $key=>$val){
												if ( ! preg_match($regex, $val)){
													$error_found = TRUE;
													$message = array('message'=>'Grades submitted have values that are not allowed.', 'severity'=>'error');	
												}
											} 
										}
									} else {
										$error_found = TRUE;
										$message = array('message'=>'Error seen while uploading grades', 'severity'=>'error');
									}
									
									
									if ( ! $error_found){
										//When all validation went right... lets upload the grades...
										if ($return = $this->grades_model->update_grades($period, $data, $course_offering_id)) {
											//SMS call...
											$sms_infos = $this->grades_model->course_code_phone_number_grade($period, $data, $course_offering_id);
											$template = "HNUSMS: [[idnumber]], your [period] grade for the subject: [course] is [grade].";
											$this->load->library('sms_client', $this->config->item('sms_api_config'));
											$this->load->model('sms_model');
											$messages = array();
											foreach ($sms_infos as $row){
												$grade_field = "{$period}_grade";
												$message = str_replace(array('[idnumber]', '[period]', '[course]', '[grade]'), array($row->students_idno, ucfirst($period), $row->course_code, $row->$grade_field), $template);
												$messages[] = (object)array('number'=>$this->sms_model->ten_digit_sms_number($row->phone_number), 'message'=>$message);
											}
											$return = json_decode($this->sms_client->send_multiple_messages($messages));
											$message = array('message'=>'Grades successfully uploaded', 'severity'=>'success'); 
										} else
											$message = array('message'=>'Error seen while uploading your grades', 'severity'=>'error'); //an error that just propped up... 
									}
									
								} else {
									$message = array('message'=>'Nonce Error', 'severity'=>'error');	
								}
								break;
				default:
								break;
			}
		}
		
		$academic_terms_id = $this->input->post('academic_terms_id');
		$this->load->model("hnumis/Courses_model");
		$courses['courses'] = $this->Courses_model->faculty_courses_academic_terms($this->userinfo['empno'], $academic_terms_id);
		$courses['academic_terms_id'] = $academic_terms_id;
		
		$data['results'] = $this->Enrollments_model->class_record($course_offering_id, $this->userinfo['empno']);
		
/**		if (!empty($data['results'])) {
			$data['academic_terms_id'] = $academic_terms_id;
			$data['course_offering_id'] = $course_offering_id;
			$data['course_code'] = $data['results'][0]->course_code;
			$data['descriptive_title'] = $data['results'][0]->descriptive_title;
			$data['section_code'] = $data['results'][0]->section_code;
			//the following code stores grades if there are any
			foreach($data['results'] as $result){
				$data['prelim_grades'] = $result->prelim_grade;
				$data['midterm_grades'] = $result->midterm_grade;
				$data['finals_grades'] = $result->finals_grade;
			}
		}
**/		

		if (!empty($data['results'])) {
			$data['academic_terms_id'] = $academic_terms_id;
			$data['course_offering_id'] = $course_offering_id;
			$data['course_code'] = $data['results'][0]->course_code;
			$data['descriptive_title'] = $data['results'][0]->descriptive_title;
			$data['section_code'] = $data['results'][0]->section_code;
			$data['prelim_grades_has_empty'] = FALSE;
			$data['midterm_grades_has_empty'] = FALSE;
			$data['finals_grades_has_empty'] = FALSE;
			//the following code stores grades if there are any
			foreach($data['results'] as $result){
				$data['prelim_grades_has_empty'] = $data['prelim_grades_has_empty'] || (empty($result->prelim_grade) || $result->prelim_grade=='-');
				$data['midterm_grades_has_empty'] = $data['midterm_grades_has_empty'] || (empty($result->midterm_grade) || $result->midterm_grade=='-');
				$data['finals_grades_has_empty'] = $data['finals_grades_has_empty'] || (empty($result->midterm_grade) || $result->finals_grade=='-');
			}
			$data['prelim_grades_has_empty'] = TRUE;
			$data['midterm_grades_has_empty'] = TRUE;
			$data['finals_grades_has_empty'] = TRUE;
		}

		if ( isset($message['message']) && ! empty($message['message'])) {
			$data['message'] = $message['message'];
			$data['severity'] = $message['severity'];
		}
		
		
		
		
		if ($this->input->post('academic_terms_id')) {
			$academic_terms_id = $this->input->post('academic_terms_id');
		} else {
			$this->load->model('academic_terms_model');
			$academic_terms = $this->academic_terms_model->current_academic_term();
			$academic_terms_id = $academic_terms->id;

		
		}
		
		$this->load->model('hnumis/AcademicYears_Model');
		$data2['academic_terms'] = $this->academic_terms_model->faculty_inclusive_academic_term ($this->userinfo['empno']);
		$data2['selected_term'] = $academic_terms_id;
		
		$slots    = $this->Offerings_model->ListOfferingSlots($course_offering_id);
		//print($slots[0][0]->id); die();
		if (!empty($slots)) {
			$data['time'] = $slots[0][0]->tym;
			$data['room'] = $slots[0][0]->room_no;
			$data['days'] = $slots[0][0]->days_day_code;
		}
		
		
		//print_r($data);die();
	
		
		$this->content_lib->enqueue_body_content('faculty/faculty_class_list', $data);
		$this->content_lib->enqueue_sidebar_widget('dean/list_academic_terms1', $data2, 'Select Academic Terms', 'in');
		$this->content_lib->enqueue_sidebar_widget('faculty/course_shortcuts', $courses, 'Course Shortcuts', 'in');
		$this->content_lib->content();
	}
	
	/**
	 * Description:
	 * This method generates a pdf of the list of students in a particular class handled by the faculty member.
	 */
	function generate_class_list_pdf() {
		
		$this->load->model("hnumis/Student_model");
		$this->load->model('hnumis/AcademicYears_model');
		$this->load->model('hnumis/Offerings_model');
		
		$students = $this->Student_model->ListEnrollees($this->input->post('course_offering_id'));
		$term     = $this->AcademicYears_model->getAcademicTerms($this->input->post('academic_terms_id'));
		$slots    = $this->Offerings_model->ListOfferingSlots($this->input->post('course_offering_id'));
		//print_r($students);
		//die();
		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		$l = 4;
		
		$this->hnumis_pdf->AliasNbPages();
		$this->hnumis_pdf->set_HeaderTitle('CLASS LIST');
		$this->hnumis_pdf->AddPage('P','letter');
			
		$this->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->hnumis_pdf->SetDrawColor(165,165,165);
		
		$this->hnumis_pdf->SetFont('times','',10);
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);
		$this->hnumis_pdf->Ln();
		
		$this->hnumis_pdf->Cell(30,$l,'School Year',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': '.$term->term.' '.$term->sy,0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Teacher',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': ['.$this->userinfo['empno'].'] '.$this->userinfo['lname'].', '.$this->userinfo['fname'],0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Class',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': '.$this->input->post('course_description'),0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Schedule/Room',0,0,'L',true);
		foreach($slots[0] AS $slot) {
			$this->hnumis_pdf->Cell(100,$l,': '.$slot->tym." ".$slot->days_day_code.' / ['.$slot->room_no.'] '.$slot->bldg_name,0,0,'L',true);
			$this->hnumis_pdf->Ln();
		}
		$l = 5;
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetFillColor(116,116,116);
		$this->hnumis_pdf->SetTextColor(253,253,253);
		$this->hnumis_pdf->SetX(22);
		
		$header1 = array(' ','ID No.','Name of Student','Sex','Year & Course');
		
		$w=array(10,25,90,8,30);
					
		for($i=0;$i<count($w);$i++)
			$this->hnumis_pdf->Cell($w[$i],6,$header1[$i],1,0,'C',true);
		
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);
		$this->hnumis_pdf->Ln();
		
		$num = 1;
		foreach($students AS $student) {
			$this->hnumis_pdf->SetX(22);
			$this->hnumis_pdf->Cell($w[0],$l,$num,1,0,'C',true);
			$this->hnumis_pdf->Cell($w[1],$l,$student->idno,1,0,'C',true);
			$this->hnumis_pdf->Cell($w[2],$l,$student->neym,1,0,'L',true);
			$this->hnumis_pdf->Cell($w[3],$l,$student->gender,1,0,'C',true);
			$this->hnumis_pdf->Cell($w[4],$l,$student->year_level.' '.$student->abbreviation,1,0,'C',true);
				
			$this->hnumis_pdf->Ln();
			$num++;	
		}
		$this->hnumis_pdf->Output();
	}


	
	function add_booking(){
		$activity_date = $this->input->post('activity_date');
		$activity_time_s = $this->input->post('activity_time_s');
		$activity_time_e = $this->input->post('activity_time_e');
		$activity_name = $this->input->post('activity_name');
		$equipment = $this->input->post('equipment');
		$size = $this->input->post('group_size');
		$remark = $this->input->post('remark');
		$location = $this->input->post('location');
		
		$this->load->library('av_lib');
		$this->av_lib->add_booking($activity_date,$activity_time_s,$activity_time_e,$activity_name,$equipment,$size,$remark,$location);
	}
	

	
		public function student(){
				
			$this->content_lib->set_title('Faculty | Student | ' . $this->config->item('application_title'));
			$idnum = $this->uri->segment(3);
				
			if (is_numeric($idnum)){

				$this->load->model('student_common_model');
				$this->load->model('Academic_terms_model');
				$this->load->helper('student_helper');

				$this->load->model('hnumis/shs/shs_student_model');
				$this->load->model('hnumis/shs/shs_grades_model');
				$this->load->model('hnumis/shs/shs_faculty_model');
				$this->load->model('hnumis/shs/behavior_model');
				$this->load->model('hnumis/shs/attendance_model');

				
				$this->content_lib->enqueue_header_style('image_area_select');
				$this->content_lib->enqueue_footer_script('image_area_select');
				$port = substr($idnum, 0, 3);
				$this->content_lib->set_title ('Class Adviser | ' . $idnum);
				
				$result = $this->student_common_model->my_information($idnum);	//print_r($result); die();
				
				if ($result !== FALSE) {
					//a user with that id number is seen...
					$check_student = $this->shs_faculty_model->CheckIfMyStudent($this->session->userdata('empno'), $idnum);
					
					if ($check_student) {

						$dcontent = array(
								'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
								'idnum'		=> $idnum,
								'name'		=> $result->fname . " " . $result->mname. " " . $result->lname,
								'course'	=> $result->abbreviation,
								'college_id'=> $result->colleges_id,
								'college'	=> $result->college_code,
								'level'		=> "Grade ".$result->year_level." ".$result->section_name,
								'full_home_address'	=> $result->full_home_address,
								'full_city_address' => $result->full_city_address,
								'phone_number' 		=>$result->phone_number,
								'section'			=> $result->section,
						);

						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
						else {
							if ($result->gender == 'F')
								$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
								$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
						}					

						$dcontent['signature'] = base_url('assets/img/signature.png');
						
						$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);

						$data['idnum']      	= $idnum;
						$data['term']       	= $this->academic_terms_model->getCurrentAcademicTerm(); 
						$data['academic_terms'] = $this->academic_terms_model->student_inclusive_academic_terms($idnum);

						$current_history        = $this->shs_student_model->get_StudentHistory_id($idnum, $data['term']->id); 

						if (!$current_history) {
							$current_history = $this->shs_student_model->get_StudentHistory_id($idnum, $data['academic_terms'][0]->id); 							
						}

						$data['active_tab']      = "report_card";
						$data['active_tab1']     = "student_attendance";				

						$data['class_schedules'] = $this->shs_student_model->ListClassSchedules($current_history->id); 					

						$data['selected_term']   = $data['term']->id;
						$data['show_pulldown']   = TRUE;
							
						$data['student_histories_id'] = $current_history->id;
							
						$data['observed_values'] = $this->behavior_model->ListObservedValues($current_history->id);
							
						$data['can_update_values'] = FALSE; //used to check if can update baheior values
							
						$data['school_days'] = $this->attendance_model->ListSchoolDays($data['academic_terms'][0]->id);
							
						$data['present_days'] = $this->attendance_model->ListPresentDays($data['academic_terms'][0]->id, $current_history->id);

						$this->content_lib->enqueue_body_content('shs/teacher/student_management', $data);
												
					} else {
						$this->content_lib->set_message('Student does not belong to any of your classes!', 'alert-error');												
					}
						
				} else {
					//Has a Numeric ID Number but is not found in database...
					//todo: Create a view to be placed here...
					$this->content_lib->set_message('Student does not exist', 'alert-error');
				}

			}
					
			$this->content_lib->content();	
		}

		
	function list_all_bookings_of_the_day(){
		$activity_date = $this->input->post('date');
		$this->load->library('av_lib');
		$this->av_lib->list_all_bookings_of_the_day($activity_date);
	}
		
	function calendar(){
		$this->content_lib->enqueue_header_style('ggpopover');
		$this->content_lib->enqueue_footer_script('ggpopover');
		
		$browser = getBrowser(null, true);
		if (!$browser){
			$this->content_lib->set_message('Unable to determine your browser','alert-error');
			$this->content_lib->content();
			return false;
		}else{
			if ($browser['name'] !='Google Chrome' ){
				$this->content_lib->set_message('Sorry, but you must use Google Chrome to reserve.','alert-error');
				$this->content_lib->content();
				return false;					
			}else{
				if ((int)substr($browser['version'],0,2) < 31 ){ //ref: http://caniuse.com/#feat=input-datetime				
					$this->content_lib->set_message('Sorry, but you must use Google Chrome Version 31 or higher.','alert-error');
					$this->content_lib->content();
					return false;						
				}
			}
		}

		
		$this->load->library('av_lib');
		$this->av_lib->calendar();
	}
	
	
	function booking_history($empno=NULL){
		$this->load->model('av/av_model');
		$data['data'] = $this->av_model->booking_history($this->userinfo['empno']);
		if (!$data){
			$this->content_lib->set_message("Failed to list booking history!", 'alert-error');
		}else{
			//$this->content_lib->enqueue_footer_script('data_tables');
			//$this->content_lib->enqueue_footer_script('data_tables_tools');
			$this->content_lib->enqueue_body_content('av/booking_history',$data);				
		}
					
		$this->content_lib->content();
	}
	
	function cancel_my_booking(){
		$ids_to_cancel = implode(',',$this->input->post('check'));
		$this->load->library('av_lib');
		$this->av_lib->cancel_my_booking($ids_to_cancel);
	}
	
	
	function shs_class_schedules() {
		$this->load->library('shs/shs_faculty_lib');
	
		$this->shs_faculty_lib->shs_class_schedules();
		
	}


	public function process_student_action($idnum) {
			
			if (!$this->input->post('action')) {
				redirect('classadviser/student', 'refresh');
			}
			
			$this->load->library('shs/shs_student_lib');
		
			$this->shs_student_lib->process_student_action($idnum);
			
	}
	
	
}


