<?php 
	if (isset($history_id->can_enroll) AND $history_id->can_enroll == 'Y') {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td style="text-align:center; font-weight:bold;">STUDENT ALREADY HAS PRIVILEGE TO ENROLL!</td>
  </tr>
</table>
<?php
	} else {
?>
<div class="span12">
	<form method="post" class="form-horizontal" id="allow_enroll_form">
		<fieldset>
			<input type="hidden" name="action" value="privilege_enroll" />
<?php $this->common->hidden_input_nonce(FALSE); ?>
			<div class="control-group formSep">
				<div class="controls">
					<button id="allow_enroll" class="btn btn-warning" type="submit">Allow this student to Enroll</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<?php
	}
?>

<script>
$('#allow_enroll').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to allow student to enroll?");
	if (confirmed){
		$('#allow_enroll_form').submit();
	}		
});
</script>