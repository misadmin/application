<div class="row-fluid">
	<h3 class="heading"><small>Search Result for:</small> <strong><?php echo $query; ?></strong></h3>
<?php if (isset($start)): ?>	
	<div id="search_meta" class="well clearfix">
		<div class="row-fluid">
			<div class="pull-left">Showing <?php echo $start; ?>-<?php echo $end; ?> of <?php echo $total; ?></div>
		</div>
	</div>
<?php endif; ?>	
	<?php echo $pagination; ?>
	<div class="search_panel clearfix">
<?php if (isset($results) && count($results) > 0): ?>		
<?php foreach ($results as $result): ?>
	<div class="search_item clearfix">
		<div class="thumbnail pull-left"><a href="<?php echo site_url("{$role}/student/{$result['idnum']}"); ?>"><img src="<?php echo $result['image']; ?>" style="width:100px"></a></div>
		<div class="search_content">
			<div style="padding-left:10px"><h4><?php echo $result['fullname']?> <?php echo $result['mname']; ?></h4></div>
			<div style="padding-left:10px"><a href="<?php echo site_url("{$role}/student/{$result['idnum']}"); ?>"><?php echo $result['idnum']; ?></a></div>
		</div>
	</div>
<?php endforeach; ?>
<?php endif; ?>
	</div>
</div>
<?php echo $pagination; ?>
<form id="srcform2" action="<?php echo site_url("/{$role}/{$what}/")?>" method="post">
</form>
<script>
	$('.pagination a').click(function(event){
		event.preventDefault(); 
		var page = $(this).attr('href').substring(1);
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'page',
		    value: page,
		}).appendTo('#srcform2');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'q',
		    value: '<?php echo $query; ?>',
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>