<?php
	
	class Tuition_Report_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		
		function getStudentsEnrolled($academic_terms_id, $program_groups_id=NULL, $colleges_id=NULL, $yr_level, $post_status=NULL) {
			
			$result = null;
						
			if ($program_groups_id) {
				$q = "SELECT 
							aa.year_level, 
							f.abbreviation,
							f.id AS program_groups_id,
							COUNT(DISTINCT(a.students_idno)) AS numEnroll,
							SUM(g.paying_units) AS total_units
						FROM
							student_histories AS a,
							assessments AS aa,
							course_offerings AS b,
							enrollments AS c,
							prospectus AS d,
							academic_programs AS e,
							acad_program_groups AS f,
							courses AS g
						WHERE
							a.id = c.student_history_id
							AND aa.student_histories_id = a.id
							AND b.id = c.course_offerings_id
							AND a.prospectus_id = d.id
							AND d.academic_programs_id = e.id
							AND e.acad_program_groups_id = f.id
							AND g.id = b.courses_id
							AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} ";

				if ($yr_level != '0') {
					$q = $q . " AND aa.year_level = {$this->db->escape($yr_level)} "; 
				}

			} else {
				switch ($colleges_id) {
					//Do not include Grad School if all colleges selected
					case '0':
								$q = "SELECT 
											aa.year_level, 
											f.college_code AS abbreviation,
											f.id AS program_groups_id,
											COUNT(DISTINCT(a.students_idno)) AS numEnroll,
											SUM(g.paying_units) AS total_units
										FROM
											student_histories AS a,
											assessments AS aa,
											course_offerings AS b,
											enrollments AS c,
											prospectus AS d,
											academic_programs AS e,
											colleges AS f,
											courses AS g
										WHERE
											a.id = c.student_history_id 
											AND aa.student_histories_id = a.id
											AND b.id = c.course_offerings_id
											AND a.prospectus_id = d.id
											AND d.academic_programs_id = e.id
											AND g.id = b.courses_id
											AND e.colleges_id = f.id
											AND f.status = 'Active'
											AND f.id != 10
											AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
				
								if ($yr_level != '0') {
									$q .= " AND aa.year_level = {$this->db->escape($yr_level)} "; 
								}
								
								break;
					default:
								$q = "SELECT 
											aa.year_level, 
											f.college_code AS abbreviation,
											f.id AS program_groups_id,
											COUNT(DISTINCT(a.students_idno)) AS numEnroll,
											SUM(g.paying_units) AS total_units
										FROM
											student_histories AS a,
											assessments AS aa,
											course_offerings AS b,
											enrollments AS c,
											prospectus AS d,
											academic_programs AS e,
											colleges AS f,
											courses AS g
										WHERE
											a.id = c.student_history_id
											AND aa.student_histories_id = a.id
											AND b.id = c.course_offerings_id
											AND a.prospectus_id = d.id
											AND d.academic_programs_id = e.id
											AND g.id = b.courses_id
											AND e.colleges_id = f.id
											AND f.id = {$this->db->escape($colleges_id)}
											AND f.status = 'Active'
											AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
				
								if ($yr_level != '0') {
									$q .= " AND aa.year_level = {$this->db->escape($yr_level)} "; 
								}

								break;
				}
			}			
			//die($q);

			//log_message("INFO", "TUITION_BASIC");  // Toyet 1.11.2019 
			//log_message("INFO", print_r($q,true));  // Toyet 11.28.2018 

			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}	

		//
		// THIS FUNCTION HAS BEEN MARKED WITH SUFFIX _OLD.  THIS WILL BE SUPERSEDED WITH A NEWK FUNCTION OF THE
		// SAME NAME.  THIS IS TO ENSURE THAT POSTED ELEMENTS WILL NO LONGER CHANGE -Toyet 4.5.2019
		//
		function getTuitionFeeEnrolled_OLD($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {

				if ($yr_level != '0') {
					$q = "
					SELECT  
                        aa.year_level,
						CONCAT(e.abbreviation,'-', aa.year_level) AS course_yr,
						f.abbreviation,
						f.id AS program_groups_id,
						count(distinct a.students_idno) AS numEnroll,
						sum(g.paying_units) AS total_units,
						h.rate,
						e.abbreviation as program ";

				} else {
					$q = "
					select year_level,
					       course_yr,
							 abbreviation,
							 program_groups_id,
							 count(distinct numEnroll) as numEnroll,
							 sum(total_units) as total_units,
							 rate,
							 program
					from (
							SELECT  
								case
									when aa.year_level = 1 then 1
									when aa.year_level = 2 then 2
									when aa.year_level = 3 then 2
									when aa.year_level = 4 then 2
									when aa.year_level = 5 then 2
									when aa.year_level = 6 then 2
								end as year_level, 
								CONCAT(e.abbreviation,'-', aa.year_level) AS course_yr,
								f.abbreviation,
								f.id AS program_groups_id,
								a.students_idno AS numEnroll,
								g.paying_units AS total_units,
								h.rate,
								e.abbreviation as program ";
				}

				$q .= " FROM
						student_histories AS a, 
						assessments AS aa,
						course_offerings AS b,
						enrollments AS c,
						prospectus AS d,
						academic_programs AS e,
						acad_program_groups AS f,
						courses AS g,
						fees_schedule AS h
					WHERE
						a.id = c.student_history_id
						AND aa.student_histories_id = a.id
						AND b.id = c.course_offerings_id
						AND a.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND e.acad_program_groups_id = f.id
						AND g.id = b.courses_id
						AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
						AND h.fees_subgroups_id = 201
						AND (c.post_status IS NULL OR TRIM(c.post_status)='')
						AND h.acad_program_groups_id = e.acad_program_groups_id 
						AND h.academic_terms_id = a.academic_terms_id 
						AND h.yr_level = aa.year_level ";												

				if ($yr_level != '0') {
							
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} 
							AND g.id NOT IN (SELECT 
							  					x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = {$this->db->escape($yr_level)}
												AND x.posted = 'Y' ) 
							AND g.id NOT IN (SELECT 
							  					x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = h.yr_level
												AND x.posted = 'Y' ) 
										AND course_code not in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
							group by course_yr, rate, e.id";
				} else {
					$q .= " AND g.id NOT IN (SELECT 
							  					x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = h.yr_level
												AND x.posted = 'Y' ) 
										AND course_code not in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
							) temp
							group by program, rate";
				}

						
			} else {
				//print($colleges_id); die();
				switch ($colleges_id) {
					case '0':
							//do not include Grad School 						
							$q = "SELECT 
										aa.year_level, 
										CONCAT(e.abbreviation,'-',aa.year_level) AS course_yr, 
										f.abbreviation,
										f.id AS program_groups_id,
										COUNT(DISTINCT(a.students_idno)) AS numEnroll,
										SUM(g.paying_units) AS total_units,
										h.rate,
										e.abbreviation as program
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										courses AS g,
										fees_schedule AS h,
										colleges AS i
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND h.fees_subgroups_id = 201
										AND h.acad_program_groups_id = e.acad_program_groups_id 
										AND h.academic_terms_id = a.academic_terms_id 
										AND h.yr_level = aa.year_level 
										AND c.post_status IS NULL
										AND i.id = e.colleges_id
										AND f.levels_id not in (7,12)
										AND i.status = 'Active'	";
										
							if ($yr_level != '0') {										
								$q .= "	AND aa.year_level = {$this->db->escape($yr_level)} 
										AND g.id NOT IN (SELECT 
															x.courses_id
														FROM
															tuition_others AS x
														WHERE
															x.academic_terms_id = {$this->db->escape($academic_terms_id)}
															AND x.yr_level = {$this->db->escape($yr_level)}
															AND x.posted = 'Y' ) 
										GROUP BY
											e.id , h.rate";
							} else {
								$q .= " AND g.id NOT IN (SELECT 
															x.courses_id
														FROM
															tuition_others AS x
														WHERE
															x.academic_terms_id = {$this->db->escape($academic_terms_id)}
															AND x.yr_level = h.yr_level
															AND x.posted = 'Y' ) 
										AND course_code not in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
										AND c.date_enrolled <= aa.transaction_date
										GROUP BY
											e.id, h.rate ";
							}
							
							break;
							
					default:
							$q = "SELECT  
										aa.year_level, 
										CONCAT(e.abbreviation,'-',aa.year_level) AS course_yr, 	
										f.abbreviation,
										f.id AS program_groups_id,
										COUNT(DISTINCT(a.students_idno)) AS numEnroll,
										SUM(g.paying_units) AS total_units,
										h.rate,
										e.abbreviation as program
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										courses AS g,
										fees_schedule AS h,
										colleges AS i
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND h.fees_subgroups_id = 201
										AND h.acad_program_groups_id = e.acad_program_groups_id 
										AND h.academic_terms_id = a.academic_terms_id 
										AND h.yr_level = aa.year_level 
										AND i.id = {$this->db->escape($colleges_id)}
										AND i.id = e.colleges_id
										AND i.status = 'Active'	";
							
							if ($yr_level != '0') {
								
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)}
										AND g.id NOT IN (SELECT 
															x.courses_id
														FROM
															tuition_others AS x
														WHERE
															x.academic_terms_id = {$this->db->escape($academic_terms_id)}
															AND x.yr_level = {$this->db->escape($yr_level)}
															AND x.posted = 'Y' ) 
										AND course_code not in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
										GROUP BY
											e.id, h.rate ";
							} else {
								$q .= " AND g.id NOT IN (SELECT 
															x.courses_id
														FROM
															tuition_others AS x
														WHERE
															x.academic_terms_id = {$this->db->escape($academic_terms_id)}
															AND x.yr_level = h.yr_level 
															AND x.posted = 'Y' ) 
										AND course_code not in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
										GROUP BY
											e.id, h.rate ";
							}
							break;
				}
			
			
			}	
			
			//die($q);
			//log_message("INFO", "COLLEGE GROUP ID  => ".print_r($q,true)); // Toyet 5.8.2018; 8.6.2018
				
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		//
		// FULL EDIT FROM THE ORIGINAL FUNCTION OF THE SAME NAME.  THIS IS TO ENSURE THAT POSTED ELEMENTS WILL NO 
		// LONGER CHANGE. -Toyet 4.5.2019
		// 
		function getTuitionFeeEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q="SELECT 
						ass.year_level,
						CONCAT(ap.abbreviation,'-',ass.year_level) AS course_yr,
						ag.group_name,
						ap.acad_program_groups_id AS program_groups_id,
						count(distinct sh.students_idno) AS numEnroll,
						sum(ahc.paying_units) AS total_units,
						ahc.rate,
						ap.abbreviation AS program
					FROM assessments ass 
					LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
					LEFT JOIN assessments_history_courses ahc ON ahc.assessments_id=ass.id
					LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
					LEFT JOIN acad_program_groups ag ON ag.id=ap.acad_program_groups_id
					WHERE sh.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND ap.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND ahc.course_code not in ('WITNESS SCI',
														'WITNESS SOC',
														'WITNESS CUL',
														'REED 1',
														'REED 2',
														'REED 3',
														'REED 4',
														'ROTC',
														'NSTP 1',
														'NSTP 2',
														'MS 11',
														'MS 12',
														'MS 21',
														'MS 31',
														'MS 32',
														'MS 41',
														'MS 42',
														'CWTS') ";
				if ($yr_level != 0)
					$q .= "AND ass.year_level = {$this->db->escape($yr_level)} ";

				$q .= "GROUP BY program, rate";
				//log_message("INFO", "BY GROUP => ".print_r($q,true)); // Toyet 5.8.2018; 8.6.2018

			} else {
				//print($colleges_id); die();
				switch ($colleges_id) {
					case '0':
							//do not include Grad School 						
						$q="SELECT 
								ass.year_level,
								CONCAT(ap.abbreviation,'-',ass.year_level) AS course_yr,
								ag.group_name,
								ap.acad_program_groups_id AS program_groups_id,
								count(distinct sh.students_idno) AS numEnroll,
								sum(ahc.paying_units) AS total_units,
								ahc.rate,
								ap.abbreviation AS program
							FROM assessments ass 
							LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
							LEFT JOIN assessments_history_courses ahc ON ahc.assessments_id=ass.id
							LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
							LEFT JOIN acad_program_groups ag ON ag.id=ap.acad_program_groups_id
							WHERE sh.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ap.colleges_id != 10
								AND ahc.course_code not in ('WITNESS SCI',
																'WITNESS SOC',
																'WITNESS CUL',
																'REED 1',
																'REED 2',
																'REED 3',
																'REED 4',
																'ROTC',
																'NSTP 1',
																'NSTP 2',
																'MS 11',
																'MS 12',
																'MS 21',
																'MS 31',
																'MS 32',
																'MS 41',
																'MS 42',
																'CWTS') ";
						if ($yr_level != 0)
							$q .= "AND ass.year_level = {$this->db->escape($yr_level)} ";

						$q .= "GROUP BY program, rate";
							
							break;
							
					default:
						$q="SELECT 
								ass.year_level,
								CONCAT(ap.abbreviation,'-',ass.year_level) AS course_yr,
								ag.group_name,
								ap.acad_program_groups_id AS program_groups_id,
								count(distinct sh.students_idno) AS numEnroll,
								sum(ahc.paying_units) AS total_units,
								ahc.rate,
								ap.abbreviation AS program
							FROM assessments ass 
							LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
							LEFT JOIN assessments_history_courses ahc ON ahc.assessments_id=ass.id
							LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
							LEFT JOIN acad_program_groups ag ON ag.id=ap.acad_program_groups_id
							WHERE sh.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ap.colleges_id = {$this->db->escape($colleges_id)}
								AND ahc.course_code not in ('WITNESS SCI',
																'WITNESS SOC',
																'WITNESS CUL',
																'REED 1',
																'REED 2',
																'REED 3',
																'REED 4',
																'ROTC',
																'NSTP 1',
																'NSTP 2',
																'MS 11',
																'MS 12',
																'MS 21',
																'MS 31',
																'MS 32',
																'MS 41',
																'MS 42',
																'CWTS') ";
						if ($yr_level != 0)
							$q .= "AND ass.year_level = {$this->db->escape($yr_level)} ";

						$q .= "GROUP BY program, rate";

						break;
				}
			
			
			}	
			
			//die($q);
			//log_message("info", print_r($q,true));   // Toyet 4.23.2019
				
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}



		// added Toyet 9.27.2018
		//   - for checking purposes on Assessment Summary by College 
		function ListStudentsByProgramGroupsID($academic_terms_id=null, $program_groups_id=null, 
											  $year_level=1,$program=null,$rate=0) {

			$result = null;

			$sql = "SELECT
						aa.year_level, 
						CONCAT(e.abbreviation,'-',aa.year_level) AS course_yr, 
						f.abbreviation,
						f.id AS program_groups_id,
						a.students_idno AS numEnroll,
						concat(st.lname,', ', st.fname,' ',st.mname) as name,
						sum(g.paying_units) AS total_units,
						h.rate,
						e.abbreviation as program						
					FROM
						student_histories AS a,
						assessments AS aa,
						course_offerings AS b,
						enrollments AS c,
						prospectus AS d,
						academic_programs AS e,
						acad_program_groups AS f,
						courses AS g,
						fees_schedule AS h,
						colleges AS i,
						students as st
					WHERE
						a.id = c.student_history_id
						AND aa.student_histories_id = a.id
						AND b.id = c.course_offerings_id
						AND a.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND e.acad_program_groups_id = f.id
						AND g.id = b.courses_id
						AND a.academic_terms_id = '{$academic_terms_id}' 
						AND h.fees_subgroups_id = 201
						AND h.acad_program_groups_id = e.acad_program_groups_id 
						AND h.academic_terms_id = a.academic_terms_id 
						AND h.yr_level = aa.year_level 
						AND i.id = e.colleges_id
						AND f.levels_id not in (7,12)
						AND st.idno=a.students_idno
						AND i.status = 'Active'	 AND g.id NOT IN (SELECT 
											x.courses_id
										FROM
											tuition_others AS x
										WHERE
											x.academic_terms_id = '{$academic_terms_id}'
											AND x.yr_level = h.yr_level
											AND x.posted = 'Y' ) 
						AND course_code not in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
						and f.id = '{$program_groups_id}' ";

				if($year_level<>0){
					$sql .= "and aa.year_level = {$year_level} ";
				} else {
					//$sql .= "and aa.year_level > 1 ";
					$sql .= "and aa.year_level > 0 ";
				}

				$sql .= "and e.abbreviation='{$program}'
				         and h.rate={$rate} 
				         GROUP BY concat(st.lname,', ', st.fname,' ',st.mname),program";

			//log_message("INFO", "ListStudentsByProgramGroupsID   ==> ".print_r($sql,true)); // Toyet 9.29.2018

			$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}		

		// added Toyet 1.17.2019
		//   - for checking purposes on Assessment Summary by College 
		function ListStudentsByProgramID($academic_terms_id=null, $colleges_id=null, 
										 $year_level=1,$program=null,$rate=0) {

			$result = null;

			$sql = "SELECT
						aa.year_level, 
						CONCAT(e.abbreviation,'-',aa.year_level) AS course_yr, 
						f.abbreviation,
						f.id AS program_groups_id,
						a.students_idno AS numEnroll,
						concat(st.lname,', ', st.fname,' ',st.mname) as name,
						sum(g.paying_units) AS total_units,
						h.rate,
						e.abbreviation as program						
					FROM
						student_histories AS a,
						assessments AS aa,
						course_offerings AS b,
						enrollments AS c,
						prospectus AS d,
						academic_programs AS e,
						acad_program_groups AS f,
						courses AS g,
						fees_schedule AS h,
						colleges AS i,
						students as st
					WHERE
						a.id = c.student_history_id
						AND aa.student_histories_id = a.id
						AND b.id = c.course_offerings_id
						AND a.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND e.acad_program_groups_id = f.id
						AND g.id = b.courses_id
						AND a.academic_terms_id = '{$academic_terms_id}' 
						AND h.fees_subgroups_id = 201
						AND h.acad_program_groups_id = e.acad_program_groups_id 
						AND h.academic_terms_id = a.academic_terms_id 
						AND h.yr_level = aa.year_level 
						AND i.id = e.colleges_id
						AND f.levels_id not in (7,12)
						AND st.idno=a.students_idno
						AND i.status = 'Active'	 AND g.id NOT IN (SELECT 
											x.courses_id
										FROM
											tuition_others AS x
										WHERE
											x.academic_terms_id = '{$academic_terms_id}'
											AND x.yr_level = h.yr_level
											AND x.posted = 'Y' ) 
						AND course_code not in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
						and e.abbreviation = '{$program}' ";

				if($year_level<>0){
					$sql .= "and aa.year_level = {$year_level} ";
				} //else {
					//$sql .= "and aa.year_level > 1 ";
					//$sql .= "and aa.year_level > 0 ";
				//}

				$sql .= "and h.rate={$rate} 
				         GROUP BY e.abbreviation, concat(st.lname,' ',st.fname,' ',st.mname), program";

			//log_message("INFO", "ListStudentsByProgramCollegeID   ==> ".print_r($sql,true)); // Toyet 1.17.2018

			$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}		



		// added Toyet 1.11.2019
		//   - for checking purposes on Assessment Summary by College 
		//   - lists all students taking up the REED Group courses
		function ListStudentsByProgramGroupsID2($academic_terms_id=null, $program_groups_id=null, 
												$year_level,$course_code,$rate,$program=null) {

			$result = null;

			$sql = "SELECT ass.year_level,
						   CONCAT(ap.abbreviation,'-',ass.year_level) AS course_yr, 
						   ap.abbreviation,
						   ass.id AS program_groups_id,
						   sh.students_idno AS numEnroll,
						   concat(st.lname,', ', st.fname,' ',st.mname) as name,
						   sum(ahc.paying_units) AS total_units,
						   ahc.rate
					FROM assessments ass
					LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
					LEFT JOIN assessments_history_courses ahc ON ahc.assessments_id=ass.id
					LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
					LEFT JOIN students st ON st.idno=sh.students_idno
					WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
					   AND ap.acad_program_groups_id={$this->db->escape($program_groups_id)}
					   AND ahc.course_code ={$this->db->escape($course_code)} ";

			if($year_level != 0){
				$sql .= "AND ass.year_level={$this->db->escape($year_level)} ";
			}

			$sql .= "GROUP BY name ";

			//log_message("INFO", "ListStudentsByProgramGroupsID2  ==> ".print_r($sql,true)); // Toyet 9.29.2018

			$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		// added Toyet 1.17.2019
		//   - for checking purposes on Assessment Summary by College 
		//   - lists all students taking up the REED Group courses
		function ListStudentsByProgramID2($academic_terms_id=null, $program_groups_id=null, 
												$year_level=1,$course_code,$rate,$program=null) {
			$result = null;

			$sql = "SELECT
						aa.year_level, 
						CONCAT(e.abbreviation,'-',aa.year_level) AS course_yr, 
						f.abbreviation,
						f.id AS program_groups_id,
						a.students_idno AS numEnroll,
						concat(st.lname,', ', st.fname,' ',st.mname) as name,
						sum(g.paying_units) AS total_units,
						h.rate,
						e.abbreviation as program						
					FROM
						student_histories AS a,
						assessments AS aa,
						course_offerings AS b,
						enrollments AS c,
						prospectus AS d,
						academic_programs AS e,
						acad_program_groups AS f,
						courses AS g,
						fees_schedule AS h,
						colleges AS i,
						students as st
					WHERE
						a.id = c.student_history_id
						AND aa.student_histories_id = a.id
						AND b.id = c.course_offerings_id
						AND a.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND e.acad_program_groups_id = f.id
						AND g.id = b.courses_id
						AND a.academic_terms_id = '{$academic_terms_id}' 
						AND h.fees_subgroups_id = 201
						AND h.acad_program_groups_id = e.acad_program_groups_id 
						AND h.academic_terms_id = a.academic_terms_id 
						AND h.yr_level = aa.year_level 
						AND i.id = e.colleges_id
						AND f.levels_id not in (7,12)
						AND st.idno=a.students_idno
						AND i.status = 'Active'	 AND g.id NOT IN (SELECT 
											x.courses_id
										FROM
											tuition_others AS x
										WHERE
											x.academic_terms_id = '{$academic_terms_id}'
											AND x.yr_level = h.yr_level
											AND x.posted = 'Y' )
						and c.status != 'Withdrawn' 
						AND g.course_code = '{$course_code}'
						AND e.abbreviation = '{$program}' ";

				//	$sql .= "and aa.year_level = {$year_level} ";
				//} else {
					//$sql .= "and aa.year_level > 1 ";
				//	$sql .= "and aa.year_level > 0 ";
				//}

				$sql .= "and h.rate={$rate} 
				         and aa.year_level = {$year_level} 
				         GROUP BY e.abbreviation, concat(st.lname,' ',st.fname,' ',st.mname), program";

			//log_message("INFO", "EXTRACTED: ListStudentsByProgramID   ==> ".print_r($sql,true)); // Toyet 1.17.2018

			$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}	


		function ListTuitionOthersEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {			
				$q = "SELECT 
							aa.year_level,
							f.abbreviation,
							g.course_code,
							COUNT(DISTINCT(a.students_idno)) AS enrollees,
							(COUNT(DISTINCT(a.students_idno)) * g.paying_units) AS paying_units,
							i.rate
						FROM
							student_histories AS a,
							assessments AS aa,
							course_offerings AS b,
							enrollments AS c,
							prospectus AS d,
							academic_programs AS e,
							acad_program_groups AS f,
							courses AS g,
							fees_schedule AS h,
							tuition_others AS i
						WHERE
							a.id = c.student_history_id
							AND aa.student_histories_id = a.id
							AND b.id = c.course_offerings_id
							AND a.prospectus_id = d.id
							AND d.academic_programs_id = e.id
							AND e.acad_program_groups_id = f.id
							AND g.id = b.courses_id
							AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND aa.year_level = h.yr_level 
							AND aa.year_level = i.yr_level 
							AND f.id = h.acad_program_groups_id 
							AND h.fees_subgroups_id = 201
							AND c.post_status IS NULL
							AND h.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND i.courses_id = g.id
							AND i.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND i.posted = 'Y' ";
					
				if ($yr_level != '0') {
						$q .= " AND aa.year_level = {$this->db->escape($yr_level)} 
								GROUP BY
									g.id, i.rate 
								ORDER BY
									g.course_code ";
				} else {
					$q .= " GROUP BY
								g.id, i.rate 
							ORDER BY
								g.course_code ";
				}
			} else {
				switch ($colleges_id) {
					case '0':
							//do not include Grad School
							$q = "SELECT 
										aa.year_level, 
										f.abbreviation,
										g.course_code,
										COUNT(DISTINCT(a.students_idno)) AS enrollees,
										(COUNT(DISTINCT(a.students_idno)) * g.paying_units) AS paying_units,
										i.rate
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										courses AS g,
										fees_schedule AS h,
										tuition_others AS i,
										colleges AS j
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND e.colleges_id = j.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND aa.year_level = i.yr_level 
										AND f.id = h.acad_program_groups_id 
										AND h.fees_subgroups_id = 201
										AND h.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND i.courses_id = g.id
										AND i.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND i.posted = 'Y' 
										AND f.levels_id != 7
										AND j.status = 'Active'  ";
								
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} 
										GROUP BY
											g.id, i.rate 
										ORDER BY
											g.course_code ";
							} else {
								$q .= " GROUP BY
											g.id, i.rate 
										ORDER BY
											g.course_code ";
							}
			
							break;
					default:
							
							$q = "SELECT 
										aa.year_level, 
										f.abbreviation,
										g.course_code,
										COUNT(DISTINCT(a.students_idno)) AS enrollees,
										(COUNT(DISTINCT(a.students_idno)) * g.paying_units) AS paying_units,
										i.rate
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										courses AS g,
										fees_schedule AS h,
										tuition_others AS i,
										colleges AS j
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND e.colleges_id = j.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND aa.year_level = i.yr_level 
										AND f.id = h.acad_program_groups_id 
										AND h.fees_subgroups_id = 201
										AND h.academic_terms_id = a.academic_terms_id
										AND i.courses_id = g.id
										AND i.academic_terms_id = a.academic_terms_id
										AND i.posted = 'Y' 
										AND j.id = {$this->db->escape($colleges_id)}
										AND j.status = 'Active' ";
								
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} 
										GROUP BY
											g.id, i.rate 
										ORDER BY
											g.course_code ";
							} else {
								$q .= " GROUP BY
											g.id, i.rate 
										ORDER BY
											g.course_code ";
							}
							break;
				}			
			
			}			 
			
			//die($q);
			////log_message("INFO","OTHER TUITION FEES ==>".print_r($q,true)); // Toyet 9.17.2018

			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		
		}


		//
		// FULL EDIT FROM THE ORIGINAL FUNCTION OF THE SAME NAME.  THIS IS TO ENSURE THAT POSTED ELEMENTS WILL NO 
		// LONGER CHANGE. -Toyet 4.6.2019
		// 
		function extractOtherFromTuitionFeeEnrolled_OLD($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {

				if ($yr_level != '0') {
					$q = "
					SELECT  
                        aa.year_level,
						CONCAT(e.abbreviation,'-', aa.year_level) AS course_yr,
						f.abbreviation,
						f.id AS program_groups_id,
						count(distinct a.students_idno) AS numEnroll,
						sum(g.paying_units) AS total_units,
						h.rate,
						e.abbreviation as program,
						g.course_code ";

				} else {
					$q = "
					select year_level,
					       course_code,
					       count(distinct numEnroll) as numEnroll,
					       sum(total_units) as total_units,
					       rate,
					       program,
					       course_yr
					from (
							SELECT  
								case
									when aa.year_level = 1 then 1
									when aa.year_level = 2 then 2
									when aa.year_level = 3 then 3
									when aa.year_level = 4 then 4
									when aa.year_level = 5 then 5
									when aa.year_level = 6 then 6
								end as year_level, 
								CONCAT(e.abbreviation,'-', aa.year_level) AS course_yr,
								f.abbreviation,
								f.id AS program_groups_id,
								a.students_idno AS numEnroll,
								g.paying_units AS total_units,
								h.rate,
								e.abbreviation as program,
								g.course_code ";
				}

				$q .= " FROM
						student_histories AS a, 
						assessments AS aa,
						course_offerings AS b,
						enrollments AS c,
						prospectus AS d,
						academic_programs AS e,
						acad_program_groups AS f,
						courses AS g,
						fees_schedule AS h
					WHERE
						a.id = c.student_history_id
						AND aa.student_histories_id = a.id
						AND b.id = c.course_offerings_id
						AND a.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND e.acad_program_groups_id = f.id
						AND g.id = b.courses_id
						AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
						AND h.fees_subgroups_id = 201
						AND (c.post_status IS NULL OR TRIM(c.post_status)='')
						AND h.acad_program_groups_id = e.acad_program_groups_id 
						AND h.academic_terms_id = a.academic_terms_id 
						AND h.yr_level = aa.year_level ";												

				if ($yr_level != '0') {
							
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} 
							AND g.id NOT IN (SELECT 
							  					x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = {$this->db->escape($yr_level)}
												AND x.posted = 'Y' ) 
							AND course_code in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
							group by course_code, e.id";
				} else {
					$q .= " AND g.id NOT IN (SELECT 
							  					x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = h.yr_level
												AND x.posted = 'Y' ) 
							AND course_code in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
							AND c.date_enrolled <= aa.transaction_date
							) temp
							group by year_level,course_code";
				}

				log_message("INFO", "EXTRACTED OTHER TUITION => ".print_r($q,true)); // Toyet 5.8.2018; 8.6.2018; 1.20.2019
						
			} else {
				//print($colleges_id); die();
				switch ($colleges_id) {
					case '0':
							//do not include Grad School 						
							$q = "SELECT 
										aa.year_level, 
										CONCAT(e.abbreviation,'-',aa.year_level) AS course_yr, 
										f.abbreviation,
										f.id AS program_groups_id,
										COUNT(DISTINCT(a.students_idno)) AS numEnroll,
										SUM(g.paying_units) AS total_units,
										h.rate,
										e.abbreviation as program,
										g.course_code
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										courses AS g,
										fees_schedule AS h,
										colleges AS i
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND h.fees_subgroups_id = 201
										AND h.acad_program_groups_id = e.acad_program_groups_id 
										AND h.academic_terms_id = a.academic_terms_id 
										AND h.yr_level = aa.year_level 
										AND c.post_status IS NULL
										AND i.id = e.colleges_id
										AND f.levels_id != 7
										AND i.status = 'Active'	";
										
							if ($yr_level != '0') {										
								$q .= "	AND aa.year_level = {$this->db->escape($yr_level)} 
										AND g.id NOT IN (SELECT 
															x.courses_id
														FROM
															tuition_others AS x
														WHERE
															x.academic_terms_id = {$this->db->escape($academic_terms_id)}
															AND x.yr_level = {$this->db->escape($yr_level)}
															AND x.posted = 'Y' ) 
							AND course_code in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
										GROUP BY
											e.id ";
							} else {
								$q .= " AND g.id NOT IN (SELECT 
															x.courses_id
														FROM
															tuition_others AS x
														WHERE
															x.academic_terms_id = {$this->db->escape($academic_terms_id)}
															AND x.yr_level = h.yr_level
															AND x.posted = 'Y' ) 
							AND course_code in ('WITNESS SCI',
                                                'WITNESS SOC',
                                                'WITNESS CUL',
                                                'REED 1',
                                                'REED 2',
                                                'REED 3',
                                                'REED 4',
						    		            'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS')
										AND c.date_enrolled <= aa.transaction_date
										GROUP BY
											g.course_code, h.rate ";
							}
							
							break;
							
					default:
							$q =   "select ass.year_level,
										concat(ap.abbreviation,' ',ass.year_level) as course_yr,
										ag.abbreviation,
										ag.id,
										count(st.idno) as numEnroll,
										sum(crs.paying_units) as total_units,
										fs.rate,
										ap.abbreviation as program,
										crs.course_code
									from enrollments en,
										academic_programs ap,
										acad_program_groups ag,
										student_histories sh,
										course_offerings co, 
										courses crs, 
										prospectus prs, 
										students st, 
										assessments ass, 
										colleges clgs, 
										fees_schedule fs 
									where sh.id=en.student_history_id
									      and clgs.id=ap.colleges_id
									      and ass.student_histories_id=sh.id
									      and st.idno=sh.students_idno
									      and prs.id=sh.prospectus_id
									      and crs.id=co.courses_id
									      and co.id=en.course_offerings_id
									      and fs.acad_program_groups_id=ap.acad_program_groups_id 
									      and fs.academic_terms_id=sh.academic_terms_id
									      and co.academic_terms_id={$this->db->escape($academic_terms_id)}
									      and fs.fees_subgroups_id=201
									      and ap.id=prs.academic_programs_id and ap.colleges_id=clgs.id
									      and ag.id=ap.acad_program_groups_id
									      and clgs.id={$this->db->escape($colleges_id)} ";
							if ($yr_level != '0') {
								
								$q .="and ass.year_level={$this->db->escape($yr_level)}
								      and fs.yr_level={$this->db->escape($yr_level)}
								      and crs.id not in (select x.courses_id
														     from tuition_others as x
															  where x.academic_terms_id = {$this->db->escape($academic_terms_id)}
																	  and x.yr_level = {$this->db->escape($yr_level)}
																	  and x.posted = 'Y' )
								      and course_code in ('WITNESS SCI',
								                          'WITNESS SOC',
								                          'WITNESS CUL',
								                          'REED 1',
								                          'REED 2',
								                          'REED 3',
								                          'REED 4',
														  'ROTC',
														  'NSTP 1',
														  'NSTP 2',
														  'MS 11',
														  'MS 12',
														  'MS 21',
														  'MS 31',
														  'MS 32',
														  'MS 41',
														  'MS 42',
														  'CWTS')
									  group by crs.id, fs.rate, ap.abbreviation";
							} else {
								$q .= "and fs.yr_level=ass.year_level
								       and crs.id not in (select x.courses_id
														     from tuition_others as x
															  where x.academic_terms_id = {$this->db->escape($academic_terms_id)}
																	  and x.yr_level = {$this->db->escape($yr_level)}
																	  and x.posted = 'Y' )
								      and course_code in ('WITNESS SCI',
								                          'WITNESS SOC',
								                          'WITNESS CUL',
								                          'REED 1',
								                          'REED 2',
								                          'REED 3',
								                          'REED 4',
														  'ROTC',
														  'NSTP 1',
														  'NSTP 2',
														  'MS 11',
														  'MS 12',
														  'MS 21',
														  'MS 31',
														  'MS 32',
														  'MS 41',
														  'MS 42',
														  'CWTS')
										GROUP BY course_code, course_yr ";
							}
							break;
				}	
			}	
			
			//die($q);
				
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}



		//
		// FULL EDIT FROM THE ORIGINAL FUNCTION OF THE SAME NAME.  THIS IS TO ENSURE THAT POSTED ELEMENTS WILL NO 
		// LONGER CHANGE. -Toyet 4.6.2019
		// 
		function extractOtherFromTuitionFeeEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q="SELECT 
						ass.year_level,
						CONCAT(ap.abbreviation,'-',ass.year_level) AS course_yr,
						ag.group_name,
						ap.acad_program_groups_id AS program_groups_id,
						count(distinct sh.students_idno) AS numEnroll,
						sum(ahc.paying_units) AS total_units,
						ahc.rate,
						ap.abbreviation AS program,
						ahc.course_code
					FROM assessments ass 
					LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
					LEFT JOIN assessments_history_courses ahc ON ahc.assessments_id=ass.id
					LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
					LEFT JOIN acad_program_groups ag ON ag.id=ap.acad_program_groups_id
					WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)} 
					    AND ap.acad_program_groups_id={$this->db->escape($program_groups_id)}
						AND ahc.course_code in ('WITNESS SCI',
												'WITNESS SOC',
												'WITNESS CUL',
												'REED 1',
												'REED 2',
												'REED 3',
												'REED 4',
												'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS') ";
				if($yr_level!=0)
					$q .= "AND ass.year_level = {$this->db->escape($yr_level)} ";

				$q .= "GROUP BY course_code, rate ";

				log_message("info", "REED GROUP   ==>".print_r($q,true)); 

			} else {
				//print($colleges_id); die();
				switch ($colleges_id) {
					case '0':
							//do not include Grad School 						
						$q="SELECT 
								ass.year_level,
								CONCAT(ap.abbreviation,'-',ass.year_level) AS course_yr,
								ag.group_name,
								ap.acad_program_groups_id AS program_groups_id,
								count(distinct sh.students_idno) AS numEnroll,
								sum(ahc.paying_units) AS total_units,
								ahc.rate,
								ap.abbreviation AS program,
								ahc.course_code
							FROM assessments ass 
							LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
							LEFT JOIN assessments_history_courses ahc ON ahc.assessments_id=ass.id
							LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
							LEFT JOIN acad_program_groups ag ON ag.id=ap.acad_program_groups_id
							WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)} 
							    AND ap.colleges_id != 10
								AND ahc.course_code in ('WITNESS SCI',
														'WITNESS SOC',
														'WITNESS CUL',
														'REED 1',
														'REED 2',
														'REED 3',
														'REED 4',
														'ROTC',
														'NSTP 1',
														'NSTP 2',
														'MS 11',
														'MS 12',
														'MS 21',
														'MS 31',
														'MS 32',
														'MS 41',
														'MS 42',
														'CWTS') ";
						if($yr_level!=0)
							$q .= "AND ass.year_level = {$this->db->escape($yr_level)} ";

						$q .= "GROUP BY course_code, rate ";

						log_message("info", "REED GROUP   ==>".print_r($q,true)); 
							break;
							
					default:
						$q="SELECT 
								ass.year_level,
								CONCAT(ap.abbreviation,'-',ass.year_level) AS course_yr,
								ag.group_name,
								ap.acad_program_groups_id AS program_groups_id,
								count(distinct sh.students_idno) AS numEnroll,
								sum(ahc.paying_units) AS total_units,
								ahc.rate,
								ap.abbreviation AS program,
								ahc.course_code
							FROM assessments ass 
							LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
							LEFT JOIN assessments_history_courses ahc ON ahc.assessments_id=ass.id
							LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
							LEFT JOIN acad_program_groups ag ON ag.id=ap.acad_program_groups_id
							WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)} 
							    AND ap.colleges_id={$this->db->escape($colleges_id)}
								AND ahc.course_code in ('WITNESS SCI',
														'WITNESS SOC',
														'WITNESS CUL',
														'REED 1',
														'REED 2',
														'REED 3',
														'REED 4',
														'ROTC',
														'NSTP 1',
														'NSTP 2',
														'MS 11',
														'MS 12',
														'MS 21',
														'MS 31',
														'MS 32',
														'MS 41',
														'MS 42',
														'CWTS') ";
						if($yr_level!=0)
							$q .= "AND ass.year_level = {$this->db->escape($yr_level)} ";

						$q .= "GROUP BY course_code, rate ";

						log_message("info", "REED GROUP   ==>".print_r($q,true)); 

						break;
				}	
			}	
			
			//die($q);
				
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function getMatriculationEnrolled_OLD($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q = "SELECT 
							COUNT(DISTINCT(a.students_idno)) AS numEnroll,
							h.rate
						FROM
							student_histories AS a,
							assessments AS aa,
							course_offerings AS b,
							enrollments AS c,
							prospectus AS d,
							academic_programs AS e,
							acad_program_groups AS f,
							fees_schedule AS h
						WHERE
							a.id = c.student_history_id
							AND aa.student_histories_id = a.id
							AND b.id = c.course_offerings_id
							AND a.prospectus_id = d.id
							AND d.academic_programs_id = e.id
							AND e.acad_program_groups_id = f.id
							AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND aa.year_level = h.yr_level 
							AND h.fees_subgroups_id = 14
							AND h.acad_program_groups_id = e.acad_program_groups_id
							AND h.academic_terms_id = a.academic_terms_id
							AND h.posted = 'Y'  "; 
				if ($yr_level != '0') {
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
				}
			} else {
				switch ($colleges_id) {
					case '0':
							//do not include Grad School
							$q = "SELECT 
										COUNT(DISTINCT(a.students_idno)) AS numEnroll,
										h.rate
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										fees_schedule AS h,
										colleges AS i
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND e.colleges_id = i.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND h.fees_subgroups_id = 14
										AND h.acad_program_groups_id = e.acad_program_groups_id
										AND h.academic_terms_id = a.academic_terms_id
										AND h.posted = 'Y' 
										AND f.levels_id not in (7,12)
										AND i.status = 'Active' "; 

							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}
			
							break;
							
					default:
							$q = "SELECT 
										COUNT(DISTINCT(a.students_idno)) AS numEnroll,
										h.rate
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										fees_schedule AS h,
										colleges AS i
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND e.colleges_id = i.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND h.fees_subgroups_id = 14
										AND h.acad_program_groups_id = e.acad_program_groups_id
										AND h.academic_terms_id = a.academic_terms_id
										AND h.posted = 'Y' 
										AND i.id = {$this->db->escape($colleges_id)}
										AND f.levels_id not in (7,12)
										AND i.status = 'Active' "; 

							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}

							break;
				}			
			}
			
			$q .= " GROUP BY h.rate ";
					
			//die($q);
			//log_message("INFO","MATRICULATION   == >".print_r($q,true));  // Toyet 9.28.2017
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function getMatriculationEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q =   "SELECT COUNT(distinct sh.students_idno) AS numEnroll,
								       ahf.rate									       
								FROM assessments_history_fees ahf
								LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.acad_program_groups_id={$this->db->escape($program_groups_id)}
								   AND ahf.fees_groups_id=1 ";  // Matriculation

						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
						}

						$q .= "GROUP BY ahf.description "; 

			} else {
				switch ($colleges_id) {
					case '0':
							//do not include Grad School
							$q =   "SELECT COUNT(distinct sh.students_idno) AS numEnroll,
											       ahf.rate									       
											FROM assessments_history_fees ahf
											LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
											LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
											LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
											WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
											   AND ap.colleges_id != 10
											   AND ahf.fees_groups_id=1 ";  // Matriculation

							if ($yr_level != '0') {
								$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
							}

							$q .= "GROUP BY ahf.description "; 
			
							break;
							
					default:
						$q =   "SELECT COUNT(distinct sh.students_idno) AS numEnroll,
								       ahf.rate									       
								FROM assessments_history_fees ahf
								LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id={$this->db->escape($colleges_id)}
								   AND ahf.fees_groups_id=1 ";  // Matriculation

						if ($yr_level != '0') {
							$q .= " AND sh.year_level = {$this->db->escape($yr_level)} ";
						}

						$q .= " GROUP BY ahf.description ";

						break;
				}			
			}
			

					
			//die($q);
			log_message("INFO","MATRICULATION   == >".print_r($q,true));  // Toyet 9.28.2017
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function ListMiscEnrolled_OLD($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q = "SELECT
							i.description, 
							h.rate,
							COUNT(DISTINCT(a.students_idno)) AS enrollees
						FROM
							student_histories AS a,
							assessments AS aa,
							course_offerings AS b,
							enrollments AS c,
							prospectus AS d,
							academic_programs AS e,
							acad_program_groups AS f,
							fees_schedule AS h,
							fees_subgroups AS i						
						WHERE
							a.id = c.student_history_id
							AND aa.student_histories_id = a.id
							AND b.id = c.course_offerings_id
							AND a.prospectus_id = d.id
							AND d.academic_programs_id = e.id
							AND e.acad_program_groups_id = f.id
							AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND aa.year_level = h.yr_level 
							AND i.id = h.fees_subgroups_id
							AND i.fees_groups_id = 2
							AND h.acad_program_groups_id = e.acad_program_groups_id
							AND h.academic_terms_id = a.academic_terms_id
							AND h.posted = 'Y' "; 
							
				if ($yr_level != '0') {
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
				}

			} else {
				switch ($colleges_id) {
					case '0':
							//Do not include Grad School if all colleges selected
							$q = "SELECT
										i.description, 
										h.rate,
										COUNT(DISTINCT(a.students_idno)) AS enrollees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups as ag,
										fees_schedule AS h,
										fees_subgroups AS i,
										colleges AS j						
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND i.id = h.fees_subgroups_id
										AND i.fees_groups_id = 2
										AND e.acad_program_groups_id = h.acad_program_groups_id 
										AND e.colleges_id =j.id
										AND j.status = 'Active'
										AND h.academic_terms_id = a.academic_terms_id
										AND j.id NOT IN (0,8,10)
										AND h.posted = 'Y' 
										AND ag.id=e.acad_program_groups_id
										AND ag.levels_id not in (7,12) "; 

							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							} 

							break;
					default:
							$q = "SELECT
										i.description, 
										h.rate,
										COUNT(DISTINCT(a.students_idno)) AS enrollees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups as ag,
										fees_schedule AS h,
										fees_subgroups AS i,
										colleges AS j						
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND i.id = h.fees_subgroups_id
										AND i.fees_groups_id = 2
										AND e.acad_program_groups_id = h.acad_program_groups_id 
										AND e.colleges_id =j.id
										AND j.id = {$this->db->escape($colleges_id)}
										AND j.status = 'Active'
										AND h.academic_terms_id = a.academic_terms_id
										AND h.posted = 'Y' 			
										AND ag.id=e.acad_program_groups_id
										AND ag.levels_id not in (7,12) "; 

							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}

							break;
				}
			}
			
			$q .= " GROUP BY
						h.rate,i.id
					ORDER BY
						i.description";
			
			//die($q);
			//log_message("INFO","ListMiscEnrolled".print_r($q,true));  // Toyet 9.27.2018
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		// Heavy editing -Toyet 4.23.2019
		function ListMiscEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q =   "SELECT ahf.description,
						       ahf.rate,
								 COUNT(distinct sh.students_idno) AS enrollees							       
						FROM assessments_history_fees ahf
						LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
						LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
						LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
						WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
						   AND ap.acad_program_groups_id={$this->db->escape($program_groups_id)}
						   AND ahf.fees_groups_id=2 ";   // Miscellaneous
							
				if ($yr_level != '0') {
					$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
				}

				$q .= "GROUP BY ahf.description ";

			} else {
				switch ($colleges_id) {
					case '0':
						//Do not include Grad School if all colleges selected
						$q =   "SELECT ahf.description,
								       ahf.rate,
										 COUNT(distinct sh.students_idno) AS enrollees							       
								FROM assessments_history_fees ahf
								LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id != 10
								   AND ahf.fees_groups_id=2 ";   // Miscellaneous
									
						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
						}

						$q .= "GROUP BY ahf.description ";

							break;
					default:
						$q =   "SELECT ahf.description,
								       ahf.rate,
										 COUNT(distinct sh.students_idno) AS enrollees							       
								FROM assessments_history_fees ahf
								LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id={$this->db->escape($colleges_id)}
								   AND ahf.fees_groups_id=2 ";   // Miscellaneous
									
						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
						}

						$q .= "GROUP BY ahf.description ";

						break;
				}
			}			

			
			//die($q);
			//log_message("INFO","MISCELLANEOUS   ==>".print_r($q,true));  // Toyet 9.27.2018; 4.23.2019
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function ListOthersEnrolled_OLD($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status) {
			$result = null;
			
			if ($program_groups_id) {
				$q = "SELECT
						i.description, 
						h.rate,
						COUNT(DISTINCT(a.students_idno)) AS enrollees	
					FROM
						student_histories AS a,
						assessments AS aa,
						course_offerings AS b,
						enrollments AS c,
						prospectus AS d,
						academic_programs AS e,
						acad_program_groups AS f,
						fees_schedule AS h,
						fees_subgroups AS i						
					WHERE
						a.id = c.student_history_id
						AND aa.student_histories_id = a.id
						AND b.id = c.course_offerings_id
						AND a.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND e.acad_program_groups_id = f.id
						AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
						AND aa.year_level = h.yr_level 
						AND i.id = h.fees_subgroups_id
						AND i.fees_groups_id = 3
						AND h.acad_program_groups_id = e.acad_program_groups_id
						AND h.academic_terms_id = a.academic_terms_id
						AND h.posted = 'Y' "; 
				
				if ($yr_level != '0') {
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
				}

			} else {
				switch ($colleges_id) {
					case '0':
							//Do not include Grad School if all colleges selected
							$q = "SELECT
									i.description, 
									h.rate,
									COUNT(DISTINCT(a.students_idno)) AS enrollees
								FROM
									student_histories AS a,
									assessments AS aa,
									course_offerings AS b,
									enrollments AS c,
									prospectus AS d,
									academic_programs AS e,
									acad_program_groups as ag,
									fees_schedule AS h,
									fees_subgroups AS i,						
									colleges AS j						
								WHERE
									a.id = c.student_history_id
									AND aa.student_histories_id = a.id
									AND b.id = c.course_offerings_id
									AND a.prospectus_id = d.id
									AND d.academic_programs_id = e.id
									AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
									AND aa.year_level = h.yr_level 
									AND i.id = h.fees_subgroups_id
									AND i.fees_groups_id = 3
									AND e.acad_program_groups_id = h.acad_program_groups_id 
									AND e.colleges_id = j.id
									AND j.status = 'Active'
									AND h.academic_terms_id = a.academic_terms_id
									AND j.id != 10
									AND h.posted = 'Y'
									AND ag.id=e.acad_program_groups_id
									AND ag.levels_id not in (7,12) "; 
							
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}
							break;
					default:
							$q = "SELECT
									i.description, 
									h.rate,
									COUNT(DISTINCT(a.students_idno)) AS enrollees	
								FROM
									student_histories AS a,
									assessments AS aa,
									course_offerings AS b,
									enrollments AS c,
									prospectus AS d,
									academic_programs AS e,
									acad_program_groups as ag,
									fees_schedule AS h,
									fees_subgroups AS i,						
									colleges AS j						
								WHERE
									a.id = c.student_history_id
									AND aa.student_histories_id = a.id
									AND b.id = c.course_offerings_id
									AND a.prospectus_id = d.id
									AND d.academic_programs_id = e.id
									AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
									AND aa.year_level = h.yr_level
									AND i.id = h.fees_subgroups_id
									AND i.fees_groups_id = 3
									AND e.acad_program_groups_id = h.acad_program_groups_id 
									AND e.colleges_id = j.id
									AND j.id = {$this->db->escape($colleges_id)}
									AND j.status = 'Active'
									AND h.academic_terms_id = a.academic_terms_id
									AND ag.id=e.acad_program_groups_id
									AND e.acad_program_groups_id not in (7,12)
									AND h.posted = 'Y' "; 
							
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}

							$q .= " GROUP BY
										i.id	
									ORDER BY 
										i.description ";

							break;
				}			
					
			}				

		
			//die($q);
			//log_message("INFO","ListOthersEnrolled    ".print_r($q,true));  // Toyet 9.27.2018; 4.23.2019
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		// Heavily edited - Toyet 4.23.2019
		function ListOthersEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status) {
			$result = null;
			
			if ($program_groups_id) {
				$q =   "SELECT ahf.description,
						       ahf.rate,
								 COUNT(distinct sh.students_idno) AS enrollees							       
						FROM assessments_history_fees ahf
						LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
						LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
						LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
						WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
						   AND ap.acad_program_groups_id={$this->db->escape($program_groups_id)}
						   AND ahf.fees_groups_id=3 ";   // Other School Fees
							
				if ($yr_level != '0') {
					$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
				}

				$q .= "GROUP BY ahf.description ";

			} else {
				switch ($colleges_id) {
					case '0':
							//Do not include Grad School if all colleges selected
							$q =   "SELECT ahf.description,
									       ahf.rate,
											 COUNT(distinct sh.students_idno) AS enrollees							       
									FROM assessments_history_fees ahf
									LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
									LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
									LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
									WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
									   AND ap.colleges_id != 10
									   AND ahf.fees_groups_id=3 ";   // Other School Fees
										
							if ($yr_level != '0') {
								$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
							}

							$q .= "GROUP BY ahf.description ";

							break;

					default:
							$q =   "SELECT ahf.description,
									       ahf.rate,
											 COUNT(distinct sh.students_idno) AS enrollees							       
									FROM assessments_history_fees ahf
									LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
									LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
									LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
									WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
									   AND ap.colleges_id={$this->db->escape($colleges_id)}
									   AND ahf.fees_groups_id=3 ";   // Other School Fees
										
							if ($yr_level != '0') {
								$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
							}

							$q .= "GROUP BY ahf.description ";

							break;
				}			
					
			}				

		
			//die($q);
			log_message("INFO","ListOthersEnrolled    ".print_r($q,true));  // Toyet 9.27.2018; 4.23.2019
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		/*
		 * ADDED: 7/10/15 by genes
		 * new reports for Learning Resources
		 */
		function ListLearningResourcesEnrolled_OLD($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q = "SELECT
							i.description, 
							h.rate,
							COUNT(DISTINCT(a.students_idno)) AS enrollees
						FROM
							student_histories AS a,
							assessments AS aa,
							course_offerings AS b,
							enrollments AS c,
							prospectus AS d,
							academic_programs AS e,
							acad_program_groups AS f,
							fees_schedule AS h,
							fees_subgroups AS i						
						WHERE
							a.id = c.student_history_id
							AND aa.student_histories_id = a.id
							AND b.id = c.course_offerings_id
							AND a.prospectus_id = d.id
							AND d.academic_programs_id = e.id
							AND e.acad_program_groups_id = f.id
							AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND aa.year_level = h.yr_level 
							AND i.id = h.fees_subgroups_id
							AND i.fees_groups_id = 10
							AND h.acad_program_groups_id = e.acad_program_groups_id
							AND h.academic_terms_id = a.academic_terms_id
							AND h.posted = 'Y' "; 
							
				if ($yr_level != '0') {
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
				}

			} else {
				switch ($colleges_id) {
					case '0':
							//Do not include Grad School if all colleges selected
							$q = "SELECT
										i.description, 
										h.rate,
										COUNT(DISTINCT(a.students_idno)) AS enrollees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										fees_schedule AS h,
										fees_subgroups AS i,
										colleges AS j						
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND i.id = h.fees_subgroups_id
										AND i.fees_groups_id = 10
										AND e.acad_program_groups_id = h.acad_program_groups_id 
										AND e.colleges_id =j.id
										AND j.status = 'Active'
										AND h.academic_terms_id = a.academic_terms_id
										AND j.id NOT IN (0,8,10)
										AND h.posted = 'Y' "; 

							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}

							break;
					default:
							$q = "SELECT
										i.description, 
										h.rate,
										COUNT(DISTINCT(a.students_idno)) AS enrollees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										fees_schedule AS h,
										fees_subgroups AS i,
										colleges AS j						
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND i.id = h.fees_subgroups_id
										AND i.fees_groups_id = 10
										AND e.acad_program_groups_id = h.acad_program_groups_id 
										AND e.colleges_id =j.id
										AND j.id = {$this->db->escape($colleges_id)}
										AND j.status = 'Active'
										AND h.academic_terms_id = a.academic_terms_id
										AND h.posted = 'Y' "; 

							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}

							break;
				}
			}
			
			$q .= " GROUP BY
						h.rate,i.id
					ORDER BY
						i.description";
			
			//die($q);
			//log_message("INFO","ListLearningResourcesEnrolled  ".print_r($q,true));  // Toyet 9.27.2018
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		

		// Heavily edited  - Toyet 4.24.2019
		function ListLearningResourcesEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q =   "SELECT ahf.description,
						       ahf.rate,
								 COUNT(distinct sh.students_idno) AS enrollees							       
						FROM assessments_history_fees ahf
						LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
						LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
						LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
						WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
						   AND ap.acad_program_groups_id={$this->db->escape($program_groups_id)}
						   AND ahf.fees_groups_id=10 ";   // Learning Resource
							
				if ($yr_level != '0') {
					$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
				}

				$q .= "GROUP BY ahf.description ";

			} else {
				switch ($colleges_id) {
					case '0':
						//Do not include Grad School if all colleges selected
						$q =   "SELECT ahf.description,
								       ahf.rate,
										 COUNT(distinct sh.students_idno) AS enrollees							       
								FROM assessments_history_fees ahf
								LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id != 10 
								   AND ahf.fees_groups_id=10 ";   // Learning Resource
									
						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
						}

						$q .= "GROUP BY ahf.description ";

							break;
					default:
						$q =   "SELECT ahf.description,
								       ahf.rate,
										 COUNT(distinct sh.students_idno) AS enrollees							       
								FROM assessments_history_fees ahf
								LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id={$this->db->escape($colleges_id)}
								   AND ahf.fees_groups_id=10 ";   // Learning Resource
									
						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
						}

						$q .= "GROUP BY ahf.description ";

							break;
				}
			}
			

			
			//die($q);
			log_message("INFO","LEARNING RESOURCE  ==>  ".print_r($q,true));  // Toyet 9.27.2018
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		
		/*
		 * ADDED: 7/10/15 by genes
		* new reports for Student Support Services
		*/
		function ListStudentSupportEnrolled_OLD($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q = "SELECT
							i.description, 
							h.rate,
							COUNT(DISTINCT(a.students_idno)) AS enrollees
						FROM
							student_histories AS a,
							assessments AS aa,
							course_offerings AS b,
							enrollments AS c,
							prospectus AS d,
							academic_programs AS e,
							acad_program_groups AS f,
							fees_schedule AS h,
							fees_subgroups AS i						
						WHERE
							a.id = c.student_history_id
							AND aa.student_histories_id = a.id
							AND b.id = c.course_offerings_id
							AND a.prospectus_id = d.id
							AND d.academic_programs_id = e.id
							AND e.acad_program_groups_id = f.id
							AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND aa.year_level = h.yr_level 
							AND i.id = h.fees_subgroups_id
							AND i.fees_groups_id = 11
							AND h.acad_program_groups_id = e.acad_program_groups_id
							AND h.academic_terms_id = a.academic_terms_id
							AND h.posted = 'Y' "; 
							
				if ($yr_level != '0') {
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
				}

			} else {
				switch ($colleges_id) {
					case '0':
							//Do not include Grad School if all colleges selected
							$q = "SELECT
										i.description, 
										h.rate,
										COUNT(DISTINCT(a.students_idno)) AS enrollees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										fees_schedule AS h,
										fees_subgroups AS i,
										colleges AS j						
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND i.id = h.fees_subgroups_id
										AND i.fees_groups_id = 11
										AND e.acad_program_groups_id = h.acad_program_groups_id 
										AND e.colleges_id =j.id
										AND j.status = 'Active'
										AND h.academic_terms_id = a.academic_terms_id
										AND j.id NOT IN (0,8,10)
										AND h.posted = 'Y' "; 

							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}

							break;
					default:
							$q = "SELECT
										i.description, 
										h.rate,
										COUNT(DISTINCT(a.students_idno)) AS enrollees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										fees_schedule AS h,
										fees_subgroups AS i,
										colleges AS j						
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND aa.year_level = h.yr_level 
										AND i.id = h.fees_subgroups_id
										AND i.fees_groups_id = 11
										AND e.acad_program_groups_id = h.acad_program_groups_id 
										AND e.colleges_id =j.id
										AND j.id = {$this->db->escape($colleges_id)}
										AND j.status = 'Active'
										AND h.academic_terms_id = a.academic_terms_id
										AND h.posted = 'Y' "; 

							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}

							break;
				}
			}
			
			$q .= " GROUP BY
						h.rate,i.id
					ORDER BY
						i.description";
			
			//die($q);
			//log_message("INFO",print_r($q,true)); // Toyet 9.27.2018
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		

		// Heavily edited - Toyet 4.24.2019 
		function ListStudentSupportEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q =   "SELECT ahf.description,
						       ahf.rate,
								 COUNT(distinct sh.students_idno) AS enrollees							       
						FROM assessments_history_fees ahf
						LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
						LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
						LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
						WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
						   AND ap.acad_program_groups_id={$this->db->escape($program_groups_id)}
						   AND ahf.fees_groups_id=11 ";   // Student Support Services
							
				if ($yr_level != '0') {
					$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
				}

				$q .= "GROUP BY ahf.description ";

			} else {
				switch ($colleges_id) {
					case '0':
						//Do not include Grad School if all colleges selected
						$q =   "SELECT ahf.description,
								       ahf.rate,
										 COUNT(distinct sh.students_idno) AS enrollees							       
								FROM assessments_history_fees ahf
								LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id != 10 
								   AND ahf.fees_groups_id=11 ";   // Student Support Services
									
						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
						}

						$q .= "GROUP BY ahf.description, ahf.rate ";

						
						break;

					default:
						$q =   "SELECT ahf.description,
								       ahf.rate,
										 COUNT(distinct sh.students_idno) AS enrollees							       
								FROM assessments_history_fees ahf
								LEFT JOIN assessments ass ON ass.id=ahf.assessments_id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id={$this->db->escape($colleges_id)}
								   AND ahf.fees_groups_id=11 ";   // Student Support Services
									
						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} ";
						}

						$q .= "GROUP BY ahf.description, ahf.rate ";

						break;
				}
			}
			

			
			//die($q);
			log_message("INFO","STUDENT SUPPORT   ==>".print_r($q,true)); // Toyet 9.27.2018
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function ListAdditionalOthersEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q = "SELECT
						i.id,
						i.description, 
						h.rate,
						COUNT(DISTINCT(a.students_idno)) AS enrollees,
						CONCAT(h.yr_level, 
						CASE
    						WHEN h.yr_level%100 BETWEEN 11 AND 13 THEN 'th'
    						WHEN h.yr_level%10 = 1 THEN 'st'
    						WHEN h.yr_level%10 = 2 THEN 'nd'
    						WHEN h.yr_level%10 = 3 THEN 'rd'
    						ELSE 'th'
						END) AS yr_level 
					FROM
						student_histories AS a 
							LEFT JOIN prospectus AS d ON a.prospectus_id = d.id,
						assessments AS aa,
						course_offerings AS b,
						enrollments AS c,
						academic_programs AS e,
						acad_program_groups AS f,
						fees_schedule AS h,
						fees_subgroups AS i						
					WHERE
						a.id = c.student_history_id
						AND aa.student_histories_id = a.id
						AND b.id = c.course_offerings_id
						AND d.academic_programs_id = e.id
						AND e.acad_program_groups_id = f.id
						AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
						AND aa.year_level = h.yr_level 
						AND i.id = h.fees_subgroups_id
						AND i.fees_groups_id = 4
						AND h.acad_program_groups_id = e.acad_program_groups_id
						AND h.academic_terms_id = a.academic_terms_id
						AND h.posted = 'Y' "; 
				
				if ($yr_level != '0') {
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
				}

			} else {
				switch ($colleges_id) {
					case '0':
						//Do not include Grad School if all colleges selected
							$q = "SELECT
									i.id,
									i.description, 
									h.rate,
									COUNT(DISTINCT(a.students_idno)) AS enrollees,
									CONCAT(aa.year_level, 
									CASE
			    						WHEN aa.year_level%100 BETWEEN 11 AND 13 THEN 'th'
			    						WHEN aa.year_level%10 = 1 THEN 'st'
			    						WHEN aa.year_level%10 = 2 THEN 'nd'
			    						WHEN aa.year_level%10 = 3 THEN 'rd'
			    						ELSE 'th'
									END) AS yr_level 
								FROM
									student_histories AS a 
										LEFT JOIN prospectus AS d ON a.prospectus_id = d.id,
									assessments AS aa,
									course_offerings AS b,
									enrollments AS c,
									academic_programs AS e,
									acad_program_groups AS ag,
									fees_schedule AS h,
									fees_subgroups AS i,						
									colleges AS j						
								WHERE
									a.id = c.student_history_id
									AND aa.student_histories_id = a.id
									AND b.id = c.course_offerings_id
									AND d.academic_programs_id = e.id
									AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
									AND aa.year_level = h.yr_level 
									AND i.id = h.fees_subgroups_id
									AND i.fees_groups_id = 4
									AND e.acad_program_groups_id = h.acad_program_groups_id 
									AND e.colleges_id = j.id
									AND j.status = 'Active'
									AND h.academic_terms_id = a.academic_terms_id
									AND j.id != 10
									AND ag.id=e.acad_program_groups_id
									AND ag.levels_id not in (7,12)
									AND h.posted = 'Y' "; 
							
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}
							break;

					default:
							$q = "SELECT
									i.id,
									i.description, 
									h.rate,
									COUNT(DISTINCT(a.students_idno)) AS enrollees,
									CONCAT(aa.year_level, 
									CASE
			    						WHEN aa.year_level%100 BETWEEN 11 AND 13 THEN 'th'
			    						WHEN aa.year_level%10 = 1 THEN 'st'
			    						WHEN aa.year_level%10 = 2 THEN 'nd'
			    						WHEN aa.year_level%10 = 3 THEN 'rd'
			    						ELSE 'th'
									END) AS yr_level 
								FROM
									student_histories AS a 
										LEFT JOIN prospectus AS d ON a.prospectus_id = d.id,
									assessments AS aa,
									course_offerings AS b,
									enrollments AS c,
									academic_programs AS e,
									fees_schedule AS h,
									fees_subgroups AS i,						
									colleges AS j						
								WHERE
									a.id = c.student_history_id
									AND aa.student_histories_id = a.id
									AND b.id = c.course_offerings_id
									AND d.academic_programs_id = e.id
									AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
									AND aa.year_level = h.yr_level 
									AND i.id = h.fees_subgroups_id
									AND i.fees_groups_id = 4
									AND e.acad_program_groups_id = h.acad_program_groups_id 
									AND e.colleges_id = j.id
									AND j.id = {$this->db->escape($colleges_id)}
									AND j.status = 'Active'
									AND h.academic_terms_id = a.academic_terms_id
									AND e.levels_id not in (7,12)
									AND h.posted = 'Y' "; 
							
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} ";
							}
							break;
				}			
					
			}				
			$q .= " GROUP BY
						aa.year_level, 
						i.id
					ORDER BY 
						i.description, 
						aa.year_level ";
		
			//die($q);
			//log_message("INFO","ListAdditionalOthersEnrolled  ".print_r($q,true)); // Toyet 9.27.2018
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		/*
		* UPDATED: 9/27/2016 by Genes
		* condition 'AND c.post_status IS NULL' was wirtten back
		* post_status for 'added' subjects must not be included
		*/
		//
		// FULL EDIT FROM THE ORIGINAL FUNCTION OF THE SAME NAME.  THIS IS TO ENSURE THAT POSTED ELEMENTS WILL NO 
		// LONGER CHANGE. -Toyet 4.6.2019
		// 
		function ListLabFeesEnrolled_OLD($academic_years_id, $academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q = "SELECT 
								aa.year_level,
								f.abbreviation,
								g.course_code,
								COUNT(DISTINCT(a.students_idno)) AS enrollees,
								SUM(g.paying_units) AS paying_units,
								h.rate,
								(COUNT(DISTINCT(a.students_idno))*h.rate) AS fees
							FROM
								student_histories AS a,
								assessments AS aa,
								course_offerings AS b,
								enrollments AS c,
								prospectus AS d,
								academic_programs AS e,
								acad_program_groups AS f,
								courses AS g 
									LEFT JOIN laboratory_fees AS h ON g.id = h.courses_id
							WHERE
								a.id = c.student_history_id
								AND aa.student_histories_id = a.id
								AND b.id = c.course_offerings_id
								AND a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND e.acad_program_groups_id = f.id
								AND g.id = b.courses_id
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND h.academic_years_id = {$this->db->escape($academic_years_id)}
								AND (c.post_status IS NULL OR TRIM(c.post_status)='')
								AND h.posted = 'Y' ";

				if ($yr_level != '0') {
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} "; 
				}
			
			} else {
				switch ($colleges_id) {
					case '0':
						//Do not include Grad School if all colleges selected
							$q = "SELECT 
										aa.year_level, 
										f.abbreviation,
										g.course_code,
										COUNT(DISTINCT(a.students_idno)) AS enrollees,
										SUM(g.paying_units) AS paying_units,
										h.rate,
										(COUNT(DISTINCT(a.students_idno))*h.rate) AS fees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										colleges AS i,
										courses AS g LEFT JOIN laboratory_fees AS h ON g.id = h.courses_id
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND e.colleges_id = i.id
										AND i.status = 'Active'
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND h.academic_years_id = {$this->db->escape($academic_years_id)}
										AND i.id != 10
										AND h.posted = 'Y' 
										AND c.post_status IS NULL ";
						
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} "; 
							}
			
							break;
					default:
							$q = "SELECT 
										aa.year_level, 
										f.abbreviation,
										g.course_code,
										COUNT(DISTINCT(a.students_idno)) AS enrollees,
										SUM(g.paying_units) AS paying_units,
										h.rate,
										(COUNT(DISTINCT(a.students_idno))*h.rate) AS fees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										colleges AS i,
										courses AS g LEFT JOIN laboratory_fees AS h ON g.id = h.courses_id
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND e.colleges_id = i.id
										AND i.id = {$this->db->escape($colleges_id)}
										AND i.status = 'Active'
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND h.academic_years_id = {$this->db->escape($academic_years_id)}
										AND h.posted = 'Y' 
										AND c.date_enrolled <= aa.transaction_date
										AND c.post_status IS NULL ";
							
							if ($colleges_id != 10) {
								$q .= "	AND i.id != 10";
							}
							
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} "; 
							}
							break;
				}			
			}		

			$q .= " GROUP BY
						g.id 
					ORDER BY
						g.course_code"; 
			
			//die($q);
			//log_message("INFO", "ListLabFeesEnrolled  ".print_r($q,true)); // Toyet 5.8.2018

			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}
		

		//
		// FULL EDIT FROM THE ORIGINAL FUNCTION OF THE SAME NAME.  THIS IS TO ENSURE THAT POSTED ELEMENTS WILL NO 
		// LONGER CHANGE. -Toyet 4.6.2019
		// 
		function ListLabFeesEnrolled($academic_years_id, $academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q =   "SELECT ass.year_level,
						       ap.abbreviation,
						       ahl.course_code,
						       count(sh.students_idno) AS enrollees,
						       0 AS paying_units,
						       ahl.rate,
						       sum(ahl.rate) AS fees
						FROM assessments ass
						LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
						LEFT JOIN assessments_history_lab ahl ON ahl.assessments_id=ass.id
						LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
						WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
						   AND ap.acad_program_groups_id={$this->db->escape($program_groups_id)}
						   AND ahl.course_code IS NOT NULL ";

				if ($yr_level != '0') {
					$q .= " AND ass.year_level = {$this->db->escape($yr_level)} "; 
				}

				$q .= "GROUP BY course_code ";

			} else {
				switch ($colleges_id) {
					case '0':
						//Do not include Grad School if all colleges selected
						$q =   "SELECT ass.year_level,
								       ap.abbreviation,
								       ahl.course_code,
								       count(sh.students_idno) AS enrollees,
								       0 AS paying_units,
								       ahl.rate,
								       sum(ahl.rate) AS fees
								FROM assessments ass
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								LEFT JOIN assessments_history_lab ahl ON ahl.assessments_id=ass.id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id != 10
								   AND ahl.course_code IS NOT NULL ";

						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} "; 
						}
						
						$q .= "GROUP BY course_code ";
		
						break;

					default:
						$q =   "SELECT ass.year_level,
								       ap.abbreviation,
								       ahl.course_code,
								       count(sh.students_idno) AS enrollees,
								       0 AS paying_units,
								       ahl.rate,
								       sum(ahl.rate) AS fees
								FROM assessments ass
								LEFT JOIN academic_programs ap ON ap.id=ass.academic_programs_id
								LEFT JOIN assessments_history_lab ahl ON ahl.assessments_id=ass.id
								LEFT JOIN student_histories sh ON sh.id=ass.student_histories_id
								WHERE sh.academic_terms_id={$this->db->escape($academic_terms_id)}
								   AND ap.colleges_id={$this->db->escape($colleges_id)}
								   AND ahl.course_code IS NOT NULL ";

						if ($yr_level != '0') {
							$q .= " AND ass.year_level = {$this->db->escape($yr_level)} "; 
						}
						
						$q .= "GROUP BY course_code ";

						break;
				}			
			}		

			log_message("INFO", "ListLabFeesEnrolled  ".print_r($q,true)); // Toyet 5.8.2018; 4.6.2019

			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}



		/*
		 * ADDED: 7/13/15 by genes
		 */
		function ListAffiliationEnrolled($academic_terms_id, $program_groups_id, $colleges_id, $yr_level, $post_status=NULL) {
			$result = null;
			
			if ($program_groups_id) {
				$q = "SELECT 
								aa.year_level, 
								i.description,
								f.abbreviation,
								g.course_code,
								COUNT(DISTINCT(a.students_idno)) AS enrollees,
								SUM(g.paying_units) AS paying_units,
								i.rate,
								(COUNT(DISTINCT(a.students_idno))*i.rate) AS fees
							FROM
								student_histories AS a,
								assessments AS aa,
								course_offerings AS b,
								enrollments AS c,
								prospectus AS d,
								academic_programs AS e,
								acad_program_groups AS f,
								courses AS g,
								fees_affiliation_courses AS h,
								fees_affiliation AS i
							WHERE
								a.id = c.student_history_id
								AND aa.student_histories_id = a.id
								AND b.id = c.course_offerings_id
								AND a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND e.acad_program_groups_id = f.id
								AND g.id = b.courses_id
								AND g.id = h.courses_id
								AND i.id = h.fees_affiliation_id
								AND i.academic_terms_id=a.academic_terms_id
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND i.posted = 'Y' ";
		
				if ($yr_level != '0') {
					$q .= " AND aa.year_level = {$this->db->escape($yr_level)} "; 
				}
				
				$q .= " GROUP BY
						i.id
					ORDER BY
						i.description";
					
			} else {
				switch ($colleges_id) {
					case '0':
						//Do not include Grad School if all colleges selected
							$q = "SELECT 
										aa.year_level, 
										j.description,
										f.abbreviation,
										g.course_code,
										COUNT(DISTINCT(a.students_idno)) AS enrollees,
										SUM(g.paying_units) AS paying_units,
										j.rate,
										(COUNT(DISTINCT(a.students_idno))*j.rate) AS fees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										colleges AS i,
										courses AS g,
										fees_affiliation_courses AS h,
										fees_affiliation AS j
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND e.colleges_id = i.id
										AND i.status = 'Active'
										AND g.id = h.courses_id
										AND j.id = h.fees_affiliation_id
										AND j.academic_terms_id=a.academic_terms_id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND i.id != 10
										AND j.posted = 'Y' ";
						
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} "; 
							}
			
							break;

					default:
							$q = "SELECT 
										aa.year_level, 
										j.description,
										f.abbreviation,
										g.course_code,
										COUNT(DISTINCT(a.students_idno)) AS enrollees,
										SUM(g.paying_units) AS paying_units,
										j.rate,
										(COUNT(DISTINCT(a.students_idno))*j.rate) AS fees
									FROM
										student_histories AS a,
										assessments AS aa,
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										acad_program_groups AS f,
										colleges AS i,
										courses AS g,
										fees_affiliation_courses AS h,
										fees_affiliation AS j
									WHERE
										a.id = c.student_history_id
										AND aa.student_histories_id = a.id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND e.acad_program_groups_id = f.id
										AND g.id = b.courses_id
										AND e.colleges_id = i.id
										AND i.id = {$this->db->escape($colleges_id)}
										AND i.status = 'Active'
										AND g.id = h.courses_id
										AND j.id = h.fees_affiliation_id
										AND j.academic_terms_id=a.academic_terms_id
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND j.posted = 'Y' ";
						
							if ($yr_level != '0') {
								$q .= " AND aa.year_level = {$this->db->escape($yr_level)} "; 
							}
							break;
				}

				$q .= " GROUP BY
						j.id
					ORDER BY
						j.description";
			}		

			//die($q); 
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}


		public function generatePostedRunningAssessmentReport($academic_terms_id, $academic_programs_id, $acad_program_groups_id, $max_yr_level) {

			$result = array();
			
			for ($x=1; $x<=$max_yr_level; $x++) {
				
				$result[$x] = new stdClass();
				
				$myresult   = $this->ListAcadProgramsByLevels($academic_terms_id, $academic_programs_id, $acad_program_groups_id, $x);
				
				if ($myresult) {
					$result[$x]->programs = $myresult;
				}
				
			}
		
			return $result;
			
		}

		
		private function ListAcadProgramsByLevels($academic_terms_id, $academic_programs_id, $acad_program_groups_id, $year_level) {

			$result = null;
			
					$q = "SELECT 
								{$this->db->escape($year_level)} AS year_level,
								(SELECT 
										COUNT(x.id) 
									FROM 
										student_histories AS x,
										assessments AS y 
									WHERE 
										y.student_histories_id = x.id 
										AND y.year_level = {$this->db->escape($year_level)} 
										AND y.academic_programs_id = {$this->db->escape($academic_programs_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
								) AS num_posted_students,
								(SELECT 
										SUM(y.assessed_amount) 
									FROM 
										student_histories AS x,
										assessments AS y 
									WHERE 
										y.student_histories_id = x.id 
										AND y.year_level = {$this->db->escape($year_level)}  
										AND y.academic_programs_id = {$this->db->escape($academic_programs_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
								) AS total_posted,
								(SELECT 
										COUNT(DISTINCT(a.id)) 
									FROM 
										student_histories AS a,
										prospectus AS b,
										course_offerings AS c,
										enrollments AS d
									WHERE
										a.prospectus_id = b.id
										AND a.id = d.student_history_id
										AND c.id = d.course_offerings_id
										AND a.year_level = {$this->db->escape($year_level)} 
										AND b.academic_programs_id = {$this->db->escape($academic_programs_id)}
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
								) AS num_running_students,
								(SELECT 
										x.rate 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.fees_subgroups_id = 14 
										AND x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = {$this->db->escape($year_level)}  
								) as mat_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = {$this->db->escape($year_level)} 
										AND	
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 2)
								) as misc_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = {$this->db->escape($year_level)} 
										AND 
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 3)
								) as osf_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = {$this->db->escape($year_level)} 
										AND 
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 4)
								) as aof_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = {$this->db->escape($year_level)} 
										AND	
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 10)
								) as lrf_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = {$this->db->escape($year_level)} 
										AND 
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 11)
								) as sss_rate,
								(SELECT 
												SUM(f.paying_units * g.rate)
											FROM
												student_histories AS a,
												prospectus AS b,
												academic_programs AS c,
												enrollments AS d,
												course_offerings AS e,
												courses AS f,
												tuition_others AS g
											WHERE 
												a.prospectus_id = b.id 
												AND b.academic_programs_id = c.id 
												AND a.id = d.student_history_id 
												AND d.course_offerings_id = e.id 
												AND e.courses_id = f.id 
												AND g.academic_terms_id = a.academic_terms_id
												AND g.levels_id = 6
												AND g.yr_level = a.year_level
												AND g.courses_id = e.courses_id 
												AND c.id = {$this->db->escape($academic_programs_id)}
												AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND a.year_level = {$this->db->escape($year_level)} 
												AND g.courses_id IN (2,153,154,573)
											GROUP BY 
												c.id
								) AS reed_fee,
								(SELECT 
												SUM(f.paying_units * g.rate)
											FROM
												student_histories AS a,
												prospectus AS b,
												academic_programs AS c,
												enrollments AS d,
												course_offerings AS e,
												courses AS f,
												tuition_others AS g
											WHERE 
												a.prospectus_id = b.id 
												AND b.academic_programs_id = c.id 
												AND a.id = d.student_history_id 
												AND d.course_offerings_id = e.id 
												AND e.courses_id = f.id 
												AND g.academic_terms_id = a.academic_terms_id
												AND g.levels_id = 6
												AND g.yr_level = a.year_level
												AND g.courses_id = e.courses_id 
												AND c.id = {$this->db->escape($academic_programs_id)}
												AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND a.year_level = {$this->db->escape($year_level)} 
												AND g.courses_id IN (1497,1506,1081,1067,1511,1971)
											GROUP BY 
												c.id
								) AS cwts_fee,
								(SELECT  
										SUM(g.paying_units * h.rate)
									FROM
										student_histories AS a, 
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										courses AS g,
										fees_schedule AS h
									WHERE
										a.id = c.student_history_id
										AND b.id = c.course_offerings_id
										AND a.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND g.id = b.courses_id
										AND h.fees_subgroups_id = 201
										AND h.acad_program_groups_id = e.acad_program_groups_id 
										AND h.academic_terms_id = a.academic_terms_id 
										AND h.levels_id = 6
										AND h.yr_level = a.year_level 
										AND a.year_level = {$this->db->escape($year_level)}
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND e.id = {$this->db->escape($academic_programs_id)} 
										AND b.courses_id NOT IN (SELECT 
																		courses_id 
																	FROM 
																		tuition_others 
																	WHERE 
																		academic_terms_id = {$this->db->escape($academic_terms_id)}
																		AND levels_id = 6
																		AND yr_level = {$this->db->escape($year_level)}
																)
								) AS tuition_fee,
								(SELECT 
										SUM(hh.rate)
									FROM
										student_histories AS aa,
										course_offerings AS bb,
										enrollments AS cc,
										prospectus AS dd,
										academic_programs AS ee,
										courses AS gg, 
										laboratory_fees AS hh,
										academic_terms AS jj
									WHERE
										aa.id = cc.student_history_id
										AND bb.id = cc.course_offerings_id
										AND aa.prospectus_id = dd.id
										AND dd.academic_programs_id = ee.id
										AND gg.id = bb.courses_id
										AND aa.academic_terms_id = jj.id
										AND aa.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND ee.id = {$this->db->escape($academic_programs_id)}
										AND aa.year_level = {$this->db->escape($year_level)}
										AND hh.courses_id = gg.id 
										AND hh.academic_years_id = jj.academic_years_id
										AND hh.levels_id = 6
								) AS lab_fee,
								SUM(affil.enrollees * affil.rate) AS affil_fees
							FROM 
								(SELECT 
									COUNT(DISTINCT(a.students_idno)) AS enrollees,
									i.rate
								FROM
									student_histories AS a,
									course_offerings AS b,
									enrollments AS c,
									prospectus AS d,
									academic_programs AS e,
									courses AS g,
									fees_affiliation_courses AS h,
									fees_affiliation AS i
								WHERE
									a.id = c.student_history_id
									AND b.id = c.course_offerings_id
									AND a.prospectus_id = d.id
									AND d.academic_programs_id = e.id
									AND g.id = b.courses_id
									AND g.id = h.courses_id
									AND i.id = h.fees_affiliation_id
									AND i.academic_terms_id = a.academic_terms_id
									AND c.status = 'Active'
									AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
									AND a.year_level = {$this->db->escape($year_level)}
									AND e.id = {$this->db->escape($academic_programs_id)}
									AND i.posted = 'Y' 
								GROUP BY 
									i.id
								) AS affil "; 

			//die($q);	
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
		}
		
				
		public function generatePostedRunningAssessmentReport_ByCollege($academic_terms_id, $colleges_id) {
			
			$result = NULL;
			
			$q1 = "SELECT 
							c.id AS acad_programs_id,
							c.abbreviation,
							c.acad_program_groups_id,
							c.max_yr_level
						FROM 
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							enrollments AS d
						WHERE 
							a.prospectus_id = b.id 
							AND b.academic_programs_id = c.id 
							AND a.id = d.student_history_id 
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND c.colleges_id = {$this->db->escape($colleges_id)} 
						GROUP BY 
							c.id
						ORDER BY 
							c.abbreviation ";
			//die($q1);				
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
							
				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$myresult = $this->ListAcadPrograms_YearLevels($academic_terms_id, $res->acad_programs_id, $res->acad_program_groups_id);
						
						if ($myresult) {
							$result[$cnt]->for_programs = $myresult;
						}									
						$cnt++;
					}
				}	
			}
			
			return $result;
							
		}
		

		private function ListAcadPrograms_YearLevels($academic_terms_id, $academic_programs_id, $acad_program_groups_id) {

			$result = null;
			
					$q = "SELECT 
								a.year_level,
								(SELECT 
										COUNT(x.id) 
									FROM 
										student_histories AS x,
										assessments AS y 
									WHERE 
										y.student_histories_id = x.id 
										AND y.year_level = a.year_level 
										AND y.academic_programs_id = {$this->db->escape($academic_programs_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
								) AS num_posted_students,
								(SELECT 
										SUM(y.assessed_amount) 
									FROM 
										student_histories AS x,
										assessments AS y 
									WHERE 
										y.student_histories_id = x.id 
										AND y.year_level = a.year_level  
										AND y.academic_programs_id = {$this->db->escape($academic_programs_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
								) AS total_posted,
								COUNT(DISTINCT(a.id)) AS num_running_students,
								(SELECT 
										x.rate 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.fees_subgroups_id = 14 
										AND x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = a.year_level 
								) as mat_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = a.year_level 
										AND	
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 2)
								) as misc_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = a.year_level 
										AND 
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 3)
								) as osf_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = a.year_level 
										AND 
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 4)
								) as aof_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = a.year_level 
										AND	
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 10)
								) as lrf_rate,
								(SELECT 
										SUM(x.rate) 
									FROM 
										fees_schedule AS x	
									WHERE 
										x.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
										AND x.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND x.levels_id = 6
										AND x.yr_level = a.year_level
										AND 
											x.fees_subgroups_id IN (SELECT 
																			id 
																		FROM 
																			fees_subgroups 
																		WHERE 
																			fees_groups_id = 11)
								) as sss_rate,
								(SELECT 
												SUM(f.paying_units * g.rate)
											FROM
												student_histories AS s,
												prospectus AS b,
												academic_programs AS c,
												enrollments AS d,
												course_offerings AS e,
												courses AS f,
												tuition_others AS g
											WHERE 
												s.prospectus_id = b.id 
												AND b.academic_programs_id = c.id 
												AND s.id = d.student_history_id 
												AND d.course_offerings_id = e.id 
												AND e.courses_id = f.id 
												AND g.academic_terms_id = s.academic_terms_id
												AND g.levels_id = 6
												AND g.yr_level = s.year_level
												AND g.courses_id = e.courses_id 
												AND c.id = {$this->db->escape($academic_programs_id)}
												AND s.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND s.year_level = a.year_level 
												AND g.courses_id IN (2,153,154,573)
											GROUP BY 
												c.id
								) AS reed_fee,
								(SELECT 
												SUM(f.paying_units * g.rate)
											FROM
												student_histories AS aa,
												prospectus AS b,
												academic_programs AS c,
												enrollments AS d,
												course_offerings AS e,
												courses AS f,
												tuition_others AS g
											WHERE 
												aa.prospectus_id = b.id 
												AND b.academic_programs_id = c.id 
												AND aa.id = d.student_history_id 
												AND d.course_offerings_id = e.id 
												AND e.courses_id = f.id 
												AND g.academic_terms_id = aa.academic_terms_id
												AND g.levels_id = 6
												AND g.yr_level = aa.year_level
												AND g.courses_id = e.courses_id 
												AND c.id = {$this->db->escape($academic_programs_id)}
												AND aa.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND aa.year_level = a.year_level 
												AND g.courses_id IN (1497,1506,1081,1067,1511,1971)
											GROUP BY 
												c.id
								) AS cwts_fee,
								(SELECT  
										SUM(g.paying_units * h.rate)
									FROM
										student_histories AS s, 
										course_offerings AS b,
										enrollments AS c,
										prospectus AS d,
										academic_programs AS e,
										courses AS g,
										fees_schedule AS h
									WHERE
										s.id = c.student_history_id
										AND b.id = c.course_offerings_id
										AND s.prospectus_id = d.id
										AND d.academic_programs_id = e.id
										AND g.id = b.courses_id
										AND h.fees_subgroups_id = 201
										AND h.acad_program_groups_id = e.acad_program_groups_id 
										AND h.academic_terms_id = s.academic_terms_id 
										AND h.levels_id = 6
										AND h.yr_level = s.year_level 
										AND s.year_level = a.year_level
										AND s.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND e.id = {$this->db->escape($academic_programs_id)} 
										AND b.courses_id NOT IN (SELECT 
																		courses_id 
																	FROM 
																		tuition_others 
																	WHERE 
																		academic_terms_id = {$this->db->escape($academic_terms_id)}
																		AND levels_id = 6
																		AND yr_level = s.year_level
																)
								) AS tuition_fee,
								(SELECT 
										SUM(hh.rate)
									FROM
										student_histories AS aa,
										course_offerings AS bb,
										enrollments AS cc,
										prospectus AS dd,
										academic_programs AS ee,
										courses AS gg, 
										laboratory_fees AS hh,
										academic_terms AS jj
									WHERE
										aa.id = cc.student_history_id
										AND bb.id = cc.course_offerings_id
										AND aa.prospectus_id = dd.id
										AND dd.academic_programs_id = ee.id
										AND gg.id = bb.courses_id
										AND aa.academic_terms_id = jj.id
										AND aa.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND ee.id = {$this->db->escape($academic_programs_id)}
										AND aa.year_level = a.year_level
										AND hh.courses_id = gg.id 
										AND hh.academic_years_id = jj.academic_years_id
										AND hh.levels_id = 6
								) AS lab_fee,
								(SELECT 
									(COUNT(DISTINCT(s.students_idno)) * i.rate) 
								FROM
									student_histories AS s,
									course_offerings AS b,
									enrollments AS c,
									prospectus AS d,
									academic_programs AS e,
									courses AS g,
									fees_affiliation_courses AS h,
									fees_affiliation AS i
								WHERE
									s.id = c.student_history_id
									AND b.id = c.course_offerings_id
									AND s.prospectus_id = d.id
									AND d.academic_programs_id = e.id
									AND g.id = b.courses_id
									AND g.id = h.courses_id
									AND i.id = h.fees_affiliation_id
									AND i.academic_terms_id = s.academic_terms_id
									AND s.academic_terms_id = {$this->db->escape($academic_terms_id)}
									AND s.year_level = a.year_level
									AND e.id = {$this->db->escape($academic_programs_id)}
									AND i.posted = 'Y' 
								) AS affil_fees								
							FROM 
								student_histories AS a,
								prospectus AS b,
								enrollments AS c
							WHERE 
								a.prospectus_id = b.id
								AND a.id = c.student_history_id
								AND b.academic_programs_id = {$this->db->escape($academic_programs_id)} 
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY 
								a.year_level 
							ORDER BY 
								a.year_level "; 

			//die($q);	
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
		}

		
		public function generatePostedRunningAssessmentReport_ByStudentList($academic_terms_id, $acad_programs_id, $acad_program_groups_id, $yr_level) {
			
			$result = NULL;
			
			$q1 = "SELECT 
							b.id AS student_histories_id,
							d.acad_program_groups_id,
							b.academic_terms_id,
							a.idno,
							a.lname,
							a.fname,
							a.mname,
							at.academic_years_id,
							d.abbreviation AS course_running,
							b.year_level AS yr_level_running,
							g.abbreviation AS course_posted,
							f.year_level AS yr_level_posted,
							f.assessed_amount AS amount_posted
						FROM 
							students AS a,
							student_histories AS b, 
							assessments AS f, 
							academic_programs AS g,
							prospectus AS c,
							academic_programs AS d,
							enrollments AS e,
							academic_terms AS at
						WHERE 
							a.idno = b.students_idno
							AND b.prospectus_id = c.id 
							AND c.academic_programs_id = d.id 
							AND b.id = e.student_history_id
							AND b.id = f.student_histories_id
							AND f.academic_programs_id = g.id
							AND b.academic_terms_id = at.id
							AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND f.academic_programs_id = {$this->db->escape($acad_programs_id)} 
							AND f.year_level = {$this->db->escape($yr_level)}
						GROUP BY 
							a.idno
						ORDER BY 
							a.lname,
							a.fname,
							a.mname ";
			//die($q1);				
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();							

				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$data2['student_histories_id']   = $res->student_histories_id;
						$data2['academic_terms_id']      = $res->academic_terms_id;
						$data2['acad_program_groups_id'] = $res->acad_program_groups_id;
						$data2['year_level']             = $res->yr_level_running;
						$data2['academic_years_id']      = $res->academic_years_id;
						
						$myresult = $this->compute_StudentRunningAssessment($data2);
						
						if ($myresult) {
							$result[$cnt]->amount_running = $myresult;
						}									
						$cnt++;
					}
				}	
			}
			
			return $result;
							
		}


		public function generatePostedRunningAssessmentReport_ByCollegeStudentList($academic_terms_id, $colleges_id) {
			
			$result = NULL;
			
			$q1 = "SELECT 
							c.id AS acad_programs_id,
							c.abbreviation,
							c.acad_program_groups_id,
							a.year_level
						FROM 
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							enrollments AS d
						WHERE 
							a.prospectus_id = b.id 
							AND b.academic_programs_id = c.id 
							AND a.id = d.student_history_id 
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND c.colleges_id = {$this->db->escape($colleges_id)} 
						GROUP BY 
							c.id,
							a.year_level
						ORDER BY 
							c.abbreviation,
							a.year_level ";
			//die($q1);				
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
							
				$cnt=0;
				if ($result) {
					foreach($result AS $res) {

						$myresult = $this->generatePostedRunningAssessmentReport_ByStudentList($academic_terms_id, $res->acad_programs_id, $res->acad_program_groups_id, $res->year_level);
							
						if ($myresult) {
							$result[$cnt]->program_students = $myresult;
						}									

						$cnt++;
					}
				}	
			}
			return $result;

		}

		
		private function compute_StudentRunningAssessment($data) {
			
			$this->load->model('hnumis/Student_Model');
			
			$total = 0;
			
			$tuition_basic  = $this->Student_Model->getMyTuitionFee_Basic($data['student_histories_id'], $data['acad_program_groups_id'],$data['academic_terms_id']);
			$tuition_others = $this->Student_Model->getMyTuitionFee_Others($data['student_histories_id'], $data['academic_terms_id']);
			$aff_fees       = $this->Student_Model->getMyAffiliated_Fees($data['student_histories_id']);
			$tuition_byprospectus = $this->Student_Model->getMyTuitionFee_Prospectus($data['student_histories_id'], $data['academic_terms_id']);
			$other_fees     = $this->Student_Model->getMyOtherFees($data['year_level'], $data['academic_terms_id'], $data['acad_program_groups_id']);
			$lab_fees       = $this->Student_Model->getMyLabFees($data['academic_years_id'], $data['student_histories_id']);

			$total          = $tuition_basic->tuition_fee  + $tuition_byprospectus->tuition_fee + $other_fees->rate + $lab_fees->rate ;

			if ($tuition_others) {
				$total = $total + $tuition_others->tuition_fee;
			}
				
			if ($aff_fees) {
				foreach($aff_fees AS $a_fee) {
					$total = $total + $a_fee->rate; 
				}
			}
			
			return $total;
						
		}
		
	}

?>