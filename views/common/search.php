<form id="srcform" action="<?php echo site_url("{$role}/{$what}/")?>" method="post" style="margin:0">
	<?php echo $this->common->hidden_input_nonce(); ?>
	<div class="input-append" style="width: 100%;box-sizing: border-box;">
		<input type="text" name="q" id="q" style="width:75%;" placeholder="Name / ID No" />
		<button class="btn" type="submit" style="width: 21%;"><i class="icon-search"></i></button>
	</div>
</form>
<script>
	var options, a;
		
	$( document ).ready(function() {
		options = { 
			serviceUrl:'<?php echo site_url("ajax/search_{$what}"); ?>',
			minChars:2,
		};
		a = $('#q').autocomplete(options);
		$( "#q" ).focus();
	});
</script>
<style>
.autocomplete {
	background-color: #F7F7F7;
	border: 1px solid #EEEEEE;
}
.autocomplete .selected {
	background-color: #CECECE;
}
</style>