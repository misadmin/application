<?php

	class Evaluators_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListEvaluators() {
			$result = null;
			
			$q1 = "
					SELECT 
							r.employees_empno,		
							e.lname, 
							e.fname, 
							CONCAT_ws('',LEFT(e.mname,1),'.') AS mname
						FROM 
							roles AS r LEFT JOIN employees e ON r.employees_empno = e.empno
						WHERE 
							r.role='evaluator'
							AND r.employees_empno NOT IN (SELECT x.employees_empno FROM roles AS x WHERE x.role='developer'
															AND x.employees_empno IS NOT NULL)
						ORDER BY 
							e.lname, e.fname
					" ;
			
			//print($q1); die();
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function CheckEvaluatorType($id) {
			if($id == null) { return null; }
				
			$result = new stdClass();
				
			$q1 = "SELECT
							id
						FROM
							roles
						WHERE
							role = 'evaluator'
							AND employees_empno = {$this->db->escape($id)} ";

			//print($q1); die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
				$result->eval_type = 'employee';
			} else {
				$result->eval_type = 'student';
		}
				
			return $result;
		}
		
		

	}
	

//end of evaluators_model.php