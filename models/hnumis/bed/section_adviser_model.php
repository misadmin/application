<?php

	class Section_adviser_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		
		function List_MySections($empno=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							b.block_sections_id,
							b.section_name,
							c.room_no,
							e.year_level,
							d.description,
							b.academic_years_id,
							CONCAT(f.end_year-1,' - ',f.end_year) AS sy,
							(SELECT 
									COUNT(DISTINCT(x.students_idno))
								FROM 
									basic_ed_histories AS x 
								WHERE 
									x.bed_block_sections_id = a.id 
							) AS num_students,
							(SELECT 
									COUNT(DISTINCT(y.courses_id))
								FROM 
									bed_course_offerings AS y,
									bed_block_course_offerings AS w 
								WHERE 
									y.id = w.bed_course_offerings_id 
									AND w.bed_block_sections_id = a.id 
							) AS num_subjects,
							d.abbreviation
						FROM 
							block_sections AS a,
							bed_block_sections AS b,
							rooms AS c,
							academic_programs AS d,
							bed_academic_programs AS e,
							academic_years AS f
						WHERE 
							a.id = b.block_sections_id
							AND b.rooms_id = c.id 
							AND a.academic_programs_id = d.id 
							AND e.academic_programs_id = d.id 
							AND b.academic_years_id = f.id 
							AND b.block_adviser = {$this->db->escape($empno)} 					
						ORDER BY 
							f.end_year DESC ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
			
		}


		function ListClassSchedules($block_sections_id=NULL) {

			$result = NULL;	

			$sql = "SELECT 
							CONCAT(TIME_FORMAT(h.from_time,'%h:%i%p'),' - ',TIME_FORMAT(h.to_time,'%h:%i%p')) AS time_sched,
							h.from_time,
							h.day_name,
							e.id,
							(SELECT 
									q.descriptive_title
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									courses AS q,
									bed_block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.bed_course_offerings_id = p.id 
									AND l.id = m.academic_terms_id 
									AND r.bed_block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Mon' 
								GROUP BY 
									l.academic_years_id
							) AS mon,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									employees AS q,
									bed_block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Mon'
									AND p.id = s.bed_course_offerings_id 
									AND l.id = m.academic_terms_id 
									AND s.bed_block_sections_id = {$this->db->escape($block_sections_id)}
								GROUP BY 
									l.academic_years_id
							) AS mon_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									courses AS q,
									bed_block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.bed_course_offerings_id = p.id 
									AND l.id = m.academic_terms_id 
									AND r.bed_block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Tue' 
								GROUP BY 
									l.academic_years_id
							) AS tue,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									employees AS q,
									bed_block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Tue'
									AND p.id = s.bed_course_offerings_id 
									AND l.id = m.academic_terms_id 
									AND s.bed_block_sections_id = {$this->db->escape($block_sections_id)}
								GROUP BY 
									l.academic_years_id
							) AS tue_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									courses AS q,
									bed_block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.bed_course_offerings_id = p.id 
									AND l.id = m.academic_terms_id 
									AND r.bed_block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Wed' 
								GROUP BY 
									l.academic_years_id
							) AS wed,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									employees AS q,
									bed_block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Wed'
									AND p.id = s.bed_course_offerings_id 
									AND l.id = m.academic_terms_id 
									AND s.bed_block_sections_id = {$this->db->escape($block_sections_id)}
								GROUP BY 
									l.academic_years_id
							) AS wed_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									courses AS q,
									bed_block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.bed_course_offerings_id = p.id 
									AND l.id = m.academic_terms_id 
									AND r.bed_block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Thu' 
								GROUP BY 
									l.academic_years_id
							) AS thu,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									employees AS q,
									bed_block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Thu'
									AND p.id = s.bed_course_offerings_id 
									AND l.id = m.academic_terms_id 
									AND s.bed_block_sections_id = {$this->db->escape($block_sections_id)}
								GROUP BY 
									l.academic_years_id
							) AS thu_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									courses AS q,
									bed_block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.bed_course_offerings_id = p.id 
									AND l.id = m.academic_terms_id 
									AND r.bed_block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Fri' 
								GROUP BY 
									l.academic_years_id
							) AS fri,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									academic_terms AS l,
									room_occupancy AS m,
									room_occupancy_bed_course_offerings_slots AS n,
									bed_course_offerings_slots AS o,
									bed_course_offerings AS p,
									employees AS q,
									bed_block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.bed_course_offerings_slots_id = o.id 
									AND o.bed_course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Fri'
									AND p.id = s.bed_course_offerings_id 
									AND l.id = m.academic_terms_id 
									AND s.bed_block_sections_id = {$this->db->escape($block_sections_id)}
								GROUP BY 
									l.academic_years_id
							) AS fri_teacher
						FROM 
							bed_block_course_offerings AS d,
							bed_course_offerings AS e,
							bed_course_offerings_slots AS f,
							room_occupancy_bed_course_offerings_slots AS g,
							room_occupancy AS h
						WHERE 
							d.bed_course_offerings_id = e.id 
							AND e.id = f.bed_course_offerings_id 
							AND f.id = g.bed_course_offerings_slots_id
							AND g.room_occupancy_id = h.id
							AND d.bed_block_sections_id = {$this->db->escape($block_sections_id)} 
						GROUP BY 
							h.from_time
						ORDER BY 
							h.from_time";
			//return $sql;
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}


		function CheckIfMyStudent($empno=NULL, $students_idno=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.block_sections_id
						FROM 
							bed_block_sections AS a,
							basic_ed_histories AS b
						WHERE 
							a.block_sections_id = b.bed_block_sections_id  
							AND a.block_adviser = {$this->db->escape($empno)} 
							AND b.students_idno = {$this->db->escape($students_idno)}
						LIMIT 1 ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
		
		}
		
		
	}
	
?>	