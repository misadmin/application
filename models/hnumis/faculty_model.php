<?php 
 
	class Faculty_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListFaculty($college_id) 
		{
			$result = null;
			
			$q = "
					SELECT 
						e.empno, CONCAT(e.empno, ': ', e.lname, ', ', e.fname) as neym
					FROM
						employees e
					LEFT JOIN
						roles r
					ON
						e.empno = r.employees_empno
					LEFT JOIN
						colleges c
					ON
						r.colleges_id=c.id
					WHERE
						c.id='{$college_id}'
					GROUP BY
						e.empno
					ORDER BY
						e.lname, e.fname
				";
				
			/*$q = "SELECT a.empno, CONCAT(a.empno,': ',a.lname,', ',a.fname) AS neym 
					FROM 
						employees AS a, col_faculty AS b
					WHERE
						a.empno=b.employees_empno 
						AND colleges_id = {$this->db->escape($college_id)} 
					ORDER BY a.lname, a.fname ";
			*/	
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		//Added: May 2, 2013 by Amie
		function List_faculty_by_college($empno) {
			$result = null;
			
			$q2  = "SELECT DISTINCT(CONCAT(e.lname, ', ', e.fname)) as name, e.empno
						FROM employees e, roles rf, roles rd
						WHERE e.empno = rf.employees_empno
						AND rf.colleges_id = rd.colleges_id
						AND rd.role = 'dean'
						AND rd.employees_empno = {$this->db->escape($empno)}
						ORDER by name";
	
				$query = $this->db->query($q2);
				if( $query && $query->num_rows() > 0){
					return $query->result();
				} else {
					return FALSE;
				}
			}
			


		function ListAllFaculty() {
			$result = null;
			
/**			$q = "
					SELECT 
						a.empno, 
						CONCAT(a.lname,', ',a.fname) AS neym 
					FROM 
						employees AS a, 
						col_faculty AS b
					WHERE 
						a.empno=b.employees_empno 
						AND a.status = 'active'
					ORDER BY 
						a.lname, a.fname 
					"; **/
			$q = "
				SELECT 
					e.empno, CONCAT_ws(', ',e.lname,e.fname) neym
				FROM
					employees e
				LEFT JOIN roles r ON e.empno = r.employees_empno
				WHERE
					e.status = 'active'
					AND r.role='faculty'
				ORDER BY
					e.lname, e.fname
			";	
			
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		function get_faculty_details($empno) {
			$result = null;
			
			$q = "SELECT 
						a.empno, 
						CONCAT(a.lname,', ',a.fname) AS neym 
					FROM 
						employees AS a, 
						col_faculty AS b
					WHERE 
						a.empno=b.employees_empno
						AND a.empno =  {$this->db->escape($empno)}
						AND a.status = 'active'
					ORDER BY 
						a.lname, a.fname ";
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		
		//Added: May 7, 2013 by Amie
		//used to get faculty details who have at least taught for 1 sem.
		function get_faculty($empno) {
			$result = null;
			
			$q = "SELECT 
						a.empno, 
						CONCAT(a.lname,', ',a.fname) AS neym 
					FROM 
						employees AS a, 
						col_faculty AS b
					WHERE 
						a.empno=b.employees_empno
						AND a.empno =  {$this->db->escape($empno)}
					ORDER BY 
						a.lname, a.fname ";
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}	
		
		//Added: May 16, 2014 by Amie
		function get_employees() {
			$result = null;
				
			$q = "SELECT a.empno, CONCAT(a.lname,', ',a.fname, ' ', a.mname) AS name
					FROM employees a
					ORDER BY a.lname, a.fname ";

			
			$query = $this->db->query($q);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}

	//Added: May 7, 2013
	//used in searching for faculty members to generate the exception report
	function list_faculty_by_college_term($term, $college_id) {
		$result = null;
			
			$q = "SELECT DISTINCT(a.empno), CONCAT(a.lname,', ',a.fname) AS name
					FROM employees AS a, course_offerings co, academic_terms ac, roles r
					WHERE a.empno = co.employees_empno
						AND co.status = 'A'
						AND co.academic_terms_id = {$this->db->escape($term)}
						AND a.empno = r.employees_empno
						AND r.role = 'faculty'
						AND r.colleges_id =  {$this->db->escape($college_id)}
					ORDER BY name";
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				return $query->result();
			}
			return $result;
	}

	
	function List_faculty_load($acad_term_id) {
		$result = null;
			
		$q2  = "SELECT e.empno,
					CONCAT(e.lname, ', ', e.fname) as name, 
					c.course_code,
					c.credit_units,
					c.paying_units,
					o.section_code,
					cos.start_time,
					cos.end_time,
					cos.days_day_code,
					rm.room_no,
					count(en.id) as num_enrollees 
				FROM 
					employees e
						LEFT JOIN course_offerings o ON e.empno = o.employees_empno
							LEFT JOIN enrollments en ON o.id = en.course_offerings_id
								LEFT JOIN courses c ON c.id = o.courses_id
									LEFT JOIN course_offerings_slots cos ON o.id = cos.course_offerings_id
										LEFT JOIN rooms rm ON cos.rooms_id = rm.id
				WHERE
					o.academic_terms_id = {$this->db->escape($acad_term_id)}
				GROUP BY o.id
				ORDER by e.empno, name, cos.days_day_code, cos.start_time";
	
		$query = $this->db->query($q2);
		if( $query && $query->num_rows() > 0){
		return $query->result();
	} else {
			return FALSE;
		}
	}
	
	
		/*
		 * @author: genes
		 * @date: 7/22/2014
		 * @remarks: roles should not be used as basis to determine type of employee (faculty or non-ac)
		 */
		function Count_Enrollees_by_faculty($empno,$academic_terms_id) {
			$result = null;
				
			$q = "SELECT
						i.course_code,
						h.section_code,
						CONCAT(i.course_code,'-',h.section_code) AS course_section,
						f.abbreviation,
						COUNT(c.students_idno) AS stud
					FROM
						courses AS i,
						course_offerings AS h 
							LEFT JOIN enrollments AS d ON d.course_offerings_id=h.id
							LEFT JOIN student_histories AS c ON c.id=d.student_history_id
							LEFT JOIN prospectus AS g ON c.prospectus_id=g.id
							LEFT JOIN academic_programs AS f ON g.academic_programs_id=f.id
					WHERE
						h.courses_id=i.id
						AND h.employees_empno = {$this->db->escape($empno)}
						AND h.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.status = 'A' 
					GROUP BY
						h.courses_id,h.section_code,f.id
					ORDER BY
						h.courses_id,h.section_code,f.abbreviation";
		
			//print($q);die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		}
		
	
}


//end of college_model.php