
<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<body">

<div style="background:#FFF; width:50%; text-align:center; margin:auto;" align="center">
	<form id="view_withdraw_sked" method="post" action="" class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="2" />
	<table height="100" align="left" cellpadding="0" cellspacing="0" class="head1" style="width:65%; margin-top:10px;">
  		<tr class="head">
			<td colspan="3" class="head" style="text-align:center; padding:10px;">List Withdrawal Schedule </td>
		</tr>
  		<tr>
    		<td colspan="3">  
  				<table border="0" cellpadding="2" cellspacing="0" class="inside" align="left">
					<tr>
									<td width="110" align="left">Academic Year </td>
									<td width="7" align="left">:</td>
									<td width="207" align="left">
									<select name="academic_terms_id" class="form">
								    <option value="">-- Select Academic Year --</option>
					  <?php
								  		
								        foreach($academic_terms AS $academic_term) {
										print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy.
											  "</option>");	
								}  ?> </select> </td>
   	 				</tr>
					<tr>
				  		<td colspan="3">
							<table style="width:100%;" border="0" cellspacing="0" cellpadding="0" id="term">
    							<tr>
									<td width="34%" align="left">Term</td>
									<td width="3%" align="left">:</td>
									<td width="63%" align="left">
									<select name="terms_id" class="form">
								    <option value="">-- Select Term --</option>
								  <?php
									 foreach($terms AS $term) {
										print("<option value=".$term->id.">".$term->terms_name."</option>");	
								}  ?> </select> </td>
   	 							</tr>
  							</table>
    						<table style="width:100%;" border="0" cellspacing="0" cellpadding="0" id="program">
								<tr>
									<td width="34%" align="left">Level</td>
									<td width="3%" align="left">:</td>
									<td width="63%" style="text-align:left;">
										<select id="level_id" name="level_id" class="form">
											<option value="">-- Select Level --</option>
										<?php	
											 foreach($levels AS $level) {
												print("<option value=".$level->id.">".$level->level."</option>");
										} ?>		
								  </select>  </td>
								</tr>
			  				</table>    					</td>
  					</tr>
					<tr align="center"> 
						<td colspan="3"> <button class="adjust" type="submit">Continue!</button> </td>
					</tr>
    			</table>
			</td>
 		</tr>
 	</table>
 </form>
</div>
</body>







  