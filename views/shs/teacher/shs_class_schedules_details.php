

			<div style="width:100%; overflow:auto; padding-top:5px;" id="show_students_to_submit_grades">
				<table class="table table-hover table-bordered table-striped" >
					<thead>
						<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
							<td class="shs_header" style="width:8%;">Strand-<br>Grade Level</td>
							<td class="shs_header" style="width:8%;">Section</td>
							<td class="shs_header" style="width:5%;">Course<br>Code</td>
							<td class="shs_header" style="width:15%;">Descriptive Title</td>
							<td class="shs_header" style="width:15%;">Class<br>Adviser</td>
							<td class="shs_header" style="width:15%;">Schedule</td>
							<td class="shs_header" style="width:5%;">No. of<br>Enrollees</td>
							<td class="shs_header" style="width:5%;">Classlist</td>
							<td class="shs_header" style="width:6%;">Midterm<br>Submitted</td>
							<td class="shs_header" style="width:6%;">Midterm<br>Confirmed</td>
							<td class="shs_header" style="width:6%;">Finals<br>Submitted</td>
							<td class="shs_header" style="width:6%;">Finals<br>Confirmed</td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($class_schedules) {
								foreach($class_schedules AS $sched) {
						?>	
									<tr>
										<td style="vertical-align:middle;"><?php print($sched->abbreviation.'-Grade '.$sched->grade_level); ?></td>
										<td style="vertical-align:middle; text-align:center;">
											<a href="<?php print($sched->block_sections_id); ?>" 
												strand_grade="<?php print($sched->abbreviation.'-Grade '.$sched->grade_level.' ['.$sched->section_name.']'); ?>"
												desciptive_title="<?php print($sched->descriptive_title); ?>"
												courses_id="<?php print($sched->courses_id); ?>"
												course_offerings_id="<?php print($sched->course_offerings_id); ?>"
												block_adviser="<?php print("Class Adviser: ".$sched->block_adviser); ?>"
												class="list_students" >
												<?php print($sched->section_name); ?>
											</a>
										</td>
										<td style="vertical-align:middle;"><?php print($sched->course_code); ?></td>
										<td style="vertical-align:middle;"><?php print($sched->descriptive_title); ?></td>
										<td style="vertical-align:middle;"><?php print($sched->block_adviser); ?></td>
										<td>
											<?php 
												if ($sched->time_sched) {
													foreach ($sched->time_sched AS $tsched) {
											?>
													<div>
														<?php print($tsched->course_sched); ?>
													</div>
											<?php
													}
												}
											?>
										</td>
										<td style="vertical-align:middle; text-align:center;"><?php print($sched->num_enrollees); ?></td>
										<td style="vertical-align:middle; text-align:center;">
											<a href="#" 
												teacher="<?php print($teacher); ?>"
												strand="<?php print($sched->abbreviation.' Grade '.$sched->grade_level.'-'.$sched->section_name); ?>"
												class_adviser="<?php print($sched->block_adviser); ?>"
												room_no="<?php print($sched->room_no); ?>"
												section_id="<?php print($sched->id); ?>"
												descriptive_title="<?php print($sched->descriptive_title); ?>"
												class="download_classlist_pdf">
												<img src="<?php print(base_url('assets/images/pdf_icon.png')); ?>" style="height:18px;" /> 
											</a>									
										</td>
										<td style="vertical-align:middle; text-align:center;"><?php print($sched->midterm_date); ?></td>
										<td style="vertical-align:middle; text-align:center;"><?php print($sched->midterm_confirm_date); ?></td>
										<td style="vertical-align:middle; text-align:center;"><?php print($sched->finals_date); ?></td>
										<td style="vertical-align:middle; text-align:center;"><?php print($sched->finals_confirm_date); ?></td>
									</tr>

						<?php 
								}
							}
						?>
					</tbody>
				</table>	
			</div>

			
<script>
	$('.list_students').click(function(e){
		e.preventDefault();		
		
		var block_sections_id   = $(this).attr('href');
		var strand_grade        = $(this).attr('strand_grade');
		var courses_id          = $(this).attr('courses_id');
		var desciptive_title    = $(this).attr('desciptive_title');
		var block_adviser       = $(this).attr('block_adviser');
		var course_offerings_id = $(this).attr('course_offerings_id');

		var element2          = $("option:selected", "#select2");
		var academic_terms_id = element2.val();

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/shs_class_schedules'));?>",
			data: { "block_sections_id": block_sections_id,
					"courses_id": courses_id,
					"course_offerings_id": course_offerings_id,
					"action": 'list_students'},
			dataType: 'json',
			success: function(response) {				
				$('#main_view_class_schedules').html(response.output); 	
				$('#show_strand_grade').html(strand_grade); 	
				$('#show_descriptive_title').html(desciptive_title); 	
				$('#show_block_adviser').html(block_adviser); 	
				$('#show_academic_terms_id').val(academic_terms_id); 	
				$('.heading').html('Senior High School - Grades Submission'); 	
			}
		});
		
	});

</script>
			
			