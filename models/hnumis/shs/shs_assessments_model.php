<?php

	class Shs_assessments_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

		private $assessments_id = NULL;
		
		
		function get_assessments_id() {
			return $this->assessments_id;
		}

		
		function AddAssessment($data) {
			
			$query = "INSERT INTO 
							assessments (student_histories_id,
										assessed_amount, 
										transaction_date,
										employees_empno,
										academic_programs_id,
										year_level
										)
						VALUES (
								{$this->db->escape($data['student_histories_id'])},
								{$this->db->escape($data['assessed_amount'])},
								NOW(),
								{$this->db->escape($data['employees_empno'])},
								{$this->db->escape($data['academic_programs_id'])},
								{$this->db->escape($data['year_level'])} 
								) ";
								
			if ($this->db->query($query)) {

				$this->assessments_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}	
		
		}
		
		
		function AddAssessment_History_Courses($data) {
			
			$sql = "INSERT INTO 
							assessments_history_courses (assessments_id,
										course_code, 
										paying_units,
										rate,
										encoded_by)
						VALUES (
								{$this->db->escape($data['assessments_id'])},
								{$this->db->escape($data['course_code'])},
								{$this->db->escape($data['paying_units'])},
								{$this->db->escape($data['rate'])},
								{$this->db->escape($data['encoded_by'])} 
								) ";
								
			if ($this->db->query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			}	
		}
		

		function AddAssessment_History_Fees($data) {
			
			$sql = "INSERT INTO 
							assessments_history_fees (assessments_id,
										fees_groups_id, 
										description,
										rate,
										encoded_by)
						VALUES (
								{$this->db->escape($data['assessments_id'])},
								{$this->db->escape($data['fees_groups_id'])},
								{$this->db->escape($data['description'])},
								{$this->db->escape($data['rate'])},
								{$this->db->escape($data['encoded_by'])} 
								) ";
								
			if ($this->db->query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			}	
		}


		function AddAssessment_History_Lab($data) {
			
			$sql = "INSERT INTO 
							assessments_history_lab (assessments_id,
										course_code, 
										rate,
										encoded_by)
						VALUES (
								{$this->db->escape($data['assessments_id'])},
								{$this->db->escape($data['course_code'])},
								{$this->db->escape($data['rate'])},
								{$this->db->escape($data['encoded_by'])} 
								) ";
								
			if ($this->db->query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			}	
		}


		function AddCourse_toEnrollments($data) {
			
			$sql = "INSERT INTO 
							enrollments (student_history_id,
										course_offerings_id, 
										enrolled_by,
										date_enrolled,
										post_status)
						VALUES (
								{$this->db->escape($data['student_history_id'])},
								{$this->db->escape($data['course_offerings_id'])},
								{$this->db->escape($data['enrolled_by'])},
								{$this->db->escape($data['date_enrolled'])},
								{$this->db->escape($data['post_status'])} 
								) ";
								
			if ($this->db->query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			}	
		}

		
		public function ListStudentsToAssess($data) {
			$result = null;
		
			
			// OLD Condition
			//   AND a.prospectus_id = {$this->db->escape($data['prospectus_id'])} 
			//
			// NEW Condition - Changed by Toyet 6.28.2018
			//   AND b.academic_programs_id = {$this->db->escape($data['academic_programs_id'])}
	
			$q1 = "SELECT 
						a.id AS student_histories_id,
						a.year_level,
						b.academic_programs_id,
						c.acad_program_groups_id,
						a.block_sections_id
					FROM 
						student_histories AS a,
						prospectus AS b,
						academic_programs AS c
					WHERE 
						a.prospectus_id = b.id 
						AND b.academic_programs_id = c.id
						AND a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])}
						AND b.academic_programs_id = {$this->db->escape($data['academic_programs_id'])} 
						AND a.year_level = {$this->db->escape($data['year_level'])} 
						AND a.can_enroll = 'Y'
						AND c.colleges_id = 12
						AND a.block_sections_id IS NOT NULL 
						AND a.id NOT IN (SELECT 
												x.student_histories_id 
											FROM 
												assessments AS x)
					ORDER BY 
						a.students_idno ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}


		function CheckTermProgram_HasAssessment($academic_terms_id,$academic_programs_id) {

			$q1 = "SELECT
							a.id
						FROM
							assessments AS a,
							student_histories AS b,
							prospectus AS c
						WHERE
							a.student_histories_id = b.id
							AND b.prospectus_id = c.id
							AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND c.academic_programs_id = {$this->db->escape($academic_programs_id)} ";
		
			$query = $this->db->query($q1);
		
			if($query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		
		}
		

		function getStudentsEnrolled($academic_terms_id, $academic_programs_id, $year_level=NULL) {
			$result = null;
			
			$q1 = "SELECT 
						COUNT(DISTINCT(a.students_idno)) AS num_students
					FROM 
						student_histories AS a,
						assessments AS b
					WHERE 
						a.id = b.student_histories_id
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
						
			if ($academic_programs_id != "All") {
				$q1 .= " AND b.academic_programs_id = {$this->db->escape($academic_programs_id)} ";
			}
		
			if ($year_level) {
				$q1 .= " AND b.year_level = {$this->db->escape($year_level)} ";
			}
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
	
		}
		
		
		function ListTuitionFees($academic_terms_id, $academic_programs_id, $year_level=NULL) {
			$result = null;
			
			$q1 = "SELECT 
						SUM(c.paying_units) AS paying_units,
						SUM(c.rate) AS rate,
						COUNT(DISTINCT(a.students_idno)) AS num_students,
						c.rate AS amt,
						CONCAT(e.abbreviation,'-',b.year_level) AS strand_grade
					FROM 
						student_histories AS a,
						assessments AS b,
						assessments_history_courses AS c,
						prospectus AS d,
						academic_programs AS e
					WHERE 
						a.id = b.student_histories_id
						AND b.id = c.assessments_id
						AND a.prospectus_id = d.id 
						AND d.academic_programs_id = e.id
						AND d.academic_programs_id = b.academic_programs_id
						AND b.year_level = a.year_level
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
						
			if ($academic_programs_id != "All") {
				$q1 .= " AND b.academic_programs_id = {$this->db->escape($academic_programs_id)} ";
			}
			
			if ($year_level) {
				$q1 .= " AND b.year_level = {$this->db->escape($year_level)} ";
			}
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
								
		}
		
		
		function ListAssessedMisc($academic_terms_id, $academic_programs_id, $year_level=NULL) {
			$result = null;
			
			$q1 = "SELECT 
						d.id,
						d.fees_group
					FROM 
						student_histories AS a,
						assessments AS b,
						assessments_history_fees AS c,
						fees_groups AS d
					WHERE 
						a.id = b.student_histories_id
						AND b.id = c.assessments_id
						AND c.fees_groups_id = d.id
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
						
			if ($academic_programs_id != "All") {
				$q1 .= " AND b.academic_programs_id = {$this->db->escape($academic_programs_id)} ";
			}
			
			if ($year_level) {
				$q1 .= " AND b.year_level = {$this->db->escape($year_level)} ";
			}
			
			$q1 .= " GROUP BY 
						c.fees_groups_id 
					ORDER BY 
						d.weight ";

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();

				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$myresult = $this->ListAssessedMisc_Dtls($res->id, $academic_terms_id, $academic_programs_id, $year_level);
						
						if ($myresult) {
							$result[$cnt]->fees_groups = $myresult;
						}									
						$cnt++;
					}
				}	
			} 
			
			return $result;			
			
		}
		
		
		private function ListAssessedMisc_Dtls($fees_groups_id, $academic_terms_id, $academic_programs_id, $year_level=NULL) {
			$result = null;
			
			$q1 = "SELECT 
						c.fees_groups_id,
						c.description,
						SUM(c.rate) AS rate,
						COUNT(DISTINCT(a.students_idno)) AS num_students,
						c.rate AS amt
					FROM 
						student_histories AS a,
						assessments AS b,
						assessments_history_fees AS c
					WHERE 
						a.id = b.student_histories_id
						AND b.id = c.assessments_id
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
						AND c.fees_groups_id = {$this->db->escape($fees_groups_id)} ";

			if ($academic_programs_id != "All") {
				$q1 .= " AND b.academic_programs_id = {$this->db->escape($academic_programs_id)} ";
			}
			
			if ($year_level) {
				$q1 .= " AND b.year_level = {$this->db->escape($year_level)} ";
			}
			
			$q1 .= " GROUP BY 
						c.description ";

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
					
		}


		function ListAssessedLab($academic_terms_id, $academic_programs_id, $year_level=NULL) {
			$result = null;
			
			$q1 = "SELECT 
						c.course_code,
						c.rate,
						COUNT(DISTINCT(a.students_idno)) AS num_students
					FROM 
						student_histories AS a,
						assessments AS b,
						assessments_history_lab AS c
					WHERE 
						a.id = b.student_histories_id
						AND b.id = c.assessments_id
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
						
			if ($academic_programs_id != "All") {
				$q1 .= " AND b.academic_programs_id = {$this->db->escape($academic_programs_id)} ";
			}
			
			if ($year_level) {
				$q1 .= " AND b.year_level = {$this->db->escape($year_level)} ";
			}
			
			$q1 .= " GROUP BY 
						c.course_code,
						c.rate
					ORDER BY 
						c.course_code ";

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
					
		}

		
		function ListUnpostedAssessments($academic_terms_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.idno,
							a.lname,
							a.fname,
							a.mname,
							a.gender,
							d.abbreviation,
							b.year_level
						FROM 
							students AS a,
							student_histories AS b,
							prospectus AS c,
							academic_programs AS d
						WHERE 
							a.idno = b.students_idno
							AND b.prospectus_id = c.id 
							AND c.academic_programs_id = d.id
							AND d.colleges_id = 12
							AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND b.can_enroll = 'Y'
							AND b.id NOT IN (SELECT 
													x.student_histories_id 
												FROM 
													assessments AS x 
											)
						ORDER BY 
							b.year_level,
							c.academic_programs_id,
							a.lname,
							a.fname,
							a.mname ";
			
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
			
		}
		
		
	}
	
?>
