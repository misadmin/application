<?php 

class Mass_printing_model extends CI_Model{
	
	public function grades ($academic_terms_id, $program_id, $year_level){
		$this->db->select("concat('SY ',ay.end_year-1,'-',ay.end_year) sy,
			ap.abbreviation as academic_program,
			co.section_code,
			c.descriptive_title,
			sh.year_level,
			CASE tr.term
				WHEN 1 THEN '1st Sem' 
				WHEN 2 THEN '2nd Sem'
				WHEN 3 THEN 'Summer'
				END AS term,
			c.course_code,	c.credit_units units,
			s.idno, concat(s.lname,', ', s.fname) fullname,  
			e.prelim_grade, e.midterm_grade, e.finals_grade", FALSE);
		$this->db->from('enrollments e');
		$this->db->join('course_offerings co', 'co.id=e.course_offerings_id', 'left');
		//$this->db->join('course_offerings_slots col', 'col.course_offerings_id=co.id', 'left');
		$this->db->join('courses c', 'c.id=co.courses_id', 'left');
		$this->db->join('student_histories sh', 'sh.id=e.student_history_id', 'left');
		$this->db->join('students s', 's.idno=sh.students_idno', 'left');
		$this->db->join('academic_terms tr', 'tr.id=sh.academic_terms_id', 'left');
		$this->db->join('prospectus pr', 'pr.id=sh.prospectus_id', 'left');
		$this->db->join('academic_programs ap', 'ap.id=pr.academic_programs_id', 'left');
		$this->db->join('academic_years ay', 'ay.id=tr.academic_years_id', 'left');
		$this->db->where('ap.id', $program_id);
		$this->db->where('tr.id', $academic_terms_id);
		$this->db->where('sh.year_level', $year_level);
		$this->db->order_by('s.lname, s.fname, c.course_code');
		$query = $this->db->get();
		if ($query && $query->num_rows() > 0) {
			//print_r($query->result()); die();
			return $query->result(); 
		} else {
			return array();			
		}
	}
	
	public function academic_groups (){
		$this->db->select('id, group_name, abbreviation')->from('acad_program_groups')->order_by('abbreviation');
		$query = $this->db->get();
		if($query && $query->num_rows())
			return $query->result(); else
			return array();
	}

	
	public function master_list($academic_terms_id, $wheres=array(), $order_by='s.lname, s.fname'){
		$this->db->select("
			s.idno, concat_ws(' ',concat_ws(', ',s.lname,s.fname), s.mname) student, s.gender, ag.abbreviation program_group, ap.abbreviation program, sh.year_level, 
			sum(e.`status`='Active') subjects, sum((e.`status`='Active')*c.credit_units) units,
			TIMESTAMPDIFF(YEAR,s.dbirth,CURDATE()) as age
		", FALSE);
		$this->db->from('enrollments e');
		$this->db->join('student_histories sh','sh.id = e.student_history_id', 'LEFT');
		$this->db->join('course_offerings co', 'co.id = e.course_offerings_id', 'LEFT');
		$this->db->join('courses c','c.id = co.courses_id', 'LEFT');
		$this->db->join('students s','s.idno = sh.students_idno', 'LEFT');
		$this->db->join('prospectus p','p.id = sh.prospectus_id', 'LEFT');
		$this->db->join('academic_programs ap','ap.id = p.academic_programs_id', 'LEFT'); 
		$this->db->join('acad_program_groups ag','ag.id = ap.acad_program_groups_id', 'LEFT');
		$this->db->where('sh.academic_terms_id', $academic_terms_id);
		if(count($wheres) > 0){
			foreach($wheres as $where){
				$this->db->where($where);
			}
		}
		$this->db->group_by('sh.id');	
		$this->db->order_by($order_by);
		$query = $this->db->get();	
		if($query && $query->num_rows())
			return $query->result(); else
			return array();
	}
	
	public function enrollment_list ($academic_terms_id, $academic_program, $year_level, $order_by='s.lname, s.fname'){
		$this->db->select("
			s.idno, concat_ws(' ', concat_ws(', ', s.lname, s.fname), s.mname) fullname, s.gender, ap.abbreviation program, sh.year_level, c.course_code, c.credit_units, pc.is_bracketed,
			concat('SY ',ay.end_year-1,'-',ay.end_year) sy, CASE at.term
				WHEN 1 THEN '1st Sem' 
				WHEN 2 THEN '2nd Sem'
				WHEN 3 THEN 'Summer'
				END AS academic_term
			", FALSE);
		$this->db->from('enrollments e');
		$this->db->join('student_histories sh','sh.id = e.student_history_id', 'LEFT');
		$this->db->join('course_offerings co', 'co.id = e.course_offerings_id', 'LEFT');
		$this->db->join('courses c','c.id = co.courses_id', 'LEFT');
		$this->db->join('students s','s.idno = sh.students_idno', 'LEFT');
		$this->db->join('prospectus p','p.id = sh.prospectus_id', 'LEFT');
		$this->db->join('academic_programs ap','ap.id = p.academic_programs_id', 'LEFT');
		$this->db->join('(
			SELECT p.id as prospectus_id, pc.courses_id, pc.is_bracketed 
			FROM prospectus p	
			LEFT JOIN prospectus_terms pt ON pt.prospectus_id=p.id 
			LEFT JOIN prospectus_courses pc ON pc.prospectus_terms_id=pt.id
			) as pc', 'pc.prospectus_id=sh.prospectus_id AND pc.courses_id=c.id', 'LEFT');
		$this->db->join('academic_terms at', 'sh.academic_terms_id=at.id', 'left');
		$this->db->join('academic_years ay', 'ay.id=at.academic_years_id', 'left'); 
		$this->db->where('sh.academic_terms_id', $academic_terms_id);
		$this->db->where('ap.id', $academic_program);
		$this->db->where('sh.year_level', $year_level);
		$order_by = 's.lname, s.fname, c.course_code';	
		$this->db->order_by($order_by);
		$query = $this->db->get();
		//echo $this->db->last_query(); die();
		if($query && $query->num_rows())
			return $query->result(); else
			return array();
	}
	
	
	public function basic_ed_enrollees($academic_years_id=114){
		$sql = "
			SELECT l.`level`, h.yr_level, bes.section_name, h.students_idno, concat_ws(', ',s.lname,s.fname) as student
			FROM basic_ed_histories h 
			LEFT JOIN basic_ed_sections bes 
				ON bes.id = h.basic_ed_sections_id 
			LEFT JOIN students s 
				ON s.idno = h.students_idno
			LEFT JOIN levels l 
				ON l.id = h.levels_id 
			WHERE 
				h.academic_years_id = '{$academic_years_id}'
			ORDER BY 
				l.id, h.yr_level, bes.section_name, student				
		";
		
		
	}
}