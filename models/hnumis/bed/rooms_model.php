<?php

	class Rooms_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

		private $my_query = NULL;
		
		function get_my_query() {
			return $this->my_query;
		}
		
		
		function ListDayNames($day_id) {
			$result = null;
			
			$q1 = "SELECT a.day_code, a.day_name
						FROM
							days AS a
						WHERE
							a.day_code={$this->db->escape($day_id)} ";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		function ListRooms() {
			$result = null;
			
			$query = $this->db->query("
					SELECT
						r.id,
						r.room_no,
						b.bldg_name as building_code,
						r.room_type
					FROM 
						rooms r
					LEFT JOIN
						buildings b
					ON 
						r.buildings_id=b.id
					");
					/*
					SELECT a.id, a.room_no, a.building_code, a.room_type
										FROM rooms AS a, buildings AS b
										WHERE a.buildings_id=b.id ");
					*/
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		//NOTE: list vacant rooms for BED is by academic year, not academic term as compared to college and SHS
		/*
		function ListVacantRooms($academic_years_id=NULL, $start_time=NULL, $end_time=NULL, $days=NULL, $buildings=NULL, $rooms_id=FALSE,$course_offerings_slots_id=NULL, $section_rooms_id=NULL) {
			$result = null;
			
			//$field_rms = "(208)";
			//NOTE: always show Field (room_id=208)
			
			if (!$rooms_id) { //new schedule
				$q1 = "(SELECT 
							a.room_no, 
							a.id 
						FROM 
							rooms AS a, 
							buildings AS b
						WHERE
							a.buildings_id = b.id 
							AND b.id IN " .$buildings. "
							AND a.status = 'active'
							AND a.id NOT IN (SELECT 
													d.rooms_id 
												FROM 
													room_occupancy AS d
												WHERE
													d.academic_terms_id IN (SELECT 
															x.id 
														FROM 
															academic_terms AS x 
														WHERE 
															x.academic_years_id = {$this->db->escape($academic_years_id)}
															AND x.term > 0
														)
													AND (
															(ADDTIME(d.from_time,'00:01') BETWEEN {$this->db->escape($start_time)} AND {$this->db->escape($end_time)} 
																OR SUBTIME(d.to_time,'00:01') BETWEEN {$this->db->escape($start_time)} AND {$this->db->escape($end_time)}  
															) 
															OR 
															({$this->db->escape($start_time)} BETWEEN ADDTIME(d.from_time,'00:01') AND SUBTIME(d.to_time,'00:01') 
																OR {$this->db->escape($end_time)} BETWEEN ADDTIME(d.from_time,'00:01') AND ADDTIME(d.to_time,'00:01')
															)
														)
													AND d.day_name IN " .$days. "
											)
						) 
						
						UNION
						
						(SELECT
								a.room_no,
								a.id
							FROM
								rooms AS a, 
								buildings AS b
							WHERE
								a.buildings_id = b.id 
								AND b.id IN " .$buildings. "
								AND a.status = 'always on'
						)	
						ORDER BY
							room_no ";
			
			} else { //schedule is changed
				$q1 = "(SELECT 
							a.room_no, 
							a.id 
						FROM 
							rooms AS a, 
							buildings AS b
						WHERE
							a.buildings_id = b.id 
							AND b.id IN " .$buildings. "
							AND a.status='active'
							AND a.id NOT IN (SELECT 
													d.rooms_id 
												FROM 
													room_occupancy AS d
												WHERE
													d.academic_terms_id IN (SELECT 
															x.id 
														FROM 
															academic_terms AS x 
														WHERE 
															x.academic_years_id = {$this->db->escape($academic_years_id)}
															AND x.term > 0
														)
													AND (
															(ADDTIME(d.from_time,'00:01') BETWEEN {$this->db->escape($start_time)} AND {$this->db->escape($end_time)} 
																OR SUBTIME(d.to_time,'00:01') BETWEEN {$this->db->escape($start_time)} AND {$this->db->escape($end_time)}  
															) 
															OR 
															({$this->db->escape($start_time)} BETWEEN ADDTIME(d.from_time,'00:01') AND SUBTIME(d.to_time,'00:01') 
																OR {$this->db->escape($end_time)} BETWEEN ADDTIME(d.from_time,'00:01') AND ADDTIME(d.to_time,'00:01')
															)
														)
														AND d.day_name IN " .$days. "
											)
						)
						
						UNION				
						
						(SELECT
							a.room_no,
							a.id
						FROM
							rooms AS a, 
							buildings AS b
						WHERE
							a.buildings_id = b.id 
							AND b.id IN " .$buildings. " ";

							
				if ($section_rooms_id) { //used for BED section
					$q1 .= " AND (a.id = {$this->db->escape($rooms_id)} 
							 OR a.id = {$this->db->escape($section_rooms_id)}) ";
				} else {
					$q1 .= " AND a.id = {$this->db->escape($rooms_id)} ";
				}

				
				$q1 .= " AND a.status='active' 
						)
						
						UNION
						
						(SELECT
								a.room_no,
								a.id
							FROM
								rooms AS a, 
								buildings AS b
							WHERE
								a.buildings_id = b.id 
								AND b.id IN " .$buildings. " 
								AND a.status = 'always on'
						)
						
						ORDER BY
							room_no ";
			
			}
			

			//return $q1;
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		} */
				
		public function listBEDRooms($buildings=NULL) {
			
			$result = null;
			
			$list = implode(",", $buildings);
			
			$q1 = "SELECT 
							a.room_no, 
							a.id 
						FROM 
							rooms AS a, 
							buildings AS b
						WHERE
							a.buildings_id = b.id 
							AND a.status = 'active' 
							AND b.id IN (".$list.")";
				
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
							
		}


		function AddRoomOccupancy($occupancy_data) {
			
			$results = $this->ListDayNames($occupancy_data['days_day_code']);
			
			foreach ($results AS $dname) {
				$query = "INSERT INTO 
								room_occupancy (rooms_id,
												academic_terms_id,
												from_time,
												to_time,
												day_name, 
												occupancy_type,
												encoded_by)
								VALUES ({$this->db->escape($occupancy_data['rooms_id'])},
									{$this->db->escape($occupancy_data['academic_terms_id'])},
									{$this->db->escape($occupancy_data['start_time'])},
									{$this->db->escape($occupancy_data['end_time'])},
									{$this->db->escape($dname->day_name)},
									'course_offering',
									{$this->session->userdata('empno')}
								)";
				
				$this->my_query = $query;

				if (!$this->db->query($query)) {
					return FALSE;
				} else {
					
					$occupancy_data['room_occupancy_id'] = $this->db->insert_id();
					
					if (!$this->AddRoomOccupancyCourseOfferingsSlots($occupancy_data)) {
						return FALSE;
					}
				}
				
			}

			return TRUE;
			
		}

		
		private function AddRoomOccupancyCourseOfferingsSlots($data) {
				
			$q1 = "INSERT INTO
							room_occupancy_course_offerings_slots (room_occupancy_id,
																	course_offerings_slots_id,
																	encoded_by)
						VALUES (
							{$this->db->escape($data['room_occupancy_id'])},
							{$this->db->escape($data['course_offerings_slots_id'])},
							{$this->session->userdata('empno')}
						) ";

			$this->my_query = $q1;

			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
		/*
		NOTE: function is changed because structure was modified due to SHS system. Refer to 
				ClearRoomsOccupancy() function - Genes 1/28/2017
				
		function ClearRoomOccupancy($course_offerings_slots) {
			
			$q = "DELETE FROM 
						room_occupancy
					WHERE 
						course_offerings_slots_id = {$this->db->escape($course_offerings_slots)} ";
			
			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				//print($q); die(); 
				return FALSE;
			}			
		}*/
		
		

		function ClearRoomsOccupancy($course_offerings_slots_id=NULL) {
			
			$q1 = "DELETE FROM 
								room_occupancy 
							WHERE 
								id IN (SELECT 
												a.room_occupancy_id 
											FROM 
												room_occupancy_course_offerings_slots AS a,
												course_offerings_slots AS b
											WHERE 
												a.course_offerings_slots_id = b.id 
												AND b.id = {$this->db->escape($course_offerings_slots_id)}
										) ";
			//die($q1);
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
										
		}

		
		function ClearRoomsOccupancy_ByOfferingsID($course_offerings_id=NULL) {
			
			$q1 = "DELETE FROM 
								room_occupancy 
							WHERE 
								id IN (SELECT 
												a.room_occupancy_id 
											FROM 
												room_occupancy_course_offerings_slots AS a,
												course_offerings_slots AS b
											WHERE 
												a.course_offerings_slots_id = b.id 
												AND b.course_offerings_id = {$this->db->escape($course_offerings_id)}
										) ";
			//die($q1);
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
										
		}
		
	}
	
	
//end of rooms_model.php
