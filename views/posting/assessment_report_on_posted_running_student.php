
<style>
	.myhead {
		background-color: #EAEAEA; 
	}
	
	td.head1 {
		text-align:center;
		vertical-align:middle;
		font-weight:bold;
	}

</style>

<h2 class="heading">Program Assessment Summary Report - Student List</h2>

	<div style="width:100%;">

		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 35px; padding: 2px; margin-bottom:20px;">
			<div style="float:right; overflow:auto;">
				<div id="show_change_icon" style="float:left; padding-right:8px; margin-top:6px;">
				</div>
				<div style="float:left; padding-top:10px; padding-right:10px; font-weight:bold;">
					Year Level: 				
				</div>
				<div style="float:left; padding-top:5px; padding-right:10px;">
					<select name="yr_level" class="form-control change_yr_level" style="width:auto; height:auto;" id="yr_level">
						<?php 
							if ($yr_levels) {
								foreach ($yr_levels AS $k=>$v) {
						?>		
									<option value="<?php print($k); ?>" 
										academic_terms_id="<?php print($academic_terms_id); ?>"
										acad_programs_id="<?php print($acad_programs_id); ?>" 
										acad_program_groups_id="<?php print($acad_program_groups_id); ?>" 
										college_name="<?php print($college_name); ?>"
										abbreviation="<?php print($abbreviation); ?>"
										description="<?php print($description); ?>"
										term_sy="<?php print($term_sy); ?>" >
										<?php print($v); ?>
									</option>
						<?php
								}
							}
						?>
					</select>
				</div>
			</div>	
		</div>
		
		<div style="width:100%; margin:0px auto; text-align:center; font-size:20px; font-weight:bold; color:#022E7A;" >
			<?php print($college_name); ?>
		</div>
		<div style="width:100%; margin:0px auto; text-align:center; font-size:16px; font-weight:bold; font-style:italic; color:#0A5901; padding-top:5px;" >
			<?php print($term_sy); ?>
		</div>
		<div style="width:100%; margin:0px auto; text-align:center; font-size:16px; font-weight:bold; color:#0A5901; padding-top:5px;" >
			<?php print($description); ?>
		</div>
				

		<div style="width:95%; overflow:auto; margin:0 auto; margin-top:15px;" id="data_for_download" >
			<?php
				$data = array(
							'college_name'=>$college_name,
							'abbreviation'=>$abbreviation,
							'description'=>$description,
							'term_sy'=>$term_sy,
							'year_level'=>$year_level,
							'reports'=>$reports,
							);
				$this->load->view('posting/views/download_link',$data);					
			?>
		</div>
		<div style="width:95%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_students" >
			<?php			
				$data = array(
							'reports'=>$reports,
							);
				$this->load->view('posting/views/list_of_students_with_posted_running_per_program',$data);					
			?>
		</div>


		
	</div>	
	

<script>
	$(document).ready(function(){
		$('.change_yr_level').bind('change', function(){
			
			var element1               = $("option:selected", "#yr_level");
			var yr_level               = element1.val();
			var academic_terms_id      = element1.attr('academic_terms_id');
			var acad_programs_id       = element1.attr('acad_programs_id');
			var acad_program_groups_id = element1.attr('acad_program_groups_id');
			var college_name           = element1.attr('college_name');
			var abbreviation           = element1.attr('abbreviation');
			var description            = element1.attr('description');
			var term_sy                = element1.attr('term_sy');
			
			$('#show_change_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:24px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/posted_running_student'); ?>",
				data: {
						"yr_level": yr_level,
						"academic_terms_id": academic_terms_id,
						"acad_programs_id": acad_programs_id,
						"acad_program_groups_id": acad_program_groups_id,
						"college_name": college_name,
						"abbreviation": abbreviation,
						"description": description,
						"term_sy": term_sy,
						"action": "change_yr_level" },
				dataType: 'json',
				success: function(response) {													

					$('#data_for_download').html(response.download); 
					$('#show_extracted_students').html(response.students); 
					
					$('#show_change_icon').html('');
					
				}
			});

		});
	});
</script> 

	