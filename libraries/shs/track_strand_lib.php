<?php

	class track_strand_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		
		/*
		 * @added: 11/2/2016
		 * @author: genes
		 */
		public function track_management() {
		
			$this->CI->load->model('hnumis/shs/track_strand_model');
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_tracks');
			
			switch ($action) {
				case 'list_tracks':
		
					$tracks = $this->CI->track_strand_model->ListTracks();
					
					$data = array(
									"tracks"=>$tracks,
								);
					
					break;
					
				case 'add_track':

					$data['abbreviation'] = $this->CI->input->post('abbreviation');
					$data['group_name']   = $this->CI->input->post('group_name');
					$data['num_year']     = $this->CI->input->post('num_year');
					$data['levels_id']    = 12;
					
					if ($this->CI->track_strand_model->AddAcadProgramGroups($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['tracks'] = $this->CI->track_strand_model->ListTracks();
					
					$my_return['output'] = $this->CI->load->view('shs/tracks/list_all_tracks', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_track':

					$data['id']           = $this->CI->input->post('track_id');
					$data['abbreviation'] = $this->CI->input->post('abbreviation');
					$data['group_name']   = $this->CI->input->post('group_name');
					$data['num_year']     = $this->CI->input->post('num_year');
					
					if ($this->CI->track_strand_model->UpdateAcadProgramGroups($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['tracks'] = $this->CI->track_strand_model->ListTracks();
					
					$my_return['output'] = $this->CI->load->view('shs/tracks/list_all_tracks', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'delete_track':

					if ($this->CI->track_strand_model->DeleteTrack($this->CI->input->post('track_id'))) {
						$my_return['msg'] = TRUE;
					} else {
						$my_return['msg'] = FALSE;
					}

					$data['tracks'] = $this->CI->track_strand_model->ListTracks();
					
					$my_return['output'] = $this->CI->load->view('shs/tracks/list_all_tracks', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
		
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/track_management',$data);
			$this->CI->content_lib->content();
				
		}
		
		
		public function strand_management() {
		
			$this->CI->load->model('hnumis/shs/track_strand_model');
			
			$data['tracks']   = $this->CI->track_strand_model->ListTracks();

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_strands');
			
			switch ($action) {
				case 'list_strands':
		
					$data['strands']  = $this->CI->track_strand_model->ListStrands();
					$data['track_id'] = 0;

					break;
					
				case 'add_strand':

					$data['acad_program_groups_id'] = $this->CI->input->post('track_id');
					$data['abbreviation'] 			= $this->CI->input->post('abbreviation');
					$data['description']  			= $this->CI->input->post('description');
					$data['max_yr_level'] 			= $this->CI->input->post('max_yr_level');
					$data['colleges_id']  			= 12;
					$data['status']      			= 'O';
					$data['inserted_by']  			= $this->CI->session->userdata('empno');

					if ($this->CI->track_strand_model->AddStrand($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['strands']  = $this->CI->track_strand_model->ListStrands($this->CI->input->post('track_id'));
					$data['track_id'] = $this->CI->input->post('track_id');
					
					$my_return['output'] = $this->CI->load->view('shs/strand_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_strand':

					$data['acad_program_groups_id'] = $this->CI->input->post('track_id');
					$data['abbreviation'] 			= $this->CI->input->post('abbreviation');
					$data['description']  			= $this->CI->input->post('description');
					$data['max_yr_level'] 			= $this->CI->input->post('max_yr_level');
					$data['id']  			        = $this->CI->input->post('strand_id');
					$data['update_by'] 				= $this->CI->session->userdata('empno');

					if ($this->CI->track_strand_model->UpdateStrand($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['strands']  = $this->CI->track_strand_model->ListStrands($this->CI->input->post('track_id'));
					$data['track_id'] = $this->CI->input->post('track_id');
					
					$my_return['output'] = $this->CI->load->view('shs/strand_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_status':

					$data['id']        = $this->CI->input->post('strand_id');
					$data['status']    = $this->CI->input->post('status');
					$data['track_id']  = $this->CI->input->post('track_id');
					$data['update_by'] = $this->CI->session->userdata('empno');
					
					if ($this->CI->track_strand_model->UpdateStrandStatus($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['strands']  = $this->CI->track_strand_model->ListStrands($this->CI->input->post('track_id'));
					$data['track_id'] = $this->CI->input->post('track_id');
					
					$my_return['output'] = $this->CI->load->view('shs/tracks/list_all_strands', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'delete_strand':

					$this->CI->track_strand_model->DeleteStrand($this->CI->input->post('strand_id'));
					$data['track_id'] = $this->CI->input->post('track_id');

					if (!$this->CI->input->post('track_id')) {
						$data['strands']  = $this->CI->track_strand_model->ListStrands();
					} else {
						$data['strands']  = $this->CI->track_strand_model->ListStrands($this->CI->input->post('track_id'));
					}
					
					
					$my_return['output'] = $this->CI->load->view('shs/tracks/list_all_strands', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'change_track':
					$data['track_id'] = $this->CI->input->post('track_id');
					
					if (!$this->CI->input->post('track_id')) {
						$data['strands']  = $this->CI->track_strand_model->ListStrands();
					} else {
						$data['strands']  = $this->CI->track_strand_model->ListStrands($this->CI->input->post('track_id'));
					}

					$my_return['output'] = $this->CI->load->view('shs/tracks/list_all_strands', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/strand_management',$data);
			$this->CI->content_lib->content();
				
		}

				
	}
	
?>