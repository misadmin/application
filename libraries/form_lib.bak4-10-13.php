<?php 

/**
 * Creates a form automatically with validations where you save time and effort :)
 * 
 * This Library require you to include this following files:
 * 
 *   Javascripts:
 *        Jquery
 *        Jquery.validate
 *        Twitter Bootstrap
 *        
 *   CSSs:
 *        Twitter Bootstrap
 * 
 * @author Kevin Felisilda 6128492
 * @author Aljun Bajao 6247912
 *
 */
class Form_lib_bak implements Form_lib_Interface_bak {
	
	/**
	 * holds the id control
	 * @var String
	 */
	private $form_id;
	
	/**
	 * holds the attribute controls
	 * @var Array
	 */
	private $attributes;
	
	/**
	 * holds the input controls
	 * 
	 * @var Multi Array
	 */
	private $inputs;
	
	/**
	 *  holds the hidden input controls
	 * @var Array
	 */
	private $hidden_inputs;
	
	/**
	 *  holds the rules
	 * @var Array
	 */
	private $rules;
	
	/**
	 * self explanatory
	 * @var String
	 */
	private $reset_label;
	
	/**
	 * self explanatory
	 * @var Boolean
	 */
	private $reset_enabled;
	
	/**
	 * holds the params for submit button
	 * @var String
	 */
	private $submit;
	
	/**
	 * class for every control group
	 * 
	 * @var String
	 */
	private $control_group_class;
	
	/**
	 * constructor
	 * @param unknown_type $form_attributes
	 */
	public function __construct($form_attributes=NULL){
		if (!is_null($form_attributes))
			$this->set_attributes($form_attributes);
		else
			$this->attributes = array();
		
		$this->clear_form();
	}
	
	public function clear_form(){
		$this->form_id = '';
		$this->inputs = array();
		$this->hidden_inputs = array();
		$this->rules = array();
		
		$this->submit = array('enabled'=>FALSE,'label'=>'Submit','class'=>'');
		$this->reset = array('enabled'=>FALSE,'label'=>'Reset','class'=>'');
		
		for ($i=0;$i<7;$i++){
			$this->form_id .= substr(str_shuffle('qwertyuiopasdfghjklzxcvbnm'),0,1);
		}
	}
	
	/**
	 * sets the id in the form
	 * @param string $form_id
	 */
	public function set_id($form_id){
		$this->form_id = $form_id;
	}
	
	/**
	 * sets all the attributes in the form
	 * @param string $form_attributes
	 */
	public function set_attributes($form_attributes){
		$this->attributes = $form_attributes;
	}
	
	/**
	 * Enqueue Hidden Input to the form 
	 * @param  string $name
	 * @param  string $value
	 * @param  string $attributes
	 * @return boolean
	 */
	public function enqueue_hidden_input($name, $value='', $attributes=array()){
		if (empty($name)){
			return FALSE;
		}
		$input = array();
		$input['type'] = 'hidden';
		$input['name'] = $name;
		$input['value'] = $value;
		$input['attributes'] = $this->extract_attributes($attributes);
		
		array_push($this->hidden_inputs, $input);
	}
	
	/**
	 * Enqueue Password Input to the form
	 * 
	 * <code>void enqueue_text_input (mixed $data [, String $label='' [, $placeholder='' [, $value='' [, $required=FALSE [, $prepend='' [, $append='' [, $rules='']]]]])</code>
	 * 
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * //inline method                         //id              //label            //placeholder //textbox value  //is required
	 * $this->form_lib->enqueue_password_input('password',       'Password',         '*******',      '',           TRUE);
	 * $this->form_lib->enqueue_password_input('passwordconfirm','Confirm Password', '*******',      '',           TRUE);
	 * 
	 * //array method
	 * $password_input = array('name' => 'password',
	 *                         'label' => 'Password',
	 *                         'rules' => array('required','minlength:3'),
	 *                         'placeholder' => '******'
	 *                        );
	 * $this->form_lib->enqueue_password_input($password_input);
	 * </pre>
	 * 
	 * 
	 * @param Assoc Array or String $data
	 * @param String $label
	 * @param String $placeholder
	 * @param String $value
	 * @param Boolean $required
	 * @param String $prepend
	 * @param String $append
	 * @param String $rules
	 * @param array $attributes
	 */
	public function enqueue_password_input($data, $label='', $placeholder='', $value='', $required=FALSE, $prepend='', $append='', $rules=''){
		$this->enqueue_input('password',$data, $label, $placeholder, $value, $required, $prepend, $append, $rules);
	}
	
	/**
	 * Enqueue Text Input to the form
	 * 
	 * <code>void enqueue_text_input (mixed $data [, String $label='' [, $placeholder='' [, $value='' [, $required=FALSE [, $prepend='' [, $append='' [, $rules='']]]]])</code>
	 * 
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * //inline method                     //id              //label            //placeholder //textbox value  //is required  //prepend  //append  //rules
	 * $this->form_lib->enqueue_text_input('username',       'Username',         'Username',       '',           TRUE           );
	 * $this->form_lib->enqueue_text_input('email',          'Email Address',    'Email Add',      '',           TRUE           );
	 * 
	 * //array method
	 * $password_input = array('name' => 'password',
	 *                         'label' => 'Password',
	 *                         'rules' => array('required','minlength:3'),
	 *                         'placeholder' => '******'
	 *                        );
	 * $this->form_lib->enqueue_password_input($password_input);
	 * </pre>
	 * 
	 * 
	 * @param Assoc Array or String $data
	 * @param String $label
	 * @param String $placeholder
	 * @param String $value
	 * @param Boolean $required
	 * @param String $prepend
	 * @param String $append
	 * @param String $rules
	 * @param array $attributes //not yet implemented
	 */
	public function enqueue_text_input($data, $label='', $placeholder='', $value='', $required=FALSE, $prepend='', $append='', $rules=''){
		$this->enqueue_input('text',$data, $label, $placeholder, $value, $required, $prepend, $append, $rules);
	}
	
	/**
	 * Enqueue Input to the form
	 * @param String $type
	 * @param Multi $data
	 * @param String $label
	 * @param String $placeholder
	 * @param String $value
	 * @param String $required
	 * @param String $prepend
	 * @param String $append
	 * @param String $rules
	 * @param String $attributes
	 * @return Boolean
	 */
	private function enqueue_input($type, $data, $label='', $placeholder='', $value='', $required='', $prepend='', $append='', $rules='', $attributes=''){
		//data is empty, nothing to do in here
		if (empty($data)){
			return FALSE;
		}
		
		//create input and rules variable
		$input = array();
		$rules = array();
		
		//check if data is an associative array 
		if (is_array($data)){
			if (!isset($data['name']) || empty($data['name'])){
				return FALSE;
			}
			$input['name'] = $data['name'];
			$input['label'] = isset($data['label']) ? $data['label'] : '';
			$input['placeholder'] = isset($data['placeholder']) ? $data['placeholder'] : '';
			$input['value'] = isset($data['value']) ? $data['value'] : '';
			$input['required'] = isset($data['required']) ? $data['required'] : '';
			$input['size'] = isset($data['size']) ? $data['size'] : '';
			$input['append'] = isset($data['append']) ? $data['append'] : '';
			$input['prepend'] = isset($data['prepend']) ? $data['prepend'] : '';
			$input['attributes'] = isset($data['attributes']) ? $this->extract_attributes($data['attributes']) : '';
			$input['rules'] = isset($data['rules']) ? $data['rules'] : '';
			$input['class'] = isset($data['class']) ? $data['class'] : '';
		} else { //data is a string thus data will be equal to name and other parameters will be passed
			$input['name'] = $data;
			$input['label'] = $label;
			$input['placeholder'] = $placeholder;
			$input['value'] = $value;
			$input['append'] = $append;
			$input['prepend'] = $prepend;
			$input['rules'] = $rules;
			$input['attributes'] = $this->extract_attributes($attributes);
		}
		
		//insert rules
		
		//check if rule is a string, not an array
		if (!is_array($input['rules'])){
			$input['rules'] = array($input['rules']);
		}
		
		//rule must be not empty
		if (count($input['rules']) > 0) {
			foreach ($input['rules'] as $rule){
				$rule_array = explode(':',$rule);
				$this->set_rule($input['name'],$rule_array[0],(isset($rule_array[1]) ? $rule_array[1] : ''));
			}
		}
		
		$input['type'] = $type;
		array_push($this->inputs, $input);
	}
	
	/**
	 * Enqueue Text Area to the form
	 * @param Multi $data
	 * @param String $label
	 * @param String $value
	 * @param Boolean $required
	 */
	public function enqueue_textarea($data, $label, $class='', $required=FALSE, $value='', $rules=''){
		if (empty($data)){
			return False;
		}
		$input = array();
		$input['type'] = 'textarea';
		if (is_array($data)){
			$input['name'] = $data['name'];
			$input['label'] = $data['label'];
			$input['value'] = $data['value'];
			$input['rules'] = $data['rules'];
			if (isset($data['required'])){
				$this->set_rule($data['name'],'required');
			}
		}
		else {
			$input['name'] = $data;
			$input['label'] = $label;
			$input['value'] = $value;
			$input['rules'] = $rules;
			if ($required){
				$this->set_rule($data,'required');
			}
		}
		
		if (is_array($input['rules']) AND count($input['rules']) > 0) {
			foreach ($input['rules'] as $rule){
				$rule_array = explode(':',$rule);
				$this->set_rule($input['name'],$rule_array[0],(isset($rule_array[1]) ? $rule_array[1] : ''));
			}
		}
		
		array_push($this->inputs, $input);
	}
	
	public function enqueue_checkbox($data, $label, $class){
		
	}
	
	/**
	 * Sets the rule in the form
	 * @param String $target
	 * @param String $name
	 * @param String $parameter
	 */
	public function set_rule($target,$name,$parameter=''){
		//check
		if (empty($target) || empty($name)){
			return False;
		}
		
		//process rule
		$to_insert_rule = array();
		
		//if rule is just a require string
		if ($name == 'required'){
			$to_insert_rule['rule'] = $name;
			$to_insert_rule['value'] = 'true';
		}
		else {
			$to_insert_rule['rule'] = $name;
			if ($name == 'equalTo')
				$to_insert_rule['value'] = '"#{form_id}-'.$parameter.'"';
			else
				$to_insert_rule['value'] = $parameter;
		}
		
		$this->rules[$target][] = $to_insert_rule;
		
		/*
		echo '<pre>';
		print_r($this->rules);
		print_r($to_insert_rule);
		die();
		
		//check if there are rules entered previously
		if (count($this->rules) > 0){
			$found = False;
			//loop through the rule targets
			foreach ($this->rules as $rule_target => $rules){
				//check if the target has a rule already define
				if ($rule_target == $target){
					$found = True;
					break;
				}
			}
			if ($found){
				$this->rules[$target][] = $to_insert_rule;
			}
		}
		else {
			
		} */
	}
	
	/**
	 * Set Submit Button to the form
	 * Submit button will be place at the bottom part of the form
	 * @param String $label
	 * @param Multi $class
	 */
	public function set_submit_button($label, $class=''){
		$this->submit['enabled'] = TRUE;
		$this->submit['label'] = $label;
		$this->submit['class'] = (is_array($class) ? implode(' ',$class) : $class);
	}
	/**
	 * Set Reset Button to the form
	 * Submit button will be place at the bottom part of the form
	 * @param String $label
	 * @param Multi $class
	 */
	public function set_reset_button($label, $class=''){
		$this->reset['enabled'] = TRUE;
		$this->reset['label'] = $label;
		$this->reset['class'] = (is_array($class) ? implode(' ',$class) : $class);
	}
	
	/**
	 * Adds a class attribute to each control group.
	 * @param Multi $class
	 */
	public function add_control_class ($class=''){
		if (!empty($class)){
			if (is_array($class) && count($class)>0)
				$this->control_group_class = implode(' ', $class); else
				$this->control_group_class = $class;
		}
	}
	
	public function enqueue_free_line($line){
		$input = array();
		$input['type'] = 'freeline';
		$input['content'] = $line;
		array_push($this->inputs, $input);
	}
	
	/**
	 * Process all the form elements
	 * @param Boolean $return
	 * @return String
	 */
	public function content($return=TRUE,$debug=FALSE){
		if($debug){
			echo '<pre>';
			print_r($this);
			exit();
		}
		
		//$form_id = (!empty($this->form_id) ? $this->form_id : str_shuffle('kevinfelisilda')); //haha
		$form_id = $this->form_id;
		
		$form = '<form id="'.$form_id.'"'.$this->extract_attributes($this->attributes).'>';
		if (count($this->hidden_inputs) > 0){
			foreach($this->hidden_inputs as $input){
				$form .= '<div class="hidden"><input type="'.$input['type'].'" id="'.$input['name'].'" name="'.$input['name'].'" value="'.$input['value'].'"'.$input['attributes'].'></div>';
			}
		}
		$form .= '<fieldset>';
		if (count($this->inputs) > 0){
			foreach($this->inputs as $input){
				if ($input['type'] == 'text' || $input['type'] == 'password'){
					$extra = (!empty($input['prepend']) || !empty($input['append']) ? TRUE : FALSE);
					$form .= '<div class="control-group ' . $this->control_group_class . '">';
					$form .= (!empty($input['label']) ? '<label for="'.$input['name'].'" class="control-label">'.$input['label'].'</label>' : '');
					$form .= '<div class="controls">';
					if ($extra){
						//$form .= '<div class="'.(!empty($input['prepend']) ? 'input-prepend' : '').(!empty($input['append']) ? ' input-append' : '').'">';
						$form .= sprintf('<div class="%s%s">',(!empty($input['prepend']) ? 'input-prepend' : ''),(!empty($input['append']) ? (!empty($input['prepend']) ? ' ' : '').'input-append' : ''));
					}
					if (!empty($input['prepend'])){
						//$form .= '<span class="add-on" id="'.$input['name'].'-prepend">'.$input['prepend'].'</span>';
						$form .= sprintf('<span class="add-on" id="%s">%s</span>',substr(base64_encode($input['append'].'-prepend'),0),$input['prepend']);
						//$form .= "<script>$('#fname').width(206-($('#fnameprep').width()+11))</script>";
					}
					//$input['name'] is made as ID?
					$form .= '<input type="'.$input['type'].'" id="'.$form_id.'-'.$input['name'].'" name="'.$input['name'].'" value="'.$input['value'].'" placeholder="'.$input['placeholder'] . '"';
					
					//added by justing... this is always set as an array or an empty string... lets test for empty
					if (! empty($input['class'])){
						if(is_array($input['class']) && count($input['class']) > 0){
							$form .= ' class="' . implode(' ', $input['class']) . '"';
						} else {
							//it could be a string...
							$form .= ' class="' . $input['class'] . '"';
						}
					}
					$form .= '>';
					if (!empty($input['append'])){
						$form .= sprintf('<span class="add-on" id="%s">%s</span>',substr(base64_encode($input['append'].'-append'),0),$input['append']);
					}
					if ($extra){
						$form .= '</div>';
					}
					$form .= '</div></div>';
					//$form .= '<div class="formSep"></div>';
				}
				else if ($input['type'] == 'textarea'){
					$form .= '<div class="control-group ' . $this->control_group_class . '">';
					$form .= '<label class="control-label" for="'.$form_id.'-'.$input['name'].'">'.$input['label'].'</label>';
					$form .= '<div class="controls">';
					$form .= '<textarea style="resize:vertical" rows="3" id="'.$form_id.'-'.$input['name'].'" name="'.$input['name'].'">'.$input['value'].'</textarea>';
					$form .= '</div></div>';
				}
				else if ($input['type'] == 'checkbox'){
					$form .= '<label class="checkbox">';
					$form .= '<input type="checkbox" value="'.$input['value'].'">';
					$form .= $input['label'];
					$form .= '</label>';
				}
				else if ($input['type'] == 'freeline'){
					$form .= $input['content'];
				}
			}
			if ($this->submit['enabled'] || $this->reset['enabled']){
				$form .= '<div class="form-actions">';
				$form .= ($this->submit['enabled'] ? '<button type="submit" style="margin-right:10px" class="'.(!empty($this->submit['class']) ? $this->submit['class'] : 'btn btn-success').'">'.$this->submit['label'].'</button>' : '');
				$form .= ($this->reset['enabled'] ? '<button type="reset" id="'.$form_id.'-reset" class="'.(!empty($this->reset['class']) ? $this->reset['class'] : 'btn').'">'.$this->reset['label'].'</button>' : '');
				$form .= '</div>';
			}
		}
			
		$form .= '</fieldset></form>';
		
		//scripts
		if (count($this->rules) > 0){
			
			$rules = "onkeyup:false,errorClass:'help-inline',validClass:'text-success',rules:{";
			foreach($this->rules as $rule => $data){
				$rules .= $rule.':{';
				foreach($data as $parameter){
					$rules .= $parameter['rule'].':'.str_replace('{form_id}',$form_id,$parameter['value']).',';
				}
				$rules .= '},';
			}
			$rules .= '},highlight:function(element,errorClass){$(element).parent().parent().addClass(\'error\');},unhighlight:function(element,errorClass){$(element).parent().parent().removeClass(\'error\');},errorElement:"span",submitHandler:function(form){console.log(form)}';
			
			$form .= '<script>$(document).ready(function(){$("#';
			$form .= $form_id;
			$form .= '").validate({';
			$form .= $rules;
			$form .= '});';
			
			//put reset button script
			if ($this->reset['enabled']){
				$form .= "$('#".$form_id."-reset').bind('click', function(){ $('#".$form_id."').validate().resetForm(); $('#".$form_id." .control-group.error').removeClass(\"error\")})";
			}
			
			//lets enqueue all the other scripts here...
			if(!empty($this->enqueued_scripts)){
				foreach ($this->enqueues_scripts as $script){
					$form .= $script;
				}
			}
			$form .= '});</script>';
		}
		
		return $form;
	}
	
	/**
	 * This function will concatinate all the atrributes
	 * @param Array or String $attributes
	 * @return String
	 */
	private function extract_attributes($attributes){
		if (is_array($attributes)){
			$string = '';
			foreach($attributes as $key => $value){
				switch($key){
					case 'class':
						$concat = ' ';
						break;
					case 'style':
						$concat = ';';
						break;
					default:
						$concat = ',';
						break;
				}
				$value = (is_array($value)) ? implode($concat,$value) : $value;
				$string .= $key.'="'.$value.'" ';
			}
			$attributes = trim($string);
		}
		return (!empty($attributes) ? ' '.$attributes : '');
	}
}

interface Form_lib_Interface_bak {
	public function content($return=TRUE);
}

$script = "
$(document).bind('load', function(){
	
		});";