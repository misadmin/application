<h2 class="heading">Courses with Laboratory Fees</h2>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">
<form id="change_sy" method="POST">
					<select name="academic_year_id" id="academic_year_id">
						<?php
							foreach($school_years AS $school_year) {
						?>
							<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->sy); ?> </option>	
						<?php 
							}
						?>					
					</select>
								
</form>						
</div>

<h4><?php print($school_yr); ?></h4>
<?php 
	if ($lab_courses) {
?>
<div style="width:60%;">
	<table class="table table-bordered table-striped table-hover" >
		<thead>
			<tr style="background-color: #EAEAEA; font-weight: bold;">
				<td style="width:20%; text-align:center;">
					Course Code
				</td>
				<td style="width:65%; text-align:center;">
					Description
				</td>
				<td style="width:15%; text-align:center;">
					Rate
				</td>	
				</tr>
		</thead>
		<?php
			foreach($lab_courses AS $course) {
		?>
		<tr>	
			<td style="padding-left: 30px;">
				<?php 
					print($course->course_code);
				?>
			</td>
			<td style="text-align: left;">
				<?php 
					print($course->descriptive_title);
				?>
			</td>
			<td style="text-align: right;">
				<?php 
					print($course->rate);
				?>
			</td>
		</tr>
	<?php 
			}
	?>
		</table>
	</div>
<?php 
	}
?>
	

	
<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 3,
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>

