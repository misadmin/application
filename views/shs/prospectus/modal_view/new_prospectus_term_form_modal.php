
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Strand</div>
			<div class="col2a">
				<input type="text" class="form-control" id="description_strand" style="width:380px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Grade Level</div>
			<div class="col2a">
				<select class="form-control" style="width:150px; height:auto;" id="grade_level_modal" >
					<option value="11">Grade 11</option>
					<option value="12">Grade 12</option>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Term</div>
			<div class="col2a">
				<select class="form-control" style="width:150px; height:auto;" id="term_level_modal" >
					<option value="1">1st Semester</option>
					<option value="2">2nd Semester</option>
					<option value="3">Summer</option>
				</select>
			</div>
		</div>
	</div>
