<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; margin-bottom:10px;">
 <h2 class="heading">Students With Privileges</h2>
  <table align="left" cellpadding="2" cellspacing="0" style="width:35%; border:solid; border-width:1px; border-color:#060; margin-top:10px; margin-left:20px;">
	<tr>
       	<td width="199">Academic Term </td>
       	<td width="15">:</td>
        <td width="438"><?php print($academic_term->term." ".$academic_term->sy); ?></td>
    </tr>
	<tr>
       	<td width="199">Privilege </td>
       	<td width="15">:</td>
        <td width="438"><?php print($privilege->scholarship) ?></td>
    </tr>
  </table>
  <br />
  <br />
  <br />
</div>

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; margin-top:50px;">
  <table width="75%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:60%; padding:0px; margin:20px;">
    <thead>
      <tr>
        <td width="15%" align="center">ID No</td>
        <td width="30%" align="center">Name</td>
        <td width="30%" align="center">Course/Year</td>
        <td width="15%" align="center">Discount Percentage</td>
        <td width="15%" align="center">Discount</td>
      </tr>
    </thead>
    <?php
		if ($with_privileges) {
			foreach ($with_privileges AS $privilege) {
	?>
    <tr>
      <?php
	 	  if($privilege->approve_status =='N'){ 	
	   ?>
      <td align="left" valign="top"><?php print("<a class=\"privilege\" href=\"{$privilege->id}\"> {$privilege->students_idno} </a>"); ?></td>
      <?php
	 	}
	  else{
	?>
      <td align="left" valign="top"><?php print($privilege->students_idno); ?></td>
      <?php	
		}
	?>
      <td><?php print($privilege->lname.", ".$privilege->fname); ?></td>
      <td><?php print($privilege->abbreviation."-".$privilege->year_level); ?></td>
      <td><?php print($privilege->discount_percentage); ?></td>
      <td><?php print($privilege->discount_amount); ?></td>
    </tr>
    <?php
			}
		}
    ?>
  </table>
</div>
  
   <form id="srcform2"  method="post">
 	 <input type="hidden" name="step" value="3" />
   </form>
<script>
	$('.privilege').click(function(event){
		event.preventDefault(); 
		var  privilege = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'privilege',
		    value: privilege,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>


