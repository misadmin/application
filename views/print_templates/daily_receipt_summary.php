<?php
$header = "========================================================================================================================================\n";
$header.= "   Status   OR Number            OR Date        Teller ID           ID Number                  Payor               Code           Amount\n";
$header.= "========================================================================================================================================\n";
 
$lines = array();
$contents = "";
$first_page = TRUE;
foreach ($records as $machine_ip => $value){
	if(!$first_page){
		$contents.=$this->print_lib->char_form_feed();
	}
	$first_page = FALSE;
	$contents .= $this->print_lib->bold(). "DAILY RECEIPT SUMMARY" .$this->print_lib->regular()."\n";
	$contents .= $this->print_lib->condensed() . str_pad("Machine IP", 30) . " : " . $machine_ip . "\n";
	$contents .= str_pad("Date", 30) . " : " . $date . "\n";
	$val = $summary[$machine_ip];
	$contents .= str_pad("NON-VAT Receipt Range", 30) . " : "  . (empty($val['non-vat']) ? '' : (implode(', ', range_of_arrays($val['non-vat'])))) . "\n";
	$contents .= str_pad("VAT Receipt Range", 30) . " : " . (empty($val['vat']) ? '' : (implode(', ', range_of_arrays($val['vat'])))) . "\n";
	$contents .= "\n";
	$machine_total = 0;
	foreach($payment_methods[$machine_ip] as $method=>$total) {
		$machine_total+=$total;
		$contents.= str_pad($method, 30) ." : " . str_pad(number_format($total, 2), 30, " ", STR_PAD_LEFT) . "\n";
	}
	$contents .= str_pad('Total', 30) . " : " . str_pad(number_format($machine_total, 2), 30, " ", STR_PAD_LEFT) . "\n";
	$contents .= "\n";
	
	$contents .= str_pad("All Transaction Gross", 30) . " : " . str_pad(number_format((isset($gross_transaction_totals[$machine_ip]) ? $gross_transaction_totals[$machine_ip] : 0), 2), 30, " ", STR_PAD_LEFT) . "\n";
	$contents .= str_pad("All Transaction Voided", 30) . " : " . str_pad(number_format((isset($void_transaction_totals[$machine_ip]) ? $void_transaction_totals[$machine_ip] : 0), 2), 30, " ", STR_PAD_LEFT) . "\n";
	$contents .= str_pad("All Transaction Net", 30) . " : " . str_pad(number_format((isset($net_transaction_totals[$machine_ip]) ? $net_transaction_totals[$machine_ip] : 0), 2), 30, " ", STR_PAD_LEFT) . "\n";
	$contents .= "\n";
	$contents.= $header;
	foreach ($value as $teller_code => $transactions){
		$contents.= $teller_code . " (" . $transactions['name'] . ")\n";
		$total_amount_tc = 0;
		$total_amount_tc_voided = 0;
		foreach($transactions['transactions'] as $transaction){
			$contents.= str_pad($transaction->status=='void' ? ' VOIDED ' : "", 8, " ", STR_PAD_LEFT);
			$contents.= str_pad($transaction->receipt_no, 14, " ", STR_PAD_BOTH);
			$contents.= str_pad($transaction->transaction_date, 16, " ", STR_PAD_BOTH);
			$contents.= str_pad(substr($transaction->teller, 0, 22), 22, " ", STR_PAD_LEFT);
			$contents.= str_pad($transaction->payor_id, 10, " ", STR_PAD_LEFT);
			$contents.= str_pad(substr(trim($transaction->payor), 0, 34), 36, " ", STR_PAD_LEFT);
			$contents.= str_pad(substr(trim($transaction->teller_code), 0, 8), 10, " ", STR_PAD_BOTH);
			$contents.= str_pad(number_format($transaction->whole_amount, 2), 16, " ", STR_PAD_LEFT) . "\n";
			$total_amount_tc+=(float)$transaction->whole_amount;
			$total_amount_tc_voided += $transaction->status=='void' ? $transaction->whole_amount : 0;
		}
		$contents.= str_pad("", 137, "-", STR_PAD_LEFT) . "\n";
		$contents.= str_pad("TOTAL GROSS (" . $transaction->teller_code . ") : ", 111, " ", STR_PAD_LEFT) . str_pad(number_format($total_amount_tc, 2), 24, " ", STR_PAD_LEFT). "\n";
		$contents.= str_pad("TOTAL VOIDED (" . $transaction->teller_code . ") : ", 111, " ", STR_PAD_LEFT) . str_pad(number_format($total_amount_tc_voided, 2), 24, " ", STR_PAD_LEFT). "\n";
		$contents.= str_pad("TOTAL NET (" . $transaction->teller_code . ") : ", 111, " ", STR_PAD_LEFT) . str_pad(number_format(($total_amount_tc - $total_amount_tc_voided), 2), 24, " ", STR_PAD_LEFT). "\n";
	}
	$contents.=$this->print_lib->no_condensed();	
}
?><div id="daily_report_print" style="display:none;"><?php 
echo $this->print_lib->init();
$lines = explode("\n", $contents);
$line_count = 0;
$page_line = 0;
$page = 1;
$max_line_count = 60;
while ($line_count < count($lines)){
	if($page_line < $max_line_count) {
		$page_line++;
	} else {
		$page_line = 0;
		echo $this->print_lib->char_form_feed();
	}
	echo $lines[$line_count] . "\n";	
	$line_count++;	
}
echo $this->print_lib->char_form_feed();
?></div>