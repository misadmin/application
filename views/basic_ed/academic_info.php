<?php 

$levels = array(0 => 'NS',
		2 => 'Kinder',
		3 => 'Grade School',
		4 => 'HS-Day',
		5 => 'HS-Night',
);

$current_and_upcoming_years = array();
if (isset($academic_years)){
	foreach ($academic_years as $academic_year){
		$current_and_upcoming_years[$academic_year->academic_years_id] = $academic_year->sy;
	}
}

$this->form_lib->set_attributes(array('class'=>'form-horizontal', 'method'=>'post'));
$this->form_lib->add_control_class('formSep');
$this->form_lib->enqueue_hidden_input('nonce', $this->common->nonce());

if (isset($student_current_history->enrolled) && $student_current_history->enrolled == 'enrolled') {
	$this->form_lib->enqueue_hidden_input('action', 'update_enrollment'); 
	$this->form_lib->enqueue_hidden_input('history_id', $student_current_history->history_id);
} else 
	$this->form_lib->enqueue_hidden_input('action', 'insert_enrollment');

//echo '<pre>';
//print_r($student_current_history);
//die();

$this->form_lib->enqueue_select_input('academic_year', $current_and_upcoming_years, 'Enroll for');
$this->form_lib->enqueue_select_input('levels', $levels, 'Levels', 'levels','',isset($student_current_history->department_id)?$student_current_history->department_id:'');
$this->form_lib->enqueue_select_input('year_level', array(1=>1), 'Year Level', 'year_level');
$this->form_lib->enqueue_select_input('sections', array(), 'Section', 'sections');
$this->form_lib->set_submit_button('Update Enrollment','btn btn-primary');

?>
<div class="row-fluid">
	<h3 class="heading">Latest Academic Info</h3>
	<div class="vcard clearfix">
		<ul class="info">
			<li><span class="item-key">School Year</span><div class="vcard-item"><?php echo isset($student_current_history->sy)? $student_current_history->sy:''; ?></div></li>
			<li><span class="item-key">Status</span><div class="vcard-item"><?php echo isset($student_current_history->enrolled) ? ucfirst($student_current_history->enrolled): ''; ?></div></li>
			<li><span class="item-key">Department</span><div class="vcard-item"><?php echo isset($student_current_history->department)? $student_current_history->department:''; ?></div></li>	
			<li><span class="item-key">Year Level</span><div class="vcard-item"><?php echo isset($student_current_history->yr_level) ? $student_current_history->yr_level:''; ?></div></li>
			<li><span class="item-key">Section</span><div class="vcard-item"><?php echo isset($student_current_history->section_name) ? $student_current_history->section_name:''; ?></div></li>
			<li><div class="vcard-item"><a href="#academicInfoModal" role="button" class="btn btn-primary" data-toggle="modal">Update Enrollment</a></div></li>
		</ul>
	</div>
</div>
<div id="academicInfoModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="academicInfoModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="academicInfoModalLabel">Enrollment Details</h3>
  </div>
  <div class="modal-body">
  <?php 
    $this->form_lib->content(FALSE);
    ?>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<script>
$(document).ready(function(){
	<?php 
	$levels_id_converted = array(0=>'1',2=>'2',3=>'6',4=>'4',5=>'4')
	?>
	$('#year_level').html(set_options(<?php echo isset($student_current_history->department_id) ? $levels_id_converted[$student_current_history->department_id]:0 ?>));
});

$('#levels').on('change', function(){
	switch ($(this).val()){
		case '0' : $('#year_level').html(set_options(1));
				break;
		case '2' : $('#year_level').html(set_options(2));
				break;
		case '3' : $('#year_level').html(set_options(6));
				break;
		case '4' : $('#year_level').html(set_options(4));
				break;
		case '5' : $('#year_level').html(set_options(4));
				break;
	}
	var level = $('#levels option:selected').val();
	$.ajax({
		url      : "<?php echo site_url('ajax/sections'); ?>",
		type     : 'get',
		dataType : 'json',
		data     : 'year=1&level='+level,
		success  : function( response ) {
			console.log(response);
			var select_content = '';
			if (response==null){
				select_content = "";
			} else {
				response.map(function(value){
					select_content += '<option value="'+value.id+'">'+value.section+'</option>';
				});
			}
			$('#sections').html(select_content);
		},
		error : function (response){
			console.log(response);
		}
	});
});

$('#year_level').on('change', function(){
	var level = $('#levels option:selected').val();
	var year = $('#year_level option:selected').val();
	
	$.ajax({
		url      : "<?php echo site_url('ajax/sections'); ?>",
		type     : 'get',
		dataType : 'json',
		data     : 'year='+year+'&level='+level,
		success  : function( response ) {
			console.log(response);
			var select_content = '';
			response.map(function(value){
				select_content += '<option value="'+value.id+'">'+value.section+'</option>';
			});
			$('#sections').html(select_content);
		},
		error : function (response){
			console.log(response);
		}
	});
});

function set_options(num){
	var i;
	var content;
	for(i=1; i<=num;i++){
		content = content + '<option value="' + i + '">' + i + '</option>';
	}
	return content;	
}
</script>