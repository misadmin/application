<!-- <style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>
 -->
 <div style="float:left; width:auto; margin-top:10px">
<!-- <div style="background:#FFF; width:40%; text-align:center; margin:auto;" align="center"> -->
	<form id="batch_posting_basic_ed_form" method="post" action="" class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="2" />
	<input type="hidden" name="academic_terms_id" value="<?php print($terms_sy->id);?>" />
	<input type="hidden" name="academic_years_id" value="<?php print($terms_sy->academic_years_id);?>" />
	


<div class="formSep">
	<h2>Posting Basic Education</h2>
</div> 


	<div class="control-group formSep">
		<label class="control-label">Academic Year</label>
		<div class="controls">
			<?php print($terms_sy->sy); ?>
		</div>
	</div>
	
	<div class="control-group formSep">
		<label class="control-label">Level</label>
		<div class="controls">
		   <select id="select_level" name="levels" >
				<option value="">--Select Level--</option>
				<?php 
					foreach($levels as $level) { 
				?>
					<option value="<?php print($level->id); ?>" >
								<?php print($level->level); ?></option>
				<?php 
					}
				?>
			</select>
		</div>
	</div>
	
	<div class="my_level" style="display:none;">	 
		<div class="control-group formSep ">
			<label class="control-label">Year Level</label>
		    <div class="controls">
				<div class="preschool" style="display:none;">	
					<select name="yr_level_pre">
						<option value="1">Preschool 1</option>
					</select>
				</div>
		    	<div class="kinder" style="display:none;">	
					<select name="yr_level_k">
						<option value="1">Kindergarten 1</option>
					</select>
		    	</div>
				<div class="gs" style="display:none;">
					<select name="yr_level_gs">
						<option value="1">Grade 1</option>
						<option value="2">Grade 2</option>
						<option value="3">Grade 3</option>
						<option value="4">Grade 4</option>
						<option value="5">Grade 5</option>
						<option value="6">Grade 6</option>
					</select>
				</div>
				<div class="hd" style="display:none;">
					<select name="yr_level_hs">
						<option value="7">Grade 7</option>
						<option value="8">Grade 8</option>
						<option value="9">Grade 9</option>
						<option value="10">Grade 10</option>
					</select>
				</div>
				<div class="sh" style="display:none;">
					<select name="yr_level_sh">
						<option value="11">Grade 11</option>
						<option value="12">Grade 12</option>
					</select>
				</div>
	    	</div>
		</div>   
	 
		<div class="control-group">
			<div class="controls">
				<button class="btn btn-success" type="submit" id="batch_post_basic_ed" >Post!</button>
			</div>
		</div>	
	</div>


</form>
</div>




<!--  

	
	<table align="left" cellpadding="0" cellspacing="0" style="width:70%; margin-top:10px;" class="head1">
  		<tr class="head">
			<td colspan="3" class="head" style="text-align:center; padding:10px;">POSTING BASIC EDUCATION </td>
		</tr>
  		<tr>
    		<td colspan="3">  
  				<table border="0" cellpadding="2" cellspacing="0" class="inside" align="left">
					<tr>
					  <td width="104" align="left" valign="middle">Academic Year </td>
					  <td width="6" align="left" valign="middle">:</td>
					  <td width="240" style="text-align:left; vertical-align:middle;"><?php print($terms_sy->sy); ?></td>
				  </tr>
					<tr>
					  <td align="left" valign="middle">Level</td>
					  <td align="left" valign="middle">:</td>
					  <td style="text-align:left; vertical-align:middle;">
					  <select name="levels_id" id="levels_id">
					  	<?php
							foreach($levels AS $level) {
								print("<option value=\"$level->id\">$level->level</option>");
							}
						?>
                      </select></td>
				  </tr>
					<tr>
					  <td align="left" valign="middle">Year Level </td>
					  <td align="left" valign="middle">:</td>
					  <td style="text-align:left; vertical-align:middle;"><select name="yr_level" id="yr_level">
					    <option value="1" selected="selected">1</option>
					    <option value="2">2</option>
					    <option value="3">3</option>
					    <option value="4">4</option>
					    <option value="5">5</option>
					    <option value="6">6</option>
					    <option value="7">7</option>
					    <option value="8">8</option>
					    <option value="9">9</option>
					    <option value="10">10</option>
					    <option value="11">11</option>
					    <option value="12">12</option>
					</select>
					  </td>
				  </tr>

					<tr align="center"> 
						<td colspan="3" align="right"><input name="batch_post_button" type="submit" id="batch_post_basic_ed" value="Post!" class="btn btn-success"></td>
					</tr>
    			</table>
			</td>
 		</tr>
 	</table>
 </form>
 </div>

-->

<script>
$('#batch_post_basic_ed').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to POST assessments of all students enrolled in this level?");

	if (confirmed){
		$('#batch_posting_basic_ed_form').submit();
	}		
});
</script>

 <script>
	 $(document).ready(function (e) {
    	$('#select_level').change(function () {
        	if ($(this).val() == '') {
            	$('.my_level').hide();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.sh').hide();            	
			} 

        	if ($(this).val() == '11') {
            	$('.my_level').show();
        		$('.preschool').show();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.sh').hide();            	
			} 

    		if ($(this).val() == '1') {
            	$('.my_level').show();
    			$('.preschool').hide();
    			$('.kinder').show();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.sh').hide();            	
			} 

        	if ($(this).val() == '3') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').show();
            	$('.hd').hide();
            	$('.sh').hide();            	
			} 

        	if ($(this).val() == '4') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').show();
            	$('.sh').hide();            	
			} 

        	if ($(this).val() == '12') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.sh').show();
			}

    	});
	});
 </script>


  
