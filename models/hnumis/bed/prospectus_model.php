<?php

	class Prospectus_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

		//private $prospectus_courses_id = null;

		public function ListProspectuses($colleges_id=14) {
				
			$result=null; 
			
			$q1 = "SELECT 
							a.id,
							b.abbreviation,
							b.description, 
							IF(b.status='O','Offered','Frozen') as program_status,
							a.effective_year, 
							IF(a.status='A','Active','Inactive') as prospectus_status,
							a.status,
							c.group_name AS level,
							e.year_level,
							c.id AS acad_program_groups_id,
							b.id AS academic_programs_id,
							d.id AS levels_id
						FROM 
							prospectus AS a,
							academic_programs AS b,
							acad_program_groups AS c,
							levels AS d,
							bed_academic_programs AS e 
						WHERE 
							a.academic_programs_id=b.id 
							AND c.id = b.acad_program_groups_id 
							AND d.id = c.levels_id 
							AND b.id = e.academic_programs_id 
							AND b.colleges_id= {$this->db->escape($colleges_id)}
						ORDER BY 
							d.rank, 
							b.abbreviation, 
							a.effective_year DESC ";
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();			
			}
				
			return $result;
		}


		function AddProspectus($data) {
				
			$query = "INSERT INTO
							prospectus (academic_programs_id, effective_year, inserted_by)
						VALUES (
								{$this->db->escape($data['academic_programs_id'])},
								{$this->db->escape($data['effective_year'])},
								{$this->db->escape($data['inserted_by'])})";
			
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
	    function DeleteProspectus($prospectus_id) {
			
			$q = "DELETE FROM
						prospectus 
					WHERE
						id = {$this->db->escape($prospectus_id)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


		function UpdateProspectus($data){

			$q1 = "UPDATE prospectus
					SET effective_year = {$this->db->escape($data['effective_year'])},
						status = {$this->db->escape($data['status'])}
		 			WHERE
						id = {$this->db->escape($data['id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}


		public function UpdateCourseType($data){

			$q1 = "UPDATE 
							bed_type_prospectus_courses
						SET 
							subject_types_id = {$this->db->escape($data['subject_types_id'])}
						WHERE
							prospectus_courses_id = {$this->db->escape($data['prospectus_courses_id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

		
		function ListProspectusTerms_Courses($prospectus_id) {

			$result = null;
				
			$q1 = "SELECT 
							a.id, 
							a.year_level, 
							a.term AS pros_term, 
							a.max_credit_units, 
							a.max_bracket_units,
							b.effective_year AS term,
							c.description AS grade_level
						FROM 
							prospectus_terms AS a,
							prospectus AS b,
							academic_programs AS c 
						WHERE 
							a.prospectus_id = b.id 
							AND b.academic_programs_id = c.id 
							AND a.prospectus_id={$this->db->escape($prospectus_id)}
						ORDER BY 
							a.year_level, 
							a.term " ;
		//die($q1);
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
							
				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$myresult = $this->ListProspectusCourses($res->id);
						
						if ($myresult) {
							$result[$cnt]->courses = $myresult;
						}									
						$cnt++;
					}
				}	
			}
				
			return $result;
			
		}



		function AddProspectusTerm($data) {
				
			$query = "INSERT INTO
							prospectus_terms (prospectus_id,year_level,term,inserted_by)
						VALUES (
								{$this->db->escape($data['prospectus_id'])},
								{$this->db->escape($data['year_level'])},
								{$this->db->escape($data['term'])},
								{$this->db->escape($data['inserted_by'])})";
			//return $query;
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


		public function getYrLevelByProspectusID($prospectus_id=null) {
			$result=null; 
			
			$q1 = "SELECT 
							a.id,
							b.abbreviation,
							b.description,
							a.academic_programs_id
						FROM 
							prospectus AS a,
							academic_programs AS b
						WHERE 
							a.academic_programs_id=b.id
							AND a.id = {$this->db->escape($prospectus_id)} ";
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
			
		}


		function AddProspectusTermCourse($data) {
				
			$query = "INSERT INTO
							prospectus_courses (prospectus_terms_id,courses_id,inserted_by)
						VALUES (
								{$this->db->escape($data['prospectus_terms_id'])},
								{$this->db->escape($data['courses_id'])},
								{$this->db->escape($data['inserted_by'])})";
			//return $query;
			if ($this->db->query($query)) {
				$data['prospectus_courses_id'] = $this->db->insert_id();
				
				if ($this->AddTypeProspectusCourse($data)) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		}


	    function DeleteProspectusTermCourse($prospectus_course_id) {
			
			$q = "DELETE FROM
						prospectus_courses 
					WHERE
						id = {$this->db->escape($prospectus_course_id)} ";
			//return $q;
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


	    function DeleteProspectusTerm($prospectus_term_id) {
			
			$q = "DELETE FROM
						prospectus_terms
					WHERE
						id = {$this->db->escape($prospectus_term_id)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


		public function getNextProspectus($year_level=NULL, $acad_program_groups_id=NULL) {
			
			$result=null; 
			
			$q1 = "SELECT 
							a.id,
							c.year_level,
							d.levels_id
						FROM 
							prospectus AS a,
							academic_programs AS b,
							bed_academic_programs AS c,
							acad_program_groups AS d 
						WHERE 
							a.academic_programs_id = b.id 
							AND b.id = c.academic_programs_id 
							AND a.status = 'A' 
							AND d.id = b.acad_program_groups_id 
							AND b.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)} 
							AND c.year_level = {$this->db->escape($year_level)} 
						ORDER BY 
							a.id DESC 
						LIMIT 1 ";
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
			
		}

		
/***********************************	
*	PRIVATE FUNCTIONS
************************************/

		private function ListProspectusCourses($prospectus_terms_id) {
						
			$result = null;
				
			$q1 = "SELECT 
						d.id, 
						d.courses_id,
						e.course_code, 
						e.descriptive_title,
						IF(d.is_bracketed = 'Y',CONCAT('(',e.credit_units,')'),e.credit_units) AS credit_units,
						d.cutoff_grade,
						d.num_retakes,
						d.elective,
						d.is_bracketed,
						d.is_major,
						a.description AS type_description,
						b.subject_types_id
					FROM 
						bed_subject_types AS a,
						bed_type_prospectus_courses AS b,
						prospectus_courses AS d, 
						courses AS e
					WHERE 
						a.id=b.subject_types_id
						AND b.prospectus_courses_id=d.id
						AND d.courses_id=e.id 
						AND d.prospectus_terms_id={$this->db->escape($prospectus_terms_id)} 
					ORDER BY
						a.id, 
						d.is_bracketed DESC, 
						e.course_code " ;
		//die($q1);
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
		
		
		private function AddTypeProspectusCourse($data) {
			$query = "INSERT INTO
								bed_type_prospectus_courses (
											subject_types_id,
											prospectus_courses_id,
											encoded_by
										)
								VALUES (
											{$this->db->escape($data['subject_types_id'])},
											{$this->db->escape($data['prospectus_courses_id'])},
											{$this->db->escape($data['inserted_by'])} )";
			//return $query;
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		}

		
	}
	
?>
