
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Grade/Section</div>
			<div class="col2a">
				<input type="text" class="form-control" id="description_update_modal" style="width:210px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Strand</div>
			<div class="col2a">
				<input type="text" class="form-control" id="strand_update_modal" style="width:380px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Assign Adviser</div>
			<div class="col2a">
				<select id="adviser_id_modal" class="form-control" >
					<option value="NULL">None</option>
					<?php
						if ($advisers) {
							foreach($advisers AS $adviser) {
					?>
								<option value="<?php print($adviser->empno); ?>"><?php print($adviser->neym); ?></option>
					<?php 
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>
