	<div id="maincontainer" class="clearfix container-fluid">
		<div id="datacontainer" class="row-fluid">
			<div class="<?php if(empty($sidebar)) echo "span12"; else echo "span9";?> search_page">
<?php if (!empty($message)):	?>
				<div class="row-fluid">
					<div class="alert <?php echo $severity; ?>">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo urldecode($message); ?>
					</div>
				</div>
<?php endif; ?>
<?php if(isset($content)): ?>
<script>
	var sTS = <?php echo microtime(TRUE) //Server Time Stamp ?>;
	var cRef = new Date; <?php //client Reference time ?>
	var sMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	$().ready(function(){
		var ck = document.createElement("p");
		ck.id = 'lil-stat';
		var dPos = $('#datacontainer').position();
		ck.style.top = dPos['top'];
		ck.innerHTML = '<?php echo $this->status_schedule; ?> &middot; <span id="datetime" data-toggle="tooltip" title="Server Date Time"></span>';
		$('#datacontainer').before(ck);
		updateClock();
		setInterval(function() {
			updateClock();
		}, 1000);
	})
	function updateClock(){
		var c = new Date((sTS +  (new Date - cRef) / 1000)*1000); <?php //server timestamp + client elapsed time?>
		var year = c.getFullYear();
		var month = c.getMonth();
		var date = c.getDate();
		var hour = c.getHours()%12;
	    var min = c.getMinutes();
	    var sec = c.getSeconds();
	    var am = c.getHours()/12;
	    $('#datetime').text(sMonth[month]+' '+format(date)+', '+year+' \267 '+format(hour)+':'+format(min)+':'+format(sec)+' '+(am > 1 ? 'PM' : 'AM'));
	}
	function format(num){
		return num < 10 ? '0'+num : num;
	}
</script>
<?php echo $content ?>
<?php endif ?>
			</div>
<?php if( ! empty($sidebar)) : ?>

<!-- modified: removed: style="padding-top:41px", ra 8/17/13 -->
	<div id="sidebar" class="span3">
<?php echo $sidebar; ?>
			</div>
<?php endif; ?>
		</div>
	</div>