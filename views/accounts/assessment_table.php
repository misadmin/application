<?php

$assessment_sample = array(	
		'Tuition Basic' => array('Tuition Basic'=>552.44),
		'Matriculation Fees' => array(
				'Matriculation Fee'=>275.26
		),
		'Miscellaneous Fees' => array(
				'Athletic'=>194.16,
				'Audio Visual'=>81.36,
				'Dental'=>82.19
		),
		'Other School Fees' => array(
				'Energy Fee'=>275.00,
				'Internet Fee'=>474.55
		)
);

$assessment = isset($assessment) ? $assessment : $assessment_sample;

//print_r($assessment);

?>
<style>
td.money{
	text-align: right;
}
</style>
<div class="span9">
<table class="table">
	<tbody>
		<tr>
			<td colspan=3>Tuition Fees: (@ <?php echo number_format($assessment['Tuition Basic']['Tuition Basic'],2); ?>)</td>
		</tr>
		<tr>
			<td colspan=3>Misc. Fees and Other Fees:</td>
		</tr>
		<?php
			$subtotal = array();
			foreach($assessment as $key=>$val):
				if($key != "Tuition Basic"):
		?>
		<tr>
			<td colspan=3>&nbsp;&nbsp;<?php echo $key;?></td>
		</tr>
				<?php
					$subtotal[$key] = 0;
					foreach($val as $desc=>$rate):
				?>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $desc; ?></td>
			<td class="money"><?php echo number_format($rate,2); ?></td>
			<td></td>
		</tr>
				<?php
					$subtotal[$key] += $rate;
					endforeach;
				?>
		<tr>
			<td></td>
			<td></td>
			<td class="money"><?php echo number_format($subtotal[$key],2); ?></td>
		</tr>
		<?php
				endif;
			endforeach;
		?>
		<tr>
			<td>TOTAL Assessment</td>
			<td></td>
			<td class="money">
			<?php
				$total = 0;
				foreach ($subtotal as $val){
					$total += $val;
				}
				echo number_format($total,2);
			?>
			</td>
		</tr>
	</tbody>
</table>
</div>
		