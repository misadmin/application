		
			<div id="show_strand_grade" style="font-weight:bold; font-size:18px; color:#087A04;"></div>
			<div id="show_descriptive_title" style="font-weight:bold; font-size:24px; color:#02186F; margin-top:8px; font-style:italic;"></div>
			<div id="show_block_adviser" style="font-weight:bold; font-size:15px; color:#404040; margin-top:8px; margin-bottom:10px; "> </div>
			<div id="show_academic_terms_id"> </div>
			<div id="what_period"> </div>
			<?php 
				if ($midterm_confirm) {
			?>		
				<div style="font-weight:bold; font-size:14px; color:#A50505;">
					The Class Adviser confirmed the Midterm Grades on: <?php print($midterm_confirm_date); ?>
				</div>
			<?php 
				}
			?>
			<?php 
				if ($finals_confirm) {
			?>		
				<div style="font-weight:bold; font-size:14px; color:#A50505;">
					The Class Adviser confirmed the Finals Grades on: <?php print($finals_confirm_date); ?>
				</div>
			<?php 
				}
			?>
			<div style="width:60%; overflow:auto; margin-top:20px;">
				<form id="submit_grades_form" method="POST" action="<?php print(site_url($this->uri->segment(1).'/shs_class_schedules'));?>" >
					<input type="hidden" name="action" value="submit_grades" />					
					<?php $this->common->hidden_input_nonce(FALSE); ?>
					<input type="hidden" name="course_offerings_id" 
						value="<?php if ($students) print($students[0]->course_offerings_id); ?>" />	
					<table class="table table-hover table-bordered table-striped table-condensed" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold; height:50px;" class="shs_header" >
								<td class="shs_header" style="width:10%;">Count</td>
								<td class="shs_header" style="width:14%;">ID No.</td>
								<td class="shs_header" style="width:50%;">Name</td>
								<td class="shs_header" style="width:13%;">
									<?php 
										if ($midterm_confirm) {
									?>		
											Midterm<br>Grade
									<?php 
										} else {
									?>
											<a href="#" class="show_midterm">Midterm<br>Grade</a>									
									<?php 
										}
									?>
								</td>
								<td class="shs_header" style="width:13%;">
									<?php 
										if ($finals_confirm) {
									?>		
											Finals<br>Grade
									<?php 
										} else {
									?>
											<a href="#" class="show_finals">Finals<br>Grade</a>									
									<?php 
										}
									?>
								</td>
							</tr>
						</thead>
						<tbody>
							<?php
								$cnt=0;

								if ($students) {
									foreach($students  AS $student) {
							?>			
										<tr>
											<td style="text-align:right;"><?php print($cnt+1); ?>.</td>
											<td style="text-align:center;">
												<a href="<?php print(site_url($this->uri->segment(1).'/student')."/".$student->idno);?>" >
													<?php print($student->idno); ?>
												</a>													
											</td>
											<td><?php print($student->name); ?></td>
											<td style="text-align:center;">
												<?php 
													if ($midterm_confirm) {
												?>
														<input type="text" 
															style="width:25px; height:17px; font-size:14px; text-align:center; font-weight:bold;" 
															value="<?php print($student->midterm_grade); ?>" disabled />
												<?php 
													} else {
												?>
														<input type="text" name="midterm[<?php print($student->enrollments_id); ?>]" 
															style="width:25px; height:17px; font-size:14px; display:none; text-align:center; font-weight:bold;" 
															class="midterm_box" 
															id="midterm_<?php print($cnt); ?>"
															my_array="midterm[]"
															tabindex="<?php print($cnt+1); ?>"
															value="<?php print($student->midterm_grade); ?>"
														/>												
												<?php 
													}
												?>
											</td>
											<td style="text-align:center;">
												<?php 
													if ($finals_confirm) {
												?>
														<input type="text" 
															style="width:25px; height:17px; font-size:14px; text-align:center; font-weight:bold;" 
															value="<?php print($student->finals_grade); ?>" disabled />
												<?php 
													} else {
												?>
														<input type="text" name="finals[<?php print($student->enrollments_id); ?>]" 
															style="width:25px; height:17px; font-size:14px; display:none; text-align:center; font-weight:bold;" 
															class="finals_box" 
															id="finals_<?php print($cnt); ?>"
															my_array="finals[]"
															tabindex="<?php print($cnt+1); ?>"
															value="<?php print($student->finals_grade); ?>"
														/>
												<?php 
													}
												?>
											</td>
										</tr>
							<?php
										$cnt++;
									}
								}
							?>
						</tbody>
					</table>
				</form>	
			</div>

			<?php
				if ((!$midterm_confirm OR !$finals_confirm) AND $students) {
			?>		
					<div style="width:60%; margin-top:10px; font-weight:bold; color:#C90303; font-size:14px; ">
							NOTE: Blank grades are NOT allowed. Grades can no longer be updated after Class Adviser confirms them!
					</div>
				
					<div style="width:60%; margin-top:10px; text-align:right;">
						<button class="btn btn-success submit_grades" 
							count="<?php print($cnt); ?>" >Submit Grades!</button>
					</div>
			<?php 
				}
			?>
			
<script>
	$('.show_midterm').click(function(e){
		e.preventDefault();	

		$('.midterm_box').show();
		$('.finals_box').hide();
		$('#what_period').val('Midterm');

		$("input[my_array='midterm[]']").each(function() {

			if (!$(this).val()) {
				$(this).focus();
				return false;
			}

		});
	});	

	$('.show_finals').click(function(e){
		e.preventDefault();	

		$('.midterm_box').hide();
		$('.finals_box').show();
		$('#what_period').val('Finals');

		$("input[my_array='finals[]']").each(function() {

			if (!$(this).val()) {
				$(this).focus();
				return false;
			}

		});
		
	});	
	
	
</script>


<script>
	$(".midterm_box").blur(function(){
		
		var midterm_grade = $(this).val();

		if (parseFloat(midterm_grade) > 100 || parseFloat(midterm_grade) < 60) {
			alert("Acceptable Grades from 60-100 only!");
			$(this).val('');
			$(this).focus();
			return false;
		} else if (isNaN(midterm_grade)) { //is not a number
			alert("String values are not accepted!");
			$(this).val('');
			$(this).focus();
			return false;
		} 
			
	});

	$(".finals_box").blur(function(){
	
		var finals_grade = $(this).val();

		if (parseFloat(finals_grade) > 100 || parseFloat(finals_grade) < 60) {
			alert("Acceptable Grades from 60-100 only!");
			$(this).val('');
			$(this).focus();
			return false;
		} else if (isNaN(finals_grade)) { //is not a number
			alert("String values are not accepted!");
			$(this).val('');
			$(this).focus();
			return false;
		} 
			
		
	});

</script>


<script>
	$('.submit_grades').click(function(e){
		e.preventDefault();	

		var period = $('#what_period').val();
		var academic_terms_id = $('#show_academic_terms_id').val();
		var cnt = $(this).attr('count');
		
		if (!period) {
			alert("Click Midterm or Finals to input grades!");
			return false;
		}

		switch (period) {
			
			case 'Midterm':
				for (i=0; i<cnt; i++) {
					
					var my_val = $('#midterm_'+i).val();

					if (!my_val) {
						alert("Blank grades not allowed!");
						$('#midterm_'+i).focus();
						return false;
					}
				}	
			
				break;
				
			case 'Finals':
				for (i=0; i<cnt; i++) {
					
					var my_val = $('#finals_'+i).val();

					if (!my_val) {
						alert("Blank grades not allowed!");
						$('#finals_'+i).focus();
						return false;
					}
				}	
			
				break;
				
			
		}
		
		
		var confirmed = confirm('Continue to submit your '+period+' grades?');

		if (confirmed){
			
			$('<input>').attr({
				type: 'hidden',
				name: 'period',
				value: period,
			}).appendTo('#submit_grades_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_terms_id',
				value: academic_terms_id,
			}).appendTo('#submit_grades_form');
			
			$('#submit_grades_form').submit();
			
		}
		
	});	
	
</script>
