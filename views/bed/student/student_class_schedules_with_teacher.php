

			<?php 
			//print($this->session->userdata['role']); die();
				if (isset($block_section_data) AND ($this->session->userdata['role'] == 'student')) {
			?>
					<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px; margin-bottom:5px;">
						<div style="font-size:25px; font-weight:bold; margin-bottom:10px; color:#030B3D;">
							<?php print("Grade ".$block_section_data->grade_level." - ".$block_section_data->section_name); ?>
						</div>
						<div style="font-size:20px; font-weight:bold; font-style:italic; margin-bottom:5px; color:#025E05;">
							<?php print($block_section_data->strand); ?>
						</div>
						<div style="font-size:14px; font-weight:bold;">
							<div style="float:left; width:120px; clear:both;">
								Class Adviser
							</div>
							<div style="float:left; width:15px;">
								:
							</div>
							<div style="float:left;" class="show_class_adviser">
								<?php print($block_section_data->class_adviser); ?>
							</div>
							<div style="float:left; width:120px; clear:both;">
								Classroom
							</div>
							<div style="float:left; width:15px;">
								:
							</div>
							<div style="float:left;">
								<?php print($block_section_data->room_no); ?>
							</div>
						</div>
					</div>
			<?php 
				}
			?>

				<div>	
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:15%; height:30px;">Time</td>
								<td class="shs_header" style="width:17%; height:30px;">Monday</td>
								<td class="shs_header" style="width:17%; height:30px;">Tuesday</td>
								<td class="shs_header" style="width:17%; height:30px;">Wednesday</td>
								<td class="shs_header" style="width:17%; height:30px;">Thursday</td>
								<td class="shs_header" style="width:17%; height:30px;">Friday</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($class_schedules) {
									foreach($class_schedules AS $class_sched) {
							?>			
										<tr>
											<td style="text-align:center; vertical-align: middle;"><?php print($class_sched->time_sched); ?></td>
											<td><?php 
													print($class_sched->mon);
													print("<div style='font-weight:bold; color:#AE0303; font-style:italic;'>".$class_sched->mon_teacher."</div>"); 
												?>
											</td>
											<td><?php 
													print($class_sched->tue); 
													print("<div style='font-weight:bold; color:#AE0303; font-style:italic;'>".$class_sched->tue_teacher."</div>"); 
												?>
											</td>
											<td><?php 
													print($class_sched->wed); 
													print("<div style='font-weight:bold; color:#AE0303; font-style:italic;'>".$class_sched->wed_teacher."</div>"); 
												?>
											</td>
											<td><?php 
													print($class_sched->thu); 
													print("<div style='font-weight:bold; color:#AE0303; font-style:italic;'>".$class_sched->thu_teacher."</div>"); 
												?>
											</td>
											<td><?php 
													print($class_sched->fri); 
													print("<div style='font-weight:bold; color:#AE0303; font-style:italic;'>".$class_sched->fri_teacher."</div>"); 
												?>
											</td>
										</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
				</div>
