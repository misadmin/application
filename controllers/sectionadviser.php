<?php 

	class Sectionadviser extends MY_Controller {
			
		public function __construct(){
			parent::__construct();		
			$this->content_lib->set_title ('Section Adviser | ' . $this->config->item('application_title'));
			$this->content_lib->enqueue_footer_script('validate');
			$this->content_lib->enqueue_footer_script('autocomplete');
			$this->content_lib->enqueue_footer_script('jquery-ui');
			
			if ($this->session->userdata('role')=='sectionadviser'){
					$this->navbar_data['menu'] = array(
													'Students'=>'student',					
													'My Section'=>'my_section'
												);
			}
			
			$this->content_lib->set_navbar_content('', $this->navbar_data);
			
		}
		

		public function student(){
				
			$this->content_lib->set_title('Section Adviser | Student | ' . $this->config->item('application_title'));
			$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
			$query = $this->input->post('q');
			$idnum = $this->uri->segment(3);

			if ( ! empty($query)){
				
				//A search query occurs... so lets query the student_common_model
				$page = $this->input->post('page') ? $this->input->post('page') : 1;
				$this->load->model('student_common_model');
				$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
				$total = $this->student_common_model->total_search_results($query);
				
				$this->load->library('pagination_lib');
				
				if ($results !== FALSE) {
					$pagination = $this->pagination_lib->pagination('', $total, $page);
					
					$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
					$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
							? $total
							: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
					);
					
					//A result or several results were found
					if (count($results) > 1){
						//when several results are found... lets show the search result page...
						$res=array();
						foreach ($results as $result){
							$port = substr($result->idno, 0, 3);
							if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
								$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
							else { 
								if ($result->gender=='F')
									$image = base_url($this->config->item('no_image_placeholder_female')); else
									$image = base_url($this->config->item('no_image_placeholder_male'));
							}
							$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
						}
						$data = array(
								'start'		=>$start, 
								'end'		=>$end, 
								'total'		=>$total, 
								'pagination'=>$pagination, 
								'results'	=>$res, 
								'query'		=>$query
							);
						$this->content_lib->enqueue_body_content ('common/search_result', $data);
					} else {
						redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
					}
				}
			}
					
			if (is_numeric($idnum)){

				$this->load->model('student_common_model');
				$this->load->model('Academic_terms_model');
				$this->load->helper('student_helper');

				$this->load->model('hnumis/student_model');
				//$this->load->model('hnumis/shs/shs_grades_model');
				$this->load->model('hnumis/bed/section_adviser_model');
				//$this->load->model('hnumis/shs/behavior_model');
				//$this->load->model('hnumis/shs/attendance_model');

				
				$this->content_lib->enqueue_header_style('image_area_select');
				$this->content_lib->enqueue_footer_script('image_area_select');
				$port = substr($idnum, 0, 3);
				$this->content_lib->set_title ('Section Adviser | ' . $idnum);
				
				$result = $this->student_common_model->my_information($idnum);	//print_r($result); die();
				
				if ($result !== FALSE) {
					//a user with that id number is seen...
					$check_student = $this->section_adviser_model->CheckIfMyStudent($this->session->userdata('empno'), $idnum);
					
					if ($check_student) {

						$dcontent = array(
								'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
								'idnum'		=> $idnum,
								'name'		=> $result->fname . " " . $result->mname. " " . $result->lname,
								'course'	=> $result->abbreviation,
								'college_id'=> $result->colleges_id,
								'college'	=> $result->college_code,
								'level'		=> $result->section_name,
								'full_home_address'	=> $result->full_home_address,
								'full_city_address' => $result->full_city_address,
								'phone_number' 		=> $result->phone_number,
								'section'			=> $result->section,
								'bed_status'		=> $result->bed_status,
						);

						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
						else {
							if ($result->gender == 'F')
								$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
								$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
						}					

						$dcontent['signature'] = base_url('assets/img/signature.png');
						
						$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
					
						if ($this->student_model->student_is_basic_ed($idnum)) {
							
							$data['idnum']      	= $idnum;
							//$data['term']       	= $this->academic_terms_model->getCurrentAcademicTerm(); 
							$data['academic_terms'] = $this->academic_terms_model->student_inclusive_academic_terms($idnum);
							if(count($data['academic_terms'])==1){
								$data['academic_terms'][] = $data['academic_terms'][0];
							}

							$current_history        = $this->shs_student_model->get_StudentHistory_id($idnum, $data['academic_terms'][0]->id); 

							$data['active_tab']      = "class_schedules";
							$data['active_tab1']     = NULL;				

							$data['class_schedules'] = $this->shs_student_model->ListClassSchedules($current_history->id); 					
							
							$data['grades']            = $this->shs_grades_model->List_Student_Grades($idnum); 
							$data['selected_term']     = $data['academic_terms'][0]->id;						

							//$data['grades']            = $this->shs_grades_model->List_MyGrades($idnum, $data['term']->id); 
							//$data['can_update_remark'] = TRUE; //used to check if can update remark for Report Card
							
							$data['show_pulldown']   = TRUE;
							
							$data['student_histories_id'] = $current_history->id;
							
							$data['observed_values'] = $this->behavior_model->ListObservedValues($current_history->id);
							
							$data['can_update_values'] = TRUE; //used to check if can update behavior values
							
							$data['school_days'] = $this->attendance_model->ListSchoolDays($data['academic_terms'][0]->id);
							
							$data['present_days'] = $this->attendance_model->ListPresentDays($data['academic_terms'][0]->id, $current_history->id);

							$this->content_lib->enqueue_body_content('shs/class_adviser/student_management', $data);
												
						} else {
							$this->content_lib->set_message('Student records not in Basic Education!', 'alert-error');						
						}		
					} else {
						$this->content_lib->set_message('Student does not belong to any of your classes!', 'alert-error');												
					}
						
				} else {
					//Has a Numeric ID Number but is not found in database...
					//todo: Create a view to be placed here...
					$this->content_lib->set_message('Student does not exist', 'alert-error');
				}

			}
					
			$this->content_lib->content();	
		}

		
		public function my_section() {
			
			$this->load->library('bed/section_lib');
		
			$this->section_lib->class_adviser_section();
		}
		
		//NOTE: this function handles all actions for the student tabs
		public function process_student_action($idnum) {
			
			if (!$this->input->post('action')) {
				redirect('classadviser/student', 'refresh');
			}
			
			$this->load->library('shs/shs_student_lib');
		
			$this->shs_student_lib->process_student_action($idnum);
			
		}

	}

?>