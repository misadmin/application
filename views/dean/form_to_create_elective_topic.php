<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<body >

 <div style="background:#FFF; width:40%; text-align:center; margin:auto;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head" style="text-align:left; padding:10px;">
  CREATE ELECTIVE COURSE </td>
  </tr>
  <tr>
    <td colspan="3">
<form action="<?php echo site_url('dean/assign_elective_topic');?>" method="post" name="prereq">
<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="4" />
  
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
<tr>
  <td colspan="3" align="left" valign="middle" style="font-weight:bold; font-size:14px;">
  <?php 
  		print($prospectus->description." - ".$prospectus->effective_year); 
  		print("<br>".$courses->course_code." - ".$courses->descriptive_title); 
  ?></td>
  </tr>
<tr>
  <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="course">
    <tr>
      <td width="19%" align="left">Course Topic </td>
      <td width="2%" align="left">:</td>
      <td width="79%" align="left"><select name="topic_courses_id" id="topic_courses_id" class="form">
        <?php
				foreach($college_courses AS $course) {
					print("<option value=".$course->id.">".$course->course_code."</option>");	
				}
			?>
      </select></td>
    </tr>
  </table>
    </td>
  </tr>
<tr>
  <td width="97">&nbsp;</td>
  <td width="3">&nbsp;</td>
  <td width="401" align="left"><input type="submit" name="button" id="button" value="Save Topic!"  class="btn btn-success" /></td>
</tr>
    </table>
</form>
    
<div style="width:95%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-hover" style="margin:10px;">
	    <thead>
  <tr>
    <th width="79%" class="header">Topic</th>
    <th width="9%" class="header">Create 
      Prereq</th>
    <th width="12%" class="header">Delete</th>
  </tr>
	</thead>
	<?php
		if ($topics) {
			foreach ($topics AS $topic) {
	
	?>
  <tr>
    <td><?php print($topic->course_code." - ".$topic->descriptive_title); ?></td>
    <td style="text-align:center;">
	<a class="detail_prereq" href="<?php print($topic->id); ?>" ><i class="icon-edit"></i></a>		</td>
    <td style="text-align:center; vertical-align:middle;">
      <a class="delete_topic" href="<?php print($topic->id); ?>" my_id="<?php print($topic->id); ?>">
        <!-- <img src="<?php print(base_url('assets/img/icon-recycle.gif')); ?>" width="16" height="16" align="absmiddle" style="vertical-align:middle; border:none" />  -->
        <i class="icon-trash"></i></a></td>
  </tr>
	<?php
			}
		}	
	?>
  <thead>
  <tr>
    <th colspan="3" class="header" style="text-align:left">
 	<form action="<?php echo site_url('dean/assign_elective_topic');?>" method="post" name="prereq">
	<input type="hidden" name="step" value="2" />
    <input type="submit" name="button" id="button" value="Back to List of Electives!"  class="btn btn-success" />
    </form></th>
    </tr>
  </thead>
</table>
</div>
    </td>
    </tr>
    </table>
    </div>

</body>

  <form id="delete_topic2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="step" value="5" />
</form>

<script>
	$('.delete_topic').click(function(event){
		event.preventDefault(); 
		var topic_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'topic_id',
		    value: topic_id,
		}).appendTo('#delete_topic2');
		$('#delete_topic2').submit();
	});
</script>

  <form id="detail_prereq2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="step" value="6" />
</form>

<script>
	$('.detail_prereq').click(function(event){
		event.preventDefault(); 
		var topic_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'topic_id',
		    value: topic_id,
		}).appendTo('#detail_prereq2');
		$('#detail_prereq2').submit();
	});
</script>

