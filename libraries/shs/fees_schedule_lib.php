<?php

	class fees_schedule_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		
		/*
		 * @added: 11/2/2016
		 * @author: genes
		 */
		
		public function create_fees_schedules_shs() {
		
			$this->CI->load->model('accounts/fees_schedule_model');
			$this->CI->load->model('hnumis/shs/track_strand_model');
			$this->CI->load->model('hnumis/Academicyears_model');
			$this->CI->load->model('hnumis/Programs_model');
			$this->CI->load->model('academic_terms_model');
			$this->CI->load->helper('url');

			$school_years = $this->CI->fees_schedule_model->get_school_years();
			$fees_groups = $this->CI->fees_schedule_model->get_feesgroups();

			if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){

				$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');

				switch ($action) {
					case 'mainpage':

						$data = array(
									"fees_groups"=>$fees_groups,
									"school_years"=>$school_years,
									"fees_subgroups"=>$this->CI->fees_schedule_model->ListFeesSubGroups($fees_groups[0]->id),
									"tracks"=>$this->CI->track_strand_model->ListTracks(),
									"terms"=>$this->CI->academic_terms_model->ListAcademicTerms($school_years[0]->id),
							);

						$this->CI->content_lib->enqueue_body_content('shs/fees_schedule/fees_schedule_shs_form',$data);
						$this->CI->content_lib->content();

						break;
						
					case 'change_academic_year':

						$data = array(
									"fees_groups"=>$fees_groups,
									"school_years"=>$school_years,
									"fees_subgroups"=>$this->CI->fees_schedule_model->ListFeesSubGroups($fees_groups[0]->id),
									"tracks"=>$this->CI->track_strand_model->ListTracks(),
									"terms"=>$this->CI->academic_terms_model->ListAcademicTerms($this->CI->input->post('academic_year_id')),
						);

						$my_return['output'] = $this->CI->load->view('shs/fees_schedule/list_terms', $data, TRUE);
						
						echo json_encode($my_return,JSON_HEX_APOS);
					
						return;
					
					case 'review_fees':
					
						$selected_terms  = $this->CI->academic_terms_model->ListTerms_ByArray(implode(',', $this->CI->input->post('semester')));

						$selected_tracks = $this->CI->track_strand_model->ListTracksByArray(implode(',', $this->CI->input->post('tracks')));
						
						$data = array(
									"academic_year"=>$selected_terms[0]->sy,
									"terms"=>$selected_terms,
									"fee_category"=>$this->CI->input->post('fees_group_id'),
									"fees_subgroup"=>$this->CI->fees_schedule_model->getFeesSubGroup($this->CI->input->post('fees_subgroups')),
									"tracks"=>$selected_tracks,
									"grade_levels"=>$this->CI->input->post('grade_level'),
									"rate"=>$this->CI->input->post('rate'),
									);
									
						$this->CI->content_lib->enqueue_body_content('shs/fees_schedule/review_fees_schedule_shs_form',$data);
						$this->CI->content_lib->content();

						break;
						
					case 'save_fees':

						$acad_program_groups = json_decode($this->CI->input->post('tracks'));
						$academic_terms      = json_decode($this->CI->input->post('terms'));
						$year_levels         = json_decode($this->CI->input->post('grade_levels'));
						$errors              = array();
						
						foreach($acad_program_groups AS $acad_program_group) {
							foreach($academic_terms AS $academic_term) {
								foreach($year_levels AS $k=>$yr_level) {
									$data['fees_subgroups_id']      = $this->CI->input->post('fees_subgroups_id');
									$data['acad_program_groups_id'] = $acad_program_group->acad_program_groups_id;
									$data['academic_terms_id']      = $academic_term->id;
									$data['levels_id']              = 12;
									$data['yr_level']               = $yr_level;
									$data['rate']                   = $this->CI->input->post('rate');
									$data['posted']                 = "Y";

									$status =  $this->CI->fees_schedule_model->AddFeesSchedule($data);
									if ($status['error']) {
										$errors[] = $status;
									}
								}
							}
						}

						if ($errors) {
							$msg = "Fees Schedule successfully created! Duplicate fees are found on: ";
							foreach ($errors AS $k=>$v) {
								foreach ($v AS $k1=>$v1) {
									$msg .= $k1."==".$v1."<br>";
								}
							}
						} else {
							$msg = "Fees Schedule successfully created!";
						}


						$data = array(
									"fees_groups"=>$fees_groups,
									"school_years"=>$school_years,
									"fees_subgroups"=>$this->CI->fees_schedule_model->ListFeesSubGroups($fees_groups[0]->id),
									"tracks"=>$this->CI->track_strand_model->ListTracks(),
									"terms"=>$this->CI->academic_terms_model->ListAcademicTerms($school_years[0]->id),
							);

						$this->CI->content_lib->set_message($msg, 'alert-success');
						$this->CI->content_lib->enqueue_body_content('shs/fees_schedule/fees_schedule_shs_form',$data);
						$this->CI->content_lib->content();

						break;
						
					case 'search_fees_subgroups':
						
						$data['fees_subgroups'] = $this->CI->fees_schedule_model->ListFeesSubGroups($this->CI->input->post('fees_groups_id'));

						$my_return['output'] = $this->CI->load->view('shs/fees_schedule/list_fees_subgroups', $data, TRUE);
						
						echo json_encode($my_return,JSON_HEX_APOS);
					
						return;
					
				}

			} else {
				$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
				$this->CI->index();
			}
		}



		public function view_update_schedule_for_shs() {
			$this->CI->load->model('hnumis/shs/shs_assessments_model');
			$this->CI->load->model('hnumis/shs/shs_fees_schedule_model');
			$this->CI->load->model('hnumis/Programs_model');
			$this->CI->load->model('academic_terms_model');
			$this->CI->load->model('hnumis/Academicyears_model');
			//$this->CI->load->model('teller/Assessment_model');
			$this->CI->load->model('accounts/fees_schedule_model');

			$can_update=FALSE;
		
			$current_term  = $this->CI->academic_terms_model->getCurrentAcademicTerm(); //print_r($current_term); die();
			$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);

			$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 'mainpage');
			
			/* only accounts role can edit/delete fees */
			if ($this->CI->session->userdata('role') == 'accounts') { 
				$can_update=TRUE;
			}		

			switch ($step) {
				case 'mainpage':

					$strands   = $this->CI->shs_fees_schedule_model->ListAcadPrograms_with_fees($current_term->id); //print_r($strands); die();

					if ($this->CI->shs_assessments_model->CheckTermProgram_HasAssessment($current_term->id,$strands[0]->id)) {
						$can_update=FALSE;
					}
					
					$data['academic_terms_id']      = $current_term->id;				

					$description   = "";
					$program_items = "";
					$group_name    = "";

					if ($strands) {
						$selected_strand    			= $strands[0]->id;
						$description        			= $strands[0]->description;
						$group_name         			= $strands[0]->group_name;				
						$lab_items         				= $this->CI->shs_fees_schedule_model->ListLaboratoryItems($current_term->sy_id, $strands[0]->id); //print_r($lab_items); die();
    					$data['acad_program_groups_id'] = $strands[0]->acad_program_groups_id;
					} else {
						$lab_items = NULL;
						$data['acad_program_groups_id'] = NULL;
					}

					$schedule_items = $this->CI->shs_fees_schedule_model->ListFeesSchedule_FeesGroups($data);

					$data = array("my_program"=>array('track_name'=>$group_name,
													'description'=>$description,
													'sy_semester'=>$current_term->term." ".$current_term->sy,
													),
									"strands"=>$strands,
									"terms"=>$acad_terms,
									"current_term"=>$current_term->id,
									"schedule_items"=>$schedule_items,
									"can_update"=>$can_update,
									"lab_items"=>$lab_items,
							);
						
					break;
		
				case "change_pulldown": //pulldown menu is clicked
					
					$strands   = $this->CI->shs_fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
					$lab_items = $this->CI->shs_fees_schedule_model->ListLaboratoryItems($this->CI->input->post('academic_years_id'), $this->CI->input->post('academic_programs_id'));

					$data['academic_terms_id']      = $this->CI->input->post('academic_terms_id');
					$data['acad_program_groups_id'] = $this->CI->input->post('acad_program_groups_id');

					if ($this->CI->shs_assessments_model->CheckTermProgram_HasAssessment($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'))) {
						$can_update=FALSE;
					} else {
						$can_update = TRUE;
					}

					$schedule_items = $this->CI->shs_fees_schedule_model->ListFeesSchedule_FeesGroups($data);

					if ($strands) {
						$selected_strand    = $strands[0]->id;
						$description        = $strands[0]->description;
						$group_name         = $strands[0]->group_name;				
					}

					$data = array("my_program"=>array('track_name'=>$this->CI->input->post('group_name'),
													'description'=>$this->CI->input->post('description'),
													'sy_semester'=>$this->CI->input->post('sy'),
													),
									"strands"=>$strands,
									"terms"=>$acad_terms,
									"current_term"=>$this->CI->input->post('academic_terms_id'),
									"schedule_items"=>$schedule_items,
									"can_update"=>$can_update,
									"lab_items"=>$lab_items,
					);
					
					$my_return['output'] = $this->CI->load->view('shs/fees_schedule/fees_schedule_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'delete_fee_schedule': //delete schedule fee
					
					if ($this->CI->fees_schedule_model->DeleteFeesSchedule($this->CI->input->post('fees_schedule_id'))) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;						
					}	
										
					$strands   = $this->CI->shs_fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
					$lab_items = $this->CI->shs_fees_schedule_model->ListLaboratoryItems($this->CI->input->post('academic_years_id'), $this->CI->input->post('academic_programs_id'));

					$data['academic_terms_id']      = $this->CI->input->post('academic_terms_id');
					$data['acad_program_groups_id'] = $this->CI->input->post('acad_program_groups_id');

					$schedule_items = $this->CI->shs_fees_schedule_model->ListFeesSchedule_FeesGroups($data);

					if ($this->CI->shs_assessments_model->CheckTermProgram_HasAssessment($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'))) {
						$can_update=FALSE;
					} else {
						$can_update = TRUE;
					}

					$data = array("my_program"=>array('track_name'=>$this->CI->input->post('group_name'),
													'description'=>$this->CI->input->post('description'),
													'sy_semester'=>$this->CI->input->post('sy'),
													),
									"strands"=>$strands,
									"schedule_items"=>$schedule_items,
									"can_update"=>$can_update,
									"lab_items"=>$lab_items,
					);
					
					$my_return['output'] = $this->CI->load->view('shs/fees_schedule/fees_schedule_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'update_fee_schedule': 
					
					$data['rate']             = $this->CI->input->post('rate');
					$data['fees_schedule_id'] = $this->CI->input->post('fees_schedule_id');				
					
					if ($this->CI->fees_schedule_model->UpdateFeesSchedule($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;						
					}	
										
					$strands   = $this->CI->shs_fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
					$lab_items = $this->CI->shs_fees_schedule_model->ListLaboratoryItems($this->CI->input->post('academic_years_id'), $this->CI->input->post('academic_programs_id'));

					$data['academic_terms_id']      = $this->CI->input->post('academic_terms_id');
					$data['acad_program_groups_id'] = $this->CI->input->post('acad_program_groups_id');

					$schedule_items = $this->CI->shs_fees_schedule_model->ListFeesSchedule_FeesGroups($data);

					if ($this->CI->shs_assessments_model->CheckTermProgram_HasAssessment($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'))) {
						$can_update=FALSE;
					} else {
						$can_update = TRUE;
					}

					$data = array("my_program"=>array('track_name'=>$this->CI->input->post('group_name'),
													'description'=>$this->CI->input->post('description'),
													'sy_semester'=>$this->CI->input->post('sy'),
													),
									"strands"=>$strands,
									"schedule_items"=>$schedule_items,
									"can_update"=>$can_update,
									"lab_items"=>$lab_items,
					);
					
					$my_return['output'] = $this->CI->load->view('shs/fees_schedule/fees_schedule_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'delete_lab_fee': //delete laboratory fee

					if ($this->CI->fees_schedule_model->DeleteLaboratoryFees($this->CI->input->post('lab_fee_id'))) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;						
					}	
										
					$strands   = $this->CI->shs_fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
					$lab_items = $this->CI->shs_fees_schedule_model->ListLaboratoryItems($this->CI->input->post('academic_years_id'), $this->CI->input->post('academic_programs_id'));

					$data['academic_terms_id']      = $this->CI->input->post('academic_terms_id');
					$data['acad_program_groups_id'] = $this->CI->input->post('acad_program_groups_id');

					$schedule_items = $this->CI->shs_fees_schedule_model->ListFeesSchedule_FeesGroups($data);

					if ($this->CI->shs_assessments_model->CheckTermProgram_HasAssessment($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'))) {
						$can_update=FALSE;
					} else {
						$can_update = TRUE;
					}

					$data = array("my_program"=>array('track_name'=>$this->CI->input->post('group_name'),
													'description'=>$this->CI->input->post('description'),
													'sy_semester'=>$this->CI->input->post('sy'),
													),
									"strands"=>$strands,
									"schedule_items"=>$schedule_items,
									"can_update"=>$can_update,
									"lab_items"=>$lab_items,
					);
					
					$my_return['output'] = $this->CI->load->view('shs/fees_schedule/fees_schedule_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_lab_fee': //edit Laboratory Fee
					
					$data['rate'] = $this->CI->input->post('rate');
					$data['laboratory_fees_id'] = $this->CI->input->post('lab_fee_id');				
					
					if ($this->CI->fees_schedule_model->UpdateLaboratoryFees($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;						
					}	
										
					$strands   = $this->CI->shs_fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
					$lab_items = $this->CI->shs_fees_schedule_model->ListLaboratoryItems($this->CI->input->post('academic_years_id'), $this->CI->input->post('academic_programs_id'));

					$data['academic_terms_id']      = $this->CI->input->post('academic_terms_id');
					$data['acad_program_groups_id'] = $this->CI->input->post('acad_program_groups_id');

					$schedule_items = $this->CI->shs_fees_schedule_model->ListFeesSchedule_FeesGroups($data);

					if ($this->CI->shs_assessments_model->CheckTermProgram_HasAssessment($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'))) {
						$can_update=FALSE;
					} else {
						$can_update = TRUE;
					}

					$data = array("my_program"=>array('track_name'=>$this->CI->input->post('group_name'),
													'description'=>$this->CI->input->post('description'),
													'sy_semester'=>$this->CI->input->post('sy'),
													),
									"strands"=>$strands,
									"schedule_items"=>$schedule_items,
									"can_update"=>$can_update,
									"lab_items"=>$lab_items,
					);
					
					$my_return['output'] = $this->CI->load->view('shs/fees_schedule/fees_schedule_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
										
			}
		
			$this->CI->content_lib->enqueue_body_content('shs/fees_schedule/schedule_by_shs',$data);
			$this->CI->content_lib->content();
		
		
		}



		public function lab_fees_shs(){
			$this->CI->load->model('hnumis/shs/shs_fees_schedule_model');
			$this->CI->load->model('hnumis/Academicyears_model');
			$this->CI->load->model('hnumis/courses_model');
			$this->CI->load->helper('url');

			$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 1);

			switch ($step) {
				case 1:

					$school_years = $this->CI->shs_fees_schedule_model->get_school_years();

					if ($this->CI->input->post('academic_year_id')) {
						$data = array(
								"school_years"=>$school_years,
								"laboratories"=>$this->CI->shs_fees_schedule_model->ListLab_with_no_fees($this->CI->input->post('academic_year_id')),
								"selected_year"=>$this->CI->input->post('academic_year_id'),
						);

					} else {
						$data = array(
								"school_years"=>$school_years,
								"laboratories"=>$this->CI->shs_fees_schedule_model->ListLab_with_no_fees($school_years[0]->id),
								"selected_year"=>$school_years[0]->id,
						);
					}

					$this->CI->content_lib->enqueue_body_content('accounts/fees_schedule/laboratory_fees_form',$data);
					$this->CI->content_lib->content();

					break;
				case 2:
					$selected_labs="";
					foreach($this->CI->input->post('laboratory') AS $k=>$v) {
						if ($v) {
							$lab = $this->CI->courses_model->getCourseDetails($k);
							$selected_labs[] = array('id'=>$lab->id,'course_code'=>$lab->course_code,'rate'=>$v);
						}
					}
					$data = array(
							"academic_year"=>$this->CI->Academicyears_model->GetAcademicYear($this->CI->input->post('academic_year_id')),
							"laboratories"=>$selected_labs,
					);
					$this->CI->content_lib->enqueue_body_content('accounts/fees_schedule/review_laboratory_fees_form',$data);
					$this->CI->content_lib->content();

					break;
				case 3:
					$laboratories = json_decode($this->CI->input->post('laboratories'));
					foreach ($laboratories AS $lab) {
						$data['academic_years_id'] = $this->CI->input->post('academic_year');
						$data['levels_id']         = 12;
						$data['courses_id']        = $lab->id;
						$data['rate']              = $lab->rate;
						$data['posted']            = "Y";
						$this->CI->shs_fees_schedule_model->AddLaboratory_Fees($data);
						//FIXME: No error trapping here. what if insertion will fail? this will still display "successfully created" message.
					}

					$this->CI->content_lib->set_message("Laboratory Fees successfully created!", 'success');
					$this->CI->index();

					break;
			}
		}
		

		
	}
	
?>

