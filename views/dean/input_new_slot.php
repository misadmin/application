<h2 class="heading">Offer Schedule Slot</h2>
<?php
$hrs = array('1:00'=>'1 Hour','1:30'=>'1 Hour, 30 Mins','2:00'=>'2 Hours','2:30'=>'2 Hours, 30 Mins','3:00'=>'3 Hours');
$time = array();
$t1 = mktime(7,30,0,0,0,0);
$tym1 = date('h:i A',$t1);
$tym  = date('H:i:s',$t1);
$cnt = 0;
while ($cnt < (30*25)) {
	$time[] = array('id'=>$tym,'value'=>$tym1);
	$cnt = $cnt + 30;
	$t1 = mktime(7,30+$cnt,0,0,0,0);
	$tym1 = date('h:i A',$t1);
	$tym  = date('H:i:s',$t1);
}

$this->form_lib->set_id('add-sched-slot');
$this->form_lib->set_attributes(array('method'=>'post','class'=>'form-horizontal'));
$this->form_lib->add_control_class('formSep');
$this->form_lib->enqueue_hidden_input('nonce',$this->common->nonce());
$this->form_lib->enqueue_hidden_input('step','6');
$this->form_lib->enqueue_text_data('Academic Terms',$academic_terms->term." ".$academic_terms->sy);
$this->form_lib->enqueue_text_data('Course [Section]',$offerings->course_code."[".$offerings->section_code."]");
$this->form_lib->enqueue_text_data('Schedule',$ids = implode(', ',array_map(function($item) { return $item->tym." ".$item->days_day_code." [".$item->room_no."]"; }, $offering_slot[0])));
$this->form_lib->enqueue_select_input2('start_time','Time','{id}','{value}',$time,'','','span2');
$this->form_lib->enqueue_select_input('num_hr',$hrs,'Number of Hours','','span2');
$this->form_lib->enqueue_select_input2('day_code','Day','{day_code}','{day_code}',$days,'','','span2');
$this->form_lib->set_submit_button('Continue');
$this->form_lib->content(FALSE);
