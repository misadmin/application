<style>
tr.deleted {color: #837D7D; font-style: italic;}
</style>


<?php
	if (!($student_inclusive_terms)) {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td style="text-align:center; font-weight:bold;">STUDENT HAS NO ASSESSMENT YET!</td>
  </tr>
</table>
<?php
	} 
?>

<?php
	$assessment_sample = array(
			'Tuition Basic' => array('Tuition Basic'=>552.44),
			'Matriculation Fees' => array(
					'Matriculation Fee'=>275.26
			),
			'Miscellaneous Fees' => array(
					'Athletic'=>194.16,
					'Audio Visual'=>81.36,
					'Dental'=>82.19
			),
			'Other School Fees' => array(
					'Energy Fee'=>275.00,
					'Internet Fee'=>474.55
			)
	);
	
	$assessment = isset($assessment) ? $assessment : $assessment_sample;
	$cost_per_unit = isset($assessment['Tuition Basic']['Tuition Basic']) ? $assessment['Tuition Basic']['Tuition Basic'] : 0 ;
	$total_tuition = 0;
	$total_lab_fees = 0;
	$total_a_fee=0;
	
?>
<?php if(($student_inclusive_terms)){ ?>
<div class="span6">
	<?php if(isset($current_term)): ?>
	<h3 class="heading"><strong>Assessment for <?php echo $current_term; ?></strong></h3>

	<?php elseif(isset($student_inclusive_terms)): ?>
	<!-- Added by Toyet 12.06.2017
		 as requested by Sir Charlie:Accounts -->
	<?php
		echo '  <a href="#jumpto"><button class="btn btn-small btn-primary"> Jump To Print Buttons </button></a>';
	?>
				<form id="history_form" method="post">
					<?php $this->common->hidden_input_nonce(FALSE); ?>
					<?php action_form("change_term", FALSE); ?>
					<select id="history_id" name="history_id">
						<?php foreach($student_inclusive_terms as $term): ?>
						<option <?php echo $selected_history_id == $term->student_histories_id ? "selected" : ""; ?> value="<?php echo $term->student_histories_id; ?>|<?php echo $term->id; ?>"><?php echo $term->term . " " . $term->sy; ?></option>
						<?php endforeach; ?>
					</select>
				</form>

	<?php endif; ?>

	<h4 class="heading">Tuition Fees (@ P <?php echo number_format($cost_per_unit, 2); ?>/unit)</h4>
	<?php if (is_array($courses) && !empty($courses)): $total_pay_units = 0; $total_units = 0;?>
		<table class="table table-hover" width="100%">
			<thead>
				<tr>
					<th width="40%" style="text-align:center; vertical-align:middle">Course</th>
					<th width="10%" style="text-align:center; vertical-align:middle">Load Units</th>
					<th width="10%" style="text-align:center; vertical-align:middle">Pay Units</th>
					<th width="20%" style="text-align:right; vertical-align:middle">Total</th>
					<th width="20%"></th>
				</tr>
			</thead>
			<tbody>
	<?php 
	    ////log_message("INFO", print_r($courses,true)); // Toyet 5.30.2018
		foreach ($courses as $course):
				$total_tuition += ($course['pay_units'] * (isset($other_courses_payments[$course['id']]) && array_key_exists($course['id'], $other_courses_payments) ? $other_courses_payments[$course['id']] : $cost_per_unit));
				$total_pay_units += $course['pay_units'];
				$total_units += $course['units'];
				
	            ////log_message("INFO", print_r($course['enrollments_history_id'],true)); // Toyet 5.30.2018
				if (!$course['enrollments_history_id'] or $course['enrollments_history_id']=='dissolved') {  
					////log_message("INFO", 'inside =>'.print_r($course['enrollments_history_id'],true)); // Toyet 5.30.2018
	?>
						<tr class = "<?php echo (!empty($course['enrollments_history_id'])  ? 'deleted' : ''); ?>">
							<td><?php echo$course['name']; ?></td>
							<td style="text-align: center"><?php echo $course['units'] != 0 ? number_format($course['units'], 1) : '' ; ?></td>
							<td style="text-align: center"><?php echo $course['pay_units'] != 0 ? number_format($course['pay_units'], 2) : ''; ?></td>
							<td style="text-align: right; font-weight: bold;">
								<?php
									$pay_units = (isset($other_courses_payments[$course['id']]) && array_key_exists($course['id'], $other_courses_payments) ? $other_courses_payments[$course['id']] : $cost_per_unit) * $course['pay_units'];
									echo $pay_units !=0 ? number_format($pay_units,2) : '';
								?>
							</td>
							<td style="text-align: right;">
							<?php 
								if ($course['re_enrollments_id']) {
									print("<span style='font-style:italic; color: #FF0000;'>Re-enrolled</span>");
								} else {
									if ($course['withdrawn_on']) {
										print("<span style='font-style:italic; color: #FF0000;'>Withdrawn</span>");
									} else if ($course['post_status'] == 'added') {
											print("<span style='font-style:italic; color: #FF0000;'>Added</span>");
									} else if (!empty($course['enrollments_history_id'])){
											echo "<span style='font-style:italic; color: #837D7D;'>". UCFirst($course['post_status']) . "</span>";
									} else if ($course['post_status'] == 'dissolved'){
											echo "<span style='font-style:italic; color: #837D7D;'>". "Dissolved" . "</span>";
									}else {
										if ($course['assessment_id']) {
											print("<span style='font-style:italic; color: #FF0000;'>Posted</span>");
										} else {
											print("<span style='font-style:italic; color: #FF0000;'>Unposted</span>");
										}
									}
								}
							?>
							</td>
						</tr> 

<?php   
			}else{

				if ($assessment_date < $course['transaction_date'] ) {
?>
				

						<tr class = "<?php echo (!empty($course['enrollments_history_id'])  ? 'deleted' : ''); ?>">
							<td><?php echo $course['name']; ?></td>
							<td style="text-align: center"><?php echo $course['units'] != 0 ? number_format($course['units'], 1) : '' ; ?></td>
							<td style="text-align: center"><?php echo $course['pay_units'] != 0 ? number_format($course['pay_units'], 2) : ''; ?></td>
							<td style="text-align: right; font-weight: bold;">
								<?php
									$pay_units = (isset($other_courses_payments[$course['id']]) && array_key_exists($course['id'], $other_courses_payments) ? $other_courses_payments[$course['id']] : $cost_per_unit) * $course['pay_units'];
									echo $pay_units !=0 ? number_format($pay_units,2) : '';
								?>
							</td>
							<td style="text-align: right;">
							<?php 
								if ($course['re_enrollments_id']) {
									print("<span style='font-style:italic; color: #FF0000;'>Re-enrolled</span>");
								} else {
									if ($course['withdrawn_on']) {
										print("<span style='font-style:italic; color: #FF0000;'>Withdrawn</span>");
									} else if ($course['post_status'] == 'added') {
											print("<span style='font-style:italic; color: #FF0000;'>Added</span>");
									} else if (!empty($course['enrollments_history_id'])){
											echo "<span style='font-style:italic; color: #837D7D;'>". UCFirst($course['post_status']) . "</span>";
									}else {
										if ($course['assessment_id']) {
											print("<span style='font-style:italic; color: #FF0000;'>Posted</span>");
										} else {
											print("<span style='font-style:italic; color: #FF0000;'>Unposted</span>");
										}
									}
								}
							?>
							</td>
						</tr> 


<?php 			}
			}

			endforeach; 
?>
			</tbody>
			<tfoot>
				<tr style="font-weight: bold;">
					<td style="text-align: right">&nbsp;</td>
					<td style="text-align: center;border-top:1px solid #08843E;"><?php echo number_format($total_units, 2); ?></td>
					<td style="text-align: center;border-top:1px solid #08843E;"><?php echo number_format($total_pay_units, 2); ?></td>
					<td style="border-top:1px solid #08843E;">&nbsp;</td>
					<td style="text-align: right;">P <?php echo number_format($total_tuition, 2); ?></td>
				</tr>
			</tfoot>
		</table>
	<?php endif;?>
		<h4 class="heading" style="margin-top:20px">Miscellaneous and Other Fees:</h4>
		
		<?php 
			$misc_subtotal = array();
			if (!empty($assessment) && count($assessment) > 0) {
			foreach($assessment as $key=>$val):
				if($key != "Tuition Basic"):
		?>
		<table width="100%">
			<thead>
				<tr>
					<th width="5%">&nbsp;</th>
					<th width="55%" style="text-align: center">&nbsp;</th>
					<th width="20%" style="text-align: right">&nbsp;</th>
					<th width="20%"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="4" style="font-weight:bold"><?php echo $key; ?></td>
				</tr>
				<?php
					$misc_subtotal[$key] = 0;
					foreach($val as $desc=>$rate):
				?>
				<tr>
					<td>&nbsp;</td>
					<td><?php echo $desc; ?></td>
					<td style="text-align:right"><?php echo $rate; ?></td>
					<td>&nbsp;</td>
				</tr>
				<?php
					$misc_subtotal[$key] += $rate;
					endforeach;
				?>
				<tr>
					<td>&nbsp;</td>
					<td></td>
					<td></td>
					<td style="text-align:right"><?php echo "P ".number_format($misc_subtotal[$key], 2); ?></td>
				</tr>
			</tbody>
		</table>
		<?php
				endif;
			endforeach;
			}
		?>
		<?php 
			if ($affiliated_fees) {
		?>
			<table style="width:100%;">
				<tr>
					<td colspan="3" style="font-weight:bold;">Other Additional School Fees</td>
				</tr>
				<?php 
					foreach($affiliated_fees AS $a_fee) {
				?>
				<tr>
					<td style="width:5%;">&nbsp;</td>
					<td style="width:55%;"><?php print($a_fee->description); ?></td>
					<td style="text-align:right; width:20%;"><?php print(number_format($a_fee->rate,2)); ?></td>
					<td style="width:20%;">&nbsp;</td>					
				</tr>
				<?php 
						$total_a_fee = $total_a_fee + $a_fee->rate;
					}
				?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td style="text-align:right;"><?php print(number_format($total_a_fee,2)); ?></td>					
				</tr>
			</table>
			<br>
	<?php 
			}
	?>	
	<?php if (is_array($lab_courses) && count($lab_courses) > 0): ?>
		<h4>Laboratory Fees</h4>	
		<table width="100%"> 
			<thead>
				<tr>
					<th width="5%">&nbsp;</th>
					<th width="55%">&nbsp;</th>
					<th width="20%" style="text-align: center">&nbsp;</th>
					<th width="20%" style="text-align: right">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach ($lab_courses as $lab_course): $total_lab_fees += $lab_course['amount'] ;
				if ($lab_course['amount']) {
	?>
				<tr>
					<td>&nbsp</td>
					<td><?php echo $lab_course['name']; ?></td>
					<td style="text-align: right"><?php echo number_format($lab_course['amount'], 2); ?></td>
					<td>&nbsp</td>
				</tr>
	<?php 
				}
	endforeach; ?>			
			</tbody>
			<tfoot>
				<tr>
					<td>&nbsp</td>
					<td>&nbsp</td>
					<td>&nbsp</td>
					<td style="text-align: right" > <?php echo "P " . number_format($total_lab_fees, 2); ?></td>
				</tr>
			</tfoot>
		</table>
	<?php endif;?>	
	<hr>
		<table width="100%"> 
			<thead>
				<tr>
					<th width="60%" style="text-align: left"><h4>TOTAL Assessment:</h4></th>
					<th width="20%" ">&nbsp;</th>
					<th width="20%" style="text-align: right">
					<?php
						$total_misc = 0;
						foreach ($misc_subtotal as $subtotal){
							$total_misc += $subtotal;
						}
						$total_assessment = $total_tuition + $total_misc + $total_lab_fees + $total_a_fee;
						echo "P " . number_format($total_assessment, 2);
					?>
				
					</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
</div>

<script>
	$(document).ready(function(){
		$('#history_id').bind('change', function(){
			$('#history_form').submit();
		});
	});
</script>
<?php
//print_r($ledger_data); die(); 
//$selected
if(isset($ledger_data)){
	foreach($ledger_data as $item){
		//if($item['transaction_detail']=='COLLEGE REGISTRATION' &&)
	}
}
if(in_array($this->session->userdata('role'), $this->config->item('roles_allowed_to_print_assessment'))):
foreach($student_inclusive_terms as $val){
	if($selected_history_id == ''){
		$term = $val->term;
		$sy = $val->sy;
		break;
	}
	if($selected_history_id == $val->student_histories_id){
		$term = $val->term;
		$sy = $val->sy;
	}		
}

$tuition_fees_content[0] = array(
		'fee_type'=>'TUITION ' . $term . ' SY ' . $sy . ' @ ' . number_format($cost_per_unit, 2),
);
//print_r($courses); die();
if (is_array($courses) && !empty($courses)){
	foreach ($courses as $course){// $total_tuition += ($course['pay_units'] * $cost_per_unit); $total_pay_units += $course['pay_units']; $total_units += $course['units'];
			if (!$course['withdrawn_on']) {
				$tuition_fees_content[count($tuition_fees_content)] = array(
						'subject' => $course['name'],
						'units' => $course['units'],
						'hours' => $course['pay_units'],
						'amount' => (isset($other_courses_payments[$course['id']]) && array_key_exists($course['id'], $other_courses_payments) ? $other_courses_payments[$course['id']] : $cost_per_unit) * $course['pay_units'],
				);
			}
	}
}

if (!empty($assessment) && count($assessment) > 0) {
	foreach($assessment as $key=>$val){
		if($key != "Tuition Basic"){
			$miscellaneous_fees_content[] = array(
				'fee_type'=>$key,
				'amount'=>$misc_subtotal[$key]
			);
		}
	}
}

if (is_array($lab_courses) && count($lab_courses) > 0){
	$miscellaneous_fees_content[] = array(
		'fee_type'=>'Laboratory Fees',
		'amount'=>$total_lab_fees
	);
}


//added by Tatskie on Jun 30, 2015
if ($affiliated_fees)
{
	$affiliated_total = 0;
	foreach ($affiliated_fees as $affiliated_fee) {
			$affiliated_total += $affiliated_fee->rate;
	}
	$miscellaneous_fees_content[] = array('fee_type'=>'Other Additional School Fees','amount'=>$affiliated_total);
}


//$this->load->library('balances_lib');
//print_r($tuition_fees_content); die();
if( $current_academic_term_obj->term == 'Summer'){

	// $payable_midterm = $this->balances_lib->due_this_period('midterm', TRUE);
	$payable_midterm = $this->balances_lib->due_this_period('Midterm',$total_assessment, $current_academic_term_obj->term);

} else {

	// $payable_prelim = $this->balances_lib->due_this_period('prelim', TRUE);;
	$payable_prelim = $this->balances_lib->due_this_period('Prelim',$total_assessment, $current_academic_term_obj->term);  //edited Toyet 11.23.2017
	//$payable_prelim = $this->balances_lib->due_this_period();

}

//added Toyet 11.23.2017
//  attempt to extract balance before assessment
$temp = $this->balances_lib->current_term_transactions();
////log_message('INFO',"printing.... BAL B4 ASSESS  ==>".print_r($temp,true));
if( $temp[0]->transaction_detail = 'FORWARDED BALANCE>>>>' ){
	//$balance_before_assessment = $temp[0]->debit-$temp[0]->credit;
	$balance_before_assessment = $this->balances_lib->balanceBeforeAssessment();
}
$balance_before_assessment = round($balance_before_assessment,2);

////log_message("INFO", print_r($ledger_data, true));  //Toyet 7.5.2018
////log_message('INFO',"printing.... BAL B4 ASSESS  ==>".print_r($balance_before_assessment,true));

// $payable_prelim = $this->balances_lib->due_this_period(TRUE, TRUE); 
//$trans_after_assess = $this->balances_lib->transAfterAssessment();  //add Toyet 7.10.2018
////log_message('INFO',"trans after assess ==>".print_r($trans_after_assess,true));
$current_period = $this->balances_lib->current_period();
////log_message('INFO',"current period ==>".print_r($current_period,true));
$debitAdj_AfterAssess = $this->balances_lib->total_current_debit;
$credit_AfterAssess = $this->balances_lib->total_current_credit;
$divisor = 4;
$AddDebitAdj = 0;
switch ($current_period) {
	case "Prelim":
		$divisor = 4;
		break;
	case "Midterm":
		$divisor = 3;
		break;
	case "Prefinal":
		$divisor = 2;
		break;
	case "Final":
		$divisor = 1;
		break;
}
$AddDebitAdj = $debitAdj_AfterAssess / $divisor;
$trans_after_assess = $credit_AfterAssess;

// this really need overhauling... Toyet 12.5.2018
// special note:  balances_lib pre-computes everything
//                so, there is no more need to recompute
//                things over
//$payable_prelim = $this->balances_lib->payable_for_current_period;
if($current_academic_term_obj->term != 'Summer')
	$payable_midterm = ($payable_prelim * 2);

//log_message("INFO", print_r($payable_prelim,true)); // Toyet 8.8.2018
//log_message("INFO", print_r($payable_midterm,true)); // Toyet 8.8.2018

$this->load->view('print_templates/student_assessment',
		array(
				'selected_history_id'=>$selected_history_id,
				'current_academic_term_obj'=>$current_academic_term_obj,
				'term'=>$current_academic_term_obj->term,
				'tuition_fees_content'=>isset($tuition_fees_content) ? $tuition_fees_content : array(),
				'miscellaneous_fees_content'=>isset($miscellaneous_fees_content) ? $miscellaneous_fees_content : array(),
				'idnumber'=>$student_details['idnumber'],
				'familyname'=>$student_details['familyname'],
				'firstname'=>$student_details['firstname'],
				'middlename'=>$student_details['middlename'],
				'level'=>$student_details['level'],
				'course'=>$student_details['course'],
				'payable_midterm'=>(isset($payable_midterm) ? $payable_midterm : 0),
				'payable_prelim'=>(isset($payable_prelim) ? $payable_prelim : 0),
				'period' => $this->balances_lib->current_period(),
				'bal_b4_assessment'=>$balance_before_assessment,
				'assessed_value'=>$total_assessment,
				'trans_after_assess'=>$trans_after_assess,
		)
);
endif;
?>
<?php } ?>	
