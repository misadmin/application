
<h2 class="heading">List of Offered Courses</h2>
<?php echo validation_errors(); 
  ?>

<div class="span10" style="width:95%; margin-left:0px;">

	<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">

		<div style="float:right;">
			<div style="float:left; padding-right:4px; font-size:12px;" >
				<a href="#" class="btn btn-primary new_offering" >
					<i class="icon-plus-sign" style="margin-top:2px;"></i> New Offering
				</a>
			</div>

			<div style="float:left;">
				<select name="academic_terms_id" id="academic_terms_id" style="width:auto;">
					<?php 
						foreach($terms AS $term) {
					?>
							<option value="<?php print($term->id); ?>" 
									term_text="<?php print($term->term." ".$term->sy); ?>"
									<?php if ($term->id == $selected_term) { print("selected"); } ?>>
									<?php print($term->term.' '.$term->sy); ?> 
							</option>
					<?php 	
						}
					?>
				</select>
			</div>
		</div>

	</div>

	<style>
	 table th.center{text-align:center; vertical-align:middle;}
	 table td.middle-td{text-align:left; vertical-align:middle;}
	 table td.center-td{text-align:center; vertical-align:middle;}
	 
	</style>

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#000; background:#FFF; " id="test">
		  <table id="offerings" class="table table-bordered table-hover table-condensed" style="width:100%; padding:0px; margin:0px;">
			<thead>
				<tr>
				  <th width="8%" class="center">Catalog ID</th>
				  <th width="5%" class="center">Section</th>
				  <th width="26%" class="center">Descriptive Title</th>
				  <th width="5%" class="center">Units</th>      
				  <th width="15%" class="center">Time</th>
				  <th width="8%" class="center">Room</th>
				  <th width="7%" class="center">Enrolled</th>
				  <th width="17%" class="center">Faculty</th>
				  <th width="3%" class="center">Allocations</th>
				  <th width="6%" class="center">Status</th>
				</tr> 
			</thead>

		<?php

			$parallel = false;
			if ($offerings) {
				foreach ($offerings AS $offer) {
					//if ($offer->max_students > 99) die('bound to fail'); //@FIXME				
					$id = "('".$offer->id."')";	
					$parallel = $this->Offerings_Model->getOffering($offer->id);
					if ($parallel->parallel_no) {
						$parallel_no = ltrim($parallel->parallel_no,'0');
					} else {
						$parallel_no = 0;
					}					
					
		?>					

					<tr>
						<td class="middle-td"><?php echo $offer->course_code; ?></td>
						<td class="center-td">
		<?php	
							if ($offer->status == 'DISSOLVED' 
								/* 
								 * OR $this->session->userdata('academic_terms_id') > $selected_term
								 *  
								 * This restriction has been to transferred to dean controller to allow 
								 * assigning/changing of faculty on previous semesters.
								 * The rest of actions are still restricted
								 * -- by Tatskie May 12, 2015 
								 */ 
							   ) {
								echo $offer->section_code;
							} else {
		?>
								<a class="course_action" href="<?php echo $offer->id; ?>" parallel_no="<?php echo $offer->parallel_no; ?>" courses_id="<?php echo $offer->courses_id ;?>"> 
									<?php echo $offer->section_code; ?>
								</a>
		<?php 						
							}
		?>					
						</td>
						<td class="middle-td"><?php echo $offer->descriptive_title; ?></td>
						<td class="center-td"><?php echo $offer->credit_units; ?></td>
		<?php 						
						$offering_slots = $this->Offerings_Model->ListOfferingSlots($offer->id);

						if ($offering_slots) {
		?>

							<td class="middle-td">
												
		<?php 					
							foreach($offering_slots[0] AS $slots) {
								echo $slots->tym." ".$slots->days_day_code . "<br>"; 
							}
		?>		  			
							</td>
							<td class="center-td">

		<?php
							
							if ($offering_slots) {
								foreach($offering_slots[0] AS $slots) {
									echo $slots->room_no . "<br>";
								}
							}
		?>					
							</td>
		<?php 					
						} else {
		?>
							<td class="center-td">
							<td class="center-td">
		<?php 					
						}
		?>					
							<td class="center-td">

		<?php 					
							if ($offer->enrollee_parallel) { 
								if($offer->enrollee_parallel > 0) {
		?> 
									<a href="#" data-toggle="modal" data-target=#modal_classlist1_<?php echo $offer->id;?> data-project-id=""><?php echo str_pad($offer->enrollee_parallel,2,'0',STR_PAD_LEFT);?></a>/<?php echo $offer->max_students;?>
		<?php 									
								} else { 
									echo str_pad($offer->enrollee_parallel,2,'0',STR_PAD_LEFT);  
									echo "/" . $offer->max_students;
								}
								
								
		?>
							<div class="modal hide fade" id="modal_classlist1_<?php print($offer->id); ?>" offer_id="<?php print($id); ?>" style="width:750px; margin-left: -375px;">
							   <div class="modal-header">
								   <button type="button" class="close" data-dismiss="modal">�</button>
								   <h3>List of Students</h3>
							   </div>
								<div class="modal-body">            
									<div style="text-align:left; margin-top:0px; font-size:13px; font-weight:bold;">
											<?php print($offer->course_code." [".$offer->section_code."]<br>"); ?>
											<?php print($offer->descriptive_title.'<br>'); 
												if ($offering_slots) {
													foreach($offering_slots[0] AS $slots) {
														print($slots->tym.' '.$slots->days_day_code.' ['.$slots->room_no.']<br>'); 
													}
												}
											?>
									</div>
			
									<div id="modalContent_<?php print($offer->id); ?>" >
										   text here
									</div>
								</div>
							   <div class="modal-footer">
								   <a href="#" class="btn" data-dismiss="modal">Close</a>
							   </div>
							</div>

							<script>
							$('#modal_classlist1_<?php print($offer->id); ?>').on('show', function(){
								  var offer_id = $(this).attr('offer_id');
								  $('#modalContent_<?php print($offer->id); ?>').html('loading...')
							
								  $.ajax({
									  cache: false,
									  type: 'GET',
									  url: '<?php echo site_url($this->uri->segment(1).'/list_enrolled_students');?>',
									  data: {offer_id: <?php print($offer->id); ?>,parallel_no: <?php print($parallel_no); ?>,num: <?php print("1"); ?>},
									  success: function(data) {
										$('#modalContent_<?php print($offer->id); ?>').html(data); //this part to pass the var
									  }
								  });
								})
							</script>						

		<?php 
								
						} else {

							if($offer->enrollee > 0){
		?> 
									<a href="#" data-toggle="modal" data-target=#modal_classlist2_<?php echo $offer->id;?> data-project-id=""><?php echo str_pad($offer->enrollee,2,'0',STR_PAD_LEFT);?></a>/<?php echo str_pad($offer->max_students,2,'0',STR_PAD_LEFT); ?>

							<div class="modal hide fade" id="modal_classlist2_<?php print($offer->id); ?>" 
								offer_id="<?php print($id); ?>" style="width:750px; margin-left: -375px;">
								<div class="modal-header">
								   <button type="button" class="close" data-dismiss="modal">�</button>
								   <h3>List of Students</h3>
								</div>
								<div class="modal-body">            
									<div style="text-align:left; margin-top:0px; font-size:13px; font-weight:bold;">
										<?php print($offer->course_code." [".$offer->section_code."]<br>"); ?>
										<?php print($offer->descriptive_title.'<br>'); 
											if ($offering_slots) {
												foreach($offering_slots[0] AS $slots) {
													print($slots->tym.' '.$slots->days_day_code.' ['.$slots->room_no.']<br>'); 
												}
											}
										?>
									</div>
			
									<div id="modalContent_<?php print($offer->id); ?>" >
										   text here
									</div>
								</div>
								<div class="modal-footer">
									<a href="#" class="btn" data-dismiss="modal">Close</a>
								</div>
							</div>

							<script>
							$('#modal_classlist2_<?php print($offer->id); ?>').on('show', function(){
								  var offer_id = $(this).attr('offer_id');
								  $('#modalContent_<?php print($offer->id); ?>').html('loading...')
							
								  $.ajax({
									  cache: false,
									  type: 'GET',
									  url: '<?php echo site_url($this->uri->segment(1).'/list_enrolled_students');?>',
									  data: {offer_id: <?php print($offer->id); ?>,parallel_no: <?php print($parallel_no); ?>,num: <?php print("2"); ?>},
									  success: function(data) {
										$('#modalContent_<?php print($offer->id); ?>').html(data); //this part to pass the var
									  }
								  });
								})
							</script>						

		<?php 	
							} else { 
									echo str_pad($offer->enrollee,2,'0',STR_PAD_LEFT) . "/" . str_pad($offer->max_students,2,'0',STR_PAD_LEFT);
							}
						} 
		?>
							</td>
							<td class="middle-td"><?php echo $offer->employees_empno ." ". $offer->emp_name; ?> </td>
							<td class="middle-td" style="text-align:center;">
								<a href="#" data-toggle="modal" data-target=#modal_allocation_<?php echo $offer->id;?> data-project-id=""><i class="icon-zoom-in"></i></a>

							<div class="modal hide fade" id="modal_allocation_<?php print($offer->id); ?>" 
								offer_id="<?php print($offer->id); ?>" >
								<div class="modal-header">
								   <button type="button" class="close" data-dismiss="modal">�</button>
								   <h3>List of Allocations</h3>
								</div>
								<div class="modal-body">            
									<div style="text-align:left; margin-top:0px; font-size:13px; font-weight:bold;">
										<?php print($offer->course_code." [".$offer->section_code."]<br>"); ?>
										<?php print($offer->descriptive_title.'<br>'); 
											if ($offering_slots) {
												foreach($offering_slots[0] AS $slots) {
													print($slots->tym.' '.$slots->days_day_code.' ['.$slots->room_no.']<br>'); 
												}
											}
										?>
									</div>
			
									<div id="modalContentAlloc_<?php print($offer->id); ?>" >
										   text here
									</div>
								</div>
								<div class="modal-footer">
									<a href="#" class="btn" data-dismiss="modal">Close</a>
								</div>
							</div>

							<script>
							$('#modal_allocation_<?php print($offer->id); ?>').on('show', function(){
								  var offer_id = $(this).attr('offer_id');
								  $('#modalContentAlloc_<?php print($offer->id); ?>').html('loading...')
							
								  $.ajax({
									  cache: false,
									  type: 'GET',
									  url: '<?php echo site_url($this->uri->segment(1).'/list_allocations');?>',
									  data: {offer_id: <?php print($offer->id); ?>,parallel_no: <?php print($parallel_no); ?>,num: <?php print("3"); ?>},
									  success: function(data) {
										$('#modalContentAlloc_<?php print($offer->id); ?>').html(data); //this part to pass the var
									  }
								  });
								})
							</script>						


							</td> 
							
							<td class="middle-td"><?php echo strtolower($offer->status); ?></td> 
							</tr>
		<?php 
					} //end of: foreach $offerings 
			}

		?>

			</table>
		</div>
		</div>

</div>		


	<form id="srcform2" action="<?php echo site_url("dean/offerings")?>" method="post">
		<input type="hidden" name="step" value="2" />
	</form>

	<script>
		$('.course_action').click(function(event){
			event.preventDefault(); 

			var offering_id = $(this).attr('href');
			var courses_id = $(this).attr('courses_id');
			var parallel_no = $(this).attr('parallel_no');
			
			$('<input>').attr({
				type: 'hidden',
				name: 'offering_id',
				value: offering_id,
			}).appendTo('#srcform2');
			$('<input>').attr({
				type: 'hidden',
				name: 'courses_id',
				value: courses_id,
			}).appendTo('#srcform2');
			$('<input>').attr({
				type: 'hidden',
				name: 'parallel_no',
				value: parallel_no,
			}).appendTo('#srcform2');

			$('#srcform2').submit();

		});

	</script>

<script>
$().ready(function() {
	$('#offerings').dataTable({
	    		"aoColumnDefs": [{ "bSearchable": false, "aTargets": [ 1,2,3,4,5,6 ] }],		
				"iDisplayLength": 50,
			    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
	});
    $("#offerings").stickyTableHeaders();
    $('#offerings').stickyTableHeaders('updateWidth');
	
  })
</script>


<form id="change_sy" method="POST">

</form>

<script>
	$(document).ready(function(){
		$('#academic_terms_id').bind('change', function(){
			show_loading_msg();

			var element           = $("option:selected", "#academic_terms_id");
			var academic_terms_id = element.val();

			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 1,
			}).appendTo('#change_sy');
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_terms_id',
				value: academic_terms_id,
			}).appendTo('#change_sy');
			
			$('#change_sy').submit();
		});
	});
</script>




<script>
	$('.new_offering').click(function(event){
					
		var element2  = $("option:selected", "#academic_terms_id");
		var term_id   = element2.val();
		
		$('#term_text_modal').val(element2.attr('term_text'));

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/course_schedules'); ?>",
			data: {	"term_id": term_id,
					"action": "extract_data_for_new_offerings" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#courses_modal').html(response.courses); 
				
				$('#days_modal').html(response.days); 
				$('#selected_acad_term_id').val(term_id); 
				
				$('#rooms_offering_modal').html(response.rooms); 
				
				$('#show_add_icon').html('');
				
			}
		});

		
		$('#modal_new_offering').modal('show');
		
	});
</script>				


	<div id="modal_new_offering" class="modal hide fade" style="width:45%; margin-left:-400px">
		<div class="modal-dialog" >
			<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">New Offering</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$this->load->view('dean/modals/new_offering_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-primary add_offering_button">Add Offering!</a>
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
			</div>
		</div>
	</div>


	<form id="add_new_offering_form" method="POST" action="<?php echo site_url($this->uri->segment(1).'/course_schedules'); ?>" >
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="save_offering" />
	</form>

	
	<script>
		$('.add_offering_button').click(function(event){
			event.preventDefault();

			var confirmed = confirm('Continue to create offering?');

			if (confirmed){
		
				var element2          = $("option:selected", "#academic_terms_id");
				var academic_terms_id = element2.val();

				var courses_id        = $('#courses_id').val();
				var sched_type        = $('input[name=sched_type]:checked').val();
				var room_id           = $('input[name=room_id]:checked').val();
				var start_time        = $('#start_time').val();
				var num_hr            = $('#num_hr').val();
				var day_code          = $('#day_code').val();
				var max_students      = $('#max_students').val();
				var additional_charge = $('input[name=additional_charge]:checked').val();
				
				$('<input>').attr({
					type: 'hidden',
					name: 'academic_terms_id',
					value: academic_terms_id,
				}).appendTo('#add_new_offering_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'courses_id',
					value: courses_id,
				}).appendTo('#add_new_offering_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'sched_type',
					value: sched_type,
				}).appendTo('#add_new_offering_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'room_id',
					value: room_id,
				}).appendTo('#add_new_offering_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'start_time',
					value: start_time,
				}).appendTo('#add_new_offering_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'num_hr',
					value: num_hr,
				}).appendTo('#add_new_offering_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'day_code',
					value: day_code,
				}).appendTo('#add_new_offering_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'max_students',
					value: max_students,
				}).appendTo('#add_new_offering_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'additional_charge',
					value: additional_charge,
				}).appendTo('#add_new_offering_form');

				$('#add_new_offering_form').submit();
			
			}
			
		});		
	</script>
