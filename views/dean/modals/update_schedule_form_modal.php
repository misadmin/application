

	<style>
		.col1_offer {
			float:left;
			width:25%;
			font-size:12px;	
			font-weight:bold;
			padding-top:6px;
		}
	
		.col2_offer {
			float:left;
			width:75%;
			font-size:12px;	
			font-weight:bold;
		}
		
	</style>

	
		<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
			
			<div style="width:100%">
				<div class="col1_offer">Course [Section]:</div>
				<div class="col2_offer">
					<input type="text" class="form-control" id="course_section_update" style="width:350px;" disabled />
				</div>
			</div>
			
			<div style="width:100%">
				<div class="col1_offer">Schedule:</div>
				<div class="col2_offer">
					<input type="text" class="form-control" id="schedule_update" style="width:350px;" disabled />
				</div>
			</div>

			<div style="width:100%; clear:both;" id="time_sched" >
				<div class="col1_offer">Time:</div>
				<div class="col2_offer" >
						<select id="start_time_update" name="start_time_update" style="width:auto;"> 
						  	<?php
								$t1 = mktime(7,30,0,0,0,0); 
								$tym1 = date('h:i A',$t1);
								$tym  = date('H:i:s',$t1);
								$cnt = 0;
								while ($cnt < (30*25)) {
						   		 	print("<option value=\"$tym\" >$tym1</option>\n");
									$cnt = $cnt + 30;
									$t1 = mktime(7,30+$cnt,0,0,0,0); 
									$tym1 = date('h:i A',$t1);
									$tym  = date('H:i:s',$t1);			
								}
							?>							
						</select>
						
						<select name="num_hr_update" id="num_hr_update" style="width:auto;"> 
			      			<option value="01:00" selected="selected">1 Hour</option>
			      			<option value="01:30">1.5 Hours</option>
			      			<option value="02:00">2.0 Hours</option>
			      			<option value="02:30">2.5 Hours</option>
			      			<option value="03:00">3.0 Hours</option>
			      			<option value="03:30">3.5 Hours</option>
			      			<option value="04:00">4.0 Hours</option>
			      			<option value="04:30">4.5 Hours</option>
			      			<option value="05:00">5.0 Hours</option>
			      			<option value="05:30">5.5 Hours</option>
			      			<option value="06:00">6.0 Hours</option>
			      			<option value="06:30">6.5 Hours</option>
			      			<option value="07:00">7.0 Hours</option>
			      			<option value="07:30">7.5 Hours</option>
			      			<option value="08:00">8.0 Hours</option>
			      			<option value="08:30">8.5 Hours</option>
			      			<option value="09:00">9.0 Hours</option>
			      			<option value="09:30">9.5 Hours</option>
			      			<option value="10:00">10.0 Hours</option>
			      			<option value="10:30">10.5 Hours</option>
			      			<option value="11:00">11.0 Hours</option>
			      			<option value="11:30">11.5 Hours</option>
			      			<option value="12:00">12.0 Hours</option>
			      			<option value="12:30">12.5 Hours</option>
			      			<option value="13:00">13.0 Hours</option>
			      		</select>		
				</div>
			</div>

			<div style="width:100%; clear:both;">
				<div class="col1_offer">Days:</div>
				<div class="col2_offer" id="days_update" >
				</div>
			</div>
						
			<div style="width:98%; clear:both; padding:5px; border:solid 1px #adaaaa;">
				<h4>Select a room from the following buildings:</h4>
				<div id="rooms_offering_update" style="text-align:center;">
				
				</div>
			</div>
				
		</div>			


<script>
	$(document).ready(function(){
		
		$('#start_time_update').bind('change', function(){

			var element    		  = $("option:selected", "#start_time_update");
			var start_time		  = element.val();	
			var element  		  = $("option:selected", "#num_hr_update");
			var num_hr    		  = element.val();
			var element    		  = $("option:selected", "#day_code_update");
			var day_code   		  = element.val();
			var academic_terms_id = $('#academic_terms_id_update').val();
			var old_bldgs_id      = $('#old_bldgs_id_update').val();
			var old_rooms_id      = $('#old_rooms_id_update').val();
			var course_offerings_slots_id = $('#course_offerings_slots_id_update').val();
			
			$('#rooms_offering_update').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:25px;' />"); 

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/course_schedules'); ?>",
				data: {	"academic_terms_id": academic_terms_id,
						"rooms_id": old_rooms_id,
						"start_time": start_time,
						"num_hr": num_hr,
						"day_code": day_code,
						"course_offerings_slots_id": course_offerings_slots_id,
						"old_bldgs_id": old_bldgs_id,
						"action": "extract_vacant_rooms_for_update" },
				dataType: 'json',
				success: function(response) {													
																			
					$('#rooms_offering_update').html(response.rooms); 
					
				}
			});

		});


		$('#num_hr_update').bind('change', function(){

			var element    		  = $("option:selected", "#start_time_update");
			var start_time		  = element.val();	
			var element  		  = $("option:selected", "#num_hr_update");
			var num_hr    		  = element.val();
			var element    		  = $("option:selected", "#day_code_update");
			var day_code   		  = element.val();
			var academic_terms_id = $('#academic_terms_id_update').val();
			var old_bldgs_id      = $('#old_bldgs_id_update').val();
			var old_rooms_id      = $('#old_rooms_id_update').val();
			var course_offerings_slots_id = $('#course_offerings_slots_id_update').val();

			$('#rooms_offering_update').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:25px;' />"); 

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/course_schedules'); ?>",
				data: {	"academic_terms_id": academic_terms_id,
						"rooms_id": old_rooms_id,
						"start_time": start_time,
						"num_hr": num_hr,
						"day_code": day_code,
						"course_offerings_slots_id": course_offerings_slots_id,
						"old_bldgs_id": old_bldgs_id,
						"action": "extract_vacant_rooms_for_update" },
				dataType: 'json',
				success: function(response) {													
																			
					$('#rooms_offering_update').html(response.rooms); 
					
				}
			});

		});
		
	});
</script> 		
		
