<style>
	table.tibolclass 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
</style>
<div class="row-fluid span8">
<div class="loading"></div>

<div class="row-fluid">
	<?php if(isset($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $message; ?>
	</div>
	<?php endif; ?>
</div>


<div class="row-fluid">
	<?php if(isset($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $message; ?>
	</div>
	<?php endif; ?>
	<form id="frm_entry" method="post" action="" class="form-inline">
		<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<div>
					<label class="control-label" for="year_start" >SY Start:
						<div>
						<input type="text" id="year_start"  name="year_start" value="<?php echo date('Y'); ?>" placeholder=<?php echo date('Y'); ?>  style="width:90px;" />
						</div>
					</label>
					<label class="control-label" for="year_end">SY End:
						<div>
						<input type="text" id="year_end"  name="year_end" value ="<?php echo date('Y'); ?>"  placeholder=<?php echo date('Y'); ?>  style="width:90px;" />
						</div>
					</label>
				</div>
				<div>
					<label for="print_to" class="control-label">Target: </label>
					<div class="controls">
						<select id="print_to" name="print_to"  style="width:90px;" >
							<option value="Screen">Screen</option>	
							<option value="CSV">CSV</option>	
							<option value="" disabled>---</option>						
							<option value="DOC" disabled>Word</option>						
							<option value="PDF" disabled>PDF</option>
						</select>
						<button class="btn btn-primary" type="submit">Submit</button>
					</div>	
				</div>					
					
			</fieldset>
	</form>
</div>
<form method="post" action="<?php echo site_url('extras/paying_units_view'); ?>" id="frm_entry2">
	<?php echo $this->common->hidden_input_nonce(); ?>
</form>


<div id="table_here">
<hr>
<?php 
	if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {

?>

<table id="tibolid" class="table table-bordered table-condensed tibolclass">
<thead>
	<tr>
		<th></th>
		<th>School Year</th>
		<th>Semester</th>
		<th>College</th>
		<th>Tuition Total</th>
		<th>Tuition Total <br>(WD's Considered)</th>		
		<th>REED Total</th>
		<th>REED Total <br>(WD's Considered)</th>		
	</tr>
</thead>

<?php
	$conf_term = $this->config->item('terms');
	if (count($rows) < 1) {
		echo '<td class="center" colspan=8>No record found!</td>';
	}else{
		$row_num = 0;
		$sum_tuition = 0.00;
		foreach($rows as $row){
			$row_num++;
			$trm = $conf_term[$row->term];
			$key = $row->end_year . '-' . $row->term . '-' . $row->college_code;
			//$sum_tuition +=$row->tuition_total;
?>
			<tr class="
<?php 
			switch ($trm) {
				case 'First Semester':
					echo 'warning';
					break;
				case 'Second Semester':
					echo 'success';
					break;
				case 'Summer':
					echo 'info';
					break;
				default:
					break;
			} 
?>
">
			<td class="center"><?php echo $row_num .'.'; ?> </td>
			<td class="center"><?php echo $row->end_year-1 . ' - ' . $row->end_year; ?></td>	
			<td class="left"><?php echo $trm  ; ?></td>
			<td class="left"><?php echo $row->college_code; ?></td>
			<td class="right"><?php echo number_format($row->tuition_total,2); ?></td>
			<td class="right"><?php echo number_format($row->tuition_total_wd,2); ?></td>
			<td class="right"><?php echo isset($reed_rows[$key]['reed']) ? number_format($reed_rows[$key]['reed'],2) : '0.00' ; ?></td>
			<td class="right"><?php echo isset($reed_rows[$key]['reed_wd']) ? number_format($reed_rows[$key]['reed_wd'],2) : '0.00' ; ?></td>
			</tr>		

<?php
		} 		
		//if ($row_num > 1) echo '<tr><td colspan=6 class="right"><b>Total...</b></td><td class="center"><b>'. number_format($sum_tuition,2) . '</b></td></tr>';
	}
?>

</table>

<?php } 
?>

</div>

</div>

<script>
$(document).ready(function(){
	$("#tibolid").stickyTableHeaders();
    $("#tibolid").stickyTableHeaders('updateWidth');

	$('#frm_entry').validate({
		submitHandler: function(form) {
			if ($("#print_to").val()!='CSV')
				$('.loading').delay(5).fadeIn(100);
			form.submit();
		}
	});
    
    $("#year_end").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });	
    $("#year_start").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });	
    
    
});
    

$('#colleges').change(function(){
	var college_id = $('#colleges option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
		dataType: "json",
		success: function(data){
			var doptions = make_options(data);
			$('#programs').html(doptions);
		}	
	});	
});


function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}


</script>