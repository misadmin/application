<div style="text-align:center;">
<table border="0" cellpadding="2" cellspacing="0" 
		class="table table-striped table-condensed table-hover" style="width:100%;">
		<tr>
				<td>School</td>
				<td>:</td>
				<td><input type="text" name="school" style="width:400px;" value="<?php print($school_info->school); ?>"></td>
		</tr>	
		<tr>
				<td>Abbreviation</td>
				<td>:</td>
				<td><input type="text" name="abbreviation" style="width:400px;" value="<?php print($school_info->abbreviation); ?>"></td>
		</tr>	
		<tr>
				<td>School Address</td>
				<td>:</td>
				<td>
					
					<?php if($school_info->town_id) {?>		
						<select name="country_id" id="country_id1">
								<option value="<?php print($Saddress->country_id);?>"><?php print($Saddress->country);?></option>											
										<?php						
											foreach($allcountry_groups AS $country_group) {
												echo "<option value=".$country_group->id.">" . $country_group->name ."</option>";	
											}
										?>					
						</select> 
					
					    <select name="province_id" id="province_id1">				
						<option value="<?php print($Saddress->province_id);?>" class="<?php print($Saddress->country_id);?>"> <?php print($Saddress->prov); ?> </option>											
								<?php 					
									foreach($allprovince_groups AS $province_group) {
										print("<option value=".$province_group->id." class=".$province_group->countries_id.">" . $province_group->name ."</option>");
									}
								?>									
					</select> 
					<select name="towns_id" id="towns_id1">						
						<option value="<?php print($Saddress->town_id);?>" class="<?php print($Saddress->province_id);?>"><?php print($Saddress->town); ?></option>											
								<?php 					
									foreach($alltown_groups AS $town_group) {
										print("<option value=".$town_group->id." class=".$town_group->provinces_id.">" . $town_group->name ."</option>");
									}
									?>									

					</select>
					<script type="text/javascript" charset="utf-8">
			          $(function(){
			              $("#province_id1").chained("#country_id1"); 
			              $("#towns_id1").chained("#province_id1"); 
			           //   $("#schools_id").chained("#towns_id1"); 
						});
					</script>		 
					<?php }else {?>
						<select name="country_id" id="country_id2">
										<option value="00137">Philippines</option>											
											<?php						
												foreach($country_groups AS $country_group) {
													echo "<option value=".$country_group->id.">" . $country_group->name ."</option>";	
												}
											?>					
						</select> 
									<select name="province_id" id="province_id2">						
										<option value="49" class="00137">Bohol</option>											
											<?php 					
												foreach($province_groups AS $province_group) {
													print("<option value=".$province_group->id." class=".$province_group->countries_id.">" . $province_group->name ."</option>");
												}
											?>									
									</select> 
									<select name="towns_id" id="towns_id2">						
										<option value="960" class="49">Tagbilaran City</option>											
											<?php 					
												foreach($town_groups AS $town_group) {
													print("<option value=".$town_group->id." class=".$town_group->provinces_id.">" . $town_group->name ."</option>");
												}
											?>									
							</select> 
							<script type="text/javascript" charset="utf-8">
			         	  $(function(){
			              $("#province_id2").chained("#country_id2"); 
			              $("#towns_id2").chained("#province_id2"); 
			           //   $("#schools_id").chained("#towns_id1"); 
						});
					</script>	
					
					<?php }?>
				</td>
		</tr>
		</tr>	
				<td>Status</td>
				<td>:</td>
				<td><select name="status">
				   <?php if($school_info->status=='INACTIVE') {?>
						<option selected='selected'>
											 INACTIVE 
											</option>
											<option> 
											ACTIVE
											</option>
										   <?php }else {?>	
											  <option>
											 INACTIVE
											</option>
											<option selected='selected'> 
											ACTIVE
											</option>
											<?php }?>	
											</select></td>
		</tr>
	</table>
</div>

