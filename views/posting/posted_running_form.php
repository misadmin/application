

		<h2 class="heading">Assessment Report - Posted Against Running Balance</h2>

		<div>
			<form id="generate_report_form" method="post" class="form-horizontal">
				<input type="hidden" name="action" value="generate_assessment" />
				
				<div class="control-group acad_term" style="display:block;">
					<label class="control-label" for="academic_term">Academic Term&nbsp;&nbsp;:</label>
					<div class="controls ">
						 <select name="academic_terms_id" id="academic_terms_id">
							<?php
								foreach($academic_terms AS $academic_term) {
							?>			
									<option value="<?php print($academic_term->id); ?>" 
										<?php if ($current_term->id == $academic_term->id) print("selected"); ?> 
										term_sy="<?php print($academic_term->term." ".$academic_term->sy); ?>" >
										<?php 
											print($academic_term->term." ".$academic_term->sy);
										?>
									</option>
							<?php
								}
							?>
						</select>
					</div>
				</div>
								
				<div class="control-group">
					<label class="control-label" for="college">College&nbsp;&nbsp;:</label>
					<div class="controls ">
						<select name="colleges_id" id="colleges_id">
							<option value="0">--Select College--</option> 
							<?php
								foreach($colleges AS $college) {
							?>			
									<option value="<?php print($college->id); ?>" 
										college_name="<?php print($college->name); ?>" >
										<?php print($college->college_code); ?>
									</option>
							<?php
								}
							?>
						</select>
					</div>
				</div>

				<div class="control-group has_college" style="display:none;">
					<label class="control-label" for="college">Program:</label>
					<div class="controls" id="college_programs">
					
					</div>
				</div>
				
				<!-- <div class="control-group has_college" style="display:none;">
					<label class="control-label" for="college">Year Level:</label>
					<div class="controls">
						<select name="year_level" id="year_level">
							<option value="1">1st Year</option>
							<option value="2">2nd Year</option>
							<option value="3">3rd Year</option>
							<option value="4">4th Year</option>
							<option value="5">5th Year</option>
						</select>
					</div>
				</div>
				-->

				<div class="control-group">
					 <div class="controls">
						<button class="btn btn-primary" name="generate_report_button" type="submit" id="generate_report" disabled>Generate Report!</button>
					</div>
				</div>
				
			</form>
		</div>

		<div style="margin-top:20px; text-align:center;" id="show_extract_icon">
			
		</div>
		

<script>
$('#colleges_id').bind('click', function(event){
	event.preventDefault();

		var element     = $("option:selected", "#colleges_id");
		var colleges_id = element.val();

		if (colleges_id != 0) {

			$('#generate_report').prop("disabled", false);
			$('.has_college').show();

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/posted_running'); ?>",
				data: {	"colleges_id": colleges_id,
						"action": "extract_college_programs" },
				dataType: 'json',
				success: function(response) {													
					$('#college_programs').html(response.programs); 
				}
			});
			
		} else {
			$('#generate_report').prop("disabled", true);
			$('.has_college').hide();
		}
});
	
</script>


<script>
$('#generate_report').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to generate Tuition Report?");

	if (confirmed){
		
		var element      = $("option:selected", "#academic_terms_id");
		var term_sy      = element.attr('term_sy');
		var element      = $("option:selected", "#colleges_id");
		var college_name = element.attr('college_name');
		var element      = $("option:selected", "#acad_programs_id");
		var abbreviation = element.attr('abbreviation');
		var description  = element.attr('description');
		var max_yr_level = element.attr('max_yr_level');
		var acad_program_groups_id = element.attr('acad_program_groups_id');

		$('#show_extract_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' />"); 

		$('<input>').attr({
			type: 'hidden',
			name: 'term_sy',
			value: term_sy,
		}).appendTo('#generate_report_form');

		$('<input>').attr({
			type: 'hidden',
			name: 'college_name',
			value: college_name,
		}).appendTo('#generate_report_form');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'abbreviation',
			value: abbreviation,
		}).appendTo('#generate_report_form');

		$('<input>').attr({
			type: 'hidden',
			name: 'description',
			value: description,
		}).appendTo('#generate_report_form');

		$('<input>').attr({
			type: 'hidden',
			name: 'acad_program_groups_id',
			value: acad_program_groups_id,
		}).appendTo('#generate_report_form');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'max_yr_level',
			value: max_yr_level,
		}).appendTo('#generate_report_form');
		
		$('#generate_report_form').submit();
	}		
});
</script>





  