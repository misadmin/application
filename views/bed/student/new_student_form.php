
	<style>
		.enrol_header {
			background: linear-gradient(to bottom, rgba(147,206,222,1) 0%, rgba(117,189,209,1) 41%, rgba(73,165,191,1) 100%);
			color:#FFFFFF; 
			font-weight: bold;
		}
		
		req {
			color: red;
			font-weight: bold;
			font-size: 18px;
		}
	</style>

		<div class="container-fluid">
			
			<div class="span12" style="margin-left:0px;">
				<h2>New Student - Basic Education</h2>
			</div>
			
			<div class="row"></div>
			
			<div class="span8" style="border: 1px solid #c1c6c7;  overflow: hidden; margin-left:0px; padding-bottom: 10px;">
				<div style="margin-bottom: 20px; overflow: hidden;  padding: 10px; color: #3a3837; " class="enrol_header">
					Personal Information
				</div>
				
				<div class="row"></div>
				
				<div class="span12 row">
					
						<div class="span4">
							<div class="form-group">
								<label for="lname">Lastname<req>*</req></label>
								<input type="text" class="form-control search_student" id="lname" name="lname" placeholder="Family Name" autofocus="autofocus" />
							</div>
						</div>
							
						<div class="span4">
							<div class="form-group">
								<label for="fname">Firstname<req>*</req></label>
								<input type="text" class="form-control search_student" id="fname" name="fname" placeholder="Firstname" />
							</div>
						</div>
							
						<div class="span4">
							<div class="form-group">
								<label for="mname">Middle name<req>*</req></label>
								<input type="text" class="form-control search_student" id="mname" name="mname" placeholder="Middle name" />
							</div>
						</div>		
				</div>
				
				<div class="span12 row">
						<div class="span4">
							<div class="form-group">
								<label for="lname">Date of Birth<req>*</req></label>
								<div class="input-append date" id="student_dbirth" data-date-format="yyyy-mm-dd" >
									<input type="text" name="dbirth" id="dbirth" class="span6 search_student" readonly />
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>
							</div>
						</div>
						
						<div class="span4">
							<div class="form-group">
								<label for="fname">Gender<req>*</req></label>
								<select name="gender" id="gender">
									<option value="">--Select Gender--</option>
									<option value="M">Male</option>
									<option value="F">Female</option>
								</select>
							</div>
						</div>
						
						<div class="span4">
							<div class="form-group">
								<label for="lname">LRN</label>
								<input type="text" class="form-control" id="lrn" name="lrn" placeholder="Learner Reference Number"  />
							</div>
						</div>
					
				</div>
			</div>
						
			<div class="span4">
				<div id="show_search_code_icon" style="color: #FF0000; font-weight: bold;"></div>
				<div id="list_of_students" style="margin:0 auto; border: 1px solid #c1c6c7;  overflow: hidden; display:none;">

				</div>
			</div>		
		
			<div class="row" style="margin:25px;"></div>

			<div class="span12" style="border: 1px solid #c1c6c7;  overflow: hidden; margin-left:0px; padding-bottom: 10px;">
				<div style="margin-bottom: 20px; overflow: hidden;  padding: 10px; color: #3a3837;" class="enrol_header">
					Enrolment Details
				</div>
				
				<div class="row"></div>
				
				<div class="span12 row">
					<div class="span3">
						<div class="form-group">
							<label for="lname">Academic Year<req>*</req></label>
						   <select name="academic_year" id="acad_years_id" >
								<?php 
									foreach ($academic_years as $academic_year){
								?>
										<option value="<?php print($academic_year->id); ?>" >
										<?php print($academic_year->sy); ?></option>
								<?php 
									}
								?>
							</select>
						</div>
					</div>
					
					<div class="span3">
						<div class="form-group">
							<label for="fname">Grade/Year Level<req>*</req></label>
						   <select id="prospectus_id" name="prospectus_id" >
								<option value="0">--Select Level--</option>
								<?php 
									foreach($yr_levels as $yr_level) { 
								?>
									<option value="<?php print($yr_level->id); ?>" 
													levels_id="<?php print($yr_level->levels_id); ?>" 
													year_level="<?php print($yr_level->year_level); ?>">
												<?php print($yr_level->level." - ".$yr_level->description); ?></option>
								<?php 
									}
								?>
							</select>
						</div>
					</div>
										
					<div class="span3">
						<div class="form-group">
							<label for="enrolment_type">Enrollment Type<req>*</req></label>
							<select name="enrolment_type" id="enrolment_type">
								<option value="regular">Regular</option>
								<option value="transferee">Transferee</option>
								<option value="isped-inclusion">ISPED Inclusion</option>
								<option value="isped-selfcontained">ISPED Self-contained</option>
							</select>
						</div>
					</div>

					
				</div>
				
			</div>

			<div class="row" style="margin:25px;"></div>

			<div class="span12" style="border: 1px solid #c1c6c7;  overflow: hidden; margin-left:0px; padding-bottom: 10px;">
				<div style="margin-bottom: 20px; overflow: hidden;  padding: 10px; color: #3a3837;" class="enrol_header">
					Emergency Information
				</div>
				
				<div class="row"></div>
				
				<div class="span12 row">
					<div class="span3">
						<div class="form-group">
							<label for="lname">Lastname<req>*</req></label>
							<input type="text" class="form-control" id="emergency_lname" name="emergency_lname" placeholder="Emergency Lastname" />
						</div>
					</div>
					
					<div class="span3">
						<div class="form-group">
							<label for="lname">Firstname<req>*</req></label>
							<input type="text" class="form-control" id="emergency_fname" name="emergency_fname" placeholder="Emergency Firstname" />
						</div>
					</div>
					
					<div class="span3">
						<div class="form-group">
							<label for="lname">Relation<req>*</req></label>
							<select name="emergency_relation" class="form-control" id="emergency_relation">
								<option value="parent">Parent</option>
								<option value="brother">Brother</option>
								<option value="sister">Sister</option>
								<option value="auntie">Auntie</option>
								<option value="uncle">Uncle</option>
								<option value="step-parent">Step-parent</option>
								<option value="neighbour">Neighbour</option>
								<option value="friend">Friend</option>
								<option value="partner">Partner</option>
							</select>
						</div>
					</div>		
					
					<div class="span3">
						<div class="form-group">
							<label for="lname">Contact No.<req>*</req></label>
							<input type="text" class="form-control" id="emergency_contact" name="emergency_contact" placeholder="Emergency Contact" />
						</div>
					</div>
					
				</div>

				<div class="span12 row">
					
					<div class="span12">
						<div class="form-group">
							<label for="lname">Address<req>*</req></label>
										<div id="country_group" style="float:left; padding-left:5px;">
											<?php 
												$data1['countries'] = $countries;
												$data1['type']      = NULL;
												$this->load->view('common/places/select_countries', $data1);
											?>	
										</div>

										<div id="province_group" style="float:left; padding-left:5px;">
											<?php 
												$data1['provinces'] = $provinces;
												$data1['type']      = NULL;
												$this->load->view('common/places/select_provinces', $data1);
											?>	
										</div> 

										<div id="town_group" style="float:left; padding-left:5px;">
											<?php 
												$data1['towns'] 	= $towns;
												$data1['type']      = NULL;
												$this->load->view('common/places/select_towns', $data1);
											?>	
										</div> 
								 
										<div id="brgy_group" style="float:left; padding-left:5px;"></div> 
						</div>
					</div>
					
				</div>	

			</div>

			<div class="row" style="margin:25px;"></div>

			<div class="span10" style="overflow: hidden; margin-left:0px;">
				<div style="margin-bottom: 20px;">
					<input type="submit" class="btn btn-danger" name="register_student" id="register_student" value="Register Student!" disabled />
				</div>
				
				<div class="row"></div>
			</div>
		
		</div>


	<script>
		$(document).ready(function(){
			$("#country_id").val('137'); //Philippines
			$("#province_id").val('49'); //Bohol
			
			$('#student_dbirth').datepicker();
		});
	</script>
	
	<script>
		$(".search_student").blur(function(){
				
			var lastname   = $('#lname').val();
			var firstname  = $('#fname').val();
			var middlename = $('#mname').val();

			$('#show_search_code_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
				
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/bed_enrolment'); ?>",
				data: {"lastname": lastname,
						"firstname": firstname,
						"middlename": middlename,						
						"action": "search_student" },
				dataType: 'json',
				success: function(response) {													
					if (response.found) {
						$('#show_search_code_icon').html('Student of the same possible name found!'); 
						$('#list_of_students').show();
						$('#list_of_students').html(response.output);
					} else {
						$('#show_search_code_icon').html(''); 	
						$('#list_of_students').hide();						
					}
				}
			});
			
		});
		
	</script>

	
	<script>
		$("#lname").keyup(function(){
			
			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
			
		});
		
		$("#fname").keyup(function(){
			
			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
			
		});

		$("#mname").keyup(function(){
			
			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
			
		});

		$("#dbirth").keyup(function(){
						
			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
			
		});
		
		$("#dbirth").blur(function(){
			
			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
			
		});

		$('#gender').change(function(event){
			event.preventDefault();

			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
		
		});
		

		$("#emergency_lname").keyup(function(){
			
			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
			
		});

		$("#emergency_fname").keyup(function(){
			
			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
			
		});

		$("#emergency_contact").keyup(function(){
			
			if (required_has_value()) {
				$('#register_student').prop("disabled", false);
			} else {
				$('#register_student').prop("disabled", true);
			}
			
		});
		
		function isEmpty(v) {
			if (v === '' || v === null) {
				return true;
			} else {
				return false;
			}
		}
		
		
		function required_has_value() {
			
			var lname			= $('#lname').val();
			var fname			= $('#fname').val();
			var mname			= $('#mname').val();
			var dbirth			= $('#dbirth').val();
			var gender			= $("select#gender option:selected").attr('value');
			var prospectus_id	= $("select#prospectus_id option:selected").attr('value');
			var emergency_lname		= $('#emergency_lname').val();
			var emergency_fname		= $('#emergency_fname').val();
			var emergency_contact	= $('#emergency_contact').val();
			var town_id				= $("select#town_id option:selected").attr('value');

			if (prospectus_id != 0 && town_id.length > 0) {
				if (isEmpty(lname) || isEmpty(fname) || isEmpty(mname) || isEmpty(dbirth) || isEmpty(emergency_lname) || isEmpty(emergency_fname) || isEmpty(emergency_contact)) {
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
				
		}


		$('#register_student').click(function(e){
			e.preventDefault();

			if (!required_has_value()) {
				alert('All fields are required!');
			} else {
				
				var lname				= $('#lname').val();
				var fname				= $('#fname').val();
				var mname				= $('#mname').val();
				var dbirth				= $('#dbirth').val();
				var gender				= $("select#gender option:selected").attr('value');
				var lrn					= $('#lrn').val();
				var acad_years_id		= $("select#acad_years_id option:selected").attr('value');
				var prospectus_id		= $("select#prospectus_id option:selected").attr('value');
				var enrolment_type		= $("select#enrolment_type option:selected").attr('value');
				var emergency_lname		= $('#emergency_lname').val();
				var emergency_fname		= $('#emergency_fname').val();
				var emergency_relation	= $("select#emergency_relation option:selected").attr('value');
				var emergency_contact	= $('#emergency_contact').val();
				var brgy_id				= $("select#brgy_id option:selected").attr('value');
				var street_name			= $('#street_name').val();
				var levels_id			= $("select#prospectus_id option:selected").attr('levels_id');
				var year_level			= $("select#prospectus_id option:selected").attr('year_level');
			
				var confirmed = confirm('Continue to register new student?');

					if (confirmed){

						$('<input>').attr({
							type: 'hidden',
							name: 'lastname',
							value: lname,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'firstname',
							value: fname,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'middlename',
							value: mname,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'dbirth',
							value: dbirth,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'gender',
							value: gender,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'lrn',
							value: lrn,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'acad_years_id',
							value: acad_years_id,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'prospectus_id',
							value: prospectus_id,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'enrolment_type',
							value: enrolment_type,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'emergency_lname',
							value: emergency_lname,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'emergency_fname',
							value: emergency_fname,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'emergency_relation',
							value: emergency_relation,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'emergency_contact',
							value: emergency_contact,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'brgy_id',
							value: brgy_id,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'street_name',
							value: street_name,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'levels_id',
							value: levels_id,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'year_level',
							value: year_level,
						}).appendTo('#new_student_form');		
						
						$('#new_student_form').submit();
					}
			}
			
		});
		
	</script>

	<form id="new_student_form" method="POST" class="form-horizontal">
		<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="register_student" />
	</form>

