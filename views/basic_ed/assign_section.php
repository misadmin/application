<?php 
	if ($stud_status != 'active') {
		print("<h3>Student NOT Active! Section cannot be updated!</h3>");
	} else {
?>
<?php if (isset($message) && $tab=='11') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
  <div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:10px; "> 
 <form method="post" id="assignSection">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "assign_section"/>
	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" > 
	    <table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;"> 
	  <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    	<strong><font face="Verdana, Arial, Helvetica, sans-serif"> ASSIGN SECTION</font></strong>
		   </div>
		   </td>
 	 </tr>
 	 
	 </table>	

	<div style="margin-left:20px; width:90%; margin-bottom:20px; ">
	     
	   <!--    <table width="18%" align="left" cellpadding="0" cellspacing="0" style="width:50%;">
			  <tr>
				  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">  -->
				   <label class="control-label"> <b> For Academic Year: </b> </label>
					<div class="controls">
						<select id="academic_year_id" name="academic_year_id">
						<?php
							foreach($academic_years AS $academic_year) {
								print("<option value=".$academic_year->id.">".$academic_year->start_year."-".$academic_year->end_year."</option>");	
							}
						?>
						</select>
					</div>
			<!-- 	   </td>
				</tr>   
			 
		 	 <tr>
				  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">  -->
				    <label class="control-label" for="level"><b> Section:</b></label>
				    <div class="controls">
				    <select id="section" name="section" class="form">
						<option value="">-- Select Section --</option>
							<?php foreach ($sections as $section): ?>
								<option value="<?php echo $section->id; ?>"><?php echo $section->section_name; ?></option>
							<?php endforeach; ?>
						</select>
				   	</div>
			<!--  	   </td>
		 	 </tr>
		 	 <tr>
		 	     <div style="margin-left:25px;">
		 		 <td colspan = "2"> -->
			</div>
			 <div class="controls">
		 			 <button class="btn btn-primary" type="submit" style="margin-left:20px;" >Assign Section</button>
		 	</div>		 
		</div>	 
		 	<!-- 	 </td>
		 		 
		 	 </tr>
		  </table> 
 	 </div>  -->
 	 
  
	</form>
  <!--   </div>  --> 
  
  <?php 
	}
  ?>