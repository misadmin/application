<?php 
class faculty_lib {
	
	protected $CI;
	public function __construct(){
		$this->CI =& get_instance();
	}


	public function faculty($faculty_only=FALSE){
		$this->CI->content_lib->set_title((in_array($this->CI->role, array('hrdmo',)) ? strtoupper($this->CI->role) : ucfirst($this->CI->role)) .' | Faculty | ' . $this->CI->config->item('application_title'));
		$query = $this->CI->input->post('q');
		$empno = $this->CI->uri->segment(3);
		$this->CI->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->CI->role, 'what'=>'faculty'), 'Search Employees', 'in');
		
	
		if ($query){
			//print_r($query);die();
			//There is a query...
			$this->CI->load->model('employee_common_model');
			$results = $this->CI->employee_common_model->search($query,$faculty_only);
	
			if (count($results) > 1){
				//more than 1 results... lets show it in a page
				foreach ($results as $result){
					if (is_file(FCPATH . $this->CI->config->item('employee_images_folder') . ltrim($result->empno, '0') . ".jpg"))
						$image = base_url($this->CI->config->item('employee_images_folder') . ltrim($result->empno, '0') . ".jpg");
					else {
						//$image = base_url($this->CI->config->item('no_image_placeholder_female')); else
						$image = base_url($this->CI->config->item('no_image_placeholder_male'));
					}
					$res[] = array('image'=>$image, 'idno'=>$result->empno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
				}
				$this->CI->content_lib->enqueue_body_content('common/employee_search_result', array('results'=>$res, 'query'=>$query, 'role'=>$this->CI->role));
			} elseif (count($results)==1){
				redirect (site_url("{$this->CI->role}/faculty/{$results[0]->empno}"));
			} else {
				//no result...
	
			}
		}
	
		if ($empno){
			//An employee number was used...
			$this->CI->load->model('employee_common_model');
			$this->CI->load->model('academic_terms_model');
			$data = $this->CI->employee_common_model->my_information($empno);
			//We're going to need tabs... lets load the tab library:
			$this->CI->load->library('tab_lib');
	
			//User may have selected what academic term and year...
			if ($this->CI->input->post('academic_terms_id')) {
				$academic_terms_id = $this->CI->input->post('academic_terms_id');
			} else {
				$faculty_inclusive_academic_terms = $this->CI->academic_terms_model->faculty_inclusive_academic_term ($empno);
				$academic_terms = $this->CI->academic_terms_model->current_academic_term();
				$academic_terms_id = isset($faculty_inclusive_academic_terms[0]->id) ? $faculty_inclusive_academic_terms[0]->id : $academic_terms->id;
			}
	
			//Lets process actions intended for this employee...
			switch ($this->CI->input->post('action')){
	
				case 'show_class_list'	:
					$this->CI->load->model('hnumis/Enrollments_model');
					$this->CI->load->model('hnumis/Courses_model');
	
					$course_offering_id = $this->CI->input->post('course_offering_id');
					$results = $this->CI->Enrollments_model->class_record($course_offering_id, $empno);
	
					$courses['courses'] = $this->CI->Courses_model->faculty_courses_academic_terms($empno, $academic_terms_id);
					$courses['academic_terms_id'] = $academic_terms_id;
					$courses['action'] = 'show_class_list';
	
					$academic_terms = $this->CI->academic_terms_model->faculty_inclusive_academic_term ($empno);
					$this->CI->content_lib->enqueue_sidebar_widget ('dean/list_academic_terms1',array('academic_terms'=>$academic_terms, 'selected_term'=>$academic_terms_id), 'Select Academic Terms', 'in');
	
	
					$this->CI->tab_lib->enqueue_tab ('Schedule', 'faculty/class_list',
							array('results'=>$results,
									'selected_term'=>$academic_terms_id,
									'course_offerings_id'=>$course_offering_id,
									'credit_units'=> (isset($results[0]->credit_units)?$results[0]->credit_units: ""),
									'descriptive_title' => (isset($results[0]->descriptive_title)?$results[0]->descriptive_title: ""),
									'course_code' => (isset($results[0]->course_code) ? $results[0]->course_code: ""),
									'section_code' => (isset($results[0]->section_code)? $results[0]->section_code: ""), ),
							'schedule',
							TRUE);
	
					$this->CI->content_lib->enqueue_sidebar_widget('faculty/course_shortcuts', $courses, 'Course Shortcuts', 'in');
					break;
				case 'download_grading_sheet':
	
					$this->CI->load->model('hnumis/Reports_model');
	
					$course['course_description'] = $this->CI->input->post('course_description');
					$course['credit_units'] = $this->CI->input->post('credit_units');
	
					$this->CI->Reports_model->generate_faculty_grading_sheet_pdf($this->CI->input->post('course_offerings_id'), $course, $this->CI->input->post('download_type'));
	
					return;
				case 'download_class_list':
	
					$this->CI->load->model('hnumis/Reports_model');
					//print_r("test"); die();
					$course['course_description'] = $this->CI->input->post('course_description');
					$course['credit_units'] = $this->CI->input->post('credit_units');
	
					$this->CI->Reports_model->generate_class_list_pdf($this->CI->input->post('course_offerings_id'), $course, $this->CI->input->post('download_type'));
	
					return;
				default:
					$this->CI->load->model('hnumis/Courses_model');
					$academic_terms = $this->CI->academic_terms_model->faculty_inclusive_academic_term ($empno);
					$this->CI->content_lib->enqueue_sidebar_widget ('dean/list_academic_terms1',array('academic_terms'=>$academic_terms, 'selected_term'=>$academic_terms_id), 'Select Academic Terms', 'in');
					$result = $this->CI->Courses_model->faculty_courses_academic_terms($empno, $academic_terms_id);
					$this->CI->tab_lib->enqueue_tab ('Schedule', 'faculty/list_courses', array('result'=>$result, 'term'=>(isset($result[0]->term) ? $result[0]->term : ""), 'school_year'=>(isset($result[0]->sy) ? $result[0]->sy : ""), 'selected_term'=>$academic_terms_id), 'schedule', TRUE);
					break;
			}
	
	
			if (is_file(FCPATH . $this->CI->config->item('employee_images_folder') . ltrim($empno, '0') . ".jpg"))
				$image = base_url($this->CI->config->item('employee_images_folder') . ltrim($empno, '0') . ".jpg");
			else {
				//$image = base_url($this->CI->config->item('no_image_placeholder_female')); else
				//todo: add gender to employees table...
				$image = base_url($this->CI->config->item('no_image_placeholder_male'));
			}
	
			$data['image'] = $image;
			$this->CI->content_lib->enqueue_body_content('common/employee_profile', $data);
	
			$this->CI->load->model('hnumis/AcademicYears_Model');
			$this->CI->load->model('hnumis/Courses_model');
	
			$academic_terms = $this->CI->academic_terms_model->faculty_inclusive_academic_term ($empno);
	
			$data['selected_term'] = $academic_terms_id;
			$this->CI->content_lib->enqueue_body_content('', $this->CI->tab_lib->content());
		}
	
		if ( ! $empno && ! $query){
			//No query was done by the DEAN... and no employee was found yet...
			$data = array();
			if ($this->CI->input->post('letter')){
				$data['letter'] = $this->CI->input->post('letter');
				$this->CI->load->model('employee_common_model');
				$data['results'] = $this->CI->employee_common_model->search_by_starting_letter($this->CI->input->post('letter'),$faculty_only);
			}
			$this->CI->content_lib->enqueue_body_content('common/employee_search_landing', $data);
		}
	
		$this->CI->content_lib->content();
	}
	
}