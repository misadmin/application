<?php
class Cashier extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->content_lib->set_title ('Cashier | ' . $this->config->item('application_title'));
		$this->navbar_data['menu'] = 
							array('Receipts'=>
									array(
										'Post' 	=> 'post_receipts',
										'Void' 	=> 'void_receipts',
										'Search'=> 'search_receipt',
											
										),
									
								'Reports'=>		
									array('Daily Receipt Summary' => 'daily_receipt_summary',),
							);
		
		$this->content_lib->set_navbar_content('', $this->navbar_data);

		/*
		$client_ip = $this->input->ip_address();
		
		if (! in_array($client_ip, $this->config->item('finance_allowed_ips'))){
			die($client_ip . ' May God have mercy on your soul...');
		}
		*/
	}

	public function index(){
		//$this->content_lib->enqueue_sidebar_widget('sidebar/sidebar', array('content'=>'This is a warning'), 'Warning', 'in');
		parent::index();
	}
	
	public function daily_receipt_summary(){
		$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
		$this->load->model('teller/teller_model');
		$this->content_lib->enqueue_footer_scripts_array(array('date_picker', 'maskedinput'));
		$date = ($this->input->post('date') ? $this->input->post('date') : date('Y-m-d'));
		$data = array('date'=>$date,);
		$this->content_lib->enqueue_body_content('teller/receipt_summary_index', $data);
		$this->load->helper('array_range_helper');
		if($transactions = $this->teller_model->new_daily_receipt_summary($date)){
			//transactions are found today...
			$items = array();
			$payment_methods = array();
			$receipts = array();
			$sum_from_items = array();
			$ips = array();
			foreach($transactions as $transaction){
				$ips[] = $transaction->machine_ip;
				//print_r($transactions); die();
				$items[$transaction->machine_ip][$transaction->teller_code]['transactions'][$transaction->receipt_no] = $transaction;
				$items[$transaction->machine_ip][$transaction->teller_code]['name'] = $transaction->description;
				//Sum kind of magic...
				if(isset($receipts[$transaction->machine_ip])){
					$receipts[$transaction->machine_ip][] = (object)array('receipt_no'=>$transaction->receipt_no, 'type'=>$transaction->tax_type); 
				} else {
					$receipts[$transaction->machine_ip] = array((object)array('receipt_no'=>$transaction->receipt_no, 'type'=>$transaction->tax_type));
				}
				
			}
			/* This is where the magic begins... */
			$summary_void = array();
			$summary_gross = array();
			$summary_net = array();
			$total_gross = 0;
			$total_net = 0;
			$total_void = 0;
			foreach($items as $ip=>$teller_code_transactions){
				foreach($teller_code_transactions as $trans){
					foreach($trans['transactions'] as $tran){
						$total_gross += $tran->whole_amount;
						$total_net += ($tran->status!='void' ? $tran->whole_amount : 0);
						$total_void += ($tran->status=='void' ? $tran->whole_amount : 0);
						if(isset($summary_gross[$ip]))
							$summary_gross[$ip] += $tran->whole_amount; else
							$summary_gross[$ip] = $tran->whole_amount;
						if($tran->status == 'void'){
							if(isset($summary_void[$ip]))
								$summary_void[$ip] += $tran->whole_amount; else
								$summary_void[$ip] = $tran->whole_amount;	
						} else {
							if(isset($summary_net[$ip]))
								$summary_net[$ip] += $tran->whole_amount; else
								$summary_net[$ip] = $tran->whole_amount;
						}
						if(!isset($sum_from_items[$ip])) {
							$sum_from_items[$ip] = (!in_array($tran->status, array('void', 'deleted')) ? $tran->whole_amount : 0); 
						} else {
							$sum_from_items[$ip] += (!in_array($tran->status, array('void', 'deleted')) ? $tran->whole_amount : 0);
						}
					}		
				}
			}
			$daily_payment_method_summary = $this->teller_model->daily_payment_method_summary($date);
			$magicked_sum = array();
			foreach($daily_payment_method_summary as $payment_method_){
				if(!isset($magicked_sum[$payment_method_->machine_ip][$payment_method_->payment_method]))
					$magicked_sum[$payment_method_->machine_ip][$payment_method_->payment_method] = $payment_method_->amount_sum; else 
					$magicked_sum[$payment_method_->machine_ip][$payment_method_->payment_method] += $payment_method_->amount_sum;
			}
			
			foreach($sum_from_items as $ip => $item_sum){
				$magicked = 0;
				if (isset($magicked_sum[$ip])){
					foreach ($magicked_sum[$ip] as $sum_magicked){
						$magicked += $sum_magicked; 
					}
					$magicked_sum[$ip]['Cash'] = $item_sum - $magicked;
				} else {
					$magicked_sum[$ip]['Cash'] = $item_sum;	
				}
			}
			
			//for summary:
			$summary = array();
			$ips = array_unique($ips);
			foreach($ips as $ip){
				$summary[$ip] = array(
						'non-vat'=>array(),
						'vat'=>array(),	);	
			}
			foreach($receipts as $ip=>$receipt_b){
				foreach($receipt_b as $receipt){
					if($receipt->type=='NV')
						$summary[$ip]['non-vat'][] = $receipt->receipt_no;
					elseif($receipt->type=='V')
						$summary[$ip]['vat'][] = $receipt->receipt_no;
				}
			}
			//print_r($magicked_sum); die();			
			/* This is where the magic ends ... */
			$this->content_lib->enqueue_body_content('teller/receipt_summary_table', 
					array(
							'records'=>$items, 
							'payment_methods'=>$magicked_sum, 
							'summary'=>$summary, 
							'void_transaction_totals'=>$summary_void, 
							'gross_transaction_totals'=>$summary_gross, 
							'net_transaction_totals'=>$summary_net,
							'total_gross'=>$total_gross,
							'total_void'=>$total_void,
							'total_net'=>$total_net));
			$this->load->library('print_lib');
			$this->content_lib->enqueue_body_content('print_templates/daily_receipt_summary', array('records'=>$items, 'payment_methods'=>$magicked_sum));
		} else {
			$this->content_lib->enqueue_body_content('', '<h3>This machine has No Transactions Today!</h3>');
		}
		$this->content_lib->content();	
	}

	
	public function post_receipts(){
		$this->load->model('teller/teller_model');
		if ($this->common->nonce_is_valid($this->input->post('nonce'))) {

			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch($step) {
				case 1: 					
						$this->content_lib->enqueue_footer_scripts_array(array('date_picker', 'maskedinput'));
						$date = ($this->input->post('date') ? $this->input->post('date') : date('Y-m-d'));
						$data = array('date'=>$date,);		
						$this->content_lib->enqueue_body_content('cashier/unposted_receipt_index', $data);
						break;
				case 2: //query unposted receipts 						
						$for_posting = $this->teller_model->get_payments_for_posting(date($this->input->post('date')));						
						if ($for_posting) {
								$data1['receipts'] = $for_posting;
								$data1['date1'] = date($this->input->post('date'));
								$this->content_lib->enqueue_body_content('cashier/post_receipts', $data1);
						} else {							
							$this->content_lib->enqueue_body_content('', '<h3>No unposted receipts!</h3>');
						}
						break;
				case 3: //post unposted receipts
						if ($posted = $this->teller_model->post_receipts_ver2($this->input->post('id_to_post'), $this->session->userdata('empno')) AND $posted > 0){
							$receipts_count = $this->session->flashdata('receipts_count');
							$this->content_lib->set_message("Successfully posted $posted out of $receipts_count payments!","alert-success");				
						}else{ 
							$this->content_lib->set_message('Something went wrong while posting!','alert-error');						
						}
						break;
				default:
					break;
			}
			
			$this->content_lib->content();
			
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}	
		
		
	}
	
	public function void_receipts() {
	
		$this->content_lib->set_title('Cashier | Receipts | ' . $this->config->item('application_title'));
		$this->load->model('teller/teller_model');
				
		
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
					
			switch ($step) {
				case 1:
					$this->content_lib->enqueue_body_content('cashier/search_or');
					$this->content_lib->content();
					break;	
				case 2:  if ($this->common->nonce_is_valid($this->input->post('nonce'))){
								$or = $this->input->post('or');
								$payertype = $this->teller_model->get_payertype($or);
								
								//print_r($payertype); die();
								
								if($payertype) {
									if ($payertype->students_idno != NULL)
										$type = 'S';
									else if ($payertype->employees_empno != NULL)
										$type = 'E';
									else if ($payertype->other_payers_id != NULL)
										$type = 'O';
									else $type = 'I';
								
								
									$data['details'] = $this->teller_model->get_ORdetails($or, $type);
									if ($data['details']->status == 'unposted') {
										$this->content_lib->enqueue_body_content('cashier/void_receipts',$data);
										$this->content_lib->content(); 
									} else {
										$this->content_lib->set_message("OR is either void or posted!", 'alert-error');
										$this->content_lib->content(); 
									}
								} else {
									$this->content_lib->set_message("No result found for that query", 'alert-error');
									$this->content_lib->content(); 
								}
							 } else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
									$this->content_lib->content();
							}
							//$this->index();
							break;
				case 3: 
						if ($this->common->nonce_is_valid($this->input->post('nonce'))){
							$data['empno'] = $this->session->userdata('empno');
							$data['or'] = $this->input->post('payment_id');
														
							$this->teller_model->void_receipts($data);
							$this->content_lib->set_message("Receipt successfully voided!", 'alert-success');
							$this->content_lib->content(); 
							
						} else {
								$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
								$this->content_lib->content(); 
						}
						//$this->index();
						break;		
			}
					
	}
	
	
	public function student(){
	
		$this->content_lib->set_title('Teller | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
	
		if ( ! empty($query)){
				
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
				
			$this->load->library('pagination_lib');
				
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
	
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
	
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
	
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('teller/teller_model');
			
			$result = $this->teller_model->get_student($idnum);
		
			$this->content_lib->set_title ('Teller | ' . $idnum);
			$bank_codes = $this->input->post('bank_codes');
			$this->content_lib->enqueue_header_script('directPrint');
			
			$tab = 'teller';
			switch ($this->input->post('action')){
				
				case 'change_tax_type'	: 
								$tax_type = $this->input->post('tax_type');
								$this->session->set_userdata(array('tax_type'=> $tax_type));
								break;
				case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;				
				case 'checkout'			:
								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
									
									$items = json_decode($this->input->post('items'));
									$payments = json_decode($this->input->post('payments'));
									$receipt_no = $this->input->post('receipt_no');
									$machine_ip = $this->input->ip_address();
									
									$remarks = '';
									foreach($items as $item){
										$remarks .= $item->remarks . ' ';
									}
									
									$teller_items = array();
									$total_cost = 0;
									foreach ($items as $item){
										$total_cost += $item->amount;
										$item_info = $this->teller_model->teller_code_info($item->teller_code);
										$teller_items[] = $item_info->description . " (" . $item_info->teller_code . ")";
										$teller_item_codes[] = $item_info->teller_code;
									}
									
									$payment_total = 0;
									$payment_methods = array();
									foreach ($payments as $key=>$payment){
										$payment_total += $payment->amount;
										switch ($payment->type){
											case 'cash'	: $payment_methods[] = array(
																	'type'=>'cash');
															break;
											case 'check' : 
															$bank_info = $this->teller_model->bank_info($payment->bank_id);
															$payment_methods[] = array(
																	'type'=>'check',
																	'bank'=>$bank_info->code,
																	'date'=>$payment->check_date,
																	'no'=>$payment->check_number,);
															break;
											case 'credit_card' :
															$payment_methods[] = array(
																	'type'=>'credit card',
																	'bank'=>$payment->credit_card_company,
																	'date'=>$payment->credit_card_expiry,
																	'no'=>$payment->credit_card_number,);
												
															break;
											case 'debit_card' :
															$payment_methods[] = array(
																	'type'=>'debit card',
																	'bank'=>$payment->debit_card_company,
																	'date'=>$payment->debit_card_expiry,
																	'no'=>$payment->debit_card_number,);
															break;
											case 'bank'			:
															$bank_info = $this->teller_model->bank_info($payment->bank_code);
															$payment_methods[] = array(
																'type'=>'bank',
																'bank'=>$bank_info->code,
																'date'=>$payment->date_received,
																);
															break;
											default:
															break;
										}
									}
									
									$change = $payment_total - $total_cost;
									
									if (! is_numeric($receipt_no)) {
										$this->content_lib->set_message('Error(s) seen while posting payment to the database. ' . $receipt_no , 'alert-error');
									} else {
										if ($return = $this->teller_model->checkout($result->payers_id, $this->session->userdata('empno'), $payments, $items, $receipt_no, $machine_ip, $this->session->userdata('tax_type'))) {
											//successfully posted to the database...
											//we generate the receipt...
											$this->load->library('print_lib');
											$this->load->helper('number_words_helper');
											$trace_string = '\*' . $receipt_no . ",{$this->input->ip_address()},{$this->session->userdata('empno')},{$this->session->userdata('lname')}," . substr($this->session->userdata('fname'), 0, 1) . '*\\';
											$this->content_lib->enqueue_body_content('print_templates/receipt', 
													array(
															'id_number'=>$idnum, 
															'date'=>date("M j, Y"), 
															'name'=>$result->lname . ', ' . $result->fname . ' ' . substr($result->mname, 0, 1), 
															'course_year'=>$result->year_level . " " . $result->abbreviation,
															'item_code'=>$teller_item_codes[0],
															'items'=>$teller_items,
															'payment'=>$total_cost,
															'remarks'=>$remarks,
															'trace_string'=>$trace_string,
															'payment_methods'=>$payment_methods,
															));
											$this->content_lib->set_message('Successfully posted transaction.<h2>Last Transaction Change: P ' . number_format($change, 2) . '</h2>', 'alert-success');
											//$this->content_lib->set_message($return, 'alert-success');
										} else {
											//error posting to the database...
											$this->content_lib->set_message('Error(s) seen while posting payment to the database.', 'alert-error');
										}
									}
								} else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
								}
								break;
				default: 
								break;
				
			}
			$this->load->model('teller/teller_model');
			
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->year_level,
						'full_home_address'	=>$result->home_address, 
				);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				$this->load->library('print_lib');
				
				//tab contents
				$this->load->library('tab_lib');
				//teller form
				$bank_codes = $this->teller_model->get_bank_codes();
				$teller_values = array(
						'teller_codes'=>$this->teller_model->get_teller_codes($this->session->userdata('tax_type')), 
						'bank_codes'=>$bank_codes,
						);
				$assessment_values = array(
						
						);
				
				$ledger_data = array();
				$ledger_data = $this->teller_model->get_ledger_data($result->payers_id);
				$this->load->model('academic_terms_model');
				
				$this->tab_lib->enqueue_tab('Teller', 'teller/teller', $teller_values, 'teller', $tab=='teller');
				$this->load->model('accounts/accounts_model');
				$this->load->model('hnumis/enrollments_model');
				
				//assessment details
				$acontent = array(
						'idnumber'	=>$result->idno,
						'familyname'=>$result->lname,
						'firstname'	=>$result->fname,
						'middlename'=>$result->mname,
						'level'		=>$result->year_level,
						'course'	=>$result->abbreviation,
						'due_now' 	=> '',
						'succeeding_dues'	=> '7,756.87'
				);
				
				//assessment form
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				//
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
				} else {
					$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
				}
				$lab_courses = array();
				$courses = array();
				$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
				
				if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab') {
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
									);
						} 
						$courses[] = array(
								'id'=>$course->id,
								'name'=>$course->course_code,
								'units'=>$course->credit_units,
								'pay_units'=>$course->paying_units
								);  
					}
				}
				
				$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment', 
						array(
								'other_courses_payments'=>$other_courses_payments,
								'assessment'=> $assessment,
								'courses'=>$courses,
								'lab_courses'=>$lab_courses,
								'student_inclusive_terms' =>$student_inclusive_terms,
								'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
								'student_details'=>$acontent
						), 
								'assessment', $tab=='assessment');
				
				//enqueue ledger
				$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$ledger_data,
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date
						), 'ledger', $tab=='ledger');
			
				//sidebar...
				$this->content_lib->enqueue_sidebar_widget('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type')), 'Tax Type', 'in');
				
				$receipts = $this->teller_model->get_last_receipts();
				if ($receipts)
					$receipt_no = ($this->session->userdata('tax_type')=='vat' ? $receipts->vat_receipt_no : $receipts->non_vat_receipt_no); else
					$receipt_no = "";
				 
				$this->content_lib->enqueue_sidebar_widget('teller/receipt_number', array('receipt_no'=>$receipt_no), 'Receipt No.', 'in');
				
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				$tab_content = $this->tab_lib->content();
				$this->content_lib->enqueue_body_content("", $tab_content);
	
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}	
		
		$this->content_lib->content();
		
	}
	

	public function report($what, $date=""){
		$this->load->model('teller/teller_model');
		$this->load->library('pdf_lib');
		switch ($what){
			case 'daily_report' :
				$data = array('date'=>$date,);
				if($transactions = $this->teller_model->new_daily_receipt_summary($date)){ 
					//transactions are found today...
					$items = array();
					$payment_methods = array();
					foreach($transactions as $transaction){
						if(!isset($items[$transaction->machine_ip][$transaction->teller_code][$transaction->receipt_no])){
							$items[$transaction->machine_ip][$transaction->teller_code]['transactions'][$transaction->receipt_no] = $transaction;
							$items[$transaction->machine_ip][$transaction->teller_code]['name'] = $transaction->description;
						}
						if(!isset($payment_methods[$transaction->machine_ip][$transaction->payment_method])){
							if(!in_array($transaction->status, array('void', 'deleted')))
								$payment_methods[$transaction->machine_ip][$transaction->payment_method] = $transaction->amount;
						} else {
							if(!in_array($transaction->status, array('void', 'deleted')))
								$payment_methods[$transaction->machine_ip][$transaction->payment_method] += $transaction->amount;
						}
					}
					$content = $this->load->view('teller/pdf/daily_report_content', array('records'=>$items, 'payment_methods'=>$payment_methods), TRUE);
					$this->load->view('teller/pdf_summary_table', array('content'=>$content, 'date'=>$date));
				} else {
					$this->content_lib->enqueue_body_content('', '<h3>This machine has No Transactions Today!</h3>');
					$this->content_lib->content();
				}
				break;
		}
	}

	function search_receipt(){
		$this->load->library('extras_lib');
		$this->extras_lib->search_receipt();
	}

}
