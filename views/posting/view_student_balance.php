<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}
</style>

<div style="background:#FFF; width:920px;" align="center">
<div style="width:100%; margin-bottom:40px;" align="center">	

<table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
	 <thead> 
		<tr class="head">
  			<th colspan="4" class="head" style="text-align:center;">  BALANCE OF STUDENTS (as of <?php echo date('Y-m-d'); ?> ) </th>
  	   </tr>
	   <tr class="head">
  			<th colspan="4" class="head" style="text-align:center;">  <?php print($college->name); ?>  </th>
  	   </tr>
	   <tr class="head">
  			<th colspan="4" class="head" style="text-align:center; ">  <?php echo $program->abbreviation. " - " .$year; ?>  </th>
  	   </tr>
       <tr>
		  <th width=10% class="head" style="vertical-align:middle;">No.</th>
		  <th width="10%" class="head" style="vertical-align:middle;">ID No.</th>
		  <th width="20%" class="head" style="vertical-align:middle;">NAME</th>
		  <th width="10%" class="head" style="vertical-align:middle;">BALANCE</th>
		</tr>
    </thead>
    <?php if ($class_payers) { ?>
			<?php $count = 0; ?>
			<?php foreach ($class_payers AS $payer) { 
				$count += 1;;
				$ledger_data = $this->teller_model->get_ledger_data($payer->payers_id);
				$student = $this->Col_Students_Model->get_student($payer->students_idno);
				$running_balance = 0;
			    if ($ledger_data) {
					foreach ($ledger_data AS $ledge) {
						$running_balance +=  $ledge['debit'] - $ledge['credit'];
					}  ?>
					<tr> 
						<td style="text-align:center; vertical-align:right; "><?php print $count; ?></td>
						<td style="text-align:center; vertical-align:middle; "><?php print($student->students_idno); ?></td>
						<td style="text-align:left; vertical-align:left; "><?php print($student->neym); ?></td>
						<td style="text-align:right; vertical-align:right; "><?php echo ($running_balance < 0 ? "(".number_format(abs($running_balance), 2).")" : number_format($running_balance,2));  ?></td>	
					</tr>
	<?php		}
			}
		  }
			?>
</table>
</div>
</div>


