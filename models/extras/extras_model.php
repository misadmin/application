<?php

class Extras_model extends CI_Model {
	private $last_error;
	
	public function __construct(){
		parent::__construct();
	} 

	
	function list_last_assessment_dates(){
		$sql="
			select a.transaction_date, h.id history_id, s.idno, cap_first(concat(s.fname,' ',s.lname)) student, ap.abbreviation, h.year_level
			from assessments a
				join student_histories h on h.id = a.student_histories_id
				join academic_terms tr on tr.id = h.academic_terms_id
				join students s on s.idno = h.students_idno
				join prospectus p on p.id = h.prospectus_id
				join academic_programs ap on ap.id = p.academic_programs_id				
			where tr.`status` = 'current'
				order by transaction_date, ap.abbreviation,  s.lname, s.fname
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	
	}

	
	function list_added_subjects($date, $history_id){
		$sql = "
			select c.course_code, co.section_code, e.date_enrolled, e.enrolled_by enrolled_by_id
				,if(!isnull(s.idno),cap_first(concat(s.fname,' ',s.lname)), cap_first(concat(substr(emp.fname,1,1),'. ',emp.lname))) enrolled_by_name,
				if(!isnull(e.withdrawn_on),if(e.withdrawn_on <='2013-12-17 23:59:59','WD on or Before 17 Dec 2013','WD After 17 Dec 2013') ,'Added After Assessment') as remark
			from enrollments e
				join course_offerings co on co.id = e.course_offerings_id
				join courses c on c.id = co.courses_id
				join student_histories h on h.id = e.student_history_id
				left join students s on s.idno = e.enrolled_by 
				left join employees emp on emp.empno = e.enrolled_by 
			where  h.id = '{$history_id}' and (e.date_enrolled > '{$date}' or !isnull(e.withdrawn_on))
				order by remark, date_enrolled
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}


	function list_isis_balances(){
		$sql = "
			select * from 
				(
				select l.stud_id, sum(ifnull(l.debit, 0) - ifnull(l.credit,0)) as balance
				from student.ledger l
				where l.status !='void' 
				-- and l.stud_id=05504632
				group by stud_id
				) t
			where t.balance !=0 
			limit 100
		";		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){			
			return $query->result();
		}else{
			return false;
		}
	}

	function list_isis_balances2($idno){
		$sql = "
			select t.*,h.academic_terms_id,h.prospectus_id, h.academic_terms_id, h.year_level, ap.abbreviation  
			from 
				(
				select l.stud_id, sum(ifnull(l.debit, 0) - ifnull(l.credit,0)) as balance
				from student.ledger l
				where l.status !='void' 
					and l.stud_id='{$idno}'
				group by stud_id
				) t
				join (select students_idno, academic_terms_id,prospectus_id,year_level 
						from student_histories 
						where students_idno='{$idno}'
						order by academic_terms_id desc
						limit 1
						) h on h.students_idno = t.stud_id 
					join prospectus p on p.id = h.prospectus_id
					join academic_programs ap on ap.id = p.academic_programs_id 						
			where t.balance !=0 								
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	
	function get_payers_id($idno){
		$sql = "
			select id as payers_id 
			from payers 
			where students_idno = '{$idno}'  
			limit 1
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	}
	
	function list_students($college, $program){
		$sql = "
			select h.students_idno, ap.abbreviation, c.college_code 
			from student_histories h 
			left join prospectus p on p.id = h.prospectus_id
			left join academic_programs ap on ap.id = p.academic_programs_id
			left join colleges c on c.id = ap.colleges_id
			where 
			ap.colleges_id = '{$college}'" 
			. (!empty($program ) ? " and ap.id = '{$program}' " : "") 
			." group by h.students_idno
			order by h.academic_terms_id desc "				
		;
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

}
?>