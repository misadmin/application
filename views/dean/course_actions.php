

<h2 class="heading">Course Offering</h2>
<?php
	$enrollees = 0;
	if ($offerings->enrollee_parallel) {
	  	 $enrollees = $offerings->enrollee_parallel;
	} else {
	  	 $enrollees = $offerings->enrollee;	
	}
?>
<h3><?php print($academic_terms->term." ".$academic_terms->sy); ?></h3>
<h2 id="course_section_text">
<?php print($offerings->course_code."[".$offerings->section_code."] - ".$offerings->descriptive_title); ?>
</h2>
<h4>
	<?php
		$offering_slots = $this->Offerings_Model->ListOfferingSlots($offerings->id,FALSE);
			foreach($offering_slots[0] AS $slots) {
				print("<span style='padding-right:5px;'>".$slots->tym."</span>");
				print("<span style='padding-right:10px;'>".$slots->days_day_code."</span>");
				print("<span style=\"color:green; font-weight:bold;\">Rm.".$slots->room_no."/".$slots->bldg_name."</span><br>");
			}
	?>
	Faculty: <?php print($offerings->employees_empno." ".$offerings->emp_name); ?>
</h4>

<div class="tabbable" style="padding-top: 20px;">
	<ul class="nav nav-tabs">
		<li <?php if ($active=="tab1") print("class=\"active\""); ?>><a href="#tab1" data-toggle="tab">Schedule Slot</a></li>
		<li <?php if ($active=="tab2") print("class=\"active\""); ?>><a href="#tab2" data-toggle="tab">Enrolled/Capacity</a></li>
		<li <?php if ($active=="tab3") print("class=\"active\""); ?>><a href="#tab3" data-toggle="tab">Allocation</a></li>
		<li <?php if ($active=="tab4") print("class=\"active\""); ?>><a href="#tab4" data-toggle="tab">Parallel Course/s</a></li>
		<li <?php if ($active=="tab5") print("class=\"active\""); ?>><a href="#tab5" data-toggle="tab">Assign Faculty</a></li>
		<li <?php if ($active=="tab7") print("class=\"active\""); ?>><a href="#tab7" data-toggle="tab">Dissolve Offering</a></li>
		<li <?php if ($active=="tab8") print("class=\"active\""); ?>><a href="#tab8" data-toggle="tab">Delete Offering</a></li>
	</ul>
			       
	<div class="tab-content">
		<div class="tab-pane <?php if ($active=="tab1") print("active"); ?>" id="tab1" style="padding-left:20px;">
			<?php 
				if (isset($conflict_students)) {
			?>
					<div style="width:65%; padding-bottom:20px;">
					<span style="margin-top:10px; font-weight: bold; font-size: 14px; color: #FF0000;">
					List of students whose schedules are in conflict!</span>
					<table class="table table-hover table-condensed" style="width:100%;">
						<thead>
							<tr style="font-weight:bold; background:#EAEAEA; text-align:center;">
								<th style="width:4%;">No.</th>
								<th style="width:8%;">ID No.</th>
								<th style="width:38%;">Name</th>
								<th style="width:15%;">Course/Year</th>
								<th style="width:15%;">Subject/Section</th>
								<th style="width:20%;">Schedule</th>
							</tr>
						</thead>
					<?php 
						$cnt=1;
						foreach($conflict_students AS $stud) {
					?>
						<tr>
							<td style="text-align:right;"><?php print($cnt); ?></td>
							<td><?php print($stud->idno); ?></td>
							<td><?php print($stud->name); ?></td>
							<td><?php print($stud->abbreviation."-".$stud->year_level); ?></td>
							<td><?php print($stud->course_code."[".$stud->section_code."]"); ?></td>
							<td><?php print($stud->start_time."-".$stud->end_time." ".$stud->days_day_code); ?></td>
						</tr>
					<?php 
							$cnt++;
						}
					?>
					</table>
					</div>
			<?php 
				}
			?>
			
		 	<?php
				if ($offering_slots) {
			?>
					<table class="table table-striped table-hover" style="width:40%;">
						<thead>
							<tr style="font-weight:bold; background:#EAEAEA;">
								<th style="width:45%;">Schedule</th>
								<th style="width:35%;">Room/Building</th>
								<th style="width:10%; text-align:center;">Delete</th>
								<th style="width:10%; text-align:center;">Edit</th>
							</tr>
						</thead>
					<?php 
						$cnt=1;
						foreach($offering_slots[0] AS $slots) {
							$slot_info = $this->Offerings_Model->getCourseOfferings_Slot($slots->id);
					?>
							<tr>
								<td>
									<?php print($slots->tym." ".$slots->days_day_code); ?>
								</td>
								<td>
									<?php print("Rm.".$slots->room_no."/".$slots->bldg_name); ?>
								</td>
								<?php 
									if ($enrollees == 0) {
										if ($cnt > 1) { //prevents the first record to be deleted
								?>
											<td style="text-align:center;">
												<a href="#" class="delete_slot" 
													course_offerings_slots_id="<?php print($slot_info->course_offerings_slots_id); ?>"
													rooms_id="<?php print($slot_info->rooms_id); ?>" 
													days_day_code="<?php print($slot_info->days_day_code); ?>" 
													start_time="<?php print($slot_info->start_time); ?>" 
													end_time="<?php print($slot_info->end_time); ?>" >
													<i class="icon-trash"></i>
												</a>
											</td>
								<?php 
										} else {
								?>
											<td style="text-align:center;">
												&nbsp;
											</td>
								<?php			
											$cnt++;
										}
									} else {
								?>
										<td style="text-align:center;">
											&nbsp;
										</td>
								<?php
									}
								?>	
								<td style="text-align:center;">
									<a href="#" class="edit_schedule_slot" 
										academic_terms_id="<?php print($offerings->academic_terms_id); ?>"
										rooms_id="<?php print($slots->rooms_id); ?>"
										slot_start_time="<?php print($slots->start_time); ?>" 
										old_schedule="<?php print($slots->tym." ".$slots->days_day_code." [Rm.".$slots->room_no."/".$slots->bldg_name."]"); ?>"
										slot_num_hr="<?php print($slot_info->num_hr); ?>"
										slot_day_code="<?php print($slots->days_day_code); ?>"
										course_offerings_slots_id="<?php print($slots->id); ?>"
										old_bldgs_id="<?php print($slots->old_bldgs_id); ?>"
										course_offerings_id="<?php print($offerings->id); ?>" >
										<i class='icon-edit'></i>
									</a>	
								</td>
							</tr>
					<?php 
						}
					?>
					</table>
			<?php 
				}

				if ($enrollees == 0) {
			?>
					<div style="margin-top:10px; font-weight: bold; font-size: 14px; color: #FF0000;">
						<a href="#" class="btn btn-primary offer_new_schedule_slot" style="font-weight: normal;"
							academic_terms_id="<?php print($offerings->academic_terms_id); ?>"
							old_schedule="<?php print($slots->tym." ".$slots->days_day_code." [Rm.".$slots->room_no."/".$slots->bldg_name."]"); ?>"
							course_offerings_id="<?php print($offerings->id); ?>" >
							<i class="icon-plus icon-white"></i> Offer Schedule Slots
						</a>
					</div>
			<?php 
				} else {
			?>
					<div style="margin-top:10px; font-weight: bold; font-size: 14px; color: #FF0000;">
						Schedule slots CANNOT BE ASSIGNED anymore because there are already enrollees for this course!
					</div>
			<?php	
				}
			?>
			
      	</div>
			                
   		<div class="tab-pane <?php if ($active=="tab2") print("active"); ?>" id="tab2" style="padding-left:20px;">
   				<table class="table table-striped" style="width: 200px; font-size: 14px;">
   					<tr>
   						<td style="width: 65%; text-align:left; ">No. of Enrollees</td>
   						<td style="width: 5%; text-align:left;">:</td>
   						<td style="width: 30%; font-weight: bold; text-align:right;"><?php print($enrollees); ?></td>
   					</tr>
   					<tr>
   						<td style="width: 65%; text-align:left; ">Max. Class Size</td>
   						<td style="width: 5%; text-align:left;">:</td>
   						<td style="width: 30%; font-weight: bold; text-align:right;"><?php print($offerings->max_students); ?></td>
   					</tr>
   				</table>
				<div style="margin-top:10px;">
 					<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#edit_class_size"><i class="icon-pencil icon-white"></i> Edit Class Size</a>
   				</div>
   		</div>
			       		
       	<div class="tab-pane <?php if ($active=="tab3") print("active"); ?>" id="tab3" style="padding-left:20px; margin-top: 0px; padding-top:0px;">
     		<?php 
				$block_names = $this->Offerings_Model->ListBlockOfferings($offerings->id);
				if ($block_names) {
					print("<table class=\"table table-bordered\" style=\"width: 80%;\">");
					print("<tr style=\"font-weight:bold; background:#EAEAEA;\">");
					print("<td colspan=\"4\">List of Allocated Blocks</td></tr>");
					$cnt=1;
					foreach($block_names AS $bname) {
						if ($this->uri->segment(2) == 'offerings') {
							if ($cnt < 5) {
								print("<td style=\"width:25%;\">");
								print("<a class='unblock_section' href='$bname->id'><i class='icon-trash' style='padding-right:10px;'></i></a>");
								print($bname->block_name);
								print("</td>");
								$cnt++;
							} else {
								print("</tr><tr><td style=\"width:25%;\">");
								print("<a class='unblock_section' href='$bname->id'><i class='icon-trash' style='padding-right:10px;'></i></a>");
								print($bname->block_name);
								print("</td>");
								$cnt=2;
							}
						} else {
							if ($cnt < 5) {
								print("<td style=\"width:25%;\">".$bname->block_name."</td>");
								$cnt++;
							} else {
								print("</tr><tr><td style=\"width:25%;\">".$bname->block_name."</td>");
								$cnt=2;
							}
						}
					}
					for($i=$cnt; $i<5; $i++) {
						print("<td style=\"width:25%;\">&nbsp;</td>");
					}
					print("</tr></table><p>");
				}
				
				$allocations = $this->Offerings_Model->ListAllocation($offerings->id);
				if ($allocations) {
					print("<table class=\"table table-bordered\" style=\"width: 80%;\">");
					print("<tr style=\"font-weight:bold; background:#EAEAEA;\">");
					print("<td colspan=\"4\">List of Allocated Programs</td></tr>");
					$cnt=1;
					foreach($allocations AS $alloc) {
						if ($this->uri->segment(2) == 'offerings') {
							if ($cnt < 5) {
								print("<td style=\"width:25%;\">");
								print("<a class='deallocate_section' href='$alloc->id'><i class='icon-trash' style='padding-right:10px;'></i></a>");
								print($alloc->allocation_name);
								print("</td>");
								$cnt++;
							} else {
								print("</tr><tr><td style=\"width:25%;\">");
								print("<a class='deallocate_section' href='$alloc->id'><i class='icon-trash' style='padding-right:10px;'></i></a>");
								print($alloc->allocation_name);
								print("</td>");
								$cnt=2;
							}
						} else {
							if ($cnt < 5) {
								print("<td style=\"width:25%;\">".$alloc->allocation_name."</td>");
								$cnt++;
							} else {
								print("</tr><tr><td style=\"width:25%;\">".$alloc->allocation_name."</td>");
								$cnt=2;
							}
						}
					}
					
					for($i=$cnt; $i<5; $i++) {
						print("<td style=\"width:25%;\">&nbsp;</td>");
					}
					print("</tr></table>");
				}
			 ?>
			<div style="margin-top:10px; ">
				<!-- <a class="btn btn-primary" id="allocate_offering" href="<?php print($offerings->id); ?>">Allocate Offering</a> -->
				<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#allocate_offering"><i class="icon-plus icon-white"></i> Allocate Offering</a>
   			</div>
   		</div>
			            
   		<div class="tab-pane <?php if ($active=="tab4") print("active"); ?>" id="tab4" style="padding-left:20px;">
      		<?php
				$parallel_offerings = $this->Offerings_Model->ListParallel_Offerings($offerings->id,$offerings->parallel_no);
				if ($parallel_offerings) {
			?>
					<table class="table table-striped table-bordered table-hover" style="width:50%;">
						<thead>
							<tr style="font-weight:bold; background:#EAEAEA;">
								<th style="width:20%; text-align:center;">Course<br>Code</th>
								<th style="width:10%; text-align:center; vertical-align:middle;">Section</th>
								<th style="width:60%; text-align:left; vertical-align:middle;">Descriptive Title</th>
								<th style="width:10%; text-align:center;">Owner College</th>
							</tr>
						</thead>
			<?php 		
					foreach($parallel_offerings AS $parallel) {
						print("<tr><td style='text-align:left;'>".$parallel->course_code."</td>");			
						print("<td style='text-align:center;'>".$parallel->section_code."</td>");			
						print("<td style='text-align:left;'>".$parallel->descriptive_title."</td>");
						print("<td style='text-align:center;'>".$parallel->college_code."</td></tr>");
					}
			?>		
					</table>
			<?php 
				}
			?>
			<div style="margin-top:10px; ">
				<!--  <a class="offer_parallel" href="">Offer Parallel Course</a> -->
				<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#offer_parallel"><i class="icon-plus icon-white"></i> Offer Parallel Course</a>
   			</div>
       	</div>

   		<div class="tab-pane <?php if ($active=="tab5") print("active"); ?>" id="tab5" style="padding-left:20px;">
   			<div style="font-weight: bold;">
   			Current Faculty Assigned: <?php print($offerings->employees_empno." ".$offerings->emp_name); ?>
   			</div>
			<div style="margin-top:10px; ">
				<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#assign_faculty"><i class="icon-hand-up icon-white"></i> Assign Faculty to Schedule</a>
   			</div>
       	</div>


		<?php 
				//use to print pdf for dissolve students
				
				$list_parallel_offerings = $this->Offerings_Model->ListParallel_Offerings($offerings->id,$offerings->parallel_no,FALSE);
				if (!$list_parallel_offerings) {
					$list_parallel_offerings=json_encode($offerings);	
				} else {
					$list_parallel_offerings=json_encode($list_parallel_offerings);
				}
				$list_dissolve_students=json_encode($students);
				$list_dissolve_students = preg_replace("/'/", "\&#39;", $list_dissolve_students);
				
				$offering_slots_dissolve=json_encode($offering_slots); 
				
		?>
		<div class="tab-pane <?php if ($active=="tab7") print("active"); ?>" id="tab7" style="padding-left:20px;">
			<a href="#" class="btn btn-info" id="download_affected_students" 
				dissolve_students='<?php print($list_dissolve_students); ?>'
				academic_term="<?php print($academic_terms->term." ".$academic_terms->sy); ?>"
				teacher="<?php print($offerings->employees_empno." ".$offerings->emp_name); ?>"
				parallel_offerings='<?php print($list_parallel_offerings); ?>'
				offering_slots='<?php print($offering_slots_dissolve); ?>'
				><i class="icon-arrow-down icon-white"></i> Download Affected Students</a> 
   			<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#dissolve_offering"><i class="icon-file icon-white"></i> List Affected Students</a>
       	</div>

   		<div class="tab-pane <?php if ($active=="tab8") print("active"); ?>" id="tab8" style="padding-left:20px; color: #FF0000; font-weight: bold; "> 

			<?php
				if ($enrollees == 0) {
			?>
					<div style="margin-bottom:10px;">
						NOTE: This process will permanently remove the offering from the list! 
						If incase this offering has a parallel, those parallel courses will NOT BE DELETED!
					</div>

					<a class="btn btn-danger" id="delete_offering" 
							href="<?php print($offerings->id); ?>" 
							parallel_no="<?php print($offerings->parallel_no); ?>" 
							style="font-weight: normal;">
						<i class="icon-trash icon-white"></i>Delete Offering
					</a>
			<?php		
				} else {
			?>
					This offering cannot be deleted anymore because there are already enrollees!
			<?php	
				}
			?>	

       	</div>

   	</div>
</div> <!-- /tabbable -->


<!--  MODAL bodies    -->

  
 <div class="modal hide fade" id="edit_class_size">
	<form action="<?php echo site_url('dean/offerings');?>" method="post" name="edit_class" 
		onSubmit="return CheckClassSize(this); " >
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="step" value="16" />
		<input type="hidden" name="enrollees" value="<?php print($enrollees); ?>" />
   <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">�</button>
       <h3>EDIT CLASS SIZE</h3>
   </div>
   <div class="modal-body">
  		<table class="table">
		    <tr>
      			<td align="left" valign="top">Course [Section]</td>
      			<td align="left" valign="top">:</td>
      			<td align="left"><?php print($offerings->course_code."[".$offerings->section_code."] - ".$offerings->descriptive_title); ?></td>
    		</tr>
    		<tr>
      			<td align="left" valign="top">Schedule</td>
      			<td align="left" valign="top">:</td>
      			<td align="left">
      			<?php 
					if ($offering_slots) {
						foreach($offering_slots[0] AS $slots) {
							print("<span style='padding-right:5px;'>".$slots->tym."</span>");
							print("<span style='padding-right:10px;'>".$slots->days_day_code."</span>");
							print("<span style=\"color:green; font-weight:bold;\">Rm.".$slots->room_no."/".$slots->bldg_name."</span><br>");
						}
					}
				?>
      			</td>
			</tr>
		    <tr>
      			<td align="left" valign="top">No. of Enrollees</td>
      			<td align="left" valign="top">:</td>
      			<td align="left"><?php print($enrollees); ?></td>
    		</tr>
			<tr>
				<td align="left">Class Size</td>
  				<td align="left">:</td>
  				<td align="left">
  				<input name="max_stud" type="text" id="max_stud" maxlength="10" value="<?php print($offerings->max_students); ?>" style="width:25px;" autofocus>
  				</td>
			</tr>
		    <tr>
      			<td align="left" valign="top" colspan="3" style="font-weight: bold; color:#FF0000; font-style:italic;">NOTE: Class size must be greater than the number of enrollees!</td>
    		</tr>
    	</table>
   </div>
   <div class="modal-footer">
	   <input type="submit" name="button" id="button" value="Edit Size!"  class="btn btn-warning" />
       <a href="#" class="btn" data-dismiss="modal">Close</a>
   </div>
   </form>
</div>


<div class="modal hide fade" id="allocate_offering">
	<form action="<?php echo site_url('dean/offerings'); ?>" method="post" name="alloc_off">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="12" />
   <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">�</button>
       <h3>ALLOCATE OFFERING</h3>
   </div>
   <div class="modal-body">
	  	<table class="table" style="width:100%">
			<tr>
		  		<td width="76" align="left">Allocation</td>
		  		<td width="5" align="left">:</td>
		  		<td width="419" align="left" valign="middle">
					<input name="allocation_type" type="radio" class="form" 
							onClick="document.getElementById('blocks').style.display = 'table';
									 document.getElementById('groups').style.display = 'none'; 
									 document.getElementById('programs').style.display = 'none';
									 return;" 
							value="blocks" checked="checked"/>  Block Section
					<input name="allocation_type" type="radio" class="form" 
							onClick="document.getElementById('blocks').style.display = 'none';
									 document.getElementById('groups').style.display = 'table'; 
									 document.getElementById('programs').style.display = 'none';
									 return;" 
							value="program_groups"/>
							Program Groups
					<input name="allocation_type" type="radio" class="form" 
							onClick="document.getElementById('blocks').style.display = 'none';
									 document.getElementById('groups').style.display = 'none'; 
									 document.getElementById('programs').style.display = 'table';
									 return;" 
							value="programs"/> Program/Year Level 
				</td>
				</tr>
				<tr>
				  <td align="left">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td align="left" valign="middle">
					  
				  <table class="table" id="blocks" style="width:100%; display: block;">
					<tr>
      					<td width="50%" align="left">Block Section</td>
					   	<td width="2%" align="left">:</td>
					   	<td width="48%" style="text-align:left;"> 
					   		<select name="blocksection" id="blocksections_id">
					   		<?php  foreach($blocksection as $block) {
									print("<option value= $block->id> ".$block->abbreviation. " - " .$block->yr_level." [ ".$block->section_name." ]</option>");
							} ?>
			      		  </select>
			      		</td>
    				</tr>
    				<tr>
    					<td colspan="3" style="font-weight: bold; color:#FF0000;">NOTE: No block sections will appear if no blocks 
    							are created. To create block sections, go to Academics->Create Block Section.</td> 
    				</tr>
  				</table>
							
				<table class="table" id="groups" style="width:100%; display:none;">
					<tr>
						<td width="50%" align="left">Program Group</td>
        				<td width="2%" align="left">:</td>
        				<td width="48%" style="text-align:left;">
						<select id="groups" name="groups">
							<?php  foreach($program_groups as $p_group) {
										print("<option value=$p_group->id>".$p_group->abbreviation."</option>");
									} 
							?>
						</select>
						</td>
      				</tr>
					<tr>
        				<td width="50%" align="left">Year Level</td>
        				<td width="2%" align="left">:</td>
		        		<td width="48%" style="text-align:left;">
		        		<select name="yearlevel1" id="yearlevel1">
							<option value="0" selected="selected">All Year Levels</option>
							<option value="1">First Year</option>
							<option value="2">Second Year</option>
							<option value="3">Third Year</option>
							<option value="4">Fourth Year</option>
							<option value="5">Fifth Year</option>
						</select>
						</td>
      				</tr>
			   	</table>	
						
				<table class="table" id="programs" style="width:100%; display:none;">
      				<tr>
        				<td width="50%" align="left">Academic Program</td>
			    		<td width="2%" align="left">:</td>
			    		<td width="48%" style="text-align:left;">
						<select id="programs" name="id">
							<?php foreach ($programs as $program): ?>
									<option value="<?php echo $program->id; ?>"><?php echo $program->abbreviation; ?></option>
							<?php endforeach; ?>
						</select>
						</td>
      				</tr>
					<tr>
        				<td width="50%" align="left">Year Level</td>
        				<td width="2%" align="left">:</td>
		        		<td width="48%" style="text-align:left;">
		        		<select name="yearlevel" id="yearlevel">
					    <option value="0" selected="selected">All Year Levels</option>
		   				<option value="1">First Year</option>
					    <option value="2">Second Year</option>
					    <option value="3">Third Year</option>
					    <option value="4">Fourth Year</option>
					    <option value="5">Fifth Year</option>
						</select>
						</td>
      				</tr>
			   	</table>	
		  </td>
	  </tr> 
	</table>
  </div>
   <div class="modal-footer">
	   <input type="submit" name="button" id="button" value="Allocate!" class="btn btn-warning" />
       <a href="#" class="btn" data-dismiss="modal">Close</a>
   </div>
   </form>
</div>


<div class="modal hide fade" id="offer_parallel">
	<form action="<?php echo site_url('dean/offerings'); ?>" method="post" name="parallel_offering">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="create_parallel_offering" />
	<input type="hidden" name="academic_terms_id" value="<?php print($offerings->academic_terms_id); ?>" />
	<div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">�</button>
       <h3>CREATE PARALLEL OFFERING</h3>
   </div>
   <div class="modal-body">
		<table class="table">
		    <tr>
      			<td align="left" valign="top">Course [Section]</td>
      			<td align="left" valign="top">:</td>
      			<td align="left"><?php print($offerings->course_code."[".$offerings->section_code."] - ".$offerings->descriptive_title); ?></td>
    		</tr>
    		<tr>
      			<td align="left" valign="top">Schedule</td>
      			<td align="left" valign="top">:</td>
      			<td align="left">
      			<?php 
					if ($offering_slots) {
						foreach($offering_slots[0] AS $slots) {
							print("<span style='padding-right:5px;'>".$slots->tym."</span>");
							print("<span style='padding-right:10px;'>".$slots->days_day_code."</span>");
							print("<span style=\"color:green; font-weight:bold;\">Rm.".$slots->room_no."/".$slots->bldg_name."</span><br>");
						}
					}
				?>
      			</td>
			</tr>
			<tr>
				<td align="left">Courses</td>
  				<td align="left">:</td>
  				<td align="left">
  				<select name="courses_id" id="courses_id" class="form1">
    				<?php
							foreach($courses AS $course) {
								print("<option value=".$course->id.">".$course->course_code."</option>");	
							}
					?>
  				</select>
  				</td>
			</tr>
    </table>
  </div>
   <div class="modal-footer">
	   <input type="submit" name="button" id="button" value="Create Parallel Offering!" class="btn btn-warning" />
       <a href="#" class="btn" data-dismiss="modal">Close</a>
   </div>
   </form>
</div>


<div class="modal hide fade" id="assign_faculty">
	<form action="<?php echo site_url('dean/offerings'); ?>" method="post" name="assign_faculty">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="4" />
   <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">�</button>
       <h3>ASSIGN FACULTY TO HANDLE SCHEDULE</h3>
   </div>
   <div class="modal-body">
		<table class="table">
		    <tr>
      			<td align="left" valign="top">Course [Section]</td>
      			<td align="left" valign="top">:</td>
      			<td align="left"><?php print($offerings->course_code."[".$offerings->section_code."] - ".$offerings->descriptive_title); ?></td>
    		</tr>
    		<tr>
      			<td align="left" valign="top">Schedule</td>
      			<td align="left" valign="top">:</td>
      			<td align="left">
      			<?php 
					if ($offering_slots) {
						foreach($offering_slots[0] AS $slots) {
							print("<span style='padding-right:5px;'>".$slots->tym."</span>");
							print("<span style='padding-right:10px;'>".$slots->days_day_code."</span>");
							print("<span style=\"color:green; font-weight:bold;\">Rm.".$slots->room_no."/".$slots->bldg_name."</span><br>");
						}
					}
				?>
      			</td>
			</tr>
			<tr>
				<td align="left">Faculty</td>
  				<td align="left">:</td>
  				<td align="left">
  				<select name="employee_empno" id="employee_empno" class="form1">
    				<?php
							foreach($faculty AS $teacher) {
								print("<option value=".$teacher->empno.">".$teacher->neym."</option>");	
							}
					?>
  				</select>
  				</td>
			</tr>
    </table>
  </div>
   <div class="modal-footer">
	   <input type="submit" name="button" id="button" value="Assign Faculty!" class="btn btn-warning" />
       <a href="#" class="btn" data-dismiss="modal">Close</a>
   </div>
   </form>
</div>


<div class="modal hide fade" id="dissolve_offering">
	<form action="<?php echo site_url('dean/offerings'); ?>" method="post" name="dissolve_offering" id="dissolve_section_form" />
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="course_offerings_id" value='<?php print($offerings->id); ?>'>
		<input type="hidden" name="dissolve_students" value='<?php print($list_dissolve_students); ?>'>
		<input type="hidden" name="academic_term" value="<?php print($academic_terms->term." ".$academic_terms->sy); ?>">
		<input type="hidden" name="teacher" value="<?php print($offerings->employees_empno." ".$offerings->emp_name); ?>">
		<input type="hidden" name="parallel_offerings" value='<?php print($list_parallel_offerings); ?>'>
		<input type="hidden" name="offering_slots" value='<?php print($offering_slots_dissolve); ?>'>
		<input type="hidden" name="step" value="dissolve_offering" />
	   <div class="modal-header">
	       <button type="button" class="close" data-dismiss="modal">�</button>
	       <h3>LIST OF STUDENTS</h3>
	   </div>
   <div class="modal-body">
		<table class="table table-striped table-hover" style="width:100%;">
			<thead>
				<tr style="font-weight:bold; background:#EAEAEA;">
					<th style="width:5%;">&nbsp;</th>
					<th style="width:10%; text-align:center; vertical-align:middle;">ID No.</th>
					<th style="width:60%; vertical-align:middle;">Name</th>
					<th style="width:20%; text-align:left; vertical-align:middle;">Course</th>
					<th style="width:5%; text-align:center;">Year<br>Level</th>					
				</tr>
			</thead>
   			<?php 
				if ($students) {
					$cnt=1;
					foreach($students AS $student) {
						print("<tr><td style=\"text-align:center;\">".$cnt."</td>");
						print("<td style=\"text-align:center;\">".$student->idno."</td>");
						print("<td>".$student->neym."</td>");
						print("<td>".$student->abbreviation."</td>");
						print("<td style=\"text-align:center;\">".$student->year_level."</td></tr>");
						$cnt++;
					}
				}
			?>
    </table>
  </div>
   <div class="modal-footer">
	   <input type="submit" name="button" id="dissolve_section" value="Dissolve Offering!" class="btn btn-danger" />
       <a href="#" class="btn" data-dismiss="modal">Close</a>
   </div>
   </form>
</div>

<!--   END OF MODAL BODIES -->


<script>
	$('#dissolve_section').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to dissolve the section?');

		if (confirmed){
			$('#dissolve_section_form').submit();
		} 
		
	});
</script>
  <form id="srcform7" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="step" value="13" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('.unblock_section').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want unblock the section?');

		if (confirmed){
			var block_id = $(this).attr('href');

			$('<input>').attr({
				type: 'hidden',
				name: 'block_id',
				value: block_id,
			}).appendTo('#srcform7');		
			$('#srcform7').submit();
		} 
		
	});
</script>


  <form id="srcform8" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="step" value="14" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('.deallocate_section').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want deallocate the section?');

		if (confirmed){
			var alloc_id = $(this).attr('href');

			$('<input>').attr({
				type: 'hidden',
				name: 'alloc_id',
				value: alloc_id,
			}).appendTo('#srcform8');		
			$('#srcform8').submit();
		} 
		
	});
</script>


<form id="delete_offering_form" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="step" value="delete_offering" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('#delete_offering').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete the course offering?');

		if (confirmed){
			var course_offerings_id = $(this).attr('href');
			var parallel_no = $(this).attr('parallel_no');

			$('<input>').attr({
				type: 'hidden',
				name: 'course_offerings_id',
				value: course_offerings_id,
			}).appendTo('#delete_offering_form');		
			$('<input>').attr({
				type: 'hidden',
				name: 'parallel_no',
				value: parallel_no,
			}).appendTo('#delete_offering_form');		
			
			$('#delete_offering_form').submit();
		} 
		
	});
</script>

  <form id="srcform11" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="step" value="18" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('.edit_additional_charge').click(function(event){
		event.preventDefault(); 
		var status = $(this).attr('status');
				
		$('<input>').attr({
		    type: 'hidden',
		    name: 'status',
		    value: status,
		}).appendTo('#srcform11');
		$('#srcform11').submit();
	});
</script>


<form id="dissolve_students_form" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post" target="_blank">
  <input type="hidden" name="step" value="20" />
</form>
<script>
	$('#download_affected_students').click(function(event){
		event.preventDefault(); 
		var dissolve_students = $(this).attr('dissolve_students');
		var academic_term = $(this).attr('academic_term');
		var teacher = $(this).attr('teacher');
		var parallel_offerings = $(this).attr('parallel_offerings');
		var offering_slots = $(this).attr('offering_slots');

		$('<input>').attr({
			type: 'hidden',
			name: 'academic_term',
			value: academic_term,
		}).appendTo('#dissolve_students_form');		
		$('<input>').attr({
			type: 'hidden',
			name: 'teacher',
			value: teacher,
		}).appendTo('#dissolve_students_form');		
		$('<input>').attr({
			type: 'hidden',
			name: 'parallel_offerings',
			value: parallel_offerings,
		}).appendTo('#dissolve_students_form');			
		$('<input>').attr({
			type: 'hidden',
			name: 'offering_slots',
			value: offering_slots,
		}).appendTo('#dissolve_students_form');			
		$('<input>').attr({
		    type: 'hidden',
		    name: 'dissolve_students',
		    value: dissolve_students,
		}).appendTo('#dissolve_students_form');

		$('#dissolve_students_form').submit();
	});
</script>

  <form id="delete_slot2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="delete_offerings_slot" />
</form>

<script>
	$('.delete_slot').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to remove this schedule slot?');

		if (confirmed){
			var course_offerings_slots_id = $(this).attr('course_offerings_slots_id');
			var rooms_id                  = $(this).attr('rooms_id');
			var days_day_code             = $(this).attr('days_day_code');
			var start_time                = $(this).attr('start_time');
			var end_time                  = $(this).attr('end_time');
		
			$('<input>').attr({
				type: 'hidden',
				name: 'course_offerings_slots_id',
				value: course_offerings_slots_id,
			}).appendTo('#delete_slot2');

			$('<input>').attr({
				type: 'hidden',
				name: 'rooms_id',
				value: rooms_id,
			}).appendTo('#delete_slot2');

			$('<input>').attr({
				type: 'hidden',
				name: 'days_day_code',
				value: days_day_code,
			}).appendTo('#delete_slot2');

			$('<input>').attr({
				type: 'hidden',
				name: 'start_time',
				value: start_time,
			}).appendTo('#delete_slot2');

			$('<input>').attr({
				type: 'hidden',
				name: 'end_time',
				value: end_time,
			}).appendTo('#delete_slot2');

			$('#delete_slot2').submit();
		}
		
	});
</script>


<script language="JavaScript" type="text/javascript">
<!--
  function isblank(s)
	   {
    	   for(var i = 0; i < s.length; i++) {
        	   var c = s.charAt(i);
	           if((c != ' ') && (c != '\n') && (c != '\t')) return false;
    	   }
	       return true;
	   }

	function CheckClassSize(form)
	{

		if ((form.max_stud.value == "") || isblank(form.max_stud.value) || (parseInt(form.max_stud.value) == 0))  {
	    	alert("Please enter Class Size!");
	    	form.max_stud.focus();
    		return false ;
	  	}
		
		if (parseInt(form.max_stud.value) < parseInt(form.enrollees.value)) {
			
			alert("Class Size is less than No. of Enrollees!" );
    		form.max_stud.focus();
    		return false ;
  		}
  		// ** END **
		return true ;
	}
//-->
</script>


<script>
	$('.edit_schedule_slot').click(function(event){
					
		var course_section    		  = $('#course_section_text').html();
		
		var academic_terms_id 		  = $(this).attr('academic_terms_id');
		var rooms_id          		  = $(this).attr('rooms_id');
		var start_time        		  = $(this).attr('slot_start_time');
		var num_hr                    = $(this).attr('slot_num_hr');
		var day_code                  = $(this).attr('slot_day_code');
		var course_offerings_slots_id = $(this).attr('course_offerings_slots_id');
		var schedule				  = $(this).attr('old_schedule');
		var old_bldgs_id			  = $(this).attr('old_bldgs_id');	
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/course_schedules'); ?>",
			data: {	"academic_terms_id": academic_terms_id,
					"rooms_id": rooms_id,
					"start_time": start_time,
					"num_hr": num_hr,
					"day_code": day_code,
					"course_offerings_slots_id": course_offerings_slots_id,
					"old_bldgs_id": old_bldgs_id,
					"action": "extract_vacant_rooms_for_update" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#days_update').html(response.days); 
				$('#rooms_offering_update').html(response.rooms); 
				
			}
		});

		$('#modal_update_schedule').modal('show');
		$('#course_section_update').val(course_section);
		$('#schedule_update').val(schedule);
		
		$('#start_time_update').val(start_time);
		$('#num_hr_update').val(num_hr);
		$('#academic_terms_id_update').val(academic_terms_id);
		$('#old_bldgs_id_update').val(old_bldgs_id);
		$('#course_offerings_slots_id_update').val(course_offerings_slots_id);
		$('#old_rooms_id_update').val(rooms_id);
		
		$('#course_offerings_id').val($(this).attr('course_offerings_id'));
		
	});
</script>		


	<div id="modal_update_schedule" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_schedule_form" action="<?php echo site_url('dean/offerings');?>" method="POST" >
					<?php $this->common->hidden_input_nonce(FALSE); ?>
					<input type="hidden" name="step" value="update_schedule" />
					<input type="hidden" name="academic_terms_id" id="academic_terms_id_update" />
					<input type="hidden" name="course_offerings_id" id="course_offerings_id" />
					<input type="hidden" name="course_offerings_slots_id" id="course_offerings_slots_id_update" />
					<input type="hidden" name="old_bldgs_id" id="old_bldgs_id_update" />
					<input type="hidden" name="old_rooms_id" id="old_rooms_id_update" />

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Schedule</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$this->load->view('dean/modals/update_schedule_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update Schedule!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	
<script>
	$(document).ready(function(){
		$('#update_schedule_form').submit(function() {

		var confirmed = confirm('Continue to update schedule?');

			if (!confirmed){
				return false;
			}
			
		});	

	});
</script>



<script>
	$('.offer_new_schedule_slot').click(function(event){
					
		var course_section    		  = $('#course_section_text').html();
		
		var academic_terms_id 		  = $(this).attr('academic_terms_id');
		var schedule				  = $(this).attr('old_schedule');
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/course_schedules'); ?>",
			data: {	"term_id": academic_terms_id,
					"action": "extract_data_for_new_offerings" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#days_modal').html(response.days); 
				$('#selected_acad_term_id').val(academic_terms_id); 

				$('#rooms_offering_modal').html(response.rooms); 
				
			}
		});

		$('#modal_new_schedule_slot').modal('show');
		$('#course_section_new').val(course_section);
		$('#schedule_new').val(schedule);
		
		$('#academic_terms_id_new').val(academic_terms_id);
		
		$('#course_offerings_id_new').val($(this).attr('course_offerings_id'));
		
	});
</script>		


	<div id="modal_new_schedule_slot" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="offer_new_schedule_slot_form" action="<?php echo site_url('dean/offerings');?>" method="POST" >
					<?php $this->common->hidden_input_nonce(FALSE); ?>
					<input type="hidden" name="step" value="offer_new_schedule_slot" />
					<input type="hidden" name="academic_terms_id" id="academic_terms_id_new" />
					<input type="hidden" name="course_offerings_id" id="course_offerings_id_new" />

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Offer New Schedule Slot</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$this->load->view('dean/modals/offer_new_schedule_slot_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Offer New Schedule Slot!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	
<script>
	$(document).ready(function(){
		$('#offer_new_schedule_slot_form').submit(function() {

		var confirmed = confirm('Continue to offer new schedule slot?');

			if (!confirmed){
				return false;
			}
			
		});	

	});
</script>	
	
	
	