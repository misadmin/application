<?php

function generate_password($num=8){
	
	// 1 and l are removed
	
	if ($num>40)
		$num = 40;
	
	return substr(str_shuffle('qwertyuiopasdfghjkzxcvbnmQWERTYUOASDFGHJKLZXCVBNM023456789'),0,$num);
}