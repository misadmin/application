
			<div class="col1" id="province_state"></div>
			<div class="col2a" id="show_select_province">
				<select name="select_province" id="select_province" class="form-control" style="font-style:italic;" >
					<option value="">--Please Select--</option>
					<option value="new_province" style="font-weight:bold;" id="new_prov_state"></option>
					<?php
						foreach ($provinces AS $province) {
							print("<option value=".$province->id.">".$province->name."</option>");
						}
					?>
				</select>
			</div>

			
<script>
	$(document).ready(function(){

		$("select#select_province").change(function(){

			$('#new_country_group').hide();
			$('#new_province_group').hide();
			$('#edit_town_group').hide();
			$('#edit_brgy_group').hide();

			var country_name = $("select#select_country option:selected").attr('country_name'); 
			var province_id  = $("select#select_province option:selected").attr('value'); 

			if (province_id.length > 0 ) { 

				if (province_id == 'new_province') {
					if (country_name == 'Philippines') {
						$('#new_province_group').show();
						$('#new_province_name').focus();						
					} else {
						$('#new_state_group').show();
						$('#new_state_name2').focus();
					}
				} else {	
					$('#edit_town_group').show();
					
					$.ajax({
						type: "POST",
						url: "<?php echo site_url($this->uri->segment(1).'/update_address');?>",
						data: {"province_id": province_id,
								"action": 'list_towns'},
						cache: false,
						dataType: 'json',
						beforeSend: function () { 
							$('#edit_town_group').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");
						},
						success: function(response) {    
							$("#edit_town_group").html(response.output);
							if (country_name == 'Philippines') {
								$('#town_suburb').html('Town:');
								$('#new_town_suburb').html('New Town');
							} else {
								$('#town_suburb').html('Suburb:');
								$('#new_town_suburb').html('New Suburb');
							}
						}
					});
				}
			}
		});
	});
</script>
			