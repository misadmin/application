<?php

	class College_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListCollegeDepartments($college_id) 
		{
			$result = null;
			$q = "SELECT * 
					FROM 
						departments 
					WHERE 
						colleges_id = {$this->db->escape($college_id)} 
						AND department != 'No Department' 
					ORDER BY
					    department	";
						

			$query = $this->db->query($q);
			if( $query && $query->num_rows() > 0){
				return $query->result();
			} else {
				return FALSE;
			}
		}
		
		
		
		
		function getDepartment($id=null) {
			if($id == null) { return null; }
			
			$result = null;
			
			$q1 = "SELECT *
						FROM 
							departments
						WHERE 
							id = {$this->db->escape($id)} ";
			$query = $this->db->query($q1);

			//print($q1);
			//die();

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		//Added by justing... LIST ALL COLLEGES...
		public function all_colleges ($status='Active'){
			$sql = "
				SELECT
					id, college_code, name
				FROM
					colleges as a
				WHERE 
					a.status = {$this->db->escape($status)}
				ORDER BY
					name
				";
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0)
				return $query->result(); else
				return FALSE;	
		}
		
		/****/
		public function programs_from_college ($college_id, $status=''){
			
			$sql = "
				SELECT
					ap.id, ap.abbreviation, ap.description, ap.status	
				FROM
					academic_programs ap
				LEFT JOIN
					colleges c
				ON
					c.id=ap.colleges_id
				JOIN 
					prospectus pr
				ON 
					pr.academic_programs_id = ap.id
				WHERE
					c.id={$this->db->escape($college_id)}				
				AND 
					not isnull(pr.id) 
				AND
				";
			
			if ( empty($status)){
				$sql .= " ap.status IN ('O','U')\n";
			} else {
				$sql .= " ap.status={$this->db->escape($status)}\n";
			}
			$sql .= "
					GROUP BY 
						ap.id
					ORDER BY 
						ap.abbreviation				
				";
			
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0)
				return $query->result(); else
				return FALSE;
		}


		//ADDED: 02/16/13		
		function ListColleges() {
			$q = "SELECT 
						a.id,
						a.college_code,
						a.name						
					FROM
						colleges AS a
					WHERE 
					    a.status = 'Active'	
					ORDER BY 
						a.college_code";

			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}
		
	//Added:2/23/2013
		function getCollegeName($col_id) {
			$q = "SELECT 
						a.name
				FROM
						colleges AS a
					WHERE 
						a.id = {$this->db->escape($col_id)}";
			
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}	
		
		
		function ListCollegeWithoutAssessment($academic_terms_id) {
			
			$result = NULL;
			
			$q = "SELECT 
						a.id,
						a.college_code,
						a.name						
					FROM
						colleges AS a
					WHERE 
					    a.status = 'Active'	
						AND a.id != 10
						AND a.id 
							NOT IN (SELECT e.colleges_id 
										FROM
											assessments AS b,
											student_histories AS c,
											prospectus AS d,
											academic_programs AS e
										WHERE
											b.student_histories_id = c.id
											AND c.prospectus_id = d.id
											AND d.academic_programs_id = e.id
											AND c.academic_terms_id =  {$this->db->escape($academic_terms_id)}
										GROUP BY
											e.colleges_id )
					ORDER BY 
						a.college_code";
			
			//print($q);
			//die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		
		}
		

		/*
		 * @ADDED: 10/10/14
		 * @author: genes
		 */
		function getCollegebyProspectusID($prospectus_id) {
			$q = "SELECT
						a.colleges_id
					FROM
						academic_programs AS a,
						prospectus AS b
					WHERE
						a.id = b.academic_programs_id
						AND b.id = {$this->db->escape($prospectus_id)}";
				
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
				
		}
		
	}


//end of college_model.php