 <?php if (isset($message) && $tab=='18') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>

<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;"> 
	  <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    	<strong><font face="Verdana, Arial, Helvetica, sans-serif"> UPDATE/DELETE WITHDRAWAL</font></strong>
		   </div>
		   </td>
 	 </tr>
 	 
</table>	

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666;  margin-top:10px;">
  <table width="60%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:60%; padding:0px; margin:10px;">
    <thead>
      <tr>
         <th width="15%" style="text-align:center;">School Year</th>
		<th width="10%" style="text-align:center;">Date Withdrawn</th>
		 <th width="30%" style="text-align:center;">Reason</th>
		  <th width="10%" style="text-align:center;">Update</th>
		    <th width="10%" style="text-align:center;">Delete</th>
       
       </tr>
    </thead>
    <?php
    
	if ($withhdrawals) {
			foreach ($withhdrawals AS $withdraw) {
	?>
    <tr>
      <td style="text-align:left;"><?php print($withdraw->sy); ?></td>
	  <td style="text-align:center;"><?php print($withdraw->withdrawn_on); ?></td>
	  <td style="text-align:left;" ><?php print($withdraw->withdrawn_reason); ?></td>
	<!--    <td style="text-align:center;"><?php// print number_format($gpa->gpa,2); ?></td>-->
	  
      <td style="text-align:center; vertical-align:middle; ">
			 		<a href="#" data-toggle="modal" data-target="#modal_edit_wd_<?php print($withdraw->id); ?>" 
								data-project-id=><i class="icon-edit"></i></a>
	    <!-- modal for EDITING GPA -->
		   
		    <div class="modal hide fade" id="modal_edit_wd_<?php print($withdraw->id); ?>" >
				<form method="post" id="edit_wd_form_<?php print($withdraw->id); ?>">
					<input type="hidden" name="action" value="update_withdrawal" />
			    	<input type="hidden" name="withdraw_id" value= "<?php print($withdraw->id);?>"/>
				 	
				 	<?php $this->common->hidden_input_nonce(FALSE); ?>
				 <div class="modal-header">
				 	<button type="button" class="close" data-dismiss="modal">�</button>
				      		<h3>Edit Withdrawal</h3>
				 </div>
				 <div class="modal-body">
				 			<div style="text-align:center;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									
									<tr>
										<td style="width:85px;"><b> Withdrawn On <i>(mm/dd/yyyy) </i> </b></td>
										<td style="width:10px;">:</td>
										<td>
											<input type="date" name="dateW" value="<?php echo ($withdraw->withdrawn_on); ?>" >
										</td>
									</tr>	
									
									<tr>
										<td style="width:85px;"> <b> Reason </b></td>
										<td style="width:10px;">:</td>
										<td> <input type="text" name="reason" style="width:300px; text-align:left"; value ="<?php print($withdraw->withdrawn_reason);?> "> </td>
									</tr>
									
								</table>
							</div>
				</div>
				<div class="modal-footer">
				    		<input type="submit" class="btn btn-primary" id="edit_wd_<?php print($withdraw->id); ?>" value="EDIT Withdrawal">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
			</form>
		</div>		
				     <!-- End of EDITING GPA -->
								
			</td>
			
			<!-- Modal for deleting withdrawal -->
			
			 <td style="text-align:center; vertical-align:middle; ">
			 		<a href="#" data-toggle="modal" data-target="#modal_delete_wd_<?php print($withdraw->id); ?>" 
								data-project-id='<?php print($gpa); ?>'><i class="icon-trash"></i></a>
	 
		    <div class="modal hide fade" id="modal_delete_wd_<?php print($withdraw->id); ?>" >
				<form method="post" id="delete_wd_form_<?php print($withdraw->id); ?>">
					<input type="hidden" name="action" value="delete_withdrawal" />
			    	<input type="hidden" name="withdraw_id" value= "<?php print($withdraw->id);?>"/>
				 	
				 	<?php $this->common->hidden_input_nonce(FALSE); ?>
				 <div class="modal-header">
				 	<button type="button" class="close" data-dismiss="modal">�</button>
				      		<h3>Delete Withdrawal</h3>
				 </div>
				 <div class="modal-body">
				 			<div style="text-align:center;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									
									<tr>
										<td style="width:100px;"><b> Withdrawn On </b></td>
										<td style="width:10px;">:</td>
										<td>
											<?php echo ($withdraw->withdrawn_on); ?>
										</td>
									</tr>	
									
									<tr>
										<td style="width:100px;"> <b> Reason </b></td>
										<td style="width:10px;">:</td>
										<td> <?php print($withdraw->withdrawn_reason);?>  </td>
									</tr>
									
								</table>
							</div>
				</div>
				<div class="modal-footer">
				    		<input type="submit" class="btn btn-primary" id="delete_wd_<?php print($withdraw->id); ?>" value="DELETE Withdrawal">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
			</form>
		</div>		
				 
			</td>
			
			<!-- End deleting withdrawal -->
		</tr>		
			<?php 		
			}
		?>
	<?php 		
			}else{
		?>	
		<tr>
			  <td colspan = "5">
		  <b>  No Withdrawals </b>
		    </td>
		</tr>
		
	<?php }?>	
	</table>
	
	</div>
   		
<form id="edit_wdr" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="update_withdrawal" />
		
</form>			


<script>
	$('.edit_wd').click(function(event){
		event.preventDefault(); 
	
		     var withdraw_id = $(this).attr('href');
		
	  		$('<input>').attr({
			    type: 'hidden',
			    name: 'withdraw_id',
			    value: withdraw_id,
			}).appendTo('#edit_wdr');
			$('#edit_wdr').submit();
		
	});
</script>


<form id="delete_wdr" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="delete_withdrawal" />
		
</form>			


<script>
	$('.delete_wd').click(function(event){
		event.preventDefault(); 
	
		     var withdraw_id = $(this).attr('href');
		
	  		$('<input>').attr({
			    type: 'hidden',
			    name: 'withdraw_id',
			    value: withdraw_id,
			}).appendTo('#delete_wdr');
			$('#delete_wdr').submit();
		
	});
</script>


<script>
	$(document).ready(function(){
		$('#dateW').datepicker();
	
	});
	</script>
	
	
	