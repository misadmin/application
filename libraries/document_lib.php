<?php 

class Document_lib {
	private $CI;
	private $reflection_obj;
	
	public function __construct($class=NULL){
		$this->CI =& get_instance();
		if (!is_null($class))
			$this->set_class($class);
	}
	
	public function set_class($class){
		$class_name = end(explode('/',$class['name'])); //the object itself less the folder
		
		if (class_exists($class_name)) {
			//echo 'Class exists';
			if ($class_name != 'document_lib')
				$this->reflection_obj = new ReflectionClass ($class_name); else
				$this->reflection_obj = new ReflectionClass ($this);
		} else {
			
			switch ($class['type']) {
			    case 'libraries':
					$this->CI->load->library($class['name']);
					$this->reflection_obj = new ReflectionClass ($this->CI->$class_name);
			        break;
			    case 'controllers':
					$this->CI->load->library('../controllers/' . $class['name']);
					$this->reflection_obj = new ReflectionClass ($this->CI->$class_name);
			        break;
			    case 'models':
					$this->CI->load->model($class['name']);
					$this->reflection_obj = new ReflectionClass ($this->CI->$class_name);
			        break;
			    default:
			    	break;
			}
		}
		//die();
		$this->process_class();
	}
	
	public function process_class(){
		$content = array();
		$content['name'] = $this->reflection_obj->getName();
		$content['filename'] = $this->reflection_obj->getFileName(); //end(explode('\\',$this->reflection_obj->getFileName()));
		$content['description'] = $this->extract_comments($this->reflection_obj->getDocComment());
		$parent = $this->reflection_obj->getParentClass();
		$content['parent'] = ($parent) ? $parent->getName() : '(none)';
		$methods = $this->reflection_obj->getMethods();
		foreach($methods as $method){
			$name = $method->name;
			$description = $this->extract_comments($method->getDocComment());
			$scope = $this->extract_scope($method);
			
			$content['methods'][] = array(
											'name' => $name,
											'scope' => $scope,
											'description' => $description
										);
		}
		
		$attributes = $this->reflection_obj->getProperties();
		
		if ($attributes) {
			foreach ($attributes as $attribute){
				$name = $attribute->getName();
				$scope = $this->extract_scope($attribute);
				$description = $this->extract_comments($attribute->getDocComment());
				
				$content['attributes'][] = array(
												'name' => $name,
												'scope' => $scope,
												'description' => $description
											);
			}
		} else {
			$content['attributes'] = array();
		}
		return $content;
	}
	
	private function extract_comments($comment){
		//$search = array("/**","*/","\t"," * "," *");
		//$replace = array('','','','','');
		$search = array("/**","*/"," *","<?","?>","@var",'@param','@return','Note!');
		$replace = array("","","","&lt;?","?&gt;",'<span class="label label-info">Variable type:</span>','<span class="label label-info">Parameter type:</span>','<span class="label label-info">Return Type:</span>','<span class="label label-important">Note!</span>');
		$lines = explode("\n",str_replace($search,$replace,$comment));
		$string = "";
		foreach ($lines as $line){
			$test = preg_replace('!\s+!','',$line);
			$string .= (!empty($test) ? $line.'<br>' : '');
		}
		//var_dump($lines);
		//die();
		return $string;
	}
	
	private function extract_scope($obj){
		if (!is_object($obj)) {
			return FALSE;
		}
		
		if ($obj->isPublic())
			return 'Public';
		else if ($obj->isPrivate())
			return 'Private';
		else if ($obj->isProtected())
			return 'Protected';
		else if ($obj->isPublic())
			return 'Public';
		else if ($obj->isStatic())
			return 'Static';
		return 'Default';
	}
}