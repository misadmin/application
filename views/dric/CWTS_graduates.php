<div style="margin-top:10px">
<?php if($graduates){ $cnt = 1; ?>
<h2 class="heading"> LIST of <?php print($course_nm." (SY ".$term->sy." ".$term->term."-->".$gender); ?> )</h2>
<h2> <a href="download" class="btn btn-link pull-right" id="download_list"><i class="icon-download-alt"></i> <strong>Download</strong></a> </h2>	

<table class="table table-striped table-bordered">
	<thead >
		<tr align="center">
			<th>Cnt</th>
			<th>Name</th>
			<th>Course</th>
			<th>Birth date</th>
			<th>Age</th>
			<th>Address</th>
			<th>Tel. No.</th>
			<th>Grade</th>
		</tr>
	</thead>
	<tbody> 

	
	<?php foreach ($graduates as $graduate): ?>
		<tr>
			<td><?php print($cnt); ?></td>
			<td><?php print($graduate->sname); ?></td>
			<td><?php print($graduate->abbreviation); ?></td>
			<td><?php print($graduate->dbirth); ?> </td>
			<td><?php print($graduate->age); ?></td>
			<td><?php echo $graduate->home_address; ?></td>
			<td><?php echo $graduate->phone_number; ?></td>
			<td><?php print($graduate->finals_grade); ?></td>
		</tr>
<?php
	$cnt++;
	endforeach; ?>
<?php }else{ ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			    <tr>
					&nbsp;
				</tr>
				<tr>
					<td style="text-align:center; font-weight:bold;">No Graduates on this Term </td>
				</tr>
			</table> 
<?php }?>
	</tbody>
</table>

<form method="post" action="<?php echo site_url("{$role}/download_CWTS_grads_csv"); ?>" id="download_list_form">
			<input type="hidden" name="term_id" value="<?php echo $term->id; ?>" />
			<input type="hidden" name="term" value="<?php echo $term->term; ?>" />
			<input type="hidden" name="sy" value="<?php echo $term->sy; ?>" />
			<input type="hidden" name="gender" value="<?php echo $gender; ?>" />
</form>

<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
		
</div> 
