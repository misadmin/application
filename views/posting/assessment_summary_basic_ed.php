<div style="width:auto;">
<div style="text-align:right">
<a class="download_assess" href="" academic_years_id="<?php print($acad_yr->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>
 <table width="150%" border="0" cellspacing="0" cellpadding="1" class="table table-bordered" id="table-summary">
 	<thead>
  <tr>
    <td colspan="14" align="center" style="text-align:center; font-weight:bold; font-size:16px;">BASIC EDUCATION <br />
      ASSESSMENT SUMMARY<br /> 
	<?php
		print($acad_yr->sy);
	 ?></td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td style="width:10%; text-align:center;">DEPARTMENTS</td>
    <td style="width:6%; text-align:center;">1</td>
    <td style="width:6%; text-align:center;">2</td>
    <td style="width:6%; text-align:center;">3</td>
    <td style="width:6%; text-align:center;">4</td>
    <td style="width:6%; text-align:center;">5</td>
    <td style="width:6%; text-align:center;">6</td>
    <td style="width:6%; text-align:center;">7</td>
    <td style="width:6%; text-align:center;">8</td>
    <td style="width:6%; text-align:center;">9</td>
    <td style="width:6%; text-align:center;">10</td>
    <td style="width:6%; text-align:center;">11</td>
    <td style="width:6%; text-align:center;">12</td>
    <td style="width:6%; text-align:center;">TOTAL</td>
  </tr>
	</thead>
  <?php 
		$grand_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'assess6'=>0,
							'assess7'=>0,'assess8'=>0,'assess9'=>0,'assess10'=>0,'assess11'=>0,'assess12'=>0,
							'total'=>0);
  		foreach($departments AS $department) {
			
			$total = 0;

			$total = $department->assess1+$department->assess2+$department->assess3+
					$department->assess4+$department->assess5+$department->assess6+
					$department->assess7+$department->assess8+$department->assess9+
					$department->assess10+$department->assess11+$department->assess12; 
			$grand_total['assess1'] = $grand_total['assess1'] + $department->assess1;
			$grand_total['assess2'] = $grand_total['assess2'] + $department->assess2;
			$grand_total['assess3'] = $grand_total['assess3'] + $department->assess3;
			$grand_total['assess4'] = $grand_total['assess4'] + $department->assess4;
			$grand_total['assess5'] = $grand_total['assess5'] + $department->assess5;
			$grand_total['assess6'] = $grand_total['assess6'] + $department->assess6;
			$grand_total['assess7'] = $grand_total['assess7'] + $department->assess7;
			$grand_total['assess8'] = $grand_total['assess8'] + $department->assess8;
			$grand_total['assess9'] = $grand_total['assess9'] + $department->assess9;
			$grand_total['assess10'] = $grand_total['assess10'] + $department->assess10;
			$grand_total['assess11'] = $grand_total['assess11'] + $department->assess11;
			$grand_total['assess12'] = $grand_total['assess12'] + $department->assess12;
			$grand_total['total'] = $grand_total['total'] + $total;
  ?>
  <tr>
    <td style="text-align:left; padding-left:30px;"><?php print($department->level); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($department->assess1,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($department->assess2,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($department->assess3,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($department->assess4,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($department->assess5,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($department->assess6,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($department->assess7,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($department->assess8,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($department->assess9,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($department->assess10,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($department->assess11,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($department->assess12,2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($total,2)); ?></td>
  </tr>
  <?php
		}
   ?>
<tr style="background:#EAEAEA;">
    <td style="text-align:right; font-weight:bold;">GRAND TOTAL:</td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess1'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess2'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess3'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess4'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess5'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess6'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess7'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess8'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess9'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess10'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess11'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess12'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['total'],2)); ?></td>
  </tr>
</table>
<div style="text-align:right">
<a class="download_assess" href="" academic_years_id="<?php print($acad_yr->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>

</div>

  <form id="download_assess_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="3" />
</form>
<script>
$(document).ready(function(){
	   $('.download_assess').click(function(event){
			event.preventDefault(); 
			var academic_years_id = $(this).attr('academic_years_id');
				
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_years_id',
				value: academic_years_id,
			}).appendTo('#download_assess_summary_form');
			$('#download_assess_summary_form').submit();
			
		});
	var offset = $('.navbar').height();
    $("#table-summary").stickyTableHeaders({fixedOffset: offset-1});
    $('#table-summary').stickyTableHeaders('updateWidth');
	
});
	   
	   
 </script>
