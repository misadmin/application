<style>
	.table th {text-align:center; vertical-align:middle;}
	td.right {text-align:right}
	td.center {text-align:center}
</style>
<div class="row-fluid span8">
<h3> List of Students with Balances:</h3>
<hr> 
<form action="" class="form-horizontal" id="rsi" method="post">
	<input type="hidden" name="nonce" value="<?php echo $this->common->nonce() ?>" />
	<input type="text"  name="stud_start" placeholder="From Start Name" value="" />
	<input type="text"  name="stud_end" placeholder="To End Name" value="" />
	<button type="submit" class="btn btn-success">Search</button>
</form>



<table id="students_balances" class="table table-bordered table-striped">
    <thead>
		<tr>
			<th width="10%">Payers ID</th>
			<th width="10%">ID No</th>
			<th width="40%">Student</th>
			<th width="30%">Program</th>
			<th width="10%">Balance</th>      
		</tr>
    </thead>

<?php 
	if (isset($balances)){
		//print_r($balances);die();
		foreach ($balances AS $key =>$balance) {
?>
<tr> 
 <td class="center"><?php  echo $key; ?></td> 
 <td class="center"><?php  echo $balance['idno']; ?></td> 
 <td><?php echo $balance['student']; ?></td> 
 <td><?php echo $balance['abbreviation'] . " " . $balance['year_level']; ?></td> 
 <td class="right"><?php  echo number_format($balance['balance'],2); ?></td> 
 
<?php 
		}
	}else{
?>
	<tr><td class="center" colspan="5">No Record found!</td>
<?php 		
	}
?>    
    

</table>

</div>
<script>
$(document).ready(function(){
	var oTable = $("#students_balances").dataTable({
		"iDisplayLength": 100,
		"aLengthMenu": [[10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "All"]],
	});

	oTable.fnSort( [ [3,'asc'], ] );
	

});	

</script>
