
	<h2 class="heading">Students Registered - Not Enrolled</h2>
	<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 35px; margin-bottom:10px; padding:2px;">
			<div style="float:right; overflow:auto;">
				<div id="show_change_data_icon" style="float:left; padding-right:8px; margin-top:7px;">
				</div>
				<div style="float:left; padding-top:3px;" class="b2">
					<select name="academic_terms_id" class="select_change" id="academic_terms_id">
						<?php 
							foreach($terms AS $term) {
						?>
								<option value="<?php print($term->id); ?>" 
									sy="<?php print($term->term.' '.$term->sy); ?>" <?php if($selected_term == $term->id) print("selected"); ?> >
									<?php print($term->term.' '.$term->sy); ?> 
								</option>
						<?php 	
							}
						?>
					</select>
				</div>
				<div style="float:left; padding-top:3px;" class="b2">
					<select name="academic_programs_id" class="select_change" id="academic_programs_id" style="width:auto;">
						<option value="All" 
							description="All Strands" <?php if($selected_strand == 'All') print("selected"); ?> >All Strands</option>
						<?php 
							if ($strands) {
								foreach($strands AS $strand) {
						?>
								<option value="<?php print($strand->id); ?>" <?php if($selected_strand == $strand->id) print("selected"); ?>
									description="<?php print($strand->description); ?>" >
									<?php print($strand->description); ?> 
								</option>
						<?php 
								}
							}
						?>
					</select>
				</div>
			</div>
	</div>

	<div>
		<h3 id="program_desc" style="color:#011B9F; font-style:italic;"></h3>
		<h4 id="program_sy"> </h4>
	</div>

	<div id="show_list_of_students">
		<?php 
			$this->load->view('shs/reports/list_of_students', array('not_sectioned'=>'N') );
		?>
	</div>

	
<script>
	$( window ).load(function() {

		var element1             = $("option:selected", "#academic_terms_id");
		var academic_terms_id    = element1.val();
		var sy                   = element1.attr('sy');
		var element2             = $("option:selected", "#academic_programs_id");
		var academic_programs_id = element2.val();
		var description          = element2.attr('description');

		$('#program_sy').html(sy);
		$('#program_desc').html(description);
	});
</script>
			
		
<script>
	//$(document).ready(function(){
		$('.select_change').bind('change', function(){
			
			var element1             = $("option:selected", "#academic_terms_id");
			var academic_terms_id    = element1.val();
			var sy                   = element1.attr('sy');
			var element2             = $("option:selected", "#academic_programs_id");
			var academic_programs_id = element2.val();
			var description          = element2.attr('description');
			
			$('#show_change_data_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/registered_not_enrolled'); ?>",
				data: {"academic_terms_id": academic_terms_id,
						"academic_programs_id": academic_programs_id,
						"sy": sy,
						"description": description,
						"action": "change_selection" },
				dataType: 'json',
				success: function(response) {													

					$('#show_list_of_students').html(response.output); 
					
					$('#program_desc').html(description); 
					$('#program_sy').html(sy); 

					$('#show_change_data_icon').html('');
					
				}
			});

		});
	//});
</script> 
