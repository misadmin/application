<?php 
	if ($stud_status != 'active') {
		print("<h3>Student NOT Active! Year Level cannot be updated!</h3>");
	} else {
?>
<?php if (isset($message) && $tab=='13') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:10px; ">
 <form method="post" id="update_year">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "assign_yrLevel"/>
	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" > 
	<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
	  <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		   <div class="control-group formSep">
		    <strong><font face="Verdana, Arial, Helvetica, sans-serif"> ASSIGN GRADE/YEAR LEVEL</font></strong>
		   </div>
		   </td>
 	 </tr>
	</table>	

<fieldset>	
	<div class="control" style="margin-left:20px; width:90%; margin-bottom:20px; ">
				<label class="control-label"> <b> For Academic Year: </b> </label>
					<div class="controls" >
						<select id="academic_year_id" name="academic_year_id">
						<?php
							foreach($academic_years AS $academic_year) {
								print("<option value=".$academic_year->id.">".$academic_year->start_year."-".$academic_year->end_year."</option>");	
							}
						?>
						</select>
					</div>
		</div>
	
		<div class="control-group" style="margin-left:20px; width:90%; margin-bottom:20px; ">
		    <label class="control-label" for="level"><b>Level&nbsp;&nbsp;: </b></label>
		    <div class="controls">
			    <select id="levels" name="level_id" class="form">
					<option value="">-- Select Level --</option>
						<?php foreach ($levels as $level): ?>
						
						<option value="<?php echo $level->id;?>" <?php if ($level->id == $levels_id) print("selected"); ?>> <?php echo $level->level; ?></option>
						<?php endforeach; ?>
				</select>
	    	</div>
	    </div>
	    
	     <div class="control-group" style="margin-left:20px; width:90%; margin-bottom:20px; ">
		    <label class="control-label" for="grade_level"> <b> Grade/Year Level&nbsp;&nbsp;:</b></label>
		    <div class="controls">
			    <input type="text" name="year" class="year input-xlarge" style="width:30px; text-align:center">
	    	</div>
	    </div>
	
		<div class="control-group" style="margin-left:20px; width:90%; margin-bottom:20px; ">
					<div class="controls">
						<button class="btn btn-success" type="submit">Assign Grade/Year</button>
					</div>
		</div>	
  </fieldset>
</form>
 </div>
 <?php 
	}
 ?>
 
 
 <script>
$('#levels').change(function(){
	var level_id = $('#levels option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/year"); ?>/?level=" + level_id,
		dataType: "json",
		success: function(data){
			var doptions = make_options_year(data);
			$('#year').html(doptions);
		}	
	});	
});


function make_options_year (data){
var doptions = '<option value="">-- Select Year --</option>';
for (var i = 0; i < data.length; i++) {
	doptions = doptions 
	 	+ '<option value="'
	 	+ data[i].yr_level
	 	+ '">'
	 	+ data[i].yr_level
	 	+ '</option>';
}
return doptions;
}
</script>
 
 
 <script>
$(document).ready(function(){
	$('#update_year').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			amount: { required: true, minlength: 1 },
			type: {required: true},
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	$(".year").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});
</script>
 