
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Code</div>
			<div class="col2a">
				<select class="form-control" style="width:150px; height:auto;" id="course_id_add_modal" >
					<?php 
						if ($subjects) {
							foreach ($subjects AS $subject) {
					?>		
								<option value="<?php print($subject->id); ?>" descriptive_title="<?php print($subject->descriptive_title); ?>"><?php print($subject->course_code); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Subject</div>
			<div class="col2a">
				<input type="text" id="subject_description" class="form-control" value="<?php print($subjects[0]->descriptive_title); ?>" disabled />
			</div>
		</div>
		
		<div style="width:100%">
			<div class="col1">Course Type</div>
			<div class="col2a">
				<select class="form-control" style="width:150px; height:auto;" id="stype_add_modal" >
					<?php 
						if ($s_types) {
							foreach ($s_types AS $stype) {
					?>		
								<option value="<?php print($stype->id); ?>" ><?php print($stype->description); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>

	
	<script>
		$("select#course_id_add_modal").change(function(){

			var descriptive_title = $("select#course_id_add_modal option:selected").attr('descriptive_title');
			
			$("#subject_description").val(descriptive_title);
		});
	
	</script>

