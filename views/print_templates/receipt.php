<pre id="receipt_to_print" style="display:none;"><?php
echo $this->print_lib->init();
if($this->session->userdata('printed') == 'yes')
	echo $this->print_lib->set_page_length_in_lines(33); else {
	echo $this->print_lib->set_page_length_in_lines(34);
	$this->session->set_userdata('printed', 'yes');
}
	
$counter = 0;
foreach ($items as $item){
	$remarks_arr = explode("\n", $remarks[$counter]);
?><?php
//this should be placed outside the html tag or body to be accessible, maybe after $this->content_lib->content();
//echo sprintf($this->config->item('jzebra_applet'), base_url()); ?>





<?php echo $this->print_lib->bold() . $this->print_lib->write_receipt($date, 58 /*. " " . $item_codes[$counter])*/) . "\n\n\n"; ?>
<?php echo $this->print_lib->bold() . $this->print_lib->write_receipt($names[$counter], 38) . "\n\n\n"; ?>
<?php 
// $from_left = 26 - strlen($course_year);
$from_left = 26 - strlen(substr($course_year, 0, 14));
echo $this->print_lib->bold() . $this->print_lib->write_receipt(substr($course_year, 0, 14), 38);
for($count = 1; $count < $from_left; $count++)
 echo " ";

preg_match ('!\((.*?)\)!', $items[$counter], $rest);
echo $id_number . "\n\n\n\n\n"; ?>
<?php echo $this->print_lib->condensed() . $this->print_lib->write_receipt("*" . convert_number_to_words($payments[$counter]) . "*", 70) . "\n\n\n"; ?>
<?php echo $this->print_lib->no_condensed() . $this->print_lib->bold() . $this->print_lib->write_receipt(number_format($payments[$counter],2), 15) . '                                     ' . str_pad(number_format($payments[$counter],2), 12, '*', STR_PAD_LEFT) . "\n\n"; ?>
<?php echo $this->print_lib->bold() . $this->print_lib->write_receipt((strlen($items[$counter]) > 35 && isset($rest[1]) ? $rest[1] : substr($items[$counter], 0, 35)), 45) . "\n"; ?>
<?php  $has_cash = FALSE; $has_check = FALSE; $has_card = FALSE;
foreach ($payment_methods as $payment_method){
	if($payment_method['type']=='cash')
		$has_cash = TRUE;

	if($payment_method['type']=='check')
		$has_check = TRUE;
	
	if($payment_method['type']=='credit card' || $payment_method['type']=='debit card')
		$has_card = TRUE;
}
$lines = 21;
foreach($remarks_arr as $remark){echo $this->print_lib->condensed(); echo $this->print_lib->write_receipt($remark, 90). "\n"; echo $this->print_lib->no_condensed();
$lines++;
} echo "\n\n"?>
<?php
$lines = $lines + 2;
$extra = 0;
$remarks_count = 0;
$data = array();
if (count($payment_methods) >= count($remarks_arr)) {
	foreach ($payment_methods as $payment_method){
		$extra++;
		$remarks_count++;
		$data[] = array('remarks'=>(empty($remarks_arr[$remarks_count])? " " : $remarks_arr[$remarks_count]), 'payment'=>$payment_method['type'], 'checkdate'=>(isset($payment_method['date'])?$payment_method['date']:''), 'bank'=>(isset($payment_method['bank'])?$payment_method['bank']:''), 'check_no'=>(isset($payment_method['no'])?$payment_method['no']:''));
	} 
} else {
	$payment_count = 0;
	foreach ($remarks_arr as $fremark){
		$extra++;
		$remarks_count++;
		$data[] = array('remarks'=>(empty($fremark[$remarks_count])?" " : $fremark[$remarks_count]), 'payment'=>(isset($payment_methods[$payment_count]['type']) ? $payment_methods[$payment_count]['type'] : ""), 'checkdate'=>(isset($payment_methods[$payment_count]['date'])?$payment_methods[$payment_count]['date']:''), 'bank'=>(isset($payment_methods[$payment_count]['bank'])?$payment_methods[$payment_count]['bank']:''), 'check_no'=>(isset($payment_methods[$payment_count]['no'])?$payment_methods[$payment_count]['no']:''));
		$payment_count++;
	}
}
$this->print_lib->set_data($data);
//echo $this->print_lib->draw_table(120, " ", TRUE) . "\n";?>
<?php 
//$this->print_lib->clear_table();
/*
$this->print_lib->set_table_header(array(array('id'=>'remarks', 'name'=>" ", 'width'=>40), array('id'=>'payment', 'name'=>$trace_strings[$counter], 'width'=>60)));
echo $this->print_lib->draw_table(120, " ");
echo "\n";
*/
echo $this->print_lib->condensed() . $this->print_lib->write_receipt($trace_strings[$counter], 70);
$lines++;
echo $this->print_lib->no_condensed();
/*
	for ($count = $lines; $count < 30; $count++){
		echo "\n";
	}
echo "\n\n."; */ 
$counter++;
//this line is temporarily replaced with hardcoded \n\n\n\n\n\n\n -this is seven lines
echo $this->print_lib->char_form_feed();
//echo '\n\n\n\n\n\n\n'; //seven lines

} 
?></pre>
<script>
function print(content) {
    var applet = document.jzebra;
    if (applet != null) {
       applet.append('\x1B\x40');
       applet.append(content);
       applet.print();
	}
}
    
$(document).ready(function(){
	var content_to_print = $('#receipt_to_print').text();	
	var applet = document.jzebra;
	
	if (applet != null) {
		// Searches for default printer
		applet.findPrinter();
	}
	print(content_to_print);
	$("#reprint_receipt").on('click', function(){
		print(content_to_print);
	});
});
</script>
