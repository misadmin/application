<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<div style="background:#FFF; width:40%; text-align:center; margin:auto;">
	<form action=" <?php echo site_url('dean/masterlist'); ?>" method="post">  <?php echo validation_errors(); ?>
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="step" value="3" />
				<input type="hidden" name="academic_term" value="<?php print($academic_term->id)?>" />
	<table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  		<tr class="head">
  			<td colspan="3" class="head" style="text-align:center; padding:10px;">VIEW STUDENTS </td>
    	</tr>
    	<tr>
    		<td colspan="3">
				<table border="0" cellpadding="4" cellspacing="0" class="inside" style="width:100%">
					<tr>
        				<td width="30%" align="left">Academic Program</td>
			        	<td width="2%" align="left">:</td>
			        	<td width="68%" style="text-align:left;">
							 <select id="programs" name="programs" class="form">
								<?php foreach ($programs as $program): ?>
										<option value="<?php echo $program->id; ?>"><?php echo $program->abbreviation; ?></option>
								<?php endforeach; ?>
							</select>
						</td>
      				</tr>
					<tr>
        				<td width="30%" align="left">Year Level</td>
        				<td width="2%" align="left">:</td>
		        		<td width="68%" style="text-align:left;"><select name="yearlevel" id="yearlevel" class="form">
						    <option value="1" selected="selected">First Year</option>
						    <option value="2">Second Year</option>
						    <option value="3">Third Year</option>
						    <option value="4">Fourth Year</option>
						    <option value="5">Fifth Year</option>
						  </select> </td>
      				</tr>
			   	</table>	
			</td>
		</tr> 
		
		<tr>
      		<td colspan="3" class="head" style="text-align:center;"><input type="submit" name="button" id="button" value="View!" class="btn btn-success" /></td>
		</tr>
	</table>
</div>

</form>


