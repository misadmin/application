<?php 
 
	class For_fix_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		private $num_rows = 0;
		
		
		function get_num_rows() {
			return $this->num_rows;
		}

		
		function ListStudents($student_type=NULL) {
			$result = null;
			
			$q = "SELECT 
						idno,
						meta
					FROM
						students
					WHERE ";
			
			if ($student_type) {
				$q .= " student_type = {$this->db->escape($student_type)} ";
			} else {
				$q .= " student_type IS NULL ";
			}
			
			$q .= " AND meta IS NOT NULL ";	
			
			//die($q);
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$this->num_rows = $query->num_rows();
				$result         = $query->result();
			} 
			
			return $result;
			
		}

		
		function Add_Emergency_info($data) {

			$q1 = "INSERT INTO
						emergency_info (
										students_idno,
										emergency_notify,
										emergency_relation,
										emergency_telephone,
										emergency_email_address,
										barangays_id,
										encoded_by)
				VALUES (
						{$this->db->escape($data['students_idno'])},
						{$this->db->escape($data['emergency_notify'])},
						{$this->db->escape($data['emergency_relation'])},						
						{$this->db->escape($data['emergency_telephone'])},
						{$this->db->escape($data['emergency_email_address'])},
						{$this->db->escape($data['barangays_id'])},
						{$this->db->escape($data['encoded_by'])}) 
				ON DUPLICATE KEY UPDATE 
						emergency_notify = {$this->db->escape($data['emergency_notify'])},
						emergency_relation = {$this->db->escape($data['emergency_relation'])},
						emergency_telephone = {$this->db->escape($data['emergency_telephone'])},
						emergency_email_address = {$this->db->escape($data['emergency_email_address'])},
						barangays_id = {$this->db->escape($data['barangays_id'])},
						encoded_by = {$this->db->escape($data['encoded_by'])} ";

			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
								
		}
		
		
		function getStudent($students_idno=NULL) {
			$result = null;
			
			$q = "SELECT 
						idno,
						meta
					FROM
						students
					WHERE 
						idno = {$this->db->escape($students_idno)} 
						AND meta IS NOT NULL ";	
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
	}

?>