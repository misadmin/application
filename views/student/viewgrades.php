<?php if (isset($terms) && count($terms) > 0): ?>
<?php foreach($terms as $term): ?>
<h4 style="margin-top: 20px;"><?php echo $term['school_year']; ?> <?php echo $term['term']; ?> (<?php echo $term['program'] . " " . $term['year_level']; ?>)</h4>
<table class="table table-bordered mediaTable activeMediaTable" id="MediaTable-0">
	<thead>
		<tr>
			<th width="14%">Catalog Number</th>
			<th width="38%">Descriptive Title</th>
			<th width="6%">Prelim</th>
			<th width="6%">Midterm</th>
			<th width="6%">Finals</th>
			<th width="20%">Teacher</th>
			<th width="10%">Credit Units</th>
		</tr>
	</thead>
	<tbody>
<?php if(isset($term['courses']) && count($term['courses']) > 0): ?>
<?php
	foreach($term['courses'] as $subject): ?>
		<tr <?php if ($subject['course']->finals_grade > 3.0 || ! is_numeric ($subject['course']->finals_grade)) echo 'class="error" '; ?>>
			<td><?php //echo $subject['course']->course_code; 
			print ("<a class=\"course\" href=\"{$subject['course']->courseid}\"> {$subject['course']->course_code} </a>"); ?> </td>
			<?php //print("<a class=\"schedule_slot\" href=\"{$offer->id}\">{$offer->course_code}[{$offer->section_code}]</a>"); ?>
			<td><?php echo $subject['course']->descriptive_title; ?></td>
			<td style="text-align:center;"><?php echo $subject['course']->prelim_grade; ?></td>
			<td style="text-align:center;"><?php echo $subject['course']->midterm_grade; ?></td>
			<td style="text-align:center;"><?php echo $subject['course']->finals_grade; ?></td>
			<td><?php echo $subject['course']->instructor; ?></td>
			<td style="text-align:center;"><?php echo $subject['course']->credit_units; ?></td>
		</tr>
<?php endforeach; ?>
<?php endif; ?>
	</tbody>
</table>
<?php endforeach; ?>
<?php else: ?>
	<h3>No Records Found</h3>
<?php endif;?>

<form id="srcform2" action="<?php echo site_url("dean/creditcourse")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="1" />
</form>
<script>
	$('.course').click(function(event){
		event.preventDefault(); 
		var course_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_id',
		    value: course_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>