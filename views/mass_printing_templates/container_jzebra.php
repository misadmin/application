<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title; ?></title>
		<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
		
		<style>
		#content {width:400px;}
		.btn {
			margin-left:150px;
			margin-top:150px;
			height: 30px;
			width: 100px;
			color: white;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #007500;
			background-image: -webkit-gradient(linear,0 0,0 100%,from(#008C00),to(#007500));
			background-image: -webkit-linear-gradient(top,#008C00,#007500);
			background-image: -o-linear-gradient(top,#008C00,#007500);
			background-image: linear-gradient(to bottom,#008C00,#007500);
			background-image: -moz-linear-gradient(top,#008C00,#007500);
			background-repeat: repeat-x;
			border-color: #008C00 #008C00 #007500;
			filter: progid:dximagetransform.microsoft.gradient(startColorstr='#008C00',endColorstr='#007500',GradientType=0);
			filter: progid:dximagetransform.microsoft.gradient(enabled=false);
		}
		.btn.disabled, .btn[disabled] {
			cursor: default;
			background-image: none;
			opacity: .65;
			filter: alpha(opacity=65);
			-webkit-box-shadow: none;
			-moz-box-shadow: none;
			box-shadow: none;
		}
		</style>
	</head>
	<body>
	<?php echo $content; ?>
	<div id="content">
		<button class="btn">Print</button>
	</div>
	<script>
	function print(content) {
	    var dapplet = document.jzebra;
	    if (dapplet != null) {
	    	dapplet.append('\x1B\x40');
	       dapplet.append(content);
	       dapplet.print();
		}
	}
	$(document).ready(function(){
		$('.btn').click(function(){
			var applet = document.jzebra;
			var content_to_print = $('#to_print').text();
			var toprint_array = content_to_print.split(">");			
			console.log(toprint_array);  //this is in preparation for a from-to printing in cases of 
			                             //paper jams - Toyet 1.11.2019
			if (applet != null) {
				// Searches for default printer
				applet.findPrinter();
			}

			print(content_to_print);
		});	
	});
</script>
	</body>
</html>
<applet name="jzebra" code="jzebra.PrintApplet.class" archive="<?php echo base_url('assets/jzebra.jar'); ?>" width="1px" height="1px">
	<param name="printer" value="zebra">
</applet>