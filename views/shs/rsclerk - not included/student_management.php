

	<div class="tabbable" style="width:99%; margin:0 auto; margin-top:15px;" id="show_extracted_items" >
		<ul class="nav nav-tabs">
					<li class="dropdown <?php if($active_tab == 'student_info') print("active"); ?>" >
						<a href="#student_info" data-toggle="tab">Student Information</a>
					</li>
					<li class="dropdown <?php if($active_tab == 'assign_lrn') print("active"); ?>" >
						<a href="#assign_lrn" data-toggle="tab">Assign LRN</a>
					</li>
		</ul>
		
	
		<div class="tab-content" >
			<div class="tab-pane <?php if($active_tab == 'student_info') print("active"); ?>" id="student_info">
				<?php
					$this->load->view('common/student_information_full');
				?>
			</div>

			<div class="tab-pane <?php if($active_tab == 'assign_lrn') print("active"); ?>" id="assign_lrn">
				<?php
					$this->load->view('shs/student/assign_lrn');
				?>
			</div>
					
		</div>

	</div>

	