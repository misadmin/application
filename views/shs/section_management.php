<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>

<div id="show_section_management_page" >
	<h2 class="heading">Section Management</h2>
	<div id="rounded_header" style="width:<?php print($various['page_width1']); ?>; font-size:12px; <?php print($various['page_align']); ?> margin-top:15px; padding:0px;">
		<div id="container">
			<div id="left" >
				<div id="show_error_msg" style="padding-left:10px; padding-top:10px; color:#F92424; font-weight:bold; font-size:14px;">
					
				</div>
			</div>
			<div id="right">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<?php 
					if ($various['can_edit']) {
				?>
				<div style="float:left; padding-top:4px; padding-right:4px; font-size:14px;" >
					<a href="#" class="btn btn-success add_section" >
						<i class="icon-plus-sign" style="margin-top:2px;"></i> New Section
					</a>
				</div>
				<?php	
					}
				?>
					<div style="float:left; padding-top:5px;" class="b2">
						<select name="grade_level" class="form-control change_select" style="width:auto; height:auto;" id="select1">
							<option value="0" <?php if ($grade_level == 0) print("selected"); ?>>All Grade Levels</option>
							<option value="11" grade_text="Grade 11" <?php if ($grade_level == 11) print("selected"); ?>>Grade 11</option>
							<option value="12" grade_text="Grade 12" <?php if ($grade_level == 12) print("selected"); ?>>Grade 12</option>
							
						</select>
					</div>
					<div style="float:left; padding-top:5px;" class="b2">
						<select name="term_id" class="form-control change_select" style="width:auto; height:auto;" id="select2">
							<?php 
								if ($terms) {
									foreach ($terms AS $term) {
							?>		
										<option value="<?php print($term->id); ?>" 
											term_text="<?php print($term->term." ".$term->sy); ?>"
											<?php if ($term_id == $term->id) print("selected"); ?>>
											<?php print($term->term." ".$term->sy); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
				
			</div>
		</div>
	</div>

	
	<div style="width:<?php print($various['page_width2']); ?>; overflow:auto; <?php print($various['page_align']); ?> margin-top:15px;" id="show_extracted_items" >
		<?php
			$data = array(
					"sections"=>$sections,
			);
			
			if ($various['can_edit']) {
				$this->load->view('shs/sections/list_all_sections',$data);			
			} else {
				$this->load->view('shs/sections/list_all_sections_for_reports',$data);							
			}
		?>
	</div>
</div>



<script>
	$('.add_section').click(function(event){
		
		$('#modal_new_section').modal('show');
		
		var element1    = $("option:selected", "#select1");	
		
		if (element1.val() == 0) {
			$('#grade_level_modal').val(11);
			$('#grade_text_modal').val('Grade 11');
		} else {
			$('#grade_level_modal').val(element1.val());
			$('#grade_text_modal').val(element1.attr('grade_text'));
		}
		
		var element2  = $("option:selected", "#select2");
		$('#term_id_modal').val(element2.val());
		$('#term_text_modal').val(element2.attr('term_text'));

		$('#section_name_modal').val('');
	});
</script>				


	<div id="modal_new_section" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_section_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">New Section</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="term_id_modal"></div>
						<?php
							$data['strands'] = $strands;
							$this->load->view('shs/sections/modal_view/new_section_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Section!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<script>
	$('#add_section_form').submit(function(e) {
		e.preventDefault();
		
		var grade_level  = $('#grade_level_modal').val();
		var term_id      = $('#term_id_modal').val();
		var strand_id    = $('#strand_id_add_modal').val();
		var section_name = $('#section_name_modal').val();

		$('#modal_new_section').modal('hide');
		
		$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/section_management'); ?>",
			data: {"grade_level": grade_level,
					"term_id": term_id,
					"strand_id": strand_id,
					"section_name": section_name,
					"action": "add_section" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_section_management_page').html(response.output); 
				
				$('#show_add_icon').html('');
				
			}
		});
		
	});
</script>				


<script>
	$(document).ready(function(){
		
		$('.change_select').bind('change', function(){
			
			var element1    = $("option:selected", "#select1");
			var grade_level = element1.val();

			var element2    = $("option:selected", "#select2");
			var term_id     = element2.val();

			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/section_management'); ?>",
				data: {"grade_level": grade_level,
						"term_id": term_id,
						"action": "change_select" },
				dataType: 'json',
				success: function(response) {													

					$('#show_extracted_items').html(response.output); 

					$('#show_add_icon').html("")
										
				}
			});

		});
	});
</script> 

<!--This script forces the select to go back to default once Back button of browser is pressed-->
<script>
$(document).ready(function () {
    $("select").each(function () {
        $(this).val($(this).find('option[selected]').val());
    });
})
</script>