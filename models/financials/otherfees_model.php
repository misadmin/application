<?php

	class Otherfees_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


	//Added: 2/20/2013
		function AddOther_Fee($data) 
		{
			foreach($data['academic_programs'] as $program)
			{
				$query = "INSERT INTO other_fees (id, academic_terms_id, academic_programs_id, other_fee_items_id, rate) 
   						      VALUES ('',
									   {$this->db->escape($data['academic_terms_id'])},
									   {$this->db->escape($data['otherfee_item_id'])},
									   {$this->db->escape($program->id)},
									   {$this->db->escape($data['rate'])})";
				$this->db->query($query);					
			}						
			
			$id = @mysql_insert_id();
			//print($query);
			//die();
			return $id;
	}
	
	//added: 2/20/2013			
		function ListOtherFeeItems() {
			$result = null;
				
			$q1 = "SELECT
					id,
					description
				   FROM
					 other_fee_items
				   ORDER BY 
					  description";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}					
		
//Added:2/18/2013

	function AddNew_Fee_Item($item_name) {
	   //wala pa ni nahuman 
		$query = "INSERT INTO misc_items (id, description) 
   						      VALUES ('',
									  {$this->db->escape($item_name)})";
				$this->db->query($query);					
								
			
			$id = @mysql_insert_id();
			//print($query);
			//die();
			return $id;
	}
		
}

//end of college_model.php