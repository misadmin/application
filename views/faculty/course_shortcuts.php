<?php if(count($courses) > 0):?>
<ul>
<?php foreach ($courses as $course): ?>
	<li><a class="course_id" href="<?php echo $course->course_offerings_id; ?>"><?php echo $course->course_code . " " . $course->section_code; ?></a>
<?php endforeach; ?>
</ul>
<?php else: ?>
	No Courses in this Term
<?php endif; ?>
<form method="post" id="course_shortcut" action="">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="academic_terms_id" value="<?php echo $academic_terms_id; ?>" />
<?php if(isset($action)) : ?>
	<input type="hidden" name="action" value="<?php echo $action; ?>" />
<?php endif; ?>
</form>
<script>
	$('a.course_id').click(function(event){
		event.preventDefault(); 
		var courseid = $(this).attr('href');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_offering_id',
		    value: courseid,
		}).appendTo('#course_shortcut');
		$('#course_shortcut').submit();		
	});
</script>