<style>
	td.grade_header {
		background-color:#C3C3C3;
		font-size:bold;
		text-align:center;
		font-size:12px;
		vertical-align:middle;
		height:auto;
		padding:2px;
	}

</style>


					<div style="width:100%; overflow:auto; margin-top:2px; margin-bottom:25px;" id="show_remark_here">
						<table class="table table-hover table-bordered table-striped table-condensed" >
							<thead>
								<tr style="text-align:center; font-size:11px; font-weight:bold;"  class="shs_header"  >
									<td class="grade_header" rowspan="2" style="width:10%;">Course<br>Code</td>
									<td class="grade_header" rowspan="2" style="width:37%;">Descriptive Title</td>
									<td class="grade_header" rowspan="2" style="width:25%;">Teacher</td>
									<td class="grade_header" colspan="2" style="width:12%;">Quarter</td>
									<td class="grade_header" rowspan="2" style="width:6%;">Final<br>Grade</td>
									<td class="grade_header" rowspan="2" style="width:10%;">Remarks</td>
								</tr>
								<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
									<td class="grade_header" style="width:6%;">1</td>
									<td class="grade_header" style="width:6%;">2</td>
								</tr>
							</thead>
							<tbody>
								<?php
									if ($grades) {
										foreach($grades AS $grade) {
								?>			
											<tr>
												<td style="text-align:center;"><?php print($grade->course_code); ?></td>
												<td><?php print($grade->descriptive_title); ?></td>
												<td><?php print($grade->teacher); ?></td>
												<td style="text-align:center; font-weight:bold;"><?php print($grade->midterm_grade); ?></td>
												<td style="text-align:center; font-weight:bold;"><?php print($grade->finals_grade); ?></td>
												<td style="text-align:center; font-weight:bold;"><?php print($grade->final_grade); ?></td>
												<td style="text-align:center;">
														<div id="remark_icon_<?php print($grade->enrollments_id); ?>" style="float:left;">
														</div> 
														<div style="text-align:center;">
														
															<?php 
																if ($can_update_remark AND $grade->final_grade) {
															?>
																	<a href="#" 
																		enrollments_id="<?php print($grade->enrollments_id); ?>" 
																		students_idno = "<?php print($idnum); ?>"
																		final_grade="<?php print($grade->final_grade); ?>"
																		remark="<?php print($grade->remark); ?>"
																		course_code="<?php print($grade->course_code); ?>"
																		teacher="<?php print($grade->teacher); ?>"
																		class="show_modal_remark" >
																		<?php 
																			if ($grade->remark) {
																				print($grade->remark);
																			} else {
																				print("<i class=\"icon-edit\"></i>");	
																			}
																		?>	
																	</a>
															<?php 
																} else {
																	print($grade->remark);
																}
															?>
														</div>
												</td>
											</tr>
								<?php 
										}
									}
								?>
							</tbody>
						</table>
					</div>

					
<script>
	$('.show_modal_remark').click(function(event){
		event.preventDefault();

		var enrollments_id = $(this).attr('enrollments_id');
		var remark         = $(this).attr('remark');
		var students_idno  = $(this).attr('students_idno');
   	
		$('#enrollments_id').val(enrollments_id);
		$('#students_idno').val(students_idno);
		
		if (remark) {
			$('#remark').val(remark);
		} else {
			$('#remark').val('Prom');			
		}
		
		$('#course_code').html($(this).attr('course_code'));
		$('#teacher').html($(this).attr('teacher'));
		$('#final_grade').html($(this).attr('final_grade'));
		
		$('#modal_change_remark').modal('show');
	});
</script>				


	<div id="modal_change_remark" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="change_remark_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Remark</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="enrollments_id"></div>
						<div id="students_idno"></div>
						<?php														
							$this->load->view('shs/class_adviser/modal_view/remark_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update Remark!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<script>
	$('#change_remark_form').submit(function(e) {
		e.preventDefault();
		
		var enrollments_id  = $('#enrollments_id').val();
		var students_idno   = $('#students_idno').val();
		var remark          = $('#remark').val();

		var element2    = $("option:selected", "#remarks");
		var term_id     = element2.val();

		$('#remark_icon_'+enrollments_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$('#modal_change_remark').modal('hide');

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/my_section'));?>",
			data: {"enrollments_id": enrollments_id,
					"students_idno": students_idno,
					"remark": remark,
					"term_id": term_id,
					"action": "update_remark" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_remark_here').html(response.output); 
			
				$('#remark_icon_'+enrollments_id).html("");
			}
		});
		
	});
</script>				

	
	
	
	