<h2 class="heading">Fees Schedule - Senior High School</h2>

<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<div class="control-group formSep">
				<div class="control-label">
					School Year
				</div>
				<div class="controls" style="font-weight:bold; padding-top:5px;">
					<?php 
						print($academic_year);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">
					Semester Term
				</div>
				<div class="controls" style="font-weight:bold; padding-top:5px;">
					<?php
						if ($terms) {
							foreach($terms AS $term) {
					?>
								<div style="padding-bottom:5px;"><?php print($term->term); ?></div>
					<?php
							}	
						}		
					?>								
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">
					Fees Category
				</div>
				<div class="controls" style="font-weight:bold; padding-top:5px;">
					<?php 
						print($fees_subgroup->fees_group);					
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">
					Item
				</div>
				<div class="controls" style="font-weight:bold; padding-top:5px;">
					<?php 
						print($fees_subgroup->description);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">
					Track
				</div>
				<div class="controls" style="font-weight:bold; padding-top:5px;">
					<?php
						if ($tracks) {
							foreach($tracks AS $track) {
					?>
								<div style="padding-bottom:5px;">
									<?php print($track->group_name); ?>
								</div>
								<div style="padding-left:30px; font-style:italic; color:#02074E; font-weight:normal;">
									<?php print("<i class='icon-ok'></i> ".$track->strand_description); ?>
								</div>
								
					<?php
							}	
						}		
					?>								
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">
					Grade Level
				</div>
				<div class="controls" style="font-weight:bold; padding-top:5px;">
					<?php 
						foreach($grade_levels AS $k => $v) {
					?>
							<div style="padding-bottom:5px;"><?php print("Grade ".$v); ?></div>
					<?php
						}
					?>										
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label" >
					Amount
				</div>
				<div class="controls" style="font-weight:bold; padding-top:5px;">
					<?php print(number_format($rate,2));?>			
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<button class="btn btn-warning" id="save_fees" fees_subgroups_id="<?php print($fees_subgroup->id); ?>" 
						grade_levels='<?php print(json_encode($grade_levels)); ?>' 
						tracks='<?php print(json_encode($tracks)); ?>'
						terms='<?php print(json_encode($terms)); ?>' 
						rate="<?php print($rate); ?>">Save Fees Schedule >></button>
				</div>
			</div>
			</div>				
	</div>
</div>



 <form id="save_fees_sched_form" method="post">
  <input type="hidden" name="action" value="save_fees" />
</form>

<script>
	$('#save_fees').click(function(event){
		event.preventDefault(); 

		var fees_subgroups_id = $(this).attr('fees_subgroups_id');
		var tracks            = $(this).attr('tracks');
		var grade_levels      = $(this).attr('grade_levels');
		var terms             = $(this).attr('terms');
		var rate              = $(this).attr('rate');
		
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'fees_subgroups_id',
		   	value: fees_subgroups_id,
		}).appendTo('#save_fees_sched_form');

		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'tracks',
		   	value: tracks,
		}).appendTo('#save_fees_sched_form');

		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'grade_levels',
		   	value: grade_levels,
		}).appendTo('#save_fees_sched_form');

		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'terms',
		   	value: terms,
		}).appendTo('#save_fees_sched_form');

		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'rate',
		   	value: rate,
		}).appendTo('#save_fees_sched_form');

		$('#save_fees_sched_form').submit();
	});
</script>



