
<div id="show_strand_management_page" >
	<h2 class="heading">Strand Management</h2>
	<div id="rounded_header" style="width:99%; font-size:12px; margin:0 auto; margin-top:15px; padding:0px;">
			<div style="float:right; overflow:auto;">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<div style="float:left; padding-top:6px; " >
					<a href="#" class="btn btn-success add_strand" style="font-size:12px; height:17px; margin-right:8px; margin-bottom:2px;" >
						<i class="icon-plus-sign"></i> New Strand
					</a>
				</div>
					<div style="float:left; padding-top:5px;" class="b2">
						<select name="track_id" class="form-control change_track" style="width:auto; height:auto;" id="select1">
							<option value="0" <?php if ($track_id == 0) print("selected"); ?> pos_name="All Tracks">All</option>
							<?php 
								if ($tracks) {
									foreach ($tracks AS $track) {
							?>		
										<option value="<?php print($track->id); ?>" 
											abbreviation="<?php print($track->abbreviation); ?>" 
											<?php if ($track_id == $track->id) print("selected"); ?>>
											<?php print($track->group_name); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
			</div>
	</div>
	
	<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_items" >
		<?php
			$data = array(
					"tracks"=>$tracks,
					"strands"=>$strands,
			);
			
			$this->load->view('shs/tracks/list_all_strands',$data);					
		?>
	</div>
</div>



<script>
	$('.add_strand').click(function(event){
		
		$('#abbreviation_modal').focus();

		$('#abbreviation_modal').val('');
		$('#description_modal').val('');
		$('#max_yr_level_modal').val(2);
		
		$('#modal_new_strand').modal('show');
	});
</script>				


	<div id="modal_new_strand" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_strand_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">New Strand</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$data['tracks'] = $tracks;
							$this->load->view('shs/tracks/modal_view/new_strand_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Strand!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#add_strand_form').submit(function(e) {
		e.preventDefault();
		
		var track_id     = $('#track_id_add_modal').val();
		var abbreviation = $('#abbreviation_modal').val();
		var description  = $('#description_modal').val();
		var max_yr_level = $('#max_yr_level_modal').val();

		$('#modal_new_strand').modal('hide');
		
		$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/strand_management'); ?>",
			data: {"track_id": track_id,
					"abbreviation": abbreviation,
					"description": description,
					"max_yr_level": max_yr_level,
					"action": "add_strand" },
			dataType: 'json',
			success: function(response) {													

				$('#show_strand_management_page').html(response.output); 
				
				$('#show_add_icon').html('');
				
			}
		});
		
	});
</script>				


<script>
	$(document).ready(function(){
		$('.change_track').bind('change', function(){
			
			var element1 = $("option:selected", "#select1");
			var track_id = element1.val();

			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/strand_management'); ?>",
				data: {"track_id": track_id,
						"action": "change_track" },
				dataType: 'json',
				success: function(response) {													

					$('#show_extracted_items').html(response.output); 
					
					$('#show_add_icon').html('');
					
				}
			});

		});
	});
</script> 


