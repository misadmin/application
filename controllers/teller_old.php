<?php
class Teller extends MY_Controller {

	public function __construct(){
		parent::__construct();
		//print_r($this->session->all_userdata()); die();
		$this->navbar_data['menu'] = 
							array('Payors'=>
									array('Student'		=>'student',
										'Employee'		=>'employee',
										'Other Payors'	=>'other_payer'),
								'Reports'=>		
									array('Daily Receipt Summary' => 'daily_receipt_summary',
										  'Deposit'  => 'deposit',),
								'Other'=>
									array('Setttings'      =>'settings',
											'Receipts' => 'receipts',
										  'Void Receipt'   =>'void_receipt',
										 'Add Other Payer' =>'add_other_payer'),
											
		);
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		/*
		$client_ip = $this->input->ip_address();
		
		if (! in_array($client_ip, $this->config->item('finance_allowed_ips'))){
			die($client_ip . ' May God have mercy on your soul...');
		}
		*/
		$this->load->model('teller/teller_model');

		
		if (!$this->teller_model->teller_id($this->session->userdata('empno'))){
			$this->session->sess_destroy();
			die("Teller not recognized!");
		}
		$tax_type = $this->session->userdata('tax_type');
		
		if ( ! $tax_type)
			$this->session->set_userdata(array('tax_type'=>'nonvat')); //default... todo: must be set at config...
		$this->content_lib->enqueue_footer_script('maskedinput');
		
	}

	/**
	 * Restrictions in the checkout method
	 * 1. Each item will have its own receipt.
	 * 2. Multiple items pushed to cart can be paid with only one payment method.
	 * 3. Multiple items pushed to cart must have the same tax type ie (VAT or NON-VAT)
	 * 4. One item pushed to cart can have more than one payment method.
	 *
	 *	
	 */
	public function student(){
	
		$this->content_lib->set_title('Teller | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
	
		if ( ! empty($query)){
				
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
				
			$this->load->library('pagination_lib');
				
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
	
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
	
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url() . "/{$this->role}/student/{$results[0]->idno}");
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
	
		if (is_numeric($idnum)){
			//print_r ($this->session->all_userdata()); die();
			//after the search... when results are found....
			//enqueues the jzebra applet...
			$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('teller/teller_model');
			
			$result = $this->teller_model->get_student($idnum);
		
			$this->content_lib->set_title ('Teller | ' . $idnum);
			$bank_codes = $this->input->post('bank_codes');
			
			$tab = 'teller';
			switch ($this->input->post('action')){
				
				case 'change_tax_type'	: 
								$tax_type = $this->input->post('tax_type');
								$this->session->set_userdata(array('tax_type'=> $tax_type));
								break;
				case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;				
				case 'checkout'			:
								$use_session = FALSE;
								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
									$items = json_decode($this->input->post('items'));
									$payments = json_decode($this->input->post('payments'));
									$receipt_no = $this->input->post('receipt_no');
									$machine_ip = $this->input->ip_address();
									$remarks = array();
									foreach($items as $item){
										$dremarks = str_replace("[newline]", "\n", $item->remarks) . " ";
										$remarks[] = $dremarks;
									}
									
									$teller_items = array();
									$total_cost = 0;
									foreach ($items as $item){
										$total_cost += $item->amount;
										$item_info = $this->teller_model->teller_code_info($item->teller_code);
										$teller_items[] = $item_info->description . " (" . $item_info->teller_code . ")";
										$teller_item_codes[] = $item_info->teller_code;
										$item_costs[] = $item->amount;
									}
									
									$payment_total = 0;
									$payment_methods = array();
									
									foreach ($payments as $key=>$payment){
										$payment_total += $payment->amount;
										
										switch ($payment->type){
											case 'carry_over'	:
															foreach ($this->session->userdata('payment_methods') as $payment_method){
																$payment_methods[] = $payment_method; 
															}
															$use_session = TRUE;
															break;
											case 'cash'	: $payment_methods[] = array(
																	'type'=>'cash');
															break;
											case 'check' : 
															$bank_info = $this->teller_model->bank_info($payment->bank_id);
															$payment_methods[] = array(
																	'type'=>'check',
																	'bank'=>$bank_info->code,
																	'date'=>$payment->check_date,
																	'no'=>$payment->check_number,);
															break;
											case 'credit_card' :
															$payment_methods[] = array(
																	'type'=>'credit card',
																	'bank'=>$payment->credit_card_company,
																	'date'=>$payment->credit_card_expiry,
																	'no'=>$payment->credit_card_number,);
												
															break;
											case 'debit_card' :
															$payment_methods[] = array(
																	'type'=>'debit card',
																	'bank'=>$payment->debit_card_company,
																	'date'=>$payment->debit_card_expiry,
																	'no'=>$payment->debit_card_number,);
															break;
											case 'bank'			:
															$bank_info = $this->teller_model->bank_info($payment->bank_code);
															$payment_methods[] = array(
																'type'=>'bank',
																'bank'=>$bank_info->code,
																'date'=>$payment->date_received,
																);
															break;
											default:
															break;
										}
									}
									
									$change = $payment_total - $total_cost;
									
									if (! is_numeric($receipt_no)) {
										$this->content_lib->set_message('Error(s) seen while posting payment to the database. Please check your OR number and try again.' . $receipt_no , 'alert-error');
									} else {
										$payment_methods_ids = ($use_session ? $this->session->userdata('payment_methods_ids') : NULL);
										$this->db->trans_start();										
										if ($return = $this->teller_model->checkout($result->payers_id, $this->session->userdata('empno'), $payments, $items, $receipt_no, $machine_ip, $this->session->userdata('tax_type'), $payment_methods_ids)) {
											$this->db->trans_complete();
											$this->session->set_userdata('payment_methods', $payment_methods);
											//successfully posted to the database...
											//TODO: HOOK events for the item paid... CR, HR, ER
											if (in_array('CR', $teller_item_codes)) {
												$this->load->model('hnumis/student_model');
												$ret = $this->student_model->assess_student_year_level_by_id ($idnum);
												if ($ret['has_history_id']===TRUE) {
													//$ret is an associative array of 'year_level', 'prospectus_id'
													$this->student_model->update_student_history ($ret['history_id'], array('can_enroll'=>'Y'));
													$this->load->helper('password_generator');
													$password = generate_password();
													$password_remarks = "password = {$password}";
													$this->load->model('student_common_model');
													$this->student_common_model->update_information($idnum, array('password'=>$password));
													$in_count = 0;
													foreach ($teller_item_codes as $code){
														if($code=='CR'){
															$remarks[$in_count] = $password_remarks;
														}
														$in_count++;
													}
												} else {
													$this->student_model->insert_student_history($idnum, $ret['prospectus_id'], 0, $ret['year_level'], TRUE);
												}
											}
											
											if (in_array('HR', $teller_item_codes)){
												//student is registering to High school...
											}
											
											if (in_array('ER', $teller_item_codes)){
												
											}
											
											foreach ($items as $item){
												$name_on_receipt[] = $item->name_on_receipt;
												$trace_strings[] = '\*' . $receipt_no . ",{$this->input->ip_address()},{$this->session->userdata('empno')},{$this->session->userdata('lname')}," . substr($this->session->userdata('fname'), 0, 1) . '*\\';
												$receipt_no++; 
											}
											 
											//we generate the receipt...
											$this->load->library('print_lib');
											$this->load->helper('number_words_helper');
											
											$this->content_lib->enqueue_body_content('print_templates/receipt', 
													array(
															'id_number'=>$idnum, 
															'date'=>date("M j, Y"), 
															'names'=>$name_on_receipt, 
															'course_year'=>$result->year_level . " " . $result->abbreviation,
															'item_codes'=>$teller_item_codes,
															'items'=>$teller_items,
															'payments'=>$item_costs,
															'remarks'=>$remarks,
															'trace_strings'=>$trace_strings,
															'payment_methods'=>$payment_methods,
															));
											$this->session->set_userdata('last_transaction_change', $change);
											$this->session->set_userdata('payments', $payments);
											$this->session->set_userdata('payment_methods', $payment_methods);
											$this->content_lib->set_message('Successfully posted transaction.<h1>Last Transaction Change: P ' . number_format($change, 2) . '</h1>', 'alert-success');
											//$this->content_lib->set_message($return, 'alert-success');
										} else {
											//error posting to the database...
											$this->db->trans_rollback();
											$this->content_lib->set_message('Error(s) seen while posting payment to the database.', 'alert-error');
										}
									}
								} else {									
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
								}
								break;
				default: 
								break;
				
			}
			$this->load->model('teller/teller_model');
			
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->year_level,
						'full_home_address'	=>$result->home_address, 
				);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				$this->load->library('print_lib');
				//tab contents
				$this->load->library('tab_lib');
				//teller form
				$this->load->model('academic_terms_model');
				$this->load->model('hnumis/student_model');
				
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$bank_codes = $this->teller_model->get_bank_codes();
				
				$ledger_data = array();
				$ledger_data = $this->teller_model->get_ledger_data($result->payers_id);
				$number_of_periods = ($current_academic_terms_obj->term=='Summer' ? 2 : 4);
				$earliest_enrollment_schedule = $this->academic_terms_model->earliest_enrollment_schedule();
				$student_is_enrolled = $this->student_model->student_is_enrolled($idnum);
				
				$teller_values = array(
						'name' => $result->fname . " " . $result->lname,
						'teller_codes'=>$this->teller_model->get_teller_codes($this->session->userdata('tax_type')), 
						'bank_codes'=>$bank_codes,
						'ledger_data'=>$ledger_data,
						'period'=>$this->academic_terms_model->what_period(),
						'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
						'number_of_periods' => $number_of_periods,
						'earliest_enrollment_schedule' => $earliest_enrollment_schedule,
						'type' => 'student',
						'student_is_enrolled' => $student_is_enrolled,
						);
				
				$assessment_values = array();
				
				$this->tab_lib->enqueue_tab('Teller', 'teller/teller', $teller_values, 'teller', $tab=='teller');
				$this->load->model('accounts/accounts_model');
				$this->load->model('hnumis/enrollments_model');
				
				//assessment details
				$acontent = array(
						'idnumber'	=>$result->idno,
						'familyname'=>$result->lname,
						'firstname'	=>$result->fname,
						'middlename'=>$result->mname,
						'level'		=>$result->year_level,
						'course'	=>$result->abbreviation,
				);
				
				//assessment form
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
				} else {
					$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
				}
				$lab_courses = array();
				$courses = array();
				$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
				
				if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab') {
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
									);
						} 
						$courses[] = array(
								'id'=>$course->id,
								'name'=>$course->course_code,
								'units'=>$course->credit_units,
								'pay_units'=>$course->paying_units
								);  
					}
				}
				
				$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment', 
						array(
								'other_courses_payments'=>$other_courses_payments,
								'assessment'=> $assessment,
								'courses'=>$courses,
								'lab_courses'=>$lab_courses,
								'student_inclusive_terms' =>$student_inclusive_terms,
								'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
								'student_details'=>$acontent
						), 
								'assessment', $tab=='assessment');
				
				//enqueue ledger
				$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$ledger_data,
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date
						), 'ledger', $tab=='ledger');
			
				//sidebar...
				$this->content_lib->enqueue_sidebar_widget('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type')), 'Tax Type', 'in');
				
				$receipts = $this->teller_model->get_last_receipts();
				if ($receipts)
					$receipt_no = ($this->session->userdata('tax_type')=='vat' ? $receipts->vat_receipt_no : $receipts->non_vat_receipt_no); else
					$receipt_no = "";
				 
				$this->content_lib->enqueue_sidebar_widget('teller/receipt_number', array('receipt_no'=>$receipt_no), 'Receipt No.', 'in');
				$dcontent['profile_sidebar'] = array();
				$dcontent['profile_sidebar'][] = $this->content_lib->enqueue_content_string('common/search', array('role'=>'teller', 'what'=>'student'));
				$dcontent['profile_sidebar'][] = $this->content_lib->enqueue_content_string('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type'),'receipt_no'=>$receipt_no));
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				$this->content_lib->enqueue_body_content("", $this->tab_lib->content());
				$this->content_lib->flush_sidebar();//the tellers didn't like the sidebar...
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}	
		$this->content_lib->content();
		//this should be placed here, outside the html tag or body to be accessible
	}
	

	public function employee(){
		$this->content_lib->set_title('Teller| Employee | ' . $this->config->item('application_title'));
		$query = $this->input->post('q');
		$empno = $this->uri->segment(3);
		$this->content_lib->enqueue_sidebar_widget('teller/search_employee', array('role'=>$this->role, 'what'=>'faculty'), 'Search Employees', 'in');
		if ($query){
			//There is a query...
			$this->load->model('employee_common_model');
			$results = $this->employee_common_model->search($query);
				
			if (count($results) > 1){
				//more than 1 results... lets show it in a page
				foreach ($results as $result){
					if (is_file(FCPATH . $this->config->item('employee_images_folder') . ltrim($result->empno, '0') . ".jpg"))
						$image = base_url($this->config->item('employee_images_folder') . ltrim($result->empno, '0') . ".jpg");
					else {
						//$image = base_url($this->config->item('no_image_placeholder_female')); else
						$image = base_url($this->config->item('no_image_placeholder_male'));
					}
					$res[] = array('image'=>$image, 'idno'=>$result->empno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
				}
				$this->content_lib->enqueue_body_content('teller/employee_search_result', array('results'=>$res, 'query'=>$query, 'role'=>$this->session->userdata('role')));
			} elseif (count($results)==1){
				redirect (site_url("{$this->role}/employee/{$results[0]->empno}"));
			} else {
				//no result...
	
			}
		}
	
		if ($empno){
			//An employee number was used...
			$this->load->model('employee_common_model');
			$this->load->model('academic_terms_model');
			$data = $this->employee_common_model->my_information($empno);
			//We're going to need tabs... lets load the tab library:
			$this->load->library('tab_lib');
				
			//User may have selected what academic term and year...
			if ($this->input->post('academic_terms_id')) {
				$academic_terms_id = $this->input->post('academic_terms_id');
			} else {
				$academic_terms = $this->academic_terms_model->current_academic_term();
				$academic_terms_id = $academic_terms->id;
			}
				
			//Lets process actions intended for this employee...
			switch ($this->input->post('action')){
				case 'change_tax_type'	: case 'change_tax_type'	: 
								$tax_type = $this->input->post('tax_type');
								$this->session->set_userdata(array('tax_type'=> $tax_type));
								break;
					break;
				/*
				 * batman performs here
				 */
				default:
					$this->load->model('hnumis/Courses_model');
					//$academic_terms = $this->academic_terms_model->faculty_inclusive_academic_term ($empno);
					//$this->content_lib->enqueue_sidebar_widget ('dean/list_academic_terms1',array('academic_terms'=>$academic_terms, 'selected_term'=>$academic_terms_id), 'Select Academic Terms', 'in');
					//$result = $this->Courses_model->faculty_courses_academic_terms($empno, $academic_terms_id);
					//$this->tab_lib->enqueue_tab ('Schedule', 'faculty/list_courses', array('result'=>$result, 'term'=>(isset($result[0]->term) ? $result[0]->term : ""), 'school_year'=>(isset($result[0]->sy) ? $result[0]->sy : ""), 'selected_term'=>$academic_terms_id), 'schedule', TRUE);
					break;
			}
				
				
			if (is_file(FCPATH . $this->config->item('employee_images_folder') . ltrim($empno, '0') . ".jpg"))
				$image = base_url($this->config->item('employee_images_folder') . ltrim($empno, '0') . ".jpg");
			else {
				//$image = base_url($this->config->item('no_image_placeholder_female')); else
				//todo: add gender to employees table...
				$image = base_url($this->config->item('no_image_placeholder_male'));
			}
				
			$data['image'] = $image;
			$this->content_lib->enqueue_body_content('common/employee_profile', $data);
			$this->load->model('teller/teller_model');				
			//$this->load->model('hnumis/AcademicYears_Model');
			//$this->load->model('hnumis/Courses_model');				
			//$academic_terms = $this->academic_terms_model->faculty_inclusive_academic_term ($empno);				
			//$data['selected_term'] = $academic_terms_id;
			//$this->content_lib->enqueue_body_content('', $this->tab_lib->content());

			//tab contents
			
			$bank_codes = $this->teller_model->get_bank_codes();
			$teller_values = array(
					'teller_codes'=>$this->teller_model->get_teller_codes($this->session->userdata('tax_type')),
					'bank_codes'=>$bank_codes,
					'type'=>'employee',
			);
			$receipts = $this->teller_model->get_last_receipts();
			$receipt_no = ($this->session->userdata('tax_type')=='vat' ? $receipts->vat_receipt_no : $receipts->non_vat_receipt_no);
			
			$this->load->library('tab_lib');
			//teller form
			$this->tab_lib->enqueue_tab('Teller', 'teller/teller', $teller_values, 'teller', TRUE);
			//sidebar...
			$this->content_lib->enqueue_sidebar_widget('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type')), 'Tax Type', 'in');
				
				$receipts = $this->teller_model->get_last_receipts();
				if ($receipts)
					$receipt_no = ($this->session->userdata('tax_type')=='vat' ? $receipts->vat_receipt_no : $receipts->non_vat_receipt_no); else
					$receipt_no = "";
				 
				$this->content_lib->enqueue_sidebar_widget('teller/receipt_number', array('receipt_no'=>$receipt_no), 'Receipt No.', 'in');
				$dcontent['profile_sidebar'] = array();
				$dcontent['profile_sidebar'][] = $this->content_lib->enqueue_content_string('common/search', array('role'=>'teller', 'what'=>'student'));
				$dcontent['profile_sidebar'][] = $this->content_lib->enqueue_content_string('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type'),'receipt_no'=>$receipt_no));						
			$tab_content = $this->tab_lib->content();
			$this->content_lib->enqueue_body_content("", $tab_content);
		}
	
		if ( ! $empno && ! $query){
			//No query was done by the DEAN... and no employee was found yet...
			$data = array();
			if ($this->input->post('letter')){
				$data['letter'] = $this->input->post('letter');
				$this->load->model('employee_common_model');
				$data['results'] = $this->employee_common_model->search_by_starting_letter($this->input->post('letter'));
			}
			$this->content_lib->enqueue_body_content('teller/employee_search_landing', $data);
		}
	
		$this->content_lib->content();
	}
	
	public function other_payer(){
		$this->load->model('teller/teller_model');
		
		
		if ($return = $this->teller_model->get_teller_codes()){
			$this->content_lib->enqueue_body_content('',print_r($return, TRUE));
		} else {
			$this->content_lib->enqueue_body_content('',print_r($this->teller_model->last_error(), TRUE));
		}
		$this->content_lib->content();
	}

	public function inhouse(){
		$this->content_lib->enqueue_body_content('','inhouse ');
		$this->content_lib->content();
	} 
	 
	
	
	public function deposit(){
		$this->content_lib->enqueue_body_content('','deposit here');
		$this->content_lib->content();
	}

	public function settings(){
		$this->load->model('teller/teller_model');
		//process actions here:
		if ($this->input->post('action')) {
			if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
				switch ($this->input->post('action')) {
					case 'update_receipts' :
						$data = array(
							'machine_ip'		=>$this->input->ip_address(),
							'vat_receipt_no'	=>$this->input->post('vat-receipt'),
							'non_vat_receipt_no'=>$this->input->post('nonvat-receipt')
						);
						if ($ret = $this->teller_model->update_receipt_numbers($data))
							$this->content_lib->set_message('Receipts successfully set.', 'alert-success'); else
							$this->content_lib->set_message('Error(s) found while updating receipt numbers.', 'alert-error');
						break;
				}
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}	
		} 
		
		if ($receipts = $this->teller_model->get_last_receipts ()){
			$receipt_data = array(
					'vat_receipt'=>$receipts->vat_receipt_no,
					'nonvat_receipt'=>$receipts->non_vat_receipt_no,
					);
		} else {
			$receipt_data = array(
					'vat_receipt'=>'',
					'nonvat_receipt'=>'',
			);
		}
		
		$this->load->library('tab_lib');
		$this->tab_lib->enqueue_tab ('Receipt Numbers', 'teller/receipt_settings', $receipt_data, 'receipt_settings', TRUE);
		
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->content();
	}

	public function void_receipt(){
		$this->content_lib->enqueue_body_content('','void receipt here');
		$this->content_lib->content();
	}
	
	public function add_other_payer(){
		$this->content_lib->enqueue_body_content('','add other payer here');
		$this->content_lib->content();
	}
	
	public function daily_receipt_summary(){
		$this->load->model('teller/teller_model');		
		$date = $this->input->post('date');
		$submitted = $this->input->post('submitted');		
		if ($date AND $submitted){
			//create date string
			$data = array();
			$data['date'] = date('jS \of F Y',strtotime($date));
			//get records
			$data['records'] = $this->teller_model->daily_receipt_summary(date('Y-m-d',strtotime($date)));
			$data['payment_methods'] = $this->config->item('payment_methods');
			$this->load->library('table_lib');
			$this->content_lib->enqueue_body_content('teller/receipt_summary_table',$data);
		}
		else {
			$this->content_lib->enqueue_header_style('date_picker');
			$this->content_lib->enqueue_footer_script('date_picker');
			//$this->content_lib->enqueue_footer_script('validate');			
			$this->content_lib->enqueue_body_content('teller/receipt_summary_index');
		}
		$this->content_lib->content();
	}

	public function receipts(){
		$this->load->model('teller/teller_model');
		$date = $this->input->post('date');
		$payment_methods = $this->input->post('payment_methods');  
		$submitted = $this->input->post('submitted');
		$payment_id = $this->input->post('payment_id');
		if ($date AND $submitted){
			//create date string
			$data = array();
			$data['date'] = date('jS \of F Y',strtotime($date));
			$data['receipts'] = $this->teller_model->receipts(date('Y-m-d',strtotime($date)),$payment_methods);
			//print_r($data);
			//die();
			$this->load->library('table_lib');
			$this->content_lib->enqueue_body_content('teller/receipt_report',$data);
		}
		else if ($payment_id){
			echo $payment_id;
			die();
		}
		else {
			$this->content_lib->enqueue_header_style('date_picker');
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_body_content('teller/receipt_report_index');
		}
		$this->content_lib->content();
	
	}
	
	
	
	
	private function change_tax_type(){
		
	}
	
	
}
