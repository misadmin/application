<?php if (isset($terms) && count($terms) > 0): ?> 
<?php if ($terms) {
			foreach($terms as $term): ?>
<h4 style="margin-top: 20px;"><?php echo $term['school_year']; ?> <?php echo $term['term']; ?> (<?php echo $term['program'] . " " . $term['year_level']; ?>)</h4>
<table class="table table-bordered mediaTable activeMediaTable" id="MediaTable-0">
	<thead>
		<tr>
			<th width="14%">Catalog Number</th>
			<th width="38%">Descriptive Title</th>
			<th width="6%">Prelim</th>
			<th width="6%">Midterm</th>
			<th width="6%">Finals</th>
			<th width="20%">Teacher</th>
			<th width="10%">Credit Units</th>
		</tr>
	</thead>
	<tbody>
<?php //print_r($terms);die(); ?>
<?php if(isset($term['courses']) && count($term['courses']) > 0): ?>
<?php
	foreach($term['courses'] as $subject):
		
		$update_history = $this->db->escape(htmlspecialchars(json_encode($subject['course']->update_history),ENT_QUOTES)); 
		?>
		<tr>
			<td><?php echo $subject['course']->course_code; ?></td>
			<td><?php echo $subject['course']->descriptive_title; ?></td>
			<td style="text-align:center;"> <a class="edit_grade" href="<?php print($idnum);?>" enrollment_id="<?php print($subject['course']->enrollment_id);?>" period='P' 
											<?php if($subject['course']->prelim_grade) { ?>
												 grade="<?php print($subject['course']->prelim_grade);?>" update_history="<?php //echo $update_history; ?>"  > <?php echo $subject['course']->prelim_grade;?> </a></td>
											<?php }
											 else{
												$temp_grade = '-';
											?>
												grade="<?php print($temp_grade);?>" update_history="<?php //echo $update_history; ?>"  > <?php echo '--'; ?></a></td>
										<?php } ?>	 
			<td style="text-align:center;"> <a class="edit_grade" href="<?php print($idnum);?>" enrollment_id="<?php print($subject['course']->enrollment_id);?>" period ='M'
											<?php if($subject['course']->midterm_grade) { ?>
												grade="<?php print($subject['course']->midterm_grade);?>" update_history="<?php //echo $update_history; ?>"  > <?php echo $subject['course']->midterm_grade; ?></a></td>
											<?php } else{
												$temp_grade = '-';
												?>
												grade="<?php print($temp_grade);?>" update_history="<?php //echo $update_history; ?>"  > <?php echo '--'; ?></a></td>
											<?php } ?>		
			<td style="text-align:center;"> <a class="edit_grade" href="<?php print($idnum);?>" enrollment_id="<?php print($subject['course']->enrollment_id);?>" period ='F'
										<?php if($subject['course']->finals_grade) { ?>
												grade="<?php print($subject['course']->finals_grade);?>" update_history="<?php echo $update_history; ?>"  > <?php echo $subject['course']->finals_grade; ?></td>
										<?php } else{
												$temp_grade = '-';
												?>
												grade="<?php print($temp_grade);?>" update_history="<?php //echo $update_history; ?>"  > <?php echo '--'; ?></a></td>
										<?php } ?>			
			<td><?php echo $subject['course']->instructor; ?></td>
			<td style="text-align:center;"><?php echo $subject['course']->credit_units; ?></td>
		</tr>
<?php endforeach;  ?>
<?php endif; ?>
	</tbody>
</table>
<?php endforeach; } ?>
<?php else: ?>
	<h3>No Records Found</h3>
<?php endif;?>

	<form id="editGrade" action="<?php echo site_url('registrar/edit_grade/' . $this->uri->segment(3)); ?>" method="post" >
		<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="open_edit_grade_form" />
	</form>			

<script>
	$('.edit_grade').click(function(event){
		event.preventDefault(); 
		var idnum 			= $(this).attr('href');
		var enrollment_id 	= $(this).attr('enrollment_id');
		var prev_grade 		= $(this).attr('grade');
		var period 			= $(this).attr('period');
		var update_history 	= $(this).attr('update_history'); 
		$('<input>').attr({
		    type: 'hidden',
		    name: 'status',
		    value: status,
		}).appendTo('#editGrade');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'enrollment_id',
		    value: enrollment_id,
		}).appendTo('#editGrade');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prev_grade',
		    value: prev_grade,
		}).appendTo('#editGrade');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'period',
		    value: period,
		}).appendTo('#editGrade');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'update_history',
		    value: update_history,
		}).appendTo('#editGrade');
		console.log();
		
		$('#editGrade').submit();
	});
</script>