<?php

	class Electives_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

		function ListElectiveTopic($prospectus_courses_id) {
			$result = null;
				
			$q1 = "SELECT
						b.id, 
						a.course_code, 
						a.descriptive_title
					FROM 
						courses AS a, 
						elective_courses AS b,
						prospectus_terms AS c,
						prospectus_courses AS d
					WHERE 
						a.id=b.topic_courses_id
						AND b.prospectus_courses_id=d.id
						AND c.id=d.prospectus_terms_id
						AND b.prospectus_courses_id = {$this->db->escape($prospectus_courses_id)} 
					ORDER BY
						a.course_code ";		
		
			//print($q1);
			//die();
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		
		}
		
		
		function AddElectiveTopic($topic_data) {				
			$query = "
					INSERT INTO
						elective_courses (prospectus_courses_id, topic_courses_id)
					VALUES (
						{$this->db->escape($topic_data['prospectus_courses_id'])},
						{$this->db->escape($topic_data['topic_courses_id'])}
					)
				";
		
			$result = $this->db->query($query);
			return $this->db->insert_id();
		}

			
		function DeleteElectiveTopic($topic_id) {
				
			$query = "DELETE 
						FROM 
							elective_courses 
						WHERE
							id = {$this->db->escape($topic_id)} ";
			
			//print($query);
			//die();
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
						
		}


		function getTopicInfo($elective_courses_id) 
		{
			$result = null;
			
			$q = "SELECT 
						b.id,
						a.course_code,
						a.descriptive_title
					FROM 
						courses AS a,
						elective_courses AS b
					WHERE 
						a.id = b.topic_courses_id
						AND b.id = {$this->db->escape($elective_courses_id)} ";
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
			

		function ListTopicPrereqs($elective_courses_id, $prereq_type=NULL) {
			$result = null;
			
			if ($prereq_type == 'M') {
				$q1 = "(SELECT 
								a.id AS elective_topic_prereq_id,
								b.id AS prereq_id, 
								CONCAT('[',d.course_code,'] ',d.descriptive_title) AS my_prereq, 
								a.prereq_type
							FROM 
								elective_topic_prereq AS a, 
								elective_topic_prereq_courses AS b,
								prospectus_courses AS c,
								courses AS d
							WHERE 
								a.id=b.elective_topic_prereq_id 
								AND b.prospectus_courses_id=c.id 
								AND c.courses_id=d.id
								AND a.elective_courses_id={$this->db->escape($elective_courses_id)}
								AND b.prereq_type = 'M' )
						   UNION
						 (SELECT 
								a.id AS elective_topic_prereq_id,
								b.id AS prereq_id, 
								CASE b.yr_level 
									WHEN 1 THEN '1st Year Standing'
									WHEN 2 THEN '2nd Year Standing'
									WHEN 3 THEN '3rd Year Standing'
									WHEN 4 THEN '4th Year Standing'
									WHEN 5 THEN '5th Year Standing'
								END	AS my_prereq, 
								a.prereq_type
							FROM 
								elective_topic_prereq AS a, 
								elective_topic_prereq_yrlevel AS b
							WHERE 
								a.id=b.elective_topic_prereq_id 
								AND a.elective_courses_id={$this->db->escape($elective_courses_id)} )" ;
			} else {
				$q1 = "(SELECT 
								a.id AS elective_topic_prereq_id,
								b.id AS prereq_id, 
								CONCAT('[',d.course_code,'] ',d.descriptive_title) AS my_prereq, 
								a.prereq_type
							FROM 
								elective_topic_prereq AS a, 
								elective_topic_prereq_courses AS b,
								prospectus_courses AS c,
								courses AS d
							WHERE 
								a.id=b.elective_topic_prereq_id 
								AND b.prospectus_courses_id=c.id 
								AND c.courses_id=d.id
								AND a.elective_courses_id={$this->db->escape($elective_courses_id)} )
						   UNION
						 (SELECT 
								a.id AS elective_topic_prereq_id,
								b.id AS prereq_id, 
								CASE b.yr_level 
									WHEN 1 THEN '1st Year Standing'
									WHEN 2 THEN '2nd Year Standing'
									WHEN 3 THEN '3rd Year Standing'
									WHEN 4 THEN '4th Year Standing'
									WHEN 5 THEN '5th Year Standing'
								END	AS my_prereq, 
								a.prereq_type
							FROM 
								elective_topic_prereq AS a, 
								elective_topic_prereq_yrlevel AS b
							WHERE 
								a.id=b.elective_topic_prereq_id 
								AND a.elective_courses_id={$this->db->escape($elective_courses_id)} )" ;
			}
			//print($q1);
			//die();
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		
		}


		function AddTopicPrerequisite($prereq_data) {
				
			$q1 = "INSERT INTO
						elective_topic_prereq (id, elective_courses_id, prereq_type)
					VALUES (
						'',
						{$this->db->escape($prereq_data['elective_courses_id'])},
						{$this->db->escape($prereq_data['prereq_type'])})";
			
			$this->db->query($q1);
			$id = @mysql_insert_id();
			
			switch ($prereq_data['prereq_type']) {
				case 'C':
					$this->AddPrereq_Courses($id, $prereq_data['prospectus_courses_id'], $prereq_data['prereqs']);
					break;
				case 'Y':
					$this->AddPrereq_YrLevel($id,$prereq_data['yr_level']);
					break;
			}						

			return;
		}
		
		
		function AddPrereq_Courses($elective_topic_prereq_id, $prospectus_courses_id, $prereqs)	{
				
			$query = "INSERT INTO 
							elective_topic_prereq_courses (id, elective_topic_prereq_id, prospectus_courses_id, prereq_type)
						VALUES ('',
								{$this->db->escape($elective_topic_prereq_id)},
								{$this->db->escape($prospectus_courses_id)},
								'M')";
					
			$this->db->query($query);
			$id2 = @mysql_insert_id();
			
			if ($prereqs) {
				foreach ($prereqs AS $prereq) {
					$query = "INSERT INTO 
								elective_topic_prereq_courses (id, elective_topic_prereq_id, prospectus_courses_id, prereq_type)
								VALUES ('',
									{$this->db->escape($elective_topic_prereq_id)},
									{$this->db->escape($prereq->prospectus_courses_id)},
									'C')";
	
					$this->db->query($query);
				}
			}

			return $id2;
		}
				

		function AddPrereq_YrLevel($elective_topic_prereq_id, $yr_level)	{
				
			$query = "INSERT INTO 
							elective_topic_prereq_yrlevel (id, yr_level, elective_topic_prereq_id)
						VALUES ('',
								{$this->db->escape($yr_level)},
								{$this->db->escape($elective_topic_prereq_id)})";
					
			$this->db->query($query);
			$id2 = @mysql_insert_id();
					
			return $id2;
					
		}


		function DeleteElectiveTopicPrereqs($id) {
				
			$query = "DELETE 
						FROM 
							elective_topic_prereq 
						WHERE
							id = {$this->db->escape($id)} ";
			
			//print($query);
			//die();
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
						
		}

		
	}
	

//end of electives_model.php