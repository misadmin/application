
<style>
	div#msg_header_transfer {
		border-radius: 5px; 
		background-color: #D7D4D4; 
		border: solid; 
		border-width: 1px; 
		border-color: #B7B6B6; 
		text-align:left; 
		padding-left: 10px;
		margin:0 auto;
		margin-bottom:20px;
		font-family: arial;
		font-size: 14px;
		width: 98%;
		height: 35px;
		line-height: 35px;
		font-weight:bold;
		color:#B50303;
</style>


	<div class="row-fluid">
		<?php 
			if (!$is_registered) {
		?>
				<div id="msg_header_transfer"> 
					Student not registered yet for the current semester! He/she needs to Register and Enroll first!
				</div>
		<?php		
			} elseif(!$is_enrolled) {
		?>	
				<div id="msg_header_transfer"> 
					Student is not enrolled yet for current semester!
				</div>
		<?php		
			} elseif(!$is_sectioned) {
		?>	
				<div id="msg_header_transfer"> 
					Student is not sectioned yet!
				</div>
		<?php		
			} else {
		?>	
			<div id="msg_header_transfer"> </div>
			<div class="form-horizontal">
				<fieldset>
					<div class="control-group formSep">
						<label for="grade_level" class="control-label">Section to Transfer</label>
						<div class="controls">
							<select id="block_sections_id_to_transfer" name="block_sections_id_to_transfer" class="form-control" >
								<option value="">--Select Section--</option>
								<?php 
									if ($transfer_sections) {
										foreach ($transfer_sections AS $section) {
								?>
										<option value="<?php print($section->id); ?>" 
											<?php if ($section->id == $current_section_id) print("disabled"); ?> ><?php print("Grade ".$section->yr_level.'-'.$section->section_name.' '.$section->abbreviation); ?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label for="strand" class="control-label" id="show_transfer_to_section_icon">
						</label>
						<div class="controls">
							<a href="<?php print($idnum); ?>" 
								student_history_id="<?php print($student_histories_id); ?>" 
								class="btn btn-warning transfer_to_section" >Transfer to Section!
							</a>
						</div>
					</div>	
				</fieldset>

			</div>
		<?php
			}
		?>	

	</div>


<script>
	$('.transfer_to_section').click(function(e){
		e.preventDefault();		
		
		var block_sections_id = $('#block_sections_id_to_transfer').val();
		
		if (isEmpty(block_sections_id)) {
			alert('Select a section to transfer the student!');
		} else {
			
			var confirmed = confirm('Transfer student to section?');

			if (confirmed){
		
				var students_idno      = $(this).attr('href');
				var student_history_id = $(this).attr('student_history_id');

				$('#show_transfer_to_section_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

				$.ajax({
					cache: false,
					type: 'POST',
					url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+students_idno,
					data: { student_history_id: student_history_id,
							block_sections_id: block_sections_id,
							action: 'transfer_to_section'},
					dataType: 'json',
					success: function(response) {				
						$('#show_extracted_items').html(response.output); 	

						$('h4.media-heading').html(response.student_info);

						if (response.success) {
							$('#msg_header_transfer').html("Student successfully transferred to section!");
						}
					}
				});
			}
		}
	});

</script>

	
	