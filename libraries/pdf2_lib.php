<?php 
require_once(DIRNAME(BASEPATH).'/application/third_party/tcpdf/tcpdf.php');

class Pdf2_lib extends TCPDF {
	public $header_image;
	public $header_summary;

	// Page footer
	public function Footer() {
		$this->SetFont('helvetica', '', 9);
		$this->SetTextColorArray($this->footer_text_color);
		//set style for cell border
		$line_width = (0.85 / $this->k);
		$this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));

		$w_page = isset($this->l['w_page']) ? $this->l['w_page'].' ' : '';
		$pagenumtxt = $w_page.$this->getAliasNumPage().' of '.$this->getAliasNbPages();

		$this->SetY(-18);
		$date = date( 'M d, Y h:ia' );
		//Print page number
		$this->Cell(100, 8, $this->getAliasRightShift().$pagenumtxt, 'T', 0, 'R');
		$this->Cell(0, 8, "Printed  ".$date, 'T', 0, 'R');
	}
}