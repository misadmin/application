
		<div style="width:100%; float:right; overflow:auto; padding:10px;" >
			<a href="#" style="text-decoration: none; color:#4E4D4D; font-size:12px; "
						college_name="<?php print($college_name); ?>"
						abbreviation="<?php print($abbreviation); ?>"
						description="<?php print($description); ?>"
						term_sy="<?php print($term_sy); ?>"
						year_level="<?php print($year_level); ?>"
						reports='<?php print(json_encode($reports)); ?>'
						class="download_excel_report" >
						<div style="float:right; padding-left:3px; padding-top:2px;">Download to Excel</div>
						<div id="excel_labels" style="float:right;">
							<img src="<?php print(base_url('assets/images/excel_icon.png')); ?>" style="height:20px;" /> 
						</div>
			</a>
		</div>

		<div style="width:100%;" >
			<table class="table table-hover table-bordered table-striped" >
				<thead>
					<tr class="myhead">
						<td class="head1" style="width:5%;" rowspan="2">Count</td>
						<td class="head1" style="width:9%;" rowspan="2">ID<br>Number</td>
						<td class="head1" style="width:15%;" rowspan="2">Lastname</td>
						<td class="head1" style="width:15%;" rowspan="2">Firstname</td>
						<td class="head1" style="width:10%;" rowspan="2">Middlename</td>
						<td class="head1" style="width:23%;" colspan="3">POSTED</td>
						<td class="head1" style="width:23%;" colspan="3">RUNNING</td>
					</tr>
					<tr class="myhead">
						<td class="head1" style="width:9%;">Course</td>
						<td class="head1" style="width:4%;">Year</td>
						<td class="head1" style="width:10%;">Amount</td>
						<td class="head1" style="width:9%;">Course</td>
						<td class="head1" style="width:4%;">Year</td>
						<td class="head1" style="width:10%;">Amount</td>
					</tr>
				</thead>
				<tbody>
					<?php
						if ($reports) {
							$cnt=1;
							foreach($reports AS $student) {
								if (number_format($student->amount_posted,2) != number_format($student->amount_running,2)) {
									$bg = "#FF9B94";
								} else {
									$bg = "#FFFFFF";
								}	
					?>			
								<tr>
									<td style="text-align:right; background-color: <?php print($bg); ?>">
										<?php print($cnt); ?>.
									</td>
									<td style="text-align:center; background-color: <?php print($bg); ?>">
										<a href="<?php print(site_url($this->uri->segment(1).'/student')."/".$student->idno);?>" >
											<?php print($student->idno); ?>
										</a>
									</td>
									<td style="background-color: <?php print($bg); ?>"><?php print($student->lname); ?></td>
									<td style="background-color: <?php print($bg); ?>"><?php print($student->fname); ?></td>
									<td style="background-color: <?php print($bg); ?>"><?php print($student->mname); ?></td>
									<td style="background-color: <?php print($bg); ?>"><?php print($student->course_posted); ?></td>
									<td style="text-align:center; background-color: <?php print($bg); ?>"><?php print($student->yr_level_posted); ?></td>
									<td style="text-align:right; background-color: <?php print($bg); ?>"><?php print(number_format($student->amount_posted,2)); ?></td>
									<td style="background-color: <?php print($bg); ?>"><?php print($student->course_running); ?></td>
									<td style="text-align:center; background-color: <?php print($bg); ?>"><?php print($student->yr_level_running); ?></td>
									<td style="text-align:right; background-color: <?php print($bg); ?>"><?php print(number_format($student->amount_running,2)); ?></td>
								</tr>
					<?php 
								$cnt++;
							}
						}
					?>
				</tbody>
			</table>
		</div>

		
<script>
	$('.download_excel_report').click(function(event){
		event.preventDefault(); 
		
		var college_name = $(this).attr('college_name');
		var abbreviation = $(this).attr('abbreviation');
		var description  = $(this).attr('description');
		var term_sy      = $(this).attr('term_sy');
		var year_level   = $(this).attr('year_level');
		var reports      = $(this).attr('reports');
		
		$('#excel_labels').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/posted_running_student'); ?>",
			data: { 
					"college_name": college_name,
					"abbreviation": abbreviation,
					"description": description,
					"term_sy": term_sy,
					"year_level": year_level,
					"reports": reports,
					"action": 'create_posted_running_excel'},
			success: function(response) {
				window.location.href = '<?php print(base_url("downloads/assessment_".$abbreviation."_".$year_level.".xls")); ?>';

				$('#excel_labels').html("<img src='<?php print(base_url('assets/images/excel_icon.png')); ?>' style='height:20px;'> ")
			}
	
		});
			
	});
		
</script>	

		