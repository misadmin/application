<?php

	class Rooms_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListDayNames($day_id) {
			$result = null;
			
			$q1 = "SELECT a.day_code, a.day_name
						FROM
							days AS a
						WHERE
							a.day_code={$this->db->escape($day_id)} ";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		function ListRooms() {
			$result = null;
			
			$query = $this->db->query("
					SELECT
						r.id,
						r.room_no,
						b.bldg_name as building_code,
						r.room_type
					FROM 
						rooms r
					LEFT JOIN
						buildings b
					ON 
						r.buildings_id=b.id
					");
					/*
					SELECT a.id, a.room_no, a.building_code, a.room_type
										FROM rooms AS a, buildings AS b
										WHERE a.buildings_id=b.id ");
					*/
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function ListVacantRooms($academic_terms_id, $start_time, $end_time, $days, $building_id, $rooms_id=FALSE,$course_offerings_slots_id=NULL) {
			$result = null;
			
			//$field_rms = "(208)";
			//NOTE: always show Field (room_id=208)
			/*if (!$rooms_id) { //new schedule
				$q1 = "(SELECT 
							a.room_no, 
							a.id 
						FROM 
							rooms AS a, buildings AS b
						WHERE
							a.buildings_id=b.id AND b.id={$this->db->escape($building_id)}
							AND a.status='active'
							AND a.id NOT IN (SELECT e.rooms_id 
											FROM 
												course_offerings AS c, 
												room_occupancy AS d, 
												course_offerings_slots AS e,
												rooms AS f 
											WHERE
												e.id=d.course_offerings_slots_id
												AND f.id=e.rooms_id 
												AND c.id=e.course_offerings_id 
												AND c.academic_terms_id={$this->db->escape($academic_terms_id)}
												AND (({$this->db->escape($start_time)} BETWEEN ADDTIME(e.start_time,'00:01') 
												AND SUBTIME(e.end_time,'00:01') 
												OR {$this->db->escape($end_time)} BETWEEN ADDTIME(e.start_time,'00:01') 
												AND ADDTIME(e.end_time,'00:01')) 
												AND d.day_name IN $days)
											)
						) 
						UNION
						(SELECT
								a.room_no,
								a.id
							FROM
								rooms AS a, buildings AS b
							WHERE
								a.buildings_id=b.id AND b.id={$this->db->escape($building_id)}
								AND a.status='always on'
						)	
						ORDER BY
							room_no ";
			//print($q1); die();	
			} else { //schedule is changed
				$q1 = "(SELECT 
							a.room_no, 
							a.id 
						FROM 
							rooms AS a, buildings AS b
						WHERE
							a.buildings_id=b.id AND b.id={$this->db->escape($building_id)}
							AND a.status='active'
							AND a.id NOT IN (SELECT e.rooms_id 
											FROM 
												course_offerings AS c, 
												room_occupancy AS d, 
												course_offerings_slots AS e,
												rooms AS f 
											WHERE
												e.id=d.course_offerings_slots_id
												AND f.id=e.rooms_id 
												AND c.id=e.course_offerings_id 
												AND c.academic_terms_id={$this->db->escape($academic_terms_id)}
												AND (({$this->db->escape($start_time)} BETWEEN ADDTIME(e.start_time,'00:01') 
												AND SUBTIME(e.end_time,'00:01') 
												OR {$this->db->escape($end_time)} BETWEEN ADDTIME(e.start_time,'00:01') 
												AND ADDTIME(e.end_time,'00:01')) 
												AND d.day_name IN $days)
										) 			
						)
						UNION				
						(SELECT
							a.room_no,
							a.id
						FROM
							rooms AS a, 
							buildings AS b
						WHERE
							a.buildings_id=b.id AND b.id={$this->db->escape($building_id)}
							AND a.id = {$this->db->escape($rooms_id)}
							AND a.status='active' 
							AND a.id NOT IN (SELECT e.rooms_id 
											FROM 
												course_offerings AS c, 
												room_occupancy AS d, 
												course_offerings_slots AS e,
												rooms AS f 
											WHERE
												e.id=d.course_offerings_slots_id
												AND f.id=e.rooms_id 
												AND c.id=e.course_offerings_id 
												AND c.academic_terms_id={$this->db->escape($academic_terms_id)}
												AND (({$this->db->escape($start_time)} BETWEEN ADDTIME(e.start_time,'00:01') 
												AND SUBTIME(e.end_time,'00:01') 
												OR {$this->db->escape($end_time)} BETWEEN ADDTIME(e.start_time,'00:01') 
												AND ADDTIME(e.end_time,'00:01')) 
												AND d.day_name IN $days)
												AND e.id!={$this->db->escape($course_offerings_slots_id)}
										)  
						)
						UNION
						(SELECT
								a.room_no,
								a.id
							FROM
								rooms AS a, buildings AS b
							WHERE
								a.buildings_id=b.id AND b.id={$this->db->escape($building_id)}
								AND a.status='always on'
						)
						ORDER BY
							room_no ";
			
			}
			*/
			
			
			if (!$rooms_id) { //new schedule
				$q1 = "(SELECT 
							a.room_no, 
							a.id 
						FROM 
							rooms AS a, 
							buildings AS b
						WHERE
							a.buildings_id = b.id 
							AND b.id = {$this->db->escape($building_id)}
							AND a.status = 'active'
							AND a.id NOT IN (SELECT 
													d.rooms_id 
												FROM 
													room_occupancy AS d, 
													rooms AS f 
												WHERE
													f.id=d.rooms_id 
													AND d.academic_terms_id={$this->db->escape($academic_terms_id)}
													AND (({$this->db->escape($start_time)} BETWEEN ADDTIME(d.from_time,'00:01') 
													AND SUBTIME(d.to_time,'00:01') 
													OR {$this->db->escape($end_time)} BETWEEN ADDTIME(d.from_time,'00:01') 
													AND ADDTIME(d.to_time,'00:01')) 
													AND d.day_name IN $days)
											)
						) 
						
						UNION
						
						(SELECT
								a.room_no,
								a.id
							FROM
								rooms AS a, 
								buildings AS b
							WHERE
								a.buildings_id = b.id 
								AND b.id = {$this->db->escape($building_id)}
								AND a.status = 'always on'
						)	
						ORDER BY
							room_no ";
			
			//print($q1); die();	
			} else { //schedule is changed
				$q1 = "(SELECT 
							a.room_no, 
							a.id 
						FROM 
							rooms AS a, 
							buildings AS b
						WHERE
							a.buildings_id=b.id AND b.id={$this->db->escape($building_id)}
							AND a.status='active'
							AND a.id NOT IN (SELECT 
													d.rooms_id 
												FROM 
													room_occupancy AS d, 
													rooms AS f 
												WHERE
													f.id=d.rooms_id 
													AND d.academic_terms_id={$this->db->escape($academic_terms_id)}
													AND (({$this->db->escape($start_time)} BETWEEN ADDTIME(d.from_time,'00:01') 
													AND SUBTIME(d.to_time,'00:01') 
													OR {$this->db->escape($end_time)} BETWEEN ADDTIME(d.from_time,'00:01') 
													AND ADDTIME(d.to_time,'00:01')) 
													AND d.day_name IN $days)
											) 			
						)
						
						UNION				
						
						(SELECT
							a.room_no,
							a.id
						FROM
							rooms AS a, 
							buildings AS b
						WHERE
							a.buildings_id=b.id AND b.id={$this->db->escape($building_id)}
							AND a.id = {$this->db->escape($rooms_id)}
							AND a.status='active' 
							AND a.id NOT IN (SELECT d.rooms_id 
											FROM 
												room_occupancy AS d, 
												room_occupancy_course_offerings_slots AS e,
												rooms AS f 
											WHERE
												e.room_occupancy_id=d.id
												AND f.id=d.rooms_id 
												AND d.academic_terms_id={$this->db->escape($academic_terms_id)}
												AND (({$this->db->escape($start_time)} BETWEEN ADDTIME(d.from_time,'00:01') 
												AND SUBTIME(d.to_time,'00:01') 
												OR {$this->db->escape($end_time)} BETWEEN ADDTIME(d.from_time,'00:01') 
												AND ADDTIME(d.to_time,'00:01')) 
												AND d.day_name IN $days)
												AND e.course_offerings_slots_id != {$this->db->escape($course_offerings_slots_id)}
										)  
						)
						UNION
						(SELECT
								a.room_no,
								a.id
							FROM
								rooms AS a, 
								buildings AS b
							WHERE
								a.buildings_id = b.id 
								AND b.id = {$this->db->escape($building_id)}
								AND a.status = 'always on'
						)
						ORDER BY
							room_no ";
			
			}
			

			//return $q1;
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
				

		function AddRoomOccupancy($occupancy_data) {
			
			$results = $this->ListDayNames($occupancy_data['days_day_code']);
			
			foreach ($results AS $dname) {
				$query = "INSERT INTO 
								room_occupancy 
									(day_name, 
									course_offerings_slots_id)
								VALUES 
									({$this->db->escape($dname->day_name)},
									{$this->db->escape($occupancy_data['course_offerings_slots_id'])})";
				
				if (!$this->db->query($query)) {
					//print ($occupancy_data['course_offerings_slots_id']); die();
					return FALSE;
				}
			}
			//die();
			return TRUE;
			
		}

		function ClearRoomOccupancy($course_offerings_slots) {
			
			$q = "DELETE FROM 
						room_occupancy
					WHERE 
						course_offerings_slots_id = {$this->db->escape($course_offerings_slots)} ";
			
			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				//print($q); die(); 
				return FALSE;
			}			
		}
		
		
		/*ADDED: 11/14/2016 By: Genes
		*
		*/
		function CheckIfRoomOccupied($academic_terms_id, $from_time, $to_time, $days, $rooms_id) {
					
			$q1 = "SELECT 
						d.rooms_id 
					FROM 
						room_occupancy AS d 
					WHERE
						d.academic_terms_id={$this->db->escape($academic_terms_id)}
						AND ({$this->db->escape($start_time)} BETWEEN ADDTIME(d.from_time,'00:01') AND SUBTIME(d.to_time,'00:01') 
							OR {$this->db->escape($end_time)} BETWEEN ADDTIME(d.from_time,'00:01') AND ADDTIME(d.to_time,'00:01')) 
						AND d.day_name IN $days ";

			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
			
		}


	}
	

//end of rooms_model.php