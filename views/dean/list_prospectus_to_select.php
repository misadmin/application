<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
</style>
<div style="background:#FFF; width:70%;">
<div style="width:100%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td align="right">
	 <a class="schedule_slot" href="#" style="text-decoration:none; font-size:12px; font-weight:bold; color:#666; font-family:Verdana, Geneva, sans-serif;"><i class="icon-plus"></i> Create Prospectus</a>
	</td>
  </tr>
</table>

</div>
<div style="width:100%;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="14%" class="header" style="vertical-align:middle;">Code</th>
      <th width="44%" class="header" style="vertical-align:middle;">Program</th>
      <th width="11%" class="header" style="vertical-align:middle;">Program Status</th>
      <th width="9%" class="header" style="vertical-align:middle;">Effective<br />
        Year</th>
      <th width="13%" class="header" style="vertical-align:middle;">Prospectus 
        Status</th>
      <th width="9%" class="header"  style="vertical-align:middle;">Details</th>
    </tr>
    </thead>
    <?php
		if ($prospectus) {
			foreach ($prospectus AS $prospectus) {
	?>
    <tr>
      <td style="text-align:left; vertical-align:middle; "><?php print($prospectus->abbreviation); ?></td>
      <td style="text-align:left; vertical-align:middle; "><?php print($prospectus->description); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($prospectus->program_status); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($prospectus->effective_year); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($prospectus->prospectus_status); ?></td>
      <td style="text-align:center; vertical-align:middle; ">
      <a class="prospectus_id" href="<?php print($prospectus->id); ?>"><i class="icon-edit"></i></a></td>
    </tr>
    <?php
			}
		}
	?>
</table>
</div>
  </div>
  <form id="srcform2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.schedule_slot').click(function(event){
		event.preventDefault(); 
		var offering_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'offering_id',
		    value: offering_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>

  <form id="prospectus2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="5" />
</form>
<script>
	$('.prospectus_id').click(function(event){
		event.preventDefault(); 
		var prospectus_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prospectus_id',
		    value: prospectus_id,
		}).appendTo('#prospectus2');
		$('#prospectus2').submit();
	});
</script>