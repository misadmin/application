
	<div style="width:100%; margin-top:10px;" >
		<div style="width:55%; float:left;" >
			<button  
				grade_reports='<?php print(json_encode($grade_reports,JSON_HEX_APOS)); ?>'
				observed_values='<?php print(json_encode($observed_values,JSON_HEX_APOS)); ?>'
				school_days='<?php print(json_encode($school_days)); ?>'
				present_days='<?php print(json_encode($present_days)); ?>'
				student='<?php print(json_encode($my_student)); ?>'
				class="btn btn-primary download_report_card">Generate Report Card!</button>
		</div>

	</div>

	
<form id="download_report_card_form" method="post" action="<?php print(site_url($this->uri->segment(1).'/process_student_action/'.$idnum));?>" target="_blank">
  <input type="hidden" name="action" value="download_report_card" />
</form>

<script>
	$('.download_report_card').click(function(event){
		event.preventDefault(); 

		var grade_reports   = $(this).attr('grade_reports');
		var observed_values = $(this).attr('observed_values');
		var school_days     = $(this).attr('school_days');
		var present_days    = $(this).attr('present_days');
		var student         = $(this).attr('student');

		var element2    = $("option:selected", "#report_card_pdf");
		var term_text   = element2.attr('term_text');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'grade_reports',
			value: grade_reports,
		}).appendTo('#download_report_card_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'observed_values',
			value: observed_values,
		}).appendTo('#download_report_card_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'school_days',
			value: school_days,
		}).appendTo('#download_report_card_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'present_days',
			value: present_days,
		}).appendTo('#download_report_card_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'term_text',
			value: term_text,
		}).appendTo('#download_report_card_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'student',
			value: student,
		}).appendTo('#download_report_card_form');

		$('#download_report_card_form').submit();
		
	});
</script>
			