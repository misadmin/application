<?php 

/**
 * Model to handle all accountant related database queries.
 * 
 * @author HOLY NAME
 * @copyright HNU-MIS
 * @package HNU-MIS
 * 
 */
class Accountant_model extends MY_Model {
	
	/**
	 * This function retrieves the A/R Balance of the Basic Ed from the ISIS
	 * @param smallint $academic_year_id
	 * @param date $start_date
	 * @param date $end_date
	 * @return boolean
	 */
	function ar_balance_basic_ed_isis_by_group($academic_year_id,$start_date,$end_date){		
					
		$sql = "
			SELECT sh.academic_years_id as academic_year_id,p.levels_id, sh.yr_level as `type`,p.isis_tran_code as code, 
				IF(p.receipt_amount < 0,t.description,t.description2) as trans, 
				if(p.receipt_amount < 0, sum(p.receipt_amount<0), sum(p.receipt_amount>0)) as stud, 
				if(p.receipt_amount < 0, abs(sum(p.receipt_amount)), null) as debit,
				if(p.receipt_amount > 0, sum(p.receipt_amount), null) as credit
			FROM payments 				p
				join payers 			r  on r.id = p.payers_id
				join students 			st on st.idno = r.students_idno
				join basic_ed_histories sh on sh.students_idno = st.idno and sh.levels_id = p.levels_id and sh.academic_years_id = '{$academic_year_id}'
				join teller_codes  		t  on t.teller_code = p.isis_tran_code
			WHERE
				p.levels_id in (2,3,4,5) and p.isis_status = 'active' 
				and p.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL 1 DAY)
				group by levels_id,`type`, code	
		";
		
		$query = $this->db->query($sql);
		return ($query ? $query->result() : FALSE);
	}
	
	/**
	 * This function retrieves the A/R Balance of the Basic Ed from the HNU-MIS
	 * @param date $start_date
	 * @param date $end_date
	 * @return boolean
	 */
	function ar_balance_basic_ed_mis($start_date,$end_date){
		$sql = "
		SELECT * FROM 
		(
			SELECT p.levels_id as `type`,t.teller_code as code, t.description as trans, sum(p.receipt_amount<0) as stud, abs(sum(p.receipt_amount)) as debit, 0.00 as credit
				from payments 			p  
				join ledger l on l.payments_id = p.id 
				join payment_items i on i.payments_id = p.id 
				join teller_codes t on t.id = i.teller_codes_id 
				join payers r on r.id = p.payers_id 
				join students s on s.idno = r.students_idno 
				join academic_years ay 
				join academic_terms tr on tr.academic_years_id = ay.id and tr.id = p.academic_terms_id 
				join basic_ed_histories h on h.academic_years_id = ay.id and h.students_idno = s.idno 
			WHERE
				p.levels_id in (2,3,4,5) and p.receipt_amount < 0
				and p.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL 1 DAY)
				group by `type`, code
		UNION						
			SELECT p.levels_id as `type`,t.teller_code as code, t.description as trans, sum(p.receipt_amount>0) as stud, 0.00 as debit, sum(p.receipt_amount) as credit
				from payments 			p  
				join ledger l on l.payments_id = p.id 
				join payment_items i on i.payments_id = p.id 
				join teller_codes t on t.id = i.teller_codes_id 
				join payers r on r.id = p.payers_id 
				join students s on s.idno = r.students_idno 
				join academic_years ay 
				join academic_terms tr on tr.academic_years_id = ay.id and tr.id = p.academic_terms_id 
				join basic_ed_histories h on h.academic_years_id = ay.id and h.students_idno = s.idno 
			WHERE
				p.levels_id in (2,3,4,5) and p.receipt_amount > 0
				and p.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL 1 DAY)
				group by `type`, code
		) as tmp		
		ORDER by `type`, code		
		";
		$query = $this->db->query($sql);
		return ($query ? $query->result() : FALSE);
	}
	
	/**
	 * This function retrieves the A/R Balance of the college level based on ISIS
	 * @param date $start_date
	 * @param date $end_date
	 * @return boolean
	 */
	function ar_balance_college_isis($start_date,$end_date){		
		//6 - College, 7 - Graduate School
		$sql = "
				SELECT ag.levels_id as type, p.isis_tran_code as code, 
					IF(p.receipt_amount > 0, if(substr(p.isis_refno,1,4)='Adj#' or substr(p.isis_refno,1,3)='AR#' or isnull(p.isis_refno), t.description2,t.description), t.description2) as trans, 
					IF(p.receipt_amount > 0, sum(p.receipt_amount>0),sum(p.receipt_amount<0)) as stud, 
					IF(p.receipt_amount < 0, abs(sum(l.debit)), NULL) as debit, 
					IF(p.receipt_amount > 0, sum(l.credit), NULL) as credit 
				FROM ledger l
					join payments 			 p on p.id = l.payments_id
					join payers 			 r on r.id = p.payers_id
		 			join students  			 s on s.idno = r.students_idno
					join student_histories   sh on sh.students_idno = s.idno 
					join teller_codes 		 t on t.teller_code = p.isis_tran_code	
					join academic_terms      tr on tr.id = p.academic_terms_id and  tr.id = sh.academic_terms_id
					join prospectus          pr on pr.id = sh.prospectus_id
					join academic_programs   ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id 
					and ag.levels_id in (6,7)
				WHERE
					p.levels_id in (6,7) and
					p.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL 1 DAY)
					and p.isis_status='active'
				group by `type`,`code`
				order by `type`,code 
			";
		//print_r($sql);die();
		$query = $this->db->query($sql);		
		
		return ($query ? $query->result() : FALSE);
	}

	/** 
	 * This function retrieves the A/R Balance of the college level from the HNU-MIS
	 * @param date $start_date
	 * @param date $end_date
	 * @return boolean
	 */
	function ar_balance_college_mis($start_date,$end_date){	
		//6 - College, 7 - Graduate School
	/**	$sql_old = "
				SELECT p.levels_id as type, t.teller_code as code, t.description as trans, sum(l.id>0) as stud, 
					IF(p.receipt_amount < 0, abs(sum(l.debit)), NULL) as debit, 
					IF(p.receipt_amount > 0, sum(l.credit), NULL) as credit 
				FROM ledger l
					join payments 			 p  on p.id = l.payments_id
					join payment_items 		 i  on i.payments_id = p.id
					join payers 			 r  on r.id = p.payers_id
					join col_students 		 s  on s.students_idno = r.students_idno
		 			join teller_codes 		 t  on t.id = i.teller_codes_id
					join student_histories   sh on sh.students_idno = s.students_idno  
					join academic_terms 	 tr on tr.id = p.academic_terms_id and  tr.id = sh.academic_terms_id 
					join prospectus 		 pr on pr.id = sh.prospectus_id
					join academic_programs 	 ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
				WHERE 
					l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
				GROUP by p.levels_id,t.teller_code
				ORDER by `type`,trans 		
			";	**/
		$sql = "
		SELECT `type`, code, trans, sum(id>0) as stud, sum(debit) as debit, sum(credit) as credit
			FROM
			(
				SELECT p.levels_id as `type`, t.teller_code as code, t.description as trans, l.id, l.debit, l.credit
					FROM ledger l
					join payments p on p.id = l.payments_id
					join payment_items i on i.payments_id = p.id
					join payers r  on r.id = p.payers_id
					join col_students s on s.students_idno = r.students_idno
					join teller_codes t on t.id = i.teller_codes_id
					join student_histories sh on sh.students_idno = s.students_idno
					join academic_terms tr on tr.id = p.academic_terms_id and  tr.id = sh.academic_terms_id
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
				WHERE
					l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			UNION
				SELECT ag.levels_id as `type`, concat('Adj: ',j.description) as code, j.description as trans, l.id , l.debit, l.credit
					from ledger l
					join adjustments j on j.id = l.adjustments_id
					join payers r on r.id = j.payers_id
					join col_students s on s.students_idno = r.students_idno
					join student_histories sh on s.students_idno = sh.students_idno and sh.academic_terms_id = j.academic_terms_id
					join academic_terms tr on tr.id = j.academic_terms_id
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
				WHERE
					j.posted='Y'
					and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			UNION 
				SELECT ag.levels_id as `type`, 'Assessment' as code, 'Assessment' as trans, l.id , l.debit, l.credit
					FROM ledger l
					join assessments ass on ass.id = l.assessments_id
					join student_histories sh on sh.id = ass.student_histories_id
					join col_students s on s.students_idno = sh.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id					
				WHERE
					l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			UNION 
				SELECT ag.levels_id as type, 'Privilege' as code, sk.scholarship as trans, l.id , l.debit, l.credit
					FROM ledger l 
					join privileges_availed v on v.id = l.privileges_availed_id
					join scholarships sk on sk.id = v.scholarships_id  
					join student_histories sh on sh.id = v.student_histories_id
					join col_students s on s.students_idno = sh.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id		
				WHERE 
					v.posted = 'Y'
					and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			UNION
				SELECT ag.levels_id as type, 'Re-Enrollment' as code, c.course_code as trans, l.id , l.debit, l.credit
					FROM ledger l
					join re_enrollments re on re.id = l.re_enrollments_id
					join enrollments en on en.id = re.enrollments_id
					join course_offerings o on o.id = en.course_offerings_id
					join courses c on c.id = o.courses_id
					join student_histories sh on sh.id = en.student_history_id
					join col_students s on s.students_idno = sh.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id		
				WHERE 
					l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
					
					
			) tmp
		GROUP by `type`,code
		ORDER by `type`,trans
		";
		
		

		
		//print_r($sql);die();
		$query = $this->db->query($sql);	
		return ($query ? $query->result() : FALSE);
	}
	
	
	/**
	 * This function retrieves the A/R Balance of the college level from the ISIS
	 * @param date $start_date
	 * @param date $end_date
	 * @return boolean
	 */
	function ar_balance_college_by_group_isis($start_date,$end_date) {
		//added dummy column -> type
		$sql = "
				SELECT 
					p.levels_id, g.abbreviation as `type`,  p.isis_tran_code as code,
					IF(p.receipt_amount < 0,t.description,t.description2) as trans, 
					IF(p.receipt_amount > 0, sum(p.receipt_amount>0),sum(p.receipt_amount<0)) as stud, 
					IF(p.receipt_amount < 0, abs(sum(p.receipt_amount)), NULL) as debit, 
					IF(p.receipt_amount > 0, sum(p.receipt_amount), NULL) as credit
				FROM 
					payments 				p
					join teller_codes 		t on t.teller_code = p.isis_tran_code
					join payers 			r on r.id = p.payers_id
					join students 			s on s.idno = r.students_idno
					join student_histories 	h on h.students_idno = s.idno 
					join academic_terms 	tr on tr.id = p.academic_terms_id and tr.id = h.academic_terms_id
					join prospectus 		pr on pr.id = h.prospectus_id
					join academic_programs 	ap on ap.id = pr.academic_programs_id
					join acad_program_groups g on g.id = ap.acad_program_groups_id
				WHERE 
					p.levels_id in (6,7) 
					and p.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL 1 DAY)
				GROUP BY 
					p.levels_id,`type`,  code				
				ORDER BY  
					levels_id,`type`, code				
					";
		$query = $this->db->query($sql);			
		return ($query ? $query->result() : FALSE);
	}

	/**
	 * This function retrieves the A/R Balance of the college level grouped by academic programs from the HNU-MIS
	 * @param date $start_date
	 * @param date $end_date
	 * @return boolean
	 */
	function ar_balance_college_by_group_mis($start_date,$end_date) {
		//added dummy column -> type
		$sql = "
			SELECT p.levels_id as type, t.teller_code as code, t.description as trans, sum(l.id>0) as stud,			 
				IF(p.receipt_amount < 0, abs(sum(l.debit)), NULL) as debit, 
				IF(p.receipt_amount > 0, sum(l.credit), NULL) as credit 
			FROM ledger l
				join payments 			 p  on p.id = l.payments_id
				join payment_items 		 i  on i.payments_id = p.id
				join payers 			 r  on r.id = p.payers_id
				join col_students 		 s  on s.students_idno = r.students_idno
				join teller_codes 		 t  on t.id = i.teller_codes_id
				join student_histories   sh on sh.students_idno = s.students_idno  
				join academic_terms 	 tr on  tr.id = p.academic_terms_id and  tr.id = sh.academic_terms_id 
				join prospectus 		 pr on pr.id = sh.prospectus_id
				join academic_programs 	 ap on ap.id = pr.academic_programs_id
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id and ag.levels_id in (6,7)
			WHERE
				l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			group by t.id
			order by trans							
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);	
		return ($query ? $query->result() : FALSE);
	}
	
	/**
	 * This function retrieves the different academic program groups
	 * @return boolean
	 */
	public function get_acad_program_groups(){
		$sql = "SELECT id, abbreviation FROM acad_program_groups WHERE levels_id = 6 AND abbreviation != 'None'";		
		$query = $this->db->query($sql);		
		return ($query ? $query->result() : FALSE);
	}
	
	/**
	 * This function retrieves the distinct year levels found in the basic ed student histories
	 * @return boolean
	 */
	public function get_yr_levels(){
		$sql = "SELECT distinct yr_level FROM basic_ed_histories where levels_id in (2,3,4,5) ";
		$query = $this->db->query($sql);
		return ($query ? $query->result() : FALSE);
	}
	
	/**
	 * This function retrieves the academic years
	 * @return boolean
	 */
	public function get_academic_years(){
		$sql = "
			SELECT id,concat_ws('-',end_year-1,end_year) as academic_year 
			FROM academic_years
			ORDER BY academic_year DESC
			LIMIT 10 
			";
		$query = $this->db->query($sql);
		return ($query ? $query->result() : FALSE);
	}
	
	
	
	//COLLEGE:
	public function ar_balance_college_detail_students_all_mis($trans_code,$abbr,$start_date,$end_date,$trans_type){
		$sql_for_payments = "
			select s.idno, concat_ws(', ',s.lname, s.fname) as student, 
				format(abs(p.receipt_amount),2) as amount, t.description, 
				l.reference_number as ref, 
		        (case p.levels_id
					when 7
			            then concat_ws(' ',ap.abbreviation,h.year_level)
			        when 6
			         	then concat_ws(' ',ap.abbreviation,h.year_level)
			        when 5
			         	then 'Night HS'
			        when 4
			         	then 'Day HS'
			        when 3
			         	then 'GS'
			        when 2
			        	then 'Kinder'
			        when 0
			         	then 'NS'
		            else 
		            	'Unknown Level'
	        	end) as course_year										
			from payments p 
				join ledger 	   l on l.payments_id = p.id
				join payment_items i on i.payments_id = p.id
				join teller_codes  t on t.id = i.teller_codes_id
				join payers        r on r.id = p.payers_id
				join students 	 	s on s.idno = r.students_idno
				join student_histories h on h.students_idno = s.idno and h.academic_terms_id = p.academic_terms_id  
				join prospectus 		 	 pr on pr.id = h.prospectus_id
				join academic_programs   ap on ap.id = pr.academic_programs_id
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id			
				left join basic_ed_histories bh on bh.students_idno=s.idno and bh.levels_id = p.levels_id
				left join basic_ed_sections bes on bes.hs_section_id = bh.basic_ed_sections_id				
				left join basic_ed_sections bes2 on bes2.gs_section_id = bh.basic_ed_sections_id 				
			where
				t.teller_code = {$this->db->escape($trans_code)}
				and l.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "
				.(!empty($abbr) ? " and p.levels_id = {$this->db->escape($abbr)}" : " ")
				.($trans_type == 'credit' ? ' and p.receipt_amount > 0 ' : ' and p.receipt_amount < 0 ')
				."group by idno,p.id
			order by student
			"; 

		$sql_for_assessments = "
			SELECT ass.id, st.idno, concat_ws(', ',st.lname, st.fname) as student, format(ass.assessed_amount,2) as amount, 
					'Assessment' as  description, l.reference_number as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year	 	
			from ledger l
				join assessments ass on ass.id = l.assessments_id 
				join student_histories sh on sh.id = ass.student_histories_id 
				join col_students s on s.students_idno = sh.students_idno 
				join students st on st.idno = s.students_idno 
				join prospectus pr on pr.id = sh.prospectus_id 
				join academic_programs ap on ap.id = pr.academic_programs_id 
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id 
			where 
				l.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "
				.(!empty($abbr) ? " and ag.levels_id = {$this->db->escape($abbr)}" : " ") 										
			."group by st.idno,ass.id
			order by student 
			";

		$sql_for_adjustment = "
			SELECT j.id, st.idno, concat_ws(', ',st.lname, st.fname) as student, format(j.amount,2) as amount,
					j.description, j.id as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year
				from ledger l
				join adjustments j on j.id = l.adjustments_id
				join payers r on r.id = j.payers_id
				join col_students s on s.students_idno = r.students_idno
				join students st on st.idno = s.students_idno 
				join student_histories sh on s.students_idno = sh.students_idno and sh.academic_terms_id = j.academic_terms_id
				join academic_terms tr on tr.id = j.academic_terms_id
				join prospectus pr on pr.id = sh.prospectus_id
				join academic_programs ap on ap.id = pr.academic_programs_id
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id
			WHERE 
				j.posted='Y'
				and j.description = '" . substr($this->db->escape($trans_code),6) .			
				" and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "
				.(!empty($abbr) ? " and ag.levels_id = {$this->db->escape($abbr)}" : " ") 				
			;

		$sql_for_privilege = "
			SELECT v.id, st.idno, concat_ws(', ',st.lname, st.fname) as student, format(j.total_amount_availed,2) as amount,
					sk.scholarship, v.id as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year
				FROM ledger l 
				join privileges_availed v on v.id = l.privileges_availed_id
				join scholarships sk on sk.id = v.scholarships_id  
				join student_histories sh on sh.id = v.student_histories_id
				join col_students s on s.students_idno = sh.students_idno
				join students st on st.idno = s.students_idno 
				join prospectus pr on pr.id = sh.prospectus_id
				join academic_programs ap on ap.id = pr.academic_programs_id
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id		
			WHERE 
				v.posted='Y'
				and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "								
				.(!empty($abbr) ? " and ag.levels_id = {$this->db->escape($abbr)}" : " ") 				
			;
		//@TODO: this query is not yet done 
		$sql_for_reenrollment ="
			SELECT re.id, st.idno, concat_ws(', ',st.lname, st.fname) as student, format(re.amount_credited,2) as amount,
					c.course_code, re.id as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year		
				FROM ledger l
				join re_enrollments re on re.id = l.re_enrollments_id
				join enrollments en on en.id = re.enrollments_id
				join course_offerings o on o.id = en.course_offerings_id
				join courses c on c.id = o.courses_id
				join student_histories sh on sh.id = en.student_history_id
				join col_students s on s.students_idno = sh.students_idno
				join students st on st.idno = s.students_idno 
				join prospectus pr on pr.id = sh.prospectus_id
				join academic_programs ap on ap.id = pr.academic_programs_id
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id		
			WHERE 				
				l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "
				.(!empty($abbr) ? " and ag.levels_id = {$this->db->escape($abbr)}" : " ")				
			;
		
		switch ($trans_code){
			case 'Assessment':
				$sql = $sql_for_assessments ;
				break;
			case 'Privilege':
				$sql = $sql_for_privilege ;
				break;	
			case 'Re-Enrollment':
				$sql = $sql_for_reenrollment;
				break;
			default:
				switch( substr($trans_code,0,5) ){
					case 'Adj: ':
						$sql = $sql_for_adjustment;
						break;
					default:
						$sql = $sql_for_payments;
						break;	
				}	 
		}
		
		//print_r($sql);die();
		$query = $this->db->query($sql);		
		return ($query ? $query : FALSE);
	}

	public function ar_balance_college_detail_students_bygroup_mis($trans_code,$start_date,$end_date,$trans_type){
		$sql = "
			select s.idno, concat_ws(', ',s.lname, s.fname) as student, 
				format(abs(p.receipt_amount),2) as amount, 
				t.description, l.reference_number as ref, 
		        (case p.levels_id
					when 7
			            then concat_ws(' ',ap.abbreviation,h.year_level)
			        when 6
			         	then concat_ws(' ',ap.abbreviation,h.year_level)
			        when 5
			         	then 'Night HS'
			        when 4
			         	then 'Day HS'
			        when 3
			         	then 'GS'
			        when 2
			        	then 'Kinder'
			        when 0
			         	then 'NS'
		            else 
		            	'Unknown Level'
	        	end) as course_year										
			from payments 				p 
				join ledger 			l on l.payments_id = p.id
				join payment_items 		i on i.payments_id = p.id
				join teller_codes 		t on t.id = i.teller_codes_id
				join payers 			r on r.id = p.payers_id 
				join students 			s on s.idno = r.students_idno
				join student_histories 	h on h.students_idno = s.idno and h.academic_terms_id = p.academic_terms_id 
				join prospectus 		pr on pr.id = h.prospectus_id 
				join academic_programs 	ap on ap.id = pr.academic_programs_id 
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id
				left join basic_ed_histories bh on bh.students_idno=s.idno and bh.levels_id = p.levels_id
				left join basic_ed_sections bes on bes.hs_section_id = bh.basic_ed_sections_id				
				left join basic_ed_sections bes2 on bes2.gs_section_id = bh.basic_ed_sections_id 				
			where  
				t.teller_code = {$this->db->escape($trans_code)}
				and l.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "
				.($trans_type == 'credit' ? ' and p.receipt_amount > 0 ' : ' and p.receipt_amount < 0 ')
				."group by idno,p.id
			order by student
		";	
		$query = $this->db->query($sql);
			return ($query ? $query : FALSE);
	}
	
	public function ar_balance_college_detail_students_all_isis($trans_code,$abbr,$start_date,$end_date,$trans_type){
		$sql = "
			select s.idno, concat_ws(', ',s.lname, s.fname) as student, 
				format(abs(p.receipt_amount),2) as amount, t.description, p.isis_refno as ref, 
				concat_ws(' ',ap.abbreviation,h.year_level) as course_year 
			from payments 					p 
				join payers 				r on r.id = p.payers_id
				join students 				s on s.idno = r.students_idno
				join student_histories 	h on h.students_idno = s.idno 
				join prospectus 			pr on pr.id = h.prospectus_id 
				join academic_programs 	ap on ap.id = pr.academic_programs_id 
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id 
				join teller_codes 		t on t.teller_code = p.isis_tran_code 
		
			where
				p.from_isis = 'Y' and p.isis_tran_code = {$this->db->escape($trans_code)}
				and p.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL 1 DAY) "
				.(!empty($abbr) ? " and p.levels_id = {$this->db->escape($abbr)}" : " ")
				.($trans_type == 'credit' ? ' and p.receipt_amount > 0 ' : ' and p.receipt_amount < 0 ')
			."group by idno,p.id
			order by student
		";
		$query = $this->db->query($sql);
		return ($query ? $query : FALSE);
	}
	
	public function ar_balance_college_detail_students_bygroup_isis($trans_code,$abbr,$start_date,$end_date,$trans_type){			
		$sql="	
			select s.idno, concat_ws(', ',s.lname, s.fname) as student, format(abs(p.receipt_amount),2) as amount, tc.description, p.isis_refno as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year
				from students 			 s
				join student_histories   sh on sh.students_idno = S.idno
				join prospectus 		 pr on pr.id = sh.prospectus_id
				join academic_programs   ap on ap.id = pr.academic_programs_id
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id
				join payers 			 r  on r.students_idno = s.idno
				join payments 			 p  on p.payers_id = r.id and p.academic_terms_id = sh.academic_terms_id
				join teller_codes 		 tc on tc.teller_code = p.isis_tran_code
			where
				p.from_isis = 'Y' and p.levels_id in (6,7)
				and p.isis_tran_code = {$this->db->escape($trans_code)}
				and p.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL 1 DAY) "
				.(!empty($abbr) ? " and ag.abbreviation = {$this->db->escape($abbr)}" : " ")
				.($trans_type == 'credit' ? ' and p.receipt_amount > 0 ' : ' and p.receipt_amount < 0 ')
				."group by idno,p.id
			order by student
		";
		$query = $this->db->query($sql);
			return ($query ? $query : FALSE);
	}
	
	
	
	//BASIC ED:
	public function ar_balance_basic_detail_students_isis($academic_year_id,$levels_id,$yr_level,$trans_code,$start_date,$end_date){
		$sql="
			select s.idno, concat_ws(', ',s.lname,s.fname) as student,
				format(abs(p.receipt_amount),2) as amount, 
				tc.description, p.isis_refno as ref,
				concat_ws(' - ',l.`level`,sh.yr_level) as course_year
			from payments 				p 
				join payers  			r  on r.id = p.payers_id
				join students 			s  on s.idno = r.students_idno
				join basic_ed_histories sh on sh.students_idno = s.idno  and sh.levels_id = p.levels_id and sh.academic_years_id = '{$academic_year_id}'
				join levels 			l  on l.id = sh.levels_id 
				join teller_codes 		tc on tc.teller_code = p.isis_tran_code						
			where
				p.from_isis = 'Y'
				and p.levels_id =  '{$levels_id}'
				and sh.yr_level = '{$yr_level}' 
				and p.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL 1 DAY) 
				and p.isis_tran_code = '{$trans_code}'
			group by idno,p.id
			order by student
		";			
		//print_r($sql);die();
		$query = $this->db->query($sql);		
		return ($query);
	}	

	public function ar_balance_basic_detail_students_mis($trans_code,$start_date,$end_date,$abbr){
		$sql="
			select s.idno, concat_ws(', ',s.lname,s.fname) as student, 
				format(abs(p.receipt_amount),2) as amount, 
				t.description, l.reference_number as ref,
				concat_ws(' - ',v.level,h.yr_level) as course_year
			from payments p
				join ledger 	   l on l.payments_id = p.id
				join payment_items i on i.payments_id = p.id
				join teller_codes  t on t.id = i.teller_codes_id
				join payers        r on r.id = p.payers_id
				join students 	 	s on s.idno = r.students_idno
				join academic_years ay 
				join academic_terms tr on tr.academic_years_id = ay.id and tr.id = p.academic_terms_id
				join basic_ed_histories h on h.academic_years_id = ay.id and h.students_idno = s.idno
				join levels        v on v.id = p.levels_id
			where
				p.levels_id = '{$abbr}' 
				and p.transaction_date between '{$start_date}' and DATE_ADD('{$end_date}', INTERVAL 1 DAY)
				and t.teller_code = '{$trans_code}'  
			group by idno,p.id
			order by student		
		";
		//print_r($sql);die();		
		$query = $this->db->query($sql);
		return ($query);
	}

	public function get_students_with_written_off_records($start_stud="A",$end_stud="ZZ",$limit=500){	
		$sql = "
			SELECT * FROM (
				select 
					l.id as ledger_id,
					s.idno, cap_first(concat_ws(', ',s.lname,s.fname)) as student, 
					date_format(l.transaction_date,'%Y-%m-%d') transaction_date , 
					if(p.receipt_amount > 0, tc.description, tc.description2) as description /*only true for from_isis*/,
					p.isis_refno as refno, 
					l.debit, l.credit
				from ledger l
				join payments p on p.id = l.payments_id
				left join payers   r on r.id = p.payers_id
				left join students s on s.idno = r.students_idno
				left join teller_codes tc on tc.teller_code = p.isis_tran_code
				where p.from_isis = 'Y' 
					and p.isis_status = 'written_off'
				limit {$limit}
			) tmp
			where student between '{$start_stud}' and '{$end_stud}'
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query AND $query->num_rows() > 0)
			$result = $query->result();
		return isset($result) ? $result : FALSE;
		
	}

	public function get_payers($start_stud="A",$end_stud="ZZ",$limit=500){
		$sql = "	
			select r.id,s.idno, concat_ws(', ',s.lname,s.fname) as student, sh.abbreviation, sh.year_level
			from col_students cs
			join students s on s.idno = cs.students_idno
			join payers r on r.students_idno = s.idno
			join (
					select h.students_idno, ag.abbreviation, h.year_level,tr.id
					from student_histories h
					join prospectus p on p.id = h.prospectus_id
					join acad_program_groups ag on ag.id = p.academic_programs_id
					join academic_terms tr on tr.id = h.academic_terms_id
					join academic_years ay on ay.id = tr.academic_years_id
					group by ay.end_year desc, tr.term desc, h.students_idno
				) sh on sh.students_idno = s.idno
			where s.lname between '{$start_stud}' and '{$end_stud}' 
			limit {$limit}
						";
		$query = $this->db->query($sql);
		if ($query AND $query->num_rows() > 0)
			$result = $query->result();
		return isset($result) ? $result : FALSE;
		
	}

	public function get_payers_ids($stud_start,$stud_end){
		$sql="
			select r.id, s.idno,concat_ws(', ',s.lname,s.fname) as student,sh.abbreviation,sh.year_level
			from payers r
			join students s on s.idno = r.students_idno
			join (
					select * from 
						(select h.academic_terms_id as hist_id, h.students_idno,ap.abbreviation,h.year_level
						from student_histories h
						join prospectus p on p.id =  h.prospectus_id
						join academic_programs ap on ap.id = p.academic_programs_id		
						order by h.academic_terms_id desc
						) t
					group by students_idno
				) sh on sh.students_idno = s.idno
			where concat_ws(', ',s.lname,s.fname) between '{$stud_start}' and '{$stud_end}'
			order by abbreviation, student
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query AND $query->num_rows() > 0)
			$result = $query->result();
		return isset($result) ? $result : FALSE;	

		
	
	
	}
	
	public function get_balance($payers_id){
		$sql = "
			select payers_id, sum(if(!isnull(debit),debit,0) - if(!isnull(credit),credit,0)) as balance
			FROM 
				(
					select r.id as payers_id, l.debit, l.credit ,l.transaction_date
					from ledger l 
					left join assessments a on a.id = l.assessments_id
					left join student_histories h on h.id = a.student_histories_id
					left join payers r on r.students_idno = h.students_idno 
					where r.id = {$payers_id}
				UNION 
					select r.id as payers_id, l.debit, l.credit ,l.transaction_date
					from ledger l
					left join payments p on p.id = l.payments_id
					left join payers r on r.id = p.payers_id
					where r.id = {$payers_id}
				UNION  
					select j.payers_id, l.debit, l.credit ,l.transaction_date
					from ledger l
					left join adjustments j on j.id = l.adjustments_id 
					where j.payers_id = {$payers_id}
				UNION
					select r.id as payers_id, l.debit, l.credit ,l.transaction_date
					from ledger l
					left join privileges_availed v  on v.id = l.privileges_availed_id 
					left join student_histories  h on h.id = v.student_histories_id
					left join payers r on r.students_idno = h.students_idno
					where r.id = {$payers_id}
				UNION 
					select r.id as payers_id, l.debit, l.credit ,l.transaction_date
					from ledger l
					left join re_enrollments re on re.id = l.re_enrollments_id 
					left join enrollments enr on enr.id = re.enrollments_id
					left join student_histories sh on sh.id = enr.student_history_id
					left join payers r on r.students_idno = sh.students_idno 
					where r.id = {$payers_id}
				) t 
			group by payers_id
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result_array(); else
			return FALSE;
		
	
	}
	
	
	
	public function get_students_balances(){
		$sql = "
				
		";
		$query = $this->db->query($sql);
		if ($query AND $query->num_rows() > 0)
			$result = $query->result();
		return isset($result) ? $result : FALSE;		
	}
	
	
	//Added: 10/28/2013
	
	function ListTotalWithdrawal($academic_terms_id) {
		$result = NULL;
			
		$q = "SELECT
			a.idno,
			a.lname,
			a.fname,
			a.mname,
			b.year_level,
			d.abbreviation,
			DATE_FORMAT(e.withdrawn_on, '%b. %d, %Y')  as withdrawn_on
		FROM
			students as a,
			student_histories as b,
			prospectus as c,
			academic_programs as d,
			enrollments AS e
		WHERE
			b.academic_terms_id={$this->db->escape($academic_terms_id)}
			AND b.totally_withdrawn = 'Y'
			AND b.students_idno = a.idno
			AND b.prospectus_id = c.id
			AND c.academic_programs_id = d.id
			AND b.id = e.student_history_id 
		GROUP BY b.students_idno
		ORDER BY a.lname, a.fname";
		
			
		//print($q);
		//die();
		$query = $this->db->query($q);
			
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
	}
	function ListPartialWithdrawals($academic_terms_id) {
		$result = NULL;
			
		$q = "SELECT
			a.idno,
			concat(a.lname, ', ', a.fname, ' ', a.mname) as name,
			b.year_level,
			d.abbreviation,
			b.id as history_id
		FROM
			students as a,
			student_histories as b,
			prospectus as c,
			academic_programs as d,
			enrollments AS e
		WHERE
			b.academic_terms_id={$this->db->escape($academic_terms_id)}
			AND b.totally_withdrawn = 'N'
			AND b.students_idno = a.idno
			AND b.prospectus_id = c.id
			AND c.academic_programs_id = d.id
			AND b.id = e.student_history_id
			AND e.withdrawn_on IS NOT NULL
		GROUP BY
			b.students_idno	
		ORDER BY 
			a.lname, a.fname";
	
			
		//print($q);
		//die();
		$query = $this->db->query($q);
			
		if($query->num_rows() > 0){
		$result = $query->result();
	}
	
	return $result;
	}
}