<style>
	div#msg_header_assign {
		border-radius: 5px; 
		background-color: #D7D4D4; 
		border: solid; 
		border-width: 1px; 
		border-color: #B7B6B6; 
		text-align:left; 
		padding-left: 10px;
		margin:0 auto;
		margin-bottom:20px;
		font-family: arial;
		font-size: 14px;
		width: 98%;
		height: 35px;
		line-height: 35px;
		font-weight:bold;
		color:#B50303;
</style>

<div class="row-fluid" >
<?php
	if (!$is_enrolled and $totally_withdrawn=="N") {
?>
		<div id="msg_header_assign"> 
				Student is not enrolled yet!
		</div>
<?php 
    } elseif ($totally_withdrawn=="Y"){
?>
		<div id="msg_header_assign"> 
				Student has totally withdrawn!
		</div>
<?php

	} else {
?>
		<div id="msg_header_assign" style="display:none;">
		</div>

		<div class="form-horizontal">
			<fieldset>
				<form id="proceed_assign_to_section_form" class="form-horizontal">
					<div class="control-group formSep">
						<label for="grade_level" class="control-label">Block Section</label>
						<div class="controls">
							<select id="block_sections_id_to_assign" name="block_sections_id_to_assign" class="form-control" >
								<option value="0">--Select Section--</option>
								<?php 
									if ($block_sections) {
										foreach ($block_sections AS $section) {
								?>
										<option value="<?php print($section->id); ?>" ><?php print("Grade ".$section->yr_level.'-'.$section->section_name.' '.$section->abbreviation); ?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<div class="controls" style="float:left; padding-right:10px;">
							<input type="submit" class="btn btn-danger" name="assign_to_section" id="assign_to_section" value="Assign to Section!" disabled />
							
							<!-- <a href="<?php //print($idnum); ?>" 
								student_history_id="<?php //print($student_history_id); ?>" 
								class="btn btn-warning assign_to_section" >Assign to Section!
							</a> -->
						</div>
						<div style="float:left; padding-right:8px; margin-top:5px;" id="show_assign_to_section_icon">
						</div>		
					</div>
				</form>
			</fieldset>
		</div>
<?php 
	} 
?>

</div>


<script>
	$('#proceed_assign_to_section_form').submit(function(e){
		e.preventDefault();		
		
		var block_sections_id = $('#block_sections_id_to_assign').val();
		
		var confirmed = confirm('Assign student to section?');

		if (confirmed){
		
			var students_idno      = "<?php print($idnum); ?>";
			var student_history_id = "<?php print($student_history_id); ?>";

			$('#show_assign_to_section_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+students_idno,
				data: { student_history_id: student_history_id,
						block_sections_id: block_sections_id,
						action: 'assign_to_section'},
				dataType: 'json',
				success: function(response) {				

					$('#show_extracted_items').html(response.output); 	

					$('h4.media-heading').html(response.student_info);

					$('#show_assign_to_section_icon').html("");
					
					if (response.success) {
						$('#msg_header_assign').show();	
						$('#msg_header_assign').html("Student successfully assigned to section!");
					}
				}
			});
		}
	});

	function isEmpty(v) {
		if (v === '' || v === null) {
			return true;
		} else {
			return false;
		}
	}

</script>



<script>
$('#block_sections_id_to_assign').bind('click', function(event){
	event.preventDefault();

		var element           = $("option:selected", "#block_sections_id_to_assign");
		var block_sections_id = element.val();

		if (block_sections_id != 0) {
			$('#assign_to_section').prop("disabled", false);
		} else {
			$('#assign_to_section').prop("disabled", true);
		}
});
	
</script>


