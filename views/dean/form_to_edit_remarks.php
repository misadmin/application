<h2 class="heading">Edit Remark</h2>
<?php $this->form_lib->content(FALSE);?>
<div class="fluid span12">
	<form action="<?php echo site_url('dean/student/' . $this->uri->segment(3)); ?>" method="post"  class="form-horizontal" id="edit_remarks">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="edit_remarks" />
		<input type="hidden" name="remark_id" value="<?php print($id); ?>" />
		<fieldset>
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Remarks</label>
					<div class="controls">
						<input name="remark" type="text" id="max_stud" maxlength="50" value="<?php print($remark); ?>" style="width:750px;" >
					</div>
				
				</div>
				<div class="control-group formSep">
					<label for="view_rights" class="control-label">View Rights</label>
					<?php if($view_right == 'Private') { ?>	
					   <div class="controls">
						<input type="radio" name="view_rights" value="1" checked="checked"/> Own use <br>
						<input type="radio" name="view_rights" value="0" /> Student
				      </div>
				    <?php }else { ?>  
				       <div class="controls">
						<input type="radio" name="view_rights" value="1" /> Own use <br>
						<input type="radio" name="view_rights" value="0" checked="checked"/> Student
				      </div>
				     <?php }?> 
				    
		  		</div>
		  		
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit" name="edit_remark" id="edit_remark">Edit Remarks</button>
					</div>
				</div>
		</fieldset>
		<?php 
		//  $this->form_lib->set_id('create_course_form');
		   $this->load->library('form_validation');
		   if (form_error('remarks')) 
		      $this->form_lib->enqueue_text_helper('remarks',form_error('remarks'),'error'); 
		 // $this->form_lib->content(FALSE);
	   ?>
	</form>
</div>

<script>
$('#edit_remark').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update remark?");
    
	if (confirmed){
		$('#edit_remarks').submit();
	}
});
</script>
