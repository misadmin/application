<?php

class Educationalinfo_Model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function preliminary_education ($student_idno){
		$this->db->select ("se.id as student_educations_id, se.educ_level as level, s.school, CONCAT_WS('-', se.sy_start, se.sy_end) as school_year", FALSE)
			->from('student_educations se')
			->join('schools s', 'se.schools_id=s.id', 'left')
			->where('se.students_idno', $student_idno)
			->order_by('se.sy_start, se.sy_end');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function delete_educational_info ($se_id){
		$this->db->where('id', $se_id);
		$this->db->delete('student_educations');
		return $this->db->affected_rows();
	}
	public function insert_educational_info ($student_idno, $educational_level, $school, $sy_start, $sy_end){
		$school_id = $this->school_id($school);
		$data = array(
				'students_idno'=>$student_idno,
				'educ_level'=>ucfirst($educational_level),
				'schools_id'=>$school_id,
				'sy_start'=>$sy_start,
				'sy_end'=>$sy_end,
				);
		$this->db->insert('student_educations', $data);
		return $this->db->insert_id();	
	}
	
	public function school_id ($school){
		$this->db->select('id')
			->from('schools')
			->where('schools.school', $school)
			->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0)
			return $query->row()->id; else {
			$school_data = array(
					'school'=>$school);
			$this->db->insert('schools', $school_data);
			return $this->db->insert_id();
		}
			
	}
	
	public function update_educational_info ($student_educations_id, $educational_level, $school, $sy_start, $sy_end){
		$school_id = $this->school_id($school);
		$data = array(
				'educ_level'=>ucfirst($educational_level),
				'schools_id'=>$school_id,
				'sy_start'=>$sy_start,
				'sy_end'=>$sy_end,
		);
		
		$this->db->where('id', $student_educations_id);
		$this->db->update('student_educations', $data);
		return $this->db->affected_rows();
	}
}