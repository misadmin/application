
			<div style="padding:3px; overflow:auto;">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:10%;">Abbreviation</td>
								<td class="shs_header" style="width:45%;">Year Level Description</td>
								<td class="shs_header" style="width:10%;">Year Level<br>Status</td>
								<td class="shs_header" style="width:10%;">Effective<br>Year</td>
								<td class="shs_header" style="width:10%;">Prospectus<br>Status</td>
								<td class="shs_header" style="width:5%;">Detailed<br>Subjects</td>
								<td class="shs_header" style="width:5%;">Update</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($prospectuses) {
									foreach($prospectuses AS $prospectus) {
										if ($prospectus->status == 'A') {
											$bg = "#FFFFFF";
										} else {
											$bg = "#FFA5A5";
										}
							?>
							<tr style="background-color:<?php print($bg); ?>" >
									<td style="background-color:<?php print($bg); ?>; text-align:center;"><?php print($prospectus->abbreviation); ?></td>
									<td style="background-color:<?php print($bg); ?>"><?php print($prospectus->description); ?></td>
									<td style="background-color:<?php print($bg); ?>; text-align:center;"><?php print($prospectus->program_status); ?></td>
									<td style="background-color:<?php print($bg); ?>; text-align:center;"><?php print($prospectus->effective_year); ?></td>
									<td style="background-color:<?php print($bg); ?>; text-align:center;"><?php print($prospectus->prospectus_status); ?></td>
									<td style="background-color:<?php print($bg); ?>; text-align:center;">
										<a href="<?php print(site_url($this->uri->segment(1).'/prospectus_courses_management').'/'.$prospectus->id);?>" >
											<i class="icon-zoom-in"></i>
										</a>
									</td>
									<td style="background-color:<?php print($bg); ?>; text-align:center;">
										<a href="<?php print($prospectus->id); ?>" 
											description="<?php print($prospectus->description); ?>"
											status="<?php print($prospectus->status); ?>"
											effective_year="<?php print($prospectus->effective_year); ?>"
											class="update_prospectus" >
											<div id="show_pencil_icon_<?php print($prospectus->id); ?>" >
												<i class="icon-pencil"></i>
											</div>
										</a>
									</td>
									<td style="background-color:<?php print($bg); ?>; text-align:center;">
										<a href="<?php print($prospectus->id); ?>" class="del_prospectus" >
											<div id="show_trash_icon_<?php print($prospectus->id); ?>" >
												<i class="icon-trash"></i>
											</div>
										</a>
									</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>


<script>
	$('.del_prospectus').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete this prospectus?');

		if (confirmed){
			
			var prospectus_id = $(this).attr('href');

			$('#show_trash_icon_'+prospectus_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/prospectus_management');?>',
				data: { 
						prospectus_id: prospectus_id,
						action: 'delete_prospectus'},
				dataType: 'json',
				success: function(response) {				
					if (!response.msg) {
						$('#show_error_msg').html('Cannot be deleted because Prospectus already in use!'); 							
						$('#show_trash_icon_'+prospectus_id).html("<i class='icon-trash'></i>")
					} else {
						$('#show_error_msg').html(''); 	
						$('#show_extracted_items').html(response.output); 	
					}					
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
		
		}

	});
</script>


<script>
	$('.update_prospectus').click(function(event){
		event.preventDefault();

		var prospectus_id  = $(this).attr('href');
		var status         = $(this).attr('status');
		var effective_year = $(this).attr('effective_year');
		
		$('#show_error_msg').html('');

		$('#modal_update_prospectus').modal('show');
		
		$('#prospectus_id_update_modal').val(prospectus_id);
		$('#status_update_modal').val(status);
		$('#effective_year_update_modal').val(effective_year);
		$('#decription_update_modal').val($(this).attr('description'));
		
	});
</script>				


	<div id="modal_update_prospectus" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_prospectus_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Prospectus</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="prospectus_id_update_modal"></div>
						<?php														
							$this->load->view('bed/prospectus/modal_view/update_prospectus_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	
<script>
	$('#update_prospectus_form').submit(function(e) {
		e.preventDefault();
		
		var prospectus_id   = $('#prospectus_id_update_modal').val();
		var effective_year = $('#effective_year_update_modal').val();
		var status         = $('#status_update_modal').val();

		$('#modal_update_prospectus').modal('hide');

		$('#show_pencil_icon_'+prospectus_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/prospectus_management'); ?>",
			data: {"prospectus_id": prospectus_id,
					"effective_year": effective_year,
					"status": status,
					"action": "update_prospectus" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_items').html(response.output); 
				
				$('#show_pencil_icon_'+prospectus_id).html("<i class='icon-pencil'></i>")
				
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
		});
		
	});
</script>				


<!--
<script>
	$('.proceed_to_prospectus_courses').click(function(event){
		event.preventDefault(); 
			
		var prospectus_id = $(this).attr('href');

		$('#show_zoom_icon_'+prospectus_id).html("<img src='<?php //echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
		$.ajax({
			cache: false,
			type: 'POST',
				url: "<?php //print(site_url($this->uri->segment(1).'/prospectus_courses_management/'));?>",
				data: {"prospectus_id":	prospectus_id,
						"action": "proceed_to_prospectus_courses",
						},
				dataType: 'json',
				success: function(response) {				
					$('#show_prospectus_management_page').html(response.output); 	
				}
		});
		
	});
</script>
-->

			