
<?php

	class shs_fix_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}


		public function fix_course_offerings() {
		
			$this->CI->load->model('hnumis/shs/shs_fix_model');
			$this->CI->load->model('hnumis/shs/course_offerings_model');
			$this->CI->load->model('hnumis/shs/section_model');
			
			$data['course_offerings'] = $this->CI->shs_fix_model->List_CourseOfferings();

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_offerings');
			
			switch ($action) {
				case 'list_offerings':
		
					break;
				
				case 'extract_same_offerings':
							
					$data['same_offerings']   = $this->CI->shs_fix_model->ListSame_Offerings($this->CI->input->post('block_sections_id'),$this->CI->input->post('academic_terms_id'),$this->CI->input->post('courses_id'));
					
					$data['new_offerings_id'] = $this->CI->input->post('course_offerings_id');
					
					$my_return['output']    = $this->CI->load->view('shs/fix/display_same_offerings', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'delete_course_offering':
				
					$this->CI->db->trans_start();

					$offerings_slots = $this->CI->course_offerings_model->CountOffering_Slots($this->CI->input->post('course_offerings_id'));
					
					if ($offerings_slots->cnt > 1) { //if more than 1, delete only the slot
						if ($this->CI->section_model->DeleteRoomOccupancy($this->CI->input->post('course_offerings_slots_id'))) {
							if ($this->CI->course_offerings_model->DeleteOffering_Slots($this->CI->input->post('course_offerings_slots_id'))) {
								$this->CI->db->trans_complete();
								$my_return['msg'] = TRUE;
							} else {
								$this->CI->db->trans_rollback();
								$my_return['msg'] = FALSE;
							}
						} else {
							$this->CI->db->trans_rollback();
							$my_return['msg'] = FALSE;
						}
	
					} else { //if remaining slot, delete offering; already cascaded to child tables 
						
						if ($this->CI->section_model->DeleteRoomOccupancy($this->CI->input->post('course_offerings_slots_id'))) {
							if ($this->CI->course_offerings_model->DeleteOffering($this->CI->input->post('course_offerings_id'))) {
								$this->CI->db->trans_complete();
								$my_return['msg'] = TRUE;
							} else {
								$this->CI->db->trans_rollback();
								$my_return['msg'] = FALSE;
							}
						} else {
							$this->CI->db->trans_rollback();
							$my_return['msg'] = FALSE;
						}
						
					}

					$data['same_offerings']   = $this->CI->shs_fix_model->ListSame_Offerings($this->CI->input->post('block_sections_id'),$this->CI->input->post('academic_terms_id'),$this->CI->input->post('courses_id'));

					$my_return['output'] = $this->CI->load->view('shs/fix/display_same_offerings', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_course_offerings':

					if ($this->CI->shs_fix_model->Update_Course_Offerings_Slots($this->CI->input->post('course_offerings_slots_id'),$this->CI->input->post('new_offerings_id'))) {
						$my_return['msg'] = TRUE;						
					} else {
						$my_return['msg'] = FALSE;						
					}
					
					$data['course_offerings'] = $this->CI->shs_fix_model->List_CourseOfferings();

					$my_return['output'] = $this->CI->load->view('shs/fix/fix_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
									
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/fix/fix_management',$data);
			$this->CI->content_lib->content();
				
		}
		
		
		public function fix_bed_histories() {
			
			$this->CI->load->model('hnumis/bed/bed_student_model');
						
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_offerings');
			
			switch ($action) {
				case 'list_offerings':
		
					break;
					
				case 'fix_histories':

					$students = $this->CI->bed_student_model->listStudentsToFix();
			// print_r($students); die();
					if ($students) {
						$cnt=0;
						foreach($students AS $stud) {

							$preschool 	= array(11);
							$kinder 	= array(1,2);
							$gs		 	= array(3);
							$hs		 	= array(4,5);
							
							$yr_level = $stud->yr_level;
							
							if (in_array($stud->levels_id, $preschool)) {
								$levels_id = 11;							
							} elseif (in_array($stud->levels_id, $kinder)) {
								$levels_id = 1;
							} elseif (in_array($stud->levels_id, $gs)) {
								$levels_id = 3;
							} elseif (in_array($stud->levels_id, $hs)) {
								$levels_id = 13;
							} 
							
							if ($stud->levels_id == 4 AND $stud->yr_level == 7) {
								$levels_id = 13;
								$yr_level  = 1;
							} 
							
							if ($stud->levels_id == 4 AND $stud->yr_level == 8) {
								$levels_id = 13;
								$yr_level  = 2;
							}
							
							if ($stud->levels_id == 4 AND $stud->yr_level == 9) {
								$levels_id = 13;
								$yr_level  = 3;
							}

							if ($stud->levels_id == 4 AND $stud->yr_level == 10) {
								$levels_id = 13;
								$yr_level  = 4;
							}
														
							if ($stud->levels_id == 5 AND $stud->yr_level == 7) {
								$levels_id = 13;
								$yr_level  = 1;
							}
							
							if ($stud->levels_id == 5 AND $stud->yr_level == 8) {
								$levels_id = 13;
								$yr_level  = 2;
							}
							
							if ($stud->levels_id == 5 AND $stud->yr_level == 9) {
								$levels_id = 13;
								$yr_level  = 3;
							}

							$prospectus = $this->CI->bed_student_model->getProspectus($yr_level, $levels_id); 
							
							if ($prospectus) {
								$this->CI->bed_student_model->fixProspectus($stud->id, $prospectus->id);
							}
							
							$cnt++;
							
						}
					}
					
					$my_return['output'] = $cnt;
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
			}
			
			$data = NULL;
			
			$this->CI->content_lib->enqueue_body_content('bed/fix/fix_management',$data);
			$this->CI->content_lib->content();
							
		}
		
		
	}
	
?>