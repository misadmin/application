<?php
class form_19_print extends Print_lib {
	private $page_width;
	private $max_lines_per_page;
	private $table_def;
	private $add_ons;
	private $grades_table_def;
	
	public function __construct(){
		$this->page_width = 136;
		$this->max_lines_per_page = 60;
		$this->table_def = array(6, 10, 80, 40);
		$this->grades_table_def = array(16, 26, 8, 6, 26, 8, 6, 26, 8, 6);
		$this->add_ons = 12;
	}
	
	public function align_right ($string){
		return parent::align_right($string, $this->page_width);
	}
	public function align_center($string){
		return parent::align_center($string, $this->page_width);
	}
	private function page_header($academic_term, $academic_program, $year_level){
		$content = $this->bold() . $this->align_right('Form XIX') . $this->regular() . "\n";
		$content .= $this->align_right($year_level . " " . $academic_program) . "\n";
		$content .= $this->align_right($academic_term) . "\n\n";
		$content .= $this->align_right('Run Date: ' . str_pad(date('m/d/Y'), 12, ' ', STR_PAD_LEFT)) . "\n";
		$content .= $this->align_right('Run Time: ' . str_pad(date('h:i:s a'), 12, ' ', STR_PAD_LEFT)) . "\n";
		$content .= $this->horiz_line();
		$content .= $this->table(
				$this->table_def, 
				array(
						array(
							(object)array('content'=>'', 'align'=>'left'),
							(object)array('content'=>'ID #', 'align'=>'center'),						
							(object)array('content'=>'Student Name', 'align'=>'left'),
							(object)array('content'=>'', 'align'=>'left'),
						)
				));
		
		$content .= $this->horiz_line();
		return $content;
	}
	//horizontal line
	private function horiz_line(){
		return str_pad("", $this->page_width, "-", STR_PAD_BOTH) . "\n";
	}
	private function page_footer ($academic_program, $year_level, $page){
		$content = "\n";
		$content .= $this->horiz_line();
		$content .= $this->align_center("{$year_level} {$academic_program} Page: {$page}") . $this->char_form_feed();
		return $content;
	}
	private function format_grades_rows ($courses_grades=array()){
		$content = '';
		$count = 0;
		do {
			$continue = TRUE;
			$content .= $this->table($this->grades_table_def, 
					array(
							array(
								(object)array('content'=>'', 'align'=>'left'),
								(object)array('content'=>(isset($courses_grades[$count]->course) ? $courses_grades[$count]->course : ''), 'align'=>'left'),
								(object)array('content'=>(isset($courses_grades[$count]->final_grade) ? $courses_grades[$count]->final_grade : ''), 'align'=>'right'),
								(object)array('content'=>'|', 'align'=>'center'),
								(object)array('content'=>(isset($courses_grades[$count+1]->course) ? $courses_grades[$count+1]->course : ''), 'align'=>'left'),
								(object)array('content'=>(isset($courses_grades[$count+1]->final_grade) ? $courses_grades[$count+1]->final_grade : ''), 'align'=>'right'),
								(object)array('content'=>'|', 'align'=>'center'),
								(object)array('content'=>(isset($courses_grades[$count+2]->course) ? $courses_grades[$count+2]->course : ''), 'align'=>'left'),
								(object)array('content'=>(isset($courses_grades[$count+2]->final_grade) ? $courses_grades[$count+2]->final_grade : ''), 'align'=>'right'),
								(object)array('content'=>'', 'align'=>'right'),
								)	
							));
			$count += 3;
			if(isset($courses_grades[$count]->course)){
				$continue = TRUE;
			} else {
				$continue = FALSE;
			}
		} while ($continue);
		return $content;
	}
	private function student_contents ($students=array()){
		$count = 1;
		$content = "";
		if (is_array($students) && count($students) > 0){
			foreach ($students as $student){
				$content .= $this->table($this->table_def,
						array(
							array(
								(object)array('content'=>$count.".", 'align'=>'right'),
								(object)array('content'=>$student->student_details->id_number,'align'=>'center'),
								(object)array('content'=>$student->student_details->fullname, 'align'=>'left', 'weight'=>'bold'),
								(object)array('content'=>'Total Units: ' . $student->student_details->total_units, 'align'=>'left'),
							)
						));
				$content .= $this->format_grades_rows($student->courses);
				$content .= $this->horiz_line();
				$count++;	
			}
		}
		return explode("\n", $content);
	}
	public function content($data){
		$details = $data['program_details'];
		if(!isset($details->academic_term))
			return '';
		$students = $data['result'];
		$lines = $this->student_contents($students);
		$line_count = 0;
		$page = 1;
		$content = array();
		do {
			$page_content = $this->page_header($details->academic_term, $details->program_abbreviation, $details->year_level);
			for ($count=0; $count < ($this->max_lines_per_page - $this->add_ons); $count++){
				$page_content .= (isset($lines[$line_count]) ? $lines[$line_count]."\n" : "\n");
				$line_count++; 
			}
			$page_content .= $this->page_footer($details->program_abbreviation, $details->year_level, $page);
			$page++;
			$content[] = $page_content;
			if($line_count < count($lines)){
				$continue = TRUE;
			} else {
				$continue = FALSE;
			}
		} while($continue);
		$dcontent = '';
		foreach ($content as $cpage){
			$dcontent .= $cpage . "\n";
		}
		return $this->init().$this->condensed().$dcontent.$this->no_condensed();
	}
		
}

?><div id="to_print" style="display:none"><?php 
$form19 = new form_19_print();
echo $form19->content($data);
?></div>