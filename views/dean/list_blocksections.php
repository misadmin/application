<h2 class="heading">Block Sections</h2>
<form class="form-horizontal">
  	<div class="control-group ">
  	<div class="well pull-left" style="border-radius:0;padding:5px 17px">
		Academic Terms&nbsp	:	<strong>&nbsp<?php print($academic_terms->term." ".$academic_terms->sy);?></strong><br> 	
		Academic Program&nbsp	:	<strong>&nbsp</label><?php print($academic_program->abbreviation); ?></strong><br>
		Year Level&nbsp	:	<strong>&nbsp<?php print($yearlevel);?></strong>
		</div>	
  	</div>
</form>


<div class="row-fluid">
	<div class="span6">
		<table id="list-block-section" class="table table-bordered table-hover">
			<thead>
			 <tr class="success">
			      <th>Section Name </th>
			      <th>No. of Students </th>
			    </tr>
			 </thead>
			    <?php
					if ($blocks) {
						foreach ($blocks AS $block) {
				?>
			  <tr>    
				      <td align="center" valign="top"><?php print($block->section_name); ?></td>
				      <td align="center">
					  <?php
					  	if ($block->size > 0) { 
					  		print("<a class=\"block\" href=\"{$block->id}\"> {$block->size} </a>"); 
						} else {
					  		print("{$block->size}"); 			
						}
						?></td>
					    
			 </tr>
		    <?php
					}
				}
			?>
		</table>
	</div>
</div>


<form id="srcform2" action="<?php echo site_url("dean/view_blocksection")?>" method="post">
<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.block').click(function(event){
		event.preventDefault(); 
		var block_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'id',
		    value: block_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>

<style>
	#list-block-section td,
	#list-block-section th{
	text-align: center;
	}
</style>