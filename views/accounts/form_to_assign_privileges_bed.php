<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
	width:100%;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>
  <div style="background:#FFF; ">
<?php
if(!$history_id){ ?>
	<div>STUDENT NOT ENROLLED THIS SCHOOL YEAR</div>  
<?php } else{ 
		if($assessed){
			if (!$tuition_fee) { ?>
				<div>FEES SCHEDULE NOT POSTED YET!</div>  
			<?php
			 }
			else {
		 		 if (isset($privilege_availed) AND ($privilege_availed->posted == 'Y')) {?>  
		 			<table width="100%" border="0" cellspacing="0" cellpadding="2">
		  			<tr>
		    			<td style="text-align:center; font-weight:bold;">STUDENT'S PRIVILEGE ALREADY POSTED!  APPLY FOR ADJUSTMENT ONLY! </td>
		  			</tr>
					</table> 
				<?php } else { ?>
		
						<form method="post" id="allow_privilege_form">
							<?php $this->common->hidden_input_nonce(FALSE); ?>
						  <input type="hidden" name="action" value= "form_to_assign_privileges"/>
						  <input type="hidden" name="tution_fee" value= "<?php print($tuition_fee);?>"/>
						  <input type="hidden" name="history_id" value= "<?php print($history_id);?>"/>
						   <input type="hidden" name="stud_id" value= "<?php print($student_idno);?>"/>
						  <?php
							if (isset($privilege_availed)) {
						  ?>
						  <input type="hidden" name="privilege_id" value="<?php print($privilege_availed->id); ?>" > 
							<?php
								}
							?>
						    <table align="left" cellpadding="0" cellspacing="0" class="head1" style="width:35%; margin-left:40px;">
						      <tr class="head">
						        <td colspan="3" class="head"> ASSIGN PRIVILEGE</td>
						      </tr>
						      <tr>
						        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside">
						           <tr>
						            <td width="125" valign="middle">School Year</td>
						            <td width="4" valign="middle">:</td>
						            <td width="87" valign="middle"> <?php print($sy); ?> </td>
						          </tr>	  
						          
						           <tr>
						            <td width="125" valign="middle">Tuition Fee</td>
						            <td width="4" valign="middle">:</td>
						            <td width="87" valign="middle"><b> <?php print(number_format($tuition_fee,2)); ?> </b></td>
						          </tr>	   
						           <tr>
						          	  <td &nbsp; &nbsp;</td>
						           </tr>
						           <tr>
						          	  <td &nbsp; &nbsp;</td>
						           </tr>	
						           <tr>
						          	  <td &nbsp; &nbsp;</td>
						           </tr>   
								  <tr>
						            <td valign="middle">Privilege</td>
						            <td valign="middle">:</td>
						            <td valign="middle">
									
									<select name="privilege_id" class="form">
						              <?php
											foreach($privilege_items AS $privilege_item) {
												print("<option value=".$privilege_item->id.">".$privilege_item->scholarship."</option>");	
											}
										?>
						            </select>
								</td>
						          </tr>
								   <tr>
						            <td valign="middle"> Tuition Percentage (%) </td>
						            <td valign="middle">:</td>
						            <td valign="middle">
									
									 <input name="TuitionD" type="text" id="TuitionD" size="3" maxlength="3" value = "100" style="width:25px;" >
									
									 </td>
						          </tr>
								  <?php
										if ($other_privileges) {
								  ?>
								   <tr>
								     <td colspan="3" valign="middle"><strong><u>Other Privileges</u></strong></td>
							        </tr>
									<?php
											foreach($other_privileges AS $other) {
												if($other->id != 5){
									?>
												   <tr>
													 <td valign="middle" style="padding-left:15px;"><?php print($other->fees_group); ?></td>
													 <td valign="middle">:</td>
													 <td valign="middle"><input type="checkbox" name="<?php print("other_priv[$other->id]"); ?>" value="Y"/></td>
													</tr>
												   <tr>
								   <?php
								   				}
								   			}
								   		}
								   ?>
								     <td colspan="3" valign="middle"><input type="submit" name="button2" id="allow_privilege" value="Continue!"  class="btn btn-success" /></td>
									 
							        </tr>
						        </table></td>
						      </tr>
						    </table>
						</form>
		<?php	}		
			}
		}else { ?>
		   <div style="color:red";> <i> Assessment NOT posted yet! </i></div>  
<?php } }?>
</div>


<script>
$('#allow_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to assign privilege to student?");

	if (confirmed){
		$('#allow_privilege_form').submit();
	}		
});
</script>

  <form id="srcform3" method="post">
  <input type="hidden" name="action" value= "edit_privileges"/>
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('#edit_priv').click(function(event){
		event.preventDefault(); 
		var privilege_availed_id = $(this).attr('href');
		var tuition_fee = $(this).attr('tuition_fee');
		var paying_units = $(this).attr('paying_units');
		var history_id = $(this).attr('history_id');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'tuition_fee',
		    value: tuition_fee,
		}).appendTo('#srcform3');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'privilege_availed_id',
		    value: privilege_availed_id,
		}).appendTo('#srcform3');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'paying_units',
		    value: paying_units,
		}).appendTo('#srcform3');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'history_id',
		    value: history_id,
		}).appendTo('#srcform3');
		$('#srcform3').submit();
	});
</script>
