
		<div id="selected_acad_term_id"> </div>
		<select id="day_code" name="day_code" style="width:auto;">
	  			<?php
					foreach($days AS $day)
						print("<option value=".$day->day_code.">".$day->day_code."</option>");	
				?>								
		</select>

		
<script>
		$('#day_code').bind('change', function(){

			var element    = $("option:selected", "#start_time");
			var start_time = element.val();	
			var element    = $("option:selected", "#num_hr");
			var num_hr     = element.val();
			var element    = $("option:selected", "#day_code");
			var day_code   = element.val();
			var term_id    = $('#selected_acad_term_id').val();

			$('#rooms_offering_modal').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:25px;' />"); 

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/course_schedules'); ?>",
				data: {	"start_time": start_time,
						"num_hr": num_hr,
						"day_code": day_code,
						"term_id": term_id,
						"action": "extract_vacant_rooms" },
				dataType: 'json',
				success: function(response) {													
																			
					$('#rooms_offering_modal').html(response.rooms); 
					
				}
			});

		});

</script>