
<div class="row-fluid" >
<h2 class="heading">Fees Schedule - Senior High School</h2>
	<div class="span12">
		<div class="form-horizontal">
			<form method="POST" id="change_academic_year_form" >
			<input type="hidden" name="action" value="review_fees" />
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<div style="float:left;">
						<select name="academic_year_id" id="academic_year_id">
							<?php
								foreach($school_years AS $school_year) {
							?>
								<option value="<?php print($school_year->id); ?>" >
									<?php print($school_year->school_year . " (". $school_year->status.")"); ?> 
								</option>	
							<?php 
								}
							?>					
						</select>
					</div>
					<div style="float:left; padding-left:10px; padding-top:5px;" id="show_academic_code_icon">
						
					</div>					
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Semester Term</div>
				<div class="controls">
					<div id="list_terms">
						<?php 
							foreach($terms AS $term) {
						?>
									<div style="float:left;">
										<input type="checkbox" value="<?php print($term->id); ?>" name="semester[]" checked>
									</div>
									<div style="float:left; padding-top:3px; padding-left:7px;">
										<?php print($term->term); ?>
									</div>
									<div style="clear:both;">
									</div>
						<?php 	
							}
						?>		
					</div>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Fees Category</div>
				<div class="controls">
					<div style="float:left;">
						<select name="fees_group_id" id="fees_groups">
							<?php						
								foreach($fees_groups AS $fees_group) {
									echo "<option value=". $fees_group->id . ">" . $fees_group->fees_group ."</option>";	
								}
							?>					
						</select>
					</div>
					<div style="float:left; padding-left:10px; padding-top:5px;" id="show_search_code_icon">
						
					</div>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Item</div>
				<div class="controls">
					<select name="fees_subgroups" id="fees_subgroups">						
						<?php 					
							if (isset($fees_subgroups)){
								foreach($fees_subgroups AS $fees_subgroup) {
									print("<option value=".$fees_subgroup->id." class=".$fees_subgroup->fees_groups_id.">" . $fees_subgroup->description ."</option>");
								}
							}
						?>									
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Track</div>
				<div class="controls">
					<div style="float:left;">
						<input type="checkbox" value="Y" name="all_program" id="selectall_tracks" checked />
					</div>
					<div style="float:left; padding-top:3px; padding-left:7px;">
						ALL Tracks
					</div>											
					<?php 					
						foreach($tracks AS $track) {
					?>
							<div style="clear:both; float:left;">
								<input type="checkbox" 
									value="<?php print($track->id); ?>" 
									class="track_id" 
									name="tracks[]" 
									checked 
									group_name="<?php print($track->group_name); ?>" />
							</div>
							<div style="float:left; padding-top:3px; padding-left:7px;">
								<?php print($track->group_name); ?>
							</div>
							<div style="clear:both; float:left; padding-top:3px; padding-left:40px; font-style:italic; color:#02074E; font-weight:bold;">
								<?php print($track->strand_description); ?>
							</div>
							
					<?php 
						}
					?>									
				</div>
			</div>
			<div class="control-group formSep" id="college">
				<div class="control-label">Grade Level</div>
				<div class="controls">
					<div style="float:left;">
						<input type="checkbox" value="Y" name="all_yr" id="selectall_yr" checked>
					</div>
					<div style="float:left; padding-top:3px; padding-left:7px;">
						ALL Grade Levels
					</div>
					<div style="clear:both; float:left;">
						<input type="checkbox" value="11" name="grade_level[]" class="selectedId_yr" checked>
					</div>
					<div style="float:left; padding-top:3px; padding-left:7px;">
						Grade 11
					</div>
					<div style="clear:both; float:left;">
						<input type="checkbox" value="12" name="grade_level[]" class="selectedId_yr" checked>
					</div>
					<div style="float:left; padding-top:3px; padding-left:7px;">
						Grade 12
					</div>										
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Rate</div>
				<div class="controls">
					<input type="text" name="rate" id="rate" autofocus />									
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<div style="float:left;">
						<input type="submit" value="Proceed to Review Form >>" class="btn btn-primary">		
					</div>
					<div style="float:left; padding-left:10px; padding-top:5px;" id="show_submit_form_icon">
					
					</div>
				</div>
			</div>
			</form>
			</div>				
	</div>
</div>


<script>
	$(document).ready(function () {
	    $('#selectall_tracks').click(function () {
	        $('.track_id').prop('checked', this.checked);
		});
	
	    $('.track_id').change(function () {
	        var check = ($('.track_id').filter(":checked").length == $('.track_id').length);
	        $('#selectall_tracks').prop("checked", check);
	    });
	});
</script>


<script>
	$(document).ready(function () {
	    $('#selectall_yr').click(function () {
	        $('.selectedId_yr').prop('checked', this.checked);
	    });
	
	    $('.selectedId_yr').change(function () {
	        var check = ($('.selectedId_yr').filter(":checked").length == $('.selectedId_yr').length);
	        $('#selectall_yr').prop("checked", check);
	    });
	});
</script>


<script>
	$(document).ready(function(){
		$('#change_academic_year_form').submit(function() {
		
			var amount  = $('#rate').val();
			var tracks_selected = $('input:checkbox:checked.track_id').map(function () {
				return this.value;
			}).get(); 
			
			var grades_selected = $('input:checkbox:checked.selectedId_yr').map(function () {
				return this.value;
			}).get(); 

			if (jQuery.isEmptyObject(tracks_selected)) {
				alert('At least 1 track must be selected!');
				return false;
			} 
			
			if (jQuery.isEmptyObject(grades_selected)) {
				alert('At least 1 Grade Level must be selected!');
				return false;
			} 

			if (isEmpty(amount)) {
				alert('Rate must not be empty!');
				$('#rate').focus();
				return false;
			} 
								
		});	

	});

	function isEmpty(v) {
		if (v === '' || v === null) {
			return true;
		} else {
			return false;
		}
	}
	
</script>


<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){

			var academic_year_id = $('#academic_year_id').val();
						
			$('#show_academic_code_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/fees_schedule_shs'); ?>",
				data: {"academic_year_id": academic_year_id,
						"action": "change_academic_year" },
				dataType: 'json',
				success: function(response) {													

					$('#list_terms').html(response.output);
					
					$('#show_academic_code_icon').html("");
				
				}
			});
		});
	});
</script>


<script>
	$(document).ready(function(){
		$('#fees_groups').bind('change', function(){

			var fees_groups_id = $('#fees_groups').val();
						
			$('#show_search_code_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/fees_schedule_shs'); ?>",
				data: {"fees_groups_id": fees_groups_id,
						"action": "search_fees_subgroups" },
				dataType: 'json',
				success: function(response) {													

					$('#fees_subgroups').html(response.output);
					
					$('#show_search_code_icon').html("");
				
				}
			});
			
		});
	});
</script>

