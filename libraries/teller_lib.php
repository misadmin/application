<?php

class teller_lib {
	
	/**
	 * Codeigniter Instance
	 * @var unknown_type
	 */
	protected $CI;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	
	public function daily_receipt_summary(){	
		$this->CI->load->model('teller/teller_model');		
		$date = $this->CI->input->post('date');
		$submitted = $this->CI->input->post('submitted');	
 		if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){		
			if ($date AND $submitted){
				$machine = $this->CI->session->userdata('role')=='teller' ? trim($this->CI->session->userdata('ip_address')):"";
				$data = array();
				$data['date'] = date('jS \of F Y',strtotime($date));
				$data['records'] = $this->CI->teller_model->new_daily_receipt_summary(date('Y-m-d',strtotime($date)),$machine);
				$data['payment_methods'] = $this->CI->config->item('payment_methods');
				$this->CI->load->library('table_lib');
				$this->CI->content_lib->enqueue_body_content('teller/receipt_summary_table',$data);
			}
			else {
				$this->CI->content_lib->enqueue_header_style('date_picker');
				$this->CI->content_lib->enqueue_footer_script('date_picker');
				//$this->content_lib->enqueue_footer_script('validate');			
				$this->CI->content_lib->enqueue_body_content('teller/receipt_summary_index');
			}
 		}else{
 			$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');				
 		}
 		$this->CI->content_lib->content();
	}

}
?>
