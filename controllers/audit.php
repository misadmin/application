<?php 
class Audit extends MY_Controller {
	public $error_message = '';
	public $post_errors;	
	public $assessed_count;
	public $students_exist;
	
	function __construct(){		
		$this->post_errors = array();
		$this->assessed_count = 0 ;
		$this->students_exist = TRUE;
		
		parent::__construct();
		$this->common->need_auth();

		$this->userinfo = $this->session->all_userdata();
		
		if ($this->session->userdata('role')=='audit'){
			
			$this->navbar_data['menu'] = 
				array(
					'Students'	=> 'student',
					'Employees'   => 'faculty',
					'Assessment Posting' => array(
									'Assessment - Basic Education' => 'batch_posting_basic_ed',
									'Assessment - College'	=> 'batch_posting_college',
									'Assessment - Senior High School'	=> 'assessment_posting_shs'),
					'Withdrawal Schedule' => array(
									'New Schedule' => 'add_new_withdrawal_sked',
									'View Schedule'	=> 'view_withdrawal_sked'),				
					'Reports' => array(
									'Enrollment Summary' => array(
											'Basic Ed' 	=> 'enrollment_summary_basic_ed',
											'College' 	=> 'enrollment_summary',
											'Senior High School'=> 'shs_enrollment_summary',
											),
									'Assessment Summary' => array(
											'Basic Ed.' => 'assessment_summary_basic_ed',
											'Basic Ed. - Group' => 'assessment_basic_ed_group',
											'College Summary' 	=> 'assessment_summary',
											'Group' 	=> 'assessment_report_group',
											'College' 	=> 'assessment_report_college',	
											'Senior High - By Strand' => 'shs_assessment_report_strand',
											),
									'Comparative Assessment Reports' => array(
											'Posted and Running Balance - By College'=>'posted_running_college',
											'Posted and Running Balance - College Student List'=>'posted_running_college_student',
											'Posted and Running Balance - By Program'=>'posted_running_program',
											'Posted and Running Balance - Program Student List'=>'posted_running_student',
											),
									'Unposted Assessments' => array(
											'College and Basic Ed.'=>'unposted_assessment',
											'Senior High School'=>'shs_unposted_assessments',
											),
									'Fees Schedule' => array(
											'By Academic Program' 			=> 'schedule_by_acad_program',
											'For Basic Education' 			=> 'schedule_for_basic_ed',										
											'For Senior High School' 		=> 'schedule_for_shs',										
											'Courses with Laboratory Fees' 	=> 'courses_with_lab_fees',
											),
									'Accounts Receivables' => array(
											'A/R Transactions' 		=> 'ar_balance_ver2',
											'Schedule of A/R'		=>	'ar_balances',
											'Schedule of Deposits'	=>	'ar_deposits',
											),
									'Withdrawals' 		=> 'list_withdrawals',
									'Withdrawals v2' 	=> 'total_withdrawals',
									'Privileges' 		=> array(
											'College'			=>	'privileges',
											'Basic Education'	=>	'privileges_basic_ed'
											),
									'Course Offerings' 	=> 'course_offerings',
									'Others' => array(
											'Students w/ Enrollments Deleted After Posting'=>'list_after_posting',
											'Added Subjects After Assessments'=>'added_subjects_after_assessments',
											'List of Mismatched ISIS/MIS Balances'=>'mismatches',
											'List of Students with Multiple IDs' => 'list_students_with_more_than_one_idno',									
											'Paying Units' => 'paying_units',
											),
									),					
			);
		}
		
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		
		$this->load->model('teller/assessment_model');
		$this->load->model('hnumis/Enrollments_model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('hnumis/Programs_model');
		$this->load->model('hnumis/College_model');
		$this->load->model('hnumis/AcademicYears_model');
		$this->load->model('Academic_terms_model');		
		$this->load->model('hnumis/Offerings_Model');
	}
	
	

	public function student(){

	if ($this->common->nonce_is_valid($this->input->post('nonce'))){
		$this->content_lib->set_title('Audit | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
	
		if ( ! empty($query)){
	
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
	
			$this->load->library('pagination_lib');
	
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
	
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
	
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
	
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('hnumis/Programs_Model');
			$this->load->helper('student_helper');
			$this->load->model('hnumis/Student_model');
			$this->load->model('hnumis/Prospectus_Model');
			$this->load->model('financials/Scholarship_Model');
			$this->load->model('accounts/Accounts_model');
			$this->load->model('teller/teller_model');
			$this->load->model('financials/Payments_Model');
			$this->load->model('financials/Tuition_Model');
			$this->load->model('hnumis/Student_Model');
			$this->load->model('basic_ed/Basic_Ed_Enrollments_Model');
			
							
			//$result = $this->teller_model->get_student($idnum);
			$result = $this->student_common_model->my_information($idnum);	
			$result2 = $this->teller_model->get_student($idnum);
			//$this->content_lib->enqueue_footer_script('printelement'); // print element plugin...
			$this->content_lib->set_title ('Audit | ' . $idnum);
			//$bank_codes = $this->input->post('bank_codes');
	
			$tab = 'assessment';
			switch ($action = $this->input->post('action')){
	
				case 'form_to_post_privileges':
										//print($this->input->post('remarks')); die();
										if ($this->common->nonce_is_valid($this->input->post('nonce'))){					
											$privilege_availed_id = $this->input->post('privilege_id');
											$data['privilege_availed_id'] = $privilege_availed_id;
											$remarks = sanitize_text_field($this->input->post('remarks'));
											$user_id = $this->userinfo['empno'];
											//$privileges = $this->Scholarship_Model->ListPrivilegeAvailedDetails($privilege_availed_id);
											//$total_discount = 0;
											//foreach($privileges as $privilege){
											//	$total_discount += $privilege->discount_amount;
											
										//	}
											$posted = $this->Scholarship_Model->PostPrivileges($privilege_availed_id, $remarks, $user_id);
											if($posted){
												$this->content_lib->set_message("Privilege Successfully Posted!", "alert-success"); 
											}else{
												$this->content_lib->set_message("Error found while posting a privilege!", "alert-error");
											}
										
										} else {
											$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
											$this->index();
										}					
										
										break;
												
					case 'edit_privileges':
								$tuition_discount_info = $this->tuitionDiscount($this->input->post('tuition_fee'),$this->input->post('paying_units'),
															$this->input->post('history_id')); 
								$data['term'] = $this->Academic_terms_model->getCurrentAcademicTerm();
								$data['privilege'] = $this->Scholarship_Model->getScholarship($this->input->post('privilege_availed_id'));
								$data['paying_units'] = $tuition_discount_info[1];
								$data['tuition_fee'] = $this->input->post('tuition_fee');
								$data['history_id'] = $this->input->post('history_id');
								$tuition_info = $this->Scholarship_Model->getTuitionPrivilege($this->input->post('privilege_availed_id'));
								$data['privilege_items'] = $this->Scholarship_Model->ListPrivileges();
								$data['tuition_percentage'] = $tuition_info->discount_percentage;
								$data['amount'] = $tuition_info->discount_amount;
								$data['other_privileges'] = $this->Scholarship_Model->ListPrivilegeAvailedDetails($this->input->post(
															'privilege_availed_id'));
								$this->content_lib->enqueue_body_content('accounts/form_to_edit_privileges', $data);
								$this->content_lib->content();	
								break;
								
				case 'delete_privilege_availed':
							if ($this->common->nonce_is_valid($this->input->post('nonce'))){
								$stat = $this->Scholarship_Model->DeletePrivilegeAvailed($this->input->post('privilege_id'));
								if($stat){
								  $this->content_lib->set_message('Privilege removed!', 'alert-success');
								}

							} else {
								$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
								$this->index();
							}	
								
							break;						

				case 'edit_privilege_availed':
							if ($this->common->nonce_is_valid($this->input->post('nonce'))){
								$privilege_availed_id = $this->input->post('privilege_availed_id');
								$percentage = sanitize_text_field($this->input->post('percentage'));
								(float)$percentage;
								$scholarship_id =  $this->input->post('privilege_id');
							    $tuition_fee_basic = $this->input->post('tuition_fee_basic');
 								$tuition_fee_others = $this->input->post('tuition_fee_other');
  								$paying_units_basic = $this->input->post('paying_units_enrolled');
 								$paying_units_others = $this->input->post('paying_units_others');
  								$year_level = $this->input->post('year_level');	
								$prev_tuition_discount = $this->input->post('prev_tuition_disc');	
								$tot_amount_disc = $this->input->post('tot_amount_discount');	
								$tuition_discount_info = $this->tuitionDiscount($tuition_fee_basic, $tuition_fee_others, 
														$paying_units_basic, $paying_units_others, $year_level); 
								$total_tuition = $tuition_discount_info[0];
								$tuition_disc_rate = $percentage/100;
								$new_tuition_disc =  $tuition_disc_rate * $total_tuition;
								$new_total_disc = $tot_amount_disc - $prev_tuition_discount + $new_tuition_disc;
								$stat = $this->Scholarship_Model->EditPrivilegeAvailed($privilege_availed_id, $new_total_disc, $new_tuition_disc, 
																						$tuition_disc_rate, $scholarship_id);
								if($stat){
								  $this->content_lib->set_message('Privilege updated!', 'alert-success');
								}

							} else {
								$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
								$this->index();
							}	
								
							break;						
										
					case 'privilege_enroll' :
								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
									$this->load->model('hnumis/Student_Model');
									$term = $this->Academic_terms_model->getCurrentAcademicTerm();
									$history = $this->Student_model->getStudentHistory($result->student_histories_id);
									$data['idno'] = $result->idno;
									$data['academic_terms_id'] = $term->id;
									$data['prospectus_id'] = $result->prospectus_id;
									$data['block_sections_id'] = $history->block_sections_id;
									$data['yr_level'] = $result->year_level;
									$student = $this->Student_Model->AssessYearLevel($data);

									//needed data for adding student history
									$data['year_level'] = $student['year_level'];
									$data['students_idno'] = $result->idno;
									$this->Student_model->AddHistory_ByPrivilege($data);
									$this->content_lib->set_message('Successfully enrolled student via privilege!', 'alert-success');

									/*if ($return = $this->teller_model->privilege_enroll_student($idnum))
										$this->content_lib->set_message('Successfully enrolled student via privilege.', 'alert-success'); else
										$this->content_lib->set_message('Error in enrolling student.', 'alert-error');
									*/	
										
								} else {
									$this->content_lib->set_message('You are not allowed to resubmit this form.', 'alert-error');
								}
								break;
								
						case 'change_term':
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;
							
						case 'generate_class_schedule':
								$this->load->model("hnumis/Reports_model");
								$this->load->model("hnumis/AcademicYears_Model");
								
								$student['idno']= $result->idno;
								$student['fname']= $result->fname;
								$student['lname']= $result->lname;
								$student['mname']= $result->mname;
								$student['yr_level']= $result->year_level;
								$student['abbreviation']= $result->abbreviation;
								$student['max_bracket_units']= $this->input->post('max_units');
								$student['student_histories_id']= $this->input->post('student_histories_id');
								
								$selected_term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
								
								$this->Reports_model->generate_student_class_schedule_pdf($this->input->post('academic_terms_id'), $student, $selected_term);
								
								return;
								
						/*case 'generate_student_certificate':
								print($this->input->post('student_histories_id'));
								die();
								break;*/
				
				default:
					break;
	
			}
	
		//	$this->load->model('teller/teller_model');
	
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$grade_level = array(11,12);

				if (in_array($result->year_level,$grade_level)) { //student is SHS
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname)  . ", "  . $result->fname . " ". $result->mname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> "Grade ".$result->year_level." ".$result->section_name,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address'	=>$result->full_city_address,
							'phone_number'	=>$result->phone_number,
							'section'	=>$result->section,
					);
				} else {
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname).', '. ucwords(strtolower($result->fname)) .' '.  substr($result->mname,0,1). (empty($result->mname)?'':'.'),
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> $result->year_level,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address' =>$result->full_city_address,
							'phone_number'		=>$result->phone_number,
							'section' 			=>$result->section,
							'bed_status' 		=>$result->bed_status,
					);
				}
				
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				//tab contents
				 $this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...
		
				//operations on privilege
				
				$this->load->library('tab_lib');
				
				//Operations to view the assessment and ledger
				$this->load->library('print_lib');
				
				$assessment_values = array();
				
				$ledger_data = array();
				$ledger_data = $this->teller_model->get_ledger_data($result2->payers_id);
				$this->load->model('academic_terms_model');
				
				//$this->tab_lib->enqueue_tab('Teller', 'teller/teller', $teller_values, 'teller', TRUE);
				$this->load->model('accounts/accounts_model');
				$this->load->model('hnumis/enrollments_model');
				$this->load->model('hnumis/student_model');
				$this->load->model('hnumis/AcademicYears_Model');
				$this->load->model('hnumis/Courses_Model');
				$this->load->model('accounts/fees_schedule_model');
				$this->load->library('shs/shs_student_lib');
				$this->load->model('hnumis/shs/shs_student_model');
				
				$period_now = $this->academic_terms_model->what_period();
				
				//assessment details
				$acontent = array(
						'idnumber'	=>$result->idno,
						'familyname'=>$result->lname,
						'firstname'	=>$result->fname,
						'middlename'=>$result->mname,
						'level'		=>$result->year_level,
						'course'	=>$result->abbreviation,
						'due_now' 	=> '',
						'succeeding_dues'	=> '7,756.87'
				);
				
				//assessment form
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum);
				$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
				//print_r($student_inclusive_terms); die();
				//assessment form
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
					$hist_id = $selected_history_id;
						
				} else {
					$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
					/*EDITED: 9/28/2016 by genes
					* change of academic term variable 
					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					*/
					
					if ($student_inclusive_terms) {
						$other_courses_payments = $this->teller_model->other_courses_payments($student_inclusive_terms[0]->id, $result->year_level);
						$laboratory_fees = $this->teller_model->all_laboratory_fees($student_inclusive_terms[0]->id); //					
					} else {
						$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
						$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //						
					}
					$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
					$hist_id = $result->student_histories_id;						
				}

				$assessment_date = $this->student_model->get_student_assessment_date($hist_id);
				
				if (!$assessment_date){
					$transaction_date2=array();
					$assessment_date2[0]= (object)array('transaction_date' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . " + 1 day") ));
					$assessment_date = $assessment_date2;
				}
				
				
				//$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$lab_courses = array();
				$courses = array();
				
				if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab' && empty($course->enrollments_history_id)) {
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
									);
						} 
						$courses[] = array(
								'id'=>$course->id,
								'name'=>$course->course_code,
								'units'=>$course->credit_units,
								'pay_units'=>$course->paying_units,
								're_enrollments_id'=>$course->re_enrollments_id,
								'withdrawn_on'=>$course->withdrawn_on,
								'assessment_id'=>$course->assessment_id,
								'post_status'=>$course->post_status,
								'enrollments_history_id'=>$course->enrollments_history_id,
								'transaction_date'=>$course->transaction_date,								
								);  
					}
				}
				
				$current_term = $this->Academic_terms_model->current_academic_term();
			
				if ($action=='schedule_current_term') {
					$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $this->input->post('academic_term')); 
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $this->input->post('academic_term'));
					//$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $this->input->post('academic_term'));
					$tab = 'schedule';
					$selected_term = $this->input->post('academic_term');
				
				} else {
					if ($academic_terms) {
						$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $academic_terms[0]->id);
						$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $academic_terms[0]->id); 
					} else {
						$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_academic_terms_obj->id);
						$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $current_academic_terms_obj->id); 
					}
					$tab = 'assessment';
					//$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $current_term->id);
					$selected_term = '';
				
				}
				
				$grade_level = array(11,12);
				
				if (is_college_student($idnum)) {
				
					if (in_array($result->year_level,$grade_level)) { //student is SHS
										
						$assess = $this->shs_student_lib->StudentAssessments($current_history); 
						
						$this->tab_lib->enqueue_tab('Assessment', 'shs/student/student_assessment',
							array('assess'=>$assess,
								'academic_terms'=>$academic_terms,
								'selected_term'=>$selected_term,
								), 
								'assessment', $tab=='assessment', FALSE);
						
					} else {
						//ADDED: 6/11/15 by Genes
						$affiliated_fees = $this->fees_schedule_model->ListStudentAffiliated_Fees($hist_id);
									
						$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment', 
								array(
										'assessment_date'=>$assessment_date[0]->transaction_date,		
										'other_courses_payments'=>$other_courses_payments,
										'assessment'=> $assessment,
										'courses'=>$courses,
										'affiliated_fees'=>$affiliated_fees,
										'lab_courses'=>$lab_courses,
										'ledger_data'=>$ledger_data,
										'student_inclusive_terms' =>$student_inclusive_terms,
										'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
										'student_details'=>$acontent
								), 
										'assessment', $tab=='assessment', FALSE);
					}
				} else {
					$this->load->model('basic_ed/assessment_basic_ed_model');
					$this->load->model('basic_ed/basic_ed_enrollments_model');
					
					$basic_ed = $this->basic_ed_enrollments_model->getLatestHistoryInfo($idnum);
					
					if ($basic_ed)
						$this->tab_lib->enqueue_tab ('Assessment', 'teller/assessment_basic_ed',
								array(
										'tuition_basic'=>$this->assessment_basic_ed_model->getTuitionRateBasic($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
										'basic_ed_fees'=>$this->assessment_basic_ed_model->ListBasic_Ed_Fees($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
										'post_status'=>$this->assessment_basic_ed_model->CheckIfPosted($basic_ed->id),
										'basic_ed_sections_id' => $basic_ed->basic_ed_sections_id,
										'stud_status'=>TRUE,
							), 
								'assessment', $tab=='assessment', FALSE);
						
				}
				
				//enqueue ledger
				$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$ledger_data,
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date
						),
								'ledger', FALSE);

				
				
				//*******************end of operations to view assessment and ledger
				
					$current_term = $this->Academic_terms_model->current_academic_term();
					$paying_units_basic = $this->Student_model->getPayingUnitsBasic($idnum, $current_term->id);
					$paying_units_others = $this->Student_model->getPayingUnitsOthers($idnum, $current_term->id);
					$history_id = $this->Student_model->getEnrollStatus($idnum, $current_term->id);
					$this->load->model('hnumis/Enrollments_model');
					$grades = $this->Enrollments_model->student_grades($idnum);
						
					
					//view payments
					$payments = $this->Payments_Model->ListStudentPayments($result2->payers_id);
					$this->tab_lib->enqueue_tab ('Payments', 'student/list_student_payments2', array('payments_info'=>$payments), 'payments',
							$tab=='payments',FALSE);
					
					//***** the following code allows viewing of grades
				if (is_college_student($idnum)){
					if (in_array($result->year_level,$grade_level)) { //student is SHS
						$this->load->model('hnumis/shs/shs_grades_model');
						$grades = $this->shs_grades_model->List_Student_Grades($idnum); 
						
						$this->tab_lib->enqueue_tab ('Grades', 'shs/student/student_grades', array('grades'=>$grades), 'grades', FALSE);					
					} else {
						$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', FALSE);
					}
					
					if ($this->input->post('academic_term')) {
						$selected_term = $this->input->post('academic_term');
						$terms=$this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_term'));
						$acad_term=$terms->term;
					} else {
						$selected_term = $academic_terms[0]->id;
						$acad_term = $current_academic_terms_obj->term;
					}
					
					$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $selected_term);
					if ($selected_history) {
						$student_units = $this->Student_model->getUnits($selected_history, $acad_term);
						$student_history_id=$selected_history->id;
					} else {
						$student_units['max_bracket_units']=0;
						$student_history_id=0;
					}
						
					if (in_array($result->year_level,$grade_level)) { //student is SHS
						
						$class_schedules = $this->shs_student_model->ListClassSchedules($current_history->id); 					
						
						$this->tab_lib->enqueue_tab ('Schedule', 'shs/student/student_class_schedules', 
								array('academic_terms'=>$academic_terms,
									'selected_term'=>$selected_term,
									'class_schedules'=>$class_schedules,
									'show_pulldown'=>TRUE,
									), 
								'schedule', $tab=='schedule');
					} else { 
						$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', 
								array('schedules'=>$schedule_courses,
									  'assessment_date'=>$assessment_date,
									  'academic_terms'=>$academic_terms,
									  'academic_terms_id'=>$selected_term,
									  'max_units'=>$student_units['max_bracket_units'],
									  'student_histories_id'=>$student_history_id), 
								'schedule', $tab=='schedule');
					}
			//	}	
					
					//student can be given privilege ONLY if he already has course/s enrolled
				
					if (($paying_units_basic OR $paying_units_others) AND $history_id) {
					   	//$MyPrivilege = $this->Student_model->Check_if_with_Privilege($history_id->id);
						$unpostedPrivileges = $this->Scholarship_Model->ListUnpostedPrivilegesWdoutCash($history_id->id);
					
					    $this->tab_lib->enqueue_tab('Privileges',
								'',
								array(),
								'privileges',
								FALSE);
					    
						//if ($unpostedPrivileges) {
						
							$this->tab_lib->enqueue_tab('Unposted Privileges', 'accounts/list_unposted_privileges_to_post',
									array(
											'privileges'=>$unpostedPrivileges,	),
									'unposted', FALSE, 'privileges');
						
						//}
						
						$unpostedCashDiscount = $this->Scholarship_Model->ListUnpostedCashDiscount($history_id->id);
						
						if($history_id){
							$academic_program_group_id = $this->student_model->getAcadGroup($history_id->id);
						}
						
						$tuition_fee_basic = $this->student_model->getMyTuitionFee_Basic($history_id->id, $academic_program_group_id->
								acad_program_groups_id,	$current_term->id );
						
						$tuition_fee_others = $this->student_model->getMyTuitionFee_Others($history_id->id, $current_term->id);
						if ($tuition_fee_basic OR $tuition_fee_others) {
							$paying_units_basic = $this->student_model->getPayingUnitsBasic($idnum, $current_term->id);
							$paying_units_others_info = $this->student_model->getPayingUnitsOthers($idnum, $current_term->id);
							if($tuition_fee_others){
								if($tuition_fee_others->tuition_fee == 0){
									$tuition_fee_others = 0;
								}else {
									$tuition_fee_others = $tuition_fee_others->tuition_fee;
								}
							}
						}
							if($paying_units_others->total_paying_units_enrolled == 0){
								$paying_units_others = 0;
							}else {
								$paying_units_others = $paying_units_others->total_paying_units_enrolled;
							}
						
							$withCWTS = $this->student_model->checkIfWithCWTS($history_id->id, $current_term->id);
							if($withCWTS){
								$tuition_fee = $tuition_fee_basic->tuition_fee + $tuition_fee_others - ($withCWTS->rate * $withCWTS->paying_units);
							}else{
								$tuition_fee = $tuition_fee_basic->tuition_fee + $tuition_fee_others;
							}

						//computation for subjects withdrawn
						$total_tuition_withdrawn = 0;
						$withWithdrawals = $this->student_model->checkIfWithWithdrawals($history_id->id);
						if($withWithdrawals){
							$total_tuition_withdrawn = 0;
							$student_info = $this->Student_Model->getStudentInfo($history_id->id);
							$tuition_basic_rate = $this->Student_Model->getTuitionRateBasic($student_info->acad_program_groups_id, $student_info->academic_terms_id, $student_info->year_level);
							$total_basic_tuition_withdrawn = 0;
							$total_others_tuition_withdrawn = 0;
							foreach($withWithdrawals as $withdrawn){
								$course_info = $this->Courses_Model->getCourseInfo($withdrawn->id);
								$tuition_others_rate = $this->Student_Model->getTuitionOthersRate($withdrawn->id, $student_info->academic_terms_id, $student_info->year_level);
								if($tuition_others_rate){
									$total_others_tuition_withdrawn += $tuition_others_rate->rate * $course_info->paying_units;
								}else{
									$total_basic_tuition_withdrawn += $tuition_basic_rate->rate * $course_info->paying_units;
								}
									$total_tuition_withdrawn = $total_others_tuition_withdrawn + $total_basic_tuition_withdrawn;
							}
						}
						$tuition_fee = 	$tuition_fee - 	$total_tuition_withdrawn;
							
						//computation for privileges availed	
						$list_tuition_privileges_availed = $this->Scholarship_Model->ListPrivilegesWdoutCash($history_id->id);
						
						$total_tuition_discount = 0;
						$tuition_for_cash_discount = 0;
						if($list_tuition_privileges_availed){
							foreach ($list_tuition_privileges_availed as $tuition_disc){
								$total_tuition_discount += $tuition_disc->discount_amount;
							}
							$tuition_for_cash_discount = $tuition_fee - $total_tuition_discount;
						}
						$list_posted = $this->Scholarship_Model->getPostedCash($history_id->id);						
						
						if($unpostedCashDiscount)						
							$this->tab_lib->enqueue_tab('Unposted Cash Discounts', 'accounts/display_unposted_cashdiscount_to_post',
									array('privileges'=>$unpostedCashDiscount, 'tuition_fee'=>$tuition_fee,
											'tuition_for_cash_discount'=>$tuition_for_cash_discount,
											'withCashPriv'=>FALSE,),
									'CashDiscounts', FALSE, 'privileges');
							
					} 
					
			
				//****************************** end of operations on privilege
				
			
				
				/*if ($action=='schedule_current_term') {
					$past_privilege_availed = $this->Scholarship_Model->getPastPrivileges($idnum, $this->input->post('academic_term'));
					$tab = 'list_student_privileges';
					$selected_term = $this->input->post('academic_term');
				} else {*/
					$past_privilege_availed = $this->Scholarship_Model->getPastPrivileges($idnum, $current_term->id);
				/*	$tab = 'assessment';
					$selected_term = '';
				}*/
				
				//$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum); 
				//$selected_term = $this->input->post('academic_term');
				
				
				//the following lists the past privileges of a student
				$past_privileges_availed = $this->Scholarship_Model->ListPastPrivileges($idnum);
				//print_r($past_privileges_availed); die();
				if($past_privileges_availed){
					foreach($past_privileges_availed as $past_privilege){
						$past_privileges_details = $this->Scholarship_Model->getPastPrivilegesDetails($past_privilege->id);
					}
				}
				 
					
				$this->tab_lib->enqueue_tab ("View Past Privileges", "accounts/list_past_privileges", array('past_privileges' => $past_privileges_availed,
				), 'list_past_privileges', FALSE, 'privileges');
				//end of list past privileges
				
			    if($past_privilege_availed){
					$past_priv_details = $this->Scholarship_Model->getPastPrivilegesDetails($past_privilege_availed->id);
				}else{
					$past_priv_details = NULL;
				}

				
				
				/*$this->tab_lib->enqueue_tab ("View Past Privileges", "audit/list_student_privileges", array('past_priv_details' => $past_priv_details,
												'academic_terms'=>$academic_terms,'selected_term'=>$selected_term, 'past_privilege_availed'=>
												$past_privilege_availed), 
											 'list_student_privileges', $tab=='list_student_privileges', FALSE);	
				*/
			//for basic ed
			} else {
					$current_sy = $this->Academic_terms_model->current_academic_year();
					$current_term = $this->Academic_terms_model->current_academic_term();
					$history_id = $this->Basic_Ed_Enrollments_Model->getHistoryInfo($idnum, $current_sy->academic_years_id);
					//print_r($history_id); die();
					
					if ($history_id){ //condition added by tatskie 
							$unpostedPrivileges = $this->Scholarship_Model->ListUnpostedPrivilegesWdoutCash($history_id->id, 1);
						    $this->tab_lib->enqueue_tab('Privileges',
									'',
									array(),
									'privileges',
									FALSE);
						    
							
							$this->tab_lib->enqueue_tab('Unposted Privileges', 'accounts/list_unposted_privileges_to_post',
									array('privileges'=>$unpostedPrivileges,	),
									'unposted', FALSE, 'privileges');
						
						
							$unpostedCashDiscount = $this->Scholarship_Model->ListUnpostedCashDiscount($history_id->id, 1);
							$tuition_fee = $this->Basic_Ed_Enrollments_Model->getMyTuitionFee($current_term->id, $history_id->levels_id,
									$history_id->yr_level);
							if($tuition_fee)
								$tuition_fee = $tuition_fee->rate;
							else
								$tuition_fee = 0;
							$list_tuition_privileges_availed = $this->Scholarship_Model->ListPrivilegesWdoutCash($history_id->id, 1);
							//print_r($list_tuition_privileges_availed); die();
							$total_tuition_disc = 0;
							$tuition_for_cash_discount = 0;
							if($list_tuition_privileges_availed){
								foreach($list_tuition_privileges_availed as $tuition_discount)
									$total_tuition_disc += $tuition_discount->discount_amount;
								$tuition_for_cash_discount = $tuition_fee - $total_tuition_disc;
							}
							
							//$list_posted = $this->Scholarship_Model->getPostedCash($history_id->id, 1);

							if ($unpostedCashDiscount)
								$this->tab_lib->enqueue_tab('Unposted Cash Discounts', 'accounts/display_unposted_cashdiscount_to_post',
										array('privileges'=>$unpostedCashDiscount, 'tuition_fee'=>$tuition_fee,
												'tuition_for_cash_discount'=>$tuition_for_cash_discount,
												'withCashPriv'=>FALSE,),
										'CashDiscounts', FALSE, 'privileges');
							
							$past_privileges_availed = $this->Scholarship_Model->ListPastPrivileges($idnum, 1);
							//print_r($past_privileges_availed); die();
							if($past_privileges_availed){
								foreach($past_privileges_availed as $past_privilege){
									$past_privileges_details = $this->Scholarship_Model->getPastPrivilegesDetails($past_privilege->id);
								}
							}
							$this->tab_lib->enqueue_tab ("View Past Privileges", "accounts/list_past_privileges_bed", array('past_privileges' => $past_privileges_availed,
							), 'list_past_privileges', FALSE, 'privileges');
					}
			}	
			$tab_content = $this->tab_lib->content();
			$this->content_lib->enqueue_body_content("", $tab_content);}
	   }
	
		$this->content_lib->content();
	} else {
		$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
		$this->index();
	  }	
	
	}
	
		
private function tuitionDiscount($tuitionB, $tuitionO, $paying_unitsB, $paying_unitsO, $year_level){
		$total_units = $paying_unitsB + $paying_unitsO;
		$rateB = $tuitionB/$paying_unitsB;
		if($paying_unitsO > 0){
			$rateO = $tuitionO/$paying_unitsO;
		}else{
		    $rateO = 0;
		}
		
		
		if($year_level != 4 OR $year_level != 5){
			 if($total_units < 21){
				   $tuitionBasic = $paying_unitsB * $rateB;
				   $tuitionOthers = $paying_unitsO * $rateO;
				   $paying_units = $total_units;
	 	    }else{
				  if($paying_unitsO){
					 $tuitionOthers = $paying_unitsO * $rateO;
					 $tuitionBasic = (21 - $paying_unitsO) * $rateB;
					 $paying_units = 21;
				  }else{
					 $tuitionOthers = 0;
					 $tuitionBasic = 21 * $rateB;
					 $paying_units = 21;
				  }				 
			}		
	    }else{
		   if($total_units < 24){
				 $tuitionBasic = $paying_unitsB * $rateB;
				 $tuitionOthers = $paying_unitsO * $rateO;
				 $paying_units = $total_units;
		   }else{
		   		 if($paying_unitsO){
					 $tuitionOthers = $paying_unitsO * $rateO;
					 $tuitionBasic = (24 - $paying_unitsO) * $rateB;
					 $paying_units = 24;
				  }else{
					 $tuitionOthers = 0;
					 $tuitionBasic = 24 * $rateB;
					 $paying_units = 24;
				  }				 
		  }  
		}
		$totalTuition = $tuitionBasic + $tuitionOthers;
		return array( $totalTuition, $paying_units) ;	 

	}
	
	public function enrollment_summary() {
	
		$this->load->model('hnumis/Reports_Model');
		
		$this->Reports_Model->enrollment_summary();

	}

	
	public function enrollment_summary_basic_ed() {
	
		$this->load->model('basic_ed/Reports_Model');
		
		$this->Reports_Model->enrollment_summary_basic_ed();

	}


	public function assessment_summary() {
	
		$this->load->model('hnumis/Reports_Model');
		//print("testing"); die();
		$this->Reports_Model->assessment_summary();

	}

	
	public function assessment_summary_basic_ed() {
	
		$this->load->model('basic_ed/Reports_Model');
		
		$this->Reports_Model->assessment_summary_basic_ed();

	}

	
	public function assessment_report_group() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->assessment_report_group();
	
	}
	
	
	public function assessment_report_college() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->assessment_report_college();
	
	}
	
	
	public function schedule_by_acad_program() {
		$this->load->library('accounts_lib');
	
		$this->accounts_lib->schedule_by_acad_program();
	
	}
	
	
	public function schedule_for_basic_ed() {
		$this->load->library('accounts_lib');
	
		$this->accounts_lib->schedule_for_basic_ed();
	
	}

	public function schedule_for_shs(){
		$this->load->library('shs/fees_schedule_lib');
	
		$this->fees_schedule_lib->view_update_schedule_for_shs();

	}


	/*
	 * @ADDED: 9/11/14
	 * @author: genes
	 */
	public function assessment_basic_ed_group() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->assessment_basic_ed_group();
	
	}
		
	public function batch_posting_basic_ed() {	
		
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){

			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('basic_ed/basic_ed_sections_model');
			$this->load->model('basic_ed/Assessment_Basic_Ed_Model');
		
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:	
					$this->select_basic_ed();				
					break;
				
				case 2:
					// print_r($this->input->post()); die();

					// print_r($this->input->post('levels')); die();
					switch ($this->input->post('levels')) {
						case '11':
							$yr_level = $this->input->post('yr_level_pre');
							break;
						case '1':
							$yr_level = $this->input->post('yr_level_k');
							break;
						case '3':
							$yr_level = $this->input->post('yr_level_gs');
							break;
						case '4':
							$yr_level = $this->input->post('yr_level_hs');
							break;
						case '12':
							$yr_level = $this->input->post('yr_level_shs');
							break;														
					}
					//die();
					$this->batch_post_assessment_basic_ed(
									$this->input->post('academic_years_id'), 
									$this->input->post('academic_terms_id'), 
									$yr_level, 
									$this->input->post('levels')
							); 
					
					$this->content_lib->set_message('BATCH POSTING FOR BASIC EDUCATION COMPLETED!', 'alert-success'); 
					$this->index();
					break;
					
			}
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
			$this->index();
		}

	}	

	public function batch_posting_college() {
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){
	
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/College_Model');
			$this->load->model('hnumis/Student_Model');
			$this->load->model('hnumis/Courses_Model');
			$this->content_lib->enqueue_footer_script('data_tables');
				
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					$this->select_college();
					break;
	
				case 2:
					$terms_sy = $this->AcademicYears_Model->getCurrentAcademicTerm();
					////log_message("INFO", print_r($terms_sy,true)); //Toyet 6.5.2018
					$isWithZeroPayingUnits = $this->Courses_Model->ListCoursesWithZeroPayingUnits($this->input->post('yr_level'), $this->input->post('colleges_id'), $terms_sy->id); 

					////log_message("INFO", print_r($isWithZeroPayingUnits,true));  // Toyet 5.31.2018

					if($isWithZeroPayingUnits){
						foreach($isWithZeroPayingUnits as $zcourse){
							$errors[] = $zcourse;
						} 
						$data['errors'] = isset($errors) ? $errors : array();
						$this->content_lib->enqueue_body_content('audit/post_zero_paying_units', $data);
						$this->content_lib->content(); die();
					} else {

						if ($this->input->post('yr_level') == "0") {
							for ($x=1; $x <= 5; $x++) {
								$this->batch_post_assessment_college($x,$this->input->post('colleges_id'));
							}
						} else {
							$this->batch_post_assessment_college($this->input->post('yr_level'),$this->input->post('colleges_id'));
						}
							
						//$this->content_lib->set_message($this->session->userdata('session_message'), 'alert-success');
						//$this->session->unset_userdata('session_message');
						//$this->index();
						//break;
		
						$data['errors'] = isset($this->post_errors['errors']) ? $this->post_errors['errors'] : array();
						$data['assessed_count'] = $this->assessed_count;
						$data['students_exist'] = $this->students_exist; 
						$this->content_lib->enqueue_body_content('audit/post_result', $data);
						$this->content_lib->content(); die();
					}

			}
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			$this->index();
		}
	
	}

	
	public function assessment_posting_shs() {
		$this->load->library('shs/assessment_lib');
	
		$this->assessment_lib->assessment_posting_shs();
			
	}

	
	public function shs_assessment_report_strand() {
		$this->load->library('shs/assessment_lib');
	
		$this->assessment_lib->shs_assessment_report_strand();
			
	}

	
	
	private function batch_post_assessment_college($yr_level, $colleges_id) {

		$assessment_history_inserted_id = null;  // added Toyet 11.15.2018

		$terms_sy = $this->AcademicYears_Model->getCurrentAcademicTerm();

		$student_histories = $this->Student_Model->ListStudentsEnrolled($terms_sy->id, $yr_level, $colleges_id, TRUE);
				
		if ($student_histories) {

			foreach($student_histories AS $student) {
				
				$tuition_basic  =  $this->Student_Model->getMyTuitionFee_Basic($student->student_histories_id, $student->acad_program_groups_id, $terms_sy->id);

				$tuition_rate  =  $this->Student_Model->getMyTuitionFee_Rate($student->student_histories_id, $student->acad_program_groups_id, $terms_sy->id);

				$tuition_basic_subjects  =  $this->Student_Model->getMyTuitionFee_BasicSubjects($student->student_histories_id, $student->acad_program_groups_id, $terms_sy->id);

				$tuition_others =  $this->Student_Model->getMyTuitionFee_Others($student->student_histories_id, $terms_sy->id);
				
				//ADDED: 6/26/15 by genes
				$aff_fees =  $this->Student_Model->getMyAffiliated_Fees($student->student_histories_id);
				
				$tuition_byprospectus =  $this->Student_Model->getMyTuitionFee_Prospectus($student->student_histories_id, $terms_sy->id);
				
				$other_fees     = $this->Student_Model->getMyOtherFees($yr_level, $terms_sy->id, $student->acad_program_groups_id);

				//added by Toyet 4.3.2019
				$other_fees_details = $this->Student_Model->getMyOtherFees_Detailed($yr_level, $terms_sy->id, $student->acad_program_groups_id);

				$lab_fees       = $this->Student_Model->getMyLabFees($terms_sy->academic_years_id, $student->student_histories_id);

				//added by Toyet 4.3.2019
				$lab_courses    = $this->Student_Model->getMyLabCourses($terms_sy->academic_years_id, $student->student_histories_id);

				$total          = $tuition_basic->tuition_fee  + $tuition_byprospectus->tuition_fee + $other_fees->rate + $lab_fees->rate ;
				if ($tuition_others) {
					$total = $total + $tuition_others->tuition_fee;
				}
				
				//ADDED: 6/26/15 by genes
				if ($aff_fees) {
					foreach($aff_fees AS $a_fee) {
						$total = $total + $a_fee->rate; 
					}
				}

				$data['student_histories_id'] = $student->student_histories_id;
				$data['assessed_amount'] = $total;
				$data['employees_empno'] = $this->userinfo['empno'];
				$data['year_level'] = $yr_level;
				$data['academic_programs_id'] = $student->academic_programs_id; 
				
				// temporarily commented out FOR TESTING   -Toyet 4.2.2019
				$assessments_inserted_id = $this->assessment_model->AddAssessment($data);
				//log_message("info", print_r($assessments_inserted_id,true)); 

				/*
				//save subjects included in the assessment here. -Toyet 12.1.2017
				*/
				$data_history['assessments_inserted_id'] = $assessments_inserted_id;
				$data_history['tuition_rate'] = $tuition_rate;
				$data_history['tuition_basic'] = $tuition_basic;
				$data_history['tuition_basic_subjects'] = $tuition_basic_subjects;
				$data_history['tuition_others'] = $tuition_others;
				$data_history['aff_fees'] = $aff_fees;
				$data_history['tuition_byprospectus'] = $tuition_byprospectus;
				$data_history['other_fees_details'] = $other_fees_details;
				$data_history['lab_courses'] = $lab_courses;


				//log_message("INFO",print_r($data,true)); // Toyet 11.15.2018
				//log_message("INFO",print_r($data_history,true)); // Toyet 11.15.2018

				$assessment_history_inserted_id = $this->assessment_model->AddAssessmentHistory($data,$data_history);
				
				if ( $assessments_inserted_id == FALSE  )
					$this->post_errors['errors'][] = array(
														'idno' 		=> $student->students_idno,
														'student' 	=> ucfirst($student->lname) .', '. ucfirst($student->fname) . ' ' . ucfirst($student->mname), 
														'program' 	=> $student->abbreviation,
														'yr_level' 	=> $student->year_level,
													);
				else 
					$this->assessed_count += 1;		
				
				/*print($yr_level."<p>");
				print($tuition_basic->tuition_fee."<p>");
				print($tuition_others->tuition_fee."<p>");
				print($tuition_byprospectus->tuition_fee."<p>");
				print($other_fees->rate."<p>");
				print($lab_fees->rate."<p>");
				print("TOTAL: ".$total."<p>");
				die();*/
				
				//$this->session->set_userdata('session_message','BATCH POSTING FOR COLLEGE COMPLETED!');
			}
			
		}else{
			$this->students_exist = FALSE;
		}
		return 0;		
	}



	
	public function add_new_withdrawal_sked() {	
		
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){
		
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/Terms_Model');
			$this->load->model('hnumis/Withdrawal_Model');
			$this->load->model('registrar_model');
				
			
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:	
				
				//We're going to use the datepicker plugin...
					$this->content_lib->enqueue_footer_script('date_picker');
					$this->content_lib->enqueue_header_style('date_picker');
					$this->content_lib->enqueue_header_style('qtip');
					$this->content_lib->enqueue_footer_script('qtip');
					
					$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms();
					$current_aca_year = $this->AcademicYears_Model->current_academic_year();
					$data['terms'] = $this->Terms_Model->ListTerms();
					$data['levels'] = $this->Terms_Model->ListLevels();
				
					$this->content_lib->enqueue_body_content('audit/form_to_create_withdraw_sked', $data);
					$this->content_lib->content();			
					break;
				
				case 2:
				
					$data['academic_terms_id'] = $this->input->post('academic_terms_id');
					$data['term_id'] = $this->input->post('terms_id');
					$data['level_id'] = $this->input->post('level_id');
					$data['start_d'] = $this->input->post('start_date');
					
					//if ($this->input->post('percentage') == '100') {
				//		$deyts = $this->registrar_model->get_end_date($this->input->post('academic_terms_id'));
				//		$data['end_d'] = $deyts->end_date;
					//	int_r($data);
					//	die();
				//	} else {
					$data['end_d'] = $this->input->post('end_date');
					//}
					$data['percentage'] = (sanitize_text_field($this->input->post('percentage')))/100;
					
				//	print_r($data);
				//	die();
					$status = $this->Withdrawal_Model->insertNewWithdrawalSked($data);
					if($status){
						$this->content_lib->set_message('Successful in inserting new withdrawal schedule!', 'alert-success'); 
						$this->index();
					}else{
						$this->content_lib->set_message('Error in inserting new withdrawal schedule!', 'alert-error'); 
						$this->index();
					}
					break;
										
			}
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
			$this->index();
		}

	}	


	public function view_withdrawal_sked() {	
		
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){
		
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/Terms_Model');
			$this->load->model('hnumis/Withdrawal_Model');
			
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:	
											
					$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms();
					$current_aca_year = $this->AcademicYears_Model->current_academic_year();
					$data['terms'] = $this->Terms_Model->ListTerms();
					$data['levels'] = $this->Terms_Model->ListLevels();
				
					
					$this->content_lib->enqueue_body_content('audit/form_to_edit_withdraw_sked', $data);
					$this->content_lib->content();			
					break;
				
				case 2:	
					
					$data['academic_terms_id'] = $this->input->post('academic_terms_id');
					$data['term_id'] = $this->input->post('terms_id');
					$data['level_id'] = $this->input->post('level_id');
					$withdrawal_sked_info = $this->Withdrawal_Model->listWithdrawalSkeds($data);
					
					$this->listWithdrawSkeds($withdrawal_sked_info, $data);
					
					break;
				
				case 3:
				
				//We're going to use the datepicker plugin...
					$this->content_lib->enqueue_footer_script('date_picker');
					$this->content_lib->enqueue_header_style('date_picker');
					$this->content_lib->enqueue_header_style('qtip');
					$this->content_lib->enqueue_footer_script('qtip');
					
					$data['academic_terms_id'] = $this->input->post('academic_terms_id');
					$data['term_id'] = $this->input->post('term_id');
					$data['level_id'] = $this->input->post('level_id');
							
					$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms();
					$current_aca_year = $this->AcademicYears_Model->current_academic_year();
					$data['terms'] = $this->Terms_Model->ListTerms();
					$data['levels'] = $this->Terms_Model->ListLevels();
					$data['withdraw_sked_info'] = $this->Withdrawal_Model->getWithdrawalSked($this->input->post('withdraw_sked_id'));
					$data['withdraw_sked_id'] = $this->input->post('withdraw_sked_id');
					
				
					$this->content_lib->enqueue_body_content('audit/form_to_edit_withdraw_sked2', $data);
					$this->content_lib->content();			
					break;
					
				case 4:
				
					$data['withdraw_sked_id'] = $this->input->post('withdrawal_sked_id');
					$data['start_d'] = $this->input->post('start_date');
					$data['end_d'] = $this->input->post('end_date');
				//	print($this->input->post('start_date')."br>".$this->input->post('end_date'));
				//	die;
					$data['percentage'] = (sanitize_text_field($this->input->post('percentage')))/100;
			
					$status = $this->Withdrawal_Model->updateWithdrawalSked($data);
					if($status){
						$this->content_lib->set_message('Successful in updating withdrawal schedule!', 'alert-success'); 
						
					}else{
						$this->content_lib->set_message('Error in updating withdrawal schedule!', 'alert-error'); 
					
					}
					$data['academic_terms_id'] = $this->input->post('academic_terms_id');
					$data['term_id'] = $this->input->post('term_id');
					$data['level_id'] = $this->input->post('level_id');
					
					$withdrawal_sked_info = $this->Withdrawal_Model->listWithdrawalSkeds($data);
					$this->listWithdrawSkeds($withdrawal_sked_info, $data);	
					break;
					
					
			case 5:
					
						$withdraw_sked_id = $this->input->post('withdraw_sked_id');
						
						$status = $this->Withdrawal_Model->deleteWithdrawSked($withdraw_sked_id);
						if($status){
							$this->content_lib->set_message('Successful in deleting withdrawal schedule!', 'alert-success');
					
						}else{
							$this->content_lib->set_message('Error in deleting withdrawal schedule!', 'alert-error');
								
						}
						$data['academic_terms_id'] = $this->input->post('academic_terms_id');
						$data['term_id'] = $this->input->post('term_id');
						$data['level_id'] = $this->input->post('level_id');
							
						$withdrawal_sked_info = $this->Withdrawal_Model->listWithdrawalSkeds($data);
						$this->listWithdrawSkeds($withdrawal_sked_info, $data);
						break;
					
			}
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
			$this->index();
		}

	}	



	private function select_basic_ed() {
		$data['terms_sy'] = $this->AcademicYears_Model->getCurrentAcademicTerm();
		//$data['levels'] = $this->basic_ed_sections_model->ListBasicEd();
		$data['levels'] = $this->basic_ed_sections_model->ListBasicLevels();
		
		$this->content_lib->enqueue_body_content('audit/form_to_select_basic_ed', $data);
		$this->content_lib->content();
	}

	/**
	 * 
	 * @param unknown $academic_years_id
	 * @param unknown $academic_terms_id
	 * @param unknown $yr_level
	 * @param unknown $levels_id
	 */
	private function batch_post_assessment_basic_ed($academic_years_id, $academic_terms_id, $yr_level, $levels_id) {
		// print_r($levels_id); die();

		$student_histories = $this->basic_ed_sections_model->ListStudentsEnrolled($academic_years_id, $yr_level, $levels_id);
		// print_r($student_histories); die();
		if ($student_histories) {
			foreach($student_histories AS $student) {
				$tuition = 	$this->basic_ed_sections_model->getMyTuitionFee($academic_years_id, $levels_id, $yr_level);
				//print_r($tuition); die();
				$other_fees = $this->basic_ed_sections_model->getMyOtherFees($academic_years_id, $levels_id, $yr_level);
				//print_r($other_fees); die();
				//$tuition = 	$this->basic_ed_sections_model->getMyTuitionFee(140, $levels_id, $yr_level);
				//$other_fees = $this->basic_ed_sections_model->getMyOtherFees(140, $levels_id, $yr_level);
				$total = $tuition->rate + $other_fees->rate ;
				
				$data['basic_ed_histories_id'] = $student->basic_ed_histories_id;
				$data['assessed_amount'] = $total;
				$data['employees_empno'] = $this->userinfo['empno'];
				$data['year_level'] = $yr_level;
				$data['levels_id'] = $levels_id;
				$data['academic_years_id'] = $academic_years_id;
				//print_r($data); die();
				$this->Assessment_Basic_Ed_Model->AddAssessment($data);
				//@FIXME: error trap here. don't expect this to always return to TRUE
			}
		}
		
		return ;
	}


	private function select_college() {
	
		$data['terms_sy'] = $this->AcademicYears_Model->getCurrentAcademicTerm();
		//$data['colleges'] = $this->College_Model->ListCollegesWithoutAssessment($data['terms_sy']->id);
		$data['colleges'] = $this->College_Model->ListColleges();

		$this->content_lib->enqueue_body_content('audit/form_to_select_year_level', $data);
		$this->content_lib->content();
	}


	private function listWithdrawSkeds($withdrawal_sked_info, $data2) {
		$data['academic_terms_id'] =$data2['academic_terms_id'];
		$data['term_id'] = $data2['term_id'];
		$data['level_id'] = $data2['level_id'];
		if($withdrawal_sked_info){
			$data['academic_year'] = $withdrawal_sked_info[0]->sy;
			$data['academic_term'] = $withdrawal_sked_info[0]->term;
			$data['terms'] = $withdrawal_sked_info[0]->terms_name;
			$data['level'] = $withdrawal_sked_info[0]->level;
		}	
		$data['withdraw_schedule'] = $withdrawal_sked_info;

		$this->content_lib->enqueue_body_content('audit/list_withdrawal_sked', $data);
		$this->content_lib->content();			
	}
	
	
	function courses_with_lab_fees() {
		$this->load->library('accounts_lib');
		
		$this->accounts_lib->courses_with_lab_fees();
		
	}
	public function ar_balance_ver2(){
		$this->load->library('ar_balance_ver2');
		$this->ar_balance_ver2->index();
	}
	
	public function ar_balance_action_ver2(){
		$this->load->library('ar_balance_ver2');
		$this->ar_balance_ver2->ar_balance_ver2();
	}
	
	public function ar_balance_action_summary_ver2(){
		$this->load->library('ar_balance_ver2');
		$this->ar_balance_ver2->ar_balance_summary_ver2();
	}
		
	
	//Added: october 30, 2013 by Amie
	public function unposted_assessment(){
		
		$this->load->model('teller/assessment_model');
		$this->load->model('academic_terms_model');
				
		$term = $this->academic_terms_model->getCurrentAcademicTermID();
		$data['list'] = $this->assessment_model->listunpostedassessment($term->current_academic_term_id);
		
		$this->content_lib->enqueue_body_content('audit/list_unposted_assessment', $data);
		
		$this->content_lib->content();
	}
	//Added: 12/4/2013
	
	public function privileges(){
	
		$this->load->library('accounts_lib');
		
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->accounts_lib->privileges();
	
	}

	/*
	 * ADDED: 8/20/15 by genes
	 */
	public function privileges_basic_ed(){
	
		$this->load->library('accounts_lib');
	
		$this->accounts_lib->privileges_basic_ed();
	
	}
	
	/*
	 * this is a temporary module
	* by Genes 12/11/13
	*/
	public function list_after_posting() {
		$this->load->library('accounts_lib');
	
		$this->accounts_lib->list_after_posting();
	
	}	
	
	public function faculty(){
		$this->load->library('faculty_lib');
		$this->faculty_lib->faculty(FALSE);
	}
	
   
	public function added_subjects_after_assessments(){
		$this->load->library('extras_lib');
		$this->extras_lib->added_subjects_after_assessments();
	}

	public function mismatches(){
		$this->load->library('extras_lib');
		$this->extras_lib->mismatches();
	}

	public function ar_balances(){
		$this->load->library('extras_lib');
		$this->extras_lib->ar_schedule();
	}
	
	public function ar_deposits(){
		$this->load->library('extras_lib');
		$this->extras_lib->ar_schedule(TRUE);
	}
	
	public function list_withdrawals() {
		//$this->load->library('extras_lib'); partially done but found out there is already same report for this accountant
		//$this->extras_lib->total_withdrawals();
		$this->load->library('../controllers/accountant/');
		$this->accountant->Withdrawals();
	}

	public function total_withdrawals() {
		$this->load->library('extras_lib'); //partially done but found out there is already same report for this accountant
		$this->extras_lib->total_withdrawals();
	}
	
	public function list_students_with_more_than_one_idno(){
		$this->load->library('extras_lib');
		$this->extras_lib->list_students_with_more_than_one_idno();
	}

	function paying_units(){
		$this->load->library('extras_lib');
		$this->extras_lib->paying_units();
	}

	
	function download_fees_schedule_to_csv() {
		$this->load->library('accounts_lib');
	
		$this->accounts_lib->download_fees_schedule_to_csv();
	}
	
	function download_fees_schedule_basic_ed_to_csv() {
		$this->load->library('accounts_lib');
	
		$this->accounts_lib->download_fees_schedule_basic_ed_to_csv();
	}
	
	//ADDED: 5/31/14 genes
	function download_enrollment_summary_to_csv() {
		$this->load->model('hnumis/Reports_Model');
	
		$this->Reports_Model->download_enrollment_summary_to_csv();
	
	}
	
	/*
	 * ADDED: 6/10/14
	 * @author: genes
	 * @description: displays students enrolled from enrollment summary modal
	 */
	function get_students() {
		$this->load->model('hnumis/Enrollments_Model');
	
		$data['students'] = $this->Enrollments_Model->masterlist($this->input->get('selected_term'), $this->input->get('prog_id'), $this->input->get('yr_level'), TRUE);
	
		$this->load->view('dean/list_of_students_from_modal', $data);
	
	}
	
	/**
	 * added by Tatskie Aug 11, 2014
	 */
	function course_offerings(){
		$this->load->library('../controllers/dean');
		$this->dean->all_offerings();		
	}

	/*
	 * @ADDED: 11/19/14
	 * @author: genes
	 */
	function list_allocations() {
		$this->load->model('hnumis/Offerings_Model');

		$data['allocations'] = $this->Offerings_Model->ListAllocation($this->input->get('offer_id'));
		$data['block_names'] = $this->Offerings_Model->ListBlockOfferings($this->input->get('offer_id'));
		
		$this->load->view('dean/list_of_allocations_from_modal', $data);
	}

	/*
	* ADDED: 4/11/15
	* @author: genes
	* @description: displays students enrolled from enrollment summary modal
	*/
	function get_basic_ed_students() {
		$this->load->model('basic_ed/basic_ed_enrollments_model');
		
		$data['students'] = $this->basic_ed_enrollments_model->list_enrollees($this->input->get('prog_id'), $this->input->get('yr_level'), 0, $this->input->get('selected_year'));
		//print_r($data); die();
		$this->load->view('rsclerk/list_of_students_from_modal', $data);
		
	}


	public function shs_enrollment_summary() {
		$this->load->library('shs/reports_lib');
	
		$this->reports_lib->enrollment_summary();
			
	}

	
	public function shs_unposted_assessments() {
		$this->load->library('shs/assessment_lib');
	
		$this->assessment_lib->shs_unposted_assessments();
			
	}

	public function process_student_action($idnum) {
		
		$this->load->library('shs/shs_student_lib');
	
		$this->shs_student_lib->process_student_action($idnum);
		
	}


	public function posted_running_college() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->posted_running('college');
	
	}

	public function posted_running_college_student() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->posted_running('col_student');
	
	}
	
	public function posted_running_program() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->posted_running('program');
	
	}

	public function posted_running_student() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->posted_running('student');
	
	}
	
}
