<div style="background-color:#FFF; padding-top:20px; margin-top:20px;">
  <table class="table table-striped table-hover table-condensed" style="width:50%; border:solid; border-width:1px; border-color:#FF80FF;" align="center">
    <thead>
<tr>
<th style="text-align:center;">No.</th>
<th style="text-align:center;">Year Level</th>
<th style="text-align:center;">Term</th>
<th style="text-align:center;">Course Code</th>
<th style="text-align:center;">Descriptive Title</th>
<th style="text-align:center;">Units</th></tr>
</thead>

<?php 
	$n = 1;
	if($content): 
?>
<?php foreach($content as $prospectus): ?>
<tr>
<td><?php print($n); ?></td>
<td><?php print($prospectus->year_level); ?></td>
<td><?php print($prospectus->terms); ?></td>
<td><?php print($prospectus->course_code); ?></td>
<td><?php print($prospectus->descriptive_title); ?></td>
<td><?php print($prospectus->credit_units); ?></td>
</tr>
<?php 
	$n++;
	endforeach; ?>
<?php endif;  ?>
</table>
</div>