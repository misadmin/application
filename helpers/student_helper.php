<?php 

function next_student_number ($student_id){
	
	$last_two = substr($student_id, -2, 2);
	$last_digit = substr($student_id, -1, 1);
	
	if ((int)$last_two >= 95){
		$plus = 5;
	} else {
		if((int)$last_two >=90 && (int)$last_two < 95){
			$plus = 15;
		} else {
			if ((int)$last_digit >= 0 && (int)$last_digit <=2) {
				$plus = 17;
			} else{
				$plus = 7;
			}
		}
	}
	return $student_id + $plus;
}

function is_college_student ($student_id){
	$ci =& get_instance();
	$ci->load->model('hnumis/student_model');
	
	if ($ci->student_model->is_college_student($student_id))
		return TRUE; else
		return FALSE;
}