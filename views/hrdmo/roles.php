<div class="row-fluid span12">
	<form method="post" class="form-horizontal" id="roles_form">
		<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="update_roles">
		<div class="span6 formSep">
			<span class="span6">All Available Roles:</span>
			<span class="span6">&nbsp;&nbsp;Roles Given to User:</span>
			<select id="user-roles" multiple class="multiselect" name="roles[]">
<?php asort($roles); ?>
<?php foreach($roles as $role):  ?>
				<option value="<?php echo $role; ?>" <?php if(in_array($role, $my_roles)) echo 'selected="selected"'; ?>><?php echo ucfirst($role); ?></option>
<?php endforeach; ?>
			</select>
			<div class="control-group">
				<button id="update_roles" class="btn btn-primary" type="submit">Update Roles</button>
			</div>
		</div>
	</form>
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="myModalLabel">You need to assign a college for some roles</h3>
	</div>
	<div class="modal-body">
		<div class="form-horizontal">
			<fieldset>
			<div class="control-group formSep">
				<label for="college_id" class="control-label">Assign this college:</label>
				<div class="controls">
					<select name="college_id" id="college_id">
<?php foreach($colleges as $val): ?>
						<option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
<?php endforeach; ?>
					</select>
				</div>
			</div>
			</fieldset>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button id="final_submit" class="btn btn-primary" accesskey="P">Set College and Submit</button>
	</div>
</div>
<script>
var my_roles = <?php echo json_encode($my_roles); ?>;
var roles_needing_department = ["dean", "faculty"];
var found = false;

$(document).ready(function(){
	if($('#user-roles').length) {
	    $('#user-roles').multiSelect();
	}

	$('#update_roles').bind('click', function(event){
		event.preventDefault();

		$('#user-roles :selected').each(function(){
			if ($.inArray($(this).val(), roles_needing_department) != -1){
				found = true;
			}
		});

		if (found){
			$('#myModal').modal('show');
			return false;//the form will not be submitted.... but we open a modal..
		} else {
			console.log('not found');
			$('#roles_form').submit();
		}
	});

	$('#final_submit').bind('click', function(){
		$('<input>').attr({
		    type: 'hidden',
		    name: 'college_id',
		    value: $('#college_id :selected').val()
		}).appendTo('#roles_form');
		$('#roles_form').submit();
	});
	
});
</script>