
			<div style="padding:3px; overflow:auto;">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:15px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:15%;">Grade-Section</td>
								<td class="shs_header" style="width:30%;">Strand</td>
								<td class="shs_header" style="width:20%;">Class Adviser</td>
								<td class="shs_header" style="width:5%;">Room</td>
								<td class="shs_header" style="width:5%;">Download<br>Classlist</td>
								<td class="shs_header" style="width:5%;">Report<br>Card</td>
								<td class="shs_header" style="width:5%;">Enrolled<br>Students</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($sections) {
									foreach($sections AS $section) {
										if ($section->strand_status == 'O') {
											$bg = "#FFFFFF";
										} else {
											$bg = "#FF9C9C";
										}
							?>
							<tr>
									<td style="background-color:<?php print($bg); ?>; "><?php print('Grade '.$section->yr_level.'-'.$section->section_name); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($section->description); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($section->class_adviser); ?></td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;"><?php print($section->room_no); ?></td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;">
										<a href="#" 
											grade_section="<?php print('Grade '.$section->yr_level.'-'.$section->section_name); ?>"
											strand="<?php print($section->description); ?>"
											class_adviser="<?php print($section->class_adviser); ?>"
											room_no="<?php print($section->room_no); ?>"
											section_id="<?php print($section->id); ?>"
											abbreviation="<?php print($section->abbreviation); ?>"
											class="download_classlist_pdf">
											<img src="<?php print(base_url('assets/images/pdf_icon.png')); ?>" style="height:18px;" /> 
										</a>
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;">
										<a href="#" 
											grade_section="<?php print('Grade '.$section->yr_level.'-'.$section->section_name); ?>"
											strand="<?php print($section->description); ?>"
											class_adviser="<?php print($section->class_adviser); ?>"
											room_no="<?php print($section->room_no); ?>"
											section_id="<?php print($section->id); ?>"
											abbreviation="<?php print($section->abbreviation); ?>"
											class="download_report_card_pdf">
											<img src="<?php print(base_url('assets/images/pdf_icon.png')); ?>" style="height:18px;" /> 
										</a>
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;"><?php print($section->num_students); ?></td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>

			
			
<form id="download_classlist_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_classlist_pdf" />
</form>

<script>
	$('.download_classlist_pdf').click(function(event){
		event.preventDefault(); 
		var section_id    = $(this).attr('section_id');
		var grade_section = $(this).attr('grade_section');
		var strand        = $(this).attr('strand');
		var class_adviser = $(this).attr('class_adviser');
		var room_no       = $(this).attr('room_no');
		var abbreviation  = $(this).attr('abbreviation');

		var element2  = $("option:selected", "#select2");
		var term_sy   = element2.attr('term_text');
	
		$('<input>').attr({
			type: 'hidden',
			name: 'section_id',
			value: section_id,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'grade_section',
			value: grade_section,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'strand',
			value: strand,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'class_adviser',
			value: class_adviser,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'room_no',
			value: room_no,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'abbreviation',
			value: abbreviation,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'term_sy',
			value: term_sy,
		}).appendTo('#download_classlist_form');

		$('#download_classlist_form').submit();
		
	});
</script>
			
			
<form id="download_report_card_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_report_card_pdf" />
</form>

<script>
	$('.download_report_card_pdf').click(function(event){
		event.preventDefault(); 

		var section_id    = $(this).attr('section_id');
		
		var filename  = $(this).attr('abbreviation')+" "+$(this).attr('grade_section');

		var element2  = $("option:selected", "#select2");
		var term_id   = element2.val();
		var term_text = element2.attr('term_text');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'section_id',
			value: section_id,
		}).appendTo('#download_report_card_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'term_id',
			value: term_id,
		}).appendTo('#download_report_card_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'term_text',
			value: term_text,
		}).appendTo('#download_report_card_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'filename',
			value: filename,
		}).appendTo('#download_report_card_form');

		$('#download_report_card_form').submit();
		
	});
</script>



