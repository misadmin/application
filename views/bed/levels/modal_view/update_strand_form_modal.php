	
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Level</div>
			<div class="col2a">
				<select class="form-control" style="width:auto; height:auto;" id="track_id_update_modal" disabled >
					<?php 
						if ($levels) {
							foreach ($levels AS $level) {
					?>		
								<option value="<?php print($level->id); ?>" level_abbreviation="<?php print($level->abbreviation); ?>"><?php print($level->group_name); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Year Level</div>
			<div class="col2a">
				<select name="" id="year_level_update" class="form-control" style="width:auto;" >
					<option value="1">Year 1</option>
					<option value="2">Year 2</option>
					<option value="3">Year 3</option>				
					<option value="4">Year 4</option>
					<option value="5">Year 5</option>
					<option value="6">Year 6</option>				
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Description</div>
			<div class="col2a">
				<input type="text" class="form-control" id="description_update_modal" style="width:350px;" />
			</div>
		</div>
	</div>
