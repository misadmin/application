
<div style="margin-top:20px;">

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding: 2px; margin-bottom:20px;">
	
	<table style="width:100%; height:auto;">
	<tr>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:13%;">
	<a href="<?php print(base_url('downloads//Enrollment by Program and Course '.$show_current_term.'.xls')); ?>" class="btn btn-link">
	<i class="icon-download-alt"></i> Download as Excel</a>
	</td>
	<td style="text-align:right; vertical-align:top;">
	<form id="change_sy" method="POST">
			<select name="academic_terms_id" id="academic_terms_id">
					<?php 
						foreach($terms AS $my_term) {
					?>
							<option value="<?php print($my_term->id); ?>" <?php if ($my_term->id == $selected_term) { print("selected"); } ?>>
								<?php print($my_term->term.' '.$my_term->sy); ?> </option>
					<?php 	
						}
					?>
			</select>
	
	</form>						
	</td>
	</tr>
	</table>
</div>

<h3>No. of Students Enrolled by Program and Course</h3>
<div style="width:25%; padding-left:30px;">

<table class="table table-condensed table-hover table-bordered">
	<?php 
		if ($enrollees) {
	?>
		<tr>
			<td colspan="2" style="font-weight:bold;">Total No. of Courses Offered: </td>
			<td style="font-weight:bold; text-align:right;"><?php print($num_courses); ?></td>
		</tr>
	
	<?php 
			$cnt=1;
			//print_r($enrollees);die();
			foreach($enrollees AS $k=>$v) {
	?>
		<tr>
			<td colspan="3" style="font-weight:bold;">
				<?php 
					echo $cnt.'. '. $k . ' - ' . (count($v)>0 ?  $v[0]->teacher : '') ;
					$cnt++;
				?>
			</td>
		</tr>
		<?php 
			if ($v) {
				foreach($v AS $prog) {
		?>
				<tr>
					<td style="padding-left:80px; width:90%;"><?php print($prog->abbreviation);?></td>
					<td style="width:2%;">-</td>
					<td style="width:8%; text-align:right;"><?php print($prog->stud);?></td>
				</tr>
		<?php 
				}
			}	
		
		?>
		
	<?php 
			}
		}
	?>


</table>
<?php 
	//print_r($enrollees); die();
	/*foreach($enrollees AS $k=>$v) {
		print($k.'<br>');
		if ($v) {
			foreach($v AS $prog) {
				print($prog->abbreviation.'-'.$prog->stud.'<br>');
			}
		}
		
	}*/
?>





</div>

</div>

  <form id="download_enroll_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="4" />
</form>
<script>
	$(document).ready(function(){
  
		$('.download_enroll').click(function(event){
			event.preventDefault(); 
			var academic_terms_id = $(this).attr('academic_terms_id');
	
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_terms_id',
				value: academic_terms_id,
			}).appendTo('#download_enroll_summary_form');
			$('#download_enroll_summary_form').submit();
			
		});

		
	});
</script>


<script type="text/javascript">
	function showStuff(id) {
		document.getElementById(id).style.display = 'table-row-group';
	}
	function hideStuff(id) {
		document.getElementById(id).style.display = 'none';
	}
</script>

<script>
	$(document).ready(function(){
		$('#academic_terms_id').bind('change', function(){
			show_loading_msg();
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'display_change_sy',
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>

