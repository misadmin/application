<style>
	table.isis_t 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
	tr.sub_total {font-weight: bold;}
</style>
<h3>List of ISIS subjects not migrated to MIS:</h3>
<hr>

<div id="isis" class="span7">


<form id="frm1" method="post">  

<table class="table table-condensed table-striped isis_t">
	<thead>
	<h4><?php echo count((array)$rows) < 1 ? '' : '['. $rows[0]->stud_id . '] ' . $rows[0]->student; ?></h4>
	<tr>
		<th></th>
		<th>SY</th>
		<th>Term</th>
		<th>Catalog No.</th>
		<th>Section</th>
		<th>Prelim</th>
		<th>Midterm</th>
		<th>Final</th>		
		<th><input class="checkboxes" type="checkbox" id="ckbCheckAll" /></th>
	</tr>
	</thead>

<?php 
	//print_r($rows);die();
	$numrow = 0;
	if (count((array)$rows) < 1){ 
		echo '<tr><td class="center" colspan="9"><b>No Record Found!</b></td></tr>';
	}else{
		foreach($rows as $row){
			$numrow++;
			 		
?>
			<tr>
			<td class="center"><?php echo $numrow .'.'; ?>
			<td class="center"><?php echo $row->sy; ?></td>
			<td class="center"><?php echo $row->term; ?></td>
			<td><?php echo $row->catalog_no; ?></td>
			<td class="center"><?= $row->section ?></td>
			<td class="center"><?= $row->grade_1 ?></td>
			<td class="center"><?= $row->grade_3 ?></td>
			<td class="center"><?= $row->final_gr ?></td>
			<td class="center"><input class="checkboxes" type="checkbox" name="check[]" value="<?= $numrow-1 ?>" /></td>		
			</tr>
		
<?php
		 
		} //foreach
	} //count

?>	

</table>
<?php 
	if (count((array)$rows) > 0){ 
?>
	<input type="submit" name="action" value="migrate" class="btn btn-primary">
<?php 
	} 
?>
</form>
</div>

<script>
    $(document).ready(function () {
        $("#ckbCheckAll").click(function () {
            $(".checkboxes").attr('checked', this.checked);
        });
    });

    
</script>