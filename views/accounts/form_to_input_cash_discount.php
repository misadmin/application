<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
	width:100%;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>
  <div style="background:#FFF; ">
<?php
	if (!($assessment)) {
?>
<div style="color:red";> <i> Assessment NOT posted yet! </i> </div>

<?php
 //elseif (isset($privilege_availed) AND ($privilege_availed->posted == 'Y')) {
?>  
<!-- 	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td style="text-align:center; font-weight:bold;">STUDENT'S PRIVILEGE ALREADY POSTED!  APPLY FOR ADJUSTMENT ONLY! </td>
  </tr>
</table> -->
<?php
	} else {
		if($withCashPriv){
		 ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td style="text-align:center; font-weight:bold;">With Unposted/Posted Cash Discount Already </td>
				</tr>
			</table> 
<?php	  
		}else {
?>

		<form method="post" id="cash_discount">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
		  <input type="hidden" name="action" value= "form_to_add_cash_discount"/>
		   <input type="hidden" name="tuition_fee" value="<?php print($tuition_fee); ?>" > 
		   <input type="hidden" name="tuition_for_cash_discount" value="<?php print($tuition_for_cash_discount); ?>" > 
		  <input type="hidden" name="student_histories_id" value="<?php print($student_histories_id); ?>" > 
		  <input type="hidden" name="academic_programs_id" value="<?php print($academic_programs_id); ?>" > 
		  <input type="hidden" name="year_level" value="<?php print($year_level); ?>" > 
		 
		    <table align="center" cellpadding="0" cellspacing="0" class="head1" style="width:50%;">
		      <tr class="head">
		        <td colspan="3" class="head"> CASH DISCOUNT</td>
		      </tr>
		      <tr>
		        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside">
		           <tr>
		            <td width="40%" valign="middle">Term</td>
		            <td width="10%" valign="middle">:</td>
		            <td width="50%" valign="middle"> <?php print($term->term." ".$term->sy); ?> </td>
		          </tr>	   
				  <tr>
		            <td width="40%" valign="middle">Privilege</td>
		            <td width="10%"valign="middle">:</td>
		          	<td width="50%" valign="middle">Cash Discount</td>
			      </tr>
				  <tr>
		            <td width="40%" valign="middle"> Tuition Fee </td>
		            <td  width="10%" valign="middle">:</td>
		          	<td  width="50%" valign="middle"> <?php print number_format($tuition_fee,2)?> </td>
				  </tr>
				  <?php
					if ($privileges) {
				  ?>
				    <tr>
				      <td colspan="3" valign="middle"><strong><u>Tuition Fee Discounts Availed</u></strong></td>
			       </tr>
					<?php	//$tuition_discount = 0;
							foreach($privileges AS $other){ ?>
							  <tr>
								 <td valign="middle" style="padding-left:15px;"><?php print($other->scholarship); ?></td>
								 <td valign="middle">:</td>
								 <td valign="middle" style="padding-left:15px;"><?php print number_format($other->discount_amount,2); ?></td>
							</tr>
							
				   <?php 	//$tuition_discount += $other->discount_amount;
					} ?>
				   <tr>
					   <td valign="middle"> Total Tuition Fee Discounts Availed</td>
					   <td valign="middle">:</td>
					   <td valign="middle" style="padding-left:15px;"><?php print number_format($total_tuition_discount,2); ?></td>
				   </tr>
				  <?php// $tuition_for_cash_discount = $tuition_fee - $total_tuition_discount; ?>
				    <tr>
					   <td valign="middle"> Tuition Fee for Cash Discount</td>
					   <td valign="middle">:</td>
					   <td valign="middle" style="padding-left:15px;"><strong><?php print number_format($tuition_for_cash_discount,2); ?></strong></td>
				   </tr>
				<?php } ?>
				 <tr>
		            <td valign="middle"> Discount Percentage (%) </td>
		            <td valign="middle">:</td>
		            <td valign="middle">
					
					 <input name="CashDisc" type="text" id="CashDisc" size="3" maxlength="3" value = "0" style="width:25px;" >
					
					 </td>
		          </tr>
				
				  <tr>
				     <td colspan="3" valign="middle"><input type="submit" name="button2" id="allow_cash_discount" value="Continue!"  class="btn btn-success" /></td>
				  </tr>
		        </table></td>
		      </tr>
		    </table>
		</form>
<?php			
		}
	}	
?>
 </div>


<script>
$('#allow_cash_discount').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to add cash discount?");

	if (confirmed){
		$('#cash_discount').submit();
	}		
});
</script>

 
