<!--
<body onLoad="document.create.abbreviations.focus()">

<form action="<?php echo site_url('dean/create_program');?>" method="post" name="create">
<?php $this->common->hidden_input_nonce(FALSE); ?>
  <?php //echo validation_errors(); ?>
   
  <table style="width:50%; margin-left:20px; border:solid; border-color:#999; border-width:1px; vertical-align:middle;">
<tr>
  <td colspan="3" style="background:#093; padding:8px; color:#CCC; font-weight:bold; font-size:16px;">Create Program</td>
  </tr>
  <?php
  	if ($this->input->post('departments_id')) {
  ?>
<tr>
  <td style="padding-top:10px; padding-left:10px; vertical-align:top; ">Department</td>
  <td style="padding-top:10px; vertical-align:top; ">:</td>
  <td style="padding-top:10px; vertical-align:top; "><?php print($department_name->department); ?></td>
</tr>
<?php 
	}
?>
<tr>
        	<td style="padding-top:10px; padding-left:10px; vertical-align:top; ">Abbreviation</td>
        	<td style="padding-top:10px; vertical-align:top; ">:</td>
            <td style="padding-top:10px; vertical-align:top; "><input name="abbreviations" type="text" class="form1" id="abbreviations" style="width:50px;"/></td>
    </tr>
    	<tr>
    	  <td style="vertical-align:top; padding-left:10px; ">Description</td>
    	  <td style="vertical-align:top; ">:</td>
    	  <td style="vertical-align:top; "><input type="text" name="description" id="description" class="form1" style = "width: 500px;"/></td>
      </tr>
    	<tr>
    	  <td style="vertical-align:top; padding-left:10px; ">Group</td>
    	  <td style="vertical-align:top; ">:</td>
    	  <td style="vertical-align:top; ">
          <select name="acad_program_groups_id" id="acad_program_groups_id" class="form">
    	    <?php
				foreach($groups AS $group) {
					if ($group->id == 254) { //'Same as Program' is the default
						print("<option value=".$group->id." selected>".$group->group_name."</option>");	
					} else {
						print("<option value=".$group->id.">".$group->group_name."</option>");
					}
				}
			?>
  	    </select></td>
  	  </tr>
    	<tr>
        	<td align="left" valign="middle">&nbsp;</td>
        	<td align="left" valign="middle">&nbsp;</td>
        	<td align="left" valign="middle"><input type="submit" value="Create!" /></td>
        </tr>
  </table>
</form>

-->

<h2 class="heading">Create Program</h2>
	<form class="form-horizontal" action="<?php echo site_url('dean/create_program');?>" method="post" name="create">
		<div class="control-group">
				<!-- <label class="control-label" for="department">Department</label>
				 <div class="controls">
					<p style="padding-top: 7px">:&nbsp&nbsp<?php print($department_name->department); ?></p>
				</div>
				 -->
				<div class="control-group">
					<label class="control-label" for="abbreviation">Abbreviation</label>
		   		 	<div class="controls">
		      			:&nbsp&nbsp<input type="text" id="abbreviations" class="span3" name="abbreviations" style="width:100px;">
		   		 	</div>
	   		 	</div>
	   		 	
	   		 	<div class="control-group">
					<label class="control-label" for="abbreviation">Description</label>
		   		 	<div class="controls">
		      				:&nbsp&nbsp<input type="text" id="description" class="span6" name="description">
		   		 	</div>
	   		 	</div>
	   		 	
	   		 	<div class="control-group">
					<label class="control-label" for="abbreviation">Group</label>
		   		 	<div class="controls">
		      			:&nbsp&nbsp<select name="acad_program_groups_id" id="acad_program_groups_id" class="form" style="width:auto;">
	    	   				 <?php
								foreach($groups AS $group) {
									if ($group->id == 254) { //'Same as Program' is the default
										print("<option value=".$group->id." selected>".$group->group_name."</option>");	
									} else {
										print("<option value=".$group->id.">".$group->group_name."</option>");
									}
								}
							?>
  	    				</select>
		   		 	</div>
	   		 	</div>
	   		 	<div class="control-group">
					<label class="control-label" for="abbreviation">Maximum Year Level</label>
		   		 	<div class="controls">
		      				:&nbsp&nbsp<select name="max_yr_level" id="max_yr_level" class="form" style="width:auto;">
		      					<option value="1">1 Year</option>
		      					<option value="2">2 Years</option>
		      					<option value="3">3 Years</option>
		      					<option value="4" selected>4 Years</option>
		      					<option value="5">5 Years</option>
		      				</select>
		   		 	</div>
	   		 	</div>
	   		 	<div class="form-actions">
					<button class="btn btn-success" type="submit">Create!</button>
				</div>
	   	</div>
	</form>