
<style>
	.myhead {
		background-color: #EAEAEA; 
	}
	
	td.head1 {
		text-align:center;
		vertical-align:middle;
		font-weight:bold;
	}

</style>

<h2 class="heading">Program Assessment Summary Report - College Student List</h2>

	<div style="width:100%;">

		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 35px; padding: 2px; margin-bottom:20px;">
			<div style="float:right; padding-top:8px; margin-right:10px;" >
				<a href="#" style="text-decoration: none; color:#4E4D4D; font-size:12px; "
					college_name="<?php print($college_name); ?>"
					college_code="<?php print($college_code); ?>"
					term_sy="<?php print($term_sy); ?>"
					reports='<?php print(json_encode($reports,JSON_HEX_APOS)); ?>'
					class="download_excel_report" >
					<div style="float:right; padding-left:3px; padding-top:2px;">Download to Excel</div>
					<div id="excel_labels" style="float:right;">
						<img src="<?php print(base_url('assets/images/excel_icon.png')); ?>" style="height:20px;" /> 
					</div>
				</a>
			</div>
		</div>
		
		<div style="width:100%; margin:0px auto; text-align:center; font-size:20px; font-weight:bold; color:#022E7A;" >
			<?php print($college_name); ?>
		</div>
		<div style="width:100%; margin:0px auto; text-align:center; font-size:16px; font-weight:bold; font-style:italic; color:#0A5901; padding-top:5px;" >
			<?php print($term_sy); ?>
		</div>
				

		<div style="width:95%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_students" >
			<?php
				$data = array(
							'reports'=>$reports,
							);
			
				$this->load->view('posting/views/list_of_students_with_posted_running_per_college',$data);					
			?>
		</div>


		
	</div>	
	
<script>
	$('.download_excel_report').click(function(event){
		event.preventDefault(); 
		
		var college_name = $(this).attr('college_name');
		var college_code = $(this).attr('college_code');
		var term_sy      = $(this).attr('term_sy');
		var reports      = $(this).attr('reports');
		
		$('#excel_labels').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/posted_running_college_student'); ?>",
			data: { 
					"college_name": college_name,
					"college_code": college_code,
					"term_sy": term_sy,
					"reports": reports,
					"action": 'create_posted_running_excel'},
			//dataType: 'json',
			success: function(response) {
				//$('#show_extracted_students').html(response.students); 
				window.location.href = '<?php print(base_url("downloads/assessment_".$college_code."_students.xls")); ?>';

				$('#excel_labels').html("<img src='<?php print(base_url('assets/images/excel_icon.png')); ?>' style='height:20px;'> ")
			}
	
		});
			
	});
		
</script>	

	