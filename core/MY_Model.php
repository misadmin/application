<?php 

class MY_Model extends CI_Model {
	protected $last_error;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function set_error(){
		$this->last_error = array(
				'code' =>$this->db->_error_number(),
				'error_description' => $this->db->_error_message(),
		);
	}
	
	public function last_error(){
		return $this->last_error;
	}
	
	public function mis_time_now(){
		$this->db->query("select now()");
		return $this->query->result();
	}
}