
<tbody> 
		
	<?php if ($datestatus) {	?>
	 	
	 	<div class="formSep" style = "margin-left:30px">
			<h2><?php print($title); ?>
			   
			</h2>
			<h3><?php print($sy);?> (As of <?php print($current_date); ?>)  <a href="download" class="btn btn-link " id="download_list"><i class="icon-download-alt"></i> <strong>Download as an Excel File</strong></a></h3>
		</div>
		<!-- <h2> <a href="download" class="btn btn-link pull-right" id="download_list"><i class="icon-download-alt"></i> <strong>Download</strong></a> </h2>  -->	
		
		<table class="table table-striped table-bordered table-condensed"; style="width:75%">
			<thead >
				<tr align="center">
					<th width="5%" class="head" style="vertical-align:middle; text-align:center;">No.</th>
					<th width="5%" class="head" style="vertical-align:middle; text-align:center;">ID No.</th>
					<th width="25%" class="head" style="vertical-align:middle;">Name</th>
					<th width="6%" class="head" style="vertical-align:middle; text-align:center;">Year Level</th>
					<th width="6%" class="head" style="vertical-align:middle; text-align:center;">Gender</th>
					<th width="10%" class="head" style="vertical-align:middle;">Date Cancelled</th>
					<th width="35%" class="head" style="vertical-align:middle;">Reason for Cancellation</th>
				</tr>
			</thead>
	<?php	if($student_list) {
			$count = 0;
			foreach ($student_list as $student): ?>
				<tr>
					<?php $count++;?>
					<td  style="text-align:center;"><?php print($count); ?></td>
					<td  style="text-align:center;"><?php print($student->students_idno); ?></td>
					<td><?php print($student->neym); ?></td>
					<td style="text-align:center;"><?php print($student->yr_level); ?></td>
					<td style="text-align:center;"><?php print($student->gender); ?></td>
					<td><?php print($student->withdrawn_on); ?></td>
					<td><?php print($student->withdrawn_reason); ?></td>
				</tr>
			<?php 
			endforeach;
				
	
		 }else { ?>
			<tr>
				<td colspan = "4">
					<b> No students/pupils </b>
				<td>
			</tr>
	

		<?php 	} ?> 
	<form method="post" action="<?php echo site_url("{$role}/download_basicEd_transferredOut_csv"); ?>" id="download_list_form">
			<input type="hidden" name="sy_id" value="<?php echo $academic_year_id; ?>" />
			<input type="hidden" name="cutOffDate" value="<?php echo $cutOffDate; ?>" />
</form>


<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
	
<?php }else {?>

		<table class="table table-striped table-bordered"; style="width:50%">
		<tr>
		  <td style = "color:red">
		     <b> Cut-Off Date is not yet set  </b>
		  </td>
		</tr>
		</table>
	<?php }?>
	</table>
	</tbody>
	



		

