<div style="background-color:#FFF; padding-top:20px; margin-top:20px;">
  <table class="table table-striped table-hover table-condensed" style="width:50%; border:solid; border-width:1px; border-color:#FF80FF;" align="center">
    <thead>
<tr><th style="text-align:center;">Course Code</th>
<th style="text-align:center;">Section</th>
<th style="text-align:center;">Grade</th></tr>
</thead>



<?php if($content): ?>
<?php foreach($content as $grade): ?>
<tr>
<td><?php print($grade->course_code); ?></td>
<td><?php print($grade->section_code); ?></td>
<td><?php print($grade->finals_grade); ?></td>
</tr>
<?php endforeach; ?>
<?php endif;  ?>
</table>
</div>