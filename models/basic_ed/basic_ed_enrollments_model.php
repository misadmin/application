<?php 

	class Basic_Ed_Enrollments_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		//Added: 4/12/2013 by genes
		/**
		 * This function retrieves the enrollment summary for Basic Ed
		 * @param smallint $academic_years_id (represents the academic year of enrollment)
		 * @return NULL
		 * @updated: 9/17/14 excludes self-contained
		 */
		function ListEnrollmentSummary($academic_years_id) {
			
			$result = null;
			
			$q1 = "SELECT
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=1
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m1,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=1
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f1,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=2
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m2,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=2
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f2,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=3
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id	
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m3,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=3
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f3,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=4
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id 
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m4,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=4
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f4,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=5
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m5,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=5
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f5,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=6
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m6,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=6
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f6,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=7
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m7,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=7
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f7,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=8
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m8,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=8
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f8,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=9
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m9,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=9
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f9,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=10
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m10,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=10
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f10,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=11
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m11,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=11
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f11,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='M'
								AND bb.yr_level=12
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS m12,
						(SELECT count(bb.id)
							FROM
								students AS aa,
								basic_ed_histories AS bb,
								levels AS cc
							WHERE
								aa.idno = bb.students_idno
								AND cc.id = bb.levels_id
								AND aa.gender='F'
								AND bb.yr_level=12
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status = 'active'
								AND cc.id = c.id
								AND (bb.basic_ed_sections_id NOT IN (166,175) OR bb.basic_ed_sections_id IS NULL) )
						AS f12,
						c.level,
						c.id
					FROM
						levels AS c
					WHERE 
						c.id IN (1,2,3,4,5,8,9,11,12) ";
		
			//print($q1); die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			return $result;
		}
		
		/**
		 * Insert basic ed student history (This student has no previous history yet)
		 * 
		 * @param unknown_type $student_id
		 * @param unknown_type $academic_terms_id
		 */
		function insert_student_history($student_id, $data, $academic_years_id=0){
			$this->load->model('academic_terms_model');
			$years_id = (!empty($academic_years_id) ? $academic_years_id : $this->academic_terms_model->current_academic_year()->academic_years_id);
			$year_level = (isset($data['year_level']) ? $data['year_level'] : "");
			$levels_id = (isset($data['levels']) ? $data['levels'] : "");
			$this->db->query("INSERT into basic_ed_students (`students_idno`) VALUE({$this->db->escape($student_id)})");
			
			$sql = "
				INSERT into 
					basic_ed_histories (students_idno, academic_years_id, levels_id, yr_level)
				VALUE
					({$this->db->escape($student_id)}, {$this->db->escape($years_id)}, {$this->db->escape($levels_id)}, {$this->db->escape($year_level)})
				";
			//print_r($sql);die();
			if($this->db->query($sql)){
				return $this->db->insert_id();
			} else {
				return FALSE;
			}
		}
		
		/**
		 * Insert basic ed student history (Student was previously enrolled)
		 * 
		 * @param unknown_type $student_id
		 */
		function insert_new_student_history ($data){
			return $this->db->insert ('basic_ed_histories', $data);
		}
		
		/**
		 * update student_history given history id
		 * 
		 * @param unknown_type $student_history_id
		 * @param unknown_type $data
		 */
		function update_student_history($data, $history_id){
			$this->db->where ('id', $history_id);
			return $this->db->update ('basic_ed_histories', $data);
		}
		
		function student_current_history ($student_id){
			$this->db->select("beh.id as history_id, 
					beh.students_idno, 
					CONCAT((ay.end_year - 1), ' - ', (ay.end_year)) as sy,
					IF(ay.status='current', 'enrolled', 'not enrolled') as enrolled, 
					levels.level as department,
					levels.id as department_id,
					yr_level,
					bes.section_name", FALSE);
			$this->db->from ('basic_ed_histories as beh');
			$this->db->join ('levels', 'levels.id=beh.levels_id', 'left');
			$this->db->join ('basic_ed_sections as bes', 'beh.basic_ed_sections_id=bes.id', 'left');
			$this->db->join ('academic_years as ay', 'ay.id=beh.academic_years_id', 'left');
			$this->db->where('beh.students_idno =', $student_id, FALSE);
			$this->db->order_by('beh.id', 'desc');
			if ($query = $this->db->get())
				return $query->row(); else
				return FALSE;
		}
		
			//Added: April 27, 2013 by Amie
		//EDITED: 9/15/13 by genes
		/**
		 * This function retrieves all enrollees for a given level, year and section in a particular academic year
		 * @param tinyint $level (represents the level being referred)
		 * @param tinyint $year (represents the grade level being referred)
		 * @param smallint $section (represents the section being referred)
		 * @param smallint $acad_year (represents the academic year)
		 * @return NULL
		 */
		
		function list_enrollees($level, $year, $section=NULL, $acad_year) {
			$result = null;
			
			$q = "
					SELECT 
						p.id, 
						s.lname, 
						CONCAT(s.lname,', ',s.fname) AS name, 
						IF (b.basic_ed_sections_id IS NULL,'No Section',bs.section_name) AS section, 
						b.students_idno,
						b.levels_id,
						b.yr_level as year_level,
						b.id as hist_id  /* added Toyet 7.27.2018*/
					FROM basic_ed_histories b
					-- LEFT JOIN basic_ed_students bb ON b.students_idno = bb.students_idno
					LEFT JOIN students s ON s.idno=b.students_idno
					LEFT JOIN basic_ed_sections bs ON b.basic_ed_sections_id = bs.id
					LEFT JOIN payers p ON p.students_idno = s.idno
					WHERE
						b.levels_id={$this->db->escape($level)}
						AND b.yr_level = {$this->db->escape($year)}
						AND b.status = 'active' 
						AND (b.basic_ed_sections_id NOT IN (166,175) OR b.basic_ed_sections_id IS NULL) 
			";
			
			// this portion was commented out
			// and I removed the comment marker - Toyet 10.8.2018
			// - this controls the section limit...
			if ($section) {
				$q = $q." AND b.basic_ed_sections_id = {$this->db->escape($section)}";
			}
			
			$q = $q." AND b.academic_years_id = {$this->db->escape($acad_year)} 
							ORDER BY s.lname, s.fname";
			
			//print($q); die();
			//log_message("INFO", print_r($q,true)); 
			//die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result; 
		}
		
		public function current_enrollees ($level, $year_level, $section){
			$this->db->select('s.idno,s.fname, s.lname, s.mname, payers.id as payers_id,sh.status as bed_status, sh.yr_level,l.level,sec.section_name as section,sh.id as student_histories_id', FALSE)
				->from('basic_ed_histories as sh')
				->join('students s', 'sh.students_idno=s.idno', 'left')
				->join('levels l', 'sh.levels_id=l.id', 'left')
				->join('basic_ed_sections sec', 'sh.basic_ed_sections_id=sec.id', 'left')
				->join('academic_years ay','sh.academic_years_id=ay.id', 'left')
				->join('payers', 'sh.students_idno=payers.students_idno', 'left')
				->where('ay.status','current')
				->where('sh.levels_id', $level)
				->where('sh.basic_ed_sections_id', $section)
				->where('sh.yr_level',$year_level)
				->group_by('s.idno')
				->order_by('s.lname asc, s.fname asc');
			$query = $this->db->get();
			if($query && $query->num_rows() > 0)
				return $query->result(); else
				return array();
		}
		
		public function list_section_summary($section, $level, $year_level, $acad) {
			$result = null;
			
			$q = "SELECT c.section_name, 
						(SELECT COUNT(DISTINCT(aa.idno))
							FROM students aa, basic_ed_students AS dd, basic_ed_histories AS bb, basic_ed_sections cc
							WHERE
								aa.idno = dd.students_idno
						        AND dd.students_idno = bb.students_idno
						        AND bb.basic_ed_sections_id = cc.id
								AND aa.gender='M'
								AND bb.yr_level={$this->db->escape($year_level)}
						        AND cc.id = {$this->db->escape($section)}
						        AND bb.academic_years_id = {$this->db->escape($acad)}) AS M1,
						(SELECT COUNT(DISTINCT(aa.idno))
							FROM students aa, basic_ed_students AS dd, basic_ed_histories AS bb, basic_ed_sections cc
							WHERE
								aa.idno = dd.students_idno
						        AND dd.students_idno = bb.students_idno
						        AND bb.basic_ed_sections_id = cc.id
								AND aa.gender='F'
								AND bb.yr_level={$this->db->escape($year_level)}
						 		AND cc.id = {$this->db->escape($section)}
						       	AND bb.academic_years_id = {$this->db->escape($acad)}) as F1
				 FROM students a, basic_ed_students d, basic_ed_histories b, basic_ed_sections c
				 WHERE a.idno = d.students_idno
					 AND d.students_idno = b.students_idno
					 AND b.basic_ed_sections_id = c.id
					 and b.yr_level = {$this->db->escape($year_level)}
					 AND c.id = {$this->db->escape($section)}
					 AND b.academic_years_id = {$this->db->escape($acad)}
				 GROUP BY c.id";
			
			
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
		
		//Added: 4/29/2014 by Isah
		
		function getPrevYrLevel($idnum) {
			$result = null;

			$q1 = "SELECT yr_level
					
					FROM
						basic_ed_histories
					WHERE
						students_idno = {$this->db->escape($idnum)}
					ORDER BY academic_years_id DESC, inserted_on DESC
					LIMIT 1";
			//print($q1); die();	
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
		}
		
		//Added: 4/29/2014 by Isah
		
		function enroll_BasicEd_oldStudent($student_id, $academic_year_id, $level_id, $yrLevel){

			$result = NULL;
				
			/*$sql = " INSERT into
						basic_ed_histories (students_idno, academic_years_id, levels_id, yr_level, status, inserted_on)
					VALUE
						({$this->db->escape($student_id)},
						{$this->db->escape($academic_year_id)}, 
						{$this->db->escape($level_id)},
						{$this->db->escape($yrLevel)},
						'active',
						NOW()) 
					
					ON DUPLICATE KEY UPDATE
						yr_level={$this->db->escape($yrLevel)},
						status= 'active' ";
			
			
			$q1 = "SELECT
							id,
							yr_level,
							levels_id
						FROM
							basic_ed_histories 
						WHERE
							students_idno = {$this->db->escape($student_id)}
							AND academic_years_id = {$this->db->escape($academic_year_id)}
						ORDER BY
							id DESC
						LIMIT 1";
			
			$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row(); */
				
			$q2 = "INSERT into
						basic_ed_histories 
							(students_idno, 
							academic_years_id, 
							levels_id, 
							yr_level, 
							status, 
							inserted_on)
						VALUES
							({$this->db->escape($student_id)},
							{$this->db->escape($academic_year_id)}, 
							{$this->db->escape($level_id)},
							{$this->db->escape($yrLevel)},
							'inactive',
							NOW())"; 	  

			//print($q2); die();				
					if($this->db->query($q2)){
						return $this->db->insert_id();
					} else {
						return FALSE;
					}	
	} 
		
		
		

		//Added: 5/15/2014 by Isah
		
		/*function enroll_BasicEd_Student($student_id, $academic_year_id){
				
			$q = "UPDATE basic_ed_histories
				SET
					status = 'active'
			
				WHERE
					students_idno = {$this->db->escape($student_id)}
					AND academic_years_id = {$this->db->escape($academic_year_id)} ";
			//print($q); die();
			$this->db->query($q);
			return $this->db->affected_rows();
		}
		*/
	
		/*
		 * EDITED: 4/10/15 by genes
		 */
		function enroll_BasicEd_Student($basic_ed_histories_id,$strand_id=NULL) {
			$q = "UPDATE 
						basic_ed_histories
					SET
						status = 'active',
						strand_id = {$this->db->escape($strand_id)}
				WHERE
					id = {$this->db->escape($basic_ed_histories_id)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
		/*
		 * ADDED: 4/10/15 by genes
		 * Inactivates histories status of a student
		 */
		function set_InActive($students_idno) {
			$q = "UPDATE 
						basic_ed_histories
					SET
						status = 'inactive'
				WHERE
					students_idno = {$this->db->escape($students_idno)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}			
			
		}
		
	//Added: 5/14/2014 by Isah
		
		function update_section($stud_id, $acaYr_id, $section) {
			
			$q = "UPDATE basic_ed_histories
				SET
					basic_ed_sections_id = {$this->db->escape($section)}
			
				WHERE
					students_idno = {$this->db->escape($stud_id)}
					AND academic_years_id = {$this->db->escape($acaYr_id)} ";
			//print($q); die();
			$this->db->query($q);
			return $this->db->affected_rows();
				
		}		
		
		//Added: 5/15/2014 by Isah
		
		function update_yearLevel($stud_id, $acaYr_id, $year, $level) {
			
			$updated_by = (isset($this->userinfo['empno']) ? $this->userinfo['empno'] : '');
			
			$q = "UPDATE 
						basic_ed_histories
					SET
						yr_level = {$this->db->escape($year)},
						levels_id = {$this->db->escape($level)},
						updated_by = {$updated_by},
						updated_on = now()
					WHERE
						students_idno = {$this->db->escape($stud_id)}
						AND academic_years_id = {$this->db->escape($acaYr_id)} ";
			//print($q); die();
			$this->db->query($q);
			return $this->db->affected_rows();
		
		}
		
		//Added: 5/15/2014 by Isah
		
		function insert_gpa($stud_id, $acaYr_id, $gpa, $empno){
				
			$sql = " INSERT into
						gpas (students_idno, 
							academic_years_id, 
							gpa, 
							inserted_by, 
							inserted_on)
						VALUE
							({$this->db->escape($stud_id)},
							{$this->db->escape($acaYr_id)},
							{$this->db->escape($gpa)},
							{$this->db->escape($empno)},
							NOW())";
		
			//print_r($sql);die();
			if($this->db->query($sql)){
				return $this->db->insert_id();
			} else {
				return FALSE;
			}
	}
	
	//Added: 5/15/2014 by Isah
	function list_gpas($stud_id) {
		$result = null;
	
		$q1 = "SELECT a.id, a.gpa, CONCAT(b.end_year-1,'-',b.end_year) as sy, c.yr_level
					FROM 
						gpas as a
						LEFT JOIN 
						  academic_years as b ON  b.id = a.academic_years_id
						LEFT JOIN 
						  basic_ed_histories as c ON a.academic_years_id = c.academic_years_id AND c.students_idno = {$this->db->escape($stud_id)}
					WHERE 
					  a.students_idno = {$this->db->escape($stud_id)}
		 			ORDER BY a.academic_years_id DESC";
		//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
	}

	//Added: 5/15/2014 by Isah
	
	function update_gpa($gpa_id, $gpa) {
	
		$q = "UPDATE gpas
				SET
					gpa = {$this->db->escape($gpa)}
	
				WHERE
					id = {$this->db->escape($gpa_id)}
		 ";
		//print($q); die();
		$this->db->query($q);
		return $this->db->affected_rows();
	
	}
	
	//Added: 5/16/2014 by Isah
	
	function get_rank($idnum) {
		$result = null;
	
		$q1 = "
				SELECT b.rank
				FROM
					basic_ed_histories as a,
					levels as b 
				WHERE
					students_idno = {$this->db->escape($idnum)}
					AND a.levels_id = b.id
				ORDER BY academic_years_id DESC, inserted_on DESC
				LIMIT 1
			";
		//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
		$result = $query->row();
		}
	
		return $result;
	}
	
	//Added: 5/17/2014 by Isah
	
	function getHistoryInfo($idnum, $sy_id, $levels_id=NULL, $yr_level=NULL) {
		$result = null;

		if ($levels_id) {
			$q1 = "SELECT * 
					FROM
						basic_ed_histories 
					WHERE
						students_idno = {$this->db->escape($idnum)}
						AND academic_years_id = {$this->db->escape($sy_id)}
						AND levels_id = {$this->db->escape($levels_id)}
						AND yr_level = {$this->db->escape($yr_level)}";
		} else {
			$q1 = "SELECT * 
					FROM
						basic_ed_histories 
					WHERE
						students_idno = {$this->db->escape($idnum)}
						AND academic_years_id = {$this->db->escape($sy_id)}";
		}
		
		//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
	
		return $result;
	}
		
//Added: 5/17/2014 by Isah
		//Updated: 3/22/2019 by genes
		public function getLatestHistoryInfo($idnum=NULL) {
			
			$result = null;
		
			$q1 = "
					SELECT a.id, 
						  a.academic_years_id,
						  f.levels_id, 
						  e.year_level AS yr_level, 
						  a.basic_ed_sections_id,
						  a.status,
						  a.inserted_on,
						  a.withdrawn_by,
						  a.withdrawn_on,
						  a.withdrawn_reason,
						  b.id as academic_terms_id
					FROM
						basic_ed_histories as a,
						academic_terms as b,
						prospectus AS c,
						academic_programs AS d,
						bed_academic_programs AS e,
						acad_program_groups AS f 
					WHERE
						students_idno = {$this->db->escape($idnum)}
						AND a.academic_years_id = b.academic_years_id 
						AND a.prospectus_id = c.id 
						AND c.academic_programs_id = d.id 
						AND d.id = e.academic_programs_id 
						AND d.acad_program_groups_id = f.id 
						AND b.status = 'current'
					ORDER BY academic_years_id DESC
					LIMIT 1	
				";
			//print($q1); die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
		
			return $result;
			}
	
		//Added: 5/19/2014 by Isah
			
		function searchGPA($idnum, $acad_year) {
			$result = null;
		
			$q1 = "SELECT *
					FROM
						gpas
					WHERE
						students_idno = {$this->db->escape($idnum)}
						AND academic_years_id = {$this->db->escape($acad_year)}	";
			//	print($q1); die();
			$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		}

		//Added: 5/19/2014 by Isah
		
		function withdraw($stud_id, $acaYr_id, $reason, $dateF, $empno) {
		
			$q = "UPDATE basic_ed_histories
					SET
					status = 'transferred',
					withdrawn_on = {$this->db->escape($dateF)},
					withdrawn_by = {$this->db->escape($empno)},
					withdrawn_reason = {$this->db->escape($reason)}
				WHERE
					students_idno = {$this->db->escape($stud_id)}
					AND academic_years_id = {$this->db->escape($acaYr_id)}";
			//print($q); die();
			$this->db->query($q);
			return $this->db->affected_rows();
		
		}
		
		//Added: 5/19/2014 by Isah
		function list_withdrawals($stud_id) {
			$result = null;
		
			$q1 = "SELECT a.id, a.withdrawn_on, CONCAT(b.end_year-1,'-',b.end_year) as sy, a.withdrawn_reason
					FROM
						basic_ed_histories as a,
						academic_years as b
					WHERE
						a.students_idno = {$this->db->escape($stud_id)}
						AND (a.status = 'transferred' OR a.status = 'left')
						AND b.id = a.academic_years_id
					ORDER BY academic_years_id DESC";
			//print($q1); die();
			$query = $this->db->query($q1);
		
			if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
		}
		
		//Added: 5/22/2014 by Isah
		
		function update_withdrawals($hist_id, $withdrawDate, $withdrawReason){
		
			$q = "UPDATE basic_ed_histories
					SET
						withdrawn_on = {$this->db->escape($withdrawDate)}, 
						withdrawn_reason = {$this->db->escape($withdrawReason)}
		
					WHERE
						id = {$this->db->escape($hist_id)}
			";
			//print($q); die();
			$this->db->query($q);
			return $this->db->affected_rows();
		
		}
		
		//Added: 5/23/2014 by Isah
		
		function insertEnrollmentSched($academicYrId, $startDate, $endtDate, $empno){
		
		$sql = " INSERT into
						enrollment_schedules_bed 
						(academic_years_id, 
						 start_date, 
						 end_date,
						inserted_on,
						inserted_by)
						VALUE
							({$this->db->escape($academicYrId)},
							{$this->db->escape($startDate)},
							{$this->db->escape($endtDate)},
							NOW(),
							{$this->db->escape($empno)}
							)";
		
			//print_r($sql);die();
			if($this->db->query($sql)){
				return $this->db->insert_id();
			} else {
				return FALSE;
			}
		
		}
		
		//Added: 5/23/2014 by Isah
			
		function getEnrollmentSched($acad_year_id) {
			$result = null;
		
			$q1 = "SELECT *
						FROM
							enrollment_schedules_bed
					WHERE
						academic_years_id = {$this->db->escape($acad_year_id)}	";
			//	print($q1); die();
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
					
				return $result;
		}
		
		//Added: 5/23/2014 by Isah
		function listEnrollmentSched() {
			$result = null;
		
			$q1 = "SELECT a.id, a.start_date, 
						  a.end_date,
					      CONCAT(b.end_year-1,'-',b.end_year) AS sy 
						FROM
							enrollment_schedules_bed as a,
							academic_years as b
						WHERE
							a.academic_years_id = b.id
						ORDER by b.id DESC";

			$query = $this->db->query($q1);
		
			if($query->num_rows() > 0){
			$result = $query->result();
			}
			return $result;
	}
	
	//Added: 5/22/2014 by Isah
	
	function updateEnrollmentSched($schedule_id, $startDate, $endtDate){
	
		$q = "UPDATE enrollment_schedules_bed
				SET
					start_date = {$this->db->escape($startDate)},
					end_date = {$this->db->escape($endtDate)}
	
				WHERE
					id = {$this->db->escape($schedule_id)}";
		
		//print($q); die();
		$this->db->query($q);
		return $this->db->affected_rows();
	
	}
	
	//Added: 5/26/2014 by Isah
		
	function setTransferCutOff($tDate, $schedule_id) {
		$result = null;
	
		$q = "UPDATE enrollment_schedules_bed
				SET
					withdrawal_cutoff_date = {$this->db->escape($tDate)}
				WHERE
					id = {$this->db->escape($schedule_id)}";
		
		//print($q); die();
		$this->db->query($q);
		return $this->db->affected_rows();
	}
		
	
	//Added: 5/26/2014
	
	//Added: 5/23/2014 by Isah
	
	function insertTransferCutOff($academicYrId, $tDate, $empno){
	
		$sql = " INSERT into
					enrollment_schedules_bed
					(academic_years_id,
					withdrawal_cutoff_date,
					inserted_on,
					inserted_by)
				VALUE
					({$this->db->escape($academicYrId)},
					{$this->db->escape($tDate)},
					NOW(),
					{$this->db->escape($empno)}
					)";
	
		//print_r($sql);die();
		if($this->db->query($sql)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	
	}
	
	//Added: 5/26/2014 by Isah
	function listTransferCutOffDates() {
		$result = null;
	
		$q1 = "SELECT a.id, a.withdrawal_cutoff_date,
					      CONCAT(b.end_year-1,'-',b.end_year) AS sy
						FROM
							enrollment_schedules_bed as a,
							academic_years as b
						WHERE
							a.academic_years_id = b.id
						ORDER by b.id DESC";
	
		$query = $this->db->query($q1);
	
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
	}
	
	
	//Added: 5/17/2014 by Isah
	
	function listStudentsTransferredOut($data) {
		$result = NULL;
	
		$q = "SELECT
				a.students_idno,
				a.id AS student_histories_id,
				a.yr_level,
				CONCAT(UPPER(b.lname),', ',CONCAT(UPPER(substring(b.fname,1,1)), LOWER(substring(b.fname,2,LENGTH(b.fname)))),' ',LEFT(b.mname,1)) AS neym,
				b.gender,
				DATE_FORMAT(a.withdrawn_on,'%b. %e, %Y') AS withdrawn_on, 
				a.withdrawn_reason
			FROM
				basic_ed_histories AS a,
				students AS b
			WHERE
				a.students_idno = b.idno
				AND a.academic_years_id = {$this->db->escape($data['academic_year_id'])}
				AND a.withdrawn_on <= {$this->db->escape($data['cutOffDate'])}
			ORDER BY b.lname";
	
			
		//print($q);	die();
		$query = $this->db->query($q);
			
		if($query->num_rows() > 0){
		$result = $query->result();
	}
		
	return $result;
	}
	
	//Added: 5/17/2014 by Isah
	
	function listStudentsDroppeddOut($data) {
		$result = NULL;
	
		$q = "SELECT
					a.students_idno,
					a.id AS student_histories_id,
					a.yr_level,
					CONCAT(UPPER(b.lname),', ',CONCAT(UPPER(substring(b.fname,1,1)), LOWER(substring(b.fname,2,LENGTH(b.fname)))),' ',LEFT(b.mname,1)) AS neym,
					b.gender,
					DATE_FORMAT(a.withdrawn_on,'%b. %e, %Y') AS withdrawn_on, 
					a.withdrawn_reason
		FROM
		basic_ed_histories AS a,
		students AS b
		WHERE
		a.students_idno = b.idno
		AND a.academic_years_id = {$this->db->escape($data['academic_year_id'])}
		AND a.status = 'transferred'
		AND a.withdrawn_on > {$this->db->escape($data['cutOffDate'])}
		ORDER BY b.lname";
	
		
		//print($q);	die();
			$query = $this->db->query($q);
			
		if($query->num_rows() > 0){
		$result = $query->result();
	}
	
	return $result;
   }
   
   //Added: 5/27/2014 by Isah
   
   function enrollmentSummaryBasicEd($academic_year_id) {
   	$result = NULL;
   
   	$q = "SELECT COUNT(DISTINCT(b.id)) as num_enrollees, c.level, b.yr_level, b.levels_id
		  FROM 
			  	basic_ed_students d, 
				basic_ed_histories b, 
	   			levels as c
		  WHERE
			    d.students_idno = b.students_idno
	   			AND b.levels_id  = c.id
	      		AND b.academic_years_id = {$this->db->escape($academic_year_id)}
	      		AND b.status = 'active'
		   		GROUP BY b.levels_id, b.yr_level
	      		ORDER BY c.rank,  b.yr_level";	 
	
   	//print($q); die();
   		$query = $this->db->query($q);
   		
   	if($query->num_rows() > 0){
   		$result = $query->result();
  	}
   	return $result;
   }
   
   
		//Added: 5/29/2014 by Isah
		/*
		* EDITED: 4/12/15 by genes
		* parameters broken to details for uniformity
		*/	
		function ListBasicStudentsPromoted($cur_yr_id, $level_id, $yr_level) {
			$result = NULL;
		   		
			$q = "SELECT
					   	a.id AS student_histories_id,
					   	a.students_idno,
					   	a.inserted_on,
					   	CONCAT(UPPER(b.lname),', ',CONCAT(UPPER(substring(b.fname,1,1)), LOWER(substring(b.fname,2,LENGTH(b.fname)))),' ',LEFT(b.mname,1),'.') AS neym,
					   	a.yr_level,
					   	c.section_name,
					   	b.gender
					FROM
					   	basic_ed_histories AS a,
					   	students AS b,
					   	basic_ed_sections AS c
				   	WHERE
				   		a.academic_years_id = {$this->db->escape($cur_yr_id)}
				   		AND a.yr_level = {$this->db->escape($yr_level)}
				   		AND a.levels_id = {$this->db->escape($level_id)}
				   		AND a.status = 'active'
				   		AND a.students_idno = b.idno
				   		AND a.basic_ed_sections_id = c.id
				   	ORDER BY 
				   		a.basic_ed_sections_id, 
				   		b.lname ";
		   		
		   	$query = $this->db->query($q);
		   		
		   	if($query->num_rows() > 0){
			   	$result = $query->result();
			}
	   
	  		return $result;
	   }
   
   
		/*
		 * EDITED: 4/12/15 by genes
		 * parameters broken to details for uniformity
		 * function name also renamed
		 */
		function ListStudents_EnrolledNotSectioned($cur_yr_id, $level_id, $yr_level) {

			$result = NULL;
		   	 
		   	$q = "SELECT
					   	a.id AS student_histories_id,
					   	a.students_idno,
					   	a.inserted_on,
					   	b.gender,
					   	CONCAT(UPPER(b.lname),', ',CONCAT(UPPER(substring(b.fname,1,1)), LOWER(substring(b.fname,2,LENGTH(b.fname)))),' ',LEFT(b.mname,1),'.') AS neym
				   	FROM
					   	basic_ed_histories AS a,
					   	students AS b
				   	WHERE
					   	a.academic_years_id = {$this->db->escape($cur_yr_id)}
					   	AND a.yr_level = {$this->db->escape($yr_level)}
					   	AND a.levels_id = {$this->db->escape($level_id)}
			   			AND a.status = 'active'
			   			AND a.students_idno = b.idno
					   	AND a.basic_ed_sections_id IS NULL
		   		   	ORDER BY 
		   		   		b.lname ";

		   	//die($q); 
			$query = $this->db->query($q);
		   	 
		   	if($query->num_rows() > 0){
		   		$result = $query->result();
		   	}
		    
		  	return $result;
		  	
		}
    
	   //Added: 5/30/2014 by Isah
	   /*
	    * EDITED: 4/14/15 by genes
	    * former function: ListBasicStudentsNotPromoted($data)
		* @Updated: 3/24/2019 genes
	    */
	   public function ListStudents_Registered($cur_yr_id=NULL, $prospectus_id=NULL) {
			
			$result = NULL;
	   	 
	   		$q = "SELECT
	   					a.id AS student_histories_id,
						a.students_idno,
						a.inserted_on,
						b.gender,
						CONCAT(UPPER(b.lname),', ',CONCAT(UPPER(substring(b.fname,1,1)), LOWER(substring(b.fname,2,LENGTH(b.fname)))),' ',LEFT(b.mname,1),'.') AS neym
					FROM
	   					basic_ed_histories AS a,
	   					students AS b
				   	WHERE
					   	a.students_idno = b.idno
				   		AND a.academic_years_id = {$this->db->escape($cur_yr_id)}
					   	AND a.prospectus_id = {$this->db->escape($prospectus_id)}
					   	AND a.enrolled_status = 'N'
					ORDER BY 
						b.lname,
						b.fname ";
	   
			//return $q;
			$query = $this->db->query($q);
	   	 
			if($query->num_rows() > 0){
				$result = $query->result();
			}
	   
			return $result;
			
	   }
   
   //5/30/2014
   function ListBasicStudentsforSectioning($data) {
   	$result = NULL;
   	 
   	$q = "SELECT
			   	a.id AS student_histories_id,
			   	a.students_idno,
			   	CONCAT(UPPER(b.lname),', ',CONCAT(UPPER(substring(b.fname,1,1)), LOWER(substring(b.fname,2,LENGTH(b.fname)))),' ',LEFT(b.mname,1),'.') AS neym,
			   c.gpa
		   	FROM
			   	basic_ed_histories AS a,
			   	students AS b,
			    gpas AS c
		   	WHERE
		   	a.academic_years_id = {$this->db->escape($data['cur_yr_id'])}
		   	AND a.yr_level = {$this->db->escape($data['yr_level'])}
		   	AND a.levels_id = {$this->db->escape($data['level_id'])}
		   	AND a.status = 'active'
		   	AND a.students_idno = b.idno
		    AND a.students_idno = c.students_idno
		    AND c.academic_years_id = {$this->db->escape($data['prev_id'])}
		   	AND (a.basic_ed_sections_id IS NULL OR a.basic_ed_sections_id IN(55,56,57,58))
		  
		   	ORDER BY c.gpa DESC";
   		   	 
   		   	// 	print($q); die();
   		   	//die();
   		   	$query = $this->db->query($q);
   	 
   	if($query->num_rows() > 0){
   		$result = $query->result();
   	}
    
  	 return $result;
 }
   
   //Added: May 29, 2014 bye Amie
   /*
    * EDITED: 7/7/15 by genes
    */
	function generate_list($academicyear, $level, $yr_level, $basic_ed_sections_id) {
 		$result = null;
   		
   		/*$q = "SELECT s.lname, s.fname, s.mname, s.gender
				from students s, basic_ed_histories beh, sections_histories sh
				where s.idno = beh.students_idno
					and beh.basic_ed_sections_id = sh.basic_ed_sections_id
					and sh.academic_years_id = {$this->db->escape($academicyear)}
					and sh.levels_id ={$this->db->escape($level)}
					and sh.basic_ed_sections_id = {$this->db->escape($section)}
					and sh.year_level ={$this->db->escape($year)}
					and beh.status = 'active'
				order by gender, s.lname";
   		*/
 		$q = "SELECT 
 					a.lname, 
 					a.fname, 
 					a.mname, 
 					a.gender
				FROM
					students AS a, 
					basic_ed_histories AS b 
						LEFT JOIN sections_histories AS c 
							ON b.academic_years_id = c.academic_years_id
							AND b.levels_id = c.levels_id
							AND b.yr_level = c.year_level
							AND b.basic_ed_sections_id = c.basic_ed_sections_id
				WHERE
					a.idno = b.students_idno
					AND b.academic_years_id = {$this->db->escape($academicyear)}
					AND b.levels_id = {$this->db->escape($level)}
					AND b.yr_level = {$this->db->escape($yr_level)}
					AND b.basic_ed_sections_id = {$this->db->escape($basic_ed_sections_id)} 
 					AND b.status = 'active'
 				ORDER BY 
 					a.gender, 
 					a.lname,
 					a.fname,
 					a.mname ";
 		
 		//die($q);
   		$query = $this->db->query($q);
   		 
   		if($query->num_rows() > 0){
   			$result = $query->result();
   		}
   		
   		return $result;
   }
   
   //Added: 5/31/2014 by Isah
   
   function section_student($history_id, $section_id) {
   		
   	$q = "UPDATE basic_ed_histories
   			SET
   				basic_ed_sections_id = {$this->db->escape($section_id)}
   		
   			WHERE
   				id = {$this->db->escape($history_id)}";
  // print($q); die();
   	$this->db->query($q);
   	return $this->db->affected_rows();
   
   }
   
   //Added: 6/6/2014
   
   function delete_gpa($id) {
   
	   	$query = "DELETE FROM
	   				gpas
	   			WHERE
	   				id = {$this->db->escape($id)} ";
	   
	   	if ($this->db->query($query)) {
	   		return TRUE;
	   } else {
	   		return FALSE;
	   }
   	
   }
   
   function remove_withdrawals($hist_id){
   
   	$q = "UPDATE basic_ed_histories
   			SET
   				status = 'active',
   				withdrawn_on = NULL,
   				withdrawn_reason = NULL,
   				withdrawn_by = NULL
   				
      	WHERE
   			id = {$this->db->escape($hist_id)}	";
   	//print($q); die();
   	$this->db->query($q);
   	return $this->db->affected_rows();
   
   }
   
   //Added: 6/6/2014 by Isah
   //5/30/2014
   function ListStudentsforRandomSectioning($data) {
   	$result = NULL;
   	 
   	$q = "SELECT
   	a.id AS student_histories_id,
   	a.students_idno,
   	CONCAT(UPPER(b.lname),', ',CONCAT(UPPER(substring(b.fname,1,1)), LOWER(substring(b.fname,2,LENGTH(b.fname)))),' ',LEFT(b.mname,1),'.') AS neym
   
   	FROM
	   	basic_ed_histories AS a,
	   	students AS b
   	WHERE
	   	a.academic_years_id = {$this->db->escape($data['cur_yr_id'])}
	   	AND a.yr_level = {$this->db->escape($data['yr_level'])}
	   	AND a.levels_id = {$this->db->escape($data['level_id'])}
	   	AND a.status = 'active'
	   	AND a.students_idno = b.idno
   		AND (a.basic_ed_sections_id IS NULL OR a.basic_ed_sections_id IN(55,56,57,58))
   	ORDER BY b.lname";
   
   	 	//print($q); die();
   			//die();
   			$query = $this->db->query($q);
   			 
   			if($query->num_rows() > 0){
   			$result = $query->result();
   }
   
   return $result;
   }
   
   //Added: 6/27/2014 by Isah
   
   function getMyTuitionFee($acad_years_id, $levels_id, $yr_level) {
	   	$result = null;
	   	
	   	$q1 = "SELECT a.rate
	   			FROM
	   				academic_terms AS d,
	   				fees_schedule as a
	   					LEFT JOIN fees_subgroups as b ON a.fees_subgroups_id = b.id
	   						LEFT JOIN fees_groups as c ON b.fees_groups_id = c.id
	   			WHERE
	   				d.id = a.academic_terms_id
	   				AND d.academic_years_id = {$this->db->escape($acad_years_id)}
	   				AND a.levels_id = {$this->db->escape($levels_id)}
	   				AND a.yr_level = {$this->db->escape($yr_level)}
				   	AND c.id = 9";
	   	
	   //	print($q1); die();
	   	$query = $this->db->query($q1);
	   
	   	if($query && $query->num_rows() > 0){
	   	$result = $query->row();
	   	}
	   
	   	return $result;
	   	}
	   	//Added: 6/27/2014 by Isah
	   	 
	   	function checkIfAssessed($hist_id) {
	   		$result = null;
	   		 
	   		$q1 = "SELECT *
	   				FROM
				   		assessments_bed as a
	   				WHERE
				   		a.basic_ed_histories_id = {$this->db->escape($hist_id)}";
	   		 
	   		//print($q1); die();
	   		$query = $this->db->query($q1);
	   	
	   		if($query && $query->num_rows() > 0){
	   			$result = $query->row();
	   	}
	   	
	   	return $result;
	   	}  	
	   	
	  //Added: 6/28/2014 by Isah
	   	
	   	function pending_withdraw($stud_id, $acaYr_id, $reason, $dateF, $empno) {
	   	
	   		$q = "UPDATE basic_ed_histories
	   		SET
	   		status = 'left',
	   		withdrawn_on = {$this->db->escape($dateF)},
	   		withdrawn_by = {$this->db->escape($empno)},
	   		withdrawn_reason = {$this->db->escape($reason)}
	   		WHERE
	   		students_idno = {$this->db->escape($stud_id)}
	   		AND academic_years_id = {$this->db->escape($acaYr_id)}";
	   			//print($q); die();
	   			$this->db->query($q);
	   			return $this->db->affected_rows();
	   	
	   	}
	   	
	   	//Added: 7/23/2014 by Isah
	   	
	   	function insertExamDates($data, $year_level){
	   	
	   		$sql = " INSERT into
	   					periods_bed
	   					(academic_years_id,
	   					levels_id,
	   					year_level,
	   					exam_number,
	   					start_date,
				   		end_date,
				   		inserted_on,
				   		inserted_by)
			   		VALUE(
				   		{$this->db->escape($data['cur_yr_id'])},
				   		{$this->db->escape($data['level_id'])},
				   		'{$this->db->escape($year_level)}',
				   		{$this->db->escape($data['exam_no'])},
				   		{$this->db->escape($data['start_date'])},
				   		{$this->db->escape($data['end_date'])},
				   		NOW(),
				   		{$this->db->escape($data['empno'])}
			   		)";
	   	
	   		//print($sql);die();
	   		
	   		if($this->db->query($sql)){
	   			return TRUE;
	   		} else {
	   			return FALSE;
	   		}
	   	
		}
	
	/*
	* @Added: 7/23/2014 by Isah
	* @Updated: 9/4/14 by genes
	* @notes: levels_id added
	*/
	function listExamDates($levels_id, $cur_acad_year_id) {
		$result = null;
	
		$q1 = "SELECT 
						a.id, a.year_level,
					    CONCAT(b.end_year-1,'-',b.end_year) AS sy,
						c.level,
						DATE_FORMAT(a.start_date,'%W, %b, %e, %Y') AS start_date1,
						DATE_FORMAT(a.end_date,'%W, %b, %e, %Y') AS end_date1,
						a.start_date,
						a.end_date,
						a.exam_number
					FROM
						periods_bed as a,
						academic_years as b,
						levels as c
					WHERE
						a.academic_years_id = {$this->db->escape($cur_acad_year_id)}
						AND a.academic_years_id = b.id
						AND a.levels_id = c.id
						AND a.levels_id = {$this->db->escape($levels_id)}
					ORDER BY
						c.id DESC, 
						a.year_level, 
						a.exam_number ASC";
	
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
	}
	
	//Added: 7/24/2014 by Isah
	
	function updateExamDate($schedule_id, $start, $end) {
		$result = null;
	
		$q = "UPDATE periods_bed
				SET
					start_date = {$this->db->escape($start)},
					end_date = {$this->db->escape($end)}
		WHERE
				id = {$this->db->escape($schedule_id)}";
	
		//print($q); die();
		$this->db->query($q);
		return $this->db->affected_rows();
	}
	
	//Added: 7/24/2014 by Isah
	 
	function deleteExamDate($id) {
		 
		$query = "DELETE FROM
					periods_bed
				WHERE
				id = {$this->db->escape($id)} ";
	
		if ($this->db->query($query)) {
		return TRUE;
	} else {
	return FALSE;
	}
	
	}
	
	//Added: 8/1/2014 by isah
	
	function ListBasicLevels($rank) {
		$result = 0;
		$q = "
			SELECT id,level
			FROM levels
			WHERE status = 'active'
				AND rank >= {$this->db->escape($rank->rank)}
				AND rank < 5.0
			ORDER BY
				rank
		"; 
	
		//print($q);die();
		$query = $this->db->query($q);
	
		if	($query && $query->num_rows() > 0){
			$result = $query->result();
		}
	
		return $result;
	
	}
	
	
	function list_all_active_basic_levels(){
		$sql = "
			SELECT id,level
			FROM levels
			WHERE status = 'active' and type='basic_ed'
			ORDER BY rank
		";		
		$query = $this->db->query($sql);		
		if	($query && $query->num_rows() > 0){
			return $query->result();
		}
		return FALSE; 
		
		
	}
	
	
	/*
	 * ADDED: 4/10/15 by genes
	 */
	function ListHistories_for_Enrollment($student_idno) {
	
		$result = NULL;
		
		$q = "SELECT
					a.id,
					CONCAT('SY ',a.end_year-1,'-',a.end_year) AS sy,
					CONCAT(c.level,' ',b.yr_level) AS yr_level,
					b.id AS basic_ed_histories_id,
					b.levels_id
				FROM
					academic_years AS a,
					basic_ed_histories AS b,
					levels AS c
				WHERE
					a.id = b.academic_years_id
					AND b.levels_id = c.id
					AND b.students_idno = {$this->db->escape($student_idno)} 
				ORDER BY 
					a.id DESC 
				LIMIT 2	";
		
		//print($q); die();
		$query = $this->db->query($q);
	
		if	($query && $query->num_rows() > 0){
			$result = $query->result();
		}
	
		return $result;
	}
	
	/*
	 * ADDED: 4/10/15 by Genes
	 */
	public function Is_New_Student($student_idno=NULL) {		
		$result = NULL;		
		$q = "
			SELECT
				ay.id,
				CONCAT('SY ',ay.end_year-1,'-',ay.end_year) sy,
				bh.id basic_ed_histories_id,
				bh.academic_years_id,
				bh.status
			FROM
				academic_years ay
				join basic_ed_histories bh on bh.academic_years_id = ay.id 
				JOIN basic_ed_students AS bes ON bes.students_idno = bh.students_idno
			WHERE
				bh.students_idno = {$this->db->escape($student_idno)} 
		";
		
		//print($q); die();
		$query = $this->db->query($q);
		$result = $query->result();
		if	($query && $query->num_rows() == 1){
			$result[0]->new_student = TRUE;
		}
		else {
			if ($query->num_rows() == 0 )
				return FALSE;
			else
				$result[0]->new_student = FALSE;  			
		}
		return $result;
	}
	
	/*
	 * ADDED: 4/12/15 by genes
	 * 
	 */
	function ListLevels_With_Enrollees($acad_yr_id,$section_status) {
		$result = NULL;
		
		/*$q = "SELECT 
					(@cnt := @cnt + 1) AS rowNo, t.*
				FROM
					(SELECT
						CONCAT(c.level,' ',b.yr_level) AS display_yr_level,
						b.levels_id,
						b.yr_level
					FROM
						basic_ed_histories AS b,
						levels AS c
					WHERE
						b.levels_id = c.id
						AND b.academic_years_id = {$this->db->escape($acad_yr_id)}
						AND b.basic_ed_sections_id IS NULL
						AND b.status='active'
					GROUP BY
						c.id, b.yr_level 
					ORDER BY
						c.id, b.yr_level) AS t
				CROSS JOIN (SELECT @cnt := 0) AS dummy ";
		*/
		$q = "SELECT
					CONCAT(b.levels_id,'-',b.yr_level) AS row_id,
					CONCAT(c.level,' ',b.yr_level) AS display_yr_level,
					b.levels_id,
					b.yr_level
				FROM
					basic_ed_histories AS b,
					levels AS c
				WHERE
					b.levels_id = c.id
					AND b.academic_years_id = {$this->db->escape($acad_yr_id)} ";
		
		if ($section_status == 'not_sectioned') {
			$q .= "	AND b.basic_ed_sections_id IS NULL ";
		} else {
			$q .= "	AND b.basic_ed_sections_id IS NOT NULL ";
		}
		
		$q .= "	AND b.status='active'
				GROUP BY
					c.id, b.yr_level 
				ORDER BY
					c.id, b.yr_level";
		
		//die($q); 
		$query = $this->db->query($q);
	
		if	($query && $query->num_rows() > 0){
			$result = $query->result();
		}
	
		return $result;
		
	}

	
	
	/*
	 * ADDED: 4/14/15 by genes
	 * NOTE: No need, just use ListProspectus 3/24/2019 
	 */
	/*function ListLevels_With_Registers($acad_yr_id) {
		$result = NULL;
		
		$q = "SELECT
					CONCAT(b.levels_id,'-',b.yr_level) AS row_id,
					CONCAT(c.level,' ',b.yr_level) AS display_yr_level,
					b.levels_id,
					b.yr_level
				FROM
					basic_ed_histories AS b,
					levels AS c
				WHERE
					b.levels_id = c.id
					AND b.academic_years_id = {$this->db->escape($acad_yr_id)}
					AND b.status='inactive'
				GROUP BY
					c.id, b.yr_level 
				ORDER BY
					c.id, b.yr_level";
		
		//die($q); 
		$query = $this->db->query($q);
	
		if	($query && $query->num_rows() > 0){
			$result = $query->result();
		}
	
		return $result;
		
	}*/	
	
	
	/*
	 * ADDED: 4/20/15 by genes
	 * 
	 */
	function getLastHistoryStatus($idnum) {
		$result = NULL;
		
		$q = "SELECT 
					*
				FROM
					basic_ed_histories
				WHERE
					students_idno = {$this->db->escape($idnum)}
				ORDER BY
					id DESC
				LIMIT 1";
		
		$query = $this->db->query($q);
		
		if	($query && $query->num_rows() > 0){
			$result = $query->row();
		}
		
		return $result;
		
	}
	
	/*
	 * ADDED: 2/10/16 by genes
	 * 
	 */
	function ListK12Strands() {
		$result = NULL;
		
		$q = "SELECT 
					*
				FROM
					strand ";
		
		//die($q); 
		$query = $this->db->query($q);
	
		if	($query && $query->num_rows() > 0){
			$result = $query->result();
		}
	
		return $result;
		
	}	
	
	
}

//end of student_model.php
