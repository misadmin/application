<?php
	if (!empty($error_msg)) {
		echo '<div style="margin-bottom:10px; margin-top:10px;">';
		$this->content_lib->set_message($error_msg, "alert");
		//print($error_msg);
		echo '</div>';
	}
?>
<h2 class="heading">My Profile</h2>
<div class="row-fluid">
	<div class="span12">
		<div class="media">
			<a class="pull-left" href="#">
				<center><img src="<?php echo $image; ?>" alt="" class="media-object img-polaroid" data-src="holder.js/<?php echo (isset($profile_image_max_width) ? $profile_image_max_width.'x'.$profile_image_max_width : '140x140') ?>" style="max-width:<?php echo (isset($profile_image_max_width) ? $profile_image_max_width.'px' : '140px') ?>" /></center>
			</a>
			<div class="media-body">
				<h3 class="media-heading"><?php echo $name; ?> <span >[<?php echo $idnum; ?>] - <?php echo $level; ?> <?php echo $course; ?></span></h3>
<?php if (trim($religion)!=='') { ?>
				<h5><i class="icon-plus"></i> <?php echo $religion; ?></h5>
<?php }; ?>																
<?php if (trim($full_home_address)!=='') { ?>
				<h5><i class="icon-home"></i> <span style="font-size:13px;font-weight:normal"><?php echo str_replace(array("\r\n\r\n\r\n", "\r\n\r\n", "\n\n\n", "\n\n"), ",", $full_home_address); ?></span></h4>
<?php }; ?>												
<?php if (trim($full_city_address)!=='') { ?>
				<h5><i class="icon-barcode"></i> <span style="font-size:13px;font-weight:normal"><?php echo str_replace(array("\r\n\r\n\r\n", "\r\n\r\n", "\n\n\n", "\n\n"), ",", $full_city_address); ?></span></h4>
<?php }; ?>										
<?php if (trim($phone_number) !== '') { ?>
				<h5><i class="icon-envelope"></i> <span style="font-size:13px;font-weight:normal"><?php echo $phone_number; ?></span></h4>
<?php }; ?>												
				
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		var unresolveds = <?php echo $unresolveds; ?> 
		if (unresolveds > 0 ) {
			var redirect_to = <?php echo "'./student/tools'"; ?>;	
			$.amaran({
				content  : {'message' : 'You have ' + unresolveds + ' unresolved feedbacks!',},
				position : 'bottom right',
				inEffect:'slideRight',
				onClick: function(){ window.location = redirect_to ; },
				delay:120000,
			});
		}
	});
</script>
