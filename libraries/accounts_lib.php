<?php

class accounts_lib {
	
	/**
	 * Codeigniter Instance
	 * @var unknown_type
	 */
	protected $CI;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}


	//ADDED: 8/8/13 by genes
	//EDITED: 4/13/14 genes
	function schedule_by_acad_program() {
		$this->CI->load->model('accounts/fees_schedule_model');
		$this->CI->load->model('hnumis/Programs_model');
		$this->CI->load->model('academic_terms_model');
		$this->CI->load->model('hnumis/Academicyears_model');
		$this->CI->load->model('teller/Assessment_model');
		$can_update=FALSE;
	
		$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 'mainpage');
		
		/* only accounts role can edit/delete fees */
		if ($this->CI->session->userdata('role') == 'accounts') { 
			$can_update=TRUE;
		}		
		switch ($step) {
			case 'mainpage':
				//$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees(457, 359);
				//print_r($other_tuition_fees); die();
				$acad_programs = NULL;
				$selected_program = NULL;
				
				
				$current_term = $this->CI->academic_terms_model->getCurrentAcademicTerm();
				$acad_terms = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);
				$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($current_term->id);

				if ($this->CI->Assessment_model->CheckAcadTermHasAssessment($current_term->id)) {
					$can_update=FALSE;
				}
				
				if ($acad_programs) {
					//print($current_term->id.' = '.$acad_programs[0]->id); die();
					$selected_program   = $acad_programs[0]->id;
					$description        = $acad_programs[0]->description;
					$program_items      = $this->CI->fees_schedule_model->ListProgramItems($current_term->id,$acad_programs[0]->acad_program_groups_id);
					$lab_items          = $this->CI->fees_schedule_model->ListLaboratoryItems($current_term->sy_id, $acad_programs[0]->id);
					$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($current_term->id, $acad_programs[0]->id);
					$grp_abbreviation   = $acad_programs[0]->group_abbreviation;
					
					//ADDED: 6/11/15 by Genes
					//MODIFIED: 6/25/15 by genes
					$affiliated_items   = $this->CI->fees_schedule_model->ListAffiliated_Items($current_term->id, $acad_programs[0]->id);
				
				} else { //incase no acad_programs have fees assigned 
					$description="";
					$program_items="";
					$lab_items="";
					$other_tuition_fees="";
					$grp_abbreviation="";
					$affiliated_items="";
				}
				$data = array(
						"my_program"=>array('description'=>$description,
								'sy_semester'=>$current_term->term." ".$current_term->sy,
								'group'=>$grp_abbreviation),
						"acad_programs"=>$acad_programs,
						"terms"=>$acad_terms,
						"selected_term"=>$current_term->id,
						"selected_program"=>$selected_program,
						"academic_description"=>$description,
						"program_items"=>$program_items,
						"lab_items"=>$lab_items,
						"affiliated_items"=>$affiliated_items,
						"other_tuition_fees"=>$other_tuition_fees,
						"can_update"=>$can_update,
				);
					
				break;
	
			case 'change_pulldown': //pulldown menu is clicked
				$description="";
				$program_items="";
				$lab_items="";
				$other_tuition_fees="";
				$grp_abbreviation="";
				$program = NULL;
				
				$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
				
				if ($this->CI->input->post('academic_programs_id')) {
					$program = $this->CI->Programs_model->getProgram($this->CI->input->post('academic_programs_id'));
				} else {
					if ($acad_programs) {
						$program = $this->CI->Programs_model->getProgram($acad_programs[0]->id);
					}
				}
				
				$acad_term	   = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);

				if ($acad_term->status == 'previous' OR $this->CI->Assessment_model->CheckAcadTermHasAssessment($this->CI->input->post('academic_terms_id'))) {
					$can_update=FALSE;
				}
				
				if ($program) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$program->acad_program_groups_id);					
				} 
				
				//print_r($program_items); die();
				$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $this->CI->input->post('academic_programs_id'));
				$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				//print_r($other_tuition_fees); die();
				
				//ADDED: 6/11/15 by Genes
				$affiliated_items   = $this->CI->fees_schedule_model->ListAffiliated_Items($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				if (!$program_items AND $acad_programs) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$acad_programs[0]->acad_program_groups_id);
				}
				if (!$lab_items AND $acad_programs) {
					$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $acad_programs[0]->id);
				}
				if (!$other_tuition_fees AND $acad_programs) {
					$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $acad_programs[0]->id);
				}

				if ($program AND $program_items) {
					foreach($acad_programs AS $k) {
						if ($k->id == $program->id) {
							$description=$program->description;
							$grp_abbreviation=$program->group_abbreviation;
							break;
						} else {
							$description=$acad_programs[0]->description;
							$grp_abbreviation=$acad_programs[0]->group_abbreviation;
						}
					}
				}
				$data = array(
						"my_program"=>array('description'=>$description,
								'sy_semester'=>$acad_term->term." ".$acad_term->sy,
								'group'=>$grp_abbreviation),
						"acad_programs"=>$acad_programs,
						"terms"=>$acad_terms,
						"selected_term"=>$this->CI->input->post('academic_terms_id'),
						"selected_program"=>$this->CI->input->post('academic_programs_id'),
						"program_items"=>$program_items,
						"affiliated_items"=>$affiliated_items,
						"lab_items"=>$lab_items,
						"other_tuition_fees"=>$other_tuition_fees,
						"can_update"=>$can_update,
				);
	
				break;
				
			case 'delete_schedule_fee': //delete schedule fee
				
				if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
					if ($this->CI->fees_schedule_model->DeleteFeesSchedule($this->CI->input->post('fees_schedule_id'))) {
						$this->CI->content_lib->set_message("Fee Schedule successfully deleted!", 'alert-success');
					}	
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
					$this->CI->index();
				}

				$description="";
				$program_items="";
				$lab_items="";
				$other_tuition_fees="";
				$grp_abbreviation="";
				
				$program       = $this->CI->Programs_model->getProgram($this->CI->input->post('academic_programs_id'));
				$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
				$acad_term	   = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);
				
				if ($program) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$program->acad_program_groups_id);					
				} 
				$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $this->CI->input->post('academic_programs_id'));
				$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				//ADDED: 6/26/15 by Genes
				$affiliated_items   = $this->CI->fees_schedule_model->ListAffiliated_Items($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				if (!$program_items AND $acad_programs) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$acad_programs[0]->acad_program_groups_id);
				}
				if (!$lab_items AND $acad_programs) {
					$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $acad_programs[0]->id);
				}
				if (!$other_tuition_fees AND $acad_programs) {
					$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $acad_programs[0]->id);
				}

				if ($program AND $program_items) {
					foreach($acad_programs AS $k) {
						if ($k->id == $program->id) {
							$description=$program->description;
							$grp_abbreviation=$program->group_abbreviation;
							break;
						} else {
							$description=$acad_programs[0]->description;
							$grp_abbreviation=$acad_programs[0]->group_abbreviation;
						}
					}
				}
				$data = array(
						"my_program"=>array('description'=>$description,
								'sy_semester'=>$acad_term->term." ".$acad_term->sy,
								'group'=>$grp_abbreviation),
						"acad_programs"=>$acad_programs,
						"terms"=>$acad_terms,
						"selected_term"=>$this->CI->input->post('academic_terms_id'),
						"selected_program"=>$this->CI->input->post('academic_programs_id'),
						"program_items"=>$program_items,
						"affiliated_items"=>$affiliated_items,
						"lab_items"=>$lab_items,
						"other_tuition_fees"=>$other_tuition_fees,
						"can_update"=>$can_update,
				);
								
				break;

			case 'delete_lab_fee': //delete laboratory fee
				
				if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
					if ($this->CI->fees_schedule_model->DeleteLaboratoryFees($this->CI->input->post('laboratory_fees_id'))) {
						$this->CI->content_lib->set_message("Laboratory Fee successfully deleted!", 'alert-success');
					}	
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
					$this->CI->index();
				}

				$description="";
				$program_items="";
				$lab_items="";
				$other_tuition_fees="";
				$grp_abbreviation="";
				
				$program       = $this->CI->Programs_model->getProgram($this->CI->input->post('academic_programs_id'));
				$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
				$acad_term	   = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);
				
				if ($program) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$program->acad_program_groups_id);					
				} 
				$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $this->CI->input->post('academic_programs_id'));
				$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				//ADDED: 6/26/15 by Genes
				$affiliated_items   = $this->CI->fees_schedule_model->ListAffiliated_Items($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				if (!$program_items AND $acad_programs) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$acad_programs[0]->acad_program_groups_id);
				}
				if (!$lab_items AND $acad_programs) {
					$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $acad_programs[0]->id);
				}
				if (!$other_tuition_fees AND $acad_programs) {
					$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $acad_programs[0]->id);
				}

				if ($program AND $program_items) {
					foreach($acad_programs AS $k) {
						if ($k->id == $program->id) {
							$description=$program->description;
							$grp_abbreviation=$program->group_abbreviation;
							break;
						} else {
							$description=$acad_programs[0]->description;
							$grp_abbreviation=$acad_programs[0]->group_abbreviation;
						}
					}
				}
				$data = array(
						"my_program"=>array('description'=>$description,
								'sy_semester'=>$acad_term->term." ".$acad_term->sy,
								'group'=>$grp_abbreviation),
						"acad_programs"=>$acad_programs,
						"terms"=>$acad_terms,
						"selected_term"=>$this->CI->input->post('academic_terms_id'),
						"selected_program"=>$this->CI->input->post('academic_programs_id'),
						"program_items"=>$program_items,
						"affiliated_items"=>$affiliated_items,
						"lab_items"=>$lab_items,
						"other_tuition_fees"=>$other_tuition_fees,
						"can_update"=>$can_update,
				);
								
				break;

			case 'delete_tuition_others': //delete other tuition fee
				
				if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
					if ($this->CI->fees_schedule_model->DeleteTuitionOthers($this->CI->input->post('tuition_others_id'))) {
						$this->CI->content_lib->set_message("Other Tuition Fee successfully deleted!", 'alert-success');
					}	
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
					$this->CI->index();
				}

				$description="";
				$program_items="";
				$lab_items="";
				$other_tuition_fees="";
				$grp_abbreviation="";
				
				$program       = $this->CI->Programs_model->getProgram($this->CI->input->post('academic_programs_id'));
				$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
				$acad_term	   = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);
				
				if ($program) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$program->acad_program_groups_id);					
				} 
				$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $this->CI->input->post('academic_programs_id'));
				$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				//ADDED: 6/26/15 by Genes
				$affiliated_items   = $this->CI->fees_schedule_model->ListAffiliated_Items($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				if (!$program_items AND $acad_programs) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$acad_programs[0]->acad_program_groups_id);
				}
				if (!$lab_items AND $acad_programs) {
					$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $acad_programs[0]->id);
				}
				if (!$other_tuition_fees AND $acad_programs) {
					$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $acad_programs[0]->id);
				}

				if ($program AND $program_items) {
					foreach($acad_programs AS $k) {
						if ($k->id == $program->id) {
							$description=$program->description;
							$grp_abbreviation=$program->group_abbreviation;
							break;
						} else {
							$description=$acad_programs[0]->description;
							$grp_abbreviation=$acad_programs[0]->group_abbreviation;
						}
					}
				}
				$data = array(
						"my_program"=>array('description'=>$description,
								'sy_semester'=>$acad_term->term." ".$acad_term->sy,
								'group'=>$grp_abbreviation),
						"acad_programs"=>$acad_programs,
						"terms"=>$acad_terms,
						"selected_term"=>$this->CI->input->post('academic_terms_id'),
						"selected_program"=>$this->CI->input->post('academic_programs_id'),
						"program_items"=>$program_items,
						"affiliated_items"=>$affiliated_items,
						"lab_items"=>$lab_items,
						"other_tuition_fees"=>$other_tuition_fees,
						"can_update"=>$can_update,
				);
								
				break;

			case 7: //edit Fees Schedule
				if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
					$data['rate'] = $this->CI->input->post('rate');
					$data['fees_schedule_id'] = $this->CI->input->post('fees_schedule_id');
					
					if ($this->CI->fees_schedule_model->UpdateFeesSchedule($data)) {
						$this->CI->content_lib->set_message("Fee Schedule successfully updated!", 'alert-success');
					}	
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
					$this->CI->index();
				}

				$description="";
				$program_items="";
				$lab_items="";
				$other_tuition_fees="";
				$grp_abbreviation="";
				
				$program       = $this->CI->Programs_model->getProgram($this->CI->input->post('academic_programs_id'));
				$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
				$acad_term	   = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);
				
				if ($program) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$program->acad_program_groups_id);					
				} 
				$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $this->CI->input->post('academic_programs_id'));
				$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				//ADDED: 6/26/15 by Genes
				$affiliated_items   = $this->CI->fees_schedule_model->ListAffiliated_Items($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				if (!$program_items AND $acad_programs) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$acad_programs[0]->acad_program_groups_id);
				}
				if (!$lab_items AND $acad_programs) {
					$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $acad_programs[0]->id);
				}
				if (!$other_tuition_fees AND $acad_programs) {
					$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $acad_programs[0]->id);
				}

				if ($program AND $program_items) {
					foreach($acad_programs AS $k) {
						if ($k->id == $program->id) {
							$description=$program->description;
							$grp_abbreviation=$program->group_abbreviation;
							break;
						} else {
							$description=$acad_programs[0]->description;
							$grp_abbreviation=$acad_programs[0]->group_abbreviation;
						}
					}
				}
				$data = array(
						"my_program"=>array('description'=>$description,
								'sy_semester'=>$acad_term->term." ".$acad_term->sy,
								'group'=>$grp_abbreviation),
						"acad_programs"=>$acad_programs,
						"terms"=>$acad_terms,
						"selected_term"=>$this->CI->input->post('academic_terms_id'),
						"selected_program"=>$this->CI->input->post('academic_programs_id'),
						"program_items"=>$program_items,
						"affiliated_items"=>$affiliated_items,
						"lab_items"=>$lab_items,
						"other_tuition_fees"=>$other_tuition_fees,
						"can_update"=>$can_update,
				);
								
				break;
				
			case 8: //edit Laboratory Fee
				
				if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
					$data['rate'] = $this->CI->input->post('lab_rate');
					$data['laboratory_fees_id'] = $this->CI->input->post('laboratory_fees_id');
					
					if ($this->CI->fees_schedule_model->UpdateLaboratoryFees($data)) {
						$this->CI->content_lib->set_message("Laboratory Fee successfully updated!", 'alert-success');
					}	
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
					$this->CI->index();
				}

				$description="";
				$program_items="";
				$lab_items="";
				$other_tuition_fees="";
				$grp_abbreviation="";
				
				$program       = $this->CI->Programs_model->getProgram($this->CI->input->post('academic_programs_id'));
				$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
				$acad_term	   = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);
				
				if ($program) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$program->acad_program_groups_id);					
				} 
				$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $this->CI->input->post('academic_programs_id'));
				$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				//ADDED: 6/11/15 by Genes
				$affiliated_items   = $this->CI->fees_schedule_model->ListAffiliated_Items($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				if (!$program_items AND $acad_programs) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$acad_programs[0]->acad_program_groups_id);
				}
				if (!$lab_items AND $acad_programs) {
					$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $acad_programs[0]->id);
				}
				if (!$other_tuition_fees AND $acad_programs) {
					$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $acad_programs[0]->id);
				}

				if ($program AND $program_items) {
					foreach($acad_programs AS $k) {
						if ($k->id == $program->id) {
							$description=$program->description;
							$grp_abbreviation=$program->group_abbreviation;
							break;
						} else {
							$description=$acad_programs[0]->description;
							$grp_abbreviation=$acad_programs[0]->group_abbreviation;
						}
					}
				}
				$data = array(
						"my_program"=>array('description'=>$description,
								'sy_semester'=>$acad_term->term." ".$acad_term->sy,
								'group'=>$grp_abbreviation),
						"acad_programs"=>$acad_programs,
						"terms"=>$acad_terms,
						"selected_term"=>$this->CI->input->post('academic_terms_id'),
						"selected_program"=>$this->CI->input->post('academic_programs_id'),
						"program_items"=>$program_items,
						"affiliated_items"=>$affiliated_items,
						"lab_items"=>$lab_items,
						"other_tuition_fees"=>$other_tuition_fees,
						"can_update"=>$can_update,
				);
				
				break;
				
			case 9: //edit Other Tuition Fee
				//print($this->CI->input->post('tuition_others_id')."<br>"); 
				//print($this->CI->input->post('rate'));die();
				if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
					$data['rate'] = $this->CI->input->post('rate');
					$data['tuition_others_id'] = $this->CI->input->post('tuition_others_id');
					
					if ($this->CI->fees_schedule_model->UpdateTuitionOtherFees($data)) {
						$this->CI->content_lib->set_message("Other Tuition Fee successfully updated!", 'alert-success');
					}	
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
					$this->CI->index();
				}

				$description="";
				$program_items="";
				$lab_items="";
				$other_tuition_fees="";
				$grp_abbreviation="";
				
				$program       = $this->CI->Programs_model->getProgram($this->CI->input->post('academic_programs_id'));
				$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
				$acad_term	   = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);
				
				if ($program) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$program->acad_program_groups_id);					
				} 
				$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $this->CI->input->post('academic_programs_id'));
				$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				//ADDED: 6/11/15 by Genes
				$affiliated_items   = $this->CI->fees_schedule_model->ListAffiliated_Items($this->CI->input->post('academic_terms_id'), $this->CI->input->post('academic_programs_id'));
				
				if (!$program_items AND $acad_programs) {
					$program_items = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$acad_programs[0]->acad_program_groups_id);
				}
				if (!$lab_items AND $acad_programs) {
					$lab_items = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $acad_programs[0]->id);
				}
				if (!$other_tuition_fees AND $acad_programs) {
					$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $acad_programs[0]->id);
				}

				if ($program AND $program_items) {
					foreach($acad_programs AS $k) {
						if ($k->id == $program->id) {
							$description=$program->description;
							$grp_abbreviation=$program->group_abbreviation;
							break;
						} else {
							$description=$acad_programs[0]->description;
							$grp_abbreviation=$acad_programs[0]->group_abbreviation;
						}
					}
				}
				$data = array(
						"my_program"=>array('description'=>$description,
								'sy_semester'=>$acad_term->term." ".$acad_term->sy,
								'group'=>$grp_abbreviation),
						"acad_programs"=>$acad_programs,
						"terms"=>$acad_terms,
						"selected_term"=>$this->CI->input->post('academic_terms_id'),
						"selected_program"=>$this->CI->input->post('academic_programs_id'),
						"program_items"=>$program_items,
						"affiliated_items"=>$affiliated_items,
						"lab_items"=>$lab_items,
						"other_tuition_fees"=>$other_tuition_fees,
						"can_update"=>$can_update,
				);
				
				break;
				
		}
	
		$this->CI->content_lib->enqueue_body_content('accounts/fees_schedule/schedule_by_acad_program',$data);
		$this->CI->content_lib->content();
	
	
	}
		

	//ADDED: 8/30/13 by genes
	//EDITED: 4/2/14 includes Update and Delete
	function schedule_for_basic_ed() {
		$this->CI->load->model('accounts/fees_schedule_model');
		$this->CI->load->model('academic_terms_model');
		$this->CI->load->model('hnumis/Academicyears_model');
		$this->CI->load->model('basic_ed/rsclerk_model');
		$this->CI->load->model('basic_ed/basic_ed_sections_model');
		$this->CI->load->model('teller/Assessment_model');
		$can_update=FALSE;
		
		$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 1);
		$school_years = $this->CI->Academicyears_model->ListAcadYears();
		//$basic_levels= $this->CI->rsclerk_model->ListBasicEdLevels();
		$basic_levels = $this->CI->basic_ed_sections_model->ListBasicLevels();
		/* only accounts role can edit/delete fees */
		if ($this->CI->session->userdata('role') == 'accounts') { 
			$can_update=TRUE;
		}		
		
		switch ($step) {
			case 1:
				
				$current_term = $this->CI->academic_terms_model->getCurrentBasicTerm();
				//$acad_terms = $this->CI->academic_terms_model->ListAcademicTerms($school_years[0]->id);
				$basic_ed_items = $this->CI->fees_schedule_model->ListBasic_Ed_Items($current_term->id,$basic_levels[0]->id);

				if ($this->CI->Assessment_model->CheckAcadTermHasAssessment($current_term->id)) {
					$can_update=FALSE;
				}
				
				$data = array(
						"school_years"=>$school_years,
						"basic_levels"=>$basic_levels,
						"selected_basic_ed"=>$basic_levels[0]->id,
						"selected_year"=>$current_term->academic_years_id,
						"school_yr"=>'SY '.$current_term->sy,
						"basic_ed_description"=>$basic_levels[0]->level,
						"basic_ed_items"=>$basic_ed_items,
						"can_update"=>$can_update,
				);
					
				break;
						
			case 3: //pulldown menu clicked
				$acad_yr    = $this->CI->Academicyears_model->GetAcademicYear($this->CI->input->post('academic_year_id'));
				$acad_terms = $this->CI->academic_terms_model->ListAcademicTerms($this->CI->input->post('academic_year_id'));
				$basic_ed_items = $this->CI->fees_schedule_model->ListBasic_Ed_Items($acad_terms[0]->id,$this->CI->input->post('basic_levels_id'));
				
				if ($acad_yr->status == 'previous' OR $this->CI->Assessment_model->CheckAcadTermHasAssessment($acad_terms[0]->id)) {
					$can_update=FALSE;
				}

				$b_level = $this->CI->rsclerk_model->getBasicEdLevel($this->CI->input->post('basic_levels_id'));
				
				$data = array(
						"school_years"=>$school_years,
						"basic_levels"=>$basic_levels,
						"selected_basic_ed"=>$this->CI->input->post('basic_levels_id'),
						"selected_year"=>$this->CI->input->post('academic_year_id'),
						"school_yr"=>$acad_yr->sy,
						"basic_ed_description"=>$b_level->level,
						"basic_ed_items"=>$basic_ed_items,
						"can_update"=>$can_update,
				);
	
				break;
	
			case 4: //deletes fees schedule
				if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
					if ($this->CI->fees_schedule_model->DeleteFeesSchedule($this->CI->input->post('fees_schedule_id'))) {
						$this->CI->content_lib->set_message("Basic Education Fee Schedule successfully deleted!", 'alert-success');
					}	
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
					$this->CI->index();
				}

				$acad_yr    = $this->CI->Academicyears_model->GetAcademicYear($this->CI->input->post('academic_year_id'));
				$acad_terms = $this->CI->academic_terms_model->ListAcademicTerms($this->CI->input->post('academic_year_id'));
				$basic_ed_items = $this->CI->fees_schedule_model->ListBasic_Ed_Items($acad_terms[0]->id,$this->CI->input->post('basic_levels_id'));
				$b_level = $this->CI->rsclerk_model->getBasicEdLevel($this->CI->input->post('basic_levels_id'));
				
				$data = array(
						"school_years"=>$school_years,
						"basic_levels"=>$basic_levels,
						"selected_basic_ed"=>$this->CI->input->post('basic_levels_id'),
						"selected_year"=>$this->CI->input->post('academic_year_id'),
						"school_yr"=>$acad_yr->sy,
						"basic_ed_description"=>$b_level->level,
						"basic_ed_items"=>$basic_ed_items,
						"can_update"=>$can_update,
				);
				
				break;
				
			case 5: //edits fees schedule
				
				if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
					$data['rate'] = $this->CI->input->post('rate');
					$data['fees_schedule_id'] = $this->CI->input->post('fees_schedule_id');
					
					if ($this->CI->fees_schedule_model->UpdateFeesSchedule($data)) {
						$this->CI->content_lib->set_message("Fee Schedule successfully updated!", 'alert-success');
					}	
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
					$this->CI->index();
				}
				
				$acad_yr    = $this->CI->Academicyears_model->GetAcademicYear($this->CI->input->post('academic_year_id'));
				$acad_terms = $this->CI->academic_terms_model->ListAcademicTerms($this->CI->input->post('academic_year_id'));
				$basic_ed_items = $this->CI->fees_schedule_model->ListBasic_Ed_Items($acad_terms[0]->id,$this->CI->input->post('basic_levels_id'));
				$b_level = $this->CI->rsclerk_model->getBasicEdLevel($this->CI->input->post('basic_levels_id'));
				
				$data = array(
						"school_years"=>$school_years,
						"basic_levels"=>$basic_levels,
						"selected_basic_ed"=>$this->CI->input->post('basic_levels_id'),
						"selected_year"=>$this->CI->input->post('academic_year_id'),
						"school_yr"=>$acad_yr->sy,
						"basic_ed_description"=>$b_level->level,
						"basic_ed_items"=>$basic_ed_items,
						"can_update"=>$can_update,
				);
				
				break;
				
		}
	
		$this->CI->content_lib->enqueue_body_content('accounts/fees_schedule/schedule_for_basic_ed',$data);
		$this->CI->content_lib->content();
	
	
	}
	

	//ADDED: 9/9/13 by genes
	function courses_with_lab_fees() {
		$this->CI->load->model('accounts/fees_schedule_model');
		$this->CI->load->model('hnumis/Academicyears_model');
	
		$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 1);
		$school_years = $this->CI->Academicyears_model->ListAcadYears();
	
		switch ($step) {
			case 1:
	
				$lab_courses = $this->CI->fees_schedule_model->ListCourses_with_Lab_Fees($school_years[0]->id);
	
				$data = array(
						"school_years"=>$school_years,
						"lab_courses"=>$lab_courses,
						"selected_year"=>$school_years[0]->id,
						"school_yr"=>$school_years[0]->sy,
				);
					
				break;
	
			case 3:
	
				$acad_yr    = $this->CI->Academicyears_model->GetAcademicYear($this->CI->input->post('academic_year_id'));
				$lab_courses = $this->CI->fees_schedule_model->ListCourses_with_Lab_Fees($this->CI->input->post('academic_year_id'));
				
				$data = array(
						"school_years"=>$school_years,
						"lab_courses"=>$lab_courses,
						"selected_year"=>$this->CI->input->post('academic_year_id'),
						"school_yr"=>$acad_yr->sy,						
				);
	
				break;
	
		}
	
		$this->CI->content_lib->enqueue_body_content('accounts/fees_schedule/courses_with_lab_fees',$data);
		$this->CI->content_lib->content();
	
	}
	
	
	function privileges() {
		
		$this->CI->load->model('financials/Scholarship_Model');
		$this->CI->load->model('Academic_terms_model');
		
		$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 1);
		
		$privilege_items = $this->CI->Scholarship_Model->ListPrivileges();
		$privilege_items_id = $privilege_items[0]->id;
		$posted_status = 'Y';
		$current_term = $this->CI->Academic_terms_model->current_academic_term(); 
		$select_terms = $this->CI->Academic_terms_model->academic_terms();		
		if($this->CI->input->post('selected_term')){
			$current_selected_term_id = $this->CI->input->post('selected_term');
		} else {
			$current_selected_term_id = $current_term->id;
		}

		switch ($step) {

			case 1:

					$privilege = $this->CI->Scholarship_Model->getScholarshipInfo($privilege_items[0]->id);
					$with_privileges = $this->CI->Scholarship_Model->ListStudentsAvailed($privilege_items[0]->id,
							$current_selected_term_id,'N');
					
					$data = array(
							"posted"=>'Y',
							"privilege_items"=>$privilege_items,
							"selected_item"=>"",
							"academic_term"=>$current_term,
							"privilege"=>$privilege,
							"with_privileges"=>$with_privileges,
							"select_terms"=>$select_terms,
							"current_selected_term_id"=>$current_selected_term_id,
					);
						
					break;
								
			case 2:
					$privilege = $this->CI->Scholarship_Model->getScholarshipInfo($this->CI->input->post('privilege_items_id'));
					$with_privileges = $this->CI->Scholarship_Model->ListStudentsAvailed($this->CI->input->post('privilege_items_id'),
							$current_selected_term_id,$this->CI->input->post('posted'));
					
					$data = array(
							"posted"=>$this->CI->input->post('posted'),
							"privilege_items"=>$privilege_items,
							"selected_item"=>$this->CI->input->post('privilege_items_id'),
							"academic_term"=>$current_term,
							"privilege"=>$privilege,
							"with_privileges"=>$with_privileges,
							"select_terms"=>$select_terms,
							"current_selected_term_id"=>$current_selected_term_id,
					);
						
					break;

			case 4:
					$privilege = $this->CI->Scholarship_Model->getScholarshipInfo($this->CI->input->post('privilege_items_id'));
					$with_privileges = $this->CI->Scholarship_Model->ListStudentsAvailed($this->CI->input->post('privilege_items_id'),
							$current_selected_term_id,$this->CI->input->post('posted'));
						
					$data = array(
							"posted"=>$this->CI->input->post('posted'),
							"privilege_items"=>$privilege_items,
							"selected_item"=>$this->CI->input->post('privilege_items_id'),
							"academic_term"=>$current_term,
							"privilege"=>$privilege,
							"with_privileges"=>$with_privileges,
							"select_terms"=>$select_terms,
							"current_selected_term_id"=>$current_selected_term_id,
					);
				
					break;

			case 'download_to_pdf':
					$with_priv=json_decode($this->CI->input->post('with_priv')); 
					$scholar_desc = $this->CI->input->post('scholar_desc');
					$acad_term = $this->CI->input->post('acad_term');
					//log_message("INFO", "CASE download_to_pdf ".print_r($with_priv,true)); // Toyet 10.19.2018
					$this->generate_scholars_list_pdf($with_priv,$scholar_desc,$acad_term,'College');

					return;

			case 'download_to_csv':
					$with_priv=json_decode($this->CI->input->post('with_priv')); 
					$scholar_desc = $this->CI->input->post('scholar_desc');
					$acad_term = $this->CI->input->post('acad_term');
					////log_message("INFO", print_r($with_priv,true)); // Toyet 10.19.2018
					$this->download_scholars_list_to_csv($with_priv,$scholar_desc,$acad_term,'College');
					return;
		}
		
		$this->CI->content_lib->enqueue_body_content('accounts/list_students_with_privileges2', $data);
		$this->CI->content_lib->content();
		
	}
	
	/*
	 * ADDED: 8/20/15 by genes
	 */
	function privileges_basic_ed() {
		
		$this->CI->load->model('financials/Scholarship_Model');
		$this->CI->load->model('hnumis/academicyears_model');
		
		$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'display_default');
		
		$privilege_items = $this->CI->Scholarship_Model->ListPrivileges();
		$current_term = $this->CI->academicyears_model->current_academic_year();

		switch ($action) {

			case 'display_default':

					$privilege = $this->CI->Scholarship_Model->getScholarshipInfo($privilege_items[0]->id);
					$with_privileges = $this->CI->Scholarship_Model->ListBasicEdStudentsAvailed($privilege_items[0]->id,
							$current_term->id,'N');
					
					$data = array(
							"posted"=>'N',
							"privilege_items"=>$privilege_items,
							"selected_item"=>"",
							"academic_yr"=>$current_term,
							"privilege"=>$privilege,
							"with_privileges"=>$with_privileges,
					);
						
					break;
								
			case 'change_data':
					//print($this->CI->input->post('privilege_items_id')); die();
					$privilege = $this->CI->Scholarship_Model->getScholarshipInfo($this->CI->input->post('privilege_items_id'));
					$with_privileges = $this->CI->Scholarship_Model->ListBasicEdStudentsAvailed($this->CI->input->post('privilege_items_id'),
							$current_term->id,$this->CI->input->post('posted'));
					
					$data = array(
							"posted"=>$this->CI->input->post('posted'),
							"privilege_items"=>$privilege_items,
							"selected_item"=>$this->CI->input->post('privilege_items_id'),
							"academic_yr"=>$current_term,
							"privilege"=>$privilege,
							"with_privileges"=>$with_privileges,
					);
						
					break;

			case 'download_to_pdf':
					$with_priv=json_decode($this->CI->input->post('with_priv'));
					$scholar_desc = $this->CI->input->post('scholar_desc');
					$acad_yr = $this->CI->input->post('acad_yr');
					
					$this->generate_scholars_list_pdf($with_priv,$scholar_desc,$acad_yr);
					return;

		}
		
		$this->CI->content_lib->enqueue_body_content('accounts/list_basic_ed_students_with_privileges', $data);
		$this->CI->content_lib->content();
		
	}
	
	/*
	 * NOTE: this is a temporary module
	 * BY: Genes 12/11/13
	 */
	function list_after_posting() {
		$this->CI->load->model('accounts/accounts_model');
		$this->CI->load->model('academic_terms_model');
		
		$current_term = $this->CI->academic_terms_model->getCurrentAcademicTerm();
		
		$data['students'] = $this->CI->accounts_model->ListStudents_EnrollmentsDeleted_AfterPosting($current_term->id);

		$this->CI->content_lib->enqueue_footer_script('data_tables');
		$this->CI->content_lib->enqueue_body_content('accounts/ListStudents_EnrollmentsDeleted_AfterPosting', $data);
		$this->CI->content_lib->content();
		
	}
	
	//Added: 2/1/2014 by isah
	
	function list_of_students_withdrawals() {
		$this->CI->load->model('accounts/accounts_model');
		$this->CI->load->model('hnumis/Enrollments_model');
	
		$current_term = $this->CI->academic_terms_model->getCurrentAcademicTerm();
	
		$data['students'] = $this->CI->Enrollments_model->list_student_withdrawals($current_term->id);
		print_r($data['students']); die();
	
		$this->CI->content_lib->enqueue_footer_script('data_tables');
		$this->CI->content_lib->enqueue_body_content('accounts/ListStudents_EnrollmentsDeleted_AfterPosting', $data);
		$this->CI->content_lib->content();
	
	}
	
	
	function download_fees_schedule_to_csv(){
		
		$this->CI->load->model('accounts/fees_schedule_model');
		$this->CI->load->model('hnumis/Programs_model');
		$this->CI->load->model('academic_terms_model');
		$this->CI->load->model('hnumis/Academicyears_model');
		$this->CI->load->model('teller/Assessment_model');
				
		$program       = $this->CI->Programs_model->getProgram($this->CI->input->post('acad_programs_id'));
		$acad_programs = $this->CI->fees_schedule_model->ListAcadPrograms_with_fees($this->CI->input->post('academic_terms_id'));
		$acad_term	   = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
		$acad_terms    = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);

		$program_items      = $this->CI->fees_schedule_model->ListProgramItems($this->CI->input->post('academic_terms_id'),$program->acad_program_groups_id);					
		$other_tuition_fees = $this->CI->fees_schedule_model->ListOtherTuition_Fees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('acad_programs_id'));
		$lab_items          = $this->CI->fees_schedule_model->ListLaboratoryItems($acad_term->academic_years_id, $this->CI->input->post('acad_programs_id'));
		
		$filename = "Fees Schedule " .$this->CI->input->post('sy_semester'). " " . $this->CI->input->post('acad_groups'). ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		fputcsv ($output, array('Academic Group: '.$this->CI->input->post('acad_groups')));
		fputcsv ($output, array($this->CI->input->post('description')));
		fputcsv ($output, array($this->CI->input->post('sy_semester')));
		fputcsv ($output, array(''));
		fputcsv ($output, array(''));
		
		// output the column headings
		fputcsv($output, array('Item Group', 'Item', '1st Year', '2nd Year', '3rd Year', '4th Year', '5th Year'));

		$fees_group="";

		foreach ($program_items as $item){
			if ($fees_group != $item->fees_group) {
				$fees_group = $item->fees_group;
				
				fputcsv ($output, array($item->fees_group));
			}
			fputcsv ($output, array(' ',$item->description, $item->y1, $item->y2, $item->y3, $item->y4, $item->y5));
		}
		fputcsv ($output, array(''));
		
		//check if there are Other Tuition Fees items
		if ($other_tuition_fees) {
			fputcsv ($output, array('Other Tuition Fees'));
			foreach ($other_tuition_fees AS $other_fee) {
				fputcsv ($output, array(' ',$other_fee->course_code, $other_fee->y1, $other_fee->y2, $other_fee->y3, $other_fee->y4, $other_fee->y5));
			}
		}
		fputcsv ($output, array(''));
		
		//check if there are laboratory items
		if ($lab_items) {
			fputcsv ($output, array('LABORATORY FEES','RATE'));
			foreach ($lab_items AS $lab_item) {
				fputcsv ($output, array($lab_item->course_code, $lab_item->rate));
			}
		}
		
	}

	function download_fees_schedule_basic_ed_to_csv(){
		$this->CI->load->model('accounts/fees_schedule_model');
		$this->CI->load->model('academic_terms_model');
		$this->CI->load->model('hnumis/Academicyears_model');
		$this->CI->load->model('basic_ed/rsclerk_model');
		$this->CI->load->model('teller/Assessment_model');

		$acad_terms = $this->CI->academic_terms_model->ListAcademicTerms($this->CI->input->post('academic_year_id'));
		$basic_ed_items = $this->CI->fees_schedule_model->ListBasic_Ed_Items($acad_terms[0]->id,$this->CI->input->post('basic_levels_id'));

		$filename = "Fees Schedule Basic Education ".$this->CI->input->post('school_yr')." ".$this->CI->input->post('description'). ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		fputcsv ($output, array($this->CI->input->post('description')));
		fputcsv ($output, array($this->CI->input->post('school_yr')));
		fputcsv ($output, array(''));
		fputcsv ($output, array(''));

		fputcsv($output, array('Item Group', 'Item', '1', '2', '3', '4', '5', '6'));
		
		$fees_group="";

		foreach($basic_ed_items AS $item) {
			if ($fees_group != $item->fees_group) {
				$fees_group = $item->fees_group;
				
				fputcsv ($output, array($item->fees_group));
			}
			fputcsv ($output, array(' ',$item->description, $item->y1, $item->y2, $item->y3, $item->y4, $item->y5, $item->y6));
		}	
		
	}

	/*
	 * ADDED: 8/19/15 by genes
	 */
	private function generate_scholars_list_pdf($with_priv,$scholar_desc,$acad_term,$level=NULL) {
	
		$this->CI->load->library('my_pdf');
		$this->CI->load->library('hnumis_pdf');
		$l = 6;
		
		$this->CI->hnumis_pdf->AliasNbPages();
		$this->CI->hnumis_pdf->set_HeaderTitle($scholar_desc.": ".$acad_term);
		$this->CI->hnumis_pdf->AddPage('P','letter');
		
		$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->CI->hnumis_pdf->SetDrawColor(165,165,165);
			
		$this->CI->hnumis_pdf->SetFont('times','B',11);
		$this->CI->hnumis_pdf->SetTextColor(10,10,10);
		$this->CI->hnumis_pdf->SetFillColor(190,190,190);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->SetX(12);
		
		if ($level == 'College') {
			$header1 = array('Cnt','ID No.','Name','Course/Year','Discount','Total');
		} else {
			$header1 = array('Cnt','ID No.','Name','Level-Year','Discount','Total');
		}

		$w1 = array(11,22,60,50,25,25);

		for($i=0;$i<count($w1);$i++)
			$this->CI->hnumis_pdf->Cell($w1[$i],8,$header1[$i],1,0,'C',true);

		$this->CI->hnumis_pdf->SetFont('Arial','',10);
		$this->CI->hnumis_pdf->SetTextColor(0,0,0);
		$this->CI->hnumis_pdf->SetFillColor(255,255,255);
		$cnt=0;	
		foreach($with_priv AS $priv) {
			$cnt++;
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->SetX(12);
			$this->CI->hnumis_pdf->Cell($w1[0],$l,$cnt.'.',1,0,'R',true);
			$this->CI->hnumis_pdf->Cell($w1[1],$l,$priv->students_idno,1,0,'C',true);
			$this->CI->hnumis_pdf->Cell($w1[2],$l,utf8_decode($priv->lname).", ".utf8_decode($priv->fname),1,0,'L',true);
			if ($level == 'College') {
				$this->CI->hnumis_pdf->Cell($w1[3],$l,$priv->abbreviation."-".$priv->year_level,1,0,'L',true);
			} else {
				$this->CI->hnumis_pdf->Cell($w1[3],$l,$priv->stud_level,1,0,'L',true);
			}
			$this->CI->hnumis_pdf->Cell($w1[4],$l,($priv->discount_percentage * 100)."%",1,0,'R',true);
			$this->CI->hnumis_pdf->Cell($w1[4],$l,number_format($priv->discount_amount,2),1,0,'R',true);
			
		}
			
		$this->CI->hnumis_pdf->Output();
		
			
	}

	function download_scholars_list_to_csv($with_priv,$scholar_desc,$acad_term,$level=NULL){		

		$filename = "Scholars_List_".$scholar_desc."_".$acad_term.".csv";
		$filename = str_replace(" ","_",$filename);

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output =  fopen('php://output', 'w');
	
		fputcsv($output, array("Privilege:",$scholar_desc));
		fputcsv($output, array("Term:",$acad_term));
		fputcsv($output, array(''));
		fputcsv($output, array('Count', 'ID No', 'Name', 'Course/Year', 'Discount', 'Total'));
		
		$_count = 1;
		foreach($with_priv AS $row) {
			$name = $row->lname.', '.$row->fname.' '.$row->mname;
			fputcsv ($output, array($_count++,
				                    $row->students_idno,
				                    $name,
				                    $row->abbreviation.' '.$row->year_level,
				                    ($row->discount_percentage*100)."%",
				                    $row->discount_amount));
		}	
		
	}

}	