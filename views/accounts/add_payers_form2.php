<?php
$this->form_lib->set_id('new_payers');
$this->form_lib->set_attributes(array('method'=>'post','action'=>'#','class'=>'form-horizontal'));
$this->form_lib->enqueue_hidden_input('nonce', $this->common->nonce());
$this->form_lib->enqueue_hidden_input('action', 'add_account', array('id'=>'the-action'));
$this->form_lib->enqueue_text_input('accounts_name','Accounts Name','','',TRUE);
$this->form_lib->enqueue_select_input('inhouse',$this->config->item('payers'),'Type');
$this->form_lib->enqueue_select_input('type',$this->config->item('payers'),'Type');
$this->form_lib->set_submit_button('Add','btn btn-success');

?>

<!-- h2 class="heading">Add Payers:</h2-->
<?php echo $this->form_lib->content() ?>