<?php
//print_r($error);die();
?>

<style>
.well-error {color: #b94a48;background-color: #f2dede;border-color: #eed3d7;}
</style>
<div class="row-fluid">
	<div class="span12">
		<h2 class="heading">College Enrollment (<?php echo $scheduled_academic_term->term . " SY " . $scheduled_academic_term->sy; ?>)</h2>
	</div>
</div>
<div class="row-fluid">
<?php if($error=='SCHEDULED_ACADEMIC_TERM_NOT_MATCHING_BUT_ENOUGH_CREDIT'): ?>
	<div class="span9">
		<div class="well">
<!-- The code below has been changed   :  Toyet 3.28.2018 
	 as per order by the finance office:
			<p>You are about to enroll to the <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> for school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. Do you want to proceed? If you wish to continue click the accept button.</p>
			<form method="post">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="action" value="insert_scheduled_academic_term" >
				<button type="submit" class="btn btn-primary">Accept</button>
			</form>
-->
			<p><b>IMPORTANT!</b><br>You have availed of your excess balance as COLLEGE REGISTRATION.  Please proceed to the Accounts Clerk to enable your enrollment.  Thank you.</p>
		</div>
	</div>
<?php elseif($error=='BLOCKED_FOR_ENROLLMENT'): ?>
	<div class="span9">
		<div class="well">
			<p>You are <b>TEMPORARILY BLOCKED</b> for enrollment.  Please visit the Students Affairs Office.  Thank you.</p>
		</div>
	</div>
<?php elseif($error=='SCHEDULED_ACADEMIC_TERM_NOT_MATCHING_NOT_ENOUGH_CREDIT'): ?>
	<div class="span9">
		<div class="well">
			<p>You are currently NOT allowed to access the Enrollment Panel for <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> of school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. Please settle your account first and/or pay the <strong>College Registration</strong> fee to the Teller.</p>
		</div>
	</div>
<?php elseif($error=='HAS_CURRENT_TERM_BUT_NOT_ON_SCHEDULE'): ?>
	<div class="span9">
		<div class="well">
			<p>You are currently NOT allowed to access the Enrollment Panel for <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> of school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. The date today is NOT included in the current enrollment schedule for your course or year level. You may wait for the enrollment schedule to come or you may ask the DRIC (EDP) for an enrollment extension.</p>
		</div>
	</div>
<?php elseif($error=='HAS_CURRENT_TERM_BUT_NOT_CAN_ENROLL'): ?>
	<div class="span9">
		<div class="well">
			<p>You are currently NOT allowed to access the Enrollment Panel for <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> of school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. Please pay College Registration to the teller.</p>
		</div>
	</div>
<?php elseif($error=='HAS_CURRENT_TERM_BUT_NOT_CAN_ENROLL_AND_WITH_LEDGER_BALANCE'): ?>
	<div class="span9">
		<div class="well">
			<p>You are currently NOT allowed to access the Enrollment Panel for <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> of school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. Please pay your <?php echo  isset($balance)? '(P '. number_format($balance,2) .') ':''; ?>balance to the teller.</p>
		</div>
	</div>
<?php elseif($error=='NOT_ON_SCHEDULE_BUT_HAS_ACADEMIC_TERM'): ?>
	<div class="span9">
		<div class="well">
			<p>You are currently NOT allowed to access the Enrollment Panel for <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> of school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. We are past the Enrollment Schedule. If you want to add to your enrolled courses, request for extension from the DRIC (EDP).</p>
		</div>
	</div>
<?php elseif($error=='NOT_ON_SCHEDULE_NO_ACADEMIC_TERM'): ?>
	<div class="span9">
		<div class="well">
			<p>You are currently NOT allowed to access the Enrollment Panel for <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> of school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. We are past the Enrollment Schedule. If you want to enroll to this term, please pay College Registration and ask for extension from the DRIC (EDP).</p>
		</div>
	</div>
<?php elseif($error=='PAID_CR_BUT_CAN_NOT_ENROLL'): ?>
	<div class="span9">
		<div class="well">
			<p>You are currently NOT allowed to access the Enrollment Panel for <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> of school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. The date today is NOT included in the current enrollment schedule for your year level.</p>
		</div>
	</div>	
<?php elseif($error=='SCHEDULED_ACADEMIC_TERM_MATCHING_BUT_ENOUGH_CREDIT'): ?>
	<div class="span9">
		<div class="well">
			<p>You are about to enroll to the <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> for school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. Do you want to proceed? If you wish to continue click the accept button.</p>
			<form method="post">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="action" value="set_can_enroll_to_yes" >
				<button type="submit" class="btn btn-primary">Accept</button>
			</form>
		</div>
	</div>	
<?php endif; ?>
</div>