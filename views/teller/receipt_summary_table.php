<?php
$my_records = json_encode($records);
$my_payment_methods=json_encode($payment_methods);
?>
<style>
.void {color: #ccc;}
td.right-align{text-align:right;}
td.money{text-align:right;}
</style>
<div class="row-fluid">
	<div class="span12">
	<ul class="nav nav-tabs">
		<?php $count = 0; if($this->session->userdata('role')=='cashier'): ?>
		<li class="active"><a data-toggle="tab" href="#tab-summary" >Summary</a></li>
		<?php $count++; endif; ?>
	<?php foreach($records as $machine_ip => $value) : $count++?>
		<li <?php echo ($count==1 ? ' class="active"' : ''); ?>><a data-toggle="tab" href="#tab-<?php echo $count; ?>" >Machine: <?php echo $machine_ip; ?></a></li>
	<?php endforeach; ?>
	</ul>
<div class="tab-content">
	<?php $count = 0;  if($this->session->userdata('role')=='cashier'): ?>
	<div class="tab-pane active" id="tab-summary">
		<table class="table table-bordered" style="width:30%">
			<thead>
				<tr>
					<th colspan="2">Summary</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Date</td>
					<td class="right-align"><?php echo $date; ?></td>
				</tr>
				<tr>
					<td>Total Gross</td>
					<td class="right-align"><?php echo number_format($total_gross, 2); ?></td>
				</tr>
				<tr>
					<td>Total Voided</td>
					<td class="right-align"><?php echo number_format($total_void, 2); ?></td>
				</tr>
				<tr>
					<td>Total Net</td>
					<td class="right-align"><?php echo number_format($total_net, 2); ?></td>
				</tr>
			</tbody>
		</table>
		<table class="table table-bordered" style="margin-top:20px">
			<thead>
				<tr>
					<th>Machine Number</th>
					<th>NON-VAT Receipt Range</th>
					<th>VAT Receipt Range</th>
					<th>All Transactions Gross</th>
					<th>Voided Transactions</th>
					<th>Net Transactions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($summary as $ip => $val):?>
				<tr>
					<td><?php echo $ip; ?></td>
					<td><?php echo (empty($val['non-vat']) ? '' : (implode(', ', range_of_arrays($val['non-vat'])))); ?></td>
					<td><?php echo (empty($val['vat']) > 0 ? '' : (implode(', ', range_of_arrays($val['vat'])))); ?></td>
					<td class="money"><?php echo number_format((isset($gross_transaction_totals[$ip]) ? $gross_transaction_totals[$ip] : 0), 2); ?></td>
					<td class="money"><?php echo number_format((isset($void_transaction_totals[$ip]) ? $void_transaction_totals[$ip] : 0), 2); ?></td>
					<td class="money"><?php echo number_format((isset($net_transaction_totals[$ip]) ? $net_transaction_totals[$ip] : 0), 2); ?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php $count++; endif; ?>
<?php foreach ($records as $machine_ip => $value) : $count++; ?>
	<div class="tab-pane <?php echo ($count==1 ? 'active' : ''); ?>" id="tab-<?php echo $count; ?>">
		<h3 class="heading">Machine No: <?php echo $machine_ip; ?></h3>
		<table class="table table-bordered" style="max-width:400px">
			<thead>
				<tr>
					<th colspan="2">Payment Methods Summary</th>
				</tr>
				<tr>
					<th>Payment Method</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
<?php $machine_total = 0; ?>
<?php foreach($payment_methods[$machine_ip] as $method=>$total): ?>
		<tr>
			<td><?php echo $method; ?></td>
			<td class="right-align"><?php echo number_format($total, 2); $machine_total+=$total; ?>
		</tr>
<?php endforeach; ?>
		<tr>
			<td class="right-align">Total</td>
			<td class="right-align"><strong><?php echo number_format($machine_total, 2); ?></strong></td>
	</tbody>
</table>
<table class="table-header table table-bordered" style="margin-top: 20px">
	<thead style="visibility:visible">
		<tr>
			<th>OR Number</th>
			<th>OR Date</th>
			<th>Teller ID</th>
			<th>ID Number </th>
			<th>Payor</th>
			<th>Code</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($value as $teller_code => $transactions): ?>
		<tr><td colspan="7"><?php echo $teller_code; ?> (<?php echo $transactions['name']; ?>)</td></tr>
<?php $code_amount_total = 0; ?>
<?php foreach ($transactions['transactions'] as $transaction): ?>
		<tr <?php echo ($transaction->status=='void' ? 'class="void"' : ""); ?>>
			<td><?php echo $transaction->receipt_no; ?></td>
			<td><?php echo $transaction->transaction_date; ?><?php echo ($transaction->status=='void' ? ' (VOIDED)' : ""); ?></td>
			<td><?php echo $transaction->teller; ?></td>
			<td><?php echo $transaction->payor_id; ?></td>
			<td><?php echo $transaction->payor; ?></td>
			<td><?php echo $transaction->teller_code; ?></td>
			<td class="right-align"><?php echo number_format($transaction->whole_amount, 2); $code_amount_total+=(!in_array($transaction->status, array('void', 'deleted')) ? $transaction->whole_amount : 0); ?></td>
		</tr>
<?php endforeach; ?>
		<tr>
			<td colspan="5">&nbsp;</td>
			<td><?php echo $teller_code; ?> Sub Total:</td>
			<td style="text-align:right"><strong><?php echo number_format($code_amount_total, 2); ?></strong></td>
		</tr>
<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php endforeach; ?>
		</div>
	</div>
</div>
<script>
function print(content) {
    var dapplet = document.jzebra;
    if (dapplet != null) {
    	dapplet.append('\x1B\x40');
       dapplet.append(content);
       dapplet.print();
	}
}
$(document).ready(function(){
	 $(".table-header").stickyTableHeaders();
	 $('.table-header').stickyTableHeaders('updateWidth');
	
   $('.download_receipt_summary').click(function(event){
		event.preventDefault(); 
		var records = $(this).attr('records');
		var payment_methods = $(this).attr('payment_methods');
						
		$('<input>').attr({
			type: 'hidden',
			name: 'records',
			value: records,
		}).appendTo('#download_receipt_summary_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'payment_methods',
			value: payment_methods,
		}).appendTo('#download_receipt_summary_form');
		$('#download_receipt_summary_form').submit();
		
	});
   $('#print_summary').click(function(){
		var content_to_print = $('#daily_report_print').text();
		var applet = document.jzebra;
		if (applet != null) {
			// Searches for default printer
			applet.findPrinter();
		}
		print(content_to_print);
	});
});
</script>


