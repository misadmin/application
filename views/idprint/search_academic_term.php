<div style="float:left; width:auto; margin-top:10px">
<form action="" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="2" />
	
	
<div class="formSep">
	<h3>Choose a Term</h3>
</div> 
	<fieldset>
		<div class="control-group formSep">
			<label class="control-label"> <b> Academic Term: </b> </label>
				<div class="controls">
					<select id="academic_terms" name="academic_terms">
					<?php
						foreach($academic_terms AS $academic_term) {
							print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");	
						}
					?>
					</select>
				</div>
		</div>
	
		<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">View</button>
					</div>
		</div>	
	</fieldset>
</form>

