<?php 
//require_once(DIRNAME(BASEPATH).'/application/third_party/tcpdf/config/lang/eng.php');
require_once(DIRNAME(BASEPATH).'/application/third_party/tcpdf/tcpdf.php');

class MYPDF extends TCPDF {
	public $header_image;
	public $header_summary;

	public function setHeaderImage($img){
		$this->header_image = $img;
	}

	public function setHeaderSummary($summary){
		$this->header_summary = $summary;
	}
	//Page header
	public function Header() {
		
	}

	// Page footer
	public function Footer() {
		$this->SetFont('helvetica', '', 9);
		$this->SetTextColorArray($this->footer_text_color);
		//set style for cell border
		$line_width = (0.85 / $this->k);
		$this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));

		$w_page = isset($this->l['w_page']) ? $this->l['w_page'].' ' : '';
		$pagenumtxt = $w_page.$this->getAliasNumPage().' of '.$this->getAliasNbPages();

		$this->SetY(-18);
		$date = date( 'd-m-Y h:ia' );
		//Print page number
		$this->Cell(100, 8, $this->getAliasRightShift().$pagenumtxt, 'T', 0, 'R');
		$this->Cell(0, 8, "Printed  ".$date, 'T', 0, 'R');
	}
	
	public function format_content ($student_courses){
		$content = '<style>
table.content td{
	border: 1px solid #AAAAAA;
}
table.content td.label{
	font-weight:bold;
	text-align:right;
}
tr.head td {
	border: 1px solid #AAAAAA;				
}
</style>'; 
		$content .= '<table class="container" cellpadding="5" width="100%"><tr class="head"><td width="5%">#</td><td width="10%">Student ID</td><td width="45%">Student</td><td width="10%">Gender</td><td width="20%">Subjects Enrolled</td><td width="10%">Units</td></tr>';
		if (count($student_courses) > 0){
			$student_count = 1;
			foreach ($student_courses as $key => $student_course){
				$course_count = 1;
				foreach($student_course['courses'] as $course) {
					if($course_count==1)
						$content .= "<tr class=\"first\"><td>" . $student_count . "</td><td>" . $student_course['info']->idno . "</td><td>" . substr($student_course['info']->fullname, 0, 50) . "</td><td>" . ($student_course['info']->gender == "M" ? "  Male" : ($student_course['info']->gender == "F" ? "Female" : "      ") ) . "</td><td>" . substr($course->course, 0, 24). "</td><td>" . ($course->is_bracketed == "Y" ? "({$course->credit_units})" : $course->credit_units) . "</td></tr>\n" ;
					else
						$content .= '<tr class=\"succeeding\"><td colspan="4">&nbsp;</td><td>' . substr($course->course, 0, 24) . "</td><td>" . ($course->is_bracketed == "Y" ? "({$course->credit_units})" : $course->credit_units) . "</td></tr>\n" ;
					$course_count++;
				}
				$student_count++;
			}
		} else {
			
		}
		$content .= "</table>";
		return $content;
	}
	
	public function set_head($sy, $academic_term, $academic_program, $year_level){
		$lines =  "<h2>ENROLLMENT LIST</h2><br />";
		$lines .= "Name of Institution: <strong>Holy Name University</strong><br />\n";
		$lines .= "Address: <strong>Tagbilaran City</strong> Tel. No. <strong>(038)4123432, (038)4123763, (038)5019817 </strong><br />\n";
		$lines .= "Institutional Identifier: <strong>07040</strong><br />\n";
		$lines .= "SY and Sem/Tri: <strong>{$sy}  [{$academic_term}]</strong><br />\n";
		$lines .= "Course/Program: <strong>{$academic_program}</strong><br />\n";
		$lines .= "Year Level: <strong>{$year_level}</strong><br />\n";
		return $lines;
	}
}
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetTopMargin(35);
//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);
$pdf->SetFont('helvetica', '', 9);
$pdf->AddPage();
$pdf->writeHTML($pdf->set_head($school_year, $academic_term, $academic_program, $year_level));
$pdf->writeHTML($pdf->format_content($student_courses));//, true, false, false, false, '');
//echo $pdf->format_content($student_courses);
$pdf->Output('enrollment_list.pdf', 'I');