<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; margin-bottom:10px;">
 <h2 class="heading">Tuition Fees</h2>
  <table align="left" cellpadding="2" cellspacing="0" style="width:35%; border:solid; border-width:1px; border-color:#060; margin-top:10px; margin-left:20px;">
<tr>
        	<td width="199">Academic Year</td>
        	<td width="15">:</td>
            <td width="438"><?php print($academic_year->start_year."-".$academic_year->end_year); ?></td>
    </tr>
<tr>
  <td>College</td>
  <td>:</td>
  <td><?php print($college->name); ?></td>
</tr>
  </table>
  <br />
  <br />
<br />
</div>
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; margin-top:50px; ">
  <table width="30%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:30%; padding:0px; margin:30px;">
  <thead>
    <tr>
      <td width="40%" align="center">Programs</td>
      <td width="30%" align="center">Year Level</td>
      <td width="30%" align="center">Rate</td>
    </tr>
</thead>	
    <?php
		if ($fees) {
			foreach ($fees AS $fee) {
	?>
    <tr>
      <td align="left" valign="top"><?php print($fee->abbreviation); ?></td>
      <td><?php print($fee->yr_level); ?></td>
     <?php
	 	if($fee->posted =='N'){ 	
	?>	
	 	 <td align="left" valign="top"><?php print("<a class=\"rate\" href=\"{$fee->id}\"> {$fee->rate} </a>"); ?></td>
	<?php
	 	}
	  else{
	?>
		<td align="left" valign="top"> <?php print($fee->rate); ?></td>
	<?php	
		}
	?>		 
    </tr>
    <?php
			}
		}
	?>
  </table>
    </div>
  <form id="srcform2" action="<?php echo site_url("accounts/list_tuitionfees")?>" method="post">
  <input type="hidden" name="step" value="3" />
</form>

  
<script>
	$('.rate').click(function(event){
		event.preventDefault(); 
		var  fee_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'id',
		    value: fee_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>


