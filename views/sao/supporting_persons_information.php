<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='supporting_persons') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?> 		
		<form id="supporting_persons_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_supporting_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			 	<h4 class="heading">Persons Supporting Education aside from parents<small class="pull-right"><a href="javascript://" id="add_supporting_person" title="Click to add Supporting Person" />+ Add Supporting Person</a></small></h4>
			 <fieldset id="supporting_information">
			 	<div id="supporting_information_data">
<?php if(isset($support_information) && is_array($support_information)) : ?>
<?php foreach($support_information as $key => $val): ?>
					<h5 class="heading">Supporting Person Information<small class="pull-right"><a href="javascript://" id="delete_<?php echo $key; ?>" class="delete_support">Delete</a></small></h5>
					<div class="control-group formSep">
						<label for="supporting_information_name[<?php echo $key; ?>]" class="control-label">Name: </label>
						<div class="controls">
							<input type="text" id="supporting_information_name[<?php echo $key; ?>]" name="supporting_information_name[<?php echo $key; ?>]" class="input-xlarge" value="<?php if( isset($val['name']) && ! empty($val['name'])) echo $val['name']; ?>" />
						</div>
					</div>
					<div class="control-group formSep">
						<label for="supporting_information_occupation[<?php echo $key; ?>]" class="control-label">Occupation: </label>
						<div class="controls">
							<input type="text" id="supporting_information_occupation[<?php echo $key; ?>]" name="supporting_information_occupation[<?php echo $key; ?>]" class="input-xlarge" value="<?php if( isset($val['occupation']) && ! empty($val['occupation'])) echo $val['occupation']; ?>" />
						</div>
					</div>
					<div class="control-group formSep">
						<label for="supporting_information_relation[<?php echo $key; ?>]" class="control-label">Relationship: </label>
						<div class="controls">
							<input type="text" id="supporting_information_relation[<?php echo $key; ?>]" name="supporting_information_relation[<?php echo $key; ?>]" class="input-xlarge" value="<?php if( isset($val['relation']) && ! empty($val['relation'])) echo $val['relation']; ?>" />
						</div>
					</div>
					<div class="control-group formSep">
						<label for="supporting_information_address[<?php echo $key; ?>]" class="control-label">Address: </label>
						<div class="controls">
							<input type="text" id="supporting_information_address[<?php echo $key; ?>]" name="supporting_information_address[<?php echo $key; ?>]" class="input-xlarge" value="<?php if( isset($val['address']) && ! empty($val['address'])) echo $val['address']; ?>" />
						</div>
					</div>
<?php endforeach; ?>
<?php endif; ?>
				</div>
				<div class="control-group">
					<div class="controls">
						<button id="update_educational_information" class="btn btn-primary" type="submit">Update Support Information</button>
					</div>
				</div>
			</fieldset>
		</form>
		<form method="post" action="" id="delete_support_form">
			<input type="hidden" name="action" value="delete_support_information" />
<?php echo $this->common->hidden_input_nonce(); ?>			
		</form>
	</div>
</div>
<?php $support_count = (isset($key) ? ($key + 1) : 0);?>
<script>
var support_count = <?php echo $support_count; ?>;
var control_html = '<h5 class="heading">Supporting Person Information</h5><div class="control-group formSep"><label for="supporting_information_name[]" class="control-label">Name: </label><div class="controls"><input type="text" id="supporting_information_name[]" name="supporting_information_name[]" class="input-xlarge" value="" /></div></div><div class="control-group formSep"><label for="supporting_information_occupation[]" class="control-label">Occupation: </label><div class="controls">	<input type="text" id="supporting_information_occupation[]" name="supporting_information_occupation[]" class="input-xlarge" value="" /></div></div><div class="control-group formSep"><label for="supporting_information_relation[]" class="control-label">Relationship: </label><div class="controls">	<input type="text" id="supporting_information_relation[]" name="supporting_information_relation[]" class="input-xlarge" value="" /></div></div><div class="control-group formSep"><label for="supporting_information_address[]" class="control-label">Address: </label><div class="controls">	<input type="text" id="supporting_information_address[]" name="supporting_information_address[]" class="input-xlarge" value="" /></div></div>'

$('#add_supporting_person').click(function(){
	var new_control_html = str_replace('[]', '[' + support_count + ']', control_html);
	$('#supporting_information_data').append(new_control_html);
	$('#supporting_information_name[' + support_count + ']').focus();
	support_count = support_count + 1;
});

$('.delete_support').click(function(){
	var continue_this = confirm('Are you sure you want to delete this?');

	if (continue_this){
		var id = $(this).attr('id').substr(7);

		$('<input>').attr({
		    type: 'hidden',
		    name: 'id',
		    value: id,
		}).appendTo('#delete_support_form');
		$('#delete_support_form').submit();
	}	
});

function str_replace (search, replace, subject, count) {
  var i = 0,
    j = 0,
    temp = '',
    repl = '',
    sl = 0,
    fl = 0,
    f = [].concat(search),
    r = [].concat(replace),
    s = subject,
    ra = Object.prototype.toString.call(r) === '[object Array]',
    sa = Object.prototype.toString.call(s) === '[object Array]';
  s = [].concat(s);
  if (count) {
    this.window[count] = 0;
  }

  for (i = 0, sl = s.length; i < sl; i++) {
    if (s[i] === '') {
      continue;
    }
    for (j = 0, fl = f.length; j < fl; j++) {
      temp = s[i] + '';
      repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
      s[i] = (temp).split(f[j]).join(repl);
      if (count && s[i] !== temp) {
        this.window[count] += (temp.length - s[i].length) / f[j].length;
      }
    }
  }
  return sa ? s : s[0];
}
</script>