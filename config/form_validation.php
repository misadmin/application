<?php

$config = array(
		'create_course' => array(
				array(
						'field' => 'course_code',
						'label' => 'Course Code',
						'rules' => 'required|is_unique[courses.course_code]'
				),
				array(
						'field' => 'descriptive_title',
						'label' => 'Descriptive Title',
						'rules' => 'required'
				),
				array(
						'field' => 'credit_units',
						'label' => 'Credit Units',
						'rules' => 'required'
				),
				array(
						'field' => 'paying_units',
						'label' => 'Paying Units',
						'rules' => 'required'
				),
				array(
						'field' => 'type_id',
						'label' => 'Course Type',
						'rules' => 'required'
				),
				array(
						'field' => 'group_id',
						'label' => 'Course Group',
						'rules' => 'required'
				)
		),
);