			<div style="float:left; padding:3px; overflow:auto; width:100%; ">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:10%;">Course<br>Offerings<br>ID</td>
								<td class="shs_header" style="width:10%;">Course<br>Offerings<br>Slots ID</td>
								<td class="shs_header" style="width:10%;">Block<br>Sections<br>ID</td>
								<td class="shs_header" style="width:25%;">Schedule</td>
								<td class="shs_header" style="width:10%;">Course<br>Code</td>
								<td class="shs_header" style="width:10%;">Abbreviation</td>
								<td class="shs_header" style="width:15%;">Section Name</td>
								<td class="shs_header" style="width:5%;">Fix</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($same_offerings) {
									foreach($same_offerings AS $same_offer) {
							?>
							<tr>
									<td style="text-align:center;"><?php print($same_offer->id); ?></td>
									<td style="text-align:center;"><?php print($same_offer->course_offerings_slots_id); ?></td>
									<td style="text-align:center;"><?php print($same_offer->block_sections_id); ?></td>
									<td><?php print(nl2br($same_offer->sched)); ?></td>
									<td style="text-align:center;"><?php print($same_offer->course_code); ?></td>
									<td style="text-align:center;"><?php print($same_offer->abbreviation); ?></td>
									<td style="text-align:center;"><?php print($same_offer->section_name); ?></td>
									<td style="text-align:center;">
										<a href="<?php print($same_offer->course_offerings_slots_id); ?>" 
											new_offerings_id="<?php print($new_offerings_id); ?>"
											block_sections_id="<?php print($same_offer->block_sections_id); ?>"
											academic_terms_id="<?php print($same_offer->academic_terms_id); ?>"
											courses_id="<?php print($same_offer->courses_id); ?>"
											class="update_course_offerings" >
											<div id="show_wrench_icon_<?php print($same_offer->course_offerings_slots_id); ?>" >
												<i class="icon-wrench"></i>
											</div>
										</a>
									</td>
									<td style="text-align:center;">
										<a href="<?php print($same_offer->id); ?>" 
											course_offerings_slots_id="<?php print($same_offer->course_offerings_slots_id); ?>"
											block_sections_id="<?php print($same_offer->block_sections_id); ?>"
											academic_terms_id="<?php print($same_offer->academic_terms_id); ?>"
											courses_id="<?php print($same_offer->courses_id); ?>"
											class="del_course_offerings" >
											<div id="show_trash_icon_<?php print($same_offer->id); ?>" >
												<i class="icon-trash"></i>
											</div>
										</a>
									</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>

			
<script>
	$('.del_course_offerings').click(function(event){
		event.preventDefault(); 
			
		var course_offerings_id       = $(this).attr('href');
		var course_offerings_slots_id = $(this).attr('course_offerings_slots_id');
		var block_sections_id         = $(this).attr('block_sections_id');
		var academic_terms_id         = $(this).attr('academic_terms_id');
		var courses_id                = $(this).attr('courses_id');

		$('#show_trash_icon_'+course_offerings_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
		$.ajax({
			cache: false,
			type: 'POST',
			url: '<?php echo site_url($this->uri->segment(1).'/shs_fix');?>',
			data: { "course_offerings_slots_id": course_offerings_slots_id,
					"course_offerings_id": course_offerings_id,
					"block_sections_id": block_sections_id,
					"academic_terms_id": academic_terms_id,
					"courses_id": courses_id,
					"action": 'delete_course_offering'},
			dataType: 'json',
			success: function(response) {				
				if (!response.msg) {
					$('#show_error_msg').html('Cannot be removed because Strands are assigned!'); 							
					$('#show_trash_icon_'+course_offerings_id).html("<i class='icon-trash'></i>")
				} else {
					$('#show_error_msg').html(''); 	
					$('#show_extracted_items').html(response.output); 	
				}					
			}
		});
		
	});
</script>



<script>
	$('.update_course_offerings').click(function(event){
		event.preventDefault(); 
			
		var course_offerings_slots_id = $(this).attr('href');
		var new_offerings_id          = $(this).attr('new_offerings_id');
		var block_sections_id         = $(this).attr('block_sections_id');
		var academic_terms_id         = $(this).attr('academic_terms_id');
		var courses_id                = $(this).attr('courses_id');

		console.log();
		
		$('#show_wrench_icon_'+course_offerings_slots_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
		$.ajax({
			cache: false,
			type: 'POST',
			url: '<?php echo site_url($this->uri->segment(1).'/shs_fix');?>',
			data: { "course_offerings_slots_id": course_offerings_slots_id,
					"new_offerings_id": new_offerings_id,
					"block_sections_id": block_sections_id,
					"academic_terms_id": academic_terms_id,
					"courses_id": courses_id,
					"action": 'update_course_offerings'},
			dataType: 'json',
			success: function(response) {				
			
				$('#show_list_of_offerings').html(response.output); 

			}
		});
		
	});
</script>
			