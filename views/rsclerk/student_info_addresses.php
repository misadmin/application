<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='2') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
		<form id="student_address_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_address_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<div class="control-group formSep">
					<label for="place_of_birth" class="control-label">Place of Birth:</label>
					<div class="controls" id="place_of_birth_controls">
<?php if (isset($place_of_birth)): ?>
						<div style="margin-top:6px" id="place_of_birth_control_text">
							<strong><?php echo $place_of_birth; ?></strong>&nbsp;
							<small><a href="javascript://" id="place_of_birth_control" class="edit_toggle" >Edit</a></small>
						</div>
<?php endif; ?>
						<div id="place_of_birth_control_controls"></div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="home_address" class="control-label">Home Address:</label>
					<div class="controls" id="home_address_controls">
<?php if (isset($home_address)): ?>
						<div style="margin-top:6px" id="home_address_control_text"><strong><?php echo $home_address; ?></strong>&nbsp;<small><a href="javascript://" id="home_address_control"  class="edit_toggle" >Edit</a></small></div>
<?php endif; ?>
						<div id="home_address_control_controls"></div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="city_address" class="control-label">City Address(if renting/boarding):</label>
					<div class="controls" id="city_address_controls">
<?php if (isset($city_address)): ?>
						<div style="margin-top:6px" id="city_address_control_text"><strong><?php echo $city_address; ?></strong>&nbsp;<small><a href="javascript://" id="city_address_control"  class="edit_toggle" >Edit</a></small></div>
<?php endif; ?>
						<div id="city_address_control_controls"></div>
					</div>
				</div>
				<!-- <div class="control-group formSep">
					<label for="where_staying" class="control-label">Staying in Tagbilaran</label>
					<div class="controls">
						<select name="stay_in_city" id="stay_in_city">
<?php foreach ($this->config->item('stay_in_city') as $key => $val): ?>
							<option value="<?php echo $key; ?>" <?php if($key == $stay_with) echo ' selected="selected"'; ?>><?php echo $val; ?></option>
<?php endforeach; ?>
						</select>
					</div>
				</div> -->
				<div class="control-group">
					<div class="controls">
						<button id="update_address_information" class="btn btn-primary" type="submit">Update Address Information</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
