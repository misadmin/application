
<div style="float:left; width:auto; margin-top:10px">
<form action="" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="new_student" />
	
	
<div class="formSep">
	<h2>Add New Student</h2>
</div> 

<fieldset>
	<div class="control-group formSep">
		<label class="control-label">Family Name</label>
		<div class="controls">
			<input type="text" name="familyname" placeholder="Family Name" autofocus="autofocus" />
		</div>
	</div>
	
	<div class="control-group formSep">
		<label class="control-label">First Name</label>
		<div class="controls">
			<input type="text" name="firstname" placeholder="First Name" />
		</div>
	</div>
	
	<div class="control-group formSep">
		<label class="control-label">Middle Name</label>
		<div class="controls">
			<input type="text" name="middlename" placeholder="Middle Name" />
		</div>
	</div>
	
	<div class="control-group formSep">
		<label class="control-label">Gender</label>
		<div class="controls">
			<select name="gender">
				<option value="">--Select Gender--</option>
				<option value="M">Male</option>
				<option value="F">Female</option>
			</select>
		</div>
	</div>
	
	<div class="control-group formSep">
		<label class="control-label">Enroll For</label>
		<div class="controls">
		   <select name="academic_year" >
				<?php 
					foreach ($academic_years as $academic_year){
				?>
						<option value="<?php print($academic_year->id); ?>" >
						<?php print($academic_year->sy); ?></option>
				<?php 
					}
				?>
			</select>
	    </div>
	</div>

	<?php
		$data = array("button"=>"Add Student!");
		$this->load->view("basic_ed/includes/select_yr_and_level",$data);
	?>
	
</fieldset>
</form>

