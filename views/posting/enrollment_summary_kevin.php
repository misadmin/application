<h2 class="heading">College Enrollment Summary</h2>
<h3><?php echo $term->term." ".$term->sy?></h3>
<div class="float-right" style="text-align:left; padding:8px 0px;">
<a class="download_enroll" href="" academic_terms_id="<?php print($term->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>
<style>
#table-summary thead th {
 text-align:center;
 width:3%;
}
td.cgrp {
	cursor:pointer;
}
</style>
<script>
function toggle_table(a){
	if (a) {
		$('#table-summary thead tr:nth-child(2),#table-summary tbody td:nth-child(3n+2),#table-summary tbody td:nth-child(3n+3)').hide();
		$('#table-summary thead tr:nth-child(1) th:nth-child(n+2)').attr('colspan', '1');
		$('.cgrp').attr('colspan', '7');
		
	}
	else {
		$('#table-summary thead tr:nth-child(2),#table-summary tbody td:nth-child(3n+2),#table-summary tbody td:nth-child(3n+3)').show();
		$('#table-summary thead tr:nth-child(1) th:nth-child(n+2)').attr('colspan', '3');
		$('.cgrp').attr('colspan', '19');
	}
}
$().ready( function(){
	var collapsed = true;
	$('#table-summary thead th').bind('click', function() {
			toggle_table(collapsed);
			collapsed ^= true;
		});
	$('td.cgrp').bind('click', function(){
		var displayed = $(this).data('displayed');
		if (displayed) {
			$(this).children('span').text('Expand +');
		}
		else {
			$(this).children('span').text('Collapse -');
		}
		$(this).data('displayed',!displayed);
		$(this).parent().parent().children('tr').each(function( index ) {
			  if (index > 0){
				  if (displayed){
					$(this).hide();
				  }
				  else {
					$(this).show();
				  }
			  }
		});
	});
	$('#table-summary').children('tbody').each(function( index ) {
		$(this).children('tr').each(function( index ) {
		  if (index > 0){
			$(this).hide();
		  }
		});
	});
});
</script>
 <table width="100%" border="0" cellspacing="0" cellpadding="1" class="table table-bordered table-condensed table-hover" id="table-summary">
 	<thead>
  <tr>
    <th rowspan="2" style="width:22%; vertical-align:middle;">COLLEGES &amp; PROGRAMS </th>
    <th colspan="3" style="width:13%;">1st Year </th>
    <th colspan="3" style="width:13%;">2nd Year </th>
    <th colspan="3" style="width:13%;">3rd Year </th>
    <th colspan="3" style="width:13%;">4th Year </th>
    <th colspan="3" style="width:13%;">5th Year </th>
    <th colspan="3" style="width:13%;">GRAND TOTAL </th>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <th>M</th>
    <th>F</th>
    <th>Total</th>
    <th>M</th>
    <th>F</th>
    <th>Total</th>
    <th>M</th>
    <th>F</th>
    <th>Total</th>
    <th>M</th>
    <th>F</th>
    <th>Total</th>
    <th>M</th>
    <th>F</th>
    <th>Total</th>
    <th>M</th>
    <th>F</th>
    <th>Total</th>
  </tr>
	</thead>
  <?php 
		$grand_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
									'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
									'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
									'total_m'=>0,'total_f'=>0,'grand_total'=>0);
		$program_total = 0;
  		foreach($colleges AS $college) {
  ?>
  <tbody>
  <tr style="text-align:center;" class="success">
    <td class="cgrp" data-college-id="<?php echo $college->college_code ?>" data-displayed="0" style="text-align:left; font-weight:bold;"><?php print($college->name); ?>
     <span class="pull-right" style="font-size:10px">Expand +</span></td>
     
    </tr>
  <?php
			$programs = $this->Enrollments_Model->ListEnrollmentSummary($college->id, $academic_terms_id);
			
			$total1 = 0;
			$total2 = 0;
			$total3 = 0;
			$total4 = 0;
			$total5 = 0;			
			$total_m = 0;			
			$total_f = 0;			
			$col_total = 0;
			$college_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
									'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
									'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
									'total_m'=>0,'total_f'=>0);
			foreach ($programs AS $program) {
				$college_total['m1'] = $college_total['m1'] + $program->m1; 
				$college_total['f1'] = $college_total['f1'] + $program->f1;
				$total1 = $program->m1 + $program->f1; 
				$college_total['total1'] = $college_total['total1'] + $total1;
				$college_total['m2'] = $college_total['m2'] + $program->m2; 
				$college_total['f2'] = $college_total['f2'] + $program->f2; 
				$total2 = $program->m2 + $program->f2; 
				$college_total['total2'] = $college_total['total2'] + $total2;
				$college_total['m3'] = $college_total['m3'] + $program->m3; 
				$college_total['f3'] = $college_total['f3'] + $program->f3;
				$total3 = $program->m3 + $program->f3; 
				$college_total['total3'] = $college_total['total3'] + $total3;
				$college_total['m4'] = $college_total['m4'] + $program->m4; 
				$college_total['f4'] = $college_total['f4'] + $program->f4; 
				$total4 = $program->m4 + $program->f4; 
				$college_total['total4'] = $college_total['total4'] + $total4;
				$college_total['m5'] = $college_total['m5'] + $program->m5; 
				$college_total['f5'] = $college_total['f5'] + $program->f5; 
				$total5 = $program->m5 + $program->f5; 
				$college_total['total5'] = $college_total['total5'] + $total5;
				$total_m = $program->m1 + $program->m2 + $program->m3 + $program->m4 + $program->m5; 
				$college_total['total_m'] = $college_total['total_m'] + $total_m;
				$total_f = $program->f1 + $program->f2 + $program->f3 + $program->f4 + $program->f5; 
				$college_total['total_f'] = $college_total['total_f'] + $total_f;
				$program_total = $total_m + $total_f; 
				$col_total = $col_total + $program_total;
   ?>
  <tr style="display:none">
    <td style="text-align:left; padding-left:30px;"><?php print($program->abbreviation); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m1); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f1); ?></td>
    <td style="text-align:center; font-weight:bold;">
    		<?php 
				if ($total1) {
					print("<a class=\"download_enrollees\" program_id=\"$program->id\" academic_terms_id=\"$academic_terms_id\" year_level=\"1\" program_abbreviation=\"$program->abbreviation\" sy=\"$term->term $term->sy\" ylevel=\"[1st Year]\" href=\"\" style=\"color:#000000;text-decoration:underline\">$total1</a>");
				} else {
					print($total1); 
				}
				
			?>
				</td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m2); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f2); ?></td>
    <td style="text-align:center; font-weight:bold;">
    		<?php 
				if ($total2) {
					print("<a class=\"download_enrollees\" program_id=\"$program->id\" academic_terms_id=\"$academic_terms_id\" year_level=\"2\" program_abbreviation=\"$program->abbreviation\" sy=\"$term->term $term->sy\" ylevel=\"[2nd Year]\" href=\"\" style=\"color:#000000;text-decoration:underline\">$total2</a>");
				} else {
					print($total2); 
				}
				
			?>
	</td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m3); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f3); ?></td>
    <td style="text-align:center; font-weight:bold;">
    		<?php 
				if ($total3) {
					print("<a class=\"download_enrollees\" program_id=\"$program->id\" academic_terms_id=\"$academic_terms_id\" year_level=\"3\" program_abbreviation=\"$program->abbreviation\" sy=\"$term->term $term->sy\" ylevel=\"[3rd Year]\" href=\"\" style=\"color:#000000;text-decoration:underline\">$total3</a>");
				} else {
					print($total3); 
				}
				
			?>
	</td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m4); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f4); ?></td>
    <td style="text-align:center; font-weight:bold;">
    		<?php 
				if ($total4) {
					print("<a class=\"download_enrollees\" program_id=\"$program->id\" academic_terms_id=\"$academic_terms_id\" year_level=\"4\" program_abbreviation=\"$program->abbreviation\" sy=\"$term->term $term->sy\" ylevel=\"[4th Year]\" href=\"\" style=\"color:#000000;text-decoration:underline\">$total4</a>");
				} else {
					print($total4); 
				}
				
			?>
	</td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m5); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f5); ?></td>
    <td style="text-align:center; font-weight:bold;">
    		<?php 
				if ($total5) {
					print("<a class=\"download_enrollees\" program_id=\"$program->id\" academic_terms_id=\"$academic_terms_id\" year_level=\"5\" program_abbreviation=\"$program->abbreviation\" sy=\"$term->term $term->sy\" ylevel=\"[5th Year]\" href=\"\" style=\"color:#000000;text-decoration:underline\">$total5</a>");
				} else {
					print($total5); 
				}
				
			?>
	</td>
    <td style="text-align:center; color:#0000CC;"><?php print($total_m); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($total_f); ?></td>
    <td style="text-align:center; font-weight:bold;"><?php print($program_total); ?></td>
  </tr>
  <?php
  			}
			$grand_total['m1'] = $grand_total['m1'] + $college_total['m1'];
			$grand_total['f1'] = $grand_total['f1'] + $college_total['f1'];
			$grand_total['m2'] = $grand_total['m2'] + $college_total['m2'];
			$grand_total['f2'] = $grand_total['f2'] + $college_total['f2'];
			$grand_total['m3'] = $grand_total['m3'] + $college_total['m3'];
			$grand_total['f3'] = $grand_total['f3'] + $college_total['f3'];
			$grand_total['m4'] = $grand_total['m4'] + $college_total['m4'];
			$grand_total['f4'] = $grand_total['f4'] + $college_total['f4'];
			$grand_total['m5'] = $grand_total['m5'] + $college_total['m5'];
			$grand_total['f5'] = $grand_total['f5'] + $college_total['f5'];
			$grand_total['total1'] = $grand_total['total1'] + $college_total['total1'];
			$grand_total['total2'] = $grand_total['total2'] + $college_total['total2'];
			$grand_total['total3'] = $grand_total['total3'] + $college_total['total3'];
			$grand_total['total4'] = $grand_total['total4'] + $college_total['total4'];
			$grand_total['total5'] = $grand_total['total5'] + $college_total['total5'];
			$grand_total['total_m'] = $grand_total['total_m'] + $college_total['total_m'];
			$grand_total['total_f'] = $grand_total['total_f'] + $college_total['total_f'];
			$grand_total['grand_total'] = $grand_total['grand_total'] + $col_total;

   ?>
  <tr style="font-weight:bold;">
    <td style="text-align:right;">COLLEGE TOTAL: </td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m1']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f1']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total1']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m2']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f2']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total2']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m3']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f3']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total3']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m4']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f4']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total4']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m5']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f5']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total5']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['total_m']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['total_f']); ?></td>
    <td style="text-align:center;"><?php print($col_total); ?></td>
  </tr>
  </tbody>
   <?php
  		}
  ?>
  <tfoot>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td style="text-align:right;">GRAND TOTAL: </td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m1']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f1']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total1']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m2']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f2']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total2']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m3']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f3']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total3']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m4']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f4']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total4']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m5']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f5']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total5']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['total_m']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['total_f']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['grand_total']); ?></td>
  </tr>
  </tfoot>
</table>
<div style="text-align:left;">
<a class="download_enroll" href="" academic_terms_id="<?php print($term->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>

  <form id="download_enroll_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$(document).ready(function(){
  
		$('.download_enroll').click(function(event){
			event.preventDefault(); 
			var academic_terms_id = $(this).attr('academic_terms_id');
	
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_terms_id',
				value: academic_terms_id,
			}).appendTo('#download_enroll_summary_form');

			$('#download_enroll_summary_form').submit();

		});

		
	});
</script>


  <form id="list_enrollees_form" method="post" >
  <input type="hidden" name="step" value="4" />
</form>
<script>
	$(document).ready(function(){
  
		$('.download_enrollees').click(function(event){
			event.preventDefault(); 
			var program_id = $(this).attr('program_id');
			var academic_terms_id = $(this).attr('academic_terms_id');
			var year_level = $(this).attr('year_level');
			var program_abbreviation = $(this).attr('program_abbreviation');
			var sy = $(this).attr('sy');
			var ylevel = $(this).attr('ylevel');
	
			$('<input>').attr({
				type: 'hidden',
				name: 'program_id',
				value: program_id,
			}).appendTo('#list_enrollees_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_terms_id',
				value: academic_terms_id,
			}).appendTo('#list_enrollees_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'year_level',
				value: year_level,
			}).appendTo('#list_enrollees_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'program_abbreviation',
				value: program_abbreviation,
			}).appendTo('#list_enrollees_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'sy',
				value: sy,
			}).appendTo('#list_enrollees_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'ylevel',
				value: ylevel,
			}).appendTo('#list_enrollees_form');


			//modified by kevin -may 27
			//reason: make this request ajax
			
			//$('#list_enrollees_form').submit();
			
		});

	});
</script>


