<?php //print_r($fees_subgroups); die(); 
	//log_message("INFO", $selected_year);      // Toyet 5.11.2018
?>
<div class="row-fluid" style="margin-top:50px;">
<h2 class="heading">Fees Schedule - College</h2>
	<div class="span12">
		<div class="form-horizontal">
			<form method="POST" id="change_academic_year_form" name="MyForm" " >
			<input type="hidden" name="step" value="2">
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<select name="academic_year_id" id="academic_year_id">
						<?php
							foreach($school_years AS $school_year) {
						?>
							<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->school_year . " (". $school_year->status.")"); ?> </option>	
						<?php 
							}
						?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Semester Term</div>
				<div class="controls">
					<?php 
						foreach($terms AS $term) {
					?>
						<input type="checkbox" value="<?php print($term->id); ?>" name="semester[]" checked> <?php print($term->term); ?><br>
					<?php 	
						}
					?>															
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Fees Category</div>
				<div class="controls">
					<select name="fees_group_id" id="fees_groups">
						<!-- <option value="">Select Fees Group</option> -->											
<?php						
						foreach($fees_groups AS $fees_group) {
							echo "<option value=". $fees_group->id . ">" . $fees_group->fees_group ."</option>";	
						}
?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Item</div>
				<div class="controls">
					<select name="fees_subgroups" id="fees_subgroups">						
						<!-- <option value="">Select Item</option> -->											
<?php 					
						if (isset($fees_subgroups)){
							foreach($fees_subgroups AS $fees_subgroup) {
								print("<option value=".$fees_subgroup->id." class=".$fees_subgroup->fees_groups_id.">" . $fees_subgroup->description ."</option>");
							}
						}
?>									
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Program</div>
				<div class="controls">
					<input type="checkbox" value="Y" name="all_program" id="selectall_program" checked> ALL Programs<br>										
					<?php 					
						foreach($colleges AS $college) {
					?>
						<fieldset>
						<input type="checkbox" value="<?php print($college->id); ?>" class="selectedId_college" checked>
						<span><a href="#" onclick="showStuff('<?php print($college->id); ?>');return false;" ><?php print($college->name." (+)"); ?></a></span>
						<br> 
						<span id="<?php print($college->id);?>" style="display: none;">
						<?php 
							$programs=$this->Programs_model->ListCollegeProgramGroups($college->id);
							foreach ($programs AS $program) {
						?>
							<div style="margin-left: 40px;">
							<input type="checkbox" value="<?php print($program->id);?>" class="selectedId_program" 
									name="programs[]" checked>
							<a href="#" onclick="hideStuff('<?php print($college->id); ?>');return false;">
							<?php print($program->abbreviation." (-)"); ?></a>
							<div style="margin-left: 20px; font-style:italic;"><?php print("(".TRIM($program->prog).")"); ?></div>
							</div>
					<?php 		
							}
					?>
					</span>
					</fieldset>
					<?php 
						}
					?>									
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Year Level</div>
				<div class="controls">
					<input type="checkbox" value="Y" name="all_yr" id="selectall_yr" checked> ALL Year Levels<br>										
					<input type="checkbox" value="1" name="yr_level[]" class="selectedId_yr" checked> 1st Year<br>										
					<input type="checkbox" value="2" name="yr_level[]" class="selectedId_yr" checked> 2nd Year<br>										
					<input type="checkbox" value="3" name="yr_level[]" class="selectedId_yr" checked> 3rd Year<br>										
					<input type="checkbox" value="4" name="yr_level[]" class="selectedId_yr" checked> 4th Year<br>										
					<input type="checkbox" value="5" name="yr_level[]" class="selectedId_yr" checked> 5th Year<br>										
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Amount</div>
				<div class="controls">
					<input type="text" name="rate" value=0>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<input type="submit" value="Proceed to Review Form >>" class="btn btn-primary">									
				</div>
				
			</div>
			</form>
			</div>				
	</div>
</div>


<script language="JavaScript" type="text/javascript" src=<?php print(base_url('assets/js/chained_select/')."/jquery.min.js"); ?> ></script>
<script language="JavaScript" type="text/javascript" src=<?php print(base_url('assets/js/chained_select/')."/jquery.chained.min.js"); ?> charset="utf-8"></script>

<script type="text/javascript" charset="utf-8">
          $(function(){
              $("#fees_subgroups").chained("#fees_groups"); 
          });
</script>


<script>
	$(document).ready(function () {
	    $('#selectall_program').click(function () {
	        $('.selectedId_college').prop('checked', this.checked);
	        $('.selectedId_program').prop('checked', this.checked);
		});
	
	    $('.selectedId_college').change(function () {
	        var check = ($('.selectedId_college').filter(":checked").length == $('.selectedId_college').length);
	        $('#selectall_program').prop("checked", check);
	        $(this).closest('fieldset').find(':checkbox').prop('checked', this.checked);
	    });
	    $('.selectedId_program').change(function () {
	        var check = ($('.selectedId_program').filter(":checked").length == $('.selectedId_program').length);
	        $('#selectall_program').prop("checked", check);
	    });
});
</script>


<script>
	$(document).ready(function () {
	    $('#selectall_yr').click(function () {
	        $('.selectedId_yr').prop('checked', this.checked);
	    });
	
	    $('.selectedId_yr').change(function () {
	        var check = ($('.selectedId_yr').filter(":checked").length == $('.selectedId_yr').length);
	        $('#selectall_yr').prop("checked", check);
	    });
	});
</script>


<script type="text/javascript">
	function showStuff(id) {
		document.getElementById(id).style.display='block';
	}

	function hideStuff(id) {
		document.getElementById(id).style.display='none';
	}

	function validateForm()
	{
		var x=document.forms["MyForm"]["rate"].value;
		var x2=document.forms["MyForm"]["fees_group_id"].value; 
		var x3=document.forms["MyForm"]["fees_subgroups"].value; 
		
		
		if (x2==null || x2=="")  {
			  alert("Please select Fees Category!");
			  document.forms["MyForm"]["fees_group_id"].focus();
			  return false;
		}

		if (x3==null || x3=="")  {
			  alert("Please select item!");
			  document.forms["MyForm"]["fees_subgroups"].focus();
			  return false;
		}
		
		if (x==null || x=="")  {
		  alert("Please input rate!");
		  document.forms["MyForm"]["rate"].focus();
		  return false;
		}
	}

</script>


<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 1,
			}).appendTo('#change_academic_year_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'selected_year',
				value: document.getElementById("academic_year_id").value,
			}).appendTo('#change_academic_year_form');
			$('#change_academic_year_form').submit();
		});
	});
</script>
