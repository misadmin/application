<!-- 
<div class="row-fluid span12">
	<h3 class="heading">Class List</h3>
	<div class="span6 vcard">
		<ul id="basic_info" class="info" style="display:block;">
			<li>
				<span class="item-key">Catalog ID</span>
				<div class="vcard-item"><?php if(isset($course_offering['catalog_id'])) echo $course_offering['catalog_id']; ?></span>
			</li>
			<li>
				<span class="item-key">Section</span>
				<div class="vcard-item"><?php if(isset($course_offering['section_code'])) echo $course_offering['section_code']; ?></span>
			</li>
			<li>
				<span class="item-key">Description</span>
				<div class="vcard-item"><?php if(isset($course_offering['description'])) echo $course_offering['description']; ?></span>
			</li>
			<li>
				<span class="item-key">Offered On</span>
				<div class="vcard-item"><?php echo $course_offering['term']; ?></span>
			</li>
			<li>
				<span class="item-key">Teacher</span>
				<div class="vcard-item"><?php echo $course_offering['teacher']; ?></span>
			</li>
			<li>
				<span class="item-key">Schedule</span>
				<div class="vcard-item"><?php echo $course_offering['schedule']; ?></span>
			</li>
		</ul>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>ID Number</th>
				<th>Name</th>
				<th>Course and Year</th>
			</tr>
		</thead>
		<tbody>
		<?php if(is_array($course_offering['students']) && ! empty($course_offering['students'])): $count = 0;?>
		<?php foreach($course_offering['students'] as $student): $count++; ?>
			<tr>
				<td><?php echo $count; ?></td>
				<td><?php echo $student->idno; ?></td>
				<td><?php echo $student->lname . ", " . $student->fname . " " . $student->mname; ?></td>
				<td><?php echo $student->year_level . " " . $student->academic_program; ?></td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="4">No record(s) found.</td>
			</tr>
<?php endif; ?>
		</tbody>
	</table>
</div>
</div> 
-----------------------------------------------
 
Edited by: Aljun Bajao
5/27/2013-3:03 p.m
 -->
<h2 class="heading">Class List<span class="pull-right"><a href="<?php echo site_url("dric/pdf_class_list/{$this->input->post('course_offerings_id')}"); ?>" target="_blank" class="btn btn-success">PDF Class List</a></span></h2>
<div class="well pull-left" style="border-radius:0;padding:5px 17px">
		Catalog ID&nbsp;	:	<strong>&nbsp;<?php if(isset($course_offering['catalog_id'])) echo $course_offering['catalog_id']; ?></strong><br> 	
		Section&nbsp;	:	<strong>&nbsp;<?php if(isset($course_offering['section_code'])) echo $course_offering['section_code']; ?></strong><br>
		Description&nbsp;	:	<strong>&nbsp;<?php if(isset($course_offering['description'])) echo $course_offering['description']; ?></strong><br>
		Offered On&nbsp;	:	<strong>&nbsp;<?php echo $course_offering['term']; ?></strong><br>
		Teacher&nbsp;	:	<strong>&nbsp;<?php echo $course_offering['teacher']; ?></strong><br>
		Schedule&nbsp;	:	<strong>&nbsp;<?php echo $course_offering['schedule']; ?></strong><br>
		
</div>

<div class="row-fluid">
	<div class="span12">
		<table class="table table-hover">
		<thead>
				<tr>
					<th style="text-align:center">&nbsp;</th>
					<th style="text-align:center">ID Number</th>
					<th>Name</th>
					<th>Course and Year</th>
				</tr>
		</thead>
			<?php if(is_array($course_offering['students']) && ! empty($course_offering['students'])): $count = 0;?>
			<?php foreach($course_offering['students'] as $student): $count++; ?>
			<tr>
				<td style="text-align:center"><?php echo $count; ?></td>
				<td style="text-align:center"><?php echo $student->idno; ?></td>
				<td><?php echo $student->lname . ", " . $student->fname . " " . $student->mname; ?></td>
				<td><?php echo $student->year_level . " " . $student->academic_program; ?></td>
			</tr>
			<?php endforeach; ?>
			<?php else: ?>
			<tr>
				<td colspan="4" style="text-align:center">No record(s) found.</td>
			</tr>
			<?php endif; ?>
		</table>
	</div>
</div>