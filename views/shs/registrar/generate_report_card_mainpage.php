
		<div style="width:<?php 
								if (isset($value_width)) {
									print($value_width); 
								} else {
									print("70%");
								}
							?>;" >
			<?php 
				if($show_pulldown) {
			?>
					<div style="width:99%; margin:0 auto; ">
						<?php 
							$data = array(
										'academic_terms'=>$academic_terms,
										'selected_term'=>$selected_term,
										'select_id'=>'report_card_pdf',
										'change_select'=>'change_report_card');
							$this->load->view('shs/reports/pulldown_academic_terms',$data);
						?>
					</div>
			<?php
				}
			?>
			
			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; " id="display_generate_report_card" >
				<?php 
					$data = array(
									'grade_reports'=>$grade_reports,
									'observed_values'=>$observed_values,
									'school_days'=>$school_days,
									'present_days'=>$present_days,
									'my_student'=>$my_student
								);
								
					$this->load->view('shs/registrar/generate_report_card', $data);
				?>
			</div>
		</div>

			
<script>
	$(document).ready(function(){
		$('.change_report_card').bind('change', function(){
			
			var	student  = '<?php print(json_encode($my_student)); ?>'

			var element2 = $("option:selected", "#report_card_pdf");
			var term_id  = element2.val();
			
			var student_idno = '<?php print($idnum); ?>';

			$('#show_change_icon_report_card_pdf').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+student_idno,
				data: {	"student": student,
						"term_id": term_id,
						"action": "change_term_generate_report_card" },
				dataType: 'json',
				success: function(response) {													

					$('#display_generate_report_card').html(response.output); 

					$('#show_change_icon_report_card_pdf').html("")
										
				}
			});

		});
	});
</script> 

