
<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function hide_terms()
{
	hide('term');
}
function show_terms()
{
	show('term');
}
</script>

<body onLoad="show_terms();">

<div style="background:#FFF; width:50%; text-align:center; margin:auto;" align="center">
	<form id="edit_withdrawal_sked_form" method="post" action="" class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="4" />
	<input type="hidden" name="withdrawal_sked_id" value="<?php print($withdraw_sked_id); ?>" />
	<input type="hidden" name="academic_terms_id" value="<?php print($academic_terms_id); ?>" />
 	 <input type="hidden" name="term_id" value="<?php print($term_id); ?>" />
	  <input type="hidden" name="level_id" value="<?php print($level_id); ?>" />
	<table height="250" align="left" cellpadding="0" cellspacing="0" class="head1" style="width:75%; margin-top:10px;">
  		<tr class="head">
			<td colspan="3" class="head" style="text-align:center; padding:10px;">Edit Withdrawal Schedule </td>
		</tr>
  		<tr>
    		<td colspan="3">  
  				<table border="0" cellpadding="2" cellspacing="0" class="inside" align="left">
					<tr>
									<td width="30%" align="left">Academic Year </td>
									<td width="3%" align="left">:</td>
									<td width="67%" align="left"> <?php print($withdraw_sked_info->sy); ?>									</td>
   	 				</tr>
					<?php /*<tr>
						<td width="101" align="left" valign="middle">Terms</td>
						<td width="7" align="left" valign="middle">:</td>
						<td width="399" style="text-align:left; vertical-align:middle;">
							
							<input name="term" type="radio" class="form" onClick="hide_terms();" value="1" checked="checked" /> 
							Yearly
							
							<input name="term" type="radio" class="form" onClick="show_terms();" value="2" /> 
							Semestral
							
							<input name="term" type="radio" class="form" onClick="show_terms();" value="3" /> 
							Trimestral
					  </td>
					</tr> */ ?>
					<tr>
				  		<td colspan="3">
							<table style="width:100%;" border="0" cellspacing="0" cellpadding="0" id="term">
    							<tr>
									<td width="30%" align="left">Term</td>
									<td width="3%" align="left">:</td>
									<td width="67%" align="left">
									<?php print($withdraw_sked_info->terms_name); ?>									</td>
   	 							</tr>
  							</table>
    						<table style="width:100%;" border="0" cellspacing="0" cellpadding="0" id="program">
								<tr>
									<td width="30%" align="left">Level:</td>
									<td width="3%" align="left">:</td>
									<td width="67%" style="text-align:left;">
										<?php print($withdraw_sked_info->level); ?>	
								  </select>  </td>
								</tr>
			  				</table>
    					</td>
  					</tr>
  					<tr align="left">
						<td width="140"> Date Start </td>
						<td width="7"> : </td>
						<td width="229" class="input-append date" id="dp2" data-date-format="yyyy-mm-dd">
							<input type="text" name="start_date" id="start_date" class="span6" value="<?php print($withdraw_sked_info->
									date_start); ?>" readonly/>
					  <span class="add-on"><i class="icon-calendar"></i></span></td>
					</tr>
					
					<tr align="left">
						<td width="140"> Date End </td>
						<td width="7"> : </td>
						<td width="229" class="input-append date" id="dp3" data-date-format="yyyy-mm-dd">
							<input type="text" name="end_date" id="end_date" class="span6" value="<?php print($withdraw_sked_info->
									date_end); ?>" readonly/>
					  <span class="add-on"><i class="icon-calendar"></i></span></td>
					</tr>
					
					<tr align="left">
						<td width="140"> Percentage </td>
						<td width="7"> : </td>
						<td width="350"> 
					  <input type="text" style="text-align:left" class="money input-xlarge wide span2" id="percentage" name="percentage" 
					  				value="<?php print(number_format($withdraw_sked_info->percent,1)); ?>"/> 	</td>								
					</tr>
					<tr align="center"> 
						<td colspan="3"> <button class="adjust" type="submit">Edit Withdrawal Schedule</button> </td>
					</tr>
    			</table>
			</td>
 		</tr>
 	</table>
 </form>
</div>
</body>

<script>
	$(document).ready(function(){
		$('#dp2').datepicker();
	});
	
	$(document).ready(function(){
		$('#dp3').datepicker();
	});
	
	$(document).ready(function(){
		$('#new_withdrawal_sked').validate({
			onkeyup: false,
			errorClass: 'error',
			validClass: 'valid',
			rules: {
				amount: { required: true, minlength: 1 },
				type: {required: true},
			},
			highlight: function(element) {
				$(element).closest('div').addClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						unhighlight: function(element) {
							$(element).closest('div').removeClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						errorPlacement: function(error, element) {
							$(element).closest('div').append(error);
						}
		});
	$(".money").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});


	$('#year').change(function(){
		var year_id = $('#year option:selected').val();
		var level_id = $('#levels option:selected').val();

		
		$.ajax({
			url: "<?php echo site_url("ajax/sections"); ?>/?year=" + year_id + "&level=" + level_id ,
			dataType: "json",
			success: function(data){
				var doptions = make_options_section(data);
				$('#section').html(doptions);
			}	
		});	
	});



	$('#levels').change(function(){
		var level_id = $('#levels option:selected').val();
		
		$.ajax({
			url: "<?php echo site_url("ajax/year"); ?>/?level=" + level_id,
			dataType: "json",
			success: function(data){
				var doptions = make_options_year(data);
				$('#year').html(doptions);
			}	
		});	
	});


function make_options_year (data){
	var doptions = '<option value="">-- Select Year --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].yr_level
		 	+ '">'
		 	+ data[i].yr_level
		 	+ '</option>';
	}
	return doptions;
}

function make_options_section (data){
	var doptions = '<option value="">-- Select Section --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].section
		 	+ '</option>';
	}
	return doptions;
}

</script>


<script>
$('#edit_withdrawal_sked_form').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue edit withdrawal schedule?");

	if (confirmed){
		$('#edit_withdrawal_sked_form').submit();
	}		
});
</script>



  