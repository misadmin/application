<div style="float:left; width:auto;">
<form action="<?php 
	
	switch ($this->uri->segment(2)) {
		case 'enrollment_summary_basic_ed':
			print(site_url($this->uri->segment(1).'/enrollment_summary_basic_ed'));
			break;
		case 'assessment_summary_basic_ed':
			print(site_url($this->uri->segment(1).'/assessment_summary_basic_ed'));
			break;
	}
	?>" method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="2" name="step" />
	
	
	<div class="formSep">
		<h3>Select School Year</h3>
	</div> 

		<fieldset>
			<div class="control-group formSep">
				<label for="" class="control-label">School Year: </label>
					<div class="controls">
						<select id="academic_years_id" name="academic_years_id">
							<!--  <option value="">-- Select Academic Term --</option>  -->
          	<?php
				foreach($academic_years AS $yr) {
					print("<option value=".$yr->id.">".$yr->sy."</option>");	
				}
			?>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">Continue!</button>
					</div>
				</div>	
		</fieldset>
	</form>           
</div>