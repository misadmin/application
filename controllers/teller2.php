<?php
class Teller2 extends MY_Controller {
	
	private $last_transaction_change = 0;
	
	public function __construct(){
		parent::__construct();
		/*$string = 'A/R BIR Electronic Doc Stamps Tax (A/R-BIR ED)';
		preg_match('!\((.*?)\)!', $string, $res);
		print_r($res); die();*/

		$this->navbar_data['menu'] = 
							array('Payors'=>
									array('Student'		=>'student',
										'Employee'		=>'employee',
										'Other Payors'	=>'payors',
										'Add Other Payor'=>'new_payors',
									),
								'Reports'=>		
									array('Daily Receipt Summary' => 'daily_receipt_summary',
										  'Deposit'  => 'deposit',),
								'Other'=>
									array(
											'Setup Receipts' => 'settings',
											'Search Receipts'=> 'search_receipt'
										 ),
											
		);
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		
		$this->load->model('teller/terminal_model');
		$client_ip = $this->input->ip_address();
		//FIXME: we will block all known proxy IP's inside HNU network to prevent two or more terminals from using one IP 
		if (!$this->terminal_model->terminal_is_marked($client_ip)){
			$this->session->set_flashdata('error_message','You need to register this terminal for you to continue.');
			redirect('error/401');
		}
		
		$this->load->model('teller/teller_model');
		
		if (!$this->teller_model->teller_id($this->session->userdata('empno'))){
			$this->session->sess_destroy();
			die("Teller not recognized!");
		}
		$tax_type = $this->session->userdata('tax_type');
		
		if ( ! $tax_type)
			$this->session->set_userdata(array('tax_type'=>'nonvat')); //default... todo: must be set at config...
		$this->content_lib->enqueue_footer_script('maskedinput');
		$this->last_transaction_change = ($this->session->userdata('last_transaction_change') ? $this->session->userdata('last_transaction_change') : 0);
	}

	/**
	 * Restrictions in the checkout method
	 * 1. Each item will have its own receipt.
	 * 2. Multiple items pushed to cart can be paid with only one payment method.
	 * 3. Multiple items pushed to cart must have the same tax type ie (VAT or NON-VAT)
	 * 4. One item pushed to cart can have more than one payment method.
	 *
	 *	
	 */
	public function student(){
		//print_r($this->input->post()); die();
		$this->content_lib->set_title('Teller | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		$this->load->model('teller/teller_model');
		$receipts = $this->teller_model->get_last_receipts();
		//print_r($this->session->all_userdata() );
		//print_r($receipts);		
		if ($this->session->userdata('tax_type') == 'nonvat'){
		//die();
			if ($receipts->non_vat_receipt_no < 1){
				redirect(site_url('teller/settings'));
			}
		}
		if ($this->session->userdata('tax_type') == 'vat'){
			if ($receipts->vat_receipt_no < 1){
				redirect(site_url('teller/settings'));
			}
		}


		$change = $this->session->userdata('last_transaction_change');
		$your_new_password = $this->session->userdata('new_password');


		$last_message = '<h1>Last Transaction Change: P ' . number_format($change, 2) . '</h1>';
		
		if ($your_new_password !== FALSE AND strlen(trim($your_new_password)) > 1 )
			$last_message .= '<h2>Last Password Generated: ' . $your_new_password . '</h2>' ;
		
		
		if ($change !== FALSE OR $your_new_password !== FALSE)
			$this->content_lib->set_message($last_message, 'alert-success');
		
		
		if ( ! empty($query)){
				
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
				
			$this->load->library('pagination_lib');
				
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
	
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
	
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
	
		if (is_numeric($idnum)){
			//after the search... when results are found....
			//print_r($this->input->post());die();
			$this->load->model('hnumis/student_model');
			$this->load->helper('student_helper');
			if(!is_college_student($idnum)){
				if ($this->student_model->student_is_basic_ed($idnum)) {
					redirect(site_url("{$this->role}/basic_ed/{$idnum}"));
				}
			}
			//enqueues the jzebra applet...
			$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
			$this->load->model('student_common_model');
			$this->load->model('academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('teller/teller_model');		
			$this->load->model('financials/Payments_Model');	
			$this->load->model('hnumis/Student_Model');
			$this->load->model('accounts/fees_schedule_model');
				
			$result = $this->teller_model->get_student($idnum);	
			$this->content_lib->set_title ('Teller | ' . $idnum);
			$bank_codes = $this->input->post('bank_codes');
			$current_academic_term_id = $this->academic_terms_model->getCurrentAcademicTermID();
				
			$tab = 'teller';
			switch ($this->input->post('action')){				
				case 'change_tax_type'	: 
								$tax_type = $this->input->post('tax_type');
								$this->session->set_userdata(array('tax_type'=> $tax_type));


								if ($this->session->userdata('tax_type') == 'nonvat'){
									if ($receipts->non_vat_receipt_no < 1){
										redirect(site_url('teller/settings'));
									}
								}
								if ($this->session->userdata('tax_type') == 'vat'){
									if ($receipts->vat_receipt_no < 1){
										redirect(site_url('teller/settings'));
									}
								}
								
								
								break;
				case 'update_student':
								//print_r($current_academic_term_id);die();
								//print_r('sadsd');die();
								if ($this->input->post('what')){
									$what = $this->input->post('what');
									$this->load->model('teller/teller_model');										
									if ($this->teller_model->update_student($what,$result->idno, $result->levels_id, $result->year_level, $result->prospectus_id,$current_academic_term_id) > 0){
											$this->content_lib->set_message('Student successfull updated! <h2>Last transaction change: ' . number_format($this->last_transaction_change, 2) . '</h2>','alert-success');
											$result = $this->teller_model->get_student($idnum);												
									}		
									else 
										//$this->content_lib->set_message('Failed to update student!','alert-error');
										$update_error = "Failed to update student!";
								}								
								break;
				case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;				
				case 'checkout'			:
								$console = new Logger('overwrite');//overwrite, append, reject
								$console->writeline('student method.');
								$console->writeline("checkout occurred.");
								$use_session = FALSE;
								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
									$console->writeline("NONCE is VALID.");
									$items = json_decode($this->input->post('items'));
									//print_r($items);die();
									$payments = json_decode($this->input->post('payments'));
									$receipt_no = $this->input->post('receipt_no');
									$machine_ip = $this->input->ip_address();
									$remarks = array();
									foreach($items as $item){
										$dremarks = str_replace("[newline]", "\n", $item->remarks) . " ";
										$remarks[] = $dremarks;
									}
									
									$teller_items = array();
									$total_cost = 0;
									$total_cost_wo_cr = 0;
									foreach ($items as $item){
										$total_cost_wo_cr += (int)$item->teller_code == 50 ? 0 : $item->amount;  
										
										$total_cost += $item->amount;
										$item_info = $this->teller_model->teller_code_info($item->teller_code);
										$teller_items[] = $item_info->description . " (" . $item_info->teller_code . ")";
										$teller_item_codes[] = $item_info->teller_code;
										$item_costs[] = $item->amount;
									}
									
									$payment_total = 0;
									$payment_methods = array();
									foreach ($payments as $key=>$payment){
										$payment_total += $payment->amount;
										
										switch ($payment->type){
											case 'carry_over'	:
															foreach ($this->session->userdata('payment_methods') as $payment_method){
																$payment_methods[] = $payment_method; 
															}
															$use_session = TRUE;
															break;
											case 'cash'	: $payment_methods[] = array(
																	'type'=>'cash');
															break;
											case 'check' : 
															//When bank is not on list, we need to manually enter it on dB...
															if($payment->bank_id == 'undefined'){
																$payment->bank_id = $this->teller_model->insert_bank(array('name'=>$payment->bank_name));
															}
															$bank_info = $this->teller_model->bank_info($payment->bank_id);
															$payment_methods[] = array(
																	'type'=>'check',
																	'bank'=>$bank_info->code,
																	'date'=>$payment->check_date,
																	'no'=>$payment->check_number,);
															break;
											case 'credit_card' :
															$payment_methods[] = array(
																	'type'=>'credit card',
																	'bank'=>$payment->credit_card_company,
																	'date'=>$payment->credit_card_expiry,
																	'no'=>$payment->credit_card_number,);
												
															break;
											case 'debit_card' :
															$payment_methods[] = array(
																	'type'=>'debit card',
																	'bank'=>$payment->debit_card_company,
																	'date'=>$payment->debit_card_expiry,
																	'no'=>$payment->debit_card_number,);
															break;
											case 'bank_payment'	:
															//NOTE: Bank is not on list but manually added by teller...
															if($payment->bank_code == 'undefined'){
																$payment->bank_code = $this->teller_model->insert_bank(array('name'=>$payment->bank_name));
															}
															$bank_info = $this->teller_model->bank_info($payment->bank_code);
															$payment_methods[] = array(
																'type'=>'bank',
																'bank'=>$bank_info->code,
																'date'=>$payment->date_received,
																);
															break;
											default:
															break;
										}
									}
									
									$change = $payment_total - $total_cost;
									$password_changed = FALSE;
									if (! is_numeric($receipt_no)) {
										$this->content_lib->set_message('Error(s) seen while posting payment to the database. Please check your OR number and try again.' . $receipt_no , 'alert-error');
									} else {
										$console->writeline("receipt no: " . $receipt_no);
										$console->writeline("teller item codes: " . print_r($teller_item_codes, TRUE));
										$payment_methods_ids = ($use_session ? $this->session->userdata('payment_methods_ids') : NULL);
										$this->db->trans_start();		
										$return = $this->teller_model->checkout($result->ap_id, $result->year_level, $result->levels_id, $result->payers_id, $this->session->userdata('empno'), $payments, $items, $receipt_no, $machine_ip, $this->session->userdata('tax_type'), $payment_methods_ids);
										$console->writeline("Checkout Return: " . print_r($return, TRUE));
										if ( ! $return->error_found) {
											$this->db->trans_complete();
											$this->session->set_userdata('payment_methods', $payment_methods);
											//Justin added:
											$this->load->model('hnumis/enrollments_model');
											$this->load->model('academic_terms_model');
											//successfully posted to the database...
											//TODO: HOOK events for the item paid... CR, HR, ER
											$console->writeline("Error NOT Found...");

											//log_message("INFO", print_r($teller_item_codes,true)); // Toyet 10.24.2018

											// edited on 05/31/17 
											if (in_array('CR', $teller_item_codes)) {
												$console->writeline("Transaction has College Registration.");
												$this->load->model('hnumis/student_model');
												$ret = $this->student_model->assess_student_year_level_by_id ($idnum);
												$console->writeline(print_r($ret, TRUE));
												$this->load->helper('password_generator');
												$password = generate_password();
												$password_remarks = "password = {$password}";
												$this->student_common_model->update_information($idnum, array('password'=>$password));
												
												if($this->enrollments_model->academic_term_for_enrollment_date () == $ret['academic_terms_id']){
													//This occurs when the new academic_term is set... and he enrolls...
													$console->writeline("student history's academic term matches the term of the scheduled enrollment.");
													//Update his year level based on student assessment level...			
													$this->teller_model->update_student_history ($ret['history_id'],
															array(
																	'can_enroll'=> ( (($total_cost_wo_cr + 1.0) < $this->session->userdata('t_ledger_balance')) ? 'N' : 'Y'),
																	'year_level'=> $ret['year_level'],
															)
													);
													$console->writeline("updated history id: " . $ret['history_id']);
												} elseif($this->enrollments_model->academic_term_for_enrollment_date() > $ret['academic_terms_id']) {
													//student is enrolling for an upcoming term
													$console->writeline("student history's academic term DOES NOT Match the term of the scheduled enrollment.");
													$this->teller_model->insert_student_history($idnum, $this->enrollments_model->academic_term_for_enrollment_date(), array('can_enroll'=>'Y', 'year_level'=>$ret['year_level']));
													$console->writeline("Inserted new student history.");
												} else {
													//Not on schedule... Student will have a credit balance... he'll be able to enroll on his panel anyway
													//FIXME: Where will the year level assessment occur?
													// it is manual - during registration or enrollment
												}
													
												$this->load->model('student_common_model');
												$in_count = 0;
												foreach ($teller_item_codes as $code){
													if($code=='CR'){
														$remarks[$in_count] = $password_remarks;
														$password_changed = TRUE;
													}
													$in_count++;
												}
												
											}											
											$console->flush();
											if (in_array('HR', $teller_item_codes)){
												//student is registering to High school...
											}
											
											if (in_array('ER', $teller_item_codes)){
												
											}
											
											foreach ($items as $item){
												$name_on_receipt[] = $item->name_on_receipt;
												$trace_strings[] = '\*' . $receipt_no . ",{$this->input->ip_address()},{$this->session->userdata('empno')},{$this->session->userdata('lname')}," . substr($this->session->userdata('fname'), 0, 1) . '*\\';
												$receipt_no++; 
											}
											 
											//we generate the receipt...
											$this->load->library('print_lib');
											$this->load->helper('number_words_helper');

											$receipt_data = array(
															'id_number'=>$idnum, 
															'date'=>date("M j, Y"), 
															'names'=>$name_on_receipt, 
															'course_year'=>$result->year_level . " " . $result->abbreviation,
															'item_codes'=>$teller_item_codes,
															'items'=>$teller_items,
															'payments'=>$item_costs,
															'remarks'=>$remarks,
															'trace_strings'=>$trace_strings,
															'payment_methods'=>$payment_methods,
															'password'=>(isset($password) ? $password : ''),
															);
											
											$this->content_lib->enqueue_body_content('print_templates/receipt', 
													$receipt_data );
											$this->session->set_userdata('last_transaction_change', $change);
											$this->session->set_userdata('payments', $payments);
											$this->session->set_userdata('payment_methods', $payment_methods);
											//$this->session->set_userdata('new_password', $password ? $password : '');
											//redirect('teller/student');
											$this->content_lib->set_message('Successfully posted transaction.<h1>Last Transaction Change: P ' . number_format($change, 2) . '&nbsp;&nbsp;<button class="btn btn-success" id="reprint_receipt">Reprint this Receipt</button></h1>' . ($password_changed ? ("<strong>{$password_remarks}</strong>") : ""), 'alert-success');
											//$this->content_lib->set_message($return, 'alert-success');
											$this->content_lib->content();
											return;
										} else {
											//error posting to the database...
											$this->db->trans_rollback();
											$this->content_lib->set_message($return->error, 'alert-error');
										}
									}
								} else {									
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
								}
								$console->flush();
								break;
				default: 
					//echo "hrerere";die();
						
					break;
				
			}
			$this->load->model('teller/teller_model');
			
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->lname,
						'course'	=> $result->abbreviation,
						'academic_programs_id' => $result->ap_id,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->year_level,
						'full_home_address'	=>$result->full_home_address, 
						'full_city_address' => $result->full_city_address,
						'phone_number'		=>$result->phone_number,
						'levels_id'			=>$result->levels_id,
						'current_yr_or_term_id'=>$result->current_yr_or_term_id,
						'section' 			=>$result->section,
				);
				//print_r($this->session->all_userdata()); die();
				//print_r($dcontent);die();
				$levels_array = array(6,7,11,12);  // added for shs 11262016 1:57pm
 				$college_levels = array(6,7);
				if (!in_array($dcontent['levels_id'],$levels_array)){
					$this->content_lib->set_message("[" . $dcontent['idnum']. "] " . $dcontent['name'] . " - " . $dcontent['college'] . " " . $dcontent['course']." got an invalid Level ID: " . humanize($dcontent['levels_id'])
							."</br>"."If College, please proceed to <a href='"
							.site_url('/teller/new_college_student')."/".$dcontent['idnum']
							."'> DRIC </a>"
							."for temporary Year Level & Program or to <a href='"
							.site_url('/teller/new_basic_ed_student')."/".$dcontent['idnum']
							."'> RSClerk </a>" 
							."otherwise for Year Level & Grade assignment." ,"alert-error");
					$this->content_lib->content();					
					die();										
				}
 
				$current_year_or_term_id['id'] = (in_array($dcontent['levels_id'],$college_levels) ? (int)$current_academic_term_id->current_academic_term_id : (int)$current_academic_term_id->current_academic_year_id); 								
				$current_year_or_term_id['name'] = (in_array($dcontent['levels_id'],$college_levels) ? "term" : "year");
				
				$this->teller_model->match_history_to_current($idnum);
				//if ($current_year_or_term_id['id']  !== (int)$dcontent['current_yr_or_term_id']){
				/*
				if( $this->teller_model->student_has_current_academic_term($idnum)===FALSE) {
					//The student does not have this academic term id... He's a college student so lets add a history equal to the current term.
					$this->teller_model->match_history_to_current($idnum);
					/*
					$this->content_lib->set_message((isset($update_error)?$update_error.'<br><br>':"").
							$dcontent['name']."'s current academic ". $current_year_or_term_id['name'] . " does not match with that of School's current academic ". $current_year_or_term_id['name']."! </br>", "alert-error");
					$this->content_lib->enqueue_body_content('teller/update_student_history',array('person'=>$dcontent));
					$this->content_lib->content();					
					die();
				}
				*/
				//More concise... does what it does in a fewer moves for the teller...
				if($this->teller_model->match_history_to_current($idnum)===FALSE){
					$this->content_lib->set_message("Unable to match student's history to Current Academic Term. Please refer this to the MIS developers.");
					$this->content_lib->enqueue_body_content('teller/update_student_history',array('person'=>$dcontent));
					$this->content_lib->content();				
					die();
				}

				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				$this->load->library('print_lib');
				$this->load->library('tab_lib');
				$this->load->model('hnumis/student_model');

				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$bank_codes = $this->teller_model->get_bank_codes();
				$ledger_data = $this->teller_model->get_ledger_data($result->payers_id);
				$unposted_students_payments = $this->teller_model->get_unposted_students_payment($result->payers_id);				
				$number_of_periods = ($current_academic_terms_obj->term=='Summer' ? 2 : 4);
				$earliest_enrollment_schedule = $this->academic_terms_model->earliest_enrollment_schedule();
				$student_is_enrolled = $this->student_model->student_is_enrolled($idnum);
				//print_r($ledger_data);die();
				$this->load->library('balances_lib',
						array(
								'ledger_data'=>$ledger_data,								
								'unposted_students_payments'=>$unposted_students_payments,
								'current_academic_terms_obj'=>$current_academic_terms_obj,
								'period'=>$this->academic_terms_model->what_period(),
								'period_dates'=>$this->academic_terms_model->period_dates(),
						));

				$this->session->set_userdata('t_ledger_balance',$this->balances_lib->ledger_balance());

				//print_r($this->session->all_userdata());die();

				$teller_codes = $this->teller_model->get_teller_codes($this->session->userdata('tax_type'), TRUE, $result->levels_id);
				
				$is_assessed = $this->teller_model->is_assessed($current_academic_terms_obj->academic_years_id,$current_academic_terms_obj->id, $result->payers_id);
				//$is_assessed = $this->teller_model->is_assessed($current_academic_terms_obj->academic_years_id,459, $result->payers_id);
				
				//print_r($teller_codes);die();
				$teller_values = array(
						'name' => $result->fname . " " . $result->lname,
						'teller_codes'=> $teller_codes,						 
						'bank_codes'=>$bank_codes,
						'ledger_data'=>$this->balances_lib->all_transactions(),
						'period'=>$this->academic_terms_model->what_period(),
						'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
						'number_of_periods' => $number_of_periods,
						'earliest_enrollment_schedule' => $earliest_enrollment_schedule,
						'type' => 'student',
						'student_level_id'=>$result->levels_id,
						'current_academic_term'=>$current_academic_term_id,
						'student_is_enrolled' => $student_is_enrolled,
						'term'=>$current_academic_terms_obj->term,
						'is_assessed'=>$is_assessed,
						);
				
				$assessment_values = array();
				
				$this->tab_lib->enqueue_tab('Teller', 'teller/teller2', $teller_values, 'teller', $tab=='teller');
				$this->load->model('accounts/accounts_model');
				$this->load->model('hnumis/enrollments_model');
				
				//assessment details
				$acontent = array(
						'idnumber'	=>$result->idno,
						'familyname'=>$result->lname,
						'firstname'	=>$result->fname,
						'middlename'=>$result->mname,
						'level'		=>$result->year_level,
						'course'	=>$result->abbreviation,
				);
				
				//assessment form
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
					$hist_id = $selected_history_id;

				} else {
					$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
					$hist_id = $result->student_histories_id;

				}
				
				$assessment_date = $this->Student_Model->get_student_assessment_date($hist_id);
				
				if (!$assessment_date){
					$transaction_date2=array();
					$assessment_date2[0]= (object)array('transaction_date' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . " + 1 day") ));
					$assessment_date = $assessment_date2;
				}
				
				$lab_courses = array();
				$courses = array();
				$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
				
				if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab' && empty($course->enrollments_history_id)) {
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
									);
						} 
						$courses[] = array(
									'id'=>$course->id,
									'name'=>$course->course_code,
									'units'=>$course->credit_units,
									'pay_units'=>$course->paying_units,
									're_enrollments_id'=>$course->re_enrollments_id,
									'withdrawn_on'=>$course->withdrawn_on,
									'assessment_id'=>$course->assessment_id,
									'post_status'=>$course->post_status,
									'enrollments_history_id'=>$course->enrollments_history_id,
									'transaction_date'=>$course->transaction_date,
								);
					}
				}

				//ADDED: 7/2/15 by Genes
				$affiliated_fees = $this->fees_schedule_model->ListStudentAffiliated_Fees($hist_id);
				
				$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment_view_for_students', 
						array(
								'assessment_date'=>$assessment_date[0]->transaction_date,
								'other_courses_payments'=>$other_courses_payments,
								'assessment'=> $assessment,
								'courses'=>$courses,
								'affiliated_fees'=>$affiliated_fees,
								'lab_courses'=>$lab_courses,
								'student_inclusive_terms' =>$student_inclusive_terms,
								'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
								'student_details'=>$acontent
						),
								'assessment', $tab=='assessment');

				//enqueue ledger
				$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$this->balances_lib->all_transactions(),
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date
						), 'ledger', $tab=='ledger');
				
				$payments2 = $this->Payments_Model->ListStudentPayments($result->payers_id);
				$this->tab_lib->enqueue_tab ('Payments', 'student/list_student_payments2', array('payments_info'=>$payments2), 'payments',
						$tab=='payments',FALSE);
				//sidebar...
				$this->content_lib->enqueue_sidebar_widget('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type')), 'Tax Type', 'in');
				
				$receipts = $this->teller_model->get_last_receipts();
				if ($receipts)
					$receipt_no = ($this->session->userdata('tax_type')=='vat' ? $receipts->vat_receipt_no : $receipts->non_vat_receipt_no); else
					$receipt_no = "";

				$this->content_lib->enqueue_sidebar_widget('teller/receipt_number', array('receipt_no'=>$receipt_no), 'Receipt No.', 'in');
				$dcontent['profile_sidebar'] = array();
				$dcontent['profile_sidebar'][] = $this->content_lib->enqueue_content_string('common/search', array('role'=>'teller', 'what'=>'student'));
				$dcontent['profile_sidebar'][] = $this->content_lib->enqueue_content_string('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type'),'receipt_no'=>$receipt_no));
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				$this->content_lib->enqueue_body_content("", $this->tab_lib->content());
				$this->content_lib->flush_sidebar();//the tellers didn't like the sidebar...
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}	
		$this->content_lib->content();
		//this should be placed here, outside the html tag or body to be accessible
	}
	

	public function employee(){
		$this->content_lib->set_title('Teller| Employee | ' . $this->config->item('application_title'));
		$query = $this->input->post('q');
		$empno = $this->uri->segment(3);		

		$this->load->model('teller/teller_model');
		$receipts = $this->teller_model->get_last_receipts();		
		if ($this->session->userdata('tax_type') == 'nonvat'){
			if ($receipts->non_vat_receipt_no < 1){
				redirect(site_url('teller/settings'));
			}
		}
		if ($this->session->userdata('tax_type') == 'vat'){
			if ($receipts->vat_receipt_no < 1){
				redirect(site_url('teller/settings'));
			}
		}
		
		
		$this->content_lib->enqueue_sidebar_widget('teller/search_employee', array('role'=>$this->role, 'what'=>'faculty'), 'Search Employees', 'in');
		if ($query){
			//There is a query...
			$this->load->model('employee_common_model');
			$results = $this->employee_common_model->search($query);
				
			if (count($results) > 1){
				//more than 1 results... lets show it in a page
				foreach ($results as $result){
					if (is_file(FCPATH . $this->config->item('employee_images_folder') . ltrim($result->empno, '0') . ".jpg"))
						$image = base_url($this->config->item('employee_images_folder') . ltrim($result->empno, '0') . ".jpg");
					else {
						//$image = base_url($this->config->item('no_image_placeholder_female')); else
						$image = base_url($this->config->item('no_image_placeholder_male'));
					}
					$res[] = array('image'=>$image, 'idno'=>$result->empno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
				}
				$this->content_lib->enqueue_body_content('teller/employee_search_result', array('results'=>$res, 'query'=>$query, 'role'=>$this->session->userdata('role')));
			} elseif (count($results)==1){
				redirect (site_url("{$this->role}/employee/{$results[0]->empno}"));
			} else {
				//no result...
			}
		}
	
		if ($empno){
			if ($payor = $this->teller_model->employee_payers_id($empno))
				redirect(site_url("{$this->role}/payors/{$payor}")); else
				$this->content_lib->set_message('This employee does not exist', 'alert-error');
		}
	
		if ( ! $empno && ! $query){
			//No query was done by the DEAN... and no employee was found yet...
			$data = array();
			if ($this->input->post('letter')){
				$data['letter'] = $this->input->post('letter');
				$this->load->model('employee_common_model');
				$data['results'] = $this->employee_common_model->search_by_starting_letter($this->input->post('letter'));
			}
			$this->content_lib->enqueue_body_content('teller/employee_search_landing', $data);
		}
	
		$this->content_lib->content();
	}
	
	//Justin May 18, 2013...
	public function payors ($account=0){
		$this->load->model('teller/teller_model');
		$this->load->model('accounts/payors_model', 'payors');
		$this->load->library('table_lib');
		$this->load->library('tab_lib');

		$this->load->model('teller/teller_model');
		$receipts = $this->teller_model->get_last_receipts();
		$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
		if ($this->session->userdata('tax_type') == 'nonvat'){
			if ($receipts->non_vat_receipt_no < 1){
				redirect(site_url('teller/settings'));
			}
		}
		if ($this->session->userdata('tax_type') == 'vat'){
			if ($receipts->vat_receipt_no < 1){
				redirect(site_url('teller/settings'));
			}
		}
		
		
		$change = $this->session->userdata('last_transaction_change');		
		$last_message = '<h1>Last Transaction Change: P ' . number_format($change, 2) . '</h1>';		
		if ($change !== FALSE ) $this->content_lib->set_message($last_message, 'alert-success');
		
		
		if($this->input->post()){
			if($this->common->nonce_is_valid($this->input->post('nonce'))){				
				switch ($this->input->post('action')){
					case 'change_tax_type' :
						$tax_type = $this->input->post('tax_type');
						$this->session->set_userdata(array('tax_type'=> $tax_type));
						if ($this->session->userdata('tax_type') == 'nonvat'){
							if ($receipts->non_vat_receipt_no < 1){
								redirect(site_url('teller/settings'));
							}
						}
						if ($this->session->userdata('tax_type') == 'vat'){
							if ($receipts->vat_receipt_no < 1){
								redirect(site_url('teller/settings'));
							}
						}
						
						break;
					case 'checkout'		:
						$items = json_decode($this->input->post('items'));
						$payments = json_decode($this->input->post('payments'));
						$receipt_no = $this->input->post('receipt_no');
						$machine_ip = $this->input->ip_address();
						$remarks = array();
						$use_session = FALSE;
						foreach($items as $item){
							$dremarks = str_replace("[newline]", "\n", $item->remarks) . " ";
							$remarks[] = $dremarks;
						}
							
						$teller_items = array();
						$total_cost = 0;
						foreach ($items as $item){
							$total_cost += $item->amount;
							$item_info = $this->teller_model->teller_code_info($item->teller_code);
							$teller_items[] = $item_info->description . " (" . $item_info->teller_code . ")";
							$teller_item_codes[] = $item_info->teller_code;
							$item_costs[] = $item->amount;
						}
						//print_r($items);die();
						
						$payment_total = 0;
						$payment_methods = array();
							
						foreach ($payments as $key=>$payment){
							$payment_total += $payment->amount;
						
							switch ($payment->type){
								case 'carry_over'	:
									foreach ($this->session->userdata('payment_methods') as $payment_method){
										$payment_methods[] = $payment_method;
									}
									$use_session = TRUE;
									break;
								case 'cash'	: $payment_methods[] = array(
									'type'=>'cash');
									break;
								case 'check' :
									//When bank is not on list, we need to manually enter it on dB...
									if($payment->bank_id == 'undefined'){
										$payment->bank_id = $this->teller_model->insert_bank(array('name'=>$payment->bank_name));
									}
									$bank_info = $this->teller_model->bank_info($payment->bank_id);
									$payment_methods[] = array(
															'type'=>'check',
															'bank'=>$bank_info->code,
															'date'=>$payment->check_date,
															'no'=>$payment->check_number,);
									break;
								case 'credit_card' :
									$payment_methods[] = array(
									'type'=>'credit card',
									'bank'=>$payment->credit_card_company,
									'date'=>$payment->credit_card_expiry,
									'no'=>$payment->credit_card_number,);
						
									break;
								case 'debit_card' :
									$payment_methods[] = array(
									'type'=>'debit card',
									'bank'=>$payment->debit_card_company,
									'date'=>$payment->debit_card_expiry,
									'no'=>$payment->debit_card_number,);
									break;
								case 'bank'			:
									if($payment->bank_code == 'undefined'){
										$payment->bank_code = $this->teller_model->insert_bank(array('name'=>$payment->bank_name));
									}
									$bank_info = $this->teller_model->bank_info($payment->bank_code);
									$payment_methods[] = array(
											'type'=>'bank',
											'bank'=>$bank_info->code,
											'date'=>$payment->date_received,
									);
									break;
								default:
									break;
							}
						}
							
						$change = $payment_total - $total_cost;
							
						if (! is_numeric($receipt_no)) {
							$this->content_lib->set_message('Error(s) seen while posting payment to the database. Please check your OR number and try again.' . $receipt_no , 'alert-error');
						} else {
							$payment_methods_ids = ($use_session ? $this->session->userdata('payment_methods_ids') : NULL);
							$this->db->trans_start();								
							if ($return = $this->teller_model->checkout(null,null,null,$account, $this->session->userdata('empno'), $payments, $items, $receipt_no, $machine_ip, $this->session->userdata('tax_type'), $payment_methods_ids)) {
								$this->db->trans_complete();
								$this->session->set_userdata('payment_methods', $payment_methods);
								foreach ($items as $item){
									$name_on_receipt[] = $item->name_on_receipt;
									$trace_strings[] = '\*' . $receipt_no . ",{$this->input->ip_address()},{$this->session->userdata('empno')},{$this->session->userdata('lname')}," . substr($this->session->userdata('fname'), 0, 1) . '*\\';
									$receipt_no++;
								}
								
								//we generate the receipt...
								$this->load->library('print_lib');
								$this->load->helper('number_words_helper');
									
								$this->content_lib->enqueue_body_content('print_templates/receipt',
										array(
												'id_number'=>'Acct. No: ' . $account,
												'date'=>date("M j, Y"),
												'names'=>$name_on_receipt,
												'course_year'=>"",
												'item_codes'=>$this->teller_model->get_teller_codes($this->session->userdata('tax_type'), TRUE),
												'items'=>$teller_items,
												'payments'=>$item_costs,
												'remarks'=>$remarks,
												'trace_strings'=>$trace_strings,
												'payment_methods'=>$payment_methods,
										));
								$this->session->set_userdata('last_transaction_change', $change);
								$this->session->set_userdata('payments', $payments);
								$this->session->set_userdata('payment_methods', $payment_methods);
								$this->content_lib->set_message('Successfully posted transaction.<h1>Last Transaction Change: P ' . number_format($change, 2) . '</h1>', 'alert-success');
								$this->content_lib->content();
								die();
							} else {
								//error posting to the database...
								$this->db->trans_rollback();
								$this->content_lib->set_message('Error(s) seen while posting payment to the database.', 'alert-error');
							}
						}
						break;
				}
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}
		}
		
		if (empty($account)){
			//show dataTable for payors...
			$data['data'] = $this->payors->all_non_student_payors();
			$this->content_lib->enqueue_body_content('accounts/accounts_list_table', $data);
			$this->content_lib->enqueue_footer_script('data_tables');
		} else {
			//$change = $this->session->userdata('last_transaction_change');
				
			//$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
			if($data['account'] = $this->teller_model->payers_basic_information($account)) {
				$receipts = $this->teller_model->get_last_receipts();
				if ($receipts)
					$receipt_no = ($this->session->userdata('tax_type')=='vat' ? $receipts->vat_receipt_no : $receipts->non_vat_receipt_no); else
					$receipt_no = "";
				
				$data['profile_sidebar'] = array();
				$data['profile_sidebar'][] = $this->content_lib->enqueue_content_string('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type'),'receipt_no'=>$receipt_no));
				
				$data['teller_codes'] = $this->teller_model->get_teller_codes($this->session->userdata('tax_type'), TRUE, 99);
				$data['bank_codes'] = $this->teller_model->get_bank_codes();
				
				$this->content_lib->enqueue_body_content('teller/other_payors_information', $data);
				$this->tab_lib->enqueue_tab('POS', 'teller/other_payors', $data, 'pos', TRUE);
				$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
			} else {
				//redirect to error
				redirect(site_url('error/404'));
			}
		}
		
		$this->content_lib->content();
	}

	public function inhouse(){
		$this->content_lib->enqueue_body_content('','inhouse ');
		$this->content_lib->content();
	} 
	 
	
	
	public function deposit(){
		$this->content_lib->enqueue_body_content('cashier/metrobank_deposit_slip','');
		$this->content_lib->content();
	}

	public function settings(){
		$this->load->model('teller/teller_model');
		//process actions here:
		if ($this->input->post('action')) {
			if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
				switch ($this->input->post('action')) {
					case 'update_receipts' :
						$data = array(
							'machine_ip'		=>$this->input->ip_address(),
							'vat_receipt_no'	=>$this->input->post('vat-receipt'),
							'non_vat_receipt_no'=>$this->input->post('nonvat-receipt')
						);
						if ($ret = $this->teller_model->update_receipt_numbers($data))
							$this->content_lib->set_message('Receipts successfully set.', 'alert-success'); else
							$this->content_lib->set_message('Error(s) found while updating receipt numbers.', 'alert-error');
						break;
				}
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}	
		} 
		
		if ($receipts = $this->teller_model->get_last_receipts ()){
			//print_r($receipts);die();
			$receipt_data = array(
					'vat_receipt'=>$receipts->vat_receipt_no,
					'nonvat_receipt'=>$receipts->non_vat_receipt_no,
					);
		} else {
			$receipt_data = array(
					'vat_receipt'=>'',
					'nonvat_receipt'=>'',
			);
		}
		
		$this->load->library('tab_lib');
		$this->tab_lib->enqueue_tab ('Receipt Numbers', 'teller/receipt_settings', $receipt_data, 'receipt_settings', TRUE);
		
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->content();
	}

	
	public function add_other_payer(){
		$this->content_lib->enqueue_body_content('','add other payer here');
		$this->content_lib->content();
	}

	public function daily_receipt_summary(){
		$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
		$this->load->model('teller/teller_model');
		$this->content_lib->enqueue_footer_scripts_array(array('date_picker', 'maskedinput'));
		$date = ($this->input->post('date') ? $this->input->post('date') : date('Y-m-d'));
		$data = array('date'=>$date,);
		$this->content_lib->enqueue_body_content('teller/receipt_summary_index', $data);
		$this->load->helper('array_range_helper');
		if($transactions = $this->teller_model->new_daily_receipt_summary($date, $this->input->ip_address())){
			//transactions are found today...
			$items = array();
			$payment_methods = array();
			$receipts = array();
			$sum_from_items = array();
			$ips = array();
			foreach($transactions as $transaction){
				$ips[] = $transaction->machine_ip;
				//print_r($transactions); die();
				$items[$transaction->machine_ip][$transaction->teller_code]['transactions'][$transaction->receipt_no] = $transaction;
				$items[$transaction->machine_ip][$transaction->teller_code]['name'] = $transaction->description;
				//Sum kind of magic...
				if(isset($receipts[$transaction->machine_ip])){
					$receipts[$transaction->machine_ip][] = (object)array('receipt_no'=>$transaction->receipt_no, 'type'=>$transaction->tax_type); 
				} else {
					$receipts[$transaction->machine_ip] = array((object)array('receipt_no'=>$transaction->receipt_no, 'type'=>$transaction->tax_type));
				}
				
			}
			/* This is where the magic begins... */
			$summary_void = array();
			$summary_gross = array();
			$summary_net = array();
			$total_gross = 0;
			$total_net = 0;
			$total_void = 0;
			foreach($items as $ip=>$teller_code_transactions){
				foreach($teller_code_transactions as $trans){
					foreach($trans['transactions'] as $tran){
						$total_gross += $tran->whole_amount;
						$total_net += ($tran->status!='void' ? $tran->whole_amount : 0);
						$total_void += ($tran->status=='void' ? $tran->whole_amount : 0);
						if(isset($summary_gross[$ip]))
							$summary_gross[$ip] += $tran->whole_amount; else
							$summary_gross[$ip] = $tran->whole_amount;
						if($tran->status == 'void'){
							if(isset($summary_void[$ip]))
								$summary_void[$ip] += $tran->whole_amount; else
								$summary_void[$ip] = $tran->whole_amount;	
						} else {
							if(isset($summary_net[$ip]))
								$summary_net[$ip] += $tran->whole_amount; else
								$summary_net[$ip] = $tran->whole_amount;
						}
						if(!isset($sum_from_items[$ip])) {
							$sum_from_items[$ip] = (!in_array($tran->status, array('void', 'deleted')) ? $tran->whole_amount : 0); 
						} else {
							$sum_from_items[$ip] += (!in_array($tran->status, array('void', 'deleted')) ? $tran->whole_amount : 0);
						}
					}		
				}
			}
			$daily_payment_method_summary = $this->teller_model->daily_payment_method_summary($date);
			$magicked_sum = array();
			foreach($daily_payment_method_summary as $payment_method_){
				if(!isset($magicked_sum[$payment_method_->machine_ip][$payment_method_->payment_method]))
					$magicked_sum[$payment_method_->machine_ip][$payment_method_->payment_method] = $payment_method_->amount_sum; else 
					$magicked_sum[$payment_method_->machine_ip][$payment_method_->payment_method] += $payment_method_->amount_sum;
			}
			
			foreach($sum_from_items as $ip => $item_sum){
				$magicked = 0;
				if (isset($magicked_sum[$ip])){
					foreach ($magicked_sum[$ip] as $sum_magicked){
						$magicked += $sum_magicked; 
					}
					$magicked_sum[$ip]['Cash'] = $item_sum - $magicked;
				} else {
					$magicked_sum[$ip]['Cash'] = $item_sum;	
				}
			}
			
			//for summary:
			$summary = array();
			$ips = array_unique($ips);
			foreach($ips as $ip){
				$summary[$ip] = array(
						'non-vat'=>array(),
						'vat'=>array(),	);	
			}
			foreach($receipts as $ip=>$receipt_b){
				foreach($receipt_b as $receipt){
					if($receipt->type=='NV')
						$summary[$ip]['non-vat'][] = $receipt->receipt_no;
					elseif($receipt->type=='V')
						$summary[$ip]['vat'][] = $receipt->receipt_no;
				}
			}
			//print_r($magicked_sum); die();			
			/* This is where the magic ends ... */
			$this->content_lib->enqueue_body_content('teller/receipt_summary_table', 
					array(
							'records'=>$items, 
							'payment_methods'=>$magicked_sum, 
							'summary'=>$summary, 
							'void_transaction_totals'=>$summary_void, 
							'gross_transaction_totals'=>$summary_gross, 
							'net_transaction_totals'=>$summary_net,
							'total_gross'=>$total_gross,
							'total_void'=>$total_void,
							'total_net'=>$total_net));
			$this->load->library('print_lib');
			$this->content_lib->enqueue_body_content('print_templates/daily_receipt_summary', array('records'=>$items, 'payment_methods'=>$magicked_sum));
		} else {
			$this->content_lib->enqueue_body_content('', '<h3>This machine has No Transactions Today!</h3>');
		}
		$this->content_lib->content();
	}

	
	public function receipts(){
		$this->load->model('teller/teller_model');
		$date = $this->input->post('date');
		$payment_methods = $this->input->post('payment_methods');  
		$submitted = $this->input->post('submitted');
		$payment_id = $this->input->post('payment_id');
		if ($date AND $submitted){
			//create date string
			$data = array();
			$data['date'] = date('jS \of F Y',strtotime($date));
			$data['receipts'] = $this->teller_model->receipts(date('Y-m-d',strtotime($date)),$payment_methods);
			//print_r($data);
			//die();
			$this->load->library('table_lib');
			$this->content_lib->enqueue_body_content('teller/receipt_report',$data);
		}
		else if ($payment_id){
			echo $payment_id;
			die();
		}
		else {
			$this->content_lib->enqueue_header_style('date_picker');
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_body_content('teller/receipt_report_index');
		}
		$this->content_lib->content();
	
	}
	
	public function report($what, $date=""){
		//echo "here"; die();
		$this->load->library('pdf_lib');
		switch ($what){
			case 'daily_report' :
				$data = array('date'=>$date,);
				if($transactions = $this->teller_model->new_daily_receipt_summary($date/*, $this->input->ip_address()*/)){
					//echo "here"; die();
					//transactions are found today...
					$items = array();
					$payment_methods = array();
					foreach($transactions as $transaction){
						if(!isset($items[$transaction->machine_ip][$transaction->teller_code][$transaction->receipt_no])){
							$items[$transaction->machine_ip][$transaction->teller_code]['transactions'][$transaction->receipt_no] = $transaction;
							$items[$transaction->machine_ip][$transaction->teller_code]['name'] = $transaction->description;
						}
						if(!isset($payment_methods[$transaction->machine_ip][$transaction->payment_method])){
							$payment_methods[$transaction->machine_ip][$transaction->payment_method] = $transaction->amount;
								
						} else {
							$payment_methods[$transaction->machine_ip][$transaction->payment_method] += $transaction->amount;
						}
					}
					$content = $this->load->view('teller/pdf/daily_report_content', array('records'=>$items, 'payment_methods'=>$payment_methods), TRUE);
					//print_r($content); die();
					$this->load->view('teller/pdf_summary_table', array('content'=>$content, 'date'=>$date));
				} else {
					$this->content_lib->enqueue_body_content('', '<h3>This machine has No Transactions Today!</h3>');
				}
				break;
		}
	}
	
	private function change_tax_type(){
		
	}
	
	public function new_payors(){		
		$this->load->library('../controllers/accounts',array(),'accounts');
		$this->accounts->new_payors();
		
	}	

	public function new_college_student(){
		$this->load->library('../controllers/dric',array(),'new_college');
		$this->new_college->student();
	}

	public function new_basic_ed_student(){
		$this->load->library('../controllers/rsclerk',array(),'new_basic_ed');
		$this->new_basic_ed->student();
	}
	
	//Added by Justing 2/13/2014
	public function basic_ed($idnum){
		//enqueues the jzebra applet...
		$this->load->model('hnumis/student_model');
		$this->load->helper('student_helper');
		if (!$this->student_model->student_is_basic_ed($idnum)) {
			show_404();
		}
		$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
		$this->load->model('student_common_model');
		$this->load->model('academic_terms_model');
		$this->load->model('teller/teller_model');
		$this->load->model('financials/Payments_Model');
		$result = $this->teller_model->get_student($idnum);
		//print_r($result);die();
		$this->content_lib->set_title ('Teller | ' . $idnum);
		$bank_codes = $this->input->post('bank_codes');
		$current_academic_term_id = $this->academic_terms_model->getCurrentAcademicTermID();
		
		$tab = 'teller';
		switch ($this->input->post('action')){
		
			case 'change_tax_type'	:
				$tax_type = $this->input->post('tax_type');
				$this->session->set_userdata(array('tax_type'=> $tax_type));
		
		
				if ($this->session->userdata('tax_type') == 'nonvat'){
					if ($receipts->non_vat_receipt_no < 1){
						redirect(site_url('teller/settings'));
					}
				}
				if ($this->session->userdata('tax_type') == 'vat'){
					if ($receipts->vat_receipt_no < 1){
						redirect(site_url('teller/settings'));
					}
				}
		
		
				break;
			case 'update_student':
				//print_r($current_academic_term_id);die();
				if ($this->input->post('what')){
					$what = $this->input->post('what');
					$this->load->model('teller/teller_model');
					if ($this->teller_model->update_student($what,$result->idno, $result->levels_id, $result->year_level, $result->prospectus_id,$current_academic_term_id) > 0){
						$this->content_lib->set_message('Student successfull updated! <h2>Last transaction change: ' . number_format($this->last_transaction_change, 2) . '</h2>','alert-success');
						$result = $this->teller_model->get_student($idnum);
					}
					else
						//$this->content_lib->set_message('Failed to update student!','alert-error');
						$update_error = "Failed to update student!";
				}
				break;
			case 'change_term'		:
				$selected_history = explode("|", $this->input->post('history_id'));
				$selected_history_id = $selected_history[0];
				$selected_term_id = $selected_history[1];
				$tab = 'assessment';
				break;
			case 'checkout'			:
				$use_session = FALSE;
				if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					$items = json_decode($this->input->post('items'));
					$payments = json_decode($this->input->post('payments'));
					$receipt_no = $this->input->post('receipt_no');
					//print_r($receipt_no);die();
					$machine_ip = $this->input->ip_address();
					$remarks = array();
					foreach($items as $item){
						$dremarks = str_replace("[newline]", "\n", $item->remarks) . " ";
						$remarks[] = $dremarks;
					}
						
					$teller_items = array();
					$total_cost = 0;
					foreach ($items as $item){
						$total_cost += $item->amount;
						$item_info = $this->teller_model->teller_code_info($item->teller_code);
						$teller_items[] = $item_info->description . " (" . $item_info->teller_code . ")";
						$teller_item_codes[] = $item_info->teller_code;
						$item_costs[] = $item->amount;
					}
						
					$payment_total = 0;
					$payment_methods = array();
					foreach ($payments as $key=>$payment){
						$payment_total += $payment->amount;
		
						switch ($payment->type){
							case 'carry_over'	:
								foreach ($this->session->userdata('payment_methods') as $payment_method){
									$payment_methods[] = $payment_method;
								}
								$use_session = TRUE;
								break;
							case 'cash'	: $payment_methods[] = array(
							'type'=>'cash');
							break;
							case 'check' :
								//When bank is not on list, we need to manually enter it on dB...
								if($payment->bank_id == 'undefined'){
									$payment->bank_id = $this->teller_model->insert_bank(array('name'=>$payment->bank_name));
								}
								$bank_info = $this->teller_model->bank_info($payment->bank_id);
								$payment_methods[] = array(
										'type'=>'check',
										'bank'=>$bank_info->code,
										'date'=>$payment->check_date,
										'no'=>$payment->check_number,);
								break;
							case 'credit_card' :
								$payment_methods[] = array(
								'type'=>'credit card',
								'bank'=>$payment->credit_card_company,
								'date'=>$payment->credit_card_expiry,
								'no'=>$payment->credit_card_number,);
		
								break;
							case 'debit_card' :
								$payment_methods[] = array(
								'type'=>'debit card',
								'bank'=>$payment->debit_card_company,
								'date'=>$payment->debit_card_expiry,
								'no'=>$payment->debit_card_number,);
								break;
							case 'bank_payment'	:
								//NOTE: Bank is not on list but manually added by teller...
								if($payment->bank_code == 'undefined'){
									$payment->bank_code = $this->teller_model->insert_bank(array('name'=>$payment->bank_name));
								}
								$bank_info = $this->teller_model->bank_info($payment->bank_code);
								$payment_methods[] = array(
										'type'=>'bank',
										'bank'=>$bank_info->code,
										'date'=>$payment->date_received,
								);
								break;
							default:
								break;
						}
					}
						
					$change = $payment_total - $total_cost;
					$password_changed = FALSE;
					if (! is_numeric($receipt_no)) {
						$this->content_lib->set_message('Error(s) seen while posting payment to the database. Please check your OR number and try again.' . $receipt_no , 'alert-error');
					} else {
						$payment_methods_ids = ($use_session ? $this->session->userdata('payment_methods_ids') : NULL);
						$this->db->trans_start();
						$return = $this->teller_model->checkout($result->ap_id, $result->year_level, $result->levels_id, $result->payers_id, $this->session->userdata('empno'), $payments, $items, $receipt_no, $machine_ip, $this->session->userdata('tax_type'), $payment_methods_ids);
						if ( ! $return->error_found) {
							$this->db->trans_complete();
							$this->session->set_userdata('payment_methods', $payment_methods);
							//successfully posted to the database...
							//TODO: HOOK events for the item paid... HR, ER
							if (in_array('HR', $teller_item_codes)){
								//student is registering to High school...
							}
								
							if (in_array('ER', $teller_item_codes)){
		
							}
								
							foreach ($items as $item){
								$name_on_receipt[] = $item->name_on_receipt;
								$trace_strings[] = '\*' . $receipt_no . ",{$this->input->ip_address()},{$this->session->userdata('empno')},{$this->session->userdata('lname')}," . substr($this->session->userdata('fname'), 0, 1) . '*\\';
								$receipt_no++;
							}
		
							//we generate the receipt...
							$this->load->library('print_lib');
							$this->load->helper('number_words_helper');

							$receipt_data = array(
											'id_number'=>$idnum,
											'date'=>date("M j, Y"),
											'names'=>$name_on_receipt,
											'course_year'=>$result->year_level . " " . $result->abbreviation,
											'item_codes'=>$teller_item_codes,
											'items'=>$teller_items,
											'payments'=>$item_costs,
											'remarks'=>$remarks,
											'trace_strings'=>$trace_strings,
											'payment_methods'=>$payment_methods,
											);
								
							$this->content_lib->enqueue_body_content('print_templates/receipt',
									$receipt_data );
							$this->session->set_userdata('last_transaction_change', $change);
							$this->session->set_userdata('payments', $payments);
							$this->session->set_userdata('payment_methods', $payment_methods);
							$this->content_lib->set_message('Successfully posted transaction.<h1>Last Transaction Change: P ' . number_format($change, 2) . '&nbsp;&nbsp;<button class="btn btn-success" id="reprint_receipt">Reprint this Receipt</button></h1>' . ($password_changed ? ("<strong>{$password_remarks}</strong>") : ""), 'alert-success');
							$this->content_lib->content();
							return;
						} else {
							//error posting to the database...
							$this->db->trans_rollback();
							$this->content_lib->set_message($return->error, 'alert-error');
						}
					}
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
				}
				break;
			default:
				break;
		
		}
		$this->load->model('teller/teller_model');
			
		if ($result !== FALSE) {
			//a user with that id number is seen...
			$port = substr($idnum, 0, 3);
			$dcontent = array(
					'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
					'idnum'		=> $idnum,
					'name'		=> $result->fname . " " . $result->lname,
					'course'	=> $result->abbreviation,
					'college_id'=> $result->colleges_id,
					'college'	=> $result->college_code,
					'level'		=> $result->year_level,
					'full_home_address'	=>$result->full_home_address,
					'full_city_address' => $result->full_city_address,
					'phone_number'		=>$result->phone_number,
					'levels_id'			=>$result->levels_id,
					'current_yr_or_term_id'=>$result->current_yr_or_term_id,
					'section' 	 =>$result->section,
					'bed_status' => $result->bed_status,						
			);
			//print_r($this->session->all_userdata()); die();
			$levels_array = array(1,2,3,4,5,6,7,8,9,11,12);
			$college_levels = array(6,7);
			if (!in_array($dcontent['levels_id'],$levels_array)){
				$this->content_lib->set_message("[" . $dcontent['idnum']. "] " . $dcontent['name'] . " - " . $dcontent['college'] . " " . $dcontent['course']." got an invalid Level ID: " . humanize($dcontent['levels_id'])
						."</br>"."If College, please proceed to <a href='"
						.site_url('/teller/new_college_student')."/".$dcontent['idnum']
						."'> DRIC </a>"
						."for temporary Year Level & Program or to <a href='"
						.site_url('/teller/new_basic_ed_student')."/".$dcontent['idnum']
						."'> RSClerk </a>"
						."otherwise for Year Level & Grade assignment." ,"alert-error");
				$this->content_lib->content();
				die();
			}
		
			$current_year_or_term_id['id'] = (in_array($dcontent['levels_id'],$college_levels) ? (int)$current_academic_term_id->current_academic_term_id : (int)$current_academic_term_id->current_academic_year_id);
			$current_year_or_term_id['name'] = (in_array($dcontent['levels_id'],$college_levels) ? "term" : "year");
					
			if ($current_year_or_term_id['id']  > (int)$dcontent['current_yr_or_term_id']){
				//$this->content_lib->set_message((isset($update_error)?$update_error.'<br><br>':"").$dcontent['name']."'s current academic ". $current_year_or_term_id['name'] . " does not match with that of School's current academic ". $current_year_or_term_id['name']."! </br>", "alert-error");
				//$this->content_lib->enqueue_body_content('teller/update_student_history',array('person'=>$dcontent));
				//$this->content_lib->content();
				//die();
				
				$what='Basic Ed';				
				if ($this->teller_model->update_student($what, $result->idno, $result->levels_id, $result->year_level, $result->prospectus_id, $current_academic_term_id) > 0){
					$result = $this->teller_model->get_student($idnum);
				}
				
			}
		
		
			if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
				$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
			else {
				if ($result->gender == 'F')
					$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
					$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
			}
			$this->load->library('print_lib');
			$this->load->library('tab_lib');
			$this->load->model('hnumis/student_model');
		
			$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
			//print_r($current_academic_terms_obj);die();
			$bank_codes = $this->teller_model->get_bank_codes();
			$ledger_data = $this->teller_model->get_ledger_data($result->payers_id);
			$unposted_students_payments = $this->teller_model->get_unposted_students_payment($result->payers_id);
			$number_of_periods = ($current_academic_terms_obj->term=='Summer' ? 2 : 4);
			$earliest_enrollment_schedule = $this->academic_terms_model->earliest_enrollment_schedule();
			$student_is_enrolled = $this->student_model->student_is_enrolled($idnum);

			//Added these lines to load basic_ed_balances. Loading of balances_lib should be stopped.
			$result3 = $this->student_common_model->my_information($idnum);
			$result2 = $this->teller_model->get_student($idnum);
			$this->load->model('basic_ed/basic_ed_model','bed_model');
			$basic_ed_lib_balances_data = array(
				'ledger_data'			=> $ledger_data,
				'unposted_transactions'	=> $unposted_students_payments,
				'periods'				=> $this->bed_model->exam_dates($result2->year_level, $result2->levels_id),
				'current_academic_year' => $this->bed_model->academic_year(),
				'bed_status' 			=> $result3->bed_status,
				'current_nth_exam'      => $this->bed_model->current_exam($result2->year_level, $result2->levels_id),
				'total_number_of_exams' => count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id)),
			);
			$this->load->library('basic_ed_balances', $basic_ed_lib_balances_data);

			$this->load->library('balances_lib',
					array(
							'ledger_data'=>$ledger_data,
							'unposted_students_payments'=>$unposted_students_payments,
							'current_academic_terms_obj'=>$current_academic_terms_obj,
							'period'=>$this->academic_terms_model->what_period(),
							'period_dates'=>$this->academic_terms_model->period_dates(),
					));

			//$is_assessed = $this->teller_model->is_assessed($current_academic_terms_obj->academic_years_id,$current_academic_terms_obj->id,$result->payers_id);
				
			$teller_values = array(
					'name' => $result->fname . " " . $result->lname,
					'teller_codes'=>$this->teller_model->get_teller_codes($this->session->userdata('tax_type'), TRUE, $result->levels_id),
					'bank_codes'=>$bank_codes,
					'ledger_data'=>$this->balances_lib->all_transactions(),
					'period'=>$this->academic_terms_model->what_period(),
					'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
					'number_of_periods' => $number_of_periods,
					'earliest_enrollment_schedule' => $earliest_enrollment_schedule,
					'type' => 'student',
					'student_level_id'=>$result->levels_id,
					'current_academic_term'=>$current_academic_term_id,
					'student_is_enrolled' => $student_is_enrolled,
					'current_nth_exam'    => $this->basic_ed_balances->current_nth_exam(),
					'due_this_exam'       => $this->basic_ed_balances->due_this_exam2(),
					// 'is_assessed'=>$is_assessed,
			);
		
			$assessment_values = array();
		
			$this->tab_lib->enqueue_tab('Teller', 'teller/teller_basic_ed', $teller_values, 'teller', $tab=='teller');
			$this->load->model('accounts/accounts_model');
			$this->load->model('hnumis/enrollments_model');
		
			//assessment details
			$acontent = array(
					'idnumber'	=>$result->idno,
					'familyname'=>$result->lname,
					'firstname'	=>$result->fname,
					'middlename'=>$result->mname,
					'level'		=>$result->year_level,
					'course'	=>$result->abbreviation,
			);
		
			//assessment form
			if(isset($selected_history_id)){
				$assessment = $this->accounts_model->student_assessment($selected_history_id);
				$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
				$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
				$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
			} else {
				$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
				$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
				$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
				$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
			}
			$lab_courses = array();
			$courses = array();
			$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
		
			if(!empty($dcourses)){
				foreach ($dcourses as $course) {
					if ($course->type_description == 'Lab') {
						$lab_courses[] = array(
								'name'=>$course->course_code,
								'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
						);
					}
					$courses[] = array(
							'id'=>$course->id,
							'name'=>$course->course_code,
							'units'=>$course->credit_units,
							'pay_units'=>$course->paying_units,
							're_enrollments_id'=>$course->re_enrollments_id,
							'withdrawn_on'=>$course->withdrawn_on,
							'assessment_id'=>$course->assessment_id,
							'post_status'=>$course->post_status,
					);
				}
			}
		
			$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment',
					array(
							'other_courses_payments'=>$other_courses_payments,
							'assessment'=> $assessment,
							'courses'=>$courses,
							'lab_courses'=>$lab_courses,
							'student_inclusive_terms' =>$student_inclusive_terms,
							'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
							'student_details'=>$acontent
					),
					'assessment', $tab=='assessment');
		
			//enqueue ledger
			$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
					array(
							'ledger_data'=>$this->balances_lib->all_transactions(),
							'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
							'year_start_date'=>$current_academic_terms_obj->year_start_date
					), 'ledger', $tab=='ledger');
		
			$payments2 = $this->Payments_Model->ListStudentPayments($result->payers_id);
			//print_r($payments);die();
			$this->tab_lib->enqueue_tab ('Payments', 'student/list_student_payments2', array('payments_info'=>$payments2), 'payments',
					$tab=='payments',FALSE);
		
				
			//sidebar...
			$this->content_lib->enqueue_sidebar_widget('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type')), 'Tax Type', 'in');
		
			$receipts = $this->teller_model->get_last_receipts();
			if ($receipts)
				$receipt_no = ($this->session->userdata('tax_type')=='vat' ? $receipts->vat_receipt_no : $receipts->non_vat_receipt_no); else
				$receipt_no = "";
				
			$this->content_lib->enqueue_sidebar_widget('teller/receipt_number', array('receipt_no'=>$receipt_no), 'Receipt No.', 'in');
			$dcontent['profile_sidebar'] = array();
			$dcontent['profile_sidebar'][] = $this->content_lib->enqueue_content_string('common/search', array('role'=>'teller', 'what'=>'student'));
			$dcontent['profile_sidebar'][] = $this->content_lib->enqueue_content_string('teller/tax_type', array('tax_type'=>$this->session->userdata('tax_type'),'receipt_no'=>$receipt_no));
			$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
			$this->content_lib->enqueue_body_content("", $this->tab_lib->content());
			$this->content_lib->flush_sidebar();//the tellers didn't like the sidebar...
		} else {
			//Has a Numeric ID Number but is not found in database...
			//todo: Create a view to be placed here...
			$this->content_lib->set_message('Student does not exist', 'alert-error');
		}
		$this->content_lib->content();
	}

	function search_receipt(){
		$this->load->library('extras_lib');
		$this->extras_lib->search_receipt();
	}
}

class Logger {
	private $log="";
	private $record;
	private $log_file = "error_log.txt";
	
	public function __construct($record='reject'){
		//can be append, reject, overwrite 
		$this->record = $record;
	}
	public function writeline($line=""){
		$this->log .= date('[Y-m-d H:i:s] - ') . $line . "\n";
	}
	public function flush(){
		switch ($this->record){
			case 'append' :
				file_put_contents($this->log_file, $this->log, FILE_APPEND);
				break;
			case 'overwrite' :
				file_put_contents($this->log_file, $this->log);
				break;
			case 'reject' :
				
				break;
		}
	}
	public function set_logfile($file){
		$this->log_file = $file;
	}
}
