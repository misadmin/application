<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}
</style>

<div style="background:#FFF; width:600px;" align="center">
<div style="width:100%; margin-bottom:40px;" align="center">	

<table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
	 <thead> 
		<tr class="head">
  			<th colspan="3" class="head" style="text-align:center; padding:10px;">  EXCEPTION REPORT (<?php print($academic_term->term." ".$academic_term->sy); ?>) </th>
  	   	</tr>
		<tr class="head">
  			<th colspan="3" class="head" style="text-align:center; padding:10px;"><b> Faculty:  <?php print($faculty->neym); ?> </b></th>
  	   	</tr>
	</thead>
  	   	<?php 
			$course = $this->courses_model->faculty_courses_academic_terms($faculty->empno, $academic_term->id); ?>
			<thead>
				<tr>
					<th width="2%" class="head" style="vertical-align:middle;">ID No.</th>
					<th width="20%" class="head" style="vertical-align:middle;">NAME</th>
					<th width="2%" class="head" style="vertical-align:middle;">FINAL RATING</th>
				</tr>
			</thead>
			<?php foreach($course as $c) { 
					$num = 0;
					$failed = $this->enrollments_model->list_failures($c->course_offerings_id); ?>
					<th colspan="3" class="head" style="text-align:left; padding:10px;"> <?php echo $c->course_code.' - '. $c->schedule ?></th>	
			<?php 	if ($failed) { 
						foreach ($failed as $fail) { ?>
							<tr>
								<td style="text-align:center; vertical-align:middle; "><?php print($fail->idno); ?></td>
								<td style="text-align:left; vertical-align:middle; "><?php print($fail->name); ?></td>
								<td style="text-align:center; vertical-align:middle; "><?php print($fail->finals_grade); ?></td>
							</tr>
						<?php 	$num++;
						} ?>
						<tr>
							<th colspan="3" class="head" style="text-align:right; padding:10px;"> Number of Students: <?php echo $num; ?> </th>
						</tr>
					<?php	} else { ?>
						<tr>
							<th colspan="3" class="head" style="text-align:center; padding:10px;">No failures in this course..... </th>
						</tr>
				<?php 	}
				} ?>
    	
</table>
</div>
</div>

