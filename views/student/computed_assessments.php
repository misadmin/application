<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>

<div style="background:#FFF; width:60%; margin-left:20px;">
<h3>Assessment</h3><br>
<div style="width:100%; margin-bottom:10px;">
  <table border="0" cellspacing="0" class="table table-hover table-condensed" style="width:100%; padding:0px; margin:0px; ">
					<tr >
					  <td width="26%" >Tuition Basic</td>
					  <td width="1%" >:</td>
					  <td width="73%" ><?php print($tuition_basic); ?></td>
	  </tr>
	  <tr >
					  <td >Tuition Others</td>
					  <td >:</td>
					  <td ><?php print($tuition_others); ?></td>
	  </tr>
    	<tr >
					  <td >Tuition By Prospectus</td>
					  <td >:</td>
					  <td ><?php print($tuition_byprospectus); ?> </td>
	  </tr>
					<tr >
					  <td >Other Fees</td>
					  <td >:</td>
					  <td ><?php print($other_fees); ?> </td>
	  </tr>
					<tr >
					  <td >Lab Fees </td>
					  <td >:</td>
					  <td ><?php print($lab_fees); ?> </td>
	  </tr>
					<tr >
					  <td >TOTAL</td>
					  <td >:</td>
					  <td style="font-weight:bold;"><?php print($total); ?></td>
	  </tr>
	  	
</table>

</div>

