<div style="width:auto;">

 <table border="0" cellspacing="0" cellpadding="1" class="table table-bordered table-condensed table-hover" id="table-summary">
 	<thead>
  <tr style="background:#EAEAEA;">
    <td colspan="4" align="center" style="text-align:center; font-weight:bold; font-size:16px;">LIST OF STUDENTS<br/><?php print($program->description); ?> - <?php print($level); ?> <br/> 
	<?php
		print($term->term." ".$term->sy);
	 ?></td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td  style="width:2%; text-align:center;">No. </td>
    <td style="width:3%; text-align:center;">ID No. </td>
    <td style="width:10%; text-align:center;">NAME</td>
    <td style="width:1%; text-align:center;">GENDER</td>
  </tr>
  </thead>
  <?php 
  	$num=0;
	$male=0;
	$female=0;
	if($list) {
		foreach($list as $l) {
			$num++; ?>
	  <tr>
		<td style="text-align:right; padding-left:30px;"><?php print($num); ?></td>
		<td style="text-align:center; "><?php print($l->idno); ?></td>
		<td style="text-align:left; "><?php print($l->name); ?></td>
		<td style="text-align:center; font-weight:bold;"><?php print($l->gender); ?></td>
	  </tr>
  <?php if ($l->gender == 'M')
  			$male++;
		else $female++; 
		} ?>
		<tr style="font-weight:bold; background:#EAEAEA;">
			<td colspan="3" style="width:10%; text-align:right;">No. of Male Students: </td>
			<td style="width:1%; text-align:center;"><?php print($male); ?></td>
		</tr>
		<tr style="font-weight:bold; background:#EAEAEA;">
			<td colspan="3" style="width:10%; text-align:right;">No. of Female Students: </td>
			<td style="width:1%; text-align:center;"><?php print($female); ?></td>
		</tr>
  <?php } else { ?>
  			 <tr style="font-weight:bold; background:#EAEAEA;">
   				 <td  colspan="4" style="width:2%; text-align:center;">No enrolled student for this course on the specified semester... </td>
			</tr>
  <?php }?>
  
</table>
<div style="text-align:right">
<a class="download_masterlist" href="" academic_terms_id="<?php print($term->id);?>" program="<?php print($program->id);?>" level="<?php print($level);?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>

</div>

<form id="download_masterlist" method="post" target="_blank">
  <input type="hidden" name="step" value="4" />
</form>
<script>
	$(document).ready(function(){
  
		$('.download_masterlist').click(function(event){
			event.preventDefault(); 
			var academic_terms_id = $(this).attr('academic_terms_id');
			var program = $(this).attr('program');
			var level = $(this).attr('level');		
		
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_terms_id',
				value: academic_terms_id,
			}).appendTo('#download_masterlist');
			$('<input>').attr({
				type: 'hidden',
				name: 'program',
				value: program,
			}).appendTo('#download_masterlist');
			$('<input>').attr({
				type: 'hidden',
				name: 'level',
				value: level,
			}).appendTo('#download_masterlist');
			$('#download_masterlist').submit();
			
		});

		var offset = $('.navbar').height();
	    $("#table-summary").stickyTableHeaders({fixedOffset: offset-1});
	    $('#table-summary').stickyTableHeaders('updateWidth');
		
	});
</script>
