<?php 

class Registrar_Model extends CI_Model {
	/*
	 * constructor
	 */
	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * Add Academic Year
	 * 
	 * Adds another academic year to the system and insert also academic terms based on the config,
	 * sets enrollment schedules and periods on their corresponding tables
	 * 
	 * @return boolean True at all times
	 */
	public function add_academic_year(){
		$sql = "
			INSERT into
				academic_years (`end_year`, `start_date`, `status`)
			SELECT
				end_year + 1, CONCAT((end_year), '-', '{$this->config->item('fiscal_year_start')}'), 'incoming'
			FROM
				academic_years
			ORDER BY
				id DESC
			LIMIT 1
			";
		$query = $this->db->query($sql);
		$year_insert_id = $this->db->insert_id();
		
		foreach ($this->config->item('terms') as $key => $term){
			$sql = "
			INSERT into
				academic_terms (`academic_years_id`, `term`, `start_date`, `status`)
			VALUES
			('{$year_insert_id}', '{$key}', NULL, 'incoming')";
			
			if ($this->db->query($sql)){
				$academic_term_id = $this->db->insert_id();
				$periods_sql = "
					INSERT into
						periods (`academic_terms_id`, `period`)
					VALUES
					";
				foreach ($this->config->item('periods') as $period){
					$periods_sql .= "('{$academic_term_id}', '" . ucfirst($period) . "'),\n";
				}
				$this->db->query(rtrim($periods_sql, ",\n"));
				$year_level_sql = "
					INSERT into
						enrollment_schedules (`academic_terms_id`, `yr_level`)
					VALUES
					";
				foreach ($this->config->item('enrollment_year_levels') as $key=>$val){
					$year_level_sql .= "('{$academic_term_id}', '{$key}'),\n";
				}
				$this->db->query(rtrim($year_level_sql, ",\n"));
			}
		}
		return TRUE;
	}
	
	public function current_and_upcoming_terms(){
		$sql = "
			SELECT
				at.status, at.id as academic_terms_id,
				ay.end_year,
				es.id as enrollment_schedules_id, es.yr_level,
				at.start_date as term_start_date,
				CONCAT((ay.end_year - 1), '-', (ay.end_year)) as school_year,
				CASE term
					when '1' THEN 'First Semester'
					when '2' THEN 'Second Semester'
					when '3' THEN 'Summer' 
				END as term,
				CONCAT('{',
					GROUP_CONCAT(
						CONCAT('\"', p.period, '\":\"', p.id, ',', IFNULL(p.start_date,''), ',' , IFNULL(p.end_date,''),'\"')
					), '}'
				)	
				as period,
				es.start_date as enrollment_start_date, es.end_date as enrollment_end_date 	
			FROM
				academic_terms at
			LEFT JOIN
				academic_years ay
			ON
				ay.id=at.academic_years_id
			LEFT JOIN
				periods p
			ON
				p.academic_terms_id=at.id
			LEFT JOIN
				enrollment_schedules es
			ON
				es.academic_terms_id=at.id
			WHERE
				at.status != 'previous'
			GROUP BY
				es.id
			ORDER BY
				ay.end_year ASC, at.status DESC, es.yr_level ASC
			";
		
		$query = $this->db->query($sql);
		if ($query && count($query->num_rows()) > 0){
			$terms = array();
			foreach ($query->result() as $row){
				if (isset($terms[(int)ltrim($row->academic_terms_id, '0')])){
					$terms[(int)ltrim($row->academic_terms_id, '0')]['enrollments'] = array_merge ($terms[(int)ltrim($row->academic_terms_id, '0')]['enrollments'], array($row->yr_level=>array('enrollment_schedules_id'=>$row->enrollment_schedules_id, 'start'=>$row->enrollment_start_date, 'end'=>$row->enrollment_end_date)));
				} else {
					$periods = json_decode($row->period, TRUE);
					$terms[(int)ltrim($row->academic_terms_id,'0')] = array(
							'academic_terms_id'	=> $row->academic_terms_id,
							'end_year' => $row->end_year,
							'academic_term' => $row->term,
							'status' => $row->status,
							'school_year'=> $row->school_year,
							'start_date' => $row->term_start_date,
							'enrollments'	=> array($row->yr_level=>array('enrollment_schedules_id'=>$row->enrollment_schedules_id, 'start' => $row->enrollment_start_date, 'end'=> $row->enrollment_end_date)),
							'periods' => array(),
							);
					
					if (isset($periods) && is_array($periods)){
						foreach ($periods as $key => $period){
							$period_component = explode(',', $period);
							$terms[(int)ltrim($row->academic_terms_id, '0')]['periods'][$key] = array('period_id'=>$period_component[0], 'start'=>$period_component[1], 'end'=>$period_component[2]); 
						}
					}
				}
			}
			return $terms;
		} else {
			return FALSE;
		}
	}
	
	public function update_academic_term ($academic_term, $data){
		$sql = "
			UPDATE academic_terms SET
			";
		$this->db->trans_start();
		foreach ($data as $key => $val){
			if ($key=='status' && $val=='current'){
				$this->db->query("UPDATE academic_terms SET status='previous' WHERE status='current' LIMIT 1");
				$sql.= "`{$key}`={$this->db->escape($val)},\n";
			}else{
				$sql.= "`{$key}`={$this->db->escape($val)},\n";
			}
		}
		$sql = rtrim($sql, ",\n") . " WHERE id={$this->db->escape($academic_term)}";
		$result = $this->db->query($sql); 
		$this->db->trans_complete();
		if ($result)
			return TRUE; else
			return FALSE;
	}
	
	public function update_periods ($data){
		$sql = "
			INSERT into periods (`id`, `start_date`, `end_date`)
				VALUES
				";
		$updates = " ON DUPLICATE KEY UPDATE
						`start_date` = VALUES(`start_date`), `end_date` = VALUES(`end_date`)
				";
		foreach ($data as $key=>$val){
			$sql .= "({$this->db->escape($val['id'])}, {$this->db->escape($val['start_date'])}, {$this->db->escape($val['end_date'])}),\n";
		}
		$sql = rtrim(str_replace("''", 'NULL', $sql), ",\n");
		$sql .= $updates;
		
		$query = $this->db->query($sql);
		
		if ($query)
			return TRUE; else
			return FALSE;
	}
	
	public function update_enrollment_schedules ($data){
		$sql = "
			INSERT into enrollment_schedules (`id`, `start_date`, `end_date`)
				VALUES
				";
		$updates = " ON DUPLICATE KEY UPDATE
						`start_date` = VALUES(`start_date`), `end_date` = VALUES(`end_date`)
				";
		foreach ($data as $key=>$val){
			$sql .= "({$this->db->escape($val['id'])}, {$this->db->escape($val['start_date'])}, {$this->db->escape($val['end_date'])}),\n";
		}
		$sql = rtrim(str_replace("''", 'NULL', $sql), ",\n");
		$sql .= $updates;
		
		$query = $this->db->query($sql);
		
		if ($query)
			return TRUE; else
			return FALSE;
		
	}

	//written by tatskie  
	public function get_tor_path($idno,$tor_page){
		$initial_path = '/assets/img/tors/'; //can be stored at config 
		$sql="
				SELECT concat(t.path,'_',page_number,'.',file_extension)
				FROM 
					tor_paths t
				WHERE 
					t.students_idno = {$this->db->escape($idno)} AND
					t.page_number   = {$this->db->escape($tor_page)}				
				";

		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $initial_path . $query->result(); else
			return FALSE;		
		
	}
	
	//ADDED: 9/9/13 by Genes
	function get_end_date($academic_terms_id) {
		$result = null;
	
		$q = "SELECT
					a.end_date
				FROM
					periods AS a
				WHERE
					a.academic_terms_id = {$this->db->escape($academic_terms_id)}
					AND a.period='Final' ";
		//print($q);
		//die();
		
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
		
		return $result;
	
	}

	//Put here to emphasize the fact that only the registrar 
	//   can rectify grades   -added: 3.25.19 Toyet
	function rectify_grades($student_history_id, $course_offering_id,
	                        $prelim, $midterm, $final) {
		$result = true;
	
		$q = "update
			  enrollments en
			  set prelim_grade='{$prelim}',
			      midterm_grade='{$midterm}',
			      finals_grade='{$final}'
			  where
			  en.student_history_id = {$this->db->escape($student_history_id)}
			  and en.course_offerings_id = {$this->db->escape($course_offering_id)}";

		//log_message('info', print_r($q,true));
		//die();
		
		$query = $this->db->query($q);

		//assumed no error!!!	
		return $result;
	
	}
	
}