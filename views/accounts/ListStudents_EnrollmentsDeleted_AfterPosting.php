<?php //if (isset($dissolve_students)): ?>
<!-- 
<div style="font-size:15px; border:solid; border-width:1px; margin-bottom:10px; border-color:#E0E0E0; background:#FC3847; ">
		<a href="#" id="download_student_pdf" dissolve_students="<?php print($dissolve_students); ?>">
			<i class="icon-arrow-down"></i> LIST OF STUDENTS OF THE DISSOLVED COURSE 
		</a>
	</div>
 -->  
<?php //endif; ?>

<h2 class="heading">List of Students With Enrollment Deleted After Posting</h2>
<?php //echo validation_errors(); 
  ?>

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#000; background:#FFF; ">
  <table id="offerings" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="8%" style="text-align:center; vertical-align:middle;">ID No.</th>
      <th width="20%" style="text-align:center; vertical-align:middle;">Name</th>
      <th width="10%" style="text-align:center; vertical-align:middle;">Course Code</th>
      <th width="5%" style="text-align:center; vertical-align:middle;">Section</th>
      <th width="5%" style="text-align:center; vertical-align:middle;">Paying Units</th>      
      <th width="5%" style="text-align:center; vertical-align:middle;">Lab. Fee</th>
      <th width="5%" style="text-align:center; vertical-align:middle;">Rate</th>
      <th width="10%" style="text-align:center; vertical-align:middle;">Deleted On</th>
      <th width="5%" style="text-align:center; vertical-align:middle;">Status</th>
    </tr>
    </thead>

        
<?php
	//print_r($offerings);die();
	if ($students) {
			foreach ($students AS $student) {
					echo '<tr>';
					echo '<td style="text-align:center; vertical-align:middle; ">' . $student->students_idno .'</td>';
					echo '<td style="text-align:left; vertical-align:middle; "> ' . $student->neym .'</td>';
					echo '<td style="text-align:left; vertical-align:middle; "> ' . $student->course_code .'</td>';
					echo '<td style="text-align:center; vertical-align:middle; "> ' . $student->section_code .'</td>';
					echo '<td style="text-align:center; vertical-align:middle; "> ' . $student->paying_units .'</td>';
					echo '<td style="text-align:right; vertical-align:middle; "> ' . $student->lab_fee .'</td>';
					echo '<td style="text-align:right; vertical-align:middle; "> ' . $student->rate .'</td>';
					echo '<td style="text-align:center; vertical-align:middle; "> ' . $student->deleted_on .'</td>';
					echo '<td style="text-align:center; vertical-align:middle; "> ' . $student->status .'</td>';
					echo '</tr>';
			} //end of: foreach $offerings 
	}
?>

	</table>
</div>


  <form id="srcform2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="2" />
</form>
<div id="listOfEnrollees" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
    <h3 id="myModalLabel">Modal header</h3>
  </div>
  <div class="modal-body">
    
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save changes</button>
  </div>
</div>
<script>
	$('.class_list').click(function(event){
		event.preventDefault(); 
		var offering_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'offering_id',
		    value: offering_id,
		}).appendTo('#class_list_form');
		$('#class_list_form').submit();
		console.log($('#class_list_form'));

		//added by kevin felisilda april 24, 2013 - post request and popup
		$('#class_list_form').submit(function(event) {
			 
			  /* stop form from submitting normally */
			  event.preventDefault();
			 
			  /* get some values from elements on the page: */
			  var $form = $( this ),
			      term = $form.find( 'input[name="s"]' ).val(),
			      url = $form.attr( 'action' );
			 
			  /* Send the data using post */
			  var posting = $.post( url, { s: term } );
			 
			  /* Put the results in a div */
			  posting.done(function( data ) {
			    var content = $( data ).find( '#content' );
			    $( "#result" ).empty().append( content );
			  });
			});
	});
</script>
<script>
	$('.course_action').click(function(event){
		event.preventDefault(); 
		var offering_id = $(this).attr('href');
		var courses_id = $(this).attr('courses_id');
		var parallel_no = $(this).attr('parallel_no');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'offering_id',
		    value: offering_id,
		}).appendTo('#srcform2');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'courses_id',
		    value: courses_id,
		}).appendTo('#srcform2');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'parallel_no',
		    value: parallel_no,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>


<form id="class_list_form" action="<?php echo site_url("dean/offerings")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="21" />
</form>

<script>
$().ready(function() {
	$('#offerings').dataTable({
				"iDisplayLength": 50,
			    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
	});
    $("#offerings").stickyTableHeaders();
    $('#offerings').stickyTableHeaders('updateWidth');
	
  })
</script>


  <form id="download_students" action="<?php echo site_url("dean/offerings")?>" method="post" target="_blank">
  <input type="hidden" name="step" value="20" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('#download_student_pdf').click(function(event){
		event.preventDefault(); 
		var dissolve_students = $(this).attr('dissolve_students');
				
		$('<input>').attr({
		    type: 'hidden',
		    name: 'dissolve_students',
		    value: dissolve_students,
		}).appendTo('#download_students');
		$('#download_students').submit();
	});
</script>

