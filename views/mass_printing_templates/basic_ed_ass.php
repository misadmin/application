<?php 
$ledger['header'] = array(
		array('id'=>'date', 'name'=>'Date', 'width'=>9),
		array('id'=>'transaction', 'name'=>'Transaction', 'width'=>26),
		array('id'=>'reference', 'name'=>'Reference', 'width'=>18),
		array('id'=>'debit', 'name'=>'DR/Charges', 'width'=>11),
		array('id'=>'credit', 'name'=>'CR/Payments', 'width'=>11),
		array('id'=>'balance', 'name'=>'Balance', 'width'=>11),
);

$this->print_lib->set_table_header($ledger['header']);
//echo $this->print_lib->init();

foreach ($students as $student){
/*
	if($this->session->userdata('printed') == 'yes')
		echo $this->print_lib->set_page_length_in_lines(33); else {
		echo $this->print_lib->set_page_length_in_lines(34);
		$this->session->set_userdata('printed', 'yes');
	}
*/
	$running_balance = 0;
	echo $this->print_lib->align_right($student->basic_info->idno) . "\n";
	echo $this->print_lib->align_right(ucfirst($student->basic_info->lname).", " . ucfirst($student->basic_info->fname) . " ". ucfirst(substr($student->basic_info->mname, 0, 1)) . ".") . "\n";
	echo $this->print_lib->align_right($student->basic_info->yr_level . " - ". $student->basic_info->level . " " . $student->basic_info->section) . "\n";
	echo $this->print_lib->bold() . $this->print_lib->align_right('Assessment ') . $this->print_lib->regular(). "\n";
	$ledger_data = array();
	$total_credits = 0;
	
	foreach($student->ledger_data as $key=>$ledge){
		$debit_balance = 0;
		$debit_balance_previous_year = 0;
		
		$running_balance += $ledge->debit - $ledge->credit;
		$ledger_data[] = array(
				'date'=>$ledge->transaction_date,
				'transaction'=>$ledge->transaction_detail,
				'reference'=>isset($ledge->reference_number) && !empty($ledge->reference_number) ? (is_numeric($ledge->reference_number) ? "OR#" . $ledge->reference_number: $ledge->reference_number) : '',
				'debit'=>$ledge->debit,
				'credit'=>$ledge->credit,
				'balance'=>$running_balance,
		);
		
		$debit_balance += $ledge->debit - $ledge->credit;
		
	}
	
	$this->print_lib->set_data($ledger_data);
	echo $this->print_lib->draw_ledger(146, $student->exam_name, ceil($student->payable), "=", TRUE);

		for ($i = 1; $i < (16-count($this->print_lib->data)); $i++){
			echo "\n";
		}

	echo "Statement Of Account for {$student->exam_name} generated on " . date(DATE_RFC822) . "\n\n\n\n"; 
	echo $this->print_lib->line_feed();	


}