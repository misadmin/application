<?php 

/**
 * 
 * @param string tax_type either vat or nonvat. Defaults to 'nonvat'
 * @return object last receipt in string or False when fails or no receipt found.
 */
class Ledger_model extends MY_Model {

	
	/**
	 * 
	 * @return boolean
	 * Added: October 25, 2013 by Amie
	 */
	public function teller_codes () {
		$sql = "SELECT distinct t.id, t.teller_code, t.description, is_ledger, default_amount, t.tax_type, t.status
		FROM 
			teller_codes t
		WHERE 
			t.status='Active' 
			AND t.tax_type='NV' or t.tax_type='V'
		ORDER BY 
			t.description ASC";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			$this->set_error();
			return FALSE;
		}
	}
	
	/**
	 * 
	 * @return boolean
	 * Added: October 25, 2013 by Amie
	 */
	public function list_teller_codes () {
		$sql = "SELECT distinct t.id, t.teller_code, t.description2, is_ledger, default_amount, t.tax_type, t.status
		FROM
			teller_codes t
		WHERE
			t.status='Active'
			AND t.description2 is NOT NULL
		ORDER BY
			t.description2 ASC";
	
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			$this->set_error();
			return FALSE;
		}
	}
	
	/**
	 * 
	 * @param unknown $userid
	 * @return boolean
	 */
	public function get_student ($userid){
		$sql = "
			SELECT
				py.id as payers_id,s.idno, s.lname, s.fname, s.mname, s.gender,
				sh.year_level, sh.prospectus_id, sh.id as student_histories_id,
				c.college_code, c.id as colleges_id,
				ap.id as ap_id, ap.abbreviation,a.home_address
			FROM students as s
			LEFT JOIN payers as py ON py.students_idno = s.idno
			LEFT JOIN student_histories as sh ON s.idno=sh.students_idno
			LEFT JOIN addresses a ON a.students_idno = s.idno
			LEFT JOIN prospectus as p ON p.id = sh.prospectus_id
			LEFT JOIN academic_programs ap ON ap.id = p.academic_programs_id
			LEFT JOIN colleges c ON c.id=ap.colleges_id
			WHERE
				s.idno={$this->db->escape($userid)} 
			ORDER BY
				sh.id DESC
			LIMIT 1
		";
		
		$query = $this->db->query ( $sql );
		if ($this->db->_error_number ()) {
			$this->set_error ();
			return FALSE;
		}
		
		if (is_object ( $query ) && $query->num_rows () > 0) {
			return $query->row ();
		}else{
			return FALSE;				
		}
	}
	
	/**
	 * 
	 * @param unknown $payers_id
	 * @return boolean
	 * @author Tatskie
	 */
	function get_ledger_data ($payers_id){
		$sql = "
		SELECT date(transaction_date) as transaction_date, transaction_detail, reference_number, debit, credit, remark
		FROM 
		(
			select l.transaction_date, if(p.from_isis='Y',c.description,'Payment') as transaction_detail, if(p.from_isis='Y',p.isis_refno,l.reference_number) as reference_number, l.debit, l.credit,l.remark
			FROM ledger l
			JOIN payments     p on p.id = l.payments_id
			JOIN payers       y on y.id = p.payers_id
			JOIN teller_codes c on c.teller_code = p.isis_tran_code and p.isis_tran_code!=''
			WHERE y.id = {$payers_id}
		UNION 
			select lx.transaction_date, if(p.from_isis='Y',c.description,'Payment') as transaction_detail, if(p.from_isis='Y',p.isis_refno,lx.reference_number) as reference_number, lx.debit, lx.credit,lx.remark
			FROM ledger lx
			JOIN payments     p on p.id = lx.payments_id
			JOIN payers       yx on yx.id = p.payers_id
			JOIN teller_codes c on c.teller_code = p.isis_tran_code and p.isis_tran_code=''
			WHERE yx.id = {$payers_id}
		UNION 
			select l2.transaction_date,'Assessment' as transaction_detail, l2.reference_number, l2.debit, l2.credit,l2.remark
			FROM ledger l2
			JOIN assessments s on s.id = l2.assessments_id 
			JOIN student_histories h on h.id = s.student_histories_id
			JOIN payers  y2 on y2.students_idno = h.students_idno
			WHERE y2.id = {$payers_id}
		UNION
			select l3.transaction_date,'Adjustment' as transaction_detail, l3.reference_number, l3.debit, l3.credit,l3.remark
			FROM ledger l3
			JOIN adjustments a on a.id = l3.adjustments_id 
			WHERE a.payers_id = {$payers_id}
		UNION
			SELECT l4.transaction_date,'Privilege/Scholarship' as transaction_detail, l4.reference_number, l4.debit, l4.credit,l4.remark
			FROM ledger l4
			JOIN privileges_availed v  on v.id = l4.privileges_availed_id 
			JOIN student_histories  h  on h.id = v.student_histories_id
			JOIN payers 		    y4 on y4.students_idno = h.students_idno
			WHERE y4.id = {$payers_id}			
		) t 
		ORDER BY t.transaction_date 				
		";
		
		//print_r($sql); die(); exit();
		//log_message("INFO", print_r($sql,true));  // Toyet 5.19.2018

		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result_array(); else
			return FALSE;
		
	}
	
	/**
	 * 
	 * @param unknown $empno
	 * @return boolean
	 * Added: April 12, 2013 by Amie
	 */
	function post_adjustments($empno) {
		$q = "	
			UPDATE adjustments
			SET posted = 'Y', posted_on = NOW()
			WHERE posted = 'N'
			AND employees_empno = {$this->db->escape($empno)}
		";
				
		if ($this->db->query($q))
			return TRUE; else
			return FALSE;	
	}

	/**
	 * 
	 * @param unknown $data
	 * @param string $posted
	 * @param string $posted_by
	 * @param string $posting_date
	 * @param string $bed
	 * @return boolean
	 */
	function add_adjustment($data, $posted = 'N', $posted_by = NULL, $posting_date = NULL, $bed = FALSE) {		
		
		if ($bed === TRUE){
			$query = "
			INSERT INTO adjustments (
							payers_id, 
							academic_terms_id, 
							levels_id, 
							year_level, 
							transaction_date, 
							adjustment_type, 
							description, 
							amount, 
							remark, 
							employees_empno, 
							posted,
							posted_by,
							posted_on 
						)
				VALUES (
				'{$data['payers_id']}',
					'{$data['term']}',
					'{$data['levels_id']}',
					'{$data['year_level']}',
					'{$data['date']}',
					'{$data['type']}',
					{$this->db->escape($data['description'])},
					'{$data['amount']}',
					{$this->db->escape($data['remark'])},
					'{$data['empno']}',
					'{$posted}',
					{$this->db->escape($posted_by)},
					{$this->db->escape($posting_date)}
					
				)
			";										
		}else{ 
			$query = "
				INSERT INTO adjustments (
							payers_id, 
							academic_terms_id, 
							academic_programs_id, 
							levels_id,
							year_level, 
							transaction_date, 
							adjustment_type, 
							description, 
							amount, 
							remark, 
							employees_empno, 
							posted,
							posted_by,
							posted_on 
						)
					VALUES (
						'{$data['payers_id']}', 
						'{$data['term']}',
						'{$data['academic_programs_id']}',
						'{$data['levels_id']}',
						'{$data['year_level']}',
						'{$data['date']}', 
						'{$data['type']}', 
						{$this->db->escape($data['description'])}, 
						'{$data['amount']}', 
						{$this->db->escape($data['remark'])}, 
						'{$data['empno']}',
						'{$posted}',
						{$this->db->escape($posted_by)},
						{$this->db->escape($posting_date)}
						
					)
				";
		}

		//$this->db->trans_rollback(); print_r($query); die();
		
		if ($this->db->query($query))
			return $this->db->insert_id(); else
			return FALSE;			
	}
	
	/**
	 * 
	 * @param unknown $adj_id
	 * @return boolean
	 * Added: April 12, 2013 by Amie
	 */
	function delete_adjustment($adj_id) {
		$q = "
			DELETE from adjustments
			WHERE id = {$this->db->escape($adj_id)}
		";
		 
		if ($this->db->query($q)) //@FIXME: use affected_rows instead 
			return TRUE; else
			return FALSE;
		
	}
	/**
	 * 
	 * @param unknown $idno
	 * @param unknown $empno
	 * @return boolean
	 * Added: April 12, 2013 by Amie
	 * @edited: 11/20/14 by genes
	 * @note: can view unposted adjustments by other employees and working students 
	 */
	function get_unposted_adjustments($idno, $empno) {
		$q = "(SELECT 
					a.id as adjustment_id, 
					DATE_FORMAT(a.transaction_date,'%m/%e/%Y') transaction_date, 
					a.adjustment_type, 
					a.description, 
					a.amount, 
					a.remark,
					CONCAT(e.lname,', ',e.fname) AS transact_by,
					e.empno AS transact_by_user
				FROM 
					adjustments a, 
					payers p, 
					students s,
					employees AS e
				WHERE 
					a.payers_id = p.id
					AND a.employees_empno=e.empno
					AND p.students_idno = s.idno
					AND s.idno = {$this->db->escape($idno)}
					AND a.posted = 'N'
					-- AND a.employees_empno = {$this->db->escape($empno)}
				)
				UNION
				(SELECT 
					a.id as adjustment_id, 
					DATE_FORMAT(a.transaction_date,'%m/%e/%Y') transaction_date, 
					a.adjustment_type, 
					a.description, 
					a.amount, 
					a.remark,
					CONCAT(e.lname,', ',e.fname) AS transact_by,
					e.idno AS transact_by_user
				FROM 
					adjustments a, 
					payers p, 
					students s,
					students AS e
				WHERE 
					a.payers_id = p.id
					AND a.employees_empno=e.idno
					AND p.students_idno = s.idno
					AND s.idno = {$this->db->escape($idno)}
					AND a.posted = 'N'
					-- AND a.employees_empno = {$this->db->escape($empno)}
				)
				ORDER BY
					transaction_date	
				";
		
		
		$query = $this->db->query($q);
		if ($query && $query->num_rows() > 0)
			return $query->result_array(); else
			return FALSE;
	
	}
	
	/**
	 * @param unknown $empno
	 * @return boolean
	 * Added: April 12, 2013 by Amie
	 * @Edited: 11/20/14 by genes
	 * @note: can view unposted adjustments by other employees and working students
	 */
	function get_all_unposted_adjustments($empno=NULL) {
		$q = "(SELECT 
					date(a.transaction_date) as transaction_date, 
					a.id as adjustment_id, 
					s.idno as idno, 
					CONCAT(s.lname,', ',s.fname,' ',s.mname) as name, 
					a.adjustment_type, 
					a.description, 
					a.amount, 
					a.remark,
					e.empno AS by_user,
					CONCAT(e.lname,', ',e.fname) AS transact_by
				FROM 
					payers p, 
					students s, 
					adjustments a,
					employees AS e 
				WHERE 
					a.payers_id = p.id
					AND a.employees_empno=e.empno
					AND p.students_idno = s.idno
					AND a.posted = 'N'";
		
		if ($empno) {
			$q = $q." AND a.employees_empno = {$this->db->escape($empno)}";
		}
					
		$q = $q . "	)
				UNION 
				(SELECT 
					date(a.transaction_date) as transaction_date, 
					a.id as adjustment_id, 
					s.idno as idno, 
					CONCAT(s.lname,', ',s.fname,' ',s.mname) as name, 
					a.adjustment_type, 
					a.description, 
					a.amount, 
					a.remark,
					e.idno AS by_user,
					CONCAT(e.lname,', ',e.fname) AS transact_by
				FROM 
					payers p, 
					students s, 
					adjustments a,
					students AS e 
				WHERE 
					a.payers_id = p.id
					AND a.employees_empno=e.idno
					AND p.students_idno = s.idno
					AND a.posted = 'N'";
		
		if ($empno) {
			$q = $q." AND a.employees_empno = {$this->db->escape($empno)}";
		}
		
		$q = $q . ")
				ORDER BY 
					transaction_date, 
					adjustment_id, 
					name";

		//print($q); die();
		$query = $this->db->query($q);
		if ($query && $query->num_rows() > 0)
			return $query->result_array(); else
			return FALSE;
	
	}
	
	/*
	 * @ADDED: 11/28/2014
	 * @author: genes
	 */
	function ListEmployeesWhoCreateAdjustment() {
		$q = "(SELECT 
					'E' AS by_user,
					e.empno AS transact_id,
					CONCAT(e.lname,', ',e.fname) AS transact_by
				FROM 
					adjustments AS a,
					employees AS e 
				WHERE 
					a.employees_empno=e.empno
					AND a.posted = 'N'
				)
				UNION 
				(SELECT 
					'S' AS by_user,
					e.idno AS transact_id,
					CONCAT(e.lname,', ',e.fname) AS transact_by
				FROM 
					adjustments AS a,
					students AS e 
				WHERE 
					a.employees_empno=e.idno
					AND a.posted = 'N'
				)
				ORDER BY 
					transact_by ";
		//print($q); die();
		$query = $this->db->query($q);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
		
	}
	
	/*
	 * @ADDED: 11/28/2014
	 * @author: genes
	 */
	function getEmployeeWhoCreateAdjustment($empno=NULL) {
		$q = "(SELECT 
					a.employees_empno,
					CONCAT(b.lname,', ',b.fname) AS transact_by
				FROM 
					adjustments AS a,
					employees AS b
				WHERE 
					a.posted = 'N'
					AND a.employees_empno=b.empno
					AND a.employees_empno = {$this->db->escape($empno)}
				) 
				UNION 
				(SELECT 
					a.employees_empno,
					CONCAT(b.lname,', ',b.fname) AS transact_by
				FROM 
					adjustments AS a,
					students AS b
				WHERE 
					a.posted = 'N'
					AND a.employees_empno=b.idno
					AND a.employees_empno = {$this->db->escape($empno)}
				) ";
		//print($q); die();
		$query = $this->db->query($q);
		if ($query && $query->num_rows() > 0)
			return $query->row(); else
			return FALSE;
		
	}
	
	/**
	 * 
	 * @param unknown $data
	 * @return boolean
	 * Added: 5/2/2013
	 */	
	function insertWithdrawalFees($data) {
	
		$query = "INSERT INTO adjustments(payers_id, academic_terms_id, transaction_date, adjustment_type, description, amount, 
				remark, employees_empno, posted, posted_by, posted_on)
				VALUES ('{$data['payer_id']}', 
				'{$data['current_term_id']}',
				 NOW(), 
				'{$data['adjustment_type']}', 
				'{$data['description']}', 
				'{$data['amount']}', 
				'{$data['remark']}', 
				'{$data['empno']}',
				'Y',
				'{$data['empno']}',
				NOW())";
		//print($query);
		//die();
		if ($this->db->query($query))
			return $this->db->insert_id(); else
			return FALSE;			
	}
	
	/** 
	 * 
	 * @param unknown $hist_id
	 * @return NULL
	 * Added:  January 20, 2014 by Isah
	 */	
	function getAssessedDate($hist_id) {
	
		$result = null;
			
		$q1 = "SELECT transaction_date
				FROM
					assessments as a
				WHERE
				  a.student_histories_id = {$this->db->escape($hist_id)} ";
		//print($q1); die();
		$query = $this->db->query($q1);
	
		//print($q1);
		//die();
	
		if($query->num_rows() > 0){
		$result = $query->row();
	}
		
	return $result;
		
	}
	
	/**
	 * 
	 * @param unknown $data
	 * @param string $posted
	 * @param string $posted_by
	 * @param string $posting_date
	 * @return boolean
	 * Added: 1/27/2014 by Isah
	 *   - 
	 */	
	function add_AddedSubject_adjustment($data, $posted = 'N', $posted_by = NULL, $posting_date = NULL) {
		$insert_id = 0;
		$query = "
				INSERT INTO adjustments (payers_id, academic_terms_id, transaction_date, adjustment_type, description, amount, remark, employees_empno, posted, posted_by, posted_on)
					VALUES (
						'{$data['payers_id']}',
						'{$data['term']}',
						'{$data['date']}',
						'{$data['type']}',
						{$this->db->escape($data['description'])},
						{$this->db->escape($data['amount'])},
						{$this->db->escape($data['remark'])},
						'{$data['empno']}',
						'{$posted}',
						'{$posted_by}',
						'{$posting_date}'
					)
			";
							
		if ($this->db->query($query)){
			$insert_id = $this->db->insert_id(); 
			if($insert_id){
				$q = "
					UPDATE enrollments
					SET post_status = 'added'
					WHERE id = {$this->db->escape($data['id'])}
				";

				if ($this->db->query($q))
					return TRUE; else
					return FALSE;
			}

		}else{
			return FALSE;
		}
	}
			
}
