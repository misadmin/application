<?php

	class Migrator extends My_Controller {
		public $error_message = '';	
		function __construct(){
			parent::__construct();
			$this->common->need_auth();
	
			$this->userinfo = $this->session->all_userdata();

			$this->navbar_data['menu'] =
				array(
						'Migrate Student'		=> 'student',
						'AD Management' => 
								array(	'Lab 109B' => 'lab109b',
										'Lab 007' => 'lab007b',
								),
				);					
			
			$this->content_lib->set_navbar_content('', $this->navbar_data);	
	
	}

	
	public function student(){
		
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){
			$this->content_lib->set_title('Migrator | Student | ' . $this->config->item('application_title'));
			$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
			$query = $this->input->post('q');
			$idnum = $this->uri->segment(3);
	
			if ( ! empty($query)){
	
				//A search query occurs... so lets query the student_common_model
				$page = $this->input->post('page') ? $this->input->post('page') : 1;
				$this->load->model('student_common_model');
				$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
				$total = $this->student_common_model->total_search_results($query);
	
				$this->load->library('pagination_lib');
	
				if ($results !== FALSE) {
					if (count($results) > 1){
						$this->content_lib->set_message("Too many students found!", 'alert-error');
					} else {
						redirect(site_url("{$this->role}/migrate_isis_courses/{$results[0]->idno}"));
					}
				} else {
					//A result is NOT found...
					$this->content_lib->set_message("No result found for that query", 'alert-error');
				}
			}
		
			$this->content_lib->content();
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			$this->index();
		}
		
	}
		

	public function migrate_isis_courses($idno){
		$this->load->model('migration_model');				
		$data = array();
		
		if (($isis_courses = $this->migration_model->list_courses_from_isis($idno)) === false){
			$data['rows'] = (object)array();
		}else{
						
			$isis_courses_to_delete = $isis_courses;
			//print_r($isis_courses);die();  
			
			//see if each course is present in mis. unset if true 
			$x = 0; 
			$col_students_insertion = 0;
			foreach($isis_courses_to_delete as $isis_course){
				
				$history_id = $isis_course->history_id;
				
				//print_r(var_dump($isis_course->col_students_idno));die();
				
				if ( is_null($isis_course->col_students_idno) ){
					if ($col_students_insertion == 0 ){
						$col_inserted =  $this->migration_model->insert_col_students($idno);
						if ($col_inserted){
							$col_students_insertion++;
							//print_r($this->db->last_query());die();								
						}else{
							//print_r($this->db->last_query());die();								
						}
					}else{
						///echo "here";die();
					}
				}				
				
				
				if ( is_null($history_id)){					
					$academic_terms_id_result = $this->migration_model->get_academic_term_id($isis_course->sy,$isis_course->term);
					if ($academic_terms_id_result){ 						
						$hist_inserted =  $this->migration_model->insert_student_history($idno, $academic_terms_id_result[0]->id);
						if ($hist_inserted){
							//print_r($hist_inserted);die();
							$history_id = $hist_inserted;
						}else{
							//print_r($this->db->last_query());die();
						}
					} 					
				}

				//print_r($history_id);print_r('asdsad');die();
				$is_present = $this->migration_model->get_course_from_mis ($history_id, $isis_course->catalog_no);
				if ($is_present)
					 unset($isis_courses[$x]);
				$x++;			
			}
				
			$isis_courses = array_values((array)$isis_courses);
			//print_r($isis_courses);die();

			$courses_to_migrate = $isis_courses;
			$data['rows'] = $isis_courses;
				
				
		}		


		if ($this->input->post()){		
			switch ($this->input->post('action')){
				case 'migrate' :
					//print_r($courses_to_migrate);die();
					if ($inserted = $this->migration_model->migrate_isis_to_mis($this->input->post('check'), $courses_to_migrate) AND $inserted > 0){
						$this->content_lib->set_message("Successfully migrated $inserted records!","alert-success");
						redirect(site_url($this->session->userdata('role').'/migrate_isis_courses/'.$idno ));						
					}else{
						//print_r($this->db->last_query());die();						
						$this->content_lib->set_message('Unable to migrate! '. $this->session->userdata('err_msg') ,'alert-error');
					}
					break;
				default:
					$this->content_lib->set_message('Unrecognized action!','alert-error');
					break;
						
			}
		}
		
		$this->content_lib->enqueue_body_content('migration/isis_courses_view', $data, true);
		$this->content_lib->content();

	}
	
	function lab109b() {
		$this->load->model('migration_model');
		if (($data['rows'] = $this->migration_model->list_room_classes()) === FALSE) {
			$data['error'] = 'query_error';
			$data['error_message']= $this->db->_error_message();
			$this->content_lib->enqueue_body_content('migration/errors_view', $data);
			$this->content_lib->content(); die();
			//$this->content_lib->set_message($this->db->_error_message(),'alert-error');
		}else{
			
			//assign to an array
			$co_array = array();
			$count=0;
			foreach ($data['rows'] as  $row){
				//$co_array[$count]= $row->co_id ;				 
				$co_array[] = array('co_id'=>$row->co_id, 'subject'=>$row->course_code); 
				$count++;
			}
			//print_r($co_array);die();
			
			
			if ( $this->common->nonce_is_valid($this->input->post('nonce')) ){
				if ( $this->input->post('action') ){
				

						$allowed_actions = array('Create Student Folders','Create Student Logins','Share Students Folders');
						
						if ( in_array($this->input->post('action'), $allowed_actions) ){ 

							$checks_count = !is_array($this->input->post('check')) ? 0 : count($this->input->post('check'));
							if ( $checks_count != 1 ){
								$str_only = $checks_count > 1 ? " ONLY " : "";   
								$data['error'] = 'check_only_one';
								$data['error_message']= "Please tick $str_only one check box.";
								$this->content_lib->enqueue_body_content('migration/errors_view', $data);
							}else{
								$index = $this->input->post('check')[0];
								
								if ( ($students = $this->migration_model->list_subject_students($co_array[$index]['co_id'])) === FALSE ){
									$data['error'] = 'asdsadsadsadas';										
									$data['error_message']= $this->db->_error_message();
									$this->content_lib->enqueue_body_content('migration/errors_view', $data);								
								}else{
									$script_data['rows']   = $students;
									$script_data['action'] = $this->input->post('action');
									$this->content_lib->enqueue_body_content('migration/ad_scripts_view', $script_data);
										
									
									//print_r($students);die();
								}

							}
							
						} else {							
							$this->content_lib->set_message('Unrecognized action!','alert-error');
					} //end of switch
				}else{
					$this->content_lib->enqueue_body_content('migration/ad_management_view',$data, true);						
				}
				
			}else{
				$this->content_lib->set_message('Nonce Error!','alert-error');
				
			}  

				
		} //end of else false $data
		$this->content_lib->content();
		
		
	}


	function lab007b() {
		$this->content_lib->set_message('Under Construction!','alert-error');
		$this->content_lib->content();
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
?>