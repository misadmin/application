<?php 

class Places_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function countries (){
		return $this->fetch_results(0, 'countries');	
	}
	
	public function fetch_results ($id, $what='provinces') {
		$sql = "
			SELECT
				id, name
			FROM
				`{$what}`
			";
			
		switch ($what){
			case "provinces"	:
							$sql.="WHERE countries_id={$this->db->escape($id)}";
							break;
			case "towns"		:
							$sql.="WHERE provinces_id={$this->db->escape($id)}";
							break;
			case "barangays"	:
							$sql.="WHERE towns_id={$this->db->escape($id)}";
			default:
							break;
		}
		$sql .= "
				ORDER BY name
				";
		$query = $this->db->query($sql);
		
		if($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
	}
	
	public function country_id ($country="Philippines") {
		$sql = "
			SELECT
				id
			FROM
				countries
			WHERE
				name LIKE '%{$country}%'
			LIMIT 1";
		
		$query = $this->db->query($sql);
		$return = $query->row();
		return $return->id;
	}
	
	public function parse_address ($country_id, $province_id=FALSE, $towns_id=FALSE, $barangays_id=FALSE) {
		
		if (!$country_id)
			return "";
		
		$sql = "
			SELECT
			";
		
		$fields = "c.name as country_name, ";
		$tables = "countries c ";
		$where = "c.id={$this->db->escape($country_id)}\n";
		
		$keys = array('country');
		if ($province_id) {
			$fields .= "p.name as province_name, ";
			$tables .= "LEFT JOIN provinces p ON p.countries_id=c.id\n";
			$where .= "AND p.id={$this->db->escape($province_id)}\n";
			$keys[] = 'province';
		}
		
		if ($towns_id) {
			$fields .= "t.name as town_name, ";
			$tables .= "LEFT JOIN towns t ON t.provinces_id=p.id\n";
			$where .= "AND t.id={$this->db->escape($towns_id)}\n";
			$keys[] = 'town';
		}
		
		if ($barangays_id) {
			$fields .= "b.name as barangay_name, ";
			$tables .= "LEFT JOIN barangays b ON b.towns_id=t.id\n";
			$where .= "AND b.id={$this->db->escape($barangays_id)}\n";
			$keys[] = 'barangay';
		}
		$fields = rtrim($fields, ", ");
		$sql.= $fields . " FROM " . $tables . " WHERE " . $where;

		$query = $this->db->query($sql);
		$row = $query->row();
		
		$return = "";
		foreach ($keys as $key){
			$f = $key . "_name";
			$return = $row->$f . "\n" . $return; 	
		}
		return $return;
	}
	
	
	public function ListCountries() {
			$result = null;
			
			$q = "(SELECT 
						1 AS show_order,
						id,
						name 
					FROM
						countries
					WHERE  
						name = 'Philippines'
				)
				UNION 
				(SELECT 
						2 AS show_order,
						id, 
						name 
					FROM 
						countries 
					WHERE 
						name != 'Philippines'
				) 
				ORDER BY 
					show_order,
					name ";	
			
			//die($q);
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;

	}
	
}