
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">School Term</div>
			<div class="col2a">
				<input type="text" class="form-control" id="term_text_modal" style="width:200px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Grade Level</div>
			<div class="col2a">
				<select class="form-control" style="width:215px;" id="grade_level_modal" >
					<option value="11" >Grade 11</option>
					<option value="12" >Grade 12</option>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Strand</div>
			<div class="col2a">
				<select class="form-control" style="width:auto; height:auto;" id="strand_id_add_modal" >
					<?php 
						if ($strands) {
							foreach ($strands AS $strand) {
					?>		
								<option value="<?php print($strand->id); ?>" ><?php print($strand->description); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Section Name</div>
			<div class="col2a">
				<input type="text" class="form-control" id="section_name_modal" style="width:350px;" autofocus />
			</div>
		</div>
		<div style="width:100%; font-size:12px; font-weight:bold; color:#575555; margin-top:10px;">
			NOTE: You can only create sections on Strands that are <span style="font-weight:bold; color:#E40808;">Offered</span>!
		</div>
		
	</div>
