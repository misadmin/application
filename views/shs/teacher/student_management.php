

	<div class="tabbable" style="width:99%; margin:0 auto; margin-top:15px;" id="show_extracted_items" >
		<ul class="nav nav-tabs">
					<li class="dropdown <?php if($active_tab == 'report_card') print("active"); ?>" >
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Report Card <b class="caret"></b></a>
						<ul class="dropdown-menu" id="report_card">
							<li <?php if($active_tab1 == 'student_attendance') print("class='active'"); ?> >
								<a href="#student_attendance" data-toggle="tab">Attendance</a>
							</li>
							<li <?php if($active_tab1 == 'observed_values') print("class='active'"); ?> >
								<a href="#observed_values" data-toggle="tab">Observed Values</a>
							</li>
						</ul>
					</li>	
		</ul>
		
	
		<div class="tab-content" >
					<div class="tab-pane <?php if($active_tab1 == 'student_attendance') print("active"); ?>" id="student_attendance">
						<?php
							$this->load->view('shs/class_adviser/attendance_mainpage');
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'observed_values') print("active"); ?>" id="observed_values">
						<?php
							$this->load->view('shs/class_adviser/observed_values_mainpage');
						?>
					</div>
		</div>

	</div>

	