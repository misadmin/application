

				<div style="padding-left:20px; font-weight:bold; font-size:15px; margin-bottom:15px; color:#026105; margin-top:10px;">
					<?php 
						$gtotal = 0;
						if ($assess['assess_record']) {
					?>
						ASSESSMENT POSTED ON: <?php print($assess['assess_record']->date_assessed); ?>
					<?php 
						} else {
					?>
						ASSESSMENT UNPOSTED!
					<?php 
						}
					?>
				</div>


				<div style="padding-left:20px;">
					<div style="float:left; font-weight:bold; margin-bottom:25px; border-bottom:solid 1px #CBCBCB; width:40%;">
						<?php 
							if ($assess['tuition_basic']) {
								print("Tuition Fees (@ P ".number_format($assess['tuition_basic']->rate,2).")/unit"); 
							} else {
								print("<div style='color:#A40404; margin-bottom:10px;'>No Basic Rate assigned by Accounts!</div>");  
							}
						?>
					</div>
					
					<div style="clear:both;">
						<table class="table table-hover table-bordered table-striped table-condensed" style="width:50%;" >
							<thead>
								<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
									<td class="shs_header" style="width:56%;">Course Code - Description</td>
									<td class="shs_header" style="width:12%;">Load<br>Units</td>
									<td class="shs_header" style="width:12%;">Paying<br>Units</td>
									<td class="shs_header" style="width:20%;">Total</td>
								</tr>
							</thead>
							<tbody>
							<?php
								if ($assess['courses']) {
									$total=0;
									$total_pay_unit=0;
									foreach($assess['courses'] AS $course) {
										if ($assess['tuition_basic']) {
											$total = $course->paying_units*$assess['tuition_basic']->rate;
										}
										$gtotal = $gtotal + $total;
							?>
										<tr>
											<td>
												<?php 
													print("<div style='width:85px; float:left;'>".$course->course_code." - </div>".substr($course->descriptive_title,0,20)); 
														
													if (strlen($course->descriptive_title) > 20) {
														print('...');
													}
												?>
											</td>
											<td style="text-align:center;"><?php print($course->credit_units); ?></td>
											<td style="text-align:center;"><?php print($course->paying_units); $total_pay_unit += $course->paying_units; ?></td>
											<td style="text-align:right;"><?php print(number_format($total,2)); ?></td>
										</tr>
							<?php
									}
							?>
							</tbody>
							<tfoot>
								<tr style="font-weight:bold; background-color:#B5B2B2;">
									<td colspan="2" style="text-align:right;">TOTAL:</td>
									<td style="text-align:center;"><?php print(number_format($total_pay_unit,2)); ?></td>
									<td style="text-align:right;"><?php print(number_format($gtotal,2)); ?></td>
								</tr>
							</tfoot>
							<?php	
								}
							?>
						</table>
					</div>
					
					<div style="width:55%; text-align:right; padding-top:15px;">
						<?php print("P ".number_format($gtotal,2)); ?>
					</div>
					
					<div style="float:left; font-weight:bold; margin-top:25px; border-bottom:solid 1px #CBCBCB; width:40%; margin-bottom:20px;">
						Miscellaneous and Other Fees:
					</div>
					
					<?php 
						$misc_subtotal = array();
						if ($assess['misc_fees']) {
							foreach($assess['misc_fees'] AS $misc_fee) {
								$total=0;
					?>

								<div style="padding-top:20px;">
									<div style="clear:both; font-weight:bold; font-size:14px; ">
										<?php print($misc_fee->fees_group); ?>
									</div>
									<?php 
										if ($misc_fee->fees_subgroups) {
											foreach($misc_fee->fees_subgroups AS $fee_subgroup) {
									?>
												<div style="clear:both; float:left; padding:5px; padding-left:30px; width:30%;"> 
													<?php print($fee_subgroup->description); ?>
												</div>
												<div style="float:left; width:10%; padding:5px; text-align:right;">
														<?php print($fee_subgroup->rate); $total=$total + $fee_subgroup->rate; $misc_subtotal[$misc_fee->fees_group] = $total; ?>
												</div>
									<?php
											}
										}
									?>		
								</div>
								<div style="clear:both; width:55%; text-align:right;">
									<?php print("P ".number_format($total,2)); $gtotal=$gtotal + $total; ?>
								</div>
					<?php
							}
						}
					?>
					
					<div style="float:left; font-weight:bold; margin-top:25px; border-bottom:solid 1px #CBCBCB; width:40%;">
						Laboratory Fees:
					</div>

					<div style="clear:both;">
						<?php 
							$total = 0;
							$total_lab_fees = 0;
							$lab_courses = array();
							if ($assess['lab_fees']) {
								foreach($assess['lab_fees'] AS $lab_fee) {
						?>
									<div>
										<div style="clear:both; float:left; padding:5px; padding-left:30px; width:30%;">
											<?php print($lab_fee->course_code); $lab_courses['name'] = $lab_fee->course_code; ?>
										</div>
										<div style="float:left; width:10%; padding:5px; text-align:right;">
											<?php print(number_format($lab_fee->rate,2)); $total = $total +  $lab_fee->rate; $lab_courses['amount'] = $lab_fee->rate; ?>
										</div>
									</div>
						<?php
								}
								
								$total_lab_fees = $total;
							}
						?>
							<div style="clear:both; width:55%; text-align:right;">
								<?php print("P ".number_format($total,2)); $gtotal=$gtotal + $total; ?>
							</div>		
					</div>

					<div style="clear:both; margin-top:30px; font-weight:bold; font-size:15px;">
						<div style="float:left; width:40%;">
							TOTAL Assessment:
						</div>
						<div style="float:left; width:15%; text-align:right;">
							<?php print("P ".number_format($gtotal,2)); ?>
						</div>
					</div>
				</div>


<script>
	$(document).ready(function(){
		$('.change_assessment').bind('change', function(){
			
			var element2    = $("option:selected", "#assess");
			var term_id     = element2.val();
			
			var student_idno = '<?php print($idnum); ?>';

			$('#show_change_icon_assess').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+student_idno,
				data: {	"term_id": term_id,
						"action": "change_term_for_assessment" },
				dataType: 'json',
				success: function(response) {													

					$('#show_student_assessment_details').html(response.output); 

					$('#show_change_icon_assess').html("")
										
				}
			});

		});
	});
</script> 
		

