<?php 

$name = $familyname . ", " . $firstname . " " . substr($middlename, 0, 1) . ".";
$course_year = $level . " " . $course;

/*
array('id'=>'descriptive_title', 'name'=>"Descriptive Title", 'width'=>35),
				
 */
$headers = array(
				array('id'=>'course_code', 'name'=>'Course', 'width'=>11), //7 //20
				array('id'=>'section_code', 'name'=>' ', 'width'=>3), //3
				array('id'=>'schedule', 'name'=>'Schedule', 'width'=>16), //20 //25
				array('id'=>'room', 'name'=>'Room', 'width'=>10), //10
				array('id'=>'credit_units', 'name'=>"Units", 'width'=>5), //5
				// array('id'=>'instructor', 'name'=>"Teacher", 'width'=>25), //25
				array('id'=>'signature', 'name'=>"Signature", 'width'=>8), //8 //15
		);

$data = !empty($courses) ? $courses : array();
//print_r($data); die();
$total_units = 0;
$total_paying_units = 0;

if (! empty($courses)){
	foreach($courses as $key=>$val){
		$data[$key]['section_code'] = " ".$val['section_code'];
		$data[$key]['schedule'] = $val['time'];
		$data[$key]['signature'] = "____________________";
		// $data[$key]['signature'] = "___________________________";
		$total_units += $val['credit_units'];
		$total_paying_units += $val['paying_units'];
	}
}
//print_r($total_paying_units );die();
//print_r ($courses);
/*
$data = array(
			array('catalog'=>'ECE 421', 'section'=>'A', 'title'=> 'ELECTRONICS 1', 'units'=> 3, 'teacher'=>'Sabel', 'signature'=>'_____________________________'),
			array('catalog'=>'ECE 422', 'section'=>'B', 'title'=> 'ELECTRONICS 1', 'units'=> 3, 'teacher'=>'Sabel', 'signature'=>'_____________________________'),
		);
*/		
$this->print_lib->set_table_header ($headers);
$this->print_lib->set_data($data);
?>
<?php
//this should be placed outside the html tag or body to be accessible, maybe after $this->content_lib->content();
//echo sprintf($this->config->item('jzebra_applet'), base_url()); ?>
<div id="content_to_print" style="display:none;"><?php 
echo $this->print_lib->init();
if($this->session->userdata('printed') == 'yes')
	echo $this->print_lib->set_page_length_in_lines(33); else {
	echo $this->print_lib->set_page_length_in_lines(34);
	$this->session->set_userdata('printed', 'yes');
}
echo print_lib::align_right($idnumber) . "\n";?>
<?php echo print_lib::align_right($name) . "\n"; ?>
<?php echo print_lib::align_right($course_year) . "\n"; ?>
<?php echo print_lib::align_right($term) . "\n"; ?>
<?php echo print_lib::align_right('Class Schedule') . "\n"; ?>
<?php // echo $this->print_lib->condensed(); ?>
<?php echo $this->print_lib->draw_table(140); ?>
<?php echo "\n" . $this->print_lib->align_right('Total Units: ', 46) . " " . number_format($total_units,1) ." / Paying Units: (" . number_format($total_paying_units, 2) . ")"; ?>
<?php echo "\n\n\n"?>
<?php echo $this->print_lib->align_right('________________________', 78); ?>
<?php echo "\n" . $this->print_lib->align_right('Registrar      ', 78); ?>
<?php echo "\n\n\n\nStudent's Class Schedule printed by HNUMIS. " . date(DATE_RFC822) . "\n\n"; ?>

</div>

<div class="container span11 clearfix" style="min-height: 50px">
	<input type="button" class="btn" id="print_schedule" value="Print Current Schedule" />
</div>