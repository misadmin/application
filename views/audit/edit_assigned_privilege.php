<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
	width:100%;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

  <div style="background:#FFF; ">

<?php
	if($privilege_availed->posted == 'Y') {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td style="text-align:center; font-weight:bold;">STUDENT'S PRIVILEGE ALREADY POSTED! </td>
  </tr>
</table>
<?php
	} else {
?>  
<form method="post" id="edit_privilege_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value= "edit_privilege_availed"/>
  <input type="hidden" name="paying_units" value="paying_units"/>
  <input type="hidden" name="tuition_fee_basic" value="<?php print($tuition_fee_basic); ?>"/>
  <input type="hidden" name="tuition_fee_other" value="<?php print($tuition_fee_other); ?>"/>
  <input type="hidden" name="paying_units_enrolled" value="<?php print($paying_units_enrolled); ?>"/> 
  <input type="hidden" name="paying_units_others" value="<?php print($paying_units_others); ?>"/> 
  <input type="hidden" name="year_level" value="<?php print($year_level); ?>"/> 	
  <input type="hidden" name="tot_amount_discount" value="<?php print($total_amount_availed); ?>"/> 	
  <input type="hidden" name="prev_tuition_disc" value="<?php print($privilege_availed->discount_amount); ?>"/> 														
	
  <?php
	if (isset($privilege_availed)) {
  ?>
  <input type="hidden" name="privilege_availed_id" value="<?php print($privilege_availed->id); ?>" > 
	<?php
		}
	?>  
    <table align="center" cellpadding="0" cellspacing="0" class="head1" style="width:42%;">
      <tr class="head">
        <td colspan="3" class="head"> POST PRIVILEGES</td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside">
           <tr>
            <td width="129" valign="middle">Term</td>
            <td width="3" valign="middle">:</td>
            <td width="63" valign="middle"> <?php print($term->term." ".$term->sy); ?> </td>
          </tr>	   
		   <tr>
            <td valign="middle">Privilege</td>
            <td valign="middle">:</td>
            <td valign="middle">
			<select name="privilege_id" class="form">
              <?php
					foreach($privilege_items AS $privilege_item) {
						if($privilege_item->id == $privilege_availed->scholarships_id){
							print("<option value=".$privilege_item->id." selected>".$privilege_item->scholarship."</option>");
						}else {
							print("<option value=".$privilege_item->id.">".$privilege_item->scholarship."</option>");
						}	
					}
				?>
            </select>
			</td>
          </tr>
		   <tr>
            <td valign="middle"> Tuition Percentage  </td>
            <td valign="middle">:</td>
            <td style="padding-top:10px; vertical-align:top; "><input name="percentage" type="text" class="form1" id="percentage" 
			          value = "<?php print($privilege_availed->discount_percentage * 100);?>" style="width:50px;"/> %</td>
          </tr>
		   <tr>
		     <td valign="middle">Units</td>
		     <td valign="middle">:</td>
		     <td valign="middle"><?php print($paying_units); ?></td>
	        </tr>
			<tr>
		     <td valign="middle">Tuition Discount</td>
		     <td valign="middle">:</td>
		     <td valign="middle"><?php print(number_format($privilege_availed->discount_amount,2)); ?></td>
	      
		   <tr>
		      <td colspan="3" valign="middle"><input type="submit" name="action" id="edit_privilege" value="Edit Privilege!"  class="btn btn-success" /></td>
			</tr>
			  
        </table></td>
      </tr>
    </table>
  </form>
<?php			
	}
?>

  </div>

<script>
$('#edit_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue edit privilege?");

	if (confirmed){
		$('#edit_privilege_form').submit();
	}		
});
</script>