<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666;  margin-bottom:10px;">
 <h2 class="heading">Withdrawal Schedule </h2>
  <table align="left" cellpadding="2" cellspacing="0" style="width:35%; border:solid; border-width:1px; border-color:#060; margin-top:1px; margin-left:20px;">
 <?php 
 if ($withdraw_schedule) { ?>
     <tr>
        	<td width="199">Academic Year</td>
        	<td width="15">:</td>
            <td width="438"><?php print($academic_year); ?></td>
    </tr>
<tr>
  <td>Academic Term</td>
  <td>:</td>
  <td><?php print($academic_term); ?></td>
</tr>

<tr>
  <td>Terms</td>
  <td>:</td>
  <td><?php print($terms); ?></td>
</tr>
<tr>
  <td>Levels</td>
  <td>:</td>
  <td><?php print($level); ?></td>
</tr>
  </table>
  <br />
  <br />
<br />
</div>
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666;  margin-top:70px; ">
  <table width="90%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:40%; padding:0px; margin:20px;">
  <thead>
    <tr>
      <td width="30%" align="center">Start Date </td>
      <td width="30%" align="center">End Date </td>
      <td width="40%" align="center">Percentage</td>
	  <td width="20%" align="center">Details</td>
	   <td width="20%" align="center">Delete</td>
    </tr>
</thead>	
    <?php
	
		foreach ($withdraw_schedule AS $withdraw_sched) {
	?>
	    	<tr>
			  <td align="left" valign="top"><?php print($withdraw_sched->date_start); ?></td>
			  <td><?php print($withdraw_sched->date_end); ?></td>
			  <td><?php print($withdraw_sched->percent); ?></td>
			   <td style="text-align:center; vertical-align:middle; ">
    			  <a class="edit_withdraw_sked" href="<?php print($withdraw_sched->id); ?>"><i class="icon-edit"></i></a></td>
    		  <td style="text-align:center; vertical-align:middle; ">
               <a class="delete_withdraw_sked" href="<?php print($withdraw_sched->id); ?>"><img src="<?php print(base_url('assets/img/icon-recycle.gif'));?>" /></a>
                    		  
		     </td>	  
    	    </tr>
    <?php
		}
	}else{ ?>
		 <tr> 
		    <td colspan="6">No withdrawal schedule yet</td>
		</tr>	
	<?php }?>
  </table>
</div>

  <form id="edit_withdraw_sked_form" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" />
  <input type="hidden" name="academic_terms_id" value="<?php print($academic_terms_id); ?>" />
  <input type="hidden" name="term_id" value="<?php print($term_id); ?>" />
  <input type="hidden" name="level_id" value="<?php print($level_id); ?>" />
</form>
<script>
	$('.edit_withdraw_sked').click(function(event){
		event.preventDefault(); 
		var withdraw_sked_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'withdraw_sked_id',
		    value: withdraw_sked_id,
		}).appendTo('#edit_withdraw_sked_form');
		$('#edit_withdraw_sked_form').submit();
	});
</script>

<form id="delete_sked" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="step" value="5" />
	   <input type="hidden" name="academic_terms_id" value="<?php print($academic_terms_id); ?>" />
       <input type="hidden" name="term_id" value="<?php print($term_id); ?>" />
       <input type="hidden" name="level_id" value="<?php print($level_id); ?>" />	
	</form>			

<script>
$('.delete_withdraw_sked').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to remove withdraw schedule?");

	if (confirmed){
		var withdraw_sked_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'withdraw_sked_id',
		    value: withdraw_sked_id,
		}).appendTo('#delete_sked');
		$('#delete_sked').submit();
	}		
});
</script>
