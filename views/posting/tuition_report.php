
<style>
/*
 * Imageless CSS3 Treeview with Checkbox Support
 * @namespace window.AcidJs
 * @class CSS3Treeview
 * @version 3.0
 * @author Martin Ivanov
 * @url developer website: http://wemakesites.net/
 * @url developer twitter: https://twitter.com/#!/wemakesitesnet
 * @url developer blog http://acidmartin.wordpress.com/
 **/
 
/*
 * Do you like this solution? Please, donate:
 * https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=QFUHPWJB2JDBS
 **/
 
.acidjs-css3-treeview,
.acidjs-css3-treeview *
{
    padding: 0;
    margin: 0;
    list-style: none;
}
 
.acidjs-css3-treeview label[for]::before,
.acidjs-css3-treeview label span::before
{
    content: "\25b6";
    display: inline-block;
    margin: 2px 0 0;
    width: 13px;
    height: 13px;
    vertical-align: top;
    text-align: center;
    color: #e74c3c;
    font-size: 8px;
    line-height: 13px;
}
 
.acidjs-css3-treeview li ul
{
    margin: 0 0 0 12px;
}
 
.acidjs-css3-treeview *
{
    vertical-align: middle;
}
 
.acidjs-css3-treeview
{
    font: normal 11px/16px "Segoe UI", Arial, Sans-serif;
}
 
.acidjs-css3-treeview li
{
    -webkit-user-select: text;
    -moz-user-select: text;
    user-select: text;
}
 
.acidjs-css3-treeview input[type="checkbox"]
{
    display: none;
}
 
.acidjs-css3-treeview label
{
    cursor: pointer;
}
 
.acidjs-css3-treeview label[for]::before
{
    -webkit-transform: translatex(-24px);
    -moz-transform: translatex(-24px);
    -ms-transform: translatex(-24px);
    -o-transform: translatex(-24px);
    transform: translatex(-24px);
}
 
.acidjs-css3-treeview label span::before
{
    -webkit-transform: translatex(16px);
    -moz-transform: translatex(16px);
    -ms-transform: translatex(16px);
    -o-transform: translatex(16px);
    transform: translatex(16px);
}
 
.acidjs-css3-treeview input[type="checkbox"][id]:checked ~ label[for]::before
{
    content: "\25bc";
}
 
.acidjs-css3-treeview input[type="checkbox"][id]:not(:checked) ~ ul
{
    display: none;
}
 
.acidjs-css3-treeview label:not([for])
{
    margin: 0 4px 0 0;
}
 
.acidjs-css3-treeview label span::before
{
    content: "";
    border: solid 1px #1375b3;
    color: #1375b3;
    opacity: .50;
}
 
.acidjs-css3-treeview label input:checked + span::before
{
    content: "\2714";
    box-shadow: 0 0 2px rgba(0, 0, 0, .25) inset;
    opacity: 1;
}

</style>

<div style="width:115%;">
<div style="padding-top: 30px; padding-bottom:20px; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; width:60%; text-align:center;">
<div style="text-align:right; font-size:12px; font-weight:normal;"><a class="download_report" href="" 
		academic_terms_id="<?php print($term->id); ?>" 
		academic_years_id="<?php print($term->academic_years_id); ?>" 
		colleges_id="<?php print($colleges_id); ?>" 
		yr_level="<?php print($yr_level); ?>" 
		program_groups_id="<?php print($students->program_groups_id); ?>"
		post_status="<?php print($post_status); ?>" >Download to PDF<i class="icon-download-alt"></i></a></div>
ASSESSMENT REPORT 
	<?php
		print(" [By ".ucfirst(strtolower($report_type))."]<br>FOR ".$header."<br>".$term->term." ".$term->sy."<br>");
	?>
	<span style="color:#FF0000;">
		<?php 
			print($post_description);
		?> 	
	</span>
	<table border="0" align="center" cellpadding="0" cellspacing="0" class="table" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; width:120%">
      <tr style="background:#DAFED3; padding:10px;">
        <td colspan="4" style="font-weight:bold;"># of Students: <?php print($students->numEnroll); ?> </td>
      </tr>
      <tr>
        <td colspan="4" style="font-weight:bold; background:#EEEEEE;">Tuition & REED Fees: <?php print($students->total_units);	?>	</td>
      </tr>
	  <?php	
	  		$tuition_fee = 0;
	  		if ($tuition_basics) {
	  			$basic_total = 0;
	  			$node = 0;
	  			foreach ($tuition_basics AS $tuition_basic) {
	  				$node++;
	  ?>
      <tr>      	
        <td width="70%" style="padding-left:25px;">
			<div class="acidjs-css3-treeview">
				<ul>
					<li><input type="checkbox" id="node-<?php echo $node; ?>" /><label for="node-<?php echo $node; ?>">Tuition Fee [<?php print($tuition_basic->program.' - '.$tuition_basic->total_units." @ ".$tuition_basic->rate."] <i>(".$tuition_basic->numEnroll." students)</i>"); ?></label>
	                	<ul>
	                		<?php

	                			if($report_type=='GROUP') {
		                			$stud_list = $this->Tuition_Report_Model->ListStudentsByProgramGroupsID($term->id,$tuition_basic->program_groups_id, $yr_level, $tuition_basic->program, $tuition_basic->rate);
		                		} elseif ($report_type=='COLLEGE') {
		                			$stud_list = $this->Tuition_Report_Model->ListStudentsByProgramID($term->id,$colleges_id, $yr_level, $tuition_basic->program, $tuition_basic->rate);
		                		}


	                			if($stud_list){
	                				$st_count = 1;
	                				foreach ($stud_list as $row){
	                		?> 
	                    	<li><font face="Courier New" size=2 color="blue"><?php echo str_pad($st_count++,4,'0',STR_PAD_LEFT).'.  '.$row->course_yr.'  '.$row->numEnroll.'  '.str_pad($row->name,36,'_',STR_PAD_RIGHT).' '.$row->total_units; ?></font></li>
	                    <?php } } ?>
	                	</ul>
					</li>
				</ul>
	     	</div>
		</td>
        <td width="10%" style="padding-left:20px; text-align:right;">
          <?php
		  		$basic_total =  $basic_total + ($tuition_basic->total_units * $tuition_basic->rate);
				print(number_format(($tuition_basic->total_units * $tuition_basic->rate),2));
	?>        </td>
        <td width="10%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
      </tr>
	  <?php
	  			}
	  ?>
      <tr>
        <td style="padding-left:20px;">Tuition Fee </td>
        <td>&nbsp;</td>
        <td align="right" style="padding-left:20px; text-align:right;">
          <?php
				$tuition_fee = $tuition_fee + ($basic_total);	  
				print(number_format($basic_total,2));
	    ?>        </td>
        <td>&nbsp;</td>
      </tr>
	  <?php
	  		}
	  	if ($tuition_others) {
			foreach($tuition_others AS $tuition) {
	  ?>
      <tr>
        <td style="padding-left:20px;"><?php print($tuition->course_code." Fee [".$tuition->paying_units." @ ".$tuition->rate."] <i>(".$tuition->enrollees." students)</i>"); ?></td>
        <td>&nbsp;</td>
        <td align="right" style="padding-left:20px; text-align:right;">
		<?php
				$total_others = $tuition->paying_units * $tuition->rate;
				$tuition_fee = $tuition_fee + $total_others;
				print(number_format($total_others,2));
	    ?></td>
        <td>&nbsp;</td>
      </tr>
	  <?php
	  		}
		}
	  ?>

	  <?php
	    // TUITION OTHERS EXTRACTED FROM REGULAR TUITION BASIC
	    // This display, does not in any way, affect the amounts
	    // displayed on this report.  This grouping by extraction
	    // is used for accounting purposes only..  Toyet 9.18.2018
	    // - will only appear if there is no $tuition_others defined
	    //   in the Fees Schedule
	    if(!isset($nTotal_others_extracted))
	    	$nTotal_others_extracted = 0;
	  	if (!$tuition_others and $tuition_others_extracted) {
	  		$nTotal_others_extracted = 0;
	  		//log_message("INFO",print_r($tuition_others_extracted,true));
			foreach($tuition_others_extracted AS $tuition_other2) {
				$node++;
	  ?>
      <tr>
        <!--<td width="45%" style="padding-left:20px;"><font color="red"><?php print("@".$tuition_other2->course_yr." Fee [".$tuition_other2->total_units." @ ".$tuition_other2->rate."] <i>(".$tuition_other2->numEnroll." students)</i>"); ?></font></td>-->
        <td width="70%" style="padding-left:25px;">
			<div class="acidjs-css3-treeview">
				<ul>
					<li><input type="checkbox" id="node-<?php echo $node; ?>" /><label for="node-<?php echo $node; ?>"><?php print($tuition_other2->course_code.' |<em>'.$tuition_other2->program."</em>|-[".$tuition_other2->total_units." @ ".$tuition_other2->rate."] <i>(".$tuition_other2->numEnroll." students)</i>"); ?></label>
	                	<ul>
	                		<?php
	                		    //log_message("INFO",print_r($tuition_other2,true));
	                			if($report_type=='GROUP') {
		                			$stud_list = $this->Tuition_Report_Model->ListStudentsByProgramGroupsID2($term->id,$tuition_basic->program_groups_id,($tuition_basic->year_level<>1 ? 0:1),$tuition_other2->course_code,$tuition_other2->rate,$tuition_other2->program);
		                		} elseif($report_type=='COLLEGE') {
		                			$stud_list = $this->Tuition_Report_Model->ListStudentsByProgramID2($term->id,$tuition_basic->program_groups_id,$tuition_other2->year_level,$tuition_other2->course_code,$tuition_other2->rate,$tuition_other2->program);
		                		}

	                			if($stud_list){
	                				$st_count = 1;
	                				foreach ($stud_list as $row){
	                		?> 
	                    	<li><font face="Courier New" size=2 color="blue"><?php echo str_pad($st_count++,4,'0',STR_PAD_LEFT).'.  '.$row->course_yr.'  '.$row->numEnroll.'  '.str_pad($row->name,36,'_',STR_PAD_RIGHT).' '.$row->total_units; ?></font></li>
	                    <?php } } ?>
	                	</ul>
					</li>
				</ul>
	     	</div>
		</td>
        <td align="right" style="padding-left:20px; text-align:right;">
		<font color="red"><?php
				$total_others = $tuition_other2->total_units * $tuition_other2->rate;
				$nTotal_others_extracted += $total_others;
				print(number_format($total_others,2));
	    ?></font></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <?php
	  		}
		}
	  ?>

	  <tr>
	  	<td width="45%" style="padding-left:20px;"><font color="red">Total Others</font></td>	  	
	  	<td></td>	  	
	  	<td align="right" style="padding-left:20px; text-align:right;"><font color="red"><?php print(number_format($nTotal_others_extracted,2)); ?></font></td>	
	  	<td></td>	  	
	  </tr>


      <tr>
        <td style="padding-left:20px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="padding-left:20px; text-align:right;">&nbsp;</td>
        <td style="padding-left:20px; text-align:right;">
		<?php
			$tuition_fee = $tuition_fee+$nTotal_others_extracted;
			print(number_format($tuition_fee,2));
		?>		</td>
      </tr>
      <tr>
        <td colspan="4" style="font-weight:bold; background:#EEEEEE;">Matriculation, Misc &amp; Other Fees: </td>
      </tr>
	  <?php
	  		if ($matriculation) {
	  			$matri_total = 0;
				foreach ($matriculation AS $matri) {
	  ?>
      <tr>
        <td style="padding-left:20px;">Matriculation @ 
          <?php
		print($matri->rate);
	?></td>
        <td>&nbsp;</td>
        <td style="padding-left:20px; text-align:right;">
		<?php
			$matri_total = $matri_total + ($matri->numEnroll*$matri->rate);
			print(number_format($matri->numEnroll*$matri->rate,2));
	    ?>		</td>
        <td style="padding-left:20px; text-align:right;">&nbsp;</td>
      </tr>
	  <?php
	  			}
		?>
      <tr>
        <td style="padding-left:20px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="padding-left:20px; text-align:right;">&nbsp;</td>
        <td style="padding-left:20px; text-align:right;">
		<?php
			$tuition_fee = $tuition_fee + ($matri_total);
			print(number_format($matri_total,2));
	    ?></td>
      </tr>
	  <?php
	  		}
			
	  		if ($misc_fees) {
	  ?>
      <tr>
        <td colspan="4" style="padding-left:20px;">Miscellaneous Fees </td>
      </tr>
	  <?php
	  			$misc_total = 0;
	  			foreach($misc_fees AS $misc_fee) {
		?>
      <tr>
        <td style="padding-left:35px;"><?php print($misc_fee->description." @ ".$misc_fee->rate." <i>(".$misc_fee->enrollees." students)</i>"); ?></td>
        <td>&nbsp;</td>
        <td style="text-align:right;">
		<?php
			$misc_total = $misc_total + ($misc_fee->enrollees*$misc_fee->rate);
			print(number_format($misc_fee->enrollees*$misc_fee->rate,2));
	    ?></td>
        <td style="text-align:right;">&nbsp;</td>
      </tr>
	  <?php
	  			}
	  ?>
      <tr>
        <td style="padding-left:35px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			$tuition_fee = $tuition_fee + ($misc_total);
			print(number_format($misc_total,2));
	    ?></td>
      </tr>
	  <?php
			}

	  		if ($other_fees) {
	  ?>
      <tr>
        <td colspan="4" style="padding-left:20px;">Other School Fees </td>
      </tr>
	  <?php
	  			$others_total = 0;
	  			foreach($other_fees AS $other) {
		?>
      <tr>
        <td style="padding-left:35px;"><?php print($other->description." @ ".$other->rate." <i>(".$other->enrollees." students)</i>"); ?></td>
        <td>&nbsp;</td>
        <td style="text-align:right;">
		<?php
			$others_total = $others_total + ($other->enrollees*$other->rate);
			print(number_format($other->enrollees*$other->rate,2));
	    ?>		</td>
        <td style="text-align:right;">&nbsp;</td>
      </tr>
	  <?php
	  			}
	  ?>
	  <tr>
        <td style="padding-left:35px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			$tuition_fee = $tuition_fee + ($others_total);
			print(number_format($others_total,2));
	    ?></td>
      </tr>
	  <?php
	  		}

	  		if ($learning_resources_fees) {
	  			?>
	  		      <tr>
	  		        <td colspan="4" style="padding-left:20px;">Learning Resources Fees </td>
	  		      </tr>
	  			  <?php
	  			  			$learning_total = 0;
	  			  			foreach($learning_resources_fees AS $learning) {
	  				?>
	  		      <tr>
	  		        <td style="padding-left:35px;"><?php print($learning->description." @ ".$learning->rate." <i>(".$learning->enrollees." students)</i>"); ?></td>
	  		        <td>&nbsp;</td>
	  		        <td style="text-align:right;">
	  				<?php
	  					$learning_total = $learning_total + ($learning->enrollees*$learning->rate);
	  					print(number_format($learning->enrollees*$learning->rate,2));
	  			    ?>		</td>
	  		        <td style="text-align:right;">&nbsp;</td>
	  		      </tr>
	  			  <?php
	  			  			}
	  			  ?>
	  			  <tr>
	  		        <td style="padding-left:35px;">&nbsp;</td>
	  		        <td>&nbsp;</td>
	  		        <td style="text-align:right;">&nbsp;</td>
	  		        <td style="text-align:right;"><?php
	  					$tuition_fee = $tuition_fee + ($learning_total);
	  					print(number_format($learning_total,2));
	  			    ?></td>
	  		      </tr>
	  	<?php
	  		}
	  			  		
	  		if ($student_support_fees) {
	  	?>
	  		      <tr>
	  		        <td colspan="4" style="padding-left:20px;">Student Support Services</td>
	  		      </tr>
	    <?php
	   			$support_total = 0;
	   			foreach($student_support_fees AS $support) {
	  	?>
	   		      <tr>
	   		        <td style="padding-left:35px;"><?php print($support->description." @ ".$support->rate." <i>(".$support->enrollees." students)</i>"); ?></td>
	  		        <td>&nbsp;</td>
	  		        <td style="text-align:right;">
	  				<?php
	  					$support_total = $support_total + ($support->enrollees*$support->rate);
	  					print(number_format($support->enrollees*$support->rate,2));
	  			    ?>		</td>
		  		        <td style="text-align:right;">&nbsp;</td>
	  		      </tr>
	  			  <?php
 			  			}
	  			  ?>
	  			  <tr>
	  		        <td style="padding-left:35px;">&nbsp;</td>
	  		        <td>&nbsp;</td>
	  		        <td style="text-align:right;">&nbsp;</td>
	  		        <td style="text-align:right;"><?php
	  					$tuition_fee = $tuition_fee + ($support_total);
	  					print(number_format($support_total,2));
	  			    ?></td>
	  		      </tr>
	  <?php
	  		}
 			  			  			  		
	  		
			if ($addl_other_fees) {
	  ?>		
	  <tr>
        <td colspan="4" style="padding-left:20px;">Additional Other Fees </td>
      </tr>
	  <?php
	  			$addl_total = 0;
	  			foreach($addl_other_fees AS $addl) {
	  ?>
      <tr>
        <td style="padding-left:35px;"><?php print($addl->description." @ ".$addl->rate." <i>(".$addl->enrollees." ".$addl->yr_level." Year students)</i>"); ?></td>
        <td>&nbsp;</td>
        <td style="text-align:right;">
		<?php
			$addl_total = $addl_total + ($addl->enrollees*$addl->rate);
			print(number_format($addl->enrollees*$addl->rate,2));
	    ?>		</td>
        <td style="text-align:right;">&nbsp;</td>
      </tr>
	  <?php
	  			}
	  ?>
	  <tr>
        <td style="padding-left:35px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			$tuition_fee = $tuition_fee + ($addl_total);
			print(number_format($addl_total,2));
	    ?></td>	  
	  </tr>
	  <?php
	  		}
			
			if ($lab_fees) {
		?>
      <tr>
        <td colspan="4" style="font-weight:bold; background:#EEEEEE;">Laboratory Fees: </td>
      </tr>
	  <?php
	  			$lab_total = 0;
	  			foreach($lab_fees AS $lab_fee) {
	  ?>
      <tr>
        <td style="padding-left:20px;"><?php print($lab_fee->course_code." @ ".$lab_fee->rate." <i>(".$lab_fee->enrollees." students)</i>"); ?></td>
        <td>&nbsp;</td>
        <td style="text-align:right;">
			<?php 
				$lab_total = $lab_total + $lab_fee->fees;
				print(number_format($lab_fee->fees,2)); ?></td>
        <td style="text-align:right;">&nbsp;</td>
      </tr>
      
	  <?php
	  			}
	  ?>			
	<tr>
        <td style="padding-left:20px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			$tuition_fee = $tuition_fee + ($lab_total);
			print(number_format($lab_total,2));
	    ?></td>
      </tr>
	  <?php
	  		}
	  		
			if ($affil_fees) {
		?>
      <tr>
        <td colspan="4" style="font-weight:bold; background:#EEEEEE;">Affiliation Fees: </td>
      </tr>
	  <?php
	  			$affil_total = 0;
	  			foreach($affil_fees AS $affil) {
	  ?>
      <tr>
        <td style="padding-left:20px;"><?php print($affil->description." @ ".$affil->rate." <i>(".$affil->enrollees." students)</i>"); ?></td>
        <td>&nbsp;</td>
        <td style="text-align:right;">
			<?php 
				$affil_total = $affil_total + $affil->fees;
				print(number_format($affil->fees,2)); ?></td>
        <td style="text-align:right;">&nbsp;</td>
      </tr>
      
	  <?php
	  			}
	  ?>			
	<tr>
        <td style="padding-left:20px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			$tuition_fee = $tuition_fee + ($affil_total);
			print(number_format($affil_total,2));
	    ?></td>
      </tr>
	  <?php
	  		}
	  		
	  ?>
	<tr style="background:#DAFED3; font-weight:bold; font-size:14px;">
        <td>TOTAL Assessment: </td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			print(number_format($tuition_fee,2));
	    ?></td>
      </tr>
    </table>

	<div style="text-align:right; font-size:12px; font-weight:normal;"><a class="download_report" href="" 
		academic_terms_id="<?php print($term->id); ?>" 
		academic_years_id="<?php print($term->academic_years_id); ?>" 
		colleges_id="<?php print($colleges_id); ?>" 
		yr_level="<?php print($yr_level); ?>" 
		program_groups_id="<?php print($students->program_groups_id); ?>"
		post_status="<?php print($post_status); ?>" >Download to PDF<i class="icon-download-alt"></i></a></div>
    
  </div>


</div>

  <form id="download_tuition_report_form" method="post" target="_blank">
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.download_report').click(function(event){
		event.preventDefault(); 
		var academic_terms_id = $(this).attr('academic_terms_id');
		var academic_years_id = $(this).attr('academic_years_id');
		var colleges_id = $(this).attr('colleges_id');
		var yr_level = $(this).attr('yr_level');
		var program_groups_id = $(this).attr('program_groups_id');
		var post_status = $(this).attr('post_status');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_terms_id',
			value: academic_terms_id,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_years_id',
			value: academic_years_id,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'colleges_id',
			value: colleges_id,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'yr_level',
			value: yr_level,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'program_groups_id',
			value: program_groups_id,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'post_status',
			value: post_status,
		}).appendTo('#download_tuition_report_form');
		
		$('#download_tuition_report_form').submit();
		
	});
</script>


<script>
$(".acidjs-css3-treeview").delegate("label input:checkbox", "change", function() {
    var
        checkbox = $(this),
        nestedList = checkbox.parent().next().next(),
        selectNestedListCheckbox = nestedList.find("label:not([for]) input:checkbox");
 
    if(checkbox.is(":checked")) {
        return selectNestedListCheckbox.prop("checked", false);
    }
    selectNestedListCheckbox.prop("checked", true);
});
</script>