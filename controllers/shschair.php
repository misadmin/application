<?php 

class Shschair extends MY_Controller {
	

	public function __construct(){
		parent::__construct();		
		$this->content_lib->set_title ('SHS Chair | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_footer_script('validate');
		$this->content_lib->enqueue_footer_script('autocomplete');
		$this->content_lib->enqueue_footer_script('jquery-ui');
		
		if ($this->session->userdata('role')=='shschair'){
			$this->navbar_data['menu'] = array(
					'Students'=>array('Students' => 'student',
									),
					'Sections'=>array(
									'Section Management' => 'section_management'
									),				
					'Chair Management'=>array(
									'Track Management' => 'track_management',
									'Strand Management' => 'strand_management',
									'Courses Management' => 'courses_management',
									'Prospectus Management' => 'prospectus_management',
									'School Days Management' => 'school_days',
									),	
					'Reports'=>array(
									'Students Registered-Not Enrolled'=>'registered_not_enrolled',
									'Students Enrolled-Not Sectioned'=>'enrolled_not_sectioned',
									'Enrollment Summary'=>'shs_enrollment_summary',
									'Teacher Loads'=>'teacher_loads',
									'Grades Submission Report'=>'grades_submission_report',
									),
			);
		}
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		
	}
	

	public function student(){
			
		$this->content_lib->set_title('SHS Chair | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		
		if ( ! empty($query)){
			
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
			
			$this->load->library('pagination_lib');
			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			}
		}
		
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->helper('student_helper');
			$this->load->model('teller/teller_model');
			
			$this->content_lib->enqueue_header_style('image_area_select');
			$this->content_lib->enqueue_footer_script('image_area_select');
			$port = substr($idnum, 0, 3);
			$this->content_lib->set_title ('SHS Chair | ' . $idnum);
			
			$result = $this->student_common_model->my_information($idnum);	

			$tab = 'student_info';
				
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->mname. " " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> "Grade ".$result->year_level." ".$result->section_name,
						'full_home_address'	=> $result->full_home_address,
						'full_city_address' => $result->full_city_address,
						'phone_number' 		=>$result->phone_number,
						'section'			=> $result->section,
				);

				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}					

				$dcontent['signature'] = base_url('assets/img/signature.png');
				
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);

				$shs_levels = array(11,12); 
				
				if (!is_college_student($idnum) OR in_array($result->year_level, $shs_levels)) {

					$this->load->model('accounts/accounts_model');
					$this->load->model('financials/Payments_Model');
					$this->load->model('hnumis/shs/track_strand_model');
					$this->load->model('hnumis/shs/section_model');
					$this->load->model('hnumis/shs/shs_student_model');
					$this->load->model('hnumis/shs/shs_grades_model');
					$this->load->model('hnumis/shs/behavior_model');
					$this->load->model('hnumis/shs/attendance_model');
					$this->load->model('hnumis/prospectus_model');  // added Toyet 9.19.2018
					$this->load->library('shs/shs_student_lib');

					$data['strands']        = $this->track_strand_model->ListStrandsByLatestProspectus();
					$data['acad_terms']     = $this->shs_student_model->My_AcadTerms_toRegister($idnum);
					$data['term']       	= $this->academic_terms_model->getCurrentAcademicTerm(); 
					$data['is_registered']  = FALSE;
					$data['academic_terms_id'] = $data['term']->id;

					
					if (in_array($result->year_level, $shs_levels)) { //student is SHS

						if ($this->input->post('action')) {
							if ($this->common->nonce_is_valid($this->input->post('nonce'))){
								switch ($this->input->post('action')){
									case 'change_password'	:
													
										$this->load->model('student_common_model');
										$idno = $this->input->post('student_id');
										$newpassword = $this->input->post('password');
															
										if ($this->student_common_model->update_information($idno, array('password'=>$newpassword))) {
											$this->content_lib->set_message('Password successfully updated! New password is: '.$newpassword, 'alert-success');
										} else {
											$this->content_lib->set_message('ERROR: Password NOT updated!', 'alert-error');
										}
													
										break;
								}
								
							} else {
								$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							}
						}

						
						$data['idnum']      	= $idnum;
						$data['prospectus_list'] = $this->prospectus_model->listSHSProspectuses();

						$data['academic_terms'] = $this->academic_terms_model->student_inclusive_academic_terms($idnum);

						$result2            	= $this->teller_model->get_student($idnum);  
						$data['payments']   	= $this->Payments_Model->ListStudentPayments($result2->payers_id); 
					
						$latest_history = $this->shs_student_model->get_StudentHistory_id($idnum, $data['term']->id); 
						//log_message("INFO",print_r($latest_history,true)); // Toyet 9.20.2018
						if (!$latest_history) {
							$latest_history = $this->shs_student_model->get_StudentHistory_id($idnum, $data['academic_terms'][0]->id); 							
						}
						$data['totally_withdrawn'] = $latest_history->totally_withdrawn;
						$data['terms_to_enroll']    = $this->shs_student_model->ListTermsToEnroll($idnum);
						
						$data['block_sections']     = NULL;
						$data['active_tab']         = "class_schedules";
						$data['active_tab1']        = NULL;

						//$data['is_registered']     = FALSE;
						$data['is_enrolled']       = FALSE;	
						$data['is_sectioned']      = FALSE;
						$data['current_register']  = FALSE;
						
						$data['student_histories_id'] = $latest_history->id;
						$data['prospectus_assigned'] = $this->shs_student_model->getCurrentProspectusID($latest_history->id);

						//log_message("INFO",'kani gud'.print_r($data['prospectus_assigned'],true)); // Toyet 9.19.2018

						if ($latest_history->academic_terms_id == $data['term']->id) {
							$data['is_registered']    = TRUE;
							$data['current_register'] = TRUE;
						}


						if($latest_history->can_enroll == 'Y') {
							$data['is_enrolled']           = TRUE;
							$data['to_sections']           = $this->section_model->ListSections($latest_history->year_level,$latest_history->academic_terms_id,$data['strands'][0]->academic_programs_id);
							$data['current_prospectus_id'] = $latest_history->prospectus_id;
						} else {
							$data['is_enrolled'] = FALSE;		
							$data['to_sections'] = NULL;								
						}
							
						if (is_null($latest_history->block_sections_id)) {
							$data['is_sectioned']   = FALSE;
							$data['block_sections'] = $this->section_model->ListSections($latest_history->year_level,$latest_history->academic_terms_id,$latest_history->academic_programs_id);
						} else {
							$data['is_sectioned']      = TRUE;
							$data['transfer_sections'] = $this->section_model->ListSections($latest_history->year_level,$latest_history->academic_terms_id,$latest_history->academic_programs_id);
							$data['current_section_id']= $latest_history->block_sections_id;
						}					
							
						$data['year_level']        = $latest_history->year_level;
						$data['current_strand_id'] = $latest_history->prospectus_id;
							
						$data['class_schedules'] = $this->shs_student_model->ListClassSchedules($latest_history->id); 					
						$data['grades']          = $this->shs_grades_model->List_Student_Grades($idnum); 
						$data['selected_term']   = $data['term']->id;

						$data['observed_values'] = $this->behavior_model->ListObservedValues($latest_history->id);
								
						$data['can_update_values'] = FALSE; //used to check if can update baheior values

						$data['assess'] = $this->shs_student_lib->StudentAssessments($latest_history); 

						$data['student_type'] = "shs";
						$data['show_pulldown']   = TRUE;
					
						$data['school_days']  = $this->attendance_model->ListSchoolDays($data['academic_terms'][0]->id);
						$data['present_days'] = $this->attendance_model->ListPresentDays($data['academic_terms'][0]->id, $latest_history->id);
						
						$data['grade_reports'] = $this->shs_grades_model->List_Grades_For_Report_Cards($idnum, $data['academic_terms'][0]->id); 
	
						$from = new DateTime($result->dbirth);
						$to   = new DateTime('today');

						$data['my_student']   = array('name'=>strtoupper($result->lname.', '.$result->fname.' '.$result->mname),
													'gender'=>$result->gender,
													'age'=>$from->diff($to)->y,
													'grade'=>$latest_history->grade,
													'section'=>$latest_history->section,
													'lrn'=>$latest_history->lrn,
													'block_adviser'=>$latest_history->block_adviser,
												);	
												
						/* 'Register' tab must display only if he/she is already an SHS student.
						   For new student, Register tab will no longer display because he/she is being registered 
						   by RSClerk in New Student->Senior High School.
						*/
						if ($this->shs_student_model->is_SHS_Student($idnum)) {
							$data['show_register_tab']  = TRUE;
						} else {
							$data['show_register_tab']  = FALSE;
						}
						
					} else { //student is in Basic Ed. so show only Register

						$data['student_type']       = "basic_ed";
						$data['active_tab']         = "enrollment";
						$data['active_tab1']        = "register";
						$data['current_strand_id']  = NULL;
						$data['show_register_tab']  = TRUE;

					}
					
					$this->content_lib->enqueue_body_content('shs/student_management', $data);
										
				} else {
					$this->content_lib->set_message('Student records are in College! Refer to Digital-Records in Charge (DRIC)!', 'alert-error');						
				}				
				
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}

		}
				
		$this->content_lib->content();	
	}
	
	
	public function track_management() {
		$this->load->library('shs/track_strand_lib');
	
		$this->track_strand_lib->track_management();
	}

	public function strand_management() {
		$this->load->library('shs/track_strand_lib');
	
		$this->track_strand_lib->strand_management();
	}

	public function courses_management() {
		$this->load->library('shs/course_lib');
	
		$this->course_lib->courses_management();
	}

	public function prospectus_management() {
		$this->load->library('shs/prospectus_lib');
	
		$this->prospectus_lib->prospectus_management();		
	}

	public function prospectus_courses_management($prospectus_id=null) {
		$this->load->library('shs/prospectus_lib');
	
		if (!$prospectus_id) {
			redirect('shschair/prospectus_management', 'refresh');
		} else {
			$this->prospectus_lib->prospectus_courses_management($prospectus_id);
		}
	}
	
	public function section_management() {
		$this->load->library('shs/section_lib');
	
		$this->section_lib->section_management();		
		
	}

	public function detailed_section_management($section_id=null) {
		$this->load->library('shs/section_lib');
	
		if (!$section_id) {
			redirect('shschair/section_management', 'refresh');
		} else {
			$this->section_lib->detailed_section_management($section_id);
		}
	}

	
	//NOTE: this function handles all actions for the student tabs
	public function process_student_action($idnum) {
		
		if (!$this->input->post('action')) {
			redirect('shschair/student', 'refresh');
		}
		
		$this->load->library('shs/shs_student_lib');
	
		$this->shs_student_lib->process_student_action($idnum);
		
	}


	public function registered_not_enrolled() {
		$this->load->library('shs/reports_lib');
	
		$this->reports_lib->registered_not_enrolled();		
		
	}

	public function enrolled_not_sectioned() {
		$this->load->library('shs/reports_lib');
	
		$this->reports_lib->enrolled_not_sectioned();		
		
	}


	public function shs_enrollment_summary() {
		$this->load->library('shs/reports_lib');
	
		$this->reports_lib->enrollment_summary();
			
	}
	

	public function school_days() {
		$this->load->library('shs/attendance_lib');
	
		$this->attendance_lib->school_days();		
	}
	
	public function teacher_loads() {
		$this->load->library('shs/shs_faculty_lib');
	
		$this->shs_faculty_lib->teacher_loads();		
	}


	public function grades_submission_report() {
		$this->load->library('shs/shs_faculty_lib');
	
		$this->shs_faculty_lib->grades_submission_report();		
	}

	
}

?>