<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>

<div id="show_prospectus_management_page" >
	<h2 class="heading">Prospectus Management</h2>
	<div id="rounded_header" style="width:99%; font-size:12px; margin:0 auto; margin-top:15px; padding:0px;">
		<div id="container">
			<div id="left" >
				<div id="show_error_msg" style="padding-left:10px; padding-top:10px; color:#F92424; font-weight:bold; font-size:14px;">
					
				</div>
			</div>
			<div id="right">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<div style="float:left; padding-top:4px; padding-right:4px; font-size:14px;" >
					<a href="#" class="btn btn-success add_prospectus" >
						<i class="icon-plus-sign" style="margin-top:2px;"></i> New Prospectus
					</a>
				</div>
			</div>
		</div>
	</div>

	
	<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_items" >
		<?php
			$data = array(
					"prospectuses"=>$prospectuses,
			);
			
			$this->load->view('bed/prospectus/prospectus_details',$data);					
		?>
	</div>
</div>



<script>
	$('.add_prospectus').click(function(event){
		
		$('#modal_new_prospectus').modal('show');

		$('#show_error_msg').html('');
		
		$('#effective_year_modal').val((new Date).getFullYear());
		
	});
</script>				


	<div id="modal_new_prospectus" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_prospectus_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Create Prospectus</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$data['yr_levels'] = $yr_levels;
							$this->load->view('bed/prospectus/modal_view/new_prospectus_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Create Prospectus!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<!-- JQ to submit an update to a volunteer -->
<script>
	$('#add_prospectus_form').submit(function(e) {
		e.preventDefault();
		
		var yr_level_id     = $('#strand_id_add_modal').val();
		var effective_year 	= $('#effective_year_modal').val();

		$('#modal_new_prospectus').modal('hide');
		
		$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/prospectus_management'); ?>",
			data: {"yr_level_id": yr_level_id,
					"effective_year": effective_year,
					"action": "add_prospectus" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_items').html(response.output); 
				
				$('#show_add_icon').html('');
				
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
		});
		
	});
</script>				

