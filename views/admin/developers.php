<?php 

class developers_table extends table_lib{
	
	function __construct(){
		parent::__construct();
	}
	
	function format_name($row){
		return '<a href="' . $row->id . '">' . $row->name . '</a>';
	}
	
	function format_id ($row){
		return $row->id;
	}
}

?>