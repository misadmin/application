<?php

	class Prerequisites_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
			
   		}

		
		function AddPrerequisites($data) {
				
			$q1 = "INSERT INTO
						prerequisites (prospectus_courses_id)
					VALUES ({$this->db->escape($data['prospectus_courses_id'])}) ";
			
			if ($this->db->query($q1)) {
				$data['prerequisites_id'] = $this->db->insert_id();
				
				if ($this->AddPrereq_Courses($data)) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		}
		
		
		function AddPrereq_Courses($data)	{
				
			$query = "INSERT INTO 
							prereq_courses (prerequisites_id, prospectus_courses_id)
						VALUES ({$this->db->escape($data['prerequisites_id'])},
								{$this->db->escape($data['prereq_course_id'])})";
					
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
				

		function DeletePrerequisite($id) {
				
			$query = "DELETE 
						FROM 
							prerequisites 
						WHERE
							id = {$this->db->escape($id)} ";
			
			//return $query;
			
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
						
		}

		
	}