<?php 
// echo $idno ;
// echo "\n"; 
// echo $phone_number	;
// die(); 
?>
<div class="row-fluid">
	<div class="span12">
		<form id="phone_form" method="post" action="<?php echo site_url ("{$role}/update_phone/")?>" class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="update_phone" />
			<fieldset>
				<input type="hidden" id="idno" name="idno" value=<?php echo $idno; ?> />			
				<div class="control-group formSep">
					<label for="primary_number" class="control-label">Phone Number</label>
					<div class="controls">
						<input type="text" id="primary_number" name="primary_number" class="input-xlarge" 
							value="<?php echo $phone_number; ?>"
						/>
					</div>
				</div>
<!--
				<div class="control-group formSep">
					<label for="secondary_number" class="control-label">Secondary Number</label>
					<div class="controls">
						<input type="text" id="secondary_number" name="secondary_number" class="input-xlarge" value="" />
					</div>					
				</div>		
-->						
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Update</button>
					</div>
				</div>
										
			</fieldset>
		</form>
	</div>
</div>
<script>
$(document).ready(function(){
	$('#phone_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			primary_number: { required: true},
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
});
</script>
