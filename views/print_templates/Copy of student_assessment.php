<?php
if(in_array($this->session->userdata('role'), $this->config->item('roles_allowed_to_print_assessment'))):
/*
$idnumber = '05832437';
$familyname = 'SECRETARIA';
$firstname = 'Rowin Rey';
$middlename = 'T';
$level = 3;
$course = 'BSN';

$due_now = '';
$succeeding_dues = '7,756.87';*/

$name = $familyname . ", " . $firstname . " " . substr($middlename, 0, 1) . ".";
$course_year = $level . " " . $course;


$table_size = array(
		'width'=>60,
		'height'=>count($tuition_fees_content) < 17 ? 17 : count($tuition_fees_content)
);
$length = array('fee_type'=>12, 'subject'=>20, 'units'=>6, 'hours'=>8, 'amount'=>12);
$numbers = array(0=>'units', 1=>'hours', 2=>'amount');

$tuition_fees_header = array(
		'fee_type'=>'Fee Type',
		'subject'=>'Subject',
		'units'=>'Units',
		'hours'=>'Hrs',
		'amount'=>'Amount'
);
/*
$tuition_fees_content = array(
		0=>array(
				'fee_type'=>'TUITION 2nd Sem SY 2012-2013 @ 502.22',
		),
		1=>array(
				'subject'=>'ELECTIVE 1',
				'units'=>2,
				'hours'=>2,
				'amount'=>1004.44
		),
		2=>array(
				'subject'=>'NCM 104.N',
				'units'=>5.0,
				'hours'=>5.0,
				'amount'=>2511.10
		),
		3=>array(
				'subject'=>'NCM 105.N',
				'units'=>4.0,
				'hours'=>4.0,
				'amount'=>2008.88
		),
);*/

//$tuition_fees_content = isset($tuition_fees_content) ? $tuition_fees_content : array();

$tuition_fees_content[] = array(
		'subject'=>'',
		'units'=>'____',
		'hours'=>'_____',
		'amount'=>'_________'
);

$tuition_fees_content[] = array(
		'subject'=>'Sub-total:',
		'units'=>0,
		'hours'=>0,
		'amount'=>0
);

$total_assessment = 0;

$tuition_fees = array();
$tuition_fees[] = '+' . str_pad('', $table_size['width'], '-') . '+';
$temp_head = '| ';
foreach($tuition_fees_header as $key=>$val){
	$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
	$temp_head .= str_pad($val, $length[$key], ' ', $str_pad);
}

$tuition_fees[] = str_pad($temp_head, $table_size['width']+1, ' ') . '|';
$tuition_fees[] = $tuition_fees[0];

$totals = array();
foreach($tuition_fees_content as $row=>$value){
	$output_total = FALSE;
	$i = count($tuition_fees);
	$tuition_fees[$i] = '| ';
	foreach($value as $key=>$val){
		if($key == 'subject' && $val == $tuition_fees_content[count($tuition_fees_content)-1][$key]) $output_total = TRUE;
		if($key == 'subject' && $val == ''){
			if(count($tuition_fees) < $table_size['height']-2){
				$tuition_fees[$i] .= str_pad('', $table_size['width']-1, ' ') . '|';
				for($c = count($tuition_fees); $c < $table_size['height']-3; $c++){
					$tuition_fees[] = '|' . str_pad('', $table_size['width'], ' ') . '|';
				}
				$i = $c;
				$tuition_fees[$i] = '| ';
			}
		}
		if($key == 'fee_type'){
			$tuition_fees[$i] .= str_pad($val, $table_size['width']-2, ' ');
		} else {
			$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
			if($key == 'subject') {
				$tuition_fees[$i] .= str_pad('', $length['fee_type'], ' ');
			}
			if(in_array($key, $numbers)){
				if(is_numeric($val)){
					$totals[$key] = isset($totals[$key]) ? $totals[$key] : 0;
					$totals[$key] += $val;
					$val = number_format($val, in_array($key, array('units')) ? 1 : 2);
				}
				if($output_total) {
					$val = number_format($totals[$key], in_array($key, array('units')) ? 1 : 2);
					if($key == 'amount') $total_assessment = $totals[$key];
				}
				
			}
			$tuition_fees[$i] .= str_pad(substr($val, 0, $length[$key]), $length[$key], ' ', $str_pad);
		}
	}
	$tuition_fees[$i] = str_pad($tuition_fees[$i], $table_size['width']+1, ' ') . '|';
}

$tuition_fees[] = $this->print_lib->condensed();
$tuition_fees[count($tuition_fees)-1] = $tuition_fees[0];

//print_r($tuition_fees);

//////////////////////////////////////////////////////////// TABLE 2 ////////////////////////////////////

//$table_size = array('width'=>60, 'height'=>16);
$length = array('fee_type'=>42, 'description'=>24, 'amount'=>12);
//$numbers = array(0=>'units', 1=>'hours', 2=>'amount');

$miscellaneous_fees_header = array(
		'fee_type'=>'Fee Type',
		'description'=>'Description',
		'amount'=>'Amount'
);

/*
$miscellaneous_fees_content = array(
		0=>array(
				'fee_type'=>'Matriculation Fee',
				'amount'=>483.71
		),
		1=>array(
				'fee_type'=>'Miscellaneous Fees',
				'amount'=>2612.29
		),
		2=>array(
				'fee_type'=>'Other School Fees',
				'amount'=>1175.55
		),
		3=>array(
				'fee_type'=>'Additional Othes Fees',
				'amount'=>6962.51
		),
);*/

$miscellaneous_fees_content[] = array(
		'description'=>'',
		'amount'=>'_________'
);

$miscellaneous_fees_content[] = array(
		'description'=>'Sub-total:',
		'amount'=>0
);

$miscellaneous_fees = array();
$miscellaneous_fees[] = '+' . str_pad('', $table_size['width'], '-') . '+';
$temp_head = '| ';
foreach($miscellaneous_fees_header as $key=>$val){
	$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
	$temp_head .= str_pad($val, $key == 'fee_type' ? $length[$key]-25 : $length[$key], ' ', $str_pad);
}

$miscellaneous_fees[] = str_pad($temp_head, $table_size['width']+1, ' ') . '|';
$miscellaneous_fees[] = $miscellaneous_fees[0];

$totals = array();
foreach($miscellaneous_fees_content as $row=>$value){
	$output_total = FALSE;
	$i = count($miscellaneous_fees);
	$miscellaneous_fees[$i] = '| ';
	foreach($value as $key=>$val){
		if($key == 'description' && $val == $miscellaneous_fees_content[count($miscellaneous_fees_content)-1][$key]){
			$output_total = TRUE;
		}
		$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
		if($key == 'description') {
			$miscellaneous_fees[$i] .= str_pad('', $length['fee_type']-24, ' ');
		}
		//if()
		if(in_array($key, $numbers)){
			if(is_numeric($val)){
				$totals[$key] = isset($totals[$key]) ? $totals[$key] : 0;
				$totals[$key] += $val;
				$val = number_format($val, in_array($key, array('units', 'hours')) ? 1 : 2);
			}
			if($output_total) {
				$val = number_format($totals[$key], in_array($key, array('units', 'hours')) ? 1 : 2);
				if($key == 'amount') $total_assessment += $totals[$key];
			}

		}
		$miscellaneous_fees[$i] .= str_pad(substr($val, 0, $length[$key]), $length[$key], ' ', $str_pad);
	}
	$miscellaneous_fees[$i] = str_pad($miscellaneous_fees[$i], $table_size['width']+1, ' ') . '|';
}
if(count($miscellaneous_fees) < count($tuition_fees)-3){
	for($c = count($miscellaneous_fees); $c < count($tuition_fees)-3; $c++){
		$miscellaneous_fees[] = '|' . str_pad('', $table_size['width'], ' ') . '|';
	}
}
$sub_total[1] = $totals['amount'];
$miscellaneous_fees[] = $miscellaneous_fees[0];
$miscellaneous_fees[] = $this->print_lib->no_condensed();
$miscellaneous_fees[count($miscellaneous_fees)-1] .= str_pad('TOTAL ASSESSMENT:', ($table_size['width']/2)-10, ' '); 
$miscellaneous_fees[count($miscellaneous_fees)-1] .= str_pad(number_format($total_assessment, 2), 12, ' ', STR_PAD_LEFT);
$miscellaneous_fees[count($miscellaneous_fees)-1] .= $this->print_lib->condensed();
$miscellaneous_fees[] = str_pad('', $table_size['width']+2, '-');

//print_r($miscellaneous_fees);

/////////////////////////////////////////////////////// TABLE 1 AND 2 READY FOR PRINTING //////////////////////

$print_content = "";
for($i = 0; $i < $table_size['height']; $i++){
	$print_content .= $tuition_fees[$i] . " " . $miscellaneous_fees[$i] . "\n";
}
$print_content = trim($print_content);

?>

<?php // if($this->session->userdata('role') != 'teller')
		echo sprintf($this->config->item('jzebra_applet'), base_url()); ?>
<div id="assessment_to_print" style="display:none;">
<?php echo $this->print_lib->init(); ?>
<?php echo print_lib::align_right($idnumber) . "\n";?>
<?php echo print_lib::align_right($name) . "\n"; ?>
<?php echo print_lib::align_right($course_year) ; ?>
<?php //echo print_lib::align_right($term) . "\n"; ?>
<?php //echo print_lib::align_right('Assessment Test Print') . "\n"; ?>
<?php echo $this->print_lib->condensed(); ?>

<?php echo $print_content; ?>

<?php echo $this->print_lib->no_condensed(); ?>
<?php echo "Due Now		:\n"; ?>
<?php echo str_pad("Succeeding Dues	:", ($table_size['width']*1.2)-12, ' ') . str_pad($succeeding_dues, 10, ' ', STR_PAD_LEFT) . " *\n"; ?>
<?php echo str_pad('', $table_size['width']*1.2, '-'); ?>

<?php echo $this->print_lib->condensed(); ?>
<?php echo "*Amount can change depending on payments received and other charges incurred during the semester.\n";?>
<?php echo "  For a more detailed computation of your assessment and updated report on your payments,\n"; ?>
<?php echo "  please log in at http://student.hnu.edu.ph\n"; ?>
<?php echo "\n1 Student Assessment. Printed using HNUMIS. " . date(DATE_RFC822) . "\n\n"; ?>
</div>
<script>
function print(content) {
    var applet = document.jzebra;
    if (applet != null) {
    	applet.append('\x1B\x40');
		applet.append(content);
		//console.log(content);
		applet.print();
	}
}
    
$(document).ready(function(){
	
	$('#print_assessment').bind('click', function(){
		var content_to_print = $('#assessment_to_print').text();	
		var applet = document.jzebra;
		if (applet != null) {
			// Searches for default printer
			applet.findPrinter();
		}
		print(content_to_print);
	});
});
</script>

<div class="container span11 clearfix" style="min-height: 50px">
	<input type="button" class="btn btn-large" id="print_assessment" value="Print Student Assesment" />
</div>
<?php endif; ?>
