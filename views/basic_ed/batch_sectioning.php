<div style="margin-top:10px">
<h2 class="heading"> <?php print($basic_level." ".$yr_level);?> MASTERLIST FOR SECTIONING <?php print("(SY ".$cur_yr."-".$cur_yr_end.")"); ?></h2>

<form method="post" name="autosection" id="autosection">
<?php echo $this->common->hidden_input_nonce(); ?>
<input type="hidden" name="action" value="auto_section" />

<div class="formSep">
<?php if($students) { ?>
	<table style="width:50%">	 
		
		<tr>
			<td colspan = "4" style="text-align:right; color:green">
			   <b> <i> Make sure to choose a section and at least 1 student before clicking this button </i></b>
			</td>
			<td  style="text-align:right";>
			  <b>	<input type="submit" name="autosection" value="Assign Section!"  class="btn btn-warning"/> </b> 
			</td>
		</tr>
		
		<tr>
			<td colspan = "4" style="text-align:right; color:green">
			  &nbsp;
			</td>
			
		</tr>
		<tr>
		<td>	 
		 	<select id="section" name="section" class="form">
				<option value="">-- Select Section --</option>
					<?php foreach ($sections as $section): ?>
						<option value="<?php echo $section->id; ?>"><?php echo $section->section_name; ?></option>
					<?php endforeach; ?>
			</select>
		</td>
		</tr>
		
	</table>	
<?php } ?>
</div>

<?php if($students){?>
	<table>
		<tr>
			<td colspan = "5" style="color:red">
				<b> <i> Please tick the box to include student in the specified section </i> </b>
			</td>
		</tr>
	</table>
<?php }?>	
<table class="table table-striped table-bordered"; style="width:50%">
	
	<thead >
			
		<tr align="center">
			<th width="3%" class="head" style=" vertical-align:middle; text-align:center;"> &nbsp; </th> 
			<th width="3%" class="head" style=" vertical-align:middle; text-align:center;">No.</th>
			<th width="6%" class="head" style="vertical-align:middle; text-align:center;">Student ID</th>
			<th width="25%" class="head" style="vertical-align:middle; text-align:center;">Name</th>
			<th width="7%" class="head" style="vertical-align:middle; text-align:center;">GPA</th>
		</tr>
	</thead>
	<tbody> 

	
	<?php 
		if($students) {
			$count = 1;
			foreach ($students as $student): ?>
			<tr>
				<?php if($count <= $classSize) {?>
					<td style="text-align:center";><input type="checkbox" name="<?php print("classlist[$student->student_histories_id]"); ?>" value="Y" class="uncheck" checked/></td>
				<?php }else {?>
					<td style="text-align:center";><input type="checkbox" name="<?php print("classlist[$student->student_histories_id]"); ?>" value="Y" class="uncheck"/></td>
				<?php }?>
				<td  style="text-align:center";><?php print($count); ?></td>
				<td  style="text-align:center";><?php print($student->students_idno); ?></td>
				<td  style="text-align:left";><?php print($student->neym); ?></td>
				<td  style="text-align:center";><?php print(number_format($student->gpa,2)); ?></td>		
				<?php $count++;?>		
			</tr>
<?php
			endforeach; ?>
				
<?php	}else{ ?>
		
			<tr>
				<td colspan = "5">
				    <b> No students to be sectioned </b>
				</td>
			</tr>
		<?php }?>
</tbody>
</table>
</form>
<form method="post" action="<?php echo site_url("{$role}/download_basicEd_enrolled_not_sectioned_csv"); ?>" id="download_list_form">
			<input type="hidden" name="sy_id" value="<?php echo $cur_yr_id; ?>" />
			<input type="hidden" name="basic_level" value="<?php echo $basic_level; ?>" />
			<input type="hidden" name="yr_level" value="<?php echo $yr_level; ?>" />
			<input type="hidden" name="cur_yr" value="<?php echo $cur_yr; ?>" />
			<input type="hidden" name="level_id" value="<?php echo $level_id; ?>" />
			
</form>

<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
		
</div> 
