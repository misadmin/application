<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

<form action="<?php echo site_url('accounts/list_tuitionfees');?>" method="post">
  <?php echo validation_errors(); 
  ?>
  <input type="hidden" name="step" value="2" />
  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" style="width:50%; margin-top:35px;" class="head1">
      <tr class="head">
        <td colspan="3" class="head"> LIST ALL TUITION FEES </td>
			
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="2" cellspacing="0" class="inside">
          <tr>
            <td width="199" align="left" valign="middle">School Year </td>
            <td width="15" align="left" valign="middle">:</td>
            <td width="438" align="left" valign="middle">
			<select name="academic_years_id" id="academic_years_id" class="form">
              <?php
					foreach($academic_years AS $acad_year) {
						print("<option value=".$acad_year->id.">".$acad_year->start_year."-".$acad_year->end_year."</option>");	
					}
				?>
              </select></td>
          </tr>
          <tr>
            <td align="left" valign="middle">College</td>
            <td align="left" valign="middle">:</td>
            <td align="left" valign="middle"><select name="colleges_id" id="colleges_id" class="form">
              <?php
					foreach($colleges AS $college) {
						print("<option value=".$college->id.">".$college->name."</option>");	
					}
				?>
                        </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="button2" id="button2" value="Continue!"  class="btn btn-success" /></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</form>