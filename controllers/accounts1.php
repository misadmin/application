<?php
class Accounts1 extends MY_Controller {

	public function __construct(){
		
		parent::__construct();
		$this->content_lib->set_title('Accounts | ' . $this->config->item('application_title'));
		$this->navbar_data['menu'] = array(
							'Students'=>'student',
							'Tuition Fees'=>		
									array('Add College Fees' => 'college_tuition_fees',
										  'Add Graduate Programs Fees'  => 'grad_tuition_fees',
										  'Add ReEd/CWTS/MS Fees'  => 'other_tuition_fees'),
							'Matriculation Fees'=>		
									array('Add College Fees' => 'college_matric_fees',
										  'Add Graduate Programs Fees'  => 'grad_matric_fees',
										  'Add Basic Education Fees'  => 'basic_ed_matric_fees'),
							'Others' =>
									array('Add Miscellaneous Fees'=> 'misc_fees',
										  'Add Lab Fees'=> 'lab_fees',	
										  'Add Other Fees' =>'other_fees'),
							'View' =>
									array('View Tuition Fees'  => 'list_tuitionfees',
										  'View Matriculation Fees'  => 'list_matricfees',
										  'View Miscellaneous Fees' => 'list_miscfees',
										  'View Lab Fees' => 'list_lab_fees',
										  'View Other Fees' => 'list_other_fees',
										  'View Students with Privileges' => 'list_privileges'),
							'Settings'=>
									array('Add New Other Tuition fee item'      =>'add_new_other_tuition_fee_item', 
									'Add New Scholarship Item' => 'new_scholarship')
											
		);
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		$client_ip = $_SERVER['REMOTE_ADDR'];
		/*
		if (! in_array($client_ip, $this->config->item('finance_allowed_ips'))){
			die($client_ip . ' May God have mercy on your soul...');
		}
		*/
		$this->load->model('hnumis/College_Model');
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Programs_Model');
		$this->load->model('financials/Tuition_Model');
		$this->load->model('financials/Matriculation_Model');
		$this->load->model('financials/Miscellaneous_Model');
		$this->load->model('financials/Other_TuitionFee_Model');
		$this->load->model('financials/Otherfees_Model');
		$this->load->model('financials/Laboratory_Model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('financials/Scholarship_Model');
		$this->load->model('hnumis/Student_Model');
		$this->load->library('form_validation');
		
	}


	public function student(){
	
		$this->content_lib->set_title('Accounts Clerk| Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
	
		if ( ! empty($query)){
				
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
				
			$this->load->library('pagination_lib');
				
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
	
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
	
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url() . "/{$this->role}/student/{$results[0]->idno}");
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
	
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
				
			$this->content_lib->set_title ('Accounts Clerk | ' . $idnum);
				
			
			//For all actions... process it here...
			switch ($this->input->post('action')){
				case 'insert_new_privilege': 
											 $data['idnumber']= $idnum;
											 $term = $this->AcademicYears_Model->getCurrentAcademicTerm();
											 //print_r($term);
											// die();
											 $data['term_id'] = $term->id;
											 $data['scholarship_id'] = $this->input->post('scholarship_id');
											 $data['priv_percent'] = sanitize_text_field($this->input->post('discountP'));
											 $status = $this->Scholarship_Model->AddStudentPrivilege($data);
											 if ($status){
												$this->content_lib->set_message("Student Privilege Successfuly added!", "alert-success"); 
												
											}else{	
												$this->content_lib->set_message("Error found while adding privilege!", "alert-error");	
												
											}								 
				break;
			}
			
			$this->load->model('teller/teller_model');
			$result = $this->teller_model->get_student($idnum);
			
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->year_level,
						'full_home_address'	=>$result->home_address, 
				);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				
				//LOAD view values here...
				
				$data['scholarship_items'] = $this->Scholarship_Model->ListScholarships();
				$data['idno'] =  $idnum;
				
				//tab contents
				$this->load->library('tab_lib');
				/* Note: these are only temporary views */
				//assessment form
				$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment', array(), 'assessment', FALSE);
				//enqueue ledger
				$this->tab_lib->enqueue_tab('Ledger', 'teller/assessment', array(), 'ledger', FALSE);
				//enqueue privileges
				$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
				$enroll_stat = $this->Student_Model->getEnrollStatus($idnum, $term->id);
				if($enroll_stat){
					$this->tab_lib->enqueue_tab('Privileges', 'accounts/form_to_assign_privileges', $data, 'privileges', FALSE);
				}
				//sidebar...
				
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				$tab_content = $this->tab_lib->content();
				$this->content_lib->enqueue_body_content("", $tab_content);
	
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}	
		
		$this->content_lib->content();
	}
	
	
	//inserts new rates of college tuition fees
	public function college_tuition_fees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
		
			switch ($step) {
				case 1:
					//accepts tuition fee for college
					$this->input_col_tuition_fee();
					break;
		
				case 2:
					$data['academic_programs'] = $this->Programs_Model->ListAcaProgramsbyCollege($this->input->post('colleges_id'));
					$data['academic_years_id']    = $this->input->post('academic_years_id');
					$data['yr_level'] 		   = $this->input->post('yr_level');
					$data['rate'] = sanitize_text_field($this->input->post('rate'));
					//print_r($data);
					//die();

					
					$status = $this->Tuition_Model->AddCollege_Tuition($data);
					if($status){
					    $this->content_lib->set_message("Tuition fee Successfully added!", 'alert-success');
						$this->input_col_tuition_fee();
					}
					redirect('accounts');			
					//$this->input_col_tuition_fee();
						
					//redirect('input_col_tuition_fee');
						
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	
	//inserts new rates of post graduate tuition fees
	public function grad_tuition_fees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					//accepts tuition fee for college
					$this->input_grad_tuition_fee();
					break;
		
				case 2:
					
					$data['programs_id'] = $this->input->post('programs_id');
					$data['academic_years_id']    = $this->input->post('academic_years_id');
					$data['yr_level'] 		   = $this->input->post('yr_level');
					$data['rate'] = sanitize_text_field($this->input->post('rate'));
					
					$status = $this->Tuition_Model->AddGraduate_Tuition($data);
										
					if($status){
						$this->content_lib->set_message("Tuition fee Successfully added!", 'alert-success');
						$this->input_grad_tuition_fee();
					}
					redirect('accounts');				
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	
	
	//insert new rates of college matriculation fees
	public function college_matric_fees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					//accepts tuition fee for college
					$this->input_col_matriculation_fee();
					break;
		
				case 2:
					
					$data['academic_programs'] = $this->Programs_Model->ListAcaProgramsbyCollege($this->input->post('colleges_id'));
					$data['academic_years_id']    = $this->input->post('academic_years_id');
					$data['yr_level'] 		   = $this->input->post('yr_level');
					$data['rate'] = sanitize_text_field($this->input->post('rate'));
					
					$status = $this->Matriculation_Model->AddCollege_Matriculation($data);
					//print($status);
					//die();				
					if($status){
					   $this->content_lib->set_message("Matriculation fee Successfully added!", 'alert-success');
					   $this->input_col_matriculation_fee();
					}
				redirect('accounts');
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	
	
	//inserts new rate of post graduate matriculation fees
	public function grad_matric_fees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					//accepts tuition fee for college
					$this->input_grad_matriculation_fee();
					break;
		
				case 2:
					
					$data['programs_id'] = $this->input->post('programs_id');
					$data['academic_years_id']    = $this->input->post('academic_years_id');
					$data['yr_level'] 		   = $this->input->post('yr_level');
					$data['rate'] = sanitize_text_field($this->input->post('rate'));
					
					$status = $this->Matriculation_Model->AddGraduate_Matriculation($data);
										
					if($status){
					 	$this->content_lib->set_message("Matriculation fees Successfully added!", 'alert-success');
						$this->input_grad_matriculation_fee();
					}
					redirect('accounts');
						
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	

	//inserts new rate of miscellaneous fees
	public function misc_fees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					//accepts miscellaneous fee 
					$this->input_miscellaneous_fee();
					break;
		
				case 2:
					
					$data['academic_programs'] = $this->Programs_Model->ListProgramsByDepartment($this->input->post('group_code'));
					$data['misc_item_id']    = $this->input->post('misc_item_id');
					$data['academic_years_id']    = $this->input->post('academic_years_id');
					$data['yr_level'] 		   = $this->input->post('yr_level');
					$data['rate'] = sanitize_text_field($this->input->post('rate'));
					
					$status = $this->Miscellaneous_Model->AddMisc_Fee($data);
										
					if($status){
						$this->content_lib->set_message("Miscellaneous fees Successfully added!", 'alert-success');
						$this->input_miscellaneous_fee();
					}
					redirect('accounts');
						
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	
	//Added: 2/18/2013
	//inserts new rates of other tuition item fees - ReEd, CWTS, MS
	public function other_tuition_fees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					//accepts tuition fee for college
					$this->input_other_tuition_fee();
					break;
		
				case 2:
					
					$data['other_tuition_fee_item_id']    = $this->input->post('other_tuition_fee_item_id');
					$data['academic_years_id']    = $this->input->post('academic_years_id');
					$data['yr_level'] 		   = $this->input->post('yr_level');
					$data['rate'] = sanitize_text_field($this->input->post('rate'));
					
					$status = $this->Other_TuitionFee_Model->AddOtherTution_fee($data);
										
					if($status){
						$this->content_lib->set_message("Other tuition fees Successfully added!", 'alert-success');
						$this->input_other_tuition_fee();
					}
					redirect('accounts');
						
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	

//Added:2/20/2013
//insets new rates of other fees
public function other_fees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					//accepts other fees 
					$this->input_other_fees();
					break;
		
				case 2:
					
					$data['academic_programs'] = $this->Programs_Model->ListProgramsByDepartment($this->input->post('group_code'));
					$data['academic_terms_id']  = $this->input->post('academic_terms_id');
					$data['otherfee_item_id']    = $this->input->post('otherfee_item_id');
					$data['rate'] = sanitize_text_field($this->input->post('rate'));
					
					$status = $this->Miscellaneous_Model->AddOther_Fee($data);
										
					if($status){
						$this->content_lib->set_message("Other fees Successfully added!", 'alert-success');
						$this->input_other_fees();
					}
					redirect('accounts');
						
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}

//Added: 3/2/2013
//insets new rates of lab fees
public function lab_fees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					//accepts lab fees 
					$this->input_lab_fees();
					break;
		
				case 2:
					
					$data['academic_years_id']  = $this->input->post('academic_years_id');
					$data['course_id']    = $this->input->post('course_id');
					$data['rate'] = sanitize_text_field($this->input->post('rate'));
					
					$status = $this->Laboratory_Model->Add_Fee($data);
										
					if($status){
						$this->content_lib->set_message("Other fees Successfully added!", 'alert-success');
						$this->input_lab_fees();
					}
					redirect('accounts');
						
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}

//Added: 2/18/2013
//for adding new fee item
	public function add_new_other_tuition_fee_item() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					//accepts miscellaneous item
					$this->input_new_other_tuition_fee_item();
					break;
		
				case 2:
					
					$item_name = sanitize_text_field($this->input->post('item_name'));
					$status = $this->Tuition_Model->AddNew_OtherTuition_Item($item_name);
					
					/*}elseif($item_type == 2){
						$status = $this->Tuition_Model->AddNew_Misc_Item($item_name);
					}elseif($item_type == 3){
						$status = $this->Tuition_Model->AddNew_OtherFee_Item($item_name);			
					}else{
						$status = $this->Tuition_Model->AddNew_AddOtherFee_Item($item_name);	
					}*/
						
					if($status){
						$this->content_lib->set_message("New fee item Successfully added!", 'alert-success');
						$this->input_new_other_tuition_fee_item();
					}
					redirect('accounts');
						
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	
	//Added: 2/23/2013
	//lists all fees by college and allows update
	public function list_tuitionfees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:
					//accepts tuition fees details
					$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
					$data['colleges'] = $this->College_Model->all_colleges();
				
					$this->content_lib->enqueue_body_content('accounts/form_to_input_list_fees_details',$data);
					$this->content_lib->content();
					break;
				
				case 2:
					$college_id = $this->input->post('colleges_id');
					$aca_yr_id = $this->input->post('academic_years_id');
					$data['college_id'] = $college_id ;
					$data['academic_years_id'] = $aca_yr_id;
					
					$data2['fees'] = $this->Tuition_Model->ListTuitionFees($data);
					if($data2['fees']){
						$data2['academic_year'] = $this->AcademicYears_Model->GetAcademicYear($aca_yr_id);
						$data2['college'] =  $this->College_Model->getCollegeName($college_id);
					
						$this->content_lib->enqueue_body_content('accounts/list_tuition_fees',$data2);
						$this->content_lib->content();
					}else{
						$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
						$data['colleges'] = $this->College_Model->all_colleges();
				
						$this->content_lib->enqueue_body_content('accounts/form_to_input_list_fees_details',$data);
						$this->content_lib->set_message("No Records Yet!", 'alert-success');
						$this->content_lib->content();
					}
					//redirect('accounts');	
					break;
				
				case 3:
					
					$fee_id = sanitize_text_field($this->input->post('id'));
					$this->session->set_userdata('fee_id',$fee_id);
					$data['tuition_fee'] = $this->Tuition_Model->getFee($fee_id);	
					$data['academic_year'] = $this->AcademicYears_Model->GetAcademicYear($data['tuition_fee']->academic_years_id);
					//print_r($data);
					//die();
					$this->content_lib->enqueue_body_content('accounts/form_to_update_tuitionfee',$data);
					$this->content_lib->content();
					//$this->list_terms_courses();
					break;
			    case 4:
					
					$id = $this->session->userdata('fee_id');
					$tuition_fee = $this->input->post('rate');	
					//print($tuition_fee);
					//die();
					$status = $this->Tuition_Model->UpdateTuitionFee($id, $tuition_fee);
					if($status){
						$this->content_lib->set_message("Tuition Fee successfully updated!", 'alert-success');
						$this->content_lib->content();
					}
					redirect('accounts');
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	
	
  //Added: 3/2/2013
	//lists all matriculation fees by college and allows update
	public function list_matricfees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:
					//accepts matriculation fees details
					$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
					$data['colleges'] = $this->College_Model->all_colleges();
				
					$this->content_lib->enqueue_body_content('accounts/form_to_input_list_matricfees_details',$data);
					$this->content_lib->content();
					break;
				case 2:
					
					$college_id = $this->input->post('colleges_id');
					$aca_yr_id = $this->input->post('academic_years_id');
					$data['college_id'] = $college_id ;
					$data['academic_years_id'] = $aca_yr_id;
					
					$data2['fees'] = $this->Matriculation_Model->ListMatricFees($data);
					
					if($data2['fees']){
						$data2['academic_year'] = $this->AcademicYears_Model->GetAcademicYear($aca_yr_id);
						$data2['college'] =  $this->College_Model->getCollegeName($college_id);
					
						$this->content_lib->enqueue_body_content('accounts/list_matriculation_fees',$data2);
						$this->content_lib->content();
					}else{
						$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
						$data['colleges'] = $this->College_Model->all_colleges();
				
						$this->content_lib->enqueue_body_content('accounts/form_to_input_list_matricfees_details',$data);
						$this->content_lib->set_message("No Records Yet!", 'alert-success');
						$this->content_lib->content();
					}
					//redirect('accounts');	
					break;
				
				case 3:
					
					$fee_id = sanitize_text_field($this->input->post('id'));
					$this->session->set_userdata('fee_id',$fee_id);
					$data['matric_fee'] = $this->Matriculation_Model->getMatFee($fee_id);	
					$data['academic_year'] = $this->AcademicYears_Model->GetAcademicYear($data['matric_fee']->academic_years_id);
					//print_r($data);
					//die();
					$this->content_lib->enqueue_body_content('accounts/form_to_update_matricfee',$data);
					$this->content_lib->content();
					//$this->list_terms_courses();
					break;
			    case 4:
					
					$id = $this->session->userdata('fee_id');
					$matric_fee = $this->input->post('rate');	
					//print($tuition_fee);
					//die();
					$status = $this->Matriculation_Model->UpdateMatricFee($id, $matric_fee);
					if($status){
						$this->content_lib->set_message("Matriculation Fee successfully updated!", 'alert-success');
						$this->content_lib->content();
					}
					redirect('accounts');
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	//Added: 3/6/2013
	//Lists the miscellaneous item fee per college and allows update
	public function list_miscfees() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:
					$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
					$data['misc_items'] = $this->Miscellaneous_Model->ListMiscellaneousItems();
					$data['colleges'] = $this->College_Model->all_colleges();
				
					$this->content_lib->enqueue_body_content('accounts/form_to_input_miscellaneous_fees_details',$data);
					$this->content_lib->content();
					break;
				case 2:
					$aca_yr_id = $this->input->post('academic_years_id');
					$misc_item_id = $this->input->post('misc_item_id');
					$college_id = $this->input->post('colleges_id');
					$yr_level = $this->input->post('yr_level');
								
					$data['college_id'] = $college_id;
					$data['academic_years_id'] = $aca_yr_id;
					$data['misc_item_id'] = $misc_item_id;
					$data['yr_level'] = $yr_level;
				//	print_r($data);
					//die();		
					$data2['fees'] = $this->Miscellaneous_Model->ListMiscFees($data);
					//print_r($data2['fees']);
					//die();
					if($data2['fees']){
						$data2['academic_year'] = $this->AcademicYears_Model->GetAcademicYear($aca_yr_id);
						$data2['college'] =  $this->College_Model->getCollegeName($college_id);
						$data2['misc_item'] = $this->Miscellaneous_Model->getMiscillaneousItem($misc_item_id);
						$data2['yr_level'] = $yr_level;
					
						$this->content_lib->enqueue_body_content('accounts/list_miscellaneous_fees',$data2);
						$this->content_lib->content();
					}else{
						$data2['academic_year'] = $this->AcademicYears_Model->GetAcademicYear($aca_yr_id);
						$data2['college'] =  $this->College_Model->getCollegeName($college_id);
						$data2['misc_item'] = $this->Miscellaneous_Model->getMiscillaneousItem($misc_item_id);
				
						$this->content_lib->enqueue_body_content('accounts/form_to_input_miscellaneous_fees_details',$data);
						$this->content_lib->set_message("No Records Yet!", 'alert-success');
						$this->content_lib->content();
					}
					//redirect('accounts');	
					break;
				
				case 3:
					
					$fee_id = $this->input->post('id');
					$this->session->set_userdata('fee_id',$fee_id);
					$data['misc_fee'] = $this->Miscellaneous_Model->getMiscFee($fee_id);	
					$data['academic_year'] = $this->AcademicYears_Model->GetAcademicYear($data['misc_fee']->academic_years_id);
					//print_r($data);
					//die();
					$this->content_lib->enqueue_body_content('accounts/form_to_update_miscfee',$data);
					$this->content_lib->content();
					//$this->list_terms_courses();
					break;
			    case 4:
					
					$id = $this->session->userdata('fee_id');
					$misc_fee = sanitize_text_field($this->input->post('rate'));	
					
					$status = $this->Miscellaneous_Model->UpdateMiscFee($id, $misc_fee);
					if($status){
						$this->content_lib->set_message("Miscellaneous Fee successfully updated!", 'alert-success');
						$this->content_lib->content();
					}
					redirect('accounts');
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
//Added: 3/7/2013
//inserts a new scholarship

	public function new_scholarship() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
		
			switch ($step) {
				case 1:
					$this->input_new_scholarship_item();
					break;
		
				case 2:
					$data['scholarship_code'] = sanitize_text_field($this->input->post('scholarship_code'));
					$data['scholarship']    = sanitize_text_field($this->input->post('scholarship'));
					
					$status = $this->Scholarship_Model->AddScholarshipItem($data);
					if($status){
					    $this->content_lib->set_message("New scholarship item uccessfully added!", 'alert-success');
						$this->input_new_scholarship_item();
					}
					redirect('accounts');			
					//$this->input_col_tuition_fee();
						
					//redirect('input_col_tuition_fee');
						
					//$this->list_terms_courses();
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
	
	}
	
//Added: 3/9/2013
//lists all students per privilege and allows update
public function list_privileges() {
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:
					$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
				
					$data['privileges'] = $this->Scholarship_Model->ListScholarships();
									
					$this->content_lib->enqueue_body_content('accounts/form_to_input_list_privileges_details',$data);
					$this->content_lib->content();
					break;
				case 2:
					$aca_term_id = $this->input->post('academic_term_id');
					
					$privilege_id = $this->input->post('privilege_id');
					
					//$data['aca_term_id'] = $aca_term_id;
					//$data['privilege_id'] = $privilege_id;
					
					$data2['with_privileges'] = $this->Scholarship_Model->ListStudentsWithPrivileges($aca_term_id, $privilege_id);
					//print_r($data2['with_privileges']);
					//die();
					if($data2['with_privileges']){
						$data2['academic_term'] = $this->AcademicYears_Model->getAcademicTerms($aca_term_id);
						$data2['privilege'] =   $this->Scholarship_Model->getScholarship($privilege_id);
						
						$this->content_lib->enqueue_body_content('accounts/list_students_with_privileges',$data2);
						$this->content_lib->content();
					}else{
						$data2['academic_term'] = $this->AcademicYears_Model->getAcademicTerms($aca_term_id);
						$data2['privilege'] =   $this->Scholarship_Model->getScholarship($privilege_id);
				
						$this->content_lib->enqueue_body_content('accounts/form_to_input_list_privileges_details',$data);
						$this->content_lib->set_message("No Records Yet!", 'alert-success');
					//	$this->content_lib->content();
					}
					//redirect('accounts');	
					break;
				
				case 3:
					
					//print_r($this->input->post('privilege'));
					//die();
					$privilege_availed_id = $this->input->post('privilege');
					$this->session->set_userdata('privilege_availed_id',$privilege_availed_id);
					$data['privilege_info'] = $this->Scholarship_Model->getStudentPrivilege($privilege_availed_id);
					
					$data['privileges'] = $this->Scholarship_Model->ListScholarships();
					
					//print_r($data);
					//die();
					$this->content_lib->enqueue_body_content('accounts/form_to_update_privilege',$data);
					$this->content_lib->content();
					//$this->list_terms_courses();
					break;
			    case 4:
					
					$id = $this->session->userdata('privilege_availed_id');
					//print($id);
					//die();
					$privilege_id = sanitize_text_field($this->input->post('privilege_id'));	
					$priv_disc = sanitize_text_field($this->input->post('priv_disc'));	
					
					
					$status = $this->Scholarship_Model->UpdatePrivilege($id, $privilege_id, $priv_disc);
					if($status){
						$this->content_lib->set_title('Accounts | ' . $this->config->item('application_title'));
						$this->content_lib->set_message("Privilege successfully updated!", 'alert-success');
						//$this->content_lib->content();
					}
					redirect('accounts');
					break;
				}
	
	}
	
	public function settings(){
		$this->content_lib->enqueue_body_content('','settings here');
		$this->content_lib->content();
	}
		
	///*******************************************************
	//private functions
	
	//entry for new college tuition fee
	private function input_col_tuition_fee(){
		
		
		$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
		$data['colleges'] = $this->College_Model->ListColleges();
				
		$this->content_lib->enqueue_body_content('accounts/form_to_input_col_tuition_fees',$data);
		$this->content_lib->content();
	}
	
	//entry for new post graduate tuition fee
	private function input_grad_tuition_fee(){
		
		
		$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
		$data['programs'] = $this->Programs_Model->ListGraduatePrograms();
		
		$this->content_lib->enqueue_body_content('accounts/form_to_input_grad_tuition_fees',$data);
		$this->content_lib->content();
	}
	
	
	//entry for new college matriculation fee
	private function input_col_matriculation_fee(){
		
		
		$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
		$data['colleges'] = $this->College_Model->ListColleges();
		
		$this->content_lib->enqueue_body_content('accounts/form_to_input_col_matriculation_fees',$data);
		$this->content_lib->content();
	}
	
	private function input_grad_matriculation_fee(){
		
		
		$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
		$data['programs'] = $this->Programs_Model->ListGraduatePrograms();
		
		$this->content_lib->enqueue_body_content('accounts/form_to_input_grad_matriculation_fees',$data);
		$this->content_lib->content();
	}
	
	
	//entry for new fee item
	private function input_new_other_tuition_fee_item(){
				
		//$data['miscellaneous_items'] = $this->Miscellaneous_Model->ListMiscellaneousItems();
		$data['other_tuition_fee_items'] = $this->Other_TuitionFee_Model->ListOtherTuitionFeeItems();
		//$data['other_fees_items'] = $this->Other_TuitionFee_Model->ListOtherFeesItems();
						
	//	print_r($data);
	//	die();
		$this->content_lib->enqueue_body_content('accounts/form_to_input_new_fee_item',$data);
		$this->content_lib->content();
	}
	
	//entry for new miscellaneous fee
	private function input_miscellaneous_fee(){
				
		$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
		$data['miscellaneous_items'] = $this->Miscellaneous_Model->ListMiscellaneousItems();
				
		$this->content_lib->enqueue_body_content('accounts/form_to_input_miscellaneous_fees',$data);
		$this->content_lib->content();
	}
	
	
	//Added: 2/18/2013
	
	//entry for new other tuition fee
	private function input_other_tuition_fee(){
				
		$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
		$data['other_tuition_fee_items'] = $this->Other_TuitionFee_Model->ListOtherTuitionFeeItems();
		
		$this->content_lib->enqueue_body_content('accounts/form_to_input_other_tuition_fees',$data);
		$this->content_lib->content();
	}
	
	//Added: 2/20/2013
	//entry for new other fees
	private function input_other_fees(){
				
		$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms();
		$data['otherfee_items'] = $this->Otherfees_Model->ListOtherFeeItems();
		
		$this->content_lib->enqueue_body_content('accounts/form_to_input_other_fees',$data);
		$this->content_lib->content();
	}
	
	//Added: 3/2/2013
	//entry for lab fees
	
	private function input_lab_fees(){
				
		$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears();
		$data['lab_courses'] = $this->Courses_Model->ListAllCoursesLab();
		
		$this->content_lib->enqueue_body_content('accounts/form_to_input_lab_fees',$data);
		$this->content_lib->content();
	}
	
	private function input_new_scholarship_item(){
				
		$this->content_lib->enqueue_body_content('accounts/form_to_input_new_scholarship');
		$this->content_lib->content();
	}
	
	
	
	
	
	private function change_tax_type(){
		
	}
	
	
	
}
