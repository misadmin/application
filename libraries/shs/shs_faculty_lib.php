<?php

	class shs_faculty_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		function shs_class_schedules() {

			$this->CI->load->model('hnumis/shs/shs_faculty_model');
			$this->CI->load->model('hnumis/shs/shs_grades_model');
			
			$data['academic_terms'] = $this->CI->shs_faculty_model->ListFaculty_Inclusive_Terms($this->CI->userinfo['empno']);

			$academic_terms = $this->CI->academic_terms_model->current_academic_term(); //print_r($academic_terms); die();
	
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_schedules');
			
			switch ($action) {
				case 'list_schedules':

					$data['selected_term']   = $data['academic_terms'][0]->id; 
					$data['teacher']         = $this->CI->userinfo['lname'].', '.$this->CI->userinfo['fname'];
					
					$data['class_schedules'] = $this->CI->shs_faculty_model->List_MySHS_ClassSchedules($this->CI->userinfo['empno'], $data['selected_term']);

					break;
					
				case 'change_select':
				
					$data['selected_term']   = $this->CI->input->post('term_id'); 
					$data['teacher']         = $this->CI->userinfo['lname'].', '.$this->CI->userinfo['fname'];
					
					$data['class_schedules'] = $this->CI->shs_faculty_model->List_MySHS_ClassSchedules($this->CI->userinfo['empno'], $this->CI->input->post('term_id'));
				
					$my_return['output'] = $this->CI->load->view('shs/teacher/shs_class_schedules_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'list_students':

					$data['midterm_confirm'] = FALSE;
					$data['finals_confirm']  = FALSE;
					
					$data['students'] = $this->CI->shs_faculty_model->List_MyStudents($this->CI->input->post('block_sections_id'),$this->CI->input->post('courses_id'),'by_gender');
					
					//check if course_offerings already confirmed by Adviser
					$confirmation = $this->CI->shs_faculty_model->Adviser_Confirm($this->CI->input->post('course_offerings_id'));
					
					if (isset($confirmation->midterm_confirm_date)) {
						$data['midterm_confirm']      = TRUE;
						$data['midterm_confirm_date'] = $confirmation->midterm_confirm_date;
					}

					if (isset($confirmation->finals_confirm_date)) {
						$data['finals_confirm']      = TRUE;
						$data['finals_confirm_date'] = $confirmation->finals_confirm_date;
					}
					
					$my_return['output'] = $this->CI->load->view('shs/teacher/list_of_students_for_grade_submission', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'submit_grades':
				
					if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
						
						switch ($this->CI->input->post('period')) {
						
							case 'Midterm':
							
								$this->CI->db->trans_start(); 
								$cnt=0;
								$no_error = TRUE;
								foreach($this->CI->input->post('midterm') AS $k=>$v) {
									if (is_numeric(intval($v))) {
										if (!$this->CI->shs_grades_model->Update_Midterm_Grades($k, $v)) {
											$this->CI->db->trans_rollback();
											$this->CI->content_lib->set_message('ERROR: Grade submission not successful!', 'alert-error');	
											$no_error = FALSE;
											break;
										}
									} else {
										$this->CI->db->trans_rollback();
										$this->CI->content_lib->set_message('ERROR: Grade submitted is not numeric!', 'alert-error');	
										$no_error = FALSE;
										break;										
									}
									$cnt++;
								}
								
								if ($no_error) {
									if ($this->CI->shs_grades_model->Save_GradeSubmissionDate($this->CI->input->post('period'),$this->CI->input->post('course_offerings_id'))) {
										$this->CI->db->trans_complete();
										$this->CI->content_lib->set_message('Midterm Grades successfully submitted on '.$cnt.' students!', 'alert-success');								
									} else {
										$this->CI->db->trans_rollback();
										$this->CI->content_lib->set_message('ERROR: Recording of submission date! Please submit again!', 'alert-error');										
									}
								} else {
									$this->CI->db->trans_rollback();
									$this->CI->content_lib->set_message('ERROR: Grade submission forced entry!', 'alert-error');																			
								}
								
								break;
								
							case 'Finals':

								$this->CI->db->trans_start(); 
								$cnt=0;
								foreach($this->CI->input->post('finals') AS $k=>$v) {
									if (is_numeric(intval($v))) {
										if (!$this->CI->shs_grades_model->Update_Finals_Grades($k, $v)) {
											$this->CI->db->trans_rollback();
											$this->CI->content_lib->set_message('ERROR: Grade submission not successful!', 'alert-error');	
											break;
										}
									} else {
										$this->CI->db->trans_rollback();
										$this->CI->content_lib->set_message('ERROR: Grade submitted is not numeric!', 'alert-error');	
										break;									
									}
									$cnt++;
								}
								
								if ($this->CI->shs_grades_model->Save_GradeSubmissionDate($this->CI->input->post('period'),$this->CI->input->post('course_offerings_id'))) {
									$this->CI->db->trans_complete();
									$this->CI->content_lib->set_message('Finals Grades successfully submitted on '.$cnt.' students!', 'alert-success');								
								} else {
									$this->CI->db->trans_rollback();
									$this->CI->content_lib->set_message('ERROR: Recording of submission date! Please submit again!', 'alert-error');										
								}

								break;
						}
						
					} else {
						$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');	
					}
					
					$data['selected_term']   = $this->CI->input->post('academic_terms_id'); 
					//$data['term_sy']         = $academic_terms->term.' '.$academic_terms->sy;
					$data['teacher']         = $this->CI->userinfo['lname'].', '.$this->CI->userinfo['fname'];
					
					$data['class_schedules'] = $this->CI->shs_faculty_model->List_MySHS_ClassSchedules($this->CI->userinfo['empno'], $this->CI->input->post('academic_terms_id'));

					break;
					
				case 'download_classlist_pdf':
				
					$this->CI->load->library('shs/section_lib.php');
					$this->CI->load->model('hnumis/shs/section_model');
					
					$data['students']      = $this->CI->section_model->ListStudentsEnrolled($this->CI->input->post('section_id'),'lname');
					$data['teacher']       = $this->CI->input->post('teacher');
					$data['strand']        = $this->CI->input->post('strand');
					$data['class_adviser'] = $this->CI->input->post('class_adviser');
					$data['room_no']       = $this->CI->input->post('room_no');
					$data['term_sy']       = $this->CI->input->post('term_sy');
					$data['descriptive_title'] = $this->CI->input->post('descriptive_title');
					$data['list_type']     = 'faculty';

					$this->CI->section_lib->generate_classlist_pdf($data);
					
					return;
				
			}
			
			$this->CI->content_lib->enqueue_body_content('shs/teacher/shs_class_schedules',$data);

			$this->CI->content_lib->content();
			
		}


		function teacher_loads() {

			$this->CI->load->model('hnumis/shs/shs_faculty_model');
			$this->CI->load->model('academic_terms_model');
			
			$data['academic_terms'] = $this->CI->shs_faculty_model->ListAcad_Terms_WithTeachers();

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_teachers');
			
			switch ($action) {
				case 'list_teachers':

					$data['selected_term'] = $data['academic_terms'][0]->id; 
					$data['teachers']      = $this->CI->shs_faculty_model->ListTeachers($data['selected_term']);
					
					if ($data['teachers']) {
						$data['selected_teacher'] = $data['teachers'][0]->employees_empno; 
					} else {
						$data['selected_teacher'] = NULL;
					}
					
					$data['class_schedules'] = $this->CI->shs_faculty_model->List_MySHS_ClassSchedules($data['teachers'][0]->employees_empno, $data['selected_term']);

					break;
					
				case 'change_select':
				
					$data['teachers'] = $this->CI->shs_faculty_model->ListTeachers($this->CI->input->post('term_id'));
					
					$found = FALSE;

					foreach($data['teachers'] AS $teacher) {
						if ($teacher->employees_empno == $this->CI->input->post('employees_empno')) {
							$data['selected_teacher'] = $this->CI->input->post('employees_empno'); 
							$found = TRUE;
							break;
						} 
					}
						
					if (!$found) {	
						$data['selected_teacher'] = $data['teachers'][0]->employees_empno;
					}
					
					$data['selected_term']    = $this->CI->input->post('term_id');
					
					$data['class_schedules'] = $this->CI->shs_faculty_model->List_MySHS_ClassSchedules($data['selected_teacher'], $this->CI->input->post('term_id'));
					
					$my_return['output'] = $this->CI->load->view('shs/teacher_loads_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				
			}
			
			$this->CI->content_lib->enqueue_body_content('shs/teacher_loads_management',$data);

			$this->CI->content_lib->content();
			
		}


		function grades_submission_report() {

			$this->CI->load->model('hnumis/shs/shs_faculty_model');
			$this->CI->load->model('academic_terms_model');
			
			$data['academic_terms'] = $this->CI->shs_faculty_model->ListAcad_Terms_WithTeachers();

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':

					$data['selected_term'] = $data['academic_terms'][0]->id; 
					$data['teachers']      = $this->CI->shs_faculty_model->ListTeachers($data['selected_term'], TRUE);
										
					break;
					
				case 'change_select':
				
					$data['teachers'] = $this->CI->shs_faculty_model->ListTeachers($this->CI->input->post('term_id'), TRUE);
					
					$my_return['output'] = $this->CI->load->view('shs/reports/grades_submission_reports', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				
			}
			
			$this->CI->content_lib->enqueue_body_content('shs/grade_submission_reports_management',$data);

			$this->CI->content_lib->content();
			
		}

		
	}
	
?>	