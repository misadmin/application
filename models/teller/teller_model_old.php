<?php

class Teller_model extends MY_Model {

	/**
	 * Method: get_last_receipt
	 * 
	 * provides the last receipt No. give the tax type...
	 * 
	 * @param string tax_type either vat or nonvat. Defaults to 'nonvat'
	 * @return object last receipt in string or False when fails or no receipt found.
	 */
	public function get_last_receipts () {
		
		$machine_ip = $this->input->ip_address();
		 
		$sql = "
			SELECT vat_receipt_no, non_vat_receipt_no, suffix 
			FROM machines 
			WHERE machine_ip={$this->db->escape($machine_ip)}
			LIMIT 1	";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0) {
			return $query->row();
		} else
			return FALSE;
	}
	
	/**
	 * All teller codes
	 * 
	 * @param string $tax_type either vat or nonvat. Defaults to nonvat
	 * @return boolean
	 */
	public function get_teller_codes ($tax_type='nonvat') {
		$type = ($tax_type=='nonvat' ? 'NV' : 'V'); // type is NV when tax_type is nonvat, else type is V.
		$sql = "
			SELECT distinct t.id, t.teller_code, t.description, default_amount
			FROM teller_codes t
			WHERE t.tax_type= '{$type}'
			ORDER BY t.teller_code ASC 	";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){ 
			//added by justing...
			$return = array();
			foreach ($query->result() as $row){
		 		$return[(int)$row->id] = $row->teller_code . " | " . $row->description . " | " . $row->id . " | " . $row->default_amount;
			}
			return $return;
		}	
		else{
			$this->set_error();
			//return $this->db->_error_number() .":". $this->db->_error_mmessage();			
			return FALSE;
		}
	}
	
	public function all_teller_codes ($show_only) {
		$sql = "
		SELECT 
			distinct t.id, t.teller_code, t.description, is_ledger, default_amount, t.tax_type, t.status
		FROM 
			teller_codes t
		";
		
		switch ($show_only){
			
			case 'active' :
						$sql .= "WHERE t.status='Active'
								";
						break;
			case 'inactive' :
						$sql .= "WHERE t.status='Inactive'
								";
						break;
			case 'active_non_vat' :
						$sql .= "WHERE t.status='Active' AND t.tax_type='NV'
								";
						break;
			case 'active_vat' :
						$sql .= "WHERE t.status='Active' AND t.tax_type='V'
								";
						break;
			case 'inactive_non_vat':
						$sql .= "WHERE t.status='Inactive' AND t.tax_type='NV'
								";
						break;
			case 'inactive_vat' :
						$sql .= "WHERE t.status='Inactive' AND t.tax_type='V'
								";
						break;
			default:
						break;
		}
		
		$sql .= "ORDER BY 
			t.teller_code 
		ASC
		";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			$this->set_error();
			return FALSE;
		}
	}
	
	public function teller_codes () {
		$sql = "
		SELECT 
			distinct t.id, t.teller_code, t.description, is_ledger, default_amount, t.tax_type, t.status
		FROM 
			teller_codes t
		WHERE 
			t.status='Active' 
			AND t.tax_type='NV' or t.tax_type='V'
		ORDER BY 
			t.teller_code ASC";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			$this->set_error();
			return FALSE;
		}
	}
	
	public function update_teller_code($data){
		$sql = "
			UPDATE
				teller_codes
			SET
			";

		$columns = array('id', 'teller_code', 'description', 'default_amount', 'is_ledger', 'tax_type', 'status');
		foreach ($data as $key=>$val){
			if ( in_array($key, $columns))
				$sql .= "`{$key}`={$this->db->escape(sanitize_text_field($val))},\n";
		}
		$sql = rtrim($sql, ",\n") . " WHERE id={$this->db->escape($data['item_id'])}
		";
		return $this->db->query($sql);
	}
	
	public function insert_teller_code ($data){
		$sql = "
			INSERT into
				teller_codes
			";
		
		$columns = array('teller_code', 'description', 'default_amount', 'is_ledger', 'tax_type', 'status');
		$fields = "(";
		$value = " VALUE (";
		foreach ($data as $key => $val){
			if ( in_array($key, $columns)) {
				$fields .= "`{$key}`,";
				$value .= "{$this->db->escape(sanitize_text_field($val))},\n"; 
			}
		}
		$fields = rtrim ($fields, ",") . ")\n";
		$value = rtrim ($value, ",\n") . ")\n";
		
		$sql = $sql . $fields . $value;
		if ($this->db->query($sql))
			return $this->db->insert_id(); else
			return FALSE;
	}
		
	public function get_bank_codes () {
		$sql = "
		SELECT b.id, b.code, b.name
		FROM banks b ";
		$query = $this->db->query($sql);
		
		if ($query && $query->num_rows() > 0){
			$return = array();
			foreach ($query->result() as $row){
				$return[(int)$row->id] = $row->code . " | " . $row->name . " | " . $row->id . "" ;
			}
			return $return;
		}
		else{
			$this->set_error();
			return FALSE;
		}
	}
	
	
	public function insert_payments ($data){
		$sql = "
			INSERT INTO payments p (payers_id, is_vatable, transaction_date, machine_ip, tellers_id, receipt_no, receipt_amount, amount_tendered, remark, is_ledger)
				VALUES (
				{$data['payers_id']}, 
				NOW(), 
				{$data['machine_ip']}, 
				{$data['tellers_id']}, 
				{$data['receipt_no']}, 
				{$data['receipt_amount']}, 
				{$data['amount_tendered']}, 
				{$data['remark']}, 
				{$data['is_ledger']})
				";
						
		if ($query = $this->db->query($sql)){
			return $this->db->insert_id();
		} else{
			$this->set_error();
			return FALSE;
		}
	}
	
	public function get_last_inserted_id_of_payments(){
		/*
		 * returned value will be used as payments_id in payment_details 
		 */
		
	}

	public function insert_payment_details ($data){			
		$sql = "
			INSERT into payment_details(payments_id, teller_codes_id, amount)
				VALUES	";			
				foreach ($data as $key=>$val){
					$sql .= "(	$payments_id,  
						{$this->db->escape($key)}, 
						{$this->db->escape($val)}),\n";
				}
			$sql = rtrim($sql, ",\n") . "\n";		
			return $this->db->query($sql);		
	}
	
	//Added: April 11, 2013 by Amie
	public function get_payertype($or) {
		$result = NULL;
		$q = "SELECT pay.students_idno, pay.employees_empno, pay.other_payers_id, pay.inhouse_id
				FROM payers pay, payments p
				WHERE p.payers_id = pay.id
					AND p.receipt_no = {$this->db->escape($or)}";

		$query = $this->db->query($q);
			
			if ($query && $query->num_rows() > 0) 
				$result = $query->row();
			return $result;
	}

	//Added: April 11, 2013 by Amie
	public function get_ORdetails($or, $payertype) {
		$result = NULL;
		
		if ($payertype == 'S') {
			$q = "SELECT p.id, p.receipt_no, date(p.transaction_date) as date, p.receipt_amount, p.is_ledger, t.description, pm.payment_method, 
			         CONCAT(s.lname,', ',s.fname,' ',s.mname,'.') as name, p.status
						FROM payments AS p
						LEFT JOIN payers AS pay
						ON p.payers_id = pay.id
						LEFT JOIN students s ON s.idno = pay.students_idno
						LEFT JOIN payments_payment_methods ppm ON ppm.payments_id = p.id
						LEFT JOIN payment_methods pm ON ppm.payment_methods_id = pm.id
						LEFT JOIN payment_items pi ON pi.payments_id = p.id
						LEFT JOIN teller_codes t ON t.id = pi.teller_codes_id
					WHERE p.receipt_no = {$this->db->escape($or)}";
		}  else if ($payertype == 'E') {
			$q = "SELECT p.id, p.receipt_no, date(p.transaction_date) as date, p.receipt_amount, p.is_ledger, t.description, pm.payment_method, 
			         CONCAT(e.lname,', ',e.fname,' ',e.mname,'.') as name, p.status
						FROM payments AS p
						LEFT JOIN payers AS pay
						ON p.payers_id = pay.id
						LEFT JOIN employees e ON e.empno = pay.employees_empno
						LEFT JOIN payments_payment_methods ppm ON ppm.payments_id = p.id
						LEFT JOIN payment_methods pm ON ppm.payment_methods_id = pm.id
						LEFT JOIN payment_items pi ON pi.payments_id = p.id
						LEFT JOIN teller_codes t ON t.id = pi.teller_codes_id
					WHERE p.receipt_no = {$this->db->escape($or)}";
		} else if ($payertype == 'O') {
				$q = "SELECT p.id, p.receipt_no, date(p.transaction_date) as date, p.receipt_amount, p.is_ledger, t.description, pm.payment_method, o.name, p.status
						FROM payments AS p
						LEFT JOIN payers AS pay
						ON p.payers_id = pay.id
						LEFT JOIN other_payers o ON o.id = pay.other_payers_id
						LEFT JOIN payments_payment_methods ppm ON ppm.payments_id = p.id
						LEFT JOIN payment_methods pm ON ppm.payment_methods_id = pm.id
						LEFT JOIN payment_items pi ON pi.payments_id = p.id
						LEFT JOIN teller_codes t ON t.id = pi.teller_codes_id
					WHERE p.receipt_no = {$this->db->escape($or)}"; }

		
		$query = $this->db->query($q);
			
			if ($query && $query->num_rows() > 0) 
				$result = $query->row();
			return $result;
		
	}
	
	
	//Added: April 10, 2013 by Amie
	public function void_receipts($data) {
		$q = "UPDATE payments
				SET voided_by = {$data['empno']}, voided_on = NOW(), status='void'
				WHERE id = {$this->db->escape($data['or'])}
					AND status='unposted'";
				
		if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}		
	}
	
	//Added: April 11, 2013 by Amie
	public function get_payments_for_posting() {
		$result = NULL;
		
		$q = "
			SELECT  p.id, p.receipt_no, p.receipt_amount, p.transaction_date, p.machine_ip, t.description, te.id as teller_id, 
				s.idno, CONCAT(s.lname,', ', s.fname) as name
			FROM payments p, teller_codes t, payment_items pi, tellers te, students s, payers pay
			WHERE p.status = 'unposted'
				AND p.is_ledger = 'Y'
				AND p.tellers_id = te.id
				AND pi.payments_id = p.id
				AND pi.teller_codes_id = t.id
				AND p.payers_id = pay.id
				AND pay.students_idno = s.idno
			ORDER BY p.receipt_no, name ASC
		";

		$query = $this->db->query($q);
		if ($query && $query->num_rows() > 0)
			$result = $query->result();
			
		return $result;
	}
	
	//Added: April 11, 2013 by Amie
	public function get_data_for_ledger($payment_id) {
		$result = NULL;
		
		$q = "SELECT p.id, p.receipt_no, p.receipt_amount, p.transaction_date
				FROM payments p
				WHERE p.id = {$this->db->escape($payment_id)}";
				
		$query = $this->db->query($q);
		if ($query && $query->num_rows() > 0)
			$result = $query->row();
			
		return $result;
	}
	
	//added: April 11, 2013 by Amie
	public function post_receipts($payment_id, $empno) {
		$q = "UPDATE payments
				SET posted_by = {$this->db->escape($empno)}, posted_on = NOW(), status='posted'
				WHERE id = {$this->db->escape($payment_id)}
					AND status='unposted'";
				
		if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}		
	}

	public function get_student ($userid){
	
		$sql = "
		SELECT
			py.id as payers_id,s.idno, s.lname, s.fname, s.mname, s.gender,
			sh.year_level, sh.prospectus_id, sh.id as student_histories_id,
			c.college_code, c.id as colleges_id,
			ap.id as ap_id, ap.abbreviation,a.home_address
		FROM
			students as s
		LEFT JOIN payers as py
		ON 
			py.students_idno = s.idno
		LEFT JOIN
			student_histories as sh
		ON
			s.idno=sh.students_idno
		LEFT JOIN 
			addresses a
		ON 
			a.students_idno = s.idno
		LEFT JOIN
			prospectus  as  p
		ON
			p.id = sh.prospectus_id
		LEFT JOIN
			academic_programs ap
		ON
			ap.id = p.academic_programs_id
		LEFT JOIN
			colleges c
		ON
			c.id=ap.colleges_id
		WHERE
			s.idno={$this->db->escape($userid)} /* and ph.is_primary='Y' */
		ORDER BY
			sh.id
		DESC
		LIMIT 1
		";
	
		$query = $this->db->query($sql);
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
	
		if ( is_object($query) && $query->num_rows() > 0){
			return $query->row();
		} else
			return FALSE;
	}
	
	public function get_employee ($empno){
		$sql = "
			SELECT
				e.";
	}
	
	function privilege_enroll_student($student_id){
		$this->load->model('hnumis/Student_model');
		
		if ($this->Student_model->student_is_enrolled($student_id)){
			//student came from the dric as a first year student or a transferee... so we'll just update it...
			//todo: these queries must include calculation for year level...
			$this->load->model('hnumis/AcademicYears_model');
			$academic_term_obj = $this->AcademicYears_model->getCurrentAcademicTerm();
			$sql = "
			SELECT
				id
			FROM
				student_histories
			WHERE
				students_idno='{$student_id}'
			AND
				academic_terms_id='{$academic_term_obj->id}'
			ORDER BY
				id DESC
			LIMIT 1
			";
			$query = $this->db->query($sql);
			
			if ($query && $query->num_rows() > 0) {
				$result_obj = $query->row();
				return $this->Student_model->update_student_history($result_obj->id, array('can_enroll'=>'Y'));	
			}	
		} else {
			//prospectus model
			$this->load->model('hnumis/Prospectus_model');
			
			if ($prospectus_id = $this->Prospectus_model->student_prospectus_ids($student_id)){
				
				//$year_level = calculate_year_level($prospectus, $subjects_taken)...
				return $this->Student_model->insert_student_history ($student_id, $prospectus_id); 
			} else
				return FALSE;
		}
	}

	
	function get_ledger_data ($payers_id){
		$sql = "
		SELECT date(transaction_date) as transaction_date, transaction_detail, reference_number, debit, credit, remark
		FROM 
		(
			select l.transaction_date, c.description as transaction_detail, if(p.from_isis='Y',p.isis_refno,l.reference_number) as reference_number, l.debit, l.credit,l.remark
			FROM ledger l
			JOIN payments     p on p.id = l.payments_id
			JOIN payers       y on y.id = p.payers_id
			JOIN teller_codes c on c.teller_code = p.isis_tran_code and !isnull(p.isis_tran_code)
			WHERE y.id = {$payers_id}
		UNION  
			select lx.transaction_date, tc.description as transaction_detail, if(p.from_isis='Y',p.isis_refno,lx.reference_number) as reference_number, lx.debit, lx.credit,lx.remark
			FROM ledger lx
			JOIN payments     p on p.id = lx.payments_id
			JOIN payment_items im on im.payments_id = p.id
			join teller_codes tc on tc.id = im.teller_codes_id
			JOIN payers       yx on yx.id = p.payers_id
			WHERE isnull(p.isis_tran_code) and yx.id = {$payers_id} 
		UNION 
			select l2.transaction_date,'Assessment' as transaction_detail, l2.reference_number, l2.debit, l2.credit,l2.remark
			FROM ledger l2
			JOIN assessments s on s.id = l2.assessments_id 
			JOIN student_histories h on h.id = s.student_histories_id
			JOIN payers  y2 on y2.students_idno = h.students_idno
			WHERE y2.id = {$payers_id}
		UNION
			select l3.transaction_date,concat('Adjustment: ',a.description) as transaction_detail, 
			IF(LOCATE('WITH',a.description)> 0, concat(l3.adjustments_id,' - ',l3.remark),l3.adjustments_id ) AS reference_number,
			l3.debit, l3.credit,
			if(LOCATE('WITH',a.description)> 0,'',l3.remark) as remark
			FROM ledger l3
			JOIN adjustments a on a.id = l3.adjustments_id 
			WHERE a.payers_id = {$payers_id}
		UNION
			SELECT l4.transaction_date,'Privilege/Scholarship' as transaction_detail, l4.reference_number, l4.debit, l4.credit,l4.remark
			FROM ledger l4
			JOIN privileges_availed v  on v.id = l4.privileges_availed_id 
			JOIN student_histories  h  on h.id = v.student_histories_id
			JOIN payers 		    y4 on y4.students_idno = h.students_idno
			WHERE y4.id = {$payers_id}			
		) t 
		ORDER BY t.transaction_date 				
		";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result_array(); else
			return FALSE;
		
	}
	
	public function get_tuition_others($academic_year, $academic_term, $idno){
		$sql="
			select c.course_code,c.paying_units, tu.rate 
			from hnumis.enrollments en
			join hnumis.student_histories sh on sh.id = en.student_history_id
			join hnumis.academic_terms    tr on tr.id = sh.academic_terms_id 
			join hnumis.academic_years    ay on ay.id = tr.academic_years_id
			join hnumis.course_offerings  co on co.id = en.course_offerings_id
			join hnumis.tuition_others    tu on tu.courses_id = co.courses_id and tu.academic_terms_id = tr.id 
			join hnumis.courses           c  on c.id  = tu.courses_id
			where sh.students_idno = 5849410 /*6234004*/ and tr.term=2 and ay.end_year=2013 and
				find_in_set(sh.year_level,tu.yr_level) 				
			";
	}
	
	public function get_fees($academic_year, $academic_term, $idno){
		$sql="
			select lv.`level`, ap.abbreviation, fs.yr_level,fg.fees_group, sg.description, fs.rate
			from hnumis.fees_schedule       fs
			join hnumis.acad_program_groups pg on pg.id = fs.acad_program_groups_id
			join hnumis.academic_programs   ap on ap.acad_program_groups_id = pg.id
			join hnumis.academic_terms      tr on tr.id = fs.academic_terms_id
			join hnumis.academic_years      ay on ay.id = tr.academic_years_id
			join hnumis.fees_subgroups      sg on sg.id = fs.fees_subgroups_id
			join hnumis.fees_groups         fg on fg.id = sg.fees_groups_id
			join hnumis.levels              lv on lv.id = fs.levels_id
			join hnumis.prospectus  		  pr on pr.academic_programs_id = ap.id
			join hnumis.student_histories   sh on sh.prospectus_id = pr.id and 
															sh.academic_terms_id = tr.id and 
															find_in_set(sh.year_level,fs.yr_level)
			join hnumis.students            st on st.idno = sh.students_idno
			where ay.end_year=2013 and tr.term=2 and st.idno =  05512849
			order by ay.end_year,tr.term,fs.yr_level,fg.id, sg.description				
		";
	}	
		
	public function get_lab_fees($academic_year, $academic_term, $idno){
		$sql ="
			select c.course_code, c.paying_units, lf.rate
			from hnumis.enrollments en
			join hnumis.student_histories sh on sh.id = en.student_history_id
			join hnumis.academic_terms    tr on tr.id = sh.academic_terms_id
			join hnumis.academic_years    ay on ay.id = tr.academic_years_id
			join hnumis.course_offerings  co on co.id = en.course_offerings_id
			join hnumis.laboratory_fees   lf on lf.courses_id = co.courses_id and lf.academic_years_id = ay.id
			join hnumis.courses           c  on c.id  = lf.courses_id
			where ay.end_year=2013 and tr.term=2 and sh.students_idno = 5849410
		";
	}
	
		
	
	public function update_receipt_numbers ($data){
		$sql = "
			INSERT into machines (`machine_ip`, `vat_receipt_no`, `non_vat_receipt_no`)
				VALUES ({$this->db->escape($data['machine_ip'])}, {$this->db->escape($data['vat_receipt_no'])}, {$this->db->escape($data['non_vat_receipt_no'])})
			ON DUPLICATE KEY UPDATE
				`vat_receipt_no`=VALUES(`vat_receipt_no`), `non_vat_receipt_no`=VALUES(`non_vat_receipt_no`)
			";
		 
		return $this->db->query($sql);
	}
	
	public function checkout ($payers_id, $empno, $payments, $items, $receipt_no, $machine_ip, $tax_type, $payment_methods_ids=null){
		//payment_methods_ids can come from the session... if a carryover is done...
		$method_keys = $this->config->item('payment_method_keys');
		$methods = $this->config->item('payment_methods');
		$this->load->model('academic_terms_model');
		$current_academic_term = $this->academic_terms_model->getCurrentAcademicTerm();
		//lets calculate first the total amount tendered:
		$total_payments = 0;
		foreach ($items as $item){
			$total_payments += $item->amount;
		}
		
		$payment_ids = array();
		foreach ($items as $item){
			$is_ledger = $this->item_is_ledger($item->teller_code);
			
			$sql = "
				INSERT into
				payments (`academic_terms_id`, `transaction_date`, `machine_ip`, `tellers_id`, `receipt_no`, `payers_id`, `receipt_amount`, `amount_tendered`, `remark`, `is_ledger`, `status`)
				VALUE
				({$current_academic_term->id}, NOW(), {$this->db->escape($machine_ip)}, '{$this->teller_id($empno)}', {$this->db->escape($receipt_no)}, {$this->db->escape($payers_id)}, {$this->db->escape($item->amount)}, {$this->db->escape($total_payments)}, {$this->db->escape($item->remarks)}, '{$is_ledger}', 'unposted')
				";							
			if ($query = $this->db->query($sql)){
				$payment_id = $this->db->insert_id();
				if ($payment_id < 1) return FALSE; 
				$payment_ids[] = $payment_id;
				$item_sql = "
					INSERT into
						payment_items (`teller_codes_id`, `payments_id`, `amount`)
					VALUE
						({$this->db->escape($item->teller_code)}, '{$payment_id}', {$this->db->escape($item->amount)})
					";
				if (!$this->db->query($item_sql)) return FALSE;
			}else{
				return FALSE;
			}
			$receipt_no++;
		}
		
		if (is_null($payment_methods_ids))
			$payment_methods_ids = array();
		
		foreach ($payments as $payment){
			
			if (isset($methods[$payment->type])){
				$payment_method_sql = "
					INSERT into
						payment_methods (`payment_method`, `amount`)
					VALUE
						({$this->db->escape($methods[$payment->type])}, {$this->db->escape($payment->amount)})
					";
				
				if($this->db->query($payment_method_sql)){
					$payment_methods_id = $this->db->insert_id();
					$payment_methods_ids[] = $payment_methods_id;
					
					$payment_method_details_sql = "
						INSERT into
							payment_method_details(`payment_methods_id`, `key`, `value`)
						VALUE
						";
					foreach($method_keys[$payment->type] as $key){
						$val = isset($payment->$key) ? $payment->$key : "";
						$payment_method_details_sql .= "('{$payment_methods_id}', '{$key}', '{$val}'),\n";
					}
					$payment_method_details_sql = rtrim($payment_method_details_sql, ",\n");
					IF (!$this->db->query($payment_method_details_sql)) return FALSE;
				}else{
					return FALSE; 
				}
			}
		}
		
		foreach ($payment_ids as $payment_id){
			$payments_payment_method_sql = "
				INSERT into
					payments_payment_methods (`payments_id`, `payment_methods_id`)
				VALUES
				";
		
			foreach ($payment_methods_ids as $payment_methods_id){
				$payments_payment_method_sql .= "('{$payment_id}', '{$payment_methods_id}'),\n";
			}
			$payments_payment_method_sql = rtrim($payments_payment_method_sql, ",\n");
			IF (!$this->db->query($payments_payment_method_sql)) return FALSE;
		}
		
		//We place the payment_methods_ids to the session... 
		$this->session->set_userdata('payment_methods_ids', $payment_methods_ids);
		$field = ($tax_type == 'nonvat' ? 'non_vat_receipt_no' : 'vat_receipt_no');
		
		IF (!$this->db->query("UPDATE machines set `{$field}`='{$receipt_no}' WHERE machine_ip='{$machine_ip}'")) return FALSE;
		return TRUE;
	}
	
	public function other_courses_payments ($academic_terms_id, $year_level) {
		$sql = "
			SELECT
				courses_id,rate
			FROM
				tuition_others
			WHERE
				academic_terms_id={$this->db->escape($academic_terms_id)}
			AND
				yr_level={$this->db->escape($year_level)}
			";
		
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			$return = array();
			foreach($query->result() as $result){
				$return[$result->courses_id] = $result->rate;
			}
			return $return;
		} else{
			return FALSE;
		}
	}
	
	//Edited by: Tatskie on 7th May 2013 [and t.yr_level = sh.year_level]
	public function other_courses_payments_histories_id ($student_histories_id) {
		$sql = "
		SELECT
			courses_id,rate
		FROM
			tuition_others t
		LEFT JOIN
			academic_terms at
		ON
			at.id=t.academic_terms_id
		LEFT JOIN
			student_histories sh
		ON
			sh.academic_terms_id=at.id and t.yr_level = sh.year_level
		WHERE
			sh.id={$this->db->escape($student_histories_id)}
		";
		
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			$return = array();
			foreach($query->result() as $result){
				$return[$result->courses_id] = $result->rate;
			}
			return $return;
		} else{
			return FALSE;
		}
	}
	
	public function all_laboratory_fees ($academic_terms_id){
		$sql = "
			SELECT
				courses_id,rate
			FROM
				laboratory_fees lf
			LEFT JOIN
				academic_terms at
			ON
				at.academic_years_id=lf.academic_years_id
			WHERE
				at.id={$this->db->escape($academic_terms_id)}
			";
		
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			$return = array();
			foreach($query->result() as $result){
				$return[$result->courses_id] = $result->rate;
			}
			return $return;
		} else{
			return FALSE;
		}
	}
	
	public function teller_code_info ($teller_code){
		$sql = "
			SELECT
				t.id, t.teller_code, t.description, is_ledger, default_amount, t.tax_type, t.status
			FROM
				teller_codes t
			WHERE
				t.id={$this->db->escape($teller_code)}
			LIMIT 1";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->row(); else
			return FALSE;
	}
	
	public function bank_info ($bank_id){
		$sql = "
			SELECT
				code, name
			FROM
				banks
			WHERE
				id={$this->db->escape($bank_id)}
			LIMIT 1
			";
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0)
			return $query->row(); else
			return FALSE;
	}
	
	/**
	 * 
	 * 
	 * @param unknown_type $empno
	 * @return FALSE when no result, int tellers id when a result is found
	 */
	public function teller_id ($empno){
		$sql = "
			SELECT
				id
			FROM
				tellers
			WHERE
			";
		if ($empno < 20000){
			$sql .= "employees_empno = {$this->db->escape($empno)}";
		} else {
			$sql .= "students_idno = {$this->db->escape($empno)}";
		}
		
		$query = $this->db->query($sql);
		
		if ($query AND $query->num_rows() > 0){
			$row = $query->row();
			return $row->id;
		} else {
			return FALSE;
		}
	}
	
	public function item_is_ledger ($item_id){
		$sql = "
			SELECT
				is_ledger
			FROM
				teller_codes
			WHERE
				id={$this->db->escape($item_id)}
			";
		if ($query = $this->db->query($sql)){
			$row = $query->row();
			return ($row->is_ledger);
		}  
			return FALSE;
	}
	
	/**
	 * Returns Summary of Receipts with a given Date
	 * @param String (Date) $date
	 */
	public function daily_receipt_summary ($date){
		$sql="
			select * from (
			select p.machine_ip, concat(e.lname,' ','(',e.empno,')' ) as teller, t.teller_code, t.description,
					p.id as payment_id, s.idno, concat(s.lname,', ', s.fname) as stud, 
					p.transaction_date as transaction_date, p.receipt_no, p.receipt_amount as amount,p.`status`, pm.payment_method
				from payments        p
				join payers          r on r.id = p.payers_id
				join students        s on s.idno = r.students_idno
				join tellers         tl on tl.id = p.tellers_id
				join employees       e  on e.empno =  tl.employees_empno
				join payment_items   i on i.payments_id = p.id
				join teller_codes    t on t.id = i.teller_codes_id
				join payments_payment_methods ppm on ppm.payments_id = p.id
				join payment_methods pm on pm.id = ppm.payment_methods_id
			UNION 
			select p2.machine_ip, concat(s2b.lname,' ','(',s2b.idno,')' ) as teller, t2.teller_code, t2.description,
					p2.id as payment_id, s2.idno, concat(s2.lname,', ', s2.fname) as stud, 
					p2.transaction_date, p2.receipt_no, p2.receipt_amount as amount,p2.`status`, pm.payment_method
				from payments        p2
				join payers          r2 on r2.id = p2.payers_id
				join students        s2 on s2.idno = r2.students_idno
				join tellers         tl2 on tl2.id = p2.tellers_id
				join students        s2b  on s2b.idno =  tl2.students_idno
				join payment_items   i2 on i2.payments_id = p2.id
				join teller_codes    t2 on t2.id = i2.teller_codes_id
				join payments_payment_methods ppm on ppm.payments_id = p2.id
				join payment_methods pm on pm.id = ppm.payment_methods_id
			) as test
			where date(transaction_date) = {$this->db->escape($date)}  
			order by machine_ip, teller_code, transaction_date, receipt_no 
			";
		//print_r($sql);	die();
		$query = $this->db->query($sql);
		
		//echo '<pre>';
		//print_r($this->db->last_query());
		//die();
		
		if ($query)
		{
			return $query->result();
		}
		else
		{
			return FALSE;
		}
	}

	public function receipts ($date, $payment_methods){
		$sql="
		select * from  
			(
			select p.machine_ip, concat(e.lname,', ',e.fname, '(',e.empno,')' ) as teller, t.description, 
			p.id as payment_id, s.idno, concat(s.lname,', ', s.fname) as payor,
			p.transaction_date as transaction_date, p.receipt_no, p.receipt_amount as amount, p.`status`, pm.payment_method
			from payments        p
			join payers          r on r.id = p.payers_id
			join students        s on s.idno = r.students_idno
			join tellers         tl on tl.id = p.tellers_id
			join employees       e  on e.empno =  tl.employees_empno
			join payment_items   i on i.payments_id = p.id
			join teller_codes    t on t.id = i.teller_codes_id
			join payments_payment_methods ppm on ppm.payments_id = p.id
			join payment_methods pm on pm.id = ppm.payment_methods_id
		UNION
			select p2.machine_ip, concat(s2b.lname,', ',s2b.fname,'(',s2b.idno,')' ) as teller, t2.description,
			p2.id as payment_id, s2.idno, concat(s2.lname,', ', s2.fname) as payor,
			p2.transaction_date, p2.receipt_no, p2.receipt_amount as amount,p2.`status`, pm.payment_method
			from payments        p2
			join payers          r2 on r2.id = p2.payers_id
			join students        s2 on s2.idno = r2.students_idno
			join tellers         tl2 on tl2.id = p2.tellers_id
			join students        s2b  on s2b.idno =  tl2.students_idno
			join payment_items   i2 on i2.payments_id = p2.id
			join teller_codes    t2 on t2.id = i2.teller_codes_id
			join payments_payment_methods ppm on ppm.payments_id = p2.id
			join payment_methods pm on pm.id = ppm.payment_methods_id
			) as student_payments				
		where date(transaction_date) = {$this->db->escape($date)} 
		";		
		$sql = ($payment_methods=='All' ? $sql. " " : $sql . " and payment_method = {$this->db->escape($payment_methods)} " );
		$sql = $sql . " order by machine_ip, receipt_no, transaction_date, description";
		//print_r($sql);
		//die();
		$query = $this->db->query($sql);
		return ($query ?  $query->result() : FALSE);
	}
	
	


}
