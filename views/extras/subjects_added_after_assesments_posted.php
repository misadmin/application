<style>
	table.subjects 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
</style>

<div>
<div><br></div>
<div class="fluid span11"> 
<h3>List of Added/Withdrawn Subjects after Posting</h3>
<table id="subjects_t" class="table table-bordered subjects">
	<thead>
		<tr>		
		<th width="04%">&nbsp</th>
		<th width="08%">Date Assessed</th>
		<th width="07%">Stud ID</th>
		<th width="16%">Student</th>
		<th width="15%">Program</th>
		<th width="12%">Course Code - Section</th>
		<th width="8%">Date Enrolled</th>
		<th width="12%">Enrolled By</th>
		<th width="18%">Remark</th>
		</tr>		
	</thead>
<?php 
	$x=1;
	foreach ($items as $item){
?>
		<tr>
		<td class="center"><?php echo $x++; ?>
		<td class="center"><?php echo date('M d, Y', strtotime(str_replace('-', '/', $item['transaction_date']))); ?></td>
		<td class="center"><a href="<?php echo site_url(). $this->session->userdata('role'). '/student/'.$item['idno'] ?> "> <?php echo $item['idno']; ?></a></td>
		<td><?php echo $item['student']; ?></td>
		<td><?php echo $item['abbreviation'].' - '.$item['year_level']; ?></td>
		<td><?php echo $item['course_code']. ' - ' . $item['section_code']; ?></td>
		<td class="center"><?php echo date('M d, Y', strtotime(str_replace('-', '/', $item['date_enrolled']))); ?></td>
		<td><?php echo $item['enrolled_by_id']. ' '. $item['enrolled_by_name']  ;?></td>
		<td><?php echo $item['remark']; ?></td>
		</tr>
<?php 
	}
?>
</table>
</div>
</div>

<script>
	$(document).ready(function(){
		$("#subjects_t").stickyTableHeaders();
	    $("#subjects_t").stickyTableHeaders('updateWidth');
	});
</script>	
