
<style>
	req {
		color: red;
		font-weight: bold;
		font-size: 18px;
	}
</style>

<h2 class="heading">New Student Account - Senior High School</h2>

	<div class="row-fluid" style="width:50%; float:left;">
		<div class="form-horizontal">
			<fieldset>
				<div class="control-group formSep">
					<label for="lastname" class="control-label">Last Name:<req>*</req> </label>
					<div class="controls" >
						<div style="float:left;">
							<input type="text" name="lastname" id="lastname" placeholder="Last Name" class="input-xlarge search_student"  autofocus />
						</div>
						<div style="float:left; padding-top:5px; padding-left:10px; font-weight:bold; color:#BA0303;" id="show_search_code_icon">
							
						</div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="firstname" class="control-label">First Name:<req>*</req> </label>
					<div class="controls">
						<input type="text" name="firstname" id="firstname" placeholder="First Name" class="input-xlarge search_student"  />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="middlename" class="control-label">Middle Name:<req>*</req> </label>
					<div class="controls">
						<input type="text" name="middlename" id="middlename"  placeholder="Middle Name" class="input-xlarge search_student"  />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="dbirth" class="control-label">Date of Birth:<req>*</req> </label>
					<div class="controls">
						<input type="date" name="dbirth" id="dbirth"  placeholder="Date of Birth" class="input-xlarge search_student"  />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="contact_no" class="control-label">Contact No.:</label>
					<div class="controls">
						<input type="text" name="contact_no" id="contact_no"  placeholder="Contact Number" class="input-xlarge"  />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="gender" class="control-label">Gender: </label>
					<div class="controls">
						<select id="gender" name="gender" class="form-control" >
							<option value="M" selected>Male</option>
							<option value="F">Female</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="grade_level" class="control-label">Register For:<req>*</req></label>
					<div class="controls">
						<select id="academic_terms_id" name="academic_terms_id" class="form-control" >
							<option value="0">--Select Term--</option>
							<?php 
								if ($acad_terms) {
									foreach ($acad_terms AS $term) {
							?>
									<option value="<?php print($term->id); ?>" >
										<?php print($term->term.' '.$term->sy); ?>
									</option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="grade_level" class="control-label">Grade Level</label>
					<div class="controls">
						<select id="grade_level" name="grade_level" class="form-control" >
							<option value="11">Grade 11</option>
							<option value="12">Grade 12</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="strand" class="control-label">Strand:</label>
					<div class="controls">
						<select id="prospectus_id" name="prospectus_id" class="form-control" style="width:auto;" >
							<?php 
								if ($strands) {
									foreach ($strands AS $strand) {
							?>
										<option value="<?php print($strand->id); ?>"><?php print($strand->description); ?></option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-danger" name="register_student" id="register_student" value="Register Student!" disabled />
						<!-- <a href="#" class="btn btn-primary enroll_new_student">Add New Student!</a> -->
					</div>
				</div>	
			</fieldset>
		</div>
	</div>

	<div id="list_of_students" style="width:45%; float:left; margin:0 auto;">

	</div>


	
<script>
	$("#lastname").keyup(function(){
		
		var lastname      = $('#lastname').val();
		var firstname     = $('#firstname').val();
		var middlename    = $('#middlename').val();
		var dbirth        = $('#dbirth').val();
		var element       = $("option:selected", "#academic_terms_id");
		var acad_terms_id = element.val();

		if (acad_terms_id != 0) {
			if (isEmpty(lastname) || isEmpty(firstname) || isEmpty(middlename) || isEmpty(dbirth)) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}
		} else {
			$('#register_student').prop("disabled", true);
		}

	});
	

	$("#firstname").keyup(function(){

		var lastname      = $('#lastname').val();
		var firstname     = $('#firstname').val();
		var middlename    = $('#middlename').val();
		var dbirth        = $('#dbirth').val();
		var element       = $("option:selected", "#academic_terms_id");
		var acad_terms_id = element.val();

		if (acad_terms_id != 0) {
			if (isEmpty(lastname) || isEmpty(firstname) || isEmpty(middlename) || isEmpty(dbirth)) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}
		} else {
			$('#register_student').prop("disabled", true);
		}

	});

	$("#middlename").keyup(function(){
		
		var lastname      = $('#lastname').val();
		var firstname     = $('#firstname').val();
		var middlename    = $('#middlename').val();
		var dbirth        = $('#dbirth').val();
		var element       = $("option:selected", "#academic_terms_id");
		var acad_terms_id = element.val();

		if (acad_terms_id != 0) {
			if (isEmpty(lastname) || isEmpty(firstname) || isEmpty(middlename) || isEmpty(dbirth)) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}
		} else {
			$('#register_student').prop("disabled", true);
		}

	});

	$("#dbirth").keyup(function(){
		
		var lastname      = $('#lastname').val();
		var firstname     = $('#firstname').val();
		var middlename    = $('#middlename').val();
		var dbirth        = $('#dbirth').val();
		var element       = $("option:selected", "#academic_terms_id");
		var acad_terms_id = element.val();

		if (acad_terms_id != 0) {
			if (isEmpty(lastname) || isEmpty(firstname) || isEmpty(middlename) || isEmpty(dbirth)) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}
		} else {
			$('#register_student').prop("disabled", true);
		}

	});
		
	$('#academic_terms_id').change(function(event){
		event.preventDefault();

		var lname         = $('#lastname').val();
		var fname         = $('#firstname').val();
		var mname         = $('#middlename').val();
		var dbirth        = $('#dbirth').val();
		var element       = $("option:selected", "#academic_terms_id");
		var acad_terms_id = element.val();

		if (acad_terms_id != 0) {
			if (isEmpty(lname) || isEmpty(fname) || isEmpty(mname) || isEmpty(dbirth)) {
				$('#register_student').prop("disabled", true);
			} else {	
				$('#register_student').prop("disabled", false);
			}
		} else {
			$('#register_student').prop("disabled", true);
		}
	});

	
	function isEmpty(v) {
		if (v === '' || v === null) {
			return true;
		} else {
			return false;
		}
	}
	
</script>


	<form id="new_student_form" method="POST" class="form-horizontal">
		<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="add_new_student" />
	</form>

	<script>
		$('#register_student').click(function(e){
			e.preventDefault();

			var lastname          = $('#lastname').val();
			var firstname         = $('#firstname').val();
			var middlename        = $('#middlename').val();
			var dbirth            = $('#dbirth').val();
			var contact_no        = $('#contact_no').val();

			var element           = $("option:selected", "#gender");
			var gender            = element.val();
			var element           = $("option:selected", "#grade_level");
			var grade_level       = element.val();
			var element           = $("option:selected", "#prospectus_id");
			var prospectus_id     = element.val();
			var element           = $("option:selected", "#academic_terms_id");
			var academic_terms_id = element.val();

			if (academic_terms_id != 0) {
				if (isEmpty(lastname) || isEmpty(firstname) || isEmpty(middlename) || isEmpty(dbirth)) {
					alert('Lastname, Firstname, Middlename, Date of Birth and Register For fields are required!');
				} else {
					var confirmed = confirm('Continue to register new student?');

					if (confirmed){

						$('<input>').attr({
							type: 'hidden',
							name: 'lastname',
							value: lastname,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'firstname',
							value: firstname,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'middlename',
							value: middlename,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'dbirth',
							value: dbirth,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'contact_no',
							value: contact_no,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'gender',
							value: gender,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'grade_level',
							value: grade_level,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'prospectus_id',
							value: prospectus_id,
						}).appendTo('#new_student_form');		
						$('<input>').attr({
							type: 'hidden',
							name: 'academic_terms_id',
							value: academic_terms_id,
						}).appendTo('#new_student_form');		
						
						$('#new_student_form').submit();
					}
				}
			} else {
				$('#register_student').prop("disabled", true);
			}
		
		
		});
		
		
		function isEmpty(v) {
			if (v === '' || v === null) {
				return true;
			} else {
				return false;
			}
		}

	</script>

<script>
	$(".search_student").blur(function(){
			
		var lastname   = $('#lastname').val();
		var firstname  = $('#firstname').val();
		var middlename = $('#middlename').val();

		$('#show_search_code_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/new_shs_student'); ?>",
			data: {"lastname": lastname,
					"firstname": firstname,
					"middlename": middlename,						
					"action": "search_student" },
			dataType: 'json',
			success: function(response) {													
				if (response.found) {
					$('#show_search_code_icon').html('Student of the same possible name found!'); 
					$('#list_of_students').html(response.output);
				} else {
					$('#show_search_code_icon').html(''); 	
					$('#list_of_students').html('');						
				}
			}
		});
		
	});
	
</script>




