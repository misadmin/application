<div class="fluid span12">
	<form action="<?php echo site_url("dean/student/{$student['idnum']}"); ?>" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="advise_max_units" />
		<input type="hidden" name="academic_term_id" value="<?php echo $current_term; ?>" />
		<fieldset>
				<div class="control-group formSep">
					<label for="advised_units" class="control-label">Advised Units</label>
					<div class="controls">
						<?php echo number_format($max_units,1); //max units is a value ?> Units
					</div>
				</div>
				<div class="control-group formSep">
					<label for="max_units" class="control-label">Maximum Units</label>
					<div class="controls">
						<input name="max_units" type="text" value="" class="form" style="font-family:Verdana, Geneva, sans-serif; font-size:12px; width:20px;" />
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Advise Student</button>
					</div>
				</div>
		</fieldset>
	</form>
</div>





<?php //todo: we can advise a student to have this max units for new terms also... advised that we have a pull down menu of current and upcoming academic terms. ?>
   
  
 