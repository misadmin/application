<h3 class="heading">Print Form IX</h3>
<div class="row-fluid">
	<div class="span12">
		<form class="form-horizontal" id="form9-form">
			<fieldset>
				<div class="control-group formSep">
					<label for="date_graduated" class="control-label">Date Graduated</label>
					<div class="controls">
						<input type="text" id="form9-date_graduated" name="date_graduated" value="<?php echo $this->session->userdata('last_form9_date') ? $this->session->userdata('last_form9_date') : ''; ?>"/>
					</div>
				</div>
				<div class="control-group formSep">
					<div class="controls">
						<button type="submit" class="btn btn-success">Submit</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script>
$().ready(function(){
	$('#form9-form').on('submit', function(e){
		e.preventDefault();
		if ($('#form9-date_graduated').val()==''){
			$('#form9-date_graduated').focus();
			alert('Please set the graduation date');			
		} else {
			var url="<?php echo site_url('printer/form9/' . $student_id); ?>/";
			url += $('#form9-date_graduated').val().replace(/\//g, '-');
			
			window.open(url, 'printgrade', "height=300,width=400");
		}		
	});	
});
</script>