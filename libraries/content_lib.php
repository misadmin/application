<?php 
class Content_lib {
	
	//Codeigniter instance
	private $CI;
	//header values...
	private $title = "";
	private $header_scripts = array();
	private $header_styles = array();
	private $keep_cache;
	private $available_scripts;
	private $available_styles;
	private $html_class="";
	private $header_content = "";
	//avatar
	public $avatar_image = '';
	//navbar values...
	private $navbar_values='';
	//body values...
	private $message="";
	private $severity = 'success';
	private $body_contents = array();
	//sidebar values...
	private $widget_count = 0;
	private $sidebar_widgets = array();
	//footer values...
	private $footer_scripts = array();
	//default views
	private $header_view;
	private $navigation_view;
	private $main_container_view;
	private $sidebar_widget_wrapper_view;
	private $sidebar_wrapper_view;
	private $footer_view;
	private $minify;
	//after html
	private $after_html = '';
	
	public function __construct(){
		$this->CI =& get_instance();
		$this->title = $this->CI->config->item('application_title');
		$this->available_scripts = $this->CI->config->item('available_scripts');
		$this->available_styles = $this->CI->config->item('available_styles');
		$this->keep_cache = $this->CI->config->item('keep_cache');
		$this->enqueue_header_script_array($this->CI->config->item('preload_scripts'));
		$this->enqueue_header_style_array($this->CI->config->item('preload_styles'));
		$this->set_charset ($this->CI->config->item('charset'));
		$this->header_view = $this->CI->config->item('default_header_view');
		$this->navigation_view = $this->CI->config->item('default_navigation_view');
		$this->main_container_view = $this->CI->config->item('default_body_view');
		$this->sidebar_widget_wrapper_view = $this->CI->config->item('default_sidebar_widget_wrapper_view');
		$this->sidebar_wrapper_view = $this->CI->config->item('default_sidebar_wrapper_view');
		$this->footer_view = $this->CI->config->item('default_footer_view');
		$this->minify = $this->CI->config->item('minify_content');
	}
	
	public function set_title ($title){
		$this->title = $title;
	}
	
	public function set_charset ($charset){
		$this->charset = $charset;
	}
	
	public function set_html_class ($class) {
		$this->html_class = $class;
	}
	
	public function set_keep_cache ($keep_cache){
		$this->keep_cache = $keep_cache;
	}
	
	public function html_class(){
		return $this->html_class;
	}
	
	public function enqueue_header_script ($script){
		if (array_key_exists($script, $this->available_scripts)) {
			$this->header_scripts[] = base_url() . $this->available_scripts[$script];
		} else {
			$this->header_scripts[] = base_url() . $script;
		}
	}
	
	public function enqueue_header_script_array ($scripts=array()){
		foreach ($scripts as $script) {
			if (array_key_exists($script, $this->available_scripts)) {
				$this->header_scripts[] = base_url() . $this->available_scripts[$script];
			} else {
				$this->header_scripts[] = base_url() . $script;
			}
		}
	}
	
	public function enqueue_header_style ($style){
		if (array_key_exists($style, $this->available_styles)) {
			$this->styles[] = base_url() . $this->available_styles[$style];
		} else {
			$this->styles[] = base_url() . $style;
		}
	}
	
	public function enqueue_header_style_array ($styles = array()){
		foreach ($styles as $style) {
			if (array_key_exists($style, $this->available_styles)) {
				$this->styles[] = base_url() . $this->available_styles[$style];
			} else {
				$this->styles[] = base_url() . $style;
			}
		}
	}
	
	public function header_content ($ret = FALSE, $view='header'){
		$return = array (
				'title'			=> $this->title,
				'keep_cache'	=> $this->keep_cache,
				'scripts'		=> $this->header_scripts,
				'styles'		=> $this->styles,
				'charset'		=> $this->charset,
		);
		if( ! empty($this->html_class))
			$return['class'] = $this->html_class;
		
		ob_start ();
		$this->CI->load->view($view, $return);
		$content = ob_get_contents();
		ob_end_clean();
		
		if ($ret) {
			$this->header_content = $content; 
			return $content; 
		} else {
			echo $content;
		}
		
	}
	
	public function set_avatar_image($image_url){
		$this->avatar_image = $image_url;
	}
	
	public function set_navbar_content ($view='', $content=array()){
		$view = ( ! empty($view) ? $view : $this->navigation_view);
		$content['avatar_image'] = $this->avatar_image;
		$content = $this->enqueue_content_string($view, $content);
		$this->navbar_content = $content;
	}
	
	public function navbar_content ($return=FALSE){
		return $this->navbar_content;
	}
	
	public function content ($return=FALSE){
		$content = $this->header_content(TRUE);
		
		if ( ! empty($this->navbar_content))
			$content .= $this->navbar_content(TRUE);
		
		$content .= $this->body_content(TRUE);
		$content .= $this->footer_content(TRUE);
		
		if ($return)
			return $content; 
		else{
			if ($this->minify)
				echo $this->minify($content); else
				echo $content;
		}
			
	}
	
	public function set_message ($message, $severity='success'){
		$this->message = $message;
		$this->severity = $severity;
	}
	
	public function enqueue_body_content ($view, $content=array()) {
		if ( !empty($view))
			$contents = $this->enqueue_content_string ($view, $content); else
			$contents = $content;
		
		$this->body_contents[] = $contents;
	}
	
	public function body_content ($view=''){
		$view = ( ! empty($view) ? $view : $this->main_container_view);
		$main_content = '';
		foreach ($this->body_contents as $content){
			$main_content .= $content;
		}
		$sidebar_content = '';
		foreach ($this->sidebar_widgets as $content){
			$sidebar_content .= $content;
		}
		$data = array('sidebar_widgets' => $sidebar_content);
		if ( ! empty($sidebar_content)){
			$sidebar_content = $this->enqueue_content_string($this->sidebar_wrapper_view, $data);
		}
		$data = array(
				'message'=>$this->message,
				'severity'=>$this->severity,
				'content'=>$main_content,
				'sidebar'=>$sidebar_content,
				);
		
		if (empty($this->main_container_view))
			$body_content = $main_content; else
			$body_content = $this->enqueue_content_string ($this->main_container_view, $data);
		return $body_content;
	}
	/**
	 * Enqueues to the sidebar...
	 * 
	 * @param unknown_type $view
	 * @param unknown_type $content
	 * @param unknown_type $title
	 * @param unknown_type $in
	 */
	public function enqueue_sidebar_widget ($view, $content='', $title='', $in='') {//$in could be 'in' or ''
		$widget_content = $this->enqueue_content_string ($view, $content);
		$this->widget_count++;
		
		$data = array (
					'id'		=>	$this->widget_count,
					'content'	=>	$widget_content,
					'title'		=>	$title,
					'in'		=>	$in,
				);
		
		$content = $this->enqueue_content_string ($this->sidebar_widget_wrapper_view, $data);
		$this->sidebar_widgets[] = $content;
	}
	
	public function enqueue_footer_script ($script) {
		if (array_key_exists($script, $this->available_scripts)) {
			$this->footer_scripts[] = base_url() . $this->available_scripts[$script];
		} else {
			$this->footer_scripts[] = base_url() . $script;
		}
	}
	public function enqueue_footer_scripts_array ($scripts){
		foreach ($scripts as $script) {
			if (array_key_exists($script, $this->available_scripts)) {
				$this->footer_scripts[] = base_url() . $this->available_scripts[$script];
			} else {
				$this->footer_scripts[] = base_url() . $script;
			}
		}
	}
	
	public function footer_content ($return=FALSE) {
		$content = array(
				'footer_scripts'=>$this->footer_scripts,
				);
		
		$footer = $this->enqueue_content_string ($this->footer_view, $content);
		$footer .= $this->after_html;
		if ($return)
			return $footer; else
			echo $footer;	
	}

	public function set_main_container_view ($view=''){
		$this->main_container_view=$view;
	}
	
	public function enqueue_content_string ($view, $content) {
			
		ob_start ();
		$this->CI->load->view($view, $content);
		$contents = ob_get_contents();
		if (ob_get_contents()) ob_end_clean();
		return $contents;
	}
	
	/**
	 * Enqueues html after the HTML... if you want to enqueue other scripts or tags... enqueue it here
	 * 
	 * @param string $html
	 */
	public function enqueue_after_html($html){
		$this->after_html .= "\n" . $html;
	}
	
	public function minify($string){
		//return "here";
		if( ! in_array($this->CI->session->userdata('role'), $this->CI->config->item('minify_exceptions'))){
			return preg_replace(
					array(
						'![^:]{1}//[\w \S]+!',			//single line comments
						'!/\*[\w\s\W]+\*/!',	//multiple line comments
						'!\s+!',
						),
					array(
						' ',
						' ',
						' ',),
					$string);
		} else
			return $string;
	}
	
	/**
	 * Clear sidebar contents
	 * @param int $id The sidebar key... 
	 */
	public function flush_sidebar($id=NULL){
		if(is_null($id)){
			$this->sidebar_widgets = array();
		} else{
			unset($this->sidebar_widgets[$id]);
		}
	}
}