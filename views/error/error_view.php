<?php header("HTTP/1.1 {$code} {$message[0]}"); ?><html>
<head>
	<meta charset="UTF-8" />
	<title><?php echo $code ?> Error </title>
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap-responsive.min.css')?>" />
	<style>
	.head {
	  position: relative;
	  color: #fff;
	  text-shadow: 0 1px 3px rgba(0,0,0,.4), 0 0 30px rgba(0,0,0,.075);
	  background:url(<?php echo base_url('assets/misc/headerbg1.png')?>);
	  background-repeat:repeat-x;
	  margin-bottom:30px;
	}
	</style>
</head>
<body>
	<div class="head">
		<div class="container">
			<img src="<?php echo base_url('assets/misc/banner.png') ?>">
		</div>
	</div>
	<div class="container">
		<h2>Error <?php echo $code ?> <?php echo (isset($message[0]) ? '('.$message[0].')' : '') ?></h2>
		<h3><?php echo (isset($message[1]) ? $message[1] : '') ?></h3>
		<a href="<?php echo site_url()?>">Click Here to go back!</a>
	</div>
</body>
</html>