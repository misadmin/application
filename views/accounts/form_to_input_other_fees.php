<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

<form action="<?php echo site_url('accounts/other_fees');?>" method="post">
  <?php echo validation_errors(); 
  ?>
  <input type="hidden" name="step" value="2" />
  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" style="width:50%; margin-top:35px;" class="head1">
      <tr class="head">
        <td colspan="3" class="head"> ADD OTHER FEES </td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="2" cellspacing="0" class="inside">
          <tr>
            <td width="199" align="left" valign="middle">School Term </td>
            <td width="15" align="left" valign="middle">:</td>
            <td width="438" align="left" valign="middle">
			<select name="academic_terms_id" id="academic_terms_id" class="form">
              <?php
					foreach($academic_terms AS $acad_term) {
						print("<option value=".$acad_term->id.">".$acad_term->term." ".$acad_term->sy."</option>");	
					}
				?>
              </select></td>
          </tr>
		    <tr>
            <td align="left" valign="middle">Other fee  item</td>
            <td align="left" valign="middle">:</td>
            <td align="left" valign="middle"><select name="otherfee_item_id" id="otherfee_item_id" class="form">
              <?php
					foreach($otherfee_items AS $otherfee) {
						print("<option value=".$otherfee->id.">".$otherfee->description."</option>");	
					}
				?>
                        </select></td>
          </tr>
		  <tr>
            <td>Department </td>
            <td>:</td>
            <td><select name="group_code" id="group_code" class="form">
              <option value="C" selected="selected">College</option>
              <option value="P">Post Graduate</option>
              <option value="L">Law</option>
              </select></td>
          </tr>
          <tr>
            <td>Amount</td>
            <td>:</td>
            <td><input name="rate" type="text" id="rate" size="10" maxlength="10" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="button2" id="button2" value="Save Other Fee!"  class="btn btn-success" /></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</form>