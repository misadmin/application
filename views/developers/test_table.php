<?php 
	
	class My_Table extends Table_lib{
		
		public function __construct($table_attributes=NULL){
			parent::__construct($table_attributes);
		}
		
		public function format_row($row){
			return 'class="'.($row['gender'] == 'F' ? 'error' : 'info').'"';
		}
		
		public function format_data_id($row){
			return '<a href="'.site_url('student/'.$row['id']).'">'.$row['id'].'</a>';
		}
		
		public function format_data_gender($row){
			return ($row['gender'] == 'M' ? 'Male' : ($row['gender'] == 'F' ? 'Female' : ''));
		}
		
		public function format_cell_id($row){
			return 'class = ""';
		}
	}
	
	$head_row1 = array(
			array(	"value"=>"Students",
					"attributes"=>array("class"=>"span12",
										"colspan"=>"3"
										)
					)
			);
	
	$head_row2 = array(
			array(	"id"=>"id",
					"value"=>"ID Number",
					"attributes" => array('id'=>'id_header','style'=>'width:15%')
				),
			array(	"id"=>"name",
					"value"=>"Names",
					"attributes" => array('id'=>'name_header')
				),
			array(	"id"=>"gender",
					"value"=>"Gender"
				)
			);
	
	$data = array(
		array(
			"id"=>"6128492",
			"name"=>"Kevin Felisilda",
			"gender"=>"M",
			"password"=>"qwerty"),
		array(
			"id"=>"6128493",
			"name"=>"Rey Agunod",
			"gender"=>"M",
			"password"=>"qwerty"),
		array(
			"id"=>"6128494",
			"name"=>"Justin",
			"gender"=>"M",
			"password"=>"qwerty"),
		array(
			"id"=>"6128495",
			"name"=>"Ashley",
			"gender"=>"F",
			"password"=>"qwerty"),
		);
	
	class Student_test {
		public $id;
		public $name;
		public $gender;
		public $password;
		
		function __construct($info){
			$this->id = isset($info['id']) ? $info['id'] : '';
			$this->name = isset($info['name']) ? $info['name'] : '';
			$this->gender = isset($info['gender']) ? $info['gender'] : '';
			$this->password = isset($info['password']) ? $info['password'] : '';
		}
	}
	
	$tiro = new Student_test(array(
									"id" => "6128111",
									"name" => "Tiro",
									"gender" => "M",
									"password" => "asdasdasd"
								)
							);

	$table1 = new My_Table(array('class'=>'table','id'=>'table1'));
	?>
	<div class="span12">
	<?php 
	$table1->insert_head_row($head_row1);
	$table1->insert_head_row($head_row2);
	$table1->insert_data($data);
	$table1->insert_data($tiro);
	echo $table1->content();
	?></div>
	
	
	
	
	
	
	