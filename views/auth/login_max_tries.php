<div style="text-align: center; margin-top: 2%;">
	<img src="<?php echo base_url($this->config->item('seal')); ?>" /><br />
	<h3 class="login_title"><?php echo $this->config->item('application_title_long'); ?></h3>
</div>

<div class="login_box">
	<div class="top_b">Sign In Error</div>
		<div class="alert alert-error alert-login">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
				You reach the maximum number of attempts to log in. For security reasons you are barred from logging in. Please check again after one hour.
		</div>
		<div class="cnt_b">
			<div class="formRow">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-user"></i></span><input type="text" id="username" name="username" placeholder="Username" disabled="disabled"/>
				</div>
			</div>
			<div class="formRow">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-lock"></i></span><input type="password" id="password" name="password" placeholder="Password" disabled="disabled" />
				</div>
			</div>
		</div>
		<div class="btm_b clearfix">
			<button class="btn btn-inverse pull-right" type="submit" disabled="disabled">Sign In</button>
		</div>
</div>
<script>
	$(document).ready(function(){
		//* boxes animation
		form_wrapper = $('.login_box');
		function boxHeight() {
			form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);	
		};
		
		form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
		$('.linkform a,.link_reg a').on('click',function(e){
			var target	= $(this).attr('href'),
			target_height = $(target).actual('height');
			
			$(form_wrapper).css({
				'height'		: form_wrapper.height()
			});	
		$(form_wrapper.find('form:visible')).fadeOut(400,function(){
				form_wrapper.stop().animate({
                    height	 : target_height,
					marginTop: ( - (target_height/2) - 24)
                },500,function(){
                    $(target).fadeIn(400);
                    $('.links_btm .linkform').toggle();
		
					$(form_wrapper).css({
						'height'		: ''
					});	
				});
		});
		e.preventDefault();
		});
});
</script>
