<?php 
require_once(DIRNAME(BASEPATH).'/application/third_party/tcpdf/tcpdf.php');

class MYPDF extends TCPDF {
	public $header_image;
	public $header_summary;

	public function setHeaderImage($img){
		$this->header_image = $img;
	}

	public function setHeaderSummary($summary){
		$this->header_summary = $summary;
	}
	//Page header
	public function Header() {
		$this->Image('@' . $this->header_image, 13, 5, 20, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->SetFont('times', '', 8);
		$this->writeHTMLCell(85, 0, 40, 7, '<table><tr><td style="font-size:25;color:green;border-bottom:2px solid green;padding-bottom:15;">Holy Name University</td></tr><tr><td style="font-size:11;">Lessage St., Tagbilaran City, Bohol, Philippines</td></tr></table>' . "\n");
		//$this->writeHTMLCell(60, 0, 130, 5, $this->header_summary . "\n");
		$this->writeHTMLCell(0, 0, 15, 35, '<hr />');
	}

	// Page footer
	public function Footer() {
		$this->SetFont('helvetica', '', 9);
		$this->SetTextColorArray($this->footer_text_color);
		//set style for cell border
		$line_width = (0.85 / $this->k);
		$this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));

		$w_page = isset($this->l['w_page']) ? $this->l['w_page'].' ' : '';
		$pagenumtxt = $w_page.$this->getAliasNumPage().' of '.$this->getAliasNbPages();

		$this->SetY(-18);
		$date = date( 'M d, Y h:ia' );
		//Print page number
		$this->Cell(100, 8, $this->getAliasRightShift().$pagenumtxt, 'T', 0, 'R');
		$this->Cell(0, 8, "Printed  ".$date, 'T', 0, 'R');
	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setHeaderImage(file_get_contents(DIRNAME(BASEPATH) . "/" . $this->config->item('big_logo')));
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetTopMargin(35);
//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->SetFont('helvetica', '', 9);
$pdf->AddPage();

$pdf->Output('test.pdf', 'I');