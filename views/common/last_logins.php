<div class="row-fluid">
	<ul class="nav nav-list">
		<li class="clearfix">
		<div class="pull-left"><?php echo '<strong>Location</strong>'; ?></div>
		<div class="pull-right"><?php echo '<strong>Time</strong>'; ?></div>
	</ul>	
	<ul class="nav nav-list">
<?php if(isset($results) && count($results) > 0):?>

<?php foreach($results as $result): ?>
		<li class="clearfix">
		<div class="pull-left">
			<?php echo ucfirst($result->role)
				. ( trim($result->role =='') ? '' : ' (' ) 
				. ( substr($result->ip_address,0,3) == '10.' ? 'HNU' : $result->ip_address )
				. ( trim($result->role =='') ? '' : ')' ); 
			?>
		</div>
		<div class="pull-right"><?php echo substr(date('M d, y h:ia', strtotime($result->time_in)),0,-1) ; ?></div>
<?php endforeach; ?>
<?php endif;?>
	</ul>
</div>