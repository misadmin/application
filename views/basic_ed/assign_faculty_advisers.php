<h2 class="heading" style="margin-top:20px;">Faculty Advisers <?php echo $academicyear->sy?><br />
<?php echo $level->level ?> </h2>

<div style="text-align:center; width:100%;">
<!-- 		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; text-align:right;
						border-color: #D8D8D8; padding: 5px; margin-bottom:20px; margin-top:20px; margin-left:40px; vertical-align:middle;
						width:60%;">
				<a href="#" data-toggle="modal" data-target="#modal_add_adviser" class="btn btn-success">
					<i class="icon-plus icon-white"></i> Assign Faculty Adviser</a>	
		</div>		
 -->
						
		<?php if ($advisers) { 	?>
			<div style="width:70%; margin-bottom:40px; margin-left:40px;">
			  <table id="advisers_list" border="0" cellspacing="0" class="table table-condensed table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
			    <thead>
				    <tr>
				      <th width="10%" style="vertical-align:middle; text-align:center;">Grade/<br>Year Level</th>
				      <th width="30%" style="vertical-align:middle; text-align:center;">Section Name</th>
				   	  <th width="40%"  style="vertical-align:middle; text-align:center;">Adviser</th>
				      <th width="8%"  style="vertical-align:middle; text-align:center;">Room</th>
				      <th width="5%"  style="vertical-align:middle; text-align:center;">Remove</th>
				      <th width="7%"  style="vertical-align:middle; text-align:center;">Assign<br>Adviser</th>
				    </tr>
			    </thead>		
				<?php 
						$cnt=0;
						foreach ($advisers AS $adviser) { 
							$cnt++;
				?>
					    <tr>
					      <td style="text-align:center; vertical-align:middle; "><?php print($adviser->yr_level); ?> </td>
					      <td style="text-align:left; vertical-align:middle; "><?php print($adviser->section_name); ?> </td>
					      <td style="text-align:left; vertical-align:middle; "><?php print($adviser->name); ?> </td>
					      <td style="text-align:center; vertical-align:middle; "><?php print($adviser->room); ?> </td>
					      <td style="text-align:center; vertical-align:middle;">
					      <?php 
					      	if ($adviser->name) {
					      ?>
      						<a class="delete_adviser" href="<?php print($adviser->id); ?>" my_id="<?php print($adviser->id); ?>">
        					<i class="icon-trash"></i></a>
							<?php 
								} else {
							?>
							 &nbsp;
							 <?php 
							 	}
							 ?>       					
        					</td>					
					      <td style="text-align:center; vertical-align:middle; ">
					      		<a href="#" data-toggle="modal" data-target="#modal_add_adviser_<?php print($cnt); ?>" >
					      		<i class="icon-tag"></i></a>
		<!-- Modal to Assign Class Adviser -->
 		
		      <div class="modal hide fade" id="modal_add_adviser_<?php print($cnt); ?>" >
		 		<form method="post" id="add_adviser_form">
		   				<input type="hidden" name="step" value="assign_advisers" />
		   				<input type="hidden" name="academic_years_id" value="<?php echo $academicyear->id ?>" />
		   				<input type="hidden" name="levels_id" value="<?php echo $level->id ?>" />
		   				<input type="hidden" name="year_level" value="<?php echo $adviser->yr_level ?>" />
		   				<input type="hidden" name="basic_ed_sections_id" value="<?php print($adviser->basic_ed_sections_id); ?>" />
		   				<?php $this->common->hidden_input_nonce(FALSE); ?>
			   		<div class="modal-header">
			       		<button type="button" class="close" data-dismiss="modal">�</button>
			       		<h3>Assign Faculty Adviser</h3>
			   		</div>
				   		<div class="modal-body">
				   		
				   			<div style="text-align:left;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									<tr>
										<td>Grade/Year Level</td>
										<td>:</td>
										<td><?php print($adviser->yr_level); ?></td>
									</tr>
									<tr>
										<td>Section</td>
										<td>:</td>
										<td><?php print($adviser->section_name); ?></td>
									</tr>
									<tr>
										<td>Faculty Adviser</td>
										<td>:</td>
										<td>
											<select id="faculty" name="employees_empno" class="form" style="width:auto;">
													<?php 
														foreach ($faculty as $fac) { 
													?>
														<option value="<?php echo $fac->empno; ?>" <?php if ($fac->empno == $adviser->empno) print("selected"); ?> ><?php echo $fac->name; ?></option>
													<?php 
														}
													?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Room No</td>
										<td>:</td>
										<td>
											<input type="text" name="room" style="width:30px;" value="<?php print($adviser->room); ?>" >
										</td>
									</tr>
								</table>
							</div>
				   		
				   		</div>
					   <div class="modal-footer">
				    		<input type="submit" class="btn btn-primary" id="add_adviser" value="ASSIGN ADVISER!">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
					</form>
		    	</div>				
			 
				<!--  End of Modal to Create New Section -->		
					      		
					      </td>
        				</tr>
				<?php } ?>
			  </table>
			</div>
		<?php } ?>

</div>	

<form id="delete_adviser2" action="<?php echo site_url("rsclerk/academic_sections")?>" method="post">
  <input type="hidden" name="step" value="remove_adviser" />
	<input type="hidden" name="academicyear" value="<?php echo $academicyear->id ?>">
	<input type="hidden" name="level" value="<?php echo $level->id ?>">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
</form>

<script>
	$('.delete_adviser').click(function(event){
		event.preventDefault();
		var confirmed = confirm('Are you sure you want to remove the advised course?');
			
		if (confirmed){

			var adviser_id = $(this).attr('href');
		
			$('<input>').attr({
			    type: 'hidden',
			    name: 'adviser_id',
			    value: adviser_id,
			}).appendTo('#delete_adviser2');
			$('#delete_adviser2').submit();

		}
		
	});
</script>


