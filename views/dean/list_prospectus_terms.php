<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>
<div style="background:#FFF; width:80%;">
<div style="width:100%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr class="head">
    <td align="right" style="font-size:16px; font-weight:bold; padding:10px;">
     
	  <a class="new_term" href="#" style="text-decoration:none; font-size:12px; font-weight:bold; color:#666; font-family:Verdana, Geneva, sans-serif;"><i class="icon-plus"></i> Add term</a>
		
		</td>
    </tr>
</table>
</div>
<?php
	if ($prospectus_terms) {
		foreach ($prospectus_terms AS $term) {
?>
<div style="width:100%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td width="30%" align="left"><span style="font-size:16px; font-weight:bold;"><?php print($term->y_level." - ".$term->term); ?></span>
	    <a href="#" data-toggle="modal" data-target="#modal_edit_term<?php print($term->id); ?>"><button class="btn btn-warning" >Edit Term</button></a>

		<div class="modal hide fade" id="modal_edit_term<?php print($term->id); ?>" >
			<form action="<?php echo site_url('dean/prospectus');?>" method="post">
						<?php $this->common->hidden_input_nonce(FALSE); ?>
						<input type="hidden" name="term_id" value="<?php print($term->id); ?>" />
						<input type="hidden" name="step" value="add_prospectus_course" />
				<div class="modal-header">
				       <button type="button" class="close" data-dismiss="modal">�</button>
				       <h3>Edit Prospectus Term</h3>
				</div>
				<div class="modal-body">            
					    <?php 
					    	$data['year_level'] = $term->year_level;
					    	$data['term'] = $term->pros_term;
					    	$this->load->view('dean/modals/prospectus_term_form', $data);
					    ?>
				</div>
				<div class="modal-footer">
					<input type="submit" name="button" id="button" value="Proceed!" class="btn btn-success" />
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
    
	    
	    <a href="#" data-toggle="modal" data-target="#modal_add_course<?php print($term->id); ?>" ><button class="btn btn-success" >Add Course!</button>	</a>

		<div class="modal hide fade" id="modal_add_course<?php print($term->id); ?>" >
			<form action="<?php echo site_url('dean/prospectus');?>" method="post">
						<?php $this->common->hidden_input_nonce(FALSE); ?>
						<input type="hidden" name="term_id" value="<?php print($term->id); ?>" />
						<input type="hidden" name="step" value="add_prospectus_course" />
				<div class="modal-header">
				       <button type="button" class="close" data-dismiss="modal">�</button>
				       <h3>Add Prospectus Course</h3>
				</div>
				<div class="modal-body">            
					    <?php 
					    	$data['all_courses'] = $all_courses;
					    	$data['selected_course'] = NULL;
					    	$data['elective'] = 'N';
					    	$data['is_major'] = 'Y';
					    	$data['is_bracketed'] = 'N';
					    	$data['num_retakes'] = NULL;
					    	$data['cutoff_grade'] = NULL;
					    	$this->load->view('dean/modals/prospectus_course_form', $data);
					    ?>
				</div>
				<div class="modal-footer">
					<input type="submit" name="button" id="button" value="Proceed!" class="btn btn-success" />
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
    
	</td>
    </tr>
</table>

</div>
<div style="width:100%; margin-bottom:40px;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover" padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="10%" class="head" style="vertical-align:middle;">Course Code</th>
      <th width="35%" class="head" style="vertical-align:middle;">Descriptive Title</th>
      <th width="6%" class="head" style="vertical-align:middle;">Credit Units</th>
      <th width="6%" class="head" style="vertical-align:middle;">Delete</th>
      <th width="7%" class="head" style="vertical-align:middle;">Cut-off Grade</th>
      <th width="5%" class="head" style="vertical-align:middle;">No. of Retakes</th>
      <th width="25%" class="head" style="vertical-align:middle;">Pre-requisites</th>
      <th width="6%" class="head" style="vertical-align:middle;">Details</th>
      </tr>
    </thead>
    <?php
		$courses = $this->Prospectus_Model->ListProspectusTermCourses($term->id,'A');
		//print_r($courses); die();
		if ($courses) {
			foreach ($courses AS $course) {
	?>
    <tr>
      <td style="text-align:left; vertical-align:middle; ">
			<!-- <a class="edit_prospectus_course" prospectus_terms_id="<?php print($term->id);?>" href="<?php print($course->id);?>"><?php print($course->
	  													  course_code); ?> </a> -->

	    <a href="#" data-toggle="modal" data-target="#modal_edit_course<?php print($course->id); ?>" ><?php print($course->course_code); ?></a>

		<div class="modal hide fade" id="modal_edit_course<?php print($course->id); ?>" >
			<form action="<?php echo site_url('dean/prospectus');?>" method="post">
						<?php $this->common->hidden_input_nonce(FALSE); ?>
						<input type="hidden" name="prospectus_terms_id" value="<?php print($term->id); ?>" />
						<input type="hidden" name="prospectus_course_id" value="<?php print($course->id); ?>" />
						<input type="hidden" name="credit_units" value="<?php print($course->credit_units); ?>" />
						<input type="hidden" name="step" value="edit_prospectus_course" />
				<div class="modal-header">
				       <button type="button" class="close" data-dismiss="modal">�</button>
				       <h3>Edit Prospectus Course</h3>
				</div>
				<div class="modal-body">            
					    <?php 
					    	//$data['all_courses'] = $all_courses;
					    	$data['selected_course'] = $course;
					    	$data['elective'] = $course->elective;
					    	$data['is_major'] = $course->is_major;
					    	$data['is_bracketed'] = $course->is_bracketed;
					    	$data['num_retakes'] = $course->num_retakes;
					    	$data['cutoff_grade'] = $course->cutoff_grade;
					    	$this->load->view('dean/modals/prospectus_course_form', $data);
					    ?>
				</div>
				<div class="modal-footer">
					<input type="submit" name="button" id="button" value="Proceed!" class="btn btn-success" />
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
    
			
	  </td>
      <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($course->credit_units); ?></td>
      <td style="text-align:center; vertical-align:middle; ">
	 
	<!--	//temporarily NOT ALLOWED TO delete pending talks with registrar and deans 4.26.18 EDIT: temporarily ALLOWED for new prospectuses -->
	 <a class="delete_course" href="<?php print($course->id); ?>" prospectus_terms_id="<?php print($term->id); ?>" ><i class="icon-trash"></i></a> 

	 <?php
	 /*<img src="<?php print(base_url('assets/img/icon-recycle.gif')); ?>" width="16" height="16" align="absmiddle" style="vertical-align:middle; border:none" /></a>
	 */
	 ?>
	 </td>
     <td style="text-align:center; vertical-align:middle; "><?php print($course->cutoff_grade); ?></td>
     <td style="text-align:center; vertical-align:middle; "><?php print($course->num_retakes); ?></td>
	 <td style="text-align:left; vertical-align:middle; "><?php

      	$prereqs = $this->Prospectus_Model->ListPrereq_Courses($course->id);
		if ($prereqs) {
			$str_prereq = "";
			foreach ($prereqs AS $prereq) {
				$str_prereq = $str_prereq.$prereq->course_code.", ";
			}
			$str_prereq = substr_replace($str_prereq ,"",-2);
			print($str_prereq);
			print("<br>");
		}
		$prereqs = $this->Prospectus_Model->ListPrereq_YrLevel($course->id);
		if ($prereqs) {
			$str_prereq = "";
			foreach ($prereqs AS $prereq) {
				$str_prereq = $str_prereq.$prereq->pre_req.", ";
			}
			$str_prereq = substr_replace($str_prereq ,"",-2);
			print($str_prereq);
		}?></td>
      <td style="text-align:center; vertical-align:middle; ">
	  	<?php
			if (($term->year_level > 1) OR ($term->year_level >= 1 AND $term->pros_term > 1))  {
		?>
	  	<a class="detail_course" href="<?php print($course->id); ?>" ><i class="icon-edit"></i></a>
		<?php
			}
			//print($term->year_level." ++ ".$term->pros_term);
		?>
		</td>
      </tr>
    <?php
			}
		}
	?>
    <thead>
    <tr class="head">
      <th width="10%" class="head" style="vertical-align:middle;">&nbsp;</th>
      <th width="35%" class="head" style="vertical-align:middle;">&nbsp;</th>
      <th width="6%" class="head" style="vertical-align:middle; text-align:center;"><?php print($term->max_credit_units."/".$term->max_bracket_units); ?></th>
      <th width="6%" class="head" style="vertical-align:middle;">&nbsp;</th>
      <th width="7%" class="head" style="vertical-align:middle;">&nbsp;</th>
      <th width="5%" class="head" style="vertical-align:middle;">&nbsp;</th>
      <th width="25%" class="head" style="vertical-align:middle;">&nbsp;</th>
      <th width="6%" class="head" style="vertical-align:middle;">&nbsp;</th>
      </tr>
      </thead>
</table>
</div>
<?php
		}
	}
?>
  </div>

 <!--  
  <form id="add_course_form" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="8" />
</form>
<script>
	$('.add_course').click(function(event){
		event.preventDefault(); 
		var prospectus_terms_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prospectus_terms_id',
		    value: prospectus_terms_id,
		}).appendTo('#add_course_form');
		$('#add_course_form').submit();
	});
</script>
 -->
 
  <form id="term2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="6" />
</form>

<script>
	$('.new_term').click(function(event){
		event.preventDefault(); 
		var prospectus_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prospectus_id',
		    value: prospectus_id,
		}).appendTo('#term2');
		$('#term2').submit();
	});
</script>

  <form id="detail_course2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="10" />
</form>

<script>
	$('.detail_course').click(function(event){
		event.preventDefault(); 
		var prospectus_courses_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prospectus_courses_id',
		    value: prospectus_courses_id,
		}).appendTo('#detail_course2');
		$('#detail_course2').submit();
	});
</script>

  <form id="delete_course2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="delete_prospectus_course" />
</form>

<script>
	$('.delete_course').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to remove the course?');

		if (confirmed){
			var prospectus_courses_id = $(this).attr('href');
			var prospectus_terms_id = $(this).attr('prospectus_terms_id');
		
			$('<input>').attr({
				type: 'hidden',
				name: 'prospectus_courses_id',
				value: prospectus_courses_id,
			}).appendTo('#delete_course2');

			$('<input>').attr({
				type: 'hidden',
				name: 'prospectus_terms_id',
				value: prospectus_terms_id,
			}).appendTo('#delete_course2');

			$('#delete_course2').submit();
		}
		
	});
</script>

<!-- 
<form id="edit_pros_course" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="15" />
</form>

<script>
	$('.edit_prospectus_course').click(function(event){
		event.preventDefault(); 
		var prospectus_courses_id = $(this).attr('href');
		var prospectus_terms_id = $(this).attr('prospectus_terms_id');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prospectus_courses_id',
		    value: prospectus_courses_id,
		}).appendTo('#edit_pros_course');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prospectus_terms_id',
		    value: prospectus_terms_id,
		}).appendTo('#edit_pros_course');
		$('#edit_pros_course').submit();
	});
</script>
 -->
