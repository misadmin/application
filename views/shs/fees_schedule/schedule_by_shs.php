
<h2 class="heading">Fees Schedule For Senior High School</h2>
	<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 35px; margin-bottom:10px; padding:2px;">
			<div style="float:right; overflow:auto;">
				<div id="show_change_data_icon" style="float:left; padding-right:8px; margin-top:7px;">
				</div>
				<div style="float:left; padding-top:3px;" class="b2">
					<select name="academic_terms_id" class="select_change" id="academic_terms_id">
						<?php 
							foreach($terms AS $term) {
						?>
								<option value="<?php print($term->id); ?>" 
									sy="<?php print($term->term.' '.$term->sy); ?>" <?php if($current_term == $term->id) print("selected"); ?>
									academic_years_id="<?php print($term->academic_years_id); ?>" >
									<?php print($term->term.' '.$term->sy); ?> 
								</option>
						<?php 	
							}
						?>
					</select>
				</div>
				<div style="float:left; padding-top:3px;" class="b2">
					<select name="academic_programs_id" class="select_change" id="academic_programs_id">
						<?php 
							if ($strands) {
								foreach($strands AS $strand) {
						?>
								<option value="<?php print($strand->id); ?>" 
									group_name="<?php print($strand->group_name); ?>" 
									description="<?php print($strand->description); ?>"
									acad_program_groups_id="<?php print($strand->acad_program_groups_id); ?>" >
									<?php print($strand->track_abbrev.' - '.$strand->abbreviation); ?> 
								</option>
						<?php 
								}
							}
						?>
					</select>
				</div>
			</div>
	</div>

	
	<div id="show_fees_schedule">
		<?php 
			$this->load->view('shs/fees_schedule/fees_schedule_details');
		?>

	</div>	

		
<script>
	//$(document).ready(function(){
		$('.select_change').bind('change', function(){
			
			var element1             = $("option:selected", "#academic_terms_id");
			var academic_terms_id    = element1.val();
			var sy                   = element1.attr('sy');
			var academic_years_id    = element1.attr('academic_years_id');
			var element2             = $("option:selected", "#academic_programs_id");
			var academic_programs_id = element2.val();
			var description          = element2.attr('description');
			var group_name           = element2.attr('group_name');
			var acad_program_groups_id = element2.attr('acad_program_groups_id');
			
			$('#show_change_data_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/schedule_for_shs'); ?>",
				data: {"academic_terms_id": academic_terms_id,
						"academic_programs_id": academic_programs_id,
						"sy": sy,
						"academic_years_id": academic_years_id,
						"description": description,
						"group_name": group_name,
						"acad_program_groups_id": acad_program_groups_id,
						"step": "change_pulldown" },
				dataType: 'json',
				success: function(response) {													

					$('#show_fees_schedule').html(response.output); 
					
					$('#show_change_data_icon').html('');
					
				}
			});

		});
	//});
</script> 
