<div style="float:left; width:auto;">
<form action="<?php echo site_url("posting/remove_batch")?>" method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	
	
	
<div class="formSep">
		<h3>Search Course Offering</h3>
	</div> 

		<fieldset>
			<div class="control-group formSep">
				<label for="" class="control-label">Course Offering: </label>
					<div class="controls">
						<select name="offering" class="form">
							 <?php
								foreach($offerings AS $course) {
									print("<option value=".$course->id.">". $course->course_code. " [".$course->section_code."] - " .$course->emp_name. "</option>");	
								}  ?> </select>			
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">Continue</button>
					</div>
				</div>	
		</fieldset>
	</form>           
</div>