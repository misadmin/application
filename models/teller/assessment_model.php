<?php

	class Assessment_Model extends MY_Model {
	
  		function __construct() {
        	parent::__construct();
			
   		}

		
		function CheckStudentHasAssessment($student_histories_id) {
			$result = NULL;				
			$q1 = "
				SELECT a.id
				FROM assessments AS a
				JOIN ledger l on l.assessments_id = a.id 						
				WHERE a.student_histories_id = {$this->db->escape($student_histories_id)} 
			";		

			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}				
			return $result;		
		}
		
		function AddAssessment($assess_data) {
			$query = "
					INSERT INTO assessments (	
								student_histories_id,
								assessed_amount, 
								transaction_date,
								employees_empno,
								academic_programs_id,
								year_level)
						VALUES (
								{$this->db->escape($assess_data['student_histories_id'])},
								{$this->db->escape($assess_data['assessed_amount'])},
								NOW(),
								{$this->db->escape($assess_data['employees_empno'])},
								{$this->db->escape($assess_data['academic_programs_id'])},
								{$this->db->escape($assess_data['year_level'])}								
						)
			";
			//log_message("INFO",print_r($query,true)); // Toyet 11.15.2018
			//print_r($query);die();

			$this->db->query($query);
			$id = $this->db->insert_id();

			//log_message("info",print_r($id,true)); 
			return $id;		
		}


		// by Toyet 4.2.2019
		function AddAssessmentHistory($data,$data_history){

			//log_message("info", print_r($data,true));

			$tuition_rate = $data_history['tuition_rate']->tuition_rate;
			$assessments_id = $data_history['assessments_inserted_id'];
			$tuition_basic_subjects = $data_history['tuition_basic_subjects'];
			$other_fees = $data_history['other_fees_details'];
			$lab_courses = $data_history['lab_courses'];
			//log_message("info", print_r($lab_courses,true));

			foreach($tuition_basic_subjects as $courses){

				$sql = "insert into assessments_history_courses 
				        (assessments_id, course_code, paying_units, rate, encoded_by) values 
				        ({$assessments_id}, '{$courses->course_code}', '{$courses->paying_units}', '{$tuition_rate}',
				         {$this->userinfo['empno']})";

				$query = $this->db->query($sql);
			}

			foreach($other_fees as $other){
				$sql = "insert into assessments_history_fees
				        (assessments_id, fees_groups_id, description, rate, encoded_by) values 
				        ({$assessments_id}, {$other->groupid}, '{$other->description}', 
				        '{$other->rate}', {$this->userinfo['empno']})";

				//log_message("info", print_r($sql,true)); // Toyet 4.4.2019
				$query = $this->db->query($sql);
			}

			foreach($lab_courses as $labs){

				$course_code = $labs->course_code;
				$rate = $labs->rate;

				$sql = "insert into assessments_history_lab
				        (assessments_id, course_code, rate, encoded_by) values 
				        ( {$assessments_id}, '{$course_code}', '{$rate}', {$this->userinfo['empno']} )";

				//log_message("info", print_r($sql,true)); // Toyet 4.4.2019
				$query = $this->db->query($sql);
			}

			return TRUE;
		}



	//Added: 4/10/2013 by genes
	function ListGroupAssessmentSummary($acad_program_groups_id, $academic_terms_id) {
			
			$result = null;
			
			$q1 = "SELECT
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								students AS aa,
								student_histories AS bb,
								assessments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_histories_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND bb.year_level = 1
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)})
						AS assess1, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								students AS aa,
								student_histories AS bb,
								assessments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_histories_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND bb.year_level = 2
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)})
						AS assess2, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								students AS aa,
								student_histories AS bb,
								assessments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_histories_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND bb.year_level = 3
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)})
						AS assess3, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								students AS aa,
								student_histories AS bb,
								assessments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_histories_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND bb.year_level = 4
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)})
						AS assess4, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								students AS aa,
								student_histories AS bb,
								assessments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_histories_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND bb.year_level = 5
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)})
						AS assess5, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								students AS aa,
								student_histories AS bb,
								assessments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_histories_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND bb.year_level = 6
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)})
						AS assess6, 
						e.abbreviation
					FROM
						students AS a,
						student_histories AS b,
						prospectus AS d,
						academic_programs AS e
					WHERE 
						a.idno = b.students_idno
						AND b.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND e.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
					GROUP BY
						e.acad_program_groups_id";
		
			

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			return $result;
		}

	
		//Added: October 30, 2013 by Amie
		//UPDATED: 9/20/14 by genes
		//to include basic ed unposted assessments
		function listunpostedassessment($academic_term) {
			$result = null;
		
			$this->load->model('hnumis/academicyears_model');
			
			$acad_yr = $this->academicyears_model->getAcademicTerms($academic_term);
			
			$q = "(select 
						sh.id, 
						s.idno, 
						concat(s.lname, ', ', s.fname, ' ', s.mname) as name, 
						ap.abbreviation, 
						sh.year_level 
					from 
						student_histories sh, 
						students s, 
						academic_programs ap, 
						prospectus p
					where sh.students_idno = s.idno
					and sh.prospectus_id = p.id
					and p.academic_programs_id = ap.id
					and sh.academic_terms_id = {$this->db->escape($academic_term)}
					and sh.id in (select e.student_history_id
							from enrollments e
							where e.status != 'Withdrawn')
					and sh.id not in (select a.student_histories_id
							from assessments a, student_histories s
					                where a.student_histories_id = s.id
					                  and s.academic_terms_id = {$this->db->escape($academic_term)})) 
					
					UNION 

					(SELECT
							a.id,
							b.idno,
							CONCAT(b.lname, ', ', b.fname, ' ', b.mname) as name,
							c.level AS abbreviation,
							a.yr_level AS year_level
						FROM
							basic_ed_histories AS a,
							students AS b,
							levels AS c
						WHERE
							b.idno = a.students_idno
							AND a.levels_id = c.id
							AND a.academic_years_id = $acad_yr->academic_years_id
							AND a.basic_ed_sections_id NOT IN (166,175)
							AND a.status = 'active'
							AND a.id NOT IN (SELECT 
													basic_ed_histories_id
												FROM
													assessments_bed)
					)
					
					ORDER BY
						name ";
			
			$query = $this->db->query($q);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		}
		
		
		function getTuitionFeeEnrolledSummary($academic_terms_id, $program_groups_id) {
			$result = null;
								
			$q = "SELECT
					(SELECT
						(SUM(g.paying_units)*h.rate)
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN fees_schedule AS h ON h.acad_program_groups_id=e.acad_program_groups_id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.fees_subgroups_id = 201
						AND h.academic_terms_id = a.academic_terms_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 1
						AND g.id NOT IN (SELECT
												x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = 1
												AND x.posted = 'Y' ))
					AS amt1,
					(SELECT
						(SUM(g.paying_units)*h.rate)
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN fees_schedule AS h ON h.acad_program_groups_id=e.acad_program_groups_id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.fees_subgroups_id = 201
						AND h.academic_terms_id = a.academic_terms_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 2
						AND g.id NOT IN (SELECT
												x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = 2
												AND x.posted = 'Y' ))
					AS amt2,
					(SELECT
						(SUM(g.paying_units)*h.rate)
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN fees_schedule AS h ON h.acad_program_groups_id=e.acad_program_groups_id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.fees_subgroups_id = 201
						AND h.academic_terms_id = a.academic_terms_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 3
						AND g.id NOT IN (SELECT
												x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = 3
												AND x.posted = 'Y' ))
					AS amt3,
					(SELECT
						(SUM(g.paying_units)*h.rate)
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN fees_schedule AS h ON h.acad_program_groups_id=e.acad_program_groups_id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.fees_subgroups_id = 201
						AND h.academic_terms_id = a.academic_terms_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 4
						AND g.id NOT IN (SELECT
												x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = 4
												AND x.posted = 'Y' ))
					AS amt4,
					(SELECT
						(SUM(g.paying_units)*h.rate)
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN fees_schedule AS h ON h.acad_program_groups_id=e.acad_program_groups_id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.fees_subgroups_id = 201
						AND h.academic_terms_id = a.academic_terms_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 5
						AND g.id NOT IN (SELECT
												x.courses_id
											FROM
												tuition_others AS x
											WHERE
												x.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND x.yr_level = 5
												AND x.posted = 'Y' ))
					AS amt5 ";
			
			//print($q); die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
				
		}


		/*function getTuitionFee_OthersSummary($academic_terms_id, $program_groups_id) {
			$result = null;
								
			$q = "SELECT
					(SELECT
						SUM(g.paying_units)*h.rate
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
							LEFT JOIN tuition_others AS h ON h.courses_id=g.id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.academic_terms_id = a.academic_terms_id
						AND h.courses_id = b.courses_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 1)
				AS t_others1,
					(SELECT
						SUM(g.paying_units)*h.rate
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
							LEFT JOIN tuition_others AS h ON h.courses_id=g.id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.academic_terms_id = a.academic_terms_id
						AND h.courses_id = b.courses_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 2)
				AS t_others2,
				(SELECT
						SUM(g.paying_units)*h.rate
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
							LEFT JOIN tuition_others AS h ON h.courses_id=g.id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.academic_terms_id = a.academic_terms_id
						AND h.courses_id = b.courses_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 3)
				AS t_others3,
				(SELECT
						SUM(g.paying_units)*h.rate
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
							LEFT JOIN tuition_others AS h ON h.courses_id=g.id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.academic_terms_id = a.academic_terms_id
						AND h.courses_id = b.courses_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 4)
				AS t_others4,
				(SELECT
						SUM(g.paying_units)*h.rate
					FROM
						student_histories AS a LEFT JOIN prospectus AS d ON a.prospectus_id = d.id
							LEFT JOIN academic_programs AS e ON d.academic_programs_id = e.id
							LEFT JOIN enrollments AS c ON a.id = c.student_history_id
							LEFT JOIN course_offerings AS b ON b.id = c.course_offerings_id
							LEFT JOIN courses AS g ON g.id = b.courses_id
							LEFT JOIN tuition_others AS h ON h.courses_id=g.id
					WHERE
						e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND h.academic_terms_id = a.academic_terms_id
						AND h.courses_id = b.courses_id
						AND h.yr_level = a.year_level 
						AND a.year_level = 5)
				AS t_others5 ";
			
			//print($q); die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
				
		}*/

		function getTuitionFee_OthersSummary($academic_terms_id, $program_groups_id) {
			$result = null;
								
			$q = "SELECT
						(SELECT
							SUM(amt) AS t_others1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*g.paying_units*h.rate) AS amt
							FROM
								student_histories AS a, 
								prospectus AS d, 
								academic_programs AS e,
								enrollments AS c,
								course_offerings AS b,
								courses AS g,
								tuition_others AS h 
							WHERE
								a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND a.id = c.student_history_id	
								AND b.id = c.course_offerings_id
								AND g.id = b.courses_id
								AND h.courses_id=g.id
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND h.academic_terms_id = a.academic_terms_id
								AND h.courses_id = b.courses_id
								AND h.yr_level = a.year_level 
								AND a.year_level = 1
							GROUP BY
								g.id) AS other1)
						AS t_others1,
						(SELECT
							SUM(amt) AS t_others1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*g.paying_units*h.rate) AS amt
							FROM
								student_histories AS a, 
								prospectus AS d, 
								academic_programs AS e,
								enrollments AS c,
								course_offerings AS b,
								courses AS g,
								tuition_others AS h 
							WHERE
								a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND a.id = c.student_history_id	
								AND b.id = c.course_offerings_id
								AND g.id = b.courses_id
								AND h.courses_id=g.id
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND h.academic_terms_id = a.academic_terms_id
								AND h.courses_id = b.courses_id
								AND h.yr_level = a.year_level 
								AND a.year_level = 2
							GROUP BY
								g.id) AS other2)
						AS t_others2,
						(SELECT
							SUM(amt) AS t_others1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*g.paying_units*h.rate) AS amt
							FROM
								student_histories AS a, 
								prospectus AS d, 
								academic_programs AS e,
								enrollments AS c,
								course_offerings AS b,
								courses AS g,
								tuition_others AS h 
							WHERE
								a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND a.id = c.student_history_id	
								AND b.id = c.course_offerings_id
								AND g.id = b.courses_id
								AND h.courses_id=g.id
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND h.academic_terms_id = a.academic_terms_id
								AND h.courses_id = b.courses_id
								AND h.yr_level = a.year_level 
								AND a.year_level = 3
							GROUP BY
								g.id) AS other3)
						AS t_others3,
						(SELECT
							SUM(amt) AS t_others1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*g.paying_units*h.rate) AS amt
							FROM
								student_histories AS a, 
								prospectus AS d, 
								academic_programs AS e,
								enrollments AS c,
								course_offerings AS b,
								courses AS g,
								tuition_others AS h 
							WHERE
								a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND a.id = c.student_history_id	
								AND b.id = c.course_offerings_id
								AND g.id = b.courses_id
								AND h.courses_id=g.id
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND h.academic_terms_id = a.academic_terms_id
								AND h.courses_id = b.courses_id
								AND h.yr_level = a.year_level 
								AND a.year_level = 4
							GROUP BY
								g.id) AS other4)
						AS t_others4,
						(SELECT
							SUM(amt) AS t_others1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*g.paying_units*h.rate) AS amt
							FROM
								student_histories AS a, 
								prospectus AS d, 
								academic_programs AS e,
								enrollments AS c,
								course_offerings AS b,
								courses AS g,
								tuition_others AS h 
							WHERE
								a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND a.id = c.student_history_id	
								AND b.id = c.course_offerings_id
								AND g.id = b.courses_id
								AND h.courses_id=g.id
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND h.academic_terms_id = a.academic_terms_id
								AND h.courses_id = b.courses_id
								AND h.yr_level = a.year_level 
								AND a.year_level = 5
							GROUP BY
								g.id) AS other5)
						AS t_others5";
			
			//print($q); die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
				
		}
		
		function getMatriculationSummary($academic_terms_id, $program_groups_id) {
			$result = NULL;	
			$q1 = "SELECT 
						(SELECT
							COUNT(DISTINCT(a.students_idno))*d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							enrollments AS e
						WHERE
							a.prospectus_id = b.id
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.fees_subgroups_id = 14
							AND d.academic_terms_id = a.academic_terms_id
							AND d.yr_level = a.year_level
							AND a.year_level = 1
							AND d.posted = 'Y'
							AND e.student_history_id = a.id
							AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)})
					AS matri1,
						(SELECT
							COUNT(DISTINCT(a.students_idno))*d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							enrollments AS e
						WHERE
							a.prospectus_id = b.id
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.fees_subgroups_id = 14
							AND d.academic_terms_id = a.academic_terms_id
							AND d.yr_level = a.year_level
							AND a.year_level = 2
							AND d.posted = 'Y'
							AND e.student_history_id = a.id
							AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)})
					AS matri2,
					(SELECT
							COUNT(DISTINCT(a.students_idno))*d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							enrollments AS e
						WHERE
							a.prospectus_id = b.id
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.fees_subgroups_id = 14
							AND d.academic_terms_id = a.academic_terms_id
							AND d.yr_level = a.year_level
							AND a.year_level = 3
							AND d.posted = 'Y'
							AND e.student_history_id = a.id
							AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)})
					AS matri3,
					(SELECT
							COUNT(DISTINCT(a.students_idno))*d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							enrollments AS e
						WHERE
							a.prospectus_id = b.id
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.fees_subgroups_id = 14
							AND d.academic_terms_id = a.academic_terms_id
							AND d.yr_level = a.year_level
							AND a.year_level = 4
							AND d.posted = 'Y'
							AND e.student_history_id = a.id
							AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)})
					AS matri4,
					(SELECT
							COUNT(DISTINCT(a.students_idno))*d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							enrollments AS e
						WHERE
							a.prospectus_id = b.id
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.fees_subgroups_id = 14
							AND d.academic_terms_id = a.academic_terms_id
							AND d.yr_level = a.year_level
							AND a.year_level = 5
							AND d.posted = 'Y'
							AND e.student_history_id = a.id
							AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)})
					AS matri5 ";
						
			//print($q1); die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
			
			return $result;
		
		}

		
		function getMiscSummary($academic_terms_id, $program_groups_id) {
			$result = NULL;	
			$q1 = "SELECT
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 2
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 1
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS m1,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 2
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 2
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS m2,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 2
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 3
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS m3,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 2
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 4
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS m4,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 2
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 5
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS m5 ";
						
			//print($q1); die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
			
			return $result;
		
		}
	
		function getOtherSchoolFeesSummary($academic_terms_id, $program_groups_id) {
			$result = NULL;	
			$q1 = "SELECT
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 3
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 1
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS osf1,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 3
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 2
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS osf2,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 3
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 3
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS osf3,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 3
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 4
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS osf4,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 3
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 5
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS osf5 ";
						
			//print($q1); die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}

		
		function getAddtnlOtherFeesSummary($academic_terms_id, $program_groups_id) {
			$result = NULL;	
			$q1 = "SELECT
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 4
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 1
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS add_other_fee1,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 4
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 2
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS add_other_fee2,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 4
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 3
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS add_other_fee3,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 4
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 4
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS add_other_fee4,
						(SELECT 
							SUM(amt) AS misc1
						FROM
							(SELECT
								(COUNT(DISTINCT(a.students_idno))*d.rate) AS amt
							FROM
								student_histories AS a,
								prospectus AS b,
								academic_programs AS c,
								fees_schedule AS d,
								enrollments AS e,
								fees_subgroups AS f
							WHERE
								a.prospectus_id = b.id
								AND b.academic_programs_id = c.id
								AND c.acad_program_groups_id = d.acad_program_groups_id
								AND f.fees_groups_id = 4
								AND d.academic_terms_id = a.academic_terms_id
								AND d.yr_level = a.year_level
								AND a.year_level = 5
								AND d.posted = 'Y'
								AND e.student_history_id = a.id
								AND f.id=d.fees_subgroups_id
								AND c.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							GROUP BY
								f.id) AS misc1) 
						AS add_other_fee5 ";
						
			//print($q1); die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}

		
		function getLaboratoryFeesSummary($academic_terms_id, $program_groups_id) {
			$result = null;
								
			$q = "SELECT
						(SELECT
							SUM(amt) AS lab_fee
						FROM
							(SELECT 
								(COUNT(DISTINCT(a.students_idno))*h.rate) AS amt
							FROM
								student_histories AS a,
								course_offerings AS b,
								enrollments AS c,
								prospectus AS d,
								academic_programs AS e,
								acad_program_groups AS f,
								academic_terms AS i,
								courses AS g LEFT JOIN laboratory_fees AS h ON g.id = h.courses_id
							WHERE
								a.id = c.student_history_id
								AND b.id = c.course_offerings_id
								AND a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND e.acad_program_groups_id = f.id
								AND g.id = b.courses_id
								AND a.academic_terms_id = i.id
								AND i.id = {$this->db->escape($academic_terms_id)}
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND h.academic_years_id = i.academic_years_id
								AND a.year_level = 1
								AND h.posted = 'Y'
							GROUP BY
								g.course_code) AS lab_fee1)
						AS lab_fee1,
						(SELECT
							SUM(amt) AS lab_fee
						FROM
							(SELECT 
								(COUNT(DISTINCT(a.students_idno))*h.rate) AS amt
							FROM
								student_histories AS a,
								course_offerings AS b,
								enrollments AS c,
								prospectus AS d,
								academic_programs AS e,
								acad_program_groups AS f,
								academic_terms AS i,
								courses AS g LEFT JOIN laboratory_fees AS h ON g.id = h.courses_id
							WHERE
								a.id = c.student_history_id
								AND b.id = c.course_offerings_id
								AND a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND e.acad_program_groups_id = f.id
								AND g.id = b.courses_id
								AND a.academic_terms_id = i.id
								AND i.id = {$this->db->escape($academic_terms_id)}
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND h.academic_years_id = i.academic_years_id
								AND a.year_level = 2
								AND h.posted = 'Y'
							GROUP BY
								g.course_code) AS lab_fee2)
						AS lab_fee2,
						(SELECT
							SUM(amt) AS lab_fee
						FROM
							(SELECT 
								(COUNT(DISTINCT(a.students_idno))*h.rate) AS amt
							FROM
								student_histories AS a,
								course_offerings AS b,
								enrollments AS c,
								prospectus AS d,
								academic_programs AS e,
								acad_program_groups AS f,
								academic_terms AS i,
								courses AS g LEFT JOIN laboratory_fees AS h ON g.id = h.courses_id
							WHERE
								a.id = c.student_history_id
								AND b.id = c.course_offerings_id
								AND a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND e.acad_program_groups_id = f.id
								AND g.id = b.courses_id
								AND a.academic_terms_id = i.id
								AND i.id = {$this->db->escape($academic_terms_id)}
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND h.academic_years_id = i.academic_years_id
								AND a.year_level = 3
								AND h.posted = 'Y'
							GROUP BY
								g.course_code) AS lab_fee3)
						AS lab_fee3,
						(SELECT
							SUM(amt) AS lab_fee
						FROM
							(SELECT 
								(COUNT(DISTINCT(a.students_idno))*h.rate) AS amt
							FROM
								student_histories AS a,
								course_offerings AS b,
								enrollments AS c,
								prospectus AS d,
								academic_programs AS e,
								acad_program_groups AS f,
								academic_terms AS i,
								courses AS g LEFT JOIN laboratory_fees AS h ON g.id = h.courses_id
							WHERE
								a.id = c.student_history_id
								AND b.id = c.course_offerings_id
								AND a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND e.acad_program_groups_id = f.id
								AND g.id = b.courses_id
								AND a.academic_terms_id = i.id
								AND i.id = {$this->db->escape($academic_terms_id)}
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND h.academic_years_id = i.academic_years_id
								AND a.year_level = 4
								AND h.posted = 'Y'
							GROUP BY
								g.course_code) AS lab_fee4)
						AS lab_fee4,
						(SELECT
							SUM(amt) AS lab_fee
						FROM
							(SELECT 
								(COUNT(DISTINCT(a.students_idno))*h.rate) AS amt
							FROM
								student_histories AS a,
								course_offerings AS b,
								enrollments AS c,
								prospectus AS d,
								academic_programs AS e,
								acad_program_groups AS f,
								academic_terms AS i,
								courses AS g LEFT JOIN laboratory_fees AS h ON g.id = h.courses_id
							WHERE
								a.id = c.student_history_id
								AND b.id = c.course_offerings_id
								AND a.prospectus_id = d.id
								AND d.academic_programs_id = e.id
								AND e.acad_program_groups_id = f.id
								AND g.id = b.courses_id
								AND a.academic_terms_id = i.id
								AND i.id = {$this->db->escape($academic_terms_id)}
								AND e.acad_program_groups_id = {$this->db->escape($program_groups_id)}
								AND h.academic_years_id = i.academic_years_id
								AND a.year_level = 5
								AND h.posted = 'Y'
							GROUP BY
								g.course_code) AS lab_fee5)
						AS lab_fee5
						";
			
			//print($q); die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
				
		}
		

		function CheckAcadTermHasAssessment($academic_terms_id) {
		
			$q1 = "SELECT
							a.id
						FROM
							assessments AS a,
							student_histories AS b
						WHERE
							a.student_histories_id = b.id
							AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
		
			$query = $this->db->query($q1);
		
			if($query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		
		}
		
		/*
		 * @ADDED: 8/28/14 
		 * @AUTHOR: genes
		 */
		function getTuitionFee_Others($student_histories_id, $courses_id) {
			$result = null;
			
			$q = "SELECT 
						(a.rate * n.paying_units) AS amt
					FROM 
						tuition_others AS a,
						enrollments AS m,
						courses AS n,
						course_offerings AS o,
						student_histories AS p
					WHERE
						a.courses_id = n.id
						AND p.id = m.student_history_id
						AND o.courses_id = n.id
						AND o.id=m.course_offerings_id
						AND m.student_history_id = {$this->db->escape($student_histories_id)}
						AND p.year_level = a.yr_level
						AND p.academic_terms_id = a.academic_terms_id
						AND n.id = {$this->db->escape($courses_id)} 
					";
							
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		}
		
		
		/*
		 * @ADDED: 8/28/14 
		 * @AUTHOR: genes
		 */
		function getLab_Fee($student_histories_id, $courses_id) {
			$result = null;
				
			$q = "SELECT 
						b.rate 
					FROM
						laboratory_fees AS b,
						enrollments AS c,
						course_offerings AS d,
						academic_terms AS e,
						student_histories AS f
					WHERE
						b.courses_id = d.courses_id
						AND c.course_offerings_id = d.id
						AND f.id = c.student_history_id
						AND f.academic_terms_id = e.id
						AND b.academic_years_id = e.academic_years_id
						AND c.student_history_id = {$this->db->escape($student_histories_id)}
						AND b.courses_id = {$this->db->escape($courses_id)}
					";
							
			//print($q); die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
				
		}

	/**
	 * Queries Assessments in assessments table
	 * @author Toyet Amores
	 * @param    
	 * $date_start - starting date of transactions to get
	 * $date_end - starting date of transactions to get
	 * @return   
	 * array of Assessments
	*/
	function getComputedAssessment($date_start,$date_end) {
		$result = null;
			
		$q = "SELECT 
					    levels_id,
					    course,
					    sum(amount1) as amount1,
					    sum(amount2) as amount2,
					    sum(amount3) as amount3,
					    sum(amount4) as amount4,
					    sum(amount5) as amount5,
		                sum(amount1+amount2+amount3+
		                amount4+amount5) as total
					FROM
					    (
		                SELECT 
		        DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		            COALESCE(ag.levels_id, 99) AS levels_id,
		            COALESCE(ag.abbreviation, (CASE ag.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END), 'n/a') AS course,
		            COALESCE(ass.year_level, 'n/a') AS year_level,
		            (CASE ass.year_level
		                WHEN 1 THEN sum(ass.assessed_amount)
		                WHEN 11 THEN sum(ass.assessed_amount)
		                ELSE 0
		            END) AS amount1,
		            (CASE ass.year_level
		                WHEN 2 THEN sum(ass.assessed_amount)
		                WHEN 12 THEN sum(ass.assessed_amount)
		                ELSE 0
		            END) AS amount2,
		            (CASE ass.year_level
		                WHEN 3 THEN sum(ass.assessed_amount)
		                ELSE 0
		            END) AS amount3,
		            (CASE ass.year_level
		                WHEN 4 THEN sum(ass.assessed_amount)
		                ELSE 0
		            END) AS amount4,
		            (CASE ass.year_level
		                WHEN 5 THEN sum(ass.assessed_amount)
		                ELSE 0
		            END) AS amount5,
		            (CASE levels_id
		                WHEN 1 THEN 2
		                WHEN 2 THEN 3
		                WHEN 3 THEN 4
		                WHEN 4 THEN 5
		                WHEN 5 THEN 6
		                WHEN 6 THEN 8
		                WHEN 7 THEN 9
		                WHEN 11 THEN 1
		                WHEN 12 THEN 7
		                WHEN 99 THEN 99
		            END) AS sequencer
		    FROM
		        ledger l
		    LEFT JOIN assessments ass ON ass.id = l.assessments_id
		    LEFT JOIN student_histories sh ON sh.id = ass.student_histories_id
		    LEFT JOIN col_students s ON s.students_idno = sh.students_idno
		    LEFT JOIN students st ON st.idno = sh.students_idno
		    JOIN prospectus pr ON pr.id = sh.prospectus_id
		    JOIN academic_programs ap ON ap.id = pr.academic_programs_id
		    JOIN acad_program_groups ag ON ag.id = ap.acad_program_groups_id
		    JOIN colleges cl ON cl.id = ap.colleges_id
		    WHERE
		        l.transaction_date BETWEEN '{$date_start}' AND DATE_ADD('{$date_end}', INTERVAL '23:59:59' HOUR_SECOND)
		    group BY sequencer , course , year_level) as data1
		    group by sequencer, course
				";
						
		//print($q); die();
		$query = $this->db->query($q);
			
		////log_message('INFO',print_r($q,true));
		////log_message('INFO',print_r($query->result(),true));

		return ($query ? $query->result() : FALSE);
			
	}
		

}

