<div class="bs-docs-canvas" >
<div class="container" style="width:100%;">
  <div style="background:#059305; height:55px;">
    <table width="100%" border="0" align="left" cellpadding="2" cellspacing="0" 
    	style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#FFF;">
    <tr  >
      <td width="9%" rowspan="2" >&nbsp; </td>
      <td width="91%" align="left" valign="top"><?php print("[".$this->session->userdata('employees_empno')."] ".$this->session->userdata('neym')); ?></td>
      </tr>
    <tr  >
      <td align="left" valign="top"><?php print("[".$this->session->userdata('college_code')."] - ".$this->session->userdata('college_name')); ?></td>
    </tr>
  </table>
  </div>
<div class="bs-docs-example">
            <div id="navbar-example" class="navbar navbar-static" >
              <div class="navbar-inner" style="font-size:12px; height:8px;">
                <div class="container" style="width: auto;">
                  <a class="brand" href="<?php echo site_url('dean/dean');?>" style="font-size:10px;"><i class="icon-home"></i></a>
                  <ul class="nav" role="navigation">
                    <li class="dropdown">
                      <a id="drop1" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Program<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/create_program');?>">Create Program</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a id="drop2" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Course Offerings<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop2">
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/offer_course_schedules');?>">Offer Course Schedules</a></li>
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/offer_schedule_slots/1');?>">Offer Schedule Slots</a></li>
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/assign_faculty_to_schedule/1');?>">Assign Faculty</a></li>
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/dissolve_section');?>">Assign Block Section</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/dissolve_section');?>">Dissolve Section</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">Student<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/advise_academic_program');?>">Advise Academic Program</a></li>
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/advise_course_to_student');?>">Advise Course to Student</a></li>
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/advise_max_units/1')
						;?>">Advise Maximum Units</a></li>
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/create_remarks/1');?>">Create Remarks</a></li>
                      </ul>
                    </li>
                     <li class="dropdown">
                      <a href="#" id="drop4" role="button" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop4">
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/view_class_list/1');?>">View Class List</a></li>
                        <li><a tabindex="-1" href="<?php echo site_url('dean/dean/view_course_offerings/1');?>">View Course Offerings</a></li>
                      </ul>
                    </li>
                 </ul>
                  <ul class="nav pull-right">
                    <li id="fat-menu" class="dropdown">
                      <a href="#" id="drop5" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i><i class="caret"></i></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop5">
                        <li><a tabindex="-1" href="<?php echo site_url('dean/auth/logout');?>">Log-out</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div> <!-- /navbar-example -->
  </div> 

</div><!-- /container -->

