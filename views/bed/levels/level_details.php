
			<div style="padding:3px; overflow:auto;">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:10%;">Level ID</td>
								<td class="shs_header" style="width:15%;">Abbreviation</td>
								<td class="shs_header" style="width:45%;">Description</td>
								<td class="shs_header" style="width:10%;">No. of<br>Year Levels</td>
								<td class="shs_header" style="width:10%;">No. of Years<br>to Complete</td>
								<td class="shs_header" style="width:5%;">Update</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($levels) {
									foreach($levels AS $level) {
							?>
							<tr>
									<td style="text-align:center;"><?php print($level->id); ?></td>
									<td><?php print($level->abbreviation); ?></td>
									<td><?php print($level->group_name); ?></td>
									<td style="text-align:center;"><?php print($level->num_yr_level); ?></td>
									<td style="text-align:center;"><?php print($level->num_year); ?></td>
									<td style="text-align:center;">
											<div id="show_pencil_icon_<?php print($level->id); ?>" >
												<i class="icon-pencil"></i>
											</div>
									</td>
									<td style="text-align:center;">
											<div id="show_trash_icon_<?php print($level->id); ?>" >
												<i class="icon-trash"></i>
											</div>
									</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>


<script>
	$('.del_track').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete this track?');

		if (confirmed){
			
			var track_id = $(this).attr('href');

			$('#show_trash_icon_'+track_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/track_management');?>',
				data: { 
						track_id: track_id,
						action: 'delete_track'},
				dataType: 'json',
				success: function(response) {				
					if (!response.msg) {
						$('#show_error_msg').html('Cannot be removed because Strands are assigned!'); 							
						$('#show_trash_icon_'+track_id).html("<i class='icon-trash'></i>")
					} else {
						$('#show_error_msg').html(''); 	
						$('#show_extracted_items').html(response.output); 	
					}					
				}
			});
		
		}

	});
</script>


<!-- JQ to update track-->
<script>
	$('.update_track').click(function(event){
		event.preventDefault();

		var track_id     = $(this).attr('href');
		var abbreviation = $(this).attr('abbreviation');
		var group_name   = $(this).attr('group_name');
		var num_year     = $(this).attr('num_year');
		
		$('#modal_update_track').modal('show');
		
		$('#track_id_modal').val(track_id);
		$('#abbreviation_update_modal').val(abbreviation);
		$('#group_name_update_modal').val(group_name);
		$('#num_year_update_modal').val(num_year);
		
	});
</script>				


<!-- Modal HTML 
	Modal to Update a track
-->
	<div id="modal_update_track" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_track_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Track</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="track_id_modal"></div>
						<?php														
							$this->load->view('shs/tracks/modal_view/update_track_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- End Modal -->


<!-- JQ to submit an update to a track -->
<script>
	$('#update_track_form').submit(function(e) {
		e.preventDefault();
		
		var track_id     = $('#track_id_modal').val();
		var abbreviation = $('#abbreviation_update_modal').val();
		var group_name   = $('#group_name_update_modal').val();
		var num_year     = $('#num_year_update_modal').val();

		$('#modal_update_track').modal('hide');

		$('#show_pencil_icon_'+track_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/track_management'); ?>",
			data: {"track_id": track_id,
					"abbreviation": abbreviation,
					"group_name": group_name,
					"num_year": num_year,
					"action": "update_track" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_items').html(response.output); 
				
				$('#show_pencil_icon_'+track_id).html("<i class='icon-pencil'></i>")
				
			}
		});
		
	});
</script>				

			