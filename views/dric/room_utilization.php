<div style="margin-top:10px">
<h2 class="heading"> Room Utilization (<?php print("SY ".$term->sy." ".$term->term); ?> )</h2>
<h2> <a href="download" class="btn btn-link pull-right" id="download_list"><i class="icon-download-alt"></i> <strong>Download</strong></a> </h2>	

<table class="table table-striped table-bordered">
	<thead >
		<tr align="center">
			<th>Bldg. Name</th>
			<th>Room</th>
			<th>Schedule Day</th>
			<th>Schedule Time</th>
			<th>Teacher</th>
			<th>Subject</th>
			<th>Section</th>
			<th># of Students</th>
		</tr>
	</thead>
	<tbody> 
<?php if($offerings){ ?>
	<?php foreach ($offerings as $offer): ?>
		<tr>
			<td><?php print($offer->bldg_name); ?></td>
			<td><?php print($offer->room_no); ?></td>
			<td><?php echo $offer->day_name; ?></td>
			<td><?php echo $offer->schedule_time; ?></td>
			<td><?php print($offer->name); ?></td>
			<td><?php print($offer->course_code); ?></td>
			<td><?php print($offer->section_code); ?></td>
			<td><?php print($offer->num_enrollees); ?></td>
		</tr>
<?php 
	endforeach; ?>
<?php } ?>
	</tbody>
</table>

<form method="post" action="<?php echo site_url("{$role}/download_room_utilization_csv"); ?>" id="download_list_form">
			<input type="hidden" name="term_id" value="<?php echo $term->id; ?>" />
			<input type="hidden" name="term" value="<?php echo $term->term; ?>" />
			<input type="hidden" name="sy" value="<?php echo $term->sy; ?>" />	
</form>

<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
		
</div> 
