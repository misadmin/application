<?php

	class dean_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}
		
		
		public function offer_course_schedules() {
		
			$this->CI->load->model('hnumis/courses_model');
			$this->CI->load->model('hnumis/offerings_model');
			$this->CI->load->model('hnumis/rooms_model');
			$this->CI->load->model('hnumis/AcademicYears_Model');
						
				switch ($this->CI->input->post('action')) {
					
					case 'extract_data_for_new_offerings':	

						$data1['courses'] = $this->CI->courses_model->ListCourses($this->CI->userinfo['college_id'],'A');
						$data2['days']    = $this->CI->offerings_model->ListDays();

						$day_names = $this->CI->rooms_model->ListDayNames($data2['days'][0]->day_code);

						foreach ($day_names AS $day) {
							$extract_days[] = $day->day_name;
						}
						$d1 = json_encode($extract_days);
						$d2 = str_replace(array('"','[',']'),array("'","(",")"),$d1);

						$data3['bates_building']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),'07:30', '08:30', $d2,3);
						$data3['scanlon_building'] = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),'07:30', '08:30', $d2,9);
						$data3['freina_building']  = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),'07:30', '08:30', $d2,7);
						$data3['other_building']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),'07:30', '08:30', $d2,11);
						$data3['hs_gym']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),'07:30', '08:30', $d2,13);
						$data3['activity_center']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),'07:30', '08:30', $d2,14);
						$data3['covered_court']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),'07:30', '08:30', $d2,15);
						
						$my_return['courses'] = $this->CI->load->view('dean/modals/pulldown_courses', $data1, TRUE);
						$my_return['days']    = $this->CI->load->view('dean/modals/pulldown_days', $data2, TRUE);
						$my_return['rooms']   = $this->CI->load->view('dean/modals/display_vacant_rooms', $data3, TRUE);
						
						echo json_encode($my_return,JSON_HEX_APOS);
					
						return;
						
					case 'extract_vacant_rooms':	

						$start_time = strtotime($this->CI->input->post('start_time'));
						$hr         = date('h', strtotime($this->CI->input->post('num_hr')));
						$min        = date('i', strtotime($this->CI->input->post('num_hr')));
					
						$end_time   = date("H:i:00", strtotime('+'.$hr.' hours +'.$min.' minutes', $start_time));
						
						$day_names = $this->CI->rooms_model->ListDayNames($this->CI->input->post('day_code'));

						$start_time = date("H:i:00", strtotime($this->CI->input->post('start_time')));
						
						foreach ($day_names AS $day) {
							$extract_days[] = $day->day_name;
						}
						$d1 = json_encode($extract_days);
						$d2 = str_replace(array('"','[',']'),array("'","(",")"),$d1);
						
						//$my_return['rooms']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),$start_time, $end_time, $d2,9);
						
						//echo json_encode($my_return,JSON_HEX_APOS);
					
						//return;
						$data3['bates_building']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),$start_time, $end_time, $d2,3);
						$data3['scanlon_building'] = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),$start_time, $end_time, $d2,9);
						$data3['freina_building']  = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),$start_time, $end_time, $d2,7);
						$data3['other_building']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),$start_time, $end_time, $d2,11);
						$data3['hs_gym']           = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),$start_time, $end_time, $d2,13);
						$data3['activity_center']  = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),$start_time, $end_time, $d2,14);
						$data3['activity_center']  = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('term_id'),$start_time, $end_time, $d2,15);
						
						$my_return['rooms']   = $this->CI->load->view('dean/modals/display_vacant_rooms', $data3, TRUE);
						
						echo json_encode($my_return,JSON_HEX_APOS);
					
						return;
						
					case 'save_offering':
					
						if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){

							$s_code = $this->CI->offerings_model->getLastSection($this->CI->input->post('courses_id'), $this->CI->input->post('academic_terms_id'));
					
							//generate the last section
							if ($s_code) {
								$data['section_code'] = $this->NewSectionCode($s_code->section_code); 
							} else {
								$data['section_code'] = "A"; 
							}
			
							if ($this->CI->input->post('sched_type') == 'fixed') {

								$start_time = strtotime($this->CI->input->post('start_time'));
								$hr         = date('h', strtotime($this->CI->input->post('num_hr')));
								$min        = date('i', strtotime($this->CI->input->post('num_hr')));
							
								$end_time   = date("H:i:s", strtotime('+'.$hr.' hours +'.$min.' minutes', $start_time));
							
								$start_time = date("H:i:s", strtotime($this->CI->input->post('start_time')));
								
							} else { //flexible time is from 7:30AM - 10:30PM
							
								$start_time = "07:30:00";
								$end_time   = "22:30:00";
							
							}
							
							$data['academic_terms_id']	= $this->CI->input->post('academic_terms_id');
							$data['courses_id']        	= $this->CI->input->post('courses_id');
							$data['rooms_id']   		= $this->CI->input->post('room_id');
							$data['start_time'] 		= $start_time;
							$data['end_time'] 			= $end_time;
							$data['days_day_code'] 		= $this->CI->input->post('day_code');
							$data['max_students'] 		= $this->CI->input->post('max_students');
							$data['additional_charge'] 	= $this->CI->input->post('additional_charge');
							$data['employees_empno'] 	= "";
							$data['check_conflict']  	= TRUE;
							$data['no_offering']	  	= FALSE;
							$data['sched_type']         = $this->CI->input->post('sched_type');

							$this->CI->db->trans_start();

							$room_result = $this->CI->offerings_model->AddOfferingSlot($data);
					
							if ($room_result['conflict']) {
								
								$this->CI->db->trans_rollback();

								$error_msg = "Course NOT offered because in conflict with:<br>";

								foreach($room_result['schedules'] AS $schedule) {
									$sched = $schedule->course_code."[".$schedule->section_code."] ".
												$schedule->start_time."-".$schedule->end_time." ".
												$schedule->days_day_code." ".$schedule->room_no."<br>";
									$error_msg .= $sched;
								}
								$this->CI->content_lib->set_message($error_msg, 'alert-error');

							} else {
								if (!$room_result['rm_blocking']) {
									$this->CI->db->trans_rollback();			
									$this->CI->content_lib->set_message("ERROR in Adding Room Blocking!", 'alert-error');
								} else {
									$this->CI->db->trans_complete();
									$this->CI->content_lib->set_message("Course successfully offered!", 'alert-success');
								}
							}
					
						} else {
							$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
						}
							
						break;
						
					case 'extract_vacant_rooms_for_update':

						$start_time = strtotime($this->CI->input->post('start_time'));
						$hr         = date('h', strtotime($this->CI->input->post('num_hr')));
						$min        = date('i', strtotime($this->CI->input->post('num_hr')));
					
						$end_time   = date("H:i:00", strtotime('+'.$hr.' hours +'.$min.' minutes', $start_time));
						$start_time = date("H:i:00", strtotime($this->CI->input->post('start_time')));
						
						$day_names = $this->CI->rooms_model->ListDayNames($this->CI->input->post('day_code'));

						foreach ($day_names AS $day) {
							$extract_days[] = $day->day_name;
						}
						$d1 = json_encode($extract_days);
						$d2 = str_replace(array('"','[',']'),array("'","(",")"),$d1);

						$data3['bates_building']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('academic_terms_id'),
														$start_time, $end_time, $d2,3,$this->CI->input->post('rooms_id'),$this->CI->input->post('course_offerings_slots_id'));
						$data3['scanlon_building'] = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('academic_terms_id'),
														$start_time, $end_time, $d2,9,$this->CI->input->post('rooms_id'),$this->CI->input->post('course_offerings_slots_id'));
						$data3['freina_building']  = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('academic_terms_id'),
														$start_time, $end_time, $d2,7,$this->CI->input->post('rooms_id'),$this->CI->input->post('course_offerings_slots_id'));
						$data3['other_building']   = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('academic_terms_id'),
														$start_time, $end_time, $d2,11,$this->CI->input->post('rooms_id'),$this->CI->input->post('course_offerings_slots_id'));
						$data3['hs_gym']           = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('academic_terms_id'),
														$start_time, $end_time, $d2,13,$this->CI->input->post('rooms_id'),$this->CI->input->post('course_offerings_slots_id'));
						$data3['activity_center']  = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('academic_terms_id'),
														$start_time, $end_time, $d2,14,$this->CI->input->post('rooms_id'),$this->CI->input->post('course_offerings_slots_id'));
						$data3['covered_court']  = $this->CI->rooms_model->ListVacantRooms($this->CI->input->post('academic_terms_id'),
														$start_time, $end_time, $d2,15,$this->CI->input->post('rooms_id'),$this->CI->input->post('course_offerings_slots_id'));

						$data3['old_bldgs_id']     = $this->CI->input->post('old_bldgs_id');
						$data3['rooms_id']         = $this->CI->input->post('rooms_id');
						
						
						$data2['days']         = $this->CI->offerings_model->ListDays();
						$data2['selected_day'] = $this->CI->input->post('day_code');
						
						$my_return['days']  = $this->CI->load->view('dean/modals/pulldown_days_for_update', $data2, TRUE);
						$my_return['rooms'] = $this->CI->load->view('dean/modals/display_vacant_rooms', $data3, TRUE);
						
						echo json_encode($my_return,JSON_HEX_APOS);
					
						return;
					
				}
				
				$data['terms']     = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
				$data['offerings'] = $this->CI->offerings_model->ListOfferings($this->CI->input->post('academic_terms_id'),$this->CI->session->userdata('college_id'));
			
				$data['selected_term'] = $this->CI->input->post('academic_terms_id');

				$this->CI->content_lib->enqueue_footer_script('data_tables');
				$this->CI->content_lib->enqueue_body_content('dean/list_schedule_to_select',$data);
				$this->CI->content_lib->content();

		}

		
		
		private function NewSectionCode($section_code) {
			$s = 0;
			$s1="";
			$s2="";
			$code="";
				
			$s = strlen($section_code);
							
			if ($s == 1 AND ($section_code != 'Z')) {
				$code = CHR(ORD($section_code) + 1);
			} elseif ($s == 1 AND ($section_code == 'Y')) {
				$code = 'Z';
			} elseif ($s == 1 AND ($section_code == 'Z')) {
				$code = 'AA';
			} elseif ($s > 1) {
				$s1 = substr($section_code,0,1);
				$s2 = substr($section_code,1,1);					
				if ($s1 != 'Z') {
					if ($s2 != 'Z') {
						$code = $s1.CHR((ORD($s2) + 1));
					} else {
						$s1 = CHR(ORD($s1) + 1);
						$s2 = 'A';						
						$code = $s1.$s2;
					}
				} else {
					if ($s2 != 'Z') {
						$code = $s1.CHR((ORD($s2) + 1));
					} 
				}							
			}
			
			return $code ;
			
		}
		
		
		
	}

?>	