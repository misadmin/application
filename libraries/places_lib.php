<?php

	class places_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}
		
		
		function extract_places() {
			$this->CI->load->model('places_model');
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_provinces');

			switch ($action) {
				case 'list_countries':
					$data['countries'] = $this->CI->places_model->ListCountries();
					$this->CI->load->view('common/places/select_countries', $data);
				
					break;
					
				case 'list_provinces':
					$data['provinces'] = $this->CI->places_model->fetch_results($this->CI->input->post('country_id'),'provinces');
					$data['type']      = $this->CI->input->post('type');
					$this->CI->load->view('common/places/select_provinces', $data);
				
					break;
				
				case 'list_towns':
					$data['towns'] = $this->CI->places_model->fetch_results($this->CI->input->post('province_id'),'towns');
					$data['type']  = $this->CI->input->post('type');
					$this->CI->load->view('common/places/select_towns', $data);
					
					break;
					
				case 'list_brgys':
					$data['brgys'] = $this->CI->places_model->fetch_results($this->CI->input->post('town_id'),'barangays');
					
					if ($this->CI->input->post('type') == 'new_student') {
						$this->CI->load->view('common/places/select_brgys', $data);
					} else {
						$this->CI->load->view('common/places/select_brgys_for_update_emergency', $data);						
					}
				
					break;
			}
			
		}
		
	}

?>