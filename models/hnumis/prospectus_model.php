<?php

	class Prospectus_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
			
   		}

			
		function ListPrereq_ByCourse($prospectus_courses_id) {
			
			$result = null;
			
			$q1 = "SELECT 
						a.id, 
						b.prospectus_courses_id, 
						a.prereq_type,
						d.course_code
					FROM 
						prerequisites AS a, 
						prereq_courses AS b,
						prospectus_courses AS c,
						courses AS d
					WHERE 
						a.id=b.prerequisites_id
						AND b.prospectus_courses_id=c.id
						AND c.courses_id=d.id
						AND a.prospectus_courses_id={$this->db->escape($prospectus_courses_id)} 
						AND a.prereq_type='C' " ;
										
			$query = $this->db->query($q1);
					
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}


		function ListPrereq_Elective($elective_courses_id) {
			
			$result = null;
			
			//combine data from elective_topic_prereq_courses AND elective_topic_prereq_yrlevel		
			$q1 = "(SELECT 
						a.id, 
						e.id AS courses_id,
						b.prospectus_courses_id as child_pros_course_id,
						e.course_code,
						NULL AS yr_level,
						a.prereq_type, 'N' as from_prospectus
					FROM 
						prospectus_courses AS d,
						courses AS e,
						elective_topic_prereq AS a,
						elective_topic_prereq_courses AS b 
					WHERE 
						d.id=b.prospectus_courses_id 
						AND d.courses_id=e.id
						AND a.id=b.elective_topic_prereq_id
					    AND a.elective_courses_id={$this->db->escape($elective_courses_id)})
					UNION 
					(SELECT 
						a.id, 
						NULL AS courses_id,
						NULL as child_pros_course_id,
						NULL AS course_code,
						b.yr_level,
						a.prereq_type, 'N' as from_prospectus
					FROM 
						elective_topic_prereq AS a,
						elective_topic_prereq_yrlevel AS b 
					WHERE 
						a.id=b.elective_topic_prereq_id
					    AND a.elective_courses_id={$this->db->escape($elective_courses_id)})";
										
			$query = $this->db->query($q1);
			
			//print($q1);
			//die();
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}


		function ListPrereq_ByPrereq($prerequisites_id) {
			
			$result = null;
			
			$q1 = "SELECT c.id, c.course_code
						FROM prereq_courses AS a, prospectus_courses AS b,
							courses AS c
						WHERE a.prospectus_courses_id=b.id AND b.courses_id=c.id
							AND a.prerequisites_id= {$this->db->escape($prerequisites_id)} " ;
										
			$query = $this->db->query($q1);
			
			//print($q1);
			//die();
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}

		
		function ListPrereq_Courses($prospectus_courses_id,$prereq_type='M') {
			
			$result = NULL;
			
			if ($prereq_type == 'M') {		
				$q1 = "SELECT d.id, 
								c.id AS courses_id,
								a.prerequisites_id, 
								c.course_code, 
								c.descriptive_title, 
								d.prospectus_courses_id
							FROM 
								prereq_courses AS a, 
								prospectus_courses AS b,
								courses AS c, 
								prerequisites AS d
							WHERE 
								a.prospectus_courses_id=b.id 
								AND b.courses_id=c.id 
								AND d.id=a.prerequisites_id
								AND d.prospectus_courses_id={$this->db->escape($prospectus_courses_id)} 
								AND a.prereq_type = 'M'
							GROUP BY 
								c.course_code 
							ORDER BY
								a.id " ;
			} else { //list all prereq
				$q1 = "SELECT d.id, 
								c.id AS courses_id,
								a.prerequisites_id, 
								c.course_code, 
								c.descriptive_title, 
								d.prospectus_courses_id
							FROM 
								prereq_courses AS a, 
								prospectus_courses AS b,
								courses AS c, 
								prerequisites AS d
							WHERE 
								a.prospectus_courses_id=b.id 
								AND b.courses_id=c.id 
								AND d.id=a.prerequisites_id
								AND d.prospectus_courses_id={$this->db->escape($prospectus_courses_id)} 
							GROUP BY 
								c.course_code 
							ORDER BY
								a.id " ;				
			}
			//print($q1);
			//die();

			$query = $this->db->query($q1);
			
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}


		function ListPrereq_CutOff($prospectus_courses_id) {
			
			$result = null;
			
			$q1 = "SELECT d.id, 
				   			a.prerequisites_id, 
							CONCAT(a.grade,' Cutoff Grade') AS pre_req, 
							d.prospectus_courses_id
						FROM 
							prereq_cutoff AS a, 
							prerequisites AS d 
						WHERE 
							d.id=a.prerequisites_id 
							AND d.prospectus_courses_id={$this->db->escape($prospectus_courses_id)} " ;
										
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}

		function ListPrereq_YrLevel($prospectus_courses_id) {
			
			$result = null;
			
			$q1 = "SELECT d.id, 
							CASE a.yr_level 
								WHEN 1 THEN '1st Year Standing'
								WHEN 2 THEN '2nd Year Standing'
								WHEN 3 THEN '3rd Year Standing'
								WHEN 4 THEN '4th Year Standing'
								WHEN 5 THEN '5th Year Standing'
							END	AS pre_req
						FROM 
							prereq_yrlevel AS a, 
							prerequisites AS d
						WHERE 
							d.id=a.prerequisites_id 
							AND d.prospectus_courses_id={$this->db->escape($prospectus_courses_id)} " ;
										
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}


		/*function ListPrereq_MinUnits($prerequisites_id) {
			
			$result = null;
			
			$q1 = "SELECT a.id, a.prerequisites_id, a.units
						FROM prereq_min_units AS a 
						WHERE a.prerequisites_id='$prerequisites_id' " ;
										
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}*/
		
		function ListAllPrereq($prospectus_courses_id,$prereq_type='M') {
			$result = null;
			
			if ($prereq_type == 'M') {
				$q1 = "(SELECT d.id AS prereq_id, 
								a.prerequisites_id, 
								c.course_code AS pre_req, 
								CONCAT('[',c.course_code,'] ',c.descriptive_title) AS my_prereq, 
								d.prereq_type, 
								d.prospectus_courses_id
							FROM 
								prereq_courses AS a, 
								prospectus_courses AS b,
								courses AS c, 
								prerequisites AS d
							WHERE 
								a.prospectus_courses_id=b.id 
								AND b.courses_id=c.id 
								AND d.id=a.prerequisites_id
								AND d.prospectus_courses_id={$this->db->escape($prospectus_courses_id)}
								AND a.prereq_type = 'M')
					   UNION
					   (SELECT d.id AS prereq_id, 
								a.prerequisites_id, 
								CASE a.yr_level 
									WHEN 1 THEN '1st Year Standing'
									WHEN 2 THEN '2nd Year Standing'
									WHEN 3 THEN '3rd Year Standing'
									WHEN 4 THEN '4th Year Standing'
									WHEN 5 THEN '5th Year Standing'
								END	AS pre_req, 
								CASE a.yr_level 
									WHEN 1 THEN '1st Year Standing'
									WHEN 2 THEN '2nd Year Standing'
									WHEN 3 THEN '3rd Year Standing'
									WHEN 4 THEN '4th Year Standing'
									WHEN 5 THEN '5th Year Standing'
								END	AS my_prereq, 
								d.prereq_type, 
								d.prospectus_courses_id
							FROM 
								prereq_yrlevel AS a, 
								prerequisites AS d
							WHERE 
								d.id=a.prerequisites_id 
								AND d.prospectus_courses_id={$this->db->escape($prospectus_courses_id)}) " ;
			} else {
					$q1 = "(SELECT d.id AS prereq_id, 
								a.prerequisites_id, 
								c.course_code AS pre_req, 
								CONCAT('[',c.course_code,'] ',c.descriptive_title) AS my_prereq, 
								d.prereq_type, 
								d.prospectus_courses_id
							FROM 
								prereq_courses AS a, 
								prospectus_courses AS b,
								courses AS c, 
								prerequisites AS d
							WHERE 
								a.prospectus_courses_id=b.id 
								AND b.courses_id=c.id 
								AND d.id=a.prerequisites_id
								AND d.prospectus_courses_id={$this->db->escape($prospectus_courses_id)})
					   UNION
					   (SELECT d.id AS prereq_id, 
								a.prerequisites_id, 
								CASE a.yr_level 
									WHEN 1 THEN '1st Year Standing'
									WHEN 2 THEN '2nd Year Standing'
									WHEN 3 THEN '3rd Year Standing'
									WHEN 4 THEN '4th Year Standing'
									WHEN 5 THEN '5th Year Standing'
								END	AS pre_req, 
								CASE a.yr_level 
									WHEN 1 THEN '1st Year Standing'
									WHEN 2 THEN '2nd Year Standing'
									WHEN 3 THEN '3rd Year Standing'
									WHEN 4 THEN '4th Year Standing'
									WHEN 5 THEN '5th Year Standing'
								END	AS my_prereq, 
								d.prereq_type, 
								d.prospectus_courses_id
							FROM 
								prereq_yrlevel AS a, 
								prerequisites AS d
							WHERE 
								d.id=a.prerequisites_id 
								AND d.prospectus_courses_id={$this->db->escape($prospectus_courses_id)}) " ;
			}


			//print($q1);
			//die();
									
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			return $result;
					
		}
		
		
		
		function ListCoursesToBeTaken($prospectus_id) {
			$result = null;
			
			$q1 = "
						(
							SELECT 
								d.id,
								d.courses_id AS courses_id, 
								d.cutoff_grade, 
								d.num_retakes, 
								a.course_code,
								'N' AS course_elective
							FROM 
								prospectus AS b, 
								prospectus_terms AS c, 
								prospectus_courses AS d, 
								courses AS a  
							WHERE 
								b.id=c.prospectus_id 
								AND c.id=d.prospectus_terms_id 
								AND d.courses_id=a.id 
								AND b.id={$this->db->escape($prospectus_id)}
								AND  d.elective = 'N' 
						) 
					UNION
						(
							SELECT
								x.prospectus_courses_id AS id,
								x.topic_courses_id AS courses_id, 
								d.cutoff_grade, 
								d.num_retakes, 
								e.course_code,
								'Y' AS course_elective
							FROM 
								prospectus_terms AS c, 
								prospectus_courses AS d, 
								courses AS a,
								courses AS e,
								elective_courses AS x
							WHERE 
								c.id=d.prospectus_terms_id
								AND d.courses_id=a.id 
								AND d.id=x.prospectus_courses_id
								AND e.id=x.topic_courses_id
								AND c.prospectus_id={$this->db->escape($prospectus_id)} 
						) 
				";
			//print_r($q1);die();		
			$query = $this->db->query($q1);
			if ($query){
				if ($query->num_rows() > 0)
					return $query->result();
				else 
					return NULL;  
			}else{
				return FALSE; 
			} 
		}

		
		//added: 1/22/2013
		function ListCoursesToBeTakenByYrLevel($idno, $prospectus_id, $yr) {
			$result = null;
			
			$q1 = "SELECT 
						a.id AS courses_id,
						d.courses_id, 
						d.cutoff_grade, 
						d.num_retakes, 
						a.course_code
					FROM 
						prospectus AS b, 
						prospectus_terms AS c, 
						prospectus_courses AS d, 
						courses AS a
						
					WHERE 
						b.id=c.prospectus_id 
						AND c.id=d.prospectus_terms_id 
						AND d.courses_id=a.id 
						AND b.id={$this->db->escape($prospectus_id)} 
						AND c.year_level >= {$this->db->escape($yr)}
						AND d.elective = 'N' 
						AND d.courses_id NOT IN 
						(SELECT y.courses_id
							FROM
								enrollments AS x,
								course_offerings AS y,
								student_histories AS z
							WHERE 
								x.course_offerings_id=y.id
								AND z.id = x.student_history_id
								AND z.students_idno = {$this->db->escape($idno)} )
					ORDER BY
						a.course_code";
									
			//print($q1);
			//die();							
			$query = $this->db->query($q1);
			
			$result = FALSE;
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			return $result;
		}

		
		
		
		/**
		 * Student Prospectus
		 * 
		 */
		public function student_prospectus ($prospectus_id){
			$sql = "
				SELECT
				DISTINCT
					pt.year_level, pt.term,
					pc.id, pc.courses_id,
					c.descriptive_title, c.course_code, c.credit_units,
					p.status
				FROM
					prospectus_courses pc
				LEFT JOIN
					courses c
				ON
					pc.courses_id=c.id
				LEFT JOIN
					prospectus_terms pt
				ON
					pc.prospectus_terms_id=pt.id
				LEFT JOIN
					prospectus p
				ON 
					p.id=pt.prospectus_id
				WHERE
					p.id='{$prospectus_id}'
				ORDER BY
					year_level ASC, term, course_code 
				";
			$query = $this->db->query($sql);
			if ($query && count($query->num_rows() > 0)){
				$prospectus = array();
				
				foreach ($query->result() as $row) {
					$prospectus[$row->year_level][$row->term][] = 
							array( 
								"descriptive_title" => $row->descriptive_title,
								"courses_id" => $row->courses_id,
            					"course_code" => $row->course_code,
            					"credit_units" => $row->credit_units,
								"prospectus_courses_id" => $row->id,
							);
				
				}
				//print_r($prospectus);
			//	die();
				//log_message("INFO",$prospectus); //toyet 4.5.2018
				return $prospectus;
			} else {
				//log_message("INFO","FALSE:"); //toyet 4.5.2018
				//log_message("INFO",$prospectus_id); //toyet 4.5.2018
				//log_message("INFO",$sql); //toyet 4.5.2018
				return FALSE;
			}
			
		}

		//@EDITED: 3/31/15 by genes
		//ADDED prospectus_terms so that only courses credited during that prospectus_id is extracted
		//change back if()
		function ListCreditedCourses($students_idno, $prospectus_id) {
			$result = null;
			
			$q1 = "SELECT 
						b.courses_id
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b, 
						credited_courses AS c 
					WHERE 
						a.id=b.prospectus_terms_id
						AND b.id=c.prospectus_courses_id 
						AND c.students_idno = {$this->db->escape($students_idno)}
						AND a.prospectus_id = {$this->db->escape($prospectus_id)} ";

			$query = $this->db->query($q1);
			
			//print($q1); die();
			//if($query->num_rows() > 0){
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			/*if ($query){
				if($query->num_rows() > 0)
					$result = $query->result();
				else 
					$result = true;
			} */
			
			//return false;
			return $result;
		}
		
/*		//Added: April 9, 2013 by Amie
		function get_credited_courses($prospectus) {
			$result = null;
			
			$q1 = "SELECT c.course_code, c.descriptive_title, c.
					FROM courses AS c, credited_course
					WHERE b.id=c.prospectus_courses_id AND c.students_idno={$this->db->escape($students_idno)} ";

			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}

*/
		function ListCourseWithPrereq($courses_prospectus_id, $courses_elective_id, $prospectus_id) {
			$result = null;
			
			/*$q1 = "(SELECT 
						b.courses_id, 
						c.id,
						b.id AS prospectus_courses_id,
						y.course_code AS mother_course_code,
						d.course_code,
						'Y' AS from_prospectus,
						c.prereq_type AS prereq_type
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b, 
						prerequisites AS c,
						courses AS d,
						courses AS y						 
					WHERE 
						a.id=b.prospectus_terms_id
						AND b.id=c.prospectus_courses_id
						AND d.id=b.courses_id
						AND y.id=b.courses_id
						AND a.prospectus_id = {$this->db->escape($prospectus_id)}
						AND b.courses_id IN $courses_prospectus_id
					GROUP BY 
						b.courses_id, c.prereq_type)
					UNION
					(SELECT 
						c.id AS courses_id, 
						e.id,
						b.id AS prospectus_courses_id,
						y.course_code AS mother_course_code,
						d.course_code,
						'N' AS from_prospectus,
						e.prereq_type AS prereq_type
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b, 
						courses AS d,
						elective_courses AS c,
						elective_topic_prereq AS e,
						prospectus_courses AS x,
						courses AS y						 
					WHERE 
						a.id=b.prospectus_terms_id
						AND b.id=c.prospectus_courses_id
						AND d.id=c.topic_courses_id
						AND c.id=e.elective_courses_id
						AND x.id=c.prospectus_courses_id
						AND y.id=x.courses_id
						AND a.prospectus_id = {$this->db->escape($prospectus_id)}
						AND c.topic_courses_id IN $courses_elective_id
					GROUP BY 
						c.topic_courses_id, e.prereq_type) ";
			*/
			//print($q1);
			//die();
			
			//EDITED: 2/12/2013
			
			$q1 = "(SELECT 
						b.courses_id, 
						c.id,
						b.id AS prospectus_courses_id,
						y.course_code AS mother_course_code,
						d.course_code,
						'Y' AS from_prospectus,
						e.prospectus_courses_id AS child_pros_id,
						z.course_code AS child_course_code,
						z.id AS child_courses_id,
						NULL AS child_yrlevel,
						NULL AS topic_courses_id,
						c.prereq_type AS prereq_type
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b, 
						prerequisites AS c,
						courses AS d,
						courses AS y,
						courses AS z,
						prospectus_courses AS f, 						
						prereq_courses AS e						 
					WHERE 
						a.id=b.prospectus_terms_id
						AND b.id=c.prospectus_courses_id
						AND d.id=b.courses_id
						AND y.id=b.courses_id
						AND c.id=e.prerequisites_id
						AND e.prospectus_courses_id=f.id
						AND f.courses_id=z.id
						AND a.prospectus_id = {$this->db->escape($prospectus_id)}
						AND b.courses_id IN $courses_prospectus_id)
					UNION
					
					(SELECT 
						b.courses_id, 
						c.id,
						b.id AS prospectus_courses_id,
						y.course_code AS mother_course_code,
						d.course_code,
						'Y' AS from_prospectus,
						NULL AS child_pros_id,
						NULL AS child_course_code,
						NULL AS child_courses_id,
						e.yr_level AS child_yrlevel,
						NULL AS topic_courses_id,
						c.prereq_type AS prereq_type
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b, 
						prerequisites AS c,
						courses AS d,
						courses AS y,
						prereq_yrlevel AS e						 
					WHERE 
						a.id=b.prospectus_terms_id
						AND b.id=c.prospectus_courses_id
						AND d.id=b.courses_id
						AND y.id=b.courses_id
						AND c.id=e.prerequisites_id
						AND a.prospectus_id = {$this->db->escape($prospectus_id)}
						AND b.courses_id IN $courses_prospectus_id)
					UNION
					
					(SELECT 
						c.id AS courses_id, 
						e.id,
						b.id AS prospectus_courses_id,
						y.course_code AS mother_course_code,
						d.course_code,
						'N' AS from_prospectus,
						f.prospectus_courses_id AS child_pros_id,
						z.course_code AS child_course_code,
						z.id AS child_courses_id,
						NULL AS child_yrlevel,
						c.topic_courses_id AS topic_courses_id,
						e.prereq_type AS prereq_type
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b, 
						courses AS d,
						elective_courses AS c,
						elective_topic_prereq AS e,
						prospectus_courses AS x,
						courses AS y,
						elective_topic_prereq_courses AS f,
						prospectus_courses AS g, 
						courses AS z												 
					WHERE 
						a.id=b.prospectus_terms_id
						AND b.id=c.prospectus_courses_id
						AND d.id=c.topic_courses_id
						AND c.id=e.elective_courses_id
						AND x.id=c.prospectus_courses_id
						AND y.id=x.courses_id
						AND e.id=f.elective_topic_prereq_id
						AND f.prospectus_courses_id=g.id
						AND g.courses_id=z.id
						AND a.prospectus_id = {$this->db->escape($prospectus_id)}
						AND c.topic_courses_id IN $courses_elective_id)
						
					UNION
					(SELECT 
						c.id AS courses_id, 
						e.id,
						b.id AS prospectus_courses_id,
						y.course_code AS mother_course_code,
						d.course_code,
						'N' AS from_prospectus,
						NULL AS child_pros_id,
						NULL AS child_course_code,
						NULL AS child_courses_id,
						f.yr_level AS child_yrlevel,
						c.topic_courses_id AS topic_courses_id,
						e.prereq_type AS prereq_type
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b, 
						courses AS d,
						elective_courses AS c,
						elective_topic_prereq AS e,
						prospectus_courses AS x,
						courses AS y,
						elective_topic_prereq_yrlevel AS f
					WHERE 
						a.id=b.prospectus_terms_id
						AND b.id=c.prospectus_courses_id
						AND d.id=c.topic_courses_id
						AND c.id=e.elective_courses_id
						AND x.id=c.prospectus_courses_id
						AND y.id=x.courses_id
						AND e.id=f.elective_topic_prereq_id
						AND a.prospectus_id = {$this->db->escape($prospectus_id)}
						AND c.topic_courses_id IN $courses_elective_id)	";
			
			//print($q1);die();
			
			$query = $this->db->query($q1);
			
			if ($query){
				if($query->num_rows() > 0)
					$result = $query->result();
				else
					$result = true;
			}

			return $result;
				
		}

		//Edited: 01/21/2013
		function ListCourseNoPrereq($courses_prospectus_id, $courses_elective_id, $prospectus_id) {
			$result = null;
			
			$q1 = "(SELECT 
						b.courses_id,
						'Y' AS from_prospectus
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b
					WHERE 
						b.prospectus_terms_id = a.id
						AND a.prospectus_id = {$this->db->escape($prospectus_id)}
						AND b.id NOT IN (SELECT c.prospectus_courses_id FROM prerequisites AS c)
						AND b.courses_id IN $courses_prospectus_id)
					UNION
					(SELECT 
						a.topic_courses_id AS courses_id,
						'N' AS from_prospectus
					FROM 
						elective_courses AS a
					WHERE 
						a.id NOT IN (SELECT b.elective_courses_id FROM elective_topic_prereq AS b)
						AND a.topic_courses_id IN $courses_elective_id)";


			$query = $this->db->query($q1);
			
			if ($query){
				if($query->num_rows() > 0)
					$result = $query->result();
				else
					$result = true;
			}
			//return null;
			return $result;
				
		}
		
		/**
		 * Method to get the prospectuses of a student given his/her student_id
		 * 
		 * will return all the prospectuses if $return_all = TRUE else will return
		 * only the latest prospectus used by that student.
		 * 
		 * @param int $student_id
		 * @param boolean $return_all
		 * @return variant
		 */
		function student_prospectus_ids ($student_id, $return_all=FALSE){
			$sql = "
				SELECT
				-- DISTINCT
					sh.id as student_histories_id,
					p.id, ap.abbreviation, ap.description
				FROM
					student_histories sh
				LEFT JOIN
					prospectus p
				ON
					sh.prospectus_id=p.id
				LEFT JOIN
					academic_programs ap
				ON
					p.academic_programs_id=ap.id
				WHERE
					sh.students_idno='{$student_id}'
				ORDER BY
					sh.id
				DESC
				";
			
			//print($sql);
			//die();
			$query = $this->db->query($sql);
			
			if ($query && $query->num_rows() > 0){
				if($return_all)
					return $query->result(); else {
					$prospectus = $query->row();
					return $prospectus->id;
				}
			} else {
				return FALSE;
			}
		}
		
		/**
		 * method to return the prospectus given an academic program
		 * 
		 * @param int $academic_program_id
		 */
		public function prospectus_from_program ($academic_program_id){
			//todo: this query should check the status also...			
			$sql = "
					SELECT
						id
					FROM
						prospectus
					WHERE
						academic_programs_id = '{$academic_program_id}' 
					ORDER BY
						id
					DESC
					LIMIT 1
				";
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0)
				return $query->row()->id; else
				return FALSE;
		}


		/*****************************************************************************************************/
		
		/***********************************************************************
		 * NOTE: Added 12/30/12
		************************************************************************/
		function ListProspectus($colleges_id) {
				
			$q1 = "SELECT 
						a.id,a.effective_year, IF(a.status='A','Active','Inactive') as prospectus_status,
						b.abbreviation,b.description, IF(b.status='O','Offered','Frozen') as program_status
					FROM 
						prospectus AS a,
						academic_programs AS b
					WHERE 
						a.academic_programs_id=b.id
						AND b.colleges_id= {$this->db->escape($colleges_id)}
					ORDER BY 
						b.abbreviation, a.effective_year DESC ";
		
			$query = $this->db->query($q1);
				
			//print($q1);
			//die();
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
		
		
		function checkIfTaken($courses_id,$students_idno) {
			$result = null;
				
			// NOTE: 12/8/12
			// additional id no. of student needed
			// no need for academic_terms_id so that all courses already taken will be returned
			// @EDITED: 3/31/15 by genes
			// NOTE: ORDER BY is by a.finals_grade so that subjects taken twice will retrieve subject with passing grade
			$q1 = "SELECT a.id, a.finals_grade
						FROM 
							enrollments AS a,
							course_offerings AS b,
							student_histories AS c,
							col_students AS d
						WHERE 
							a.course_offerings_id=b.id
							AND a.student_history_id=c.id
							AND c.students_idno=d.students_idno
							AND b.courses_id= {$this->db->escape($courses_id)}
							AND d.students_idno={$this->db->escape($students_idno)}
						ORDER BY 
							-- c.id DESC
							a.finals_grade 
						LIMIT 1";
		
			//print($q1);die();
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
		}
		
		
		function checkIfCredited($courses_id,$students_idno) {
			$result = null;
			
			$q1 = "SELECT a.id
						FROM credited_courses AS a, prospectus_courses AS b
						WHERE a.prospectus_courses_id=b.id AND 
							b.courses_id= {$this->db->escape($courses_id)}
							AND a.students_idno={$this->db->escape($students_idno)} ";
									
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		}


		/*NOTE: Added on 12/30/12*/
		function AddProspectus($prospectus_data) {
				
			$query = "
						INSERT INTO
							prospectus (id, academic_programs_id, effective_year, status)
						VALUES (
								'',
								'".$prospectus_data['academic_programs_id']."',
										'".$prospectus_data['effective_year']."',
										'".$prospectus_data['status']."' )";
		
			$result = $this->db->query($query);

			return $this->db->insert_id();
		}
		
		
		//Changed ORDER BY year_level 01/01/13
		//Added CASE for year_level
		function ListProspectusTerms($prospectus_id=null) {
				
			$result = null;
				
			$q1 = "SELECT a.id, a.year_level, a.term AS pros_term, a.max_credit_units, a.max_bracket_units,
							CASE a.term
								WHEN 1 THEN '1st Semester'
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
								END AS term,
							CASE a.year_level
								WHEN 1 THEN '1st Year'
								WHEN 2 THEN '2nd Year'
								WHEN 3 THEN '3rd Year'
								WHEN 4 THEN '4th Year'
								WHEN 5 THEN '5th Year'
								END as y_level
						FROM 
							prospectus_terms AS a
						WHERE 
							a.prospectus_id={$this->db->escape($prospectus_id)}
						ORDER BY a.year_level, a.term " ;
		
			$query = $this->db->query($q1);
				
			//print($q1);
			//die();

			if($query->num_rows() > 0){
				$result = $query->result();
			}

			//log_message("INFO",print_r($q1,true));  //Toyet 5.3.2018
				
			return $result;
		}
		
		
		function ListProspectusTermCourses($prospectus_terms_id, $status=null) {
						
			$result = null;
				
			$q1 = "SELECT d.id, e.course_code, e.descriptive_title,
						IF(d.is_bracketed = 'Y',CONCAT('(',e.credit_units,')'),e.credit_units) AS credit_units,
						d.cutoff_grade,
						d.num_retakes,
						d.elective,
						d.is_bracketed,
						d.is_major
					FROM 
						prospectus_courses AS d, 
						courses AS e
					WHERE 
						d.courses_id=e.id AND
						d.prospectus_terms_id={$this->db->escape($prospectus_terms_id)} 
					ORDER BY
						d.is_bracketed DESC, e.course_code " ;
		
			$query = $this->db->query($q1);
				
			//print($q1);
			//die();
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
		
		
		function ListCoursesNotTaken ($prospectus_id, $students_idno) {
			$result = null;
				
			$q1 = "SELECT a.course_code, a.descriptive_title, a.credit_units, d.id
						FROM 
							courses AS a, 
							prospectus AS b, 
							prospectus_terms AS c, 
							prospectus_courses AS d
						WHERE 
							b.id=c.prospectus_id 
							AND c.id=d.prospectus_terms_id 
							AND d.courses_id=a.id
							AND b.id={$this->db->escape($prospectus_id)}
							AND d.id NOT IN
								(SELECT x.prospectus_courses_id
									FROM prerequisites AS x, prereq_courses AS y
									WHERE x.id=y.prerequisites_id AND y.prospectus_courses_id NOT IN () )  ";
		
		
			//print($q1);
			//die();
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
				
		}
		
		
		function getProspectus($id=null) {
			if($id == null) { return null; }
						
			$result = null;
						
			$q1 = "SELECT
						b.description,
						a.effective_year,
						a.academic_programs_id
					FROM
						prospectus AS a,
						academic_programs AS b
					WHERE
						a.academic_programs_id=b.id
						AND a.id = {$this->db->escape($id)} ";

			$query = $this->db->query($q1);
		
			//print($q1);
			//die();
		
			if($query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
		}
		
		
		function getProspectusTerm($id=null) {
			//if($id == null) { return null; }
				
			$result = null;
				
			$q1 = "SELECT a.term AS term1, a.year_level AS y_level1,
						CASE a.term
						WHEN 1 THEN '1st Semester'
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
						END AS term,
						CASE a.year_level
						WHEN 1 THEN '1st Year'
						WHEN 2 THEN '2nd Year'
						WHEN 3 THEN '3rd Year'
						WHEN 4 THEN '4th Year'
						WHEN 5 THEN '5th Year'
						END as y_level
					FROM
						prospectus_terms AS a
					WHERE
						a.id = {$this->db->escape($id)} ";

			
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
					
			return $result;
		}
		
		//NOTE: Added 01/03/2013
		function getProspectusCourse($id=null) {
			if($id == null) { return null; }
				
			$result = null;
				
			$q1 = "SELECT
						a.id, b.course_code, 
						b.descriptive_title, b.credit_units,
						a.prospectus_terms_id,
						a.is_bracketed,
						a.is_major,
						a.elective,
						a.cutoff_grade,
						a.num_retakes
					FROM
						prospectus_courses AS a,
						courses AS b
					WHERE
						a.courses_id=b.id
						AND a.id = {$this->db->escape($id)} ";

			$query = $this->db->query($q1);
		
			//print($q1);
			//die();
		
			if($query->num_rows() > 0){
				$result = $query->row();
			}
					
			return $result;
		}



		function AddProspectusTerm($term_data)	{
				
			$query = "INSERT INTO 
					prospectus_terms (prospectus_id, year_level, term, inserted_by)
					VALUES ('".$term_data['prospectus_id']."',
					'".$term_data['year_level']."',
					'".$term_data['term']."',
					'".$term_data['inserted_by']."')";
					
			$this->db->query($query);
			$id = @mysql_insert_id();
					
			return $id;
					
		}
		

		function AddProspectusCourse($course_data) {
				
			$query = "INSERT INTO 
						prospectus_courses 
								(prospectus_terms_id, 
								courses_id, 
								elective, 
								is_major, 
								cutoff_grade, 
								num_retakes, 
								is_bracketed)
						VALUES (
								{$this->db->escape($course_data['prospectus_terms_id'])},
								{$this->db->escape($course_data['courses_id'])},
								{$this->db->escape($course_data['elective'])},
								{$this->db->escape($course_data['is_major'])},
								{$this->db->escape($course_data['cutoff_grade'])},
								{$this->db->escape($course_data['num_retakes'])},
								{$this->db->escape($course_data['is_bracketed'])} )";
		
			//die($query);
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
						
		}
		

		function UpdateProspectusTermUnits($prospectus_terms_id)	{

			$q1 = "SELECT
							if (SUM(a.credit_units),SUM(a.credit_units),0) AS credit_units
						FROM
							courses AS a,
							prospectus_courses AS b
						WHERE
							a.id=b.courses_id
							AND b.is_bracketed = 'N' 
							AND b.prospectus_terms_id = {$this->db->escape($prospectus_terms_id)} ";
			
			$query1 = $this->db->query($q1);

			$q2 = "SELECT
							if (SUM(a.credit_units),SUM(a.credit_units),0) AS bracketed_units
						FROM
							courses AS a,
							prospectus_courses AS b
						WHERE
							a.id=b.courses_id
							AND b.is_bracketed = 'Y'
							AND b.prospectus_terms_id = {$this->db->escape($prospectus_terms_id)} ";
				
			$query2 = $this->db->query($q2);

			$q3 = "UPDATE
							prospectus_terms
						SET
							max_credit_units = {$query1->row()->credit_units},
							max_bracket_units = {$query2->row()->bracketed_units}
						WHERE
							id = {$this->db->escape($prospectus_terms_id)} ";
			
			//print($q3); die();
			if ($this->db->query($q3)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		//Added: 01/03/2013		
		function ListPrereqAllowed($year_level, $term, $prospectus_id) {
			$result = null;
				
			$q1 = "SELECT a.id, 
						a.courses_id, 
						b.course_code, 
						b.descriptive_title
						
					FROM
						prospectus_courses AS a,
						courses AS b,
						prospectus_terms AS c
					WHERE
						a.courses_id=b.id
						AND c.id=a.prospectus_terms_id
						AND c.prospectus_id='$prospectus_id'
						AND ((c.year_level={$this->db->escape($year_level)} AND c.term < {$this->db->escape($term)}) OR 
							c.year_level < {$this->db->escape($year_level)})
					ORDER BY b.course_code";

			//print($q1);
			//die();
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
				
		}

		function DeleteProspectusCourse($prospectus_courses_id) {
				
			$query = "DELETE 
						FROM 
							prospectus_courses 
						WHERE
							id = {$this->db->escape($prospectus_courses_id)} ";
			
			//print($query);
			//die();
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
						
		}
		
		/**
		 * Method to return all the cascaded prerequisites of a certain prospectus
		 *  
		 * @param unknown_type $prospectus_id
		 */
		function all_courses_with_cascaded_prerequisites ($prospectus_id) {
			$sql = "
					SELECT
					DISTINCT
						pt.year_level, pt.term,
						pc.id,
						pr_c.prospectus_courses_id as course_prerequisite_id, 
						c.descriptive_title, c.course_code, c.credit_units,
						c2.descriptive_title as prerequisite_descriptive_title, c2.course_code as prequisite_course_code, c2.credit_units as prerequisite_credit_units
					FROM
						prospectus_courses pc
					LEFT JOIN
						courses c
					ON
						pc.courses_id=c.id
					LEFT JOIN
						prospectus_terms pt
					ON
						pc.prospectus_terms_id=pt.id
					LEFT JOIN
						prospectus p
					ON 
						p.id=pt.prospectus_id
					/* we need to extract also NULLS from the prerequisites table */ 
					LEFT JOIN
						prerequisites pr
					ON
						pr.prospectus_courses_id = pc.id
					LEFT JOIN
						prereq_courses pr_c
					ON
						pr_c.prerequisites_id=pr.id
					LEFT JOIN
						prospectus_courses pc2
					ON
						pc2.id=pr_c.prospectus_courses_id
					LEFT JOIN
						courses c2
					ON
						c2.id=pc2.courses_id
					WHERE
						p.id={$this->db->escape($prospectus_id)} /* 201 */
					ORDER BY
						pt.year_level ASC, pt.term ASC
					";
			//The query above is sufficient to provide multiple prerequisites to a course
			// and therefore is not the culprit. NOTE: the query above is still an overkill since
			// we don't need to extract the other properties of the prerequisites (i.e. descriptive title, course code, etc.)
			// since we can extract it from the course...			
			$query = $this->db->query ($sql);
			if ($query && $query->num_rows() > 0) {
				//we loop through the result...
				$results = $query->result();
				$courses = array();
				foreach ($results as $row ){
					//Edit January 18, 2012
					//There are also courses having more than one immediate prerequisites
					//We're going to test whether this course already exists
					if (array_key_exists($row->id, $courses)) {
						//this course is already found... so lets add the prerequisite to the original row
						$courses[$row->id]['prerequisites'] = array_merge($courses[$row->id]['prerequisites'], array($row->course_prerequisite_id => array(
								'id'					=> $row->course_prerequisite_id,
								'course_description' 	=> $row->prerequisite_descriptive_title,
								'course_code' 			=> $row->prequisite_course_code,
								'credit_units' 			=> $row->prerequisite_credit_units,
						)
						)
						);
						
					} else {
						//This course is not yet added
						$courses[$row->id] = array (
								'id'					=> $row->id,
								'course_description' 	=> $row->descriptive_title,
								'course_code' 			=> $row->course_code,
								'credit_units'			=> $row->credit_units,
								'prerequisites'			=> array(),
						);
						if ( ! is_null($row->course_prerequisite_id)) {
							//this certain course has a prerequisite...
							if (isset($courses[$row->course_prerequisite_id])) {
								//the prerequisite is already set...
								$courses[$row->id]['prerequisites'] = array_merge($courses[$row->course_prerequisite_id]['prerequisites'], array($row->course_prerequisite_id => array(
										'id'					=> $row->course_prerequisite_id,
										'course_description' 	=> $row->prerequisite_descriptive_title,
										'course_code' 			=> $row->prequisite_course_code,
										'credit_units' 			=> $row->prerequisite_credit_units,
								)
								)
								);
							}
						} 
					}
				}
				return $courses;
			} else {
				return FALSE;
			}
		}
		
		
		//Added: 01/26/13 By:genes
		function ListElectives($prospectus_id) {
			$result = null;
				
			$q1 = "SELECT 
						d.id, 
						d.courses_id,
						e.course_code, 
						e.descriptive_title,
						e.credit_units,
						CASE a.term
							WHEN 1 THEN '1st Semester'
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
						END AS term,
						CASE a.year_level
							WHEN 1 THEN '1st Year'
							WHEN 2 THEN '2nd Year'
							WHEN 3 THEN '3rd Year'
							WHEN 4 THEN '4th Year'
							WHEN 5 THEN '5th Year'
						END as y_level
						
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS d, 
						courses AS e
					WHERE 
						a.id = d.prospectus_terms_id
						AND d.courses_id=e.id 
						AND	a.prospectus_id={$this->db->escape($prospectus_id)}
						AND d.elective = 'Y' 
					ORDER BY
						a.year_level, a.term" ;
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
		
		
		//Added: 2/7/2013
		
		function getPrereq_ByYrLevel($prerequisites_id = null) {
			
				
			$result = null;
				
			$q1 = "SELECT
						a.yr_level
					FROM
						prereq_yrlevel as a
					WHERE
						a.id = {$this->db->escape($prerequisites_id)} ";

			$query = $this->db->query($q1);
		
			//print($q1);
			//die();
		
			if($query->num_rows() > 0){
				$result = $query->row();
			}
					
			return $result;
		}
		
		function getPrereq_ByYrLevel_Elective($elective_topic_prereq_id=null) {
			
				
			$result = null;
				
		$q1 = "SELECT
						a.yr_level
					FROM
						elective_topic_prereq_yrlevel as a
					WHERE
						a.elective_topic_prereq_id = {$this->db->escape($elective_topic_prereq_id)} ";
		
		$query = $this->db->query($q1);
		
			//print($q1);
			//die();
		
			if($query->num_rows() > 0){
				$result = $query->row();
			}
					
			return $result;
		}

		function ListPrereq($prospectus_courses_id) {
			
			$result = null;
			
			$q1 = "(SELECT 
						a.id, 
						e.id AS courses_id,
						b.prospectus_courses_id as child_pros_course_id,
						e.course_code,
						NULL AS yr_level,
						a.prereq_type, 'Y' as from_prospectus
					FROM 
						prospectus_courses AS d,
						courses AS e,
						prerequisites AS a,
						prereq_courses AS b 
					WHERE 
						d.id=b.prospectus_courses_id 
						AND d.courses_id=e.id
						AND a.id=b.prerequisites_id
					    AND a.prospectus_courses_id={$this->db->escape($prospectus_courses_id)})
					UNION 
					(SELECT 
						a.id, 
						NULL AS courses_id,
						NULL as child_pros_course_id,
						NULL AS course_code,
						c.yr_level,
						a.prereq_type, 'Y' as from_prospectus
					FROM 
						prerequisites AS a,
						prereq_yrlevel AS c 
					WHERE 
						a.id=c.prerequisites_id
					    AND a.prospectus_courses_id={$this->db->escape($prospectus_courses_id)})";
				//	GROUP BY 
					//	course_id" ;
										
			$query = $this->db->query($q1);
			
			//print($q1);
			//die();		
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}
		
		//Added: 02/23/2013		
		//@EDITED: 3/31/15
		//NOTE:  SELECT MAX(ee.academic_terms_id) is changed to SELECT ee.academic_terms_id
		function PassPrerequisites($prospectus_courses_id, $students_idno, $yr_level) {
		//print($yr_level);
		//die();

			$q1 = "SELECT 
						nn.finals_grade,
						d.id,
						d.course_code,
						a.prereq_type,
						c.id AS prospectus_courses_id,
						NULL AS yr_level,
						nn.enrollments_id
					FROM
						prerequisites AS a,
						prereq_courses AS b,
						prospectus_courses AS c,
						courses AS d LEFT JOIN 
							(SELECT 
									e.courses_id, 
									f.finals_grade,
									f.id AS enrollments_id 
								FROM
									course_offerings AS e,
									enrollments AS f,
									student_histories AS g
								WHERE 
									e.id=f.course_offerings_id
									AND g.id=f.student_history_id
									AND g.students_idno = {$this->db->escape($students_idno)}
									AND e.academic_terms_id = (SELECT 
														ee.academic_terms_id
													FROM
														course_offerings AS ee,
														enrollments AS ff,
														student_histories AS gg
													WHERE
														ee.id=ff.course_offerings_id
														AND ee.courses_id = e.courses_id
														AND gg.id=ff.student_history_id
														AND gg.students_idno = {$this->db->escape($students_idno)}
													ORDER BY
														ff.finals_grade
														LIMIT 1
													)
								GROUP BY 
									e.courses_id
								ORDER BY
									f.student_history_id)
						AS nn ON d.id=nn.courses_id
					WHERE
						a.id=b.prerequisites_id
						AND b.prospectus_courses_id=c.id
						AND c.courses_id=d.id
						AND a.prospectus_courses_id = {$this->db->escape($prospectus_courses_id)} 
						AND (nn.finals_grade IN ('5.0','NG','INC','DR','NC','NA','INE','WD') OR nn.finals_grade IS NULL)
					 GROUP BY
					 d.id ";
			
			//print($q1); die();					

			$q2 = "SELECT 
						b.yr_level 
					FROM
						prerequisites AS a,
						prereq_yrlevel AS b
					WHERE
						a.id=b.prerequisites_id
						AND a.prospectus_courses_id = {$this->db->escape($prospectus_courses_id)} ";
					
			$query = $this->db->query($q1);
			$query2 = $this->db->query($q2);
		
			if($query->num_rows() == 0){
				if ($query2->num_rows() > 0){
					$result = $query2->row();
					if ($result->yr_level == $yr_level) { 
						return TRUE;
					} else {
						return FALSE;
					}
				} else {
					return TRUE;
				}
			} else {
				//check if prequisites are credited
				$result = $query->result();
				foreach ($result AS $course) {
					$courses_id[] = $course->prospectus_courses_id;
				}
				$c_id = implode(",", $courses_id);
				$q3 = "SELECT
								cc.id
							FROM 
								credited_courses AS cc 
							WHERE
								cc.students_idno = {$this->db->escape($students_idno)}
								AND cc.prospectus_courses_id IN ($c_id)
							GROUP BY
								cc.students_idno, cc.prospectus_courses_id";
				
				$query3 = $this->db->query($q3);
				
				//check if all prerequesites are credited
				if ($query3->num_rows() == $query->num_rows()) {
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}


		function Topic_Elective($prospectus_courses_id) {
		
			$q1 = "SELECT 
						a.topic_courses_id,
						b.course_code
					FROM
						elective_courses AS a,
						courses AS b
					WHERE
						a.topic_courses_id=b.id
						AND a.prospectus_courses_id = {$this->db->escape($prospectus_courses_id)} ";

			$query = $this->db->query($q1);
			
			//print($q1);
			//die();		
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}

		//EDITED: 4/16/14 added $prospectus_id 
		function PassPrereq_Elective_Child($topic_courses_id, $students_idno, $yr_level, $prospectus_id) {
			//print($topic_courses_id."<br>".$students_idno."<br>".$yr_level); die();
			$q1 = "SELECT 
						nn.finals_grade,
						e.id,
						e.course_code,
						b.prereq_type,
						NULL AS yr_level
					FROM
						elective_courses AS a,						
						elective_topic_prereq AS b, 
						elective_topic_prereq_courses AS c, 
						prospectus_terms AS pt,
						prospectus_courses AS d,
						courses AS e LEFT JOIN
							(SELECT 
									f.courses_id,
									g.finals_grade
								FROM
									course_offerings AS f,
									enrollments AS g,
									student_histories AS h
								WHERE
									f.id=g.course_offerings_id
									AND h.id=g.student_history_id
									AND h.students_idno = {$this->db->escape($students_idno)} 
									AND g.student_history_id IN (SELECT 
														MAX(gg.student_history_id)
													FROM
														course_offerings AS ff,
														enrollments AS gg,
														student_histories AS hh
													WHERE
														ff.id=gg.course_offerings_id
														AND hh.id=gg.student_history_id
														AND hh.students_idno = {$this->db->escape($students_idno)}
													GROUP BY
														ff.courses_id, 
														hh.students_idno, 
														hh.prospectus_id)	
								GROUP BY 
									f.courses_id
								ORDER BY
									g.student_history_id )
						AS nn ON e.id=nn.courses_id
					WHERE
						a.id=b.elective_courses_id 
						AND b.id=c.elective_topic_prereq_id
						AND c.prospectus_courses_id=d.id 
						AND d.courses_id=e.id 
						AND pt.id = d.prospectus_terms_id
						AND pt.prospectus_id = {$this->db->escape($prospectus_id)}
						AND a.topic_courses_id = {$this->db->escape($topic_courses_id)}
						AND (nn.finals_grade IN ('5.0','NG','INC','DR','NC','NA','WD','INE') OR nn.finals_grade IS NULL) 
					GROUP BY
						e.id ";

			//print($q1); die();

			$q2 = "SELECT 
						c.yr_level
					FROM
						elective_courses AS a,						
						elective_topic_prereq AS b, 
						elective_topic_prereq_yrlevel AS c
					WHERE
						a.id=b.elective_courses_id 
						AND b.id=c.elective_topic_prereq_id
						AND a.topic_courses_id = {$this->db->escape($topic_courses_id)} ";
//print($q2); die();
			$query = $this->db->query($q1);
			$query2 = $this->db->query($q2);
			
			if($query->num_rows() == 0){
				if ($query2->num_rows() > 0){
					$result = $query2->row();
					if ($result->yr_level >= $yr_level) { 
						return TRUE;
					} else {
						return FALSE;
					}
				} else {
					return TRUE;
				}
			} else {
				return FALSE;
			}

		}
		
		function program_from_prospectus_id ($prospectus_id){
			$sql = "
				SELECT
					ap.id,
					ap.abbreviation,
					ap.description
				FROM
					prospectus p
				LEFT JOIN
					academic_programs ap
				ON
					p.academic_programs_id=ap.id
				WHERE
					p.id={$this->db->escape($prospectus_id)}
				";
			
			$query = $this->db->query($sql);
			if($query && $query->num_rows() > 0){
				return $query->result();
			} else {
				return FALSE;
			}
		}
		
		function prospectuses_from_academic_programs ($academic_program_id){
			$sql = "
				SELECT
					id,
					effective_year,
					status
				FROM
					prospectus
				WHERE
					academic_programs_id={$this->db->escape($academic_program_id)}
				ORDER BY
					effective_year
				DESC";
			$query = $this->db->query($sql);
			if($query && $query->num_rows() > 0){
				return $query->result();
			} else {
				return FALSE;
			}
		}
		
		//EDITED: 4/17/14 to include elective grades
		/*
		 *UPDATED: 8/7/14 by genes
		 *Grade is based on value not academic_terms 
		 */
		/*
		 * UPDATED: 12/12/14 by genes
		 * Grade is checked if it passed the cut-off grade
		 */
		function ShowProspectusGrades($prospectus_terms_id, $students_idno) {
		
			$result = NULL;

			$q1 = "SELECT
							 d.id,
							d.course_code,
							d.descriptive_title,
							d.credit_units,
							c.is_bracketed,
							c.cutoff_grade,
							c.num_retakes,
							c.id AS prospectus_courses_id,
							nn.finals_grade,
							if (nn.finals_grade > c.cutoff_grade,'test','OK') AS test1,
							IF ((nn.finals_grade IN ('5.0','NG','INC','DR','NA','NC','INE','WD') OR nn.finals_grade IS NULL
								OR nn.finals_grade > c.cutoff_grade),
									IF((cc.other_schools_enrollments_id IS NULL AND cc.enrollments_id IS NULL),NULL,
											IF(cc.enrollments_id IS NULL,
													'OTHER*','HNU*')),
									nn.finals_grade) AS grade,
							cc.enrollments_id,
							cc.prospectus_courses_id AS credit_prospectus_courses_id,
							cc.other_schools_enrollments_id,
							nn.enrollments_id AS enroll_id,
							c.elective,
							(SELECT
									GROUP_CONCAT(c1.course_code,',',
											IF ((en.finals_grade IN ('5.0','NG','INC','DR','NA','NC','INE','WD') OR en.finals_grade IS NULL),
													'HNU*',en.finals_grade),',',c1.credit_units)
									FROM
									student_histories AS sh,
									enrollments AS en,
									course_offerings AS co,
									elective_courses AS ec,
									courses AS c1
									WHERE
									sh.id=en.student_history_id
									AND en.course_offerings_id=co.id
									AND co.courses_id=ec.topic_courses_id
									AND ec.topic_courses_id=c1.id
									AND ec.prospectus_courses_id=c.id
									AND sh.students_idno = {$this->db->escape($students_idno)}
							) AS elective_grade
							FROM
							prospectus_courses AS c
							LEFT JOIN credited_courses AS cc
							ON cc.prospectus_courses_id=c.id AND cc.students_idno = {$this->db->escape($students_idno)}
							LEFT JOIN enrollments AS nnn
							ON nnn.id=cc.enrollments_id
							LEFT JOIN course_offerings AS co
							ON co.id=nnn.course_offerings_id
							LEFT JOIN courses AS dd
							ON dd.id=co.courses_id
							LEFT JOIN other_schools_enrollments AS o
							ON o.id=cc.other_schools_enrollments_id
							LEFT JOIN courses AS d
							ON c.courses_id=d.id
							LEFT JOIN
									((SELECT
												e.courses_id AS courses_id,
												f.id AS enrollments_id,
												f.finals_grade AS finals_grade
											FROM
												course_offerings AS e,
												enrollments AS f,
												student_histories AS g
											WHERE
												e.id=f.course_offerings_id
												AND g.id=f.student_history_id
												AND g.students_idno = {$this->db->escape($students_idno)}
												AND e.academic_terms_id = (SELECT
																ee.academic_terms_id
														FROM
															course_offerings AS ee,
															enrollments AS ff,
															student_histories AS gg
														WHERE
															ee.id=ff.course_offerings_id
															AND gg.id=ff.student_history_id
															AND gg.students_idno = {$this->db->escape($students_idno)}
															AND ee.courses_id=e.courses_id
														ORDER BY
															ff.finals_grade
															LIMIT 1)
											GROUP BY
												e.courses_id
											ORDER BY
												f.student_history_id DESC)
									)
									AS nn ON d.id=nn.courses_id
							WHERE
							c.prospectus_terms_id = {$this->db->escape($prospectus_terms_id)}
							-- GROUP BY
							--	d.id
			";
			
			//die($q1);		
			
			/*$q1 = "SELECT
							d.id,
							d.course_code,
							d.descriptive_title,
							d.credit_units,
							c.is_bracketed,
							c.cutoff_grade,
							c.num_retakes,
							c.id AS prospectus_courses_id,
							nn.finals_grade,
							IF ((nn.finals_grade IN ('5.0','NG','INC','DR','NA','NC','INE','WD') OR nn.finals_grade IS NULL),
								IF((cc.other_schools_enrollments_id IS NULL AND cc.enrollments_id IS NULL),NULL,
									IF(cc.enrollments_id IS NULL,
										'OTHER*','HNU*')),
									nn.finals_grade) AS grade,
							cc.enrollments_id,
							cc.prospectus_courses_id AS credit_prospectus_courses_id,
							cc.other_schools_enrollments_id,
							nn.enrollments_id AS enroll_id,
							c.elective,
							(SELECT
									GROUP_CONCAT(c1.course_code,',',
										IF ((en.finals_grade IN ('5.0','NG','INC','DR','NA','NC','INE','WD') OR en.finals_grade IS NULL),
											'HNU*',en.finals_grade),',',c1.credit_units)
								FROM
									student_histories AS sh,
									enrollments AS en,
									course_offerings AS co,
									elective_courses AS ec,
									courses AS c1
								WHERE
									sh.id=en.student_history_id
									AND en.course_offerings_id=co.id
									AND co.courses_id=ec.topic_courses_id
									AND ec.topic_courses_id=c1.id
									AND ec.prospectus_courses_id=c.id
									AND sh.students_idno = {$this->db->escape($students_idno)}
							) AS elective_grade			
					FROM
							prospectus_courses AS c 
								LEFT JOIN credited_courses AS cc 
									ON cc.prospectus_courses_id=c.id AND cc.students_idno = {$this->db->escape($students_idno)}
								LEFT JOIN enrollments AS nnn
									ON nnn.id=cc.enrollments_id
								LEFT JOIN course_offerings AS co
									ON co.id=nnn.course_offerings_id
								LEFT JOIN courses AS dd
									ON dd.id=co.courses_id
								LEFT JOIN other_schools_enrollments AS o
									ON o.id=cc.other_schools_enrollments_id 
								LEFT JOIN courses AS d 
									ON c.courses_id=d.id
								LEFT JOIN 
									((SELECT 
											e.courses_id AS courses_id, 
											f.id AS enrollments_id,
											f.finals_grade AS finals_grade
										FROM
											course_offerings AS e,
											enrollments AS f,
											student_histories AS g
										WHERE 
											e.id=f.course_offerings_id
											AND g.id=f.student_history_id
											AND g.students_idno = {$this->db->escape($students_idno)}
											AND e.academic_terms_id = (SELECT 
																ee.academic_terms_id
															FROM
																course_offerings AS ee,
																enrollments AS ff,
																student_histories AS gg
															WHERE
																ee.id=ff.course_offerings_id
																AND gg.id=ff.student_history_id
																AND gg.students_idno = {$this->db->escape($students_idno)}
																AND ee.courses_id=e.courses_id
															ORDER BY
																ee.academic_terms_id DESC
															LIMIT 1)
										GROUP BY 
											e.courses_id
										ORDER BY
											f.student_history_id DESC)
									)
								AS nn ON d.id=nn.courses_id	
						WHERE
							c.prospectus_terms_id = {$this->db->escape($prospectus_terms_id)} 	
						-- GROUP BY
						--	d.id
					";	
			*/				

			
			/*
			$q1 = "
				SELECT 
					d.id,
					d.course_code,
					d.descriptive_title,
					d.credit_units,
					c.is_bracketed,
					c.id AS prospectus_courses_id,
					cc.grade AS credited_grade,
					IF ((nn.finals_grade IN ('5.0','NG','INC','DR','NA','NC','INE') OR nn.finals_grade IS NULL),
						IF(cc.grade IS NULL,0,CONCAT(cc.grade,'*')),
							nn.finals_grade) AS grade						
				FROM
					prospectus_courses AS c
						LEFT JOIN 
							credited_courses AS cc ON cc.prospectus_courses_id=c.id AND cc.students_idno = {$this->db->escape($students_idno)}
						LEFT JOIN
							courses AS d ON c.courses_id=d.id 
						LEFT JOIN 
						((SELECT 
								e.courses_id AS courses_id, 
								f.id AS enrollments_id,
								f.finals_grade AS finals_grade
							FROM
								course_offerings AS e,
								enrollments AS f,
								student_histories AS g
							WHERE 
								e.id=f.course_offerings_id
								AND g.id=f.student_history_id
								AND g.students_idno = {$this->db->escape($students_idno)}
								AND e.academic_terms_id = (SELECT 
													MAX(ee.academic_terms_id)
												FROM
													course_offerings AS ee,
													enrollments AS ff,
													student_histories AS gg
												WHERE
													ee.id=ff.course_offerings_id
													AND gg.id=ff.student_history_id
													AND gg.students_idno = {$this->db->escape($students_idno)}
													AND ee.courses_id=e.courses_id
												GROUP BY
													ee.courses_id, 
													gg.students_idno, 
													gg.prospectus_id
												LIMIT 1)
							GROUP BY 
								e.courses_id
							ORDER BY
								f.student_history_id DESC)
						)
					AS nn ON d.id=nn.courses_id
				WHERE
					c.courses_id=d.id
					AND c.prospectus_terms_id = {$this->db->escape($prospectus_terms_id)} 
				GROUP BY
					d.id 
				ORDER BY course_code ";
			*/
			
			//die($q1);		

			$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}

		//ADDED: 3/8/2013 By: genes
		function ListCourses_From_Other_Prospectus($owner_colleges_id, $courses_id) 
		{
			$result = null;
			
			$q2 = "SELECT 
						DISTINCT(a.course_code) AS course_code,
						a.id,
						a.descriptive_title,
						d.status,
						d.id AS prospectus_id 
					FROM
						courses AS a, 
						prospectus_courses AS b, 
						prospectus_terms AS c, 
						prospectus AS d,
						academic_programs AS e
					WHERE
						a.id=b.courses_id
						AND b.prospectus_terms_id=c.id
						AND d.id=c.prospectus_id
						AND e.id=d.academic_programs_id
						AND	a.owner_colleges_id={$this->db->escape($owner_colleges_id)} 
						AND e.colleges_id={$this->db->escape($owner_colleges_id)} 
						AND d.status='A'
						AND d.id NOT IN (SELECT
												dd.id
											FROM
												courses AS aa,
												prospectus_courses AS bb, 
												prospectus_terms AS cc, 
												prospectus AS dd
											WHERE
												aa.id=bb.courses_id
												AND bb.prospectus_terms_id=cc.id
												AND dd.id=cc.prospectus_id
												AND	aa.owner_colleges_id={$this->db->escape($owner_colleges_id)} 
												AND bb.courses_id = {$this->db->escape($courses_id)})
					ORDER BY
						a.course_code ";
			
			//print($q2);
			//die();				
			$query = $this->db->query($q2);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		
		function ListProspectusTermsByYr($prospectus_id, $yr_level) {
			$result = null;
			
			$q2 = "SELECT 
						id
					FROM
						prospectus_terms
					WHERE
						prospectus_id = {$this->db->escape($prospectus_id)} 
						AND year_level = {$this->db->escape($yr_level)}  ";

			//print($q2);
			//die();			
			$query = $this->db->query($q2);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}


		function ListMajorCoursesTaken($prospectus_terms, $students_idno) {
		
			$result = NULL;
			
			/*$q1 = "SELECT 
						c.is_major,
						d.id,
						d.course_code,
						d.descriptive_title,
						d.credit_units,
						c.id AS prospectus_courses_id,
						cc.grade AS credited_grade,
						IF ((nn.finals_grade IN ('5.0','NG','INC','DR','NA') OR nn.finals_grade IS NULL),IF(cc.grade IS NULL,0,CONCAT(cc.grade,'*')),
							nn.finals_grade) AS grade						
					FROM
						prospectus_courses AS c
							LEFT JOIN 
								credited_courses AS cc ON cc.prospectus_courses_id=c.id AND cc.students_idno = {$this->db->escape($students_idno)}
							LEFT JOIN
								courses AS d ON c.courses_id=d.id 
							LEFT JOIN 
							((SELECT 
									e.courses_id AS courses_id, 
									f.id AS enrollments_id,
									f.finals_grade AS finals_grade
								FROM
									course_offerings AS e,
									enrollments AS f,
									student_histories AS g
								WHERE 
									e.id=f.course_offerings_id
									AND g.id=f.student_history_id
									AND g.students_idno = {$this->db->escape($students_idno)}
									AND f.student_history_id IN (SELECT 
														MAX(ff.student_history_id)
													FROM
														course_offerings AS ee,
														enrollments AS ff,
														student_histories AS gg
													WHERE
														ee.id=ff.course_offerings_id
														AND gg.id=ff.student_history_id
														AND gg.students_idno = {$this->db->escape($students_idno)}
														AND ee.courses_id=e.courses_id
													GROUP BY
														ee.courses_id, 
														gg.students_idno, 
														gg.prospectus_id)
								GROUP BY 
									e.courses_id
								ORDER BY
									f.student_history_id DESC)
							)
						AS nn ON d.id=nn.courses_id
					WHERE
						c.courses_id=d.id
						AND c.is_major = 'Y'
						AND c.prospectus_terms_id IN $prospectus_terms
					GROUP BY
						d.id ";
					*/	
			
			$q1 = "SELECT 
						d.course_code,
						c.is_bracketed,
						c.is_major,
						d.credit_units,
						IF ((nn.finals_grade IN ('5.0','NG','INC','DR','NA','NC','WD','INE') OR nn.finals_grade IS NULL),IF(cc.grade IS NULL,0,cc.grade),
							nn.finals_grade) AS my_grades
					FROM
						prospectus_courses AS c
							LEFT JOIN 
								credited_courses AS cc ON cc.prospectus_courses_id=c.id AND cc.students_idno = {$this->db->escape($students_idno)}
							LEFT JOIN
								courses AS d ON c.courses_id=d.id 
							LEFT JOIN 
							((SELECT 
									e.courses_id AS courses_id, 
									f.id AS enrollments_id,
									f.finals_grade AS finals_grade
								FROM
									course_offerings AS e,
									enrollments AS f,
									student_histories AS g
								WHERE 
									e.id=f.course_offerings_id
									AND g.id=f.student_history_id
									AND g.students_idno = {$this->db->escape($students_idno)}
									AND f.student_history_id IN (SELECT 
														MAX(ff.student_history_id)
													FROM
														course_offerings AS ee,
														enrollments AS ff,
														student_histories AS gg
													WHERE
														ee.id=ff.course_offerings_id
														AND gg.id=ff.student_history_id
														AND gg.students_idno = {$this->db->escape($students_idno)}
														AND ee.courses_id=e.courses_id
													GROUP BY
														ee.courses_id, 
														gg.students_idno, 
														gg.prospectus_id)
								GROUP BY 
									e.courses_id
								ORDER BY
									f.student_history_id DESC)
							)
						AS nn ON d.id=nn.courses_id
					WHERE
						c.courses_id=d.id
						AND c.is_major = 'Y'
						AND c.prospectus_terms_id IN $prospectus_terms
					GROUP BY
						d.id ";

			//print($q1);
			//die();		

			$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}
		
		
		//Added: 4/15/2013
		
		function ListCoursesToBeAdvised($idno, $prospectus_id, $yr) {
			$result = null;
			
			$q1 = "SELECT 
						a.id AS courses_id,
						d.courses_id, 
						d.cutoff_grade, 
						d.num_retakes, 
						a.course_code
					FROM 
						prospectus AS b, 
						prospectus_terms AS c, 
						prospectus_courses AS d, 
						courses AS a
						
					WHERE 
						b.id=c.prospectus_id 
						AND c.id=d.prospectus_terms_id 
						AND d.courses_id=a.id 
						AND b.id={$this->db->escape($prospectus_id)} 
						AND c.year_level >= {$this->db->escape($yr)}
						AND d.elective = 'N' 
						AND d.courses_id NOT IN 
						(SELECT y.courses_id
							FROM
								enrollments AS x,
								course_offerings AS y,
								student_histories AS z
							WHERE 
								x.course_offerings_id=y.id
								AND z.id = x.student_history_id
								AND z.students_idno = {$this->db->escape($idno)} )
						AND a.id NOT IN
						(SELECT ac.courses_id
						   FROM advised_courses as ac
						 )  		
					ORDER BY
						a.course_code";
									
			//print($q1);
			//die();							
			$query = $this->db->query($q1);
			
			$result = FALSE;
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}
		
		//Added: 4/22/2013
		
			
		function UpdateProspectusCourse($data) {
			$q1 = NULL;
			
				$q1 = "UPDATE prospectus_courses
				 	   SET elective = {$this->db->escape($data['elective'])}, 
					   	   is_major = {$this->db->escape($data['is_major'])},
						   num_retakes = {$this->db->escape($data['num_retakes'])},
						   is_bracketed = {$this->db->escape($data['is_bracketed'])},
						   cutoff_grade = {$this->db->escape($data['cutoff_grade'])}
						  
						WHERE prospectus_courses.id = {$this->db->escape($data['prospectus_course_id'])}";
			
			//print($q1);
			//die();
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

	//Added: may 24, 2013 by Amie
	function get_max_credit_units($program, $yearlevel, $term) {
		$result = null;
		
		$q = "SELECT pt.max_credit_units
				FROM prospectus_terms pt
					JOIN prospectus p ON p.id = pt.prospectus_id
					JOIN academic_programs ap ON ap.id = p.academic_programs_id
					JOIN student_histories sh ON sh.prospectus_id = p.id
					JOIN students s ON s.idno = sh.students_idno
					JOIN academic_terms at ON at.id = sh.academic_terms_id
				WHERE pt.term = at.term
					AND pt.year_level = {$this->db->escape($yearlevel)}
					AND ap.id = {$this->db->escape($program)}
					AND at.id = {$this->db->escape($term)}";
		
		
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
			
		return $result;
	}

	
	//ADDED: 10/24/13 by genes
	function getMaxCreditByYear($prospectus_id, $year_level) {
		$result = null;
		
		$q = "SELECT
					SUM(a.max_credit_units) AS max_credit_units
				FROM
					prospectus_terms AS a
				WHERE
					a.prospectus_id = {$this->db->escape($prospectus_id)}
					AND a.year_level <= {$this->db->escape($year_level)}";
		
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
			
		return $result;
	
	}
	
	/*
	* @EDITED: 1/30/15 by genes
	* @NOTES: include grad_date
	*/
	function ListStudentSY($student_id) {
			
		$result = null;
			
 		$q1 = "SELECT
 					a.id AS student_histories_id,
 					CASE b.term
						WHEN 1 THEN '1st Semester' 
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
					END AS term,
					CONCAT('S.Y. ',c.end_year-1,' - ',c.end_year) AS sy,
 					e.description AS program_name,
 					a.prospectus_id AS prospectus_id,
 					c.end_year AS sy_year,
 					b.term AS sy_term,
 					(SELECT
 							DATE_FORMAT(g.grad_date,'%M %e, %Y')
 						FROM
 							graduation_dates AS g,
 							academic_terms AS b1,
 							academic_programs AS e1
 						WHERE
 							g.academic_terms_id=b1.id
 							AND g.academic_programs_id=e1.id
 							AND b1.id=b.id
 							AND e1.id=e.id limit 1)
 					AS grad_date,
 					e.so_text
 				FROM
 					student_histories AS a,
 					academic_terms AS b, 
 					academic_years AS c,
 					prospectus AS d,
 					academic_programs AS e,
 					enrollments AS f
 				WHERE
 					a.academic_terms_id=b.id
 					AND b.academic_years_id=c.id
 					AND a.prospectus_id=d.id
 					AND d.academic_programs_id=e.id
 					AND f.student_history_id = a.id
 					AND a.students_idno = {$this->db->escape($student_id)}
 					AND b.term != 0 
 				GROUP BY
 					a.academic_terms_id
 				ORDER BY
 					sy_year DESC, sy_term DESC";

 		//print($q1); die();
		$query = $this->db->query($q1);
			
		if ($query !== FALSE) {
			if($query->num_rows() > 0){
				$result = $query->result();
			}
		} else {
			$result = FALSE;
		}

		//log_message("INFO","ListStudentSY:"); //toyet 4.13.2018
		//log_message("INFO",print_r($q1,TRUE)); //toyet 4.13.2018

		
		return $result;
	}

	/*
	 * @ADDED: 10/3/14
	 * @author: genes
	 */
	function ListProspectusTermsWithCourses($prospectus_id=NULL) {  // edit: added c.paying_units for accounts - ra 8/23/2016
			
		$result = null;
				
		$q1 = "
				SELECT 	b.id,
						b.prospectus_terms_id, 
						a.year_level, 
						a.max_credit_units, 
						a.max_bracket_units,
						b.num_retakes,
						b.cutoff_grade,
						b.courses_id,
						IF(b.is_bracketed = 'Y',CONCAT('(',c.credit_units,')'),c.credit_units) AS credit_units,
						c.paying_units,
						CASE a.term
							WHEN 1 THEN '1st Semester'
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
							END AS term,
						CASE a.year_level
							WHEN 1 THEN '1st Year'
							WHEN 2 THEN '2nd Year'
							WHEN 3 THEN '3rd Year'
							WHEN 4 THEN '4th Year'
							WHEN 5 THEN '5th Year'
							END as y_level,
						c.course_code,
						c.descriptive_title
					FROM 
						prospectus_terms AS a,
						prospectus_courses AS b,
						courses AS c
					WHERE 
						b.prospectus_terms_id = a.id
						AND b.courses_id=c.id
						AND a.prospectus_id={$this->db->escape($prospectus_id)}
					ORDER BY 
						a.year_level, 
						a.term 
			" ;
		//print_r($q1);die();
		$query = $this->db->query($q1);
								
		if($query->num_rows() > 0){
			$result = $query->result();
		}
				
		return $result;
	}


	/*
	 * @ADDED:  9.19.2018
	 * @author: Toyet
	 */
	function listSHSProspectuses(){

		$result = null;

		$sql = "select p.id,
	       			   ap.description,
		       		   p.effective_year
				from prospectus p
				left join academic_programs ap on ap.id=p.academic_programs_id
				left join acad_program_groups ag on ag.id=ap.acad_program_groups_id
				where ag.levels_id=12
				order by description, effective_year desc;";

		$query = $this->db->query($sql);
								
		if($query->num_rows() > 0){
			$result = $query->result();
		}
				
		return $result;
	}
	
	
}