<?php

class av_lib {
	protected $CI;
	
	public function __construct(){
		$this->CI =& get_instance();
	}	
	
	public function calendar(){
		$month = $this->CI->input->get_post('month') ? 	$this->CI->input->get_post('month') :	$month = date('m') ;
		$year  = $this->CI->input->get_post('year') ? $this->CI->input->get_post('year') :  $year = date('Y') ;
		$where = $this->CI->input->get_post('where') ? $this->CI->input->get_post('where') : $where = '';
	
		if ($where == 'prev' ){
			if ($month < 2){
				$month=12;
				$year=$year-1;
			}else{
				$month = $month -1;
				$year = $year;
			}
		}
		if ($where == 'next'){
			if ($month > 11){
				$month = 1;
				$year  = $year + 1;
			}else{
				$month = $month  + 1;
				$year  = $year;
			}
		}
	
		$data = array('month' => $month, 'year' => $year);
	
		$this->CI->load->model('av/av_model');
	
		$data['av'] = $this->CI->av_model->list_bookings($year,$month);
	
		//print_r($data);die();
	
		$this->CI->content_lib->enqueue_body_content('av/calendar_view',$data);
		$this->CI->content_lib->content();
	
	}
	
	
	
	function list_all_bookings_of_the_day($activity_date){
		$this->CI->load->model('av/av_model');
		$query = $this->CI->av_model->list_all_bookings_of_the_day($activity_date);
		if (!$query){
			$output['type'] = 'Error';
			$output['message'] = 'Unable to get list of bookings!';
			$this->output->set_output(json_encode($output));
			return;
		}
		$output = array(
				'type'=>'Success',
				'heading'=>'List of bookings for ' . date('M d, Y', strtotime($activity_date)) . ':' ,
				'data'=>$query->result(),
		);
		$this->CI->output->set_output(json_encode($output));
	}
	

	function add_booking($activity_date,$activity_time_s,$activity_time_e,$activity_name,$equipment,$size,$remark,$location){
		if  (strtotime($activity_time_e) - strtotime($activity_time_s) < 3600){ //should be at least 30 minutes
			$output['type'] = 'Error';
			$output['message'] = 'Invalid Time or too short!';
			$this->CI->output->set_output(json_encode($output));
			return 0;
		}
		$this->CI->load->model('av/av_model');
	
	
		// find for conflict
		$bookings = $this->CI->av_model->list_approved_booking_of_the_day($activity_date,$location);
		if ($bookings == false){
			$output['type'] = 'Error';
			$output['message'] = 'Unable to get list of bookings!';
			//echo "Unable to get list of bookings!" . $this->db->last_query();
			$this->CI->output->set_output(json_encode($output));
			return 0;
		}
	
		if ($bookings != 1 && !empty($bookings)){
			foreach ($bookings as $booking){
				if  (strtotime($activity_time_e) > strtotime($booking->time_start) AND strtotime($booking->time_end) > strtotime($activity_time_s)){
					$output['type'] = 'Error';
					$output['message'] = "Sorry, this is in conflict with " .  $booking->booked_by .  "'s booking at "
							. $booking->time_start . "-" . $booking->time_end . " on "
									. date('M d, Y',strtotime($activity_date)) . ".";
					$this->CI->output->set_output(json_encode($output));
					return 0;
				}
	
			}
		}
	 
		if ($this->CI->av_model->add_booking($activity_date,$activity_time_s,$activity_time_e,$activity_name,$equipment,$size,$remark,$location)){
			$output['type'] = 'Success';
			$output['message'] = 'Booking successfully submitted!';
			//$output['message'] = $this->CI->db->last_query();
		}else{
			$output['type'] = 'Error';
			$output['message'] = 'Unable to submit booking!';
			//echo  "Submission Failed!";
		}
		$this->CI->output->set_output(json_encode($output));
		return;
		//$this->content_lib->set_message($activity_time_s,"alert-sucess");
		//$this->content_lib->content();
	}
	
	
	function cancel_my_booking($ids_to_cancel){
		$this->CI->load->model('av/av_model');
		if ($cancelled = $this->CI->av_model->mark_cancelled($ids_to_cancel)  AND $cancelled>0){
			$output['type'] = "Success";
			$output['message'] = "Booking cancelled successfully!";
			$output['id'] = $this->CI->input->post('check');
			//$output['message'] = $this->db->last_query() ;
		}else{
			$output['type'] = "Error";
			$output['message']="Failed to cancell!";
		}
		$this->CI->output->set_output(json_encode($output));
		return;
	}
	
	
}
?>