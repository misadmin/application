<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requirements_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}
	
	
	function ListRequirements() {
		$result = NULL;
	
		$q = "SELECT *
				FROM requirements";
			
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
	}
	
	function students_requirements($idno) {
		$result = NULL;
		
		$q = "SELECT sr.id, sr.students_idno, sr.date_submitted, r.description
				FROM students_requirements AS sr, requirements as r
				WHERE sr.requirements_id = r.id
					AND sr.students_idno = {$this->db->escape($idno)}";
	
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
	}	
	
	function add_requirement($data) {
	
		$query = "INSERT INTO students_requirements(students_idno, requirements_id, date_submitted, inserted_by)
			VALUES ('{$data['idno']}', '{$data['req_id']}', '{$data['date']}','{$data['empno']}' )";

		if ($this->db->query($query))
			return $this->db->insert_id(); else
			return FALSE;
	}
	
	function delete_requirement($req_id) {
		$q = "DELETE from students_requirements
				WHERE id = {$this->db->escape($req_id)}";
	
		if ($this->db->query($q))
			return TRUE; else
			return FALSE;
	
	}
}