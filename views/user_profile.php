<div id="maincontainer" class="clearfix">
	<div class="row-fluid">
		<div class="span9">
			<div class="tabable">
				 <ul id="myTab" class="nav nav-tabs">
					<li <?php if($active=='profile') echo 'class="active"'; ?>><a href="#profile" data-toggle="tab">Profile</a></li>
					<li <?php if($active=='password') echo 'class="active"'; ?>><a href="#password" data-toggle="tab">Password Settings</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade <?php if($active=='profile') echo 'in active'; ?>" id="profile">
						<div class="row-fluid">
							<div class="span12">
								<form id="userinfo_form" method="post" action="<?php echo site_url('/profile/profile'); ?>" class="form-horizontal">
									<input type="hidden" name="username" value="<?php echo $username; ?>"/>
									<?php $this->common_functions->insert_hidden_nonce(); ?>
									<?php if ($active=='profile' && !empty($message)) : ?>
									<div class="alert <?php echo $severity; ?>"><button type="button" class="close" data-dismiss="alert">×</button><?php echo $message; ?></div>
									<?php endif; ?>
									<fieldset>
										<div class="control-group formSep">
											<label for="username" class="control-label">Username</label>
											<div class="controls">
												<input type="text" id="username" name="usernamed" class="input-xlarge" value="<?php echo $username; ?>" disabled="true"/>
											</div>
										</div>
										<div class="control-group formSep">
											<label for="firstname" class="control-label">First name</label>
											<div class="controls">
												<input type="text" id="firstname" name="firstname" class="input-xlarge" value="<?php echo $firstname; ?>" />
											</div>
										</div>	
										<div class="control-group formSep">
											<label for="lastname" class="control-label">Family name</label>
											<div class="controls">
												<input type="text" id="lastname" name="lastname" class="input-xlarge" value="<?php echo $lastname; ?>" />
											</div>
										</div>
										<div class="control-group formSep">
											<label for="email" class="control-label">Email</label>
											<div class="controls">
												<input type="text" id="email" name="email" class="input-xlarge" value="<?php echo $email; ?>" />
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button class="btn btn-primary" type="submit">Update Personal Info</button>
											</div>
										</div>
										
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="tab-pane fade <?php if($active=='password') echo 'in active'; ?>" id="password">
						<div class="row-fluid">
							<div class="span12">
								<form id="passwords_form" method="post" action="<?php echo site_url('/profile/password'); ?>" class="form-horizontal">
									<?php $this->common_functions->insert_hidden_nonce(); ?>
									<?php if ($active=='password' && !empty($message)) : ?>
									<div class="alert <?php echo $severity; ?>"><button type="button" class="close" data-dismiss="alert">×</button><?php echo $message; ?></div>
									<?php endif; ?>
									<fieldset>
										<div class="control-group formSep">
											<label for="password" class="control-label">Old Password</label>
											<div class="controls">
												<input type="password" id="password" name="password" class="input-xlarge" value=""/>
											</div>
										</div>
										<div class="control-group formSep">
											<label for="newpassword1" class="control-label">New Password</label>
											<div class="controls">
												<input type="password" id="newpassword1" name="newpassword1" class="input-xlarge" value="" />
											</div>
										</div>	
										<div class="control-group formSep">
											<label for="newpassword2" class="control-label">Repeat New Password</label>
											<div class="controls">
												<input type="password" id="newpassword2" name="newpassword2" class="input-xlarge" value="" />
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button class="btn btn-primary" type="submit">Update Password</button>
											</div>
										</div>
										
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--form validation-->
<script>
	$(document).ready(function(){
		$('#userinfo_form').validate({
			onkeyup: false,
			errorClass: 'error',
			validClass: 'valid',
			rules: {
				username: { required: true, minlength: 3 },
				firsname: { required: true, minlength: 2 },
				lastname: { required: true, minlength: 2 },
				email: { required: true, email: true, minlength: 3 }
			},
			highlight: function(element) {
				$(element).closest('div').addClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						unhighlight: function(element) {
							$(element).closest('div').removeClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						errorPlacement: function(error, element) {
							$(element).closest('div').append(error);
						}
		});
		$('#passwords_form').validate({
			onkeyup: false,
			errorClass: 'error',
			validClass: 'valid',
			rules: {
				password: { required: true, minlength: 6 },
				newpassword1: { required: true, minlength: 6 },
				newpassword2: { required: true, equalTo: "#newpassword1" }
			},
			highlight: function(element) {
				$(element).closest('div').addClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						unhighlight: function(element) {
							$(element).closest('div').removeClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						errorPlacement: function(error, element) {
							$(element).closest('div').append(error);
						}
		});
	});
</script>


