<?php 
$display_checkbox = (isset($fired_from_posting) AND $fired_from_posting == TRUE)? TRUE:FALSE; 
?>
<style>
.written_off {color: #ff0000;background-color:#ff3300;}
.unposted {color: #6f6;background-color:#efe;}
</style>
<div class="" style="min-height:200px;">
	<div style="margin-left: 20px;" class="btn-group">
		<a id="ledger-toggler" class="btn dropdown-toggle" data-toggle="dropdown" href="#">
			<span id="selected_option">All</span>
			<span class="caret"></span>
		</a>
		<ul class="dropdown-menu">
			<li>
				<a class="show ledger-toggle-item" href="#">All</a>
				<a class="show ledger-toggle-item" href="#">This School Year</a>
			</li>
		</ul>
	</div>
		
		<table id="ledger-table" style="margin-top: 20px;" class="table  table-condensed table-bordered table-hover table-striped">
			<thead>
				<tr>
<?php echo $display_checkbox==TRUE? 
				 	'<th width="4%" style="text-align: center">&nbsp</th>':'';
?>
					<th width="10%" style="text-align: left">Tran Date</th>
					<th width="20%" style="text-align: left">Item Description</th>
					<th width="12%" style="text-align: left">Ref No.</th>
<?php echo $display_checkbox==TRUE? 
					'<th width="8%" style="text-align: right">Debit</th>
					<th width="8%" style="text-align: right">Credit</th>':
					'<th width="8%" style="text-align: right">Debit</th>
					<th width="8%" style="text-align: right">Credit</th>'
?>					
					<th width="8%" style="text-align: right">Balance</th>
					<th width="18%" style="text-align: center">Remark</th>
					<th width="10%" style="text-align: center">Status</th> 
				</tr>
			</thead>
			<tbody>
				<tr id="row_balance">
					<td colspan=" <?php echo 5+$display_checkbox; ?> " style="text-align:center">FORWARDED BALANCE >>></td>
					<td id="forwarded_balance" style="text-align: right"></td>
					<td></td>
					<td></td>
				</tr>
	<?php  if (is_array($ledger_data) && count($ledger_data) > 0): $running_balance = 0; $count=0; ?>
	<?php foreach ($ledger_data as $ledge): $running_balance +=  $ledge['debit'] - $ledge['credit']; $date=$ledge['transaction_date']; ?>
				<?php $bal = ($running_balance < 0 ? "(".number_format(abs($running_balance), 2).")" : number_format($running_balance,2)); ?>
				<tr class="ledger <?php echo $ledge['status']; ?>" 	date="<?php echo $date; ?>" bal="<?php echo $bal; ?>">
<?php
					if ($display_checkbox)
						if ($ledge['status']=='active')
							echo '<td style="text-align: center"><input class="checkboxes" type="checkbox" name="check[]'  . '" value=' . $ledge['ledger_id'] . '></td>';
						else
							echo '<td style="text-align: center"><input class="checkboxes" type="checkbox" readonly disabled name="check[]'  . '" value=' . $ledge['ledger_id'] . '></td>';
							
?>						
					<td class="transaction_date"><?php echo date('M d, Y', strtotime(str_replace('-', '/', $ledge['transaction_date']))) ; ?></td>
					<td class="transaction_detail"><?php echo $ledge['transaction_detail']; ?></td>
					<td class="reference_number" style="text-align: left"><?php echo $ledge['reference_number']; ?></td>
					<td class="debit"style="text-align: right"><?php echo ($ledge['debit']>0 ? number_format($ledge['debit'], 2):""); ?></td>
					<td class="credit" style="text-align: right"><?php echo ($ledge['credit']>0 ? number_format($ledge['credit'], 2):""); ?></td>
					<td style="text-align: right"><?php echo  ($running_balance < 0 ? "(".number_format(abs($running_balance), 2).")" : number_format($running_balance,2)); ?></td> 
					<td class="remark"><?php echo $ledge['remark']; ?></td>
					<td style="text-align:center" class="status"><?php echo ucfirst(str_replace("_", " ", ($ledge['status'])!='active' ? $ledge['status'] : '')); ?></td>
				</tr>
	<?php endforeach;  ?>
				<tr>
						
					<td colspan="8" style="text-align: right"> &nbsp; </b> </td>
				
				</tr>
	

				<?php if($bal_b4_assessment<>0) { ?>
					<tr>
						<td colspan="5" style="text-align: right"> <b> Previous Balance </b> </td>
						<!-- <td style="text-align: right"><?php echo number_format($bal_b4_assessment, 2); ?></td> -->
						<?php if($bal_b4_assessment<0){ 
							echo '<td style="text-align: right">('.number_format(abs($bal_b4_assessment), 2).')</td>';
						    } else { 
							echo '<td style="text-align: right">'.number_format($bal_b4_assessment,2).'</td>';
						} ?>
						<td colspan="2" style="text-align: right"></td>
					</tr>
				<?php } ?>

				<?php if($trans_after_assess<>0) { ?>
					<tr>
						<td colspan="5" style="text-align: right"> <b> Debit-Credit Transactions </b> </td>
						<!-- <td style="text-align: right"><?php echo number_format($bal_b4_assessment, 2); ?></td> -->
						<?php if($trans_after_assess<0){ 
							echo '<td style="text-align: right">('.number_format(abs($trans_after_assess), 2).')</td>';
						    } else { 
							echo '<td style="text-align: right">'.number_format($trans_after_assess,2).'</td>';
						} ?>
						<td colspan="2" style="text-align: right"></td>
					</tr>
				<?php } ?>

				<tr>
					<?php if (!$error) { ?>
						<td colspan="5" style="text-align: right"> <b> Due for <?php print($exam_number);?> ----> </b> </td>
						<td style="text-align: right"><?php echo ($due > 0 ? number_format($due, 2) : '0.00'); ?></td>
						<td colspan="2" style="text-align: right"></td>
					<?php } else { ?>
						<td colspan="8" style="text-align: left; color: #FF0000; font-size:15px;"> <b><?php echo $error_message; ?></b> </td>
				
					<?php }?>
				</tr>

				<?php if($bal_b4_assessment<>0) { ?>
					<tr>
						<td colspan="5" style="text-align: right"> <b> Total Payable for <?php print($exam_number);?></b> </td>
						<?php if($due<0){ 
							echo '<td style="text-align: right">'.number_format(0, 2).'</td>';
						    } else { 
							echo '<td style="text-align: right">'.number_format($due, 2).'</td>';
						} ?>
						<td colspan="2" style="text-align: right"></td>
					</tr>
				<?php } ?>


	<?php endif;?>
			</tbody>
		</table>
		
			<!-- <form method="post" id="print_statement">
				<input type="hidden" name="action" value="print_statement" />
				
				<fieldset>
				<div class="controls">
					<button id="print_statement" type="button" class="btn btn-primary btn-large">Download in PDF</button>
				</div>
				</fieldset>
			</form> -->
		
		
		
		
		
		
<?php if ($display_checkbox): ?>  		
	<button class="btn btn-primary" id="open_modal">Write-Off</button>		
	<div id="writeoff_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="WriteOffModal" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    <h3 id="feedback_modal_label">Please confirm Write-Off of the following record:</h3>
		  </div>
		  <div class="modal-body" id ="modal-body">		
				<form id="frm_writeoff" method="post" class="form-vertical">
					<input name="tab" value="ledger" type="hidden" />					
					<input type="hidden" name="action" value= "writeoff"/> 
					<input type="hidden" name="id_to_writeoff" value=""/>					
					<?php $this->common->hidden_input_nonce(FALSE); ?>
					<table class= "table table-bordered table-condensed table-striped table-hover" id="table_in_modal">		  		 
				  		<tr><td width="40%" style="text-align:right">Tran Date:</td><td width="60%"></td></tr>
				  		<tr><td style="text-align:right">Item Description:</td><td></td></tr>
				  		<tr><td style="text-align:right">Ref No:</td><td></td></tr>
				  		<tr><td style="text-align:right">Credit:</td><td></td></tr>
				  		<tr><td style="text-align:right">Debit:</td><td></td></tr>
					</table>					
					<center><br><input type="submit" value="Confirm Write-Off"></input></center>
				</form>
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  </div>
	</div>	
<?php endif ?>		
		
</div>
<script>
	$(document).ready(function(){
		var offset = $('.navbar').height();
	    $("#ledger-table").stickyTableHeaders();
	    $('#ledger-table').stickyTableHeaders('updateWidth');

		var year_start_date = "<?php echo $year_start_date; ?>";
		var start_date = "1900-01-01";
	    
		$('.ledger').each(function(){

			var date = $(this).attr('date');

			if(date < start_date){
				var bal = $(this).attr('bal');/* parseFloat($(this).attr('bal')).toFixed(2); */
				$('#forwarded_balance').html(bal);
				$(this).hide();
			}
		});

		$('.show').bind('click', function(e){
			e.preventDefault();
			var text = $(this).text();			
			$('#row_balance').show();
			switch(text){
				case "This School Year": start_date = year_start_date; break;
				case "All": start_date = "1900-01-01";
					$('#row_balance').hide(); break;
			}			
			$('#selected_option').html(text);
			$('.ledger').each(function(){
				$(this).show();
			});
			$('.ledger').each(function(){
				var date = $(this).attr('date');
				if(date < start_date){
					var bal = $(this).attr('bal');/* parseFloat($(this).attr('bal')).toFixed(2); */
					$('#forwarded_balance').html(bal);
					$(this).hide();
				}
			});

		});

		$("#open_modal").on('click',function (e) {
			var num_of_checks = $(".checkboxes:checked").length;
			if (num_of_checks != 1){ 
				alert("Please tick one checkbox!");
				return false;
			}
			var id_to_writeoff = 	$(".checkboxes:checked").attr("value");
			var transaction_date = $(".checkboxes:checked").parents("tr").children("td.transaction_date").html();	
			var transaction_detail = $(".checkboxes:checked").parents("tr").children("td.transaction_detail").html();	
			var reference_number = $(".checkboxes:checked").parents("tr").children("td.reference_number").html();	
			var credit = $(".checkboxes:checked").parents("tr").children("td:nth-child(5)").html();	
			var debit  = $(".checkboxes:checked").parents("tr").children("td:nth-child(6)").html();	
			$('#frm_writeoff input[name=id_to_writeoff]').val(id_to_writeoff);
			$("#table_in_modal tr:nth-child(1) td:nth-child(2)").html(transaction_date);
			$("#table_in_modal tr:nth-child(2) td:nth-child(2)").html(transaction_detail);		
			$("#table_in_modal tr:nth-child(3) td:nth-child(2)").html(reference_number);		
			if (credit.length > 0){
				$("#table_in_modal tr:nth-child(5) td:nth-child(2)").html('');			
				$("#table_in_modal tr:nth-child(4) td:nth-child(2)").html(credit);		
			} else {
				$("#table_in_modal tr:nth-child(4) td:nth-child(2)").html('');		
				$("#table_in_modal tr:nth-child(5) td:nth-child(2)").html(debit);			
			};
			$('#writeoff_modal').modal('show');
			e.preventDefault();			
		});		
	});
</script>
<?php if(isset($can_print_statement) && $can_print_statement):?>


<script>
$(document).ready(function(){
	$('.print_content').on('click', function(){
		var what = $(this).data('what');
		var content_to_print = $('#'+what).text();

		//alert(content_to_print);

		var hiddenElement = document.createElement('a');

		hiddenElement.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(content_to_print);
		hiddenElement.target = '_blank';
		hiddenElement.download = 'receipt.txt';
	document.getElementById('container').appendChild(hiddenElement);
		hiddenElement.click();
	});
});
</script>



<div class="row-fluid" style="margin-top:20px">
	<button class="btn btn-large btn-primary print_content" data-what="assessment_to_print">Print Assessment</button> 
	<button class="btn btn-large btn-primary print_content" data-what="invoice">Print <?php print($exam_number); ?></button> 
	<button class="btn btn-large btn-primary print_content" data-what="print_clearance">Print Clearance</button>
	<div id="container" style="display:none;"></div>
</div>

<div class="row-fluid" style="margin-top:4px">
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-large btn-info btn-lg" data-toggle="modal" data-target="#viewAssmt">View Assessment</button>
  <button type="button" class="btn btn-large btn-info btn-lg" data-toggle="modal" data-target="#viewExam2">View <?php print($exam_number); ?></button>
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal hide fade" id="viewAssmt" role="dialog" style="width:1000px; margin-left:-500px;" >
    <div class="modal-dialog modal-lg" role="document" style="font-family:'Courier New';
   font-size:11px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Student Assessment</h4>
        </div>
        <div class="modal-body">
          <pre><?php echo '<script type="text/javascript">document.write(document.getElementById("assessment_to_print").innerHTML);</script>' ?></pre>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>  
</div>


<div class="container">
  <!-- Modal -->
  <div class="modal hide fade" id="viewExam2" role="dialog" style="width:1000px; margin-left:-500px;" >
    <div class="modal-dialog modal-lg" role="document" style="font-family:'Courier New';
   font-size:11px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php print($exam_number); ?></h4>
        </div>
        <div class="modal-body">
          <pre><?php echo '<script type="text/javascript">document.write(document.getElementById("invoice").innerHTML);</script>' ?></pre>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>  
</div>
<?php endif; ?>

