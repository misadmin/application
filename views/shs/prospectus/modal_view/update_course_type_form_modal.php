
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Course</div>
			<div class="col2a">
				<input type="text" class="form-control" style="width:380px;" id="descriptive_title_update_modal" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Course Type</div>
			<div class="col2a">
				<select class="form-control" style="width:auto; height:auto;" id="subject_types_id_update_modal" >
					<?php 
						foreach ($s_types AS $stype) {
					?>		
							<option value="<?php print($stype->id); ?>" ><?php print($stype->description); ?></option>
					<?php
						}
					?>
				</select>
			</div>
		</div>
	</div>
