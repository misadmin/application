<h2 class="heading">Select Department</h2>
<form class="form-horizontal" action="<?php echo site_url('dean/create_program');?>" method="post">
	<div class="control-group">
			  <label class="control-label" for="department">Department&nbsp:&nbsp</label>
					  <div class="controls">
							 <select name="departments_id" id="departments_id">
	        					<?php
								foreach($departments AS $department) {
									echo "<option value=".$department->id.">".$department->department."</option>";	
								} ?>
	      					</select>
					</div>
	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Continue</button>
	</div>
	
	</div>
</form>