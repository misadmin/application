<?php 
class AR_Balance_ver2 {
	
	protected $CI;
	public function __construct(){
		$this->CI =& get_instance();
	}

	
	public function index(){
		
		$this->CI->load->library('tab_lib');
		$this->CI->load->model('accountant/accountant_model');
		$this->CI->load->model('hnumis/AcademicYears_Model');		
		$groups = $this->CI->accountant_model->get_acad_program_groups();		
		$academic_terms = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
		$academic_years = $this->CI->accountant_model->get_academic_years();		
		$school_years 	= $this->CI->accountant_model->get_academic_years();		
		$academic_years = array();		
		foreach($school_years as $school_year){
			unset($academic_years[$school_year->id]);
			$academic_years[$school_year->id] = $school_year->academic_year;
		}				
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');		
		$this->CI->content_lib->enqueue_body_content('accountant/ar_balance_ver2_index',
										array('message'=>$this->CI->error_message,
												'groups'=>$groups,
												'academic_years'=>$academic_years,
											)
									);
		
		$this->CI->content_lib->content();
	}

	public function ar_balance_ver2(){
		//log_message('info','TOYET: file ar_balance_ver2.php line 38');
		$this->CI->load->library('table_lib');
		//log_message('info','TOYET: file ar_balance_ver2.php line 40');
		$this->CI->load->model('accountant/acct_reports_model');
		//log_message('info','TOYET: file ar_balance_ver2.php line 42');
		$start_date = $this->CI->input->post('start_date');
		//log_message('info','TOYET: file ar_balance_ver2.php line 44');
		$end_date = $this->CI->input->post('end_date');
		//log_message('info','TOYET: file ar_balance_ver2.php line 46');
		$show_by = $this->CI->input->post('show_by');
		//log_message('info','TOYET: file ar_balance_ver2.php line 48');
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');
		$s = strtotime($start_date);
		$e = strtotime($end_date);
		//log_message('info','TOYET: file ar_balance_ver2.php line 53');
		$levels_array=array(
				0 => 'Nursery',
				1 => 'Kindergarten',
				2 => 'Kinder',
				3 => 'Grade School',
				4 => 'Day High School',
				5 => 'Night High School',
				6 => 'College',
				7 => 'Graduate School',
				8 => '',
				11 =>'Preschool',
				12 => 'Senior High School',
				99 => 'Others'
		);
		$year_level_array=array(
				1 => 'First Year',
				2 => 'Second Year',
				3 => 'Third Year',
				4 => 'Fouth Year',
				5 => 'Fifth Year'
		);

		if ($end_date <= $this->CI->config->item('last_isis_db') || $start_date <= $this->CI->config->item('last_isis_db')){
			$this->CI->error_message = 'Please enter date later than '.$this->CI->config->item('last_isis_db');
			$this->CI->index();
			return;				
		}		
		
		
		if (!$start_date || !$end_date){
			$this->CI->error_message = 'Please Enter Valid Date';
			$this->CI->index();
			return;
		}
		//check for valid start and end date
		else if (!$s || !$e || !($s <= $e)){
			$this->CI->error_message = 'End Date must be greater than or equal to Start Date! ';
			$this->CI->index();
			return;
		}
		

		if ($start_date <= $this->CI->config->item('last_isis_db')){
			$this->CI->error_message = 'Date should be after' . $this->CI->config->item('last_isis_db') ;
			$this->CI->index();
			return;
		}
		
		//$start_date = '2017-06-01';
		//$end_date = '2017-08-31';
		log_message('info','before acct_reports_model->ar_balance_mis($start_date,$end_date)');
		$data  = $this->CI->acct_reports_model->ar_balance_mis($start_date,$end_date);
		//print_r($data);die();
		//$data2 = array(
		//		'levels_id' 	=> 8, 
		//		'group_name' 	=> 'Sample',
		//		'year_level'	=> '1',
		//		'code' 			=> 'xAssessment',
		//		'description' 	=> 'xdescription',
		//		'stud' 			=> 'xstud',
		//		'debit' 		=> '0.00',
		//		'credit' 		=> '0.00',
		//	);
		
		//log_message('info','RESULT:');
		//log_message('info',print_r($data,TRUE));

		//array_push($data,(object)$data2);		
		//print_r($data);die();
		log_message('info','before enqueue_body_content("accountant/ar_balance_ver2_table"');
		$this->CI->content_lib->enqueue_body_content('accountant/ar_balance_ver2_table', array(
				'data' 		=> $data,
				'start_date'=> $start_date,
				'end_date'  => $end_date,
				'tab'      	=> 'College',
				'levels_array'=> $levels_array,
				'year_level_array'=> $year_level_array,
				)
			);
		$this->CI->content_lib->content();
	}


	public function ar_balance_ver2_raw(){
		//echo "here";die();
		$this->CI->load->library('table_lib');
		$this->CI->load->model('accountant/acct_reports_model');
		$start_date = $this->CI->input->post('start_date');
		$end_date = $this->CI->input->post('end_date');
		$show_by = $this->CI->input->post('show_by');
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');
		$s = strtotime($start_date);
		$e = strtotime($end_date);
		$levels_array=array(
				0 => 'Nursery',
				1 => 'Raw Kindergarten',
				2 => 'Kinder',
				3 => 'Grade School',
				4 => 'Day High School',
				5 => 'Night High School',
				6 => 'College',
				7 => 'Graduate School',
				8 => '',
				11 =>'Preschool',
				12 => 'Senior High School',
		);
		$year_level_array=array(
				1 => 'First Year',
				2 => 'Second Year',
				3 => 'Third Year',
				4 => 'Fouth Year',
				5 => 'Fifth Year',
		);

		if ($end_date <= $this->CI->config->item('last_isis_db') || $start_date <= $this->CI->config->item('last_isis_db')){
			$this->CI->error_message = 'Please enter date later than '.$this->CI->config->item('last_isis_db');
			$this->CI->index();
			return;				
		}		
		
		
		if (!$start_date || !$end_date){
			$this->CI->error_message = 'Please Enter Valid Date';
			$this->CI->index();
			return;
		}
		//check for valid start and end date
		else if (!$s || !$e || !($s <= $e)){
			$this->CI->error_message = 'End Date must be greater than or equal to Start Date! ';
			$this->CI->index();
			return;
		}
		

		if ($start_date <= $this->CI->config->item('last_isis_db')){
			$this->CI->error_message = 'Date should be after' . $this->CI->config->item('last_isis_db') ;
			$this->CI->index();
			return;
		}
		
		//$start_date = '2017-06-01';
		//$end_date = '2017-08-31';
		log_message('info',"before acct_reports_model->ar_balance_mis_raw($start_date,$end_date);");
		$data  = $this->CI->acct_reports_model->ar_balance_mis_raw($start_date,$end_date);
		//print_r($data);die();
		//$data2 = array(
		//		'levels_id' 	=> 8, 
		//		'group_name' 	=> 'Sample',
		//		'year_level'	=> '1',
		//		'code' 			=> 'xAssessment',
		//		'description' 	=> 'xdescription',
		//		'stud' 			=> 'xstud',
		//		'debit' 		=> '0.00',
		//		'credit' 		=> '0.00',
		//	);
		
		//log_message('info',print_r($data,TRUE));

		//array_push($data,(object)$data2);		
		//print_r($data);die();
		log_message('info','Finished extracting data...  proceeding view report');

		$this->CI->content_lib->enqueue_body_content('accountant/ar_balance_ver2_table_raw', array(
				'data' 		=> $data,
				'start_date'=> $start_date,
				'end_date'  => $end_date,
				'tab'      	=> 'College',
				'levels_array'=> $levels_array,
				'year_level_array'=> $year_level_array,
				)
			);
		$this->CI->content_lib->content();
	}


	public function ar_balance_summary_ver2(){
		//echo "here";die();
		$this->CI->load->library('table_lib');
		$this->CI->load->model('accountant/acct_reports_model');
		$start_date = $this->CI->input->post('start_date');
		$end_date = $this->CI->input->post('end_date');
		$show_by = $this->CI->input->post('show_by');
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');
		$s = strtotime($start_date);
		$e = strtotime($end_date);
		$levels_array=array(
				0 => 'Nursery',
				1 => 'Kindergarten',
				2 => 'Kinder',
				3 => 'Grade School',
				4 => 'Day High School',
				5 => 'Night High School',
				6 => 'College',
				7 => 'Graduate School',
				8 => '',
				11 => 'Preschool',
				12 => 'Senior High School',
				99 => 'Other Payors',
		);
		$year_level_array=array(
				1 => 'First Year',
				2 => 'Second Year',
				3 => 'Third Year',
				4 => 'Fouth Year',
				5 => 'Fifth Year',
		);
	
		if ($end_date <= $this->CI->config->item('last_isis_db') || $start_date <= $this->CI->config->item('last_isis_db')){
			$this->CI->error_message = 'Please enter date later than '.$this->CI->config->item('last_isis_db');
			$this->CI->index();
			return;
		}
	
	
		if (!$start_date || !$end_date){
			$this->CI->error_message = 'Please Enter Valid Date';
			$this->CI->index();
			return;
		}
		//check for valid start and end date
		else if (!$s || !$e || !($s <= $e)){
			$this->CI->error_message = 'End Date must be greater than or equal to Start Date! ';
			$this->CI->index();
			return;
		}
	
	
		if ($start_date <= $this->CI->config->item('last_isis_db')){
			$this->CI->error_message = 'Date should be after' . $this->CI->config->item('last_isis_db') ;
			$this->CI->index();
			return;
		}
	
		$data  = $this->CI->acct_reports_model->ar_balance_mis($start_date,$end_date,TRUE);
		//print_r($data);die();
		$data2 = array(
				'levels_id' 	=> 8,
				'group_name' 	=> 'Sample',
				'year_level'	=> '1',
				'trangroup'  	=> '1',
				'code' 			=> 'xAssessment',
				'description' 	=> 'xdescription',
				'stud' 			=> 'xstud',
				'debit' 		=> '0.00',
				'credit' 		=> '0.00',
		);
	
		array_push($data,(object)$data2);
		//print_r($data);die();
	
		$this->CI->content_lib->enqueue_body_content('accountant/ar_balance_ver2_summary_table', array(
				'data' 		=> $data,
				'start_date'=> $start_date,
				'end_date'  => $end_date,
				'tab'      	=> 'College',
				'levels_array'=> $levels_array,
				'year_level_array'=> $year_level_array,
		)
		);
		$this->CI->content_lib->content();
	}
	


	
	
}