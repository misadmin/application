
<h2 class="heading">Senior High School Assessment Report - By Strand</h2>

<form id="assessment_report_form" method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value="generate_report" />
	
	<div class="control-group">
		    <label class="control-label" for="academic_terms_id">Academic Term:</label>
		    <div class="controls ">
			     <select name="academic_terms_id" id="academic_terms_id">
				     	<?php
							foreach($academic_terms AS $academic_term) {
						?>
								<option value="<?php print($academic_term->id); ?>" <?php if ($current_term->id == $academic_term->id) print("selected"); ?> 
									term_sy="<?php print($academic_term->term." ".$academic_term->sy); ?>" >
									<?php print($academic_term->term." ".$academic_term->sy); ?>
								</option>	
						<?php 
							}
						?>
                  </select>
	    	</div>
	</div>	
	
	<div class="control-group">
		    <label class="control-label" for="strand_id">Strand:</label>
		    <div class="controls ">
			     <select name="strand_id" id="strand_id" style="width:auto;">
					<option value="All" selected="selected">All Strands</option>
					  	<?php
							foreach($strands AS $strand) {
						?>
								<option value="<?php print($strand->academic_programs_id); ?>" 
										description="<?php print($strand->description); ?>"
										abbreviation="<?php print($strand->abbreviation); ?>">
									<?php print($strand->description);?>
								</option>
						<?php
							}
						?>
                 </select>
	    	</div>
	</div>
	
	<div class="control-group">
		    <label class="control-label" for="grade_level">Grade Level:</label>
		    <div class="controls">
			    <select name="grade_level" id="grade_level">
					  <option value="0" selected="selected">All Grade Levels</option>
					  <option value="11">Grade 11</option>
					  <option value="12">Grade 12</option>
                </select>
	    	</div>
	</div>		
	<div class="control-group">
    	 <div class="controls">
    		<button class="btn btn-primary" name="generate_report_button" type="submit" id="gen_report_button">Generate Report!</button>
    	</div>
	</div>
	
</form>


<script>
$('#gen_report_button').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to generate Tuition Report?");

	if (confirmed){
		
		var element1    = $("option:selected", "#academic_terms_id");
		var term_sy     = element1.attr('term_sy');

		var element2    = $("option:selected", "#strand_id");
		var description = element2.attr('description');
		var abbreviation = element2.attr('abbreviation');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'term_sy',
		    value: term_sy,
		}).appendTo('#assessment_report_form');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'description',
		    value: description,
		}).appendTo('#assessment_report_form');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'abbreviation',
		    value: abbreviation,
		}).appendTo('#assessment_report_form');
		
		
		$('#assessment_report_form').submit();
	}		
});
</script>


  