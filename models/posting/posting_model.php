<?php 

class posting_model extends CI_Model {

	public function write_off($id){
		if ($id > 0)
			$sql="
				update ledger set status = 'written_off' where id = '{$id}';  
			";
		else 
			return 0;
		$this->db->query($sql);
		return $this->db->affected_rows();	
	}

	
	public function get_payers_transactions($payers_id, $last_transaction_date){
		$sql ="
			select p.id as payments_id, p.transaction_date, p.isis_status, s.idno, cap_first(concat_ws(', ',s.lname, s.fname)) as student
			from payments p
			left join payers r on r.id = p.payers_id
			left join students s on s.idno = r.students_idno		
			where from_isis='Y' and transaction_date <= '{$last_transaction_date}' and payers_id = '{$payers_id}'
			order by transaction_date desc 
		";
		//print_r($sql);$this->db->last_query();die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	}
	
	public function get_payers_transactions_for_mis($payers_id, $last_transaction_date){
		$sql ="
			select l.id as ledger_id, l.transaction_date, l.status, s.idno, cap_first(concat_ws(', ',s.lname, s.fname)) as student
			from ledger l
			left join students s on s.idno = l.idno
			left join payers   r on r.students_idno = l.idno
			where r.id = '{$payers_id}'
			order by l.transaction_date desc 
		";
		// print_r($sql);die();
		// log_message("INFO", print_r($sql,true)); // Toyet 6.14.2018
		// Die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	}

	public function write_off_payments($payments_id){
		if ($payments_id > 0)
			$sql="
				update payments 
				set isis_status = 'written_off',
					writtenoff_by = '{$this->session->userdata('empno')}',
					writtenoff_on = now()
				where id = '{$payments_id}'
			";
		else
			return 0;		
		//print_r($sql); $this->db->trans_rollback();	die();				
		$this->db->query($sql);
		return $this->db->affected_rows();		
	}

	public function write_off_ledger_for_mis($ledger_id){
		if ($ledger_id > 0)
			$sql="
				update ledger 
				set status = 'written_off'
				where id = '{$ledger_id}'
			";
		else
			return 0;
		// print_r($sql); $this->db->trans_rollback();	die();
		//log_message("INFO", print_r($sql,true)); // Toyet 6.14.2018
		$this->db->query($sql);
		return $this->db->affected_rows();		
	}

	public function mark_writeoff_table_done($idno){
		if ($idno > 0)
			$sql="
				update " .  $this->session->userdata('write_off_table') . " set status = 'written_off' where idno = '{$idno}';
			";
		else
			return 0;		
		//log_message("INFO", print_r($sql,true)); // Toyet 6.14.2018
		$this->db->query($sql);
		return TRUE; //$this->db->affected_rows();
	}


	// added by Toyet 5.21.2018
	//   - for use with generating writeoff base table
	public function payerslist(){
		$sql = "select id, students_idno from payers;";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	//added by Toyet 7.27.2018
	public function getPostedAssessment($students_idno,$student_histories_id){
		$sql = "select assessed_amount 
                from assessments ass 
                left join student_histories sh on sh.id=ass.student_histories_id
                where sh.students_idno={$students_idno}
                      and student_histories_id={$student_histories_id}";
	    log_message("INFO",print_r($sql,true));
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
