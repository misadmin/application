<?php 
//print($with_dwct."<br>");
//print($grad_hnu."<br>");
//print_r($graduated_programs);die();

//print_r($this->session); die();
//$this->session->set_userdata('flash_idnumber',$idnumber);
//$this->session->set_userdata('flashstudent_name', $firstname . ' ' . substr($middlename,0,1) . '. '. $familyname );
//$this->session->set_userdata('flash_course',$level .' '.$course);
//$this->session->set_userdata('flashcourse',$course);
?>
<?php
	if ($graduated_programs) {
		if (($with_dwct AND $grad_hnu) OR $grad_hnu) { 
?>

	<form method="POST" target="_blank">
		<input type="hidden" name="with_dwct" value="<?php if($with_dwct){
					print("Y");
				} else {
					print("N");
				}
			?>" />
		<input type="hidden" name="grad_hnu" value="<?php if($grad_hnu){
					print("Y");
				} else {
					print("N");
				}
			?>" />
  		<input type="hidden" name="action" value="generate_diploma" />

<table class="table table-condensed" style="width:50%; border: solid; border-width: 1px; 
				border-color: #D8D8D8;">
 	<thead>
		<tr>
			<th colspan="3">Generate Diploma</th>
		</tr>
	</thead>
	<tr style="vertical-align:middle">
		<td style="width:28%;">Graduated Program</td>
		<td style="width:2%;">:</td>
		<td>
			<select name="graduated_students_id" style="width:auto; height:auto;" id="hnu_program">
				<?php 
					$cnt=1;
					foreach($graduated_programs AS $g_program) {
						print("<option value=\"".$g_program->id."\" prog=\"".$g_program->academic_programs_id."\" cnt=".$cnt." >".$g_program->program_name."</option>");
						$cnt++;
					}
				?>
			</select>	
		</td>
	</tr>
	<tr style="vertical-align:middle">
		<td>Special Order</td>
		<td>:</td>
		<td><input type="text" name="so_text" value="SO (B) No. 410-0505 s. 1989 dated April 20, 1989." style="width:300px; height:auto;"></td>
	</tr>
	<?php	 
		if ($graduated_programs[0]->academic_programs_id == 613) {
	?>		
	<tr style="vertical-align:middle" class="leb_serial">
		<td>LEB No.</td>
		<td>:</td>
		<td><input type="text" name="leb_no" style="width:300px; height:auto;"></td>
	</tr>
	<tr style="vertical-align:middle" class="leb_serial">
		<td>Serial No.</td>
		<td>:</td>
		<td><input type="text" name="serial_no" style="width:300px; height:auto;"></td>
	</tr>
	<?php
		}
	?>
	<tr style="vertical-align:middle; display:none;" class="leb_serial" >
		<td>LEB No.</td>
		<td>:</td>
		<td><input type="text" name="leb_no2" style="width:300px; height:auto;"></td>
	</tr>
	<tr style="vertical-align:middle; display:none;" class="leb_serial">
		<td>Serial No.</td>
		<td>:</td>
		<td><input type="text" name="serial_no2" style="width:300px; height:auto;"></td>
	</tr>
	
		<?php 
			if ($with_dwct) {
		?>
	<tr>
		<td style="width:auto;">Graduation Date</td>
		<td style="width:2%">:</td>
		<td><input type="date" name="grad_date" style="width:130px;" value="<?php 
			print(DATE('Y-m-d')); ?>"></td>
	</tr>			
		<?php 
			}
		?>
	<tr>
		<td colspan="3">
			<input type="submit" value="Proceed >>" class="btn btn-primary">
		</td>
	</tr>	
	<tr>
		<td colspan="3" id="texting"></td>
	</tr>
</table>
</form>
<?php 
	} else {
		print("
		<span style=\"font-weight: bold; margin-left: 20px; font-size: 16px; color: #FF0000;\">Student cannot be issued a Diploma because he/she is not a graduate in this University!</span>
		");
	}
		} else {
			print("
			<span style=\"font-weight: bold; margin-left: 20px; font-size: 16px; color: #FF0000;\">SET FIRST THE GRADUATED PROGRAM!</span>
			");
		}
?>


<script>
function previewDiploma(){
	window.open("<?php echo site_url('/dric/print_diploma'); ?>", "_blank","toolbar=yes, scrollbars=yes, resizable=yes");
}
</script>				

<script>
$("#hnu_program").change(function(){
            var element = $("option:selected", this);
            var myTag = element.attr("prog");

            if (element.attr("prog") == '00613') {
				$(".leb_serial").show();				
			} else {
				$(".leb_serial").hide();				
			}
			
			//$('#texting').text(myTag);
            
        });
</script>

