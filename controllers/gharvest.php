<?php

class Gharvest extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->content_lib->set_title ('Golden Harvest | ' . $this->config->item('application_title'));
		$this->navbar_data['menu'] = array('Student'	=>'student',);
		$this->content_lib->set_navbar_content('', $this->navbar_data);

	}
	
	public function student(){
		$this->content_lib->set_title('Golden Harvest | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		$this->load->helper('student_helper');
		//$this->load->model('academic_terms_model');		
		//if ( ($gh_data = $this->academic_terms_model->getCurrentAcademicTermID()) == FALSE ) die('Error found!');
				
		if ( ! empty($query)){
						
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);			
			$this->load->library('pagination_lib');			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					//$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
		
		if (is_numeric($idnum)){
			$this->load->model('student_common_model');			
			$this->content_lib->set_title ('Golden Harvest | ' . $idnum);
			$result = $this->student_common_model->my_information($idnum);	
					
			$my_tab = array_fill(0,21,FALSE);
			$my_tab[0]= TRUE;
					
			if ($result != FALSE) {

				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname. " " . ucfirst(substr($result->mname,0,1)) . ". " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->year_level,
						'full_home_address'	=> $result->full_home_address,
						'full_city_address'	=> $result->full_city_address,
						'phone_number'		=> $result->phone_number,
						'section'			=> $result->section,						
				);
				
				
				if (!is_college_student($idnum) || $result->year_level < 3){

					$this->content_lib->set_message('Access Denied!', 'alert-error');					
					
				}else{
					
				
					if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
						$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
					else {
						if ($result->gender == 'F')
							$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
							$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
					}					
					
					$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);

				}
				
				$result = $this->student_common_model->my_information($idnum);					

				$this->load->library('tab_lib');

				if (!is_college_student($idnum) || $result->year_level < 4){
					$this->content_lib->set_message('Access Denied!', 'alert-error');
				}else{				
					$this->tab_lib->enqueue_tab ('Student Information', 'common/student_information_full', $result, 'student_info', $my_tab[0]);				
					$tab_content = $this->tab_lib->content();
					$this->content_lib->flush_sidebar();
					$this->content_lib->enqueue_body_content("", $tab_content);
				}

			} else {
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}

		$this->content_lib->content();		
	}
	
	
}