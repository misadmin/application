<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>

<div style="background:#FFF; width:60%; margin-left:20px;">
<h3>Year Level Assessment</h3><br>
<div style="width:100%; margin-bottom:10px;">
  <table border="0" cellspacing="0" class="table table-hover table-condensed" style="width:100%; padding:0px; margin:0px; ">
					<tr >
					  <td width="26%" >Academic Term </td>
					  <td width="1%" >:</td>
					  <td width="73%" ><?php print($current->term." ".$current->sy); ?></td>
	  </tr>
	  <tr >
					  <td >My Current Year Level</td>
					  <td >:</td>
					  <td ><?php print($current_yr_level); ?> Year</td>
	  </tr>
					<tr >
					  <td >None Elective Units</td>
					  <td >:</td>
					  <td ><?php print(number_format($data['total_credits']-$data['total_elective'],1)); ?> units</td>
	  </tr>
					<tr >
					  <td >Elective Units</td>
					  <td >:</td>
					  <td ><?php print(number_format($data['total_elective'],1)); ?> units</td>
	  </tr>
					<tr >
					  <td >Units Needed for Year Level Promotion<span style="color:#FF0000; font-weight:bold;">*</span></td>
					  <td >:</td>
					  <td ><?php print($data['total_credits']); ?> units</td>
	  </tr>
					<tr >
					  <td >Units Taken<span style="color:#FF0000; font-weight:bold;">*</span> </td>
					  <td >:</td>
					  <td ><?php print(number_format($data['cnt_credit_taken'],1)); ?> unit/s </td>
	  </tr>
					<tr >
					  <td >REMARKS</td>
					  <td >:</td>
					  <td style="font-weight:bold;"><?php print("RECOMMENDED Year Level: ".$data['year_level']); ?></td>
	  </tr>
	  	
	  	<?php
	  		
	  		if (($data['year_level'] > $my_yr_level) AND ($data['cnt_credit_taken'] >= $data['total_credits'])) {
	  		//if (($data['year_level'] > $my_yr_level) AND ($data['can_update'] == 'Y')) {
		?>	
		  <tr>
		  	<td>&nbsp;</td>
	  		<td>&nbsp;</td>
			<td>  			
	  		<a href="<?php print($data['year_level']); ?>" class="btn btn-primary" id="update_yr_level" history_id="<?php print($student_histories_id); ?>"><i class="icon-repeat icon-white"></i> Update Year Level!</a>
	  		</td>
	  		</tr>
		<?php 
	  		} else {	
		//} elseif (($data['year_level'] != $my_yr_level)) {
		?>
		  <tr>
		  	<td>&nbsp;</td>
	  		<td>&nbsp;</td>
			<td>
	  		Year Level can be updated only if <b><i>RECOMMENDED Year Level</i></b> is greater than <b><i>My Current Year Level</i></b> 
	  		and <b><i>Units Taken</i></b> is equal or greater than <b><i>Units Needed for Year Level Promotion</i></b>.
	  		</td>
	  	  </tr>
		<?php 
			}
	  	?>	  	
</table>

</div>

<div style="width:100%;">
  <table border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr style="font-weight:bold; vertical-align:top;">
    <td style="color:#FF0000; width:12%;">*NOTES:</td>
    <td style="text-align:justify; width:88%;">
    <ol>
    	<li>Only CREDIT units are counted. BRACKETED Units are excluded!</li>
    	<li>Year Level Assessment is done during payment of College Registration. For 
    	students with privileges that no longer need to pay the College Registration, 
    	Year Level Assessment is done during the time the Accounts Clerk allows them to enroll.</li>
    	<li>CREDIT units will NOT be counted until your instructors will submit the final passing grades.</li>
    	<li>Incase your instructors SUBMITTED their final passing grades only AFTER you paid the 
    	College Registration, you may click the <span class="btn btn-primary">
    	<i class="icon-repeat icon-white"></i> Update Year Level!</span> 
    	button that will appear above to recompute your units.</li>
    </ol>
    </td>
    </tr>
    <?php 
    	if ($this->session->userdata('role') == 'dean') { 
    ?>
    <tr style="vertical-align:top; font-weight:bold;">
	    <td style="color:#FF0000;">For the Deans:</td>
	    <td style="text-align:justify; color:#3f8020;">
    	To be able the subjects to be CREDITED AUTOMATICALLY, please note of the following:<br>
    	<ol style="margin-left:40px;">
    		<li>Elective subjects must be assigned as Electives. Click 
    		<a href="<?php print(site_url('dean/assign_elective_topic'));?>" class="btn btn-primary">
    		<i class="icon-random icon-white"></i> Assign Elective Course!</a>
    		to assign them to Electives.</li>
    		<li>Subjects MUST be assigned to ONE elective only. They will be CREDITED SEPARATELY if
    			assigned to more than one Electives. It's okay if ALL elective subjects are assigned to only
    			ONE Elective as long as they are not assigned to another Elective.</li>
    		<li>DO NOT credit the subject if it is already assigned to Electives. It will again be 
    		CREDITED SEPARATELY.</li> 
    	</ol>
    	</td> 
    </tr>
    <?php 
    	}
    ?>
</table>

</div>

</div>

  <form id="update_yr_level_form" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" /> 
  <input type="hidden" name="action" value="update_year_level" />
</form>

<script>
	$('#update_yr_level').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Proceed to UPDATE my Year Level?');

		if (confirmed){
			var new_yr_level = $(this).attr('href');
			var history_id = $(this).attr('history_id');
		
			$('<input>').attr({
				type: 'hidden',
				name: 'new_yr_level',
				value: new_yr_level
			}).appendTo('#update_yr_level_form');

			$('<input>').attr({
				type: 'hidden',
				name: 'history_id',
				value: history_id
			}).appendTo('#update_yr_level_form');

			$('#update_yr_level_form').submit();
		}
		
	});
</script>


