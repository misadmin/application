<div style="margin-top:10px">
<h2 class="heading"> Faculty Load (<?php print("SY ".$term->sy." ".$term->term); ?> )</h2>
<h2> <a href="download" class="btn btn-link pull-right" id="download_list"><i class="icon-download-alt"></i> <strong>Download</strong></a> </h2>	

<table class="table table-striped table-bordered">
	<thead >
		<tr align="center">
			<th>Emp ID.</th>
			<th>Name</th>
			<th>Subject</th>
			<th>Section</th>
			<th>Credit Units</th>
			<th>Paying Units</th>
			<th>Schedule Day</th>
			<th>Schedule Time</th>
			<th>Classroom</th>
			<th># of Students</th>
		</tr>
	</thead>
	<tbody> 
<?php if($loads){ ?>
	<?php foreach ($loads as $load): ?>
		<tr>
			<td><?php print($load->empno); ?></td>
			<td><?php print($load->name); ?></td>
			<td><?php print($load->course_code); ?> </td>
			<td><?php print($load->section_code); ?></td>
			<td><?php echo $load->credit_units; ?></td>
			<td><?php echo $load->paying_units; ?></td>
			<td><?php print($load->days_day_code); ?></td>
			<td><?php print($load->start_time."-".$load->end_time); ?></td>
			<td><?php echo $load->room_no; ?></td>
			<td><?php echo $load->num_enrollees; ?></td>
		</tr>
<?php 
	endforeach; ?>
<?php } ?>
	</tbody>
</table>

<form method="post" action="<?php echo site_url("{$role}/download_faculty_load_csv"); ?>" id="download_list_form">
			<input type="hidden" name="term_id" value="<?php echo $term->id; ?>" />
			<input type="hidden" name="term" value="<?php echo $term->term; ?>" />
			<input type="hidden" name="sy" value="<?php echo $term->sy; ?>" />
</form>

<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
		
</div> 
