<?php ?>
<h2 class="heading">Courses Offered: <?php //echo $term; ?></h2>
<table class="table table-striped table-bordered mediaTable activeMediaTable" id="MediaTable-0">
	<thead>
		<tr>
			<th>Course catalog</th>
			<th>Section</th>
			<th>Instructor</th>
			<th>Max Students</th>
			<th>Schedule</th>
		</tr>
	</thead>
	<tbody>
<?php $prev=""; ?>
<?php foreach ($result as $res): ?>
<?php 
	if($prev!=$res->course_code . $res->section_code) :
?><tr>
		<td><?php echo $res->course_code; ?></td>
		<td><?php 
		//print("<a href=".site_url('dean/assign_faculty_to_schedule/2/'.$course->id."/".$course->courses_id).">".$course->course_code." [".$course->section_code."]</a>");
		//print(site_url('dean/assign_faculty_to_schedule/2/'.$res->id));
		//print("<a href=".site_url(.$res->id).">".$res->course_code."[".$res->section_code."]</a>");
		echo $res->section_code; ?></td>
		<td><?php echo $res->fullname; ?></td>
		<td><?php echo $res->max_students; ?></td>
		<td><?php echo $res->schedule; ?></td>
	</tr>
<?php else: ?>
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><?php echo $res->schedule; ?></td>
	</tr>
<?php endif;?>
<?php $prev = $res->course_code . $res->section_code; ?>
<?php endforeach; ?>
	</tbody>
</table>
