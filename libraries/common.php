<?php
/**
 * Common methods used by controllers before completely loading the controller's functions. 
 * 
 * @package HNU-MIS
 * @subpackage libraries
 * @author HNU-MIS Team
 */
class Common {
	private $CI;
	private $nonce;
	public $previous_nonce;
	
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->library ('session');
		
		if ( ! $this->CI->session->userdata('nonce')) {
			//Nonce didn't exist yet... This will happen during startup... so lets make one
			$this->nonce = $this->new_nonce (10);
			$this->CI->session->set_userdata('nonce', $this->nonce);
		} else {
			if ($this->CI->input->post('nonce')) {
				$this->previous_nonce = $this->CI->session->userdata('nonce');
				$this->nonce = $this->new_nonce (10);
				$this->CI->session->set_userdata('nonce', $this->nonce);
			} else {
				$this->nonce = $this->CI->session->userdata('nonce');
			}
		}
	}
	
	public function need_auth(){
		$role = $this->CI->uri->segment(1);
		
		if (isset($this->role))
			return true;
		
		//user does not have an existing session OR user is not logged_in
		if ( $this->CI->session->userdata('logged_in') == FALSE ){
			//used to redirect the user to the page he/she wish to be
			$this->CI->session->set_flashdata('referrer', current_url());
			redirect ("{$role}/auth");
		}

		//user's role is not one of his roles...
		else if ( $role !== $this->CI->session->userdata('role' ) ){
			
			redirect ("{$role}/auth");
		}
			
	} 
	
	public function nonce (){
		return $this->nonce;
	}
	
	public function hidden_input_nonce($return = TRUE){
		if ($return)
			return '<input type="hidden" name="nonce" value="' . $this->nonce . '" />' . "\n"; else
			echo '<input type="hidden" name="nonce" value="' . $this->nonce . '" />' . "\n";
	}
	
	public function nonce_is_valid($nonce=''){
		if ($nonce == $this->previous_nonce)
			return TRUE; else
			return FALSE;
	}
	
	private function new_nonce ( $length = 10 ){
		$str = '1 2 3 4 5 6 7 8 9 0 a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z';
		$char_arr = explode (' ', $str);
		
		$nonce = '';
		for ($count = 1; $count <=10; $count++){
			$nonce .= $char_arr[array_rand($char_arr)]; 
		}
		return $nonce;
	}
}