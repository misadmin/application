 
<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
  <tr>
  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">
    <strong><font face="Verdana, Arial, Helvetica, sans-serif"> STUDENT PAYMENTS</font></strong>
   </td>
  </tr>
</table>	

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:10px;">
  <table width="75%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:90%; padding:0px; margin:10px;">
    <thead>
      <tr>
         <th width="10%" style="text-align:center;">OR No.</th>
		 <th width="15%" style="text-align:center;">Payment Date</th>
		 <th width="15%" style="text-align:center;">Machine No.</th>
		 <th width="15%" style="text-align:center;">Teller</th>
         <th width="20%" style="text-align:center;">Particulars</th>
         <th width="15%" style="text-align:center;">Amount Paid</th>
         <th width="10%" style="text-align:center;">Status</th>         
	  </tr>
    </thead>
    <?php
		if ($payments_info) {
			foreach ($payments_info AS $payment) {
	?>
    <tr>
      <td style="text-align:left;"><?php print($payment->receipt_no); ?></td>
	  <td style="text-align:center;"><?php print($payment->payment_date); ?></td>
	  <td style="text-align:center;"><?php print($payment->machine_ip); ?></td>
	  <td style="text-align:left;"><?php echo $payment->stud_name == NULL? $payment->emp_name : $payment->stud_name; ?> </td>
      <td><?php print($payment->description); ?></td>
      <td style="text-align:right;"><?php print number_format($payment->receipt_amount,2); ?></td>
      <td style="text-align:center;"><?php echo $payment->status; ?></td>
    </tr>
    <?php
			}
		}else{ ?>
		 <tr> 
		    <td colspan="6">No payments yet</td>
		</tr>	
		<?php }   ?>
  </table>
</div>
