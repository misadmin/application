<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

<form action="<?php echo site_url('accounts/grad_matric_fees');?>" method="post">
  <?php echo validation_errors(); 
  ?>
  <input type="hidden" name="step" value="2" />
  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" style="width:50%; margin-top:35px;" class="head1">
      <tr class="head">
        <td colspan="3" class="head"> ADD GRADUATE PROGRAMS MATRICULATION FEE </td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="2" cellspacing="0" class="inside">
          <tr>
            <td width="199" align="left" valign="middle">School Year </td>
            <td width="15" align="left" valign="middle">:</td>
            <td width="438" align="left" valign="middle">
			<select name="academic_years_id" id="academic_years_id" class="form">
              <?php
					foreach($academic_years AS $acad_year) {
						print("<option value=".$acad_year->id.">".$acad_year->start_year."-".$acad_year->end_year."</option>");	
					}
				?>
              </select></td>
          </tr>
          <tr>
            <td align="left" valign="middle">Program</td>
            <td align="left" valign="middle">:</td>
            <td align="left" valign="middle"><select name="programs_id" id="programs_id" class="form">
              <?php
					foreach($programs AS $program) {
						print("<option value=".$program->id.">".$program->description."</option>");	
					}
				?>
                        </select></td>
          </tr>
          <tr>
            <td>Year Level </td>
            <td>:</td>
            <td><select name="yr_level" id="yr_level" class="form">
              <option value="1" selected="selected">1st Year</option>
              <option value="2">2nd Year</option>
             </select></td>
          </tr>
          <tr>
            <td>Amount</td>
            <td>:</td>
            <td><input name="rate" type="text" id="rate" size="10" maxlength="10" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="button2" id="button2" value="Save Tuition Fee!"  class="btn btn-success" /></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</form>