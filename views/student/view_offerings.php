<h3 class="heading">List of Current Semester's Offerings</h3>
<div class="span9">
 <table id="offerings" class="table table-bordered table-hover table-condensed" >
   <thead>
     <tr>
       <th style="text-align:center; vertical-align:middle;" width="12%" title="Click here to sort table by Course Code">Course</th>
       <th style="text-align:center; vertical-align:middle;" width="8%" >Section</th>
       <th style="text-align:center; vertical-align:middle;" width="35%" title="Click here to sort table by description">Description</th>
       <th style="text-align:center; vertical-align:middle;" width="5%" >Units</th>
       <th style="text-align:center; vertical-align:middle;" width="20%">Time</th>
       <th style="text-align:center; vertical-align:middle;" width="10%">Room</th>
       <th style="text-align:center; vertical-align:middle;" width="10%" title="Viewing of enrolled students is not anymore allowed.">Enrolled</th>
     </tr>
   </thead>
   <?php
				if ($courses) {
					foreach ($courses AS $course) {
		?>
   <tr>
     <td style="text-align:left; vertical-align:middle; ">
<?php 
						if ($course->elective == 'Y') {
							print($course->course_code." (<i>".$course->mother_code.")</i>");	
						} else {						
							print($course->course_code);	
						}	
?>
	 </td>
     <td style="text-align:left; vertical-align:middle; "><?php print($course->section_code); ?></td>
     <td align="left" style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
     <td style="text-align:center; vertical-align:middle; "><?php 
		  	if ($course->is_bracketed == 'Y') {
				print("(".$course->credit_units.")"); 
			} else {
				print($course->credit_units); 				
			}
			?></td>
     <?php
			$offering_slots = $this->Offerings_Model->ListOfferingSlots($course->id);
			echo '<td style="text-align:left; vertical-align:middle; ">';
			foreach($offering_slots[0] AS $slots) 
				echo $slots->tym.' '.$slots->days_day_code . '<br>';
			echo '<td style="text-align:left; vertical-align:middle; ">';
			foreach($offering_slots[0] AS $slots) 
            	echo $slots->room_no . '<br>';
            
         ?></td>
     <td style="text-align:center; vertical-align:middle; "><?php print($course->enrollee."/".$course->max_students); ?></td>
   </tr>
   <?php
					}
				}
		?>
 </table> 
</div>
 <script>
$().ready(function(){
    $("#offerings").stickyTableHeaders();
    $('#offerings').stickyTableHeaders('updateWidth');	
	$('#offerings').dataTable({
				"iDisplayLength": 50,
			    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
	} 
	);
})
</script>
 