<?php 
/**
* Class MY_Config
*
* Controller that should be the parent of all employee type controllers.
* Inherits the CI_Controller class.
*
* @author HNU-MIS Team
* @package HNU-MIS
* @subpackage core
* @since version 1
*/
class MY_Config extends CI_Config {
	public function __construct(){
		parent::__construct();
	}
	
	public function base_url($uri=''){
		if($this->item('use_ssl'))
			return str_replace('http://', 'https://', parent::base_url($uri));
		else
			return parent::base_url($uri);
	}
	
	public function site_url($uri=''){
		if($this->item('use_ssl'))
			return str_replace('http://', 'https://', parent::site_url($uri));
		else
			return parent::site_url($uri); 
	}
}