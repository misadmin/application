<?php 

class Psychometrician_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * All Exams
	 * 
	 * Returns all the examination types.
	 * 
	 * @return variant false on error, db result on success
	 */
	public function all_exams ($include_inactive=TRUE){
		$sql = "
			SELECT
				id, abbreviation, description, status
			FROM
				exam_types
			";
		
		if ( ! $include_inactive)
			$sql .= "WHERE
						status='active'
					";
		
		$sql .= "ORDER BY
				abbreviation
			";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
	}
	
	/**
	 * Method: insert_exam
	 * 
	 * Inserts a new exam to the database
	 * 
	 * @param associative array $data
	 * @return variant will return insert_id when successful or false when an error occurred
	 */
	public function insert_exam ($data) {
		$sql = "
			INSERT into
				exam_types (`abbreviation`, `description`, `status`,`inserted_by`,`inserted_on`)
			VALUES
				(
					{$this->db->escape($data['abbreviation'])}, 
					{$this->db->escape($data['description'])}, 
					{$this->db->escape($data['status'])},
					{$this->db->escape($this->session->userdata('empno'))},
					now()
				)
		";
		if ($result = $this->db->query($sql)) 
			return $this->db->insert_id(); else
			return FALSE;	
	}
	
	public function update_exam ($data){
		$sql = "
			UPDATE 
				`exam_types`
			SET
				`abbreviation`={$this->db->escape($data['abbreviation'])},
				`description`={$this->db->escape($data['description'])},
				`status`={$this->db->escape($data['status'])}
			WHERE
				`id`={$this->db->escape($data['id'])}
			";
		
		if ($result = $this->db->query($sql)) 
			return TRUE; else
			return FALSE;
	}
	
	public function student_exams ($student_id, $include_inactive=FALSE) {
		
		$sql = "
			SELECT
				et.id, et.abbreviation, et.description,
				DATE_FORMAT(tr.date_taken, '{$this->config->item('mysql_date_format')}') as date_taken, 
				tr.id as test_result_id, tr.raw_score,tr.percentile, tr.description as comment, 
				CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term
			FROM
				test_results tr
			LEFT JOIN
				exam_types et
			ON
				tr.exam_types_id=et.id
			LEFT JOIN
				student_histories sh
			ON
				tr.student_histories_id=sh.id
			LEFT JOIN
				academic_terms at
			ON
				at.id=sh.academic_terms_id
			LEFT JOIN
				academic_years ay
			ON
				ay.id=at.academic_years_id
			LEFT JOIN
				employees e
			ON
				tr.employees_empno=e.empno
			WHERE
				sh.students_idno={$this->db->escape($student_id)}
			";
	
		if (!$include_inactive)
			$sql.= " AND et.status='active' ";
		
		$sql.= "ORDER BY
				tr.date_taken 
			DESC
				";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;	
	}
	
	public function insert_student_exam_result ($data) {
		$sql = "
			INSERT into
				test_results(`student_histories_id`, `employees_empno`, `exam_types_id`, `date_taken`, `raw_score`, `percentile`, `description`,inserted_on)
			VALUE
				(
				{$this->db->escape($data['student_histories_id'])}, 
				{$this->db->escape($data['employees_empno'])}, 
				{$this->db->escape($data['exam_types_id'])}, 
				{$this->db->escape($data['date_taken'])}, 
				{$this->db->escape($data['raw_score'])}, 
				{$this->db->escape($data['percentile'])}, 
				{$this->db->escape($data['description'])},
				now()
				)
			";
		//print_r($sql);die();
		if ($result = $this->db->query($sql))
			return $this->db->insert_id(); else
			return FALSE;
	}
	
	public function edit_student_exam_result ($data) {
		
		$sql = "
			UPDATE
				test_results
			SET
				`raw_score`={$this->db->escape($data['raw_score'])}, 
				`percentile`={$this->db->escape($data['percentile'])}, 
				`description`={$this->db->escape($data['description'])}
			WHERE
				id={$this->db->escape($data['id'])}
			";
		if ($result = $this->db->query($sql))
			return TRUE; else
			return FALSE;
	}
}