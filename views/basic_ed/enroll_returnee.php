 <?php if (isset($message) && $tab=='9') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:5px; ">
 <form method="post" id="promotion">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "promote_returnee"/>
	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" > 
	<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
	   <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    <strong><font face="Verdana, Arial, Helvetica, sans-serif"> PROMOTION</font></strong>
		   </div>
		   </td>
 	 </tr>
	  <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">
		    <font face="Verdana, Arial, Helvetica, sans-serif"><b> Student Payments </b></font>
		   </td>
 	 </tr>
	</table>	

		  
	<div style="margin-bottom:20px; ">
	  <table width="75%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:90%; padding:0px; margin:10px;">
	    <thead>
	      <tr>
	         <th width="8%" style="text-align:center;">OR No.</th>
			 <th width="12%" style="text-align:center;">Payment Date</th>
			 <th width="25%" style="text-align:center;">Particulars</th>
	         <th width="10%" style="text-align:center;">Amount Paid</th>
	         <th width="10%" style="text-align:center;">Status</th>
	       	 <th width="25%" style="text-align:center;">Teller</th>         
		  </tr>
	    </thead>
	    <?php
			if ($payments_info) {
				foreach ($payments_info AS $payment) {
		?>
	    <tr>
	      <td style="text-align:left;"><?php print($payment->receipt_no); ?></td>
		  <td style="text-align:center;"><?php print($payment->payment_date); ?></td>
		 <td> <?php print($payment->description); ?></td>	
	      <td style="text-align:right;"><?php print number_format($payment->receipt_amount,2); ?></td>
	      <td style="text-align:center;"><?php echo $payment->status; ?></td>
	   	  <td style="text-align:left;"><?php echo $payment->stud_name == NULL? $payment->emp_name : $payment->stud_name; ?> </td>
	    </tr>
	    <?php
				}
			}else{ ?>
			 <tr> 
			    <td colspan="6">No payments yet</td>
			</tr>	
			<?php }   ?>
	  </table>
	  </div>
	  <div style="margin-left:20px; width:90%; margin-bottom:20px; ">
	     
	      <table width="18%" align="left" cellpadding="0" cellspacing="0" style="width:50%;">
			  <tr>
				  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">
				   <label class="control-label"> <b> Academic Year: </b> </label>
					<div class="controls">
						<select id="academic_year" name="academic_year">
						<?php
							foreach($academic_years AS $academic_year) {
								print("<option value=".$academic_year->id.">".$academic_year->start_year."-".$academic_year->end_year."</option>");	
							}
						?>
						</select>
					</div>
				   </td>
				   <td> 
				    <label class="control-label" for="level"><b> Level:</b></label>
				    <div class="controls">
				    <select id="levels" name="level" class="form">
						<option value="">-- Select Level --</option>
							<?php foreach ($levels as $level): ?>
								<option value="<?php echo $level->id; ?>"><?php echo $level->level; ?></option>
							<?php endforeach; ?>
						</select>
				   	</div>
				   </td>
				    <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
				   	<label class="control-label" for="level"><b> Grade/Year:</b></label>
				   	 <div class="controls" style=text-align:center>
				        <input type="text" name="year" class="year input-xlarge" style="width:20px; text-align:center">
				     </div>   
				   </td>
		 	 </tr>
		 	 <tr>
		 	     <div style="margin-left:25px;">
		 		 <td colspan = "2">
		 		 </div>
		 			 <button class="btn btn-primary" type="submit" style="margin-left:10px;" >Enroll Pupil/Student</button>
		 		</div>	 
		 		 </td>
		 		 
		 	 </tr>
		  </table>
 	 </div>
 	 
  
</form>
 </div>
 
 
 <script>
$(document).ready(function(){
	$('#promotion').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			amount: { required: true, minlength: 1 },
			type: {required: true},
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	$(".year").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});
</script>
 