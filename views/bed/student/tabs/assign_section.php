
	<div class="row-fluid" >
		<?php 
			if ($enrolled_status != 'Y') {
		?>
				<div>
					<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>Cannot be assigned section. Student not enrolled!</div>
				</div>

		<?php 
			} else {	
		?>
				<div id="msg_header_assign">
				
				</div>
				
				<div class="form-horizontal my_section_status">
					<fieldset>
							<div class="control-group">
								<label for="grade_level" class="control-label">Current</label>
								<div class="controls">
									<input type="text" class="form-control" value="<?php print($section_name); ?>" disabled />
								</div>
							</div>
							<div class="control-group">
								<label for="grade_level" class="control-label">Section</label>
								<div class="controls">
									<select id="section_id" name="section_id" class="form-control" >
										<?php 
											if ($sections) {
												foreach($sections AS $section) {
										?>
													<option value="<?php print($section->id); ?>"><?php print($section->description."-".$section->section_name); ?></option>
										<?php
												}
											}
										?>
									</select>
								</div>
							</div>
							
							
							<div class="control-group">
								<div class="controls" style="float:left; padding-right:10px;">
									<a href="#" class="btn btn-danger" name="click-status-button" id="click-status-button" 
										basic_ed_histories_id="<?php print($basic_ed_histories_id); ?>" 
										enrolled_status="<?php print($enrolled_status); ?>" 
										acad_program_groups_id="<?php print($acad_program_groups_id); ?>" 
										academic_years_id="<?php print($academic_years_id); ?>" >Assign Section!</a>
								</div>
								<div style="float:left; padding-right:8px; margin-top:5px;" id="show_click_icon">
								</div>		
							</div>
					</fieldset>
				</div>
		<?php 
			}
		?>
	</div>

	<script>
		$('#click-status-button').click(function(e){
			e.preventDefault();		
			
			var section_id 				= $("select#section_id option:selected").attr('value');
			var basic_ed_histories_id 	= $(this).attr('basic_ed_histories_id');
			var enrolled_status 		= $(this).attr('enrolled_status');
			var acad_program_groups_id 	= $(this).attr('acad_program_groups_id');
			var academic_years_id 		= $(this).attr('academic_years_id');
			
			var confirmed = confirm('Assign section to student?');

			if (confirmed){
			
				$('#show_click_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

				$.ajax({
					cache: false,
					type: 'POST',
					url: '<?php print(base_url($this->uri->segment(1)."/process_bed_student_action")."/".$students_idno);?>',
					data: { "section_id": section_id,
							"basic_ed_histories_id": basic_ed_histories_id,
							"enrolled_status": enrolled_status,
							"acad_program_groups_id": acad_program_groups_id,
							"academic_years_id": academic_years_id,							
							"action": 'assign_section',
						},
					dataType: 'json',
					success: function(response) {				

						$('.my_section_status').html(response.output); 	
	
						$('h4.media-heading').html(response.student_info);
						
						$('#show_click_icon').html("");
						
						if (response.success) {
							$('#msg_header_assign').show();	
							$('#msg_header_assign').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Successfully assigned section!</div>');
						}
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
					
				});
			}
		});

	</script>
