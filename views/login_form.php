<div class="login_box">
<form action="<?php echo site_url("{$role}/login"); ?>" method="post" id="login_form">
	<input type="hidden" name="role" value="<?php echo $role; ?>" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
<div class="top_b">Sign in</div>
		<?php if( isset($message) ):?>
		<div class="alert alert-<?php echo $severity; ?> alert-login">
			<button type="button" class="close" data-dismiss="alert">!</button>
				<?php echo $message; ?>
		</div>
		<?php endif; ?>
		<div class="cnt_b">
			<div class="formRow">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-user"></i></span><input type="text" id="username" name="username" placeholder="Username" />
				</div>
			</div>
			<div class="formRow">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-lock"></i></span><input type="password" id="password" name="password" placeholder="Password" />
				</div>
			</div>
		</div>
		<div class="btm_b clearfix">
			<button class="btn btn-inverse pull-right" type="submit">Sign In</button>
		</div>
	</form>
</div>
<script>
	$(document).ready(function(){
		/* boxes animation */
		form_wrapper = $('.login_box');
		function boxHeight() {
			form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);	
		};
		
		form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
		$('.linkform a,.link_reg a').on('click',function(e){
			var target	= $(this).attr('href'),
			target_height = $(target).actual('height');
			
			$(form_wrapper).css({
				'height'		: form_wrapper.height()
			});	
		$(form_wrapper.find('form:visible')).fadeOut(400,function(){
				form_wrapper.stop().animate({
                    height	 : target_height,
					marginTop: ( - (target_height/2) - 24)
                },500,function(){
                    $(target).fadeIn(400);
                    $('.links_btm .linkform').toggle();
		
					$(form_wrapper).css({
						'height'		: ''
					});	
				});
		});
		e.preventDefault();
		});
				
	/* validation */
	$('#login_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			username: { required: true, minlength: 3 },
			password: { required: true, minlength: 3 }
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
			setTimeout(function() {
				boxHeight()
			}, 200)
		},
		unhighlight: function(element) {
			$(element).closest('div').removeClass("f_error");
			setTimeout(function() {
				boxHeight()
			}, 200)
		},
		
		errorPlacement: function(error, element) {
			$(element).closest('div').append(error);
		}
	});
});
</script>
