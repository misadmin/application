<h2 class="heading">List of Submitted Admission Documents</h2>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">
		<a href="#" data-toggle="modal" data-target="#modal_add_requirement" 
				data-project-id='<?php print($priv_details); ?>'><i class="icon-plus"></i> Add Requirement</a>


      <div class="modal hide fade" id="modal_add_requirement" >
 		<form method="post" id="add_requirement">
   				<input type="hidden" name="action" value="add_requirement" />
   				
   				<?php $this->common->hidden_input_nonce(FALSE); ?>	
   		<div class="modal-header">
       		<button type="button" class="close" data-dismiss="modal">�</button>
       		<h3>Add Document Details</h3>
   		</div>
   		<div class="modal-body">
 			<div style="text-align:center;">
				<table border="0" cellpadding="2" cellspacing="0" 
					class="table table-striped table-condensed table-hover" style="width:100%;">
					<tr>
						<td>Requirement</td>
						<td>:</td>
						<td>
							<select id="admission_req" name="admission_req">
							<?php 
								foreach ($admission_req as $req): ?>
									<option value="<?php echo $req->id; ?>"> <?php echo $req->description; ?></option>
							<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Submission Date</td>
						<td>:</td>
						<td>
							<div class="input-append date" id="dp2" data-date-format="yyyy-mm-dd">
								<input type="text" name="date_submitted" id="date_submitted" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</td>
					</tr>
					
				</table>
			</div>
   		</div>
	   <div class="modal-footer">
    		<input type="submit" class="btn btn-primary" id="add_requirement" value="ADD REQUIREMENT!">
	   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
       </div>
		</form>
    </div>				
				
				
</div>

<?php
	if ($submitted_req) { ?>
		<div style="width:100%; margin-bottom:40px;">
	  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
	    <thead>
		    <tr>
		      <th width="35%" class="head" style="vertical-align:middle;">Document</th>
		      <th width="35%" class="head" style="vertical-align:middle;">Submission Date</th>
		      <th width="37%" class="head" style="vertical-align:middle;">Remove</th>
		    </tr>
		 </thead>

		<?php foreach ($submitted_req AS $submitted) {
			?>	

		    <tr>
		      <td style="text-align:left; vertical-align:middle; "><?php print($submitted->description); ?></td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($submitted->date_submitted); ?></td>
		      <td style="text-align:center; vertical-align:middle; ">
			 	<a class="delete_requirement" href="<?php print($submitted->id); ?>" ><i class="icon-trash"></i></td>
		      </tr>
    <?php } ?>
      </table>
      </div>
	<?php } else {
		
		print("No Submitted Requirement!");
	}	?>


<form id="delete_requirement2" action="" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" value="delete_requirement" name="action" />
</form>

<script>
	$('.delete_requirement').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to remove the requirement?');

		if (confirmed){
			var req_id = $(this).attr('href');
		
			$('<input>').attr({
				type: 'hidden',
				name: 'req_id',
				value: req_id,
			}).appendTo('#delete_requirement2');
			$('#delete_requirement2').submit();
		}
		
	});

	$(document).ready(function(){
		$('#dp2').datepicker();
	});
</script>



