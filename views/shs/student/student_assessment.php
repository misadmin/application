
			<div style="width:55%;">
				<?php 
					$data = array(
								'academic_terms'=>$academic_terms,
								'selected_term'=>$selected_term,
								'select_id'=>'assess',
								'change_select'=>'change_assessment');
					$this->load->view('shs/reports/pulldown_academic_terms',$data);
				?>
			</div>

			<div id="show_student_assessment_details">
				<?php 
				
					if(in_array($this->session->userdata('role'), $this->config->item('roles_allowed_to_print_assessment'))) {
						$data = array(
									'assess'=>$assess,
									'student_inclusive_terms' =>$student_inclusive_terms,
									'selected_history_id'=>$selected_history_id,
									'courses'=>$courses,
									'ledger_data'=>$ledger_data,
									'assessment'=> $shs_assessment,
									'current_academic_term_obj'=>$current_academic_term_obj,
									'student_details'=>$student_details,
									'invoice_data'=>$invoice_data,    //added Toyet 11.23.2017
									);				
						//log_message('INFO','assessment ====> '.print_r($shs_assessment,true));
						$this->load->view('shs/student/student_assessment_details_with_printing',$data);
					} else {
						$data = array(
									'assess'=>$assess,);				
						$this->load->view('shs/student/student_assessment_details',$data);
					}
					
				?>
			</div>

			
