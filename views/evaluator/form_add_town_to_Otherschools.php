<h2 class="heading" style="margin-top:60px;">Update School Address</h2>
<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<form id="addTown" method="post" action="" class="form-horizontal">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" name="step" value="2" />
			<div class="control-group formSep">
				<div class="control-label">School</div>
				<div class="controls">
					<select name="school_id">
					<option value="">-- Select School --</option>
					  <?php
					        foreach($schools_list AS $school) {
								print("<option value=".$school->id.">".$school->school."</option>");	
							}  ?> 
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Town</div>
				<div class="controls">
					<select name="town_id">
					<option value="">-- Select Town --</option>
						  <?php
								foreach($town_groups AS $town) {
									print("<option value=".$town->id.">".$town->name." - ".$town->province."</option>");	
								}  ?> 
					</select>				
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					 <button class="btn btn-success" type="submit">Update School Address!</button> 
				</div>
			</div>
		</form>
		</div>
	</div>
</div>


<script type="text/javascript">
		function hide(obj)
		  {
		      var obj1 = document.getElementById(obj);
		      obj1.style.display = 'none';
		  }

		function show(obj)
		  {
		      var obj1 = document.getElementById(obj);
		      obj1.style.display = 'block';
		  }
		
		function CheckValue(field) {
			if (field.value >= 101) {
				hide('end_date');
			} else {
				show('end_date');
			}
		}
		
</script>


<script>
	$(document).ready(function(){
		$('#dp2').datepicker();
	});
	
	$(document).ready(function(){
		$('#dp3').datepicker();
	});
	
	$(document).ready(function(){
		$('#new_withdrawal_sked').validate({
			onkeyup: false,
			errorClass: 'error',
			validClass: 'valid',
			rules: {
				amount: { required: true, minlength: 1 },
				type: {required: true},
			},
			highlight: function(element) {
				$(element).closest('div').addClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						unhighlight: function(element) {
							$(element).closest('div').removeClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						errorPlacement: function(error, element) {
							$(element).closest('div').append(error);
						}
		});
	$(".money").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});


	$('#year').change(function(){
		var year_id = $('#year option:selected').val();
		var level_id = $('#levels option:selected').val();

		
		$.ajax({
			url: "<?php echo site_url("ajax/sections"); ?>/?year=" + year_id + "&level=" + level_id ,
			dataType: "json",
			success: function(data){
				var doptions = make_options_section(data);
				$('#section').html(doptions);
			}	
		});	
	});



	$('#levels').change(function(){
		var level_id = $('#levels option:selected').val();
		
		$.ajax({
			url: "<?php echo site_url("ajax/year"); ?>/?level=" + level_id,
			dataType: "json",
			success: function(data){
				var doptions = make_options_year(data);
				$('#year').html(doptions);
			}	
		});	
	});


function make_options_year (data){
	var doptions = '<option value="">-- Select Year --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].yr_level
		 	+ '">'
		 	+ data[i].yr_level
		 	+ '</option>';
	}
	return doptions;
}

function make_options_section (data){
	var doptions = '<option value="">-- Select Section --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].section
		 	+ '</option>';
	}
	return doptions;
}

</script>






  