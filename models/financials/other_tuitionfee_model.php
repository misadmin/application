<?php
//Added: 2/18/2013
	class Other_TuitionFee_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


    function ListOtherTuitionFeeItems() {
		$result = null;
			
		$q1 = "SELECT 
				  a.id AS courses_groups_id, 
				  a.group_name,
				  b.id AS other_tuition_fee_item_id 
			  FROM 
			      courses_groups as a, 
				  other_tuition_fee_items as b 
			  WHERE 
			      a.id = b.courses_groups_id	
			  ORDER BY
			      a.group_name";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}			
	
			
	function AddOtherTution_fee($data){
		$query = "INSERT INTO other_tuition_fees (id, other_tuition_fee_items_id, academic_years_id, rate, yr_level) 
   					      VALUES ('',
								  {$this->db->escape($data['other_tuition_fee_item_id'])},
								  {$this->db->escape($data['academic_years_id'])},
								  {$this->db->escape($data['rate'])},
								  {$this->db->escape($data['yr_level'])})";
		$this->db->query($query);					
 		$id = @mysql_insert_id();
		return $id;
   }
   
   //Added: 2/20/2013
   
   function ListOtherFeesItems() {
		$result = null;
				
			$q1 = "SELECT
					id,
					description
				   FROM
					  other_fee_items
				   ORDER BY 
					  description";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}			

}

//end of college_model.php