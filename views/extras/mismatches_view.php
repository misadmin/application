<style>
	table.balances 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
</style>


<div>
<div><br></div>
<div class="fluid span6"> 
<h3>List of ISIS & MIS balances</h3>


<table id="balances_t" class="table table-bordered table-striped balances">
<thead>
	<tr>
	<th width="10%">IDNO</th>
	<th width="20%">IDNO</th>
	<th width="20%">ISIS</th>
	<th width="20%">MIS</th>
	<th width="30%">Remark</th>
	</tr>
</thead>

<?php
	//echo "here";die(); 
	$row = 0;
	foreach($balances as $balance){
		$row++;
?>
		<tr >
		<td class="center"><?php echo $row .'.'; ?> </td>
		<td class="center"><?php echo $balance['stud_id']; ?></td>
		<td class="right"><?php echo number_format($balance['isis_balance'],2); ?></td>
		<td class="right"><?php echo number_format($balance['mis_balance'],2); ?></td>
		<td><?php echo $balance['remark']; ?></td>
		</tr>		

<?php 		
	}
?>




</table>
</div>
</div>

<script>
	$(document).ready(function(){
		$("#balances_t").stickyTableHeaders();
	    $("#balances_t").stickyTableHeaders('updateWidth');
	});
</script>	
