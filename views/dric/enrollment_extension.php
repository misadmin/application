<div class="row-fluid span12">
<?php if(isset($message) && $tab=='enrollment_extension'): ?>
	<div class="alert alert-<?php echo $severity; ?>">
		<a class="close" data-dismiss="alert">&times;</a>
		<?php echo $message; ?>
	</div>
<?php endif; ?>
	<form method="post" id="extend_enrollment_form">
	<input type="hidden" name="action" value="extend_enrollment_period" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
		<fieldset>
			<div class="controls">
				<button id="extend_enrollment" type="button" class="btn btn-primary btn-large">Extend Enrollment Schedule</button>
			</div>
		</fieldset>
	</form>
</div>
<script>
$('#extend_enrollment').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm ('You are about to extend for one day the enrollment period of this student. Click Ok to proceed, else click Cancel.');

	if (confirmed){
		$('#extend_enrollment_form').submit();	
	}
});
</script>