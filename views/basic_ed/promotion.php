 <?php if (isset($message) && $tab=='7') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:5px; ">
 <form method="post" id="promotion">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "promote_old_student"/>
	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" > 
	<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
	   <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    <strong><font face="Verdana, Arial, Helvetica, sans-serif"> PROMOTION</font></strong>
		   </div>
		   </td>
 	 </tr>
	  <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">
		    <font face="Verdana, Arial, Helvetica, sans-serif"><b> Student Payments </b></font>
		   </td>
 	 </tr>
	</table>	

		  
	<div style="margin-bottom:20px; ">
	  <table width="75%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:90%; padding:0px; margin:10px;">
	    <thead>
	      <tr>
	         <th width="8%" style="text-align:center;">OR No.</th>
			 <th width="12%" style="text-align:center;">Payment Date</th>
			 <th width="25%" style="text-align:center;">Particulars</th>
	         <th width="10%" style="text-align:center;">Amount Paid</th>
	         <th width="10%" style="text-align:center;">Status</th>
	       	 <th width="25%" style="text-align:center;">Teller</th>         
		  </tr>
	    </thead>
	    <?php
			if ($payments_info) {
				foreach ($payments_info AS $payment) {
		?>
	    <tr>
	      <td style="text-align:left;"><?php print($payment->receipt_no); ?></td>
		  <td style="text-align:center;"><?php print($payment->payment_date); ?></td>
		 <td> <?php print($payment->description); ?></td>	
	      <td style="text-align:right;"><?php print number_format($payment->receipt_amount,2); ?></td>
	      <td style="text-align:center;"><?php echo $payment->status; ?></td>
	   	  <td style="text-align:left;"><?php echo $payment->stud_name == NULL? $payment->emp_name : $payment->stud_name; ?> </td>
	    </tr>
	    <?php
				}
			}else{ ?>
			 <tr> 
			    <td colspan="6">No payments yet</td>
			</tr>	
			<?php }   ?>
	  </table>
	  </div>
	  <div style="margin-left:20px; width:90%; margin-bottom:20px; ">
	     
	      <table width="18%" align="left" cellpadding="0" cellspacing="0" style="width:50%;">
			  <tr>
				  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">
				   <label class="control-label"> <b> Academic Year: </b> </label>
					<div class="controls">
						<select id="academic_year" name="academic_year">
						<?php
							foreach($academic_years AS $academic_year) {
								print("<option value=".$academic_year->id.">".$academic_year->start_year."-".$academic_year->end_year."</option>");	
							}
						?>
						</select>
					</div>
				   </td>
				   <td> 
				    <label class="control-label" for="level"><b> Level:</b></label>
				    <div class="controls">
				    <select id="levels" name="level" class="form">
						<option value="">-- Select Level --</option>
							<?php foreach ($levels as $level): ?>
								<option value="<?php echo $level->id; ?>"><?php echo $level->level; ?></option>
							<?php endforeach; ?>
						</select>
				   	</div>
				   </td>
		 	 </tr>
		 	 <tr>
		 	     <div style="margin-left:25px;">
		 		 <td colspan = "2">
		 		 </div>
		 			 <button class="btn btn-primary" type="submit" style="margin-left:10px;" >Enroll Pupil/Student</button>
		 		</div>	 
		 		 </td>
		 		 
		 	 </tr>
		  </table>
 	 </div>
 	 
  
</form>
 </div>