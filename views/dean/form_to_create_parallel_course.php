<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<body onLoad="document.parallel.capacity.focus()">

 <div style="background:#FFF; width:40%; text-align:center; margin:auto;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head" style="text-align:left; padding:10px;">
  CREATE PARALLEL OFFERING </td>
  </tr>
  <tr>
    <td colspan="3">
<form action="<?php echo site_url('dean/offerings');?>" method="post" name="parallel">
<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="9" />
  
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
    <tr>
      <td align="left" valign="top">Academic Terms</td>
      <td align="left" valign="top">:</td>
      <td align="left"><?php print($academic_terms->term." ".$academic_terms->sy); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top">Course [Section]</td>
      <td align="left" valign="top">:</td>
      <td align="left"><?php print($offerings->course_code."[".$offerings->section_code."]"); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top">Schedule</td>
      <td align="left" valign="top">:</td>
      <td align="left"><?php
  		foreach($offering_slot[0] AS $slot) {
			print($slot->tym." ".$slot->days_day_code." [".$slot->room_no."]");
			print("<br>");
		}
  
  ?></td>
    </tr>
<tr>
  <td align="left">Courses</td>
  <td align="left">:</td>
  <td align="left"><select name="courses_id" id="courses_id" class="form1">
    <?php
				foreach($courses AS $course) {
					print("<option value=".$course->id.">".$course->course_code."</option>");	
				}
			?>
  </select></td>
</tr>
<tr>
  <td width="118" align="left">&nbsp;</td>
  <td width="6" align="left">&nbsp;</td>
  <td width="372" align="left"><input type="submit" name="button" id="button" value="Create Parallel Offering!"  class="btn btn-success" /></td>
</tr>
    </table>
</form>
    
    </td>
  </tr>
    </table>
    </div>

</body>
