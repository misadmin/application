<style>
	.table 
	th {text-align:center; vertical-align:middle;}
	td.right {text-align:right}
	td.center {text-align:center}
	tr.trhide {display:none}
	.green  {font-color:green}
</style>

<div class="span8 row-fluid"">
<div class="heading">
<h2>A/R Transactions Summary <h3>For Period: <?php echo  (isset($start_date) AND isset($end_date)) ? $start_date.' to '.$end_date : " "; ?></h3></h2>
	<div id="ar_balance_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="arBalanceModal" aria-hidden="true"
		style="width:850px; left:350px;">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    <h3 id="ar_balance_modal_label">Summary</h3>
		  </div>
		  <div class="modal-body">
			<table id="ar-table" class="table table-condensed table-bordered table-hovered table-striped">
				<thead>
					<th colspan=3>Code</th>
					<th>-----  S  U  M  M  A  R  Y -----<br/>Description</th>
					<th>Students</th>
					<th>Debit</th>
					<th>Credit</th>
					<th>Reference</th>
				</thead>
				<tbody id="ar_balance_modal_content">
					    
				</tbody>
			</table>
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  </div>
	</div>

	<table id="ar-table" class="table table-condensed table-bordered table-hovered table-striped">
		<thead>
			<th class="center">Prog Group</th>
			<th class="center"> * </th>
			<th class="center">Description</th>
			<th class="center">Students</th>
			<th class="center">Debit</th>
			<th class="center">Credit</th>
		</thead>	

<?php
	$levels_id  = "";
	$group_name = "";
	$year_level = "";
	$total_for_group_name_credit = 0;
	$total_for_group_name_debit = 0;	
	$iteration = 0;
	$row_num = 0;

	//log_message('INFO',print_r($data, true));  //toyet 2017.10.19

	foreach ($data as $row){
		
		$iteration++;
		$row_num++;
		
		echo $row->group_name=='Sample' ? '<tr class="trhide">' : '<tr>' ;
		
		if ($levels_id != $row->levels_id) {
			//die($row->levels_id);
			$row_num = 1;
			echo "<td class ='center' colspan=7><h3><br/>" . $levels_array[$row->levels_id] . "</h3></tr>" ; 
			echo $row->group_name == 'Sample' ? '<tr class="trhide">' : '<tr>' ;
		}
		
		$total_for_group_name_credit += $row->credit;
		$total_for_group_name_debit += $row->debit;		
?>

		<td width="5%" class="center"><?php echo $row_num .'.'; ?>
		<td width="5%"><code><?php echo $row->trangroup; ?></code>
		<td width="32%"><?php echo $row->description; ?>
		<td width="8%" class="center" >
			<a target="_blank" class="students-list btn btn-link"
				data-levels_id   = "<?php echo $row->levels_id; ?>"			 
				data-group_name  = "<?php echo $row->group_name; ?>"			 
				data-year_level  = "<?php echo $row->year_level; ?>"	
				data-trangroup   = "<?php echo $row->trangroup; ?>" 
				data-deskription = "<?php echo $row->description; ?>"
				data-tran_code   = "<?php echo $row->code; ?>" 
				data-tran_type   = "<?php echo $row->debit > 0 ? 'debit' : 'credit'; ?>"	
			 	><?php echo $row->stud; ?></a>
		<td width="12%" class="right"><?php echo number_format($row->debit,2); ?>
		<td width="12%" class="right"><?php echo number_format($row->credit,2); ?>

<?php
		$levels_id  = $row->levels_id;
		$group_name = $row->group_name;
		$year_level = $row->year_level;
} //end of foreach
?>

	</table>
</div>
</div>

<script>
	$(document).ready(function(){
		$("#ar-table").stickyTableHeaders();
		$("#ar-table").stickyTableHeaders('updateWidth');
		
		$('.students-list').bind('click', function(){
			show_loading_msg();
			$.post('<?php echo site_url('ajax/ar_details/') ?>',
				{
					'levels_id'  : $(this).data('levels_id'),
					'group_name' : $(this).data('group_name'),
					'year_level' : $(this).data('year_level'),
					'tran_code'	 : $(this).data('tran_code'),
					'tran_type'	 : $(this).data('tran_type'),
					'trangroup'	 : $(this).data('trangroup'),
					'deskription': $(this).data('deskription'),
					'start_date' : <?php echo $this->db->escape($start_date); ?>,
					'end_date'   : <?php echo $this->db->escape($end_date); ?>,
					'summary'    : <?php echo 'true'; ?>,
				},
			  function(data){
			    if (data.type == 'Success'){
				    hide_loading_msg();
				    //$('#ar_balance_modal_label').text(data.heading);
				    $('#ar_balance_modal_label').text(data.heading);
				    $('#ar_balance_modal_tran_type').text(data.tran_type);
					var str = '';
				    $.each(data.data, function (i,item){
						str += '<tr><td class="center">'+(i+1)+'</td><td class="center"><a href="student/'+item.idno+'" target="_blank">'
						+item.idno+'</a></td><td>'+item.student+'</td><td class="left">'+item.course_year+'</td><td class="center">'
						+item.transaction_date+'</td>';

						if (item.type=='Debit'){
							str += '<td class="right">'+item.amount+'</td>';
						} else {
							str += '<td></td>';
						}
						if (item.type=='Credit'){
							str += '<td class="right">'+item.amount+'</td>';
						} else {
							str += '<td></td>';
						}

						str += '<td class="center">'+item.ref+'</td></tr>';
					});
					$('#ar_balance_modal_content').html(str);
					$('#ar_balance_modal').modal('show');
			    }
			    else {
			    	hide_loading_msg();
			    	show_sticky_msg(data.type,data.message);
			    }
			  }
			,"json");
		});
		
	});
</script>