<?php 
	$n = 1;
	if($prospectus): 
?>

<div style="background-color:#FFF; padding-top:20px; margin-top:20px;">

  <table class="table table-striped table-condensed" style="width:60%; border:solid; border-width:1px; border-color:#FF80FF; color:#006;" align="center">
    <thead>
<tr>
<th width="6%" style="text-align:center;">No.</th>
<th width="13%" style="text-align:center;">Year Level</th>
<th width="9%" style="text-align:center;">Term</th>
<th width="15%" style="text-align:center;">Course Code</th>
<th width="37%" style="text-align:center;">Descriptive Title</th>
<th width="9%" style="text-align:center;">Units</th>
<th width="11%" style="text-align:center;">Grade</th>
</tr>
</thead>

<?php foreach($prospectus as $p): ?>
<?php
	$grade=array();
	$grade=$this->Col_Students_Model->getMyCourseCodeGrade($this->session->userdata('students_idno'),$p->course_code); 
	/*if ($grade) {
		print("<tr style=\"background-color:#FFF; color:#063;\">");
	} else {
		print("<tr style=\"background-color:#C00; color:#FFF;\">");	
	}*/
?>
<tr>
<td style="text-align:center;"><?php print($n); ?></td>
<td style="text-align:center;"><?php print($p->year_level); ?></td>
<td style="text-align:center;"><?php print($p->terms); ?></td>
<td><?php print($p->course_code); ?></td>
<td><?php print($p->descriptive_title); ?></td>
<td style="text-align:center;"><?php print($p->credit_units); ?></td>
<?php
	if ($grade) {
		foreach($grade AS $k) {
			print("<td style=\"text-align:center;\">$k->finals_grade</td>");
			break; //it SHOULD NOT break consider several grades on a subject
		}
	} else {
		print("<td style=\"text-align:center;\">Not taken!</td>");
	}
?>
</tr>
<?php 
	$n++;
	endforeach; ?>
</table>
</div>
<?php endif;  ?>
