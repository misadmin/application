

	<h2 class="heading">Senior High School - Class Schedules</h2>

	<div style="width:100%;" id="main_view_class_schedules" >	
		<div style="width:100%; border-radius:5px; background-color:#EAEAEA; border:solid 1px #D8D8D8; height:35px;">
				<div style="float:right;" >
					<div id="show_change_icon" style="float:left; padding-right:8px; margin-top:6px;">
							
					</div>
					<div style="float:left; padding-top:4px; margin-right:10px;" >
						<select name="term_id" class="form-control change_select" style="width:auto; height:auto;" id="select2">
							<?php 
								if ($academic_terms) {
									foreach ($academic_terms AS $term) {
							?>		
										<option value="<?php print($term->id); ?>" 
											term_text="<?php print($term->term." ".$term->sy); ?>"
											<?php if ($selected_term == $term->id) print("selected"); ?>>
											<?php print($term->term." ".$term->sy); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
		</div>

		<div id="display_schedules">
			<?php 
				$data = array(
							"class_schedules"=>$class_schedules,
							"teacher"=>$teacher);
				
				$this->load->view('shs/teacher/shs_class_schedules_details',$data);
			?>
		</div>
	</div>

	

<form id="download_classlist_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_classlist_pdf" />
</form>

<script>
	$('.download_classlist_pdf').click(function(event){
		event.preventDefault(); 
		var section_id    = $(this).attr('section_id');
		var teacher       = $(this).attr('teacher');
		var strand        = $(this).attr('strand');
		var class_adviser = $(this).attr('class_adviser');
		var room_no       = $(this).attr('room_no');
		var descriptive_title = $(this).attr('descriptive_title');

		var element2  = $("option:selected", "#select2");
		var term_sy   = element2.attr('term_text');
		
		
		$('<input>').attr({
			type: 'hidden',
			name: 'section_id',
			value: section_id,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'teacher',
			value: teacher,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'strand',
			value: strand,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'class_adviser',
			value: class_adviser,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'room_no',
			value: room_no,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'term_sy',
			value: term_sy,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'descriptive_title',
			value: descriptive_title,
		}).appendTo('#download_classlist_form');

		$('#download_classlist_form').submit();
		
	});
</script>
					
					
<script>
	$(document).ready(function(){
		$('.change_select').bind('change', function(){
			
			var element2    = $("option:selected", "#select2");
			var term_id     = element2.val();

			$('#show_change_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/shs_class_schedules'); ?>",
				data: {	"term_id": term_id,
						"action": "change_select" },
				dataType: 'json',
				success: function(response) {													

					$('#display_schedules').html(response.output); 

					$('#show_change_icon').html("")
										
				}
			});

		});
	});
</script> 

					
					