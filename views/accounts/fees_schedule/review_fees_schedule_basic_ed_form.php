<h2 class="heading">Fees Schedule - Basic Education</h2>

<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<input type="hidden" name="step" value="2">
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						print($academic_year);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Fees Category</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						print($fees_subgroup->fees_group);					
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Item</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						print($fees_subgroup->description);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Level</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						print($basic_level->level);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Grade Level</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						foreach($grade_levels AS $k => $v) {
							print($v."<br>");
						}
						
						$grade_levels = json_encode($grade_levels);
						?>										
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label" >Amount</div>
				<div class="controls" style="font-weight:bold;">
					<?php print(number_format($rate,2));?>			
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<button class="btn btn-danger" id="back_to_fees" ><< Back to Fees Schedule</button>
					<button class="btn btn-warning" id="save_fees" fees_subgroups_id="<?php print($fees_subgroup->id); ?>" 
						academic_terms_id="<?php print($term); ?>"
						levels_id="<?php print($basic_level->id); ?>" grade_levels='<?php print($grade_levels); ?>'
						rate="<?php print($rate); ?>">Save Fees Schedule >></button>
				</div>
			</div>
			</div>				
	</div>
</div>

 <form id="back_to_fees_sched_form" method="post">
  <input type="hidden" name="step" value="1" />
</form>

<script>
	$('#back_to_fees').click(function(event){
		event.preventDefault(); 
		
		$('#back_to_fees_sched_form').submit();
	});
</script>


 <form id="save_fees_sched_form" method="post">
  <input type="hidden" name="step" value="3" />
	<?php $this->common->hidden_input_nonce(FALSE); ?> 
</form>

<script>
	$('#save_fees').click(function(event){
		event.preventDefault(); 
		var fees_subgroups_id = $(this).attr('fees_subgroups_id');
		var levels_id = $(this).attr('levels_id');
		var grade_levels = $(this).attr('grade_levels');
		var academic_terms_id = $(this).attr('academic_terms_id');
		var rate = $(this).attr('rate');
		
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'fees_subgroups_id',
		   	value: fees_subgroups_id,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'levels_id',
		   	value: levels_id,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'grade_levels',
		   	value: grade_levels,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'academic_terms_id',
		   	value: academic_terms_id,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'rate',
		   	value: rate,
		}).appendTo('#save_fees_sched_form');
		$('#save_fees_sched_form').submit();
	});
</script>



