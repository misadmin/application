<?php 
class random_ids_model extends MY_Model {

	/**
	 * Queries randomly 30 ID numbers in a given college.
	 * @author   Toyet Amores
	 * @param    
	 * $college   - name of the college extract the IDs from
	 * @return   
	 * array of 30 ID Numbers that are enrolled in the current 
	 * term
	*/
	function fetch_30ids($college="CS") {

		$sql = "SELECT 
				    c.college_code as code,
				    c.name as name,
				    s.idno AS id,
				    concat(s.lname,' ',s.fname) as student_name
				FROM
					students s 
						LEFT JOIN
				    student_histories sh ON sh.students_idno = s.idno
				        LEFT JOIN
				    academic_terms act ON act.id = sh.academic_terms_id
						LEFT JOIN
					prospectus p on p.id=sh.prospectus_id
						LEFT JOIN
					academic_programs ap on ap.id=p.academic_programs_id
						LEFT JOIN
					colleges c on c.id=ap.colleges_id
				WHERE
				    act.id = 467 and c.college_code='{$college}'
				ORDER BY ceil(rand()*6780000) limit 30";

		//log_message('info',print_r($sql,TRUE));  //toyet 9.20.2017

		$query = $this->db->query($sql);	
		return ($query ? $query->result() : FALSE);
	}

	/*
	 * @author: Toyet Amores
	 * @date: 2/7/2018
	 * $description: for excel reports of random id's
	 * 
	*/
	public function CreateRandomIDsforExcel($data,$filename,$datestamp) {

			$this->load->library('PHPExcel');

			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setTitle("Random IDs Generator: ");
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
			->setSize(14);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);

			$objPHPExcel->getActiveSheet()->SetCellValue('A5', 'No.');
			$objPHPExcel->getActiveSheet()->SetCellValue('B5', 'COLLEGE');
			$objPHPExcel->getActiveSheet()->SetCellValue('C5', 'STUDENT ID');
			$objPHPExcel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C5')->getFont()->setBold(true);

			$objPHPExcel->getActiveSheet()->getStyle('A5')
							                ->getAlignment()
							                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('B5')
							                ->getAlignment()
							                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('C5')
							                ->getAlignment()
							                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->SetCellValue('A1','Randomized 30 Student IDs by College');
			$objPHPExcel->getActiveSheet()->SetCellValue('A2','List generated: '.$datestamp);
			$objPHPExcel->getActiveSheet()->SetCellValue('A3','Note: IDs will be different for each run of this list');	
				$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(false)->setSize(10);

			$num = 5;
			$temp = 'College of Arts And Sciences';
			if ($data) {
				$objPHPExcel->getActiveSheet()->getStyle('A'.$num)->getFont()->setBold(true)->setSize(12);
				$objPHPExcel->getActiveSheet()->freezePane('B6');

				$cnt=1;
				$num++;
				foreach($data as $row) {

					if($temp<>$row->name){
						$cnt = 1;
						$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, ' ');
						$objPHPExcel->getActiveSheet()->SetCellValue('B'.$num, ' ');
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, ' ');
						$num++;
					}
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, $cnt.'. ');
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$num, $row->name);
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, $row->id);
						$objPHPExcel->getActiveSheet()->getStyle('C'.$num)
										                ->getAlignment()
										                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$num, '____________________________________________');
	
					$num++;
					$cnt++;
					$temp = $row->name;
				}

			}

			$objWriter->save($filename,__FILE__);
			return;
		}


}



