<?php

/**
 * ex:
 * <pre>
 * $arr = array(1, 2, 3, 4, 5, 9, 13, 14);
 * $ranges = range_of_arrays($arr);
 * </pre>
 * 
 * @param array $arr
 * @return multitype:string number unknown
 */
function range_of_arrays($arr){
	$arr = array_unique($arr);
	asort($arr);
	$previous = (min($arr) - 2);
	$ranges = array();
	$range_count = 0;
	$count=0;
	foreach($arr as $key=>$val){
		//echo $key;
		if(($val - 1) != $previous){
			$ranges[$range_count] = $val . '-';
			if(isset($ranges[($range_count - 1)]) && substr($ranges[($range_count - 1)], -1, 1)=='-')
				$ranges[($range_count - 1)] .= $previous;
			$range_count++;
		}
		if($count == (count($arr) - 1))
			$ranges[$range_count-1] .= $val; 
		$previous = $val;
		//print_r($ranges);
		$count++;
	}
	//die();
	return $ranges;
}