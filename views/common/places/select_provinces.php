
		<select name="province_id" id="province_id" class="form-control" style="width:auto; " >
			<option value="">--Please Select--</option>
		<?php
			foreach ($provinces AS $province) {
				print("<option value=".$province->id.">".$province->name."</option>");
			}
		?>
		</select>

		

<script>
	$(document).ready(function(){

		$("select#province_id").change(function(){

			var province_id = $("select#province_id option:selected").attr('value'); 
			var type        = '<?php print($type); ?>';

				$("#brgy_group").hide();
				$("#town_group").hide();
				$('#register_student').prop("disabled", true);
			
				if (province_id.length > 0 ) { 
						
					$('#town_group').show();
							
					$.ajax({
						type: "POST",
						url: "<?php echo site_url($this->uri->segment(1).'/extract_places');?>",
						data: {
								"action": 'list_towns',
								"type": type,
								"province_id":province_id
								},
						cache: false,
						beforeSend: function () { 
							$('#town_group').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");
						},
						success: function(html) {    
							$("#town_group").html( html );
						},
						error: function (request, status, error) {
							alert(request.responseText);
						}
						
					});
				}
		});
	});
	
</script>
		