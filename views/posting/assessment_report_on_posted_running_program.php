
<style>
	.myhead {
		background-color: #EAEAEA; 
	}
	
	td.head1 {
		text-align:center;
		vertical-align:middle;
		font-weight:bold;
	}

</style>

<h2 class="heading">Program Assessment Summary Report</h2>

	<div style="width:100%;">

		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 35px; padding: 2px; margin-bottom:20px;">
			<div style="float:right; padding-top:8px; margin-right:10px;" >
				<a href="#" style="text-decoration: none; color:#4E4D4D; font-size:12px; "
					college_name="<?php print($college_name); ?>"
					abbreviation="<?php print($abbreviation); ?>"
					description="<?php print($description); ?>"
					term_sy="<?php print($term_sy); ?>"
					max_yr_level="<?php print($max_yr_level); ?>"
					reports='<?php print(json_encode($reports)); ?>'
					class="download_excel_report" >
					<div style="float:right; padding-left:3px; padding-top:2px;">Download to Excel</div>
					<div id="excel_labels" style="float:right;">
						<img src="<?php print(base_url('assets/images/excel_icon.png')); ?>" style="height:20px;" /> 
					</div>
				</a>
			</div>
		</div>
		
		<div style="width:100%; margin:0px auto; text-align:center; font-size:20px; font-weight:bold; color:#022E7A;" >
			<?php print($college_name); ?>
		</div>
		<div style="width:100%; margin:0px auto; text-align:center; font-size:16px; font-weight:bold; font-style:italic; color:#0A5901; padding-top:5px;" >
			<?php print($term_sy); ?>
		</div>
		<div style="width:100%; margin:0px auto; text-align:center; font-size:16px; font-weight:bold; color:#0A5901; padding-top:5px;" >
			<?php print($description); ?>
		</div>
				
		<div style="width:100%; margin-top:15px;">
			<table class="table table-bordered" style="width:100%;">
				<thead>
					<tr class="myhead">
						<td rowspan="2" class="head1">Year<br>Level</td>
						<td colspan="2" class="head1" style="letter-spacing: 8px;font-size: 16px;color: #880528;">POSTED</td>
						<td colspan="13" class="head1" style="letter-spacing: 8px;font-size: 16px;color: #880528;">RUNNING BALANCE</td>
					</tr>
					<tr class="myhead">
						<td class="head1"># of<br>Students</td>
						<td class="head1">TOTAL<br>AMOUNT</td>
						<td class="head1"># of<br>Students</td>
						<td class="head1">TF</td>
						<td class="head1">REED</td>
						<td class="head1">CWTS/MS</td>
						<td class="head1">MAT</td>
						<td class="head1">MISC</td>
						<td class="head1">OSF</td>
						<td class="head1">AOF</td>
						<td class="head1">LRF</td>
						<td class="head1">SSS</td>
						<td class="head1">AF</td>
						<td class="head1">Lab.</td>
						<td class="head1">TOTAL</td>
					</tr>				
				</thead>
				
				<tbody>
					<?php 
						if ($reports) {

							$total_running = 0;

							foreach($reports AS $k=>$v) {
								foreach($v->programs AS $program) {
									$total_running = $program->affil_fees + $program->tuition_fee + $program->reed_fee + $program->cwts_fee + $program->lab_fee + ($program->num_running_students*($program->mat_rate + $program->misc_rate + $program->osf_rate + $program->aof_rate + $program->lrf_rate + $program->sss_rate));
					?>
									<tr>
										<td style="text-align:center;"><?php print($program->year_level); ?></td>
										<td style="text-align:center;">
											<a href="#" class="posted_students"
												year_level="<?php print($program->year_level); ?>" 
												academic_terms_id="<?php print($academic_terms_id); ?>"
												academic_programs_id="<?php print($acad_programs_id); ?>" > 
												<?php print($program->num_posted_students); ?>
											</a>
										</td>
										<td style="text-align:right;"><?php print(number_format($program->total_posted,2)); ?></td>
										<td style="text-align:center;">
											<a href="#" class="running_students"
												year_level="<?php print($program->year_level); ?>" 
												academic_terms_id="<?php print($academic_terms_id); ?>"
												academic_programs_id="<?php print($acad_programs_id); ?>" > 
												<?php print($program->num_running_students); ?>
											</a>	
										</td>
										<td style="text-align:right;"><?php print(number_format($program->tuition_fee,2)); ?></td>
										<td style="text-align:right;"><?php print(number_format($program->reed_fee,2)); ?></td>
										<td style="text-align:right;"><?php print(number_format($program->cwts_fee,2)); ?></td>
										<td style="text-align:right;"><?php print(number_format(($program->num_running_students*$program->mat_rate),2)); ?></td>
										<td style="text-align:right;"><?php print(number_format(($program->num_running_students*$program->misc_rate),2)); ?></td>
										<td style="text-align:right;"><?php print(number_format(($program->num_running_students*$program->osf_rate),2)); ?></td>
										<td style="text-align:right;"><?php print(number_format(($program->num_running_students*$program->aof_rate),2)); ?></td>
										<td style="text-align:right;"><?php print(number_format(($program->num_running_students*$program->lrf_rate),2)); ?></td>
										<td style="text-align:right;"><?php print(number_format(($program->num_running_students*$program->sss_rate),2)); ?></td>
										<td style="text-align:right;"><?php print(number_format($program->affil_fees,2)); ?></td>
										<td style="text-align:right;"><?php print(number_format($program->lab_fee,2)); ?></td>
										<td style="text-align:right;"><?php print(number_format($total_running,2)); ?></td>
									</tr>
					<?php 
								}
							}
						}
					?>
				</tbody>
				
			
			</table>
		</div>	
		
	</div>	
	

<script>
	$('.download_excel_report').click(function(event){
		event.preventDefault(); 
		
		var college_name = $(this).attr('college_name');
		var abbreviation = $(this).attr('abbreviation');
		var description  = $(this).attr('description');
		var term_sy      = $(this).attr('term_sy');
		var max_yr_level = $(this).attr('max_yr_level');
		var reports      = $(this).attr('reports');

		
		$('#excel_labels').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/posted_running_program'); ?>",
			data: { 
					"college_name": college_name,
					"abbreviation": abbreviation,
					"description": description,
					"term_sy": term_sy,
					"max_yr_level": max_yr_level,
					"reports": reports,
					"action": 'create_posted_running_excel'},
			success: function(response) {
				window.location.href = '<?php print(base_url("downloads/assessment_".$abbreviation.".xls")); ?>';

				$('#excel_labels').html("<img src='<?php print(base_url('assets/images/excel_icon.png')); ?>' style='height:20px;'> ")
			}
	
		});
			
	});
		
</script>	


<script>
	$('.posted_students').click(function(event){
					
		var year_level           = $(this).attr('year_level');
		var academic_terms_id    = $(this).attr('academic_terms_id');
		var academic_programs_id = $(this).attr('academic_programs_id');

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/posted_running_program'); ?>",
			data: {	"year_level": year_level,
					"academic_terms_id": academic_terms_id,
					"academic_programs_id": academic_programs_id,
					"action": "extract_posted_students" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_list_of_enrolled_students').html(response.students); 
				
			}
		});

		$('#modal_list_students').modal('show');
		
	});
</script>		


<script>
	$('.running_students').click(function(event){
					
		var year_level           = $(this).attr('year_level');
		var academic_terms_id    = $(this).attr('academic_terms_id');
		var academic_programs_id = $(this).attr('academic_programs_id');

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/posted_running_program'); ?>",
			data: {	"year_level": year_level,
					"academic_terms_id": academic_terms_id,
					"academic_programs_id": academic_programs_id,
					"action": "extract_running_students" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_list_of_enrolled_students').html(response.students); 
				
			}
		});

		$('#modal_list_students').modal('show');
		
	});
</script>		


	<div id="modal_list_students" class="modal hide fade" style="width:750px; margin-left: -375px;">
		<div class="modal-dialog" >
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" style="text-align:left;">Students</h3>
				</div>
				<div class="modal-body" style="text-align:left; font-size:15px;">
					<div id="show_list_of_enrolled_students"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
				</div>
			</div>
		</div>
	</div>

	