

	<div style="width:100%;">
	
		<table class="table table-condensed table-bordered" style="table-layout:fixed;">
			<tr style="background-color:#C3C3C3; font-weight:bold; height:40px;">
				<td colspan="<?php print(count($school_days)+2); ?>" style="text-align:center; vertical-align:middle; font-size:16px;">
					ATTENDANCE RECORD
				</td>
			</tr>	
			<tr style="background-color:#E7E8E7; font-weight:bold; height:40px;">
				<td>&nbsp;</td>
		<?php 
			if ($school_days) {
				foreach ($school_days AS $sday) {
		?>			
					<td style="text-align:center; vertical-align:middle;">
						<?php print($sday->month); ?>
					</td>
		<?php 
				}
			}
		?>
				<td style="text-align:center; vertical-align:middle;">TOTAL</td>
			</tr>
			
			<tr>
				<td>School Days Per Month</td>
		<?php 
			
			$total_schooldays=0;

			if ($school_days) {
				foreach ($school_days AS $sday) {
		?>			
					<td style="text-align:center; vertical-align:middle;">
						<?php 
							print($sday->num_days); 
							$total_schooldays += $sday->num_days;
						?>
					</td>
		<?php 
				}
			}
		?>
				<td style="text-align:center; vertical-align:middle;"><?php print($total_schooldays); ?></td>
			</tr>

			<tr>
				<td>School Days Present</td>
		<?php 
			
			$total_presentdays = 0;

			if ($present_days) {
				foreach ($present_days AS $pday) {
		?>			
					<td style="text-align:center; vertical-align:middle;">
						<?php 
							print($pday->days_present); 
							$total_presentdays += $pday->days_present;
						?>
					</td>
		<?php 
				}
			}
		?>
				<td style="text-align:center; vertical-align:middle;"><?php print($total_presentdays); ?></td>
			</tr>

			<tr>
				<td>Days Tardy</td>
		<?php 
			
			$total_tardydays = 0;

			if ($present_days) {
				foreach ($present_days AS $pday) {
		?>			
					<td style="text-align:center; vertical-align:middle;">
						<?php 
							print($pday->days_tardy); 
							$total_tardydays += $pday->days_tardy;
						?>
					</td>
		<?php 
				}
			}
		?>
				<td style="text-align:center; vertical-align:middle;"><?php print($total_tardydays); ?></td>
			</tr>
			
		</table>
	</div>
					