<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Benchmark {
	private $CI;
	private $log_benchmark;
	private $elapsed_time;
	
	public function __construct(){
		$this->CI =& get_instance();
		$this->log_benchmark = $this->CI->config->item('log_benchmark');
	}
	
	public function start_benchmark (){
		$this->CI->benchmark->mark('code_start');
	}
	
	public function elapsed_time(){
		return $this->elapsed_time();
	}
	
	public function end_benchmark (){
		$this->CI->benchmark->mark('code_end');
		$this->elapsed_time = $this->CI->benchmark->elapsed_time('code_start', 'code_end');
		if ($this->log_benchmark && $this->elapsed_time > 1.3)
			file_put_contents('benchmark_log.txt', date('[Y-m-d H:i:s] - ') . current_url() . " {$this->elapsed_time} seconds \n", FILE_APPEND);
	}
	
	public function ssl_hook (){
		$ssl_on = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=='on');
		if($this->CI->config->item('use_ssl') && !$ssl_on){
			redirect(str_replace('http://', 'https://', current_url()));
		}
	}
}