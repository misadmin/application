<?php
if ( ! $keep_cache ) {
	header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
	header('Pragma: no-cache'); // HTTP 1.0.
	header('Expires: 0'); // Proxies.
}
?><!DOCTYPE html>
<html<?php if ( isset($class) ) echo " class=\"{$class}\""; ?> lang="en" browser="<?php echo $this->agent->browser() ?>" version="<?php echo $this->agent->version() ?>">
	<head>
		<meta charset="<?php echo $charset; ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php if( ! $keep_cache) : ?>
		<meta http-equiv='cache-control' content='no-cache'>
		<meta http-equiv='expires' content='0'>
		<meta http-equiv='pragma' content='no-cache'>
<?php endif; ?>
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/' . $this->config->item('favicon')); ?>">
		<title><?php echo $title; ?></title>
<?php if (count($scripts) > 0) : ?>
<?php foreach ($scripts as $script): ?>
		<script src="<?php echo $script; ?>"></script>
<?php endforeach; ?>
<?php endif; ?>
<?php if (count($styles) > 0) : ?>
<?php foreach ($styles as $style): ?>
		<link rel="stylesheet" href="<?php echo $style; ?>" />
<?php endforeach; ?>
<?php endif; ?>
		<style>
		.breadcrumb a {
			color: #597458;
			font-weight: bold;
		}
		.breadcrumb a:hover {
			color: #065304;
			text-decoration: underline;
		}
		.loading {
			background-image: url('<?php echo base_url('assets/img/loading.gif') ?>');
		}
		@font-face {
			font-family: 'Helvetica';
			src: url('<?php echo base_url('assets/font/helvetica.woff') ?>');
			src: url('<?php echo base_url('assets/font/helvetica.eot?#iefix') ?>') format('embedded-opentype'),  url('<?php echo base_url('assets/font/helvetica.woff') ?>') format('woff'),  url('<?php echo base_url('assets/font/helvetica.ttf') ?>') format('truetype');
			font-weight: normal;
			font-style: normal;
		}
		@font-face {
		  font-family: 'wf_SegoeUI';
		  font-style: normal;
		  font-weight: 400;
		  src:local("Segoe UI"),local("Segoe"),local("Segoe WP"), url(<?php echo base_url('assets/font/SegoeUI.woff') ?>) format('woff');
		}
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Open Sans'), local('OpenSans'), url(<?php echo base_url('assets/font/DroidSans.woff') ?>) format('woff');
		}
		</style>
		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link rel="stylesheet" href="<?php print(base_url('assets/css/shs.css')); ?>" />

	</head>
	<body>
	<div class="hidden" style="display:none">
	</div>
	
