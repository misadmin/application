<?php
class sms_client {
	private $conn; 
	public function __construct($config){
		$this->conn = new curl_connector($config['client_id'], $config['api_key'], $config['sms_api_url']);
	}
	public function send_message($sms_number, $message, $sticky=FALSE){
		$return = $this->conn->fetch('send_message', array('number'=>$sms_number, 'message'=>$message, 'sticky'=>($sticky?'yes':'no')));
		return $return;
	}
	public function send_multiple_messages($messages){
		$return = $this->conn->fetch('send_multiple_messages', array('messages'=>json_encode($messages)));
		return $return;
	}
}

class curl_connector {
	private $session_var;
	private $client_id;
	private $api_key;
	private $sms_api_url;
	
	public function __construct($client_id, $api_key, $sms_api_url){
		$this->client_id = $client_id;
		$this->api_key = $api_key;
		$this->sms_api_url = $sms_api_url;
	}
	public function fetch($action, $params){
		$params['client_id'] = $this->client_id;
		$params['api_key'] = $this->api_key;
		$params['action'] = $action;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->sms_api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		if(preg_match('!https://!', $this->sms_api_url)){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		}
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_POST, 1);
		$result = curl_exec($ch);
		return $result;
	}
}