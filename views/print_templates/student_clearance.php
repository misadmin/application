<?php 
$ledger['header'] = array(
		array('id'=>'date', 'name'=>'Date', 'width'=>9),
		array('id'=>'transaction', 'name'=>'Transaction', 'width'=>26),
		array('id'=>'reference', 'name'=>'Reference', 'width'=>18),
		array('id'=>'debit', 'name'=>'DR/Charges', 'width'=>11),
		array('id'=>'credit', 'name'=>'CR/Payments', 'width'=>11),
		array('id'=>'balance', 'name'=>'Balance', 'width'=>11),
);
$idnumber = isset($idnumber) ? $idnumber : "";
$payment_period = isset($payment_period) ? $payment_period : "";
$lab_courses = isset($lab_courses) ? $lab_courses : array();
$ledger['data'] = array();
if(isset($ledger_data) && is_array($ledger_data) && count($ledger_data) > 0){
	$running_balance = 0;
	foreach($ledger_data as $key=>$ledge){
		$running_balance += $ledge->debit - $ledge->credit;
			$ledger['data'][] = array(
					'date'=>$ledge->transaction_date,
					'transaction'=>$ledge->transaction_detail,
					'reference'=>isset($ledge->reference_number) && !empty($ledge->reference_number) ? "Ref: ".$ledge->reference_number : '',
					'debit'=>$ledge->debit,
					'credit'=>$ledge->credit,
					'balance'=>$running_balance,
			);
	}
}
//print_r($ledger);
if(!isset($clearance)){
	$clearance = array(
			'Library',
			'Bookstore',
			'Local Government Unit',
			'Central Student Government',
			'Student Affairs Office',
			'Dean'
			);
	$lab = array(
			'ECE 421EL',
			'ECE 424EL',
			'ES 122',
			'ECE 421EL',
			'ECE 424EL',
			'ES 122',
			'ECE 421EL',
			'ECE 424EL',
			'ES 122'
			);
} else {
	$lab = array();
}


$this->print_lib->set_ledger_title('');
$this->print_lib->set_table_header($ledger['header']);
$this->print_lib->set_data($ledger['data']);
?>

<?php
//this should be placed outside the html tag or body to be accessible
//echo sprintf($this->config->item('jzebra_applet'), base_url()); ?>
<div id="print_clearance" style="display:none;"><?php echo $this->print_lib->init(); 
if($this->session->userdata('printed') == 'yes')
	echo $this->print_lib->set_page_length_in_lines(33); else {
	echo $this->print_lib->set_page_length_in_lines(34);
	$this->session->set_userdata('printed', 'yes');
}
?>
<?php echo $this->print_lib->align_right($idnumber) . "\n";?>
<?php echo $this->print_lib->align_right($name) . "\n"; ?>
<?php echo $this->print_lib->align_right(isset($course_year) ? $course_year : '') . "\n"; ?>
<?php echo $this->print_lib->bold() . $this->print_lib->align_right('Student Clearance') . $this->print_lib->regular(). "\n"; ?>
<?php echo $this->print_lib->draw_ledger(146, $payment_period, $payable, "=", TRUE); ?>
<?php if (isset($basic_ed) && $basic_ed === TRUE)
	echo $this->print_lib->basic_ed_clearance($clearance); else 
	echo $this->print_lib->draw_clearance(array('office'=>$clearance, 'laboratory'=>$lab_courses), 78, 2, TRUE); 

?>
<?php echo $this->print_lib->char_form_feed(); ?>
</div>

<script>
</script>