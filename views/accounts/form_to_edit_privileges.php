<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	height:25px;
}
</style>

  <div style="background:#FFF; "> 
 
    <table align="center" cellpadding="0" cellspacing="0" class="head1" style="width:42%;">
      <tr class="head">
        <td colspan="4" class="head"> EDIT PRIVILEGES </td>
      </tr>
      <tr>
        <td colspan="4">
<form method="post" id="cancel_privilege_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value= "form_to_edit_privileges"/>
		
		<table border="0" cellpadding="1" cellspacing="0" class="inside" style="width:100%;">
           <tr>
            <td width="138" valign="middle">Term</td>
            <td width="5" valign="middle">:</td>
            <td width="73" valign="middle"> <?php print($term->term." ".$term->sy); ?> </td>
          </tr>	   
		  <tr>
            <td valign="middle">Privilege</td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><select name="privilege_id" class="form" value ="$privilege">
              <?php
					foreach($privilege_items AS $privilege_item) {
						if ($privilege_item->id == $privilege->scholarships_id) {
							print("<option value=".$privilege_item->id." selected=\"selected\">".$privilege_item->scholarship."</option>");	
						} else {
							print("<option value=".$privilege_item->id." >".$privilege_item->scholarship."</option>");							
						}
					}
				?>
            </select></td>
		  </tr>
		   <tr>
            <td valign="middle"> Tuition Percentage  </td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><input name="TuitionD" type="text" id="TuitionD" size="3" maxlength="3" 
									value ="<?php print($tuition_percentage); ?>" style="width:25px; height:12px;"> 
             % </td>
          </tr>
		   <tr>
		     <td valign="middle">Units</td>
		     <td valign="middle">:</td>
		     <td valign="middle" style="font-weight:bold;"><?php print($paying_units); ?></td>
          </tr>
			<?php
				if ($other_privileges) {
			?>
		    <tr>
		      <td colspan="3" valign="middle" style="padding-left:15px;">		        	      </td>
	        </tr>
		  
		    <tr>
		     <td colspan="3" valign="middle"><strong><u> Privileges</u></strong></td>
          </tr>
        </table>
</form>		</td>
      </tr>
      <tr>
        <td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="1">
          <?php
					$total = 0;
					foreach($other_privileges AS $other) {
			?>
          <tr>
            <td style="padding-left:15px;"><?php 
					if ($other->weight != 1) {
				?>
                <a class="cancel_priv" href="<?php print($other->id); ?>" priv_details_id="<?php print($other->id); ?>"> <img src="<?php print(base_url('assets/img/icon-recycle.gif')); ?>" width="16" height="16" /></a>
                <?php 
						}
					?>
                <?php 
				 			print($other->fees_group);  
					?>
            </td>
            <td>:</td>
            <td style="padding-right:15px;"><?php 
								print(number_format($other->discount_amount,2)); 
								$total = $total + $other->discount_amount;
			 		 ?>
            </td>
          </tr>
          <?php
							}
						}
					?>
          <tr style="font-weight:bold;">
            <td align="right" valign="middle" style="padding-left:15px;">TOTAL</td>
            <td valign="middle">:</td>
            <td align="left" valign="middle" style="padding-right:15px;"><?php print(number_format($total,2));  ?></td>
          </tr>
		    <tr>
		      <td colspan="3" align ="center" valign="middle"><input type="submit" name="button2" id="cancel_privilege" value="Edit!"  class="btn btn-warning" /></td>
	        </tr>
        </table></td>
      </tr>
    </table>
  </div>
<script>
$('#cancel_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to assign privilege to student?");

	if (confirmed){
		$('#cancel_privilege_form').submit();
	}		
});
</script>
 
  
  <form id="cancel_form1" action="<?php echo site_url("accounts/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="action" value="cancel_privilege"/>
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('.cancel_priv').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete the student privilege?');

		if (confirmed){
			var priv_details_id = $(this).attr('href');
			
			$('<input>').attr({
				type: 'hidden',
				name: 'priv_details_id',
				value: priv_details_id,
			}).appendTo('#cancel_form1');
			$('#cancel_form1').submit();
		}
		
	});
</script>
  