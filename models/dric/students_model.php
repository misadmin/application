<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Students_Model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }
    public function student_form9_information($student_id){
    	$this->db->select("CONCAT(CONCAT_WS(', ', s.lname, s.fname), ' ', SUBSTR(s.mname, 1, 1), '.') as fullname,
    	IF(s.gender='M', 'Male', IF(s.gender='F', 'Female','')) as gender,
    	DATE_FORMAT(s.dbirth, '%b. %e, %Y') as birthdate,
    	TIMESTAMPDIFF(YEAR,s.dbirth,CURDATE()) as age,
    	TRIM(CONCAT(IF(NOT ISNULL(a.birth_address), a.birth_address, ''), IF(NOT ISNULL(a.birth_towns_id), birth_town.name, ''), IF(NOT ISNULL(a.birth_provinces_id), CONCAT(', ', birth_province.name), ''), IF(NOT ISNULL(a.birth_countries_id), CONCAT(', ', birth_countries.name), ''))) as birth_place,
    	TRIM(CONCAT(IF(NOT ISNULL(a.home_address), a.home_address, ''), IF(NOT (ISNULL(a.home_barangays_id) OR a.home_barangays_id=''), CONCAT(', ', home_barangays.name), ''), IF(NOT ISNULL(a.home_towns_id), CONCAT(', ', home_towns.name), ''), IF(NOT ISNULL(a.home_provinces_id), CONCAT(', ', home_provinces.name), ''), IF(NOT ISNULL(a.home_countries_id), CONCAT(', ', home_countries.name), ''))) as home_address,
    	sh.abbreviation as academic_program_abbreviated, sh.description as academic_program,
    	s.meta
    			", FALSE);
    	$this->db->from('students s')
    		->join('addresses a', 's.idno=a.students_idno', 'left')
    		->join('towns birth_town', 'a.birth_towns_id=birth_town.id', 'left')
    		->join('provinces birth_province', 'birth_town.provinces_id=birth_province.id', 'left')
    		->join('countries birth_countries', 'birth_province.countries_id=birth_countries.id', 'left')
    		->join('barangays home_barangays', 'a.home_barangays_id=home_barangays.id', 'left')
    		->join('towns home_towns', 'a.home_towns_id=home_towns.id', 'left')
    		->join('provinces home_provinces', 'a.home_provinces_id=home_provinces.id', 'left')
    		->join('countries home_countries', 'a.home_countries_id=home_countries.id', 'left')
    		->join("(SELECT students_idno, academic_programs.abbreviation, academic_programs.description 
	FROM student_histories 
	LEFT JOIN prospectus ON prospectus.id=student_histories.prospectus_id 
	LEFT JOIN academic_programs ON academic_programs.id=prospectus.academic_programs_id
	LEFT JOIN academic_terms ON student_histories.academic_terms_id=academic_terms.id
	LEFT JOIN academic_years ON academic_terms.academic_years_id=academic_years.id
	WHERE student_histories.students_idno=" . $this->db->escape($student_id) . " 
	ORDER BY academic_years.end_year DESC, academic_terms.term DESC
	LIMIT 1) sh", 'sh.students_idno=s.idno', 'left')
    		->where('s.idno', $student_id);
    	$query = $this->db->get();
    	if($query->num_rows() > 0)
    		return $query->row(); else
    		return FALSE;
    }
    public function student_form9_requirements($student_id){
    	$this->db->select("GROUP_CONCAT(DISTINCT r.name ORDER BY r.name SEPARATOR ', ') as requirements", FALSE)
    		->from('students_requirements sr')
    		->join('requirements r', 'sr.requirements_id=r.id', 'left')
    		->where('sr.students_idno', $student_id);
    	$query = $this->db->get();
    	if($query->num_rows() > 0)
    		return $query->row(); else
    		return FALSE;
    }
    public function student_form9_prelim_educ($student_id){
    	$this->db->select("IF(educ_level IN ('Primary', 'Intermediate', 'Elementary'), 'elementary', IF(educ_level IN ('Secondary'), 'hs', educ_level)) as educ_prim_level,
		    	educ_level,
		    	schools.school,
		    	CONCAT(student_educations.sy_start, '-', student_educations.sy_end) as school_year
		    	", FALSE);
    	$this->db->from('student_educations')
    			->join('schools', 'schools.id=student_educations.schools_id', 'left')
    			->where('student_educations.students_idno',$student_id)
    			->order_by('sy_start');
    	$query = $this->db->get();
    	//echo $this->db->last_query(); die();
    	if($query->num_rows() > 0){
    		$elementary = array();
    		$highschool = array();
    		$year_levels = array('1st', '2nd', '3rd', '4th');
    		$level = 0;
	    	foreach($query->result() as $row){
	    		if($row->educ_prim_level=='elementary'){
	    			$elementary[] = (object)array(
	    					'level'=>$row->educ_level,
	    					'school'=>$row->school,
	    					'sy'=>$row->school_year);
	    		}
	    		if($row->educ_prim_level=='hs'){
	    			$highschool[] = (object)array(
	    					'level'=>$year_levels[$level],
	    					'school'=>$row->school,
	    					'sy'=>$row->school_year,);
	    			$level++;
	    		}
	    	}
	    	return array('elementary'=>$elementary, 'highschool'=>$highschool);
    	} else {
    		return FALSE;
    	}
    }
    public function student_form9_all_courses ($student_id){
    	$query = $this->db->query("SELECT * 
    		FROM ((SELECT
				at.id as academic_term_id, at.term, ay.end_year, CONCAT((ay.end_year - 1), '-', ay.end_year) as school_year, 
    			t.term_description as academic_term, 
    			c.course_code, c.descriptive_title, e.finals_grade,    			
				if(!isnull(eo.id), IF(eo.is_bracketed='Y', -eo.credit_units, eo.credit_units), IF(pc.is_bracketed='Y', -c.credit_units, c.credit_units)  ) as credit_units,    			
   				'Holy Name University' as school, '1' as from_hnu
				FROM
					enrollments e
				JOIN
					course_offerings co
				ON
					e.course_offerings_id=co.id
    			LEFT JOIN enrollments_override eo ON eo.enrollments_id=e.id    			    			
				LEFT JOIN
					courses c
				ON
					co.courses_id=c.id
				LEFT JOIN
					student_histories sh
				ON
					sh.id=e.student_history_id
				LEFT JOIN
					academic_terms at
				ON
					at.id=sh.academic_terms_id
				LEFT JOIN
					academic_years ay
				ON
					ay.id=at.academic_years_id
				LEFT JOIN
					term t
				ON
					`at`.`term`=t.id
				LEFT JOIN
					prospectus p
				ON
					sh.prospectus_id=p.id
				LEFT JOIN
					prospectus_courses pc
				ON
					pc.courses_id=c.id
				WHERE
					sh.students_idno=" . $this->db->escape($student_id) . "
				GROUP BY
					co.id
				ORDER BY
					ay.end_year ASC, at.term ASC)
			UNION
				(SELECT
					'' as academic_term_id, ost.id as term,	osay.end_year, CONCAT((osay.end_year - 1), '-', osay.end_year) as school_year, ost.term_description as academic_term,
					ose.catalog_no as course_code, ose.descriptive_title, ose.final_grade as finals_grade, ose.units, os.school, '0' as from_hnu
				FROM
					other_schools_enrollments as ose
				LEFT JOIN
					other_schools_student_histories ossh
				ON
					ose.other_schools_student_histories_id=ossh.id
				LEFT JOIN
					academic_years osay
				ON
					osay.id=ossh.academic_years_id
				LEFT JOIN
					term ost
				ON
					ost.id=ossh.term_id
				LEFT JOIN
					schools os
				ON
					os.id=ossh.schools_id
				WHERE
					ossh.students_idno=" . $this->db->escape($student_id) ."))
    			as unioned
    		ORDER BY unioned.end_year asc, unioned.term asc, unioned.course_code asc
			");
    		if($query->num_rows() > 0)
				return $query->result(); else
				return FALSE;
    }
}