<?php 
	$year_levels = array(
			1 => 'First',
			2 => 'Second',
			3 => 'Third',
			4 => 'Fourth',
			5 => 'Fifth',
			);
	$terms_name = array(
			1 => 'First Semester',
			2 => 'Second Semester',
			3 => 'Summer',
			);
?>
<?php foreach ($prospectus as $year => $years): ?>
	<div class="row-fluid">
		<h3 class="heading"><?php echo $year_levels[$year]; ?> Year</h3>
<?php foreach ($years as $term=>$terms): ?>
		<div class="span5">
			<h4><?php echo $terms_name[$term]; ?></h4>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Code</th>
						<th>Descriptive Title</th>
						<th>Units</th>
					</tr>
				</thead>
				<tbody>
<?php foreach($terms as $course):?>
					<tr>
						<td><?php echo str_replace('-','',$course['course_code']); ?></td>
						<td><?php echo $course['descriptive_title']; ?></td>
						<td><?php echo $course['credit_units']; ?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
		</div>	
<?php endforeach; ?>
	</div>
<?php endforeach; ?>
