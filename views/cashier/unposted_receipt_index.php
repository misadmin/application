<h2 class="heading">Summary of Receipts for Posting </h2>
<div class="row-fluid">
	<div class="span10">
		<form action="<?php echo site_url("cashier/post_receipts")?>" class="form-horizontal" id="rsi" method="post">
			<input type="hidden" name="nonce" value="<?php echo $this->common->nonce() ?>" />
			<input type="hidden" name="step" value="2" />
				<div class="input-append">
					<div class="date" id="rsi-date" data-date-format="yyyy-mm-dd" data-date="<?php echo $date; ?>">
					     <input type="text" id="rsi-date-input" name="date" placeholder="YYYY-MM-DD" value="<?php echo $date; ?>" readonly="">
						 <span class="add-on"><i class="icon-calendar"></i></span>
						 <button type="submit" class="btn btn-success">Show</button>
					</div>
				</div>
		</form>
	</div>
	</div>
<script>
$(document).ready(function() {
	$('#rsi-date').datepicker();
});
</script>