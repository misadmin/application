<?php if (sizeof($receipts)>0) { ; ?>
	<span>This is the listing of Official Receipts. You can view the detail by clicking the row link then either void the receipt or reprint it.</span><br /><br />
		<table class='table table-stripped'>
			<thead>
				<tr>
					<th></th>
					<th>Pay Method</th>
					<th>Transaction Date</th>
					<th>OR No.</th>
					<th>Machine No.</th>
					<th>Teller</th>
					<th>ID No.</th>
					<th>Payor</th>
					<th>Description</th>
					<th style="text-align:right;">Amount</th>
				</tr>
			</thead>
			<tbody>
					<?php foreach($receipts as $r): ?>
					<tr>
						<td><a class="receipt_detail" href="" data-pid="<?php echo $r->payment_id; ?>"><i class="icon-zoom-in "></a></td>
				 		<td><?php echo isset($r->payment_method) ? $r->payment_method : ""; ?></td>
						<td><?php echo isset($r->transaction_date) ? $r->transaction_date : ""; ?></td>
						<td><?php echo isset($r->receipt_no) ? $r->receipt_no : ""; ?></td>
						<td><?php echo isset($r->machine_ip) ? $r->machine_ip : ""; ?></td>
						<td><?php echo isset($r->teller) ? $r->teller : ""; ?></td>
						<td><?php echo isset($r->idno) ? $r->idno : ""; ?></td>
						<td><?php echo isset($r->payor) ? $r->payor : ""; ?></td>
						<td><?php echo isset($r->description) ? $r->description : ""; ?></td>
						<td style="text-align:right;"><?php echo isset($r->amount) ? number_format($r->amount, 2) : 0.00; ?></td>
					</tr>
					<?php endforeach; ?>
			</tbody>
		</table>
<?php } else {
	echo "<span>Sorry, no record found :( </span><br /><br />";
}?>
<form action="" class="form-horizontal" id="rd" method="post">
	<input type="hidden" name="nonce" value="<?php echo $this->common->nonce() ?>" />
</form>
<script>
$().ready(function(){
	$('.receipt_detail').click(function(event){
		//var $payment_id = $(this).attr('pay_id');
		//$('<input>').attr({
		//	type: 'hidden',
		//	name: 'payment_detail',
		//	value: payment_detail,
		//}).appendTo('#rsi');				
		//$('#rsi').submit();
		var pid = $(this).data('pid');
		$('<input>').attr({
			type: 'hidden',
			name: 'payment_id',
			value: pid
		}).appendTo('#rd');
		$('#rd').submit();
		event.preventDefault();
	});
})
</script>