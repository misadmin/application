<div class="span12">
	<form method="post" class="form-horizontal" id="allow_enroll_form">
		<fieldset>
			<input type="hidden" name="action" value="privilege_enroll" />
<?php $this->common->hidden_input_nonce(FALSE); ?>
			<div class="control-group formSep">
				<div class="controls">
					<button id="allow_enroll" class="btn btn-warning" type="submit">Allow this student to Enroll</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script>
$('#allow_enroll').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Are you sure?");
	if (confirmed){
		$('#allow_enroll_form').submit();
	}		
});
</script>