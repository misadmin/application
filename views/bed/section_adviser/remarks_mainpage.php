
		<div style="width:100%;" >

			<?php 
				if($show_pulldown) {
			?>
					<div style="width:98%; margin:0 auto; ">
						<?php 
							$data = array(
										'academic_terms'=>$academic_terms,
										'selected_term'=>$selected_term,
										'select_id'=>'remarks',
										'change_select'=>'change_remarks_term');
							$this->load->view('shs/reports/pulldown_academic_terms',$data);
						?>
					</div>
			<?php
				}
			?>
			
			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; " id="display_remarks_form" >
				<?php 
					$this->load->view('shs/class_adviser/submit_remarks_form');
				?>
			</div>
		</div>
			
			
<script>
	$(document).ready(function(){
		$('.change_remarks_term').bind('change', function(){
			
			var element2    = $("option:selected", "#remarks");
			var term_id     = element2.val();
			
			var student_idno = '<?php print($idnum); ?>';

			$('#show_change_icon_remarks').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+student_idno,
				data: {	"term_id": term_id,
						"action": "change_term_for_remarks" },
				dataType: 'json',
				success: function(response) {													

					$('#display_remarks_form').html(response.output); 

					$('#show_change_icon_remarks').html("")
										
				}
			});

		});
	});
</script> 

