<div style="float:left; width:auto; margin-top:10px">
<form action="" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="2" />
	
	
<div class="formSep">
	<h2><?php print($title); ?></h2>
</div> 
	<fieldset>
		<div class="control-group formSep">
				<label class="control-label"> <b> Academic Year: </b> </label>
					<div class="controls">
						<select id="academic_year" name="academic_year">
						<?php
							foreach($academic_years AS $academic_year) {
								print("<option value=".$academic_year->id.">".$academic_year->start_year."-".$academic_year->end_year."</option>");	
							}
						?>
						</select>
					</div>
		</div>
	
		<div class="control-group formSep">
		    <label class="control-label" for="level"><b> Level&nbsp;&nbsp;: </b></label>
		    <div class="controls">
			    <select id="levels" name="level" class="form">
					<option value="">-- Select Level --</option>
						<?php foreach ($levels as $level): ?>
						<option value="<?php echo $level->id; ?>"><?php echo $level->level; ?></option>
						<?php endforeach; ?>
				</select>
	    	</div>
	    </div>
	    
	     <div class="control-group formSep">
		    <label class="control-label" for="grade_level"><b> Grade/Year Level&nbsp;&nbsp;: </b></label>
		    <div class="controls">
			    <select id="year" name="year" class="form">
					<option value="">Select Year</option>
				</select>
	    	</div>
	    </div>
	    
	    <div class="control-group">
					<label class="control-label" for="level"><b> Class Size&nbsp;&nbsp;: </b></label>
					<div class="controls">
						 <input type="text" name="classSize" class="gpa input-xlarge" value ="50" style="width:20px; text-align:left">
					</div>
		</div>	
	
		<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">View</button>
					</div>
		</div>	
	</fieldset>
</form>

<script>
$('#levels').change(function(){
	var level_id = $('#levels option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/year"); ?>/?level=" + level_id,
		dataType: "json",
		success: function(data){
			var doptions = make_options_year(data);
			$('#year').html(doptions);
		}	
	});	
});


function make_options_year (data){
var doptions = '<option value="">-- Select Year --</option>';
for (var i = 0; i < data.length; i++) {
	doptions = doptions 
	 	+ '<option value="'
	 	+ data[i].yr_level
	 	+ '">'
	 	+ data[i].yr_level
	 	+ '</option>';
}
return doptions;
}

function make_options_section (data){
var doptions = '<option value="">-- Select Section --</option>';
for (var i = 0; i < data.length; i++) {
	doptions = doptions 
	 	+ '<option value="'
	 	+ data[i].id
	 	+ '">'
	 	+ data[i].section
	 	+ '</option>';
}
return doptions;
}

</script>