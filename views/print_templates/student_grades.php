<?php 

class Student_Grades extends print_lib {
	private $content="";
	private $width = 78;
	public function __construct($headers, $data){
		$this->set_table_header ($headers);
		$this->set_data ($data);
		$this->append_content($this->init());
		$this->append_content($this->set_page_length_in_lines(33));
	}
	
	public function header ($data){
		$this->append_content ($this->align_right($data['idnumber']));
//		$this->append_content ($this->align_right($data['idnumber'] . "-" . $data['familyname'] . ", " . $data['firstname'] . " " . substr($data['middlename'], 0, 1) . "."));
		$this->append_content ($this->align_right($data['familyname'] . ", " . $data['firstname'] . " " . substr($data['middlename'], 0, 1) . "."));
		$this->append_content ($this->align_right($data['year_level'] . " " . $data['course_that_term']));
		$this->append_content ($this->align_right($data['term_that_time'] . 'Student Grades'));
//		$this->append_content ($this->bold() . $this->align_right('Student Grades') . $this->regular());
		$this->content .= "\n";
	}
	
	public function footer (){
		
		$this->append_content("Student's Grades printed by HNUMIS. " . date(DATE_RFC822));
		$this->append_content($this->char_form_feed());
	}
	public function align_right($line){
		return str_pad($line, $this->width, ' ', STR_PAD_LEFT) . "\n";
	}
	public function append_content($content){
		$this->content .= $content;
	}
	public function draw_table(){
		$table = "";
		$border = "";
		$dcontent = "";
		$def = array();
		$grade_rows = array();
		foreach ($this->headers as $header){
			$border .= str_pad('', floor($header['width'] * $this->width/100), '-', STR_PAD_RIGHT) . " ";
			$dcontent .= str_pad($header['name'],floor($header['width'] * $this->width/100), ' ', STR_PAD_RIGHT) . " ";
			$def[] = ceil($header['width'] * $this->width/100);
		}
		foreach ($this->data as $row){
			$grade_rows[] = array(
					(object)array('content' => $row['catalog'], 'align'=>'left',),
					(object)array('content' => $row['section'], 'align'=>'center',),
					(object)array('content' => $row['title'], 'align'=>'left',),
					(object)array('content' => $row['units'], 'align'=>'center',),
					(object)array('content' => $row['prelim'], 'align'=>'center',),
					(object)array('content' => $row['midterm'], 'align'=>'center',),
					(object)array('content' => $row['finals'], 'align'=>'center',)
			);
		}
		$table = $border ."\n" . $dcontent . "\n" . $border . "\n";
		$table .= $this->table($def, $grade_rows) . "\n" . $border. "\n";
		return $table;
	}
	public function content($return=FALSE){
		$this->append_content($this->draw_table());
		$this->content .= "\n\n";
		$this->append_content($this->align_right('____________________'));
		$this->append_content(str_pad('', 58, ' ') . str_pad('Registrar', 20, ' ', STR_PAD_BOTH) . "\n");
		$lines_left = 30 - count(explode("\n", $this->content));
		for ($count = 0; $count < $lines_left; $count++)
			$this->content .= "\n";
		$this->footer();
		if($return)
			return $this->content; else
			echo $this->content;
	}
}
$headers = array(
				array('id'=>'catalog', 'name'=>'Catalog #', 'width'=>20),
				array('id'=>'section', 'name'=>"", 'width'=>5),
				array('id'=>'title', 'name'=>"Descriptive Title", 'width'=>35),
				array('id'=>'units', 'name'=>"Units", 'width'=>10),
				array('id'=>'prelim', 'name'=>"Prelim", 'width'=>10),
				array('id'=>'midterm', 'name'=>"Midterm", 'width'=>10),
				array('id'=>'finals', 'name'=>"Finals", 'width'=>10)
		);

$data = array();
$year_level 		= isset($grades[0]->year_level) ? $grades[0]->year_level : "";
$course_that_term 	= isset($grades[0]->abbreviation) ? $grades[0]->abbreviation : "";
$term_that_time 	= isset($grades[0]->term) ? $grades[0]->term : "";
$sy_that_time 		= isset($grades[0]->sy) ? $grades[0]->sy : "";

if(is_array($grades) && count($grades) > 0){
	foreach($grades as $grade){
		$data[] = array(
				'catalog'=>$grade->course_code,
				'section'=>$grade->section_code,
				'title'=>$grade->descriptive_title,
				'units'=>$grade->credit_units,
				'prelim'=>$grade->prelim_grade,
				'midterm'=>$grade->midterm_grade,
				'finals'=>$grade->finals_grade,
		);
	}	
}

$student_grade_obj = new Student_Grades ($headers, $data);
$student_grade_obj->header (array(
		'idnumber'=>$idnumber,
		'familyname'=>$familyname,
		'firstname'=>$firstname,
		'middlename'=>$middlename,
		'year_level'=>$year_level,
		'course_that_term'=>$course_that_term,
		'term_that_time'=>$term_that_time . " SY " . $sy_that_time,
		));
$student_grade_obj->set_data($data);
//this should be placed outside the html tag or body to be accessible, maybe after $this->content_lib->content();
//echo sprintf($this->config->item('jzebra_applet'), base_url()); ?>
<div id="grades_to_print" style="display:none;"><?php
$student_grade_obj->content();
/*
echo print_lib::init();
if($this->session->userdata('printed') == 'yes')
	echo $this->print_lib->set_page_length_in_lines(33); else {
	echo $this->print_lib->set_page_length_in_lines(34);
	$this->session->set_userdata('printed', 'yes');
} 
echo print_lib::align_right($idnumber) . "\n";?>
<?php echo print_lib::align_right($familyname . ", " . $firstname) . "\n"; ?>
<?php echo print_lib::align_right($year_level . " " . $course_that_term) . "\n"; ?>
<?php echo print_lib::align_right($term_that_time . " SY " . $sy_that_time) . "\n"; ?>
<?php echo print_lib::align_right('Student Grades') . "\n"; ?>
<?php echo $this->print_lib->draw_table(); ?>
<?php echo "\n\n\n"?>
<?php echo $this->print_lib->align_right('________________________', 78); ?>
<?php echo "\n" . $this->print_lib->align_right('Registrar      ', 78); ?>
<?php echo "\n\n\n\nStudent's Grades printed by HNUMIS. " . date(DATE_RFC822) . "\n\n"; */ ?>
</div>

<div id="container" style="display:none;">
</div>

<script>
function print(content) {
    var dapplet = document.jzebra;
    if (dapplet != null) {
    	dapplet.append('\x1B\x40');
       dapplet.append(content);
       dapplet.print();
	}
}
    
$(document).ready(function(){
	$('#print_grade').click(function(){
		var content_to_print = $('#grades_to_print').text();
		var vapplet = document.jzebra;
		if (vapplet != null) {
			// Searches for default printer
			vapplet.findPrinter();
		}
		print(content_to_print);
	});
	

	$('#print_schedule').click(function(){
		var content_to_print = $('#content_to_print').text();
		var applet = document.jzebra;
		if (applet != null) {
			// Searches for default printer
			applet.findPrinter();
		}
		print(content_to_print);

	});

	$('#print_schedule_irmc').click(function(){
		var content_to_print = $('#content_to_print').text();
		var content_to_print = "";
		var hiddenElement = document.createElement('a');
		content_to_print = $('#content_to_print').text();
		hiddenElement.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(content_to_print);
		hiddenElement.target = '_blank';
		hiddenElement.download = 'receipt.txt';
		document.getElementById('container').appendChild(hiddenElement);
		hiddenElement.click();
	});

	
});
</script>

