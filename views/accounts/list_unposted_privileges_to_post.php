<h2 class="heading">Unposted Privileges</h2>

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; margin-top:10px;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="padding:0px;">
    <thead>
      <tr>
         <th width="50%" style="text-align:center; vertical-align:middle;">Privileges</th>
		<th width="13%" style="text-align:center; vertical-align:middle;">Tuition<br>Discount %</th>
		 <th width="13%" style="text-align:center; vertical-align:middle;">Total<br>Discount</th>
		<!--  
		  <th width="8%" style="text-align:center; vertical-align:middle;">Edit</th> -->
		  <th width="8%" style="text-align:center; vertical-align:middle;">View/Delete</th>
		  <th width="8%" style="text-align:center; vertical-align:middle;">POST</th>
		  
       </tr>
    </thead>
    <?php if($privileges){
    		$gtotal=0;
			$total = 0;
			foreach ($privileges AS $privilege) {
				$priv_details = $this->Scholarship_Model->ListPrivilegeAvailedDetails($privilege->id);
	?>
    <tr>
      <td style="text-align:left;"><?php print($privilege->scholarship); ?></td>
	  <td style="text-align:center;"><?php print(($privilege->discount_percentage * 100)."%"); ?></td>
	  <td style="text-align:right;"><?php print number_format($privilege->total_amount_availed,2); ?></td>
   <!--    <td style="text-align:center; vertical-align:middle; ">
              <i class="icon-edit"></i>
      </td> -->
      <td style="text-align:center; vertical-align:middle; ">
     		<a href="#" data-toggle="modal" data-target="#modal_id_delete_<?php print($privilege->id); ?>" 
				data-project-id='<?php print($priv_details); ?>'><i class="icon-trash"></i></a>

    <div class="modal hide fade" id="modal_id_delete_<?php print($privilege->id); ?>" >
 		<form method="post" id="delete_privilege_form">
   				<input type="hidden" name="action" value="delete_privilege_availed" />
   				<input type="hidden" name="privilege_id" value="<?php print($privilege->id); ?>">
   		<div class="modal-header">
       		<button type="button" class="close" data-dismiss="modal">�</button>
       		<h3>Privilege Details</h3>
   		</div>
   		<div class="modal-body">
 			<div style="text-align:center;">
				<table border="0" cellpadding="2" cellspacing="0" 
					class="table table-striped table-condensed table-hover" style="width:100%;">
				 	<thead>
				  	<tr style="font-weight:bold; background:#EAEAEA;">
				    	<td width="50%" style="text-align:center; vertical-align:middle;">Description</td>
				    	<td width="25%" style="text-align:center; vertical-align:middle;">Percentage</td>
				    	<td width="25%" style="text-align:center;">Amount</td>
				    </tr>
					</thead>
					<?php 
						
						$total = 0;
						foreach($priv_details AS $detail) {
					?>
					<tr>
					    <td style="text-align:left;"><?php print($detail->fees_group); ?></td>
					    <td style="text-align:center;"><?php print(($detail->discount_percentage*100)."%"); ?></td>
					    <td style="text-align:right;"><?php print(number_format($detail->discount_amount,2)); ?></td>
					</tr>
					<?php 
						$total += $detail->discount_amount;
						}
					?>	
					 
					<thead style ="background-color: #F0F0F0;">
						<tr style ="font-weight: bold; font-size:14px;">
							<td colspan="2" style="text-align:right;">Total :</td>
							<td style="text-align:right;">  <?php print(number_format($total,2)); ?></td> 
					 	</tr>
					</thead> 
				</table>
			</div>
   		</div>
	   <div class="modal-footer">
    		<input type="submit" class="btn btn-danger" id="delete_privilege" value="DELETE PRIVILEGE!">
	   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
       </div>
		</form>
    </div>
				
				
				
              
      </td>
      <td style="text-align:center; vertical-align:middle; ">
      		<a href="#" data-toggle="modal" data-target="#modal_id_<?php print($privilege->id); ?>" 
				data-project-id='<?php print($priv_details); ?>'><i class="icon-ok"></i></a>
				
    <div class="modal hide fade" id="modal_id_<?php print($privilege->id); ?>" >
 		<form method="post" name="modal_form" id="post_privilege_form">
   				<input type="hidden" name="action" value="form_to_post_privileges" />
   				<input type="hidden" name="privilege_id" value="<?php print($privilege->id); ?>">
   		<div class="modal-header">
       		<button type="button" class="close" data-dismiss="modal">�</button>
       		<h3>Privilege Details</h3>
   		</div>
   		<div class="modal-body">
 			<div style="text-align:center;">
				<table border="0" cellpadding="2" cellspacing="0" 
					class="table table-striped table-condensed table-hover" style="width:100%;">
				 	<thead>
				  	<tr style="font-weight:bold; background:#EAEAEA;">
				    	<td width="50%" style="text-align:center; vertical-align:middle;">Description</td>
				    	<td width="25%" style="text-align:center; vertical-align:middle;">Percentage</td>
				    	<td width="25%" style="text-align:center;">Amount</td>
				    </tr>
					</thead>
					<?php 
						$total_detail=0;
						foreach($priv_details AS $detail) {
					?>
					<tr>
					    <td style="text-align:left;"><?php print($detail->fees_group); ?></td>
					    <td style="text-align:center;"><?php print(($detail->discount_percentage*100)."%"); ?></td>
					    <td style="text-align:right;"><?php print(number_format($detail->discount_amount,2)); ?></td>
					</tr>
					<?php 
						$total_detail += $detail->discount_amount;
						}
					?>	
					  <thead style ="background-color: #F0F0F0;">
						<tr style ="font-weight: bold; font-size:14px;">
							<td colspan="2" style="text-align:right;">Total :</td>
							<td style="text-align:right;"><?php print(number_format($total_detail,2)); ?></td>
						</tr>
					</thead> 
					<tr>
						<td colspan="3">Remarks: <textarea rows="5" cols="" name="remarks" id="remarks"></textarea>
					</tr>
				</table>
			</div>
   		</div>
	   <div class="modal-footer">
    		<input type="submit" class="btn btn-primary" id="post_privilege" value="POST!">
	   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
       </div>
		</form>
    </div>
				
	</td>
    </tr>
    
    <?php
          $gtotal += $privilege->total_amount_availed;
		}
	?>	
	 	<thead style ="background-color: #F0F0F0;">
		 <tr style ="font-weight: bold; font-size:14px; "> 
		    <td style="text-align: right; " colspan="2">Total :</td>
		    <td style="text-align: right; "><?php print number_format($gtotal,2); ?></td>
		    <td colspan="3">&nbsp;</td>
		 </tr>	
		</thead>
		
		<?php } else { ?>
		<tr>
		<td colspan="6">No unposted privileges</td>
		</tr>
	<?php }?>	
  </table>
</div>


<script>
$('#post_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to POST scholarship?");
    
	if (confirmed){
		
		$('#post_privilege_form').submit();
	}
});
</script>



<script>
$('#delete_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to delete scholarship?");
    
	if (confirmed){
		
		$('#delete_privilege_form').submit();
	}
});
</script>

