
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Course</div>
			<div class="col2a">
				<input type="text" class="form-control" id="descriptive_title_modal" style="width:300px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Schedule</div>
			<div class="col2a">
				<input type="text" class="form-control" id="schedule_modal" style="width:300px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Assign Teacher</div>
			<div class="col2a">
				<select id="teacher_id_modal" class="form-control" >
					<option value="NULL">None</option>
					<?php
						if ($teachers) {
							foreach($teachers AS $teacher) {
					?>
								<option value="<?php print($teacher->empno); ?>"><?php print($teacher->neym); ?></option>
					<?php 
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>
