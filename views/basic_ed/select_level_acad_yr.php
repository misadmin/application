<h2 class="heading">Basic Education Detailed Assessment Report</h2>
<form id="batch_posting_form" method="post" action="" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="display_report" />
	
	<div class="control-group">
	    <label class="control-label">Academic Year&nbsp;&nbsp;:</label>
	    <div class="controls ">
			<select name="academic_years_id" >
				<?php
					foreach($school_years AS $school_year) {
						print("<option value=".$school_year->id.">".$school_year->sy."</option>");
					}
				?>
			</select>
	    </div>
    </div>

	<div class="control-group formSep">
		<label class="control-label">Level</label>
		<div class="controls">
		   <select id="select_level" name="levels_id" >
				<option value="">--Select Level--</option>
				<option value='0'>All Levels</option>
				<?php 
					foreach($levels as $level) { 
				?>
					<option value="<?php print($level->id); ?>" >
								<?php print($level->level); ?></option>
				<?php 
					}
				?>
			</select>
		</div>
	</div>

	<div class="my_level" style="display:none;">	 
		<div class="control-group formSep ">
			<label class="control-label">Year Level</label>
		    <div class="controls">
				<div class="all_yr" style="display:none;">	
					<select name="all_yr_level" disabled>
						<option value="0" checked>All Grade/Year Levels</option>
					</select>
				</div>
				<div class="preschool" style="display:none;">	
					<select name="yr_level_pre">
						<option value="1">Preschool 1</option>
					</select>
				</div>
		    	<div class="kinder" style="display:none;">	
					<select name="yr_level_k">
						<option value="1">Kindergarten 1</option>
					</select>
		    	</div>
				<div class="gs" style="display:none;">
					<select name="yr_level_gs">
				  		<option value="0" selected="selected">All Grade Levels</option>
						<option value="1">Grade 1</option>
						<option value="2">Grade 2</option>
						<option value="3">Grade 3</option>
						<option value="4">Grade 4</option>
						<option value="5">Grade 5</option>
						<option value="6">Grade 6</option>
					</select>
				</div>
				<div class="hs" style="display:none;">
					<select name="yr_level_hs">
				  		<option value="0" selected="selected">All Grade Levels</option>
						<option value="7">Grade 7</option>
						<option value="8">Grade 8</option>
						<option value="9">Grade 9</option>
						<option value="10">Grade 10</option>
					</select>
				</div>
				<div class="shs" style="display:none;">
					<select name="yr_level_shs">
				  		<option value="0" selected="selected">All Grade Levels</option>
						<option value="11">Grade 11</option>
						<option value="12">Grade 12</option>
					</select>
				</div>

	    	</div>
		</div>   
		<div class="control-group">
			    <label class="control-label" for="post_status">Posted?&nbsp;&nbsp;:</label>
			    <div class="controls" style="font-size:14px;">
				    <input type="radio" name="post_status" value="Y" checked >Yes 
				    <input type="radio" name="post_status" value="N" style="margin-left:15px; ">No
		    	</div>
		</div>		
	 
		<div class="control-group">
			<div class="controls">
				<button class="btn btn-success" type="submit" id="batch_post" >Generate Report!</button>
			</div>
		</div>	
	</div>
    
<!--     <div class="control-group">
	    <label class="control-label" for="level">Level&nbsp;&nbsp;:</label>
	    <div class="controls ">
			<select name="levels_id" style="width:150px;" >
				<?php
					print("<option value='0'>All Levels</option>");
					foreach($levels AS $level) {
						print("<option value=".$level->id.">".$level->level."</option>");
					}	
				?>
			</select>
	    </div>
    </div>
 
     
    <div class="control-group">
	    <label class="control-label" for="grade_level">Grade Level&nbsp;&nbsp;:</label>
	    <div class="controls ">
		    <select name="grade_level" id="grade_level" style="width:150px;">
				  <option value="0" selected="selected">All Grade Levels</option>
				  <option value="1">Grade 1</option>
				  <option value="2">Grade 2</option>
				  <option value="3">Grade 3</option>
				  <option value="4">Grade 4</option>
				  <option value="5">Grade 5</option>
				  <option value="6">Grade 6</option>
				  <option value="7">Grade 7</option>
				  <option value="8">Grade 8</option>
				  <option value="9">Grade 9</option>
				  <option value="10">Grade 10</option>
				  <option value="11">Grade 11</option>
				  <option value="12">Grade 12</option>
			</select>
    	</div>
    </div>
	<div class="control-group">
		    <label class="control-label" for="post_status">Posted?&nbsp;&nbsp;:</label>
		    <div class="controls" style="font-size:14px;">
			    <input type="radio" name="post_status" value="Y" checked >Yes 
			    <input type="radio" name="post_status" value="N" style="margin-left:15px; ">No
	    	</div>
	</div>		
    
    <div class="control-group">
    	 <div class="controls">
    		<button class="btn btn-primary" name="generate_report_button" type="submit" id="batch_post">Generate Report!</button>
    	</div>
	</div>
  -->
     
</form>

<script>
$('#batch_post').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to generate Tuition Report?");

	if (confirmed){
		$('#batch_posting_form').submit();
	}		
});
</script>

 <script>
	 $(document).ready(function (e) {
    	$('#select_level').change(function () {
        	if ($(this).val() == '') {
            	$('.my_level').hide();
        		$('.all_yr').hide();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hs').hide();
            	$('.shs').hide();
			} 

        	if ($(this).val() == '0') {
            	$('.my_level').show();
        		$('.all_yr').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hs').hide();
            	$('.shs').hide();
			} 

        	if ($(this).val() == '11') {
            	$('.my_level').show();
        		$('.all_yr').hide();
        		$('.preschool').show();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hs').hide();
            	$('.shs').hide();
			} 

    		if ($(this).val() == '1') {
            	$('.my_level').show();
        		$('.all_yr').hide();
    			$('.preschool').hide();
    			$('.kinder').show();
            	$('.gs').hide();
            	$('.hs').hide();
            	$('.shs').hide();
			} 

        	if ($(this).val() == '3') {
            	$('.my_level').show();
        		$('.all_yr').hide();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').show();
            	$('.hs').hide();
            	$('.shs').hide();
			} 

        	if ($(this).val() == '4') {
            	$('.my_level').show();
        		$('.all_yr').hide();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hs').show();
            	$('.shs').hide();
			} 

        	if ($(this).val() == '12') {
            	$('.my_level').show();
        		$('.all_yr').hide();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hs').hide();
            	$('.shs').show();
			} 

    	});
	});
 </script>

  