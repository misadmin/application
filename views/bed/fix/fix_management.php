

		<div class="form-horizontal">
					<div class="control-group">
						<label for="grade_level" class="control-label">Count:</label>
						<div class="controls fix_count">
							
						</div>
					</div>
	
					<div class="control-group">
						<div class="controls" style="float:left; padding-right:10px;">
							<a href="#" class="btn btn-danger" name="click-status-button" id="click-status-button">Fix!</a>
						</div>
						<div style="float:left; padding-right:8px; margin-top:5px;" id="show_click_icon">
						</div>		
					</div>
	
		</div>
		
		
	<script>
		$('#click-status-button').click(function(e){
			e.preventDefault();		
			
			var confirmed = confirm('Update basic education histories?');

			if (confirmed){
			
				$('#show_click_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

				$.ajax({
					cache: false,
					type: 'POST',
					url: '<?php print(base_url($this->uri->segment(1)."/bed_fix")); ?>',
					data: { "action": "fix_histories"
						},
					dataType: 'json',
					success: function(response) {				

						$('.fix_count').html(response.output); 	

						$('#show_click_icon').html("");
						
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
					
				});
			}
		});

	</script>

		