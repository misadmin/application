<div style="text-align: center; margin-top: 2%;">
	<img src="<?php echo base_url($this->config->item('seal')); ?>" /><br />
	<h3 class="login_title"><?php echo $this->config->item('application_title_long'); ?></h3>
	<br />

<!-- <p style="width: 400px; margin: 0 auto;"><em><b style="color: red;" >NOTICE TO Students who paid the College Registration before March 31, 2017:</b></em> If your enrollment panel fails to display the offerings for Summer 2017, please email your ID Number to <b>mis@hnu.edu.ph</b> OR see the Accounts Clerk to enable your enrollment. Our sincerest apologies for this inconvenience. - IRMC</p>
-->

<!-- <p style="width: 400px; margin: 0 auto;"><em><b style="color: red;" >NOTICE TO Students who are unable to access their accounts, please email your ID No. and Birthdate to mis@hnu.edu.ph for verification. Thank you. - IRMC</p>
-->
<!-- <p style="width: 400px; margin: 0 auto;"><em><b style="color: red;" >NOTICE TO Students who paid the DEFERRED REGISTRATION FEE (DEF CR) prior to May 24, 2016:</b></em> If your enrollment panel fails to display the offerings for SY 2016-2017, please email your ID Number to <b>mis@hnu.edu.ph</b> OR see the Accounts Clerk to enable your enrollment. Our sincerest apologies for this inconvenience. - IRMC
	</b></p> -->

<!--	<p style="width: 400px; margin: 0 auto;"><em><b style="color: red;" >NOTICE TO SENIOR HIGH STUDENTS:</b><br /><em>Please set your 10-digit mobile number in your MIS account to receive notifications from <b>HNUSMS</b>. Visit the IRMC in the Bates Building (lower ground floor) for assistance.</em></p> -->
	
</div>

<div class="login_box">
<form action="<?php echo site_url("{$role}/login"); ?>" method="post" id="login_form">
<div class="top_b"><?php echo ucfirst($role); ?> Login</div>
		<?php if( isset($message) ):?>
		<div class="alert alert-<?php echo $severity; ?> alert-login">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $message; ?>
		</div>
		<?php endif; ?>
		<?php if (isset($referrer) AND $referrer): ?>
		<input type="hidden" name="referrer" value="<?php echo $referrer ?>" />
		<?php endif ?>
		<div class="cnt_b">
			<div class="formRow">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-user"></i></span><input type="text" id="username" name="username" placeholder="ID Number" />
				</div>
			</div>
			<div class="formRow">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-lock"></i></span><input type="password" id="password" name="password" placeholder="Password" />
				</div>
			</div>
		</div>
		<div class="btm_b clearfix">
			<button class="btn btn-inverse pull-right" type="submit">Sign In</button>
		</div>
	</form>
</div>


<script>
	$(document).ready(function(){
		//* boxes animation
		$("#username").focus();
		form_wrapper = $('.login_box');
		function boxHeight() {
			form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);	
		};
		
		form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
		$('.linkform a,.link_reg a').on('click',function(e){
			var target	= $(this).attr('href'),
			target_height = $(target).actual('height');
			
			$(form_wrapper).css({
				'height'		: form_wrapper.height()
			});	
		$(form_wrapper.find('form:visible')).fadeOut(400,function(){
				form_wrapper.stop().animate({
                    height	 : target_height,
					marginTop: ( - (target_height/2) - 24)
                },500,function(){
                    $(target).fadeIn(400);
                    $('.links_btm .linkform').toggle();
		
					$(form_wrapper).css({
						'height'		: ''
					});	
				});
		});
		e.preventDefault();
		});
				
	//* validation
	$('#login_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			username: { required: true, minlength: 1 },
			password: { required: true, minlength: 3 }
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
			setTimeout(function() {
				boxHeight()
			}, 200)
		},
		unhighlight: function(element) {
			$(element).closest('div').removeClass("f_error");
			setTimeout(function() {
				boxHeight()
			}, 200)
		},
		
		errorPlacement: function(error, element) {
			$(element).closest('div').append(error);
		}
	});
});
</script>
