

	<div style="width:100%;" id="main_view_teacher_lists" >	

		<h2 class="heading" style="width:90%;" >Senior High School - Teacher Loads</h2>

		<div style="width:90%; border-radius:5px; background-color:#EAEAEA; border:solid 1px #D8D8D8; height:35px;">
				<div style="float:right;" >
					<div id="show_change_icon" style="float:left; padding-right:8px; margin-top:6px;">
							
					</div>
					<div style="float:left; padding-top:4px; margin-right:10px;" >
						<select name="teacher_id" class="form-control change_select" style="width:auto; height:auto;" id="select1">
							<?php 
								if ($teachers) {
									foreach ($teachers AS $teacher) {
							?>		
										<option value="<?php print($teacher->employees_empno); ?>" 
											teacher_name="<?php print($teacher->name); ?>" 
											<?php if ($selected_teacher == $teacher->employees_empno) print("selected"); ?> >
											<?php print($teacher->name); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
					<div style="float:left; padding-top:4px; margin-right:10px;" >
						<select name="term_id" class="form-control change_select" style="width:auto; height:auto;" id="select2">
							<?php 
								if ($academic_terms) {
									foreach ($academic_terms AS $term) {
							?>		
										<option value="<?php print($term->id); ?>" 
											term_text="<?php print($term->term." ".$term->sy); ?>"
											<?php if ($selected_term == $term->id) print("selected"); ?>>
											<?php print($term->term." ".$term->sy); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
		</div>

		<div style="width:90%;">
			<?php 
				$data = array(
							"class_schedules"=>$class_schedules);
				
				$this->load->view('shs/teacher/teacher_loads',$data);
			?>
		</div>
	</div>

			
					
<script>
	$(document).ready(function(){
		$('.change_select').bind('change', function(){
			
			var element1        = $("option:selected", "#select1");
			var employees_empno = element1.val();

			var element2        = $("option:selected", "#select2");
			var term_id         = element2.val();

			$('#show_change_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/teacher_loads'); ?>",
				data: {	"term_id": term_id,
						"employees_empno": employees_empno,
						"action": "change_select" },
				dataType: 'json',
				success: function(response) {													

					$('#main_view_teacher_lists').html(response.output); 

					$('#show_change_icon').html("")
										
				}
			});

		});
	});
</script> 
					
					