<style>
	.table td.today { background-color:#DDD; font-size:10pt; text-align:center; }
	.table td.not-today { font-size:10pt; text-align:center; }
	.table th { text-align: center; }	
	.inputxlarge { width :100px }
	tr.approved {color: #71A371; font-weight:bold}
	tr.disapproved {color: #FF0000; }
	tr.noshow {text-decoration: line-through;}
</style>

<div class="container row-fluid span8" >

<!-- Modal View Bookings -->
<div id="modal-view" class="modal hide fade in" style="display: none; width:900px">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h3 id="view_h_val"></h3>
    </div>
    <div class="modal-body">
    	<table class="table table-condensed table-striped">
    		<thead></thead>
    		<tbody id="view-content">
    		</tbody>
    		<tfoot></tfoot>
    	</table>
    </div>
    <div class="modal-footer">
    	<?php if ($this->role == 'ReservationOfficer'): ?>
	        <input class="btn btn-info" type="submit" value="Did Not Show-Up" id="submit-didnotshowup">
	        <input class="btn btn-warning" type="submit" value="For Approval" id="submit-forapproval">
	        <input class="btn btn-danger" type="submit" value="Disapprove" id="submit-disapprove">
	        <input class="btn btn-success" type="submit" value="Approve" id="submit-approve">
        <?php endif; ?>
        <a href="#" class="btn" data-dismiss="modal">Close</a>
    </div>    
</div>



<!-- Modal Entry Form -->
<div id="modal-form" class="modal hide fade in" style="display: none; width:575px;">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times</a>
        <h3 id="form_h_val"></h3>
    </div>
    <div class="modal-body">
        <form class="contact" name="contact">
        	<input type="hidden" id="activity_date" name="activity_date" value="">
        	<label class="label" for="location">Location</label><br>
	        	<select name="location" class="input-xlarge input-xlarge">
	        		<option value="Alumni">Alumni Hall</option>
	        		<option value="BLR">Bates Lecture Room</option>
	        		<option value="GS">Grade School EMC</option>
	        		<option value="HS">High School EMC</option>
	        	</select><br>
			<label class="label" for="activity_name_s">Start Time</label>
				<input type="time" name="activity_time_s" class="input-xlarge inputxlarge">
			<label class="label" for="activity_name_e">End Time</label>
            	<input type="time" name="activity_time_e" class="input-xlarge inputxlarge"><br>
			<label class="label" for="activity_name">Name of Activity</label><br>
				<input type="text" placeholder="Name of Activity" name="activity_name" class="input-xxlarge"><br>
			<label class="label" for="equipment">Equipment Needed</label><br>
				<input type="text" placeholder="Equipment Needed" name="equipment" class="input-xxlarge"><br>
			<label class="label" for="group_size">Group size</label><br>
				<input type="number" placeholder="Group Size" name="group_size" class="input-xxlarge"><br>
			<label class="label" for="message">Remark</label><br>
				<textarea name="remark" placeholder="Remark" class="input-xxlarge"></textarea>
        </form>
    </div>
    <div class="modal-footer">
        <input class="btn btn-success" type="submit" value="Submit" id="submitform">
        <a class="btn" data-dismiss="modal">Cancel</a>
    </div>
</div>
<!-- 
<div id="thanks"><p><a data-toggle="modal" href="#form-content" class="btn btn-primary btn-large">Modal powers, activate!</a></p></div>
-->

<!-- Modal toolate -->
<div id="toolate" class="modal hide fade in" style="display: none; width:575px;">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times</a>
        <h3>Ooops... looks like you are late!</h3>
    </div>
    <div class="modal-body">
    	<p>Filing of reservation must be done at least a day ahead. Please click future dates.</p>
    </div>
    <div class="modal-footer">
        <a class="btn btn-success" data-dismiss="modal">Okay, I Understand.</a>
    </div>
</div>





<table class="table table-bordered" >
	<caption>&nbsp</caption>
	<thead>
		  <tr>
			    <th style="height:20px">
			    	<a href="<?php echo base_url(). $this->role . '/calendar/?month=' . $month . '&year='.$year.'&where=prev'; ?>">Previous</a>
			    </th>
			    <th colspan="5">
					<b><?php echo date("F", mktime(0, 0, 0, $month, 1, $year)).'&nbsp;'.$year;?></b>
			    </th>
			    <th>
			    	<a href="<?php echo base_url() . $this->role . '/calendar/?month='. $month . '&year='.$year.'&where=next'; ?>">Next</a>
			    </th>
		  </tr>
		  <tr>
		    <th width="15%">Sun</th><th width="14%">Mon</th><th width="14%">Tue</th><th width="14%">Wed</th><th width="14%">Thu</th><th width="14%">Fri</th><th width="15%">Sat</th>
		  </tr>
	</thead>
 <?php 
 
 
 $total_days=cal_days_in_month(CAL_GREGORIAN, $month, $year);

// Set empty tables
$first_spaces=date("w", mktime(0, 0, 0, $month, 1, $year));
for ($i=0;$i<$first_spaces;$i++)  echo "<td>&nbsp;</td>";
// Populate calendar tables
$total_bookings=0;
for ($i=1;$i<=$total_days;$i++){
	$dag = date("w", mktime(0, 0, 0, $month, $i, $year));
	$datex    =  $year.'-'.str_pad($month, 2, '0',STR_PAD_LEFT).'-'.str_pad($i, 2, '0',STR_PAD_LEFT);
	

	$open_modal_form_str = "open-modal-form";
	$modal_form_str = "#modal-form";
	
	if ( $this->role !== 'ReservationOfficer' ){
		if 	(date('Ymd') - date('Ymd', strtotime($datex)) > -1){
			$open_modal_form_str  = "#";
			$modal_form_str = "#toolate";
		}
	}
	
	if ($dag==0 && $i<>1) echo "</tr><tr>";
	$all = $av[$datex][0]->total_bookings;
	$fa_count = $av[$datex][0]->for_approval;
	$da_count = $av[$datex][0]->disapproved;
	$total_bookings += $all + $fa_count;
	
		if ($i==date("j") && $year==date("Y") && $month==date("n")){ //today
	?>
			<td class="today">
	<?php 		
		}else{
	?>
			<td class="not-today">
	<?php 				
		}	
	?>					
		<a class="<?php echo $open_modal_form_str;?>" 
				data-toggle="modal" 
				data-caption="Fill-up fields below to reserve for " 
				data-date="<?php echo date('M d, Y', strtotime($datex)); ?>" 
				data-activity_date="<?php echo date('Y-m-d', strtotime($datex)); ?>"   
				href="<?php echo $modal_form_str;?>"><abbr title='Click to book on this date'><strong><?php echo $i; ?></strong></abbr>
		</a><br>		 
	<?php 	
		if ($all > 0)
	?>		  			
	<?php echo ($all>0 ? ('<a class="open-modal-view" 
							data-toggle="modal" 
							data-date="' . $datex . '" 
							data-caption="View Reservations" 
							href="#"><abbr title="Click to view ' . $all .' Reservations on ' . date('M d, Y', strtotime($datex)) . '"><small>' . $all 
						. '</a></abbr>')  : '<small>' ."None") ?>
			</small></td> 
	<?php 	
}

// Set empty tables
$last_spaces = 6-date("w", mktime(0, 0, 0, $month, $total_days, $year));
for ($i=1;$i<=$last_spaces;$i++) echo "<td>&nbsp;</td>";
?> 
  </tr>
  <tfoot>
  <tr><th colspan=7>Total bookings for the month: <strong><?php echo $total_bookings; ?></strong></th></tr>
  </tfoot>
</table>
<figure><strong>Note:</strong> To book, click the date.</figure>
</div>

<?php 
	$approval_location = isset($this->userinfo['approval_location']) ? $this->userinfo['approval_location'] : "";  
	//print_r($approval_location);die();
?>



<script>
$(document).ready(function () {		
	$("input#submitform").click(function(){
		show_loading_msg();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url($this->role.'/add_booking'); ?>",  
			data: $('form.contact').serialize(),
			dataType: "json",
			success: function(data){
		    	hide_loading_msg();
				$("#modal-form").modal('hide');								
				if (data.type == 'Success'){				
					alert(data.message);
					window.location = "<?php echo site_url($this->role . '/calendar'); ?>";
				}
				else{
					show_sticky_msg(data.type,data.message);					
				}
				
			},
			error: function(data){
		    	hide_loading_msg();	
				//$("#modal-form").modal('hide');
		    	show_sticky_msg(data.type,data.message);		    	
			}
		});
	});
});
</script>


<script>
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}


$(document).ready(function () {		
	$("input#submit-approve").click(function(){
		if ($(".checkboxes:checked").length < 1){ 
			alert("Please tick at least one checkbox!");
			return false;
		}
		show_loading_msg();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url($this->role.'/approve_booking'); ?>",  
			data: $(".checkboxes:checked").serialize(),
			dataType: "json",
			success: function(data){
		    	hide_loading_msg();
				$("#modal-view").modal('hide');								
				if (data.type == 'Success'){				
					alert(data.message);
					window.location = "<?php echo site_url($this->role . '/calendar'); ?>";
				}else{
					show_sticky_msg(data.type,data.message);					
				}				
			},
			error: function(data){
				$("#modal-view").modal('hide');								
		    	hide_loading_msg();	
				alert(data.message);
		    	//show_sticky_msg(data.type,data.message);		    	
			}
		});
	});


	$("input#submit-disapprove").click(function(){
		if ($(".checkboxes:checked").length < 1){ 
			alert("Please tick at least one checkbox!");
			return false;
		}
		show_loading_msg();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url($this->role.'/disapprove_booking'); ?>",  
			data: $(".checkboxes:checked").serialize(),
			dataType: "json",
			success: function(data){
		    	hide_loading_msg();
				$("#modal-view").modal('hide');								
				if (data.type == 'Success'){				
					alert(data.message);
					window.location = "<?php echo site_url($this->role . '/calendar'); ?>";
				}else{
					show_sticky_msg(data.type,data.message);					
				}				
			},
			error: function(data){
				$("#modal-view").modal('hide');								
		    	hide_loading_msg();	
				alert(data.message);
			}
		});
	});

	$("input#submit-forapproval").click(function(){
		if ($(".checkboxes:checked").length < 1){ 
			alert("Please tick at least one checkbox!");
			return false;
		}
		show_loading_msg();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url($this->role.'/mark_forapproval'); ?>",  
			data: $(".checkboxes:checked").serialize(),
			dataType: "json",
			success: function(data){
		    	hide_loading_msg();
				$("#modal-view").modal('hide');								
				if (data.type == 'Success'){				
					alert(data.message);
					window.location = "<?php echo site_url($this->role . '/calendar'); ?>";
				}else{
					show_sticky_msg(data.type,data.message);					
				}				
			},
			error: function(data){
				$("#modal-view").modal('hide');								
		    	hide_loading_msg();	
				alert(data.message);
			}
		});
	});

	$("input#submit-didnotshowup").click(function(){
		if ($(".checkboxes:checked").length < 1){ 
			alert("Please tick at least one checkbox!");
			return false;
		}		
		show_loading_msg();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url($this->role.'/mark_didnotshowup'); ?>",  
			data: $(".checkboxes:checked").serialize(),
			dataType: "json",
			success: function(data){
		    	hide_loading_msg();
				$("#modal-view").modal('hide');								
				if (data.type == 'Success'){				
					alert(data.message);
					window.location = "<?php echo site_url($this->role . '/calendar'); ?>";
				}else{
					show_sticky_msg(data.type,data.message);					
				}				
			},
			error: function(data){
				$("#modal-view").modal('hide');								
		    	hide_loading_msg();	
				alert(data.message);
			}
		});
	});

	
});
</script>


	
<script>	
$(document).ready(function () {	
	$(document).on("click", ".open-modal-form", function () {
	     var myCaption = $(this).data('caption') + ' ' + $(this).data('date') + ':';  
	     $(".modal-header #form_h_val").text( myCaption );
	     $(".modal-body #activity_date").val($(this).data('activity_date'));
	     //$("input#activity_date").val($(this).data('activity_date'));
	});
		
});
</script>


<script>
$(document).ready(function () {	
	$('.open-modal-view').bind('click', function(){
		show_loading_msg();		
		$(".modal-header #view_h_val").text('Reservaton for the day');
		$.post("<?php echo site_url($this->role.'/list_all_bookings_of_the_day/'); ?>",  
			{ date : $(this).data('date'), },
			function(data){
				if (data.type == "Success"){
			    	hide_loading_msg();					
					$(".modal-header #view_h_val").text(data.heading);					
					var str = "<tr><th></th><th>ID</th><th>Location</th><th>Time Start</th><th>Time End</th><th>Booked On</th><th>Size</th><th>Booked By</th><th>Status</th><th></th></tr>";
				    $.each(data.data, function (i,item){
					    var istatus = item.status
					    var find = ' ';
					    var re = new RegExp(find, 'g');
					    istatus = istatus.replace(re, '');
					    var klass = 'class=\"' + istatus + '\"';
					    var ankor = '<a href="#" data-toggle="popover" data-content="Equipment: ' + item.equipment + '" data-title=" Activity/Remark: ' + item.activity + '/'  + item.remark +  '">' + item.id + '</a>';
					    var php_approval_location = <?php echo '"' . $approval_location . '"' ; ?> ;
						var approval_location = new Array();
						approval_location = php_approval_location.split(",");
						var phplink = "<?php echo site_url() . $this->role ?>/booking_history/" + item.empno ; 
						var empno_ankor = '<a href="' + phplink + '">' + item.booked_by + '</a>'; 

					    var empno_ankor  =<?php if ($this->role == 'ReservationOfficer') : ?>
					    				empno_ankor;
	    							<?php else : ?>
		    							item.booked_by;
	    							<?php endif; ?>					    		
						
						if (inArray(item.location,approval_location))		    			
					    	var check_str = '<input class=\"checkboxes\" type=\"checkbox\" name=\"check[]\" value="' + item.id + '\">';					    	
	    				else{
					    	var check_str = '<input class=\"checkboxes\" type=\"checkbox\" readonly disabled name=\"check[]\" value="' + item.id + '\">';					    	
						}
					    var chkstr =<?php if ($this->role == 'ReservationOfficer') : ?>
					    				check_str;
					    			<?php else : ?>
					    				'';
					    			<?php endif; ?>					    		
						str +='<tr ' + klass + '><td width=\"5%\">' + (i+1) + '</td>' +
							'<td width=\"06%\">' + ankor + '</td>' +						
   							'<td width=\"12%\">' + item.location + '</td>' +
	   						'<td width=\"12%\">' + item.time_start + '</td>' +
	   						'<td width=\"12%\">' + item.time_end + '</td>' +
	   						'<td width=\"15%\">' + item.booked_on + '</td>' + 
	   						'<td width=\"06%\">' + item.size + '</td>' + 
	   						'<td width=\"15%\">' + empno_ankor + '</td>' +
	   						'<td width=\"10%\">' + item.status + '</td>' +
							'<td width=\"6%\">' + chkstr  + '</td></tr>';	
			    		});
	   				$(".modal-body #view-content").html(str);
	   				$('[data-toggle="popover"]').ggpopover();	   				
					$('#modal-view').modal('show');			
				}else{
					hide_loading_msg();
			    	show_sticky_msg(data.type,data.message);
				}			
			}
		,"json");

	});
});
</script>
