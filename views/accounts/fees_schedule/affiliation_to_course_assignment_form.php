
<div class="row-fluid" style="margin-top:20px;">
<h2 class="heading">Other School Fees Course Assignment</h2>
	<div class="span12">
		<div class="form-horizontal">
			<form method="POST" id="affiliated_fees_form" name="MyForm" />
			<input type="hidden" name="action" value="submit_fee" />
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<select name="academic_year_id" class="academic_years" >
						<?php
							foreach($school_years AS $school_year) {
						?>
							<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>
							 >
								<?php print($school_year->sy); ?> </option>	
						<?php 
							}
						?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Affiliation Fees</div>
				<div class="controls">
					<?php
						if ($affiliation_fees) {
							foreach($affiliation_fees AS $a_fee) {
					?>
						<div id="<?php print($a_fee->academic_years_id); ?>" class="test" >
						<input type="checkbox" name="a_fee[]" value="<?php print($a_fee->id); ?>"  /> 
							<?php print($a_fee->description." - Php".$a_fee->rate); ?>
						</div>	
					<?php 
							}
						}
					?>					
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Course</div>
				<div class="controls">
					<select name="courses_id">
						<?php
							if ($courses) {
								foreach($courses AS $course) {
						?>
							<option value="<?php print($course->id); ?>" >
								<?php print($course->course_code); ?> 
							</option>	
						<?php 
								}
							}
						?>					
					</select>
				</div>
			</div>
			
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<input type="submit" value="Assign Fee/s!" class="btn btn-primary">									
				</div>
				
			</div>
			</form>
			</div>				
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.academic_years').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'change_sy',
			}).appendTo('#affiliated_fees_form');
		$('#affiliated_fees_form').submit();
		});
	});
</script>
					
