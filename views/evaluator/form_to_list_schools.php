<h2 class="heading" style="margin-top:20px;">College Schools</h2>

<div style="text-align:center; width:100%;">
		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; text-align:right;
						border-color: #D8D8D8; padding: 5px; margin-bottom:20px; margin-top:20px; vertical-align:middle;
						width:80%;">
			 	<a href="#" data-toggle="modal" data-target="#modal_add_school" class="btn btn-success"
								data-project-id='<?php //print($student_tor); ?>'><i class="icon-plus icon-white"></i> Add School</a>	 
		</div>		

		<!-- Modal to Create School -->
 		 
		      <div class="modal hide fade" id="modal_add_school" >
		 		<form method="post" id="add_school_form">
		   				<input type="hidden" name="action" value="add_school" />
		   				<?php $this->common->hidden_input_nonce(FALSE); ?>
		   		<div class="modal-header">
		       		<button type="button" class="close" data-dismiss="modal">�</button>
		       		<h3>Create College School</h3>
		   		</div>
		   		<div class="modal-body">
					<div style="text-align:center;">
						<table border="0" cellpadding="2" cellspacing="0" 
							class="table table-striped table-condensed table-hover" style="width:100%;">
							<tr>
								<td width="30%">School Address</td>
								<td>:</td>
								<td>
									<select name="country_id" id="country_id1">
										<option value="00137">Philippines</option>											
											<?php						
												foreach($country_groups AS $country_group) {
													echo "<option value=".$country_group->id.">" . $country_group->name ."</option>";	
												}
											?>					
									</select> 
									<select name="province_id" id="province_id1">						
										<option value="49" class="00137">Bohol</option>											
											<?php 					
												foreach($province_groups AS $province_group) {
													print("<option value=".$province_group->id." class=".$province_group->countries_id.">" . $province_group->name ."</option>");
												}
											?>									
									</select> 
									<select name="towns_id" id="towns_id1">						
										<option value="960" class="49">Tagbilaran City</option>											
											<?php 					
												foreach($town_groups AS $town_group) {
													print("<option value=".$town_group->id." class=".$town_group->provinces_id.">" . $town_group->name ."</option>");
												}
											?>									
									</select> 
								
								<script type="text/javascript" charset="utf-8">
			         				 $(function(){
							              $("#province_id1").chained("#country_id1"); 
							              $("#towns_id1").chained("#province_id1"); 
				           				//   $("#schools_id").chained("#towns_id1"); 
								});
								</script>
								</td>
							</tr>
							<tr>
								<td>School Name</td>
								<td>:</td>
								<td><input type="text" name="school" style="width:400px;"></td>
							</tr>
							<tr>
								<td>School Abbreviation</td>
								<td>:</td>
								<td><input type="text" name="school_abbreviation" style="width:100px;"></td>
							</tr>
						</table>
					</div>
		   		</div>
			   <div class="modal-footer">
		    		<input type="submit" class="btn btn-primary" id="add_school" value="ADD SCHOOL!">
			   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		       </div>
				</form>
		    </div>				
		
	<!--  End of Modal to Create School -->		
						
	<div>
		
	<?php
		if ($schools) {
	?>
		<div style="width:80%; margin-bottom:40px;">
			  <table id="school_list" border="0" cellspacing="0" class="table table-condensed table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
			    <thead>
			    <tr>
			      <th width="8%" style="vertical-align:middle; text-align:center;">Abbreviation</th>
			      <th width="40%" style="vertical-align:middle; text-align:center;">School Name</th>
			      <th width="32%" style="vertical-align:middle; text-align:center;">Address</th>
			      <th width="10%" style="vertical-align:middle; text-align:center;">Status</th>
			      <th width="5%"  style="vertical-align:middle; text-align:center;">Edit</th>
			      <th width="5%"  style="vertical-align:middle; text-align:center;">Delete</th>
			      </tr>
			    </thead>		
		<?php 
			foreach ($schools AS $school) {
		?>
		    <tr>
		      <td style="text-align:left; vertical-align:middle; "><?php print($school->abbreviation); ?> </td>
		      <td style="text-align:left; vertical-align:middle; "><?php print($school->school); ?> </td>
		      <td style="text-align:left; vertical-align:middle; "><?php print($school->school_address); ?></td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($school->status); ?></td>
		    
		     <!-- New Editing Modal -->
		      <td style="text-align:center; vertical-align:middle; ">
				<a href="#" data-toggle="modal" data-target="#modal_id1_<?php print($school->id); ?>" />
					<i class="icon-edit"></i></a>
			 
				<div class="modal hide fade" id="modal_id1_<?php print($school->id); ?>" 
					 school_id="<?php print($school->id); ?>" >
					 <form method="post" id="edit_school_form">
		   				<input type="hidden" name="action" value="edit_school" />
		   				<input type="hidden" name="school_id" value="<?php print($school->id); ?>" />
		   				<?php $this->common->hidden_input_nonce(FALSE); ?>
					    <div class="modal-header">
					       <button type="button" class="close" data-dismiss="modal">�</button>
					       <h3>Edit School</h3>
					    </div>
						<div class="modal-body">            
						      <div id="modalContent_<?php print($school->id); ?>" >
						       text here
							  </div>
						</div>
					   <div class="modal-footer">
					  	   <input type="submit" class="btn btn-primary" id="edit_school" value="EDIT SCHOOL!">
					       <a href="#" class="btn" data-dismiss="modal">Close</a>
					   </div>
				   </form>
				</div>

			  	<script>
				$('#modal_id1_<?php print($school->id); ?>').on('show', function(){
					  var school_id = $(this).attr('school_id');
					  $('#modalContent_<?php print($school->id); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_school_info');?>',
					      data: {school_id: school_id},
					      success: function(data) {
					        $('#modalContent_<?php print($school->id); ?>').html(data); //this part to pass the var
					      }
					  });
					})
				</script>					
		       
			</td>
		 <!-- End Editing Schools -->
		      
		      <?php if($school->status == "INACTIVE"){ ?>
			      <td style="text-align:center; vertical-align:middle; ">
				 		<a class="delete_school" href="<?php print($school->id); ?>"><i class="icon-trash"></i></a>
				 </td>
			 <?php } else {?>
			 	 <td style="text-align:center; vertical-align:middle; ">
			 		<a><i class="icon-trash"></i></a>
			 	 </td>
			<?php }?>	 
		   </tr>
							
		<?php
				}
		?>
		</table>
	</div>
	<?php 		
		}
	?>
  </div>
</div>	



<form id="edit_sch" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="edit_school" />
		
</form>			


<script>
	$('.edit_school').click(function(event){
		event.preventDefault(); 
	
		     var school_id = $(this).attr('href');
		
	  		$('<input>').attr({
			    type: 'hidden',
			    name: 'school_id',
			    value: school_id,
			}).appendTo('#edit_sch');
			$('#edit_sch').submit();
		
	});
</script>


<form id="delete_school_form" method="post" >
	<input type="hidden" name="action" value="delete_school" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>			

<script>
	$('.delete_school').bind('click', function(event){
		event.preventDefault();
		var confirmed = confirm("Continue to delete school?");
							
		if (confirmed){
			var id = $(this).attr('href');
								
			$('<input>').attr({
			    type: 'hidden',
			    name: 'school_id',
			    value: id,
			}).appendTo('#delete_school_form');
			$('#delete_school_form').submit();
		}		
	});
</script>


<script type="text/javascript" charset="utf-8">
    $(function(){
        $("#province_id").chained("#country_id"); 
        $("#towns_id").chained("#province_id"); 
           
	});
</script>


<script>
$().ready(function() {
	$('#school_list').dataTable( {
	    "aoColumnDefs": [{ "bSearchable": true, "aTargets": [ 1,2,3,4 ] }],
		"iDisplayLength": 50,
	    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],	    
    });
    $("#school_list").stickyTableHeaders();
    $('#school_list').stickyTableHeaders('updateWidth');
	
	
})
</script>


 
 
 <script type="text/javascript" charset="utf-8">
          $(function(){
              $("#province_id").chained("#country_id"); 
              $("#towns_id").chained("#province_id"); 
              $("#schools_id").chained("#towns_id"); 
});
</script>		
