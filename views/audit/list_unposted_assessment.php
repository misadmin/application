 
<h2 class="heading"> LIST OF STUDENTS WITH UNPOSTED ASSESSMENT (as of <?php echo date('M-d-Y'); ?> )</h2>	

<table class="table table-striped table-bordered">
	<thead >
		<tr align="center">
			<th>No.</th>
			<th>ID No.</th>
			<th>Name</th>
			<th>Program</th>
			<th>Year Level</th>
			
		</tr>
	</thead>
	<tbody> 
<?php if($list){ 
			$num=1; ?>
<?php foreach ($list as $l): ?>
		<tr>
			<td><?php echo $num; ?></td>
			<td><?php echo $l->idno; ?></td>
			<td><?php echo $l->name; ?></td>
			<td><?php echo $l->abbreviation; ?></td>
			<td><?php echo $l->year_level; ?></td>
		</tr>
<?php 
		$num++;
	endforeach; ?>
<?php } ?>
	</tbody>
</table>
