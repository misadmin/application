<?php

	class Shs_fix_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

	
		function List_CourseOfferings() {

			$result = null;
			
			$q1 = "SELECT 
							a.id AS course_offerings_id,
							c.block_sections_id,
							a.academic_terms_id,
							b.course_code,
							f.abbreviation,
							e.section_name,
							a.courses_id,
							(SELECT 
									GROUP_CONCAT(CONCAT(TIME_FORMAT(s.from_time,'%l:%i%p'),'-',TIME_FORMAT(s.to_time,'%l:%i%p'),' ',s.day_name) SEPARATOR '\n') 
								FROM 
									room_occupancy AS s,
									room_occupancy_course_offerings_slots AS t,
									course_offerings_slots AS u 
								WHERE 
									s.id = t.room_occupancy_id 
									AND t.course_offerings_slots_id = u.id 
									AND u.course_offerings_id = a.id 
							) AS sched,
							(SELECT 
									COUNT(s.id) 
								FROM 
									room_occupancy AS s,
									room_occupancy_course_offerings_slots AS t,
									course_offerings_slots AS u 
								WHERE 
									s.id = t.room_occupancy_id 
									AND t.course_offerings_slots_id = u.id 
									AND u.course_offerings_id = a.id 
							) AS cnt				
							
						FROM 
							course_offerings AS a,
							courses AS b,
							block_course_offerings AS c,
							block_sections AS d,
							shs_block_sections AS e,
							academic_programs AS f
						WHERE 
							a.courses_id = b.id
							AND a.id = c.course_offerings_id 
							AND c.block_sections_id = d.id 
							AND d.id = e.block_sections_id
							AND d.academic_programs_id = f.id
							AND a.id IN (SELECT 
												x.course_offerings_id 
											FROM 
												enrollments AS x
										)
						ORDER BY 
							cnt,
							a.courses_id,
							f.abbreviation,
							e.section_name ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function ListSame_Offerings($block_sections_id, $academic_terms_id, $courses_id) {

			$result = null;
			
			$q1 = "SELECT 
							a.id,
							c.block_sections_id,
							a.academic_terms_id,
							b.course_code,
							f.abbreviation,
							e.section_name,
							a.courses_id,
							g.id AS course_offerings_slots_id,
							(SELECT 
									GROUP_CONCAT(CONCAT(TIME_FORMAT(s.from_time,'%l:%i%p'),'-',TIME_FORMAT(s.to_time,'%l:%i%p'),' ',s.day_name) SEPARATOR '\n') 
								FROM 
									room_occupancy AS s,
									room_occupancy_course_offerings_slots AS t,
									course_offerings_slots AS u 
								WHERE 
									s.id = t.room_occupancy_id 
									AND t.course_offerings_slots_id = u.id 
									AND u.course_offerings_id = a.id 
							) AS sched				
						FROM 
							course_offerings AS a,
							courses AS b,
							block_course_offerings AS c,
							block_sections AS d,
							shs_block_sections AS e,
							academic_programs AS f,
							course_offerings_slots AS g
						WHERE 
							a.courses_id = b.id
							AND a.id = c.course_offerings_id 
							AND c.block_sections_id = d.id 
							AND d.id = e.block_sections_id
							AND d.academic_programs_id = f.id
							AND a.id = g.course_offerings_id
							AND d.id = {$this->db->escape($block_sections_id)} 
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND a.courses_id = {$this->db->escape($courses_id)} 
							AND a.id NOT IN (SELECT 
												x.course_offerings_id 
											FROM 
												enrollments AS x
										)
						ORDER BY 
							a.courses_id,
							f.abbreviation,
							e.section_name ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function Update_Course_Offerings_Slots($course_offerings_slots_id=NULL, $new_offerings_id=NULL) {

			$q1 = "UPDATE course_offerings_slots
						SET 
							course_offerings_id = {$this->db->escape($new_offerings_id)}
						WHERE
							id = {$this->db->escape($course_offerings_slots_id)} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
			
		}

		
	}
	
?>