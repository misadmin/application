

				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==3) { 
			            				print("class=\"active\""); 
			            		  } elseif (!isset($old_bldgs_id)) { 
			            		  		print("class=\"active\""); 
								  } ?> ><a href="#rmtab2" data-toggle="tab">Bates</a></li> 
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==7) { print("class=\"active\""); } ?> ><a href="#rmtab3" data-toggle="tab">Freinademetz</a></li>
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==9) { print("class=\"active\""); } ?> ><a href="#rmtab4" data-toggle="tab">Scanlon</a></li>
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==11) { print("class=\"active\""); } ?> ><a href="#rmtab5" data-toggle="tab">Other</a></li>
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==13) { print("class=\"active\""); } ?> ><a href="#rmtab6" data-toggle="tab">HS Gym</a></li>
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==14) { print("class=\"active\""); } ?> ><a href="#rmtab7" data-toggle="tab">Activity Center</a></li>
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==15) { print("class=\"active\""); } ?> ><a href="#rmtab8" data-toggle="tab">Covered Court</a></li>
					</ul>
			       
			       <div class="tab-content">
			                
			       	    <div style="background-color:#D9DDFC;" class="tab-pane <?php 
			       	    						if (isset($old_bldgs_id) AND $old_bldgs_id==3) { 
													print("active"); 
												} elseif (!isset($old_bldgs_id)) { 
													print("active"); 
												} ?>" id="rmtab2">
			           		<table class="table">
				           		<tr>
								<?php
						         	$checked_1st_rm = "checked";
						         	$cnt=1;
						         	if ($bates_building) {
										foreach($bates_building as $bates) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $bates->id)) {
													print("<td>
														<input type=\"radio\" name=\"room_id\" value=".$bates->id." checked />"
														.$bates->room_no."</td>");
												} else {
													print("<td>
														<input type=\"radio\" name=\"room_id\" value=".$bates->id." ".$checked_1st_rm." />"
														.$bates->room_no."</td>");
												}
												$cnt++;
												$checked_1st_rm = "";
											} else {
												if (isset($rooms_id) AND ($rooms_id == $bates->id)) {
													print("</tr><tr><td>
														<input type=\"radio\" name=\"room_id\" value=".$bates->id." checked />"
														.$bates->room_no."</td>");
												} else {
													print("</tr><tr><td>
														<input type=\"radio\" name=\"room_id\" value=".$bates->id." />"
														.$bates->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>
			     		</div>
			       		
			           	<div style="background-color:#CEF7DC;" class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==7) { print("active"); } ?>" id="rmtab3">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	if ($freina_building) {
										foreach($freina_building as $freina) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $freina->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$freina->id." checked />"
													.$freina->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$freina->id." ".$checked_1st_rm." />"
													.$freina->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($rooms_id) AND ($rooms_id == $freina->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$freina->id." checked />"
													.$freina->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$freina->id." />"
													.$freina->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}	
								?>
				           		</tr>	
				         	</table>
			    		</div>
			            
			      		<div style="background-color:#E5E8C8;" class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==9) { print("active"); } ?>" id="rmtab4">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	if ($scanlon_building) {
										foreach($scanlon_building as $scan) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $scan->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$scan->id." checked />"
													.$scan->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$scan->id." ".$checked_1st_rm." />"
													.$scan->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($rooms_id) AND ($rooms_id == $scan->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$scan->id." checked />"
													.$scan->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$scan->id." />"
													.$scan->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>

			        	</div>

			      		<div style="background-color:#FCDCE4;" class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==11) { print("active"); } ?>" id="rmtab5">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	//print_r($old_bldgs_id); die();
						         	if ($other_building) {
										foreach($other_building as $other) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $other->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$other->id." checked />"
													.$other->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$other->id." ".$checked_1st_rm." />"
													.$other->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($rooms_id) AND ($rooms_id == $other->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$other->id." checked />"
													.$other->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$other->id." />"
													.$other->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>

			        	</div>
			        	
			      		<div style="background-color:#F7DC6F;" class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==13) { print("active"); } ?>" id="rmtab6">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	//print_r($old_bldgs_id); die();
						         	if ($hs_gym) {
										foreach($hs_gym as $hs) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $hs->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$hs->id." checked />"
													.$hs->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$hs->id." ".$checked_1st_rm." />"
													.$hs->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($hs) AND ($rooms_id == $hs->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$hs->id." checked />"
													.$hs->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$hs->id." />"
													.$hs->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>

			        	</div>

			      		<div style="background-color:#D7BDE2;" class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==14) { print("active"); } ?>" id="rmtab7">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	//print_r($old_bldgs_id); die();
						         	if ($activity_center) {
										foreach($activity_center as $ac) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $ac->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$ac->id." checked />"
													.$ac->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$ac->id." ".$checked_1st_rm." />"
													.$ac->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($ac) AND ($rooms_id == $ac->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$ac->id." checked />"
													.$ac->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$ac->id." />"
													.$ac->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>

			        	</div>

			      		<div style="background-color:#E8DAEF;" class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==14) { print("active"); } ?>" id="rmtab8">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	//print_r($old_bldgs_id); die();
						         	if ($covered_court) {
										foreach($covered_court as $cc) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $cc->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$cc->id." checked />"
													.$cc->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$cc->id." ".$checked_1st_rm." />"
													.$cc->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($rooms_id) AND ($rooms_id == $cc->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$cc->id." checked />"
													.$cc->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$cc->id." />"
													.$cc->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>

			        	</div>






			        </div>
			    </div> <!-- /tabbable -->
.