<pre id="receipt_to_print" style="display:none;"><?php


$counter = 0;
foreach ($items as $item){
	
	$this->print_lib->set_table_header(array(array('id'=>'student', 'name'=>$names[$counter], 'width'=>50), array('id'=>'course_year', 'name'=>$course_year, 'width'=>30), array('id'=>'date', 'name'=>$date, 'width'=>20)));
	$remarks_arr = explode("\n", $remarks[$counter]);
?><?php
//this should be placed outside the html tag or body to be accessible, maybe after $this->content_lib->content();
//echo sprintf($this->config->item('jzebra_applet'), base_url()); ?>
 
 
 
  
<?php echo $this->print_lib->bold() . $this->print_lib->align_right($id_number . " " . $item_codes[$counter]) . "\n\n"; ?>
<?php 
echo $this->print_lib->draw_table(80, " "); ?>

<?php $this->print_lib->set_table_header(array(array('id'=>'payment', 'name' => "*" . convert_number_to_words($payments[$counter]) . "*", 'width'=>80), array('id'=>'payment', 'name'=> str_pad(( string )number_format($payments[$counter], 2), 12, '*', STR_PAD_LEFT), 'width'=>20))); ?>

<?php echo $this->print_lib->draw_table(80, " "); ?>

*<?php echo $items[$counter] . " "; ?>*
<?php 
echo $this->print_lib->condensed();
echo "\n";
$this->print_lib->clear_table(); 
$this->print_lib->set_table_header(array(array('id'=>'remarks', 'name'=>(empty($remarks_arr[0])? " " : $remarks_arr[0]), 'width'=>40), array('id'=>'payment', 'name'=>'Payment', 'width'=>15), array('id'=>'checkdate', 'name'=>'Check/Expiry Date', 'width'=>15), array('id'=>'bank', 'name'=>'Bank', 'width'=>15), array('id'=>'check_no', 'name'=>'Check/Credit/Acct No.', 'width'=>15)));
$extra = 0;
$remarks_count = 0;
$data = array();
if (count($payment_methods) >= count($remarks_arr)) {
	foreach ($payment_methods as $payment_method){
		$extra++;
		$remarks_count++;
		$data[] = array('remarks'=>(empty($remarks_arr[$remarks_count])? " " : $remarks_arr[$remarks_count]), 'payment'=>$payment_method['type'], 'checkdate'=>(isset($payment_method['date'])?$payment_method['date']:''), 'bank'=>(isset($payment_method['bank'])?$payment_method['bank']:''), 'check_no'=>(isset($payment_method['no'])?$payment_method['no']:''));
	} 
} else {
	$payment_count = 0;
	foreach ($remarks_arr as $fremark){
		$extra++;
		$remarks_count++;
		$data[] = array('remarks'=>(empty($fremark[$remarks_count])?" " : $fremark[$remarks_count]), 'payment'=>(isset($payment_methods[$payment_count]['type']) ? $payment_methods[$payment_count]['type'] : ""), 'checkdate'=>(isset($payment_methods[$payment_count]['date'])?$payment_methods[$payment_count]['date']:''), 'bank'=>(isset($payment_methods[$payment_count]['bank'])?$payment_methods[$payment_count]['bank']:''), 'check_no'=>(isset($payment_methods[$payment_count]['no'])?$payment_methods[$payment_count]['no']:''));
		$payment_count++;
	}
}
$this->print_lib->set_data($data);
echo $this->print_lib->draw_table(120, " ", TRUE) . "\n";?>
<?php 
$this->print_lib->clear_table();
$this->print_lib->set_table_header(array(array('id'=>'remarks', 'name'=>" ", 'width'=>40), array('id'=>'payment', 'name'=>$trace_strings[$counter], 'width'=>60)));
echo $this->print_lib->draw_table(120, " ");
echo "\n";
for ($count = 2; $count > $extra; $count--){
	echo "\n";
}
?>      Please keep this receipt. Have a nice day.<?php echo $this->print_lib->regular() . $this->print_lib->no_condensed(); ?>
 
<?php $this->print_lib->line_feed(); ?>
<?php $this->print_lib->line_feed(); ?>
<?php $this->print_lib->line_feed(); ?>
<?php $this->print_lib->line_feed(); ?>
<?php $this->print_lib->line_feed(); ?> 
<?php $counter++; } ?>
</pre>
<script>
function print(content) {
    var applet = document.jzebra;
    if (applet != null) {
       applet.append('\x1B\x40');
       applet.append(content);
       applet.print();
	}
}
    
$(document).ready(function(){
	var content_to_print = $('#receipt_to_print').text();	
	var applet = document.jzebra;
	
	if (applet != null) {
		// Searches for default printer
		applet.findPrinter();
	}
	print(content_to_print);
});
</script>