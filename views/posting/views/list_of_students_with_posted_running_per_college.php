
<?php 
	if ($reports) {	
		foreach($reports AS $program) {
			if ($program->program_students) {
?>
				<div style="border:1px solid #D0D0D0; font-weight:bold; padding:8px; font-size:14px; margin-bottom:5px; background-color:#BFFDD8;">
					<?php print($program->abbreviation."-".$program->year_level); ?>
				</div>
				<div style="width:100%;" >
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr class="myhead">
								<td class="head1" style="width:5%;" rowspan="2">Count</td>
								<td class="head1" style="width:9%;" rowspan="2">ID<br>Number</td>
								<td class="head1" style="width:15%;" rowspan="2">Lastname</td>
								<td class="head1" style="width:15%;" rowspan="2">Firstname</td>
								<td class="head1" style="width:10%;" rowspan="2">Middlename</td>
								<td class="head1" style="width:23%;" colspan="3">POSTED</td>
								<td class="head1" style="width:23%;" colspan="3">RUNNING</td>
							</tr>
							<tr class="myhead">
								<td class="head1" style="width:9%;">Course</td>
								<td class="head1" style="width:4%;">Year</td>
								<td class="head1" style="width:10%;">Amount</td>
								<td class="head1" style="width:9%;">Course</td>
								<td class="head1" style="width:4%;">Year</td>
								<td class="head1" style="width:10%;">Amount</td>
							</tr>
						</thead>
						
						<tbody>
							<?php

									$cnt=1;
									foreach($program->program_students AS $student) {
										if (number_format($student->amount_posted,2) != number_format($student->amount_running,2)) {
											$bg = "#FF9B94";
										} else {
											$bg = "#FFFFFF";
										}	
							?>			
										<tr>
											<td style="text-align:right; background-color: <?php print($bg); ?>">
												<?php print($cnt); ?>.
											</td>
											<td style="text-align:center; background-color: <?php print($bg); ?>">
												<a href="<?php print(site_url($this->uri->segment(1).'/student')."/".$student->idno);?>" >
													<?php print($student->idno); ?>
												</a>
											</td>
											<td style="background-color: <?php print($bg); ?>"><?php print($student->lname); ?></td>
											<td style="background-color: <?php print($bg); ?>"><?php print($student->fname); ?></td>
											<td style="background-color: <?php print($bg); ?>"><?php print($student->mname); ?></td>
											<td style="background-color: <?php print($bg); ?>"><?php print($student->course_posted); ?></td>
											<td style="text-align:center; background-color: <?php print($bg); ?>"><?php print($student->yr_level_posted); ?></td>
											<td style="text-align:right; background-color: <?php print($bg); ?>"><?php print(number_format($student->amount_posted,2)); ?></td>
											<td style="background-color: <?php print($bg); ?>"><?php print($student->course_running); ?></td>
											<td style="text-align:center; background-color: <?php print($bg); ?>"><?php print($student->yr_level_running); ?></td>
											<td style="text-align:right; background-color: <?php print($bg); ?>"><?php print(number_format($student->amount_running,2)); ?></td>
										</tr>
							<?php 
										$cnt++;
									}
							?>
						</tbody>
					</table>
				</div>
<?php 
			}
		}
	}
?>