<h2 class="heading"> SUMMARY OF UNPOSTED ADJUSTMENTS (as of <?php echo date('Y-m-d'); ?> )</h2>

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">

<form id="change_user" method="POST">
	    <select name="user_id" id="user_id" style="width:auto;">
				<option value="0" <?php if($selected_id == 0) print("selected"); ?>>All Users</option>
	    		<?php
					foreach($users AS $user) {
						if ($user->transact_id == $selected_id) {
							print("<option value=\"$user->transact_id\" selected>$user->transact_by</option>");
						} else {
							print("<option value=\"$user->transact_id\">$user->transact_by</option>");
						}
					}
				?>
		</select>

</form>	
</div>

<form action="batch_adjustment_posting" method="post" id="adjust_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="2" name="step" />
	
	<div class="row-fluid">
		<table class="table table-bordered table-hover table-condensed">
			<thead>
				<tr>
				      <th style="text-align:center; width:5%;">No.</th>
				      <th style="text-align:center; width:5%;">Transaction<br>Date</th>
					  <th style="text-align:center; width:5%;">Adjustment<br>ID</th>
				      <th style="text-align:center; width:5%; vertical-align: middle;">ID No.</th>
					  <th style="text-align:center; width:23%; vertical-align: middle;">Name</th>
				      <th style="text-align:center; width:15%; vertical-align: middle;">Description</th>
				      <th style="text-align:center; width:15%; vertical-align: middle;">Transacted By</th>
					  <th style="text-align:center; width:7%; vertical-align: middle;">Debit</th>
					  <th style="text-align:center; width:7%; vertical-align: middle;">Credit</th>
					  <th style="text-align:center; width:2%; vertical-align: middle;">Del.</th>
				</tr>
		    </thead>
		    
		    <?php
		    //print_r($adjustments);die();
			if ($adjustments) {
				$debit = 0;
				$credit = 0;
				$i=0;
				foreach ($adjustments AS $adj) { $i++;

			?>
		    
		 	<tr> 

		      <td style="text-align:center"><?php print($i); ?></td>
		      <td style="text-align:center"><?php print($adj['transaction_date']); ?></td>
			  <td style="text-align:center"><?php print($adj['adjustment_id']); ?></td>
			  <td style="text-align:center"> <?php print($adj['idno']);?> </td>
			  <td style="text-align:left"><a href="" title="<?php echo stripslashes($adj['remark']); ?>" ><?php print($adj['name']); ?></a></td>
			  <td style="text-align:left"><?php print($adj['description']); ?></td>
			  <td style="text-align:left"><?php print($adj['transact_by']); ?></td>
		      
		      <?php if ($adj['adjustment_type'] == 'Debit') { ?>
					  <td style="text-align:right; vertical-align:middle; "> <?php print(number_format($adj['amount'],2,'.',',')); ?> </td>
					  <td style="text-align:left; vertical-align:middle; "> </td>
			  <?php   $debit += $adj['amount']; 
			  		} else if ($adj['adjustment_type'] == 'Credit') { ?>
					  <td style="text-align:left; vertical-align:middle; "> </td>
			  	      <td style="text-align:right; vertical-align:middle; "> <?php print(number_format($adj['amount'],2,'.',',')); ?> </td>
			  <?php   $credit += $adj['amount']; 
			  		} ?>
			  <td style="text-align:center; vertical-align:middle;">
			  <?php 
			  		if ($login_user != $adj['by_user']) {
			  ?>
			  <i class="icon-asterisk"></i>
			  <?php 
			  		} else {
			  ?>
      					<a class="delete_adjustment" href="<?php print($adj['adjustment_id']); ?>">
        <i class="icon-trash"></i></a>
        	<?php 
			  		}
        	?>
        </td>
			
			</tr>
	    <?php } ?>
			<th  colspan="7" style="text-align:right; vertical-align:middle; ">TOTAL</th>
			<td style="text-align:right; vertical-align:right; "><?php print(number_format($debit,2,'.',',')); ?></td>
			<td style="text-align:right; vertical-align:right; "><?php print(number_format($credit,2,'.',',')); ?></td>
			<td style="text-align:right; vertical-align:right; "></td>
		<?php }	?>
		</table>
			<button class="btn btn-primary batch_post" type="submit">POST ADJUSTMENTS!</button>	
			<?php  if (isset($adj)) {  ?>	
				<a href="<?php echo site_url("posting/Print_adjustments_pdf/".$selected_id)?>" target="_blank">PRINT! <i class="icon-download-alt"></i></a>	
			<?php
				}
			?> 

	
	</div>
</form> 


<form action="batch_adjustment_posting" method="post" id="adjust_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="2" name="step" />
</form>

<script>
	$('.batch_post').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to post all adjustments?');

		if (confirmed){
			$('#adjust_form').submit();
		} 
		
	});

	$(function() {
		$('.adjustment_item').tooltip();
	});

</script>
<form id="delete_adjustment" action="<?php echo site_url("posting/batch_adjustment_posting")?>" method="post">
  <input type="hidden" name="step" value="3" />

</form>

<script>
	$('.delete_adjustment').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to remove this adjustment?');

		if (confirmed){
			var adjustment_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'adjustment_id',
		    value: adjustment_id,
		}).appendTo('#delete_adjustment');
		$('#delete_adjustment').submit();
		} 
		
	});
</script>


<script>
	$(document).ready(function(){
		$('#user_id').bind('change', function(){
			show_loading_msg();
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 'change_user',
			}).appendTo('#change_user');
			$('#change_user').submit();
		});
	});
</script>
