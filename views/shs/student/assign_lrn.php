
<style>
	.lrn1 {
		float:left;
		width:10%;
		text-align:right;
		padding-top:10px;
		font-weight:bold;
		font-size:15px;
	}
	
	.lrn2 {
		float:left;
		width:70%;
		padding-left:10px;
	}

</style>

	<div style="padding:5px; font-weight:bold; fontsize:14px; margin-bottom:5px; color:#B50303;" id="lrn_msg">
		<?php 
			if ($lrn) {
				print("Student assigned an LRN!");
			}
		?>
	</div>

	<div style="width:100%;" id="show_student_lrn" >
		<div style="width:55%; float:left;" >

				<div style="width:100%; overflow:auto;">
						<div class="lrn1">
							LRN:
						</div>
						<div class="lrn2" >
							<div style="float:left;">
								<input type="text" name="lrn" id="lrn" placeholder="LRN" class="input-xlarge search_lrn" style="width:150px; font-size:16px; font-weight:bold; height:30px;" value="<?php print($lrn); ?>" autofocus />
							</div>
							<div style="float:left; padding-top:8px; padding-left:10px; font-weight:bold; color:#BA0303;" id="show_search_lrn_icon">
								
							</div>
						</div>
				</div>
				<div style="width:100%;">
						<div class="lrn1">
							&nbsp;
						</div>
						<div class="lrn2">
							<div style="float:left;">
								<a href="#" class="btn btn-primary assign_lrn" 
									students_idno="<?php print($students_idno); ?>">Assign LRN!
								</a>
							</div>
							<div style="float:left; padding-top:8px; padding-left:10px; font-weight:bold; color:#BA0303;" id="show_lrn_icon">
								
							</div>
						</div>
						
				</div>
		</div>

		<div id="list_of_students" style="width:45%; float:left; margin:0 auto;">

		</div>
	</div>

	
<script>
	$('.assign_lrn').click(function(e){
		e.preventDefault();		
		
		var students_idno = $(this).attr('students_idno');
		var lrn           = $('#lrn').val();
		
		$('#show_search_lrn_icon').html(''); 	
		$('#list_of_students').html('');						

		if (lrn) {

			var lrn = $('#lrn').val();
			
			$('#show_search_lrn_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/new_shs_student'); ?>",
				data: {"lrn": lrn,
						"action": "search_lrn" },
				dataType: 'json',
				success: function(response) {													
					if (response.found) {
						$('#show_search_lrn_icon').html('LRN already assigned to another student!'); 
						$('#lrn').val(''); 
						$('#lrn').focus();
						$('#list_of_students').html(response.output);
					} else {

						var confirmed = confirm('Assign LRN to student?');
						
						$('#show_search_lrn_icon').html('');
						
						if (confirmed){
					
							$('#show_lrn_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
								
							$.ajax({
								cache: false,
								type: 'POST',
								url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+students_idno,
								data: { "lrn": lrn,
										action: 'assign_lrn'},
								dataType: 'json',
								success: function(response) {				

									$('#show_student_lrn').html(response.output); 	
									
									$('#show_lrn_icon').html("");

									if (!response.success) {
										$('#lrn_msg').html("LRN NOT assigned to student!");										
									}
								}
							});
							
						}
						
						$('#lrn').focus();
					}
				}
			});

		} else {
			alert("Blank LRN not allowed!");
			$('#lrn').focus();
		}
		
	});
	
</script>	

