
<style>
	td.report_header {
		text-align: center;
		vertical-align:middle;
		font-weight:bold;
		font-size:14px;
	}
	
	td.m {
		text-align:center;
		vertical-align:middle;
		font-weight:bold;
		color:#032FBF;
	}
	td.f {
		text-align:center;
		vertical-align:middle;
		font-weight:bold;
		color:#CD062E;
	}
	td.total {
		text-align:center;
		vertical-align:middle;
		font-weight:bold;
	}
	
</style>

	<div style="width:75%;">
			<table class="table table-hover table-bordered table-striped" >
				<thead>
					<tr style="font-size:20px; font-weight:bold;">
						<td colspan="10" style="text-align:center;">
							<div style="height:24px;">
								Senior High School
							</div>
							<div style="height:24px;">
								Enrollment Summary
							</div>
							<div style="margin-top:5px; margin-bottom:10px; font-size:18px; font-style:italic; color:#0C6E05;">
								<?php print($term_sy); ?>
							</div>	
						</td>
					</tr>
					<tr class="shs_header">
						<td class="report_header" style="width:40%;" rowspan="2">Strand</td>
						<td class="report_header" style="width:20%;" colspan="3">Grade 11</td>
						<td class="report_header" style="width:20%;" colspan="3">Grade 12</td>
						<td class="report_header" style="width:20%;" colspan="3">Total</td>
					</tr>
					<tr class="shs_header"> 
						<td class="report_header" style="width:7%;">M</td>
						<td class="report_header" style="width:7%;">F</td>
						<td class="report_header" style="width:6%;">Total</td>
						<td class="report_header" style="width:7%;">M</td>
						<td class="report_header" style="width:7%;">F</td>
						<td class="report_header" style="width:6%;">Total</td>
						<td class="report_header" style="width:7%;">M</td>
						<td class="report_header" style="width:7%;">F</td>
						<td class="report_header" style="width:6%;">Total</td>
					</tr>
				</thead>
				<tbody>
					<?php 
						if ($enrollees) {
							$gtotal_m11=0; $gtotal_f11=0; $gtotal_11=0; $gtotal_m=0;
							$gtotal_m12=0; $gtotal_f12=0; $gtotal_12=0; $gtotal_f=0;
							foreach($enrollees AS $enrol) {
					?>
								<tr>
									<td>
										<?php print($enrol->description); ?>
									</td>
									<td class="m">
										<?php print($enrol->m11); $gtotal_m11 += $enrol->m11; ?>
									</td>
									<td class="f">
										<?php print($enrol->f11); $gtotal_f11 += $enrol->f11;  ?>
									</td>
									<td class="total">
										<?php 
											$gtotal_11 += ($enrol->m11+$enrol->f11);
											
											if (($enrol->m11+$enrol->f11) > 0) {
										?>						
												<a href="<?php print($enrol->id); ?>" 
													grade_level="11" 
													strand_name="<?php print($enrol->description); ?>"
													class="list_students">
													<?php print($enrol->m11+$enrol->f11); ?>
												</a>
										<?php
											} else {
												print($enrol->m11+$enrol->f11);  
											}
										?>
									</td>
									<td class="m">
										<?php print($enrol->m12); $gtotal_m12 += $enrol->m12;  ?>
									</td>
									<td class="f">
										<?php print($enrol->f12); $gtotal_f12 += $enrol->f12;  ?>
									</td>
									<td class="total">
										<?php 
											$gtotal_12 += ($enrol->m12+$enrol->f12);
											
											if (($enrol->m12+$enrol->f12) > 0) {
										?>						
												<a href="<?php print($enrol->id); ?>" 
													grade_level="12" 
													strand_name="<?php print($enrol->description); ?>"
													class="list_students">
													<?php print($enrol->m12+$enrol->f12); ?>
												</a>
										<?php
											} else {
												print($enrol->m12+$enrol->f12);  
											}
										?>
									</td>
									<td class="m">
										<?php print($enrol->m11+$enrol->m12); $gtotal_m += ($enrol->m11+$enrol->m12); ?>
									</td>
									<td class="f">
										<?php print($enrol->f11+$enrol->f12); $gtotal_f += ($enrol->f11+$enrol->f12);  ?>
									</td>
									<td class="total">
										<?php print($enrol->m11+$enrol->f11+$enrol->m12+$enrol->f12); ?>
									</td>
								</tr>
					<?php 
							}
						}
					?>
				</tbody>
				<tfoot>
					<tr class="shs_header">
						<td style="text-align:right; font-weight:bold;" >
							GRAND TOTAL:
						</td>
						<td class="m">
							<?php print($gtotal_m11); ?>
						</td>
						<td class="f">
							<?php print($gtotal_f11); ?>
						</td>
						<td class="total">
							<?php print($gtotal_11); ?>
						</td>
						<td class="m">
							<?php print($gtotal_m12); ?>
						</td>
						<td class="f">
							<?php print($gtotal_f12); ?>
						</td>
						<td class="total">
							<?php print($gtotal_12); ?>
						</td>
						<td class="m">
							<?php print($gtotal_m); ?>
						</td>
						<td class="f">
							<?php print($gtotal_f); ?>
						</td>
						<td class="total">
							<?php print($gtotal_m + $gtotal_f); ?>
						</td>						
					</tr>
				</tfoot>
			</table>
	</div>
	

<script>
	$('.list_students').click(function(event){
		event.preventDefault();

		var strand_id   = $(this).attr('href');
		var grade_level = $(this).attr('grade_level');
		var strand_name = $(this).attr('strand_name');

		var element1          = $("option:selected", "#academic_terms_id");
		var academic_terms_id = element1.val();
		var term_sy           = element1.attr('sy');

		$('#modal_list_students').modal('show');

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/shs_enrollment_summary'); ?>",
			data: {"strand_id": strand_id,
					"grade_level": grade_level,
					"academic_terms_id": academic_terms_id,
					"action": "extract_list_of_enrolled_students" },
			dataType: 'json',
			success: function(response) {
				$('#show_list_of_enrolled_students').html(response.output); 				
			}
		});

		$('#strand_name_modal').html(strand_name);
		$('#grade_level_modal').html("Grade "+grade_level);
		$('#term_sy_modal').html(term_sy);
		
	});


</script>


	<div id="modal_list_students" class="modal hide fade" style="width:750px; margin-left: -375px;" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" style="text-align:left;">Enrolled Students</h3>
				</div>
				<div class="modal-body" style="text-align:left; font-size:15px;">
					<div style="font-size:17px; font-weight:bold; color:#017501;" id="strand_name_modal"></div>
					<div style="font-size:15px; font-weight:bold; color:#05057D;" id="grade_level_modal"></div>
					<div style="font-size:12px; font-style:italic;" id="term_sy_modal"></div>
					<div id="show_list_of_enrolled_students"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
				</div>
			</div>
		</div>
	</div>



<form id="download_enrollment_summary_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_enrollment_summary_pdf" />
</form>

<script>
	$('.download_enrollment_summary_pdf').click(function(event){
		event.preventDefault(); 
		var enrollees = '<?php print(json_encode($enrollees)); ?>';

		var element1          = $("option:selected", "#academic_terms_id");
		var term_sy           = element1.attr('sy');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'enrollees',
			value: enrollees,
		}).appendTo('#download_enrollment_summary_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'term_sy',
			value: term_sy,
		}).appendTo('#download_enrollment_summary_form');	
		$('#download_enrollment_summary_form').submit();
		
	});
</script>

	