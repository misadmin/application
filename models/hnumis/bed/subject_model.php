<?php

	class subject_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListSubjects($owner_colleges_id=NULL) {

			$result = null;
					
			$q2 = "SELECT
							a.id,
							a.course_code,
							a.course_types_id,
							a.courses_groups_id,
							a.descriptive_title,
							a.paying_units,
							a.credit_units,
							a.status,
							b.type_description
						FROM
							courses AS a,
							course_types AS b
						WHERE
							a.course_types_id = b.id
							AND a.owner_colleges_id={$this->db->escape($owner_colleges_id)} 
						ORDER BY
							a.course_code,
							a.descriptive_title";
				
			
			$query = $this->db->query($q2);
							
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
							
			return $result;

		}


		function getCourseCode($course_code,$original_course_code=FALSE) {

			$result = null;
			
			if ($original_course_code) {
				$q2 = "SELECT
							a.id,
							a.course_code
						FROM
							courses AS a
						WHERE
							a.course_code != {$this->db->escape($original_course_code)} 
							AND a.course_code = {$this->db->escape($course_code)} ";
			} else {
				$q2 = "SELECT
							a.id,
							a.course_code
						FROM
							courses AS a
						WHERE
							a.course_code = {$this->db->escape($course_code)} ";				
			}
				
			
			$query = $this->db->query($q2);
							
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
							
			return $result;

		}
		
		
		function AddBEDSubject($data) {
				
			$q1 = "INSERT INTO
							courses (course_code,courses_groups_id,course_types_id,descriptive_title,paying_units,credit_units,owner_colleges_id,inserted_by)
						VALUES (
							{$this->db->escape($data['course_code'])},
							{$this->db->escape($data['courses_groups_id'])},
							{$this->db->escape($data['course_types_id'])},
							{$this->db->escape($data['descriptive_title'])},
							{$this->db->escape($data['paying_units'])},
							{$this->db->escape($data['credit_units'])},
							{$this->db->escape($data['owner_colleges_id'])},
							{$this->db->escape($data['inserted_by'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
		function UpdateSubject($data){

			$q1 = "UPDATE courses
					SET course_code = {$this->db->escape($data['course_code'])},
					    descriptive_title = {$this->db->escape($data['descriptive_title'])},
					    paying_units = {$this->db->escape($data['paying_units'])},
					    credit_units = {$this->db->escape($data['credit_units'])},
					    course_types_id = {$this->db->escape($data['course_types_id'])},
						updated_by = {$this->db->escape($data['updated_by'])}
		 			WHERE
						id = {$this->db->escape($data['id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

		
	    function DeleteSubject($course_id) {
			
			$q = "DELETE FROM
						courses 
					WHERE
						id = {$this->db->escape($course_id)} ";

			//return $q;
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


		public function ListSubjectTypes() {

			$result = null;
					
			$q2 = "SELECT
							a.id,
							a.description
						FROM
							bed_subject_types AS a ";
				
			
			$query = $this->db->query($q2);
							
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
							
			return $result;

		}

		
		public function ListBEDCoursesByProspectus($data=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							d.id,
							d.courses_id, 
							a.course_code,
							a.descriptive_title
						FROM 
							prospectus AS b, 
							prospectus_terms AS c, 
							prospectus_courses AS d, 
							courses AS a,
							academic_programs AS e
						WHERE 
							e.id = b.academic_programs_id
							AND b.id = c.prospectus_id 
							AND c.id = d.prospectus_terms_id 
							AND d.courses_id = a.id 
							AND e.id = {$this->db->escape($data->academic_programs_id)}
							AND c.year_level = {$this->db->escape($data->grade_level)}
						ORDER BY 
							a.descriptive_title ";
			
			$query = $this->db->query($q1);
							
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
							
			return $result;
		}

		
	}
	
?>
