<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<div style="background:#FFF; width:40%; text-align:center; margin:auto;" align="center">
	<form id="batch_posting_form" method="post" action="" class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="2" />
	<table align="left" cellpadding="0" cellspacing="0" style="width:70%; margin-top:10px;" class="head1">
  		<tr class="head">
			<td colspan="3" class="head" style="text-align:center; padding:10px;">ASSESSMENT BATCH POSTING </td>
		</tr>
  		<tr>
    		<td colspan="3">  
  				<table border="0" cellpadding="2" cellspacing="0" class="inside" align="left">
					<tr>
					  <td align="left" valign="middle">Academic Term </td>
					  <td align="left" valign="middle">:</td>
					  <td style="text-align:left; vertical-align:middle;"><?php print($terms_sy->term." ".$terms_sy->sy); ?></td>
				  </tr>
					<tr>
					  <td align="left" valign="middle">College</td>
					  <td align="left" valign="middle">:</td>
					  <td style="text-align:left; vertical-align:middle;">
					  <select name="colleges_id" id="colleges_id">
					  	<?php
							foreach($colleges AS $college) {
								print("<option value=\"$college->id\">$college->name</option>");
							}
						?>
                      </select></td>
				  </tr>
					<tr>
						<td width="104" align="left" valign="middle">Year Level </td>
						<td width="6" align="left" valign="middle">:</td>
						<td width="240" style="text-align:left; vertical-align:middle;">
						<select name="yr_level" id="yr_level">
						  <option value="0" selected="selected">All Year Levels</option>
						  <option value="1">1st Year</option>
						  <option value="2">2nd Year</option>
						  <option value="3">3rd Year</option>
						  <option value="4">4th Year</option>
						  <option value="5">5th Year</option>
                          </select></td>
					</tr>
					<tr>
					  <td align="left" valign="middle">&nbsp;</td>
					  <td align="left" valign="middle">&nbsp;</td>
					  <td style="text-align:left; vertical-align:middle;">&nbsp;</td>
				  </tr>

					<tr align="center"> 
						<td colspan="3" align="right"><input name="batch_post_button" type="submit" id="batch_post" value="Post!" class="btn btn-success"></td>
					</tr>
    			</table>
			</td>
 		</tr>
 	</table>
 </form>
 </div>

<script>
$('#batch_post').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to POST assessments of all students enrolled in this college?");

	if (confirmed){
		$('#batch_posting_form').submit();
	}		
});
</script>





  