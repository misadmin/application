
<?php
	if ($laboratories) {
?>
<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<input type="hidden" name="step" value="2">
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<?php 
						print($academic_year->sy);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls" style="border-radius: 5px; border:solid; border-width:1px; border-color: #D8D8D8; width: 20%; ">
						<table class="table table-striped" >
							<thead>
								<tr style="background-color: #EAEAEA; font-weight: bold; ">
									<td style="width: 60%; text-align: left;">COURSE CODE</td>
									<td style="width: 40%; text-align: left;">RATE</td>
								<tr>
								
							</thead>
						<?php 
							$lab_rates = json_encode($laboratories);
							foreach($laboratories AS $lab) {
						?>
							<tr>
								<td><?php print($lab['course_code']); ?></td>
								<td style="text-align: right;"><?php print(number_format($lab['rate'],2)); ?></td>
							</tr>
						
						<?php 	
							} 
						?>
						</table>
				</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<button class="btn btn-danger" id="back_to_fees" ><< Back to Laboratory Fees</button>
					<button class="btn btn-warning" id="save_fees" academic_year="<?php print($academic_year->id); ?>"  
						laboratories='<?php print($lab_rates); ?>'>Save Laboratory Fees >></button>
				</div>
			</div>
			</div>				
	</div>
</div>
</div>
<?php 
	} else {
		print("<span style='font-size: 15px; font-weight: bold;'>No Laboratory Fees Encoded!!</span>");
	} 
?>
 <form id="back_to_fees_sched_form" method="post">
  <input type="hidden" name="step" value="1" />
</form>

<script>
	$('#back_to_fees').click(function(event){
		event.preventDefault(); 
		
		$('#back_to_fees_sched_form').submit();
	});
</script>


 <form id="save_fees_sched_form" method="post">
  <input type="hidden" name="step" value="3" />
</form>

<script>
	$('#save_fees').click(function(event){
		event.preventDefault(); 
		var academic_year = $(this).attr('academic_year');
		var laboratories = $(this).attr('laboratories');
		
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'academic_year',
		   	value: academic_year,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'laboratories',
		   	value: laboratories,
		}).appendTo('#save_fees_sched_form');
		$('#save_fees_sched_form').submit();
	});
</script>



