<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function show_course()
{
	show('course');
	hide('yr_level');
}
function show_YrLevel()
{
	hide('course');
	show('yr_level');
}
</script>

<body onLoad="show_course();">

 <div style="background:#FFF; width:40%; text-align:center; margin:auto;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head" style="text-align:left; padding:10px;">
  ELECTIVE COURSE PREREQUISITES
  </td>
  </tr>
  <tr>
    <td colspan="3">
<form action="<?php echo site_url('dean/assign_elective_topic');?>" method="post" name="prereq">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="7" />
  
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
<tr>
  <td colspan="3" align="left" valign="middle" style="font-weight:bold; font-size:14px;">
  <?php 
  		print($course->course_code." - ".$course->descriptive_title); 
  		print("<br>".$elective_topic->course_code." - ".$elective_topic->descriptive_title); 
  ?></td>
  </tr>
<tr>
  <td width="97" align="left" valign="middle">Type</td>
  <td width="3" align="left" valign="middle">:</td>
  <td width="401" align="left" valign="middle"><input name="prereq_type" type="radio" class="form" onClick="show_course();" value="C" checked="checked"/>
    Course
    <input name="prereq_type" type="radio" class="form" onClick="show_YrLevel();" value="Y"/> 
    Year Standing    </td>
</tr>
<tr>
  <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="course">
    <tr>
      <td width="19%" align="left">Course</td>
      <td width="2%" align="left">:</td>
      <td width="79%" align="left"><select name="prospectus_courses_id" id="prospectus_courses_id" class="form">
        <?php
				foreach($prospectus_courses AS $prospectus_course) {
					print("<option value=".$prospectus_course->id.">".$prospectus_course->course_code."</option>");	
				}
			?>
      </select></td>
    </tr>
  </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="yr_level">
      <tr>
        <td width="20%" align="left">Year Standing </td>
        <td width="2%" align="left">:</td>
        <td width="78%" align="left"><select name="yr_level" class="form" id="yr_level">
			<option value="1">1st Year Standing</option>
			<option value="2">2nd Year Standing</option>
			<option value="3" selected>3rd Year Standing</option>
			<option value="4">4th Year Standing</option>
			<option value="5">5th Year Standing</option>
           </select></td>
      </tr>
    </table>
    </td>
  </tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td align="left"><input type="submit" name="button" id="button" value="Save Prerequisite!"  class="btn btn-success" /></td>
</tr>
    </table>
</form>
    
<div style="width:95%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-hover" style="margin:10px;">
	    <thead>
  <tr>
    <th width="79%" class="header">Prerequisite</th>
    <th width="9%" class="header">Type</th>
    <th width="12%" class="header">Delete</th>
  </tr>
	</thead>
	<?php
		if ($topic_prereqs) {
			foreach ($topic_prereqs AS $prereq) {
	
	?>
  <tr>
    <td><?php print($prereq->my_prereq); ?></td>
    <td style="text-align:center;"><?php print($prereq->prereq_type); ?></td>
    <td style="text-align:center; vertical-align:middle;">
      <a class="delete_prereq" href="<?php print($prereq->elective_topic_prereq_id); ?>" my_id="<?php print($prereq->prereq_type); ?>">
        <!-- <img src="<?php print(base_url('assets/img/icon-recycle.gif')); ?>" width="16" height="16" align="absmiddle" style="vertical-align:middle; border:none" /> -->
        <i class="icon-trash"></i></a></td>
  </tr>
	<?php
			}
		}	
	?>
  <thead>
  <tr>
    <th colspan="3" class="header" style="text-align:left">
 	<form action="<?php echo site_url('dean/assign_elective_topic');?>" method="post" name="prereq">
	<input type="hidden" name="step" value="3" />
    <input type="submit" name="button" id="button" value="Back to Create Elective Topic!"  class="btn btn-success" />
    </form></th>
    </tr>
  </thead>
</table>
</div>
    </td>
    </tr>
    </table>
    </div>

</body>


  <form id="delete_prereq2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="step" value="8" />
</form>

<script>
	$('.delete_prereq').click(function(event){
		event.preventDefault(); 
		var elective_topic_prereq_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'elective_topic_prereq_id',
		    value: elective_topic_prereq_id,
		}).appendTo('#delete_prereq2');
		$('#delete_prereq2').submit();
	});
</script>

