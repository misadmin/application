<?php 

//by Toyet Amores
//8.24.2018
class Prints_counter_model extends CI_Model {


//Toyet 9.3.2018
public function add_prints_counter($student_histories_id,$period) {
	$empno_ = $this->session->userdata('empno');
	$prelim = 0; $prelim_date = "0000-00-00";
	$midterm = 0; $midterm_date = "0000-00-00";
	$prefinal=0; $prefinal_date = "0000-00-00";
	$final=0; $final_date = "0000-00-00";
	if(isset($period)){
		switch($period) {
			case 'Prelim': $prelim = 0; $prelim_date = date("Y-m-d"); break;
			case 'Midterm': $midterm = 0; $midterm_date = date("Y-m-d"); break;
			case 'Pre-Final': $prefinal = 0; $prefinal_date = date("Y-m-d"); break;
			case 'Final': $final = 0; $final_date = date("Y-m-d"); break;
			default: $prelim = 0; break;
		}

	}
	$sql = "insert into college_prints_counter
			(student_histories_id,
			 prelim,prelim_date,
	         midterm,midterm_date,
	         prefinal,prefinal_date,
	         final,final_date,
	         addedby,addedon)
			values 
			({$student_histories_id},
			 {$prelim},{$prelim_date},
             {$midterm},{$midterm_date},
             {$prefinal},{$prefinal_date},
             {$final},{$final_date},
             {$empno_},curdate());";
	$query = $this->db->query($sql);
	$sql2 = "COMMIT;";
	$this->db->query($sql2);

	//log_message("INFO",print_r($query,true)); // Toyet 8.31.2018
	if($query==1)
		return TRUE;
	else
		return FALSE; 
}	

//Toyet 9.3.2018
public function exists_in_term($student_idno,$term){
	$sql = "select cpc.student_histories_id as hist_id
			from student_histories sh
			left join college_prints_counter cpc on cpc.student_histories_id=sh.id
			where sh.students_idno={$student_idno}
			and sh.academic_terms_id={$term}
			and cpc.student_histories_id is not null";
	$query = $this->db->query($sql);
	//log_message("INFO",print_r($sql,true));
	//log_message("INFO",print_r($query->num_rows,true));
	if($query->num_rows() > 0) 
		return true; else 
		return false; 
}

//Toyet 9.3.2018
public function increment_prefinal($student_histories_id){
	//this lines of code assumes there is no error...
	$prefinal = 0;
	$sql1 = "START TRANSACTION;";
	$this->db->query($sql1);
	$sql2 = "select prefinal 
	         from college_prints_counter cpc
	         left join student_histories sh on sh.id=cpc.student_histories_id
	         where cpc.student_histories_id={$student_histories_id}
			 for update;";	
	$query = $this->db->query($sql2);
	$prefinal = $query->result()[0]->prefinal;
	$sql3 = "update college_prints_counter cpc
			 set prefinal = {$prefinal} + 1,
			     prefinal_date = curdate()
			 where cpc.student_histories_id={$student_histories_id};";
	$this->db->query($sql3);
	$sql4 = "COMMIT;";
	$this->db->query($sql4);
	//log_message("INFO",print_r($sql1,true));
	//log_message("INFO",print_r($sql2,true));
	//log_message("INFO",print_r($sql3,true));
	//log_message("INFO",print_r($sql4,true));
	return (string)$prefinal+1;
}

//Toyet 10.2.2018
public function increment_final($student_histories_id){
	//this lines of code assumes there is no error...
	$final = 0;
	$sql1 = "START TRANSACTION;";
	$this->db->query($sql1);
	$sql2 = "select final
	         from college_prints_counter cpc
	         left join student_histories sh on sh.id=cpc.student_histories_id
	         where cpc.student_histories_id={$student_histories_id}
			 for update;";	
	$query = $this->db->query($sql2);
	$final = $query->result()[0]->final;
	$sql3 = "update college_prints_counter cpc
			 set final = {$final} + 1,
			     final_date = curdate()
			 where cpc.student_histories_id={$student_histories_id};";
	$this->db->query($sql3);
	$sql4 = "COMMIT;";
	$this->db->query($sql4);
	//log_message("INFO",print_r($sql1,true));
	//log_message("INFO",print_r($sql2,true));
	//log_message("INFO",print_r($sql3,true));
	//log_message("INFO",print_r($sql4,true));
	return (string)$final+1;
}

//Toyet 9.3.2018
public function fetch_count_prefinal($student_histories_id,$term){
    $prefinal = 0;
	$sql = "select prefinal 
	        from college_prints_counter cpc
	        left join student_histories sh on sh.id=cpc.student_histories_id
	        where cpc.student_histories_id={$student_histories_id};";	
	$query = $this->db->query($sql);
	//log_message("INFO",print_r($sql,true)); // Toyet 9.19.2018
	if($query->num_rows() > 0 )
		$prefinal = $query->result()[0]->prefinal;
	return $prefinal;
}

//Toyet 10.2.2018
public function fetch_count_final($student_histories_id,$term){
    $final = 0;
	$sql = "select final 
	        from college_prints_counter cpc
	        left join student_histories sh on sh.id=cpc.student_histories_id
	        where cpc.student_histories_id={$student_histories_id};";	
	$query = $this->db->query($sql);
	//log_message("INFO",print_r($sql,true)); // Toyet 9.19.2018
	if($query->num_rows() > 0 )
		$final = $query->result()[0]->final;
	return $final;
}


	
}