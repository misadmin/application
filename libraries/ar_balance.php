<?php 

/**
 * Use to display AR Running Balance Page
 * The purpose of creating this library is that, multiple
 * controllers are using AR Balance, Thus in order to minimize
 * the code this library is loaded to display AR Running
 * balance pages
 * @author HOLY NAME - Kevin
 *
 */
class AR_Balance {
	
	/**
	 * Codeigniter Instance
	 * @var unknown_type
	 */
	protected $CI;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}
	
	/**
	 * AR Balance Index Page
	 */
	public function index(){
		$this->CI->load->library('tab_lib');
		$this->CI->load->model('accountant/accountant_model');
		$this->CI->load->model('hnumis/AcademicYears_Model');
		
		$groups = $this->CI->accountant_model->get_acad_program_groups();
		
		$academic_terms = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);

		$academic_years =  $this->CI->accountant_model->get_academic_years();		
		$school_years =  $this->CI->accountant_model->get_academic_years();		
		$academic_years = array();		
		foreach($school_years as $school_year){
			unset($academic_years[$school_year->id]);
			$academic_years[$school_year->id] = $school_year->academic_year;
		}		
		//print_r($academic_years);die();
		
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');
		
		$this->CI->breadcrumb_lib->enqueue_item('reports','Reports','');
		$this->CI->breadcrumb_lib->enqueue_item('ar_balance','A/R Balance','ar_balance');
		
		$this->CI->content_lib->enqueue_body_content('',$this->CI->breadcrumb_lib->content());
		$this->CI->content_lib->enqueue_body_content('accountant/ar_running_balance_index',
										array('message'=>$this->CI->error_message,
												'groups'=>$groups,
												'academic_years'=>$academic_years,
											)
									);
		
		$this->CI->content_lib->content();
	}

	
	public function basic(){
		$this->CI->load->library('table_lib');
		$this->CI->load->model('accountant/accountant_model');	
		$start_date = $this->CI->input->post('start_date');
		$end_date = $this->CI->input->post('end_date');
		$academic_year_id = $this->CI->input->post('academic_year_id'); 
//		$display_ay = $this->CI->input->post('display_ay');
//		if (isset($display_ay)){
//			print_r($display_ay);die();
//		}
		$s = strtotime($start_date);
		$e = strtotime($end_date);	
		

		
		
		if (!$start_date){
			$this->CI->error_message = 'Please Enter Valid Start Date!';
			$this->CI->ar_balance();
			return;
		}
		if (!$end_date){
			$this->CI->error_message = 'Please Enter Valid End Date!';
			$this->CI->ar_balance();
			return;
		}				
		//check for valid start and end date
		else if (!$s || !$e || !($s <= $e)){
			$this->CI->error_message = 'End Date must be greater than or equal to Start Date';
			$this->CI->ar_balance();
			return;
		}		
		//$data =(($end_date <= $this->CI->config->item('last_isis_db')) ? $this->CI->accountant_model->ar_balance_basic_ed($start_date,$end_date) : $this->CI->accountant_model->ar_balance_basic_ed_mis($start_date,$end_date));
		
		$yr_levels = $this->CI->accountant_model->get_yr_levels();
		$selected_groups = array();
		foreach ($yr_levels as $yr_level){
			$selected_groups[] = $yr_level ;
		}

		if (count($selected_groups) > 0){
			$imploded_group = array_map(function($obj) { return $obj->yr_level; }, $selected_groups);
			$imploded_group = "'".implode("','", $imploded_group)."'";
			$group_result=array();
			$data=array();
			if ($end_date <= $this->CI->config->item('last_isis_db')){			
				$is_isis = 'Y';
				$group_results = $this->CI->accountant_model->ar_balance_basic_ed_isis_by_group($academic_year_id,$start_date,$end_date);
			}else{			
				$is_isis = 'N';
				$group_results = $this->CI->accountant_model->ar_balance_basic_ed_mis($start_date,$end_date);
			}
			if ($group_results)
					$data = $group_results;
		}
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');
		
		$this->CI->breadcrumb_lib->enqueue_item('reports','Reports','');
		$this->CI->breadcrumb_lib->enqueue_item('ar_balance','A/R Balance','ar_balance');
		$this->CI->breadcrumb_lib->enqueue_item('ar_balance_basic','Basic ED','ar_balance_basic');
		
		$this->CI->content_lib->enqueue_body_content('',$this->CI->breadcrumb_lib->content());
		
		$this->CI->content_lib->enqueue_body_content('accountant/ar_running_balance_basic_table', array(
				'data'=>$data,
				'start_date'=>$start_date,
				'end_date'=>$end_date,
				'type'=>'Basic ED',
				'q_type'=>'basic',
				'q_groups'  => $imploded_group, //q - to be used in query
				'x_show_by' => 'group',
				'x_groups'  => (isset($selected_groups) ? $selected_groups : FALSE),				
				'is_isis'=>$is_isis,
				'academic_year_id'=>$academic_year_id,
		));
		$this->CI->content_lib->content();
	}
	
	public function college(){
		$this->CI->load->library('table_lib');
		$this->CI->load->model('accountant/accountant_model');
		$start_date = $this->CI->input->post('start_date');
		$end_date = $this->CI->input->post('end_date');
		$show_by = $this->CI->input->post('show_by');		
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');		
		$s = strtotime($start_date);
		$e = strtotime($end_date);		
		if (!$start_date || !$end_date){
			$this->CI->error_message = 'Please Enter Valid Date';
			$this->CI->index();
			return;
		}		
		//check for valid start and end date
		else if (!$s || !$e || !($s <= $e)){
			$this->CI->error_message = 'End Date must be greater than or equal to Start Date';
			$this->CI->index();
			return;
		}
		
		else if (!in_array($show_by, array('all','group'))){
			$this->CI->error_message = 'Please select whether to show all or by group';
			$this->CI->index();
			return;
		}
		
		$this->CI->breadcrumb_lib->enqueue_item('reports','Reports','');
		$this->CI->breadcrumb_lib->enqueue_item('ar_balance','A/R Balance','ar_balance');
		
		$imploded_group = '';
		if ($show_by == 'group'){
			$groups = $this->CI->accountant_model->get_acad_program_groups();			
			$selected_groups = array();				
			foreach ($groups as $group){
					$selected_groups[] = $group;					
			}			
			$data = array();
			if (count($selected_groups) > 0){
				$imploded_group = array_map(function($obj) { return $obj->abbreviation; }, $selected_groups);
				$imploded_group = "'".implode("','", $imploded_group)."'";
					//set this to the date ISIS db was last extracted
					if ($end_date <= $this->CI->config->item('last_isis_db')){ 
						$is_isis = 'Y';						
						$group_results = $this->CI->accountant_model->ar_balance_college_by_group_isis($start_date,$end_date);
					}else{
						$is_isis = 'N';						
						$group_results = $this->CI->accountant_model->ar_balance_college_by_group_mis($start_date,$end_date);						
					}
					if ($group_results) 
						$data = $group_results;					
			}				
			$this->CI->breadcrumb_lib->enqueue_item('ar_balance_college','College (By Group)','ar_balance_college');
		} else if ($show_by == 'all')	{
			//$data = (($end_date <= $this->CI->config->item('last_isis_db')) ? $this->CI->accountant_model->ar_balance_college($start_date,$end_date) : $this->CI->accountant_model->ar_balance_college_mis($start_date,$end_date));
			if ($end_date <= $this->CI->config->item('last_isis_db')){
				$data = $this->CI->accountant_model->ar_balance_college_isis($start_date,$end_date);				
				$is_isis = 'Y';
			}else{
				//echo "here";die();
				$data  = $this->CI->accountant_model->ar_balance_college_mis($start_date,$end_date);				
				$is_isis = 'N';
			}				
			$this->CI->breadcrumb_lib->enqueue_item('ar_balance_college','College (All)','ar_balance_college');
		}
		$this->CI->content_lib->enqueue_body_content('',$this->CI->breadcrumb_lib->content());
		$this->CI->content_lib->enqueue_body_content('accountant/ar_running_balance_table', array(
				'data'=>$data,
				'start_date'=> $start_date,
				'end_date'  => $end_date,
				'type'      => 'College'.($show_by == 'group' ? ' (by Group)' : ''),
				'q_type'    => 'college',
				'q_groups'  => $imploded_group, //q - to be used in query
				'x_show_by' => $show_by,
				'x_groups'  => (isset($selected_groups) ? $selected_groups : FALSE),
				'is_isis'   => $is_isis
		));		
		$this->CI->content_lib->content();
	}
}