<?php 
	$tuition_pdf=json_encode($tuition_basics);
	$misc_pdf=json_encode($misc_fees);
	$other_fees_pdf=json_encode($other_fees); 
	$addl_other_fees_pdf=str_replace("'","`",json_encode($addl_other_fees)); 
	$acad_yrs_pdf = json_encode($acad_years);
	$students_pdf = json_encode($students);
	//print_r(stripslashes($addl_other_fees_pdf)); die();
?>
<div style="width:auto;">
<div style="padding-top: 30px; padding-bottom:20px; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; width:60%; text-align:center;">

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding: 2px; margin-bottom:20px;">
	
	<table style="width:100%; height:auto;">
	<tr>
		<td style="text-align:left; vertical-align:middle; font-weight:bold; width:13%; padding:7px; font-size:12px;">
			<a class="download_report" href="" 
				tuition_basics='<?php print($tuition_pdf); ?>' 
				misc_fees='<?php print($misc_pdf); ?>'
				other_fees='<?php print($other_fees_pdf); ?>' 
				acad_years = '<?php print($acad_yrs_pdf); ?>' 
				addl_other_fees = '<?php print($addl_other_fees_pdf); ?>' 
				post_description = "<?php print($post_description); ?>" 
				students = '<?php print($students_pdf); ?>'
				header =  "<?php print($header); ?>" ><i class="icon-download-alt"></i> Download to PDF</a>
		</td>
	</tr>
	</table>
</div>


DETAILED ASSESSMENT REPORT FOR: 
	<?php
		print($header."<br>".$acad_years->sy."<br>");
	?>
	<span style="color:#FF0000;">
		<?php 
			print($post_description);
		?> 	
	</span>
	<table border="0" align="center" cellpadding="0" cellspacing="0" class="table" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal;">
      <tr style="background:#DAFED3; padding:10px;">
        <td colspan="4" style="font-weight:bold;"># of Students: <?php print($students->numEnroll); ?> </td>
      </tr>
	  <?php	
	  		$tuition_fee = 0;
	  		if ($tuition_basics) {
	  			$basic_total = 0;
	  			foreach ($tuition_basics AS $tuition_basic) {
	  ?>
      <tr>
        <td width="45%" style="padding-left:20px;">Tuition Fee [<?php
		print($tuition_basic->level."-".$tuition_basic->yr_level." @ ".$tuition_basic->rate."] <i>(".$tuition_basic->numEnroll." students)</i>");
	?> </td>
        <td width="18%" style="padding-left:20px; text-align:right;">
          <?php
		  		$basic_total =  $basic_total + ($tuition_basic->numEnroll * $tuition_basic->rate);
				print(number_format(($tuition_basic->numEnroll * $tuition_basic->rate),2));
	?>        </td>
        <td width="20%">&nbsp;</td>
        <td width="17%">&nbsp;</td>
      </tr>
	  <?php
	  			}
	  ?>
      <tr>
        <td style="padding-left:20px;">Tuition Fee </td>
        <td>&nbsp;</td>
        <td align="right" style="padding-left:20px; text-align:right;">
          <?php
				$tuition_fee = $tuition_fee + ($basic_total);	  
				print(number_format($basic_total,2));
	    ?>        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="padding-left:20px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="padding-left:20px; text-align:right;">&nbsp;</td>
        <td style="padding-left:20px; text-align:right;">
		<?php
			print(number_format($tuition_fee,2));
		?>
		</td>
      </tr>
      <tr>
        <td colspan="4" style="font-weight:bold; background:#EEEEEE;">Miscillaneous &amp; Other Fees: </td>
      </tr>
	  <?php
	  		if ($misc_fees) {
	  ?>
      <tr>
        <td colspan="4" style="padding-left:20px; font-weight:bold; font-style:italic;">Miscellaneous Fees </td>
      </tr>
	  <?php
	  			$misc_total = 0;
	  			foreach($misc_fees AS $misc_fee) {
		?>
      <tr>
        <td style="padding-left:35px;"><?php print($misc_fee->description." @ ".$misc_fee->rate." <i>(".$misc_fee->enrollees." students)</i>"); ?></td>
        <td>&nbsp;</td>
        <td style="text-align:right;">
		<?php
			$misc_total = $misc_total + ($misc_fee->enrollees*$misc_fee->rate);
			print(number_format($misc_fee->enrollees*$misc_fee->rate,2));
	    ?></td>
        <td style="text-align:right;">&nbsp;</td>
      </tr>
	  <?php
	  			}
	  ?>
      <tr>
        <td style="padding-left:35px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			$tuition_fee = $tuition_fee + ($misc_total);
			print(number_format($misc_total,2));
	    ?></td>
      </tr>
	  <?php
			}

	  		if ($other_fees) {
	  ?>
      <tr>
        <td colspan="4" style="padding-left:20px; font-weight:bold; font-style:italic;">Other School Fees </td>
      </tr>
	  <?php
	  			$others_total = 0;
	  			foreach($other_fees AS $other) {
		?>
      <tr>
        <td style="padding-left:35px;"><?php print($other->description." @ ".$other->rate." <i>(".$other->enrollees." students)</i>"); ?></td>
        <td>&nbsp;</td>
        <td style="text-align:right;">
		<?php
			$others_total = $others_total + ($other->enrollees*$other->rate);
			print(number_format($other->enrollees*$other->rate,2));
	    ?>		</td>
        <td style="text-align:right;">&nbsp;</td>
      </tr>
	  <?php
	  			}
	  ?>
	  <tr>
        <td style="padding-left:35px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			$tuition_fee = $tuition_fee + ($others_total);
			print(number_format($others_total,2));
	    ?></td>
      </tr>
	  <?php
	  		}
			
			if ($addl_other_fees) {
	  ?>		
	  <tr>
        <td colspan="4" style="padding-left:20px; font-weight:bold; font-style:italic;">Additional Other Fees </td>
      </tr>
	  <?php
	  			$addl_total = 0;
	  			foreach($addl_other_fees AS $addl) {
	  ?>
      <tr>
        <td style="padding-left:35px;" colspan="2"><?php print($addl->description." @ ".$addl->rate." <i>(".$addl->enrollees." students)</i>"); ?></td>
        <td style="text-align:right;">
		<?php
			$addl_total = $addl_total + ($addl->enrollees*$addl->rate);
			print(number_format($addl->enrollees*$addl->rate,2));
	    ?>		</td>
        <td style="text-align:right;">&nbsp;</td>
      </tr>
	  <?php
	  			}
	  ?>
	  <tr>
        <td style="padding-left:35px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			$tuition_fee = $tuition_fee + ($addl_total);
			print(number_format($addl_total,2));
	    ?></td>	  
	  </tr>
	  <?php
	  		}
	  ?>
	<tr style="background:#DAFED3; font-weight:bold; font-size:14px;">
        <td>TOTAL Assessment: </td>
        <td>&nbsp;</td>
        <td style="text-align:right;">&nbsp;</td>
        <td style="text-align:right;"><?php
			print(number_format($tuition_fee,2));
	    ?></td>
      </tr>
 <?php 
 	}
 ?>
      </table>

<!-- 	<div style="text-align:right; font-size:12px; font-weight:normal;"><a class="download_report" href="" 
		academic_terms_id="<?php print($term->id); ?>" 
		academic_years_id="<?php print($term->academic_years_id); ?>" 
		colleges_id="<?php print($colleges_id); ?>" 
		yr_level="<?php print($yr_level); ?>" 
		program_groups_id="<?php print($students->program_groups_id); ?>"
		post_status="<?php print($post_status); ?>" >Download to PDF<i class="icon-download-alt"></i></a></div>
    
  </div>
 -->

</div>

  <form id="download_tuition_report_form" method="post" target="_blank">
  <input type="hidden" name="step" value="download_to_pdf" />
</form>
<script>
	$('.download_report').click(function(event){
		event.preventDefault(); 
		var tuition_basics = $(this).attr('tuition_basics');
		var misc_fees = $(this).attr('misc_fees');
		var other_fees = $(this).attr('other_fees');
		var addl_other_fees = $(this).attr('addl_other_fees');
		var acad_years = $(this).attr('acad_years');
		var post_description = $(this).attr('post_description');
		var students = $(this).attr('students');
		var header = $(this).attr('header');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'tuition_basics',
			value: tuition_basics,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'misc_fees',
			value: misc_fees,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'other_fees',
			value: other_fees,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'addl_other_fees',
			value: addl_other_fees,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'acad_years',
			value: acad_years,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'post_description',
			value: post_description,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'students',
			value: students,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'header',
			value: header,
		}).appendTo('#download_tuition_report_form');
		
		$('#download_tuition_report_form').submit();
		
	});
</script>
