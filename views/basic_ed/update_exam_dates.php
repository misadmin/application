<h2 class="heading"><?php print("Examination Dates for: ".$selected_level_desc." ".$acad_yr->sy); ?></h2>

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding: 2px; margin-bottom:20px;">
	
	<table style="width:100%; height:auto;">
	<tr>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:20%; padding:3px;">
	<!-- <a href="<?php print(base_url('downloads/Basic Education Enrollment Summary '.$acad_yr->sy.'.xls')); ?>" class="btn btn-link" id="download_summary">
	<i class="icon-download-alt"></i> Download as Excel</a> -->
	</td>
	<td style="text-align:right; vertical-align:top;">
		<form id="change_sy" method="POST">

			<select name="levels_id" class="change_pulldown">
				<?php
					foreach($levels AS $level) {
				?>
						<option value="<?php print($level->id); ?>" <?php if ($level->id == $selected_level) { print("selected"); } ?>>
								<?php print($level->level); ?> </option>	
				<?php 
					}
				?>					
			</select>
		
			<select name="academic_year_id" class="change_pulldown">
				<?php
					foreach($school_years AS $school_year) {
				?>
						<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->sy); ?> </option>	
				<?php 
					}
				?>					
			</select>
							
	</form>
	</td>
	</tr>
	</table>						

</div>	

<div style="text-align:center; width:100%;">
		
		<?php
			$yr_level=0;
			if ($exam_dates) {
				foreach ($exam_dates AS $exam_date) {
					if ($yr_level != $exam_date->year_level) {
						$yr_level = $exam_date->year_level;
		?>
			
			<div style="width:50%;">
			<table id="section_list" border="0" cellspacing="0" class="table table-condensed table-bordered table-hover" 
					style="width:100%; padding:0px; margin:20px; margin-bottom:40px;">
		    <thead>
			<tr>
				<th colspan="5" style="font-weight:bold; font-size:14px;"><?php print("Grade Level: ".$yr_level); ?></th>
			</tr>
			    <tr>
			       <th width="5%" style="vertical-align:middle; text-align:center;">Exam No.</th>
			       <th width="15%" style="vertical-align:middle; text-align:center;">Start Date</th>
			       <th width="15%" style="vertical-align:middle; text-align:center;">End Date</th>
			       <th width="5%"  style="vertical-align:middle; text-align:center;">Edit</th>
			       <th width="5%"  style="vertical-align:middle; text-align:center;">Delete</th>
			     </tr>
			</thead>		
		<?php
					} 
					
		?>
		<tr>
		      <td style="text-align:center; vertical-align:middle; "><?php print($exam_date->exam_number); ?> </td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($exam_date->start_date1); ?> </td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($exam_date->end_date1); ?> </td>
		      
		      <td style="text-align:center; vertical-align:middle; ">
			 		<?php 
			 			if ($can_edit) {
			 		?>
			 		<a href="#" data-toggle="modal" data-target="#modal_edit_schedule_<?php print($exam_date->id); ?>" 
								data-project-id='<?php print($exam_date); ?>'><i class="icon-edit"></i></a>
					<?php 
						} else {
					?>
						<i class="icon-minus"></i>
					<?php 
						}
					?>
				  <!-- modal for EDITING enrollment schedules -->
				      
				      <div class="modal hide fade" id="modal_edit_schedule_<?php print($exam_date->id); ?>" >
				 		<form method="post" id="edit_schedule_form_<?php print($exam_date->id); ?>">
				   				<input type="hidden" name="action" value="update_schedule" />
				   				 <input type="hidden" name="schedule_id" value= "<?php print($exam_date->id);?>"/>
				   				 <input type="hidden" name="levels_id" value= "<?php print($selected_level);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <?php $this->common->hidden_input_nonce(FALSE); ?>
				   		<div class="modal-header">
				       		<button type="button" class="close" data-dismiss="modal">�</button>
				       		<h3>Edit Exam Date</h3>
				   		</div>
				   		<div class="modal-body">
				 			<div style="text-align:center;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									
									<tr>
										<td>School Year</td>
										<td>:</td>
										<td> <?php print($exam_date->sy); ?> </td>
									</tr>
									
									<tr>
										<td>Level</td>
										<td>:</td>
										<td> <?php print($exam_date->level); ?> </td>
									</tr>
									
									<tr>
										<td>Year Level</td>
										<td>:</td>
										<td> <?php print($exam_date->year_level); ?> </td>
									</tr>
									
									<tr>
										<td>Exam Number</td>
										<td>:</td>
										<td> <?php print($exam_date->exam_number); ?> </td>
									</tr>
									
									<tr>
										<td>Start Date</td>
										<td>:</td>
										<td><input type="date" name="start_date" style="width:auto;" value="<?php print($exam_date->start_date); ?>"></td>
									</tr>		
									
									<tr>
										<td>End Date</td>
										<td>:</td>
										<td><input type="date" name="end_date" style="width:auto;" value="<?php print($exam_date->end_date); ?>"></td>
									</tr>		
									
								</table>
							</div>
				   		</div>
					   <div class="modal-footer">
				    		<input type="submit" class="btn btn-primary" id="edit_schedule_<?php print($exam_date->id); ?>" value="EDIT DATE">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
						</form>
				    </div>		
				     <!-- End of EDITING a course from other schools -->
								
			</td>
			
			
			<!-- Modal for deleting exam date -->
			
			 <td style="text-align:center; vertical-align:middle; ">
			 		<?php 
			 			if ($can_edit) {
			 		?>
			 		<a href="#" data-toggle="modal" data-target="#modal_delete_exam_<?php print($exam_date->id); ?>" 
								data-project-id='<?php print($exam_date); ?>'><i class="icon-trash"></i></a>
	 				<?php 
	 					} else {
	 				?>
	 					<i class="icon-minus"></i>
	 				<?php 
	 					}
	 				?>
		    <div class="modal hide fade" id="modal_delete_exam_<?php print($exam_date->id); ?>" >
				<form method="post" id="delete_wd_form_<?php print($exam_date->id); ?>">
					<input type="hidden" name="action" value="delete_schedule" />
			    	<input type="hidden" name="schedule_id" value= "<?php print($exam_date->id);?>"/>
				   	<input type="hidden" name="levels_id" value= "<?php print($selected_level);?>"/>
				   	<input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				 	<?php $this->common->hidden_input_nonce(FALSE); ?>
				 <div class="modal-header">
				 	<button type="button" class="close" data-dismiss="modal">�</button>
				      		<h3>Delete Exam Schedule</h3>
				 </div>
				 <div class="modal-body">
				 			<div style="text-align:center;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									
									<tr>
										<td>School Year</td>
										<td>:</td>
										<td> <?php print($exam_date->sy); ?> </td>
									</tr>
									
									<tr>
										<td>Level</td>
										<td>:</td>
										<td> <?php print($exam_date->level); ?> </td>
									</tr>
									
									<tr>
										<td>Year Level</td>
										<td>:</td>
										<td> <?php print($exam_date->year_level); ?> </td>
									</tr>
									
									<tr>
										<td>Exam Number</td>
										<td>:</td>
										<td> <?php print($exam_date->exam_number); ?> </td>
									</tr>
									
									<tr>
										<td>Start Date</td>
										<td>:</td>
										<td><?php print($exam_date->start_date1); ?></td>
									</tr>		
									
									<tr>
										<td>End Date</td>
										<td>:</td>
										<td><?php print($exam_date->end_date1); ?></td>
									</tr>		
									
								</table>
							</div>
				</div>
				<div class="modal-footer">
				    		<input type="submit" class="btn btn-danger" id="delete_wd_<?php print($exam_date->id); ?>" value="DELETE Exam Date!">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
			</form>
		</div>		
				 
			</td>
			
			<!-- End deleting exam date -->
			
		   </tr>
							
		<?php
				}
		?>
		</table>
		</div>
		<?php 		
			}
		?>
		 
</div>	


<form id="edit_sched" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="update_schedule" />
		
</form>			


<script>
	$('.edit_schedule').click(function(event){
		event.preventDefault(); 
	
		var schedule_id = $(this).attr('href');
		var levels_id = $(this).attr('levels_id');
		var academic_year_id = $(this).attr('academic_year_id');
		
	  	$('<input>').attr({
		    type: 'hidden',
		    name: 'schedule_id',
		    value: schedule_id,
		}).appendTo('#edit_sched');
	  	$('<input>').attr({
		    type: 'hidden',
		    name: 'levels_id',
		    value: levels_id,
		}).appendTo('#edit_sched');
	  	$('<input>').attr({
		    type: 'hidden',
		    name: 'academic_year_id',
		    value: academic_year_id,
		}).appendTo('#edit_sched');
		
		$('#edit_sched').submit();
		
	});
</script>


<form id="delete_exam_form" method="post" >
	<input type="hidden" name="action" value="delete_schedule" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>			

<script>
	$('.delete_exam').bind('click', function(event){
		event.preventDefault();
		var confirmed = confirm("Continue to delete Exam date?");
		
		if (confirmed){
			var schedule_id = $(this).attr('href');
			var levels_id = $(this).attr('levels_id');
			var academic_year_id = $(this).attr('academic_year_id');
			
			$('<input>').attr({
			    type: 'hidden',
			    name: 'schedule_id',
			    value: schedule_id,
			}).appendTo('#delete_exam_form');
		  	$('<input>').attr({
			    type: 'hidden',
			    name: 'levels_id',
			    value: levels_id,
			}).appendTo('#delete_exam_form');
		  	$('<input>').attr({
			    type: 'hidden',
			    name: 'academic_year_id',
			    value: academic_year_id,
			}).appendTo('#delete_exam_form');
			
			$('#delete_exam_form').submit();
		}		
	});
</script>


<script>
	$(document).ready(function(){
		$('.change_pulldown').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'display_selected_term',
			}).appendTo('#change_sy');
			
			$('#change_sy').submit();
		});
	});
</script> 
