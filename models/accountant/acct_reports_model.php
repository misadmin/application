<?php 
class Acct_reports_model extends MY_Model {


	/**
	 * Queries AR Transactions data (summar/details) pertaining to
	 * or with relation to the Student's Subsidiary Ledger.
	 * @author   From the original team of IRMC
	 *           Edited: 2017 October-November by Toyet Amores
	 * @param    
	 * $start_date   - starting date of transactions to get
	 * $end_date     - ending date of transactions to get
	 * $summary      - logical (TRUE/FALSE)
	 * @return   
	 * array of AR Trans.
	*/
	//this function has been fully revised by Toyet 2017.10
	function ar_balance_mis($start_date,$end_date,$summary=FALSE){

		if ($summary) {
		$sql = "
		SELECT SQL_CACHE 
		    ar_mis.levels_id AS levels_id,
		    ar_mis.course AS group_name,
		    lpad(ar_mis.year_level,2,'0') AS year_level,
		    ar_mis.trangroup AS trangroup,
		    CONCAT(ar_mis.tellercode,': ') AS code,
		    substring_index(ar_mis.description,':',true) AS description,";
		} else {

		$sql = "
		SELECT SQL_CACHE 
		    ar_mis.levels_id AS levels_id,
		    ar_mis.course AS group_name,
		    ar_mis.year_level AS year_level,
		    ar_mis.trangroup AS trangroup,
		    CONCAT(ar_mis.tellercode,
		            ': ',
		            ar_mis.course,
		            ' ',
		            (CASE ar_mis.levels_id
				        WHEN 1 THEN CAST(ar_mis.year_level AS CHAR)
				        WHEN 2 THEN CAST(ar_mis.year_level AS CHAR)
				        WHEN 3 THEN CAST(ar_mis.year_level AS CHAR)
				        WHEN 4 THEN lpad(ar_mis.year_level,2,'0')
				        WHEN 5 THEN lpad(ar_mis.year_level,2,'0')
				        WHEN 6 THEN ''
				        WHEN 7 THEN ''
				        WHEN 11 THEN CAST(ar_mis.year_level AS CHAR)
				        WHEN 12 THEN CAST(ar_mis.year_level AS CHAR)
				        WHEN 99 THEN ''
				        END)) AS code, 
			substring_index(ar_mis.description,':',true) AS description,";
		}

		$sql .= "		
		    COUNT(ar_mis.id) AS stud,
		    SUM((CASE ar_mis.type
		        WHEN 'Debit' THEN ar_mis.amount
		        ELSE 0
		    END)) AS debit,
		    SUM((CASE ar_mis.type
		        WHEN 'Credit' THEN ar_mis.amount
		        ELSE 0
		    END)) AS credit,
		    (CASE ar_mis.levels_id
		        WHEN 1 THEN 2
		        WHEN 2 THEN 3
		        WHEN 3 THEN 4
		        WHEN 4 THEN 5
		        WHEN 5 THEN 6
		        WHEN 6 THEN 8
		        WHEN 7 THEN 9
		        WHEN 11 THEN 1
		        WHEN 12 THEN 7
		        WHEN 99 THEN 99
		    END) AS sequencer,
		    ar_mis.type as type
		FROM (
		SELECT 
	        DATE_FORMAT(p.transaction_date, '%m/%d') AS date,
	            COALESCE(p.levels_id, 99) AS levels_id,
	            tc.teller_code AS tellercode,
	            tc.description AS description,
	            p.receipt_no AS receipt,
	            COALESCE(py.students_idno, p.payers_id, py.employees_empno) AS id,
	            COALESCE(op.name, CONCAT(UPPER(s.lname), ', ', s.fname, ' ', s.mname), CONCAT(emp.lname, ', ', emp.fname, ' ', emp.mname),ofc.office) AS name,
	            COALESCE(ag.abbreviation, (CASE p.levels_id
	                WHEN 1 THEN 'Kinder '
	                WHEN 2 THEN 'Kinder '
	                WHEN 3 THEN 'Grade '
	                WHEN 4 THEN 'HS Day '
	                WHEN 5 THEN 'HS Night '
	                WHEN 11 THEN 'Pre-School '
	                WHEN 12 THEN 'Senior High '
	            END), 'n/a') AS course,
	            COALESCE(p.year_level, 'n/a') AS year_level,
	            COALESCE(p.receipt_amount,0) AS amount,
	            pm.payment_method AS method,
	            'Credit' AS type,
	            p.remark,
                'PYMT' AS trangroup
	    FROM
	        payments p
	    LEFT JOIN payers py ON py.id = p.payers_id
	    LEFT JOIN payment_items pi ON pi.payments_id = p.id
	    LEFT JOIN teller_codes tc ON tc.id = pi.teller_codes_id
	    LEFT JOIN payment_methods pm ON pm.id = pi.id
	    LEFT JOIN other_payers op ON op.id = py.other_payers_id
	    LEFT JOIN employees emp ON emp.empno = py.employees_empno
	    LEFT JOIN students s ON s.idno = py.students_idno
	    LEFT JOIN offices ofc ON ofc.id = py.inhouse_id
	    LEFT JOIN academic_programs ap ON ap.id = p.academic_programs_id
	    LEFT JOIN acad_program_groups ag ON ag.id = ap.acad_program_groups_id
	    WHERE
	    	p.status = 'posted' and 
	    	tc.is_ledger = 'Y' and
	        p.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
	    UNION
      select * from (
	    SELECT
		    DATE_FORMAT(adj.transaction_date, '%m/%d') AS date,
		    COALESCE(ag.levels_id, (SELECT e.levels_id
									FROM 
										col_students AS b,
										students AS a,
                                        student_histories AS f,
										prospectus AS c,
										academic_programs AS d,
                                        acad_program_groups AS e
									WHERE
										a.idno=b.students_idno
										AND f.students_idno=a.idno
                                        AND c.id=f.prospectus_id
										AND	d.id=c.academic_programs_id
                                        AND e.id=d.acad_program_groups_id
										AND b.students_idno = py.students_idno 
									order by f.inserted_on desc
									limit 1),(CASE 
													(SELECT 
															b.levels_id
														FROM
															students a
																LEFT JOIN
															basic_ed_histories b ON a.idno = b.students_idno
														WHERE
															a.idno = py.students_idno 
														order by levels_id desc limit 1) 
												WHEN 1 THEN null
                                                WHEN 2 THEN null
                                                WHEN 3 THEN NULL
                                                WHEN 4 THEN NULL
                                                WHEN 5 THEN NULL
                                                WHEN 6 THEN 6
                                                WHEN 7 THEN 7
                                                WHEN 11 THEN NULL
                                                WHEN 12 THEN 12
                                        END),
                                        (case adj.year_level
                                          when 11 then 12
                                          when 12 then 12
                                         end)) AS levels_id,
		    COALESCE(tc.teller_code, adj.description) AS tellercode,
		    adj.description AS description,
		    adj.id AS receipt,
		    py.students_idno as id,
		    COALESCE(op.name,
		            CONCAT(UPPER(s.lname),
		                    ', ',
		                    s.fname,
		                    ' ',
		                    s.mname),
		            CONCAT(emp.lname,
		                    ', ',
		                    emp.fname,
		                    ' ',
		                    emp.mname)) AS name,
		    COALESCE(ag.abbreviation,(SELECT 
										e.abbreviation
									FROM
										students a 
											INNER JOIN
										student_histories b ON a.idno = b.students_idno
											LEFT JOIN
										prospectus AS c ON c.id = b.prospectus_id
											LEFT JOIN
										academic_programs d ON d.id = c.academic_programs_id
											LEFT JOIN
										acad_program_groups e ON e.id = d.acad_program_groups_id
									WHERE
										a.idno = py.students_idno
									ORDER BY b.academic_terms_id desc
									limit 1),(SELECT 
							                    c.strand_code
							                FROM
							                    students a
							                        INNER JOIN
							                    basic_ed_histories b ON a.idno = b.students_idno
							                        LEFT JOIN
							                    strand c ON c.id = b.strand_id
							                WHERE
							                    a.idno = py.students_idno
							                    order by strand_code desc
							                LIMIT 1), (SELECT 
							                    bes.section_name
							                FROM
							                    students a
							                        INNER JOIN
							                    basic_ed_histories b ON a.idno = b.students_idno
							                        LEFT JOIN
							                    basic_ed_sections bes ON bes.id = b.basic_ed_sections_id
							                WHERE
							                    a.idno = py.students_idno
							                    order by section_name desc
							                LIMIT 1)) AS course,
		    COALESCE(adj.year_level,(SELECT 
										b.year_level
									FROM
										students a 
											INNER JOIN
										student_histories b ON a.idno = b.students_idno
											LEFT JOIN
										prospectus AS c ON c.id = b.prospectus_id
											LEFT JOIN
										academic_programs d ON d.id = c.academic_programs_id
									WHERE
										a.idno = py.students_idno
									ORDER BY year_level asc
									limit 1)) AS year_level,
		    adj.amount AS amount,
		    'n/a' AS method,
		    adj.adjustment_type AS type,
		    adj.remark,
            'ADJC' AS trangroup
		FROM
		    adjustments adj
		        LEFT JOIN
		    payers py ON py.id = adj.payers_id
		        LEFT JOIN
		    other_payers op ON op.id = py.other_payers_id
		        LEFT JOIN
		    employees emp ON emp.empno = py.employees_empno
		        LEFT JOIN
		    students s ON s.idno = py.students_idno
                LEFT JOIN
			student_histories sh ON sh.academic_terms_id=adj.academic_terms_id 
                AND sh.students_idno=s.idno
				LEFT JOIN
			col_students cl ON cl.students_idno = py.students_idno
				LEFT JOIN
			academic_programs ap ON ap.id=adj.academic_programs_id
				LEFT JOIN
			acad_program_groups ag on ag.id=ap.acad_program_groups_id
		        LEFT JOIN
			teller_codes tc on tc.teller_code = (
						SELECT teller_code
						from teller_codes as tc2
						where tc2.description2=substring_index(adj.description, ':', TRUE)
						limit 1
					)
		WHERE
			adj.posted = 'Y' 
		    AND adj.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)) t_
		   where t_.levels_id is not null
		UNION
      select date, levels_id, tellercode, description, receipt, id, name, course, year_level, amount,
		       method, adjtype, remark, trangroup
		from (
      select * from (
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    coalesce(j.levels_id,ag.levels_id,bsh.levels_id) AS levels_id,
		    tc.teller_code AS tellercode,
		    j.description AS description,
		    j.id AS receipt,
		    r.students_idno AS id,
		    CONCAT(UPPER(s.lname),
		                    ', ',
		                    s.fname,
		                    ' ',
		                    s.mname) AS name,
		    COALESCE((CASE coalesce(j.levels_id,ag.levels_id,bsh.levels_id)
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(j.year_level, 'n/a') AS year_level,
		    COALESCE(l.debit, COALESCE(l.credit, 0)) AS amount,
		    'n/a' AS method,
		    j.adjustment_type AS adjtype,
		    j.remark,
        'ADJB' AS trangroup
		FROM ledger l
      JOIN adjustments j ON j.id = l.adjustments_id
      JOIN payers r ON r.id = j.payers_id
      JOIN students s ON s.idno = r.students_idno
      JOIN academic_terms tr ON tr.id = j.academic_terms_id
      JOIN academic_years ay ON ay.id = tr.academic_years_id
      LEFT JOIN teller_codes tc on (tc.description = j.description or tc.description2 = j.description)
		LEFT JOIN (select * 
		           from basic_ed_histories bsh_
                 group by bsh_.students_idno, bsh_.academic_years_id
                 desc) bsh on bsh.students_idno=s.idno
	   LEFT JOIN (select *
	              from student_histories sh_
	              group by sh_.students_idno, sh_.academic_terms_id
					  desc) sh on sh.students_idno=s.idno
		LEFT JOIN prospectus prs on prs.id=sh.prospectus_id
		LEFT JOIN academic_programs ap on ap.id=prs.academic_programs_id
		LEFT JOIN acad_program_groups ag on ag.id=ap.acad_program_groups_id
		WHERE j.posted = 'Y'
		    and l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('$end_date',
		        INTERVAL '23:59:59' HOUR_SECOND)) t_
		where t_.levels_id in (1,2,3,4,11)
		group by t_.receipt) t2_
		UNION
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(ag.levels_id, 99) AS levels_id,
		    'Assessment' AS tellercode,
		    'Assessment' AS description,
		    l.assessments_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(st.lname,', ',st.fname,' ',st.mname) AS name,
		    COALESCE(ag.abbreviation,
		            (CASE ag.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(ass.year_level, 'n/a') AS year_level,
		    ass.assessed_amount AS amount,
		    'n/a' AS method,
		    'Debit' AS adjtype,
		    ass.remark,
            'ASSC' AS trangroup
		FROM
		    ledger l
		        LEFT JOIN
		    assessments ass ON ass.id = l.assessments_id
		        LEFT JOIN
		    student_histories sh ON sh.id = ass.student_histories_id
		        LEFT JOIN
		    col_students s ON s.students_idno = sh.students_idno
		        LEFT JOIN
			students st on st.idno=sh.students_idno
				JOIN
		    prospectus pr ON pr.id = sh.prospectus_id
		        JOIN
		    academic_programs ap ON ap.id = pr.academic_programs_id
		        JOIN
		    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
		        JOIN
		    colleges cl ON cl.id = ap.colleges_id
		WHERE
		    l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION 
		SELECT
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(ass.levels_id, 99) AS levels_id,
		    'Assessment' AS tellercode,
		    'Assessment' AS description,
		    l.assessments_bed_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(s.lname,', ',s.fname,' ',s.mname) AS name,
		    COALESCE((CASE ass.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(ass.year_level, 'n/a') AS year_level,
		    ass.assessed_amount AS amount,
		    'n/a' AS method,
		    'debit' AS adjtype,
		    COALESCE(ass.remark,' ') AS remark,
            'ASSB' AS trangroup
		FROM
		    assessments_bed ass
		        JOIN
		    ledger l ON l.assessments_bed_id = ass.id
		        JOIN
		    basic_ed_histories sh ON sh.id = ass.basic_ed_histories_id   
		        JOIN 
			students s on s.idno=sh.students_idno
		WHERE
		    l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('$end_date',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    ag.levels_id AS levels_id,
		    'Re-Enrollment' AS tellercode,
		    c.course_code AS description,
		    re.enrollments_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(st.lname, ', ', st.fname, ' ', st.mname) AS name,
		    COALESCE(ag.abbreviation,
		            (CASE ag.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    sh.year_level AS year_level,
		    re.amount_credited AS amount,
		    'n/a' AS method,
		    'Debit' AS adjtype,
		    l.remark,
            'REEN' AS trangroup
		FROM
		    ledger l
		        JOIN
		    re_enrollments re ON re.id = l.re_enrollments_id
		        JOIN
		    enrollments en ON en.id = re.enrollments_id
		        JOIN
		    course_offerings o ON o.id = en.course_offerings_id
		        JOIN
		    courses c ON c.id = o.courses_id
		        JOIN
		    student_histories sh ON sh.id = en.student_history_id
		        JOIN
		    col_students s ON s.students_idno = sh.students_idno
		        JOIN
		    prospectus pr ON pr.id = sh.prospectus_id
		        JOIN
		    academic_programs ap ON ap.id = pr.academic_programs_id
		        JOIN
		    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
		        JOIN
		    colleges cl ON cl.id = ap.colleges_id
				JOIN
			students st on st.idno = sh.students_idno
		WHERE
			l.status = 'posted' and
		    l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(ag.levels_id, 99) AS levels_id,
		    sk.scholarship_code AS tellercode,
		    sk.scholarship AS description,
		    l.privileges_availed_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(st.lname,', ',st.fname,' ',st.mname) AS name,
		    COALESCE(ag.abbreviation,
		            (CASE ag.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    sh.year_level AS year_level,
		    v.total_amount_availed AS amount,
		    'n/a' AS method,
		    'Credit' AS adjtype,
		    v.remark,
            'PRVC' AS trangroup
		FROM
		    ledger l
		        JOIN
		    privileges_availed v ON v.id = l.privileges_availed_id
		        JOIN
		    scholarships sk ON sk.id = v.scholarships_id
		        JOIN
		    student_histories sh ON sh.id = v.student_histories_id
		        JOIN
		    prospectus pr ON pr.id = sh.prospectus_id
		        JOIN
		    academic_programs ap ON ap.id = pr.academic_programs_id
		        JOIN
		    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
		        JOIN
		    colleges cl ON cl.id = ap.colleges_id
				JOIN
			students st on st.idno = sh.students_idno
		WHERE
		    v.posted = 'Y'
		        AND l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(sh.levels_id, 99) AS levels_id,
		    sk.scholarship_code AS tellercode,
		    sk.scholarship AS description,
		    l.privileges_availed_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(s.lname,', ',s.fname,' ',s.mname) AS name,
		    COALESCE((CASE sh.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(sh.yr_level, 'n/a') AS year_level,
		    v.total_amount_availed AS amount,
		    'n/a' AS method,
		    'Credit' AS adjtype,
		    COALESCE(v.remark,' ') AS remark,
            'PRVB' AS trangroup
		FROM
		    ledger l
		        JOIN
		    privileges_availed v ON v.id = l.privileges_availed_id
		        JOIN
		    scholarships sk ON sk.id = v.scholarships_id
		        JOIN
		    basic_ed_histories sh ON sh.id = v.basic_ed_histories_id
		        JOIN
		    students s on s.idno=sh.students_idno
		WHERE
		    v.posted = 'Y'
		        AND l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)) ar_mis 
		        WHERE levels_id is not null ";

		if ($summary){
			$sql .= "GROUP BY sequencer , trangroup, code";
		} else {

		//the sorting of the list shall be:
		//  1.  Level - (ie.Pre-School,Kinder,Grade,High,Senior High,College,Graduate School)
		//  2.  Program
		//      a.  Up to Senior High, Year Level is further grouped
		//      b.  College and Graduate School, only by Academic Program
		//      -this effect is done through the CODE field (a concatenation of 
	    //       of the teller code and the course and the year level according to
	    //       the above a and b)
		//  3.  Type - (Debit or Credit)
		// as per Mam Cecil (posting) by Toyet 2017.10
			$sql .= "GROUP BY sequencer , SUBSTRING_INDEX(code,':',-1), concat(trangroup,SUBSTRING_INDEX(code,':',true)), type";
		}

		//print_r($sql);die();
		log_message('info',"AR_BALANCE_MIS");  //toyet 9.20.2017
		log_message('info',print_r($sql,TRUE));  //toyet 9.20.2017

		$query = $this->db->query($sql);	
		return ($query ? $query->result() : FALSE);
	}


	function ar_balance_mis_raw($start_date,$end_date,$summary=FALSE){
		//die('work on progress...');	
		$sql = "
		SELECT * from (
		SELECT 
	        DATE_FORMAT(p.transaction_date, '%m/%d') AS date,
	            COALESCE(p.levels_id, 99) AS levels_id,
	            tc.teller_code AS tellercode,
	            tc.description AS description,
	            p.receipt_no AS receipt,
	            COALESCE(py.students_idno, p.payers_id, py.employees_empno) AS id,
	            COALESCE(op.name, CONCAT(UPPER(s.lname), ', ', s.fname, ' ', s.mname), CONCAT(emp.lname, ', ', emp.fname, ' ', emp.mname)) AS name,
	            COALESCE(ap.abbreviation, (CASE p.levels_id
	                WHEN 1 THEN 'Kinder '
	                WHEN 2 THEN 'Kinder '
	                WHEN 3 THEN 'Grade '
	                WHEN 4 THEN 'HS Day '
	                WHEN 5 THEN 'HS Night '
	                WHEN 11 THEN 'Pre-School '
	                WHEN 12 THEN 'Senior High '
	            END), 'n/a') AS course,
	            COALESCE(p.year_level, 'n/a') AS year_level,
	            COALESCE(p.receipt_amount,0) AS amount,
	            pm.payment_method AS method,
	            'Credit' AS type,
	            p.remark
	    FROM
	        payments p
	    LEFT JOIN payers py ON py.id = p.payers_id
	    LEFT JOIN payment_items pi ON pi.payments_id = p.id
	    LEFT JOIN teller_codes tc ON tc.id = pi.teller_codes_id
	    LEFT JOIN payment_methods pm ON pm.id = pi.id
	    LEFT JOIN other_payers op ON op.id = py.other_payers_id
	    LEFT JOIN employees emp ON emp.empno = py.employees_empno
	    LEFT JOIN students s ON s.idno = py.students_idno
	    LEFT JOIN academic_programs ap ON ap.id = p.academic_programs_id
	    WHERE
	        p.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
	    UNION
	    SELECT 
		    DATE_FORMAT(adj.transaction_date, '%m/%d') AS date,
		    COALESCE(ag.levels_id, 99) AS levels_id,
		    'Adjustment' AS tellercode,
		    adj.description AS description,
		    adj.id AS receipt,
		    COALESCE(py.students_idno,
		            adj.payers_id,
		            py.employees_empno) AS id,
		    COALESCE(op.name,
		            CONCAT(UPPER(s.lname),
		                    ', ',
		                    s.fname,
		                    ' ',
		                    s.mname),
		            CONCAT(emp.lname,
		                    ', ',
		                    emp.fname,
		                    ' ',
		                    emp.mname)) AS name,
		    COALESCE(ap.abbreviation,
		            (CASE ag.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(adj.year_level, 'n/a') AS year_level,
		    adj.amount AS amount,
		    'n/a' AS method,
		    adj.adjustment_type AS type,
		    adj.remark
		FROM
		    adjustments adj
		        LEFT JOIN
		    payers py ON py.id = adj.payers_id
		        LEFT JOIN
		    other_payers op ON op.id = py.other_payers_id
		        LEFT JOIN
		    employees emp ON emp.empno = py.employees_empno
		        LEFT JOIN
		    students s ON s.idno = py.students_idno
		        JOIN
		    student_histories sh ON s.idno = sh.students_idno
		        AND sh.academic_terms_id = adj.academic_terms_id
		        JOIN
		    prospectus pr ON pr.id = sh.prospectus_id
		        LEFT JOIN
		    academic_programs ap ON ap.id = adj.academic_programs_id
		        LEFT JOIN
		    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
		WHERE
		    adj.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(sh.levels_id, 99) AS levels_id,
		    'Adjustment' AS tellercode,
		    j.description AS description,
		    j.id AS receipt,
		    l.id AS id,
		    CONCAT(UPPER(s.lname),
		                    ', ',
		                    s.fname,
		                    ' ',
		                    s.mname) AS name,
		    COALESCE((CASE sh.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(sh.yr_level, 'n/a') AS year_level,
		    COALESCE(l.debit, COALESCE(l.credit, 0)) AS amount,
		    'n/a' AS method,
		    j.adjustment_type AS adjtype,
		    j.remark
		FROM
		    ledger l
		        JOIN
		    adjustments j ON j.id = l.adjustments_id
		        JOIN
		    payers r ON r.id = j.payers_id
		        JOIN
		    students s ON s.idno = r.students_idno
		        JOIN
		    academic_terms tr ON tr.id = j.academic_terms_id
		        JOIN
		    academic_years ay ON ay.id = tr.academic_years_id
		        JOIN
		    basic_ed_histories sh ON sh.students_idno = r.students_idno
		        AND sh.academic_years_id = ay.id
		        JOIN
		    levels lv ON lv.id = sh.levels_id
		WHERE
			sh.levels_id<>12 and
		    l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('$end_date',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(ag.levels_id, 99) AS levels_id,
		    'Assessment' AS tellercode,
		    'Assessment' AS description,
		    l.assessments_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(st.lname,', ',st.fname,' ',st.mname) AS name,
		    COALESCE(ap.abbreviation,
		            (CASE ag.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(ass.year_level, 'n/a') AS year_level,
		    ass.assessed_amount AS amount,
		    'n/a' AS method,
		    'Debit' AS adjtype,
		    ass.remark
		FROM
		    ledger l
		        LEFT JOIN
		    assessments ass ON ass.id = l.assessments_id
		        LEFT JOIN
		    student_histories sh ON sh.id = ass.student_histories_id
		        LEFT JOIN
		    col_students s ON s.students_idno = sh.students_idno
		        LEFT JOIN
			students st on st.idno=sh.students_idno
				JOIN
		    prospectus pr ON pr.id = sh.prospectus_id
		        JOIN
		    academic_programs ap ON ap.id = pr.academic_programs_id
		        JOIN
		    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
		        JOIN
		    colleges cl ON cl.id = ap.colleges_id
		WHERE
		    l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION 
		SELECT
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(ass.levels_id, 99) AS levels_id,
		    'Assessment' AS tellercode,
		    'Assessment' AS description,
		    l.assessments_bed_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(s.lname,', ',s.fname,' ',s.mname) AS name,
		    COALESCE((CASE ass.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(ass.year_level, 'n/a') AS year_level,
		    ass.assessed_amount AS amount,
		    'n/a' AS method,
		    'debit' AS adjtype,
		    COALESCE(ass.remark,' ') AS remark
		FROM
		    assessments_bed ass
		        JOIN
		    ledger l ON l.assessments_bed_id = ass.id
		        JOIN
		    basic_ed_histories sh ON sh.id = ass.basic_ed_histories_id   
		        JOIN 
			students s on s.idno=sh.students_idno
		WHERE
		    l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('$end_date',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    ag.levels_id AS levels_id,
		    'Re-Enrollment' AS tellercode,
		    c.course_code AS description,
		    re.enrollments_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(st.lname, ', ', st.fname, ' ', st.mname) AS name,
		    COALESCE(ap.abbreviation,
		            (CASE ag.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    sh.year_level AS year_level,
		    re.amount_credited AS amount,
		    'n/a' AS method,
		    'Debit' AS adjtype,
		    l.remark
		FROM
		    ledger l
		        JOIN
		    re_enrollments re ON re.id = l.re_enrollments_id
		        JOIN
		    enrollments en ON en.id = re.enrollments_id
		        JOIN
		    course_offerings o ON o.id = en.course_offerings_id
		        JOIN
		    courses c ON c.id = o.courses_id
		        JOIN
		    student_histories sh ON sh.id = en.student_history_id
		        JOIN
		    col_students s ON s.students_idno = sh.students_idno
		        JOIN
		    prospectus pr ON pr.id = sh.prospectus_id
		        JOIN
		    academic_programs ap ON ap.id = pr.academic_programs_id
		        JOIN
		    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
		        JOIN
		    colleges cl ON cl.id = ap.colleges_id
				JOIN
			students st on st.idno = sh.students_idno
		WHERE
		    l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION
		SELECT 
		    DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(ag.levels_id, 99) AS levels_id,
		    sk.scholarship_code AS tellercode,
		    sk.scholarship AS description,
		    l.privileges_availed_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(st.lname,', ',st.fname,' ',st.mname) AS name,
		    COALESCE(ap.abbreviation,
		            (CASE ag.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    sh.year_level AS year_level,
		    v.total_amount_availed AS amount,
		    'n/a' AS method,
		    'debit' AS adjtype,
		    v.remark
		FROM
		    ledger l
		        JOIN
		    privileges_availed v ON v.id = l.privileges_availed_id
		        JOIN
		    scholarships sk ON sk.id = v.scholarships_id
		        JOIN
		    student_histories sh ON sh.id = v.student_histories_id
		        JOIN
		    col_students s ON s.students_idno = sh.students_idno
		        JOIN
		    prospectus pr ON pr.id = sh.prospectus_id
		        JOIN
		    academic_programs ap ON ap.id = pr.academic_programs_id
		        JOIN
		    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
		        JOIN
		    colleges cl ON cl.id = ap.colleges_id
				JOIN
			students st on st.idno = sh.students_idno
		WHERE
		    v.posted = 'Y'
		        AND l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)
		UNION
		SELECT 
		   DATE_FORMAT(l.transaction_date, '%m/%d') AS date,
		    COALESCE(sh.levels_id, 99) AS levels_id,
		    sk.scholarship_code AS tellercode,
		    sk.scholarship AS description,
		    l.privileges_availed_id AS receipt,
		    sh.students_idno AS id,
		    CONCAT(s.lname,', ',s.fname,' ',s.mname) AS name,
		    COALESCE((CASE sh.levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		            END),
		            'n/a') AS course,
		    COALESCE(sh.yr_level, 'n/a') AS year_level,
		    v.total_amount_availed AS amount,
		    'n/a' AS method,
		    'Credit' AS adjtype,
		    COALESCE(v.remark,' ') AS remark
		FROM
		    ledger l
		        JOIN
		    privileges_availed v ON v.id = l.privileges_availed_id
		        JOIN
		    scholarships sk ON sk.id = v.scholarships_id
		        JOIN
		    basic_ed_histories sh ON sh.id = v.basic_ed_histories_id
		        JOIN
		    students s on s.idno=sh.students_idno
		WHERE
		    v.posted = 'Y'
		        AND l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
		        INTERVAL '23:59:59' HOUR_SECOND)) unified
		ORDER BY tellercode, levels_id, course, year_level, description, name;
		";
	
		//print_r($sql);die();
		////log_message('info','RAW RAW RAW RAW RAW RAW');  //toyet 9.22.2017
		////log_message('info',print_r($sql,TRUE));  //toyet 9.20.2017

		$query = $this->db->query($sql);	
		return ($query ? $query->result() : FALSE);
	}


	function ar_transactions($start_date,$end_date,$summary=FALSE){
		$sql = "
		select * from
		(
	
			SELECT levels_id, group_name, year_level, code, description, sum(id>0) as stud, sum(debit) as debit, sum(credit) as credit
			FROM
			(
			
				SELECT p.levels_id, ag.abbreviation as group_name, p.year_level, t.teller_code as code, t.description, l.id, l.debit, l.credit
				FROM ledger l
					join payments p on p.id = l.payments_id
					join payment_items i on i.payments_id = p.id
					join teller_codes t on t.id = i.teller_codes_id
					join academic_programs ap on ap.id = p.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
				WHERE
					p.levels_id in (6,7) 
					and p.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			UNION

				SELECT ag.levels_id, ag.abbreviation as group_name,sh.year_level, concat('Adj: ',j.description) as code, j.description, l.id , l.debit, l.credit
				from ledger l
					join adjustments j on j.id = l.adjustments_id
					join payers r on r.id = j.payers_id
					join col_students s on s.students_idno = r.students_idno
					join student_histories sh on s.students_idno = sh.students_idno and sh.academic_terms_id = j.academic_terms_id
					join academic_terms tr on tr.id = j.academic_terms_id
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
					join colleges cl on cl.id = ap.colleges_id
				WHERE
				j.posted='Y'
				and j.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)

			UNION
				SELECT ag.levels_id,ag.abbreviation as group_name,sh.year_level, 'Assessment' as code, 'Assessment' description, l.id , l.debit, l.credit
				FROM ledger l
					join assessments ass on ass.id = l.assessments_id
					join student_histories sh on sh.id = ass.student_histories_id
					join col_students s on s.students_idno = sh.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
					join colleges cl on cl.id = ap.colleges_id
				WHERE
				l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			UNION
				SELECT ag.levels_id,ag.abbreviation as group_name,sh.year_level, concat('Prv: ',sk.scholarship_code) as code, sk.scholarship description, l.id , l.debit, l.credit
				FROM ledger l
					join privileges_availed v on v.id = l.privileges_availed_id
					join scholarships sk on sk.id = v.scholarships_id
					join student_histories sh on sh.id = v.student_histories_id
					join col_students s on s.students_idno = sh.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
					join colleges cl on cl.id = ap.colleges_id
				WHERE
				v.posted = 'Y'
				and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			UNION
				SELECT ag.levels_id,ag.abbreviation as group_name, sh.year_level, 'Re-Enrollment' as code, c.course_code description, l.id , l.debit, l.credit
				FROM ledger l
					join re_enrollments re on re.id = l.re_enrollments_id
					join enrollments en on en.id = re.enrollments_id
					join course_offerings o on o.id = en.course_offerings_id
					join courses c on c.id = o.courses_id
					join student_histories sh on sh.id = en.student_history_id
					join col_students s on s.students_idno = sh.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
					join colleges cl on cl.id = ap.colleges_id
				WHERE
				l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
	
		) tmp"
		. ($summary ? " GROUP by levels_id, code" :  " GROUP by levels_id, group_name, code, (case when tmp.debit is null then 1 else 0 end) ")
		. ($summary ? " ORDER by levels_id, description": " ORDER by levels_id, group_name, description ");
		$sql .="
		) dt
		UNION ALL
		select * from
		(		
			SELECT p.levels_id, p.year_level as group_name, p.year_level, t.teller_code as code, t.description, sum(l.id>0) as stud, sum(debit) as debit, sum(credit) as credit
			FROM ledger l
				join payments p on p.id = l.payments_id and p.from_isis='N'
				join payment_items i on i.payments_id = p.id
				join payers r  on r.id = p.payers_id
				join teller_codes t on t.id = i.teller_codes_id
			WHERE
			p.levels_id not in (6,7) and 
			l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)"
			.($summary ? " GROUP by levels_id, code" :  " GROUP by levels_id, group_name, code")
		." UNION
			SELECT sh.levels_id, sh.yr_level group_name, sh.yr_level, concat('Adj: ',j.description) as code, j.description, sum(l.id>0) as stud, sum(debit) as debit, sum(credit) as credit
			from ledger l
				join adjustments j on j.id = l.adjustments_id
				join payers r on r.id = j.payers_id
				join basic_ed_students s on s.students_idno = r.students_idno
				join academic_terms tr on tr.id = j.academic_terms_id
				join academic_years ay on ay.id = tr.academic_years_id
				join basic_ed_histories sh on sh.students_idno =  r.students_idno and sh.academic_years_id = ay.id
				join levels lv on lv.id = sh.levels_id
			WHERE
				j.posted='Y' and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)"
				.($summary ? " GROUP by levels_id, code" : " GROUP by levels_id, group_name, code, (case when debit is null then 1 else 0 end)")
				.($summary ? " ORDER by levels_id, code" : " ORDER by levels_id, group_name, code ")
		.") dt2 ";
	
		//print_r($sql);die();
		$query = $this->db->query($sql);
		//print_r($query->result());die();
		return ($query ? $query->result() : FALSE);
	}
	
	


	public function ar_details_college_ver2($levels_id,$group_name,$year_level,$tran_code,$tran_type,$trangroup,$start_date,$end_date,$deskription,$summary,$l99='N'){

		if ($trangroup=='PYMT'){

			$sql = "
			SELECT 
			    s.idno,
			    CAP_FIRST(CONCAT_WS(', ', s.lname, s.fname)) AS student,
			    FORMAT(ABS(p.receipt_amount), 2) AS amount,
			    tc.description,
			    p.receipt_no AS ref,
			    CONCAT(COALESCE(ap.abbreviation,
			                    (CASE p.levels_id
			                        WHEN 1 THEN 'Kinder '
			                        WHEN 2 THEN 'Kinder '
			                        WHEN 3 THEN 'Grade '
			                        WHEN 4 THEN 'HS Day '
			                        WHEN 5 THEN 'HS Night '
			                        WHEN 11 THEN 'Pre-School '
			                        WHEN 12 THEN 'Senior High '
			                    END),
			                    'n/a'),' ',
			            COALESCE(p.year_level,'n/a')) AS course_year,
			    DATE_FORMAT(p.transaction_date,'%m/%d/%Y') as transaction_date,
			    'Credit' as type,
				COALESCE(ag.levels_id, (SELECT e.levels_id
									FROM 
										col_students AS b,
										students AS a,
                                        student_histories AS f,
										prospectus AS c,
										academic_programs AS d,
                                        acad_program_groups AS e
									WHERE
										a.idno=b.students_idno
										AND f.students_idno=a.idno
                                        AND c.id=f.prospectus_id
										AND	d.id=c.academic_programs_id
                                        AND e.id=d.acad_program_groups_id
										AND b.students_idno = py.students_idno 
									order by levels_id desc
									limit 1),(CASE 
													(SELECT 
															b.levels_id
														FROM
															students a
																LEFT JOIN
															basic_ed_histories b ON a.idno = b.students_idno
														WHERE
															a.idno = py.students_idno 
														order by levels_id desc limit 1) 
												WHEN 1 THEN null
                                                WHEN 2 THEN null
                                                WHEN 3 THEN NULL
                                                WHEN 4 THEN NULL
                                                WHEN 5 THEN NULL
                                                WHEN 6 THEN 6
                                                WHEN 7 THEN 7
                                                WHEN 11 THEN NULL
                                                WHEN 12 THEN 12
                                        END)) AS levels_id
			FROM
			    payments p
			        LEFT JOIN
			    payers py ON py.id = p.payers_id
			        LEFT JOIN
			    payment_items pi ON pi.payments_id = p.id
			        LEFT JOIN
			    teller_codes tc ON tc.id = pi.teller_codes_id
			        LEFT JOIN
			    payment_methods pm ON pm.id = pi.id
			        LEFT JOIN
			    other_payers op ON op.id = py.other_payers_id
			        LEFT JOIN
			    employees emp ON emp.empno = py.employees_empno
			        LEFT JOIN
			    students s ON s.idno = py.students_idno
			        LEFT JOIN
			    academic_programs ap ON ap.id = p.academic_programs_id
			    	LEFT JOIN
			    acad_program_groups ag on ag.id = ap.acad_program_groups_id
			WHERE
				p.status = 'posted' and
				tc.is_ledger = 'Y' and
			    ag.levels_id = '{$levels_id}'
			    AND tc.teller_code = SUBSTRING_INDEX('{$tran_code}', ':', TRUE) ";

			if ($levels_id==12 AND $summary=="false"){
				$sql .= "
				 and p.year_level = '{$year_level}' ";
			}

			if ($summary=="false"){
			    $sql .= "
			             AND COALESCE(ag.abbreviation,
			                    (CASE p.levels_id
			                        WHEN 1 THEN 'Kinder '
			                        WHEN 2 THEN 'Kinder '
			                        WHEN 3 THEN 'Grade '
			                        WHEN 4 THEN 'HS Day '
			                        WHEN 5 THEN 'HS Night '
			                        WHEN 11 THEN 'Pre-School '
			                        WHEN 12 THEN 'Senior High '
			                    END),
			                    'n/a') = '{$group_name}' ";
			} 

			    $sql .= "AND p.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
			        INTERVAL '23:59:59' HOUR_SECOND) "			
			."order by student;";

		} elseif ($trangroup=='ADJC'){

			$sql = "
			select adjtemp.idno,
			       adjtemp.student,
			       adjtemp.amount,
			       adjtemp.description,
			       adjtemp.ref,
			       adjtemp.course_year,
			       adjtemp.transaction_date,
			       adjtemp.type,
			       adjtemp.levels_id
			from (
			SELECT 
				s.idno,
				CAP_FIRST(CONCAT_WS(', ', s.lname, s.fname)) AS student,
				FORMAT(ABS(adj.amount), 2) AS amount,
				SUBSTRING_INDEX(adj.description, ':', TRUE) AS description,
				adj.id AS ref,
			    COALESCE(CONCAT(ap.abbreviation, ' ', adj.year_level),
			            (SELECT 
			                    CONCAT(d.abbreviation, ' ',adj.year_level) AS courseNyear
			                FROM
			                    students a
			                        INNER JOIN
			                    student_histories b ON a.idno = b.students_idno
			                        LEFT JOIN
			                    prospectus AS c ON c.id = b.prospectus_id
			                        LEFT JOIN
			                    academic_programs d ON d.id = c.academic_programs_id
			                WHERE
			                    a.idno = py.students_idno
			                ORDER BY b.academic_terms_id  desc
			                LIMIT 1),
		            COALESCE((SELECT 
		                    concat(c.strand_code,' ', b.yr_level)
		                FROM
		                    students a
		                        INNER JOIN
		                    basic_ed_histories b ON a.idno = b.students_idno
		                        LEFT JOIN
		                    strand c ON c.id = b.strand_id
		                WHERE
		                    a.idno = py.students_idno
		                    order by strand_code desc
		                LIMIT 1),(SELECT 
							                    CONCAT(bes.section_name,' ',b.yr_level)
							                FROM
							                    students a
							                        INNER JOIN
							                    basic_ed_histories b ON a.idno = b.students_idno
							                        LEFT JOIN
							                    basic_ed_sections bes ON bes.id = b.basic_ed_sections_id
							                WHERE
							                    a.idno = py.students_idno
							                    order by section_name desc
							                LIMIT 1))) AS course_year,
				DATE_FORMAT(adj.transaction_date, '%m/%d/%Y') AS transaction_date,
				adj.adjustment_type AS type,
	            COALESCE(CONCAT(ag.abbreviation, ' ', adj.year_level), (SELECT 
	                    CONCAT(e.abbreviation, ' ', adj.year_level) AS courseNyear
	                FROM
	                    students a
	                INNER JOIN student_histories b ON a.idno = b.students_idno
	                LEFT JOIN prospectus AS c ON c.id = b.prospectus_id
	                LEFT JOIN academic_programs d ON d.id = c.academic_programs_id
	                LEFT JOIN acad_program_groups e ON e.id = d.acad_program_groups_id
	                WHERE
	                    a.idno = py.students_idno
	                order by b.academic_terms_id desc
	                LIMIT 1),
		            COALESCE((SELECT 
		                    CONCAT(c.strand_code,' ', b.yr_level)
		                FROM
		                    students a
		                        INNER JOIN
		                    basic_ed_histories b ON a.idno = b.students_idno
		                        LEFT JOIN
		                    strand c ON c.id = b.strand_id
		                WHERE
		                    a.idno = py.students_idno
		                LIMIT 1),(SELECT 
							                    CONCAT(bes.section_name,' ',b.yr_level)
							                FROM
							                    students a
							                        INNER JOIN
							                    basic_ed_histories b ON a.idno = b.students_idno
							                        LEFT JOIN
							                    basic_ed_sections bes ON bes.id = b.basic_ed_sections_id
							                WHERE
							                    a.idno = py.students_idno
							                    order by section_name desc
							                LIMIT 1))) AS courseNYear2,
		        COALESCE(ag.levels_id, (SELECT e.levels_id
									FROM 
										col_students AS b,
										students AS a,
                                        student_histories AS f,
										prospectus AS c,
										academic_programs AS d,
                                        acad_program_groups AS e
									WHERE
										a.idno=b.students_idno
										AND f.students_idno=a.idno
                                        AND c.id=f.prospectus_id
										AND	d.id=c.academic_programs_id
                                        AND e.id=d.acad_program_groups_id
										AND b.students_idno = py.students_idno 
									order by f.inserted_on desc
									limit 1),(CASE 
													(SELECT 
															b.levels_id
														FROM
															students a
																LEFT JOIN
															basic_ed_histories b ON a.idno = b.students_idno
														WHERE
															a.idno = py.students_idno 
														order by levels_id desc limit 1) 
												WHEN 1 THEN null
                                                WHEN 2 THEN null
                                                WHEN 3 THEN NULL
                                                WHEN 4 THEN NULL
                                                WHEN 5 THEN NULL
                                                WHEN 6 THEN 6
                                                WHEN 7 THEN 7
                                                WHEN 11 THEN NULL
                                                WHEN 12 THEN 12
                                        END),(case adj.year_level
                                                  when 11 then 12
                                                  when 12 then 12
													       END )) AS levels_id,
                ag.group_name as groupname
			FROM
			    adjustments adj
			        LEFT JOIN
			    payers py ON py.id = adj.payers_id
			        LEFT JOIN
			    other_payers op ON op.id = py.other_payers_id
			        LEFT JOIN
			    employees emp ON emp.empno = py.employees_empno
			        LEFT JOIN
			    students s ON s.idno = py.students_idno
					LEFT JOIN
				academic_programs ap ON ap.id=adj.academic_programs_id
					LEFT JOIN
				acad_program_groups ag on ag.id=ap.acad_program_groups_id
			WHERE
				adj.posted = 'Y' ";

			$sql .= "AND adj.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
			         INTERVAL '23:59:59' HOUR_SECOND)) adjtemp
			         WHERE 
						adjtemp.levels_id is not null and
						adjtemp.description = SUBSTRING_INDEX('{$deskription}', ':', TRUE) ";

			if ($summary=="false"){
				$sql .= "AND adjtemp.type = '{$tran_type}' "; 
				if ($levels_id==12){
					$sql .= "AND adjtemp.courseNyear2 LIKE '{$group_name}%' ";
					if($l99=='N'){
						$sql .= "AND InStr(adjtemp.course_year,'{$year_level}')>0 ";
					}
				} else {
			        $sql .= "AND adjtemp.groupname LIKE '{$group_name}' ";
				}
			} else {
				$sql .= "AND adjtemp.levels_id = '{$levels_id}' ";
			}

			$sql .= "order by student;";

		} elseif ($trangroup=='ASSC'){

			$sql = "
			SELECT 
				st.idno,
	            CONCAT(st.lname,', ',st.fname,' ',st.mname) AS student,
	            FORMAT(ABS(ass.assessed_amount), 2) AS amount,
	            'Assessment' as description,
	            l.assessments_id AS ref,
	            CONCAT(ap.abbreviation,' ',ass.year_level) AS course_year,
	            DATE_FORMAT(l.transaction_date, '%m/%d/%Y') AS transaction_date,
	            'Debit' as type
			FROM
			    ledger l
			        LEFT JOIN
			    assessments ass ON ass.id = l.assessments_id
			        LEFT JOIN
			    student_histories sh ON sh.id = ass.student_histories_id
			        LEFT JOIN
			    col_students s ON s.students_idno = sh.students_idno
			        LEFT JOIN
				students st on st.idno=sh.students_idno
					JOIN
			    prospectus pr ON pr.id = sh.prospectus_id
			        JOIN
			    academic_programs ap ON ap.id = pr.academic_programs_id
			        JOIN
			    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
			        JOIN
			    colleges cl ON cl.id = ap.colleges_id
			WHERE
				l.deleted = 'N' 
				AND ag.levels_id = '{$levels_id}' ";

				if ($levels_id==12){
					$sql .= " and ass.year_level = '{$year_level}' ";
				}

				if ($summary=='false'){
					$sql .= "AND ag.abbreviation = '{$group_name}' ";
				}

				$sql .= "AND l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
					INTERVAL '23:59:59' HOUR_SECOND) order by student;;";

		} elseif ($trangroup=='PRVC'){

			$sql = "
			SELECT 
				st.idno,
	            CONCAT(st.lname,', ',st.fname,' ',st.mname) AS student,
	            FORMAT(ABS(v.total_amount_availed), 2) AS amount,
	            sk.scholarship as description,
	            l.privileges_availed_id AS ref,
	            CONCAT(ap.abbreviation,' ',sh.year_level) AS course_year,
	            DATE_FORMAT(l.transaction_date, '%m/%d/%Y') AS transaction_date,
	            'Credit' AS type
			FROM
			    ledger l
			        JOIN
			    privileges_availed v ON v.id = l.privileges_availed_id
			        JOIN
			    scholarships sk ON sk.id = v.scholarships_id
			        JOIN
			    student_histories sh ON sh.id = v.student_histories_id
			        JOIN
			    prospectus pr ON pr.id = sh.prospectus_id
			        JOIN
			    academic_programs ap ON ap.id = pr.academic_programs_id
			        JOIN
			    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
			        JOIN
			    colleges cl ON cl.id = ap.colleges_id
					JOIN
				students st on st.idno = sh.students_idno
			WHERE
			    v.posted = 'Y' 
			    	AND l.deleted = 'N' ";

			if ($summary=='false'){
				$sql .= "AND ag.abbreviation = '{$group_name}' ";
				if($levels_id==12){
					$sql .= "AND sh.year_level = '{$year_level}' ";
				}
			}

				$sql .= "AND ag.levels_id = '{$levels_id}'
				         AND sk.scholarship_code = SUBSTRING_INDEX('{$tran_code}', ':', TRUE)
			        AND l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
			        INTERVAL '23:59:59' HOUR_SECOND)
			    order by student;;
			";

		}  elseif ($trangroup=='REEN'){

			$sql = "
			SELECT 
				st.idno,
	            CONCAT(st.lname,', ',st.fname,' ',st.mname) AS student,
	            FORMAT(ABS(re.amount_credited), 2) AS amount,
	            c.course_code as description,
	            re.enrollments_id AS ref,
	            CONCAT(ap.abbreviation,' ',sh.year_level) AS course_year,
	            DATE_FORMAT(l.transaction_date, '%m/%d/%Y') AS transaction_date,
	            'Debit' AS type
			FROM
			    ledger l
			        JOIN
			    re_enrollments re ON re.id = l.re_enrollments_id
			        JOIN
			    enrollments en ON en.id = re.enrollments_id
			        JOIN
			    course_offerings o ON o.id = en.course_offerings_id
			        JOIN
			    courses c ON c.id = o.courses_id
			        JOIN
			    student_histories sh ON sh.id = en.student_history_id
			        JOIN
			    col_students s ON s.students_idno = sh.students_idno
			        JOIN
			    prospectus pr ON pr.id = sh.prospectus_id
			        JOIN
			    academic_programs ap ON ap.id = pr.academic_programs_id
			        JOIN
			    acad_program_groups ag ON ag.id = ap.acad_program_groups_id
			        JOIN
			    colleges cl ON cl.id = ap.colleges_id
					JOIN
				students st on st.idno = sh.students_idno
			WHERE
				l.delete = 'N' 
				AND ap.abbreviation = '{$group_name}' ";

			if ($summary=="false"){
				$sql .= "AND sh.year_level = '{$year_level} '";
			}

			$sql .= "AND l.transaction_date BETWEEN '2016-06-01' AND DATE_ADD('2017-08-31',
			        INTERVAL '23:59:59' HOUR_SECOND);";

		}
			// //log_message('info', 'TOYET: ');
			// //log_message('info', 'TOYET: ');
			// //log_message('INFO', '>>'.$group_name.'<<');
			// //log_message('INFO', '>>'.$year_level.'<<');
			// //log_message('INFO', '>>'.$levels_id.'<<');
			// //log_message('INFO', '>>'.$tran_code.'<<');
			// //log_message('INFO', '>>'.$end_date.'<<');
			// //log_message('INFO', '>>'.$start_date.'<<');
			// //log_message('INFO', '>>'.$tran_type.'<<');
			// //log_message('INFO', '>>'.$trangroup.'<<');
			// //log_message('INFO', '>>'.$deskription.'<<');
			// //log_message('INFO', '>>'.$summary.'<<');

			//print_r($sql);die();
			//var_dump($sql); exit();
			log_message('INFO',print_r($sql,TRUE));  //toyet 9.20.2017 //Toyet 7.13.2018

			$query = $this->db->query($sql);

			////log_message('info',print_r($query,TRUE));  //toyet 9.20.2017

			return ($query ? $query : FALSE);
		}

		public function ar_details_bed_ver2($levels_id,$group_name, $year_level,$tran_code,$tran_type,$trangroup,$start_date,$end_date,$summary){

			if ($trangroup=='PYMT'){
				$sql = "	
				SELECT 
				    COALESCE(s.idno,py.employees_empno,py.other_payers_id) as idno,
    				COALESCE(op.name, CONCAT(UPPER(s.lname), ', ', s.fname, ' ', s.mname), 
					CONCAT(emp.lname, ', ', emp.fname, ' ', emp.mname)) AS student,
				    FORMAT(ABS(p.receipt_amount), 2) AS amount,
				    tc.description,
				    p.receipt_no AS ref,
				    CONCAT(COALESCE(ap.abbreviation,
				                    (CASE p.levels_id
				                        WHEN 1 THEN 'Kinder '
				                        WHEN 2 THEN 'Kinder '
				                        WHEN 3 THEN 'Grade '
				                        WHEN 4 THEN 'HS Day '
				                        WHEN 5 THEN 'HS Night '
				                        WHEN 11 THEN 'Pre-School '
				                        WHEN 12 THEN 'Senior High '
				                    END),
				                    'n/a'),
				            coalesce(p.year_level,' ')) AS course_year,
				    DATE_FORMAT(p.transaction_date, '%m/%d/%Y') as transaction_date,
				    'Credit' AS type
				FROM
				    payments p
				        LEFT JOIN
				    payers py ON py.id = p.payers_id
				        LEFT JOIN
				    payment_items pi ON pi.payments_id = p.id
				        LEFT JOIN
				    teller_codes tc ON tc.id = pi.teller_codes_id
				        LEFT JOIN
				    payment_methods pm ON pm.id = pi.id
				        LEFT JOIN
				    other_payers op ON op.id = py.other_payers_id
				        LEFT JOIN
				    employees emp ON emp.empno = py.employees_empno
				        LEFT JOIN
				    students s ON s.idno = py.students_idno
				        LEFT JOIN
				    academic_programs ap ON ap.id = p.academic_programs_id
				WHERE ";

				    if ($levels_id=='99'){
				    	$sql .= "(p.levels_id = '".$levels_id."' or p.levels_id is NULL) ";
				    } else {
				    	$sql .= "p.levels_id = '".$levels_id."' ";
				    }

				$sql .= "AND tc.teller_code = SUBSTRING_INDEX('{$tran_code}', ':', TRUE)";

				if ($summary=='false'){
				    $sql .= " AND (p.year_level = '{$year_level}' or p.year_level is NULL) ";
				}
				    //$groupsearch = array('n/a','Kinder','Grade','HS Day','HS Night','Pre-School');
				    $groupsearch = array('n/a','Kinder','Grade','HS Day','HS Night','Pre-School');
				    if (!in_array($group_name, $groupsearch) and ($levels_id=='99')){
				         $sql .= "AND ap.abbreviation = '{$group_name}' "; 
				    }

				    $sql .= "AND tc.is_ledger = 'Y' AND p.status = 'posted' AND p.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "	
				."order by student
				";

			} ELSEIF ($trangroup=='ADJB') {
				$sql = "
				SELECT 
				    s.idno,
				    CONCAT_WS(', ', s.lname, s.fname) AS student,
				    FORMAT(ABS(j.amount), 2) AS amount,
				    j.description,
				    j.id AS ref,
				    concat((CASE coalesce(j.levels_id,sh.levels_id)
								WHEN 1 THEN 'Kinder '
								WHEN 2 THEN 'Kinder '
								WHEN 3 THEN 'Grade '
								WHEN 4 THEN 'HS Day '
								WHEN 5 THEN 'HS Night '
								WHEN 11 THEN 'Pre-School '
								WHEN 12 THEN 'Senior High '
							END),' ',
				            coalesce(j.year_level,sh.yr_level)) AS course_year,
				    DATE_FORMAT(j.transaction_date, '%m/%d/%Y') as transaction_date,
				    j.adjustment_type AS type
				FROM
				    ledger l
				        JOIN
				    adjustments j ON j.id = l.adjustments_id
				        JOIN
				    payers r ON r.id = j.payers_id
				        JOIN
				    students s ON s.idno = r.students_idno
				        JOIN
				    academic_terms tr ON tr.id = j.academic_terms_id
				        JOIN
				    academic_years ay ON ay.id = tr.academic_years_id
				        JOIN
				    (select * 
					  from basic_ed_histories bsh_
					  group by bsh_.students_idno, bsh_.academic_years_id
					  desc) sh ON sh.students_idno = r.students_idno
				        AND sh.academic_years_id = ay.id
				        JOIN
				    levels lv ON lv.id = coalesce(j.levels_id,sh.levels_id)
				        LEFT JOIN
				    teller_codes tc ON (tc.description = j.description
				        OR tc.description2 = j.description)
				WHERE
					l.deleted = 'N' ";

				if ($summary=='false'){
				    $sql .= "AND coalesce(j.year_level,sh.yr_level) = '{$year_level}' ";
				}

				    $sql .= "AND tc.teller_code = SUBSTRING_INDEX('{$tran_code}', ':', TRUE)
				        AND coalesce(j.levels_id,sh.levels_id) = '{$levels_id}'
				        AND j.adjustment_type = '{$tran_type}'
				        AND j.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
				        INTERVAL '23:59:59' HOUR_SECOND)
				ORDER BY student";
			 ////log_message('info', 'TOYET: ');
			 ////log_message('info', 'TOYET: ');
			 ////log_message('INFO', '>>'.$group_name.'<<');
			 ////log_message('INFO', '>>'.$year_level.'<<');
			 ////log_message('INFO', '>>'.$levels_id.'<<');
			 ////log_message('INFO', '>>'.$tran_code.'<<');
			 ////log_message('INFO', '>>'.$end_date.'<<');
			 ////log_message('INFO', '>>'.$start_date.'<<');
			 ////log_message('INFO', '>>'.$tran_type.'<<');
			 ////log_message('INFO', '>>'.$trangroup.'<<');
			 ////log_message('INFO', '>>'.$deskription.'<<');
			 ////log_message('INFO', '>>'.$summary.'<<');
			 //log_message("INFO", print_r($sql,true)); // Toyet 8.14.2018

			} ELSEIF ($trangroup=='ASSB') {
				$sql = "
				SELECT 
					sh.students_idno AS idno,
					CONCAT_WS(', ', s.lname, s.fname) AS student,    
					ass.assessed_amount AS amount,
					'Assessment' AS description,
				    l.assessments_bed_id AS ref,
				    CONCAT((CASE ass.levels_id
										WHEN 1 THEN 'Kinder '
				                        WHEN 2 THEN 'Kinder '
				                        WHEN 3 THEN 'Grade '
				                        WHEN 4 THEN 'HS Day '
				                        WHEN 5 THEN 'HS Night '
				                        WHEN 11 THEN 'Pre-School '
				                        WHEN 12 THEN 'Senior High '
				                    END),
				            ass.year_level) AS course_year, 
				    DATE_FORMAT(l.transaction_date, '%m/%d/%Y') as transaction_date,
				    'Debit' AS type
				FROM
				    assessments_bed ass
				        JOIN
				    ledger l ON l.assessments_bed_id = ass.id
				        JOIN
				    basic_ed_histories sh ON sh.id = ass.basic_ed_histories_id
				        JOIN
				    students s ON s.idno = sh.students_idno
				WHERE
					l.deleted = 'N' ";

				if ($summary=='false'){
					$sql .= "and ass.year_level = '{$year_level}' ";
				}

				$sql .="and ass.levels_id = '{$levels_id}'				
				    and l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
				        INTERVAL '23:59:59' HOUR_SECOND) order by student;
				";
				
			} ELSEIF ($trangroup=='PRVB') {

				$sql = "
				SELECT 
					s.idno,
				    CONCAT_WS(', ', s.lname, s.fname) AS student,
				    FORMAT(ABS(v.total_amount_availed), 2) AS amount,
				    sk.scholarship as description,
				    l.privileges_availed_id AS ref,
					CONCAT((CASE sh.levels_id
									WHEN 1 THEN 'Kinder '
									WHEN 2 THEN 'Kinder '
									WHEN 3 THEN 'Grade '
									WHEN 4 THEN 'HS Day '
									WHEN 5 THEN 'HS Night '
									WHEN 11 THEN 'Pre-School '
									WHEN 12 THEN 'Senior High '
								END),
								sh.yr_level) AS course_year,
					date_format(l.transaction_date,'%m/%d/%Y') as transaction_date,
					'Credit' AS type
				FROM
				    ledger l
				        JOIN
				    privileges_availed v ON v.id = l.privileges_availed_id
				        JOIN
				    scholarships sk ON sk.id = v.scholarships_id
				        JOIN
				    basic_ed_histories sh ON sh.id = v.basic_ed_histories_id
				        JOIN
				    students s ON s.idno = sh.students_idno
				WHERE
				    v.posted = 'Y' and l.deleted = 'N'
						AND sh.levels_id = '{$levels_id}' ";

				if ($summary=='false'){
				    $sql .= "AND sh.yr_level = '{$year_level}' ";
				}

				    $sql .= "AND sk.scholarship_code = SUBSTRING_INDEX('{$tran_code}',':',true)
				        AND l.transaction_date BETWEEN '{$start_date}' AND DATE_ADD('{$end_date}',
				        INTERVAL '23:59:59' HOUR_SECOND)"
				    ."order by student;";
			}
			
			////log_message('info',$trangroup);
			////log_message('info',print_r($sql,TRUE));

			$query = $this->db->query($sql);
			return ($query ? $query : FALSE);
		}
	
	function ar_schedule_individual($idno, $date){
		$sql="
			SELECT idno as students_idno, sum(ifnull(debit, 0) - ifnull(credit,0)) as balance 
			FROM
				(
					SELECT l.transaction_date, s.idno, l.debit, l.credit
					FROM ledger l
						join payments p on p.id = l.payments_id
						join payers r  on r.id = p.payers_id
						join student_histories sh on sh.students_idno = r.students_idno
						join students s on s.idno = sh.students_idno 
						join academic_terms tr on tr.id = p.academic_terms_id and  tr.id = sh.academic_terms_id
					WHERE
						l.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
						AND s.idno = '{$idno}'
				UNION
					SELECT l.transaction_date,  s.idno, l.debit, l.credit
					from ledger l
						join adjustments j on j.id = l.adjustments_id
						join payers r on r.id = j.payers_id
						join students s on s.idno = r.students_idno 
						join student_histories sh on s.idno= sh.students_idno and sh.academic_terms_id = j.academic_terms_id
						join academic_terms tr on tr.id = j.academic_terms_id
					WHERE
						j.posted='Y' and j.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
						AND s.idno = '{$idno}'
				UNION
					SELECT l.transaction_date,  s.idno, l.debit, l.credit
					FROM ledger l
						join assessments ass on ass.id = l.assessments_id
						join student_histories sh on sh.id = ass.student_histories_id
						join students s on s.idno = sh.students_idno 
					WHERE
						l.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
						AND s.idno = '{$idno}'
				UNION
					SELECT l.transaction_date,  s.idno, l.debit, l.credit
					FROM ledger l
						join privileges_availed v on v.id = l.privileges_availed_id
						join student_histories sh on sh.id = v.student_histories_id
						join students s on s.idno = sh.students_idno 
					WHERE
						v.posted = 'Y' and l.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
						AND s.idno = '{$idno}'
				UNION
					SELECT l.transaction_date, s.idno, l.debit, l.credit
					FROM ledger l
						join re_enrollments re on re.id = l.re_enrollments_id
						join enrollments en on en.id = re.enrollments_id
						join student_histories sh on sh.id = en.student_history_id
						join students s on s.idno = sh.students_idno 
					WHERE
						l.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
						AND s.idno = '{$idno}'		
			) tmp
			GROUP by students_idno
			";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
		
		
		
	}
		
		
	function ar_schedule($end_date, $colleges_id){
		$sql = "
			SELECT idno as students_idno, student, year_level, abbreviation, sum(ifnull(debit, 0) - ifnull(credit,0)) as balance 
			FROM
			(
				SELECT colleges_id, st.idno, cap_first(concat(st.lname,', ',st.fname)) as student, sh.year_level, ap.abbreviation, l.debit, l.credit
				FROM ledger l
					join payments p on p.id = l.payments_id
					join payers r  on r.id = p.payers_id
					join students st on st.idno = r.students_idno 
					join student_histories sh on sh.students_idno = st.idno
					join academic_terms tr on tr.id = p.academic_terms_id and  tr.id = sh.academic_terms_id
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
				WHERE
					l.transaction_date <= date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
					AND ap.colleges_id = '{$colleges_id}'
			UNION
				SELECT colleges_id, st.idno, cap_first(concat(st.lname,', ',st.fname)) as student, sh.year_level, ap.abbreviation, l.debit, l.credit
				from ledger l
					join adjustments j on j.id = l.adjustments_id
					join payers r on r.id = j.payers_id
					join students st on st.idno = r.students_idno 
					join student_histories sh on st.idno = sh.students_idno and sh.academic_terms_id = j.academic_terms_id
					join academic_terms tr on tr.id = j.academic_terms_id
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
				WHERE
					j.posted='Y' and j.transaction_date <= date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
					AND ap.colleges_id = '{$colleges_id}'
			UNION
				SELECT colleges_id, st.idno, cap_first(concat(st.lname,', ',st.fname)) as student, sh.year_level, ap.abbreviation, l.debit, l.credit
				FROM ledger l
					join assessments ass on ass.id = l.assessments_id
					join student_histories sh on sh.id = ass.student_histories_id
					join students st on st.idno = sh.students_idno 
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
				WHERE
					l.transaction_date <= date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
					AND ap.colleges_id = '{$colleges_id}'
			UNION
				SELECT colleges_id, st.idno, cap_first(concat(st.lname,', ',st.fname)) as student, sh.year_level, ap.abbreviation, l.debit, l.credit
				FROM ledger l
					join privileges_availed v on v.id = l.privileges_availed_id
					join student_histories sh on sh.id = v.student_histories_id
					join students st on st.idno = sh.students_idno 
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
				WHERE
					v.posted = 'Y' and l.transaction_date <= date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
					AND ap.colleges_id = '{$colleges_id}'
			UNION
				SELECT colleges_id, st.idno, cap_first(concat(st.lname,', ',st.fname)) as student, sh.year_level, ap.abbreviation, l.debit, l.credit
				FROM ledger l
					join re_enrollments re on re.id = l.re_enrollments_id
					join enrollments en on en.id = re.enrollments_id
					join student_histories sh on sh.id = en.student_history_id
					join students st on st.idno = sh.students_idno 
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
				WHERE
					l.transaction_date <= date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
					AND ap.colleges_id = '{$colleges_id}'
		
			) tmp
			GROUP by colleges_id, students_idno
			ORDER BY student 
		"; 

		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	
	
	}

	public function ar_transactions_details_college($levels_id,$group_name,$year_level,$tran_code,$tran_type,$start_date,$end_date){
		
		$sql_for_payments = "
			select s.idno, cap_first(concat_ws(', ',s.lname, s.fname)) as student,format(abs(p.receipt_amount),2) as amount, t.description,
				l.reference_number as ref, concat_ws(' ',ap.abbreviation,p.year_level) as course_year,ap.abbreviation, l.transaction_date
			from payments p
				join ledger 	   l on l.payments_id = p.id
				join payment_items i on i.payments_id = p.id
				join teller_codes  t on t.id = i.teller_codes_id
				join payers        r on r.id = p.payers_id
				join students 	 	s on s.idno = r.students_idno
				join academic_programs ap on ap.id = p.academic_programs_id				
				join acad_program_groups ag on ag.id = ap.acad_program_groups_id
			where
				p.levels_id = {$this->db->escape($levels_id)}
				and t.teller_code = {$this->db->escape($tran_code)}
				and ag.abbreviation =  {$this->db->escape($group_name)}
				and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "
				." and l.{$tran_type} > 0 "
				."order by student
		";
	
		$sql_for_assessments = "
				SELECT ass.id, st.idno, cap_first(concat_ws(', ',st.lname, st.fname)) student, format(ass.assessed_amount,2) as amount,
					'Assessment' as  description, l.reference_number as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year,ap.abbreviation, l.transaction_date
				from ledger l
					join assessments ass on ass.id = l.assessments_id
					join student_histories sh on sh.id = ass.student_histories_id
					join col_students s on s.students_idno = sh.students_idno
					join students st on st.idno = s.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
					join colleges cl on cl.id = ap.colleges_id
				WHERE
						ag.levels_id = {$this->db->escape($levels_id)}
						and ag.abbreviation =  {$this->db->escape($group_name)}
						and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)"
						." and l.{$tran_type} > 0 "
						."ORDER BY student
				";
	
			$sql_for_adjustment = "
				SELECT j.id, st.idno, cap_first(concat_ws(', ',st.lname, st.fname)) as student, format(j.amount,2) as amount,
					j.description, j.id as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year,ap.abbreviation, j.transaction_date
				from ledger l
					join adjustments j on j.id = l.adjustments_id
					join payers r on r.id = j.payers_id
					join col_students s on s.students_idno = r.students_idno
					join students st on st.idno = s.students_idno
					join student_histories sh on s.students_idno = sh.students_idno and sh.academic_terms_id = j.academic_terms_id
					join academic_terms tr on tr.id = j.academic_terms_id
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
					join colleges cl on cl.id = ap.colleges_id
				WHERE
					j.posted='Y'
					and ag.levels_id = {$this->db->escape($levels_id)}
					and ag.abbreviation =  {$this->db->escape($group_name)}
					and j.description = substr({$this->db->escape($tran_code)},6)
					and j.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)"
					." and l.{$tran_type} > 0 "
					."ORDER BY student"
			;

			$sql_for_privilege = "
				SELECT v.id, st.idno, cap_first(concat_ws(', ',st.lname, st.fname)) as student, format(v.total_amount_availed,2) amount,
					sk.scholarship description, v.id as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year,ap.abbreviation, l.transaction_date
				FROM ledger l
					join privileges_availed v on v.id = l.privileges_availed_id
					join scholarships sk on sk.id = v.scholarships_id
					join student_histories sh on sh.id = v.student_histories_id
					join col_students s on s.students_idno = sh.students_idno
					join students st on st.idno = s.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
					join colleges cl on cl.id = ap.colleges_id
				WHERE
					v.posted='Y'
					and ag.levels_id = {$this->db->escape($levels_id)}
					and ag.abbreviation =  {$this->db->escape($group_name)}
					and sk.scholarship_code = substr({$this->db->escape($tran_code)},6)
					and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)"
					." and l.{$tran_type} > 0 "
					."ORDER BY student"
				;

			$sql_for_reenrollment ="
				SELECT re.id, st.idno, cap_first(concat_ws(', ',st.lname, st.fname)) as student, format(re.amount_credited,2) as amount,
					c.course_code description, re.id as ref, concat_ws(' ',ap.abbreviation,sh.year_level) as course_year,ap.abbreviation, l.transaction_date
				FROM ledger l
					join re_enrollments re on re.id = l.re_enrollments_id
					join enrollments en on en.id = re.enrollments_id
					join course_offerings o on o.id = en.course_offerings_id
					join courses c on c.id = o.courses_id
					join student_histories sh on sh.id = en.student_history_id
					join col_students s on s.students_idno = sh.students_idno
					join students st on st.idno = s.students_idno
					join prospectus pr on pr.id = sh.prospectus_id
					join academic_programs ap on ap.id = pr.academic_programs_id
					join acad_program_groups ag on ag.id = ap.acad_program_groups_id
					join colleges cl on cl.id = ap.colleges_id
				WHERE
					ag.levels_id = {$this->db->escape($levels_id)}
					and ag.abbreviation =  {$this->db->escape($group_name)}
					and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) "
					." and l.{$tran_type} > 0 "
					."ORDER BY student"
			;
	
			switch ($tran_code){
				case 'Assessment':
					$sql = $sql_for_assessments ;
						break;
						/* case 'Privilege':
						$sql = $sql_for_privilege ;
						break; */
						case 'Re-Enrollment':
						$sql = $sql_for_reenrollment;
						break;
				default:
					switch( substr($tran_code,0,5) ){
						case 'Adj: ':
						$sql = $sql_for_adjustment;
						break;
							case 'Prv: ':
								$sql = $sql_for_privilege ;
								break;
							default:
							$sql = $sql_for_payments;
							break;
						}
			}

			////log_message('INFO', print_r($sql,true));
			$query = $this->db->query($sql);
			return ($query ? $query : FALSE);
	}

	public function ar_transactions_details_bed($levels_id, $group_name, $year_level, $tran_code,$tran_type,$start_date,$end_date){
		$sql = "
		select * from
			(
			select
				s.idno, cap_first(concat_ws(', ',st.lname, st.fname)) as student,format(abs(p.receipt_amount),2) as amount, t.description,
				l.reference_number as ref, concat(lv.level,' ',sh.yr_level) course_year, l.transaction_date
			from ledger l				
				join payments p on p.id = l.payments_id and p.from_isis='N'
				join payment_items i on i.payments_id = p.id
				join payers r  on r.id = p.payers_id
				join teller_codes t on t.id = i.teller_codes_id
				join students s on s.idno = r.students_idno
				join academic_terms tr on tr.id = p.academic_terms_id
				join academic_years ay on ay.id = tr.academic_years_id
				join basic_ed_histories sh on sh.students_idno = r.students_idno and sh.academic_years_id = ay.id
				join levels lv on lv.id = sh.levels_id
				join students st on st.idno = s.students_idno
			where
				lv.id = {$this->db->escape($levels_id)}
				and t.teller_code = {$this->db->escape($tran_code)}
				and sh.yr_level = {$this->db->escape($year_level)} "
				." and l.{$tran_type} > 0 "
				." and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			) tmp1
		union 
			select * from
			(
			SELECT 
				st.idno, cap_first(concat_ws(', ',st.lname, st.fname)) student, format(ass.assessed_amount,2) as amount, 'Assessment' as  description,
				l.reference_number as ref, concat(lv.level,' ',sh.yr_level) course_year, l.transaction_date
			FROM ledger l
				join assessments_bed ass on ass.id = l.assessments_bed_id 
				join basic_ed_histories sh on sh.id = ass.basic_ed_histories_id
				join students s on s.idno = sh.students_idno
				join levels lv on lv.id = sh.levels_id
			 WHERE
				lv.id       = {$this->db->escape($levels_id)} and
				sh.yr_level = {$this->db->escape($year_level)} and
				l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) and
				l.{$tran_type} > 0
			) tmp2
		union
			select * from
			(
			select
				s.idno, cap_first(concat_ws(', ',s.lname, s.fname)) as student,format(abs(j.amount),2) as amount, j.description,
				l.adjustments_id as ref, concat(lv.level,' ',sh.yr_level) course_year, l.transaction_date
			from ledger l
				join adjustments j on j.id = l.adjustments_id
				join payers r on r.id = j.payers_id
				join students s on s.idno = r.students_idno
				join academic_terms tr on tr.id = j.academic_terms_id
				join academic_years ay on ay.id = tr.academic_years_id
				join basic_ed_histories sh on sh.students_idno =  r.students_idno and sh.academic_years_id = ay.id
				join levels lv on lv.id = sh.levels_id
			where
				lv.id = {$this->db->escape($levels_id)}
				and j.posted = 'Y' and
				and j.description = substr({$this->db->escape($tran_code)},6)
				and sh.yr_level = {$this->db->escape($year_level)} "
				." and l.{$tran_type} > 0 "
				." and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)		
			) tmp3
		union
			select * from
			(
			select s.idno, cap_first(concat_ws(', ',s.lname, s.fname)) as student, format(v.total_amount_availed,2) as amount, sk.scholarship description,
				v.id as ref, concat(lv.level,' ',sh.yr_level) course_year, l.transaction_date
			from ledger l 
				join privileges_availed v on v.id = l.privileges_availed_id
				join scholarships sk on sk.id = v.scholarships_id  
				join basic_ed_histories sh on sh.id = v.basic_ed_histories_id
				join students s on s.idno = sh.students_idno
			WHERE
				v.levels_id = {$this->db->escape($levels_id)}
				and v.posted = 'Y' and
				and sk.scholarship_code = substr({$this->db->escape($tran_code)},6)  
				and sh.yr_level = {$this->db->escape($year_level)} "
				." and l.{$tran_type} > 0 "
				." and l.transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND)
			) tmp4
		order by student
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		return ($query ? $query : FALSE);
	}
	
  public function get_cash_receipts($start_date,$end_date,$summary){
  /* 
        Function Name:  cash_receipts()
        Version:        Beta 0.01 
      	Code By:        toyet amores
        Date Started:   09-18-2017
        Requested by:   Mam Alma - Accountant
        Use:            Provide data to SAP-Business One to update students subsidiary ledgers.
        Description:    Retrieves all payment transactions from the PAYMENTS table.  Accesses the 
                        cashReceipts(startdate,enddate) stored procedure.  The resulting set is
                        ordered by METHOD, TELLERCODE, RECEIPTNO
        Table(s):     	payments
                  		teller_codes
                  		payers
                  		payment_items
                  		payment_methods
                  		students
                  		other_payers
        Parameters:     $start_date - starting date of the transactions to retrieve
                        $end_date   - ending date of the transactions to retrieve
        Controller(s):  accountant
        Model(s):   	accountant_model
        View(s):    	list_cash_receipts

        Remarks:        First production level assignment.
    */

    //prepare dcr_temp table and fill with records
    $sql="call GetCashReceipts('".$start_date."','".$end_date."');";
    $query = $this->db->query($sql);

    if ($summary=='true'){
	    $sql="select distinct tellercode, description, method, sum(amount) as amount
	          from dcr_temp
	          group by tellercode, method;";
    } else { 	
	    //select desired info from dcr_temp
	    $sql="select distinct date, tellercode, description, receipt, id, name, amount, method, remark 
	          from dcr_temp
	          order by date, tellercode, method, receipt;";
    }
    ////log_message( 'INFO', print_r($sql,true));
    $query = $this->db->query($sql);

    $result = $query->result_array();
    //$result = $query->result();
    return ($result ? $result : FALSE);
  }


 public function get_adjustments($start_date,$end_date,$summary){
  /* 
        Function Name:  get_adjustments()
        Version:        Beta 0.01 
      	Code By:        toyet amores
        Date Started:   10-11-2017
        Requested by:   Mam Alma - Accountant
        Use:            Provide data to SAP-Business One to update students subsidiary ledgers.
        Description:    Retrieves all payment transactions from the PAYMENTS table.  Accesses the 
                        cashReceipts(startdate,enddate) stored procedure.  The resulting set is
                        ordered by METHOD, TELLERCODE, RECEIPTNO
        Table(s):     	payments
                  		teller_codes
                  		payers
                  		payment_items
                  		payment_methods
                  		students
                  		other_payers
        Parameters:     $start_date - starting date of the transactions to retrieve
                        $end_date   - ending date of the transactions to retrieve
        Controller(s):  accountant
        Model(s):   	accountant_model
        View(s):    	list_cash_receipts

        Remarks:        First production level assignment.
    */

    //prepare adj_temp table and fill with records
    $sql="call GetAdjustments('".$start_date."','".$end_date."');";
    $query = $this->db->query($sql);

    if ($summary=='true'){
	    $sql="select distinct tellercode, description, type, sum(amount) as amount, 
	                (CASE levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 6 THEN 'College '
		                WHEN 7 THEN 'Graduate School '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		             END) as levels_id
	          from (
			  select * from adj_temp group by receipt ) temp2
			  GROUP BY tellercode , method , levels_id;";
    } else { 	
	    //select desired info from adj_temp
	    $sql="select distinct date, tellercode, description, type, id, name, (CASE levels_id
		                WHEN 1 THEN 'Kinder '
		                WHEN 2 THEN 'Kinder '
		                WHEN 3 THEN 'Grade '
		                WHEN 4 THEN 'HS Day '
		                WHEN 5 THEN 'HS Night '
		                WHEN 6 THEN 'College '
		                WHEN 7 THEN 'Graduate School '
		                WHEN 11 THEN 'Pre-School '
		                WHEN 12 THEN 'Senior High '
		             END) as levels_id, amount, remark 
	          from adj_temp
	          order by date, tellercode, method;";
    }
    ////log_message( 'INFO', print_r($sql,true));
    $query = $this->db->query($sql);

    $result = $query->result_array();
    //$result = $query->result();
    return ($result ? $result : FALSE);
  }


	/*
	 * @author: Toyet Amores
	 * @date: 10/6/2017
	 * $description: for excel reports of cash receipts
	 * 
	 */
	public function CreateDCRforExcel($start_date, $end_date, $summary, $data) {

			$this->load->library('PHPExcel');
			
			if ($start_date==$end_date){
				$filename = "downloads/Cash_Receipts_".date_format(date_create($start_date),'Y-M-d').".xls";
			} else {
				$filename = "downloads/Cash_Receipts_".date_format(date_create($start_date),'Y-M-d')." to ".
				date_format(date_create($end_date),'Y-M-d').".xls";
			}


			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setTitle("Cash Receipts: ");
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			
			if ($start_date==$end_date){
				$objPHPExcel->getActiveSheet()->SetCellValue('A1', "Cash Receipts for:  ".date_format(date_create($start_date),'F d, Y'));
			} else {
				$objPHPExcel->getActiveSheet()->SetCellValue('A1', "Cash Receipts for:  ".date_format(date_create($start_date),'F d, Y')." to ".date_format(date_create($end_date),'F d, Y'));
			}


			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
			->setSize(14);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(17);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);

			$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'No.');
			$objPHPExcel->getActiveSheet()->SetCellValue('B3', 'TELLERCODE');
			$objPHPExcel->getActiveSheet()->SetCellValue('C3', 'DESCRIPTION');
			$objPHPExcel->getActiveSheet()->SetCellValue('D3', 'METHOD');
			$objPHPExcel->getActiveSheet()->SetCellValue('E3', 'BANK');
			$objPHPExcel->getActiveSheet()->SetCellValue('F3', 'CASH');
			$objPHPExcel->getActiveSheet()->SetCellValue('G3', 'CHECK');
			$objPHPExcel->getActiveSheet()->SetCellValue('H3', 'CREDIT CARD');
			$objPHPExcel->getActiveSheet()->SetCellValue('I3', 'DEBIT CARD');
			$objPHPExcel->getActiveSheet()->SetCellValue('J3', 'AMOUNT');

					$objPHPExcel->getActiveSheet()->getStyle('E3')
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle('F3')
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle('G3')
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle('H3')
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle('I3')
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle('J3')
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			$num=3;

			if ($data) {
				$objPHPExcel->getActiveSheet()->getStyle('A'.$num)->getFont()->setBold(true)->setSize(12);
				$objPHPExcel->getActiveSheet()->freezePane('B4');
					
				$Bank = 0;
				$Cash = 0;
				$Check = 0;
				$CreditCard = 0;
				$DebitCard = 0;
				$TotalAmt = 0;

				$cnt=1;
				$num++;
				foreach($data as $row) {
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, $cnt.'. ');
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$num, $row['tellercode']);

					if (stripos($row['description'],':')>0){
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, substr($row['description'],0,stripos($row['description'],':')));
					} else {
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, $row['description']);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$num, $row['method']);

					Switch ($row['method']) {

						case "Bank":
							$objPHPExcel->getActiveSheet()->SetCellValue('E'.$num, number_format($row['amount'],2));
							$Bank += $row['amount'];
							break;

						case "Cash":
							$objPHPExcel->getActiveSheet()->SetCellValue('F'.$num, number_format($row['amount'],2));
							$Cash += $row['amount'];							
							break;

						case "Check":
							$objPHPExcel->getActiveSheet()->SetCellValue('G'.$num, number_format($row['amount'],2));
							$Check += $row['amount'];
							break;

						case "Credit Card":
							$objPHPExcel->getActiveSheet()->SetCellValue('H'.$num, number_format($row['amount'],2));
							$CreditCard += $row['amount'];
							break;

						case "Debit Card":
							$objPHPExcel->getActiveSheet()->SetCellValue('I'.$num, number_format($row['amount'],2));
							$DebitCard += $row['amount'];
							break;

					}
					$objPHPExcel->getActiveSheet()->SetCellValue('J'.$num, $row['amount']);
					$TotalAmt += $row['amount'];

					$objPHPExcel->getActiveSheet()->getStyle('E'.$num)
												    ->getNumberFormat()
												    ->setFormatCode('#,##0.00');
					$objPHPExcel->getActiveSheet()->getStyle('E'.$num)
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
												    ->getNumberFormat()
												    ->setFormatCode('#,##0.00');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objPHPExcel->getActiveSheet()->getStyle('G'.$num)
												    ->getNumberFormat()
												    ->setFormatCode('#,##0.00');
					$objPHPExcel->getActiveSheet()->getStyle('G'.$num)
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objPHPExcel->getActiveSheet()->getStyle('H'.$num)
												    ->getNumberFormat()
												    ->setFormatCode('#,##0.00');
					$objPHPExcel->getActiveSheet()->getStyle('H'.$num)
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objPHPExcel->getActiveSheet()->getStyle('I'.$num)
												    ->getNumberFormat()
												    ->setFormatCode('#,##0.00');
					$objPHPExcel->getActiveSheet()->getStyle('I'.$num)
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objPHPExcel->getActiveSheet()->getStyle('J'.$num)
												    ->getNumberFormat()
												    ->setFormatCode('#,##0.00');
					$objPHPExcel->getActiveSheet()->getStyle('J'.$num)
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$num++;
					$cnt++;
				}
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$num, 'TOTALS:');
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$num, number_format($Bank,2));
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$num, number_format($Cash,2));
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$num, number_format($Check,2));
				$objPHPExcel->getActiveSheet()->SetCellValue('H'.$num, number_format($CreditCard,2));
				$objPHPExcel->getActiveSheet()->SetCellValue('I'.$num, number_format($DebitCard,2));
				$objPHPExcel->getActiveSheet()->SetCellValue('J'.$num, number_format($TotalAmt,2));

				$objPHPExcel->getActiveSheet()->getStyle('E'.$num)
											    ->getNumberFormat()
											    ->setFormatCode('#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$num)
								                ->getAlignment()
								                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
											    ->getNumberFormat()
											    ->setFormatCode('#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
								                ->getAlignment()
								                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objPHPExcel->getActiveSheet()->getStyle('G'.$num)
											    ->getNumberFormat()
											    ->setFormatCode('#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle('G'.$num)
								                ->getAlignment()
								                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objPHPExcel->getActiveSheet()->getStyle('H'.$num)
											    ->getNumberFormat()
											    ->setFormatCode('#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle('H'.$num)
								                ->getAlignment()
								                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objPHPExcel->getActiveSheet()->getStyle('I'.$num)
											    ->getNumberFormat()
											    ->setFormatCode('#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle('I'.$num)
								                ->getAlignment()
								                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objPHPExcel->getActiveSheet()->getStyle('J'.$num)
											    ->getNumberFormat()
											    ->setFormatCode('#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle('J'.$num)
								                ->getAlignment()
								                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			}

			$objWriter->save($filename,__FILE__);
			return;

		}

	/**
	 * @author: Toyet Amores
	 * @date: 10/7/2017
	 * $description: for excel reports of adjustments
	 * 
	 */
	public function CreateADJforExcel($start_date, $end_date, $summary, $data) {

			$this->load->library('PHPExcel');
			
			if ($start_date==$end_date){
				$filename = "downloads/Adjustments_".date_format(date_create($start_date),'Y-M-d').".xls";
			} else {
				$filename = "downloads/Adjustments_".date_format(date_create($start_date),'Y-M-d')." to ".
				date_format(date_create($end_date),'Y-M-d').".xls";
			}

			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setTitle("Adjustments: ");
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);

			if ($start_date==$end_date){
				$objPHPExcel->getActiveSheet()->SetCellValue('A1', "Adjustments for:  ".date_format(date_create($start_date),'F d, Y'));
			} else {
				$objPHPExcel->getActiveSheet()->SetCellValue('A1', "Adjustments for:  ".date_format(date_create($start_date),'F d, Y')." to ".date_format(date_create($end_date),'F d, Y'));
			}

			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
			->setSize(14);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(17);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

			$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'No.');
			$objPHPExcel->getActiveSheet()->SetCellValue('B3', 'TELLERCODE');
			$objPHPExcel->getActiveSheet()->SetCellValue('C3', 'DESCRIPTION');
			$objPHPExcel->getActiveSheet()->SetCellValue('D3', 'LEVEL');

			$objPHPExcel->getActiveSheet()->SetCellValue('E3', 'CREDIT/DEBIT MEMO');
			$objPHPExcel->getActiveSheet()->getStyle('E3')->getAlignment()->setWrapText(true);

			$objPHPExcel->getActiveSheet()->SetCellValue('F3', 'AMOUNT');
			$objPHPExcel->getActiveSheet()->getStyle('F3')
							                ->getAlignment()
							                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			$num=3;

			if ($data) {
				$objPHPExcel->getActiveSheet()->getStyle('A'.$num)->getFont()->setBold(true)->setSize(12);
				$objPHPExcel->getActiveSheet()->freezePane('B4');
					
				$TotalAmt = 0;

				$cnt=1;
				$num++;
				foreach($data as $row) {
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, $cnt.'. ');
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$num, $row['tellercode']);

					if (stripos($row['description'],':')>0){
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, substr($row['description'],0,stripos($row['description'],':')));
					} else {
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, $row['description']);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$num, $row['levels_id']);
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$num, $row['type']);
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$num, $row['amount']);
					$TotalAmt += $row['amount'];


					$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
												    ->getNumberFormat()
												    ->setFormatCode('#,##0.00');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
									                ->getAlignment()
									                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$num++;
					$cnt++;
				}

				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$num, 'TOTAL:');
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$num, number_format($TotalAmt,2));
				$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
											    ->getNumberFormat()
											    ->setFormatCode('#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
								                ->getAlignment()
								                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$num++;
				$objPHPExcel->getActiveSheet()->getStyle('F'.$num)
								                ->getAlignment()
								                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			}

			$objWriter->save($filename,__FILE__);
			return;
	}



}

