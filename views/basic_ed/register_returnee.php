<?php if ($current_history){
		print("<h3>Student Already Registered</h3>");
 }else {?>

<?php if (isset($message) && $tab=='8') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:5px; ">
 <form method="post" id="promotion">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "register_returnee"/>
	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" > 
	<input type="hidden" name="academic_year" value="<?php print($sy_id); ?>" > 
	<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
	   <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    <strong><font face="Verdana, Arial, Helvetica, sans-serif"> REGISTER STUDENT/PUPIL for SY <?php print($sy);?></font></strong>
		   </div>
		   </td>
 	 </tr>
	 
	</table>	

		  
	<div style="margin-bottom:20px; ">
	  </div>
	  <div style="margin-left:20px; width:90%; margin-bottom:20px; ">
	     
	      <table width="18%" align="left" cellpadding="0" cellspacing="0" style="width:50%;">
			  <tr>
				  
				   <td> 
				    <label class="control-label" for="level"><b> Level:</b></label>
				    <div class="controls">
				    <select id="levels" name="level" class="form">
						<option value="">-- Select Level --</option>
							<?php foreach ($levels as $level): ?>
								<option value="<?php echo $level->id; ?>"><?php echo $level->level; ?></option>
							<?php endforeach; ?>
						</select>
				   	</div>
				   </td>
				    <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
				   	<label class="control-label" for="level"><b> Grade/Year:</b></label>
				   	 <div class="controls" style=text-align:center>
				        <input type="text" name="year" class="year input-xlarge" style="width:20px; text-align:center">
				     </div>   
				   </td>
		 	 </tr>
		 	 <tr>
		 	     <div style="margin-left:25px;">
		 		 <td colspan = "2">
		 		 </div>
		 			 <button class="btn btn-primary" type="submit" style="margin-left:10px;" >Register STUDENT/PUPIL</button>
		 		</div>	 
		 		 </td>
		 		 
		 	 </tr>
		  </table>
 	 </div>
 	 
  
</form>
 </div>
 
 <?php }?>
 <script>
$(document).ready(function(){
	$('#promotion').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			amount: { required: true, minlength: 1 },
			type: {required: true},
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	$(".year").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});
</script>
 
