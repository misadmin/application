<style>
.well-error {color: #b94a48;background-color: #f2dede;border-color: #eed3d7;}
</style>

<!-- 
	<div class="row-fluid">
		<div class="span12">
			<h2 class="heading">Got Error!</h2>
		</div>
	</div>
-->
<div class="row-fluid">
<div class="span11">

<?php if($error=='SCHEDULED_ACADEMIC_TERM_NOT_MATCHING_BUT_ENOUGH_CREDIT'): ?>
	<div class="span9">
		<div class="well">
			<p>You are about to enroll to the <strong><?php echo $scheduled_academic_term->term; ?> Term</strong> for school year: <strong><?php echo ($scheduled_academic_term->end_year - 1) . " - " . $scheduled_academic_term->end_year; ?></strong>. Do you want to proceed? If you wish to continue click the accept button.</p>
			<form method="post">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="action" value="insert_scheduled_academic_term" >
				<button type="submit" class="btn btn-primary">Accept</button>
			</form>
		</div>
	</div>
<?php else: ?>
	<div class="span9">
		<div class="well">
			<p><?php echo $error_message; ?></p>
		</div>
	</div>
<?php endif; ?>

</div>
</div>