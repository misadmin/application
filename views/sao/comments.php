<style>
.switch {
  position: relative;
  display: inline-block;
  width: 35px;
  height: 20px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 15px;
  width: 15px;
  left: 2px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(15px);
  -ms-transform: translateX(15px);
  transform: translateX(15px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


<input type="hidden" id="input_student_idno" name="input_student_idno" value="<?php echo $student_idno ?>">
<input type="hidden" id="input_history_id" name="input_history_id" value="<?php echo $history_id ?>">

<div class="container" style="width:90%">

	<textarea name="commentbox" id="commentbox" style="width:90%; height:300px;" readonly onchange="scrollComment()"></textarea>

	<button type="button" class="btn-small" id="comment_update_btn" name="comment_update_btn">Update Comment(s)</button>

	<button type="button" class="btn-small" id="this_sem_only_btn" name="this_sem_only_btn">Get This Sem Comment(s) / Edit</button>

	<button type="button" class="btn-small" id="all_comments_btn" name="all_comments_btn">Get All Time Comment(s)</button>

	&nbsp&nbsp<b>Student is blocked from enrollment</b>&nbsp&nbsp
	<label class="switch">
  		<input type="checkbox" id="blocked_checkbox" name="blocked_checkbox">
  		<span class="slider round"></span>
	</label>
</div>

<script>
	$(document).ready(function(){
	var student_idno = $('#input_student_idno').val();
	var history_id = $('#input_history_id').val();
	var comments = '';

	//console.log(student_idno);
	//console.log(history_id);

	$.ajax({
		'url': '<?php echo site_url("ajax/fetch_comments"); ?>',
		'type': 'post', // performing a POST request
		'data': { student_idno: student_idno,
		          history_id: history_id },
		'dataType':'json',
		'success':function(data){
			console.log(data);
			if(data != null){  // prevent firing up the error
				for(var i=0; i < data.length; i++) {
				  var item = data[i];
				  comments = comments + item.term + item.year + '\n';
				  comments = comments + item.comments
				  if(i<data.length){
				   	comments = comments + '\n';
				  }
				}
				$( "#commentbox" ).val( comments );
				$('#commentbox').scrollTop($('#commentbox')[0].scrollHeight);
			}
		},
		error: function (request, error) {
        	console.log(arguments);
        	console.log(error);
        	alert(" Can't do because: " + error);
    	}
		});

		// check blocked_status and update display
		$.ajax({
			'url': '<?php echo site_url("ajax/get_blocked_status"); ?>',
			'type': 'post', // performing a POST request
			'data': { student_idno: student_idno,
			          history_id: history_id },
			'dataType':'json',
			'success':function(data){
				console.log(data[0].blocked);
				// this is kind of confusing...
				// the initial value of blocked_status depends on the contents in the table.
				// that is, 'Y' or 'N'.  but, the switch element uses 'on' or 'off' values.
				// so, we need to initialize values here... Toyet 3.2.2019				
				if(data[0].blocked=="Y"){
					blocked_status = 'on';
					$("#blocked_checkbox").val(data[0].blocked);
 					$('#blocked_checkbox').prop('checked', true);
				} else {
					blocked_status = 'off';
 					$('#blocked_checkbox').prop('checked', false);
				}
				$("#blocked_checkbox").val(blocked_status);

				console.log(blocked_status);
			},
			error: function (request, error) {
	        	console.log(arguments);
	        	console.log(error);
	        	//alert(" Can't do because: " + error);
	    	}
		});

	})

	$('#comment_update_btn').click(function(){
		var student_idno = $('#input_student_idno').val();
		var history_id = $('#input_history_id').val();
		var arrayOfLines = '';
		var x = document.getElementById("commentbox").readOnly;
		var comments = '';
		var thisSemExists = false;

		if(x){
			alert("Need to click  [Get This Sem Comment(s)]  button to edit comments!");
		} else {
			//needs to check if comment for the Sem exists.
			$.ajax({
				'url': '<?php echo site_url("ajax/check_if_exist_comments"); ?>',
				'type': 'post', // performing a POST request
				'data': { student_idno: student_idno,
				          history_id: history_id },
				'dataType':'json',
				'success':function(data){
					//console.log(data);
					//console.log(data.itExists);
					thisSemExists = data.itExists;
					// if comments for the Sem do exists, just update
					if(thisSemExists){
						arrayOfLines = $('#commentbox').val().split('\n');
						comments = '';
						for(var i=1; i < arrayOfLines.length; i++) {
						  if(arrayOfLines[i] != '')
						  	comments = comments + arrayOfLines[i];
						  if(i<arrayOfLines.length){
						   	comments = comments + '\n';
						  }
						}
						//comments = comments +'\n\n';
						$.ajax({
							'url': '<?php echo site_url("ajax/update_comments"); ?>',
							'type': 'post', // performing a POST request
							'data': { student_idno: student_idno,
							          history_id: history_id,
							          comments: comments },
							'dataType':'json',
							'success':function(data){
								console.log('OG NILAMPOS RA JUD...');
								console.log(data);
							},
							error: function (request, error) {
					        	console.log(arguments);
					        	console.log(error);
					    	}
						});
					} else {
						// if NOT comment for the Sem does NOT exist, insert record
						arrayOfLines = $('#commentbox').val().split('\n');
						console.log(arrayOfLines);
						comments = '';
						for(var i=0; i < arrayOfLines.length; i++) {
							if(arrayOfLines[i] != '')
						  		comments = comments + arrayOfLines[i];
						  	if(i<arrayOfLines.length){
						   		comments = comments + '\n';
						  }
						}
						$.ajax({
							'url': '<?php echo site_url("ajax/add_comments"); ?>',
							'type': 'post', // performing a POST request
							'data': { student_idno: student_idno,
							          history_id: history_id,
							          comments: comments },
							'dataType':'json',
							'success':function(data){
								//do nothing...
							},
							error: function (request, error) {
					        	console.log(arguments);
					        	console.log(error);
					    	}
						});
					}

				},
				error: function (request, error) {
		        	console.log(arguments);
		        	console.log(error);
		        	alert(" Can't do because: " + error);
		    	}
				});

		}
		document.getElementById("commentbox").readOnly = true;
	})

	$("#commentbox").click(function(){
		var x = document.getElementById("commentbox").readOnly;
		if(x){
			alert("Need to click  [Get This Sem Comment(s)]  button to edit comments!");
		}
	})

	$("#this_sem_only_btn").click(function(){
		var student_idno = $('#input_student_idno').val();
		var history_id = $('#input_history_id').val();
		var comments = '';

		$.ajax({
			'url': '<?php echo site_url("ajax/fetch_comments"); ?>',
			'type': 'post', // performing a POST request
			'data': { student_idno: student_idno,
			          history_id: history_id,
			          this_sem_only: true },
			'dataType':'json',
			'success':function(data){
				comments = '';
				if(!data){
				} else {
					for(var i=0; i < data.length; i++) {
						var item = data[i];
						comments = comments + item.term + item.year + '\n'
						comments = comments + item.comments
						comments = comments +'\n';
					}
				}
				$( "#commentbox" ).val( comments );
			},
			error: function (request, error) {
	        	console.log(arguments);
	        	console.log(error);
	        	alert(" Can't do because: " + error);
	    	}
		});
		document.getElementById("commentbox").readOnly = false;
	})


	$("#all_comments_btn").click(function(){
		var student_idno = $('#input_student_idno').val();
		var history_id = $('#input_history_id').val();
		var comments = '';

		$.ajax({
			'url': '<?php echo site_url("ajax/fetch_comments"); ?>',
			'type': 'post', // performing a POST request
			'data': { student_idno: student_idno,
			          history_id: history_id },
			'dataType':'json',
			'success':function(data){
				console.log(data);
				comments = '';
				if(!data){					
				} else {
					for(var i=0; i < data.length; i++){
						var item = data[i];
						comments = comments + item.term + item.year + '\n'
						comments = comments + item.comments
						comments = comments +'\n';
					}
					$( "#commentbox" ).val( comments );
					$('#commentbox').scrollTop($('#commentbox')[0].scrollHeight);
				}
			},
			error: function (request, error) {
	        	console.log(arguments);
	        	console.log(error);
	        	alert(" Can't do because: " + error);
	    	}
		});

		document.getElementById("commentbox").readOnly = true;
	})

	function scrollComment(){
		$('#commentbox').scrollTop($('#commentbox')[0].scrollHeight);
	};


	$("#blocked_checkbox").click( function(){
		var student_idno = $('#input_student_idno').val();
		var history_id = $('#input_history_id').val();
		var blocked_status = $("#blocked_checkbox").val();
		var new_blocked_status = "";

		console.log(blocked_status);
		if(blocked_status=='on'){
			new_blocked_status = 'off';
		} else {
			new_blocked_status = 'on';
		}
		console.log('to -> '+new_blocked_status);

		$.ajax({
			'url': '<?php echo site_url("ajax/block_unblockEnrollment"); ?>',
			'type': 'post', // performing a POST request
			'data': { student_idno: student_idno,
			          history_id: history_id,
			          blocked_status: new_blocked_status },
			'dataType':'json',
			'success':function(data){
				$("#blocked_checkbox").val(new_blocked_status);
				console.log('just set to: '+new_blocked_status);
			},
			error: function (request, error) {
	        	console.log(arguments);
	        	console.log(error);
	        	//alert(" Can't do because: " + error);
	    	}
		});
	})

</script>