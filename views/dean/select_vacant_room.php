
<h4 class="heading">Select Vacant Rooms</h4>
<div style="width:100%;" >
 
	<form action="
<?php 
		switch ($this->uri->segment(2)) {
			case 'offer_course_schedules':
				print(site_url('dean/offer_course_schedules'));
				$step = 4;
				break;
			case 'offerings':
				print(site_url('dean/offerings'));
				$step = 7;
				break;
		}
?>
		" method="post" class="form-horizontal">
<div style="float:left; width:20%;">		
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="step" value="<?php echo $step; ?>" />
		<input type="hidden" name="days_day_code" value="<?php print($days_day_code); ?>" />
		<input type="hidden" name="courses_id" value="<?php print($courses_id); ?>" />
		<input type="hidden" name="max_students" value="<?php print($max_students); ?>" />
		<input type="hidden" name="additional_charge" value="<?php print($additional_charge); ?>" />
		<?php 
			if (isset($rooms_id)) {
				print("<input type=\"hidden\" name=\"rooms_id\" value=".$rooms_id." />");
				print("<input type=\"hidden\" name=\"course_offerings_slots_id\" value=".$course_offerings_slots_id." />");
				print("<input type=\"hidden\" name=\"old_days_day_code\" value=".$old_days_day_code." />");
				print("<input type=\"hidden\" name=\"old_start_time\" value=".$old_start_time." />");
			}
		?>		
				<h4>Class Schedule</h4>			
				<table class="table table-striped table-bordered table-condensed" id="MediaTable-0">
					   <tr>
					    <td style="text-align: right" width="25%" ><strong>Term</strong></td>
					    <td width="75%"><?php print($academic_terms->term." ".$academic_terms->sy); ?></td>
					  </tr>
					  <tr>
					    <td style="text-align: right"><strong>Course</strong></td>
					    <td><?php echo $course->course_code; ?></td>
					  </tr>
					  <tr>
					    <td style="text-align: right"><strong>Description</strong></td>
					    <td><?php echo $course->descriptive_title; ?></td>
					  </tr>
					  <tr>
					    <td style="text-align: right"><strong>Units</strong></td>
					    <td><?php echo $course->credit_units; ?></td>
					  </tr>					  
					  <tr>
					    <td style="text-align: right"><strong>Time</strong></td>
					    <td><?php print($schedule); ?></td>
					  </tr>
					  <tr>
					    <td style="text-align: right"><strong>Capacity</strong></td>
					    <td><?php print($max_students); ?></td>
					  </tr>
				</table>
</div>		  
		<div style="text-align:left; float:left; margin-left:15px; width:48%;"> 		
			  <div class="formSep">
				<h4>Select Rooms from the Following Buildings</h4>
			       <div class="tabbable">
			          <ul class="nav nav-tabs">
			            <!-- <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==1) { 
			            				print("class=\"active\""); 
			            		  } elseif (!isset($old_bldgs_id)) { 
			            		  		print("class=\"active\""); 
								  } ?> ><a href="#tab1" data-toggle="tab">Lesage</a></li> -->
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==3) { 
			            				print("class=\"active\""); 
			            		  } elseif (!isset($old_bldgs_id)) { 
			            		  		print("class=\"active\""); 
								  } ?> ><a href="#tab2" data-toggle="tab">Bates</a></li> 
			            <!-- <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==3) { print("class=\"active\""); } ?> ><a href="#tab2" data-toggle="tab">Bates</a></li> -->
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==7) { print("class=\"active\""); } ?> ><a href="#tab3" data-toggle="tab">Freinademetz</a></li>
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==9) { print("class=\"active\""); } ?> ><a href="#tab4" data-toggle="tab">Scanlon</a></li>
			            <li <?php if (isset($old_bldgs_id) AND $old_bldgs_id==11) { print("class=\"active\""); } ?> ><a href="#tab5" data-toggle="tab">Other</a></li>
			          </ul>
			       
			       <div class="tab-content">
			       	   <!-- <div class="tab-pane <?php 
			       	    						if (isset($old_bldgs_id) AND $old_bldgs_id==1) { 
													print("active"); 
												} elseif (!isset($old_bldgs_id)) { 
													print("active"); 
												} ?>" id="tab1">
			   		     	<table class="table">
				           		<tr>
								<?php
						         	$checked_1st_rm = "checked";
									$cnt=1;
									if ($main_building) {
										foreach($main_building as $main) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND $rooms_id == $main->id) {
													print("<td>
														<input type=\"radio\" name=\"room_id\" value=".$main->id." checked />"
														.$main->room_no."</td>");
												} else {
													print("<td>
														<input type=\"radio\" name=\"room_id\" value=".$main->id." ".$checked_1st_rm." />"
														.$main->room_no."</td>");
												}
												$cnt++;
												$checked_1st_rm = "";
											} else {
												if (isset($rooms_id) AND $rooms_id == $main->id) {
													print("</tr><tr><td>
														<input type=\"radio\" name=\"room_id\" value=".$main->id." checked />"
														.$main->room_no."</td>");
												} else {
													print("</tr><tr><td>
														<input type=\"radio\" name=\"room_id\" value=".$main->id." />"
														.$main->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>
			         	</div> --> 
			                
			       	    <div class="tab-pane <?php 
			       	    						if (isset($old_bldgs_id) AND $old_bldgs_id==3) { 
													print("active"); 
												} elseif (!isset($old_bldgs_id)) { 
													print("active"); 
												} ?>" id="tab2">
			     		<!-- <div class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==3) { print("active"); } ?>" id="tab2"> -->
			           		<table class="table">
				           		<tr>
								<?php
						         	$checked_1st_rm = "checked";
						         	$cnt=1;
						         	if ($bates_building) {
										foreach($bates_building as $bates) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $bates->id)) {
													print("<td>
														<input type=\"radio\" name=\"room_id\" value=".$bates->id." checked />"
														.$bates->room_no."</td>");
												} else {
													print("<td>
														<input type=\"radio\" name=\"room_id\" value=".$bates->id." ".$checked_1st_rm." />"
														.$bates->room_no."</td>");
												}
												$cnt++;
												$checked_1st_rm = "";
											} else {
												if (isset($rooms_id) AND ($rooms_id == $bates->id)) {
													print("</tr><tr><td>
														<input type=\"radio\" name=\"room_id\" value=".$bates->id." checked />"
														.$bates->room_no."</td>");
												} else {
													print("</tr><tr><td>
														<input type=\"radio\" name=\"room_id\" value=".$bates->id." />"
														.$bates->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>
			     		</div>
			       		
			           	<div class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==7) { print("active"); } ?>" id="tab3">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	if ($freina_building) {
										foreach($freina_building as $freina) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $freina->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$freina->id." checked />"
													.$freina->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$freina->id." ".$checked_1st_rm." />"
													.$freina->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($rooms_id) AND ($rooms_id == $freina->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$freina->id." checked />"
													.$freina->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$freina->id." />"
													.$freina->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}	
								?>
				           		</tr>	
				         	</table>
			    		</div>
			            
			      		<div class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==9) { print("active"); } ?>" id="tab4">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	if ($scanlon_building) {
										foreach($scanlon_building as $scan) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $scan->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$scan->id." checked />"
													.$scan->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$scan->id." ".$checked_1st_rm." />"
													.$scan->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($rooms_id) AND ($rooms_id == $scan->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$scan->id." checked />"
													.$scan->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$scan->id." />"
													.$scan->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>

			        	</div>

			      		<div class="tab-pane <?php if (isset($old_bldgs_id) AND $old_bldgs_id==11) { print("active"); } ?>" id="tab5">
				        	<table class="table">
				           		<tr>
								<?php
						         	$cnt=1;
						         	//print_r($old_bldgs_id); die();
						         	if ($other_building) {
										foreach($other_building as $other) {
											if ($cnt < 5) {
												if (isset($rooms_id) AND ($rooms_id == $other->id)) {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$other->id." checked />"
													.$other->room_no."</td>");
												} else {
													print("<td>
													<input type=\"radio\" name=\"room_id\" value=".$other->id." ".$checked_1st_rm." />"
													.$other->room_no."</td>");
												}
												$cnt++;
											} else {
												if (isset($rooms_id) AND ($rooms_id == $other->id)) {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$other->id." checked />"
													.$other->room_no."</td>");
												} else {
													print("</tr><tr><td>
													<input type=\"radio\" name=\"room_id\" value=".$other->id." />"
													.$other->room_no."</td>");
												}
												$cnt=2;
											}
										}
									}
								?>
				           		</tr>	
				         	</table>

			        	</div>
			        	
			        	</div>
			    	</div> <!-- /tabbable -->
				</div>
				<div class="control-group" >
					<div class="controls" style="text-align:left;">
						<button class="btn btn-success" type="submit">Create Schedule!</button>
					</div>
				</div>			  
			</div>	  
	</form>
</div>
