<?php 
	$max_year = date('Y');
	$min_year = $max_year - 70; 
?>
<div class="row-fluid">
	<div class="span12">
		<form id="student_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_information" />
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<fieldset>
				<div class="control-group formSep">
					<label for="firstname" class="control-label">First Name</label>
					<div class="controls">
						<input type="text" id="firstname" name="firstname" class="input-xlarge" value="<?php echo $firstname; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="familyname" class="control-label">Family Name</label>
					<div class="controls">
						<input type="text" id="familyname" name="familyname" class="input-xlarge" value="<?php echo $familyname; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="middlename" class="control-label">Middle Name</label>
					<div class="controls">
						<input type="text" id="middlename" name="middlename" class="input-xlarge" value="<?php echo $middlename; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="middlename" class="control-label">Date of Birth</label>
					<div class="controls">
						<select name="birth_year" id="birth_year">
<?php for ($year = $max_year; $year<= $min_year; $year++): ?>
							<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
<?php endfor; ?>						
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button id="generate_password" class="btn" type="button">Generate Password</button>
						<button id="update_password" class="btn btn-primary" disabled="disabled" type="submit">Update Password</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#birthdate').datepicker();
	});
</script>