<h2 class="heading">Daily Receipt Summary</h2>
<div class="row-fluid">
	<div class="span8">
		<form action="" class="form-horizontal" id="rsi" method="post">
			<input type="hidden" name="nonce" value="<?php echo $this->common->nonce() ?>" />
			<input type="hidden" name="submitted" value="1" />
				<div class="input-append">
					<div class="date" id="rsi-date" data-date-format="yyyy-mm-dd" data-date="<?php echo $date; ?>">
					     <input type="text" id="rsi-date-input" name="date" placeholder="YYYY-MM-DD" value="<?php echo $date; ?>" readonly="">
						 <span class="add-on"><i class="icon-calendar"></i></span>
						 <button type="submit" class="btn btn-success">Show</button>
					</div>
				</div>
		</form>
	</div>
	<div class="span4" style="margin-left:20px"><a href="<?php echo site_url("{$this->session->userdata('role')}/report/daily_report/{$date}"); ?>" target="_blank" class="pull-right btn btn-success">Download PDF</a> <a href="#" id="print_summary" style="margin-right:10px;" class="pull-right btn btn-primary">Print Daily Report</a></div>
</div>
<script>
$(document).ready(function() {
	$('#rsi-date').datepicker();
});
</script>