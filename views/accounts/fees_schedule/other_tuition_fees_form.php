
<div class="row-fluid" style="margin-top:20px;">
<h2 class="heading">Other Tuition Fees</h2>
	<div class="span12">
		<div class="form-horizontal">
			<form method="POST" id="other_tuition_form" name="MyForm">
			<input type="hidden" name="step" value="2">
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<select name="academic_year_id" id="academic_year_id">
						<?php
							foreach($school_years AS $school_year) {
						?>
							<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->school_year . " (". $school_year->status.")"); ?> </option>	
						<?php 
							}
						?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Semester Term</div>
				<div class="controls">
					<?php 
						foreach($terms AS $term) {
					?>
						<input type="checkbox" value="<?php print($term->id); ?>" name="semester[]" checked> <?php print($term->term); ?><br>
					<?php 	
						}
					?>															
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Course Code</div>
				<div class="controls">
					<select name="courses_id" id="courses_id">
						<?php						
							foreach($courses AS $course) {
								echo "<option value=". $course->id . ">" . $course->course_code ."</option>";	
							}
						?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Year Level</div>
				<div class="controls">
					<input type="checkbox" value="Y" name="all_yr" id="selectall_yr" checked> ALL Year Levels<br>										
					<input type="checkbox" value="1" name="yr_level[]" class="selectedId_yr" checked> 1st Year<br>										
					<input type="checkbox" value="2" name="yr_level[]" class="selectedId_yr" checked> 2nd Year<br>										
					<input type="checkbox" value="3" name="yr_level[]" class="selectedId_yr" checked> 3rd Year<br>										
					<input type="checkbox" value="4" name="yr_level[]" class="selectedId_yr" checked> 4th Year<br>										
					<input type="checkbox" value="5" name="yr_level[]" class="selectedId_yr" checked> 5th Year<br>										
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Amount</div>
				<div class="controls">
					<input type="text" name="rate">									
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<input type="submit" value="Proceed to Review Form >>" class="btn btn-primary">									
				</div>
				
			</div>
			</form>
			</div>				
	</div>
</div>


<script type="text/javascript">

	function validateForm()
	{
		var x=document.forms["MyForm"]["rate"].value;
		
		
		if (x==null || x=="")  {
		  alert("Please input rate!");
		  document.forms["MyForm"]["rate"].focus();
		  return false;
		}
	}
	
</script>


<script>
	$(document).ready(function () {
	    $('#selectall_yr').click(function () {
	        $('.selectedId_yr').prop('checked', this.checked);
	    });
	
	    $('.selectedId_yr').change(function () {
	        var check = ($('.selectedId_yr').filter(":checked").length == $('.selectedId_yr').length);
	        $('#selectall_yr').prop("checked", check);
	    });
	});
</script>


<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 1,
			}).appendTo('#other_tuition_form');
			$('#other_tuition_form').submit();
		});
	});
</script>

