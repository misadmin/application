<?php 
if(!isset($print_count_final))
	$print_count_final = 1;

$header = array(
			array('id'=>'date', 'name'=>'Date', 'width'=>12, 'align'=>'center'),
			array('id'=>'transaction', 'name'=>'Transaction', 'width'=>30, 'align'=>'left'),
			array('id'=>'reference', 'name'=>'Reference', 'width'=>17, 'align'=>'right'),
			array('id'=>'debit', 'name'=>'DR/Charges', 'width'=>15, 'align'=>'right'),
			array('id'=>'credit', 'name'=>'CR/Payments', 'width'=>15, 'align'=>'right'),
			array('id'=>'balance', 'name'=>'Balance', 'width'=>15, 'align'=>'right'),
			array('id'=>'clearance', 'name'=>'Clearance', 'width'=>26, 'align'=>'left'),
		  );

$idnumber = isset($idnumber) ? $idnumber : "";
$payment_period = isset($payment_period) ? $payment_period : "";
$lab_courses = isset($lab_courses) ? $lab_courses : array();

$ledger['data'] = array();
if(isset($ledger_data) && is_array($ledger_data) && count($ledger_data) > 0){
	$running_balance = 0;
	foreach($ledger_data as $key=>$ledge){
		$running_balance += $ledge->debit - $ledge->credit;
			$ledger['data'][] = array(
					'date'=>$ledge->transaction_date,
					'transaction'=>$ledge->transaction_detail,
					'reference'=>isset($ledge->reference_number) && !empty($ledge->reference_number) ? "Ref: ".$ledge->reference_number : '',
					'debit'=>$ledge->debit,
					'credit'=>$ledge->credit,
					'balance'=>$running_balance,
			);
	}
}

if($is_SHS){
	$offices_clearance = array(
			'Lib',
			'Bkstore',
			"SSG",
			"SAO",
			"Asst Principal",
			"GH/WS/Acad Scholar",); // added ra for working scholar: TODO: filter only for actual working
} else {
	$offices_clearance = array(
			'Lib',
			'Bkstore',
			"CSG",
			"SAO",
			"Dean",
			"GH/WS/Acad Scholar",); // added ra for working scholar: TODO: filter only for actual working
}

$offices_clearance = array_merge($offices_clearance, $lab_courses);
$clearance_lines = array();

foreach ($offices_clearance as $office){

	if(is_array($office)){
		$clearance_lines[] = str_pad($office['name'].":", $header[6]['width'], " ", STR_PAD_RIGHT);
	} else {
		$clearance_lines[] = str_pad($office.":", $header[6]['width'], " ", STR_PAD_RIGHT);
	}
	$clearance_lines[] = str_pad("", $header[6]['width'], "_", STR_PAD_RIGHT);
}
$offices_clearance = $clearance_lines;
$lab_courses = $lab_courses;

$page_width = 136;
$page_height = 33;

?>

<?php 
	$line_now = 1;
    $content = $this->print_lib->init(); 

	if($this->session->userdata('printed') == 'yes')
		$content .= $this->print_lib->set_page_length_in_lines(33); else {
		$content .= $this->print_lib->set_page_length_in_lines(34);
		$this->session->set_userdata('printed', 'yes');
	}

	$content .= $this->print_lib->init().$this->print_lib->regular().$this->print_lib->no_condensed();
	$content .= $this->print_lib->align_right($idnumber)."\n";
	$content .= $this->print_lib->align_right($name)."\n";
	$content .= $this->print_lib->align_right($course_year." (".$section_name.")")."\n";
	$content .= $this->print_lib->bold().$this->print_lib->align_right('STUDENT CLEARANCE').$this->print_lib->regular().$this->print_lib->condensed()."\n";
	$line_now += 4;

	$header_double_lines = '';
	$this_header = $header;
	$running_balance = 0;
	$this_ledger_data = $ledger_data;
	$ledger_data = array();
	//prepare ledger data...
	foreach($this_ledger_data as $key=>$ledge){
		$running_balance += $ledge->debit - $ledge->credit;//debit balance...
		$ledger_data[] = array(
				'type'=>'ledger_data',
				'date'=>$ledge->transaction_date,
				'transaction'=>$ledge->transaction_detail,
				'reference'=>isset($ledge->reference_number) && !empty($ledge->reference_number) ? (is_numeric($ledge->reference_number) ? 'OR#' . $ledge->reference_number: $ledge->reference_number): '',
				'debit'=>number_format((float)$ledge->debit, 2),
				'credit'=>number_format((float)$ledge->credit, 2),
				'balance'=>($running_balance < 0 ? "(" . number_format(abs($running_balance), 2) . ")" : number_format($running_balance, 2)),
		);
	}
	$ledger_data[] = array(
			'type'=>'horiz_line',
			);
	$ledger_data[] = array(
			'type'=>'final',
			'data'=>number_format(($running_balance > 0 ? $running_balance : 0), 2),
			);
	$ledger_data[] = array(
			'type' => 'none',
			'value'=> '',
	);
	$ledger_data[] = array(
			'type' => 'none',
			'value'=> '',
			);
	$ledger_data[] = array(
			'type' => 'none',
			'value'=> '_______________________',
			);
	$ledger_data[] = array(
			'type' => 'none',
			'value'=> '    Accounts Clerk    ',
	);
	if(count($ledger_data) > count($offices_clearance))
		$max_count = count($ledger_data); else
		$max_count = count($offices_clearance);

	$header = '';		
	foreach ($this_header as $head){
		$head = (object)$head;
		$pad_with = $head->id=='balance' ? "|":" ";
		$header_double_lines .= str_pad('', $head->width, '=', STR_PAD_BOTH).$pad_with;
		$header .= str_pad($head->name, $head->width, ' ', STR_PAD_BOTH). $pad_with;
	}
	$all_header = "{$header_double_lines}\n{$header}\n{$header_double_lines}\n";
	$content .= $all_header;
	$line_now += 3;

	for ($count=0; $count<$max_count; $count++){
		if (isset($ledger_data[$count]['type']) && $ledger_data[$count]['type']=='ledger_data'){
			foreach ($this_header as $head){
				$head = (object)$head;
				switch ($head->align){
					case 'center'	: $pad=STR_PAD_BOTH; break;
					case 'right'	: $pad=STR_PAD_LEFT; break;
					default			: $pad=STR_PAD_RIGHT; break;
				}
				$pad_with = $head->id=='balance' ? "|" : " ";
				if($head->id!='clearance'){
					$content .= (isset($ledger_data[$count][$head->id]) ? str_pad(substr($ledger_data[$count][$head->id], 0, $head->width), $head->width, " ", $pad) . $pad_with :
							str_pad("", $head->width+1, " ", STR_PAD_BOTH));
				} else {
					$content .= isset($offices_clearance[$count]) ? $offices_clearance[$count] : "";
				}
			}
			$content .= "\n";
			$line_now += 1;

		} elseif (isset($ledger_data[$count]['type']) && $ledger_data[$count]['type']=='horiz_line'){
			foreach ($this_header as $head){
				$head = (object)$head;
				switch ($head->align){
					case 'center'	: $pad=STR_PAD_BOTH; break;
					case 'right'	: $pad=STR_PAD_LEFT; break;
					default			: $pad=STR_PAD_RIGHT; break;
				}
				$pad_with = $head->id=='balance' ? "|" : "-";
				if($head->id!='clearance'){
					$content .= str_pad("", ($head->width), "-").$pad_with;
				} else {
					$content .= isset($offices_clearance[$count]) ? $offices_clearance[$count] : "";
				}
			}
			$content .= "\n";
			$line_now += 1;

		} elseif(isset($ledger_data[$count]['type']) && $ledger_data[$count]['type']=='final'){
			$total_pad = 0;
			foreach($this_header as $head){
				if (!in_array($head['id'], array('clearance'))){
					$total_pad += $head['width']+1; 
				}
			}
			$content .= $this->print_lib->bold().str_pad("Please pay this amount for FINALS:  P " . $ledger_data[$count]['data'], $total_pad-1, " ", STR_PAD_LEFT). $this->print_lib->regular()."|";
			$content .= isset($offices_clearance[$count]) ? $offices_clearance[$count] : "";
			$content .= "\n";
			$line_now += 1;

		} elseif(isset($ledger_data[$count]['type']) && $ledger_data[$count]['type']=='none'){
			$total_pad = 0;
			foreach($this_header as $head){
				if (!in_array($head['id'], array('clearance'))){
					$total_pad += $head['width']+1; 
				}
			}
			$content .= str_pad($ledger_data[$count]['value'], $total_pad-1, " ", STR_PAD_LEFT)."|";
			$content .= isset($offices_clearance[$count]) ? $offices_clearance[$count] : "";
			$content .= "\n";
			$line_now += 1;

		} else { 
			$total_pad = 0;
			foreach($this_header as $head){
				if (!in_array($head['id'], array('clearance'))){
					$total_pad += $head['width']+1;
				}
			}
			$content .= str_pad("", $total_pad-1, " ", STR_PAD_LEFT)."|";
			$content .= isset($offices_clearance[$count]) ? $offices_clearance[$count] : "";
			$content .= "\n"; 
			$line_now += 1;
		}
	}

        // $line_row limit was 28
	if($line_now<28){
		while($line_now<28){
			$content .= "\n";
			$line_now +=1;
		}
	}	

	$content .= "Prints:[<span id='final_counter'>".$print_count_final."</span>]";
	$content .= $this->print_lib->line_feed();
	$content .= "Student Clearance. Printed by HNUMIS. " . date('l, F j, Y h:i:s A');
	$content .= "\n\n\n\n";

	echo $content;
?>
