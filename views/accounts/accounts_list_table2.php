<?php 

$this->table_lib->set_attributes(array('class'=>'table','id'=>'accounts_list'));
$head_row = array(
		array("id"=>"name",
				"value"=>"Accounts Name",
				"attributes" => array('id'=>'accounts_id_col','style'=>'width:70%')
		),
		array("id"=>"type",
				"value"=>"Type",
				"attributes" => array('id'=>'accounts_name_col')
		)
);

$this->table_lib->insert_head_row($head_row);


// sample

$data = array(
			array('name'=>'ABCD',
				  'type'=>'In House',
				  'flag'=>'2'
				),
		array('name'=>'ABCD',
				'type'=>'In House',
				  'flag'=>'1'
		),
		array('name'=>'ABCD',
				'type'=>'In House',
				  'flag'=>'1',
		),
		array('name'=>'ABCD',
				'type'=>'In House',
				  'flag'=>'1'
		),
		array('name'=>'ABCD',
				'type'=>'In House',
				  'flag'=>'1'
		),
		);

$this->table_lib->insert_data($data);

?>

<!-- h2 class="heading">List of Accounts</h2 -->
<div class="row-fluid">
<div class="span6">
<?php echo $this->table_lib->content() ?>
</div>
<div class="span6">
<?php echo $this->table_lib->content() ?>
</div>
</div>