<?php

	class section_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		
		/*
		 * @added: Feb 9, 2019
		 * @author: genes
		 */
		public function section_management() {
		
			$this->CI->load->model('hnumis/academicyears_model');
			$this->CI->load->model('hnumis/bed/section_model');
			$this->CI->load->model('hnumis/bed/year_level_model');
			
			switch ($this->CI->session->userdata('role')) {
				
				case 'principal':
						$data['various'] = array('can_edit'=>TRUE,
												'page_width1'=>'99%',
												'page_width2'=>'99%',
												'page_align'=>'margin:0 auto;', 
												);
												
						break;
					
				default:
						$data['various'] = array('can_edit'=>FALSE,
												'page_width1'=>'70%',
												'page_width2'=>'71%',
												'page_align'=>'', 
												);

						break;
			}
			
			
			$data['years']   	= $this->CI->academicyears_model->ListAcadYears();
			$data['levels']		= $this->CI->year_level_model->ListLevels();

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_sections');
			
			switch ($action) {
				case 'list_sections':
		
					$academic_year      	= $this->CI->academicyears_model->current_academic_year();
					$data['acad_year_id']	= $academic_year->id;
					
					$data['sections']    	= $this->CI->section_model->ListSections($data['levels'][0]->id, $academic_year->id);

					break;
					
				case 'change_select':
					
					$data2['sections']    = $this->CI->section_model->ListSections($this->CI->input->post('acad_program_groups_id'),$this->CI->input->post('academic_years_id'));

					if ($data['various']['can_edit']) {
						$my_return['output'] = $this->CI->load->view('bed/sections/list_all_sections', $data2, TRUE);
					} else {
						$my_return['output'] = $this->CI->load->view('bed/sections/list_all_sections_for_reports', $data2, TRUE);						
					}
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'extract_year_levels':
				
					$data2['year_levels'] 	= $this->CI->year_level_model->ListYearLevels($this->CI->input->post('acad_program_groups_id'));
					
					$my_return['output'] 	= $this->CI->load->view('bed/pulldown_menu/select_year_levels', $data2, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'add_section':

					$data['section_name']         	= $this->CI->input->post('section_name');
					$data['academic_programs_id'] 	= $this->CI->input->post('academic_programs_id');
					$data['academic_years_id']    	= $this->CI->input->post('academic_years_id');
					$data['yr_level'] 				= $this->CI->input->post('year_level');
					$data['block_sections_type']  	= 'bed';
					$data['inserted_by']  		  	= $this->CI->session->userdata('empno');
					
					$this->CI->db->trans_start();
					
					if ($this->CI->section_model->AddBlockSection($data)) {
						$this->CI->db->trans_complete();
						$my_return['success'] = TRUE;
					} else {
						$this->CI->db->trans_rollback();
						$my_return['success'] = FALSE;
					}

					$data2['sections']    = $this->CI->section_model->ListSections($this->CI->input->post('acad_program_groups_id'),$this->CI->input->post('academic_years_id'));

					$my_return['output'] = $this->CI->load->view('bed/sections/list_all_sections', $data2, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'delete_section':

					if ($this->CI->input->post('room_no')) {
						$rm_no = $this->CI->input->post('room_no');
					} else {
						$rm_no = NULL;
					}
					
					$this->CI->db->trans_start();

					if ($this->CI->section_model->DeleteSection($this->CI->input->post('section_id'), $rm_no)) {
						$this->CI->db->trans_complete();
						$my_return['msg'] = TRUE;
					} else {
						$this->CI->db->trans_rollback();
						$my_return['msg'] = FALSE;
					}

					$data2['sections']    = $this->CI->section_model->ListSections($this->CI->input->post('acad_program_groups_id'),$this->CI->input->post('academic_years_id'));

					$my_return['output'] = $this->CI->load->view('bed/sections/list_all_sections', $data2, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'download_classlist_pdf':

					$data1['students']      = $this->CI->section_model->ListStudentsEnrolled($this->CI->input->post('section_id'));
					$data1['grade_section'] = $this->CI->input->post('grade_section');
					$data1['strand']        = $this->CI->input->post('strand');
					$data1['class_adviser'] = $this->CI->input->post('class_adviser');
					$data1['room_no']       = $this->CI->input->post('room_no');
					$data1['term_sy']       = $this->CI->input->post('term_sy');
					$data1['abbreviation']  = $this->CI->input->post('abbreviation');
					$data1['list_type']     = 'adviser';
										
					$this->generate_classlist_pdf($data1);
					
					return;
		
				case 'download_report_card_pdf':
				
					$data1['students']  = $this->CI->section_model->ListStudentsEnrolled($this->CI->input->post('section_id'));
					$data1['term_id']   = $this->CI->input->post('term_id');
					$data1['term_text'] = $this->CI->input->post('term_text');
					$data1['filename']  = $this->CI->input->post('filename');

					$this->generate_section_report_card_pdf($data1);

					return;
					
			}
			//print_r($data); die();	
			$this->CI->content_lib->enqueue_body_content('bed/section_management',$data);
			$this->CI->content_lib->content();
				
		}
			

		public function detailed_section_management($section_id=NULL) {

			$this->CI->load->model('hnumis/bed/section_model');
			$this->CI->load->model('hnumis/bed/year_level_model');
			$this->CI->load->model('hnumis/faculty_model');
			$this->CI->load->model('hnumis/bed/rooms_model');
			$this->CI->load->model('hnumis/bed/course_offerings_model');
			$this->CI->load->model('hnumis/bed/subject_model');
			$this->CI->load->model('hnumis/academic_terms_model');
			
			$data['section']    = $this->CI->section_model->getSection($section_id);
			
			if (!$data['section']) {
				redirect(base_url());
			}				
			
			$data['section_id']	= $section_id;
			
			$data['schedules']  = $this->CI->section_model->ListSectionSchedules($section_id);
			$data['students']   = $this->CI->section_model->ListStudentsEnrolled($section_id);
			$data['assessment'] = $this->CI->section_model->getBlockSectionAssessment($section_id);
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':
				
					$data['active_tab'] = "tab3";

					break;
				
				case 'extract_teachers':
				
					$data2['teachers'] 		= $this->CI->faculty_model->ListAllFaculty();
					$data2['teacher_id'] 	= $this->CI->input->post('block_adviser');
					
					$my_return['output']= $this->CI->load->view('bed/pulldown_menu/select_teachers', $data2, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'assign_section_adviser':

					$data['block_sections_id'] = $section_id;
					
					if ($this->CI->input->post('block_adviser') == "NULL") {
						$data['block_adviser'] = NULL;
					} else {
						$data['block_adviser'] = $this->CI->input->post('block_adviser');
					}
					
					$data['updated_by']        = $this->CI->session->userdata('empno');
					$data['active_tab']        = "tab3";
					
					if ($this->CI->section_model->AssignBlockAdviser($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}

					$data['section'] = $this->CI->section_model->getSection($section_id);
					
					$my_return['output'] = $this->CI->load->view('bed/detailed_section_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'update_section_name':

					$data['block_sections_id'] = $section_id;
					$data['section_name']      = $this->CI->input->post('section_name');			
					$data['updated_by']        = $this->CI->session->userdata('empno');
					$data['active_tab']        = "tab3";
					
					if ($this->CI->section_model->UpdateSectionName($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}

					$data['section'] = $this->CI->section_model->getSection($section_id);
					
					$my_return['output'] = $this->CI->load->view('bed/detailed_section_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'extract_bed_rooms':
				
					$days = "('Mon','Tue','Wed','Thu','Fri')";
					$start_time = '07:30';
					$end_time   = '17:30';
					
					$buildings = array(5,6,10); //building ids for janseen, GS, HS

					
					/*  NOTE: no more checking of vacant rooms as per Rey's instruction.
					*         Just display rooms for Janseen, GS, HS - Genes 13/02/2019
					*/
					$data2['bed_rooms'] = $this->CI->rooms_model->listBEDRooms($buildings);
										
					$data2['rm_id'] 	= $data['section']->rooms_id;
					$data2['var_name'] 	= "room_id";

					$my_return['bed_buildings'] = $this->CI->load->view('bed/sections/modal_view/display_bed_rooms', $data2, TRUE);

					$my_return['rooms_id']      = $data['section']->rooms_id;
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'assign_room_no':

					$data2['block_sections_id'] = $section_id;
					$data2['rooms_id']          = $this->CI->input->post('rooms_id');

					$data2['academic_years_id'] = $data['section']->academic_years_id;
					$data2['start_time']        = '07:30:00';
					$data2['end_time']          = '17:30:00';
					//$data2['day_names1']        = "('Mon','Tue','Wed','Thu','Fri')";    // used in CheckIfRoomOccupied
					$data2['day_names']         = array('Mon','Tue','Wed','Thu','Fri'); //used in AssignRoom
					$data2['occupancy_type']    = "bed_section";
					
					$data2['encoded_by']        = $this->CI->session->userdata('empno');
					$data2['updated_by']        = $this->CI->session->userdata('empno');
					
					$this->CI->db->trans_begin();
					
					if (!$data['section']->rooms_id) {
							
						//extract acad terms (exclude summer) for the given acad yr
						$academic_terms = $this->CI->academic_terms_model->listBEDAcadTerms($data['section']->academic_years_id);
					
						foreach($academic_terms AS $acad_term) {
							$data2['academic_terms_id'] = $acad_term->id;
								
							$this->CI->section_model->AssignRoom_RoomOccupancy($data2);
							$this->CI->section_model->AssignRoomTo_BEDBlockSection($data2);
								
						}

					} else {
						
							$this->CI->section_model->UpdateRoom_RoomOccupancy($data2);
							$this->CI->section_model->AssignRoomTo_BEDBlockSection($data2);
							
							$this->CI->course_offerings_model->UpdateOfferingSlotsRoom($data2); //update automatically all rooms in the corresponding course offerings

					}
						
					if ($this->CI->db->trans_status() === FALSE) {
						$this->CI->db->trans_rollback();
					} else {
						$this->CI->db->trans_commit();
					}	

					$data['section'] 	= $this->CI->section_model->getSection($section_id);
					$data['schedules'] 	= $this->CI->section_model->ListSectionSchedules($section_id);
					$data['active_tab']	= "tab3";
					
					$my_return['output'] = $this->CI->load->view('bed/detailed_section_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'extract_courses_to_create_schedule':
				
					$data2['courses'] 		= $this->CI->subject_model->ListBEDCoursesByProspectus($data['section']);

					$my_return['courses'] 	= $this->CI->load->view('bed/sections/modal_view/display_courses_to_create_schedule', $data2, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'create_class_schedule':
				
					$data['block_sections_id'] = $section_id;
					$data['courses_id']        = $this->CI->input->post('courses_id');
					$data['rooms_id']          = $data['section']->rooms_id;

					$data['academic_years_id'] = $data['section']->academic_years_id;
					$data['start_time']        = date('H:i:s', strtotime($this->CI->input->post('start_time')));
					$data['end_time']          = date('H:i:s', strtotime($this->CI->input->post('end_time')));
					$data['day_names']         = $this->CI->input->post('days_selected');
					$data['occupancy_type']    = "course_offering";
					
					$data['inserted_by']       = $this->CI->session->userdata('empno');
					$data['encoded_by']        = $this->CI->session->userdata('empno');

					$this->CI->db->trans_begin();

					//check if the course on the particular section and acad year is already offered
					$course_offerings = $this->CI->course_offerings_model->getCourse_Offerings($section_id,$this->CI->input->post('courses_id'),$data['section']->academic_years_id);
					
					if (!$course_offerings) { //if not yet offered, create record

						$this->CI->course_offerings_model->AddBEDCourseOfferings($data);
							
					} else { //if already offered, create another record on child course offerings slots
						
						$data['bed_course_offerings_id'] = $course_offerings->course_offerings_id;
						
						$this->CI->course_offerings_model->AddBEDCourseOfferingsSlots($data);
																				
					}

					$data['bed_course_offerings_slots_id'] = $this->CI->course_offerings_model->get_BEDCourse_Offerings_Slots_Id();
							
					//extract acad terms (exclude summer) for the given acad yr
					$academic_terms = $this->CI->academic_terms_model->listBEDAcadTerms($data['section']->academic_years_id);
					
					foreach($academic_terms AS $acad_term) {
						$data['academic_terms_id'] = $acad_term->id;
								
						$this->CI->section_model->AssignRoom_RoomOccupancy($data);							
					}
					
					if ($this->CI->db->trans_status() === FALSE) {
						$this->CI->db->trans_rollback();
					} else {
						$this->CI->db->trans_commit();
					}	
					
					$data['section']   	= $this->CI->section_model->getSection($section_id);
					$data['schedules'] 	= $this->CI->section_model->ListSectionSchedules($section_id);
					$data['active_tab']	= "tab1";
					
					$my_return['output'] = $this->CI->load->view('bed/detailed_section_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'delete_schedule':
				
					$this->CI->db->trans_begin();

					$offerings_slots = $this->CI->course_offerings_model->CountOffering_Slots($this->CI->input->post('bed_course_offerings_id'));
					
					if ($offerings_slots->cnt > 1) { //if more than 1, delete only the slot
						
						$this->CI->section_model->DeleteRoomOccupancy($this->CI->input->post('bed_course_offerings_slots_id'));
						
						$this->CI->course_offerings_model->DeleteOffering_Slots($this->CI->input->post('bed_course_offerings_slots_id'));
						
					} else { //if remaining slot, delete offering; already cascaded to child tables 
						
						$this->CI->section_model->DeleteRoomOccupancy($this->CI->input->post('bed_course_offerings_slots_id'));
						
						$this->CI->course_offerings_model->DeleteOffering($this->CI->input->post('bed_course_offerings_id'));
						
					}
					
					if ($this->CI->db->trans_status() === FALSE) {
						$this->CI->db->trans_rollback();
					} else {
						$this->CI->db->trans_commit();
					}	
					
					$data['active_tab'] = "tab1";
					$data['section']    = $this->CI->section_model->getSection($section_id);
					$data['schedules']  = $this->CI->section_model->ListSectionSchedules($section_id);
					
					$my_return['output'] = $this->CI->load->view('bed/detailed_section_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'extract_subject_teachers':
				
					$data2['teachers'] 		= $this->CI->faculty_model->ListAllFaculty();
					$data2['teacher_id'] 	= $this->CI->input->post('teacher_empno');
					
					$my_return['output']= $this->CI->load->view('bed/pulldown_menu/select_teachers', $data2, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'assign_teacher':

					$data['bed_course_offerings_id'] = $this->CI->input->post('bed_course_offerings_id');
					
					if ($this->CI->input->post('employees_empno') == "NULL") {
						$data['employees_empno'] = NULL;
					} else {
						$data['employees_empno'] = $this->CI->input->post('employees_empno');
					}
					
					$data['updated_by']        = $this->CI->session->userdata('empno');
					$data['active_tab']        = "tab1";
					
					if ($this->CI->course_offerings_model->AssignTeacher($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}

					$data['active_tab'] = "tab1";
					$data['section']    = $this->CI->section_model->getSection($section_id);
					$data['schedules']  = $this->CI->section_model->ListSectionSchedules($section_id);
					
					$my_return['output'] = $this->CI->load->view('bed/detailed_section_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'extract_rooms_for_change_schedule_offering':
				
					$days = $this->CI->input->post('days_names');
					
					$buildings = array(5,6,10); //building ids for janseen, GS, HS

					/*  NOTE: no more checking of vacant rooms as per Rey's instruction.
					*         Just display rooms for Janseen, GS, HS - Genes 13/02/2019
					*/
					$data['bed_rooms'] 	= $this->CI->rooms_model->listBEDRooms($buildings);
					
					$data['rm_id'] 		= $this->CI->input->post('offerings_slots_rooms_id');
					$data['var_name']  	= "room_id_change";
					
					$my_return['bed_buildings'] = $this->CI->load->view('bed/sections/modal_view/display_bed_rooms', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'change_schedule_of_offering':
					
					$data['block_sections_id']         = $section_id;

					$data['rooms_id']                  = $this->CI->input->post('rooms_id');
					$data['start_time']                = date('H:i:s', strtotime($this->CI->input->post('start_time'))); //used for course_offerings_slots table
					$data['end_time']                  = date('H:i:s', strtotime($this->CI->input->post('end_time')));   //used for course_offerings_slots table
					$data['from_time']                 = date('H:i:s', strtotime($this->CI->input->post('start_time'))); //used for room_occupancy table
					$data['to_time']                   = date('H:i:s', strtotime($this->CI->input->post('end_time')));   //used for room_occupancy table
					$data['day_names1']                = $this->CI->input->post('days_names');
					$data['day_names']                 = $this->CI->input->post('array_days');

					$data['bed_course_offerings_slots_id'] = $this->CI->input->post('course_offerings_slots_id');
					$data['occupancy_type']            = "course_offering";
					
					$data['inserted_by']               = $this->CI->session->userdata('empno');
					$data['encoded_by']                = $this->CI->session->userdata('empno');
					$data['active_tab']                = "tab1";

					$this->CI->db->trans_begin();
					
					//extract acad terms (exclude summer) for the given acad yr
					$academic_terms = $this->CI->academic_terms_model->listBEDAcadTerms($data['section']->academic_years_id);
					
					foreach($academic_terms AS $acad_term) {
						$data['academic_terms_id'] = $acad_term->id;
								
						//$this->CI->section_model->AssignRoom_RoomOccupancy($data);							
						$this->CI->section_model->DeleteInsertRoom_RoomOccupancy($data);
						
					}
					
					$this->CI->course_offerings_model->UpdateSchedule_CourseOfferingsSlot($data);
					
					if ($this->CI->db->trans_status() === FALSE) {
						$this->CI->db->trans_rollback();
					} else {
						$this->CI->db->trans_commit();
					}	

					$data['section'] = $this->CI->section_model->getSection($section_id);
					$data['schedules'] = $this->CI->section_model->ListSectionSchedules($section_id);
					
					$my_return['output'] = $this->CI->load->view('bed/sections/class_schedule', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

			}

			$this->CI->content_lib->enqueue_body_content('bed/detailed_section_management',$data);
			$this->CI->content_lib->content();
			
		}

				
		public function class_adviser_section() {
			
			$this->CI->load->model('hnumis/bed/section_adviser_model');
			$this->CI->load->model('hnumis/bed/section_model');
			$this->CI->load->model('hnumis/shs/shs_faculty_model');
			$this->CI->load->model('hnumis/shs/shs_grades_model');
			$this->CI->load->model('hnumis/shs/attendance_model');
			$this->CI->load->model('hnumis/shs/behavior_model');
			
			$data['sections']   = $this->CI->section_adviser_model->List_MySections($this->CI->session->userdata('empno'));

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_sections');
			
			switch ($action) {
				case 'list_sections':

					break;
				
				case 'extract_section_schedule':
				
					$data['class_schedules'] = $this->CI->section_adviser_model->ListClassSchedules($this->CI->input->post('section_id'));
					$data['show_pulldown']   = FALSE;

					$my_return['output']     = $this->CI->load->view('bed/student/student_class_schedules', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;				
				
				case 'extract_section_subjects':
				
					$data['courses']     = $this->CI->section_model->ListCoursesAssigned($this->CI->input->post('section_id'));

					$my_return['output'] = $this->CI->load->view('shs/class_adviser/my_class_subjects', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;				
				
				case 'extract_section_students':

					$data['students']      = $this->CI->section_model->ListStudentsEnrolled($this->CI->input->post('section_id'), 'gender');
					$data['grade_section'] = $this->CI->input->post('grade_section');
					$data['strand']        = $this->CI->input->post('description');
					$data['room_no']       = $this->CI->input->post('room_no');
					$data['term_sy']       = $this->CI->input->post('term_sy');
					$data['abbreviation']  = $this->CI->input->post('abbreviation');
					$data['link_student']  = TRUE;
					
					$my_return['download'] = $this->CI->load->view('shs/reports/download_students_header', $data, TRUE);
								
					$my_return['output']   = $this->CI->load->view('shs/reports/list_of_enrolled_students', $data, TRUE);
												
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;				

				case 'extract_students_for_attendance':

					$data['academic_terms_id'] = $this->CI->input->post('academic_terms_id');
					$data['section_id']        = $this->CI->input->post('section_id');
									
					$data['months']	= $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('academic_terms_id'));	
					
					if ($data['months'] != NULL) {
						$data['students']       = $this->CI->attendance_model->ListStudentsForAttendance($this->CI->input->post('section_id'), $data['months'][0]->id, $data['academic_terms_id']);
						$data['school_days_id'] = $data['months'][0]->id;
						$my_return['success']   = TRUE; 
					} else {
						$data['students']       = NULL;
						$data['school_days_id'] = NULL;
						$my_return['success']   = FALSE; 
					}
					

					$my_return['output'] = $this->CI->load->view('shs/class_adviser/list_students_for_attendance', $data, TRUE);
												
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;				
					
				case 'change_extract_students_for_attendance':

					$data['academic_terms_id'] = $this->CI->input->post('academic_terms_id');
					$data['section_id']        = $this->CI->input->post('section_id');

					$data['months']	        = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('academic_terms_id'));						
					$data['students']       = $this->CI->attendance_model->ListStudentsForAttendance($this->CI->input->post('section_id'), $this->CI->input->post('school_days_id'),$data['academic_terms_id']);
					$data['school_days_id'] = $this->CI->input->post('school_days_id');

					$my_return['output'] = $this->CI->load->view('shs/class_adviser/list_students_for_attendance', $data, TRUE);
												
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;				
					
				case 'download_class_list_to_pdf':			

					$data['students']      = json_decode($this->CI->input->post('students'));
					$data['grade_section'] = $this->CI->input->post('grade_section');
					$data['strand']        = $this->CI->input->post('strand');
					$data['class_adviser'] = $this->CI->session->userdata('lname').", ".$this->CI->session->userdata('fname');
					$data['room_no']       = $this->CI->input->post('room_no');
					$data['term_sy']       = $this->CI->input->post('term_sy');
					$data['abbreviation']  = $this->CI->input->post('abbreviation');
					$data['list_type']     = 'adviser';
										
					$this->generate_classlist_pdf($data);
					
					return;
					
				case 'extract_students_for_grade_confirm':
				
					$data['midterm_confirm']     = FALSE;
					$data['finals_confirm']      = FALSE;
					$data['midterm_submitted']   = FALSE;
					$data['finals_submitted']    = FALSE;					
					$data['course_offerings_id'] = $this->CI->input->post('course_offerings_id');
					$data['block_sections_id']   = $this->CI->input->post('block_sections_id');
					$data['courses_id']			 = $this->CI->input->post('courses_id');
					
					$data['students'] = $this->CI->shs_faculty_model->List_MyStudents($this->CI->input->post('block_sections_id'),$this->CI->input->post('courses_id'),'by_gender');

					//check if course_offerings already confirmed by Adviser
					$confirmation = $this->CI->shs_faculty_model->Adviser_Confirm($this->CI->input->post('course_offerings_id'));
					
					if (isset($confirmation->midterm_submitted_date)) {
						$data['midterm_submitted'] = TRUE;
					}
					
					if (isset($confirmation->finals_submitted_date)) {
						$data['finals_submitted'] = TRUE;
					}

					if (isset($confirmation->midterm_confirm_date)) {
						$data['midterm_confirm']      = TRUE;
						$data['midterm_confirm_date'] = $confirmation->midterm_confirm_date;
					}

					if (isset($confirmation->finals_confirm_date)) {
						$data['finals_confirm']      = TRUE;
						$data['finals_confirm_date'] = $confirmation->finals_confirm_date;
					}
					

					$my_return['output'] = $this->CI->load->view('shs/class_adviser/list_of_students_for_grade_confirmation', $data, TRUE);
				
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;				
				
				case 'confirm_grades':
											
					$data['midterm_confirm_by']  = $this->CI->session->userdata('empno');
					$data['finals_confirm_by']   = $this->CI->session->userdata('empno');
					$data['period']              = $this->CI->input->post('period');
					$data['course_offerings_id'] = $this->CI->input->post('course_offerings_id');

					if ($this->CI->shs_grades_model->Save_GradeConfirmDate($data)) {
						$my_return['success'] = TRUE;
						
						$confirmation = $this->CI->shs_faculty_model->Adviser_Confirm($this->CI->input->post('course_offerings_id'));
						
						switch ($this->CI->input->post('period')) {
							
							case 'Midterm':
								$my_return['message'] = 'Midterm Grades confirmed on: '.$confirmation->midterm_confirm_date;
								
								break;
								
							case 'Finals':
								$my_return['message'] = 'Finals Grades confirmed on: '.$confirmation->finals_confirm_date;
								
								break;							
							
						}
						
					} else {
						$my_return['success'] = FALSE;
					}					
						

					$data['block_sections_id']   = $this->CI->input->post('block_sections_id');
					$data['courses_id']			 = $this->CI->input->post('courses_id');

					$data['students'] = $this->CI->shs_faculty_model->List_MyStudents($this->CI->input->post('block_sections_id'),$this->CI->input->post('courses_id'));

					//check if course_offerings already confirmed by Adviser
					$confirmation = $this->CI->shs_faculty_model->Adviser_Confirm($this->CI->input->post('course_offerings_id'));
									
					if (isset($confirmation->midterm_confirm_date)) {
						$data['midterm_confirm'] = TRUE;
						$data['midterm_confirm_date'] = $confirmation->midterm_confirm_date;
					}

					if (isset($confirmation->finals_confirm_date)) {
						$data['finals_confirm'] = TRUE;
						$data['finals_confirm_date'] = $confirmation->finals_confirm_date;
					}
					
					$data['courses']     = $this->CI->section_model->ListCoursesAssigned($this->CI->input->post('block_sections_id'));

					$my_return['output'] = $this->CI->load->view('shs/class_adviser/my_class_subjects', $data, TRUE);
				
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;		

				case 'submit_attendance':
				
					if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
						
						$data1['school_days_id'] = $this->CI->input->post('school_days_id');
						$data1['encoded_by']     = $this->CI->session->userdata('empno');
						$tardy                   = $this->CI->input->post('days_tardy');
						
						foreach($this->CI->input->post('days_present') AS $k=>$v) {
							
							if ($v) {
								$data1['student_histories_id'] = $k;
								$data1['days_present']         = $v;
								$data1['days_tardy']           = $tardy[$k];
								
								if ($this->CI->attendance_model->SubmitAttendance($data1)) {
									$this->CI->content_lib->set_message('Student Attendance successfully submitted!', 'alert-success');								
								} else {
									$this->CI->content_lib->set_message('ERROR: Attendance submission not successful!', 'alert-error');
								}
							}
						}
					
					} else {
						$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');	
					}
						
					break;
					
				case 'update_marking': //by Class Adviser
				
					$data['student_histories_id']   = $this->CI->input->post('student_histories_id');
					$data['behavior_statements_id'] = $this->CI->input->post('behavior_statements_id');
					$data['marking']                = $this->CI->input->post('marking');
					$data['period']                 = $this->CI->input->post('period');
					$data['encoded_by']             = $this->CI->session->userdata('empno');
					
					if ($this->CI->behavior_model->Add_StudentBehaviorStatements($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}
					
					$data['observed_values'] = $this->CI->behavior_model->ListObservedValues($this->CI->input->post('student_histories_id'));

					$data['can_update_values'] = TRUE; //used to check if can update baheior values
				
					$my_return['output'] = $this->CI->load->view('shs/class_adviser/observed_values', $data, TRUE);
				
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;		
				
				case 'update_remark': //by Class Adviser
				
					$data['enrollments_id'] = $this->CI->input->post('enrollments_id');
					$data['remark']         = $this->CI->input->post('remark');
					$data['encoded_by']     = $this->CI->session->userdata('empno');
					
					if ($this->CI->shs_grades_model->Add_ReportCard_Remark($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}
					
					$data['grades'] = $this->CI->shs_grades_model->List_MyGrades($this->CI->input->post('students_idno'), $this->CI->input->post('term_id')); 
					$data['idnum']  = $this->CI->input->post('students_idno');

					$data['can_update_remark'] = TRUE; //used to check if can update remark for Report Card
				
					$my_return['output'] = $this->CI->load->view('shs/class_adviser/submit_remarks_form', $data, TRUE);
				
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;		
					
			}
			
			$this->CI->content_lib->enqueue_body_content('bed/section_adviser/my_sections',$data);
			$this->CI->content_lib->content();
							
			
		}
		
		
		public function generate_classlist_pdf($data) {
			ob_start();

			$this->CI->load->library('my_pdf');
			$this->CI->load->library('hnumis_pdf');
		
			$this->CI->hnumis_pdf->AliasNbPages();		
			$this->CI->hnumis_pdf->set_HeaderTitle('CLASSLIST');
			$this->CI->hnumis_pdf->AddPage('P','folio');
			
			$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->CI->hnumis_pdf->SetDrawColor(200,200,200);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();
			$l=5;
						
			$this->CI->hnumis_pdf->SetY(27);
			//$this->CI->hnumis_pdf->SetX(15);

			$this->CI->hnumis_pdf->SetFont('times','',11);
			$this->CI->hnumis_pdf->SetFillColor(255,255,255);

			$this->CI->hnumis_pdf->Cell(25,$l,'School Year:',0,0,'L',true);
			$this->CI->hnumis_pdf->Cell(70,$l,$data['term_sy'],0,0,'L',true);
			$this->CI->hnumis_pdf->Cell(25,$l,'Room No.:',0,0,'L',true);
			$this->CI->hnumis_pdf->Cell(40,$l,$data['room_no'],0,1,'L',true);
			$this->CI->hnumis_pdf->Cell(25,$l,'Strand:',0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('times','B',11);
			$this->CI->hnumis_pdf->Cell(70,$l,$data['strand'],0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('times','',11);
			$this->CI->hnumis_pdf->Cell(25,$l,'Class Adviser:',0,0,'L',true);
			$this->CI->hnumis_pdf->Cell(40,$l,$data['class_adviser'],0,1,'L',true);
			
			if ($data['list_type'] == 'adviser') {
				$this->CI->hnumis_pdf->Cell(25,$l,'Grade-Section:',0,0,'L',true);
				$this->CI->hnumis_pdf->SetFont('times','B',11);
				$this->CI->hnumis_pdf->Cell(40,$l,$data['grade_section'],0,1,'L',true);
				$this->CI->hnumis_pdf->SetFont('times','',11);
			} else {
				$this->CI->hnumis_pdf->Cell(25,$l,'Teacher:',0,0,'L',true);
				$this->CI->hnumis_pdf->Cell(70,$l,$data['teacher'],0,0,'L',true);
				$this->CI->hnumis_pdf->Cell(25,$l,'Course:',0,0,'L',true);
				$this->CI->hnumis_pdf->SetFont('times','B',11);
				$this->CI->hnumis_pdf->Cell(40,$l,$data['descriptive_title'],0,1,'L',true);
				$this->CI->hnumis_pdf->SetFont('times','',11);
			}
			
			$this->CI->hnumis_pdf->SetFont('times','B',11);
			$this->CI->hnumis_pdf->SetTextColor(10,10,10);
			$this->CI->hnumis_pdf->SetFillColor(200,200,200);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->SetX(15);

			$header1 = array('Count','ID No.','Lastname','Firstname','Middlename','Gender');

			$w1 = array(15,25,40,40,40,25);
			
			for($i=0;$i<count($w1);$i++)
				$this->CI->hnumis_pdf->Cell($w1[$i],8,$header1[$i],1,0,'C',true);

			$l=6;
			$this->CI->hnumis_pdf->Ln();

			$this->CI->hnumis_pdf->SetFillColor(255,255,255);
			$this->CI->hnumis_pdf->SetTextColor(0,0,0);
			$this->CI->hnumis_pdf->SetFont('Arial','',10);

			if ($data['students']) {
				$cnt=1;
				foreach($data['students'] AS $student) {
					$this->CI->hnumis_pdf->SetX(15);
					$this->CI->hnumis_pdf->Cell($w1[0],$l,$cnt.'.',1,0,'R',true);
					$this->CI->hnumis_pdf->Cell($w1[1],$l,$student->idno,1,0,'C',true);
					$this->CI->hnumis_pdf->Cell($w1[2],$l,utf8_decode($student->lname),1,0,'L',true);
					$this->CI->hnumis_pdf->Cell($w1[3],$l,$student->fname,1,0,'L',true);
					$this->CI->hnumis_pdf->Cell($w1[4],$l,$student->mname,1,0,'L',true);
					$this->CI->hnumis_pdf->Cell($w1[5],$l,$student->gender,1,0,'C',true);
					$this->CI->hnumis_pdf->Ln();
					$cnt++;
				}
			}
			
			if ($data['list_type'] == 'adviser') {
				$this->CI->hnumis_pdf->Output($data['abbreviation'].'-'.$data['grade_section'].'.pdf','I');
			} else {
				$this->CI->hnumis_pdf->Output($data['strand'].'.pdf','I');				
			}
	
			ob_end_flush(); 	
		}


		
		private function generate_section_report_card_pdf($data) {

			ob_start();

			$this->CI->load->model('hnumis/shs/shs_student_model');
			$this->CI->load->model('hnumis/shs/shs_grades_model');
			$this->CI->load->model('hnumis/shs/behavior_model');
			$this->CI->load->model('hnumis/shs/attendance_model');
			$this->CI->load->library('my_pdf');
			$this->CI->load->library('hnumis_pdf');
			$this->CI->load->library('shs/shs_student_lib.php');
			
			$new_size = array(215.9,330.2); //this is the size for a halfpage longbond

			$this->CI->hnumis_pdf->WithHeader(FALSE);		
			$this->CI->hnumis_pdf->WithFooter(FALSE);		
			$this->CI->hnumis_pdf->SetAutoPageBreak(false);
			
			$this->CI->hnumis_pdf->AliasNbPages();		
			$this->CI->hnumis_pdf->AddPage('P',$new_size);
			
			$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->CI->hnumis_pdf->SetDrawColor(200,200,200);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();
			
			$data1['fullwidth'] = FALSE;
			
			if ($data['students']) {
				
				$cnt = 0;
				$data1['y_attendance'] = 125;
				$data1['y_descriptor'] = 53;
				
				foreach ($data['students'] AS $sec_student) {

					$selected_history       = $this->CI->shs_student_model->get_StudentHistory_id($sec_student->idno, $data['term_id']);  

					$data1['grade_reports'] = $this->CI->shs_grades_model->List_Grades_For_Report_Cards($sec_student->idno, $data['term_id']); 
					$data1['school_days']   = $this->CI->attendance_model->ListSchoolDays($data['term_id']);
					$data1['present_days']  = $this->CI->attendance_model->ListPresentDays($data['term_id'], $selected_history->id);
					$data1['term_text']     = $data['term_text'];

					$from = new DateTime($sec_student->dbirth);
					$to   = new DateTime('today');

					$student1 = new StdClass();
					
					$student1->name    = utf8_decode(strtoupper($sec_student->lname.', '.$sec_student->fname.' '.$sec_student->mname));
					$student1->gender  = $sec_student->gender;
					$student1->age     = $from->diff($to)->y;
					$student1->grade   = $selected_history->grade;
					$student1->section = $selected_history->section;
					$student1->lrn     = $selected_history->lrn;
					$student1->block_adviser = $selected_history->block_adviser;
					$student1->strand  = $selected_history->strand;
					
					$data1['student']    = $student1;

					//log_message("INFO",print_r($data1,true)); // Toyet 9.11.2018
					
					$this->CI->shs_student_lib->headings($data1);
					
					$this->CI->shs_student_lib->name_and_section($data1);

					$this->CI->shs_student_lib->grades($data1);

					$this->CI->shs_student_lib->attendance($data1);

					$this->CI->shs_student_lib->side_descriptors($data1);
					
					$this->CI->hnumis_pdf->SetDrawColor(0,0,0);
					$this->CI->hnumis_pdf->Line(6,165.1,215.9-6,165.1);
					$data1['y_attendance'] = 291;
					$data1['y_descriptor'] = 217;

					if (!$data1['fullwidth']) {
						$data1['fullwidth']  = TRUE;
					}
					
					$cnt ++;
					
					if ($cnt == 2) {
						$this->CI->hnumis_pdf->AddPage('P',$new_size);
						$cnt = 0;
						$data1['y_attendance'] = 125;
						$data1['y_descriptor'] = 53;
						$data1['fullwidth'] = FALSE;
					}

				}
				
				//start here for the 2nd page of Report Card
				
				$this->CI->hnumis_pdf->AddPage('P',$new_size);

				$cnt = 0;
				$data1['y_values'] = 20;
				
				foreach ($data['students'] AS $sec_student) {

					$selected_history        = $this->CI->shs_student_model->get_StudentHistory_id($sec_student->idno, $data['term_id']);  

					$data1['observed_values'] = $this->CI->behavior_model->ListObservedValues($selected_history->id);

					$student1 = new StdClass();
					
					$student1->block_adviser = $selected_history->block_adviser;
					$student1->lrn           = $selected_history->lrn;
					
					$data1['student']    = $student1;
					
					$this->CI->shs_student_lib->observed_values($data1); 
					
					$this->CI->shs_student_lib->footings($data1);
					
					$this->CI->hnumis_pdf->SetDrawColor(0,0,0);
					$this->CI->hnumis_pdf->Line(6,165.1,215.9-6,165.1);
					$data1['y_values'] = -18;

					if (!$data1['fullwidth']) {
						$data1['fullwidth']  = TRUE;
					}
					
					$cnt ++;
					
					if ($cnt == 2) {
						$this->CI->hnumis_pdf->AddPage('P',$new_size);
						$cnt = 0;
						$data1['y_values']  = 20;
						$data1['fullwidth'] = FALSE;
					}

				}
				
			}

			
			ob_end_clean();
			
			$this->CI->hnumis_pdf->Output("Report Card - ".$data['filename'].'.pdf','I');
	
			ob_end_flush(); 	
		}


	}
	
?>
