

<h2 class="heading">Senior High School Assessment Report - By Strand</h2>

	<div style="width:80%;">

		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 35px; padding: 2px; margin-bottom:20px;">
			<div style="float:right; padding-top:8px; margin-right:10px;" >
				<a href="#" style="text-decoration: none; color:#4E4D4D; font-size:12px; "
					abbreviation="<?php print($abbreviation); ?>"
					grade_level="<?php print($grade_level); ?>"
					term_sy="<?php print($term_sy); ?>"
					num_students="<?php print($num_students); ?>"
					tuition_fees='<?php print(json_encode($tuition_fees)); ?>'
					misc_fees='<?php print(json_encode($misc_fees)); ?>'
					lab_fees='<?php print(json_encode($lab_fees)); ?>'
					class="download_pdf_report" >
					<div style="float:right; padding-left:3px; padding-top:2px;">Download to PDF</div>
					<div id="excel_labels" style="float:right;">
						<img src="<?php print(base_url('assets/images/pdf_icon.png')); ?>" style="height:20px;" /> 
					</div>
				</a>
			</div>
		</div>
		
		<div style="width:100%; margin:0px auto; text-align:center; font-size:20px; font-weight:bold; color:#022E7A;" >
			DETAILED ASSESSMENT REPORT FOR: <?php print($description." - ".$grade_level); ?>
		</div>
		<div style="width:100%; margin:0px auto; text-align:center; font-size:16px; font-weight:bold; font-style:italic; color:#0A5901; padding-top:5px;" >
			<?php print($term_sy); ?>
		</div>
		
		<div style="border:solid 1px #D4D6D4; line-height:30px; padding-left:5px; font-size:16px; font-weight:bold; background-color:#DDFFD9; vertical-align:middle; margin-top:20px;">
			No. of Students: <?php print($num_students); ?>
		</div>
		
		<div style="margin-top:15px; padding-left:10px;">
			<?php 
				$grand_total = 0;
				if ($tuition_fees) {
					$total_tuition = 0;
					foreach($tuition_fees AS $tuition) {
			?>
						<div style="width:40%; float:left;">
							Tuition Fee [<?php print($tuition->paying_units); ?> @ <?php print($tuition->amt); ?>] (<?php print($tuition->num_students); ?> students)
						</div>
						<div style="width:20%; float:left; text-align:right;">
							<?php print(number_format($tuition->paying_units*$tuition->amt,2)); ?>
						</div>
			<?php
						$total_tuition = $total_tuition + ($tuition->paying_units*$tuition->amt);
					}
				}
			?>
		
			<div style="clear:both; width:80%; font-weight:bold;">
				<div style="width:80%; float:left;">
					Tuition Fee
				</div>
				<div style="width:20%; float:left; text-align:right;">
					<?php print(number_format($total_tuition,2)); $grand_total += $total_tuition; ?>
				</div>
			</div>

			<div style="clear:both;">
			</div>
			
			<div style="width:100%; margin-top:15px;">
			<?php 
				$total_misc = 0;
				if ($misc_fees) {
					foreach($misc_fees AS $misc) { 
			?>
					<div style="margin-top:15px; font-weight:bold;">
						<?php 
							print($misc->fees_group); 
							
							if ($misc->fees_groups) {
								foreach($misc->fees_groups AS $fgroup) {
						?>
									<div style="clear:both; padding-left:20px; font-weight:normal;">
										<div style="width:50%; float:left;">
											<?php print($fgroup->description); ?> @ <?php print($fgroup->amt); ?> (<?php print($fgroup->num_students); ?> students)
										</div>
										<div style="width:12%; float:left; text-align:right;">
											<?php print(number_format($fgroup->amt*$fgroup->num_students,2)); $total_misc += $fgroup->amt*$fgroup->num_students; ?>
										</div>
									</div>
						<?php
								}
							}
						?>
					</div>
					<div style="width:80%; clear:both; float:left; text-align:right; font-weight:bold;">
						<?php print(number_format($total_misc,2)); $grand_total += $total_misc; $total_misc = 0; ?>
					</div>
					<div style="clear:both;">
					</div>
			<?php
					}
				}
			?>
			</div>
			
			<div style="clear:both;">
			</div>
			
			<?php 
				$total_lab = 0;
				if ($lab_fees) {
			?>
			<div style="font-weight:bold;">
				Laboratory Fees
			</div>
			<?php 
					foreach($lab_fees AS $lab_fee) {
			?>
						<div style="clear:both; padding-left:20px; font-weight:normal;">
							<div style="width:50%; float:left;">
								<?php print($lab_fee->course_code); ?> @ <?php print($lab_fee->rate); ?> (<?php print($lab_fee->num_students); ?> students)
							</div>
							<div style="width:12%; float:left; text-align:right;">
								<?php print(number_format($lab_fee->rate*$lab_fee->num_students,2)); $total_lab += $lab_fee->rate*$lab_fee->num_students; ?>							
							</div>
						</div>	
			<?php 
					}
			?>	
					<div style="width:80%; clear:both; float:left; text-align:right; font-weight:bold;">
						<?php print(number_format($total_lab,2)); $grand_total += $total_lab; $total_lab = 0; ?>
					</div>
					<div style="clear:both;">
					</div>
			<?php
				}
			?>
			
		</div>

		<div style="margin-top:25px; border:solid 1px #D4D6D4; font-weight:bold; font-size:16px; background-color:#DDFFD9; height:30px; ">
			<div style="width:20%; float:left; line-height:30px; vertical-align:middle; padding-left:5px;">
				TOTAL Assessment:
			</div>
			<div style="width:60%; float:left; text-align:right; line-height:30px; vertical-align:middle; ">
				<?php print(number_format($grand_total,2)); ?>
			</div>
		</div>
	</div>	
	

<form id="download_tuition_report_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_to_pdf" />
</form>

<script>
	$('.download_pdf_report').click(function(event){
		event.preventDefault(); 
		var abbreviation = $(this).attr('abbreviation');
		var grade_level  = $(this).attr('grade_level');
		var term_sy      = $(this).attr('term_sy');
		var num_students = $(this).attr('num_students');
		var tuition_fees = $(this).attr('tuition_fees');
		var misc_fees    = $(this).attr('misc_fees');
		var lab_fees     = $(this).attr('lab_fees');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'abbreviation',
			value: abbreviation,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'grade_level',
			value: grade_level,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'term_sy',
			value: term_sy,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'num_students',
			value: num_students,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'tuition_fees',
			value: tuition_fees,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'misc_fees',
			value: misc_fees,
		}).appendTo('#download_tuition_report_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'lab_fees',
			value: lab_fees,
		}).appendTo('#download_tuition_report_form');
		
		$('#download_tuition_report_form').submit();
		
	});
</script>

	