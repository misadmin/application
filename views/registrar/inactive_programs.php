<div class="row-fluid span12">
	<form method="post" id="inactive_programs_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
		<fieldset>
			<div class="control-group">
				<div class="controls">
					<select id="inactive_colleges" name="college">
						<option value="">-- Select College --</option>
<?php foreach ($colleges as $college): ?>
						<option value="<?php echo $college->id; ?>" <?php if(isset($college_id) && $college_id==$college->id) echo ' selected="selected" '?>><?php echo $college->name; ?></option>
<?php endforeach; ?>
					</select>
					<span id="inactive_loading_img" style="margin-left:10px;"><img style="width:30px;height:30px" src="<?php echo base_url($this->config->item('loading_image')); ?>" /></span>
				</div>
			</div>
			<div>
				<table class="table table-striped table-bordered mediaTable activeMediaTable" id="inactive_programs_table">
					<thead>
						<tr>
							<th width="3%"><input type="checkbox" id="inactive_select_all" /></th>
							<th width="25%">Program Abbreviation</th>
							<th width="72%">Program Name</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="control-group">
				<div class="controls">
					<button class="disabled btn btn-danger submit" id="activate_inactive">Activate Program(s)</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script>
$('#inactive_colleges').bind('change', function(){
	var college_id = $('#inactive_colleges option:selected').val();
	
	$('#inactive_loading_img').show();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=F",
		dataType: "json",
		success: function(data){
			var rows = make_rows(data, 'inactive');
			$('#inactive_programs_table > tbody').html(rows);
			$('#inactive_loading_img').hide();
		}	
	});

	$('#activate_inactive').removeClass('disabled');
});

$('#inactive_select_all').bind('change', function(){
	
	if ($(this).is(':checked')){
		$('.inactive_selector').each(function(){
			$(this).attr('checked', 'checked');
		});
	} else {
		$('.inactive_selector').each(function(){
			$(this).removeAttr('checked');
		});
	}
});
</script>