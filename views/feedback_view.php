<?php


class feedback extends table_lib{
	
	function format_data_id ($data){		
		//return  '<a href="feedback/' . $data['id'] . '">' . $data['id'] . '</a>';
		return '<input class="checkboxes" type="checkbox" name="check[]'  . '" value=' . $data['id'] . '>';
	}

	function format_data_developers_remark ($data){
		return nl2br($data['developers_remark']);
	}
	
	function format_data_idnum ($data){
		$directory 	= "/developer/";
		$directory .= $data['idnum'] < 20000 ? "faculty" : "student";
		$idno 		= $data['idnum'] < 20000 ? $data['idnum'] : str_pad($data['idnum'],8,'0',STR_PAD_LEFT);
		if ($this->role !=='student')		
			return	'<a href="'. site_url("{$directory}/{$idno}") .  '" >' . $idno . '</a>' ;
		else
			return $idno; 
		
	
	}
	
	
	
	function format_cell_id ($data){
		return ' style="text-align: center; vertical-align:middle" ';
	}
	
	
	function format_cell_developers_remark ($data){
		return 'class="dev_remark"  ';
	}
	
	function format_cell_status ($data){
		return ' style="text-align: center;"';
	}

	function format_cell_fbid ($data){
		return ' style="text-align: center; vertical-align:middle" ';
	}

	function format_cell_idnum ($data){
		$idno 		= $data['idnum'] < 20000 ? $data['idnum'] : str_pad($data['idnum'],8,'0',STR_PAD_LEFT);
		$image_folder = $data['idnum'] < 20000 ? "employees" : "students/".substr($idno,0,3);
		if ($this->role !=='student')
			return 'class="hoverme"  style="text-align: center; vertical-align:middle; position:relative"" data-idno="'.base_url('assets/img/'.$image_folder.'/' . $idno . ".jpg").'" ';
		else
			return ' style="text-align: center; vertical-align:middle" ';
	}

	function format_cell_name ($data){
		return ' style="text-align: left; vertical-align:middle" ';
	}
	
	
	function format_row($data){
		if ($data['status']=='Resolved') return 'class = "success" ';
	}
	
}


$feedback = new feedback();
$feedback->role =  $role;

$feedback->set_attributes(array('class'=>'table table-bordered','id'=>'feedback_list'));

$head_row = array(
		array('id'=>'id',
				"value"=>"Action",
				"attributes"=>array('id'=>'id', 'style'=>'width:5%; text-align:center; vertical-align:middle;')				
		),		
		array('id'=>'fbid',
				"value"=>"FB#",
				"attributes"=>array('fbid'=>'fbid', 'style'=>'width:5%; text-align:center; vertical-align:middle;')				
		),		
		array('id'=>'idnum',
				"value"=>"ID No.",
				"attributes"=>array('id'=>'idnum', 'style'=>'width:7%; text-align:center; vertical-align:middle;')
		),
		array("id"=>"name",
				"value"=> ($role !=='student' ? "Complainant" : " Recipient "),
				"attributes" => array('id'=>'name','style'=>'width:11%; text-align:center; vertical-align:middle;')
		),
		array("id"=>"comment",
				"value"=>"Comment",
				"attributes" => array('id'=>'comment', 'style'=>'width:40%; text-align:center; vertical-align:middle;')
		),
		array("id"=>"submitted",
				"value"=>"Updated",
				"attributes" => array('id'=>'submitted', 'style'=>'width:7%; text-align:center; vertical-align:middle;')
		), 
		array("id"=>"developers_remark",
				"value"=>"Response",
				"attributes" => array('id'=>'developers_remark', 'style'=>'width:28%; text-align:left; vertical-align:middle;')
		), 
		array("id"=>"status",
				"value"=>"Status",
				"attributes" => array('id'=>'status', 'style'=>'width:12%; text-align:center; vertical-align:middle;')
		), 
);

$feedback->insert_head_row($head_row);
$feedback->insert_data($data);

?>
<style>
.hover-image
{
overflow:hidden;
position: relative;
top: -20px;
left: 74px;
box-shadow: 1px 1px 12px -2px;
border-radius: 2px;
height: 64px;
width: 64px;
}

.input-tatsize 
{ 
width:300px; 
}
</style>

<h2 class="heading">Feedbacks</h2>
<div class="row-fluid">
	<form method="post">
		<?php echo $feedback->content() ?>
		<input type="submit" name="action" value="Delete" class="btn btn-danger">
		<input type="submit" name="action" value="Mark as Resolved" class="btn btn-primary">
		<input type="submit" name="action" value="Unresolve" class="btn btn-primary">
		<!-- <input type="submit" name="action" value="Developer's Remark" class="btn btn-primary"> -->		
		<button class="btn btn-primary" id="open_modal">Respond</button>
	</form>
	<div id="feedback_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="FeedbackModal" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    <h3 id="feedback_modal_label">Respond Here</h3>
		  </div>
		  <div class="modal-body" id ="modal-body">		  	
		  	<?php
		  		$this->form_lib->set_id('dev_feedback');
		  		$this->form_lib->set_attributes(array('class'=>'form-vertical', 'method'=>'post'));
			  	$this->form_lib->add_control_class('formSep');
			  	$this->form_lib->enqueue_hidden_input('nonce', $this->common->nonce());
			  	$this->form_lib->enqueue_hidden_input("id_to_update", "");
			  	$this->form_lib->enqueue_hidden_input("action", "Remark");
			  	$this->form_lib->enqueue_textarea(
			  			array( 
			  					"name"=>"dev_remark",
			  					"label"=>"Response Statement:",
								"rules"=>array("required","minlength:3"),
								"class"=>"input-xxlarge",
			  			)
			  	);		  	 
				$this->form_lib->set_submit_button('Submit','btn btn-primary');
				$this->form_lib->content(FALSE); 
		  	?>
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  </div>
	</div>	
</div>


<script>
	$().ready(function(){
		var oTable = $('#feedback_list').dataTable(
					{
						"iDisplayLength": 100,
					    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All",]],
				  	} 
				);
		oTable.fnSort( [ [7,'desc'],[5,'asc'] ] );

		$("#open_modal").on('click',function (e) {
			var num_of_checks = $(".checkboxes:checked").length;
			if (num_of_checks != 1){ 
				alert("Please tick one checkbox!");
				return false;
			}
			var id_to_update = 	$(".checkboxes:checked").attr("value");
			var remark_to_prepend = $(".checkboxes:checked").parents("tr").children("td.dev_remark").html();	
			//console.log(remark_to_prepend);	
			$('#dev_feedback input[name=id_to_update]').val(id_to_update);
			$('#dev_feedback textarea[name=dev_remark]').val(remark_to_prepend + "\n\n") ;			
			$('#feedback_modal').modal('show');
			e.preventDefault();			
		});

		$( ".hoverme" ).hover(
			//$id_to_hover = 	
			function() {
				
				$( this ).append( $('<img class="hover-image" src="'+$(this).data('idno')+'">') );
			}, 
			function() {
				$( this ).find( ".hover-image" ).remove();
			}
		);
		
	});
	
</script>