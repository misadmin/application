<?php //TODO: I'll rename this pos.php since this is the Point of Sale... 
$type = (isset($type) ? $type : '');
$latest_assessment = 0;
$running_balance = 0;
$name_on_receipt = $account->fullname;
?><div class="row-fluid span12">
	<div class="span5">
		<form class="form-vertical">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<fieldset>
				<div class="control-group formSep" >
					<label class="control-label" for="name_on_receipt">Name on Receipt:</label>
					<div class="controls">
						<input type="text" class="input-xlarge wide span12" id="name_on_receipt" name="name_on_receipt" placeholder="" value="<?php echo $name_on_receipt; ?>"/> 				
					</div>		
					<div class="controls">
						<select class="input-xlarge wide span12" id="teller_code" name="teller_code">
						<?php foreach($teller_codes as $code): ?>
							<?php $code_expanded = explode(" | ", $code);?>
							<option value="<?php echo $code ?>"><?php echo $code_expanded[1] ?></option>
						<?php endforeach; ?>
						</select>		
					</div>		
					<div class="controls">
						<input type="text" style="text-align:right" class="money input-xlarge wide span8" id="amount" name="amount" placeholder="Amount"/> 				
					</div>									
					<div class="controls">
						<textarea rows="2" class="text_input input-xlarge wide span12" id="remarks" name="remarks" placeholder="Remarks" style="height:45px" /></textarea> 				
					</div>									
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary pull-right" id="add_to_cart">Add to Cart</button>
					</div>
				</div>	
			</fieldset>
		</form>
	</div>
	<div id="cart" class="span7 pull-right">
		<table id="cart-content" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="5%"><input type="checkbox" id="select_all" /></th>
					<th width="15%">Code</th>
					<th width="60%">Description</th>
					<th width="20%">Amount</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3" style="text-align: right">Total</td>
					<td style="text-align: right"><strong id="total_amount">0.00</strong></td>
				</tr>
			</tfoot>
		</table>
		<p class="bs-docs-separator">
		<div class="row-fluid clearfix">
			<button id="delete_item" class="btn btn-danger" disabled="disabled">Delete</button>
			<button id="checkout" class="btn btn-success pull-right" data-toggle="modal" data-backdrop="false" data-target="#myModal" disabled="disabled" accesskey="A">Checkout (Alt+A)</button>
		</div>	
	</div>
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="myModalLabel">Checkout</h3>
	</div>
	<div class="modal-body">
		<h1 class="heading">Total Due:<span class="pull-right"><?php echo $this->config->item('currency_sign'); ?> <strong id="amount_needed"></strong></span></h1>
		<table id="payment-method" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="80%">Payment Method</th>
					<th width="20%">Amount</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			<tfoot>
				<tr>
					<td style="text-align: right">Total Amount Tendered</td>
					<td style="text-align: right" id="total_amount_tendered"></td>
				</tr>
				<tr>
					<td style="text-align: right; font-weight: bold;">Change Due</td>
					<td style="text-align: right" id="total_change"></td>
				</tr>
			<tfoot>
		</table>
		<div  class="tabbable" style="margin-top: 10px">
			<ul class="nav nav-tabs">
				<li class="active" ><a href="#cash" data-toggle="tab">Cash</a></li>
				<li ><a href="#check" data-toggle="tab">Check</a></li>
				<li ><a href="#creditcard" data-toggle="tab">Credit Card</a></li>
				<li ><a href="#debitcard" data-toggle="tab">Debit Card</a></li>
				<li ><a href="#bank" data-toggle="tab">Bank</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="cash">
					<div class="form-horizontal">
						<fieldset>
							<div class="control-group">
								<label for="amount_tendered_cash" class="control-label">Amount (Alt+C)</label>
								<div class="controls">
									<input autofocus type="text" style="text-align:right" class="money input-xlarge wide span4" id="amount_tendered_cash" name="amount_tendered_cash" accesskey="C" />
								</div>									
							</div>
							<div class="control-group">
								<div class="controls">
									<button id="cash_payment" class="btn btn-success">Add to Payment Method</button>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="tab-pane" id="check">
					<div class="form-horizontal">
						<fieldset>
							<div class="control-group">
								<label for="check_bank" class="control-label">Issuing Bank:</label>
								<div class="controls">
									<select name="bank_codes" id="check_bank_codes" class="bank">
										<option value="0">Select Bank</option>
										<?php foreach($bank_codes as $bank): $bank_exploded = explode(' | ', $bank); ?>
										<option value="<?php echo $bank; ?>"><?php echo $bank_exploded[1]; ?></option>									
										<?php endforeach; ?>
										<option value="undefined">Not on list</option>
									</select>
									<!--  input autofocus type="text" name="bank_codes" id="check_bank_codes"   /-->
								</div>
							</div>
							<div class="control-group" style="display:none;" id="new_bank_code_container"> 
								<label for="date_received" class="control-label">New Bank</label>
								<div class="controls">
									<input type="text" name="new_bank_code" id="new_bank_code" class="" />
								</div>
							</div>
							<div class="control-group">
								<label for="check_date" class="control-label">Check Date:</label>
								<div class="controls">
									<input type="text"  name="check_date" id="check_date" value=<?php echo date('m/d/Y') ?> />
								</div>
							</div>
							<div class="control-group">
								<label for="check_number" class="control-label">Check Number:</label>
								<div class="controls">
									<input type="text"  name="check_number" id="check_number"  />
								</div>
							</div>
							<div class="control-group">
								<label for="check_payee" class="control-label">Payee:</label>
								<div class="controls">
									<input type="text"  name="check_payee" id="check_payee" />
								</div>
							</div>
							<div class="control-group">
								<label for="check_amount" class="control-label">Check Amount:</label>
								<div class="controls">
									<input class="money" type="text"  name="check_amount" id="check_amount" />
								</div>
							<div class="control-group">
								<div class="controls">
									<button id="check_payment" class="btn btn-success">Add to Payment Method</button>
								</div>
							</div>									
						</fieldset>
					</div>				
				</div>
				<div class="tab-pane" id="creditcard">
					<div class="form-horizontal">
						<fieldset>
							<div class="control-group">
								<label for="credit_card_company" class="control-label">Company:</label>
								<div class="controls">
									<input autofocus type="text"  name="credit_card_company" id="credit_card_company">
								</div>
							</div>
							<div class="control-group">
								<label for="credit_card_number" class="control-label">Card Number:</label>
								<div class="controls">
									<input type="text" name="credit_card_number" id="credit_card_number">
								</div>
							</div>
							<div class="control-group">
								<label for="credit_card_owner" class="control-label">Card Owner:</label>
								<div class="controls">
									<input type="text" name="credit_card_owner" id="credit_card_owner">
								</div>
							</div>
							<div class="control-group">
								<label for="credit_card_expiry" class="control-label">Expiry:</label>
								<div class="controls">
									<input type="text"  name="credit_card_expiry" id="credit_card_expiry">
								</div>
							</div>
							<div class="control-group">
								<label for="credit_card_amount_deducted" class="control-label">Amount:</label>
								<div class="controls">
									<input class="money" type="text"  name="credit_card_amount_deducted" id="credit_card_amount_deducted" />
								</div>
							</div>
							<div class="control-group">
								<div class="controls">								
									<button id="credit_card_payment" class="btn btn-success">Add to Payment Method</button>
								</div>						
							</div>									
						</fieldset>
					</div>				
				</div>
				<div class="tab-pane" id="debitcard">
					<div class="form-horizontal">
						<fieldset>
							<div class="control-group">
								<label for="debit_card_company" class="control-label">Company:</label>
								<div class="controls">	
									<input autofocus type="text"  name="debit_card_company" id="debit_card_company" value="Pitakard" />
								</div>
							</div>
							<div class="control-group">
								<label for="debit_card_number" class="control-label">Card Number:</label>
								<div class="controls">
									<input type="text"  name="debit_card_number" id="debit_card_number" />
								</div>
							</div>	
							<div class="control-group">
								<label for="debit_card_owner" class="control-label">Card Owner:</label>
								<div class="controls">
									<input type="text"  name="debit_card_owner" id="debit_card_owner" />
								</div>
							</div>
							<div class="control-group">
								<label for="debit_card_expiry" class="control-label">Expiry Date:</label>
								<div class="controls">
									<input type="text"  name="debit_card_expiry" id="debit_card_expiry" />
								</div>
							</div>
							<div class="control-group">
								<label for="debit_card_amount_deducted" class="control-label">Amount:</label>
								<div class="controls">
									<input class="money" type="text"  name="debit_card_amount_deducted" id="debit_card_amount_deducted" />
								</div>
							</div>								
							<div class="control-group">
								<div class="controls">
									<button id="debit_card_payment" style="margin-left: 20px" class="btn btn-success">Add to Payment Method</button>
								</div>						
							</div>									
						</fieldset>
					</div>				
				</div>
				<div class="tab-pane" id="bank">
					<div class="form-horizontal">
						<fieldset>
							<div class="control-group">
								<label for="bank_name" class="control-label">Bank:</label>
								<div class="controls">
									<select name="bank_name" id="payment_bank_code" class="bank">
										<option value="0">Select Bank</option>
									<?php foreach($bank_codes as $bank): $bank_exploded = explode(' | ', $bank); ?>
									<option value="<?php echo $bank; ?>"><?php echo $bank_exploded[1]; ?></option>									
									<?php endforeach; ?>
									<option value="undefined">Not on list</option>
									</select>
									<!-- input autofocus type="text"  name="bank_name" id="payment_bank_codes" /-->
								</div>
							</div>
							<div class="control-group" style="display:none;" id="new_bank_container"> 
								<label for="date_received" class="control-label">New Bank</label>
								<div class="controls">
									<input type="text" name="new_bank" id="new_bank" class="" />
								</div>
							</div>
							<div class="control-group"> 
								<label for="date_received" class="control-label">Date Received:</label>
								<div class="controls">
									<input type="text"  name="date_received" id="date_received" value=<?php echo date('m/d/Y') ?> />
								</div>
							</div>
							<div class="control-group">
								<label for="depositor" class="control-label">Depositor:</label>
								<div class="controls">
									<input type="text"  name="depositor" id="depositor" >
								</div>
							</div>
							<div class="control-group">
								<label for="amount_received" class="control-label">Amount Received:</label>
								<div class="controls">
									<input class="money" type="text"  name="amount_received" id="amount_received">
								</div>
							</div>
							<div class="control-group">
								<div class="controls"> 
									<button id="bank_payment" style="margin-left: 20px" class="btn btn-success">Add to Payment Method</button>
								</div>						
							</div>									
						</fieldset>
					</div>				
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button id="carry_over" class="btn btn-success" disabled="disabled" accesskey="o">Carry Over (Alt+O)</button>
		<button id="final_checkout" class="btn btn-primary" disabled="disabled" accesskey="p">Check Out (Alt+P)</button>
	</div>
</div>
<form method="post" id="final_checkout_form">
	<input type="hidden" name="action" value="checkout" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<?php 
$has_carry_over = FALSE;
if ($payments = $this->session->userdata('payments')){
	$has_carry_over = TRUE;//this probably has a carry over...
	foreach($payments as $payment){
		if ($payment->type == 'cash')
			$has_carry_over = FALSE;
	}
}
?> 
<script>
	var current_change = <?php echo ($this->session->userdata('last_transaction_change') ? $this->session->userdata('last_transaction_change') : '0'); ?>;
	var teller_codes = <?php echo json_encode(($teller_codes)); ?>;
	var teller_codes_values = [];
	var bank_codes = <?php echo json_encode(($bank_codes)); ?>; 
	var bank_codes_values = []; 
	var item_count = 0;
	var total_amount = 0;
	var type_set = false;
	var items = [];
	var amount_tendered = 0;
	var validate_credit_card = true;
	var carry_over = <?php echo ($has_carry_over ? json_encode($this->session->userdata('payments')) : 'null'); ?>;
		
	$.each(teller_codes, function(index, value){
		teller_codes_values.push(value);
	});

	$.each(bank_codes, function(index, value){
		bank_codes_values.push(value);
	});
	
	
	var options, a;
	jQuery(function(){
	   options = { 
		   lookup: teller_codes_values,
	   };
	   a = $('#teller_type').autocomplete(options);
	});

	$(document).ready(function(){
		$('#credit_card_number').mask("9999-9999-9999-9999");  
		$('#debit_card_number').mask("9999-9999-9999-9999");  
		$("#credit_card_expiry").mask("99/99",{placeholder: "-" });
		$("#debit_card_expiry").mask("99/99",{placeholder: "-" });
		$("#check_date").mask("99/99/9999",{placeholder: "-" });
		$('#teller_code').focus();
	});

	$('#teller_type').bind('focusout', function(event){
		var teller_type = $('#teller_type').val().split(' | ');
		var amount = teller_type[3];
		
		if (teller_type[0] == 'CT'){
			amount = $('#tuition_payable').html();
			$('#remarks').val('Admit until: ');
		}
		$('#amount').val($.trim(amount));
	});

	$('#teller_code').on('change', function(){
		var teller_code_all = $(this).val().split(' | ');
		$('#amount').val(teller_code_all[3]);
	});
	
	//$(".money").keydown(function(event) {
	$(document).on("keydown",".money",function(event) {
	
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });

    $('#add_to_cart').on('click', function(event){
        event.preventDefault();
		var teller_type = $('#teller_code').val().split(' | ');
		var name_on_receipt = $('#name_on_receipt').val();
		var amount = parseFloat($('#amount').val().replace(',', ''));
		var remarks = $('#remarks').val().replace(/\n/g, "[newline]");
		
		if (teller_type[2] == undefined){
			alert ('Teller code you set is undefined. Please make corrections. Thank you');
		} else {
			if ($('#teller_type').val()=='' || $('#amount').val() == '' || amount == 0){
				alert ('Please set both Item and Amount. Thank you.');
			} else {
				var amount_str = $('#amount').val().split('.');
				//tatskie...
				if (typeof amount_str[1] != 'undefined' && amount_str[1].length > 2) {
					alert ('Please limit the number of digits to two.');
					$('#amount').focus();	
				} else {
					//a success is encountered after browser side validation
					total_amount = total_amount + amount;
					item_count = item_count + 1;					
				}
					
				$('#cart-content > tbody:last').append('<tr id="row' + item_count + '" name_on_receipt="' + name_on_receipt + '" teller_code="' + teller_type[2] + '" amount="' + amount + '" remarks="' + remarks + '"><td><input type="checkbox" class="item_box" value="' + item_count + '" /></td><td>' + teller_type[0] + '</td><td>' + teller_type[1] + '</td><td style="text-align:  right">' + toFixed(amount, 2) + '</td></tr>');
				$('#total_amount').html(toFixed(total_amount, 2));
				$('#teller_type').val('');
				$('#amount').val('');
				$('#remarks').val('');
				$('#amount_needed').html(toFixed(total_amount, 2));
				$('#checkout').removeAttr('disabled');
				$('#delete_item').removeAttr('disabled');
			}
		}
	});

	$('#select_all').bind('change', function(event){
		if ($(this).is(':checked')){
			$('.item_box').each(function(){
				$(this).attr('checked', 'checked');
			});
		} else {
			$('.item_box').each(function(){
				$(this).removeAttr('checked');
			});
		}
	});

	$('#delete_item').bind('click', function(){
		var confirmed = confirm("Are you sure you want to delete this item?");

		if (confirmed){
			$('.item_box:checked').each(function(){
				var item_id = parseInt($(this).parents('tr').attr('id').substring(3));
				var item_amount = parseInt($(this).parents('tr').attr('amount'));
				var item_info;
				var count = 0;
					
				total_amount = total_amount - item_amount;
				$('#total_amount').html(toFixed(total_amount, 2));
				$('#amount_needed').html(toFixed(total_amount, 2));
				$(this).parents('tr').remove();
				if ($('#cart-content tbody tr').length == 0){
					$('#delete_item').attr('disabled', 'disabled');
					$('#checkout').attr('disabled', 'disabled');
					$('#teller_type').removeAttr('disabled');
					$('#amount').removeAttr('disabled');
					$('#remarks').removeAttr('disabled');
					$('#add_to_cart').removeAttr('disabled');
					$('#teller_type').focus();
				}
			});
		}
	});

	$('#checkout').bind('click', function(){
		$('#total_change').html(toFixed((amount_tendered - total_amount), 2));
		if(current_change > 0 && carry_over!=null){
			$('#carry_over').removeAttr('disabled'); 
		}
	});

	$('#carry_over').bind('click', function(){
		var amount = current_change;
		amount_tendered = amount_tendered + amount;
		
		$('#payment-method > tbody:last').append('<tr type="carry_over" amount="' + amount + '"><td>Carry Over</td><td style="text-align:right">' + toFixed(amount, 2) + '</td></tr>');
		$('#total_amount_tendered').html(toFixed(amount_tendered, 2));
		$('#total_change').html(toFixed(amount_tendered - total_amount, 2));

		if ( (total_amount - amount_tendered) <= 0){
			$('#final_checkout').removeAttr('disabled');
		}
		$(this).attr('disabled', 'disabled'); 
	});

	$('#myModal').on('show', function(){
		
	});
	
	$('#cash_payment').bind('click', function(){

		if ($('#amount_tendered_cash').val() ==""){
			alert ("Please set an amount");
			$('#amount_tendered_cash').focus();
		} else {
			var amount_str = $('#amount').val().split('.');
			//tatskie...
			if (typeof amount_str[1] != 'undefined' && amount_str[1].length > 2) {
				alert ('Please limit the number of digits to two.');
				$('#amount').focus();	
			} else {
				var amount = parseFloat($('#amount_tendered_cash').val());
				amount_tendered = amount_tendered + amount;
				
				$('#payment-method > tbody:last').append('<tr type="cash" amount="' + amount + '"><td><i class="icon-remove remove-payment"></i> Cash</td><td style="text-align:right">' + toFixed(amount, 2) + '</td></tr>');
				$('#total_amount_tendered').html(toFixed(amount_tendered, 2));
				$('#total_change').html(toFixed(amount_tendered - total_amount, 2));
				$('#amount_tendered_cash').val("");

				if ( (total_amount - amount_tendered) <= 0){
					$('#final_checkout').removeClass('disabled');
					$('#final_checkout').removeAttr('disabled');
				} 
				$('#amount_tendered_cash').focus();
			}
		}
	});
	
	$(document).on('click', '#check_payment', function(){
		var check_payee = $('#check_payee').val();
		var check_date = $('#check_date').val();
		var bank_details = $('#check_bank_codes').val().split(' | ');
		var bank_id = bank_details[2];
		var check_number = $('#check_number').val();

		if ($('#check_bank_codes').val() == "0"){
			alert ("Please select bank.");
			$('#check_bank_codes').focus();
			return false;
		}
		if ($('#check_bank_codes').val() == "undefined" && $('#new_bank_code').val() ==""){
			alert ("Please specify a bank.");
			$('#new_bank_code').focus();
			return false;
		}
		if ($('#check_number').val() == ""){
			alert ("Please specify check number.");
			$('#check_number').focus();
			return false;
		}
		if ($('#check_payee').val() == ""){
			alert ("Please specify check payee.");
			$('#check_payee').focus();
			return false;
		}
		if ($('#check_amount').val() ==""){
			alert ("Please enter the check amount");
			$('#check_amount').focus();
			return false;
		}

		if($('#check_bank_codes').val() == "undefined"){
			bank_details = ['', $('#new_bank_code').val(), 'undefined'];
			bank_id = 'undefined';
		}
		if($('#check_bank_codes').val() != "undefined"){
			bank_details = $('#check_bank_codes').val().split(' | ');
			bank_id = bank_details[2];
		}
		console.log(bank_details);
		var amount_str = $('#amount').val().split('.');
		//tatskie...
		if (typeof amount_str[1] != 'undefined' && amount_str[1].length > 2) {
			alert ('Please limit the number of digits to two.');
			$('#check_amount').focus();	
		} else {
			var amount = parseFloat($('#check_amount').val());
			amount_tendered = amount_tendered + amount;
			
			$('#payment-method > tbody:last').append('<tr type="check" amount="' + amount + '" payee="'+ check_payee +'" bank_id="'+ bank_id + '" bank_name="'+ bank_details[1] + '" check_date="' + check_date + '" check_number="'+ check_number + '" ><td><i class="icon-remove remove-payment"></i> Check ('+ bank_details[1] +')</td><td style="text-align:right">' + toFixed(amount, 2) + '</td></tr>');
			$('#total_amount_tendered').html(toFixed(amount_tendered, 2));
			$('#total_change').html(toFixed(amount_tendered - total_amount, 2));
			$('#check_amount').val("");
			if ( (total_amount - amount_tendered) <= 0){
				$('#final_checkout').removeClass('disabled');
				$('#final_checkout').removeAttr('disabled');
			}
			$('#check_bank_codes').val("");
			$('#new_bank_code_container').hide();
			$('#check_number').val("");
			$('#check_payee').val(""); 
			$('#check_bank_codes').focus();
		}
	
	});
	$('#credit_card_number, #debit_card_number').focus(function(){
		$('#credit_card_error').remove();
	});
	$('#credit_card_number, #debit_card_number').blur(function(){
		if( ! valid_credit_card($(this).val().replace(/[^\d]/g, ''))){
			$(this).parent().append('<span id="credit_card_error" style="color:red;font-weight:bold">&nbsp;Possible Invalid Card No.</span>');
		} else {
			$('#credit_card_error').remove();
		}
	});
	
	$('#credit_card_payment').bind('click', function(){
		var credit_card_company = $('#credit_card_company').val();
		var credit_card_number = $('#credit_card_number').val();
		var credit_card_owner = $('#credit_card_owner').val();
		var credit_card_expiry = $('#credit_card_expiry').val();

		if ($('#credit_card_company').val() ==""){
			alert ("Please specify credit card company");
			$('#credit_card_company').focus();
			return false;
		}
		if ($('#credit_card_number').val() ==""){
			alert ("Please specify credit card number.");
			$('#credit_card_number').focus();
			return false;
		}
		/* if (validate_credit_card && $('#credit_card_number').val() !="" && ! valid_credit_card($('#credit_card_number').val().replace(/[^\d]/g, ''))){
			alert ("Please specify a valid credit card number.");
			$('#credit_card_number').focus();
			return false;
		} */
		if ($('#credit_card_owner').val() ==""){
			alert ("Please specify credit card owner");
			$('#credit_card_owner').focus();
			return false;
		}

		if ($('#credit_card_expiry').val() ==""){
			alert ("Please specify credit card expiration date");
			$('#credit_card_expiry').focus();
			return false;
		}
		
		if ($('#credit_card_amount_deducted').val() ==""){
			alert ("Please enter the amount");
			$('#credit_card_amount_deducted').focus();
			return false;
		}

		var amount_str = $('#credit_card_amount_deducted').val().split('.');
		//tatskie...
		if (typeof amount_str[1] != 'undefined' && amount_str[1].length > 2) {
			alert ('Please limit the number of digits to two.');
			$('#credit_card_amount_deducted').focus();	
		} else {
			var amount = parseFloat($('#credit_card_amount_deducted').val());
			amount_tendered = amount_tendered + amount;
			
			$('#payment-method > tbody:last').append('<tr type="credit_card" amount="' + amount + '" credit_card_company="'+ credit_card_company +'" credit_card_number="'+ credit_card_number + '" credit_card_owner="' + credit_card_owner + '" credit_card_expiry="'+ credit_card_expiry + '" ><td><i class="icon-remove remove-payment"></i> Credit Card</td><td style="text-align:right">' + toFixed(amount, 2) + '</td></tr>');
			$('#total_amount_tendered').html(toFixed(amount_tendered, 2));
			$('#total_change').html(toFixed(amount_tendered - total_amount, 2));
			$('#credit_card_amount_deducted').val("");
			$('#credit_card_company').val('');
			$('#credit_card_expiry').val('')
			$('#credit_card_owner').val('')
			$('#credit_card_number').val('');
			$('#credit_card_error').remove();
			
			if ( (total_amount - amount_tendered) <= 0){
				$('#final_checkout').removeAttr('disabled');
			} 
			$('#credit_card_company').focus();
		}
		
	});

	$('#debit_card_payment').bind('click', function(){
		var debit_card_company = $('#debit_card_company').val();
		var debit_card_number = $('#debit_card_number').val();
		var debit_card_owner = $('#debit_card_owner').val();
		var debit_card_expiry = $('#debit_card_expiry').val();
		
		if ($('#debit_card_company').val() == ""){
			alert ('Please specify the card company');
			$('#debit_card_company').focus();
			return false;
		}
		if ($('#debit_card_number').val() == ""){
			alert ('Please specify the debit card number.');
			$('#debit_card_number').focus();
			return false;
		}
		/* if ( validate_credit_card && $('#debit_card_number').val() !="" && ! valid_credit_card($('#debit_card_number').val().replace(/[^\d]/g, ''))){
			alert ('Please specify a valid debit card number.');
			$('#debit_card_number').focus();
			return false;
		}*/
		if ($('#debit_card_owner').val() ==""){
			alert ("Please enter the debit card owner");
			$('#debit_card_owner').focus();
			return false;
		}
		if ($('#debit_card_expiry').val() ==""){
			alert ("Please enter the debit card expiration date");
			$('#debit_card_expiry').focus();
			return false;
		}				
		if ($('#debit_card_amount_deducted').val() ==""){
			alert ("Please enter the debit card amount");
			$('#debit_card_amount_deducted').focus();
			return false;
		}
		var amount_str = $('#debit_card_amount_deducted').val().split('.');
		//tatskie...
		if (typeof amount_str[1] != 'undefined' && amount_str[1].length > 2) {
			alert ('Please limit the number of digits to two.');
			$('#debit_card_amount_deducted').focus();	
		} else {
			var amount = parseFloat($('#debit_card_amount_deducted').val());
			amount_tendered = amount_tendered + amount;
			
			$('#payment-method > tbody:last').append('<tr type="debit_card" amount="' + amount + '" debit_card_company="'+ debit_card_company +'" debit_card_number="'+ debit_card_number + '" debit_card_owner="' + debit_card_owner + '" debit_card_expiry="'+ debit_card_expiry + '" ><td><i class="icon-remove remove-payment"></i> Debit Card</td><td style="text-align:right">' + toFixed(amount, 2) + '</td></tr>');
			$('#total_amount_tendered').html(toFixed(amount_tendered, 2));
			$('#total_change').html(toFixed(amount_tendered - total_amount, 2));

			$('#debit_card_number').val('');
			$('#debit_card_company').val('');
			$('#debit_card_owner').val('');
			$('#debit_card_expiry').val('');
			$('#debit_card_amount_deducted').val("");
			$('#credit_card_error').remove();
			
			if ( (total_amount - amount_tendered) <= 0){
				$('#final_checkout').removeAttr('disabled');
			} 
			$('#debit_card_amount_deducted').focus();
		}
	});	

	$('#bank_payment').bind('click', function(){
		var bank_details;
		var bank_code;

		if($('#payment_bank_code').val() == '0'){
			alert('Please select bank');
			$('#payment_bank_code').focus();
			return false;
		}
		
		if($('#payment_bank_code').val() == 'undefined' && $('#new_bank').val() == ''){
			alert('Please fill in the bank name');
			$('#new_bank').focus();
			return false;
		}
		
		var date_received = $('#date_received').val();
		var depositor = $('#depositor').val();

		if (depositor==''){
			alert("Please specify depositor's name.");
			$('#depositor').focus();
			return false;
		}
		
		if ($('#amount_received').val() ==""){
			alert ("Please enter the bank payment amount");
			$('#amount_received').focus();
			return false;
		} 
		
		if($('#payment_bank_code').val() == 'undefined' && $('#new_bank').val() != ''){
			bank_details = ['', $('#new_bank').val(), 'undefined'];
			bank_code = 'undefined';	
		}
		
		if($('#payment_bank_code').val() != 'undefined'){
			bank_details = $('#payment_bank_code').val().split(' | ');
			bank_code = bank_details[2];
		}
		
		var amount_str = $('#amount_received').val().split('.');
		//tatskie...
		if (typeof amount_str[1] != 'undefined' && amount_str[1].length > 2) {
			alert ('Please limit the number of digits to two.');
			$('#amount_received').focus();
			return false;	
		}

		//Everything successfully passed...
		var amount = parseFloat($('#amount_received').val());
		amount_tendered = amount_tendered + amount;
		total_change = total_amount - amount_tendered;
		
		$('#payment-method > tbody:last').append('<tr type="bank_payment" amount="' + amount + '" bank_code="'+ bank_code +'" bank_name="'+bank_details[1]+'" date_received="'+ date_received + '" depositor="' + depositor + '"><td><i class="icon-remove remove-payment"></i> Bank Payment (' + bank_details[1] + ') </td><td style="text-align:right">' + toFixed(amount, 2) + '</td></tr>');
		$('#total_amount_tendered').html(toFixed(amount_tendered, 2));
		$('#total_change').html(toFixed(amount_tendered - total_amount, 2));
		$('#depositor').val("");
		$('#amount_received').val("");

		if ( (total_amount - amount_tendered) <= 0){
			$('#final_checkout').removeAttr('disabled');
		}
		
	});	

	$('#final_checkout').bind('click', function(){
		if ($(this).hasClass('disabled'))
			return false;
		var items = '[';
		var payments = '[';
		//process items:
		$('#cart-content tbody tr').each(function(){
			//json encode everything here...
			items = items + '{"teller_code":"' + $(this).attr('teller_code') + '","name_on_receipt":"' + $(this).attr('name_on_receipt') + '","amount":"' + $(this).attr('amount') + '","remarks":"' + $(this).attr('remarks').replace(/"|\\/g, "") + '"},';
		});
		items = items.substring(0, (items.length - 1)) + ']';

		//process payment methods:
		$('#payment-method tbody tr').each(function(){
			var attributes = []; 
			var row_values = [];
			var this_row = $(this);
			
			payments = payments + '{';

			switch ($(this).attr('type')){
				case 'carry_over' :
								attributes = ['type', 'amount'];
								break; 
				case 'cash'		:
								attributes = ['type', 'amount']; 
								break;
				case 'check'	:
								attributes = ['type', 'amount', 'bank_id', 'bank_name', 'check_date', 'payee', 'check_date', 'check_number'];
								break;
				case 'credit_card' :
								attributes = ['type', 'amount', 'credit_card_company', 'credit_card_number', 'credit_card_owner', 'credit_card_expiry'];
								break;
				case 'debit_card' :
								attributes = ['type', 'amount', 'debit_card_company', 'debit_card_number', 'debit_card_owner', 'debit_card_expiry'];
								break;
				case 'bank_payment' :
								attributes = ['type', 'amount', 'bank_code', 'bank_name', 'date_received', 'depositor'];
								break; 		
			}
			
			$.each(attributes, function(index, name){
				payments = payments + '"' + name + '":"' + this_row.attr(name).replace('"', '\"') + '",';  
			});
			payments = payments.substring(0, (payments.length - 1)) + '},'; // get rid of the trailing comma then add a };
		});
		
		payments = payments.substring(0, (payments.length - 1)) + ']';
		console.log ("payments: " + payments);

		var receipt_no = $('#receipt_no').html();
		
		$('<input>').attr({
			type: 'hidden',
			id: 'items',
			name: 'items',
			value: items
		}).appendTo('#final_checkout_form');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'payments',
			name: 'payments',
			value: payments
		}).appendTo('#final_checkout_form');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'receipt_no',
			name: 'receipt_no',
			value: receipt_no
		}).appendTo('#final_checkout_form');
		
		$('#final_checkout_form').submit();
		
	});

	$(document).on('change', '.bank', function(){
		if($(this).val()=='undefined'){
			switch($(this).attr('name')){
				case 'bank_codes' :
					$('#new_bank_code_container').show('slow');
					break;
				case 'bank_name' :
					$('#new_bank_container').show('slow');
					break;
			}		
		} else {
			switch($(this).attr('name')){
				case 'bank_codes' :
					$('#new_bank_code_container').hide('slow');
					break;
				case 'bank_name' :
					$('#new_bank_container').hide('slow');
					break;
			}
		}
	});

	$(document).on('click', '.remove-payment', function(){
		if($(this).hasClass('disabled'))
			return false;
		var amount = parseFloat($(this).parents('tr').attr('amount'));
		amount_tendered = amount_tendered - amount;
		total_change = amount_tendered - total_amount;
		$(this).addClass('disabled');		
		$(this).parents('tr').hide('slow', function(){
			$(this).remove();
		});
		$('#total_amount_tendered').html(toFixed(amount_tendered, 2));
		$('#total_change').html(toFixed(total_change,2));

		if ( (total_amount - amount_tendered) > 0){
			$('#final_checkout').addClass('disabled');
		}
	});
	
	function toFixed(value, precision) {
	    var power = Math.pow(10, precision || 0);
	   	var ret = String(Math.round(value * power) / power);
	   	var parts = ret.split(".");

	   	if (typeof parts[1] == 'undefined'){
			ret = ret + ".00";
	   	} else {
			while (parts[1].length < precision){
				parts[1] = parts[1] + '0';
				ret = ret + '0';
			}
	   	}
	   	return ret;
	}

	function valid_credit_card(input){
		//Luhn Algorithm...
		var sum = 0;
		var numdigits = input.length;
		var parity = numdigits % 2;
		for(var i=0; i < numdigits; i++) {
			var digit = parseInt(input.charAt(i))
			if(i % 2 == parity) digit *= 2;
			if(digit > 9) digit -= 9;
			sum += digit;
		}
		return (sum % 10) == 0;
		//return true;
	}		
</script>
<style>
#teller_code {background-color: #F9FFF9;border: 1px solid #8DB98D;height:50px;font-size:24px;}
</style>