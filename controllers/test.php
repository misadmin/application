<?php

class Test extends CI_Controller {
	/*this is added in comp3*/
	function __construct(){
		parent::__construct();
		$menu = array(
				'role'=>'dean',
				'name'=>'John Doe',
				'menu'=>array(
						'Student'=>'student',
						'Faculty'=>'faculty',
						'Pull Down'=>array('Here'=>'here'))
		);
		$this->content_lib->set_navbar_content('', $menu);
	}
	
	function index(){
		$data = array(
				'header'=>$this->load->view('pdf/includes/header', TRUE),);
		$this->load->view('pdf/test', $data);
	}
	
	public function prerequisites_of_prerequisite (){
		
		//201
		$this->load->model('hnumis/Prospectus_model');
		print_r ($this->Prospectus_model->all_courses_with_cascaded_prerequisites (207));
	}
	
	public function testing (){
		$this->load->model('col_students_model');
		print_r($this->col_students_model->student_grades_given_history_id('6234004', '188783'));
	}
	public function test_print(){
		$this->load->library('print_lib');
		$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
		$this->load->model('dric/students_model');
		$students_idno = '06129700';
		$student_info = $this->students_model->student_form9_information($students_idno);
		$requirements = $this->students_model->student_form9_requirements($students_idno);
		$prelim_educ = $this->students_model->student_form9_prelim_educ($students_idno);
		$meta = json_decode($student_info->meta);
		$dcourses = $this->students_model->student_form9_all_courses($students_idno);
		$lines = array();
		if ($dcourses){
			$term = '';
			foreach ($dcourses as $course){
				if($term != $course->academic_term . ", " . $course->school_year . ", " . $course->school){
					$term = $course->academic_term . ", " . $course->school_year . ", " . $course->school;
					$lines[] = (object)array('content'=>'', 'rating'=>'', 'credit'=>'', 'underlined'=>FALSE,'from_hnu'=>'', 'academic_term'=>'');
					$lines[] = (object)array('content'=>str_pad(substr($term, 0, 60), 60, " ", STR_PAD_RIGHT), 'rating'=>'', 'credit'=>'', 'underlined'=>TRUE,'from_hnu'=>'', 'academic_term'=>'');
				}
				if(strlen($course->course_code) > 15 || strlen($course->descriptive_title) > 44){
					$lines[] = (object)array('content'=>str_pad(substr($course->course_code, 0, 15), 15, ' ', STR_PAD_RIGHT) . " " . str_pad(substr(trim($course->descriptive_title), 0, 44), 44, ' ', STR_PAD_RIGHT), 'rating'=>$course->finals_grade, 'credit'=>$course->credit_units, 'underlined'=>FALSE,'from_hnu'=>($course->from_hnu=='1'), 'academic_term'=>$course->academic_term_id);
					$lines[] = (object)array('content'=>str_pad(substr(trim($course->course_code), 15, 15), 15, ' ', STR_PAD_RIGHT) . " " . str_pad(substr(trim($course->descriptive_title), 44, 44), 44, ' ', STR_PAD_RIGHT), 'rating'=>'', 'credit'=>'', 'underlined'=>FALSE, 'from_hnu'=>'', 'academic_term'=>'');
				} else {
					$lines[] = (object)array('content'=>str_pad(substr($course->course_code, 0, 15), 15, ' ', STR_PAD_RIGHT) . " " . str_pad(trim($course->descriptive_title), 44, ' ', STR_PAD_RIGHT), 'rating'=>$course->finals_grade, 'credit'=>$course->credit_units, 'underlined'=>FALSE, 'from_hnu'=>($course->from_hnu=='1'), 'academic_term'=>$course->academic_term_id);
				}
			}
		}
		$guardian_parent = isset($meta->family_info->fathers_name) && !empty($meta->family_info->fathers_name) ? $meta->family_info->fathers_name : (isset($meta->family_info->mothers_name) && ! empty($meta->family_info->mothers_name) ? $meta->family_info->mothers_name : (isset($meta->student_emergency_info->emergency_notify) ? $meta->student_emergency_info->emergency_notify : '')); 
		$this->content_lib->enqueue_body_content('print_templates/form9', 
				array(
						'student_info'=>$student_info,
						'requirements'=>$requirements->requirements,
						'elementary'=>$prelim_educ['elementary'],
						'highschool'=>$prelim_educ['highschool'],
						'guardian_parent'=>$guardian_parent,
						'date_of_graduation'=>'2014-03-26',
						'courses'=>$lines,
						'last_sem'=>$course->academic_term_id,
						));
		$this->content_lib->content();
	}
	
	public function test_dues(){
		$this->load->model('teller/teller_model');
		$this->load->model('academic_terms_model');
		$ledger_data= $this->teller_model->get_ledger_data(00053796);
		$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
		
		$this->load->library('balances_lib', 
					array(
						'ledger_data'=>$ledger_data, 
						'academic_term'=>$current_academic_terms_obj->id, 
						'period'=>''));
		//$this->load->library('balances_lib');
		$this->balances_lib->bad_debts();
		
		//$this->content_lib->enqueue_body_content('test_dues',array());
		//$this->content_lib->content();
		
	}
	
	function next_idno ($idno){
		$this->load->helper('student_helper');
		echo next_student_number($idno);
	}
	
	function suffix(){
		$this->load->helper('mis_helper');
		echo suffixize('ramon cimafrnca'); 
	}

	function l_first(){
		$this->load->helper('mis_helper');
		$word='melchor abc def ghi a. sy';
		//echo $word .'<br>'; 
		echo l_first($word);
	}
	
	function exploiters (){
		$enrolled_summer = $this->db->select('students.fname, students.mname, students.lname, students.idno, student_histories.inserted_on, student_histories.id')
			->from('student_histories')
			->join('students', 'students.idno=student_histories.students_idno')
			->group_by('students.idno')
			->order_by('students.lname')
			->where('student_histories.year_level', 1)
			->where('student_histories.can_enroll', 'Y')
			->where('student_histories.academic_terms_id', 456)->get()->result();
		$suspects = array();
		foreach ($enrolled_summer as $enrollee){
			$enrolled = $this->db->select('count(*) as enrollment_count', FALSE)
				->from('enrollments')
				->join('student_histories', 'student_histories.id=enrollments.student_history_id', 'left')
				->where('student_histories.students_idno', $enrollee->idno)
				->where('student_histories.academic_terms_id', 456)
				->group_by('student_histories.students_idno')
				->get();
			$enrolled_count = isset($enrolled->row()->enrollment_count) ? $enrolled->row()->enrollment_count : 0;
			//echo $enrollee->idno . " - " . $enrollee->lname . ", " . $enrollee->fname . " - " . $enrolled_count . "\n";
			//echo $enrolled_count > 0 ? $enrollee->idno . " - " . $enrollee->lname . ", " . $enrollee->fname . " - " . $enrolled_count . "\n" : "";
			if($enrolled_count > 0)
				$suspects[] = $enrollee;			
		}
		
		foreach ($suspects as $suspect){
			$enrolled = $this->db->select('count(*) as enrollment_count', FALSE)
				->from('enrollments')
				->join('student_histories', 'student_histories.id=enrollments.student_history_id', 'left')
				->where('student_histories.students_idno', $suspect->idno)
				->where('student_histories.academic_terms_id', 457)
				->group_by('student_histories.students_idno')
				->get();
			$enrolled_count = isset($enrolled->row()->enrollment_count) ? $enrolled->row()->enrollment_count : 0;
			if($enrolled_count > 0)
				echo $suspect->idno . " - " . $suspect->lname . ", " . $suspect->fname . " - " . $enrolled_count . "\n";
		}
	}
}