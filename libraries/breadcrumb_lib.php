<?php 

/**
 * Bread Crumb Library
 * 
 * Creates bread crumb from array items
 * @author HOLY NAME
 *
 */
class Breadcrumb_lib {
	
	/**
	 * Breadcrumb item buffer
	 * @var Array
	 */
	private $items;
	
	/**
	 * Creates Breadcrumb instance
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->load->library('breadcrumb_lib');
	 * </pre>
	 */
	public function __construct(){
		$this->items = array();
	}
	
	/**
	 * Add item to Breadcrumb
	 * <code>bool enqueue_item(String $id, String $label, String $link)</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->breadcrumb_lib->enqueue_item('ledger','Ledger','/student/ledger'));
	 * </pre>
	 * @param String $id
	 * @param String $label
	 * @param String $link
	 * @return Bool
	 */
	public function enqueue_item($id,$label,$link){
		if (empty($id) || empty($label))
			return FALSE;
		
		return $this->items[$id] = array('label'=>$label,'link'=>$link);
	}
	
	/**
	 * Remove Item from Breadcrumb
	 * <code>bool dequeue_item(String $id)</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->breadcrumb_lib->dequeue_item('ledger');
	 * </pre>
	 * @param String $id
	 */
	public function dequeue_item($id){
		if (isset($this->items[$id])){
			unset($this->items[$id]);
			return TRUE;
		}
		return FALSE;
	}
	
	/**
	 * Deletes all breadcrumb data
	 * <code>void clear_items([$include_home = TRUE])</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * //delete all items
	 * $this->breadcrumb->clear_items();
	 * //exclude home item
	 * $this->breadcrumb->clear_items(FALSE);
	 * </pre>
	 * @param unknown_type $include_home
	 */
	public function clear_items($include_home=TRUE){
		if ($include_home)
			$this->items = array();
		else{
			$home = reset($this->items);
			$this->items = array($home);
		}
	}
	
	/**
	 * Generate Breadcrumbs
	 * <code>String content([$return = TRUE])</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * //returns the breadcrumbs
	 * $this->breadcrumb->content();
	 * //prints the breadcrumbs
	 * $this->breadcrumb_lib->content(FALSE);
	 * </pre>
	 */
	public function content($return=TRUE){
		
		$br = '<ul class="breadcrumb">';
		
		//check last element of array
		end($this->items);
		$last = key($this->items);
		
		foreach ($this->items as $key => $val){
			//if last element
			if ($last == $key)
				$br .= '<li class="active">'.$val['label'].'</li>';
			else
				$br .= '<li><a href="'.$val['link'].'">'.$val['label'].'</a> <span class="divider">/</span></li>';
		}
		$br .= '</ul>';
		
		if (!$return){
			echo $br;
			return;
		}
		
		return $br;
	}
	
	/*
	<ul class="breadcrumb">
	<li><a href="./">Home</a> <span class="divider">/</span></li>
	<li class="active">A/R Running Balance</li>
	</ul>
	*/
}