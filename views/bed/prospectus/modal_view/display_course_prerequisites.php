
	<div style="overflow:auto; width:100%;" id="show_prerequisites_data">
		<table class="table table-hover table-bordered table-striped table-condensed" style="font-size:12px;">
			<thead>
				<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
					<td class="shs_header" style="width:15%;">Course<br>Code</td>
					<td class="shs_header" style="width:85%;">Descriptive Title</td>
				</tr>
			</thead>
			<tbody>
				<?php
					if ($prereqs) {
						foreach($prereqs AS $prereq_course) {
				?>
							<tr>	
								<td style="text-align:center;"><?php print($prereq_course->pre_req); ?></td>
								<td><?php print($prereq_course->descriptive_title); ?></td>
							</tr>
				<?php
						}
					}
				?>
			</tbody>
		</table>
	</div>	

	
