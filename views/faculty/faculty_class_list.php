<?php //print_r($prelim_grades); print_r($midterm_grades); print_r($final_grades); die()?>
<style>
.hover-image
{

position: relative;
top: -20px;
left: 70px;
box-shadow: 1px 1px 12px -2px;
border-radius: 2px;
height: 100px;
width: 120px;


}
</style>

<?php if (isset($message)): ?>
		<div class="alert alert-<?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $message; ?>
		</div>
<?php endif; ?>
<?php if ( !empty($descriptive_title)): ?>
<h3 class="heading">Course: <?php echo $descriptive_title . "({$course_code}) {$section_code}"; ?> <br> <?php print($time."  ".$days);?></h3>
<div class="float-right" style="text-align:left; padding:8px 0px;">
	<a id="download_class_sched" href="" course_description="<?php print('['.$course_code.'-'.$section_code.'] '.$descriptive_title); ?>" 
		course_offering_id="<?php print($course_offering_id); ?>" academic_terms_id="<?php print($academic_terms_id); ?>" >
		Download Class List to PDF<i class="icon-download-alt"></i></a>
</div>
<?php endif; ?>

<table class="table table-striped table-bordered mediaTable table-hover activeMediaTable" id="MediaTable-0">
	<form method="post" id="this_class_record" action="">
		<fieldset>
		<input type="hidden" name="action" value="post_grade" />
		<input type="hidden" name="period" id="period" value="" />
		<?php echo $this->common->hidden_input_nonce(); ?>
<?php if (isset($academic_terms_id)): ?>
		<input type="hidden" name="academic_terms_id" value="<?php echo $academic_terms_id; ?>" />
<?php endif; ?>
<?php if (isset($course_offering_id)): ?>
		<input type="hidden" name="course_offering_id" value="<?php echo $course_offering_id; ?>" />
<?php endif; ?>
		<thead>
			<tr>
				<th width="5%">No.</th>
				<th width="10%">Student ID</th>
				<th width="15%">Acad. Program</th>
				<th width="40%">Student Name</th>
				<?php if ( ! $prelim_grades_has_empty) { ?>
					<th width="10%">Prelim</a></th>
				<?php }else{?>	
					<th width="10%"><a id="prelim" class="grade_switch" href="javascript://" />Prelim</a></th>
				<?php }?>	
				
				<?php if( ! $midterm_grades_has_empty){?>
					<th width="10%">Midterms</a></th>
				<?php }else {?>	
					<th width="10%"><a id="midterm" class="grade_switch" href="javascript://" />Midterms</th>
				<?php }?>	
				<?php if( ! $finals_grades_has_empty){?>
					<th width="10%">Finals</a></th>
				<?php }else{?>	
					<th width="10%"><a id="finals" class="grade_switch" href="javascript://" />Finals</th>
				<?php } ?>	
			</tr>
		</thead>
		<tbody>
<?php if(is_array($results) && count($results) > 0): $count=0; ?>
<?php foreach($results as $result): $count++; 
	$idno 		= trim(str_pad($result->idno,8,'0',STR_PAD_LEFT));
	$image_folder = "students/".substr($idno,0,3);

?>
			<tr>
				<td style="text-align: right"><?php echo $count; ?></td>
				<td style="text-align: center"><a class="hoverme" data-idno="<?php echo base_url('assets/img/'.$image_folder.'/' . $idno . '.jpg'); ?>"> <?php echo trim($result->idno); ?></a></td>
				<td style="text-align: center"><?php echo $result->year_level . " " . $result->academic_program; ?></td>
				<td><?php echo $result->lname . ", " . $result->fname ." ". $result->mname; ?></td>
				<td style="text-align: center" class="prelim" for="<?php echo $result->id; ?>">
<?php if( empty($result->prelim_grade) || $result->prelim_grade=="-") : ?>
					<input type="text" style="width: 25px; margin-bottom:0px;"  class="prelim_grade" name="prelim[<?php echo $result->id; ?>]" />
<?php else: ?>		
					<?php echo $result->prelim_grade; ?>
<?php endif; ?>
				</td>
				<td style="text-align: center" class="midterm" for="<?php echo $result->id; ?>">
<?php if( empty($result->midterm_grade) || $result->midterm_grade=="-") : ?>
					<input type="text" style="width: 25px; margin-bottom:0px;" class="midterm_grade" name="midterm[<?php echo $result->id; ?>]"/>
<?php else: ?>					
					<?php echo $result->midterm_grade; ?>
<?php endif; ?>
				</td>
				<td style="text-align: center" class="finals" for="<?php echo $result->id; ?>">
<?php if( empty($result->finals_grade) || $result->finals_grade=="-") : ?>
					<input type="text" style="width: 25px; margin-bottom:0px;" class="finals_grade" name="finals[<?php echo $result->id; ?>]"/>

<?php else: ?>					
					<?php echo $result->finals_grade; ?>					
<?php endif; ?></td>
			</tr>
<?php endforeach; ?>
<?php else: ?>
			<tr>
				<td colspan="6">No Record Found</td>
			</tr>
<?php endif; ?>
			<tr>
				<td colspan="6"><input class="btn btn-inverse btn-submit pull-right" type="submit" id="submit_" value="Submit Grades" /></td>
			</tr>
		</tbody>
		</fieldset>
	</form>
</table>
<script>
var periods = <?php echo json_encode(array('prelim', 'midterm', 'finals')); ?>;
var grade_inputted = false;
var current_edit;

$(document).ready(function(){
	$('#submit_').attr('disabled', 'disabled');
		
	jQuery.each(periods, function(index, value){
		$('.' + value + '_grade').each(function(){
			$(this).hide();
		})
	});

	$('.grade_switch').live("click", function(){
		var period = $(this).attr('id');
		var confirmed = true;
		
		$('#period').val(period);
		
		if (grade_inputted && current_edit!=period){
			var confirmed = confirm('The current grades you inputted on this period will be lost.\nSuggest that you continue and submit this period.\nAre you sure you want to transfer to this period?');
		}

		if (confirmed){		 
			jQuery.each(periods, function(index, value){
				if (value != period){
					reset_forms(value);
				}	
			});
			$('.' + period + '_grade_current').hide();
			
			$('.' + period + '_grade').each(function(){
				$(this).show();
			});
			current_edit = period;
			grade_inputted = false;
			$('#submit_').attr('disabled', 'disabled');
		}
	});

	jQuery.each(periods, function(index, value){
		$('.' + value + '_grade').change(function(){
			grade_inputted = true;
			$('#submit_').removeAttr('disabled');
		});
	});
	
	$('#submit_').click(function(event){
		event.preventDefault();
		var no_err_found = no_error_exists();
		
		if ( ! no_err_found){
			//ERRORS ARE FOUND...
			alert ('Errors are found in the grades you inputted.\nPlease check and make corrections.'); //promote this to config...
		} else {
			console.log('errors not found');
			var continue_this = confirm('Are you sure you want to submit this?'); //promote this to config...

			if (continue_this){
				$('#this_class_record').submit();
			}
		}
	});
});

function ucfirst (str) {
	str += '';
	var f = str.charAt(0).toUpperCase();
	return f + str.substr(1);
}

function reset_forms (column){
	$('.' + column + '_grade').each(function(){
		$(this).val("");
		$(this).hide();
	});
}

function no_error_exists (){
	//the reason that this is a negative logic is:
	//we can't get out of a .each loop if it doesn't return FALSE... this is a big SIGH!
	var pattern = <?php echo $this->config->item('basic_grade_regex'); ?>; // if not finals
	var finals_pattern = <?php echo $this->config->item('finals_grade_regex'); ?>; //finals related
	var period = $('#period').val();
	var error_does_not_exist = true;
	var pattern_to_use = pattern;

	if (period == 'finals'){
		pattern_to_use = finals_pattern;
	}
		
	jQuery('.' + period + '_grade').each(function(){
		var grade = $(this).val();
		console.log(grade);
		
		if (pattern_to_use.test(grade)){
			console.log('no error');
			$(this).closest('td').removeClass("f_error");
		} else {
			console.log('error found');
			$(this).closest('td').addClass("f_error");
			$(this).focus();
			error_does_not_exist = false;
			return false;
		}
	});
	return error_does_not_exist;
}
</script>


	<form id="download_class_sched_form" method="post" target="_blank">
 		<input type="hidden" name="step" value="2" />
	</form>
	<script>
		$(document).ready(function(){
	  
			$('#download_class_sched').click(function(event){
				event.preventDefault(); 
				var course_description = $(this).attr('course_description');
				var course_offering_id = $(this).attr('course_offering_id');
				var academic_terms_id = $(this).attr('academic_terms_id');
				
				$('<input>').attr({
					type: 'hidden',
					name: 'course_description',
					value: course_description,
				}).appendTo('#download_class_sched_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'course_offering_id',
					value: course_offering_id,
				}).appendTo('#download_class_sched_form');
				$('<input>').attr({
					type: 'hidden',
					name: 'academic_terms_id',
					value: academic_terms_id,
				}).appendTo('#download_class_sched_form');
				
				$('#download_class_sched_form').submit();
	
			});

			
			$( ".hoverme" ).hover(
				function() {					
					$( this ).append( $('<img class="hover-image" src="'+$(this).data('idno')+'">') );
				}, 
				function() {
					$( this ).find( ".hover-image" ).remove();
				}
			);
			
			
		});
	</script>


