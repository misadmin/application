

<!-- 
	All Tuition Reports will now based on Posted Assessments! No more selection for Unposted to simplify query in Tuition Report Model. 
-->


			<h2 class="heading">Assessment Report - By College</h2>
			<form id="batch_posting_form" method="post" action="" class="form-horizontal">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="step" value="2" />
				
				<div class="control-group">
					<label class="control-label" for="academic_term">Academic Term&nbsp;&nbsp;:</label>
					<div class="controls ">
						 <select name="academic_terms_id" id="academic_terms_id">
							<?php
								foreach($academic_terms AS $academic_term) 
									{
										if ($current_term->id == $academic_term->id) {
											print("<option value=".$academic_term->id." selected>".$academic_term->term." ".$academic_term->sy."</option>");
										} else {
											print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");	
										}
									}
							?>
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="college">College&nbsp;&nbsp;:</label>
					<div class="controls ">
						<select name="colleges_id" id="colleges_id">
							<option value="0" selected="selected" >All Colleges (Except Grad. School)</option>
								<?php
									foreach($colleges AS $college)
										{
											print("<option value=\"$college->id\">$college->college_code</option>");
										}
								?>
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="yr_level">Year Level&nbsp;&nbsp;:</label>
					<div class="controls ">
						<select name="yr_level" id="yr_level">
							  <option value="0" selected="selected">All Year Levels</option>
							  <option value="1">1st Year</option>
							  <option value="2">2nd Year</option>
							  <option value="3">3rd Year</option>
							  <option value="4">4th Year</option>
							  <option value="5">5th Year</option>
						</select>
					</div>
				</div>
			<!--	<div class="control-group">
						<label class="control-label" for="post_status">Posted?&nbsp;&nbsp;:</label>
						<div class="controls" style="font-size:14px;">
							<input type="radio" name="post_status" value="Y" checked >Yes 
							<input type="radio" name="post_status" value="N" style="margin-left:15px; ">No
						</div>
				</div>		
			-->    
				<div class="control-group">
					 <div class="controls">
						<button class="btn btn-primary" name="generate_report_button" type="submit" id="batch_post">Generate Report!</button>
					</div>
				</div>
				
			</form>

<script>
$('#batch_post').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to generate Tuition Report?");

	if (confirmed){
		$('#batch_posting_form').submit();
	}		
});
</script>





  