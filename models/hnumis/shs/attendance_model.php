<?php

	class Attendance_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		
		function ListSchoolDays($academic_terms_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.id,
							DATE_FORMAT(a.school_month,'%M %Y') AS school_month,
							DATE_FORMAT(a.school_month,'%M') AS month,
							DATE_FORMAT(a.school_month,'%b') AS month_report,
							a.num_days
						FROM 
							school_days AS a
						WHERE 
							a.academic_terms_id = {$this->db->escape($academic_terms_id)} 					
						ORDER BY 
							a.school_month ";
			//return $sql;
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
			
		}


		function ListPresentDays($academic_terms_id=NULL, $student_histories_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.id,
							if (b.days_present,b.days_present,0) AS days_present,
							if (b.days_tardy,b.days_tardy,0) AS days_tardy
						FROM 
							school_days AS a 
								LEFT JOIN student_attendance AS b ON a.id = b.school_days_id AND b.student_histories_id = {$this->db->escape($student_histories_id)} 
						WHERE 
							a.academic_terms_id = {$this->db->escape($academic_terms_id)} 		
						ORDER BY 
							a.school_month ";
			//return $sql;
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
			
		}
		
		
		function AddSchoolDays($data) {
				
			$q1 = "INSERT INTO
							school_days (academic_terms_id,
										school_month,
										num_days,
										encoded_by)
						VALUES (
							{$this->db->escape($data['academic_terms_id'])},
							{$this->db->escape($data['school_month'])},
							{$this->db->escape($data['num_days'])},
							{$this->db->escape($data['encoded_by'])}) ";
			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
	    function DeleteSchoolDays($school_days_id) {
			
			$q = "DELETE FROM
						school_days 
					WHERE
						id = {$this->db->escape($school_days_id)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


		function UpdateSchoolDays($data){

			$q1 = "UPDATE school_days
					SET num_days = {$this->db->escape($data['num_days'])}
		 			WHERE
						id = {$this->db->escape($data['school_days_id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}


		function ListStudentsForAttendance($section_id=NULL, $school_days_id=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							a.idno,
							a.lname,
							a.fname,
							a.mname,
							a.gender,
							b.year_level,
							CONCAT(a.lname,', ',a.fname,' ',UPPER(LEFT(a.mname,1)),'.') AS name,
							(SELECT 
									num_days 
								FROM 
									school_days 
								WHERE 
									id = {$this->db->escape($school_days_id)}
							) AS num_days,
							(SELECT 
									days_present 
								FROM 
									student_attendance 
								WHERE 
									school_days_id = {$this->db->escape($school_days_id)}
									AND student_histories_id = b.id
							) AS days_present,
							(SELECT 
									days_tardy 
								FROM 
									student_attendance 
								WHERE 
									school_days_id = {$this->db->escape($school_days_id)}
									AND student_histories_id = b.id
							) AS days_tardy,
							b.id AS student_histories_id
						FROM 
							students AS a,
							student_histories AS b,
							block_sections AS c,
							shs_block_sections AS d 
						WHERE 
							 a.idno = b.students_idno
							 AND b.block_sections_id = c.id 
							 AND c.id = d.block_sections_id 
							 AND b.block_sections_id = {$this->db->escape($section_id)} 
						ORDER BY 
							a.gender,
							a.lname,
							a.fname,
							a.mname ";
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function SubmitAttendance($data) {

			$q1 = "INSERT INTO
							student_attendance (student_histories_id,
										school_days_id,
										days_present,
										days_tardy,
										encoded_by)
						VALUES (
							{$this->db->escape($data['student_histories_id'])},
							{$this->db->escape($data['school_days_id'])},
							{$this->db->escape($data['days_present'])},
							{$this->db->escape($data['days_tardy'])},
							{$this->db->escape($data['encoded_by'])}) 
						ON DUPLICATE KEY UPDATE 
							days_present = {$this->db->escape($data['days_present'])},
							days_tardy = {$this->db->escape($data['days_tardy'])},
							encoded_by = {$this->db->escape($data['encoded_by'])} ";
			//die($q1);
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		}


		function ListStudentAttendance($student_histories_id=NULL) {
			$result = null;
			
			/*$q1 = "SELECT 
							a.id,
							
			
			
					";
			
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			*/
			return $result;

		}

	}
	
?>	
