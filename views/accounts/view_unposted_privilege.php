<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
</style>

  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" class="head1" style="width:42%;">
      <tr class="head">
        <td colspan="3" class="head"> VIEW ASSIGNED PRIVILEGE</td>
      </tr>
    
            <td valign="middle">Scholarship/Privilge</td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print($privilege); ?></td>
           
		  </tr>
		
		 </tr>
            <td valign="middle">Privilege Discount %</td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print(number_format($tuition_percentage * 100,1)); ?></td>
           
		  </tr>
		    <tr>
		     <td colspan="3" valign="middle"><br></td>
	        </tr>
		    <tr>
		     <td colspan="3" valign="middle"><strong><u> Discounts</u></strong></td>
	        </tr>
	       
			<?php
			//$total = 0;
			 foreach($privileges_details AS $privilege) {
			?>
		  	 <tr>
		  			<?php   
		  			   if($privilege->fees_group_id == 9){ ?>
		  			          <td valign="middle" style="padding-left:15px;"><?php print('Tuition Fee');  ?></td>
		  			<?php }else{       
		  			?>          
		  			   <td valign="middle" style="padding-left:15px;"><?php print($privilege->fees_group);  ?></td>
		  			<?php }?>   
		     		   <td valign="middle">:</td>
		     		   <td align="right" valign="middle" style="padding-right:15px;"><?php print(number_format($privilege->discount_amount,2)); 
					  // $total = $total + $privilege->discount_amount;
						?></td>
	        </tr>
		<?php
			}
		?>
		   <tr style="font-weight:bold;">
		     <td align="right" valign="middle" style="padding-left:15px;">TOTAL</td>
		     <td valign="middle">:</td>
		     <td align="right" valign="middle"  style="font-weight:bold;"><?php print(number_format($total_disc_amount,2));  ?></td>
	      </tr>
	       <tr>
  			<td align ="center"><input type="submit" name="delete" id="delete" value="Delete!" /></td>
		   </tr>
        </table></td>
      </tr>
    </table>
  </div>
  
    <form id="delete_privilege_form" method="post">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value= "delete_unposted_privilege_form"> 
		<input type="hidden" name="privilege_id" value="<?php print($privilege_id);?>" />
   </form>

<script>
$('#delete').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to delete privilege of student?");

	if (confirmed){
		$('#delete_privilege_form').submit();
	}		
});
</script>

