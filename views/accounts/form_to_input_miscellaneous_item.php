<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

<form action="<?php echo site_url('accounts/add_misc_item');?>" method="post">
  <?php echo validation_errors(); 
  ?>
  <input type="hidden" name="step" value="2" />
  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" style="width:50%; margin-top:35px;" class="head1">
      <tr class="head">
        <td colspan="3" class="head"> ADD MISCELLANEOUS ITEM </td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="2" cellspacing="0" class="inside">
		    <tr>
            <td width="199" align="left" valign="middle">Existing Items </td>
            <td width="15" align="left" valign="middle">:</td>
            <td width="438" align="left" valign="middle"><select name="misc_item_id" id="misc_item_id" class="form">
             <?php
					foreach($miscellaneous_items AS $misc) {
						print("<option value=".$misc->id.">".$misc->description."</option>");	
					}
				?>
                        </select></td>
          </tr>
		  
          <tr>
            <td>New Item Name </td>
            <td>:</td>
            <td><input name="item_name" type="text" id="item_name" size="10" maxlength="10" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="button2" id="button2" value="Save New Miscellaneous Item!"  class="btn btn-success" /></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</form>