<?php if (isset($terms) && count($terms) > 0): ?>
<?php foreach($terms as $term): ?>
<h4 style="margin-top: 20px;"><?php echo $term['school_year']; ?> <?php echo $term['term']; ?> (<?php echo $term['program'] . " " . $term['year_level']; ?>)</h4>
<table class="table table-bordered mediaTable activeMediaTable" id="MediaTable-0">
	<thead>
		<tr>
			<th width="14%">Catalog Number</th>
			<th width="38%">Descriptive Title</th>
			<th width="6%">Prelim</th>
			<th width="6%">Midterm</th>
			<th width="6%">Finals</th>
			<th width="20%">Teacher</th>
			<th width="10%">Credit Units</th>
		</tr>
	</thead>
	<tbody>
<?php if(isset($term['courses']) && count($term['courses']) > 0): ?>
<?php
	foreach($term['courses'] as $subject): ?>
		<tr <?php //if ($subject['course']->finals_grade > 3.0 || ! is_numeric ($subject['course']->finals_grade)) echo 'class="error" '; ?>>
			<td><?php echo $subject['course']->course_code; ?></td>
			<td><?php echo $subject['course']->descriptive_title; ?></td>
			<td style="text-align:center;"> <a class="edit_grade" href="<?php print($idnum);?>" enrollment_id="<?php print($subject['course']->enrollment_id);?>" period='P'> <?php echo $subject['course']->prelim_grade;?> </a></td>
			<td style="text-align:center;"> <a class="edit_grade" href="<?php print($idnum);?>" enrollment_id="<?php print($subject['course']->enrollment_id);?>" period = 'M'><?php echo $subject['course']->midterm_grade; ?></a></td>
			<td style="text-align:center;"> <a class="edit_grade" href="<?php print($idnum);?>" enrollment_id="<?php print($subject['course']->enrollment_id);?>" period = 'F'><?php echo $subject['course']->finals_grade; ?></td>
			<td><?php echo $subject['course']->instructor; ?></td>
			<td style="text-align:center;"><?php echo $subject['course']->credit_units; ?></td>
		</tr>
<?php endforeach; ?>
<?php endif; ?>
	</tbody>
</table>
<?php endforeach; ?>
<?php else: ?>
	<h3>No Records Found</h3>
<?php endif;?>

  <form id="edit_prelim_grade" method="post" action="" >
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value="open_edit_grade_form"/>
 </form>

<script>
	$('.edit_grade').click(function(event){
		event.preventDefault(); 
		var idnum = $(this).attr('href');
		var enrollment_id = $(this).attr('enrollment_id');
		var period = $(this).attr('period');
					
			$('<input>').attr({
				type: 'hidden',
				name: 'idnum',
				value: idnum,
			}).appendTo('#edit_prelim_grade');	
			$('<input>').attr({
				type: 'hidden',
				name: 'enrollment_id',
				value: enrollment_id,
			}).appendTo('#edit_prelim_grade');	
			$('<input>').attr({
				type: 'hidden',
				name: 'period',
				value: period,
			}).appendTo('#edit_prelim_grade');	
			$('#edit_prelim_grade').submit();
		}
		
	});
</script>
