<?php
  
	class Courses_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function getCourse($course_id) 
		{
			if($course_id == null) { return null; }
			
			$result = null;
			
			$query = $this->db->query("SELECT * 
										FROM
											courses 
										WHERE
											id={$this->db->escape($course_id)}");
						
			
			if($query && $query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;	
			}
						
		}
		
		function getCourseType($course_id) 
		{
			if($course_id == null) { return null; }
			
			$result = null;
			
			$query = $this->db->query("SELECT 
										a.course_type
								      FROM
										course_types as a,
										courses as b
									  WHERE
											b.id={$this->db->escape($course_id)} 
											AND b.course_types_id = a.id");
			
			if($query && $query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;	
			}
						
		}
		
	function getCourseGroup($course_id) 
		{
			if($course_id == null) { return null; }
			
			$result = null;
			
			$query = $this->db->query("SELECT 
										a.group_name
								      FROM
										courses_groups as a,
										courses as b
									  WHERE
											b.id={$this->db->escape($course_id)} 
											AND b.courses_groups_id = a.id");
			
			if($query && $query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;	
			}
						
		}
		
		//Added: Feb. 14, 2013 by Amie
		function getCourseDetails($course_id) 
		{
			if($course_id == null) { return null; }
			
			$result = null;
			
			$query = $this->db->query("
					SELECT * 
					FROM
						courses  
					WHERE
						id={$this->db->escape($course_id)} 
				");
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
						
		}
		
		//Added: Feb. 9, 2013 by Amie
		function getProspectusCourse($course_id) 
		{
			if($course_id == null) { return null; }
			
			$result = null;
			
			$query = $this->db->query("SELECT pc.id, c.course_code, c.descriptive_title, c.credit_units 
										FROM
											courses c, prospectus_courses pc
										WHERE
											pc.courses_id = c.id
											AND c.id = {$this->db->escape($course_id)} ");
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;		
		}
		
		function RetrieveCourse($course_id) 
		{
			if($course_id == null) { return null; }
			
			$result = null;
			
			$query = $this->db->query("
					SELECT *
					FROM 
						courses 
					WHERE
						id={$this->db->escape($course_id)} 
				");
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}

			return $result;
						
		}
		

/*		function ListCourseTypes() 
		{
			$result = null;
			
			$query = $this->db->query("SELECT * 
										FROM 
											course_type 
										ORDER BY
											course_type");
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}*/

		function ListCourseGroups() 
		{
			$result = null;
			
			$query = $this->db->query("SELECT * 
										FROM 
											courses_groups 
										ORDER BY
											group_name ");
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		//please check
		//input argument is not used in the query
		//justin
/* 		function AddCourse($course_data){			
			$sql = "
					insert into {$this->table} 
						(name, `desc`) 
					values (
						'".$role_data['name']."',
						'".$role_data['desc']."'
					)
				";			
			$this->db->query($sql);			
		}
 */
		//UPDATED 3/8/2013 By: Genes
		function ListCourses($owner_colleges_id,$status=NULL) {
			$result = null;
			
			/*$q2 = "
				(SELECT 
						DISTINCT(a.course_code) AS course_code,
						a.id,
						a.descriptive_title,
						a.paying_units,
						a.credit_units
					FROM
						courses AS a
					WHERE
						a.owner_colleges_id={$this->db->escape($owner_colleges_id)} 
				)
				
				UNION
					
				(SELECT 
						DISTINCT(a.course_code) AS course_code,
						a.id,
						a.descriptive_title,
						a.paying_units,
						a.credit_units
					FROM
						courses AS a, 
						elective_courses AS b
					WHERE
						a.id=b.topic_courses_id
						AND	a.owner_colleges_id={$this->db->escape($owner_colleges_id)} 
				)
						 
				ORDER BY
					course_code ";
			*/
				$q3="
				select 	course_code,tmp.id,descriptive_title,paying_units,credit_units,course_types_id,ct.type_description
				from
					(
						(SELECT
						DISTINCT(a.course_code) AS course_code,a.id,a.descriptive_title,a.paying_units,a.credit_units,a.course_types_id
						FROM
						courses AS a
						WHERE
						a.owner_colleges_id={$this->db->escape($owner_colleges_id)} )
					UNION
						(SELECT
						DISTINCT(a.course_code) AS course_code,
						a.id,
						a.descriptive_title,
						a.paying_units,
						a.credit_units,
						a.course_types_id
						FROM
						courses AS a,
						elective_courses AS b
						WHERE
						a.id=b.topic_courses_id
						AND	a.owner_colleges_id={$this->db->escape($owner_colleges_id)} )
					) as tmp
				join course_types ct on ct.id =  tmp.course_types_id
				ORDER BY 	course_code				
				";
				
			//log_message("INFO", print_r($q3) ); //toyet 4.5.2018

			//return $q2;								
			$query = $this->db->query($q3);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
		}
		

	/*
	 * @author: genes 6/20/14
	 * 
	 */	
	function ListCollegeCourses($owner_colleges_id)
		{
			$result = null;
				
			$q2 = "SELECT
							a.id,
							a.course_code,
							a.course_types_id,
							a.courses_groups_id,
							a.descriptive_title,
							a.paying_units,
							a.credit_units,
							a.is_service_course,
							a.status
						FROM
							courses AS a
						WHERE
							a.owner_colleges_id={$this->db->escape($owner_colleges_id)} 
						ORDER BY
							a.course_code";
			
			$query = $this->db->query($q2);
						
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
						
			return $result;
	}
					
						
		
		/**
		 * Retrieves courses given academic terms id and program id
		 * 
		 * @param int $academic_terms_id
		 * @param int $program_id
		 */
		public function courses_academic_terms_program($academic_terms_id, $colleges_id){
			$sql = "
				SELECT 
					co . * , 
					CONCAT(e.lname,  ', ', e.fname) AS fullname, 
					CONCAT(cos.start_time, '-', cos.end_time, ' ', cos.days_day_code) as schedule,
					course_code
				FROM
					course_offerings co
				LEFT JOIN 
					employees e 
				ON 
					e.empno = co.employees_empno
				LEFT JOIN 
					courses cs 
				ON 
					cs.id = co.courses_id
				LEFT JOIN 
					prospectus_courses pc 
				ON 
					pc.courses_id = cs.id
				LEFT JOIN 
					prospectus_terms pt 
				ON 
					pt.id = pc.prospectus_terms_id
				LEFT JOIN 
					prospectus pr 
				ON 
					pt.prospectus_id = pr.id
				LEFT JOIN 
					academic_terms at 
				ON 
					at.id = co.academic_terms_id
				LEFT JOIN
					academic_years ay 
				ON 
					at.academic_years_id = ay.id
				RIGHT JOIN
					course_offerings_slots cos 
				ON 
					cos.course_offerings_id=co.id
				WHERE 
					at.id ={$this->db->escape($academic_terms_id)}
				AND
					cs.owner_colleges_id={$this->db->escape($colleges_id)}
				GROUP BY cos.id
				ORDER BY course_code, section_code
				";
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0)
				return $query->result(); else
				return FALSE;
		}
		

		/**
		 * Provides the courses of a faculty given his/her employee id and the academic term id.
		 * Added by Justin
		 * EDITED by genes
		 * includes no. of enrollees, rm no, bldg
		 */
		public function faculty_courses_academic_terms($employee_id, $academic_term){
			//TODO: explicitly enumerate *		
			$sql = "
				SELECT
					CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term,
					co.id as course_offerings_id, co.`section_code`, co.`max_students`,
					c.*, COUNT(en.id) AS num_enrollees, rm.room_no, bldg.bldg_name,
					CONCAT(cos.start_time, '-', cos.end_time, ' ', cos.days_day_code) as schedule
				FROM
					course_offerings co
				LEFT JOIN
					academic_terms at
				ON
					co.academic_terms_id=at.id
				LEFT JOIN
					courses c
				ON
					co.courses_id=c.id
				LEFT JOIN
					course_offerings_slots cos
				ON
					cos.course_offerings_id=co.id
				LEFT JOIN
					academic_years ay
				ON
					ay.id=at.academic_years_id
				LEFT JOIN 
					enrollments AS en
				ON
					en.course_offerings_id=co.id
				LEFT JOIN 
					rooms AS rm
				ON 
					rm.id=cos.rooms_id
				LEFT JOIN
					buildings AS bldg
				ON
					bldg.id=rm.buildings_id
				WHERE
					co.employees_empno={$this->db->escape($employee_id)}
				AND
					at.id={$this->db->escape($academic_term)}
				AND co.id NOT IN (SELECT 
										a.course_offerings_id 
									FROM 
										block_course_offerings AS a,
										shs_block_sections AS b 
									WHERE 
										a.block_sections_id = b.block_sections_id 
								)
				GROUP BY
					co.id
				ORDER BY
					cos.days_day_code, cos.start_time";
			
			
			$query = $this->db->query($sql);
		
			if($query->num_rows() > 0)
				return $query->result(); else
				return FALSE;
		}

		
		/**
		 * Returns course information from course offering id...
		 * 
		 * @param unknown_type $course_offerings_id
		 * @return boolean
		 */
		public function course_info_from_course_offering_id ($course_offerings_id){
			// todo: if enrollments table is complete... we can delete this method.
			//for now, lets use this one...
			$sql = "SELECT
						*
					FROM
						courses c
					LEFT JOIN
						course_offerings co
					ON
						c.id=co.courses_id
					WHERE
						co.id={$this->db->escape($course_offerings_id)}
					LIMIT
						1
					";
			$query = $this->db->query($sql);
			
			if ($query->num_rows() > 0)
				return $query->row(); else
				return FALSE;
		}
		
		//EDITED: 10/28/13 by genes
		//ADDED: prospectus_courses so that we will know if course is bracketed
		public function student_courses_from_academic_terms ($student_id, $academic_terms_id) {
			$sql = "
				SELECT
					DISTINCT co.id, co.section_code, e.id as enroll_id, e.status, r.room_no, b.bldg_name, 
					sl.days_day_code, DATE_FORMAT(sl.start_time, '%h:%i') as start_time, DATE_FORMAT(sl.end_time, '%h:%i %p') as end_time,
					c.course_code, c.descriptive_title, 
					if(!isnull(eo.id), eo.credit_units, c.credit_units) as credit_units,					
					if(!isnull(eo.id), eo.paying_units, c.paying_units) as paying_units,										
					cap_first(CONCAT_ws(', ',emp.lname,emp.fname)) AS instructor, re_enroll.enrollments_id,
					if(!isnull(eo.id), eo.is_bracketed, ifnull(pc.is_bracketed,'N')) AS is_bracketed,					
					IF(ISNULL(e.enrolled_by), '$student_id', e.enrolled_by) as enrollee_id, 
					e.date_enrolled, 
					IF(ISNULL(e.withdrawn_on),'', e.withdrawn_on) as date_withdrawn,
					IF(ISNULL(e.withdrawn_by),'', e.withdrawn_by) as withdrawn_by
				FROM enrollments e
					LEFT JOIN student_histories sh ON sh.id=e.student_history_id
					LEFT JOIN course_offerings co ON co.id=e.course_offerings_id
					join course_offerings_slots sl on sl.course_offerings_id = co.id
					LEFT JOIN courses c ON	c.id=co.courses_id
					left join prospectus_courses pc on pc.courses_id = c.id
					LEFT JOIN academic_terms at ON at.id=sh.academic_terms_id
					LEFT JOIN academic_years ay ON ay.id=at.academic_years_id
					LEFT JOIN employees emp ON	emp.empno=co.employees_empno
					LEFT JOIN prospectus p ON p.id=sh.prospectus_id
					LEFT JOIN academic_programs ap ON p.academic_programs_id=ap.id
					left join rooms r on r.id=sl.rooms_id
					LEFT JOIN buildings b ON r.buildings_id=b.id
					LEFT JOIN re_enrollments as re_enroll ON re_enroll.enrollments_id = e.id
					LEFT JOIN enrollments_override eo ON eo.enrollments_id=e.id					
				WHERE 
					sh.students_idno='{$student_id}'
					AND sh.academic_terms_id='{$academic_terms_id}'
				ORDER BY	
					days_day_code, DATE_FORMAT(sl.start_time, '%H'), course_code					
				";
			
			/**				FROM
			 prospectus_terms AS pt,
			 prospectus_courses AS pc,
			 enrollments e
			 LEFT JOIN course_offerings co ON e.course_offerings_id=co.id
			 LEFT JOIN courses c ON co.courses_id=c.id
			 JOIN course_offerings_slots cos ON cos.course_offerings_id=co.id
			 LEFT JOIN student_histories sh ON e.student_history_id=sh.id
			 LEFT JOIN rooms r ON r.id=cos.rooms_id
			 LEFT JOIN buildings b ON r.buildings_id=b.id
			 LEFT JOIN employees emp ON emp.empno=co.employees_empno
			 LEFT JOIN re_enrollments as re_enroll ON re_enroll.enrollments_id = e.id
			 WHERE
			 sh.students_idno='{$student_id}'
			 AND sh.academic_terms_id='{$academic_terms_id}'
			 AND pt.prospectus_id = sh.prospectus_id
			 AND pc.prospectus_terms_id = pt.id
			 AND pc.courses_id = c.id
			 ORDER BY
			 days_day_code,start_time,course_code
			 **/
					
			//log_message("INFO", print_r($sql, true)); // Toyet 5.24.2018
			//print($sql);die();
			$query = $this->db->query($sql);
			//print_r($query);die();
			if ($query && $query->num_rows() > 0){
				//We're going to resolve here the time schedule because of separate time slots...
				//and return the result as an associative array...
				$courses = array();
				foreach ($query->result() as $row) {
					
					if (array_key_exists($row->id, $courses)) {
						//Lets append the slot to the schedule...
						$courses[$row->id]['schedule'] .= "\n" .$row->bldg_name . " " . $row->room_no. " " . $row->days_day_code . " " . $row->start_time . "-" . $row->end_time; 
					} else {
						$courses[$row->id] = array(
										'id'				=>$row->id,
										'status'			=>$row->status,
                                        'enroll_id'			=>$row->enroll_id,								
										'course_code'		=>$row->course_code,
										'section_code'		=>$row->section_code,
										'descriptive_title'	=>$row->descriptive_title,
										/*'schedule' 			=>$row->bldg_name. " " .$row->room_no. " " . $row->days_day_code . " " . $row->start_time . "-" . $row->end_time,*/
										'schedule' 			=>$row->days_day_code." ".$row->start_time . "-" . $row->end_time . " ". $row->room_no ,
										'time' 				=>$row->start_time . "-" . $row->end_time." ". $row->days_day_code ,
										'room' 				=>$row->room_no,
										'credit_units'		=>$row->credit_units,
										'instructor'		=>$row->instructor,
										'enrollments_id'    =>$row->enrollments_id,	
										'is_bracketed'		=>$row->is_bracketed,
										'paying_units'		=>$row->paying_units,
										'enrollee_id'		=>$row->enrollee_id,
										'date_enrolled'		=>$row->date_enrolled,
										'date_withdrawn'	=>$row->date_withdrawn,
										'withdrawn_by'		=>$row->withdrawn_by,
								);
					}
				}
			
				return $courses;				
			} else {
				return FALSE;
			}
			
		}

		/***************************************************************************/
		//NOTE: Added 1/1/2013
		/***************************************************************************/
		function ListAllCourses() {
				
			$result = null;
				
			$q1 = "SELECT
						a.id, a.course_code,a.credit_units, a.descriptive_title, a.paying_units
					FROM
						courses AS a
					ORDER BY
						a.course_code " ;
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
		
	function ListCourseTypes() {
				
			$result = null;
				
			$q1 = "SELECT
						a.id, a.type_description
					FROM
						course_types AS a
					WHERE
					    a.id = 8 || a.id = 11
					ORDER BY
						a.type_description" ;
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
		
	//Added: 3/2/2013
		function ListAllCoursesLab() {
				
			$result = null;
				
			$q1 = "SELECT
						a.id, a.course_code
					FROM
						courses AS a
					WHERE
						a.course_types_id = 11
					ORDER BY
						a.course_code" ;
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
		
		//Added by Justing...
		public function list_courses_with_limit ($limit, $academic_term, $status='all', $page=1, $count=50){
			$start = ($page - 1) * $count;
			$sql = "
				SELECT
				SQL_CALC_FOUND_ROWS
					c.course_code, co.id as course_offerings_id, co.section_code, c.descriptive_title, c.credit_units,
					CONCAT(r.room_no, ' ', b.bldg_name) as room, 
					GROUP_CONCAT(DISTINCT CONCAT(cos.start_time, '-', cos.end_time, ' ', cos.days_day_code) SEPARATOR '<br />' ) as schedule,
					IF(ISNULL(co.id), 0, co.max_students) as max_enrollment_count,
					IF(ISNULL(co.id), 0, COUNT(DISTINCT en.id)) as enrolled_count, 
					IF(ISNULL(e.lname), '', CONCAT(e.lname, ', ', e.fname)) as teacher,
					IF (co.status='D', 'Dissolved', 'Active') as status  
				FROM 
					courses c 
				LEFT JOIN 
					course_offerings co 
				ON 
					c.id=co.courses_id 
				LEFT JOIN 
					course_offerings_slots cos 
				ON 
					co.id=cos.course_offerings_id 
				LEFT JOIN 
					enrollments en 
				ON 
					en.course_offerings_id=co.id 
				LEFT JOIN 
					rooms r 
				ON 
					r.id=cos.rooms_id 
				LEFT JOIN 
					buildings b 
				ON 
					r.buildings_id=b.id 
				LEFT JOIN 
					employees e 
				ON 
					e.empno=co.employees_empno
				";
			
			if ( ! empty($limit['program'])) {
				$sql.= "LEFT JOIN
						prospectus_courses pc
					ON
						pc.courses_id=c.id
					LEFT JOIN
						prospectus_terms pt
					ON
						pc.prospectus_terms_id=pt.id
					LEFT JOIN
						prospectus p
					ON
						p.id=pt.prospectus_id
					";
			}
				
			$sql .= "WHERE
					co.academic_terms_id={$this->db->escape($academic_term)}
				";
						
			if ( ! empty($limit['college'])) {
				$sql .= " AND c.owner_colleges_id={$this->db->escape($limit['college'])} ";	
			}
			
			if ( ! empty($limit['program'])) {
				$sql .= " AND p.academic_programs_id={$this->db->escape($limit['program'])} ";	
			}
			
			switch ($status){
				case 'active' :
					$sql .= " AND co.status='A' ";
					break;
				case 'dissolved' :
					$sql .= " AND co.status='D' ";
					break;
				default:
					
					break;
			}
			
			
			$sql .= "
			GROUP BY
				co.id
			ORDER BY
				c.course_code, co.section_code
			-- LIMIT {$start}, {$count}
			";

			//echo $sql;
			//die();
			
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0) {
				$return['result'] = $query->result();
				$query = $this->db->query("SELECT FOUND_ROWS() as total_rows");
				$row = $query->row();
				$return['total_rows'] = $row->total_rows;
				return $return;
			} else
				return FALSE;
		}
		 
	//Added: 3/23/2013
	
	function AddNewCourse($course_data) {
			$query = "INSERT INTO courses 
							(id, 
							 course_code, 
							 courses_groups_id,
							 course_types_id,
							 owner_colleges_id,
							 descriptive_title,
							 credit_units,
							 paying_units,
							 is_service_course,
							 inserted_by,
							 inserted_on
							)
							VALUES (NULL,
									{$this->db->escape($course_data['course_code'])},
									{$this->db->escape($course_data['group_id'])},
									{$this->db->escape($course_data['type_id'])},
									{$this->db->escape($course_data['college_id'])},
									{$this->db->escape($course_data['descriptive_title'])},
									{$this->db->escape($course_data['credit_units'])},
									{$this->db->escape($course_data['paying_units'])},
									{$this->db->escape($course_data['service_course'])},
									{$this->session->userdata('empno')},
									now()
								)";
			//print($query);die();
			//FIXME: What if insertion was failed?
	   		if ($this->db->query($query)) {
   				return TRUE;
   			} else {
   				return FALSE;
   			}
	}
		 
	/*
	 * @Updated: 11/6/14
	 * @editor: genes
	 */	
	function updateCourse($data) {
			
			 $q = "UPDATE courses 
					SET 
		    			descriptive_title ={$this->db->escape($data['descriptive_title'])},
						course_types_id ={$this->db->escape($data['type_id'])},
						paying_units ={$this->db->escape($data['paying_units'])},
						credit_units ={$this->db->escape($data['credit_units'])},
						is_service_course ={$this->db->escape($data['service_course'])},
						updated_by = {$this->db->escape($data['empno'])},
						updated_on = NOW()
					WHERE
						id={$this->db->escape($data['courses_id'])} ";
			
			//print($q); die();
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
	
	}
	
	//Added: 4/15/2013
	
	function ListAllCoursesToBeAdvised() {
				
			$result = null;
				
			$q1 = "
					SELECT
						a.id, a.course_code,a.credit_units, a.descriptive_title, a.paying_units
					FROM
						courses AS a
			--	WHERE a.id NOT IN
			-- 	(SELECT ac.courses_id
			--		   FROM advised_courses as ac
			--		 )  	
					ORDER BY
						a.course_code " ;
			
			//offer only courses that are offered
			$q1 ="
				select a.id, a.course_code,a.credit_units, a.descriptive_title, a.paying_units
				from courses a 
				join course_offerings co on co.courses_id = a.id and co.`status` ='A'
				join academic_terms tr on tr.id = co.academic_terms_id and tr.`status` !='previous' 
				group by a.course_code 
				order by a.course_code 					
				";
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		}
	
	//Added: 5/2/2013
	
	function getCourseInfo($enrollments_id){
			$result = 0;
			$q = "SELECT c.paying_units,
						 d.type_description,
						 c.courses_groups_id,
						 c.course_code, c.id
					FROM
						enrollments as a, 
						course_offerings as b,
						courses as c,
						course_types as d
						
					WHERE
						 a.id = {$this->db->escape($enrollments_id)}
						 AND a.course_offerings_id = b.id
						 AND b.courses_id = c.id
						 AND c.course_types_id = d.id";	 
	         
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
	}
	
	//Added: 9/9/2013
	
	function checkIfCourseOther($course_id) {
		$q1 = "SELECT a.rate 
		FROM
			tuition_others as a
		WHERE
			a.courses_id ={$this->db->escape($course_id)}
		GROUP BY 
			a.academic_terms_id, a.levels_id ";
			
		$query = $this->db->query($q1);
	
		if($query->num_rows() > 0){
		return TRUE;
	} else {
	return FALSE;
		}
	
	}
    
	//Added: March 11, 2013 by Amie
	function course_masterlist() {
		$result = 0;
		$q = "SELECT distinct(c.course_code), c.descriptive_title, c.paying_units, c.credit_units, c.paying_units as contact_hours, co.college_code, ct.course_type
				FROM courses c, colleges co, course_types ct, course_offerings coffer
				WHERE c.owner_colleges_id = co.id
					AND c.course_types_id = ct.id
					AND c.id = coffer.courses_id
				ORDER BY c.course_code";

		$query = $this->db->query($q);
				
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
	}
	
	function ListCoursesAll()
	{
		$result = null;
			
		$q2 = "
		(SELECT
		DISTINCT(a.course_code) AS course_code,
		a.id,
		a.descriptive_title,
		a.paying_units,
		a.credit_units
		FROM
		courses AS a
		)
		UNION
			
		(SELECT
				DISTINCT(a.course_code) AS course_code,
				a.id,
				a.descriptive_title,
				a.paying_units,
				a.credit_units
				FROM
				courses AS a,
				elective_courses AS b
				WHERE
				a.id=b.topic_courses_id
				 )
					
				ORDER BY
				course_code "
				;
	
				$q3="
				select 	course_code,tmp.id,descriptive_title,paying_units,credit_units,course_types_id,ct.type_description
				from
				(
				(SELECT
				DISTINCT(a.course_code) AS course_code,a.id,a.descriptive_title,a.paying_units,a.credit_units,a.course_types_id
				FROM
				courses AS a
				 )
				UNION
				(SELECT
				DISTINCT(a.course_code) AS course_code,
				a.id,
				a.descriptive_title,
				a.paying_units,
				a.credit_units,
				a.course_types_id
				FROM
				courses AS a,
				elective_courses AS b
				WHERE
				a.id=b.topic_courses_id
				 )
				) as tmp
				join course_types ct on ct.id =  tmp.course_types_id
				ORDER BY 	course_code
				";
	
	
				//print_r($q3);die();
				$query = $this->db->query($q3);
					
				if($query->num_rows() > 0){
				$result = $query->result();
	}
		
	return $result;
		
	}
			 
 //Added: 6/9/2014 by Isah

	function ListFacultyLoad($academic_terms_id, $empno) {
		$result = null;
			
		$q = "SELECT a.id,
				a.academic_terms_id,
				a.courses_id,
				a.section_code,
				a.max_students,
				c.course_code,
				c.credit_units,
				c.descriptive_title,
				(SELECT
					COUNT(xx.id)
					FROM
					course_offerings AS aa,
					enrollments AS xx,
					parallel_offerings AS yy
					WHERE
					aa.id=xx.course_offerings_id
					AND aa.id=yy.course_offerings_id
					AND yy.parallel_no = y.parallel_no) AS enrollee_parallel,
				COUNT(x.id) AS enrollee,
				IF(a.status='A','ACTIVE','DISSOLVED') AS status,
				y.parallel_no
			FROM
				courses AS c,
				course_offerings AS a,
				enrollments AS x 
					LEFT JOIN parallel_offerings AS y ON a.id=y.course_offerings_id
			WHERE
				a.courses_id = c.id
				AND a.id=x.course_offerings_id
				AND	a.academic_terms_id={$this->db->escape($academic_terms_id)} 
				AND a.employees_empno = {$this->db->escape($empno)} ";
			
		
		//print($q); die();
			$query = $this->db->query($q);
				
			if($query->num_rows() > 0){
			$result = $query->result();
			}
		
			return $result;
		
		}


	// Toyet 5.31.2018
	public function ListCoursesWithZeroPayingUnits($yr_level, $colleges_id, $terms_sy) {
		$result = NULL;
		$sql = "select cl.name, c.course_code, c.descriptive_title, c.paying_units
				from student_histories sh 
				left join prospectus p on p.id=sh.prospectus_id 
				left join academic_programs ap on ap.id=p.academic_programs_id 
				left join enrollments en on en.student_history_id=sh.id 
				join course_offerings co on co.id=en.course_offerings_id 
				join courses c on c.id=co.courses_id 
				left join colleges cl on cl.id=c.owner_colleges_id 
				where 
				   sh.academic_terms_id = {$terms_sy} 
					and c.paying_units = 0 
					and c.owner_colleges_id = {$colleges_id} 
				group by course_code"; 

		$query = $this->db->query($sql);
		
		//log_message("INFO", print_r($sql,true)); // Toyet 5.31.2018

		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

}
	

//end of course_model.php