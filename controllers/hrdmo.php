<?php

class Hrdmo extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->content_lib->set_title ('HRDMO | ' . $this->config->item('application_title'));
		$this->navbar_data['menu'] = array(
						'Students' => 'student',
						'Employees'=>array(
								'Employees'=>'faculty',
								'New Employee'=>'new_employee',
								),
					);
		$this->content_lib->set_navbar_content('', $this->navbar_data);
	}
	
	public function faculty(){
		$this->content_lib->set_title('HRDMO | Employees | ' . $this->config->item('application_title'));
		$query = $this->input->post('q');
		$empno = $this->uri->segment(3);
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'faculty'), 'Search Employees', 'in');
		
		if ($query){
			//There is a query...
			$this->load->model('employee_common_model');
			$results = $this->employee_common_model->search($query);
			
			if (count($results) > 1){
				//more than 1 results... lets show it in a page
				foreach ($results as $result){
					if (is_file(FCPATH . $this->config->item('employee_images_folder') . ltrim($result->empno, '0') . ".jpg"))
						$image = base_url($this->config->item('employee_images_folder') . ltrim($result->empno, '0') . ".jpg");
					else {
						//$image = base_url($this->config->item('no_image_placeholder_female')); else
						$image = base_url($this->config->item('no_image_placeholder_male'));
					}
					$res[] = array('image'=>$image, 'idno'=>$result->empno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
				}
				$this->content_lib->enqueue_body_content('common/employee_search_result', array('results'=>$res, 'query'=>$query, 'role'=>'hrdmo'));
			} elseif (count($results)==1){
				redirect (site_url("{$this->role}/faculty/{$results[0]->empno}"));
			} else {
				//no result...
				
			}
		}
		
		if ($empno){
			//An employee number was used...
			$this->load->model('employee_common_model');
			$this->load->model('academic_terms_model');
			$this->load->model('hnumis/College_model');
			//$data = $this->employee_common_model->my_information($empno);
			//We're going to need tabs... lets load the tab library:
			$this->load->library('tab_lib');
			
			//User may have selected what academic term and year...
			if ($this->input->post('academic_terms_id')) {
				$academic_terms_id = $this->input->post('academic_terms_id');
			} else {
				$academic_terms = $this->academic_terms_model->current_academic_term();
				$academic_terms_id = $academic_terms->id;
			}
			
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				//Lets process actions intended for this employee...
				switch ($this->input->post('action')){
					case 'update_roles'		:
						$my_roles_obj = $this->employee_common_model->my_roles($empno);
						$my_roles = array();
						
						foreach ($my_roles_obj as $my_role) {
							$my_roles[] = $my_role->role; //an array of roles
						}
						
						$roles_assigned = $this->input->post('roles');//roles already assigned to specified user
						if (! $roles_assigned) {
							$roles_assigned = array();
						}

						//roles to be deleted...
						$roles_to_delete = array();
						if (is_array($roles_assigned)) {
							foreach ($my_roles as $my_role) {
								if ( ! in_array($my_role, $roles_assigned)){
									$roles_to_delete[] = $my_role; 
								}
							}
						}
						
						//roles to add...
						$roles_to_add = array();
						$roles_to_update = array();
						if (count($roles_assigned > 0)){
							foreach ($roles_assigned as $role_assigned){
								if ( ! in_array($role_assigned, $my_roles)){
									$roles_to_add[] = $role_assigned;
								}
								//if role has a college, we're going to hook it to a college so we need to update this role...
								if ( in_array($role_assigned, $this->config->item('roles_with_colleges'))){
									$roles_to_update[] = $role_assigned;  
								}
							}
						}
						
						$college_id = ($this->input->post('college_id') ? $this->input->post('college_id') : 0);
						if (is_array($roles_to_delete) && count($roles_to_delete) > 0)
							$this->employee_common_model->delete_roles($empno, $roles_to_delete);
						if (is_array($roles_to_add) && count($roles_to_add) > 0)
							$this->employee_common_model->add_roles($empno, $roles_to_add, $college_id);
						if (is_array($roles_to_update) && count($roles_to_update) > 0)
							$result = $this->employee_common_model->update_roles($empno, $roles_to_update, $college_id);
						$this->content_lib->set_message('Roles are updated.', 'alert-success');
						//$this->content_lib->set_message($result, 'alert-success');
						break;
					case 'change_password'	:
						if ($this->employee_common_model->update_information($empno, array('password'=>$this->input->post('password')))){
							$this->content_lib->set_message('Password is updated to: <strong>' . $this->input->post('password') . '</strong>', 'alert-success');
						} else {
							$this->content_lib->set_message('Error(s) encountered while updating password.', 'alert-error');
						}
						break;
					default:
						
						break;
				}
			} else {
				//an invalid nonce...
				$this->content_lib->set_message($this->config->item('nonce_error_message', 'alert-error'));	
			}
			$data = $this->employee_common_model->my_information($empno);
			$this->content_lib->enqueue_header_style('multi_select');
			$this->content_lib->enqueue_footer_script('multi_select');
			$this->load->model('hnumis/Courses_model');
			
			$my_roles_obj = $this->employee_common_model->my_roles($empno);
			$my_roles = array(); 
			foreach ($my_roles_obj as $my_role) {
				$my_roles[] = $my_role->role;
			}
			$colleges = $this->College_model->all_colleges();
			$academic_terms = $this->academic_terms_model->faculty_inclusive_academic_term ($empno);
			$this->content_lib->enqueue_sidebar_widget ('dean/list_academic_terms1',array('academic_terms'=>$academic_terms, 'selected_term'=>$academic_terms_id), 'Select Academic Terms', 'in');
			$result = $this->Courses_model->faculty_courses_academic_terms($empno, $academic_terms_id);
			$this->tab_lib->enqueue_tab ('Schedule', 'faculty/list_courses', array('result'=>$result, 'term'=>(isset($result[0]->term) ? $result[0]->term : ""), 'school_year'=>(isset($result[0]->sy) ? $result[0]->sy : ""), 'selected_term'=>$academic_terms_id), 'schedule', TRUE);
			$this->tab_lib->enqueue_tab ('Roles', 'hrdmo/roles', array("my_roles" => $my_roles, "roles" => $this->config->item('roles'), 'colleges'=>$colleges), 'roles', FALSE);
			$this->tab_lib->enqueue_tab ('Password', 'hrdmo/password', array(), 'password', FALSE);
			
			if (is_file(FCPATH . $this->config->item('employee_images_folder') . ltrim($empno, '0') . ".jpg"))
				$image = base_url($this->config->item('employee_images_folder') . ltrim($empno, '0') . ".jpg");
			else {
				//$image = base_url($this->config->item('no_image_placeholder_female')); else
				//todo: add gender to employees table...
				$image = base_url($this->config->item('no_image_placeholder_male'));
			}
			
			$data['image'] = $image;
			$this->content_lib->enqueue_body_content('common/employee_profile', $data);
			
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/Courses_model');
			
			$academic_terms = $this->academic_terms_model->faculty_inclusive_academic_term ($empno);
			
			$data['selected_term'] = $academic_terms_id;
			$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		}
		
		if ( ! $empno && ! $query){
			//No query was done by the DEAN... and no employee was found yet...
			$data = array();
			if ($this->input->post('letter')){
				$data['letter'] = $this->input->post('letter');
				$this->load->model('employee_common_model');
				$data['results'] = $this->employee_common_model->search_by_starting_letter($this->input->post('letter'));
			}
			$this->content_lib->enqueue_body_content('common/employee_search_landing', $data);
		}
		
		$this->content_lib->content();
	}
	
	public function new_employee(){
		$this->load->library('tab_lib');
		$this->content_lib->set_title('HRDMO | New Employee | ' . $this->config->item('application_title'));
		if ($this->input->post('action')){
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				switch ($this->input->post('action')){
					case 'new_employee' :
						$this->load->model('employee_common_model');
						if ($ret = $this->employee_common_model->new_employee($this->input->post()))
							$this->content_lib->set_message('Successfully added employee with id number: ' . $ret, 'alert-success'); else
							$this->content_lib->set_message('Error found while adding new employee', 'alert-error');
						break;
				}
			} else{
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}
		}
		
		$this->content_lib->enqueue_footer_script('date_picker');
		$this->content_lib->enqueue_header_style('date_picker');
		
		$this->load->model('common_model');
		$this->load->model('Places_model');
		$religions = $this->common_model->religions();
		$citizenships = $this->common_model->citizenships();
		$countries = $this->Places_model->countries();
		$philippine_id = $this->Places_model->country_id('Philippines');
		$provinces = $this->Places_model->fetch_results($philippine_id, 'provinces');
		
		$this->tab_lib->enqueue_tab('New Employee', 'hrdmo/new_employee', array('provinces'=>$provinces, 'philippine_id'=>$philippine_id, 'countries'=>$countries), 'new_employeee', TRUE);
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->content();
	}
	
	public function student(){
	
		$this->content_lib->set_title('HRDMO | Students | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
	
		if ( ! empty($query)){
	
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
	
			$this->load->library('pagination_lib');
	
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
	
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
	
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
	
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/Student_model');
		
			//$this->content_lib->enqueue_footer_script('printelement'); // print element plugin...
			$this->content_lib->set_title ('HRDMO | ' . $idnum);
			$this->load->model('student_common_model');
			
			if ($this->input->post('action')) {
				
				if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					switch ($this->input->post('action')){
						case 'update_roles' : 
							$my_roles_obj = $this->student_common_model->my_roles($idnum);
							$my_roles = array();
							
							foreach ($my_roles_obj as $my_role) {
								$my_roles[] = $my_role->role; //an array of roles
							}
							
							$roles_assigned = $this->input->post('roles');//roles already assigned to specified user
							if (! $roles_assigned) {
								$roles_assigned = array();
							}
							
							//roles to be deleted...
							$roles_to_delete = array();
							if (is_array($roles_assigned)) {
								foreach ($my_roles as $my_role) {
									if ( ! in_array($my_role, $roles_assigned)){
										$roles_to_delete[] = $my_role;
									}
								}
							}
							
							//roles to add...
							$roles_to_add = array();
							$roles_to_update = array();
							if (count($roles_assigned > 0)){
								foreach ($roles_assigned as $role_assigned){
									if ( ! in_array($role_assigned, $my_roles)){
										$roles_to_add[] = $role_assigned;
									}
									//if role has a college, we're going to hook it to a college so we need to update this role...
									if ( in_array($role_assigned, $this->config->item('roles_with_colleges'))){
										$roles_to_update[] = $role_assigned;
									}
								}
							}
							
							$college_id = ($this->input->post('college_id') ? $this->input->post('college_id') : 0);
							
							if (is_array($roles_to_delete) && count($roles_to_delete) > 0)
								$this->student_common_model->delete_roles($idnum, $roles_to_delete);
							if (is_array($roles_to_add) && count($roles_to_add) > 0)
								$this->student_common_model->add_roles($idnum, $roles_to_add, $college_id);
							if (is_array($roles_to_update) && count($roles_to_update) > 0)
								$result = $this->student_common_model->update_roles($idnum, $roles_to_update, $college_id);
							
							$this->content_lib->set_message('Roles are updated.', 'alert-success');
							//$this->content_lib->set_message($result, 'alert-success');
							break;
							
						default:
							break;
					
					}	
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
				}
			}
	
			$result = $this->student_common_model->my_information($idnum);
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->year_level,
						'full_home_address'	=>$result->full_home_address,
						'full_city_address' =>$result->full_city_address,
						'phone_number'		=>$result->phone_number,
						'section'			=>$result->section,
				);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				//tab contents
				$this->content_lib->enqueue_header_style('multi_select');
				$this->content_lib->enqueue_footer_script('multi_select');
				$droles = $this->student_common_model->my_roles($idnum);
				$my_roles = array();
				foreach($droles as $my_role){
					$my_roles[] = $my_role->role;
				}
				
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				$this->load->library('tab_lib');
				$this->tab_lib->enqueue_tab('Roles', 'hrdmo/roles', array('roles'=>$this->config->item('roles_for_students'), 'my_roles'=>$my_roles), 'student_roles', TRUE);
				$tab_content = $this->tab_lib->content();
				$this->content_lib->enqueue_body_content("", $tab_content);
			}
	
		}
	
		$this->content_lib->content();
	}
}