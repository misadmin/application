<?php 
$date_start = '22 Aug 2012';
$date_end = '22 Aug 2012';
$grand_total = 212789.45;
$non_vat_or_numbers = '8769087-1290876';
$vat_or_numbers = '8769087-1290876';
$voided_total = 1232324.20;
$valid_total = 1234.23;
$ip = '10.10.2.241';

echo $this->print_lib->condensed();
echo $this->print_lib->align_center('Holy Name University', 130) . "\n";
echo $this->print_lib->align_center('Tagbilaran City', 130) . "\n\n";
echo $this->print_lib->align_center('OFFICIAL RECEIPTS SUMMARY', 130) . "\n";
echo $this->print_lib->align_center('PERIOD: ' . $date_start . " - " . $date_end, 130) . "\n";
echo str_pad('', 130, '-') . "\n";

echo str_pad('Grand Total :', 30, " ", STR_PAD_LEFT);
echo str_pad(number_format($grand_total,2), 30, " ", STR_PAD_LEFT);
echo str_pad('VAT OR :', 40, " ", STR_PAD_LEFT);
echo str_pad($vat_or_numbers . "  ", 30, " ", STR_PAD_LEFT) . "\n";

echo str_pad('Voided Total :', 30, " ", STR_PAD_LEFT);
echo str_pad(number_format($voided_total,2), 30, " ", STR_PAD_LEFT);
echo str_pad('NON-VAT OR :', 40, " ", STR_PAD_LEFT);
echo str_pad($non_vat_or_numbers . "  ", 30, " ", STR_PAD_LEFT) . "\n";

echo str_pad('Valid Total :', 30, " ", STR_PAD_LEFT);
echo str_pad(number_format($valid_total,2), 30, " ", STR_PAD_LEFT);
echo str_pad('Submitted by:', 40, " ", STR_PAD_LEFT);
echo str_pad("" . "  ", 30, " ", STR_PAD_LEFT) . "\n";
echo str_pad('', 130, '-') . "\n";

echo str_pad('Machine No :', 30, " ", STR_PAD_LEFT);
echo str_pad($ip, 30, " ", STR_PAD_LEFT) . "\n";

echo str_pad('', 130, '-') . "\n";
echo str_pad('O.R. Date - Time', 30, " ", STR_PAD_RIGHT);
echo str_pad('O.R. No.', 20, " ", STR_PAD_BOTH);
echo str_pad('M/T', 12, " ", STR_PAD_BOTH);
echo str_pad('Amount', 14, " ", STR_PAD_BOTH);
echo str_pad('Code', 10, " ", STR_PAD_BOTH);
echo str_pad('ID No.', 9, " ", STR_PAD_BOTH);
echo str_pad('Name', 34, " ", STR_PAD_BOTH) . "\n";
echo str_pad('', 130, '-') . "\n";


