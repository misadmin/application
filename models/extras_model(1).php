<?php

class Extras_model extends CI_Model {
	private $last_error;
	
	public function __construct(){
		parent::__construct();
	} 

	function list_sub_levels($top_level) {
		$result = NULL;		
		switch ($top_level) {			
			case 'all':
				$sql = "
					SELECT id, level as sublevel 
					FROM levels 
					WHERE id in (2,3,4,5)
					ORDER BY id						
				"; //@FIXME: to be modified 
				break;
			case 'college':
				$sql = "
					SELECT id, name as sublevel 
					FROM colleges
					WHERE status = 'Active'	
					ORDER BY college_code
				";
				break; 
			case 'basic_ed':
				$sql = "
					SELECT id, level as sublevel 
					FROM levels 
					WHERE id in (2,3,4,5)
					ORDER BY id
				";
				break;
		}
		$query = $this->db->query($sql);		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}			
		return $result;
	}
	
	function list_levels(){
		$sql ="
			select `id`,`level` from levels where id between 2 and 6
		";
		$query = $this->db->query($sql);		
		if($query && $query->num_rows() > 0){
			return $query->result();				
		}else{ 
			return false;
		}
	}
	
	function list_levels_and_deparments(){
		$sql ="
			select  `level` as `department`, 'N' as 'is_college'
				from levels 
				where id not in (6,7,8,9) and `level` !='NullFromISIS'
			union 
			select distinct cl.college_code as `department`, 'Y' as 'is_college'
				from colleges cl 
				where college_code not in ('None','NSTP')
		";
		$sql ="
			select  `level` as `department`, 'N' as 'is_college'
				from levels
				where id not in (6,7,8,9) and `level` !='NullFromISIS'
			union
			select 'College' as `department`, 'Y' as 'is_college'
		";
		
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	
	
	function list_last_assessment_dates(){
		$sql="
			select a.transaction_date, h.id history_id, s.idno, cap_first(concat(s.fname,' ',s.lname)) student, ap.abbreviation, h.year_level
			from assessments a
				join student_histories h on h.id = a.student_histories_id
				join academic_terms tr on tr.id = h.academic_terms_id
				join students s on s.idno = h.students_idno
				join prospectus p on p.id = h.prospectus_id
				join academic_programs ap on ap.id = p.academic_programs_id				
			where tr.`status` = 'current'
				order by transaction_date, ap.abbreviation,  s.lname, s.fname
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	
	}

	
	function list_added_subjects($date, $history_id){
		$sql = "
			select c.course_code, co.section_code, e.date_enrolled, e.enrolled_by enrolled_by_id
				,if(!isnull(s.idno),cap_first(concat(s.fname,' ',s.lname)), cap_first(concat(substr(emp.fname,1,1),'. ',emp.lname))) enrolled_by_name,
				if(!isnull(e.withdrawn_on),if(e.withdrawn_on <='2013-12-17 23:59:59','WD on or Before 17 Dec 2013','WD After 17 Dec 2013') ,'Added After Assessment') as remark
			from enrollments e
				join course_offerings co on co.id = e.course_offerings_id
				join courses c on c.id = co.courses_id
				join student_histories h on h.id = e.student_history_id
				left join students s on s.idno = e.enrolled_by 
				left join employees emp on emp.empno = e.enrolled_by 
			where  h.id = '{$history_id}' and (e.date_enrolled > '{$date}' or !isnull(e.withdrawn_on))
				order by remark, date_enrolled
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}


	function list_isis_balances(){
		$sql = "
			select * from 
				(
				select l.stud_id, sum(ifnull(l.debit, 0) - ifnull(l.credit,0)) as balance
				from student.ledger l
				where l.status !='void' 
				-- and l.stud_id=05504632
				group by stud_id
				) t
			where t.balance !=0 
			limit 100
		";		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){			
			return $query->result();
		}else{
			return false;
		}
	}

	function list_isis_balances2($idno){
		$sql = "
			select t.*,h.academic_terms_id,h.prospectus_id, h.academic_terms_id, h.year_level, ap.abbreviation  
			from 
				(
				select l.stud_id, sum(ifnull(l.debit, 0) - ifnull(l.credit,0)) as balance
				from student.ledger l
				where l.status !='void' and l.stud_id='{$idno}'
				group by stud_id
				) t
				join (select students_idno, academic_terms_id,prospectus_id,year_level 
						from student_histories 
						where students_idno='{$idno}'
						order by academic_terms_id desc
						limit 1
						) h on h.students_idno = t.stud_id 
					join prospectus p on p.id = h.prospectus_id
					join academic_programs ap on ap.id = p.academic_programs_id 						
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	
	function get_payers_id($idno){
		$sql = "
			select id as payers_id 
			from payers 
			where students_idno = '{$idno}'  
			limit 1
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	}
	
	function list_students($college, $program){
		$sql = "
			select h.students_idno, ap.abbreviation, c.college_code 
			from student_histories h 
			left join prospectus p on p.id = h.prospectus_id
			left join academic_programs ap on ap.id = p.academic_programs_id
			left join colleges c on c.id = ap.colleges_id
			where 
			ap.colleges_id = '{$college}'" 
			. (!empty($program ) ? " and ap.id = '{$program}' " : "") 
			." group by h.students_idno
			order by h.academic_terms_id desc "				
		;
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function list_students2($top_levels, $sub_level,$current_end_year){
		$sql="";
		switch ($top_levels){
			case 'all':
				$sql ="
					select h.students_idno, cap_first(concat(s.lname,', ',s.fname)) student, h.yr_level as year_level, bs.section_name as abbreviation
					from basic_ed_histories h
					join basic_ed_sections bs on bs.id = h.basic_ed_sections_id
					join students s on s.idno = h.students_idno
					join academic_years ay on ay.id = h.academic_years_id
					where ay.`status`='current' and h.levels_id = '{$sub_level}' 						
					order by h.yr_level, bs.section_name, student
				";						
				break;
			case 'basic_ed':				
				$sql ="
					select h.students_idno, cap_first(concat(s.lname,', ',s.fname)) student, concat(l.`level`,' ', h.yr_level) as year_level, 
						'' as abbreviation, h.levels_id, ay.end_year
					from
						(select bh1.*
							from basic_ed_histories bh1
							left join basic_ed_histories bh2 on bh2.students_idno = bh1.students_idno
							and (ifnull(bh1.academic_years_id,0) + ifnull(bh1.yr_level,0) < ifnull(bh2.academic_years_id,0) + ifnull(bh2.yr_level,0))
							where bh2.students_idno is null
							group by bh1.students_idno
						) h 
					join students s on s.idno = h.students_idno
					join academic_years ay on ay.id = h.academic_years_id
					join levels l on l.id = h.levels_id
					where h.levels_id = '{$sub_level}' and ('{$current_end_year}' - end_year <= 8 )
						and h.students_idno not in (select distinct students_idno from student_histories)
					order by end_year desc, student, year_level						
				";								
				break;
			case 'college':
				$sql = "
					select h.students_idno, cap_first(concat(s.lname,', ',s.fname)) student, h.year_level, 
						ap.abbreviation, '6' as levels_id, ay.end_year
					from
						(select h1.*
							from student_histories h1
							left join student_histories h2 on h2.students_idno = h1.students_idno
							and (ifnull(h1.academic_terms_id,0) < ifnull(h2.academic_terms_id,0) )
							where h2.students_idno is null
						) h
					join prospectus p on p.id = h.prospectus_id
					join academic_programs ap on ap.id = p.academic_programs_id
					join academic_terms tr on tr.id = h.academic_terms_id
					join academic_years ay on ay.id = tr.academic_years_id					
					join students s on s.idno = h.students_idno
					where ap.colleges_id = '{$sub_level}'  and ('{$current_end_year}' - end_year <= 8 )   
					order by end_year desc, student, abbreviation, h.year_level
				";
			break;
				
			
		}
		//print_r($sql);die();
		if (!empty($sql)){
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	

	function list_students_bed($levels_id){
		$sql = "
			select h.students_idno, cap_first(concat(s.lname,', ',s.fname)) student, l.`level`,  sc.section_name, h.yr_level as year_level
			from basic_ed_histories h
			join academic_years ay on ay.id = h.academic_years_id
			join basic_ed_sections sc on sc.id = h.basic_ed_sections_id
			join levels l on l.id = h.levels_id
			join students s on s.idno = h.students_idno
			where h.levels_id='{$levels_id}' and ay.`status`='current'
			group by h.students_idno
			order by ay.end_year desc, h.yr_level, student
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
		
	
	
	function last_balance($payers_id, $date){
		$sql="
			SELECT sum(ifnull(debit, 0) - ifnull(credit,0)) as balance
				FROM 
				(
					SELECT y2.id as payers_id, h.academic_terms_id, l2.transaction_date, l2.debit, l2.credit
					FROM ledger l2
					JOIN assessments s on s.id = l2.assessments_id 
					JOIN student_histories h on h.id = s.student_histories_id
					JOIN payers  y2 on y2.students_idno = h.students_idno
					WHERE y2.id = {$payers_id}
		 				and l2.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
				UNION 
					SELECT y.id as payers_id, p.academic_terms_id, l.transaction_date, l.debit, l.credit
					FROM ledger l
					JOIN payments     p on p.id = l.payments_id
					JOIN payers       y on y.id = p.payers_id
					JOIN teller_codes c on c.teller_code = p.isis_tran_code and !isnull(p.isis_tran_code)
					WHERE y.id = {$payers_id}
		 				and l.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
		 		UNION  
					SELECT yx.id as payers_id, p.academic_terms_id, lx.transaction_date, lx.debit, lx.credit
					FROM ledger lx
					JOIN payments     p on p.id = lx.payments_id
					JOIN payment_items im on im.payments_id = p.id
					join teller_codes tc on tc.id = im.teller_codes_id
					JOIN payers       yx on yx.id = p.payers_id
					WHERE isnull(p.isis_tran_code) and yx.id = {$payers_id} 
		 				and lx.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
				UNION
					select a.payers_id, a.academic_terms_id, a.transaction_date, l3.debit, l3.credit
					FROM ledger l3
					JOIN adjustments a on a.id = l3.adjustments_id 
					WHERE a.payers_id = {$payers_id}
		 				and l3.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
				UNION
					SELECT y4.id as payers_id, h.academic_terms_id, l4.transaction_date, l4.debit, l4.credit
					FROM ledger l4
					JOIN privileges_availed v  on v.id = l4.privileges_availed_id 
					join scholarships       s  on s.id = v.scholarships_id
					JOIN student_histories  h  on h.id = v.student_histories_id
					JOIN payers 		    y4 on y4.students_idno = h.students_idno
					WHERE y4.id = {$payers_id}			
		 				and l4.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
				UNION 
					SELECT y5.id as payers_id, sh.academic_terms_id, l5.transaction_date, l5.debit, l5.credit
					FROM ledger l5
					JOIN re_enrollments 		re  on re.id  = l5.re_enrollments_id 
					JOIN enrollments 			enr on enr.id = re.enrollments_id
					JOIN student_histories 		sh  on sh.id  = enr.student_history_id
					JOIN payers 				y5  on y5.students_idno = sh.students_idno 
					where y5.id = {$payers_id}				
		 				and l5.transaction_date <= date_add('{$date}', INTERVAL '23:59:59' HOUR_SECOND)
			) as t 
			LEFT JOIN academic_terms `at` ON at.id=t.academic_terms_id
			LEFT JOIN academic_years  ay ON ay.id=at.academic_years_id
			GROUP BY payers_id
			ORDER BY t.transaction_date				
		";
		//die($sql);
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
		
	}

	public function list_students_with_total_withdrawals() {
		$sql = "
			select * from 
			(
				select h.students_idno, cap_first(concat(s.lname,', ',s.fname)) student,  
					ap.abbreviation, h.year_level,
					sum(e.`id`>0) as enrolled, sum(e.`status`='Withdrawn') as withdrawns
				from student_histories h 
				join enrollments e on e.student_history_id = h.id
				join students s on s.idno = h.students_idno
				join prospectus p on p.id = h.prospectus_id
				join academic_programs ap on ap.id = p.academic_programs_id
				where h.academic_terms_id=455 group by h.students_idno
			) t
			where withdrawns >= enrolled
			order by student				
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	public function list_enrolled_vs_withdrawals($end_yr) {	
		$sql_old = "
			select concat(end_year-1,'-',end_year) as sy, `term`, academic_terms_id,college_code,abbreviation, sum(withdrawns - enrolled < 0 ) as `enrollments`, sum(withdrawns - enrolled >= 0 ) as withdrawals
			from (
					select ay.end_year, tr.`term`, h.academic_terms_id, h.students_idno, cap_first(concat(s.lname,', ',s.fname)) student,  
						c.college_code,ap.abbreviation, 
						sum(e.`id`>0) as enrolled, sum(e.`status`='Withdrawn') as withdrawns
					from student_histories h 
					join enrollments e on e.student_history_id = h.id
					join students s on s.idno = h.students_idno
					join prospectus p on p.id = h.prospectus_id
					join academic_programs ap on ap.id = p.academic_programs_id
					join colleges c on c.id = ap.colleges_id
					join academic_terms tr on tr.id = h.academic_terms_id
					join academic_years ay on ay.id = tr.academic_years_id
					where ay.end_year = '{$end_yr}'
					group by h.academic_terms_id, h.students_idno
				) t
			group by academic_terms_id,college_code,abbreviation
			order by academic_terms_id,college_code,abbreviation			
		";
		$sql="
			select college_code,abbreviation,
				group_concat(distinct if(enr_1 is not null, enr_1, null)) as enr_1,
				group_concat(distinct if(wit_1 is not null, wit_1, null)) as wit_1,
				group_concat(distinct if(enr_2 is not null, enr_2, null)) as enr_2,
				group_concat(distinct if(wit_2 is not null, wit_2, null)) as wit_2,
				group_concat(distinct if(enr_3 is not null, enr_3, null)) as enr_3,
				group_concat(distinct if(wit_3 is not null, wit_3, null)) as wit_3
			from
			(
				select college_code,abbreviation,
						if(`term`=1, sum(enrolled - withdrawns >  0 and `term`=1 ), null) as `enr_1`, 
						if(`term`=1, sum(enrolled - withdrawns <= 0 and `term`=1 ), null) as `wit_1`,
						if(`term`=2, sum(enrolled - withdrawns >  0 and `term`=2 ), null) as `enr_2`, 
						if(`term`=2, sum(enrolled - withdrawns <= 0 and `term`=2 ), null) as `wit_2`,
						if(`term`=3, sum(enrolled - withdrawns >  0 and `term`=3 ), null) as `enr_3`, 
						if(`term`=3, sum(enrolled - withdrawns <= 0 and `term`=3 ), null) as `wit_3`				
						
				from ( 
						select ay.end_year, tr.`term`, h.academic_terms_id, h.students_idno, cap_first(concat(s.lname,', ',s.fname)) student, c.college_code,ap.abbreviation, 
							sum(e.`id`>0) as enrolled, sum(e.`status`='Withdrawn') as withdrawns
						from student_histories h 
						join enrollments e on e.student_history_id = h.id
						join students s on s.idno = h.students_idno
						join prospectus p on p.id = h.prospectus_id
						join academic_programs ap on ap.id = p.academic_programs_id
						join colleges c on c.id = ap.colleges_id
						join academic_terms tr on tr.id = h.academic_terms_id
						join academic_years ay on ay.id = tr.academic_years_id
						where ay.end_year = '{$end_yr}' 
						  -- and ap.abbreviation = 'BS Psych' and term=2 and h.year_level=1
						group by h.academic_terms_id, h.students_idno
					) t
				group by academic_terms_id, college_code, abbreviation
				order by academic_terms_id, college_code, abbreviation
			) tmp
			group by college_code,abbreviation				
		";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	

	function list_students_with_more_than_one_idno(){
		$sql = "
			select cap_first(student) as student,idno,counts 
			from 
				(
				select concat(s.lname,', ',s.fname,' ',s.mname) as student, group_concat(s.idno order by idno asc) as idno, count(*) counts 
				from students s
				where s.is_active='Y' 
				group by student
				order by counts desc 
				limit 1000
				) tmp
			where counts > 1
			order by student
		";
		$query = $this->db->query($sql);
		if ($query){
			return $query->result();
		}else{
			return false;
		}
		
	}
	
	
	function search_receipt($receipt_no, $start_date, $end_date, $payor, $teller_code, $machine_ip, $chk1, $chk2, $chk3, $chk4, $chk5){
		//echo "check1 is ". $chk1; echo "<br>"; echo "check2 is ". $chk2; 
		$sql="
				select p.machine_ip, p.id, p.transaction_date, p.posted_on, tc.description, p.receipt_no, p.receipt_amount, 
					if(!isnull(r.students_idno), s.idno, if(!isnull(r.other_payers_id),op.id,if(!isnull(r.employees_empno),e2.empno,if(!isnull(r.inhouse_id),o.id,'invalid payor')))) as payor_id, 	
					if(!isnull(r.students_idno), cap_first(concat(s.lname,', ',s.fname)), if(!isnull(r.other_payers_id),cap_first(op.name),if(!isnull(r.employees_empno),cap_first(concat(e2.lname,', ',e2.fname)),if(!isnull(r.inhouse_id),o.office,'invalid payor')))) as payor, 	
					if(!isnull(s2.idno), s2.idno, e.empno) as teller_id,
					if(!isnull(s2.idno), cap_first(concat(s2.lname,', ',s2.fname)), cap_first(concat(e.lname,', ',e.fname))) as teller
				from payments p
					left join payers r on r.id = p.payers_id
					left join students s on s.idno = r.students_idno
					left join tellers t on t.id = p.tellers_id
					left join students s2 on s2.idno = t.students_idno
					left join employees e on e.empno = t.employees_empno
					left join payment_items i on i.payments_id = p.id
					left join teller_codes tc on tc.id = i.teller_codes_id 
					left join other_payers op on op.id = r.other_payers_id 
					left join employees e2 on e2.empno = r.employees_empno
					left join offices o on o.id = r.inhouse_id 		
				WHERE 
					i.amount = p.receipt_amount
					and from_isis='N' "
			.($chk1=='1' ? " and transaction_date between '{$start_date}' and date_add('{$end_date}', INTERVAL '23:59:59' HOUR_SECOND) " : "" )
			.($chk2=='1' ? " and receipt_no = '{$receipt_no}' " : "")
			.($chk3=='1' ? " 
						and concat(if(!isnull(r.students_idno), s.idno, if(!isnull(r.other_payers_id),op.id,if(!isnull(r.employees_empno),e2.empno,if(!isnull(r.inhouse_id),o.id,'invalid payor')))), ' ', 
					 		if(!isnull(r.students_idno), cap_first(concat(s.lname,', ',s.fname)), if(!isnull(r.other_payers_id),cap_first(op.name),if(!isnull(r.employees_empno),cap_first(concat(e2.lname,', ',e2.fname)),if(!isnull(r.inhouse_id),o.office,'invalid payor')))) 
						) like '%{$payor}%' " : "")
			.($chk4=='1' ? " and teller_code = '{$teller_code}' " : "")
			.($chk5=='1' ? " and machine_ip = '{$machine_ip}' " : "")
			." 	ORDER BY 
					transaction_date, receipt_no 
			";

		if ($chk1 !=="1" AND $chk2 !=="1" AND $chk3 !=="1" AND $chk4 !=="1" AND $chk5 !=="1")
			$sql="select from payments where receipt_no='-1' ";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query){
			return $query->result();
		}else{
			return false;
		}
		
	}
	
	function paying_units($program_id, $yr_level, $end_year, $term){
		$sql="
			select ay.end_year, tr.term, h.students_idno as idno, cap_first(concat(s.lname,', ',s.fname)) as student, concat(h.year_level, ' ',ap.abbreviation) as program, sum(ifnull(c.paying_units,0)) as pay_units
			from enrollments e
			join course_offerings co on co.id = e.course_offerings_id
			join courses c on c.id = co.courses_id
			join student_histories h on h.id = e.student_history_id
			join students s on s.idno = h.students_idno
			join academic_terms tr on tr.id = h.academic_terms_id  -- and tr.id =455
			join academic_years ay on ay.id = tr.academic_years_id  
			join prospectus p on p.id = h.prospectus_id
			join academic_programs ap on ap.id = p.academic_programs_id
			where ap.id = '{$program_id}' and h.year_level = '{$yr_level}' 
				and ay.end_year =  '{$end_year}' and tr.term = '{$term}'
			group by h.students_idno
			order by pay_units,student
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query){
			return $query->result();
		}else{
			return false;
		}
	}

	function paying_units_by_program($program_id, $end_year, $term){
		$sql="
		select ay.end_year, tr.term, h.students_idno as idno, cap_first(concat(s.lname,', ',s.fname)) as student, concat(h.year_level, ' ',ap.abbreviation) as program, sum(ifnull(c.paying_units,0)) as pay_units
		from enrollments e
		join course_offerings co on co.id = e.course_offerings_id
		join courses c on c.id = co.courses_id
		join student_histories h on h.id = e.student_history_id
		join students s on s.idno = h.students_idno
		join academic_terms tr on tr.id = h.academic_terms_id  -- and tr.id =455
		join academic_years ay on ay.id = tr.academic_years_id
		join prospectus p on p.id = h.prospectus_id
		join academic_programs ap on ap.id = p.academic_programs_id
		where ap.id = '{$program_id}' and h.year_level = '{$yr_level}'
		and ay.end_year =  '{$end_year}' and tr.term = '{$term}'
		group by h.students_idno
		order by pay_units,student
		";
		$query = $this->db->query($sql);
		if ($query){
		return $query->result();
	}else{
			return false;
		}
	}
	
	
	function list_tuition_fees_total($year_start, $year_end){		
		$sql = "
			select ay.end_year, tr.`term`, cl.college_code
				,sum(c.paying_units * fs.rate) as tuition_total
				,sum(if(!isnull(e.withdrawn_percent ), c.paying_units * (1 - (e.withdrawn_percent/100)) ,  c.paying_units) * fs.rate)  as tuition_total_wd
				,'0.00' as reed_total
			from enrollments e
			join student_histories h on h.id = e.student_history_id
			join course_offerings co on co.id = e.course_offerings_id
			join courses c on c.id = co.courses_id
			join prospectus p on p.id = h.prospectus_id
			join academic_programs ap on ap.id = p.academic_programs_id
			join acad_program_groups ag on ag.id = ap.acad_program_groups_id
			join fees_schedule fs on fs.fees_subgroups_id=201
				and fs.acad_program_groups_id = ag.id 
				and fs.academic_terms_id = h.academic_terms_id
				and fs.levels_id = ag.levels_id
				and fs.yr_level = h.year_level
				and c.id not in (select distinct courses_id from tuition_others)
			join academic_terms tr on tr.id = h.academic_terms_id
			join academic_years ay on ay.id = tr.academic_years_id
			join colleges cl on cl.id = ap.colleges_id
			where 
				ay.end_year between '{$year_start}' AND '{$year_end}'				
			group by ap.colleges_id, tr.id
			order by ay.end_year, tr.`term`, college_code				
		";
		
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query){
			return $query->result();
		}else{
			return false;
		}		
	}
	
	function list_reed_fees_total($year_start, $year_end){
		$sql = "				
			select ay.end_year, tr.`term`, cl.college_code
				,sum(c.paying_units * tu.rate) as reed_total
				,sum(if(!isnull(e.withdrawn_percent ), c.paying_units * (1 - (e.withdrawn_percent/100)) ,  c.paying_units) * tu.rate)  as reed_total_wd
			from enrollments e
			join student_histories h on h.id = e.student_history_id
			join course_offerings co on co.id = e.course_offerings_id
			join courses c on c.id = co.courses_id and lower(substr(c.course_code,1,4))='reed'
			join prospectus p on p.id = h.prospectus_id
			join academic_programs ap on ap.id = p.academic_programs_id
			join acad_program_groups ag on ag.id = ap.acad_program_groups_id
			join tuition_others tu on tu.courses_id = c.id and tu.academic_terms_id = h.academic_terms_id and tu.levels_id=ag.levels_id and tu.yr_level=h.year_level
			join academic_terms tr on tr.id = h.academic_terms_id
			join academic_years ay on ay.id = tr.academic_years_id
			join colleges cl on cl.id = ap.colleges_id							
			where
				ay.end_year between '{$year_start}' AND '{$year_end}'
			group by ap.colleges_id, tr.id
			order by ay.end_year, tr.`term`, college_code
		";
		
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query){
			return $query->result();
		}else{
			return false;
		}
	}

	
	function write_off_list($department, $wo_type){
		$sql = "	
			select * from (
				select py.id as payers_id, w.idno,concat(s.lname,', ',s.fname) as student,
				if(!isnull(t.students_idno), 6, (SELECT bh.levels_id
														   FROM basic_ed_histories bh
														   LEFT JOIN levels v ON v.id = bh.levels_id 
														   where bh.students_idno=w.idno
														   order by bh.students_idno, bh.academic_years_id desc limit 1)) as levels_id,
				coalesce(t.year_level,(SELECT bh.yr_level
											  FROM basic_ed_histories bh
											  LEFT JOIN levels v ON v.id = bh.levels_id 
											  where bh.students_idno=w.idno
											  order by bh.students_idno, bh.academic_years_id desc limit 1)) as year_level,
				coalesce(c.college_code, (SELECT v.`level`
												  FROM basic_ed_histories bh
												  LEFT JOIN levels v ON v.id = bh.levels_id 
												  where bh.students_idno=w.idno
												  order by bh.students_idno, bh.academic_years_id desc limit 1)) as department,
				w.balance,w.last_transaction,w.last_transaction2, ' ' as first_transaction,				
				w.`status`
				from " . $this->session->userdata('write_off_table') . " w
				left join (
				      select max(h.year_level) as year_level, 
					  h.students_idno,
					  h.prospectus_id from student_histories h group by h.students_idno desc 
				) t on t.students_idno = w.idno 
				left join prospectus p on p.id = t.prospectus_id
				left join academic_programs ap on ap.id = p.academic_programs_id
				left join colleges c on c.id = ap.colleges_id
				left join students s on s.idno = w.idno
				join (	select ll.idno
						from ledger ll
						where ll.`status` ='active'
						group by ll.idno
				) tt on tt.idno = w.idno
				left join payers py on py.students_idno=w.idno
				where w.`status` ='active' 
			) xx 
			
			where balance "
			
				. ($wo_type == 'Receivable' ?  " < -1.0 " : " > 1.0 ")
				. ($department == 'College' ?  " and levels_id='6' ": " and levels_id !='6' and department = '{$department}' ").												
				" order by department, year_level, student								
		";
	
		log_message("INFO", 'ANHI'); // Toyet 5.11.2018
		log_message("INFO", print_r($sql,true)); // Toyet 5.11.2018

		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function write_off_list_slow($department, $wo_type){
		$sql = "
			select idno,student,year_level,department,date_format(last_transaction,'%b %d %Y') as last_transaction,balance,active_balance from 
				(
				select s.idno, concat(s.lname,', ',s.fname) as student, 
					if(!isnull(ht.students_idno),ht.year_level, ifnull(bht.yr_level, '')) as year_level, 
					if(!isnull(ht.students_idno),cl.college_code, ifnull(l.`level`, '')) as department,	
					td.last_transaction, td.balance, active_balance
				from 
					( select * from (
							select  p.payers_id, max(p.transaction_date) as last_transaction, 
								sum(p.receipt_amount) as balance, 
								sum(if(p.isis_status='active',p.receipt_amount,0)) as active_balance 
							from payments p
							where p.from_isis = 'Y' and p.isis_status !='void' 
							group by p.payers_id
							) tdnew
						where balance !=0 and last_transaction < '2011-04-01 00:00:00' and active_balance "
							. ($wo_type == 'Receivable' ? " < 0 " : " >= 0 " ).												
					") td
				join payers r on r.id = td.payers_id
				join students s on s.idno = r.students_idno
				left join
						(
						select h.students_idno, max(h.academic_terms_id) as academic_terms_id, h.prospectus_id, h.year_level
						from student_histories h
						group by h.students_idno
						order by h.students_idno
						) ht on ht.students_idno = s.idno 
				left join prospectus pr on pr.id = ht.prospectus_id 
				left join academic_programs ap on ap.id = pr.academic_programs_id
				left join colleges cl on cl.id = ap.colleges_id			
				left join 
						(
						select bh.students_idno, max(bh.academic_years_id) as `academic_years`, bh.levels_id, bh.yr_level
						from basic_ed_histories bh
						group by bh.students_idno
						order by bh.students_idno
						) bht on bht.students_idno = s.idno 
				left join levels l on l.id = bht.levels_id 
				) ttt			 
			where department = '{$department}' 
			order by department, year_level, student
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query){
			return $query->result();
		}else{
			return false;
		}		
	}
	
	function get_current_end_year(){
		$sql ="
				select end_year from academic_years where status = 'current'
		";
		$query = $this->db->query($sql);

		if ($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
				
		
	}
	
}
