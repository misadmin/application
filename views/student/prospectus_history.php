<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>

<div class="span9" style="width:90%;" />

<div style="background:#FFF; width:100%; margin-left:20px;">
<div style="width:100%;">
	<span style="color:red"><b>NOTE:</b> <i>Grades with asterisk (*) are CREDITED COURSES</i></span>
</div>
<?php $display_note = false;?>
<?php
	if ($prospectus_terms) {
		foreach ($prospectus_terms AS $term) {
?>
<div style="width:100%;">
<p>
<table border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td align="left" style="font-size:16px; font-weight:bold;"><?php print($term->y_level." - ".$term->term); ?>
      <a class="add_course" href="<?php print($term->id); ?>" style="text-decoration:none; font-size:12px; font-weight:bold; color:#666; font-family:Verdana, Geneva, sans-serif;"></a></td>
    </tr>
</table>

</div>
<div style="width:100%; margin-bottom:40px;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover table-condensed" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="15%" class="head" style="vertical-align:middle;">Course Code</th>
      <th width="30%" class="head" style="vertical-align:middle;">Descriptive Title</th>
      <th width="7%" class="head" style="vertical-align:middle;">Cut-off Grade</th>
      <th width="7%" class="head" style="vertical-align:middle;">No. of Retakes</th>
      <th width="27%" class="head" style="vertical-align:middle;">Pre-requisites</th>
      <th width="7%" class="head" style="vertical-align:middle;">Credit Units</th>
      <th width="7%" class="head" style="vertical-align:middle;">GRADE</th>
      </tr>
    </thead>
    <?php
		$courses = $this->Prospectus_Model->ShowProspectusGrades($term->id,$student_idno);
		//$courses = $this->Prospectus_Model->ShowProspectusGrades('1234','05840986');
		//print_r($courses); die();
		if ($courses) {
			foreach ($courses AS $course) {
				$credited_courses = $this->Student_Model->ListCreditedCourses($student_idno, $course->prospectus_courses_id);
				//print_r($credited_courses); die();
				if ($course->grade) {
	?>
					<tr class="success">
					  <td style="text-align:left; vertical-align:middle; "><?php echo str_replace('-','',$course->course_code); ?></td>
					  <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->cutoff_grade); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->num_retakes); ?></td>
					  <td style="text-align:left; vertical-align:middle; ">
					  <?php 

					  $prereqs = $this->Prospectus_Model->ListPrereq_Courses($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->course_code.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  	print("<br>");
					  }
					  $prereqs = $this->Prospectus_Model->ListPrereq_YrLevel($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->pre_req.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  		}
					  
					  ?></td>
					  <td style="text-align:center; vertical-align:middle; ">
					  <?php 
					  		if ($course->is_bracketed == 'Y') {
					  			print('('.$course->credit_units.')'); 
							} else {
								print($course->credit_units);
							}
						?></td>
					  <td align="center" style="text-align:center; vertical-align:middle; ">
					  <?php 
					  	if (strpos($course->grade,'*')){ 
								$display_note = true; //modified by tatskie. So legend will display only when student has credited courses. Asterisks are collored red. 
								
								echo '<sup style="color:red;"><strong>' .substr($course->grade,-1).'</strong></sup>';?>  
							
									<a href="#" data-toggle="modal" data-target="#modal_show_credit_<?php print($course->prospectus_courses_id); ?>"
									data-project-id='<?php print($course->grade); ?>'>
									<i class="icon-zoom-in"></i></a>
						  						  					 
						  					
						  	 <!-- modal for showing credited course -->
      
						      <div class="modal hide fade" id="modal_show_credit_<?php print($course->prospectus_courses_id); ?>" >
						 			<form method="post" id="remove_course_form_<?php print($course->prospectus_courses_id); ?>">
						   				<input type="hidden" name="action" value="remove_credited_course" />
						   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							   		<div class="modal-header">
							       		<button type="button" class="close" data-dismiss="modal">�</button>
							       		<h3>Credited Course</h3>
							   		</div>
							   		<div class="modal-body" style="text-align:center;">
							   				<div style="text-align:left; font-weight: bold; font-size:16px;">
							   					<?php 
													print($course->course_code." - ".$course->descriptive_title);
													print("<br><i>".$course->credit_units." Unit/s</i>");
							   					?>
							   				</div>
							   				<table class="table table-condensed table-hover" style="width:100%; border:1px;">
							   				<thead>
							   					<tr>
							   					<th style="width:10%; text-align:center; vertical-align: middle; ">Catalog No.</th>
							   					<th style="width:55%; text-align:center; vertical-align: middle; ">Descriptive Title</th>
							   					<th style="width:10%; text-align:center; vertical-align: middle;">Units</th>
							   					<th style="width:10%; text-align:center; vertical-align: middle; ">Final Grade</th>
							   					<th style="width:15%; text-align:center; vertical-align: middle; ">Where Taken?</th>
							   												   					
							   					</tr>	
							   				</thead>
											<?php 
												foreach($credited_courses AS $ccourse) {
													print("<td style=\"vertical-align:middle;\">".$ccourse->cat_no."</td>");
													print("<td style=\"vertical-align:middle;\">".$ccourse->descriptive_title."</td>");
													print("<td style=\"text-align:center; vertical-align:middle;\">".$ccourse->units."</td>");
													print("<td style=\"text-align:center; vertical-align:middle;\">".$ccourse->final_grade."</td>");
													print("<td style=\"text-align:center; vertical-align:middle;\">".$ccourse->taken_where."</td>");
														
													print("</tr>");
												}
											?>   				
							   			</table>
							   			
					   				</div>
								   <div class="modal-footer">
							    		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							       </div>
							       </form>
						    </div>	
						    
						  <!-- end of modal for showing credited course -->
						  
					<?php 		
						 //	echo substr($course->grade,0,strpos($course->grade,'*')) . '<sup style="color:red;"><strong>' .substr($course->grade,-1).'</strong></sup>';
						}else{ 
							echo $course->grade ;
						}
					?>
						</td>
	  				</tr>
	<?php
				} else {
					$my_code="";
					if ($course->elective == 'Y') {
						$pattern = '/,/';
						$subject = $course->elective_grade;
						$my_code = preg_split($pattern, $subject);
						//print_r($my_code); die();
	?>
					<tr <?php if (isset($my_code[1]) AND ($my_code[1] != 'HNU*')) { print("style=\"background:#FFFFD9\""); }?>>
					  <td style="text-align:left; vertical-align:middle; ">
					  	<?php
					  		if (isset($my_code[1]) AND ($my_code[1] != 'HNU*')) { 
					  			//print_r($my_code); die();
					  			foreach($my_code AS $k=>$v) {
					  				$num = $k/3; //get only the 0,3,6... array
					  				if (floor($num) == $num) {
					  					print($course->course_code.' <i>['.$my_code[$k].']</i><br>');
					  				}
					  			} 
							} else {
				  				print($course->course_code); 
							}
						?></td>
					  <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->cutoff_grade); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->num_retakes); ?></td>
					  <td style="text-align:left; vertical-align:middle; ">
					  <?php 

					  $prereqs = $this->Prospectus_Model->ListPrereq_Courses($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->course_code.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  	print("<br>");
					  }
					  $prereqs = $this->Prospectus_Model->ListPrereq_YrLevel($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->pre_req.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  		}
					  
					  ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->credit_units); ?></td>
					  <td style="text-align:center; vertical-align:middle; ">
					  	<?php
					  		if (isset($my_code[1]) AND ($my_code[1] != 'HNU*')) { 
					  			foreach($my_code AS $k=>$v) {
					  				$num=$k/3;
					  				if (floor($num) == $num) {
				  						print($my_code[$k+1]."<br>");
					  				}
					  			} 
							} else {
				  				print("&nbsp;"); 
							}
					  	?>	
						</td>
					</tr>
	<?php
					} else {
	?>
					<tr>
					  <td style="text-align:left; vertical-align:middle; "><?php echo str_replace('-','',$course->course_code); ?></td>
					  <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->cutoff_grade); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->num_retakes); ?></td>
					  <td style="text-align:left; vertical-align:middle; ">
					  <?php 

					  $prereqs = $this->Prospectus_Model->ListPrereq_Courses($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->course_code.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  	print("<br>");
					  }
					  $prereqs = $this->Prospectus_Model->ListPrereq_YrLevel($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->pre_req.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  		}
					  
					  ?></td>
					  
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->credit_units); ?></td>
					  <td style="text-align:left; vertical-align:middle; ">&nbsp;</td>
					</tr>	
	<?php 		
					}		
				}
			}
		}
	?>
</table>
</div>
<?php
		}
	}	
?>
<?php if ($display_note) echo '<span style="color:red"><b>NOTE:</b> <i>Grades in asterisk(*) are CREDITED COURSES</i></span>' ?>
</div>
</div>
