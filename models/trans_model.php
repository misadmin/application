<?php

	class trans_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		function start_trans() {
		
			$q = "START TRANSACTION";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		
		}
		
		
		function rollback_trans() {

			$q = "ROLLBACK";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		}
		
		
		function commit_trans() {

			$q = "COMMIT";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		
		}
		
	}
	
?>