<?php

class Academic_terms_model extends CI_Model {
	private $last_error;
	
	public function __construct(){
		parent::__construct();
	} 
	
	/**
	 * This function creates a new academic year and designates a start date.
	 * @param year $year (end year of the academic year)
	 * @param date $start_date (beginning date of the academic year)
	 * @return boolean
	 */
	public function create_year($year, $start_date=""){
		$start_year = $year - 1;

		$start_date = (!empty($start_date) ? $start_date : "{$start_year}-{$this->config->item('fiscal_year_start')}");
		$data = array(
				'end_year'=>$this->db->escape($year),
				'start_date'=>$this->db->escape($start_date),
				);		
		$query = $this->db->query( $this->db->insert_string('academic_years', $data) );
		
		if ($this->db->_error_number()) {
			$this->last_error = array(
									'code' =>$this->db->_error_number(),
									'error_description' => $this->db->_error_message(),
								);
			return FALSE;
			
		} else
			return $this->db->insert_id();
					
	}
	
	public function last_error(){
		return $this->last_error;
	}
	
	/**
	 * This function retrieves the details of an academic term.
	 * @param smallint $academic_terms_id
	 * @return boolean
	 */
	public function current_academic_term ($academic_terms_id=0){
		$sql = "
				SELECT
					at.id, at.start_date as semester_start_date,
					ay.end_year, ay.start_date as year_start_date,
					ay.id as sy_id,
				CASE at.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
							END AS term, 
						CONCAT(ay.end_year-1,'-',ay.end_year) AS sy
				FROM
					academic_terms at
				LEFT JOIN
					academic_years ay
				ON
					at.academic_years_id=ay.id
				";
		
		if ( ! empty($academic_terms_id))
			$sql .= "WHERE at.id={$this->db->escape($academic_terms_id)}"; else 
			$sql .= "WHERE at.status='current'";
		
		$sql .= " ORDER by at.id DESC
				LIMIT 1";
		//return $sql;
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->row(); else
			return FALSE;
	}
	
	/**
	 * This function returns the current academic term.
	 * @return NULL
	 */
	public function getCurrentAcademicTerm() {
			//$result = null;
		//	return "kfgdg";
			$sql = "SELECT a.academic_years_id, a.id, 
						a.start_date as semester_start_date, 
						b.start_date as year_start_date,
						CASE a.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
							END AS term, 
						CONCAT(b.end_year-1,'-',b.end_year) AS sy,
						b.end_year,
						a.term as num_term,
						b.id AS sy_id
					FROM 
						academic_terms AS a  
					JOIN academic_years AS b ON a.academic_years_id=b.id
					WHERE 						 
						a.status='current' 
					ORDER BY a.id DESC 
					LIMIT 1	";
			//die($sql);//return $sql;
			$query = $this->db->query($sql);
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}

	function getCurrentBasicTerm() {
			$result = null;
			
			$q1 = "SELECT a.academic_years_id, a.id, 
						b.start_date as year_start_date,
						CONCAT(b.end_year-1,'-',b.end_year) AS sy,
						b.id AS sy_id,
						b.status
					FROM 
						academic_terms AS a, 
						academic_years AS b
					WHERE 
						a.academic_years_id=b.id 
						AND b.status='current' 
						AND a.term=1 ";
			
			$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		
	/**
	 * This function retrieves the IDs of the current academic term and year.
	 */
	//Added by Tatskie on July 16, 2013
	function getCurrentAcademicTermID(){
		$sql = "
			SELECT 
				tr.id as current_academic_term_id,
				ay.id as current_academic_year_id
			FROM 
				academic_terms tr 
			JOIN 
				academic_years ay 
			ON 
				ay.id = tr.academic_years_id
			WHERE 
				tr.`status` = 'current' 
		";
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 1){
			$this->output->set_output("Too many current academic terms!");
		}elseif(($query && $query->num_rows() < 1)){
			$this->output->set_output("Current academic term not yet set!");
		}else{ 
			return $query->row();				
		} 
		return false; 		
	}	
		
	/**
	 * This function returns ONLY the inclusive academic terms of a faculty.
	 * @param smallint $employee_id
	 * @return boolean
	 */
	public function faculty_inclusive_academic_term ($employee_id){
		$sql = "
				select DISTINCT at.id, CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term
					from
				course_offerings    co
				join courses                c  on c.id  = co.courses_id
				join academic_terms         at on at.id = co.academic_terms_id
				join academic_years         ay on ay.id = at.academic_years_id
				where
					co.employees_empno={$this->db->escape($employee_id)}
				order by
				ay.end_year DESC, at.term DESC
				";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
	}
	
	/**
	 * Method to return ONLY the academic terms for a student.
	 * 
	 * @param int $student_id
	 * @return variant result or false if none found...
	 */
	public function student_inclusive_academic_terms ($student_id,$basic_ed=FALSE) {
		//NOTE: This query returns the academic terms including the current which should not be
		if($basic_ed){
			$sql="
				SELECT
				DISTINCT
					sh.id as student_histories_id,
					CONCAT(ay.end_year-1,'-',ay.end_year) AS student_sy,
					ay.id AS academic_years_id
				FROM 
					basic_ed_histories sh
				LEFT JOIN
					academic_years ay
				ON
					ay.id=sh.academic_years_id
				WHERE
					sh.students_idno={$this->db->escape($student_id)}
				ORDER BY
					ay.end_year DESC
				";
		} else {
			$sql = "
				SELECT
				DISTINCT
					sh.id as student_histories_id,
					at.id, CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term,
					CASE at.term
						WHEN 1 THEN '1st Semester' 
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
					END AS student_term,
					CONCAT(ay.end_year-1,'-',ay.end_year) AS student_sy
				FROM 
					student_histories sh
				LEFT JOIN
					academic_terms at
				ON
					at.id=sh.academic_terms_id
				LEFT JOIN
					academic_years ay
				ON
					ay.id=at.academic_years_id
				WHERE
					sh.students_idno={$this->db->escape($student_id)}
				ORDER BY
					ay.end_year DESC, at.term DESC
				";
			}
		
		//log_message("INFO", print_r($sql,true)); // Toyet 6.13.2018
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
	}
	

	/**
	 * Method to return ONLY the academic terms for a student.
	 * ( Basically, similar from above but with an additional field: levels_id
	 *   -by Toyet 8.16.2018 )
	 * @param int $student_id
	 * @return variant result or false if none found...
	 */
	public function stud_incl_acad_terms_levels_id($student_id,$basic_ed=FALSE) {
		//NOTE: This query returns the academic terms including the current which should not be
		if($basic_ed){
			$sql="
				SELECT
				DISTINCT
					sh.id as student_histories_id,
					at.id, 
					CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, 
					if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term,
					CASE at.term
						WHEN 1 THEN '1st Semester' 
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
					END AS student_term,
					CONCAT(ay.end_year-1,'-',ay.end_year) AS student_sy,
					sh.levels_id,
					sh.yr_level as year_level,
					lv.level as program
				FROM basic_ed_histories sh
				LEFT JOIN academic_years ay ON ay.id=sh.academic_years_id
				left join academic_terms at on at.academic_years_id=ay.id
				left join levels lv on lv.id=sh.levels_id
				WHERE
					sh.students_idno={$this->db->escape($student_id)}
				group BY
					ay.end_year DESC
				";
		} else {
			$sql = "
				SELECT
				DISTINCT
					sh.id as student_histories_id,
					at.id, CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term,
					CASE at.term
						WHEN 1 THEN '1st Semester' 
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
					END AS student_term,
					CONCAT(ay.end_year-1,'-',ay.end_year) AS student_sy,
					ag.levels_id,
					sh.year_level,
					ap.abbreviation as program
				FROM 
					student_histories sh
				LEFT JOIN academic_terms at ON at.id=sh.academic_terms_id
				LEFT JOIN academic_years ay ON ay.id=at.academic_years_id
				left join prospectus prs on prs.id=sh.prospectus_id
				left join academic_programs ap on ap.id=prs.academic_programs_id
				left join acad_program_groups ag on ag.id=ap.acad_program_groups_id
				WHERE
					sh.students_idno={$this->db->escape($student_id)}
				ORDER BY
					ay.end_year DESC, at.term DESC
				";
			}
		
		//log_message("INFO", print_r($sql,true)); // Toyet 6.13.2018; 8.16.2018
		
		//print_r($sql); die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
	}
	

		
	/**
	 * This function retrieves the current or previous terms
	 * @param unknown_type $limit
	 * @return boolean
	 */
	public function academic_terms ($limit=0, $include_nulls=TRUE){
		$sql = "
			SELECT
				at.id,
				CASE at.term
					WHEN 1 THEN 'First Sem' 
					WHEN 2 THEN 'Second Sem'
					WHEN 3 THEN 'Summer'
				END AS term, 
				CONCAT(ay.end_year-1,'-',ay.end_year) AS sy	
			FROM
				academic_terms at
			LEFT JOIN
				academic_years ay
			ON
				at.academic_years_id=ay.id
			WHERE
				at.status='current' OR at.status='previous'
			";
		if(!$include_nulls){
			$sql .= "AND NOT isnull(at.term)";	
		}
		$sql .= "
			ORDER BY
				ay.end_year DESC, at.term DESC
			";
		if (!empty($limit)){
			$sql .= " LIMIT {$limit}";
		}

		$query = $this->db->query($sql);
		
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			return FALSE;
		}
	}

	
	public function academic_terms_incoming($limit=0, $include_nulls=TRUE, $current_status=TRUE){
		$sql = "
			SELECT
				at.id,
				CASE at.term
					WHEN 1 THEN 'First Sem'
					WHEN 2 THEN 'Second Sem'
					WHEN 3 THEN 'Summer'
				END AS term,
				CONCAT(ay.end_year-1,'-',ay.end_year) AS sy
			FROM
				academic_terms at
			LEFT JOIN
				academic_years ay
			ON
				at.academic_years_id=ay.id
			WHERE
				at.status='incoming' ";
		
		if ($current_status) {
			$sql .= " OR at.status='current' ";
		}
		 					
		if(!$include_nulls){
			$sql .= " AND NOT isnull(at.term)";
		}
		$sql .= "
			ORDER BY
				ay.end_year DESC, at.term DESC
			";
	
		$query = $this->db->query($sql);
	
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			return FALSE;
		}
	}
	
	
	
	/**
	 * This function retrieves the period based on the time
	 * @param time $time (timestamp)
	 * @return string|boolean
	 */
	public function what_period($time=NULL){
		
		$sql = "
				SELECT
					p.period,
					UNIX_TIMESTAMP(at.start_date) as start_date, 
					UNIX_TIMESTAMP(DATE_ADD(p.end_date, INTERVAL 23 HOUR)) as end_date
				FROM
					periods p
				LEFT JOIN
					academic_terms at
				ON
					at.id=p.academic_terms_id
				WHERE
					at.status='current'
				";
		
	/*	$sql ="			
				select p.period
				from periods p
				left join academic_terms at 
					ON at.id = p.academic_terms_id  				
				where unix_timestamp(now()) between unix_timestamp(p.start_date) and (unix_timestamp(p.end_date)+23*60*60) 
					and at.status='current'  
			";*/
		
		/*
		$query = $this->db->query($sql);
		if ($query AND $query->num_rows() > 0)
			return $query->row()->period;
		else 
			return '';
		*/
		
		
		$query = $this->db->query($sql);
		$time = (is_null($time) ? time() : $time);
		if ($query && $query->num_rows() > 0){
			$rows = $query->result();
			//print_r($rows);die();
			foreach ($rows as $row){
				if ($time > $row->start_date && $time <= $row->end_date) {
					return $row->period;
				}
			}
			return "";
		} else {
			return FALSE;
		}
	}
	
	/**
	 * This function retrieves the minimum enrollment date given an academic term
	 * @param smallint $academic_terms_id
	 * @return boolean
	 */
	public function earliest_enrollment_schedule ($academic_terms_id=NULL){
		
		$sql = "
			SELECT
				MIN(`at`.`start_date`) as start_date
			FROM
				enrollment_schedules es
			LEFT JOIN
				academic_terms `at`
			ON
				es.academic_terms_id=`at`.id
			WHERE
			";
		if(!is_null($academic_terms_id)){
			$sql .= " at.id={$this->db->escape($academic_terms_id)}
			";
		}else{
			$sql .= " at.status='current'
			";
		}
		$sql .= " LIMIT 1";
		$query=$this->db->query($sql);
		if($query && $query->num_rows() > 0){
			$row = $query->row();
			return $row->start_date;
		} else {
			return FALSE;
		}	
	}
	
	/**
	 * This function retrieves the current academic year
	 * @return boolean
	 */
	public function current_academic_year(){
		$sql = "
			SELECT
				ay.id as academic_years_id, 
				CONCAT(ay.end_year - 1, ' - ', ay.end_year) as sy
			FROM
				academic_years ay
			LEFT JOIN
				academic_terms at
			ON
				ay.id=at.academic_years_id
			WHERE
				at.status='current'
			LIMIT 1
			";
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->row();
		} else {
			return FALSE;
		}
	}
	
	/**
	 * This function retrieves the academic year designated as incoming
	 * @return boolean
	 */
	public function incoming_academic_year(){
		$sql = "
			SELECT
				ay.id as academic_years_id, 
				CONCAT(ay.end_year - 1, ' - ', ay.end_year) as sy
			FROM
				academic_years ay
			LEFT JOIN
				academic_terms at
			ON
				ay.id=at.academic_years_id
			WHERE
				ay.status='incoming'
			ORDER BY
				end_year 
			ASC
			LIMIT 1
			";
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->row();
		} else {
			return FALSE;
		}
	}
	
	/**
	 * This function retrieves the current and incoming academic years
	 * @return boolean
	 */
	public function current_and_incoming_year(){
		$sql = "
			SELECT
				ay.id as academic_years_id,
				CONCAT(ay.end_year - 1, ' - ', ay.end_year) as sy
			FROM
				academic_years ay
			LEFT JOIN
				academic_terms at
			ON
				ay.id=at.academic_years_id
			WHERE
				ay.status='incoming' OR ay.status='current'
			ORDER BY
				end_year
			ASC
			";
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			return FALSE;
		}
	}

	//ADDED: 8/5/13 by Genes
	//EDITED: added my_term 8/18/13 by genes
	/**
	 * This function retrieves the academic terms of a given academic year
	 * @param smallint $academic_years_id
	 * @return NULL
	 */
	function ListAcademicTerms($academic_years_id) {
		$result = null;
	
		$q = "SELECT
					a.id,
					CASE a.term
						WHEN 1 THEN '1st Semester'
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
					END AS term,
					a.term AS my_term,
					CONCAT(b.end_year - 1, ' - ', b.end_year) as sy
				FROM
					academic_terms AS a,
					academic_years AS b
				WHERE
					a.academic_years_id = b.id
					AND a.academic_years_id = {$this->db->escape($academic_years_id)}
					AND a.term > 0
				ORDER BY
					a.academic_years_id, 
					a.term ";
		//die($q);
		//log_message("INFO", print_r($q,true));  // Toyet 5.11.2018
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
	}
	
	/**
	 * NOTE: used for BED module
	 * This function retrieves the academic terms of a given academic year for BED
	 * @param smallint $academic_years_id
	 * @return NULL
	 */
	public function listBEDAcadTerms($academic_years_id=NULL) {

		$result = null;
	
		$q = "SELECT
					a.id,
					CASE a.term
						WHEN 1 THEN '1st Semester'
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
					END AS term,
					a.term AS my_term,
					CONCAT(b.end_year - 1, ' - ', b.end_year) as sy
				FROM
					academic_terms AS a,
					academic_years AS b
				WHERE
					a.academic_years_id = b.id
					AND a.academic_years_id = {$this->db->escape($academic_years_id)}
					AND a.term IN (1,2) 
				ORDER BY
					a.academic_years_id, 
					a.term ";

		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
		
	}
	
	
	function is_withinAcadYear($term, $year, $trans_year) {
				
		$sql = "SELECT ay.id
				FROM academic_years ay, academic_terms at
				WHERE ay.id = at.academic_years_id
					AND at.id = {$this->db->escape($term)}
					AND ay.id = {$this->db->escape($year)}
					AND ((ay.end_year-1 <= '{$this->db->escape($trans_year)}') 
						AND (ay.end_year >= '{$this->db->escape($trans_year)}'))";
		
		
		$query = $this->db->query($sql);
					
		if($query && $query->num_rows() > 0){
			return TRUE; }
		else return FALSE;
	}
	
	function period_dates($academic_terms_id=0){
		$this->db->select('p.period,p.start_date,p.end_date')
			->from('periods p');
		if($academic_terms_id==0){
			$this->db->join('academic_terms at', 'at.id=academic_terms_id','left')->where('at.status','current');
		} else {
			$this->db->where('p.academic_terms_id', $academic_terms_id);
		}
		$this->db->order_by('p.start_date');
		$query = $this->db->get();
		if($query && $query->num_rows() > 0)
			return $query->result(); else
			return array();
	}
	
	//Added: 3/12/2014 by Isah
	//@EDITED: 2/10/15 by genes
	//@NOTE: include academic programs
	function listGradDates($academic_term_id) {
		$result = null;
	
		$q = "SELECT
				a.id,
				a.grad_date,
				b.college_code,
				c.abbreviation
			FROM
				graduation_dates AS a, 
				colleges as b,
				academic_programs AS c
			WHERE
				a.academic_programs_id = c.id
				AND c.colleges_id = b.id
				AND a.academic_terms_id =  {$this->db->escape($academic_term_id)}	
			ORDER BY
				a.grad_date, b.college_code";
	
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
	
		return $result;
	}
	
	//@EDITED: 2/10/15 by genes
	//@NOTE: college_id changed to acad_program_id
	function addGradDate($grad_data, $acad_program_id) {
	
		$query = "	INSERT INTO
						graduation_dates (academic_terms_id, academic_programs_id, colleges_id, grad_date)
					VALUES (
							'".$grad_data['acad_term_id']."',
							   {$this->db->escape($acad_program_id)},	
							   0,	
							'".$grad_data['grad_date']."')";
	
		//print($query); die();
		$result = $this->db->query($query);
		return $this->db->insert_id();
	}
	
	function deleteGradDate($id) {
	
		$query = "DELETE FROM
					graduation_dates
				WHERE
					id = {$this->db->escape($id)} ";
	
		//print($query); die();
	
		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
	function getNextSYFirstSemester($current_term_id) {
		$result = null;
	
		$q = "SELECT
					a.id
				FROM
					academic_terms AS a
				WHERE
					a.term = 1 
					AND id > {$this->db->escape($current_term_id)}
				LIMIT 1 ";
	
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
	
		return $result;
		
	}	
	
	function ListTerms_ByArray($array_terms_id=NULL) {
		$result = null;
	
		$q = "SELECT
					a.id,
					CASE a.term
						WHEN 1 THEN '1st Semester'
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
					END AS term,
					a.term AS my_term,
					CONCAT(b.end_year - 1, ' - ', b.end_year) as sy
				FROM
					academic_terms AS a,
					academic_years AS b
				WHERE
					a.academic_years_id = b.id
					AND a.id IN (".$array_terms_id.")
				ORDER BY
					a.academic_years_id, 
					a.term ";

		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
		
	}
	
}

?>