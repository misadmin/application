<?php

$machine_transactions = array();
$transactions = array();
foreach ($records as $record){
	$machine_transactions[$record->machine_ip][$record->teller_code][] = $record;
}

foreach ($machine_transactions as $machine_ip => $value) : 

$Cash_total = 0;
$Check_total = 0;
$Credit_Card_total = 0;
$Debit_Card_total = 0;
$Bank_total = 0;


?>
<style>
	.void {
		color: #ccc;
	}
</style>
<table class="table table-bordered" style="margin-top: 20px">
	<thead>
		<tr>
			<th colspan="8">Machine No: <?php echo $machine_ip; ?></th>
		</tr>
<!-- 
<?php foreach ($payment_methods as $method): ?>
		<tr>
			<td colspan="1"><?php echo $method; ?> Total: </td>
			<td colspan="1" style="text-align: right;"><?php echo number_format(1000, 2); ?></td>
			<td colspan="6"></td>
		</tr>
<?php endforeach; ?>
-->
		<tr>
			<th>OR Date Time</th>
			<th>OR Number</th>
			<th>Teller ID</th>
			<th>Name</th>
			<th>ID Number </th>
			<th>Code</th>
			<th>Amount</th>
			<th>PayMethod</th>
		</tr>
	<tbody>
	</thead>
<?php foreach ($value as $teller_code => $transactions): ?>
		<tr><td colspan="7"><?php echo $teller_code; ?></td></tr>
<?php 
	$code_amount_total = 0;

	
	
	foreach ($transactions as $transaction): 
		$code_amount_total += $transaction->amount;
		switch ($transaction->payment_method){
			case 'Cash':
				$Cash_total += $transaction->amount;
				break;
			case 'Check':
				$Check_total += $transaction->amount;
				break;
			case 'Credit Card':
				$Credit_Card_total += $transaction->amount;
				break;
			case 'Debit Card':
				$Debit_Card_total += $transaction->amount;
				break;
			case 'Bank':
				$Bank_total += $transaction->amount;
				break;
			default:
				echo "toinks!";
				die();
		}		 
?>
		<tr <?php echo ($transaction->status=='void' ? 'class="void"' : ""); ?>>
			<td><?php echo $transaction->transaction_date; ?><?php echo ($transaction->status=='void' ? ' (VOIDED)' : ""); ?></td>
			<td><?php echo $transaction->receipt_no; ?></td>
			<td><?php echo $transaction->teller; ?></td>
			<td><?php echo $transaction->payor; ?></td>
			<td><?php echo $transaction->payor_id; ?></td>
			<td><?php echo $transaction->teller_code; ?></td>
			<td style="text-align: right"><?php echo number_format($transaction->amount, 2); ?></td>
			<td><?php echo $transaction->payment_method; ?></td>
		</tr>
<?php endforeach; ?>
		<tr>
			<td colspan="5">&nbsp;</td>
			<td><?php echo $teller_code; ?> Sub Total:</td>
			<td style="text-align:right"><?php echo number_format($code_amount_total, 2); ?></td>
			<td>&nbsp</td>
		</tr>
		
		
		
<?php endforeach; ?>
		<tr><td colspan="8"><strong>Summary:</strong></td></tr>
		<?php 
			echo $Cash_total != 0 ? '<tr><td colspan="1">Cash Total: </td><td colspan="1" style="text-align: right;">'. number_format($Cash_total, 2).'</td><td colspan="6"></td></tr>':'';		
			echo $Check_total!= 0 ? '<tr><td colspan="1">Check Total: </td><td colspan="1" style="text-align: right;">'. number_format($Check_total, 2).'</td><td colspan="6"></td></tr>':'';
			echo $Credit_Card_total!=0 ? '<tr><td colspan="1">Credit Card Total: </td><td colspan="1" style="text-align: right;">'. number_format($Credit_Card_total, 2).'</td><td colspan="6"></td></tr>':'';
			echo $Debit_Card_total!=0 ? '<tr><td colspan="1">Debit Card Total: </td><td colspan="1" style="text-align: right;">'. number_format($Debit_Card_total, 2).'</td><td colspan="6"></td></tr>':'';
			echo $Bank_total !=0 ? '<tr><td colspan="1">Bank Total: </td><td colspan="1" style="text-align: right;">'. number_format($Bank_total, 2). '</td><td colspan="6"></td></tr>':'';
		?> 

	</tbody>
</table>

<?php
	echo "<br><br>"; 
	endforeach; 
?>