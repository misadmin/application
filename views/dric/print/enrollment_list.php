<div class="row-fluid span12">
	<div class="form-horizontal">
		<fieldset id="enrollment_fieldset">
			<div class="control-group formSep">
				<label class="control-label" for="academic_terms">Academic Term</label>
				<div class="controls">
					<select name="academic_term" class="print_selector">
						<option value="">-- Select Academic Term --</option>
							<?php foreach ($academic_terms as $academic_term): ?>
							<?php if(!is_null($academic_term->term)): ?>
							<option value="<?php echo $academic_term->id; ?>"><?php echo $academic_term->term . "-" . $academic_term->sy; ?></option>
							<?php endif; ?>
							<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="colleges">College</label>
				<div class="controls">
					<select name="college" class="print_selector">
						<option value="">-- Select College --</option>
							<?php foreach ($colleges as $college): ?>
							<option value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
							<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="programs">Program</label>
				<div class="controls">
					<select name="program" class="print_selector">
						<option value="">-- Select Program --</option>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="year_level">Year Level</label>
				<div class="controls">
					<select name="year_level" class="print_selector">
						<option value="">-- Select Year Level --</option>
						<?php for($count = 1; $count<=5; $count++ ): ?>
						<option value="<?php echo $count; ?>"><?php echo $count; ?></option>
						<?php endfor; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="controls">
					<button class="btn btn-success disabled print_switch" data-what="enrollment_list">Print Enrollment List</button>
					<button class="btn btn-success disabled print_switch" data-what="enrollment_list" data-how="pdf">Save PDF</button>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<script>
function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}
</script>