<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<body onLoad="document.edit_class.max_stud.focus()">

 <div style="background:#FFF; width:40%; text-align:center; margin:auto;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head" style="text-align:left; padding:10px;">
  EDIT CLASS SIZE </td>
  </tr>
  <tr>
    <td colspan="3">
<form action="<?php echo site_url('dean/offerings');?>" method="post" name="edit_class" >
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="16" />
  
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
    <tr>
      <td align="left" valign="top">Academic Terms</td>
      <td align="left" valign="top">:</td>
      <td align="left"><?php print($academic_terms->term." ".$academic_terms->sy); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top">Course [Section]</td>
      <td align="left" valign="top">:</td>
      <td align="left"><?php print($offerings->course_code."[".$offerings->section_code."]"); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top">Schedule</td>
      <td align="left" valign="top">:</td>
      <td align="left"><?php
  		foreach($offering_slot[0] AS $slot) {
			print($slot->tym." ".$slot->days_day_code." [".$slot->room_no."]");
			print("<br>");
		}
  
  ?></td>
    </tr>
    <tr>
      <td align="left">Enrollee/s</td>
      <td align="left">:</td>
      <td align="left"><?php print($enrollee); ?></td>
    </tr>
    <tr>
  <td align="left">Class Size </td>
  <td align="left">:</td>
  <td align="left"><input name="max_stud" type="text" id="max_stud" maxlength="10" value="<?php print($max_stud); ?>" style="width:20px;"> </td>
</tr>
<tr>
  <td width="118" align="left">&nbsp;</td>
  <td width="6" align="left">&nbsp;</td>
  <td width="372" align="left"><input type="submit" name="button" id="button" value="Edit Size!"  class="btn btn-success" /></td>
</tr>
    </table>
</form>
    
    </td>
  </tr>
    </table>
    </div>

</body>

<script>
	$('.course_action').click(function(event){
		event.preventDefault(); 
		var offering_id = $(this).attr('href');
		var courses_id = $(this).attr('courses_id');
		var parallel_no = $(this).attr('parallel_no');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'offering_id',
		    value: offering_id,
		}).appendTo('#srcform2');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'courses_id',
		    value: courses_id,
		}).appendTo('#srcform2');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'parallel_no',
		    value: parallel_no,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>
