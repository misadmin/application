<div class="row-fluid span12">
	<div class="form-horizontal">
		<fieldset id="grades_fieldset">
			<div class="control-group formSep">
				<label class="control-label" for="academic_terms">Academic Term</label>
				<div class="controls">
					<select name="academic_term" class="print_selector">
						<option value="">-- Select Academic Term --</option>
							<?php foreach ($academic_terms as $academic_term): ?>
							<?php if(!is_null($academic_term->term)): ?>
							<option value="<?php echo $academic_term->id; ?>"><?php echo $academic_term->term . "-" . $academic_term->sy; ?></option>
							<?php endif; ?>
							<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="colleges">College</label>
				<div class="controls">
					<select name="college" class="print_selector">
						<option value="">-- Select College --</option>
							<?php foreach ($colleges as $college): ?>
							<option value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
							<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="programs">Program</label>
				<div class="controls">
					<select name="program"  class="print_selector">
						<option value="">-- Select Program --</option>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="year_level">Year Level</label>
				<div class="controls">
					<select name="year_level" class="print_selector">
						<option value="">-- Select Year Level --</option>
						<?php for($count = 1; $count<=12; $count++ ): ?>
						<option value="<?php echo $count; ?>"><?php echo $count; ?></option>
						<?php endfor; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="controls">
					<button class="btn btn-success disabled print_switch" data-what="grades">Print Grades</button>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<script>
$().ready(function(){
	$('select[name="college"]').on('change', function(){
		var college_id = $(this).val();
		var that = $(this);
		$.ajax({
			url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
			dataType: "json",
			success: function(data){
				var doptions = make_options(data);
				that.parents('fieldset').find('select[name="program"]').html(doptions);
			}	
		});	
	});
	$('.print_switch').on('click', function(){
		if($(this).hasClass('disabled'))
			return false;
		var what = $(this).data('what');

		if($(this).data('how') == 'pdf'){
			var url = "<?php echo site_url('dric/pdf'); ?>/" + what + "/" + $(this).parents('fieldset').find('select[name="academic_term"]').val();
			url += "/" + $(this).parents('fieldset').find('select[name="program"]').val() + "/" + $(this).parents('fieldset').find('select[name="year_level"]').val();
			console.log(url);
			window.location.href = url;
		} else {
			var url = "<?php echo site_url('printer'); ?>/" + what + "/" + $(this).parents('fieldset').find('select[name="academic_term"]').val();
			url += "/" + $(this).parents('fieldset').find('select[name="program"]').val() + "/" + $(this).parents('fieldset').find('select[name="year_level"]').val();
			window.open(url, 'printgrade', "height=300,width=400");
		}
	});

	$('.print_selector').on('change', function(){
		var err_found = false;
		var that = $(this);
		that.parents('fieldset').find('.print_selector').each(function(){
			if($(this).val()=='') {
				err_found = true;
				$(this).parents('fieldset').find('.print_switch').addClass('disabled');
			}
		});
		if(!err_found)
			that.parents('fieldset').find('.print_switch').removeClass('disabled');
	});
});
</script>
