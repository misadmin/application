<?php //print_r(get_defined_vars());die();
?>
<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='0') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
		<form id="student_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
			
				<div class="control-group formSep">
					<label for="familyname" class="control-label">Family Name</label>
					<div class="controls">
						<input type="text" id="familyname" name="familyname" class="input-xlarge" value="<?php if( ! empty($firstname)) echo $familyname; ?>" />
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="firstname" class="control-label">First Name</label>
					<div class="controls">
						<input type="text" id="firstname" name="firstname" class="input-xlarge" value="<?php if( ! empty($firstname)) echo $firstname; ?>" />
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="middlename" class="control-label">Middle Name</label>
					<div class="controls">
						<input type="text" id="middlename" name="middlename" class="input-xlarge" value="<?php if( ! empty($firstname)) echo $middlename; ?>" />
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="gender" class="control-label">Sex</label>
					<div class="controls">
						<select name="gender" id="gender" class="span2">
							<?php foreach ($this->config->item('genders') as $key=>$val): ?>
											<option value="<?php echo $key; ?>" <?php if($key==$gender) echo ' selected="selected"'; ?>><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="birthdate" class="control-label">Date of Birth</label>
					<div class="controls">
						<div class="input-append date" id="dp2" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
							<input type="date" name="birthdate" value="<?php  if( ! empty($birthdate)) echo $birthdate; else echo date('Y-m-d'); ?>" >
						<!-- 	<input type="date" name="birthdate" id="birthdate" class="span6" value="<?php // if( ! empty($birthdate)) echo $birthdate; else echo date('Y-m-d'); ?>" readonly/>  -->
							
						</div>
						
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="civil_status" class="control-label">Civil Status</label>
					<div class="controls">
						<select name="civil_status" id="civil_status" class="span2">
<?php foreach ($this->config->item('civil_statuses') as $val): ?>
							<option value="<?php echo $val; ?>" <?php if($val==$civil_status) echo ' selected="selected"'; ?>><?php echo $val; ?></option>
<?php endforeach; ?>
						</select>
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="citizenship" class="control-label">Nationality</label>
					<div class="controls">
						<select name="citizenship" id="citizenship" class="span2">
<?php foreach ($citizenships as $val): ?>
							<option value="<?php echo $val->id; ?>" <?php if($val->id==$citizenships_id) echo ' selected="selected"'; ?>><?php echo $val->citizenship; ?></option>
<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="religion" class="control-label">Religion</label>
					<div class="controls">
						<select name="religion" id="religion" class="span3">
	<?php foreach ($religions as $val): ?>
							<option value="<?php echo $val->id; ?>" <?php if($val->id==$religions_id) echo ' selected="selected"'; ?>><?php echo $val->religion; ?></option>
		<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="control-group formSep">
					<label for="phone_number" class="control-label">Phone No.:</label>
					<div class="controls" id="phone_number">
						<input type="text" name="phone_number" id="phone_number" class="input-xlarge" value="<?php if(isset($phone_number)) echo $phone_number; ?>" />
					</div>
				</div>
				
				
				<div class="control-group">
					<div class="controls">
						<button id="update_information" class="btn btn-primary" type="submit">Update Information</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script>
$(function(){
	$('#dp2').datepicker();
});

$(document).ready(function(){
	$.validator.addMethod(
	        "regex",
	        function(value, element, regexp) {
	            var re = new RegExp(regexp);
	            return this.optional(element) || re.test(value);
	        },
	        "Unacceptable values found"
	);
	
	$('#student_information_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			firstname: { required: true},
			familyname: { required: true},
			birthdate: { required: true}
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	/** 
	 $("#firstname").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
	 $("#familyname").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
	 $("#middlename").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" }); 
	 **/
});
</script>