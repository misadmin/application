

	<div id="class_list_items">
		<h2 class="heading">Class List</h2>

		<div id="rounded_header" style="height:32px; margin-left: unset;">

			<div style="float:right;">
				<div style="float:left;">
					<select name="block_sections_id" id="block_sections_id" style="margin:0px; margin-right: 10px;">
						<?php
							if ($sections) {
								foreach($sections AS $section) {
						?>
									<option value="<?php print($section->id); ?>" <?php if($selected_section_id == $section->id) print("selected"); ?> >
										<?php print($section->section_name); ?> </option>	
						<?php 
								}
							}
						?>					
					</select>
				
				</div>

				<div style="float:left;">
					<select name="prospectus_id" class="levels_id" id="prospectus_id" style="margin:0px; margin-right: 10px;">
						<?php
							foreach($prospectuses AS $prospectus) {
						?>
								<option value="<?php print($prospectus->id); ?>" <?php if($selected_prospectus_id == $prospectus->id) print("selected"); ?>
										academic_programs_id="<?php print($prospectus->academic_programs_id); ?>" >
										<?php print($prospectus->description); ?> </option>	
						<?php 
							}
						?>					
					</select>
				
				</div>
				
				<div style="float:left;">
					<select name="academic_year_id" class="levels_id" id="academic_year_id" style="margin:0px;">
						<?php
							foreach($school_years AS $school_year) {
						?>
								<option value="<?php print($school_year->id); ?>" <?php if($selected_academic_year_id == $school_year->id) print("selected"); ?> >
										<?php print($school_year->sy); ?> </option>	
						<?php 
							}
						?>					
					</select>
				
				</div>
				
			</div>

		</div>

		<div id="show_extracted_students">
			<?php 
				$this->load->view('bed/reports/students_class_list_details');
			?>
		</div>
	</div>
	
	
	<script>
		$('.levels_id').bind('change', function(){
				
				var block_sections_id 		= $("select#block_sections_id option:selected").attr('value');
				var prospectus_id 			= $("select#prospectus_id option:selected").attr('value');
				var academic_programs_id 	= $("select#prospectus_id option:selected").attr('academic_programs_id');
				var academic_year_id 		= $("select#academic_year_id option:selected").attr('value');
				
				show_loading_msg();
				
				$.ajax({
					cache: false,
					type: 'POST',
					url: "<?php echo site_url($this->uri->segment(1).'/bed_class_list'); ?>",
					data: {"block_sections_id": block_sections_id,
							"prospectus_id": prospectus_id,
							"academic_programs_id": academic_programs_id,
							"academic_year_id": academic_year_id,
							"action": "list_selected", 
						},
					dataType: 'json',
					success: function(response) {													

						$('#class_list_items').html(response.output); 
						
						$(".loading").hide();
						
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
				});
				
			});
	</script>


	<script>
		$("select#block_sections_id").change(function(){
				
			var block_sections_id 		= $("select#block_sections_id option:selected").attr('value');
			var prospectus_id 			= $("select#prospectus_id option:selected").attr('value');
			var academic_programs_id 	= $("select#prospectus_id option:selected").attr('academic_programs_id');
			var academic_year_id 		= $("select#academic_year_id option:selected").attr('value');
				
			show_loading_msg();
				
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/bed_class_list'); ?>",
				data: {"block_sections_id": block_sections_id,
						"prospectus_id": prospectus_id,
						"academic_programs_id": academic_programs_id,
						"academic_year_id": academic_year_id,
						"action": "list_selected_by_section", 
					},
				dataType: 'json',
				success: function(response) {													

					$('#class_list_items').html(response.output); 
						
					$(".loading").hide();
						
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
				
		});
	</script>
