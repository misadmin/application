<?php

	class Shs_student_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

		private $students_idno = NULL;
		
		
		function get_students_idno() {
			return $this->students_idno;
		}
		
		
		function My_AcadTerms_toRegister($idnum) {
			
			$result=NULL;

			$sql = "SELECT
							a.id,
							CASE a.term
								WHEN 1 THEN 'First Sem'
								WHEN 2 THEN 'Second Sem'
								WHEN 3 THEN 'Summer'
							END AS term,
							CONCAT(b.end_year-1,'-',b.end_year) AS sy
				FROM
					academic_terms a,
					academic_years b
				WHERE
					a.academic_years_id=b.id 
					AND a.status IN ('current','incoming') 
					AND a.id NOT IN (SELECT 
											x.academic_terms_id 
										FROM 
											student_histories AS x 
										WHERE 
											x.students_idno = {$this->db->escape($idnum)}
									) 
				ORDER BY
					b.end_year DESC, 
					a.term ";
			
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		
		function AddNewStudent_ToSHSStudent($data) {

			$q1 = "INSERT INTO
							students (lname, 
										fname, 
										mname, 
										gender,
										dbirth,
										student_type)
						VALUES (
							{$this->db->escape($data['lname'])},
							{$this->db->escape($data['fname'])},
							{$this->db->escape($data['mname'])},
							{$this->db->escape($data['gender'])},
							{$this->db->escape($data['dbirth'])},
							{$this->db->escape($data['student_type'])} ) ";
			//die($q1);
			if ($this->db->query($q1)) {
				$this->students_idno   = $this->db->insert_id();
				$data['students_idno'] = $this->students_idno;
				
				if ($this->AddStudent_ToHistories($data)) { //add to student_histories table
					if ($this->AddStudent_PhoneNumber($data)) {
						if ($this->AddNewStudent_ToPayers($data)) {
							return TRUE;
						} else {
							return FALSE;						
						}
					} else {
						return FALSE;
					}
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
			
		}

		
		public function AddStudent_ToHistories($data) {
			
			$q1 = "INSERT INTO
						student_histories (students_idno,
												academic_terms_id,
												prospectus_id,
												year_level,
												inserted_on,
												inserted_by)
						VALUES (
							{$this->db->escape($data['students_idno'])},
							{$this->db->escape($data['academic_terms_id'])},
							{$this->db->escape($data['prospectus_id'])},
							{$this->db->escape($data['year_level'])},
							NOW(),
							{$this->db->escape($data['inserted_by'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}	
			
		}


		public function AddStudent_PhoneNumber($data) {
			
			$q1 = "INSERT INTO
						phone_numbers (students_idno,
												phone_type,
												owner_type,
												phone_number,
												is_primary)
						VALUES (
							{$this->db->escape($data['students_idno'])},
							{$this->db->escape($data['phone_type'])},
							{$this->db->escape($data['owner_type'])},
							{$this->db->escape($data['phone_number'])},
							{$this->db->escape($data['is_primary'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}	
			
		}
		
		
		private function AddNewStudent_ToPayers($data) {
			
			$q1 = "INSERT INTO
						payers (students_idno)
						VALUES (
							{$this->db->escape($data['students_idno'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}	
			
		}
		
		
		function UpdateCan_Enroll_StudentHistories($student_histories_id) {
			$q1 = "UPDATE 
							student_histories
						SET 
							can_enroll = 'Y'
						WHERE
							id = {$this->db->escape($student_histories_id)} ";
			//return $q1;			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
				
		}
		

		function AssignToSection_StudentHistories($data) {
			$q1 = "UPDATE 
							student_histories
						SET 
							block_sections_id = {$this->db->escape($data['block_sections_id'])}
						WHERE
							id = {$this->db->escape($data['student_histories_id'])} ";
			//return $q1;			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
				
		}

		
		function AssignToStrand_StudentHistories($data) {
			$q1 = "UPDATE 
							student_histories
						SET 
							prospectus_id = {$this->db->escape($data['prospectus_id'])},
							block_sections_id = {$this->db->escape($data['block_sections_id'])}
						WHERE
							id = {$this->db->escape($data['student_histories_id'])} ";
			//return $q1;			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
				
		}

		
		function SearchStudent($data) {
			$result = null;
			
			if (!$data['fname']) {
				return $result;
			}
			
			$q1 = "SELECT 
							a.idno,
							a.fname,
							a.lname,
							a.mname,
							a.gender
						FROM 
							students AS a 
						WHERE 
							a.lname = {$this->db->escape($data['lname'])}
							AND a.fname LIKE '%".$data['fname']."%' ";
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		
		function ListTermsToEnroll($students_idno) {
			$result = null;
			
			$q1 = "SELECT 
						c.academic_terms_id, 
						CASE a.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
							END AS term, 
						CONCAT(b.end_year-1,'-',b.end_year) AS sy,
						c.id AS student_histories_id
					FROM 
						student_histories AS c,
						academic_terms AS a, 
						academic_years AS b
					WHERE 
						a.academic_years_id = b.id 
						AND c.academic_terms_id = a.id 
						AND c.can_enroll = 'N' 
						ANd c.students_idno = {$this->db->escape($students_idno)} 
					ORDER BY 
						b.end_year,
						a.term desc ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		public function get_StudentHistory_id ($student_id, $academic_term=NULL){
			
			$result=NULL;
			
			$sql = "SELECT
							a.id, 
							a.students_idno,
							a.prospectus_id,
							a.academic_terms_id,
							a.year_level,
							a.can_enroll,
							a.block_sections_id,
							b.academic_programs_id,
							c.acad_program_groups_id,
							d.academic_years_id,
							f.lrn,
							CONCAT('Grade ',a.year_level,' - ',c.description) AS grade,
							CONCAT('(',g.section_name,')') AS section,
							UPPER(CONCAT(h.fname,' ',LEFT(h.mname,1),'. ',h.lname)) AS block_adviser,
							c.abbreviation as strand,
							a.totally_withdrawn,
							CASE d.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
							END AS term
						FROM
							student_histories AS a 
								LEFT JOIN students_lrn AS f ON f.students_idno=a.students_idno 
								LEFT JOIN shs_block_sections AS g ON a.block_sections_id=g.block_sections_id 
								LEFT JOIN employees AS h ON h.empno=g.block_adviser,
							prospectus AS b,
							academic_programs AS c,
							academic_terms AS d
						WHERE
							a.prospectus_id = b.id
							AND b.academic_programs_id = c.id
							AND a.academic_terms_id = d.id
							AND a.students_idno = {$this->db->escape($student_id)} 
							AND	a.academic_terms_id = {$this->db->escape($academic_term)} 
						ORDER BY 
							a.id DESC 
						LIMIT 1 ";
			//return $sql;
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		
		public function getStudentTuitionBasicRate($data) {
			$result=NULL;
			
			$sql = "SELECT
							a.rate
						FROM
							fees_schedule AS a
						WHERE
							a.fees_subgroups_id = 201
							AND a.acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])} 
							AND a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])} 
							AND a.levels_id = 12 
							AND a.yr_level = {$this->db->escape($data['grade_level'])} ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}	
		
		
		public function ListStudentHistories($students_idno) {
			$result=NULL;
			
			$sql = "SELECT
							a.id, 
							a.students_idno,
							a.prospectus_id,
							a.academic_terms_id,
							a.year_level,
							a.can_enroll,
							a.block_sections_id,
							b.academic_programs_id,
							c.acad_program_groups_id,
							d.academic_years_id
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							academic_terms AS d
						WHERE
							a.prospectus_id = b.id
							AND b.academic_programs_id = c.id
							AND a.academic_terms_id = d.id
							AND a.students_idno = {$this->db->escape($students_idno)} 
							AND d.status IN ('current','previous')
						ORDER BY 
							a.id DESC ";
		//return $sql;
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		public function ListMiscFees($data) {
			$result=NULL;
			
			$sql = "SELECT
							b.fees_groups_id,
							c.fees_group
						FROM
							fees_schedule AS a,
							fees_subgroups AS b,
							fees_groups AS c
						WHERE
							a.fees_subgroups_id = b.id 
							AND b.fees_groups_id = c.id
							AND a.fees_subgroups_id != 201
							AND a.acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])} 
							AND a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])} 
							AND a.levels_id = 12 
							AND a.yr_level = {$this->db->escape($data['grade_level'])}  
						GROUP BY 
							c.id 
						ORDER BY 
							c.weight ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();

				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$data['fees_groups_id'] = $res->fees_groups_id;
						
						$myresult = $this->ListFeesSubgroups($data);
						
						if ($myresult) {
							$result[$cnt]->fees_subgroups = $myresult;
						}									
						$cnt++;
					}
				}	
			} 
			
			return $result;
			
		}	

		
		private function ListFeesSubgroups($data) {
			$result=NULL;
			
			$sql = "SELECT
							b.description,
							a.rate
						FROM
							fees_schedule AS a,
							fees_subgroups AS b
						WHERE
							a.fees_subgroups_id = b.id 
							AND b.fees_groups_id = {$this->db->escape($data['fees_groups_id'])}
							AND a.fees_subgroups_id != 201
							AND a.acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])} 
							AND a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])} 
							AND a.levels_id = 12 
							AND a.yr_level = {$this->db->escape($data['grade_level'])}  
						ORDER BY 
							b.description ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();
			
			} 
			
			return $result;
		}


		public function getStudentInfo($student_histories_id) {
			$result=NULL;
			
			$sql = "SELECT
							a.students_idno,
							CONCAT(b.fname,' ',b.mname,' ',b.lname) AS name,
							UPPER(CONCAT(b.lname,', ',b.fname,' ',b.mname)) AS lname_first,
							CONCAT(d.abbreviation,' Grade ',a.year_level) AS program,
							e.section_name,
							a.academic_terms_id,
							b.gender,
							TIMESTAMPDIFF(YEAR,b.dbirth,CURDATE()) AS age
						FROM
							student_histories AS a 
								LEFT JOIN shs_block_sections AS e ON a.block_sections_id = e.block_sections_id, 
							students AS b,
							prospectus AS c,
							academic_programs AS d
						WHERE
							a.students_idno = b.idno 
							AND a.prospectus_id = c.id 
							AND c.academic_programs_id = d.id
							AND a.id = {$this->db->escape($student_histories_id)} ";
//return $sql;
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;

		}		


		function getMyLabFees($academic_years_id, $student_histories_id) {

			$result = NULL;	

			$sql = "SELECT 
							SUM(a.rate) AS rate 
						FROM 
							laboratory_fees AS a 
						WHERE 
							a.academic_years_id = {$this->db->escape($academic_years_id)} 
							AND a.levels_id = 12 
							AND a.courses_id IN (SELECT 
														b.courses_id 
													FROM 
														course_offerings AS b,
														block_course_offerings AS c,
														student_histories AS d
													WHERE 
														b.id = c.course_offerings_id 
														AND c.block_sections_id = d.block_sections_id 
														AND d.id = {$this->db->escape($student_histories_id)}
												) ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}

		
		function getMyMisc_Fee($data) {

			$result = NULL;	

			$sql = "SELECT
							SUM(a.rate) AS rate
						FROM
							fees_schedule AS a
						WHERE
							a.fees_subgroups_id != 201
							AND a.acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])} 
							AND a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])} 
							AND a.levels_id = 12 
							AND a.yr_level = {$this->db->escape($data['grade_level'])} ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}

		function getTotalPayingUnits($student_histories_id) {
			$result = NULL;	

			$sql = "SELECT 
							SUM(a.paying_units) AS p_units
						FROM 
							courses AS a
						WHERE 
							a.id IN (SELECT 
											b.courses_id 
										FROM 
											course_offerings AS b,
											block_course_offerings AS c,
											student_histories AS d
										WHERE 
											b.id = c.course_offerings_id 
											AND c.block_sections_id = d.block_sections_id 
											AND d.id = {$this->db->escape($student_histories_id)} ) ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
				
		}


		// Edit(s):
		//      + added assessed_amount field  -Toyet 11.23.2017
		function checkIf_IsAssessed($student_histories_id) {
			$result = NULL;	

			$sql = "SELECT 
							a.id,
							DATE_FORMAT(a.transaction_date,'%W, %M %e, %Y @ %h:%i%p') AS date_assessed,
							a.assessed_amount
						FROM 
							assessments AS a
						WHERE 
							a.student_histories_id = {$this->db->escape($student_histories_id)} ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}
		
		
		function ListRegisteredNotEnrolled($academic_terms_id=NULL, $academic_programs_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.idno,
							a.lname,
							a.fname,
							a.mname,
							a.gender,
							b.year_level,
							d.description
						FROM 
							students AS a,
							student_histories AS b,
							prospectus AS c,
							academic_programs AS d
						WHERE 
							a.idno = b.students_idno
							AND b.prospectus_id = c.id 
							AND c.academic_programs_id = d.id
							AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} 
			
							AND b.can_enroll = 'N' 
							AND d.colleges_id = 12 ";
			
			if ($academic_programs_id) {
				$sql .= " AND d.id = {$this->db->escape($academic_programs_id)} ";
			}
			
			$sql .= " ORDER BY ";

			if ($academic_programs_id) {
				$sql .= "d.description, ";
			} else {
				$sql .= "b.year_level, ";
			}

			$sql .= "a.lname,
					 a.fname,
					 a.mname ";

			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}

		
		function ListEnrolledNotSectioned($academic_terms_id=NULL, $academic_programs_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.idno,
							a.lname,
							a.fname,
							a.mname,
							a.gender,
							b.year_level,
							d.description							
						FROM 
							students AS a,
							student_histories AS b,
							prospectus AS c,
							academic_programs AS d
						WHERE 
							a.idno = b.students_idno
							AND b.prospectus_id = c.id 
							AND c.academic_programs_id = d.id
							AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND b.can_enroll = 'Y' 
							AND b.block_sections_id IS NULL
							AND d.colleges_id = 12 ";

			if ($academic_programs_id) {
				$sql .= " AND d.id = {$this->db->escape($academic_programs_id)} ";
			}
			
			$sql .= " ORDER BY 
							b.year_level,
							a.lname,
							a.fname,
							a.mname ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}

	
		
/*		function ListClassSchedules($student_histories_id) {

			$result = NULL;	

			$sql = "SELECT 
							e.courses_id,
							a.block_sections_id,
							j.course_code,
							j.descriptive_title,
							CASE h.day_name
								WHEN 'Mon' THEN 1
								WHEN 'Tue' THEN 2
								WHEN 'Wed' THEN 3
								WHEN 'Thu' THEN 4
								WHEN 'Fri' THEN 5
							END AS day_id,
							-- GROUP_CONCAT(CONCAT('<div style=\'width:150px; float:left;\'>',TIME_FORMAT(h.from_time,'%h:%i%p'),'-',TIME_FORMAT(h.to_time,'%h:%i%p'),' ',h.day_name,'</div>[',g.room_no,']') ORDER BY h.from_time SEPARATOR '<br>') AS course_sched,
							CONCAT(k.lname,', ',k.fname) AS teacher
						FROM 
							student_histories AS a,
							block_course_offerings AS d,
							course_offerings AS e 
								LEFT JOIN employees AS k ON e.employees_empno=k.empno,
							course_offerings_slots AS f,
							rooms AS g,
							room_occupancy AS h,
							room_occupancy_course_offerings_slots AS i,
							courses AS j
						WHERE 
							a.block_sections_id = d.block_sections_id 
							AND d.course_offerings_id = e.id 
							AND e.id = f.course_offerings_id 
							AND f.rooms_id = g.id 
							AND g.id = h.rooms_id 
							AND h.id = i.room_occupancy_id 
							AND f.id = i.course_offerings_slots_id
							AND e.courses_id = j.id 
							AND a.id = {$this->db->escape($student_histories_id)} 
						GROUP BY 
							j.id
						ORDER BY 
							h.from_time,
							day_id ";

			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
				
				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						
						$myresult = $this->ListTimeSched($res->block_sections_id, $res->courses_id);
						
						if ($myresult) {
							$result[$cnt]->time_sched = $myresult;
						}									
						$cnt++;
					}
				}	
			}
			
			return $result;
		
		}


		private function ListTimeSched($block_sections_id, $courses_id) {
			$result = NULL;	

			$sql = "SELECT 
							CASE d.day_name
								WHEN 'Mon' THEN 1
								WHEN 'Tue' THEN 2
								WHEN 'Wed' THEN 3
								WHEN 'Thu' THEN 4
								WHEN 'Fri' THEN 5
							END AS day_id,
							CONCAT('<div style=\'width:150px; float:left;\'>',TIME_FORMAT(d.from_time,'%h:%i%p'),'-',TIME_FORMAT(d.to_time,'%h:%i%p'),' ',d.day_name,'</div>[',c.room_no,']') AS course_sched
						FROM 
							rooms AS c,
							room_occupancy AS d,
							room_occupancy_course_offerings_slots AS e,
							block_course_offerings AS f,
							course_offerings AS g,
							course_offerings_slots AS h
						WHERE 
							c.id = d.rooms_id 
							AND d.id = e.room_occupancy_id
							AND f.course_offerings_id = g.id 
							AND g.id = h.course_offerings_id
							AND h.id = e.course_offerings_slots_id
							AND g.courses_id = {$this->db->escape($courses_id)} 
							AND f.block_sections_id = {$this->db->escape($block_sections_id)} 
						ORDER BY 
							d.from_time,
							day_id ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}
*/		
		
		function ListClassSchedules($student_histories_id) {

			$result = NULL;	

			$sql = "SELECT 
							CONCAT(TIME_FORMAT(h.from_time,'%h:%i%p'),'-',TIME_FORMAT(h.to_time,'%h:%i%p')) AS time_sched,
							h.from_time,
							h.day_name,
							e.id,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									courses AS q,
									block_course_offerings AS r,
									student_histories AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = s.block_sections_id
									AND s.id = {$this->db->escape($student_histories_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Mon'
							) AS mon,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s,
									student_histories AS t
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Mon'
									AND p.id = s.course_offerings_id 
									AND t.block_sections_id = s.block_sections_id
									AND t.id = {$this->db->escape($student_histories_id)}
							) AS mon_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p 
										LEFT JOIN employees AS u ON p.employees_empno=u.empno,
									courses AS q,
									block_course_offerings AS r,
									student_histories AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = s.block_sections_id
									AND s.id = {$this->db->escape($student_histories_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Tue'
							) AS tue,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s,
									student_histories AS t
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Tue'
									AND p.id = s.course_offerings_id 
									AND t.block_sections_id = s.block_sections_id
									AND t.id = {$this->db->escape($student_histories_id)}
							) AS tue_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p 
										LEFT JOIN employees AS u ON p.employees_empno=u.empno,
									courses AS q,
									block_course_offerings AS r,
									student_histories AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = s.block_sections_id
									AND s.id = {$this->db->escape($student_histories_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Wed'
							) AS wed,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s,
									student_histories AS t
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Wed'
									AND p.id = s.course_offerings_id 
									AND t.block_sections_id = s.block_sections_id
									AND t.id = {$this->db->escape($student_histories_id)}
							) AS wed_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p 
										LEFT JOIN employees AS u ON p.employees_empno=u.empno,
									courses AS q,
									block_course_offerings AS r,
									student_histories AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = s.block_sections_id
									AND s.id = {$this->db->escape($student_histories_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Thu'
							) AS thu,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s,
									student_histories AS t
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Thu'
									AND p.id = s.course_offerings_id 
									AND t.block_sections_id = s.block_sections_id
									AND t.id = {$this->db->escape($student_histories_id)}
							) AS thu_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p 
										LEFT JOIN employees AS u ON p.employees_empno=u.empno,
									courses AS q,
									block_course_offerings AS r,
									student_histories AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = s.block_sections_id
									AND s.id = {$this->db->escape($student_histories_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Fri'
							) AS fri,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s,
									student_histories AS t
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Fri'
									AND p.id = s.course_offerings_id 
									AND t.block_sections_id = s.block_sections_id
									AND t.id = {$this->db->escape($student_histories_id)}
							) AS fri_teacher
						FROM 
							student_histories AS a,
							block_course_offerings AS d,
							course_offerings AS e,
							course_offerings_slots AS f,
							room_occupancy_course_offerings_slots AS g,
							room_occupancy AS h
						WHERE 
							a.block_sections_id = d.block_sections_id 
							AND d.course_offerings_id = e.id 
							AND e.id = f.course_offerings_id 
							AND f.id = g.course_offerings_slots_id
							AND g.room_occupancy_id = h.id
							AND a.academic_terms_id = e.academic_terms_id
							AND a.id = {$this->db->escape($student_histories_id)} 
						GROUP BY 
							h.from_time
						ORDER BY 
							h.from_time";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}
		

		function ListMyAssesed_Courses($student_histories_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							b.course_code,
							c.descriptive_title,
							b.paying_units,
							c.credit_units,
							b.rate
						FROM 
							assessments AS a,
							assessments_history_courses AS b,
							courses AS c
						WHERE 
							a.id = b.assessments_id
							AND b.course_code = c.course_code
							AND a.student_histories_id = {$this->db->escape($student_histories_id)} 
						ORDER BY 
							b.course_code ";
			//die($sql);
			//log_message("INFO", "ListMyAssesed_Courses"); // Toyet 6.27.2018
			//log_message("INFO", print_r($sql,true)); // Toyet 6.27.2018

			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}


		function ListMyAssesed_LabFees($student_histories_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							b.course_code,
							b.rate
						FROM 
							assessments AS a,
							assessments_history_lab AS b
						WHERE 
							a.id = b.assessments_id
							AND a.student_histories_id = {$this->db->escape($student_histories_id)} 
						ORDER BY 
							b.course_code ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}


		function ListMyAssessed_MiscFees($student_histories_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							b.fees_groups_id,
							c.fees_group
						FROM 
							assessments AS a,
							assessments_history_fees AS b,
							fees_groups AS c
						WHERE 
							a.id = b.assessments_id
							AND b.fees_groups_id = c.id
							AND a.student_histories_id = {$this->db->escape($student_histories_id)} 
						GROUP BY 
							c.id
						ORDER BY 
							c.weight ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
				
				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$myresult = $this->ListMyAssessed_Misc_Dtls($res->fees_groups_id, $student_histories_id);
						
						if ($myresult) {
							$result[$cnt]->fees_subgroups = $myresult;
						}									
						$cnt++;
					}
				}	
				
			}
			
			return $result;
		
		}
		
		
		private function ListMyAssessed_Misc_Dtls($fees_groups_id, $student_histories_id) {
			$result = NULL;	

			$sql = "SELECT 
							b.description,
							b.rate
						FROM 
							assessments AS a,
							assessments_history_fees AS b
						WHERE 
							a.id = b.assessments_id
							AND a.student_histories_id = {$this->db->escape($student_histories_id)} 
							AND b.fees_groups_id = {$this->db->escape($fees_groups_id)}
						ORDER BY 
							b.description ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}


		function ListEnrolled_Students($academic_terms_id=NULL, $academic_programs_id=NULL, $grade_level=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.idno,
							a.lname,
							a.fname,
							a.mname,
							a.gender,
							b.year_level
						FROM 
							students AS a,
							student_histories AS b,
							prospectus AS c
						WHERE 
							a.idno = b.students_idno
							AND b.prospectus_id = c.id 
							AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND c.academic_programs_id = {$this->db->escape($academic_programs_id)}
							AND b.year_level = {$this->db->escape($grade_level)}
							AND b.can_enroll = 'Y'
						ORDER BY 
							a.lname,
							a.fname,
							a.mname ";
			
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}


		function SearchLRN($data) {
			$result = null;
					
			$q1 = "SELECT 
							a.idno,
							a.fname,
							a.lname,
							a.mname,
							a.gender
						FROM 
							students AS a,
							students_lrn AS b
						WHERE 
							a.idno = b.students_idno 
							AND b.lrn = {$this->db->escape($data['lrn'])} ";
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		
		function Assign_LRN($data) {

			$q1 = "INSERT INTO
							students_lrn (
										students_idno,
										lrn,
										encoded_by)
							VALUES (
								{$this->db->escape($data['students_idno'])},
								{$this->db->escape($data['lrn'])},
								{$this->db->escape($data['encoded_by'])}) 
							ON DUPLICATE KEY UPDATE 
								lrn = {$this->db->escape($data['lrn'])},
								encoded_by = {$this->db->escape($data['encoded_by'])} ";
					
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		
		}
		
		
		function get_LRN($students_idno) {
			
			$result = null;
					
			$q1 = "SELECT 
							a.lrn
						FROM 
							students_lrn AS a
						WHERE 
							a.students_idno = {$this->db->escape($students_idno)} ";
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
			
		}


		function is_SHS_Student($students_idno) {
					
			$q1 = "SELECT 
							a.id
						FROM 
							student_histories AS a,
							shs_block_sections AS b
						WHERE 
							a.block_sections_id = b.block_sections_id
							AND a.students_idno = {$this->db->escape($students_idno)} 
						LIMIT 1 ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				return TRUE;
			} 
			
			return FALSE;
			
		}


		/*
		*   @Added:  Toyet 9.19.2018
		*/
		function getCurrentProspectusID($student_hist_id){

			$result = null;
			
			$sql = "select prospectus_id
					from student_histories 
					where id={$student_hist_id}";

			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();
			}
					
			return $result;
		}


		/*
		*   @Added:  Toyet 9.19.2018
		*/
		function saveNewProspectusID($student_hist_id,$prospectus_id){

			//log_message("INFO","duol na 2x..."); // Toyet 9.19.2018

			$result = null;
			
			$sql = "update student_histories 
			        set prospectus_id={$prospectus_id}
					where id={$student_hist_id}";

			//log_message("INFO",print_r($sql,true)); // Toyet 9.19.2018

			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();
			}
					
			return $result;
		}

		/*
		*   @Added:  Toyet 9.19.2018
		*/
		function totallyWithdrawEnrollment($student_hist_id){

			//log_message("INFO","duol na 2x..."); // Toyet 9.20.2018

			$result = null;
			
			$sql = "update student_histories 
			        set block_sections_id = NULL,
			            can_enroll = 'N',
			            totally_withdrawn = 'Y',
			            updated_by = '{$this->session->userdata('empno')}', 
			            updated_on = current_timestamp() 
					where id={$student_hist_id};";

			//log_message("INFO",print_r($sql,true)); // Toyet 9.19.2018

			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();
			}
					
			return $result;
		}
		
	}
	
?>
