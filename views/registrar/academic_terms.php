 <?php //print_r($academic_terms); die();?>
 <div class="row-fluid">
    <div class="span12">
<?php if($academic_terms_id = $this->input->post('id')): $academic_term = $academic_terms[(int)ltrim($academic_terms_id, '0')]?>
		<h3 class="heading">Upcoming Academic Term: <?php echo $academic_term['academic_term'] . " " . $academic_term['school_year']; ?><span class="pull-right"><a id="new_academic_year" href="javascript:void(0);" class="btn">Add Academic Year</a></span></h3>
<?php else: $academic_term = array_shift($academic_terms); $academic_terms_id = $academic_term['academic_terms_id']; ?>
		<h3 class="heading">Current Academic Term:  <?php echo $academic_term['academic_term'] . " " . $academic_term['school_year']; ?><span class="pull-right"><a id="new_academic_year" href="javascript:void(0);" class="btn">Add Academic Year</a></span></h3>
<?php endif; ?>
<?php if(is_array($message) && count($message) > 0): ?>
		<div class="alert alert-<?php echo $message['severity']; ?>"><?php echo $message['message']; ?></div>
<?php endif; ?>
		 <form id="simple_wizard" class="stepy-wizzard form-horizontal" method="post" action="">
		 	<?php echo $this->common->hidden_input_nonce(); ?>
		 	<input type="hidden" name="action" value="edit_academic_term" />
		 	<input type="hidden" name="id" value="<?php echo $academic_terms_id; ?>" />
		 	<fieldset title="Academic Term Info">
		 		<legend class="hide">Academic term schedules&hellip;</legend>
		 			<div class="formSep control-group">
						<label for="s_password" class="control-label">Status:</label>
						<div class="controls" style="margin-top: 5px">
<?php if($academic_term['status'] == 'incoming'): ?>
							<select name="academic_status"><option value="incoming" selected="selected">Incoming</option><option value="current">Set Current</option></select>				
<?php else: ?>
							<strong><?php echo ucfirst($academic_term['status']); ?></strong>
<?php endif; ?>
						</div>
					</div>
		 			<div class="formSep control-group">
						<label for="s_password" class="control-label">School Year:</label>
						<div class="controls" style="margin-top: 5px">
							<strong><?php echo $academic_term['school_year']; ?></strong>
						</div>
					</div>
					<div class="formSep control-group">
						<label for="s_password" class="control-label">Academic Term:</label>
						<div class="controls" style="margin-top: 5px">
							<strong><?php echo $academic_term['academic_term']; ?></strong>
						</div>
					</div>
		 			<div class="formSep control-group">
		 				<label for="start_date" class="control-label">Start of Term Date:</label>
		 				<div class="controls">
		 					<div class="input-append date" id="term_start" data-date-format="yyyy-mm-dd"  data-date="<?php  if( ! empty($academic_term['start_date'])) echo trim($academic_term['start_date']); else echo ""; ?>">
    							<input type="text" name="start_date" id="start_date" class="span6" value="<?php  if( ! empty($academic_term['start_date'])) echo trim($academic_term['start_date']); else echo ""; ?>" readonly/>
    							<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
		 				</div>
					</div>
			</fieldset>
			<fieldset title="Enrollment Dates">
		 		<legend class="hide">Enrollment dates for the different year levels&hellip;</legend>
<?php foreach ($this->config->item('enrollment_year_levels') as $key=>$val): ?>
		 			<div class="formSep control-group">
		 				<label for="start_date" class="control-label"><?php echo $val; ?></label>
		 				<div class="controls">
		 					<small>From</small>
		 					<div class="input-append date" id="enroll_start<?php echo $key; ?>" data-date-format="yyyy-mm-dd" data-date="<?php if( ! empty($academic_term['enrollments'][$key]['start'])) echo $academic_term['enrollments'][$key]['start']; ?>">
    							<input type="text" name="enroll_start_date[<?php echo ltrim($academic_term['enrollments'][$key]['enrollment_schedules_id'], '0'); ?>]" id="enroll_start_date<?php echo $val; ?>" class="span6" value="<?php if( ! empty($academic_term['enrollments'][$key]['start'])) echo $academic_term['enrollments'][$key]['start'];?>" readonly/>
    							<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<small>To</small>
							<div class="input-append date" id="enroll_end<?php echo $key; ?>" data-date-format="yyyy-mm-dd" data-date="<?php if( ! empty($academic_term['enrollments'][$key]['end'])) echo $academic_term['enrollments'][$key]['end'];?>">
    							<input type="text" name="enroll_end_date[<?php echo ltrim($academic_term['enrollments'][$key]['enrollment_schedules_id'], '0'); ?>]" id="enroll_end_date<?php echo $val; ?>" class="span6" value="<?php if( ! empty($academic_term['enrollments'][$key]['end'])) echo $academic_term['enrollments'][$key]['end'];?>" readonly/>
    							<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
		 				</div>
					</div>
<?php endforeach; ?>
			</fieldset>
			<fieldset title="Examination Schedules">
		 		<legend class="hide">Each period's exam schedules&hellip;</legend>
<?php $count=0; foreach($this->config->item('periods') as $val): $count++;?>
		 			<div class="formSep control-group">
		 				<label for="start_date" class="control-label"><?php echo ucfirst($val); ?></label>
		 				<div class="controls">
		 					<small>From</small>
		 					<div class="input-append date" id="period_start_<?php echo $count; ?>" data-date-format="yyyy-mm-dd" data-date="<?php if( ! empty($academic_term['periods'][ucfirst($val)]['start'])) echo $academic_term['periods'][ucfirst($val)]['start']; ?>">
    							<input type="text" name="period_start_date[<?php echo ltrim($academic_term['periods'][ucfirst($val)]['period_id'], '0'); ?>]" id="period_start_date<?php echo $count; ?>" class="span6" value="<?php if( ! empty($academic_term['periods'][ucfirst($val)]['start'])) echo $academic_term['periods'][ucfirst($val)]['start'];?>" readonly/>
    							<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<small>To</small>
							<div class="input-append date" id="period_end_<?php echo $count; ?>" data-date-format="yyyy-mm-dd" data-date="<?php if( ! empty($academic_term['enrollments'][$key]['end'])) echo $academic_term['periods'][ucfirst($val)]['end'];?>">
    							<input type="text" name="period_end_date[<?php echo ltrim($academic_term['periods'][ucfirst($val)]['period_id'], '0'); ?>]" id="period_end_date<?php echo $count; ?>" class="span6" value="<?php if( ! empty($academic_term['periods'][ucfirst($val)]['end'])) echo $academic_term['periods'][ucfirst($val)]['end'];?>" readonly/>
    							<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
		 				</div>
					</div>
<?php endforeach; ?>					
			</fieldset>
            <button type="button" class="finish btn btn-primary"><i class="icon-ok icon-white"></i>Update Academic Term</button>
                </form>
	</div>
</div>
<form method="post" action="" id="add_academic_year_form">
	<?php echo $this->common->hidden_input_nonce(); ?>
	<input type="hidden" name="action" value="add_academic_year" />
</form>
<script>
$(document).ready(function() {
	 $('#term_start').datepicker();

	 for (i=0; i<=6; i++){
	 	$('#enroll_start' + i).datepicker();
	 	$('#enroll_end' + i).datepicker();
	 	if (i >= 1 && i <= 4){
	 		$('#period_start_' + i).datepicker();
	 		$('#period_end_' + i).datepicker();
		}
	 }
	 
	 $('#new_academic_year').click(function(event){
		 
		if (confirm("This action will add another academic year to the system.\nIf you want to proceed click Ok. Otherwise, click Cancel.")){
			$('#add_academic_year_form').submit();
		}
	 });
	//* simple wizard
	hnumis_wizard.simple();
	hnumis_wizard.steps_nb();
});

hnumis_wizard = {
	simple: function(){
		$('#simple_wizard').stepy({
			titleClick	: true,
			nextLabel:      'Next <i class="icon-chevron-right icon-white"></i>',
			backLabel:      '<i class="icon-chevron-left"></i> Back'
		});
	},
	//* add numbers to step titles
	steps_nb: function(){
		$('.stepy-titles').each(function(){
			$(this).children('li').each(function(index){
				var myIndex = index + 1
				$(this).append('<span class="stepNb">'+myIndex+'</span>');
			})
		})
	}
};
</script>