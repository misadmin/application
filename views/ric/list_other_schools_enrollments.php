		<h2 class="heading">Transcript of Records from Other Schools</h2>
		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">
				<a href="#" data-toggle="modal" data-target="#modal_add_school" 
						data-project-id='<?php //print($priv_details); ?>'><i class="icon-plus"></i> New School TOR</a>
		
		<!--  modal for creating other school TOR -->
		      <div class="modal hide fade" id="modal_add_school" >
		 		<form method="post" id="add_school_form">
		   				<input type="hidden" name="action" value="add_other_school_details" />
		   				<?php $this->common->hidden_input_nonce(FALSE); ?>
		   		<div class="modal-header">
		       		<button type="button" class="close" data-dismiss="modal">�</button>
		       		<h3>Add TOR Details</h3>
		   		</div>
		   		<div class="modal-body">
		 			<div style="text-align:center;">
						<table border="0" cellpadding="2" cellspacing="0" 
							class="table table-striped table-condensed table-hover" style="width:100%;">
							<tr>
								<td width="30%">School Year</td>
								<td width="2%">:</td>
								<td width="68%">
									<select name="academic_years_id">
										<?php 
											foreach($academic_years AS $acad_yr) {
												print("<option value=".$acad_yr->id.">".$acad_yr->sy."</option>");
											}
										?>
									</select>
								</td>
								</tr>
							<tr>
								<td>Academic Term</td>
								<td>:</td>
								<td>
									<select name="term_id">
										<?php 
											foreach($academic_terms AS $acad_term) {
												print("<option value=".$acad_term->id.">".$acad_term->term_description."</option>");
											}
										?>
									</select>	
								</td>
							</tr>
							<tr>
								<td>School Address</td>
								<td>:</td>
								<td>
									<select name="country_id" id="country_id">
										<option value="00137">Philippines</option>											
											<?php						
												foreach($country_groups AS $country_group) {
													echo "<option value=".$country_group->id.">" . $country_group->name ."</option>";	
												}
											?>					
									</select> 
									<select name="province_id" id="province_id">						
										<option value="49" class="00137">Bohol</option>											
											<?php 					
												foreach($province_groups AS $province_group) {
													print("<option value=".$province_group->id." class=".$province_group->countries_id.">" . $province_group->name ."</option>");
												}
											?>									
									</select> 
									<select name="towns_id" id="towns_id">						
										<option value="960" class="49">Tagbilaran City</option>											
											<?php 					
												foreach($town_groups AS $town_group) {
													print("<option value=".$town_group->id." class=".$town_group->provinces_id.">" . $town_group->name ."</option>");
												}
											?>									
									</select> 
								</td>
							</tr>
							<tr>
								<td>School Name</td>
								<td>:</td>
								<td> 
									<select name="schools_id" id="schools_id">						
										<?php 					
											foreach($schools_list AS $school) {
												print("<option value=".$school->id." class=".$school->towns_id.">" . $school->school ."</option>");
											}
										?>									
									</select> 
								</td>
							</tr>
							<tr>
								<td>Program</td>
								<td>:</td>
								<td><input type="text" name="program" style="width:300px;"></td>
							</tr>	
						</table>
					</div>
		   		</div>
			   <div class="modal-footer">
		    		<input type="submit" class="btn btn-primary" id="add_school" value="ADD TOR DETAILS!">
			   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		       </div>
				</form>
		    </div>				
		<!--  end for creating other school TOR -->
						
						
		</div>
		
		<div>
		
		<?php
			if ($other_schools) {
				foreach ($other_schools AS $other) {
		?>
		<div style="width:100%; margin-bottom:5px; ">
		<table border="0" cellspacing="0" cellpadding="3" style="width:100%;">
		  <tr>
		    <td width="72%" align="left" style="font-size:16px; font-weight:bold;">
		    	<?php 
		    		print("<span style='font-size:18px; padding-right:20px;'>".$other->program."</span><br>");
		    		print("<span style='font-size:14px; font-style:italic;'>".$other->term." - ".$other->school."</span><br>");
		    		print("<span style='font-size:14px; font-style:italic;'>".$other->school_address."</span>");	
		     	?>
		     </td>
		     <td width="10%" align="right">
				<a href="#" data-toggle="modal" data-target="#modal_edit_school_<?php print($other->id); ?>" 
						data-project-id='<?php print($other); ?>'><i class="icon-edit"></i> Edit</a>

					<!--  modal for editing other school TOR -->
					      <div class="modal hide fade" id="modal_edit_school_<?php print($other->id); ?>" >
					 		<form method="post" id="edit_school_form">
					   				<input type="hidden" name="action" value="edit_other_school_tor" />
					   				<input type="hidden" name="other_id" value="<?php print($other->id); ?>" />
					   				<?php $this->common->hidden_input_nonce(FALSE); ?>
					   		<div class="modal-header">
					       		<button type="button" class="close" data-dismiss="modal">�</button>
					       		<h3>Edit TOR Details</h3>
					   		</div>
					   		<div class="modal-body">
					 			<div style="text-align:center;">
									<table border="0" cellpadding="2" cellspacing="0" 
										class="table table-striped table-condensed table-hover" style="width:100%;">
										<tr>
											<td width="30%">School Year</td>
											<td width="2%">:</td>
											<td width="68%">
												<select name="academic_years_id">
													<?php 
														foreach($academic_years AS $acad_yr) {
															if ($acad_yr->id == $other->academic_years_id) {
																print("<option value=".$acad_yr->id." selected>".$acad_yr->sy."</option>");
															} else {
																print("<option value=".$acad_yr->id.">".$acad_yr->sy."</option>");
															}
														}
													?>
												</select>
											</td>
											</tr>
										<tr>
											<td>Academic Term</td>
											<td>:</td>
											<td>
												<select name="term_id">
													<?php 
														foreach($academic_terms AS $acad_term) {
															if ($acad_term->id == $other->term_id) {
																print("<option value=".$acad_term->id." selected>".$acad_term->term_description."</option>");
															} else {
																print("<option value=".$acad_term->id.">".$acad_term->term_description."</option>");
															}
														}
													?>
												</select>	
											</td>
										</tr>
										<tr>
											<td>School Address</td>
											<td>:</td>
											<td><?php print($other->school_address); ?></td>
										</tr>
										<tr>
											<td>School Name</td>
											<td>:</td>
											<td> <?php //print($other->schools_id); ?>
												<select name="schools_id">						
													<?php 	
														$student_schools = $this->OtherSchools_Model->listOtherSchools($other->towns_id);				
														foreach($student_schools AS $ss) {
															if ($ss->id == $other->schools_id) {
																print("<option value=".$ss->id." selected>".$ss->school ."</option>");
															} else {
																print("<option value=".$ss->id." >".$ss->school ."</option>");
															}
														}
													?>									
												</select> 
											</td>
										</tr>
										<tr>
											<td>Program</td>
											<td>:</td>
											<td><input type="text" name="program" style="width:300px;" value="<?php print($other->program); ?>"></td>
										</tr>	
									</table>
								</div>
					   		</div>
						   <div class="modal-footer">
					    		<input type="submit" class="btn btn-primary" id="edit_school" value="EDIT TOR DETAILS!">
						   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					       </div>
							</form>
					    </div>				
					<!--  end for editing other school TOR -->

		     </td>
		     <td width="8%" align="right">
			<!-- modal for deleting school TOR -->
		     
		     		<a href="#" data-toggle="modal" data-target="#modal_id_delete_<?php print($other->id); ?>"  
						data-project-id='<?php print($other); ?>'><i class="icon-trash"></i> Remove</a>
		
		    <div class="modal hide fade" id="modal_id_delete_<?php print($other->id); ?>" >
		 		<form method="post" id="delete_school_tor_form_<?php print($other->id); ?>">
		   				<input type="hidden" name="action" value="delete_school_tor" />
		   				<input type="hidden" name="other_school_enrollments_id" value="<?php print($other->id); ?>">
		   				<?php $this->common->hidden_input_nonce(FALSE); ?>
			   		<div class="modal-header">
			       		<button type="button" class="close" data-dismiss="modal">�</button>
			       		<h3>School TOR Details</h3>
			   		</div>
			   		<div class="modal-body">
		 			<div style="text-align:center; margin-bottom: 10px;">
		 			
		 	    	<?php 
		 		   		print("<span style='font-size:18px; padding-right:20px;'>".$other->program."</span><br>");
		    			print("<span style='font-size:14px; font-style:italic;'>".$other->term." - ".$other->school."</span><br>");
		    			print("<span style='font-size:14px; font-style:italic;'>".$other->school_address."</span>");	
		     		?>
		 
					</div>
					<div style="text-align:left; color: #FF0000; font-weight:bold;">
						NOTE: This will also remove all courses under this TOR! If this item has been already 
						credited by the dean, it CANNOT BE REMOVED. 
					</div>
			   		</div>
				   <div class="modal-footer">
			    		<input type="submit" class="btn btn-danger" id="delete_tor_<?php print($other->id); ?>" value="REMOVE SCHOOL TOR!">
				   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			       </div>
				</form>
		    </div>
		    
		    <script>
				$('#delete_tor_<?php print($other->id); ?>').bind('click', function(event){
					event.preventDefault();
					var confirmed = confirm("Continue to remove School TOR?");
		    
					if (confirmed){
					
						$('#delete_school_tor_form_<?php print($other->id); ?>').submit();
					}
				});
			</script>
			
		     <!-- End of Remove School TOR -->
		     
		     </td>
		   
		    <td width="10%" align="right">
		  <!-- modal for adding course from other schools -->
		      
		          <a href="#" data-toggle="modal" data-target="#modal_add_course_<?php print($other->id); ?>" 
						data-project-id='<?php print($other->id); ?>'><i class="icon-plus"></i> Add Course</a>
		
		      <div class="modal hide fade" id="modal_add_course_<?php print($other->id); ?>" >
		 		<form method="post" id="add_course_form_<?php print($other->id); ?>">
		   				<input type="hidden" name="action" value="add_other_school_course" />
		   				 <input type="hidden" name="other_school_enroll_id" value= "<?php print($other->id);?>"/>
		   				<?php $this->common->hidden_input_nonce(FALSE); ?>
		   		<div class="modal-header">
		       		<button type="button" class="close" data-dismiss="modal">�</button>
		       		<h3>Add Course</h3>
		   		</div>
		   		<div class="modal-body">
		 			<div style="text-align:center;">
						<table border="0" cellpadding="2" cellspacing="0" 
							class="table table-striped table-condensed table-hover" style="width:100%;">
							
							<tr>
								<td>Catalog No.</td>
								<td>:</td>
								<td><input type="text" name="catalog_no" style="width:100px;"></td>
							</tr>	
							
							<tr>
								<td>Descriptive Title</td>
								<td>:</td>
								<td><input type="text" name="desc_title" style="width:300px;"></td>
							</tr>
							
							<tr>
								<td>Units</td>
								<td>:</td>
								<td><input type="text" name="units" style="width:70px;"></td>
							</tr>		
							
							<tr>
								<td>Final Grade</td>
								<td>:</td>
								<td><input type="text" name="fin_grade" style="width:30px;"></td>
							</tr>	
							
							<tr>
								<td>Remarks</td>
								<td>:</td>
								<td><input type="text" name="remarks" style="width:300px;"></td>
							</tr>	
						</table>
					</div>
		   		</div>
			   <div class="modal-footer">
		    		<input type="submit" class="btn btn-primary" id="add_course_<?php print($other->id); ?>" value="ADD COURSE!">
			   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		       </div>
				</form>
		    </div>		
		     <!-- End of adding a course from other schools -->
		   
		   		
			</td>
		    </tr>
		</table>
		
		</div>
		<div style="width:100%; margin-bottom:40px;">
		  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
		    <thead>
		    <tr>
		      <th width="10%" class="head" style="vertical-align:middle;">Catalog No.</th>
		      <th width="35%" class="head" style="vertical-align:middle;">Descriptive Title</th>
		      <th width="6%" class="head" style="vertical-align:middle;">Units</th>
		      <th width="6%" class="head" style="vertical-align:middle;">Final Grade</th>
		      <th width="37%" class="head" style="vertical-align:middle;">Remarks</th>
		      <th width="10%" class="head" style="vertical-align:middle;">Edit</th>
		      <th width="10%" class="head" style="vertical-align:middle;">Del</th>
		      </tr>
		    </thead>
		    <?php
				$courses = $this->OtherSchools_Model->ListCourses($other->id);
				if ($courses) {
					foreach ($courses AS $course) {
			?>

		    <tr>
		      <td style="text-align:left; vertical-align:middle; "><?php print($course->catalog_no); ?> </td>
		      <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($course->units); ?></td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($course->final_grade); ?></td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($course->remarks); ?></td>
		      <td style="text-align:center; vertical-align:middle; ">
			 		<a href="#" data-toggle="modal" data-target="#modal_edit_course_<?php print($course->id); ?>" 
								data-project-id='<?php print($course); ?>'><i class="icon-edit"></i></a>

		      
				  <!-- modal for EDITING course from other schools -->
				      
				      <div class="modal hide fade" id="modal_edit_course_<?php print($course->id); ?>" >
				 		<form method="post" id="edit_course_form_<?php print($course->id); ?>">
				   				<input type="hidden" name="action" value="edit_other_school_course" />
				   				 <input type="hidden" name="course_id" value= "<?php print($course->id);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
				   		<div class="modal-header">
				       		<button type="button" class="close" data-dismiss="modal">�</button>
				       		<h3>Edit Course</h3>
				   		</div>
				   		<div class="modal-body">
				 			<div style="text-align:center;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									
									<tr>
										<td>Catalog No.</td>
										<td>:</td>
										<td><input type="text" name="catalog_no" style="width:100px;" value="<?php print($course->catalog_no); ?>"></td>
									</tr>	
									
									<tr>
										<td>Descriptive Title</td>
										<td>:</td>
										<td><input type="text" name="desc_title" style="width:300px;" value="<?php print($course->descriptive_title); ?>"></td>
									</tr>
									
									<tr>
										<td>Units</td>
										<td>:</td>
										<td><input type="text" name="units" style="width:70px;" value="<?php print($course->units); ?>"></td>
									</tr>		
									
									<tr>
										<td>Final Grade</td>
										<td>:</td>
										<td><input type="text" name="fin_grade" style="width:30px;" value="<?php print($course->final_grade); ?>"></td>
									</tr>	
									
									<tr>
										<td>Remarks</td>
										<td>:</td>
										<td><input type="text" name="remarks" style="width:300px;" value="<?php print($course->remarks); ?>"></td>
									</tr>	
								</table>
							</div>
				   		</div>
					   <div class="modal-footer">
				    		<input type="submit" class="btn btn-primary" id="edit_course_<?php print($course->id); ?>" value="EDIT COURSE!">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
						</form>
				    </div>		
				     <!-- End of EDITING a course from other schools -->
								
			</td>
		      <td style="text-align:center; vertical-align:middle; ">
			 		<a class="delete_course" href="<?php print($course->id); ?>" ><i class="icon-trash"></i></a></td>
		      </tr>
		      
		    <?php
					}
				}
			?>
		
		</table>
		</div>
		<?php
			}
		}
		?>
  </div>
		


<!--  <script language="JavaScript" type="text/javascript" src=<?php print(base_url('assets/js/chained_select/')."/jquery.min.js"); ?> ></script> -->
<!-- <script language="JavaScript" type="text/javascript" src=<?php print(base_url('assets/js/chained_select/')."/jquery.chained.min.js"); ?> charset="utf-8"></script> -->


<script type="text/javascript" charset="utf-8">
          $(function(){
              $("#province_id").chained("#country_id"); 
              $("#towns_id").chained("#province_id"); 
              $("#schools_id").chained("#towns_id"); 
});
</script>


<form id="del_other_school_form" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="delete_other_school_course" />
</form>			

<script>
$('.delete_course').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to remove course?");

	if (confirmed){
		var course_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_id',
		    value: course_id,
		}).appendTo('#del_other_school_form');
		$('#del_other_school_form').submit();
	}		
});
</script>
