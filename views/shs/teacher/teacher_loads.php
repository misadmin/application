

			<div style="width:100%; overflow:auto; padding-top:5px;" id="show_students_to_submit_grades">
				<table class="table table-hover table-bordered table-striped" >
					<thead>
						<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
							<td class="shs_header" style="width:10%;">Strand-<br>Grade Level</td>
							<td class="shs_header" style="width:8%;">Section</td>
							<td class="shs_header" style="width:8%;">Course<br>Code</td>
							<td class="shs_header" style="width:24%;">Descriptive Title</td>
							<td class="shs_header" style="width:5%;">Credit<br>Units</td>
							<td class="shs_header" style="width:20%;">Class<br>Adviser</td>
							<td class="shs_header" style="width:20%;">Schedule</td>
							<td class="shs_header" style="width:5%;">No. of<br>Enrollees</td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($class_schedules) {
								$total_load=0;
								foreach($class_schedules AS $sched) {
						?>	
									<tr>
										<td style="vertical-align:middle;"><?php print($sched->abbreviation.'-Grade '.$sched->grade_level); ?></td>
										<td style="vertical-align:middle; text-align:center;">
											<?php print($sched->section_name); ?>
										</td>
										<td style="vertical-align:middle; text-align:center;"><?php print($sched->course_code); ?></td>
										<td style="vertical-align:middle;"><?php print($sched->descriptive_title); ?></td>
										<td style="vertical-align:middle; text-align:center;"><?php print($sched->credit_units); ?></td>
										<td style="vertical-align:middle;"><?php print($sched->block_adviser); ?></td>
										<td>
											<?php 
												if ($sched->time_sched) {
													foreach ($sched->time_sched AS $tsched) {
											?>
													<div>
														<?php print($tsched->course_sched); ?>
													</div>
											<?php
													}
												}
											?>
										</td>
										<td style="vertical-align:middle; text-align:center;"><?php print($sched->num_enrollees); ?></td>
									</tr>

						<?php 
									$total_load += $sched->credit_units;
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr style="text-align:center; font-size:13px; font-weight:bold;" class="shs_header">
							<td colspan="4" style="text-align:right;">TOTAL LOAD:</td>
							<td style="text-align:center;"><?php print(number_format($total_load,1)); ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>	
					</tfoot>
				</table>	
			</div>



			
			