<style>
	th.pull-center, td.pull-center {
	text-align:center;
}
</style>
<h3 class="heading">Class Schedule: <?php echo $term; ?> <?php echo $school_year; ?></h3>
<table class="table table-striped table-bordered mediaTable activeMediaTable" id="MediaTable-0">
	<thead>
		<tr>
			<th class="pull-center">Course</th>
			<th style="text-align: center">Section</th>
			<th style="text-align: center">Enrollees</th>
			<th style="text-align: center">Descriptive Title</th>
			<th style="text-align: center">Units</th>
			<th style="text-align: center">Time</th>
			<th style="text-align: center">Room</th>
		</tr>
	</thead>
	<tbody>
<?php  $total_credit_units = 0; ?>
<?php if (is_array($result) && count($result) > 0 ): ?>
<?php foreach ($result as $res) : ?>
<?php $total_credit_units += $res->credit_units; ?>
	<tr>
		<td><?php echo $res->course_code; ?></td>
		<td style="text-align: center"><a class="course_id" href="<?php echo $res->course_offerings_id; ?>"><?php echo $res->section_code; ?></a></td>
		<td style="text-align: center; font-weight:bold;"><?php print($res->num_enrollees."/".$res->max_students); ?></td>
		<td><?php echo $res->descriptive_title; ?></td>
		<td style="text-align: center;"><?php echo $res->credit_units; ?></td>
		<td><?php echo $res->schedule;?>
		<td class="pull-center"><?php echo $res->room_no=='NotInHNUMIS'? '' : $res->room_no; ?>
	</tr>
<?php endforeach; ?> 
	<tr>
		<td colspan="4" style="text-align:right"><strong>Total Load:</strong></td>
		<td><strong><?php echo number_format($total_credit_units,1) ?> units</strong></td>
		<td>&nbsp</td>
		<td>&nbsp</td>
	</tr>

<?php else: ?>
	<tr><td colspan="6">No Class Schedule Found</td>
<?php endif; ?>
	</tbody>
</table>
<form id="courses_form" method="post" action="">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="academic_terms_id" value="<?php echo $selected_term; ?>"/>
	<input type="hidden" name="action" value="show_class_list" />
</form>
<script>
	$('a.course_id').click(function(event){
		event.preventDefault(); 
		var courseid = $(this).attr('href');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_offering_id',
		    value: courseid,
		}).appendTo('#courses_form');
		$('#courses_form').submit();		
	});
</script>