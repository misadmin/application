
			<div style="padding:3px; overflow:auto; height:400px;">
					<table class="table table-hover table-bordered table-striped" id="list_of_courses" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:10%;">Course<br>Code</td>
								<td class="shs_header" style="width:40%;">Description Title</td>
								<td class="shs_header" style="width:16%;">Course Type</td>
								<td class="shs_header" style="width:7%;">Paying<br>Units</td>
								<td class="shs_header" style="width:7%;">Credit<br>Units</td>
								<td class="shs_header" style="width:10%;">Status</td>
								<td class="shs_header" style="width:5%;">Update</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($courses) {
									foreach($courses AS $course) {
							?>
							<tr>
									<td><?php print($course->course_code); ?></td>
									<td><?php print($course->descriptive_title); ?></td>
									<td style="text-align:center;"><?php print($course->type_description); ?></td>
									<td style="text-align:center;"><?php print($course->paying_units); ?></td>
									<td style="text-align:center;"><?php print($course->credit_units); ?></td>
									<td style="text-align:center;"><?php print($course->status); ?></td>
									<td style="text-align:center;">
										<a href="<?php print($course->id); ?>" 
											course_code="<?php print($course->course_code); ?>"
											descriptive_title="<?php print($course->descriptive_title); ?>"
											paying_units="<?php print($course->paying_units); ?>"
											credit_units="<?php print($course->credit_units); ?>"
											course_types_id="<?php print($course->course_types_id); ?>"
											class="update_course" >
											<div id="show_pencil_icon_<?php print($course->id); ?>" >
												<i class="icon-pencil"></i>
											</div>
										</a>
									</td>
									<td style="text-align:center;">
										<a href="<?php print($course->id); ?>" class="delete_course" >
											<div id="show_trash_icon_<?php print($course->id); ?>" >
												<i class="icon-trash"></i>
											</div>
										</a>
									</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>


<script>
	$('.delete_course').click(function(event){
		event.preventDefault(); 
		$('#show_error_msg').html('');

		var confirmed = confirm('Are you sure you want to delete this course?');

		if (confirmed){
			
			var course_id = $(this).attr('href');

			$('#show_trash_icon_'+course_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/courses_management');?>',
				data: { 
						course_id: course_id,
						action: 'delete_course'},
				dataType: 'json',
				success: function(response) {
					if (!response.msg) {
						$('#show_error_msg').html('Cannot be removed because this course is in use!'); 							
						$('#show_trash_icon_'+course_id).html("<i class='icon-trash'></i>")
					} else {
						$('#show_error_msg').html(''); 	
						$('#show_extracted_items').html(response.output); 	
					}					
				}
			});
		
		}

	});
</script>


<!-- JQ to update course-->
<script>
	$('.update_course').click(function(event){
		event.preventDefault();

		var course_id         = $(this).attr('href');
		var course_code       = $(this).attr('course_code');
		var descriptive_title = $(this).attr('descriptive_title');
		var paying_units      = $(this).attr('paying_units');
		var credit_units      = $(this).attr('credit_units');
		var course_types_id   = $(this).attr('course_types_id');
		
		$('#show_error_msg').html('');

		$('#modal_update_course').modal('show');
		
		$('#course_id_modal').val(course_id);
		$('#course_code_update_modal').val(course_code);
		$('#descriptive_title_update_modal').val(descriptive_title);
		$('#paying_units_update_modal').val(paying_units);
		$('#credit_units_update_modal').val(credit_units);
		$('#course_types_id_update_modal').val(course_types_id);
		$('#original_course_code').val(course_code);

		$('#show_update_search_code_icon').html(''); 	

	});
</script>				


	<div id="modal_update_course" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_course_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Track</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="course_id_modal"></div>
						<?php														
							$this->load->view('shs/courses/modal_view/update_course_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<!-- JQ to submit an update to a course -->
<script>
	$('#update_course_form').submit(function(e) {
		e.preventDefault();
		
		var course_id         = $('#course_id_modal').val();
		var course_code       = $('#course_code_update_modal').val();
		var descriptive_title = $('#descriptive_title_update_modal').val();
		var paying_units      = $('#paying_units_update_modal').val();
		var credit_units      = $('#credit_units_update_modal').val();
		var course_types_id   = $('#course_types_id_update_modal').val();

		$('#modal_update_course').modal('hide');
		
		$('#show_pencil_icon_'+course_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/courses_management'); ?>",
			data: {"course_id": course_id,
					"course_code": course_code,
					"descriptive_title": descriptive_title,
					"paying_units": paying_units,
					"credit_units": credit_units,
					"course_types_id": course_types_id,
					"action": "update_course" },
			dataType: 'json',
			success: function(response) {													

				$('#show_extracted_items').html(response.output); 
				
				$('#show_pencil_icon_'+course_id).html("<i class='icon-pencil'></i>")
				
			}
		});
		
	});
</script>				

