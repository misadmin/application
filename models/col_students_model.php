<?php

	class Col_Students_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
			
   		}
		
		function get_student($id=null)
		{
			if($id == null) { return null; }
			
			$result = null;
			
			$q = "SELECT b.students_idno, CONCAT(a.lname,', ',a.fname) AS neym
						FROM col_students AS b,
							students AS a
						WHERE a.idno=b.students_idno AND 
							b.students_idno = ".$this->db->escape($id);

			$query = $this->db->query($q);
		
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		function getMyGrades($id=null) 
		{
			if($id == null) { return null; }
			
			$result = null;
			
			$query = $this->db->query("SELECT g.students_idno,e.course_code,h.section_code,f.finals_grade
											FROM student_histories AS g,
												enrollments AS f,
												course_offerings AS h,
												courses AS e
											WHERE g.id=f.student_history_id AND
												h.id=f.course_offerings_id AND
												e.id=h.courses_id AND
					 							g.students_idno = ".$this->db->escape($id));
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;			
			
		}
		
		function getMyCourseCodeGrade($id=null,$course_code=null) 
		{
			if($id == null) { return null; }
			
			$result = null;
			
			$query = $this->db->query("SELECT f.finals_grade
											FROM student_histories AS g,
												enrollments AS f,
												course_offerings AS h,
												courses AS e
											WHERE g.id=f.student_history_id AND
												h.id=f.course_offerings_id AND
												e.id=h.courses_id AND
					 							g.students_idno = {$this->db->escape($id)} AND 
												e.course_code={$this->db->escape($course_code)} ");
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;			
			
		}
		
		//Justin added November 13, 2013
		public function student_inclusive_terms($student_id){
			$this->db->select("sh.id, CASE at.term WHEN 1 THEN '1st Semester' WHEN 2 THEN '2nd Semester' WHEN 3 THEN 'Summer' END AS term, CONCAT(ay.end_year-1,'-',ay.end_year) AS sy", FALSE);
			$this->db->from('student_histories sh')->join('academic_terms at', 'sh.academic_terms_id=at.id', 'left');
			$this->db->join('academic_years ay', 'at.academic_years_id=ay.id', 'left');
			$this->db->where('sh.students_idno', $student_id);
			$this->db->order_by('ay.end_year desc, at.term desc');
			$query = $this->db->get();
			if($query->num_rows() > 0)	
				return $query->result(); else
				return FALSE;
		}
		
		public function student_grades_given_history_id($student_id, $history_id){
			$this->db->select("CONCAT(ay.end_year-1,'-',ay.end_year) AS sy, CASE at.term WHEN 1 THEN '1st Semester' WHEN 2 THEN '2nd Semester' WHEN 3 THEN 'Summer' END AS term, 
					c.course_code, co.section_code, c.descriptive_title,
					if(!isnull(eo.id),eo.credit_units,c.credit_units) as credit_units,
					e.prelim_grade, e.midterm_grade, e.finals_grade, sh.year_level, ap.abbreviation", FALSE);
			$this->db->from('enrollments e');
			$this->db->join('enrollments_override eo', 'eo.enrollments_id = e.id', 'left');				
			$this->db->join('course_offerings co', 'e.course_offerings_id=co.id', 'left');
			$this->db->join('courses c', 'co.courses_id=c.id', 'left');
			$this->db->join('student_histories sh', 'e.student_history_id=sh.id', 'left');
			$this->db->join('academic_terms at', 'sh.academic_terms_id=at.id', 'left');
			$this->db->join('academic_years ay', 'ay.id=at.academic_years_id', 'left');
			$this->db->join('prospectus p', 'sh.prospectus_id=p.id', 'left');
			$this->db->join('academic_programs ap', 'ap.id=p.academic_programs_id', 'left');
			$this->db->where('e.student_history_id', $history_id);
			$this->db->where('sh.students_idno', $student_id);
			$this->db->order_by('c.course_code');
			$query = $this->db->get();
			//echo $this->db->last_query(); die();
			if($query->num_rows() > 0){
				return $query->result();
			} else
				return FALSE;
		}		


		// added Toyet 2.27.2019
		public function get_student_comments($student_id=NULL, $history_id=NULL, $this_sem_only=false){
			$query = "";
			$result = NULL;

			//log_message('info', 'INSIDE THE MODEL ==>>'.print_r($this_sem_only,true)); 

			if($this_sem_only==true) {
				$sql = "SELECT shc.student_histories_id, 
						       sh.academic_terms_id,
						       case
						          when act.term = 1 then '1st Sem '
						          when act.term = 2 then '2nd Sem '
						          when act.term = 3 then 'Summer '
								 END AS term,
						       CONCAT('SY ',date_format(act.start_date,'%Y'),'-',YEAR(act.start_date)+1) AS year,
						       shc.comments
						from student_histories_comments shc 
						left join student_histories sh ON sh.id=shc.student_histories_id
						LEFT JOIN academic_terms act ON act.id=sh.academic_terms_id
						LEFT JOIN academic_years acy ON acy.id=act.academic_years_id
				        where shc.student_histories_id=".$history_id." order by sh.academic_terms_id";
			} else {
				$sql = "SELECT shc.student_histories_id, 
						       sh.academic_terms_id,
						       case
						          when act.term = 1 then '1st Sem '
						          when act.term = 2 then '2nd Sem '
						          when act.term = 3 then 'Summer '
								 END AS term,
						       CONCAT('SY ',date_format(act.start_date,'%Y'),'-',YEAR(act.start_date)+1) AS year,
						       shc.comments
						from student_histories_comments shc 
						left join student_histories sh ON sh.id=shc.student_histories_id
						LEFT JOIN academic_terms act ON act.id=sh.academic_terms_id
						LEFT JOIN academic_years acy ON acy.id=act.academic_years_id
						where shc.student_idno=".$student_id." order by sh.academic_terms_id";
			}

			//log_message("info", print_r($sql,true));

			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;	
		}

		// added Toyet 2.27.2019
		public function update_student_comments($student_id=NULL, $history_id, $comments){
			$query = "";
			$result = NULL;

			$sql = 'update student_histories_comments shc
			        set shc.comments="'.$comments.'"
			        where shc.student_idno="'.$student_id.'"
					and shc.student_histories_id="'.$history_id.'"';

			//log_message("info", print_r($sql,true));

			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;	
		}

		// added Toyet 2.27.2019
		public function check_student_comments($student_id=NULL, $history_id){
			$query = "";
			$result = NULL;

			$sql = "SELECT shc.student_histories_id, 
						       sh.academic_terms_id,
						       case
						          when act.term = 1 then '1st Sem '
						          when act.term = 2 then '2nd Sem '
						          when act.term = 3 then 'Summer '
								 END AS term,
						       CONCAT('SY ',date_format(act.start_date,'%Y'),'-',YEAR(act.start_date)+1) AS year,
						       shc.comments
						from student_histories_comments shc 
						left join student_histories sh ON sh.id=shc.student_histories_id
						LEFT JOIN academic_terms act ON act.id=sh.academic_terms_id
						LEFT JOIN academic_years acy ON acy.id=act.academic_years_id
				        where shc.student_histories_id=".$history_id;

			//log_message("info", print_r($sql,true));

			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;	
		}

		// added Toyet 2.27.2019
		public function add_student_comments($student_id=NULL, $history_id, $comments){
			$query = "";
			$result = NULL;

			$sql = 'insert into student_histories_comments
			        (student_idno, student_histories_id, comments) 
			        values ('.$student_id.', '.$history_id.', "'.trim($comments).'")';

			//log_message("info", print_r($sql,true));

			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;	
		}

		// added Toyet 2.28.2019
		public function get_blocked_status($student_id=NULL, $history_id=NULL){
			$query = "";
			$result = NULL;

			// $sql = 'select blocked 
			//         from student_histories sh
			//         where sh.id='.$history_id.' 
			//         and sh.students_idno='.$student_id;
			$sql = 'select blocked 
			        from students st
			        where st.idno='.$student_id;

			//log_message("info", print_r($sql,true));

			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;	
		}

		// added Toyet 2.28.2019
		public function update_blocked_status($student_id=NULL, $history_id, $blocked_status){
			$query = "";
			$result = NULL;
			$new_blocked_status = "";

			//log_message("info", print_r($blocked_status,true));

			if($blocked_status=='on'){
				$new_blocked_status = "Y";
			} else {
				$new_blocked_status = "N";
			}

			// $sql = 'update student_histories sh
			// 		set blocked="'.$new_blocked_status.'"
			//         where sh.id='.$history_id.' 
			//         and sh.students_idno='.$student_id;
			$sql = 'update students st
					set blocked="'.$new_blocked_status.'"
			        where st.idno='.$student_id;

			//log_message("info", print_r($sql,true));

			$query = $this->db->query($sql);

			//log_message("info", print_r($query,true)); 

			return $query;	
		}



	}

