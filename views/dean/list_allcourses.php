<h2 class="heading">List of Courses</h2>

<div class="span10">

		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; 
				height: 30px; padding: 5px; text-align:right; margin-bottom:20px; vertical-align:middle;">
				<a href="#" data-toggle="modal" data-target="#modal_add_course" /><i class="icon-plus"></i> Add Course</a>
  			
				</div>

	<div>
			<table id="list_of_courses" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
		    <thead>
		    <tr>
		      <th width="17%" class="header" style="vertical-align:middle;">Course Code </th>
		      <th width="39%" class="header" style="vertical-align:middle;">Descriptive Title </th>
		      <th width="14%" class="header" style="vertical-align:middle;">Credit Units </th>
		      <th width="17%" class="header" style="vertical-align:middle;">Paying Units </th>
		      <th width="13%" class="header"  style="vertical-align:middle;">Details</th>
		      </tr>
		    </thead>
		    <?php
				if ($courses) {
					foreach ($courses AS $course) {
			?>
		    <tr>
		      <td style="text-align:left; vertical-align:middle; "><?php print($course->course_code); ?></td>
		      <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($course->credit_units); ?></td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($course->paying_units); ?></td>
		      <td style="text-align:center; vertical-align:middle; ">
		      		<a href="#" data-toggle="modal" data-target="#modal_id1_<?php print($course->id); ?>" /><i class="icon-edit"></i></a>
  				
					<div class="modal hide fade" id="modal_id1_<?php print($course->id); ?>" courses_id="<?php print($course->id); ?>" />
						<form method="post" id="edit_course_form">
				   				<input type="hidden" name="courses_id" value="<?php print($course->id); ?>" />
				   				<input type="hidden" name="action" value="update_course" />
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
					   <div class="modal-header">
					       <button type="button" class="close" data-dismiss="modal">�</button>
					       <h3>Edit Course</h3>
					   </div>
						<div class="modal-body">            
						      <div id="modalContent_<?php print($course->id); ?>" >
						       text here
							  </div>
						</div>
					   <div class="modal-footer">
		    				<input type="submit" class="btn btn-primary" id="update_course" value="Update Course!">
					       <a href="#" class="btn" data-dismiss="modal">Close</a>
					   </div>
					   </form>
					</div>
					
					<script>
					$('#modal_id1_<?php print($course->id); ?>').on('show', function(){
						  var courses_id = $(this).attr('courses_id');
						  $('#modalContent_<?php print($course->id); ?>').html('loading...')
					
						  $.ajax({
						      cache: false,
						      type: 'GET',
						      url: '<?php echo site_url($this->uri->segment(1).'/show_course_details');?>',
						      data: {courses_id: courses_id },
						      success: function(data) {
						        $('#modalContent_<?php print($course->id); ?>').html(data); //this part to pass the var
						      }
						  });
						})
					</script>						
  						
  						
  				</td>
		      </tr>
		    <?php
					}
				}
			?>
		</table>
	</div>
</div>


<div class="modal hide fade" id="modal_add_course" >
		<form method="post" id="add_course_form">
   				<input type="hidden" name="action" value="add_course" />
   				<?php $this->common->hidden_input_nonce(FALSE); ?>
		   		<div class="modal-header">
		       		<button type="button" class="close" data-dismiss="modal">�</button>
		       		<h3>Add Course</h3>
		   		</div>
		   		<div class="modal-body">
		   				<table class="table table-condensed" style="width:100%;">
							<tr>
								<td style="Width:25%;">Course Code</td>
								<td style="width:75%;"><input type="text" name="course_code" style="width:100px;"></td>
							</tr>
							<tr>
								<td>Descriptive Title</td>
								<td><input type="text" name="descriptive_title" style="width:300px;"></td>
							</tr>
							<tr>
								<td>Credit Units</td>
								<td><input type="text" name="credit_units" style="width:80px;"></td>
							</tr>
							<tr>
								<td>Paying Units</td>
								<td><input type="text" name="paying_units" style="width:80px;"></td>
							</tr>
							<tr>
								<td>Service Course?</td>
								<td><input type="radio" name="service_course" value="N" checked> No <input type="radio" name="service_course" value="Y" style="margin-left:20px;"> Yes
								</td>
							</tr>
							<tr>
								<td>Course Type</td>
								<td><select name="type_id" style="width:auto;">
									<?php 
										foreach($types AS $type) {
											print("<option value=".$type->id.">".$type->type_description."</option>");
										}
									?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Course Group</td>
								<td><select name="group_id" style="width:auto;">
									<?php 
										foreach($groups AS $group) {
											print("<option value=".$group->id.">".$group->group_name."</option>");
										}
									?>
									</select>
								</td>
							</tr>
						</table>
		   		</div>
			   <div class="modal-footer">
		    		<input type="submit" class="btn btn-primary" id="add_course" value="Create Course!">
			   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		       </div>
	</form>
</div>				

<!-- 
<script>
$('#modal_add_course').on('show', function(){
	  $('#modalContent_<?php print($program['id']); ?>').html('loading...')

	  $.ajax({
	      cache: false,
	      type: 'GET',
	      url: '<?php echo site_url($this->uri->segment(1).'/show_add_course_form');?>',
	      data: {},
	      success: function(data) {
	        $('#modalContent').html(data); //this part to pass the var
	      }
	  });
	})
</script>						
 -->
 	    
<script>
$().ready(function() {
	$('#list_of_courses').dataTable({
				"iDisplayLength": 50,
			    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
	});
    $("#list_of_courses").stickyTableHeaders();
    $('#list_of_courses').stickyTableHeaders('updateWidth');
	
  })
</script>