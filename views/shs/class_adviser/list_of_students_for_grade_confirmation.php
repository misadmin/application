		
			<div id="show_class_teacher" style="font-weight:bold; font-size:15px; color:#404040; margin-bottom:10px;"></div>
			<?php 
				if ($midterm_confirm) {
			?>		
				<div style="font-weight:bold; font-size:14px; color:#A50505;">
					Midterm Grades were confirmed on: <?php print($midterm_confirm_date); ?>
				</div>
			<?php 
				}
			?>
			<?php 
				if ($finals_confirm) {
			?>		
				<div style="font-weight:bold; font-size:14px; color:#A50505;">
					Finals Grades were confirmed on: <?php print($finals_confirm_date); ?>
				</div>
			<?php 
				}
			?>

			<div style="width:100%; overflow:auto; margin-top:20px;">
					<table class="table table-hover table-bordered table-striped table-condensed" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold; height:50px;" class="shs_header" >
								<td class="shs_header" style="width:8%;">Count</td>
								<td class="shs_header" style="width:12%;">ID No.</td>
								<td class="shs_header" style="width:50%;">Name</td>
								<td class="shs_header" style="width:15%;">Midterm<br>Grade</td>
								<td class="shs_header" style="width:15%;">Finals<br>Grade</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($students) {
									$cnt=1;
									foreach($students  AS $student) {
							?>			
										<tr>
											<td style="text-align:right;"><?php print($cnt); ?>.</td>
											<td style="text-align:center;">
												<?php print($student->idno); ?>
											</td>
											<td><?php print($student->name); ?></td>
											<td style="text-align:center;">
												<input type="text" style="width:25px; height:17px; font-size:14px; text-align:center; font-weight:bold;" 
													value="<?php print($student->midterm_grade); ?>" disabled />
												
											</td>
											<td style="text-align:center;">
												<input type="text" style="width:25px; height:17px; font-size:14px; text-align:center; font-weight:bold;" 
													value="<?php print($student->finals_grade); ?>" disabled />
											</td>
										</tr>
							<?php
										$cnt++;
									}
								}
							?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" style="text-align:right;">
									<div id="confirm_button"></div>
								</td>
								<td>
									<?php
									// date mod by RA: 9.21.17 to enable confirmation 
									// 	if ($midterm_submitted AND !$midterm_confirm AND $students) {
									?>		
											<button class="btn btn-warning confirm_grades" 
												course_offerings_id="<?php print($course_offerings_id); ?>"
												block_sections_id="<?php print($block_sections_id); ?>"
												courses_id="<?php print($courses_id); ?>"
												period="Midterm" >Confirm Midterm Grades!</button>
									<?php 
									//	}
									?>
								</td>
								<td>
									<?php
									//	if ($finals_submitted AND !$finals_confirm AND $students) {
									?>		
										<button class="btn btn-primary confirm_grades" id="finals_button"
											course_offerings_id="<?php print($course_offerings_id); ?>"
											block_sections_id="<?php print($block_sections_id); ?>"
											courses_id="<?php print($courses_id); ?>"
											period="Finals" >Confirm Finals Grades!</button>
									<?php 
									//	}
									?>
								</td>
							</tr>
						</tfoot>
					</table>
			</div>


			
<script>
	$('.confirm_grades').click(function(e){
		e.preventDefault();	

		var course_offerings_id = $(this).attr('course_offerings_id');
		var block_sections_id   = $(this).attr('block_sections_id');
		var courses_id          = $(this).attr('courses_id');
		var period              = $(this).attr('period');
		var teacher             = $('#show_class_teacher').text();

		$('#show_my_subjects').hide();

		var confirmed = confirm('Continue confirming the '+period+' grades?');

		if (confirmed){
			
			$('#confirm_button').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/my_section'));?>",
				data: { "block_sections_id": block_sections_id,
						"courses_id": courses_id,
						"course_offerings_id": course_offerings_id,
						"period": period,
						"action": 'confirm_grades'},
				dataType: 'json',
				success: function(response) {				

					$('.heading').html('Senior High School - Grades Confirmation'); 	

					$('#section_list').html(response.output); 	
					$('#section_list').show();
					$('#show_my_students').hide();
					
					if (response.success) {
						$('#show_msg').html(response.message);
						$('#show_msg').show();						
					}

				}
			});

			$('#confirm_button').html("");
			
		}
		
	});	
	
</script>
