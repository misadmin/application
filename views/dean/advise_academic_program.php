<div class="fluid span12">
	<form action="<?php echo site_url('dean/student/' . $this->uri->segment(3)); ?>" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="advise_academic_program" />
		<fieldset>
				<div class="control-group formSep">
					<label for="prospectus_id" class="control-label">Academic Program</label>
					<div class="controls">
						<select name="prospectus_id" id="prospectus_id" class="form">
<?php foreach($prospectus AS $pros): ?>
							<option value="<?php echo $pros->prospectus_id; ?>"><?php echo $pros->abbreviation ." - " . $pros->effective_year; ?></option>	
<?php endforeach; ?>	</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="yr_level" class="control-label">Year Level</label>
					<div class="controls">
						<select name="yr_level" id="yr_level" class="form">
<?php $yr['yr_level'] = array(1=>1,2,3,4,5); foreach($yr['yr_level'] AS $k => $v):  ?>
							<option value="<?php echo $k; ?>" <?php if($k==$year_level) echo ' selected="selected" ';?>><?php echo $v; ?></option>	
<?php endforeach; ?>	</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Advise Academic Program</button>
					</div>
				</div>
		</fieldset>
	</form>
</div>