<?php

class basic_ed_lib {
	
	/**
	 * Codeigniter Instance
	 * @var unknown_type
	 */
	protected $CI;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	
	
	/*
	 * @added: 4/12/15
	 * @author: genes
	 */
	public function enrolled_not_sectioned() {
	
		$this->CI->load->model('hnumis/AcademicYears_Model');
		$this->CI->load->model('basic_ed/Basic_Ed_Enrollments_Model');
		
		$action     = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_students');
		$school_years = $this->CI->AcademicYears_Model->ListAcadYears();

		switch ($action) {
			case 'list_students':
	
				$selected_level = NULL;
				$display_level = NULL;
				$yr_level = NULL;
				$students = NULL;
				
				$levels = $this->CI->Basic_Ed_Enrollments_Model->ListLevels_With_Enrollees($school_years[0]->id,'not_sectioned');

				if ($levels) {
					$selected_level = $levels[0]->row_id;
					$display_level = $levels[0]->display_yr_level;
					$yr_level = $levels[0]->yr_level;
					$students = $this->CI->Basic_Ed_Enrollments_Model->ListStudents_EnrolledNotSectioned($school_years[0]->id,
																	$levels[0]->levels_id, $levels[0]->yr_level);
					if ($students) {
						$this->CreateExcel_ForDownload($students,$levels[0]->display_yr_level,$school_years[0]->sy,1);
					}
				}

				$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$school_years[0]->id,
							"levels"=>$levels,
							"selected_level"=>$selected_level,
							"display_level"=>$display_level,
							"display_sy"=>$school_years[0]->sy,
							"yr_level"=>$yr_level,
							"students"=>$students,
						);
				
				break;
	
			case 'list_selected':
				
				$levels = $this->CI->Basic_Ed_Enrollments_Model->ListLevels_With_Enrollees($this->CI->input->post('academic_year_id'),'not_sectioned');

				$students = $this->CI->Basic_Ed_Enrollments_Model->ListStudents_EnrolledNotSectioned($this->CI->input->post('academic_year_id'),
																	$this->CI->input->post('levels_id'),
																	$this->CI->input->post('yr_level'));
				$row_id = $this->CI->input->post('row_id');
				$display_level = $this->CI->input->post('display_level');											
				$display_sy = $this->CI->input->post('display_sy');											
				
				if (!$students) {
					$display_level = "";
					if ($levels) {
						$students = $this->CI->Basic_Ed_Enrollments_Model->ListStudents_EnrolledNotSectioned($this->CI->input->post('academic_year_id'),
																$levels[0]->levels_id, $levels[0]->yr_level);
						$display_level = $levels[0]->display_yr_level;
						$this->CreateExcel_ForDownload($students,$display_level,$display_sy,1);
					}
				} else {
					$this->CreateExcel_ForDownload($students,$display_level,$display_sy,1);
				}
																		
				$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$this->CI->input->post('academic_year_id'),
							"levels"=>$levels,
							"selected_level"=>$row_id,
							"display_level"=>$display_level,
							"display_sy"=>$display_sy,
							"yr_level"=>$this->CI->input->post('yr_level'),
							"students"=>$students,
						);
			
				break;
				
		}
			
		$this->CI->content_lib->enqueue_body_content('basic_ed/enrolled_not_sectioned',$data);
		$this->CI->content_lib->content();
			
	}
	

	/*
	 * @added: 4/13/15
	 * @author: genes
	 */
	public function enrolled_sectioned() {
	
		$this->CI->load->model('hnumis/AcademicYears_Model');
		$this->CI->load->model('basic_ed/Basic_Ed_Enrollments_Model');
		
		$action     = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_students');
		$school_years = $this->CI->AcademicYears_Model->ListAcadYears();

		switch ($action) {
			case 'list_students':
	
				$selected_level = NULL;
				$display_level = NULL;
				$yr_level = NULL;
				$students = NULL;
				
				$levels = $this->CI->Basic_Ed_Enrollments_Model->ListLevels_With_Enrollees($school_years[0]->id,'sectioned');

				if ($levels) {
					$selected_level = $levels[0]->row_id;
					$display_level = $levels[0]->display_yr_level;
					$yr_level = $levels[0]->yr_level;
					$students = $this->CI->Basic_Ed_Enrollments_Model->ListBasicStudentsPromoted($school_years[0]->id,
																	$levels[0]->levels_id, $levels[0]->yr_level);
					if ($students) {
						$this->CreateExcel_ForDownload($students,$levels[0]->display_yr_level,$school_years[0]->sy,2);
					}
				} 

				$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$school_years[0]->id,
							"levels"=>$levels,
							"selected_level"=>$selected_level,
							"display_level"=>$display_level,
							"display_sy"=>$school_years[0]->sy,
							"yr_level"=>$yr_level,
							"students"=>$students,
						);
				
				break;
	
			case 'list_selected':
				
				$levels = $this->CI->Basic_Ed_Enrollments_Model->ListLevels_With_Enrollees($this->CI->input->post('academic_year_id'),'sectioned');

				$students = $this->CI->Basic_Ed_Enrollments_Model->ListBasicStudentsPromoted($this->CI->input->post('academic_year_id'),
																	$this->CI->input->post('levels_id'),
																	$this->CI->input->post('yr_level'));
				$row_id = $this->CI->input->post('row_id');
				$display_level = $this->CI->input->post('display_level');											
				$display_sy = $this->CI->input->post('display_sy');											
				
				if (!$students) {
					$display_level = "";
					if ($levels) {
						$students = $this->CI->Basic_Ed_Enrollments_Model->ListBasicStudentsPromoted($this->CI->input->post('academic_year_id'),
																$levels[0]->levels_id, $levels[0]->yr_level);
						$display_level = $levels[0]->display_yr_level;
						$this->CreateExcel_ForDownload($students,$display_level,$display_sy,2);
					}
				} else {
					$this->CreateExcel_ForDownload($students,$display_level,$display_sy,2);
				}
																		
				$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$this->CI->input->post('academic_year_id'),
							"levels"=>$levels,
							"selected_level"=>$row_id,
							"display_level"=>$display_level,
							"display_sy"=>$display_sy,
							"yr_level"=>$this->CI->input->post('yr_level'),
							"students"=>$students,
						);
			
				break;
				
		}
			
		$this->CI->content_lib->enqueue_body_content('basic_ed/enrolled_sectioned',$data);
		$this->CI->content_lib->content();
			
	}
	

	
	/*
	 * @added: 4/12/15
	 * @author: genes
	 * @Updated: 3/24/2019
	 */
	public function registered_not_enrolled() {
	
		$this->CI->load->model('hnumis/AcademicYears_Model');
		$this->CI->load->model('hnumis/bed/prospectus_model');
		$this->CI->load->model('hnumis/bed/basic_ed_enrollments_model');
		
		$school_years	= $this->CI->AcademicYears_Model->ListAcadYears();
		
		$action     	= ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_students');

		switch ($action) {
			case 'list_students':
					
				$prospectuses 	= $this->CI->prospectus_model->ListProspectuses();

				$students 		= $this->CI->basic_ed_enrollments_model->ListStudents_Registered($school_years[0]->id, $prospectuses[0]->id);
					
				if ($students) {
					//$this->CreateExcel_ForDownload($students,$levels[0]->display_yr_level,$school_years[0]->sy,3);
				}

				$data = array(
							"prospectuses"=>$prospectuses,
							"description"=>$prospectuses[0]->description,
							"school_years"=>$school_years,
							"display_sy"=>$school_years[0]->sy,
							"students"=>$students,
						);
				
				break;
	
			case 'list_selected':
				
				$students = $this->CI->basic_ed_enrollments_model->ListStudents_Registered($this->CI->input->post('academic_year_id'), $this->CI->input->post('prospectus_id'));
					
				$data1 = array(
							"description"=>$this->CI->input->post('description'),
							"display_sy"=>$this->CI->input->post('display_sy'),
							"students"=>$students,
						);
						
				$my_return['output'] = $this->CI->load->view('bed/reports/registered_not_enrolled_students', $data1, TRUE);
					
				echo json_encode($my_return,JSON_HEX_APOS);
				
				return;
				
		}
			
		$this->CI->content_lib->enqueue_body_content('basic_ed/registered_not_enrolled',$data);
		$this->CI->content_lib->content();
			
	}
	
	
	/*
	 * 4/13/15 by genes
	 */
	private function CreateExcel_ForDownload($students,$display_level,$display_sy,$status) {
		$this->CI->load->library('PHPExcel');
		
		switch ($status) {
			case 1:
				$title="Enrolled But Not Sectioned";
				$max_row="D";
				break;
			case 2:
				$title="Enrolled And Sectioned";
				$max_row="E";
				break;
			case 3:
				$title="Registered But Not Enrolled";
				$max_row="D";
				break;
		}
		
		$filename = "downloads/".$title." ".$display_level."-".$display_sy.".xls";
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setTitle($title." ".$display_level."-".$display_sy);
		
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		
		$objPHPExcel->getActiveSheet()->getPageMargins()
		->setTop(1)
		->setRight(0.4)
		->setLeft(0.4)
		->setBottom(1);
		
		$objPHPExcel->getActiveSheet()->getPageSetup()
		->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT)
		->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER);
		
		$objPHPExcel->getActiveSheet()->getHeaderFooter()
		->setOddHeader('&L&"-,Bold Italic"Holy Name University - Basic Education')
		->setOddFooter('&L&B&IRun Date: &D &T' . '&RPage &P of &N');

		$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title.": ".$display_level."-".$display_sy);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
			->setSize(14);

		$styleArray = array(
				'borders' => array(
						'inside'     => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array(
										'argb' => '00000000'
								)
						),
						'outline'     => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array(
										'argb' => '00000000'
								)
						)
				)
		);
			
		$objPHPExcel->getActiveSheet()->getStyle('A3:'.$max_row.'3')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()
			->getStyle('A3:'.$max_row.'3')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()->setRGB('EEEEEE');
		$objPHPExcel->getActiveSheet()->getStyle('A3:'.$max_row.'3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:'.$max_row.'3')
			->getAlignment()
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(21);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6.5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(32);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(9);
		if ($status == 2) {
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		}
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', 'ID No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', 'Gender');
		if ($status == 2) {
			$objPHPExcel->getActiveSheet()->SetCellValue('E3', 'Year-Section');
		}	
		$row=4;
				
		if ($students) {
			$count = 1;
			foreach($students AS $student) {
				$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row.':B'.$row)
					->getAlignment()
					->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->getActiveSheet()
					->getStyle('D'.$row)
					->getAlignment()
					->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row.':'.$max_row.$row)->getBorders()
					->getAllBorders()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $count);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $student->students_idno);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $student->neym);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $student->gender);
				if ($status == 2) {
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $student->yr_level.' - '.$student->section_name);	
				}
				$count++;
				$row++;
			}
		}
		
		$objWriter->save($filename,__FILE__);
		
		return;
	}  	
}