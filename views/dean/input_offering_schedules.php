<h4 class="heading">Input Schedule</h4>
<div class="row-fluid" > 
	<div class="span7">
		<form action="<?php echo site_url('dean/offer_course_schedules');?>" method="post" class="form-horizontal">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" name="step" value="3" />  
			<?php echo validation_errors();  ?>  
		
			<fieldset>
				<div class="control-group formSep">
					<label for="" class="control-label">Academic Terms: </label>
					<label for="" class="control-label"><strong><?php print($academic_terms->term." ".$academic_terms->sy); ?></strong></label>
				</div>
					
				<div class="control-group formSep">
					<label for="" class="control-label">Courses: </label>
					<div class="controls">
						<select id="courses_id" name="courses_id" style="width:auto;"> <!-- shouldn't id and name be the same? -->
			
				      	<?php
							foreach($courses AS $course) {
								print("<option value=".$course->id.">".$course->course_code." - ".$course->credit_units." Units</option>");	
							}
						?>			
						
						</select>
					</div>
				</div>
			
				<div class="control-group formSep" >
					<label for="" class="control-label">Schedule Type: </label>
					<div class="controls" style="vertical-align:middle;">
						<input name="sched_type" type="radio" value="fixed" checked="checked" style="vertical-align:text-bottom" onClick="show_time_sched();"/> Fixed 
						<input name="sched_type" type="radio" value="flexible" style="margin-left:10px; vertical-align:text-bottom;"  onClick="hide_time_sched();"/> Flexible            
					</div>
				</div>	
			
				<div class="control-group formSep" id="time_sched">
					<label for="" class="control-label">Time: </label>
					<div class="controls">
						<select id="start_time" name="start_time" style="width:auto;"> <!-- shouldn't id and name be the same? -->	
						  	<?php
								$t1 = mktime(7,30,0,0,0,0); 
								$tym1 = date('h:i A',$t1);
								$tym  = date('H:i:s',$t1);
								$cnt = 0;
								while ($cnt < (30*25)) {
						   		 	print("<option value=\"$tym\" >$tym1</option>\n");
									$cnt = $cnt + 30;
									$t1 = mktime(7,30+$cnt,0,0,0,0); 
									$tym1 = date('h:i A',$t1);
									$tym  = date('H:i:s',$t1);			
								}
							?>							
						</select>
						
						<select name="num_hr" id="num_hr" style="width:auto;"> 
			      			<option value="01:00" selected="selected">1 Hour</option>
			      			<option value="01:30">1.5 Hours</option>
			      			<option value="02:00">2.0 Hours</option>
			      			<option value="02:30">2.5 Hours</option>
			      			<option value="03:00">3.0 Hours</option>
			      			<option value="03:30">3.5 Hours</option>
			      			<option value="04:00">4.0 Hours</option>
			      			<option value="04:30">4.5 Hours</option>
			      			<option value="05:00">5.0 Hours</option>
			      			<option value="05:30">5.5 Hours</option>
			      			<option value="06:00">6.0 Hours</option>
			      			<option value="06:30">6.5 Hours</option>
			      			<option value="07:00">7.0 Hours</option>
			      			<option value="07:30">7.5 Hours</option>
			      			<option value="08:00">8.0 Hours</option>
			      			<option value="08:30">8.5 Hours</option>
			      			<option value="09:00">9.0 Hours</option>
			      			<option value="09:30">9.5 Hours</option>
			      			<option value="10:00">10.0 Hours</option>
			      			<option value="10:30">10.5 Hours</option>
			      			<option value="11:00">11.0 Hours</option>
			      			<option value="11:30">11.5 Hours</option>
			      			<option value="12:00">12.0 Hours</option>
			      			<option value="12:30">12.5 Hours</option>
			      			<option value="13:00">13.0 Hours</option>
			      			</select>		
					</div>
				</div>	
			
				<div class="control-group formSep">
					<label for="" class="control-label">Days: </label>
					<div class="controls">
						<select id="days_id" name="day_code" style="width:auto;"> <!-- shouldn't id and name be the same? -->		
				  			<?php
								foreach($days AS $day)
									print("<option value=".$day->day_code.">".$day->day_code."</option>");	
							?>								
						</select>
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="max_students" class="control-label">Class Capacity: </label>
					<div class="controls">
						<input type="text" name="max_students" id="max_students" placeholder="" class="input-mini" value="40" size="5" />
					</div>
				</div>
					
				<div class="control-group formSep">
					<label for="additional_charge" class="control-label">Additional Charge: </label>
					<div class="controls">
						<input type="radio" name="additional_charge" id="radio" placeholder="No" class="input-xlarge" value="N" checked="checked" /> No
						<input type="radio" name="additional_charge" id="radio2" placeholder="Yes" class="input-xlarge" value="Y" /> Yes
					</div>
				</div>
				
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">Continue >></button>
					</div>
				</div>		 
			</fieldset>
		</form>
	</div>

<!-- </div> -->

<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'block';
  }

function show_time_sched()
{
	show('time_sched');
}

function hide_time_sched()
{
	hide('time_sched');
}
</script>
