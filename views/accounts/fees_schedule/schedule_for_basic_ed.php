<h2 class="heading">Fees Schedule for Basic Education</h2>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; 
	border-color: #D8D8D8; height: 35px; padding: 2px; margin-bottom:20px;">
<table style="width:100%; padding:0px; height:auto;">
<tr>
<td style="text-align:left; vertical-align:top; font-weight:bold;">
<a href="download" class="btn btn-link" id="download_fees_schedule"><i class="icon-download-alt"></i> Download as CSV</a>
</td>
<td style="text-align:right; vertical-align:top;">	
<form id="change_sy" method="POST">
					<select name="academic_year_id" id="academic_year_id">
						<?php
							foreach($school_years AS $school_year) {
						?>
							<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->sy); ?> </option>	
						<?php 
							}
						?>					
					</select>
								
					<select name="basic_levels_id" id="basic_levels_id">
					<?php 
						foreach($basic_levels AS $basic_ed) {
					?>
							<option value="<?php print($basic_ed->id); ?>" <?php if ($basic_ed->id == $selected_basic_ed) { print("selected"); } ?> >
								<?php print($basic_ed->level); ?> </option>
					<?php 	
						}
					?>
					</select>
</form>		
</td>
</tr>
</table>
				
</div>
<h3><?php print($basic_ed_description)?></h3>
<h4><?php print($school_yr); ?></h4>
<?php 
	$grand_total = array('gtotal1'=>0,'gtotal2'=>0,'gtotal3'=>0,'gtotal4'=>0,'gtotal5'=>0,'gtotal6'=>0,
						'gtotal7'=>0,'gtotal8'=>0,'gtotal9'=>0,'gtotal10'=>0,'gtotal11'=>0,'gtotal12'=>0);
	if ($basic_ed_items) {
?>
<div>
	<table class="table table-bordered table-striped table-hover" >
		<thead>
			<tr style="background-color: #EAEAEA; font-weight: bold;">
				<td style="width:28%; text-align:center;">
					ITEM
				</td>
				<td style="width:6%; text-align:center;">
					1
				</td>
				<td style="width:6%; text-align:center;">
					2
				</td>	
				<td style="width:6%; text-align:center;">
					3
				</td>
				<td style="width:6%; text-align:center;">
					4
				</td>	
				<td style="width:6%; text-align:center;">
					5
				</td>
				<td style="width:6%; text-align:center;">
					6
				</td>
				<td style="width:6%; text-align:center;">
					7
				</td>
				<td style="width:6%; text-align:center;">
					8
				</td>
				<td style="width:6%; text-align:center;">
					9
				</td>
				<td style="width:6%; text-align:center;">
					10
				</td>
				<td style="width:6%; text-align:center;">
					11
				</td>
				<td style="width:6%; text-align:center;">
					12
				</td>
				</tr>
		</thead>
		<?php
			$fees_group="";
			$flag = 1;
			$total_y1 = 0;
			$total_y2 = 0;
			$total_y3 = 0;
			$total_y4 = 0;
			$total_y5 = 0;
			$total_y6 = 0;
			$total_y7 = 0;
			$total_y8 = 0;
			$total_y9 = 0;
			$total_y10 = 0;
			$total_y11 = 0;
			$total_y12 = 0;
			
			foreach($basic_ed_items AS $item) {
				if ($fees_group != $item->fees_group) {
					$prev_group = $fees_group;
					$fees_group = $item->fees_group;
					
					if($flag > 1){
						if($prev_group != "Tuition Basic"){
							?>
							<tr>	
								<td style='font-weight:bold; text-align:right;'>
									<?php 
										print("Total ".$prev_group.":");
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y1,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y2,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y3,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y4,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y5,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y6,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y7,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y8,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y9,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y10,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y11,2));
									?>
								</td>
								<td style="text-align: right; font-weight: bold; font-size:15px">
									<?php 
										print(number_format($total_y12,2));
									?>
								</td>
						</tr>
				<?php }}	
					print("<tr>");
					print("<td style='font-weight: bold;' colspan='13'>".$fees_group."</td>");
					print("</tr>");
					$total_y1 = 0;
					$total_y2 = 0;
					$total_y3 = 0;
					$total_y4 = 0;
					$total_y5 = 0;
					$total_y5 = 0;
					$total_y6 = 0;
					$total_y7 = 0;
					$total_y8 = 0;
					$total_y9 = 0;
					$total_y10 = 0;
					$total_y11 = 0;
					$total_y12 = 0;
				}
				$flag += 1 ;
		?>
		<tr>	
			<td style="padding-left: 30px;">
				<?php 
					print($item->description);
				?>
			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y1,2));
					if ($can_update AND $item->y1) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee1."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee1\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					
					$total_y1 = $total_y1 + $item->y1;
					$grand_total['gtotal1'] += $item->y1; 
				?>
				<!-- Modal to edit Fee Schedule Year 1-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee1); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee1); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee1);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>1</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y1);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee1); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 1 -->		

<script>
$('#update_fee_<?php print($item->fee1); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee1); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y2,2));
					if ($can_update AND $item->y2) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee2."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee2\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y2 = $total_y2 + $item->y2;
					$grand_total['gtotal2'] += $item->y2; 
				?>
				<!-- Modal to edit Fee Schedule Year 2-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee2); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee2); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee2);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>2</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y2);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee2); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 2 -->		

<script>
$('#update_fee_<?php print($item->fee2); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee2); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y3,2));
					if ($can_update AND $item->y3) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee3."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee3\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y3 = $total_y3 + $item->y3;
					$grand_total['gtotal3'] += $item->y3; 
				?>
				<!-- Modal to edit Fee Schedule Year 3-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee3); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee3); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee3);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>3</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y3);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee3); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 3 -->		

<script>
$('#update_fee_<?php print($item->fee3); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee3); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y4,2));
					if ($can_update AND $item->y4) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee4."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee4\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y4 = $total_y4 + $item->y4;
					$grand_total['gtotal4'] += $item->y4; 
				?>
				<!-- Modal to edit Fee Schedule Year 4-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee4); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee4); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee4);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>4</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y4);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee4); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 4 -->		

<script>
$('#update_fee_<?php print($item->fee4); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee4); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y5,2));
					if ($can_update AND $item->y5) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee5."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee5\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y5 = $total_y5 + $item->y5;
					$grand_total['gtotal5'] += $item->y5; 
				?>
				<!-- Modal to edit Fee Schedule Year 5 -->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee5); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee5); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee5);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>5</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y5);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee5); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 5 -->		

<script>
$('#update_fee_<?php print($item->fee5); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee5); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y6,2));
					if ($can_update AND $item->y6) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee6."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee6\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y6 = $total_y6 + $item->y6;
					$grand_total['gtotal6'] += $item->y6; 
				?>
				<!-- Modal to edit Fee Schedule Year 6 -->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee6); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee6); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee6);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>6</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y6);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee6); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 6 -->		

<script>
$('#update_fee_<?php print($item->fee6); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee6); ?>').submit();
	}		
});
</script>

			</td>

			<td style="text-align: right;">
				<?php 
					print(number_format($item->y7,2));
					if ($can_update AND $item->y7) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee7."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee7\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y7 = $total_y7 + $item->y7;
					$grand_total['gtotal7'] += $item->y7; 
				?>
				<!-- Modal to edit Fee Schedule Year 7 -->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee7); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee7); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee7);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>7</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y7);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee7); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 7 -->		

<script>
$('#update_fee_<?php print($item->fee7); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee7); ?>').submit();
	}		
});
</script>

			</td>
			
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y8,2));
					if ($can_update AND $item->y8) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee8."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee8\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y8 = $total_y8 + $item->y8;
					$grand_total['gtotal8'] += $item->y8; 
				?>
				<!-- Modal to edit Fee Schedule Year 8 -->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee8); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee8); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee8);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>8</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y8);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee8); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 8 -->		

<script>
$('#update_fee_<?php print($item->fee8); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee8); ?>').submit();
	}		
});
</script>

			</td>
			
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y9,2));
					if ($can_update AND $item->y9) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee9."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee9\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y9 = $total_y9 + $item->y9;
					$grand_total['gtotal9'] += $item->y9; 
				?>
				<!-- Modal to edit Fee Schedule Year 9 -->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee9); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee9); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee9);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>9</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y9);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee9); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 9 -->		

<script>
$('#update_fee_<?php print($item->fee9); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee9); ?>').submit();
	}		
});
</script>

			</td>
			
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y10,2));
					if ($can_update AND $item->y10) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee10."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee10\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y10 = $total_y10 + $item->y10;
					$grand_total['gtotal10'] += $item->y10; 
				?>
				<!-- Modal to edit Fee Schedule Year 10 -->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee10); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee10); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee10);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>10</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y10);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee10); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 10 -->		

<script>
$('#update_fee_<?php print($item->fee10); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee10); ?>').submit();
	}		
});
</script>

			</td>
			
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y11,2));
					if ($can_update AND $item->y11) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee11."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee11\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y11 = $total_y11 + $item->y11;
					$grand_total['gtotal11'] += $item->y11; 
				?>
				<!-- Modal to edit Fee Schedule Year 11 -->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee11); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee11); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee11);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>11</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y11);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee11); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 11 -->		

<script>
$('#update_fee_<?php print($item->fee11); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee11); ?>').submit();
	}		
});
</script>

			</td>
			
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y12,2));
					if ($can_update AND $item->y12) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee12."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee12\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y12 = $total_y12 + $item->y12;
					$grand_total['gtotal12'] += $item->y12; 
				?>
				<!-- Modal to edit Fee Schedule Year 12 -->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee12); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee12); ?>">
				   				<input type="hidden" name="step" value="5" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee12);?>"/>
				   				 <input type="hidden" name="academic_year_id" value= "<?php print($selected_year);?>"/>
				   				 <input type="hidden" name="basic_levels_id" value= "<?php print($selected_basic_ed);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($basic_ed_description); ?></h3>
									<h3><?php print($school_yr); ?></h3><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>12</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y12);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee12); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule Year 12 -->		

<script>
$('#update_fee_<?php print($item->fee12); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Basic Education Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee12); ?>').submit();
	}		
});
</script>

			</td>
			
		</tr>
	<?php 
			}
	?>
	<tr>	
		<td style='font-weight: bold; text-align:right;'>
			<?php 
				print("Total ".$fees_group.":");
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y1,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y2,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y3,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y4,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y5,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y6,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y7,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y8,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y9,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y10,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y11,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y12,2));
			?>
		</td>
	</tr>
	<tr style="font-weight:bold; font-size:15px;">
		<td style="text-align:right; background-color:#EAEAEA;">GRAND TOTAL:</td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal1'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal2'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal3'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal4'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal5'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal6'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal7'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal8'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal9'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal10'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal11'],2)); ?></td>
		<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal12'],2)); ?></td>
	</tr>
	
		</table>
	</div>
<?php 
	} else {
		print("<div style='margin-left:20px; margin-top:20px; font-size:16px; font-weight:bold; color:#FF0000;'>");
		print("No Fees Schedules encoded yet!");
		print("</div>");
	}
?>
	

	
<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 3,
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>

<script>
	$(document).ready(function(){
		$('#basic_levels_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 3,
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>

<script>
$('.delete_fee').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to delete Basic Education Scheduled Fee?");

	if (confirmed){
		var fees_schedule_id = $(this).attr('href');

		$('<input>').attr({
			type: 'hidden',
			name: 'step',
			value: 4,
		}).appendTo('#change_sy');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'fees_schedule_id',
		    value: fees_schedule_id,
		}).appendTo('#change_sy');

		$('#change_sy').submit();
	}		
});
</script>

<form method="post" id="download_list_form" action="<?php echo site_url("{$role}/download_fees_schedule_basic_ed_to_csv"); ?>" />
			<input type="hidden" name="academic_year_id" value="<?php print($selected_year); ?>" />
			<input type="hidden" name="basic_levels_id" value="<?php print($selected_basic_ed); ?>" />
			<input type="hidden" name="description" value="<?php print($basic_ed_description); ?>" />
			<input type="hidden" name="school_yr" value="<?php print($school_yr); ?>" />
</form>

<script>
	$('#download_fees_schedule').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>


