<?php 

$headers = array(
		array('id'=>'sequence', 'name'=>'#', 'width'=>7),
		array('id'=>'idno', 'name'=>"ID No", 'width'=>11),
		array('id'=>'student_name', 'name'=>"Student Name", 'width'=>45),
		array('id'=>'age', 'name'=>"Age", 'width'=>5),
		array('id'=>'gender', 'name'=>"Gen", 'width'=>5),
		array('id'=>'academic_program', 'name'=>"Program & Year", 'width'=>20),
		array('id'=>'units', 'name'=>"Units", 'width'=>7)
);

foreach($master_list as $key => $student){
	$data[] = array(
			'sequence'=>($key + 1),
			'idno'=>$student->idno,
			'student_name'=>$student->student,
			'units'=>$student->units,
			'age'=>($student->age > 80 ? '' : $student->age),
			'academic_program'=>$student->year_level . " " . $student->program,
			'gender'=>" " . $student->gender,
	);
}

$this->print_lib->set_table_header ($headers);
$this->print_lib->set_data($data);
?><div id="to_print" style="display:none;"><?php echo $this->print_lib->draw_table(); ?></div>