<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function show_permission()
{
	show('permission');
}
function hide_permission()
{
	hide('permission');
}
</script>

<body onLoad="hide_permission();">

<form id="transfer_form" action="<?php echo site_url('registrar/force_enrollment');?>" method="post">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="2" />
	<input type="hidden" name="offering_id" value="<?php print($offerings->id); ?>" />
	
	
<h3 class="heading"><?php print("Course: [".$offerings->course_code."-".$offerings->section_code."] ".$offerings->descriptive_title); ?></h3>

  
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; ">
	<div>
	<table align="left" cellpadding="2" cellspacing="0" style="width:50%; margin-top:5px; margin-left:20px;">
		<tr>
		   	<td width="15%">School Year</td>
		   	<td width="2%">:</td>
		    <td width="83%"><?php print($academic_terms->term." ".$academic_terms->sy); ?></td>
		</tr>
		<tr>
			<td align="left" valign="top">Teacher</td>
		  	<td align="left" valign="top">:</td>
		  	<td><?php print("[".$offerings->employees_empno."] ".$offerings->emp_name); ?></td>
		</tr>
		<tr>
		  	<td>Class</td>
		  	<td>:</td>
		  	<td><?php print($offerings->course_code."[".$offerings->section_code."] - ".$offerings->descriptive_title); ?></td>
		</tr>
		<tr>
		  	<td align="left" valign="top">Schedule/Room</td>
		  	<td align="left" valign="top">:</td>
		  	<td>
		  		<?php
			  		if($offering_slot){
						foreach($offering_slot[0] AS $slot) {
							print($slot->tym." ".$slot->days_day_code." [".$slot->room_no."]");
							print("<br>");
						}
			  		}else{
						print("No data available");
						print("<br>");
					}
		  		?>
		  	</td>
		</tr>

  	</table>
  	</div>
	
	<div>
  	<table class="table table-bordered table-hover" style="width:60%; padding:0px; margin:0px; margin-left:20px;">
	  	<thead>
		    <tr style="height:30px; background:#060; color:#FFF; font-weight:bold;">
			      <th width="7%" style="text-align:center"></th>
				  <th width="7%" style="text-align:center"></th>
				  <th width="15%" style="text-align:center">ID No.</th>
				  <th width="48%" style="text-align:center">Name</th>
			      <th width="18%" style="text-align:center">Course</th>
			      <th width="12%" style="text-align:center">Year</th>
		    </tr>
	    </thead>
    	<?php
	    	$cnt = 1;
			if ($enrollees) {
				foreach ($enrollees AS $enrollee) {
		?>
    	<tr>
		      <td align="center" valign="top"><?php print($cnt.".");?></td>
			  <td><input type="checkbox" name="<?php print("newlist[$enrollee->stud_histories_id]"); ?>" value="Y" class="uncheck"/> </td>
			  <!-- <td><input type="checkbox" name="newlist[]" value="<?php print($enrollee->stud_histories_id); ?>" class="uncheck"/>-->
			  <td style="text-align:center" valign="top"><?php print($enrollee->idno); ?></td>
		      <td><?php print($enrollee->neym); ?></td>
		      <td align="left" valign="top"><?php print($enrollee->abbreviation); ?></td>
		      <td style="text-align:center" valign="top"><?php print($enrollee->year_level); ?></td>
    	</tr>
	    <?php
				$cnt = $cnt + 1;}
			}
		?>
  	</table>
	</div>
	
  
   <table align="left" cellpadding="2" cellspacing="0" style="width:50%; margin-top:10px; margin-left:20px;">
   <tr>
       <td width="372" align="left"><input type="submit" name="move" id="move" value="Move to"  class="btn btn-success" /></td>
    	<td width="1000" align="left"><select name="section" id="new_offering_id" class="form">
                <?php 
  		foreach($new_offerings as $new_offering) {
			print("<option value= $new_offering->new_offering_id> ".$new_offering->course_code. " [ " .$new_offering->section_code." ] - ".$new_offering->start_time." - ". $new_offering->end_time." ". $new_offering->days_day_code." </option>");
		} ?>
            </select></td>
    </tr>
<tr>
</tr>
</table>

</form>
  </div>
<script>
$(function () {
    $('.checkall').on('click', function () {
        $(this).closest('fieldset').find(':checkbox').prop('checked', this.checked);
		show_permission();
    });
});
</script>
<script>
$('.uncheck').click(function(){
      $('.checkall').attr('checked', false);
	  hide_permission();
})
</script>

<script>
	$('#move').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm("Continue to transfer students?");

	if (confirmed){
		var new_offering_id = 
	   	$('#transfer_form').submit();
	}		
});
</script>
  
