<div class="span12">
	<form method="post" id="new_teller_code_form" class="form-horizontal">
		<fieldset>
			<?php action_form('new_teller_code', FALSE); ?>
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<div class="control-group formSep">
					<label class="control-label" for="item_code">Item Code</label>
					<div class="controls"><input type="text" id="teller_code" name="teller_code"  class="input-xlarge"/></div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="description">Description</label>
					<div class="controls"><input type="text" id="description" name="description" class="input-xlarge"/></div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="default_amount">Default Value</label>
					<div class="controls"><input type="text" id="default_amount" name="default_amount" style="text-align:right"  class="input-xlarge"/></div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="teller_visible">Visible to Teller?</label>
					<div class="controls">
						<select name="teller_visible" id="teller_visible">
							<option value="Y">Yes</option>
							<option value="N">No</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="is_ledger">Is Ledger?</label>
					<div class="controls">
						<select name="is_ledger" id="is_ledger">
							<option value="Y">Yes</option>
							<option value="N">No</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="tax_type">Tax Type</label>
					<div class="controls">
						<select name="tax_type" id="tax_type">
							<option value="NV">Non-VAT</option>
							<option value="V">VAT</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="status">Status</label>
					<div class="controls">
						<select name="status" id="status">
							<option value="active">Active</option>
							<option value="inactive">Inactive</option>
						</select>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-success">Submit</button>
				
		</fieldset>
	</form>
</div>
<script>
$(document).ready(function(){
	$('#new_teller_code_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			teller_code: { required: true },
			description: { required: true }
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
			setTimeout(function() {
				boxHeight()
			}, 200)
		},
		unhighlight: function(element) {
			$(element).closest('div').removeClass("f_error");
			setTimeout(function() {
				boxHeight()
			}, 200)
		},
		
		errorPlacement: function(error, element) {
			$(element).closest('div').append(error);
		}
	});	
})
</script>