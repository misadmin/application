
			<div class="col1" id="town_suburb"></div>
			<div class="col2a" id="show_select_town">
				<select name="select_town" id="select_town" class="form-control" style="font-style:italic;" >
					<option value="">--Please Select--</option>
					<option value="new_town" style="font-weight:bold;" id="new_town_suburb"></option>
					<?php
						foreach ($towns AS $town) {
							print("<option value=".$town->id.">".$town->name."</option>");
						}
					?>
				</select>
			</div>

			
<script>
	$(document).ready(function(){

		$("select#select_town").change(function(){

			$('#new_province_group').hide();
			$('#new_town_group').hide();
			$('#edit_brgy_group').hide();

			var country_name = $("select#select_country option:selected").attr('country_name'); 
			var town_id      = $("select#select_town option:selected").attr('value'); 

			if (town_id.length > 0 ) { 

				if (town_id == 'new_town') {
					if (country_name == 'Philippines') {
						$('#new_town_group').show();
						$('#new_town_name2').focus();						
					} else {
						$('#new_suburb_group').show();
						$('#new_suburb_name3').focus();
					}
				} else {	
					$('#edit_brgy_group').show();
					
					$.ajax({
						type: "POST",
						url: "<?php echo site_url($this->uri->segment(1).'/update_address');?>",
						data: {"town_id": town_id,
								"action": 'list_brgys'},
						cache: false,
						dataType: 'json',
						beforeSend: function () { 
							$('#edit_brgy_group').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");
						},
						success: function(response) {    
							$("#edit_brgy_group").html(response.output);
							if (country_name == 'Philippines') {
								$('#brgy_st').html('Barangay:');
								$('#new_brgy_st').html('New Barangay');
							} else {
								$('#brgy_st').html('Street:');
								$('#new_brgy_st').html('New Street');
							}
						}
					});
				}
			}
		});
	});
</script>
			