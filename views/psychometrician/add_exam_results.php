<div class="fluid span12">
<?php 
	if (count($exam_types) == count($student_exams_id_arrays)) : 
?>
	<h3>All Exams Taken</h3>
<?php else: ?>
	<form id="new_exam_form" method="post" action="" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="encode_exam_result" />
			<fieldset>
				<div class="control-group formSep">
					<label for="exam_abbreviation" class="control-label">Exam Type</label>
					<div class="controls">
						<select id="exam_abbreviation" name="exam_type">
<?php 
foreach ($exam_types as $exam): ?>
<?php if ( ! in_array($exam->id, $student_exams_id_arrays)):?>
							<option value="<?php echo $exam->id; ?>"><?php echo $exam->abbreviation; ?></option>
<?php endif; ?>
<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="term_taken" class="control-label">Term and Year Taken</label>
					<div class="controls">
						<select id="term_taken" name="histories_id">
<?php 
foreach ($inclusive_terms as $term): ?>
							<option value="<?php echo $term->student_histories_id; ?>"><?php echo $term->term; ?> <?php echo $term->sy; ?></option>
<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="date_taken" class="control-label">Date Taken</label>
					<div class="controls">
						<div class="input-append date" id="dp2" data-date-format="yyyy-mm-dd">
							<input type="text" name="date_taken" id="date_taken" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="exam_raw_score" class="control-label">Exam Raw Score</label>
					<div class="controls">
						<input type="text" name="exam_raw_score" id="exam_raw_score" class="span3" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="exam_percentile" class="control-label">Percentile</label>
					<div class="controls">
						<input type="text" name="exam_percentile" id="exam_percentile" class="span3" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="exam_comment" class="control-label">Exam Comment</label>
					<div class="controls">
						<textarea id="exam_comment" name="exam_comment" class="input-xlarge"></textarea>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Add Examination Result</button>
					</div>
				</div>
			</fieldset>
	</form>
	<script>
	$(document).ready(function(){
		$('#dp2').datepicker();
		$('#new_exam_form').validate({
			onkeyup: false,
			errorClass: 'error',
			validClass: 'valid',
			rules: {
				date_taken: { required: true},
				exam_raw_score: { required: true, number: true, range: [0, 100]},
				exam_percentile: { required: true, number: true, range: [0, 100]},
			},
			highlight: function(element) {
				$(element).closest('div').addClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						unhighlight: function(element) {
							$(element).closest('div').removeClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						errorPlacement: function(error, element) {
							$(element).closest('div').append(error);
						}
		});
			
	});
	</script>
<?php endif; ?>
</div>
