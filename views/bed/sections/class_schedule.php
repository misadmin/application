<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>


<?php 
	if (is_null($section->rooms_id)) {	
?>
		<div style="font-size:15px; font-weight:bold; color:#E40808;">
			Class Schedules cannot be created! Assign your classroom first before you can create class schedules!
		</div>
<?php	
	} elseif (is_null($section->prospectus_id)) {
?>
		<div style="font-size:15px; font-weight:bold; color:#E40808;">
			Class Schedules cannot be created because <span style="color:#025E05;"><?php //print($section->strand); ?></span> does not have any Prospectus yet. Click <a href="<?php print(site_url($this->uri->segment(1).'/prospectus_management'));?>" class="btn btn-success">Here!</a> to create the Prospectus.
		</div>
<?php 
	} else {
		
		if ($assessment) {		
?>		
		<div style="font-size:15px; font-weight:bold; color:#E40808; padding-left:10px; margin-bottom:15px;">
			Class Schedules cannot be added anymore! Assessment was posted by Audit on: <?php print($assessment->date_assessed); ?>
		</div>
<?php
		}
?>			
	<div id="rounded_header" style="width:99%; font-size:12px; margin:0 auto; margin-top:0px; padding:0px;">
		<div id="container">
			<div id="left" >
				<div id="show_error_msg_class_schedule" style="padding-left:10px; padding-top:10px; color:#F92424; font-weight:bold; font-size:14px;">
					
				</div>
			</div>
			<div id="right">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<div style="float:left; padding-top:6px; " >
					<?php 
						if (!$assessment) {
					?>							
						<a href="<?php print($section->id); ?>" class="btn btn-success add_schedule" style="font-size:12px; height:17px; margin-right:8px; margin-bottom:2px;" >
							<i class="icon-plus-sign"></i> New Schedule
						</a>
					<?php 
						}
					?>
				</div>
			</div>
		</div>
	</div>

			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; ">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:10%;">Course<br>Code</td>
								<td class="shs_header" style="width:35%;">Descriptive Title</td>
								<td class="shs_header" style="width:30%;">Schedule/Room</td>
								<td class="shs_header" style="width:20%;">Teacher</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($schedules) {
									foreach($schedules AS $sched) {
							?>			
										<tr>
											<td><?php print($sched->course_code); ?></td>
											<td><?php print($sched->descriptive_title); ?></td>
											<td>
												<div id="container">
													<div id="left" style="width:230px;">
														<?php print($sched->stime.'-'.$sched->etime.' ['.$sched->day_name.']'); ?>
													</div>
													<div id="left">
														<?php print("- Rm.".$sched->room_no); ?>
													</div>
													<div id="right">
														<a href="<?php print($section->id); ?>" 
															bed_course_offerings_id="<?php print($sched->bed_course_offerings_id); ?>"
															bed_course_offerings_slots_id="<?php print($sched->bed_course_offerings_slots_id); ?>"
															descriptive_title="<?php print($sched->descriptive_title); ?>"
															schedule="<?php print($sched->stime.'-'.$sched->etime.' ['.$sched->day_name.']'); ?>"
															days_names="<?php print($sched->days_names); ?>"
															array_days="<?php print($sched->day_name); ?>"
															start_time="<?php print($sched->start_time); ?>"
															end_time="<?php print($sched->end_time); ?>"
															offerings_slots_rooms_id="<?php print($sched->offerings_slots_rooms_id); ?>"
															class="change_schedule_offering" >
															<div id="show_change_schedule_icon_<?php print($sched->bed_course_offerings_slots_id); ?>" >
																<i class="icon-edit"></i>
															</div>
														</a>
													</div>
												</div>
											</td>
											<td>
												<div id="container">
													<div id="left">
														<?php print($sched->teacher); ?>
													</div>
													<div id="right">
														<a href="<?php print($section->id); ?>" 
															bed_course_offerings_id="<?php print($sched->bed_course_offerings_id); ?>"
															bed_course_offerings_slots_id="<?php print($sched->bed_course_offerings_slots_id); ?>"
															descriptive_title="<?php print($sched->descriptive_title); ?>"
															schedule="<?php print($sched->start_time.'-'.$sched->end_time.' ['.$sched->day_name.']'); ?>"
															teacher_empno="<?php print($sched->employees_empno); ?>"
															class="assign_teacher" >
															<div id="show_assign_teacher_icon_<?php print($sched->bed_course_offerings_slots_id); ?>" >
																<i class="icon-edit"></i>
															</div>
														</a>
													</div>
												</div>
											</td>
											<td style="text-align:center;">
												<?php 
													if (!$assessment) {
												?>							
														<a href="<?php print($section->id); ?>" 
															bed_course_offerings_id="<?php print($sched->bed_course_offerings_id); ?>"
															bed_course_offerings_slots_id="<?php print($sched->bed_course_offerings_slots_id); ?>"
															class="delete_schedule" >
															<div id="show_trash_icon_<?php print($sched->bed_course_offerings_slots_id); ?>" >
																<i class="icon-trash"></i>
															</div>
														</a>
												<?php 
													}
												?>
											</td>										
										</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>

<?php		
	}
?>


<script>
	$('.add_schedule').click(function(event){
		event.preventDefault();		
		
		$('#show_error_msg').html('');
					
		var section_id = $(this).attr('href');

		$('input:checkbox').attr('checked',false);

		$('#modal_new_schedule').modal('show');

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management'));?>"+'/'+section_id,
			data: {"action": "extract_courses_to_create_schedule" },
			dataType: 'json',
			success: function(response) {
				$('#section_group_name').val($(".section_group_name").text());
				$('#section_name').val($(".section_description_name").text());
				
				$('#schedule_courses_modal').html(response.courses);

			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
			
		});
		
	});
</script>				


	<div id="modal_new_schedule" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_course_schedule_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">New Subject</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$this->load->view('bed/sections/modal_view/new_schedule_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Schedule!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	
<script>
	$('#add_course_schedule_form').submit(function(e) {
		e.preventDefault();
		
		var courses_id    = $('#courses_id_modal').val();
		var start_time    = $('#start_time_modal').val();
		var end_time      = $('#end_time_modal').val();
		var days_selected = $('input:checkbox:checked.day_modal').map(function () {
			return this.value;
		}).get(); 

		if (jQuery.isEmptyObject(days_selected)) {
			alert('At least 1 day must be selected!');
		} else {
			$('#modal_new_schedule').modal('hide');

			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id));?>",
				data: {"courses_id": courses_id,
						"start_time": start_time,
						"end_time": end_time,					
						"days_selected": days_selected,					
						"action": "create_class_schedule" },
				dataType: 'json',
				success: function(response) {

					$('#show_section_management_page').html(response.output); 
					
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});					
		}
		
	});
</script>				
	

<script>
	$('.delete_schedule').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete this section?');

		if (confirmed){
			
			var bed_course_offerings_id       = $(this).attr('bed_course_offerings_id');
			var bed_course_offerings_slots_id = $(this).attr('bed_course_offerings_slots_id');

			$('#show_trash_icon_'+bed_course_offerings_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id));?>",
				data: {"bed_course_offerings_id": bed_course_offerings_id,
						"bed_course_offerings_slots_id": bed_course_offerings_slots_id,
						"action": 'delete_schedule'},
				dataType: 'json',
				success: function(response) {				

					$('#show_section_management_page').html(response.output); 	

				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
		
		}

	});
</script>

	
<!-- JQ for Assign Teacher -->
<script>
	$('.assign_teacher').click(function(event){
		event.preventDefault();

		var teacher_empno 					= $(this).attr('teacher_empno');
		var schedule 						= $(this).attr('schedule');
		var descriptive_title 				= $(this).attr('descriptive_title');
		var bed_course_offerings_id			= $(this).attr('bed_course_offerings_id');
		var bed_course_offerings_slots_id	= $(this).attr('bed_course_offerings_slots_id');
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id); ?>",
			data: {"action": "extract_subject_teachers",
					"teacher_empno": teacher_empno,
				},
			dataType: 'json',
			success: function(response) {													

				$('#modal_assign_teacher').modal('show');
				
				$('#subject_modal').val(descriptive_title);
				$('#schedule_modal').val(schedule);
				$("#course_offerings_id_subject_teacher").val(bed_course_offerings_id);
				$("#course_offerings_slots_id_subject_teacher").val(bed_course_offerings_slots_id);
					
				$('#show_subject_teachers').html(response.output); 
																			
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
					
		});

	});
</script>				

	<div id="modal_assign_teacher" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="assign_teacher_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Subject Teacher</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="course_offerings_id_subject_teacher"></div>
						<div id="course_offerings_slots_id_subject_teacher"></div>						
						<?php
							$this->load->view('bed/sections/modal_view/assign_teacher_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Assign Teacher!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script>
	$('#assign_teacher_form').submit(function(e) {
		e.preventDefault();
		
		var employees_empno     		= $('#teacher_id').val();
		var bed_course_offerings_id 	= $('#course_offerings_id_subject_teacher').val();
		var course_offerings_slots_id	= $('#course_offerings_slots_id_subject_teacher').val();

		$('#modal_assign_teacher').modal('hide');

		$('#show_assign_teacher_icon_'+course_offerings_slots_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id));?>",
			data: {"employees_empno": employees_empno,
					"bed_course_offerings_id": bed_course_offerings_id,
					"action": "assign_teacher" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_section_management_page').html(response.output); 

				$('#show_assign_teacher_icon_'+course_offerings_slots_id).html("<i class='icon-edit'></i>")
				
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
			
		});
		
	});
</script>				
<!-- End for JQ Assign Teacher -->


<!-- JQ for Change Room -->
<script>
	$('.change_schedule_offering').click(function(event){
		event.preventDefault();

		var array_days = $(this).attr('array_days');
		var start_time = $(this).attr('start_time');
		var end_time   = $(this).attr('end_time');
		var days_names = $(this).attr('days_names');
		
		var array      = array_days.split(',');

		var offerings_slots_rooms_id = $(this).attr('offerings_slots_rooms_id');
 
		$('input:checkbox').attr('checked',false);
		
		jQuery.each(array, function(index, item) {
			$("input:checkbox[value="+item+"]").attr("checked", true);
		});
		
		$('#modal_change_schedule_offering').modal('show');

		$('#descriptive_title_room_offering_modal').val($(this).attr('descriptive_title'));
		$('#start_time_change_modal').val(start_time);
		$('#end_time_change_modal').val(end_time);
		$('#course_offerings_slots_id_modal').val($(this).attr('bed_course_offerings_slots_id'));
		$('#course_offerings_id_modal').val($(this).attr('bed_course_offerings_id'));

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id));?>",
			data: {	"days_names": days_names,
					"start_time": start_time,
					"end_time": end_time,
					"offerings_slots_rooms_id": offerings_slots_rooms_id,
					"action": "extract_rooms_for_change_schedule_offering" },
			dataType: 'json',
			success: function(response) {
				
				$("#vacant_rooms_for_offering1").html(response.bed_buildings);														
			
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
			
		});
				
	});
</script>				

	<div id="modal_change_schedule_offering" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="change_schedule_offering_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Change Schedule</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="course_offerings_slots_id_modal"></div>
						<div id="course_offerings_id_modal"></div>
						<?php
							$this->load->view('bed/sections/modal_view/change_schedule_of_offfering_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Change Schedule!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script>
	$('#change_schedule_offering_form').submit(function(e) {
		e.preventDefault();
		
		var rooms_id                  = $('input[name=room_id_change]:checked').val();
		var course_offerings_slots_id = $('#course_offerings_slots_id_modal').val();
		var course_offerings_id       = $('#course_offerings_id_modal').val();
		var start_time                = $('#start_time_change_modal').val();
		var end_time                  = $('#end_time_change_modal').val();
		var days_selected             = $('input:checkbox:checked.day_change_modal').map(function () {
			return this.value;
		}).get(); 

		var days_names    = "('" + days_selected.join("','") + "')";

		$('#modal_change_schedule_offering').modal('hide');

		$('#show_change_schedule_icon_'+course_offerings_slots_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id));?>",
			data: {"rooms_id": rooms_id,
					"course_offerings_slots_id": course_offerings_slots_id,
					"start_time": start_time,
					"end_time": end_time,
					"array_days": days_selected,
					"days_names": days_names,
					"action": "change_schedule_of_offering" },
			dataType: 'json',
			success: function(response) {									

				$('#class_schedule').html(response.output); 

				if (!response.success) {
					$('#show_error_msg_class_schedule').html(response.error_msg);
				}
				
				$('#show_change_schedule_icon_'+course_offerings_slots_id).html("<i class='icon-edit'></i>")
				
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
			
		});
		
	});
</script>				
<!-- End for JQ Change Room -->

