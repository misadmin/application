<ul>
<?php foreach ($academic_terms as $term): ?>
<?php if($term['status'] == 'incoming'): ?>
	<li><a href="#" class="incoming_term" id="<?php echo $term['academic_terms_id']; ?>"><?php echo $term['school_year']; ?> (<?php echo $term['academic_term']; ?>)</a></li>
<?php endif;?>
<?php endforeach;?>
</ul>
<form method="post" action="" id="academic_term_form">
	<input type="hidden" name="action" value="show_academic_term" />
	<?php echo $this->common->hidden_input_nonce(); ?>
</form>
<script>
$('a.incoming_term').click(function(event){
	event.preventDefault(); 
	var id = $(this).attr('id');
	
	$('<input>').attr({
	    type: 'hidden',
	    name: 'id',
	    value: id,
	}).appendTo('#academic_term_form');

	$('#academic_term_form').submit();
});
</script>