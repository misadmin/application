<h2 class="heading">Basic Education History</h2>
<div style="width:70%;">
Found Students:
	<table class="table table-bordered table-striped table-hover table-condensed" >
		<thead>
			<tr style="background-color: #EAEAEA; font-weight: bold;">
				<td style="width:10%; text-align:center;">
					History No.
				</td>
				<td style="width:15%; text-align:center;">
					ID No.
				</td>
				<td style="width:50%; text-align:center;">
					Name
				</td>
				<td style="width:10%; text-align:center;">
					Year Level
				</td>	
				<td style="width:15%; text-align:center;">
					Section
				</td>
				<td style="width:10%; text-align: center;">Code</td>
			</tr>
		</thead>
		<?php
			if ($find_students) {
				foreach($find_students AS $stud) {
		?>
		<tr>	
			<td style="text-align: center;">
				<?php 
					print($stud->id);
				?>
			</td>
			<td style="text-align: center;">
				<?php 
					print($stud->idno);
				?>
			</td>
			<td style="text-align: left;">
				<?php 
					print($stud->neym);
				?>
			</td>
			<td style="text-align: center;">
				<?php 
					print($stud->yr_level);
				?>
			</td>
			<td style="text-align: left;">
				<?php 
					print($stud->section_name);
				?>
			</td>
			<td style="text-align: left;">
				&nbsp;
			</td>

		</tr>
	<?php 
				}
			}
	?>
	</table>
</div>

<div style="width:70%;">
Not Found Students:
	<table class="table table-bordered table-striped table-hover table-condensed" >
		<thead>
			<tr style="background-color: #EAEAEA; font-weight: bold;">
				<td style="width:15%; text-align:center;">
					ID No.
				</td>
				<td style="width:50%; text-align:center;">
					Name
				</td>
				<td style="width:10%; text-align:center;">
					Year Level
				</td>	
				<td style="width:15%; text-align:center;">
					Section
				</td>
				<td style="width:10%; text-align: center;">Code</td>
			</tr>
		</thead>
		<?php
			if ($not_find_students) {
				foreach($not_find_students AS $stud) {
		?>
		<tr>	
			<td style="text-align: center;">
				<?php 
					print($stud->idno);
				?>
			</td>
			<td style="text-align: left;">
				<?php 
					print($stud->neym);
				?>
			</td>
			<td style="text-align: center;">
				<?php 
					print($stud->yr_level);
				?>
			</td>
			<td style="text-align: left;">
				<?php 
					print($stud->section_name);
				?>
			</td>
			<td style="text-align: left;">
				&nbsp;
			</td>

		</tr>
	<?php 
				}
			}
	?>
	</table>
</div>
