<style>
	.c1 {
		float:left;
		width:30%;
		font-size:14px;
		font-weight:bold;
		margin-bottom:8px;
	}
	
	.c2 {
		float:left;
		width:70%;
		font-size:14px;
		color:#064D01;
		font-weight:bold;
		margin-bottom:10px;
	}

</style>

	
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%;">
			<div class="c1">Period</div>
			<div class="c2" id="period"></div>
		</div>
		<div style="width:100%;">
			<div class="c1">Core Values</div>
			<div class="c2" id="value_description"></div>
		</div>
		<div style="width:100%;">
			<div class="c1">Behavior Statements</div>
			<div class="c2" id="behavior_statement" ></div>
		</div>
		<div style="width:100%;">
			<div class="c1">Marking</div>
			<div class="c2" >
				<select id="marking" class="form-control" style="width:auto;">
					<option value="NULL">--Select--</option>
					<option value="AO">Always Observed</option>
					<option value="SO">Sometimes Observed</option>
					<option value="RO">Rarely Observed</option>
					<option value="NO">Not Observed</option>
				</select>
			</div>
		</div>
	</div>
