<?php

	class attendance_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		public function school_days() {
		
			$this->CI->load->model('hnumis/AcademicYears_Model');
			$this->CI->load->model('hnumis/shs/attendance_model');
			
			$data['terms'] = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE); //print_r($data['terms']); die();

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_school_days');
			
			switch ($action) {
				case 'list_school_days':
		
					$this->CI->load->model('academic_terms_model');
					
					$academic_terms      = $this->CI->academic_terms_model->current_academic_term(); //print_r($academic_terms); die();
					$data['term_id']     = $academic_terms->id;
					$data['school_days'] = $this->CI->attendance_model->ListSchoolDays($academic_terms->id);
					$data['school_term'] = $academic_terms->term;
					
					break;
					
				case 'add_school_day':

					$data['academic_terms_id'] = $this->CI->input->post('term_id');
					$data['num_days']     	   = $this->CI->input->post('num_days');
					$data['school_month']      = $this->CI->input->post('school_month');
					$data['encoded_by']        = $this->CI->session->userdata('empno');
					
				
					if ($this->CI->attendance_model->AddSchoolDays($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}

					$data['school_days'] = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('term_id'));

					$my_return['output'] = $this->CI->load->view('shs/attendance/list_school_days', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'change_select':

					$data['term_id']     = $this->CI->input->post('term_id');
					$data['school_term'] = $this->CI->input->post('school_term');
					
					$data['school_days'] = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('term_id'));

					$my_return['output'] = $this->CI->load->view('shs/attendance/list_school_days', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'list_months':

					$k1 = $this->CI->input->post('end_year')-1;
					$k  = $this->CI->input->post('end_year');
					
					switch ($this->CI->input->post('school_term')) {
						case '1st Semester':
									
							$data['months'] = array($k1.'-06-01'=>'June',
													$k1.'-07-01'=>'July',
													$k1.'-08-01'=>'August',
													$k1.'-09-01'=>'September',
													$k1.'-10-01'=>'October');
							break;
									
						case '2nd Semester':
							$data['months'] = array($k1.'-11-01'=>'November',
													$k1.'-12-01'=>'December',
													$k.'-01-01'=>'January',
													$k.'-02-01'=>'February',
													$k.'-03-01'=>'March');
							break;

						case 'Summer':
							$data['months'] = array($k.'-04-01'=>'April',
													$k.'-05-01'=>'May');
							break;		
					}
					
					$my_return['output'] = $this->CI->load->view('shs/attendance/modal_view/select_school_days', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'delete_school_day':

					if ($this->CI->attendance_model->DeleteSchoolDays($this->CI->input->post('school_days_id'))) {
						$my_return['msg'] = TRUE;
					} else {
						$my_return['msg'] = FALSE;
					}

					$data['school_days'] = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('term_id'));

					$my_return['output'] = $this->CI->load->view('shs/attendance/list_school_days', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_school_day':

					$data['school_days_id']    = $this->CI->input->post('school_days_id');
					$data['num_days']     	   = $this->CI->input->post('num_days');
				
					if ($this->CI->attendance_model->UpdateSchoolDays($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}

					$data['school_days'] = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('term_id'));

					$my_return['output'] = $this->CI->load->view('shs/attendance/list_school_days', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/school_days_management',$data);
			$this->CI->content_lib->content();
				
		}
		

		
	}
	
?>