
<div style="float:left; width:auto; margin-top:10px">
<form action="" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="set dates" />
	
	
<div class="formSep">
	<h2><?php print($title); ?></h2>
</div> 
	<fieldset>
		<div class="control-group formSep">
				<label class="control-label"> <b> Academic Year: </b> </label>
					<div class="controls">
						<select id="academic_year" name="academic_year" style="width: auto;">
						<?php
							foreach($academic_years AS $academic_year) {
								print("<option value=".$academic_year->id.">".$academic_year->start_year."-".$academic_year->end_year."</option>");	
							}
						?>
						</select>
					</div>
		</div>
	
		<div class="control-group formSep">
		    <label class="control-label" for="level"><b> Level&nbsp;&nbsp;: </b></label>
		    <div class="controls">
			   <select id="select_level" name="level" class="form" style="width:auto;">
					<option value="">-- Select Level --</option>
						<?php 
							foreach($levels as $level) { 
						?>
							<option value="<?php print($level->id); ?>" >
								<?php print($level->level); ?></option>
						<?php 
							}
						?>
				</select>
				
				<!-- <select name="level" id="select_level" style="width: auto;">
					<option value="2" id="ki">Kinder</option>
					<option value="3" id="gs">Grade School</option>
					<option value="4" id="hd">High School - Day</option>
					<option value="5" id="hn">High School - Night</option>
				</select> 
				  -->
	    	</div>
	    </div>
	 
<div class="my_level" style="display:none;">	 
	 <div class="control-group formSep ">
		    <label class="control-label" for="grade_level" ><b> Grade/Year Level&nbsp;&nbsp;: </b></label>
		    <div class="controls">
			    <!-- <select id="year_level" name="year" class="form">
					<option value="">-- Select Year --</option>
					<option value="1" class = 2>1</option>
					<option value="2" class = 2>2</option>
					<option value="3" class = 3>Grades 1-5</option>
					<option value="4" class = 3>Grade 6</option>
					<option value="5" class = 4>Grades 7-11</option>
					<option value="6" class = 4>Grade 12</option>
					<option value="7" class = 5>4th year</option>
				</select> -->
				<div class="preschool" style="display:none;">	
					<input type="checkbox" name="p1" checked disabled> Preschool 1<br>
				</div>
		    	<div class="kinder" style="display:none;">	
					<input type="checkbox" name="k1" checked disabled> Kinder 1<br>
				</div>
				<div class="gs" style="display:none;">
					<input type="checkbox" name="gs1" checked> Grade 1<br>
					<input type="checkbox" name="gs2" checked> Grade 2<br>
					<input type="checkbox" name="gs3" checked> Grade 3<br>
					<input type="checkbox" name="gs4" checked> Grade 4<br>
					<input type="checkbox" name="gs5" checked> Grade 5<br>
					<input type="checkbox" name="gs6" checked> Grade 6<br>
				</div>
				<div class="hd" style="display:none;">
					<input type="checkbox" name="hd7" checked> Grade 7<br>
					<input type="checkbox" name="hd8" checked> Grade 8<br>
					<input type="checkbox" name="hd9" checked> Grade 9<br>
					<input type="checkbox" name="hd10" checked> Grade 10<br>
				</div>

				<div class="sh" style="display:none;">
					<input type="checkbox" name="sh11" checked> Grade 11<br>
					<input type="checkbox" name="sh12" checked> Grade 12<br>
				</div>


				<!--  <div class="hn" style="display:none;">
					<input type="checkbox" name="hn4" checked disabled> 4th Year<br>
				</div> -->
	    	</div>
	 </div>   
	 
	<div class="control-group">
		<label class="control-label" for="grade_level"><b> Number of Exams&nbsp;&nbsp;: </b></label>
		<div class="controls">
			<input type="text" name="num_exams" class="num_exams" style="width:30px; text-align:center" value = 6>
		</div>
	</div>
		
	<div class="control-group">
		<label class="control-label" for="grade_level"><b> Start at Exam No.&nbsp;&nbsp;: </b></label>
		<div class="controls">
			<input type="text" name="start_exam_no" class="start_exam_no" style="width:30px; text-align:center" value = 1>
		</div>
	</div>
		
		
	<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">Continue</button>
					</div>
	</div>	
</div>

</fieldset>
</form>

<!-- 
<script type="text/javascript" charset="utf-8">
  				 $(function(){
       					$("#year_level").chained("#levels");
				});
</script>
 -->
 
 <script>
	 $(document).ready(function (e) {
    	$('#select_level').change(function () {
        	if ($(this).val() == '') {
            	$('.my_level').hide();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.sh').hide();
			} 

        	if ($(this).val() == '11') {
            	$('.my_level').show();
        		$('.preschool').show();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.sh').hide();
			} 

    		if ($(this).val() == '1') {
            	$('.my_level').show();
    			$('.preschool').hide();
    			$('.kinder').show();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.sh').hide();
			} 

        	if ($(this).val() == '3') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').show();
            	$('.hd').hide();
            	$('.sh').hide();
			} 

        	if ($(this).val() == '4') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').show();
            	$('.sh').hide();
			} 

       	if ($(this).val() == '12') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.sh').show();
			} 


    	});
	});
 </script>