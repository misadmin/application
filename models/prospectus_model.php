<?php

	class Prospectus_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
			
			$this->table1 = "persons";
			$this->table2 = "students";
   		}
				
		function getProspectus($idno)
		{
			$result = null;
			
			$query = $this->db->query("SELECT c.year_level,c.terms,e.course_code,e.descriptive_title,e.credit_units
										FROM col_students AS a,
											prospectus AS b,
											prospectus_terms AS c,
											prospectus_courses AS d,
											courses AS e											
										WHERE a.prospectus_id=b.id AND b.id=c.prospectus_id AND c.id=d.prospectus_terms_id AND
											d.courses_id=e.id AND
											a.students_idno={$this->db->escape($idno)}
										GROUP BY b.id,c.id,e.id
										ORDER BY c.year_level,c.terms" );
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}
		
		
		function HighlightProspectus($idno) 
		{
			$result = null;
			
			$query = $this->db->query("SELECT c.year_level,c.terms,e.course_code,e.descriptive_title,e.credit_units,f.finals_grade
										FROM col_students AS a,
											prospectus AS b,
											prospectus_terms AS c,
											student_histories AS g,
											courses AS e,
											prospectus_courses AS d LEFT JOIN course_offerings AS h ON d.courses_id=h.courses_id  	
												LEFT JOIN enrollments AS f ON f.course_offerings_id=h.id							
										WHERE a.prospectus_id=b.id AND b.id=c.prospectus_id AND c.id=d.prospectus_terms_id AND
											d.courses_id=e.id AND
											a.students_idno=g.students_idno AND 
											g.id=f.student_history_id AND
											a.students_idno={$this->db->escape($idno)}
										GROUP BY d.courses_id
										ORDER BY c.year_level,c.terms");
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		

	}

?>