<?php

class Studentwin extends CI_Controller {
	
	private $navigation;
	private $role;
	private $userinfo;
	private $ConflictEnrollmentID;
	
	/**
	 * Please put a description
	 * @var very_unknown_type
	 */
	private $CannotContinue = TRUE;
	
	private $student_info;
	
	private $AlertMsg;
	private $AlertType;
	private $assessment;
	private $student_image;
	public $status_schedule;
	
	function __construct(){		
		parent::__construct();
		$this->common->need_auth();

		$this->userinfo = $this->session->all_userdata(); //print_r($this->userinfo); die();
		$this->role = $this->uri->segment(1); // a student can have other roles other than a student...
		
		$this->content_lib->set_title ('Student | ' . $this->config->item('application_title'));
		$navbar_data = array(
				'name'			=> $this->userinfo['fname'] . " " . $this->userinfo['lname'],
				'role'			=> $this->role,
				'roles'			=> $this->session->userdata('roles'),
		);
		
		//print($this->userinfo['yr_level']);
		//die();
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Student_Model');
		$this->load->model('hnumis/Enrollments_Model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('hnumis/Prospectus_Model');
		$this->load->model('hnumis/Offerings_Model');
		$this->load->model('hnumis/Grades_Model');
		$this->load->model('hnumis/Rooms_Model');
		$this->load->model('hnumis/Programs_Model');
		$this->load->model('teller/assessment_model');

	
		$this->remarks = $this->Student_Model->CheckForNewRemarks($this->userinfo['idno']);
		//print_r($this->remarks);
		//die();

		if ($this->CheckEnroll($this->userinfo['yr_level']) AND ($this->CannotContinue) AND (!$this->assessment)){
			$navbar_data['menu'] = array(
					'Enrollment'	=>'enroll',
					'Offerings' 	=>'offerings',
					'Academics'		=>'academics',
					'Financials'	=>'financials',
					
			);
		} else {
			$navbar_data['menu'] = array(
					'Enrollment'=>'not_allowed',
					'Offerings'=>'offerings',
					'Academics'=>'academics',
					'Financials'=>'financials',
			);
		}
		//print_r($this->CheckEnroll($this->userinfo['yr_level']));
		//die();
		
		
		$this->content_lib->set_navbar_content('', $navbar_data);
		
		$this->load->model('academic_terms_model');
		
		$current_academic_term = $this->academic_terms_model->getCurrentAcademicTerm();
		$this->status_schedule = $this->academic_terms_model->what_period() . " &middot; " . $current_academic_term->term . " &middot; SY " . $current_academic_term->sy;
		
		//student information is now available through all the class methods
		$this->load->model('student_common_model');
		$this->student_info = $this->student_common_model->my_information($this->userinfo['idno']);
		
		//this is used for avatar$this->userinfo['idno']
		$port = substr($this->userinfo['idno'], 0, 3);
		
		if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$this->userinfo['idno']}.jpg"))
			$this->student_image = base_url($this->config->item('student_images_folder') . "{$port}/{$this->userinfo['idno']}.jpg");
		else {
			if ($this->student_info->gender == 'F')
				$this->student_image = base_url($this->config->item('no_image_placeholder_female')); else
				$this->student_image = base_url($this->config->item('no_image_placeholder_male'));
		}
		
		$this->content_lib->set_avatar_image($this->student_image);
	}
	
	public function index (){
		$this->load->model('common_model');		
		$this->content_lib->set_title ('Student | Dashboard | ' . $this->config->item('application_title'));

		$dcontent = array(
				'image'		=> $this->student_image,
				'idnum'		=> $this->userinfo['idno'],
				'name'		=> $this->student_info->fname ." " . $this->student_info->mname. " " . $this->student_info->lname,
				'course'	=> $this->student_info->abbreviation,
				'level'		=> $this->student_info->year_level,
				'full_home_address'	=> $this->student_info->full_home_address,
				'full_city_address'	=> $this->student_info->full_city_address,
				'religion'	=> $this->student_info->religion,
				'phone_number'=>$this->student_info->phone_number,
			
		);
		
		$dcontent['error_msg'] = "";
		if (!$this->CannotContinue) {
			$dcontent['error_msg'] = "You cannot enroll anymore because one of your required subjects has a failing grade. See your dean as soon as possible!";
		}
		
		$current_academic_term = $this->academic_terms_model->getCurrentAcademicTerm();
		$current_student_history = $this->Student_Model->get_StudentHistory_id($this->userinfo['idno'], $current_academic_term->id);
		//print_r($current_student_history); die();
		
		if($current_student_history){
		
			$this->assessment = $this->assessment_model->CheckStudentHasAssessment($current_student_history->id);
			
			if ($this->assessment) {
				$data = $this->AcademicYears_Model->getCurrentAcademicTerm();
				$dcontent['error_msg'] = "YOUR ASSESSMENT FOR <b>".$data->term." ".$data->sy."</b> HAS ALREADY BEEN POSTED!";		
			}
		}	
		
		if ($this->remarks) {
			$dcontent['error_msg'] = "Please tick your Messages tab to view new messages!";
		}
		
		$dcontent['profile_image_max_width'] = $this->config->item('profile_image_max_width');
		
		$this->content_lib->enqueue_body_content('student/dashboard', $dcontent);
		$this->load->library('tab_lib');
		$this->tab_lib->enqueue_tab("My Information", 'common/student_information_full', $this->student_info, 'student_info', TRUE);
		$all_remarks = $this->Student_Model->ListNewRemarks($this->userinfo['idno']);
		//print_r($remarks_list);
		//die();

		if ($this->common->nonce_is_valid($this->input->post('nonce'))){		
			if($action = $this->input->post('action')){			
				if ($feedback_count = $this->common_model->feedback_counter($this->userinfo['idno'])){
					if ($feedback_count >= $this->config->item('student_max_feedback')){
						$this->content_lib->set_message('Maximum allowed number of feedbacks is reached', 'alert-error');										
						$this->content_lib->content();
						return;
					}
				}
				switch ($action){
					case 'new_feedback':
						$comment = sanitize_text_field($this->input->post('comment'));
						if (trim($comment) == '')
							$this->content_lib->set_message('Feedback field is required!', 'alert-error');
						else{
							if($insert_id = $this->common_model->add_feedback($this->userinfo['idno'],$comment,'students_idno')){
								$this->content_lib->set_message('Feedback successfully submitted!</br>'
										.'HNUMIS Team will look into it ASAP.', 'alert-success');
							}else{
								$this->content_lib->set_message('Error in submission!', 'alert-error');
							}
						}
						break;
				}
			}		
		}else{
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');				
		}
		$this->load->library('form_lib');
		$comment = array();
		
		$feedback_count = $this->common_model->feedback_counter($this->userinfo['idno']);
		$feedback_status = $this->config->item('student_max_feedback') - $feedback_count  . "/" . $this->config->item('student_max_feedback');		
		$this->tab_lib->enqueue_tab("Messages", 'student/view_remarks', array('all_remarks'=>$all_remarks), 'view_remarks', FALSE);
		if ($feedback_count < $this->config->item('student_max_feedback'))
			$this->content_lib->enqueue_sidebar_widget('feedbacks/feedbacks_view', $comment, 'Submit Feedback to Developers Team [Allowed: '.$feedback_status.']:', 'in');				
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content().'<br><br><br><br>');
		$this->content_lib->content();
	}
	
	public function not_allowed(){
		$this->content_lib->set_message('Sorry, but you re not allowed to access this item at the moment!<br>' . $this->session->userdata('not_allowed_to_enroll_message'), 'alert-error');
		$this->content_lib->content();		
	}
	
	public function academics (){
		
		$this->load->library('tab_lib');
		$this->content_lib->set_title ('Student | Academics | ' . $this->config->item('application_title'));
		
				
		if ($this->input->post('academic_terms_id')) {
			$academic_terms_id = $this->input->post('academic_terms_id');
		} else {
			$academic_terms = $this->academic_terms_model->current_academic_term();
			$academic_terms_id = $academic_terms->id;
		}
		
		$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($this->session->userdata('idno'));
		//$this->content_lib->enqueue_sidebar_widget('dean/list_academic_terms1',
		//		array('academic_terms'=>$academic_terms, 'selected_term'=>$academic_terms_id),
		//		'Select Term',
		//		'in');
		
		$courses = $this->Courses_Model->student_courses_from_academic_terms ($this->session->userdata('idno'), $academic_terms_id);
		$grades = $this->Enrollments_Model->student_grades($this->session->userdata('idno'));
		//$data['grades'] = $this->Grades_Model->ListStudentGrades($academic_terms_id,$this->session->userdata('idno'));
		$prospectus = $this->Prospectus_Model->student_prospectus($this->Prospectus_Model->student_prospectus_ids($this->session->userdata('idno')));
		
		$prospectus_history = $this->Prospectus_Model->ListProspectusTerms($this->session->userdata('prospectus_id'));

		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
		switch ($step) {
			case 1:
				break;
			case 2:
				$this->generate_class_schedule_pdf($this->input->post('academic_terms_id'), $this->input->post('max_units'));
				return;
		}
		
		$student = new stdClass();
		//print_r($this->userinfo);
		//die();
		$student->prospectus_id = $this->userinfo['prospectus_id'];
		$student->year_level = $this->userinfo['yr_level'];
		$student->students_idno = $this->userinfo['idno'];
		//$student->academic_terms_id = $this->userinfo['academic_terms_id'];
		$student->academic_terms_id = $academic_terms_id;
		$student->id = $this->userinfo['student_histories_id'];
		
		$student_units = $this->Student_Model->getUnits($student, $academic_terms_id);
		
		//print_r($student_units);
		//die();

		if (!$student_units) {
			$student_units['max_bracket_units']=0;
		}
		
		$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', array('schedules'=>$courses, 
				'academic_terms_id'=>$academic_terms_id, 'max_units'=>$student_units['max_bracket_units'],
				'student_histories_id'=>$this->userinfo['student_histories_id']), 'schedule', TRUE);
		
		$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', FALSE);
		$this->tab_lib->enqueue_tab('Prospectus',
			'',
			array(),
			'prospectus',
			FALSE);
		$this->tab_lib->enqueue_tab ('My Prospectus', 'student/prospectus', array('prospectus'=>$prospectus), 'prospectus', FALSE, 'prospectus');
		$this->tab_lib->enqueue_tab ('Prospectus History', 'student/prospectus_history', 
										array('prospectus_terms'=>$prospectus_history, 
											 'student_idno'=>$this->userinfo['idno']), 
										'prospectus_history_tab', FALSE, 'prospectus');
										
		$current = $this->AcademicYears_Model->getCurrentAcademicTerm();
		
		$acad_id = $this->Prospectus_Model->getProspectus($this->userinfo['prospectus_id']);
		$this->userinfo['max_yr_level']=$this->Programs_Model->getProgram($acad_id->academic_programs_id);
		$data = $this->Student_Model->AssessYearLevel($this->userinfo);
		
		$this->tab_lib->enqueue_tab ('Year Level Assessment', 'student/yr_level_assessment', array('current'=>$current, 'data'=>$data, 'current_yr_level'=>$this->ordinalSuffix($this->userinfo['yr_level'])), 'assessment', FALSE, 'prospectus');
									
										
		$tab = $this->tab_lib->content();
		
		$this->content_lib->enqueue_body_content('', $tab);
		$this->content_lib->content();
	}
	
	
	private function ordinalSuffix( $n )
	{
	  return $n.date('S',mktime(1,1,1,1,( (($n>=10)+($n>=20)+($n==0))*10 + $n%10) ));
	}
	
	
	public function offerings() {

		$this->content_lib->enqueue_footer_script('data_tables');
		$this->DisplayCourses(TRUE);

	}
	
	
	public function enroll(){

		if (!$this->CheckEnroll($this->userinfo['yr_level']) OR (!$this->CannotContinue)){
			$this->index();
			return;
		}

		if ($this->common->nonce_is_valid($this->input->post('nonce'))){

			$this->content_lib->set_title ('Student | Enrollment | ' . $this->config->item('application_title'));
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
				switch ($step) {
					case 1:
						$this->DisplayCourses();
						break;
					case 2:
						if ($this->StudentOKToEnroll($this->input->post('course_offerings_id'),$this->input->post('courses_id'),
														$this->session->userdata('prospectus_id'),$this->input->post('advised'),$this->input->post('elective'))) {					
							$data['student_histories_id']  = $this->session->userdata('student_histories_id');
							$data['course_offerings_id']   = $this->input->post('course_offerings_id');	
							$data['ip_address']            = $this->input->ip_address();	
							$this->Student_Model->EnrollStudentToOffering($data);
							$this->AlertMsg = "Successfully Enrolled!";	
							$this->AlertType = "Success";									
						} 
						
						$this->DisplayCourses();
						
						break;
					case 3:
						$this->Student_Model->WithdrawCourse($this->input->post('enrollments_id'));
						$this->AlertMsg = "Schedule Successfully Withdrawn!";															
						$this->AlertType = "Success";									
						$this->DisplayCourses();					
						break;
					case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;	
				}
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
			$this->index();
		}
						
	}
	
	
	public function financials (){
		$this->content_lib->set_title ('Student | Financials | ' . $this->config->item('application_title'));
		$this->load->model('accounts/accounts_model');
		$this->load->model('teller/teller_model');
		$this->load->model('hnumis/enrollments_model');
		
		
		$tab = 'assessment';
		$result = $this->teller_model->get_student($this->userinfo['idno']);
		switch ($action = $this->input->post('action')){
			
		  		case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								//print($result->student_histories_id);
								//die();
								$tab = 'assessment';
								
								break;	
		}
		//$assessment = $this->accounts_model->student_assessment($this->session->userdata('student_histories_id'), $this->session->userdata('yr_level'));
		
	
		$this->load->library('tab_lib');
		
		//$result = $this->teller_model->get_student($this->userinfo['idno']);
		
		//assessment form
		$inclusive_academic_term = $this->academic_terms_model->student_inclusive_academic_terms ($this->userinfo['idno']);
		
		$inclusive_academic_term = $inclusive_academic_term ? $inclusive_academic_term[0] : FALSE;
		$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
		$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
		$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id);
		
		//$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id);
		if(isset($selected_history_id)){
			$assessment = $this->accounts_model->student_assessment($selected_history_id);
			$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
			$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
			$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
		} else {
			$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
			$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
			$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
			$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
		}
		$lab_courses = array();
		$courses = array();
		
		
			if(!empty($dcourses)){
				foreach ($dcourses as $course) {
					if ($course->type_description == 'Lab') {
					   if($laboratory_fees){
					 		$lab_courses[] = array(
							'name'=>$course->course_code,
							'amount'=>(array_key_exists($course->id, $laboratory_fees) ? $laboratory_fees[$course->id] : 0),
							);
						}else{
							$lab_courses[] = array(
							'name'=>$course->course_code,
							'amount'=>0,
							);
						}
					}	
					$courses[] = array(
						'id'=>$course->id,
						'name'=>$course->course_code,
						'units'=>$course->credit_units,
						'pay_units'=>$course->paying_units,
						're_enrollments_id'=>$course->re_enrollments_id,
						'withdrawn_on'=>$course->withdrawn_on,
					);
				}
			}
		
	//	$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
		$ledger_data = array();
		$ledger_data = $this->teller_model->get_ledger_data($result->payers_id);
		$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms($this->session->userdata('idno'));
	
		$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment', 
						array(
								'other_courses_payments'=>$other_courses_payments,
								'assessment'=> $assessment,
								'courses'=>$courses,
								'lab_courses'=>$lab_courses,
								'student_inclusive_terms' =>$student_inclusive_terms,
								'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
								
						), 
								'assessment', $tab=='assessment', FALSE);
		
		
		/*$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment',
				array(
						'current_term'=>$inclusive_academic_term->term." ".$inclusive_academic_term->sy,
						'other_courses_payments'=>$other_courses_payments,
						'assessment'=> $assessment,
						'courses'=>$courses,
						'lab_courses'=>$lab_courses
				),
				'assessment', TRUE);*/
		
		$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$ledger_data,
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date
						), 'ledger', FALSE);
		
		//$this->tab_lib->enqueue_tab('Assessment', 'accounts/assessment_table', array('assessment'=>$assessment), 'assessment', TRUE);
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->content();
	}
	
	
	
	/**
	 * Tools
	 * 
	 * Aside from the dashboard is the most common "grouped action" of any role...
	 * 
	 * @param string $action specified action
	 */
	public function tools(){
		
		$action = $this->input->post('action');		
		if (method_exists($this, $action)) {
			//action is a private method...
			call_user_func_array(array($this, $action), array());
		} else {
			//show tools...
			//print_r(get_defined_vars());die();
			//var_dump($user_data->idno); die();
			//print_r( $this->student_info->idno);die();
			$data = array(
					'name'	=>	$this->userinfo['fname'] . " " . $this->userinfo['lname'],
					'role'	=>	$this->role,
					'phone_number'=>$this->student_info->phone_number,
					'idno'			=> $this->student_info->idno,
					'errors' => array(),
			);
			$this->content_lib->set_title ('Tools | ' . $this->config->item('application_title'));
			
			$this->load->library('tab_lib');
			$this->tab_lib->enqueue_tab ('Password Settings', 'common/tools', $data, 'password', TRUE);
			$this->tab_lib->enqueue_tab ('Phone Number', 'common/phone_number_update',array(), 
						'phone_number', false);
				
			$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
			$this->content_lib->enqueue_footer_script ('password_strength');
			$this->content_lib->content();
		}
	}
	
	public function update_phone(){
		$this->load->model('student_common_model');

		if ( ! $this->common->nonce_is_valid($this->input->post('nonce'))){
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');				
		}
		
		$primary_number 	= $this->input->post('primary_number');
		$idno 				= $this->input->post('idno');
		
		if ( !$this->student_common_model->update_student_number($idno,'mobile','student',$primary_number) )
			$this->content_lib->set_message('Failed to save phone number', 'alert-error');
		else
			$this->content_lib->set_message('Phone number successfully saved', 'alert-success');		
		
		$this->content_lib->content();
	}
	
	private function change_password(){
		
		$this->load->model('student_common_model');
		$data = array(
				'name'	=>	$this->userinfo['fname'] . " " . $this->userinfo['lname'],
				'role'	=>	$this->role,
				'title'	=>	'Tools',
		);		
		$old_password = $this->input->post('password');
		$new_password = $this->input->post('newpassword1', TRUE);
		$new_password2 = $this->input->post('newpassword2', TRUE);
		
		//server side validation...
		$errors = array();
		if ( ! $this->common->nonce_is_valid($this->input->post('nonce')))
			$errors[] = 'Nonce is invalid. ';
		
		if ( ! $this->student_common_model->user_is_valid($this->userinfo['idno'], $old_password))
			$errors[] = 'Old password did not match. ';
		
		if ( $new_password != $new_password2 )
			$errors[] = 'Passwords did not match. ';
		
		//regular expression validation...
		$regex = "#[ ']+?#";
		if (preg_match($regex, $new_password))
			$errors[] = 'Invalid Characters found. ';
		
		if (count($errors) == 0) {
			$result = $this->student_common_model->update_information($this->userinfo['idno'], array('password'=>$new_password));
			if ( ! $result)
				$errors[] = 'Error found while updating password. '; 
		}
		
		if ( count($errors) > 0 )
			$data['errors'] = $errors; else 
			$data['success'] = TRUE;
		
		$this->load->library('tab_lib');
		$this->tab_lib->enqueue_tab ('Password Settings', 'common/tools', $data, 'password', TRUE);
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->enqueue_footer_script ('password_strength');
		$this->content_lib->content();
	}
	
	/***********************************
	 * Private Methods...
	 ***********************************/
		private function CheckEnroll($yr_level) {

			$current  = $this->AcademicYears_Model->getCurrentAcademicTerm();	

			$status   = $this->Student_Model->getEnrollStatus($this->session->userdata('idno'),$current->id);
			
			if ($this->Student_Model->is_new_enrollee($this->session->userdata('idno')) === TRUE) {
				$schedule = $this->Enrollments_Model->getEnrollmentSchedule($current->id,$yr_level-1);
				
			} else {
				$schedule = $this->Enrollments_Model->getEnrollmentSchedule($current->id,$yr_level);			
			}

			
			//$date = date("Y-m-d");// current date
			
			//$current_date  = strtotime(date("Y-m-d", strtotime($date)) . " +15 hours");			
			
			$current_date = strtotime(date('Y-m-d'));
			
			
			
			//print_r($current_date);
			
			//echo date('m/d/Y', $current_date);
			//die();

			$this->session->set_userdata('not_allowed_to_enroll_message','');				
			
			if ($status) {
				$extension = strtotime($status->extension_end);
				$this->session->set_userdata('student_histories_id',$status->id);
				$this->session->set_userdata('academic_terms_id',$current->id);
				$this->session->set_userdata('year_level',$status->year_level);
				$this->session->set_userdata('block_sections_id',$status->block_sections_id);			
				//$this->session->set_userdata('prospectus_id',$status->prospectus_id);			
			} else {
				$this->session->set_userdata('not_allowed_to_enroll_message','The System failed to get your Enrollment Status!');				
				return FALSE;
			}

			if ($schedule) {
				$start_date = strtotime($schedule->start_date);
				$end_date = strtotime($schedule->end_date);
				//print_r($schedule);die();
				//echo $start_date; die();
				
			} else {
				$this->session->set_userdata('not_allowed_to_enroll_message','The System failed to get the Enrollment Schedule for your Year Level!');				
				return FALSE;
			}

			echo date("M d, Y",$current_date). " ".$current_date ; echo "<p>";		
			echo date("M d, Y",$start_date)." ".$start_date;echo "<p>";
			echo date("M d, Y",$end_date)." ".$end_date;
			die;
  			if ($status->can_enroll == 'Y' AND (($current_date >= $start_date AND $current_date <= $end_date) OR $extension >= $current_date)) {
				$this->getUnits(); //retrieve max credit and bracketed units
				return TRUE;
			} else {
				$this->session->set_userdata('not_allowed_to_enroll_message','Your Enrollment Schedule is: <strong>' . date("M d, Y", $start_date)  . ' to ' . date("M d, Y", $end_date)) . '</strong>';				
				return FALSE;
			}
		}


/******************************
** CheckAllowedCoursesToEnroll
** Edited: 01/21/2013
******************************/
		private function CheckAllowedCoursesToEnroll() {
			//list courses to be taken
			$Course_FromProspectus = $this->Prospectus_Model->ListCoursesToBeTaken($this->session->userdata('prospectus_id'));
			
			$grade_array = array('5.0','NG','INC','DR',NULL);
			$d2 = "('')";
			$CoursesFailed = array();
			$CoursesFailed_Elective = array();
			$CoursesNoPrereq = array();
			$NotTakenProspectus = array();
			$NotTakenElective = array();
			$final3 = array();


			if ($Course_FromProspectus) {
				foreach($Course_FromProspectus AS $course) {
					//check if course is taken excluding the current term
					$data2 = $this->Prospectus_Model->checkIfTaken($course->courses_id,$this->session->userdata('idno'));
					if (!$data2) {
						//if not taken, temporarily save to array
						$CoursesNotTaken[] = $course;
					} elseif (in_array($data2->finals_grade, $grade_array)) {	
						if ($course->course_elective == 'N') {										
							$CoursesFailed[] = $course->courses_id;
						} else {
							$CoursesFailed_Elective[] = $course->courses_id;						
						}
					} else {
							if ($course->cutoff_grade AND ($data2->finals_grade > $course->cutoff_grade)) {
								$CoursesFailed[] = $course->courses_id;
							}
					}
				}
				
				//print_r($CoursesNotTaken); die();

				//list all credited courses of particular student
				$data_CreditedCourses = $this->Prospectus_Model->ListCreditedCourses($this->session->userdata('idno'));
				$n=0;
				if ($data_CreditedCourses) {
					foreach($data_CreditedCourses AS $course) {
						//remove courses that are not taken but credited
						if (in_array($course->courses_id,$CoursesNotTaken)) {
							unset($CoursesNotTaken[$n]);
							$n++;
						}
					}
				}

				//print_r($CoursesNotTaken);
				//die();
				
				foreach($CoursesNotTaken AS $course) {
					if ($course->course_elective == 'N') {
						$NotTakenProspectus[] = $course->courses_id;
					} else {
						$NotTakenElective[] = $course->courses_id;					
					}
				}
				
				$d1 = json_encode($NotTakenProspectus);
				$d2 = str_replace(array('"','[',']'),array("'","(",")"),$d1);
				
				if ($NotTakenElective) {
					$d3 = json_encode($NotTakenElective);
					$d4 = str_replace(array('"','[',']'),array("'","(",")"),$d3);
				} else {
					$d4 = "('')";
				} 
				
				//print_r($d2); die();
				//$d2 are courses NOT TAKEN, NOT CREDITED
				//$d4 are courses NOT TAKEN, ELECTIVE
				//OK NA output of $d2
//**************************************************************************************************************

				//list all preprequisites of not taken and not credited courses
				$Courses_WithPrereq = $this->Prospectus_Model->ListCourseWithPrereq($d2, $d4, $this->session->userdata('prospectus_id'));
				
				$Courses_NoPrereq = $this->Prospectus_Model->ListCourseNoPrereq($d2, $d4, $this->session->userdata('prospectus_id'));
				$NotTaken_WPassedPrereq='';
				$NotTaken_WPassedPrereq_Elective='';
				$NotTaken_WCreditedPrereq = '';		
				$NotTaken_WCreditedPrereq_Elective = '';	
				$MyAdvisedCourses='';
				$Courses_NoPrereq_Prospectus='';
				$Courses_NoPrereq_Elective='';
				//check courses with prerequisites
				//print_r($Courses_NoPrereq);
				//die();
				$Final_Courses_WithPrereq = '';
				$Final_Courses_WithPrereq_Elective = array();
				//print_r($Courses_NoPrereq);
				//die();

				$yr = $this->session->userdata('year_level');
				//print($yr);
			//	die();
				if ($Courses_WithPrereq) {
					foreach ($Courses_WithPrereq AS $course) {
						if ($course->from_prospectus == 'Y') {
							if ($this->Prospectus_Model->PassPrerequisites($course->prospectus_courses_id, $this->session->userdata('idno'), 
																				$this->session->userdata('year_level'), $this->session->userdata('prospectus_id'))) {
								$Final_Courses_WithPrereq[] = $course->courses_id;
							}
						} else {
							$courses_children = $this->Prospectus_Model->Topic_Elective($course->prospectus_courses_id, $this->session->userdata('idno'),
																				$this->session->userdata('year_level'));
							if ($courses_children) {
								foreach ($courses_children AS $child) {
									if ($this->Prospectus_Model->PassPrereq_Elective_Child($child->topic_courses_id, $this->session->userdata('idno'),
																				$this->session->userdata('year_level'), $this->session->userdata('prospectus_id'))) {
										//print($child->topic_courses_id."<br>");
										$Final_Courses_WithPrereq_Elective[] = $child->topic_courses_id;									
									}
								}
							}
						}
					}
				}
				
				//print_r($Final_Courses_WithPrereq); die();
				
				//for courses with no prerequisites
				if ($Courses_NoPrereq) {
					foreach ($Courses_NoPrereq AS $data5) {
						if ($data5->from_prospectus == 'Y') {
							$Courses_NoPrereq_Prospectus[] = $data5->courses_id;
						} else {
							$Courses_NoPrereq_Elective[] = $data5->courses_id;						
						}
					}
				}
				
				//for advised courses
				$AdvisedCourses = $this->Student_Model->ListAdvisedCourses($this->session->userdata('student_histories_id'));
			
				if ($AdvisedCourses) {
					foreach ($AdvisedCourses AS $data6) {
						$MyAdvisedCourses[] = $data6->courses_id;
					}
				}

			
				$final3['CoursesFailed']					= $CoursesFailed;
				$final3['CoursesFailed_Elective']			= $CoursesFailed_Elective;
				$final3['Final_Courses_WithPrereq']			= $Final_Courses_WithPrereq;
				$final3['Final_Courses_WithPrereq_Elective']= $Final_Courses_WithPrereq_Elective;				
				$final3['Courses_NoPrereq_Prospectus'] 		= $Courses_NoPrereq_Prospectus;
				$final3['Courses_NoPrereq_Elective']		= $Courses_NoPrereq_Elective;
				$final3['NotTaken_WPassedPrereq']  	 		= $NotTaken_WPassedPrereq;
				$final3['NotTaken_WPassedPrereq_Elective'] 	= $NotTaken_WPassedPrereq_Elective;
				$final3['NotTaken_WCreditedPrereq'] 		= $NotTaken_WCreditedPrereq;
				$final3['NotTaken_WCreditedPrereq_Elective']= $NotTaken_WCreditedPrereq_Elective;
				$final3['MyAdvisedCourses'] 				= $MyAdvisedCourses;
						
			}

			//print_r($final3); die();
			return $final3;
		}

		
		
/******************************
** StudentOKToEnroll
******************************/
		private function StudentOKToEnroll($course_offerings_id, $courses_id, $prospectus_id, $advised, $elective) {
			
			//NOTE: this method must be UPDATED to include computation of units_enrolled of elective subjects by: genes 5/4/13
			//$data3 = $this->Student_Model->getUnitsEnrolled($this->session->userdata('idno'),$this->session->userdata('academic_terms_id'),
				//												$this->session->userdata('prospectus_id'));


			$grades    = $this->Grades_Model->ListStudentGrades($this->session->userdata('academic_terms_id'),$this->session->userdata('idno'));
			
			$total_credit_units_enrolled = 0;
			$total_bracketed_units_enrolled = 0;
			
			if ($grades) {
				foreach($grades AS $grade) {
					if ($grade->is_bracketed == 'N') {
						$total_credit_units_enrolled += $grade->credit_units;
					} else {
						$total_bracketed_units_enrolled +=  $grade->credit_units;
					}
				}
			}
			
			$data1 = $this->Offerings_Model->getCourseOfferingInfo($course_offerings_id,$prospectus_id,$advised,$elective);
			$credit_units = 0;
			
			//$total_credit_units_enrolled = 0;
			if ($data1->is_bracketed == 'N') {
				//if ($data3) {
					//$total_credit_units_enrolled = $data3->total_credit_units_enrolled;
				//}
				$credit_units = $total_credit_units_enrolled + $data1->credit_units;
				if ($credit_units > $this->session->userdata('max_units_to_enroll')) {
					$this->AlertMsg = "Overload Credit Units!";
					$this->AlertType = "Error";
					return FALSE;
				}
			}
		
//			if (($data3->total_credit_units_enrolled + $data3->total_bracketed_units_enrolled + $data1->credit_units) > $this->session->userdata('max_bracket_units')) {

			// if (($total_credit_units_enrolled + $total_bracketed_units_enrolled  + $data1->credit_units) > $this->session->userdata('max_bracket_units')) {
/*			if (($total_credit_units_enrolled + $data1->credit_units) > $this->session->userdata('max_bracket_units')) {
					$this->AlertMsg = "Overload Total Units!";
					$this->AlertType = "Error";
					return FALSE;			
			}
*/				
				
			//check if student already enrolled on particular course on particular academic term
			$data1 = $this->Student_Model->getEnrolledCourse($this->session->userdata('academic_terms_id'),$courses_id,$this->session->userdata('idno'));
			if (!$data1) {
				$offering = $this->Offerings_Model->ListOfferingSlots($course_offerings_id);
					
				foreach ($offering[0] AS $offer) {
								
							$day_names = $this->Rooms_Model->ListDayNames($offer->days_day_code);
									
							foreach ($day_names AS $day) {
								$data1 = $this->Offerings_Model->getConflictEnrollment($this->session->userdata('idno'),$this->session->userdata('academic_terms_id'),
																							$offer->start_time,$offer->end_time,$day->day_name);
								//check if there is conflict
								if ($data1) {
									//store conflicting Enrollment ID
									$this->ConflictEnrollmentID[] = $data1->id;
								}					
							}
						}
							
				if ($this->ConflictEnrollmentID) {
							$this->AlertMsg = "Conflict offering!";
							$this->AlertType = "Error";
							return FALSE;
						} else {
							return TRUE;
						}
							
			} else {
				$this->AlertMsg = "Course already enrolled!";
				$this->AlertType = "Error";
				return FALSE;
			}
		}
		
		
		//Edited: 01/24/2013
		//retrieves maximum credit units and maximum bracketed units
		private function getUnits() {
			$max_units_to_enroll=0;
			$max_bracket_units=0;

			$data4 = $this->Student_Model->getCheckWithSummmer($this->session->userdata('prospectus_id'), $this->session->userdata('yr_level'));
			$data5 = $this->AcademicYears_Model->getCurrentAcademicTerm();
			
			if($data5->term == "Summer" AND !$data4) {
				$max = $this->Student_Model->getMaxInSummmer($this->session->userdata('idno'), $this->session->userdata('academic_terms_id'));
				if ($max) {
					$max_units_to_enroll = $max->max_units; 
					$max_bracket_units = 12; 				
				} else {
					$max_units_to_enroll = 9; 
					$max_bracket_units = 12; 
				}
			} else {						
				$data2 = $this->Student_Model->getMaxUnits($this->session->userdata('student_histories_id'),$this->session->userdata('academic_terms_id'));
				//NOTE: by justing (3/4/2013)
				//concern1: this is already called by the constructor ... why?
				//bug: if $data2 is not set... will cause a notice on landing page...
				//suggestion: add filters on possibly not set data by testing whether that data is set or not...
				//$this->content_lib->set_message (print_r($data5, FALSE), 'alert-error'); 
				//if (! isset($data2)) {
					$max_units_to_enroll = $data2->max_units_to_enroll; 
					$max_bracket_units = $data2->max_bracket_units;
				//} 
			} 
			
			$this->session->set_userdata('max_units_to_enroll',$max_units_to_enroll);
			$this->session->set_userdata('max_bracket_units',$max_bracket_units);		
		}


		
		private function DisplayCourses($return=FALSE) {
			$d2=$this->CheckAllowedCoursesToEnroll();
	
			$CoursesInElective = array_unique(array_merge(
													(array)$d2['CoursesFailed_Elective'],
													(array)$d2['Final_Courses_WithPrereq_Elective'], 
													(array)$d2['Courses_NoPrereq_Elective'], 
													(array)$d2['NotTaken_WPassedPrereq_Elective'], 
													(array)$d2['NotTaken_WCreditedPrereq_Elective']));

			$CoursesInElective = array_values($CoursesInElective);
			$CoursesInElective = json_encode($CoursesInElective);
			$CoursesInElective = str_replace(array('"','[',']'),array("'","(",")"),$CoursesInElective);
			

			$CoursesInProspectus = array_unique(array_merge(
													(array)$d2['CoursesFailed'],
													(array)$d2['Final_Courses_WithPrereq'], 
													(array)$d2['Courses_NoPrereq_Prospectus'], 
													(array)$d2['NotTaken_WPassedPrereq'],
													(array)$d2['NotTaken_WCreditedPrereq']));

			$CoursesInProspectus = array_values($CoursesInProspectus);
			$CoursesInProspectus = json_encode($CoursesInProspectus);
			$CoursesInProspectus = str_replace(array('"','[',']'),array("'","(",")"),$CoursesInProspectus);

			//print_r($d2['NotTaken_WPassedPrereq']); die();
			//print_r($CoursesInProspectus); die();

			if (!empty($d2['MyAdvisedCourses'])) {
				$AdvisedCourses = json_encode($d2['MyAdvisedCourses']);
				$AdvisedCourses = str_replace(array('"','[',']'),array("'","(",")"),$AdvisedCourses);
			} else {
				$AdvisedCourses = "('')";
			}			
			
			$academic_terms = $this->AcademicYears_Model->getCurrentAcademicTerm();

			$this->session->set_userdata('academic_terms_id',$academic_terms->id);
			
			
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			$data['ConflictEnrollmentID'] = $this->ConflictEnrollmentID;
			
			$block_id = $this->Student_Model->getStudentBlock($this->session->userdata('idno'), $this->session->userdata('academic_terms_id'), 
							$this->session->userdata('prospectus_id'));

			//print_r($block_id); die();

			if (isset($block_id) AND $block_id->block_sections_id != NULL) {				
				$data['courses'] = $this->Offerings_Model->ListOfferingsThatCanBeTakenByBlock($this->session->userdata('academic_terms_id'),$CoursesInProspectus,$AdvisedCourses,
																		$CoursesInElective,$this->session->userdata('prospectus_id'),$block_id->block_sections_id,
																		$this->session->userdata('student_histories_id'));				
			} else {
				$data['courses'] = $this->Offerings_Model->ListOfferingsThatCanBeTaken($this->session->userdata('academic_terms_id'),
															$CoursesInProspectus,$AdvisedCourses,
															$CoursesInElective,$this->session->userdata('prospectus_id'), 
															$this->session->userdata('student_histories_id'));
			}			
			$data['grades']    = $this->Grades_Model->ListStudentGrades($this->session->userdata('academic_terms_id'),$this->session->userdata('idno'));
			
			$data['AlertMsg'] = $this->AlertMsg;
			$data['AlertType'] = $this->AlertType;
			
			//print_r($CoursesInProspectus);
			//die();
			

			if (!$return) {
				$data['title'] = "Enroll Courses";
				$this->content_lib->enqueue_body_content('student/header_title', $data);
				$this->content_lib->enqueue_body_content('student/list_offerings_that_can_be_taken', $data);
				$this->content_lib->content();
			} else {
				//$data['title'] = "Offerings";
				//$this->content_lib->enqueue_body_content('student/header_title', $data);
				$this->content_lib->enqueue_body_content('student/view_offerings', $data);
				$this->content_lib->content();			
			}
	
		}


		public function __destruct(){
			$this->db->close();
		}

		
		/**
		 * Description:
		 * This method generates a pdf file of the list of students in a particular class handled by the faculty member.
		 */
		function generate_class_schedule_pdf($academic_term_id, $max_units) {
			
			$this->load->model("hnumis/Reports_model");

			//print_r($this->userinfo);
			//die();
			$this->userinfo['max_bracket_units'] = $max_units;
			$current = $this->AcademicYears_Model->getCurrentAcademicTerm();
			$this->Reports_model->generate_student_class_schedule_pdf($academic_term_id, $this->userinfo, $current);
		
		}
		
}
