<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='family_info') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?> 		
		<form id="student_family_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_family_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<div class="control-group formSep">
					<label for="fathers_name" class="control-label">Father's Name:</label>
					<div class="controls" id="place_of_birth_controls">
						<input type="text" name="fathers_name" id="fathers_name" class="input-xlarge" value="<?php if(isset($fathers_name)) echo $fathers_name; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="fathers_address" class="control-label">Father's Address:</label>
					<div class="controls" id="fathers_address_controls">
<?php if ( ! empty($fathers_address)): ?>
						<div style="margin-top:6px" id="fathers_address_control_text"><strong><?php echo $fathers_address; ?></strong>&nbsp;<small><a href="javascript://" id="fathers_address_control"  class="edit_toggle" >Edit</a></small></div>
<?php endif; ?>
						<div id="fathers_address_control_controls"></div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="mothers_name" class="control-label">Mother's Name:</label>
					<div class="controls" id="place_of_birth_controls">
						<input type="text" name="mothers_name" id="mothers_name" class="input-xlarge" value="<?php if(isset($mothers_name)) echo $mothers_name; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="mothers_address" class="control-label">Mother's Address:</label>
					<div class="controls" id="mothers_address_controls">
<?php if ( ! empty($mothers_address)): ?>
						<div style="margin-top:6px" id="mothers_address_control_text"><strong><?php echo $mothers_address; ?></strong>&nbsp;<small><a href="javascript://" id="mothers_address_control"  class="edit_toggle" >Edit</a></small></div>
<?php endif; ?>
						<div id="mothers_address_control_controls"></div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="num_brothers" class="control-label">Number of Brothers:</label>
					<div class="controls" id="num_brothers_controls">
						<input type="text" name="num_brothers" id="num_brothers" class="input-xlarge span1" value="<?php if(isset($num_brothers)) echo $num_brothers; else echo "0"; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="num_sisters" class="control-label">Number of sisters:</label>
					<div class="controls" id="num_sisters_controls">
						<input type="text" name="num_sisters" id="num_sisters" class="input-xlarge span1" value="<?php if(isset($num_sisters)) echo $num_sisters; else echo "0"; ?>" />
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button id="update_family_information" class="btn btn-primary" type="submit">Update Family Information</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>