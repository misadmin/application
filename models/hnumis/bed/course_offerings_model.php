<?php

	class Course_offerings_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

		private $bed_course_offerings_slots_id = NULL;
		
		
		function get_BEDCourse_Offerings_Slots_Id() {
			return $this->bed_course_offerings_slots_id;
		}
		
		
		public function AddBEDCourseOfferings($data=NULL) {

			$q1 = "INSERT INTO
							bed_course_offerings (academic_years_id, 
												courses_id, 
												inserted_by,
												inserted_on)
						VALUES (
							{$this->db->escape($data['academic_years_id'])},
							{$this->db->escape($data['courses_id'])},
							{$this->db->escape($data['inserted_by'])},
							NOW()) ";
			//return $q1;
			if ($this->db->query($q1)) {
				$data['bed_course_offerings_id'] = $this->db->insert_id();
				
				if ($this->AddBEDCourseOfferingsSlots($data)) { //add to bed_course_offerings_slots table
					if ($this->AddBEDBlockCourseOfferings($data)) { //add to bed_block_course_offerings table
						return TRUE;
					} else {
						return FALSE;
					}
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
			
		}

		
		public function AddBEDCourseOfferingsSlots($data=NULL) {
			
			$q1 = "INSERT INTO
						bed_course_offerings_slots (bed_course_offerings_id,
												rooms_id,
												start_time,
												end_time,
												inserted_by)
						VALUES (
							{$this->db->escape($data['bed_course_offerings_id'])},
							{$this->db->escape($data['rooms_id'])},
							{$this->db->escape($data['start_time'])},
							{$this->db->escape($data['end_time'])},
							{$this->db->escape($data['inserted_by'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				$this->bed_course_offerings_slots_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}	
			
		}
		
		
		private function AddBEDBlockCourseOfferings($data=NULL) {
			
			$q1 = "INSERT INTO
						bed_block_course_offerings (bed_block_sections_id,
												bed_course_offerings_id,
												inserted_by)
						VALUES (
							{$this->db->escape($data['block_sections_id'])},
							{$this->db->escape($data['bed_course_offerings_id'])},
							{$this->db->escape($data['inserted_by'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

			
		public function DeleteOffering($bed_course_offerings_id=NULL) {
			
			$q2 = "DELETE FROM 
							bed_course_offerings 
						WHERE 
							id = {$this->db->escape($bed_course_offerings_id)} ";
			//return $q2;
			if ($this->db->query($q2)) {
				return TRUE;
			} else {
				return FALSE;
			}

		}


		public function DeleteOffering_Slots($bed_course_offerings_slots_id=NULL) {

			$q2 = "DELETE FROM 
							bed_course_offerings_slots 
						WHERE 
							id = {$this->db->escape($bed_course_offerings_slots_id)} ";
			//return $q2;
			if ($this->db->query($q2)) {
				return TRUE;
			} else {
				return FALSE;
			}

		}

		
		public function AssignTeacher($data=NULL) {
			$q1 = "UPDATE bed_course_offerings
					SET employees_empno = {$this->db->escape($data['employees_empno'])},
						updated_by = {$this->db->escape($data['updated_by'])}
		 			WHERE
						id = {$this->db->escape($data['bed_course_offerings_id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
			
		}


		public function UpdateOfferingSlotsRoom($data=NULL) {
			
			$q1 = "UPDATE 
							bed_course_offerings_slots
						SET 
							rooms_id = {$this->db->escape($data['rooms_id'])}
						WHERE
							bed_course_offerings_id IN (SELECT 
															bed_course_offerings_id 
														FROM 
															bed_block_course_offerings 
														WHERE 
															bed_block_sections_id = {$this->db->escape($data['block_sections_id'])}
													) ";
					
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		
		}


		public function UpdateSchedule_CourseOfferingsSlot($data=NULL) {
			
			$q1 = "UPDATE 
							bed_course_offerings_slots
						SET 
							rooms_id = {$this->db->escape($data['rooms_id'])},
							start_time = {$this->db->escape($data['start_time'])},
							end_time = {$this->db->escape($data['end_time'])},
							updated_by = {$this->db->escape($data['encoded_by'])}
						WHERE
							id = {$this->db->escape($data['bed_course_offerings_slots_id'])} ";
					
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		
		}
		
		
		public function CheckIfCourseOffered($data=NULL) {
			$result = null;
			
			$q = "SELECT 
						g.courses_id,
						f.year_level,
						f.term,
						d.id
					FROM
						bed_course_offerings AS a,
						bed_grades AS b,
						bed_block_course_offerings AS c,
						basic_ed_histories AS d,
						prospectus AS e,
						prospectus_terms AS f,
						prospectus_courses AS g,
						bed_academic_programs AS h
					WHERE 
						a.id = b.bed_course_offerings_id
						AND b.bed_course_offerings_id = c.bed_course_offerings_id 
						AND c.bed_block_sections_id = d.bed_block_sections_id
						AND d.prospectus_id = e.id 
						AND e.id = f.prospectus_id 
						AND f.id = g.prospectus_terms_id 
						AND e.academic_programs_id = h.academic_programs_id
						AND h.year_level = f.year_level
						AND a.courses_id = g.courses_id
						AND g.courses_id = {$this->db->escape($data['courses_id'])} 
						AND g.id = {$this->db->escape($data['prospectus_course_id'])} 
					LIMIT 1 ";
			
			//return $q;
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
			
		}

		
		public function getCourse_Offerings($block_sections_id=NULL, $courses_id=NULL, $academic_years_id=NULL) {
			$result = null;
			
			$q = "SELECT 
						a.id AS course_offerings_id
					FROM
					    bed_course_offerings AS a,
						bed_block_course_offerings AS b
					WHERE 
						a.id = b.bed_course_offerings_id
						AND b.bed_block_sections_id = {$this->db->escape($block_sections_id)}
					    AND a.courses_id = {$this->db->escape($courses_id)}
						AND a.academic_years_id={$this->db->escape($academic_years_id)} ";
			
			//return $q;
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
		
		function CountOffering_Slots($bed_course_offerings_id=NULL) {
			$result = null;
			
			$q = "SELECT 
						COUNT(*) AS cnt
					FROM
					    bed_course_offerings_slots AS a
					WHERE 
						a.bed_course_offerings_id = {$this->db->escape($bed_course_offerings_id)} ";
			
			//return $q;
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
		
		
	}
	
?>
