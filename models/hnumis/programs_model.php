<?php

	class Programs_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListProgramGroups() {
			$result = null;
			
			$q1 = "SELECT * 
						FROM 
							acad_program_groups 
						WHERE
							levels_id IS NOT NULL
						GROUP BY 
							abbreviation
						ORDER BY
							levels_id,group_name";
			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		function getProgram($academic_programs_id) {
			$result = null;
			
			$q1 = "SELECT 
							a.id, 
							a.abbreviation, 
							a.description, 
							a.acad_program_groups_id, 
							a.colleges_id, 
							a.departments_id,
							a.max_yr_level,
							b.abbreviation AS group_abbreviation
						FROM 
							academic_programs AS a,
							acad_program_groups AS b
						WHERE 
							a.acad_program_groups_id=b.id
							AND a.id = {$this->db->escape($academic_programs_id)}";
			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}

		
		//ADDED: 8/7/13 by genes
		function getProgramGroup($acad_program_groups_id) {
			$result = null;
				
			$q1 = "SELECT 
						a.id, 
						a.group_name,
						GROUP_CONCAT(' ',b.abbreviation ORDER BY b.abbreviation) AS prog
					FROM 
						acad_program_groups AS a LEFT JOIN academic_programs AS b ON a.id=b.acad_program_groups_id
					WHERE 
						a.id = {$this->db->escape($acad_program_groups_id)}";
				
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
			$result = $query->row();
			}
				
			return $result;
				
		}
		
		
		public function all_programs ($college_id, $status='', $distinct=TRUE){
			
			$sql = "SELECT
					";
			if ($distinct)
				$sql.= 'DISTINCT(ap.abbreviation),'; else
				$sql.= 'ap.abbreviation,';
			
			$sql .= "
					ap.id,
					ap.description,
					ap.status as program_status,
					p.effective_year,
					p.status as prospectus_status
			FROM
					academic_programs as ap
			LEFT JOIN
					prospectus as p
			ON
					p.academic_programs_id=ap.id
			WHERE
					ap.colleges_id='{$college_id}'
			";
			
			if ($status=='F' || $status=='U' || $status=='O' )
				$sql.= "AND status='{$status}'
						";
			
			if ($distinct)
				$sql.= "GROUP BY ap.abbreviation
						";
			
			$query = $this->db->query($sql);
			
			if ( $query && $query->num_rows() > 0)
				return $query->result(); 
			else {
				echo $this->db->_error_message() . " " . $sql;
				return FALSE;
			}
		}

		//Added: March 6, 2013 by Amie
		//EDITED: 8/8/13 by genes added description field
		public function all_programs_colleges (){
			
			$sql = "SELECT DISTINCT(ap.abbreviation),
					ap.id,
					ap.description
				FROM
					academic_programs as ap
				WHERE
					ap.status='O' 
				ORDER BY ap.abbreviation ASC ";
			
			
			$query = $this->db->query($sql);
			
			if ( $query && $query->num_rows() > 0)
				return $query->result(); 
			else {
				echo $this->db->_error_message() . " " . $sql;
				return FALSE;
			}
		}

		
		/***************************************************************************************************************
		 *                                                                                                              *
		*                                                                                                              *
		****************************************************************************************************************/
		/*NOTE: Changed last 01/26/2013 */
		function ListPrograms($colleges_id, $status) {
			$result = null;
				
			if ($status == 'O') {
				$q1 = "SELECT
							a.description,
							a.abbreviation,
							a.id,
							a.acad_program_groups_id,
							b.id AS prospectus_id,
							b.effective_year, 
							IF(a.status='O','OFFERED','FROZEN') AS status
						FROM
							academic_programs AS a,
							prospectus AS b
						WHERE
							a.id=b.academic_programs_id 
							AND b.status='A'
							AND a.status='O'
							AND a.colleges_id = {$this->db->escape($colleges_id)}
						ORDER BY 
							a.abbreviation, b.effective_year DESC";
			} else {
				$q1 = "SELECT
							a.description,
							a.abbreviation,
							a.id,
							IF(a.status='O','OFFERED','FROZEN') AS status
						FROM
							academic_programs AS a
						WHERE
							a.colleges_id = {$this->db->escape($colleges_id)}
						ORDER BY 
							a.abbreviation ";
			}				
			//print_r($q1);die();
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
		}
		
		
		/*NOTE: Changes made on 12/30/12*/
		function AddProgram($program_data) {
				
			if ($program_data['departments_id']) {
			$query = "
						INSERT INTO
							academic_programs (id, departments_id, colleges_id, acad_program_groups_id, abbreviation, description, status, max_yr_level, inserted_by, inserted_on)
						VALUES (
							'',
							'".$program_data['departments_id']."',
							'".$program_data['colleges_id']."',
							'".$program_data['acad_program_groups_id']."',
							'".$program_data['abbreviations']."',
							'".$program_data['description']."',
							'".$program_data['status']."',
							'".$program_data['max_yr_level']."',
							'".$program_data['inserted_by']."',
							NOW() )";
			} else {
			$query = "
					INSERT INTO
							academic_programs (id, colleges_id, acad_program_groups_id, abbreviation, description, status, max_yr_level, inserted_by, inserted_on)
							VALUES (
							'',
							'".$program_data['colleges_id']."',
							'".$program_data['acad_program_groups_id']."',
							'".$program_data['abbreviations']."',
							'".$program_data['description']."',
							'".$program_data['status']."',
							'".$program_data['max_yr_level']."',
							'".$program_data['inserted_by']."',
							NOW() )";
			}
			//print($query); die();
			$result = $this->db->query($query);
			return $this->db->insert_id();
		}
				
	//added: 2/16/2013			
		function ListGraduatePrograms() {
			$result = null;
				
			$q1 = "SELECT
					a.description,
					a.abbreviation,
					a.id,
					b.id AS prospectus_id,
					b.effective_year
					FROM
						academic_programs AS a,
						prospectus AS b,
						acad_program_groups as c
					WHERE
						c.id = a.acad_program_groups_id
						AND a.id=b.academic_programs_id 
						AND b.status='A'
						AND a.status='O'
						AND c.group_type = 'P'
					ORDER BY 
						a.abbreviation, b.effective_year DESC";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}		
				
	//added: 2/16/2013			
	function ListProgramsByDepartment($department) {
		$result = null;
				
		if($department == 'C') {
			$q1 = "SELECT
						a.description,
						a.abbreviation,
						a.id,
						b.id AS prospectus_id,
						b.effective_year 
					FROM
						academic_programs AS a,
						prospectus AS b,
						acad_program_groups as c
					WHERE
						c.id = a.acad_program_groups_id
						AND a.id=b.academic_programs_id 
						AND b.status='A'
						AND a.status='O'
						AND c.group_type = 'C'
					ORDER BY 
						a.abbreviation, b.effective_year DESC";
			} elseif($department == 'P'){
			    $q1 = "SELECT
							a.description,
							a.abbreviation,
							a.id,
							b.id AS prospectus_id,
							b.effective_year 
						FROM
							academic_programs AS a,
							prospectus AS b,
							acad_program_groups as c
						WHERE
							c.id = a.acad_program_groups_id
							AND a.id=b.academic_programs_id 
							AND b.status='A'
							AND a.status='O'
							AND c.group_type = 'P'
						ORDER BY 
							a.abbreviation, b.effective_year DESC";
			}				
			else{
				$q1 = "SELECT
							a.description,
							a.abbreviation,
							a.id,
							b.id AS prospectus_id,
							b.effective_year 
						FROM
							academic_programs AS a,
							prospectus AS b,
							acad_program_groups as c
						WHERE
							c.id = a.acad_program_groups_id
							AND a.id=b.academic_programs_id 
							AND b.status='A'
							AND a.status='O'
							AND c.group_type = 'L'
						ORDER BY 
							a.abbreviation, b.effective_year DESC";
			}				
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}
	
	//Added: 2/23/2013
	//todo: this method is the same as: all_programs and ListPrograms... 
	function ListAcaProgramsbyCollege($colleges_id) {
		$result = null;
				
		$q1 = "SELECT
						a.description,
						a.abbreviation,
						a.id,
						a.status AS status,
						a.acad_program_groups_id,
						a.max_yr_level
					FROM
						academic_programs AS a
					WHERE
						a.colleges_id = {$this->db->escape($colleges_id)} 
						AND a.status = 'O'
					ORDER BY 
						a.abbreviation";
						
		$query = $this->db->query($q1);
			
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;			
	}
	
	/**
	 * update programs given the id's and status...
	 * 
	 * @param array $program_ids of ids
	 * @param string $status 'U', 'F', 'O'
	 */
	function update_programs($program_ids=array(), $status='O'){
		$sql = "
			UPDATE
				academic_programs
			SET
				status={$this->db->escape($status)}
			WHERE
				id
			IN (
			";
		
		if (!empty($program_ids) && count($program_ids) > 0){
			foreach ($program_ids as $program_id){
				$sql .= "{$this->db->escape($program_id)},"; 	
			}
			$sql = rtrim($sql, ',') . ")\n";
			return $this->db->query($sql);
		} else {
			return FALSE;
		}
	}

	function academic_program_info ($id){
		$sql = "
			SELECT
				id,
				abbreviation,
				description
			FROM
				academic_programs
			WHERE
				id={$this->db->escape($id)}
			";
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->row();
		} else {
			return FALSE;
		}
	}
	
	function getNumber_of_Years($academic_programs_id) {
		$sql = "
			SELECT
				a.id,
				a.max_yr_level
			FROM
				academic_programs AS a
			WHERE
			
				a.id={$this->db->escape($academic_programs_id)}	";
				
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->row();
		} else {
			return FALSE;
		}
	}
	
	function ListAcadPrograms($acad_program_groups_id) {
		$result = null;
				
		$q1 = "SELECT
					a.id,
		 			a.abbreviation,
					a.status AS status
					FROM
						academic_programs AS a
					WHERE
						a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)} 
						AND a.status = 'O'";
		//print($q1);
	//	die();
		$query = $this->db->query($q1);
			
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;			
	
	}
	
	//I use this so that only acad_program_groups with academic_programs will be returned
	function ListProgramWithGroups() {
			$result = null;
			
			$q1 = "SELECT 
							a.* 
						FROM 
							acad_program_groups AS a,
							academic_programs AS b
						WHERE
							a.id = b.acad_program_groups_id
							AND b.status = 'O'
						GROUP BY 
							a.abbreviation";
			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
	}

//Added: 4/23/2013

    function ListProgramGroupbyCollege($college_id) {
			$result = null;
			
			$q1 = "SELECT 
							a.* 
						FROM 
							acad_program_groups AS a,
							academic_programs AS b
						WHERE
							b.colleges_id = {$this->db->escape($college_id)} 
							AND a.id = b.acad_program_groups_id
							AND b.status = 'O'
						GROUP BY 
							a.abbreviation";
			
			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
	}
	
	function getDepartment($department_id) {
			$result = null;
			
			$q1 = "SELECT department
						FROM departments
						WHERE id = {$this->db->escape($department_id)}";
			
				$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
		
			
	function updateAcademicProgram($academic_program_id, $abbreviation, $description, $group_id){

		$q1 = "UPDATE academic_programs
					SET acad_program_groups_id = {$this->db->escape($group_id)},
					    abbreviation = {$this->db->escape($abbreviation)},
						description = {$this->db->escape($description)}
		 			WHERE
						id = {$this->db->escape($academic_program_id)}";
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
	
	//Added: April 27, 2013 by Amie
	function list_payers_by_program($program_id, $year) {
		$result = null;
			
			$q1 = "SELECT DISTINCT(py.id) as payers_id, s.idno as students_idno
					FROM students as s
						LEFT JOIN payers as py ON py.students_idno = s.idno
						LEFT JOIN student_histories as sh ON s.idno=sh.students_idno
						LEFT JOIN prospectus as p ON p.id = sh.prospectus_id
						LEFT JOIN academic_programs ap ON ap.id = p.academic_programs_id
					WHERE
						ap.id = {$this->db->escape($program_id)}
						AND sh.year_level = {$this->db->escape($year)}
					ORDER BY s.lname";
			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
	}
	
	//Added: 8/2/2013
	function ListCollegeProgramGroups($colleges_id) {
		$result = null;
	
		$q1 = "SELECT
					b.group_name,
					b.abbreviation,
					b.id,
					GROUP_CONCAT(' ',a.abbreviation ORDER BY a.abbreviation) AS prog
				FROM
					academic_programs AS a,
					acad_program_groups AS b
				WHERE
					a.acad_program_groups_id=b.id
					AND a.colleges_id = {$this->db->escape($colleges_id)}
					AND a.status = 'O'
				GROUP BY
					b.group_name
				ORDER BY
					b.abbreviation";
		
		//print($q1);
		//die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
	}
	
	//Added: 3/12/2014 by Isah
	
	function ListAcaPrograms() {
		$result = null;
	
		$q1 = "SELECT
		a.description,
		a.abbreviation,
		a.id,
		a.status AS status,
		a.colleges_id
		FROM
		academic_programs AS a
		WHERE
		a.status = 'O'
		ORDER BY a.abbreviation";
	
		$query = $this->db->query($q1);
			
		if($query->num_rows() > 0){
		$result = $query->result();
	}
		return $result;
	}
	
	function listAccreditors() {
		$result = null;
	
		$q1 = "SELECT
					a.id,
					a.acronym,
					a.accreditor
				FROM
					accreditors AS a
		
				ORDER BY a.accreditor";
	
		$query = $this->db->query($q1);
			
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
	}
	
	function addAccredLevel($accred_data) {
	
		$query = "
						INSERT INTO
							accreditations (id, academic_programs_id, accreditors_id, level, date_approved, start_date, end_date)
						VALUES (
							'',
							'".$accred_data['program_id']."',
							'".$accred_data['accreditor_id']."',
							'".$accred_data['level']."',
							'".$accred_data['date_approved']."',
									'".$accred_data['start_date']."',
									'".$accred_data['end_date']."' )";
		
		$result = $this->db->query($query);
		return $this->db->insert_id();
	}
	
	
	//ADDED: genes 3/25/14
	function ListAccreditedPrograms() {

		$result = null;
		
		$q1 = "SELECT
					c.abbreviation,
					b.acronym,
					a.level,
					DATE_FORMAT(a.date_approved,'%M %e, %Y') AS date_approved,
					DATE_FORMAT(a.start_date,'%M %e, %Y') AS start_date,
					DATE_FORMAT(a.end_date,'%M %e, %Y') AS end_date
				FROM
					accreditations AS a,
					accreditors AS b,
					academic_programs AS c
				WHERE
					a.accreditors_id = b.id
					AND a.academic_programs_id = c.id 
				ORDER BY 
					c.abbreviation ";
		
		//print($q1); die();
		$query = $this->db->query($q1);

		
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
		
	}

	
	/*
	 * @ADDED: 10/2/14
	 * @author: genes
	 */
	function getProgramByProspectusID($prospectus_id) {
		$result = null;
			
		$q1 = "SELECT
					a.abbreviation,
					a.description,
					b.effective_year,
					c.name
				FROM
					academic_programs AS a,
					prospectus AS b,
					colleges AS c
				WHERE
					b.academic_programs_id=a.id
					AND a.colleges_id=c.id
					AND b.id = {$this->db->escape($prospectus_id)}";
			
		$query = $this->db->query($q1);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
			
	}
	
 	/*
 	 * @ADDED: 3/25/15 by genes 
 	 */
 	function ListProgramsWithGraduates($college_id,$acad_term) {
 		$result = null;

		$q = "SELECT 
					DISTINCT(f.id) AS academic_programs_id,
					f.abbreviation
				FROM 
					student_histories AS c, 
					graduated_students AS d, 
					prospectus AS e, 
					academic_programs AS f
				WHERE
					c.id=d.student_histories_id
					AND c.prospectus_id=e.id
					AND e.academic_programs_id=f.id  
					AND f.colleges_id = {$this->db->escape($college_id)}
					AND c.academic_terms_id = {$this->db->escape($acad_term)}
				ORDER BY
					f.abbreviation ";
 		//print($q); die();
 		$query = $this->db->query($q);
 		
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 				
 		return $result;			
 			
 	}

	
}
	
//program_model.php