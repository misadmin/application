<div class="row-fluid">
	<ul class="nav nav-list">
		<li><a href="https://www.hnu.edu.ph/policies_and_memos/HRDMO-2018-09.pdf" target="_blank">HRDMO-2018-09: Application for Leave of Absence</a></li>
		<li><br><em>Policies and memoranda for faculty and staff will soon be added here for quick reference.</em></li>
	</ul>
</div>
