<div class="row-fluid">
<?php if(isset($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
		<a class="close" data-dismiss="alert">&times;</a>
		<?php echo $message; ?>
	</div>
<?php endif; ?>
	<form id="move_student_form" method="post" action="" class="form-horizontal">
		<div class="formSep">
			<h3>Move Existing Student to College</h3>
		</div>
	<?php echo $this->common->hidden_input_nonce(); ?>
	<?php action_form('add_college_history', FALSE); ?>
		<fieldset>
				<div class="control-group formSep">
					<label for="college" class="control-label">College: </label>
					<div class="controls">
						<select id="colleges" name="college">
							<option value="">-- Select College --</option>
<?php foreach ($colleges as $college): ?>
							<option value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="programs" class="control-label">Program: </label>
					<div class="controls">
						<select id="programs" name="program">
							<option value="">-- Select Program --</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Enroll Student to College</button>
					</div>
				</div>	
		</fieldset>
	</form>
</div>
<script>
jQuery.validator.addMethod("notEqual", function(value, element, param) {
	return this.optional(element) || value != param;
}, "Please select a program");

$('#move_student_form').validate({
	onkeyup: false,
	errorClass: 'error',
	validClass: 'valid',
	rules: {
		college: { required: true },
		program: { required: true }
	},
	highlight: function(element) {
		$(element).closest('div').addClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	unhighlight: function(element) {
		$(element).closest('div').removeClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	
	errorPlacement: function(error, element) {
		$(element).closest('div').append(error);
	}
});

$('#colleges').change(function(){
	var college_id = $('#colleges option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
		dataType: "json",
		success: function(data){
			var doptions = make_options(data);
			$('#programs').html(doptions);
		}	
	});	
});

function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}
</script>