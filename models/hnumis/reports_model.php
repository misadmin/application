<?php // DEV COPY

	class Reports_Model extends CI_Model {

  		function __construct() {
        	parent::__construct();

   		}


		function enrollment_summary() {

			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/College_Model');
			$this->load->model('hnumis/Enrollments_Model');
			$this->load->model('hnumis/Programs_Model');
			$this->load->model('hnumis/offerings_model');
			$this->load->model('academic_terms_model');
				
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			$colleges = $this->College_Model->ListColleges();
			$acad_terms = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
			$all_students = new stdClass();
	//die("ok");		
			switch ($step) {
				case 1:
					$current_term = $this->academic_terms_model->getCurrentAcademicTerm();
					$offerings = count($this->offerings_model->ListAllOfferings($current_term->id));
					$total_units = $this->Enrollments_Model->getTotal_Units($current_term->id);
					$total_enrollees = $this->Enrollments_Model->getTotalEnrollments($current_term->id);					
					$all_students = $this->College_Enrollment_Summary($colleges, $current_term, $current_term->id, $total_units, $total_enrollees, $offerings);

					
					/*foreach($all_students AS $k=>$stud) {
						print('== '.$k.' = '.$stud['name'].'<br>');
						//print('=='.$stud['name']['abbreviation'].'<br>');
						if (isset($stud[$k])) {
							foreach($stud[$k] AS $k1=>$my_stud) {
								//print($my_stud['abbreviation'].'<br>');
								print('** '.$k1.' * '.$my_stud['abbreviation'].'<br>');
							}
						}
					}
					//print_r($all_students); 
					die();*/
					
					$data = array(
							"terms"=>$acad_terms,
							"selected_term"=>$current_term->id,
							"students"=>$all_students,
							"offerings"=>$offerings,
							"total_units"=>$total_units,
							"enrollees"=>$total_enrollees,
							"show_current_term" => $current_term->term.' '.$current_term->sy,
					);
					
				
					break;
				case 3:
					$term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
					$offerings       = count($this->offerings_model->ListAllOfferings($this->input->post('academic_terms_id')));
					$total_units     = $this->Enrollments_Model->getTotal_Units($this->input->post('academic_terms_id'));
					$total_enrollees = $this->Enrollments_Model->getTotalEnrollments($this->input->post('academic_terms_id'));
						
					$all_students = $this->College_Enrollment_Summary($colleges, $term, $term->id, $total_units, $total_enrollees, $offerings);
					
					$data = array(
							"terms"=>$acad_terms,
							"selected_term"=>$this->input->post('academic_terms_id'),
							"students"=>$all_students,
							"offerings"=>$offerings,
							"total_units"=>$total_units,
							"enrollees"=>$total_enrollees,
							"show_current_term" => $term->term.' '.$term->sy,
					);
					break;
				case 4:
					$this->create_enrollment_summary($this->input->post('academic_terms_id'), 1);
					return;
				case 5:
					$data['students'] = $this->Enrollments_Model->masterlist($this->input->post('academic_terms_id'), $this->input->post('program_id'), $this->input->post('year_level'));
					$data['program_abbreviation'] = $this->input->post('program_abbreviation');
					$data['sy'] = $this->input->post('sy');
					$data['ylevel'] = $this->input->post('ylevel');
					$this->content_lib->enqueue_body_content('posting/list_enrollees',$data);
					$this->content_lib->content();
					return;
			}

			$this->content_lib->enqueue_body_content('posting/enrollment_summary',$data);
			$this->content_lib->content();
				
		}


		function assessment_summary() {

			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('teller/Assessment_Model');
			$this->load->model('hnumis/Enrollments_Model');
			$this->load->model('hnumis/Programs_Model');

			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);

			switch ($step) {
				case 1:
					$this->list_academic_terms();
					break;
				case 2:
					//die($this->input->post('post_status'));
					$this->create_assessment_summary($this->input->post('academic_terms_id'), 0, NULL, 
														$this->input->post('post_status'));
					break;
				case 3:
					//print("TOTAL ASSESSMENT: ".$this->input->post('total_assessment')); die();
					$this->create_assessment_summary($this->input->post('academic_terms_id'), 1, 
														$this->input->post('total_assessment'), 
														$this->input->post('post_status'));
					break;
			}

		}


		function student_certificate() {

			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/College_Model');
			$this->load->model('hnumis/Enrollments_Model');
			$this->load->model('hnumis/Programs_Model');

			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);

			switch ($step) {
				case 1:
					$this->list_academic_terms();
					break;
				case 2:
					$this->create_enrollment_summary($this->input->post('academic_terms_id'), 0);
					break;
				case 3:
					$this->create_enrollment_summary($this->input->post('academic_terms_id'), 1);
					break;
			}

		}


		function list_academic_terms() {

			$this->load->model('academic_terms_model');

			$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
			$academic_terms = $this->academic_terms_model->current_academic_term();
			$data['current_term'] = $this->academic_terms_model->getCurrentAcademicTerm();

			$this->content_lib->enqueue_body_content('posting/list_academic_terms',$data);
			$this->content_lib->content();

		}


		function create_enrollment_summary($academic_terms_id, $value) {

			$data['term'] = $this->AcademicYears_Model->getAcademicTerms($academic_terms_id);

			$data['academic_terms_id'] = $academic_terms_id;
			$data['colleges'] = $this->College_Model->ListColleges();
			if ($value == 0) {
				$this->content_lib->enqueue_body_content('posting/enrollment_summary',$data);
				$this->content_lib->content();
			} else {
				$this->generate_enrollment_summary_pdf($academic_terms_id, $data);
			}

		}


		function create_assessment_summary($academic_terms_id, $value, $assessment, $post_status) {

			$data['term'] = $this->AcademicYears_Model->getAcademicTerms($academic_terms_id);
			$data['academic_terms_id'] = $academic_terms_id;
			//$data['programs'] = $this->Programs_Model->ListProgramGroups();
			//$data['assessment'] = json_decode($assessment); 
			$data['post_status'] = $post_status;

			//added Toyet 12.11.2017
			$termdate = $this->AcademicYears_Model->getTermDateByAcadTermsID($academic_terms_id);
			$date_start = $termdate->start_date;
			$month = ((int)substr($date_start,6,2))+4;
			$date_end = substr($date_start,0,4).'-'.(string)$month.'-30';
			//log_message('INFO',print_r($date_end,true));
			$data['college_assmts'] = $this->assessment_model->getComputedAssessment($date_start,$date_end);
			//log_message('INFO',print_r($data->college_assmts,true));
			//print($assessment); die();

			
			if ($value == 0) {
				$this->content_lib->enqueue_body_content('posting/assessment_summary',$data);
				$this->content_lib->content();
			} else {
				$this->generate_assessment_summary_pdf($academic_terms_id, $data);
			}

		}
 

		function generate_enrollment_summary_pdf($academic_terms_id, $data) {

			$this->load->library('my_pdf');
			$this->load->library('hnumis_pdf');

			$this->hnumis_pdf->AliasNbPages();
			$this->hnumis_pdf->set_HeaderTitle('ENROLLMENT SUMMARY - '.$data['term']->term." ".$data['term']->sy);
			$this->hnumis_pdf->AddPage('L','folio');

			$this->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->hnumis_pdf->SetDrawColor(165,165,165);
			$this->hnumis_pdf->Ln();

			//$this->hnumis_pdf->SetFont('times','B',12);

			$this->hnumis_pdf->SetFont('times','B',9);
			$this->hnumis_pdf->SetFillColor(116,116,116);
			$this->hnumis_pdf->SetTextColor(253,253,253);

			$this->hnumis_pdf->SetX(15);
			$this->hnumis_pdf->Cell(50,15,'College',1,0,'C',true);

			$r = 1;
			$x = 65;
			$y = 35;

			$header1 = array('1st Year','2nd Year','3rd Year','4th Year','5th Year','GRAND TOTAL');

			$w1 = array(42,42,42,42,42,42);

			for($i=0;$i<count($w1);$i++)
				$this->hnumis_pdf->Cell($w1[$i],10,$header1[$i],1,0,'C',true);


			$header2 = array('M','F','TOTAL','M','F','TOTAL','M','F','TOTAL','M','F','TOTAL',
									'M','F','TOTAL','M','F','TOTAL');

			for($i=0;$i<count($header2);$i++) {
				$this->hnumis_pdf->SetXY($x,$y);
				$this->hnumis_pdf->MultiCell(14,5,$header2[$i],1,'C',true);

				$x = $x + 14;
				$r++;
				if ($r > 18) {
					$r=1;
					$x=10;
					$y = $y + 28;
				}
			}

			$w=array(50,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14);

			$this->hnumis_pdf->SetFont('Arial','',10);
			$this->hnumis_pdf->SetTextColor(0,0,0);
			$grand_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
										'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
										'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
										'total_m'=>0,'total_f'=>0,'grand_total'=>0);
			$program_total = 0;
			foreach ($data['colleges'] as $college) {
				$this->hnumis_pdf->SetX(15);
				$programs = $this->Enrollments_Model->ListEnrollmentSummary($college->id, $academic_terms_id);

				$total1 = 0;$total2 = 0;$total3 = 0;$total4 = 0;$total5 = 0;$total_m = 0;$total_f = 0;$col_total = 0;

				$college_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
										'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
										'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
										'total_m'=>0,'total_f'=>0);

				$this->hnumis_pdf->SetFillColor(200,200,200);
				$this->hnumis_pdf->Cell(302,8,$college->name,1,0,'L',true);
				$this->hnumis_pdf->SetFillColor(255,255,255);
				$this->hnumis_pdf->Ln();
				$this->hnumis_pdf->SetFont('Arial','',9);

				foreach ($programs AS $program) {
					
					$this->hnumis_pdf->SetX(15);
					$college_total['m1'] = $college_total['m1'] + $program->m1;
					$college_total['f1'] = $college_total['f1'] + $program->f1;
					$total1 = $program->m1 + $program->f1;
					$college_total['total1'] = $college_total['total1'] + $total1;
						
					$college_total['m2'] = $college_total['m2'] + $program->m2;
					$college_total['f2'] = $college_total['f2'] + $program->f2;
					$total2 = $program->m2 + $program->f2;
					$college_total['total2'] = $college_total['total2'] + $total2;
						
					$college_total['m3'] = $college_total['m3'] + $program->m3;
					$college_total['f3'] = $college_total['f3'] + $program->f3;
					$total3 = $program->m3 + $program->f3;
					$college_total['total3'] = $college_total['total3'] + $total3;
						
					$college_total['m4'] = $college_total['m4'] + $program->m4;
					$college_total['f4'] = $college_total['f4'] + $program->f4;
					$total4 = $program->m4 + $program->f4;
					$college_total['total4'] = $college_total['total4'] + $total4;
						
					$college_total['m5'] = $college_total['m5'] + $program->m5;
					$college_total['f5'] = $college_total['f5'] + $program->f5;
					$total5 = $program->m5 + $program->f5;
					$college_total['total5'] = $college_total['total5'] + $total5;
						
					$total_m = $program->m1 + $program->m2 + $program->m3 + $program->m4 + $program->m5;
					$college_total['total_m'] = $college_total['total_m'] + $total_m;
					$total_f = $program->f1 + $program->f2 + $program->f3 + $program->f4 + $program->f5;
					$college_total['total_f'] = $college_total['total_f'] + $total_f;
					$program_total = $total_m + $total_f;
					$col_total = $col_total + $program_total;

					if ($program_total) {
						$this->hnumis_pdf->Cell($w[0],8,'     '.$program->abbreviation,1,0,'L',true);
	
						$this->hnumis_pdf->Cell($w[1],8,$program->m1,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[2],8,$program->f1,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[3],8,$total1,1,0,'C',true);
	
						$this->hnumis_pdf->Cell($w[4],8,$program->m2,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[5],8,$program->f2,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[6],8,$total2,1,0,'C',true);
	
						$this->hnumis_pdf->Cell($w[7],8,$program->m3,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[8],8,$program->f3,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[9],8,$total3,1,0,'C',true);
	
						$this->hnumis_pdf->Cell($w[10],8,$program->m4,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[11],8,$program->f4,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[12],8,$total4,1,0,'C',true);
	
						$this->hnumis_pdf->Cell($w[13],8,$program->m5,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[14],8,$program->f5,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[15],8,$total5,1,0,'C',true);
	
						$this->hnumis_pdf->Cell($w[16],8,$total_m,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[17],8,$total_f,1,0,'C',true);
						$this->hnumis_pdf->Cell($w[18],8,$program_total,1,0,'C',true);
						$this->hnumis_pdf->Ln();
					}
				}
				$this->hnumis_pdf->SetX(15);
				$this->hnumis_pdf->Cell($w[0],8,'College Total :',1,0,'R',true);
				$this->hnumis_pdf->Cell($w[1],8,$college_total['m1'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[2],8,$college_total['f1'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[3],8,$college_total['total1'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[4],8,$college_total['m2'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[5],8,$college_total['f2'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[6],8,$college_total['total2'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[7],8,$college_total['m3'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[8],8,$college_total['f3'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[9],8,$college_total['total3'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[10],8,$college_total['m4'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[11],8,$college_total['f4'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[12],8,$college_total['total4'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[13],8,$college_total['m5'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[14],8,$college_total['f5'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[15],8,$college_total['total5'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[13],8,$college_total['total_m'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[14],8,$college_total['total_f'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[15],8,$col_total,1,0,'C',true);
				$grand_total['m1'] = $grand_total['m1'] + $college_total['m1'];
				$grand_total['f1'] = $grand_total['f1'] + $college_total['f1'];
				$grand_total['m2'] = $grand_total['m2'] + $college_total['m2'];
				$grand_total['f2'] = $grand_total['f2'] + $college_total['f2'];
				$grand_total['m3'] = $grand_total['m3'] + $college_total['m3'];
				$grand_total['f3'] = $grand_total['f3'] + $college_total['f3'];
				$grand_total['m4'] = $grand_total['m4'] + $college_total['m4'];
				$grand_total['f4'] = $grand_total['f4'] + $college_total['f4'];
				$grand_total['m5'] = $grand_total['m5'] + $college_total['m5'];
				$grand_total['f5'] = $grand_total['f5'] + $college_total['f5'];
				$grand_total['total1'] = $grand_total['total1'] + $college_total['total1'];
				$grand_total['total2'] = $grand_total['total2'] + $college_total['total2'];
				$grand_total['total3'] = $grand_total['total3'] + $college_total['total3'];
				$grand_total['total4'] = $grand_total['total4'] + $college_total['total4'];
				$grand_total['total5'] = $grand_total['total5'] + $college_total['total5'];
				$grand_total['total_m'] = $grand_total['total_m'] + $college_total['total_m'];
				$grand_total['total_f'] = $grand_total['total_f'] + $college_total['total_f'];
				$grand_total['grand_total'] = $grand_total['grand_total'] + $col_total;

				$this->hnumis_pdf->Ln();

			}
			$this->hnumis_pdf->SetX(15);
			$this->hnumis_pdf->SetFillColor(202,202,202);
			$this->hnumis_pdf->Cell($w[0],8,'Grand Total :',1,0,'R',true);
			$this->hnumis_pdf->Cell($w[1],8,$grand_total['m1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[2],8,$grand_total['f1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[3],8,$grand_total['total1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[4],8,$grand_total['m2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[5],8,$grand_total['f2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[6],8,$grand_total['total2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[7],8,$grand_total['m3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[8],8,$grand_total['f3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[9],8,$grand_total['total3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[10],8,$grand_total['m4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[11],8,$grand_total['f4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[12],8,$grand_total['total4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[13],8,$grand_total['m5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[14],8,$grand_total['f5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[15],8,$grand_total['total5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[13],8,$grand_total['total_m'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[14],8,$grand_total['total_f'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[15],8,$grand_total['grand_total'],1,0,'C',true);
			$this->hnumis_pdf->Ln();

			$this->hnumis_pdf->Output();
		}


		function generate_assessment_summary_pdf($academic_terms_id, $data) {

			//print_r($data['assessment']); die();
			$this->load->library('my_pdf');
			$this->load->library('hnumis_pdf');

			$this->hnumis_pdf->AliasNbPages();
			$this->hnumis_pdf->set_HeaderTitle('ASSESSMENT SUMMARY - '.$data['term']->term." ".$data['term']->sy);
			$this->hnumis_pdf->AddPage('P','letter');

			$this->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->hnumis_pdf->SetDrawColor(165,165,165);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->SetFont('times','B',10);
			$this->hnumis_pdf->SetFillColor(116,116,116);
			$this->hnumis_pdf->SetTextColor(253,253,253);

			$header1 = array('        ','Grade 11','Grade 12',' ',' ',' ',' ',' ');
			$w1 = array(40,26,26,26,26,26,26); 
			for($i=0;$i<count($w1);$i++)
				$this->hnumis_pdf->Cell($w1[$i],6,$header1[$i],0,0,'C',true);
			$this->hnumis_pdf->Ln();
			$header2 = array('PROGRAMS','1st Year','2nd Year','3rd Year','4th Year','5th Year','TOTAL');
			$w2 = array(40,26,26,26,26,26,26); 
			for($i=0;$i<count($w2);$i++)
				$this->hnumis_pdf->Cell($w2[$i],6,$header2[$i],0,0,'C',true);

			$this->hnumis_pdf->Ln();

			$this->hnumis_pdf->SetFont('Arial','',9);
			$this->hnumis_pdf->SetTextColor(0,0,0);

			$grand_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'total'=>0);
			$sub_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'total'=>0);
			$grad_school = FALSE;
			
			$this->hnumis_pdf->SetFillColor(255,255,255);

			$l = 6;
			$total = $total1 = $total2 = $total3 = $total4 = $total5 = 0;
				
			//print_r($data['assessment']);  die();
			foreach($data['college_assmts'] AS $assess) {
				//$program_name = key($assess);
				$program_name = $assess->course;
				//foreach($assess AS $me) {
					//print($me[0]); die();
					//$total = $me[0] + $me[1] + $me[2] + $me[3] + $me[4];
					$total = $assess->amount1+$assess->amount2+$assess->amount3+$assess->amount4+$assess->amount5;
					
					$grand_total['assess1'] = $grand_total['assess1'] + $assess->amount1;  //$me[0];
					$grand_total['assess2'] = $grand_total['assess2'] + $assess->amount2;  //$me[1];
					$grand_total['assess3'] = $grand_total['assess3'] + $assess->amount3;  //$me[2];
					$grand_total['assess4'] = $grand_total['assess4'] + $assess->amount4;  //$me[3];
					$grand_total['assess5'] = $grand_total['assess5'] + $assess->amount5;  //$me[4];
					$grand_total['total']   = $grand_total['total']   + $total;
						
					if ($total) {
						if ($assess->levels_id == 7 AND !$grad_school) {
							$grad_school = TRUE;

							$this->hnumis_pdf->SetFillColor(202,202,202);
							$this->hnumis_pdf->Cell($w1[0],$l,'SUB TOTAL: ',1,0,'R',true);
							$this->hnumis_pdf->Cell($w1[1],$l,number_format($sub_total['assess1'],2),1,0,'R',true);
							$this->hnumis_pdf->Cell($w1[2],$l,number_format($sub_total['assess2'],2),1,0,'R',true);
							$this->hnumis_pdf->Cell($w1[3],$l,number_format($sub_total['assess3'],2),1,0,'R',true);
							$this->hnumis_pdf->Cell($w1[4],$l,number_format($sub_total['assess4'],2),1,0,'R',true);
							$this->hnumis_pdf->Cell($w1[5],$l,number_format($sub_total['assess5'],2),1,0,'R',true);
							$this->hnumis_pdf->Cell($w1[6],$l,number_format($sub_total['total'],2),1,0,'R',true);
							$this->hnumis_pdf->Ln();
							
							$sub_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'total'=>0);
						}

						$sub_total['assess1'] = $sub_total['assess1'] + $assess->amount1;  //$me[0];
						$sub_total['assess2'] = $sub_total['assess2'] + $assess->amount2;  //$me[1];
						$sub_total['assess3'] = $sub_total['assess3'] + $assess->amount3;  //$me[2];
						$sub_total['assess4'] = $sub_total['assess4'] + $assess->amount4;  //$me[3];
						$sub_total['assess5'] = $sub_total['assess5'] + $assess->amount5;  //$me[4];
						$sub_total['total']   = $sub_total['total']   + $total;
						
						$this->hnumis_pdf->SetFillColor(255,255,255);
						$this->hnumis_pdf->Cell($w1[0],$l,$program_name,1,0,'L',true);
						$this->hnumis_pdf->Cell($w1[1],$l,number_format($assess->amount1,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[2],$l,number_format($assess->amount2,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[3],$l,number_format($assess->amount3,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[4],$l,number_format($assess->amount4,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[5],$l,number_format($assess->amount5,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[6],$l,number_format($total,2),1,0,'R',true);
						$this->hnumis_pdf->Ln();
					}
				//}
			}
			//die();
				
			$this->hnumis_pdf->SetFillColor(202,202,202);
			$this->hnumis_pdf->Cell($w1[0],$l,'SUB TOTAL: ',1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[1],$l,number_format($sub_total['assess1'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[2],$l,number_format($sub_total['assess2'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[3],$l,number_format($sub_total['assess3'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[4],$l,number_format($sub_total['assess4'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[5],$l,number_format($sub_total['assess5'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[6],$l,number_format($sub_total['total'],2),1,0,'R',true);
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->Cell($w1[0],$l,'GRAND TOTAL: ',1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[1],$l,number_format($grand_total['assess1'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[2],$l,number_format($grand_total['assess2'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[3],$l,number_format($grand_total['assess3'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[4],$l,number_format($grand_total['assess4'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[5],$l,number_format($grand_total['assess5'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[6],$l,number_format($grand_total['total'],2),1,0,'R',true);
			$this->hnumis_pdf->Ln();

			$this->hnumis_pdf->Output();
		}



	function generate_student_certificate_pdf($student) {

		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		$l = 5;

		$this->hnumis_pdf->AliasNbPages();
		//$this->hnumis_pdf->set_HeaderTitle('TUITION REPORT FOR: '.$tuition['header']." [".$tuition['term']->term." ".$tuition['term']->sy."]");

			$this->hnumis_pdf->AddPage('P','folio');

			$this->hnumis_pdf->SetDisplayMode('fullwidth');

			$this->hnumis_pdf->SetFont('times','',10);
			$this->hnumis_pdf->SetTextColor(10,10,10);
			$this->hnumis_pdf->Line(10,27,200,27);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Cell(190,$l,Date('F j, Y'),0,0,'R',false);
			$this->hnumis_pdf->Ln();
			//$this->hnumis_pdf->Ln();  // Toyet 11.7.2018

			$this->hnumis_pdf->Cell(200,$l,'TO WHOM IT MAY CONCERN:',0,0,'L',false);
			$this->hnumis_pdf->Ln(8);

			$content = "This is to certify that ".strtoupper($student['name'])." is officially enrolled at Holy Name University for this ".$student['term']->term." ".$student['term']->sy." taking up ".$student['course_year']." and has the following charges: ";

			$this->hnumis_pdf->MultiCell(200,5,$content,0,'L',false);

			$this->hnumis_pdf->SetFont('times','',9);
			$l = 3;
			$this->hnumis_pdf->Ln();
			$total_tuition=0;

			if ($student['courses']) {
				$this->hnumis_pdf->Cell(200,$l,'Tuition Fees: (@ '.$student['courses'][0]->rate.')',0,0,'L',false);
				$this->hnumis_pdf->Ln();

				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(60,$l,' ',0,0,'R',false);
				$this->hnumis_pdf->Cell(30,$l,'Load Units',0,0,'C',false);
				$this->hnumis_pdf->Cell(30,$l,'Pay Units',0,0,'C',false);
				$this->hnumis_pdf->Ln();

				$total_load=0;
				$total_paying=0;
				foreach($student['courses'] AS $course) {
					$total_load += $course->credit_units;
					$total_paying += $course->paying_units;
					$total_tuition += $course->tuition_fee;
					$this->hnumis_pdf->SetX(14);
					$this->hnumis_pdf->Cell(60,$l,$course->course_code,0,0,'L',false);
					$this->hnumis_pdf->Cell(30,$l,$course->credit_units,0,0,'C',false);
					$this->hnumis_pdf->Cell(30,$l,$course->paying_units,0,0,'C',false);
					$this->hnumis_pdf->Cell(30,$l,number_format($course->tuition_fee,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(60,$l,' ',0,0,'R',false);
				$this->hnumis_pdf->Cell(30,$l,number_format($total_load,2),0,0,'C',false);
				$this->hnumis_pdf->Cell(30,$l,$total_paying,0,0,'C',false);
				$this->hnumis_pdf->Cell(60,$l,number_format($total_tuition,2),0,0,'R',false);
				$this->hnumis_pdf->Ln();
				$this->hnumis_pdf->Ln();

				if($student['matriculation']){
					$this->hnumis_pdf->SetX(14);
					$this->hnumis_pdf->Cell(60,$l,'Misc. Fees and Other Fees',0,0,'L',false);
					$this->hnumis_pdf->Ln();
					$this->hnumis_pdf->SetX(18);
					$this->hnumis_pdf->Cell(60,$l,$student['matriculation']->code,0,0,'L',false);
					$this->hnumis_pdf->Cell(86,$l,number_format($student['matriculation']->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
					$total_tuition += $student['matriculation']->rate;
					$this->hnumis_pdf->SetX(14);
					$this->hnumis_pdf->Cell(180,$l,number_format($student['matriculation']->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}

			}

			if ($student['miscs']) {
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(60,$l,'Miscellaneous Fees',0,0,'L',false);
				$this->hnumis_pdf->Ln();
				$total_misc = 0;
				foreach ($student['miscs'] AS $misc) {
					$total_misc += $misc->rate;
					$this->hnumis_pdf->SetX(18);
					$this->hnumis_pdf->Cell(60,$l,$misc->description,0,0,'L',false);
					$this->hnumis_pdf->Cell(86,$l,number_format($misc->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}
				$total_tuition += $total_misc;
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(180,$l,number_format($total_misc,2),0,0,'R',false);
				$this->hnumis_pdf->Ln();
			}

			if ($student['other_fees']) {
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(60,$l,'Other School Fees',0,0,'L',false);
				$this->hnumis_pdf->Ln();
				$total_other = 0;
				foreach ($student['other_fees'] AS $other) {
					$total_other += $other->rate;
					$this->hnumis_pdf->SetX(18);
					$this->hnumis_pdf->Cell(60,$l,$other->description,0,0,'L',false);
					$this->hnumis_pdf->Cell(86,$l,number_format($other->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}
				if ($student['learning_resources']) {
					$total_other += $student['learning_resources']->rate;
					$this->hnumis_pdf->SetX(18);
					$this->hnumis_pdf->Cell(60,$l,$student['learning_resources']->description,0,0,'L',false);
					$this->hnumis_pdf->Cell(86,$l,number_format($student['learning_resources']->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}
				if ($student['student_support']) {
					$total_other += $student['student_support']->rate;
					$this->hnumis_pdf->SetX(18);
					$this->hnumis_pdf->Cell(60,$l,$student['student_support']->description,0,0,'L',false);
					$this->hnumis_pdf->Cell(86,$l,number_format($student['student_support']->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}
				
				$total_tuition += $total_other;
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(180,$l,number_format($total_other,2),0,0,'R',false);
				$this->hnumis_pdf->Ln();
			}

			if ($student['affiliated_fees']) {
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(60,$l,'Other Additional School Fees',0,0,'L',false);
				$this->hnumis_pdf->Ln();
				$total_affiliated = 0;
				foreach ($student['affiliated_fees'] AS $affil_fee) {
					$total_affiliated += $affil_fee->rate;
					$this->hnumis_pdf->SetX(18);
					$this->hnumis_pdf->Cell(60,$l,$affil_fee->description,0,0,'L',false);
					$this->hnumis_pdf->Cell(86,$l,number_format($affil_fee->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}
				$total_tuition += $total_affiliated;
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(180,$l,number_format($total_affiliated,2),0,0,'R',false);
				$this->hnumis_pdf->Ln();
			}
				
			if ($student['addtl_other_fees']) {
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(60,$l,'Additional Other Fees',0,0,'L',false);
				$this->hnumis_pdf->Ln();
				$total_addtl = 0;
				foreach ($student['addtl_other_fees'] AS $addtl) {
					$total_addtl += $addtl->rate;
					$this->hnumis_pdf->SetX(18);
					$this->hnumis_pdf->Cell(60,$l,$addtl->description,0,0,'L',false);
					$this->hnumis_pdf->Cell(86,$l,number_format($addtl->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}
				$total_tuition += $total_addtl;
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(180,$l,number_format($total_addtl,2),0,0,'R',false);
				$this->hnumis_pdf->Ln();
			}

			if ($student['lab_fees']) {
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(60,$l,'Laboratory Fees',0,0,'L',false);
				$this->hnumis_pdf->Ln();
				$total_lab = 0;
				foreach ($student['lab_fees'] AS $lab_fee) {
					$total_lab += $lab_fee->rate;
					$this->hnumis_pdf->SetX(18);
					$this->hnumis_pdf->Cell(60,$l,$lab_fee->course_code,0,0,'L',false);
					$this->hnumis_pdf->Cell(86,$l,number_format($lab_fee->rate,2),0,0,'R',false);
					$this->hnumis_pdf->Ln();
				}
				$total_tuition += $total_lab;
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(180,$l,number_format($total_lab,2),0,0,'R',false);
				$this->hnumis_pdf->Ln();
			}
			$this->hnumis_pdf->SetFont('times','B',10);
			$this->hnumis_pdf->SetFillColor(200,200,200);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Cell(60,6,'TOTAL Assessment',0,0,'L',true);
			$this->hnumis_pdf->Cell(124,6,number_format($total_tuition,2),0,0,'R',true);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->SetFont('times','',10);

			$content = "This certification is issued upon the request of ".strtoupper($student['name'])." for ";
			$content .= $student['reason'] . ".";

			$this->hnumis_pdf->MultiCell(200,5,$content,0,'L',false);

			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Cell(20,$l,'Prepared by:',0,0,'R',false);
			$this->hnumis_pdf->Cell(105,$l,'Audited by:',0,0,'R',false);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->SetFont('times','B',10);
			$this->hnumis_pdf->SetX(35);
			$this->hnumis_pdf->Cell(50,$l,$student['accounts_clerk'],0,0,'C',false);
			$this->hnumis_pdf->SetX(140);
			$this->hnumis_pdf->Cell(50,$l,'JOSEFINA J. NAMOC, MBA',0,0,'C',false);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->SetX(35);
			$this->hnumis_pdf->SetFont('times','',10);
			$this->hnumis_pdf->Cell(50,$l,'College Accounts Clerk',0,0,'C',false);
			$this->hnumis_pdf->SetX(140);			
			$this->hnumis_pdf->Cell(50,$l,'Internal Auditor',0,0,'C',false);

			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Cell(50,$l,'Noted by:',0,0,'L',false);
			$this->hnumis_pdf->Cell(78,$l,'Approved by:',0,0,'R',false);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->SetX(35);
			$this->hnumis_pdf->SetFont('times','B',10);
			$this->hnumis_pdf->Cell(50,$l,'ALMA GAUDENCIA A. PEREZ, CPA',0,0,'C',false);
			$this->hnumis_pdf->SetX(140);
			$this->hnumis_pdf->Cell(50,$l,'FR. CYRUS T. MERCADO, SVD',0,0,'C',false);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->SetX(35);
			$this->hnumis_pdf->SetFont('times','',10);
			$this->hnumis_pdf->Cell(50,$l,'Accountant',0,0,'C',false);
			$this->hnumis_pdf->SetX(140);
			$this->hnumis_pdf->SetFont('times','',10);
			$this->hnumis_pdf->Cell(50,$l,'Vice President for Finance',0,0,'C',false);

			$this->hnumis_pdf->Output();

			return;
	}

	/**
	 * Description:
	 * This method generates a pdf file of the class schedule for a particular student.
	 */

	function generate_student_class_schedule_pdf($academic_term_id, $student, $current) {

			$this->load->model("hnumis/Courses_model");
			$this->load->model('hnumis/Offerings_model');
			$this->load->model('hnumis/Student_Model');
			
			//$courses = $this->Grades_model->ListStudentGrades($academic_term_id, $student['idno']);
			$courses = $this->Courses_model->student_courses_from_academic_terms ($student['idno'], $academic_term_id);
			//print_r($student); die();	
			$this->load->library('my_pdf');
			$this->load->library('hnumis_pdf');
			$l = 7;

			$new_size = array(220,150);

			$this->hnumis_pdf->AliasNbPages();
			$this->hnumis_pdf->set_HeaderTitle("STUDENT'S CLASS SCHEDULE",$student['student_histories_id']);
			//$this->hnumis_pdf->set_Footer(-25);
			$this->hnumis_pdf->AddPage('P',$new_size);

			$this->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->hnumis_pdf->SetDrawColor(165,165,165);

			$this->hnumis_pdf->SetFont('times','',10);
			$this->hnumis_pdf->SetTextColor(10,10,10);
			$this->hnumis_pdf->SetFillColor(255,255,255);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();

			$neym = utf8_decode($student['lname'].', '.$student['fname'].' '.SUBSTR($student['mname'],0,1));
			
			$my_course = $this->Student_Model->getCurrent_History($student['student_histories_id']);

			$this->hnumis_pdf->SetXY(175,10);
			$this->hnumis_pdf->Cell(30,5,$student['idno'],0,0,'R',true);
			$this->hnumis_pdf->SetXY(175,14);
			$this->hnumis_pdf->Cell(30,5,utf8_decode($neym),0,0,'R',true);
			$this->hnumis_pdf->SetXY(175,18);
			$this->hnumis_pdf->Cell(30,5,$my_course->year_course,0,0,'R',true);
			$this->hnumis_pdf->SetXY(175,22);
			$this->hnumis_pdf->Cell(30,5,$current->term.' '.$current->sy,0,0,'R',true);



			$this->hnumis_pdf->SetFont('times','B',10);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->SetFillColor(116,116,116);
			$this->hnumis_pdf->SetTextColor(253,253,253);
			$this->hnumis_pdf->SetXY(15,30);

			//$header1 = array('Catalog No.','Section','Units','Schedule/[Room] Building',"Teacher's Signature");
			$header1 = array('Catalog No.','Section','Units','Time','Room', "Teacher's Signature");
					
			//$w=array(30,15,15,50,20,60);
			$w=array(30,15,15,60,20,40);
				
			for($i=0;$i<count($w);$i++) {
				$this->hnumis_pdf->Cell($w[$i],$l,$header1[$i],1,0,'C',true);
			}

			$this->hnumis_pdf->SetFont('times','',10);
			$this->hnumis_pdf->SetTextColor(10,10,10);
			$this->hnumis_pdf->SetFillColor(255,255,255);
			$this->hnumis_pdf->Ln();
			$total_units = 0;
			//print_r($courses);
			//die();
			if ($courses) {
				foreach($courses AS $course) {
					$this->hnumis_pdf->SetX(15);
					$offering_slots = $this->Offerings_model->ListOfferingSlots($course['id']);

					$total_units = $total_units + $course['credit_units'];

					if ($offering_slots[1] > 1) {
						$mycount=0;
						foreach ($offering_slots[0] AS $slots) 
							$mycount++;						
						$l=8; 
						if ($mycount==3)
							$l=12;
						elseif ($mycount==4)
							$l=16;
						$this->hnumis_pdf->Cell($w[0],$l,$course['course_code'],1,0,'L',false);
						$this->hnumis_pdf->Cell($w[1],$l,$course['section_code'],1,0,'C',false);
						if ($course['is_bracketed'] == 'Y') {
							$this->hnumis_pdf->Cell($w[2],$l,'('.$course['credit_units'].')',1,0,'C',false);
						} else {
							$this->hnumis_pdf->Cell($w[2],$l,$course['credit_units'],1,0,'C',false);
						}
						$x1=4;
						$cnt=0;
						foreach ($offering_slots[0] AS $slots) {
							//$timetest .=$slots->tym." ".$slots->days_day_code ."\n";
							$this->hnumis_pdf->SetX(75);
							$sched = $slots->tym." ".$slots->days_day_code." /[".$slots->room_no."] ".$slots->bldg_name;
							$time = $slots->tym." ".$slots->days_day_code;
							$room = $slots->room_no ;
							//$this->hnumis_pdf->Cell($w[3],$x1,$sched,1,0,'L');
							$this->hnumis_pdf->Cell($w[3],$x1,$time,1,0,'L');
							$this->hnumis_pdf->Cell($w[4],$x1,$room,1,0,'L');
							$this->hnumis_pdf->Ln();
							$cnt++;
						}
						
						$y = $this->hnumis_pdf->GetY();
						$this->hnumis_pdf->SetXY(155,$y-($x1*$cnt));
						$this->hnumis_pdf->Cell($w[5],$l,' ',1,0,'C',false);
						//$this->hnumis_pdf->Cell($w[4],$l,' ',1,0,'C',false);
					} else {
						$l=6;
						$this->hnumis_pdf->Cell($w[0],$l,$course['course_code'],1,0,'L',false);
						$this->hnumis_pdf->Cell($w[1],$l,$course['section_code'],1,0,'C',false);
											if ($course['is_bracketed'] == 'Y') {
							$this->hnumis_pdf->Cell($w[2],$l,'('.$course['credit_units'].')',1,0,'C',false);
						} else {
							$this->hnumis_pdf->Cell($w[2],$l,$course['credit_units'],1,0,'C',false);
						}
						foreach ($offering_slots[0] AS $slots) {
							$this->hnumis_pdf->SetX(75);
							$sched = $slots->tym." ".$slots->days_day_code." /[".$slots->room_no."] ".$slots->bldg_name;
							$time = $slots->tym." ".$slots->days_day_code;
							$room = $slots->room_no;
							//$this->hnumis_pdf->Cell($w[3],$l,$sched,1,0,'L');
							$this->hnumis_pdf->Cell($w[3],$l,$time,1,0,'L');
							$this->hnumis_pdf->Cell($w[4],$l,$room,1,0,'C');
							$this->hnumis_pdf->Ln();
						}
						$y = $this->hnumis_pdf->GetY();
						$this->hnumis_pdf->SetXY(155,$y-$l);
						$this->hnumis_pdf->Cell($w[5],$l,' ',1,0,'C',false);
					}

					$this->hnumis_pdf->Ln();

				}
			}


			$this->hnumis_pdf->SetX(42);
			$this->hnumis_pdf->Cell(22,6,'Total Units: ',0,0,'L',false);
			$this->hnumis_pdf->SetFont('times','B',10);
			//TODO: display na the paying units instead
			//.' / '.number_format($student['max_bracket_units'],1) 
			$this->hnumis_pdf->Cell(15,6,number_format($total_units,1),0,1,'L',false);
			//$this->hnumis_pdf->Cell(15,6,'Student Histories ID: '.$student['student_histories_id'],0,0,'L',false);

			$this->hnumis_pdf->SetXY(10,120);
			$this->hnumis_pdf->SetFont('courier','I',10);


			if ($this->uri->segment(1) == 'registrar' OR $this->uri->segment(1) == 'dric') {
				$this->hnumis_pdf->SetX(150);
				$this->hnumis_pdf->Cell(20,1,'___________________________',0,1,'C',false);
				$this->hnumis_pdf->Ln();
				$this->hnumis_pdf->SetX(150);
				$this->hnumis_pdf->Cell(20,6,'Registrar',0,1,'C',false);
			} else {
				$msg = "NOTE: This is a system-generated document. Registrar's signature is no longer needed. For Official Copy of this document, please inquire at the Office of the Registrar. Thank you.";
				$this->hnumis_pdf->MultiCell(200,4,$msg,0,'L',false);
			}

			$this->hnumis_pdf->Output();

	}


	function generate_student_grades_pdf($grade_term, $student) {
		
		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		$l = 5;
	
		$new_size = array(220,150);
	
		$this->hnumis_pdf->AliasNbPages();
		$this->hnumis_pdf->set_HeaderTitle("STUDENT'S GRADES");
		//$this->hnumis_pdf->set_Footer(-25);
		$this->hnumis_pdf->AddPage('P',$new_size);
	
		$this->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->hnumis_pdf->SetDrawColor(165,165,165);
	
		$this->hnumis_pdf->SetFont('times','',10);
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();
	
		//print_r($grade_term); die();
		$this->hnumis_pdf->SetXY(175,10);
		$this->hnumis_pdf->Cell(30,5,$student['idno'],0,0,'R',true);
		$this->hnumis_pdf->SetXY(175,14);
		$this->hnumis_pdf->Cell(30,5,utf8_decode($student['lname'].', '.$student['fname'].' '.SUBSTR($student['mname'],0,1)),0,0,'R',true);
		$this->hnumis_pdf->SetXY(175,18);
		$this->hnumis_pdf->Cell(30,5,$student['yr_level'].' '.$student['abbreviation'],0,0,'R',true);
		$this->hnumis_pdf->SetXY(175,22);
		$this->hnumis_pdf->Cell(30,5,$student['term'].' '.$student['school_year'],0,0,'R',true);
	
	
	
		$this->hnumis_pdf->SetFont('times','B',10);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetFillColor(116,116,116);
		$this->hnumis_pdf->SetTextColor(253,253,253);
		$this->hnumis_pdf->SetXY(15,30);
	
		//$header1 = array('Catalog No.','Section','Units','Schedule/[Room] Building',"Teacher's Signature");
		$header1 = array('Catalog #',' ','Descriptive Title','Units','Prelim', 'Midterm','Final');
			
		//$w=array(30,15,15,50,20,60);
		$w=array(30,15,80,15,15,15,15);
	
		for($i=0;$i<count($w);$i++) {
			$this->hnumis_pdf->Cell($w[$i],$l,$header1[$i],1,0,'C',true);
		}
	
		$this->hnumis_pdf->SetFont('times','',10);
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);
		$this->hnumis_pdf->Ln();
		$total_units = 0;
		
		//print_r($grade_term); die();
		
		//foreach ($grade_term AS $subj) {
		//	print($subj->course->course_code);
		//}
		//die();
		foreach($grade_term AS $subject) {
			$this->hnumis_pdf->SetX(15);
			$this->hnumis_pdf->Cell($w[0],$l,$subject->course->course_code,0,0,'L',true);
			$this->hnumis_pdf->Cell($w[1],$l,$subject->course->section_code,0,0,'C',true);
			$this->hnumis_pdf->Cell($w[2],$l,$subject->course->descriptive_title,0,0,'L',true);
			$this->hnumis_pdf->Cell($w[3],$l,strtoupper($subject->course->credit_units),0,0,'C',true);
			$this->hnumis_pdf->Cell($w[3],$l,strtoupper($subject->course->prelim_grade),0,0,'C',true);
			$this->hnumis_pdf->Cell($w[3],$l,strtoupper($subject->course->midterm_grade),0,0,'C',true);
			$this->hnumis_pdf->Cell($w[3],$l,strtoupper($subject->course->finals_grade),0,0,'C',true);
			$this->hnumis_pdf->Ln();
		}
	
		$this->hnumis_pdf->SetX(42);
		//$this->hnumis_pdf->Cell(22,6,'Total Units: ',0,0,'L',false);
		//$this->hnumis_pdf->SetFont('times','B',10);
		//TODO: display na the paying units instead
		//.' / '.number_format($student['max_bracket_units'],1)
		//$this->hnumis_pdf->Cell(15,6,number_format($total_units,1),0,1,'L',false);
		//$this->hnumis_pdf->Cell(15,6,'Student Histories ID: '.$student['student_histories_id'],0,0,'L',false);
	
		$this->hnumis_pdf->SetXY(10,120);
		$this->hnumis_pdf->SetFont('courier','I',10);
	
	
		if ($this->uri->segment(1) == 'registrar' OR $this->uri->segment(1) == 'dric') {
			$this->hnumis_pdf->SetX(150);
			$this->hnumis_pdf->Cell(20,1,'___________________________',0,1,'C',false);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->SetX(150);
			$this->hnumis_pdf->Cell(20,6,'Registrar',0,1,'C',false);
		} else {
			$msg = "NOTE: This is a system-generated document. Registrar's signature is no longer needed. For Official Copy of this document, please inquire at the Office of the Registrar. Thank you.";
			$this->hnumis_pdf->MultiCell(200,4,$msg,0,'L',false);
		}
	
		$this->hnumis_pdf->Output();
	
	}
	
	function generate_faculty_grading_sheet_pdf($course_offerings_id, $course, $download_type) {

		$this->load->model("hnumis/Student_model");
		$this->load->model('hnumis/AcademicYears_model');
		$this->load->model('hnumis/Offerings_model');
		$this->load->model('hnumis/Grades_model');
		
		$WithHeader = false;
		
		$offering = $this->Offerings_model->getOffering($course_offerings_id);
		$students = $this->Student_model->ListEnrollees($course_offerings_id);
		$term     = $this->AcademicYears_model->getAcademicTerms($offering->academic_terms_id);
		$slots    = $this->Offerings_model->ListOfferingSlots($course_offerings_id);
		$grade_sub= $this->Grades_model->getGradesSubmissionDate($course_offerings_id);
		
		$prelim  = "";
		$midterm = "";
		$finals  = "";

		if ($grade_sub) {
			$prelim  = $grade_sub->prelim_date;
			$midterm = $grade_sub->midterm_date;
			$finals  = $grade_sub->finals_date;			
		} 
		
		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		$l = 4;

		$this->hnumis_pdf->AliasNbPages();
		$this->hnumis_pdf->set_HeaderTitle('FACULTY GRADING SHEET');
		$this->hnumis_pdf->WithHeader($WithHeader);
		$this->hnumis_pdf->AddPage('P','legal');

		$this->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->hnumis_pdf->SetDrawColor(165,165,165);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();
		
		if (!$WithHeader) {
			//$this->hnumis_pdf->SetY(38);
			$this->hnumis_pdf->SetFont('times','B',10);	
			$this->hnumis_pdf->Cell(200,$l,'GRADING SHEET',0,0,'C',false);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
		}
		
		$this->hnumis_pdf->SetFont('arial','',10); // 8
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);
		

		$this->hnumis_pdf->Cell(30,$l,'School Year',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': '.$term->term.' '.$term->sy,0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Teacher',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': ['.$offering->employees_empno.'] '.$offering->emp_name,0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Class',0,0,'L',true);
		$this->hnumis_pdf->Cell(50,$l,': '.$course['course_description'].'   Units: '.number_format($course['credit_units'],2),0,0,'L',true);
		//$this->hnumis_pdf->Cell(20,$l,'   Units: '.number_format($course['credit_units'],2),0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Schedule/Room',0,0,'L',true);
		foreach($slots[0] AS $slot) {
			$this->hnumis_pdf->Cell(100,$l,': '.$slot->tym." ".$slot->days_day_code.' / ['.$slot->room_no.'] '.$slot->bldg_name,0,0,'L',true);
			$this->hnumis_pdf->Ln();
		}

		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetFillColor(116,116,116);
		//$this->hnumis_pdf->SetTextColor(253,253,253);
		//$this->hnumis_pdf->SetX(22);
		$l = 4; // 3
		
		//$header1 = array(' ','ID No.','Name of Student','Sex','Yr Course','Prelim','Re-Exam','Midterm','Re-Exam','Final','Re-Exam');
		$header1 = array(' ','ID No.','Name of Student','Gender','Yr Course','Prelim','Midterm','Final');
		
		//$w=array(8,20,55,8,20,13,15,13,15,13,15);
		// $w=array(8,20,70,12,25,17,17,17);
		$w=array(8,20,62,12,32,17,17,17);
		
		//$r = 1;
		//$x = 10; 
		//$y = 45;
		//$l1=array(8,8,8,8,8,8,4,4,4,8,4);

		/*for($i=0;$i<count($header1);$i++) {
			$this->hnumis_pdf->SetXY($x,$y);
			$this->hnumis_pdf->MultiCell($w[$i],$l1[$i],$header1[$i],1,'C',true);
		
			$x = $x + $w[$i];
		}*/
		for($i=0;$i<count($header1);$i++) {
			$this->hnumis_pdf->Cell($w[$i],5,$header1[$i],'B',0,'C',false);
		}
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);

		$num = 1;
		foreach($students AS $student) {
			$this->hnumis_pdf->Cell($w[0],$l,$num,0,0,'C',true);
			$this->hnumis_pdf->Cell($w[1],$l,$student->idno,0,0,'C',true);
			$this->hnumis_pdf->Cell($w[2],$l,utf8_decode($student->neym),0,0,'L',true);
			$this->hnumis_pdf->Cell($w[3],$l,$student->gender,0,0,'C',true);
			$this->hnumis_pdf->Cell($w[4],$l,$student->year_level.' '.$student->abbreviation,0,0,'L',true);
			if ($download_type == 'with_grades') {
				$this->hnumis_pdf->Cell($w[5],$l,$student->prelim_grade,0,0,'C',true);
				$this->hnumis_pdf->Cell($w[6],$l,$student->midterm_grade,0,0,'C',true);
				$this->hnumis_pdf->Cell($w[7],$l,$student->finals_grade,0,0,'C',true);
			} else {
				$this->hnumis_pdf->Cell($w[5],$l,'____',0,0,'C',true);
				$this->hnumis_pdf->Cell($w[6],$l,'____',0,0,'C',true);
				$this->hnumis_pdf->Cell($w[7],$l,'____',0,0,'C',true);
			}	
			$this->hnumis_pdf->Ln();
			$num++;
		}
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(185,1,'','B',2,'L',false);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();
		$y = $this->hnumis_pdf->GetY();
		$this->hnumis_pdf->Cell(20,$l,'GRADING: ',0,0,'L',false);
		
		$this->hnumis_pdf->Cell(30,$l,'1.0 - 95-100%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'1.4 - 91%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'1.8 - 87%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.2 - 83%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.6 - 79%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'3.0 - 75%',0,0,'L',false);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(30);
		$this->hnumis_pdf->Cell(30,$l,'1.1 - 94%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'1.5 - 90%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'1.9 - 86%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.3 - 82%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.7 - 78%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'5.0 - 70%',0,0,'L',false);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(30);
		$this->hnumis_pdf->Cell(30,$l,'1.2 - 93%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'1.6 - 89%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.0 - 85%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.4 - 81%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.8 - 77%',0,0,'L',false);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(30);
		$this->hnumis_pdf->Cell(30,$l,'1.3 - 92%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'1.7 - 88%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.1 - 84%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.5 - 80%',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'2.9 - 76%',0,0,'L',false);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();
		
		/*$grade=array(1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0,5.0);
		$equiv=array('95-100%','94%','93%','92%','91%','90%','89%','88%','87%','86%','85%','84%','83%','82%','81%','80%','79%',
					'78%','77%','76%','75%','70%');
		
		$cnt=0;
		$w=30;
		for($i=0;$i<count($grade);$i++) {
			if ($cnt==4) {
				$w=$w+30;
				$this->hnumis_pdf->SetXY($w,$y);
				$cnt=0;
			}
			$cnt++;
			$this->hnumis_pdf->Cell(20,$l,number_format($grade[$i],1).' - '.$equiv[$i],0,2,'L',false);
			
		}*/
		
		$this->hnumis_pdf->SetX(30);
		$this->hnumis_pdf->Cell(30,$l,'DR - 5.0',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'NG - No Grade',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'WD - Withdrawn',0,0,'L',false);
		$this->hnumis_pdf->Cell(30,$l,'NA - Never Attended',0,0,'L',false);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(10);
		
		$this->hnumis_pdf->Cell(185,1,'','B',2,'L',false);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(70);
		$this->hnumis_pdf->Cell(45,$l,'Date Submitted',0,0,'C',false);
		$this->hnumis_pdf->SetX(130);
		$this->hnumis_pdf->Cell(45,$l,"Teacher's Signature",0,2,'C',false);
		$this->hnumis_pdf->SetX(30);
		$this->hnumis_pdf->Cell(30,$l,'PRELIM',0,0,'L',false);
		$this->hnumis_pdf->SetX(70);
		$this->hnumis_pdf->Cell(45,$l,$prelim,2,0,'C',false);
		$this->hnumis_pdf->SetX(130);
		$this->hnumis_pdf->Cell(45,$l,' ','B',2,'C',false);
		$this->hnumis_pdf->SetX(30);
		$this->hnumis_pdf->Cell(30,$l,'MIDTERM',0,0,'L',false);
		$this->hnumis_pdf->SetX(70);
		$this->hnumis_pdf->Cell(45,$l,$midterm,2,0,'C',false);
		$this->hnumis_pdf->SetX(130);
		$this->hnumis_pdf->Cell(45,$l,' ','B',2,'C',false);
		$this->hnumis_pdf->SetX(30);
		$this->hnumis_pdf->Cell(30,$l,'FINAL',0,0,'L',false);
		$this->hnumis_pdf->SetX(70);
		$this->hnumis_pdf->Cell(45,$l,$finals,2,0,'C',false);
		$this->hnumis_pdf->SetX(130);
		$this->hnumis_pdf->Cell(45,$l,' ','B',2,'C',false);
		$this->hnumis_pdf->SetX(30);
		$this->hnumis_pdf->Cell(30,$l,'APPROVED',0,0,'L',false);
		$this->hnumis_pdf->SetX(70);
		$this->hnumis_pdf->Cell(45,$l,' ','B',0,'C',false);
		$this->hnumis_pdf->SetX(130);
		$this->hnumis_pdf->Cell(45,$l,' ','B',2,'C',false);
		$this->hnumis_pdf->SetX(70);
		$this->hnumis_pdf->Cell(45,$l,'DEAN/HEAD',0,0,'C',false);
		$this->hnumis_pdf->SetX(130);
		$this->hnumis_pdf->Cell(45,$l,'REGISTRAR',0,2,'C',false);
		
		

		$this->hnumis_pdf->Output();

	}

	
	function generate_class_list_pdf($course_offerings_id, $course, $download_type) {

		$this->load->model("hnumis/Student_model");
		$this->load->model('hnumis/AcademicYears_model');
		$this->load->model('hnumis/Offerings_model');
		$this->load->model('hnumis/Grades_model');
		
		$offering = $this->Offerings_model->getOffering($course_offerings_id);
		$students = $this->Student_model->ListEnrollees($course_offerings_id);
		$term     = $this->AcademicYears_model->getAcademicTerms($offering->academic_terms_id);
		$slots    = $this->Offerings_model->ListOfferingSlots($course_offerings_id);
		$grade_sub= $this->Grades_model->getGradesSubmissionDate($course_offerings_id);
		
		$prelim  = "";
		$midterm = "";
		$finals  = "";

		if ($grade_sub) {
			$prelim  = $grade_sub->prelim_date;
			$midterm = $grade_sub->midterm_date;
			$finals  = $grade_sub->finals_date;			
		} 
		
		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		$l = 4;

		$this->hnumis_pdf->AliasNbPages();
		$this->hnumis_pdf->set_HeaderTitle('CLASS LIST');
		//$this->hnumis_pdf->WithHeader($WithHeader);
		$this->hnumis_pdf->AddPage('P','letter');

		$this->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->hnumis_pdf->SetDrawColor(165,165,165);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();
			
		$this->hnumis_pdf->SetFont('arial','',10);
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);
		

		$this->hnumis_pdf->Cell(30,$l,'School Year',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': '.$term->term.' '.$term->sy,0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Teacher',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': ['.$offering->employees_empno.'] '.$offering->emp_name,0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Class',0,0,'L',true);
		$this->hnumis_pdf->Cell(50,$l,': '.$course['course_description'].'   Units: '.number_format($course['credit_units'],2),0,0,'L',true);
		//$this->hnumis_pdf->Cell(20,$l,'   Units: '.number_format($course['credit_units'],2),0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Schedule/Room',0,0,'L',true);
		foreach($slots[0] AS $slot) {
			$this->hnumis_pdf->Cell(100,$l,': '.$slot->tym." ".$slot->days_day_code.' / ['.$slot->room_no.'] '.$slot->bldg_name,0,0,'L',true);
			$this->hnumis_pdf->Ln();
		}

		$this->hnumis_pdf->Ln();
		//$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetFillColor(116,116,116);
		//$this->hnumis_pdf->SetTextColor(253,253,253);
		//$this->hnumis_pdf->SetX(22);
		$l = 4;
		
		//$header1 = array(' ','ID No.','Name of Student','Sex','Yr Course','Prelim','Re-Exam','Midterm','Re-Exam','Final','Re-Exam');
		$header1 = array(' ','ID No.','Name of Student','Gender','Yr Course');
		
		//$w=array(8,20,55,8,20,13,15,13,15,13,15);
		$w=array(8,25,70,40,25);
		
		for($i=0;$i<count($header1);$i++) {
			$this->hnumis_pdf->Cell($w[$i],5,$header1[$i],'B',0,'C',false);
		}
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);

		$num = 1;
		foreach($students AS $student) {
			$this->hnumis_pdf->Cell($w[0],$l,$num,0,0,'C',true);
			$this->hnumis_pdf->Cell($w[1],$l,$student->idno,0,0,'C',true);
			$this->hnumis_pdf->Cell($w[2],$l,utf8_decode($student->neym),0,0,'L',true);
			$this->hnumis_pdf->Cell($w[3],$l,$student->gender,0,0,'C',true);
			$this->hnumis_pdf->Cell($w[4],$l,$student->year_level.' '.$student->abbreviation,0,0,'C',true);
			$this->hnumis_pdf->Ln();
			$num++;
		}
		

		$this->hnumis_pdf->Output();

	}
	

	
	function Generate_TOR_PDF($student,$stud_tor) {
		
		$this->load->model("hnumis/Student_Model");
		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		$this->load->helper('mis_helper');
		
		$fathers_name = "";
		$mothers_name = "";
		$primary = "";
		$primary_sy = "";
		$intermediate = "";
		$intermediate_sy = "";
		$secondary = "";
		$secondary_sy = "";
		
		$s = json_decode($student->meta);
		//print_r($s); die();
		if ($s) {
			if (isset($s->family_info->fathers_name)) {
				$fathers_name    = l_first(utf8_decode($s->family_info->fathers_name));
				//print($fathers_name); //die();
			}
			if (isset($s->family_info->mothers_name)) {
				$mothers_name    = l_first(utf8_decode($s->family_info->mothers_name));
				//print($mothers_name); die();
			}
			if (isset($s->educational_background->Primary)) {
				$primary         = utf8_decode($s->educational_background->Primary);
			}
			if (isset($s->educational_background->Primary_sy)) {
				$primary_sy      = $s->educational_background->Primary_sy;
			}
			if (isset($s->educational_background->Intermediate)) {
				$intermediate    = utf8_decode($s->educational_background->Intermediate);
			}
			if (isset($s->educational_background->Intermediate_sy)) {
				$intermediate_sy = $s->educational_background->Intermediate_sy;
			}
			if (isset($s->educational_background->Secondary)) {
				$secondary       = utf8_decode($s->educational_background->Secondary);
			}
			if (isset($s->educational_background->Secondary_sy)) {
				$secondary_sy    = $s->educational_background->Secondary_sy;
			}		
		}
		
		$stud['prospectus_id'] = $student->prospectus_id;
		$stud['yr_level'] = $student->year_level;
		$stud['idno'] = $student->idno;
		$stud['max_yr_level'] = $student->max_yr_level;
		//print_r($student); die();

		$student_tor = $this->Student_Model->List_Student_TOR($stud);
		$taken_programs = $this->Student_Model->List_Taken_Programs($stud);
		//$graduated_programs= $this->Student_Model->List_Graduated_Programs($student->idno,TRUE);
		$graduated_programs= $this->Student_Model->List_Student_Programs($stud);
		
		//print_r($graduated_programs); die();
		$l = 5;
		
		$this->hnumis_pdf->set_TOR_header(TRUE);
		$this->hnumis_pdf->set_TOR_footer(TRUE);
		if ($stud_tor['grant_status']) {
			$this->hnumis_pdf->set_granted_status(TRUE);
		} else {
			$this->hnumis_pdf->set_granted_status(FALSE);
		}
		$this->hnumis_pdf->set_student_idno($student->idno);
		
		$this->hnumis_pdf->AddFont('bookosb','','BOOKOSB.php');
		$this->hnumis_pdf->AliasNbPages();
		$this->hnumis_pdf->AddPage('P','folio');

		//TOR setters
		$this->hnumis_pdf->SetAutoPageBreak(TRUE,93);
		//$neym = l_first2(utf8_decode($student->fname.' '.$student->mname.' '.$student->lname)); // genes : l_first 
		$neym = utf8_decode($student->lname.', '.$student->fname.' '.$student->mname); // genes : l_first 
		//print($neym); die();
		$this->hnumis_pdf->set_student_name($neym);
		$this->hnumis_pdf->set_student_program($student->my_acad_program);
		$this->hnumis_pdf->set_graduated_programs($graduated_programs);
		$this->hnumis_pdf->set_prepared_employee($stud_tor['prepared_by']);
		$this->hnumis_pdf->set_checked_employee($stud_tor['checked_by']);
		$this->hnumis_pdf->set_date_tor($stud_tor['date_tor']);
		$this->hnumis_pdf->set_remarks($stud_tor['remarks']);
		
		
		$this->hnumis_pdf->SetDisplayMode('fullwidth');
		$idnum = substr($student->idno,0,3);
		
		if ($stud_tor['grant_status']) {
			$this->hnumis_pdf->SetY(60);
			$this->hnumis_pdf->Image(site_url('assets/img/students/'.$idnum.'/'.$student->idno.'.jpg'),155,69,47,48,'JPG');
		} else {
			$this->hnumis_pdf->SetY(50);
			$this->hnumis_pdf->Image(site_url('assets/img/students/'.$idnum.'/'.$student->idno.'.jpg'),155,59,47,48,'JPG');
		}
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->Cell(30,$l,'Personal Data');
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',11);
		$this->hnumis_pdf->Cell(30,$l,'Name');
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->SetX(45);
		//$this->hnumis_pdf->Cell(30,$l,utf8_decode($student->lname).', '.utf8_decode($student->fname).' '.utf8_decode($student->mname));
		$this->hnumis_pdf->Cell(30,$l,$neym);
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',11);
		$this->hnumis_pdf->Cell(30,$l,'Birth Date');
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->SetX(45);
		$this->hnumis_pdf->Cell(30,$l,$student->dbirth_formatted);
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',11);
		$this->hnumis_pdf->Cell(30,$l,'Birth Place');
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->SetX(45);
		$this->hnumis_pdf->MultiCell(100,$l,utf8_decode($student->my_full_birth_address),0,'L');
		
		//$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',11);
		$this->hnumis_pdf->Cell(30,$l,'Gender');
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->SetX(45);
		$this->hnumis_pdf->Cell(30,$l,$student->my_gender);
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',11);
		$this->hnumis_pdf->Cell(30,$l,'Religion');
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->SetX(45);
		$this->hnumis_pdf->Cell(30,$l,$student->my_religion);
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',11);
		$this->hnumis_pdf->Cell(30,$l,'Citizenship');
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->SetX(45);
		$this->hnumis_pdf->Cell(30,$l,$student->my_citizenship);
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',11);
		$this->hnumis_pdf->Cell(30,$l,'Parents');
		$this->hnumis_pdf->SetFont('bookosb','',11);
		// if ($student->mname AND !ctype_space($fathers_name)) { //student is NOT illigitimate
		
		if ((!ctype_space($fathers_name)) && (!ctype_space($mothers_name))) { //student is NOT illigitimate :  edit by ra 4/2/14
			$this->hnumis_pdf->SetX(45);
			$this->hnumis_pdf->Cell(30,$l,$fathers_name.' (Father)');
			$this->hnumis_pdf->Ln();	
			$this->hnumis_pdf->SetX(45);
			$this->hnumis_pdf->Cell(30,$l,$mothers_name.' (Mother)');
		} elseif (!ctype_space($mothers_name)) {
			$this->hnumis_pdf->SetX(45);
			$this->hnumis_pdf->Cell(30,$l,$mothers_name.' (Mother)');
		} elseif (!ctype_space($fathers_name)) {
			$this->hnumis_pdf->SetX(45);
			$this->hnumis_pdf->Cell(30,$l,$fathers_name.' (Father)');
		}
//		if (!ctype_space($mothers_name)) {
//			$this->hnumis_pdf->SetX(45);
//			$this->hnumis_pdf->Cell(30,$l,$mothers_name.' (Mother)');
//		}
			
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',11);
		$this->hnumis_pdf->Cell(30,$l,'Address');
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->SetX(45);
		$this->hnumis_pdf->MultiCell(100,$l,utf8_decode($student->my_full_home_address),0,'L');
		//$this->hnumis_pdf->Ln();
		$l = $l-1;
		$this->hnumis_pdf->SetX(0);
		$this->hnumis_pdf->SetFont('bookosb','',12);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(0,$l,'Record of Preliminary Education',0,0,'C');
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetFont('helvetica','B',10);
		$this->hnumis_pdf->Cell(60,$l,'Completed:');
		$this->hnumis_pdf->Cell(70,$l,'Name of School',0,0,'C');
		$this->hnumis_pdf->Cell(30,$l,' ',0,0,'C');
		$this->hnumis_pdf->Cell(30,$l,'School Year',0,0,'C');
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',10);
		$this->hnumis_pdf->Cell(30,$l,'Primary');
		$this->hnumis_pdf->Cell(70,$l,$primary,0,0,'L');
		$this->hnumis_pdf->Cell(55,$l,' ',0,0,'C');
		$this->hnumis_pdf->Cell(30,$l,$primary_sy,0,0,'C');
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',10);
		$this->hnumis_pdf->Cell(30,$l,'Intermediate');
		$this->hnumis_pdf->Cell(70,$l,$intermediate,0,0,'L');
		$this->hnumis_pdf->Cell(55,$l,' ',0,0,'C');
		$this->hnumis_pdf->Cell(30,$l,$intermediate_sy,0,0,'C');
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(15);
		$this->hnumis_pdf->SetFont('helvetica','',10);
		$this->hnumis_pdf->Cell(30,$l,'Secondary');
		$this->hnumis_pdf->Cell(70,$l,$secondary,0,0,'L');
		$this->hnumis_pdf->Cell(55,$l,' ',0,0,'C');
		$this->hnumis_pdf->Cell(30,$l,$secondary_sy,0,0,'C');
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$first_course=TRUE;
		//print_r($graduated_programs); die();
		
		$l1=5;
		foreach($graduated_programs AS $program) {
			if (strlen($program->program_name) > 70) {
				$l1 = $l1-1;
				if ($first_course) {
					$this->hnumis_pdf->Cell(30,$l1,'Course:');
					$first_course = FALSE;
					$this->hnumis_pdf->SetX(45);
					$this->hnumis_pdf->MultiCell(157,$l1,$program->program_name,0,'L');
				} else {
					$this->hnumis_pdf->SetX(45);
					$this->hnumis_pdf->MultiCell(157,$l1,$program->program_name,0,'L');
				}
			} else {
				if ($first_course) {
					$this->hnumis_pdf->Cell(30,$l1,'Course:');
					$first_course = FALSE;
					$this->hnumis_pdf->SetX(45);
					$this->hnumis_pdf->MultiCell(157,$l1,$program->program_name,0,'L');
				} else {
					$this->hnumis_pdf->SetX(45);
					$this->hnumis_pdf->MultiCell(157,$l1,$program->program_name,0,'L');
				}
			}
		}

		//$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetY($this->hnumis_pdf->GetY()+1);
		$this->hnumis_pdf->SetLineWidth(0.5);
		$this->hnumis_pdf->SetFont('bookosb','',12);
		$this->hnumis_pdf->Cell(0,8,'C O L L E G I A T E    R E C O R D','TB',0,'C');
		$this->hnumis_pdf->SetFont('helvetica','',9);
		$this->hnumis_pdf->Ln();

		$this->hnumis_pdf->SetXY(13,$this->hnumis_pdf->GetY()+2);
		$this->hnumis_pdf->MultiCell(25,3.5,'School Term & Course No.',0,'C');
		$this->hnumis_pdf->SetXY(38,$this->hnumis_pdf->GetY()-7);
		$this->hnumis_pdf->MultiCell(120,7,'Descriptive Title',0,'C');
		$this->hnumis_pdf->SetXY(172,$this->hnumis_pdf->GetY()-7);
		$this->hnumis_pdf->MultiCell(20,7,'Final Rating',0,'L');
		$this->hnumis_pdf->SetXY(188,$this->hnumis_pdf->GetY()-7);
		$this->hnumis_pdf->MultiCell(15,7,'Units',0,'C');
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Line(10,$this->hnumis_pdf->GetY()-6,206,$this->hnumis_pdf->GetY()-6);
		$this->hnumis_pdf->SetFont('helvetica','',10);
		
		$this->hnumis_pdf->SetY($this->hnumis_pdf->GetY()-3); //start for the content of the 1st page
		$two_liner = FALSE;
		
		foreach($student_tor AS $tor) {
			$taken_courses = $this->Student_Model->List_Student_TOR_Courses($tor->student_histories_id,$tor->other_schools_id,$tor->prospectus_id);
			//print_r($taken_courses); die();
			if ($taken_courses) {
				$this->hnumis_pdf->SetFont('bookosb','',10);
				//if ($this->hnumis_pdf->GetY() >= 230) { //School name to print to next page
				//	$this->hnumis_pdf->AddPage();
				//} 

				$this->hnumis_pdf->MultiCell(195,$l,$tor->term." ".$tor->sy." ".utf8_decode($tor->school_name),0,'L'); // edit by ra 4/2/14

				$this->hnumis_pdf->SetFont('arial','',9);
				
				foreach($taken_courses AS $course) {
					if ($this->hnumis_pdf->GetY() > 233) {
						$this->hnumis_pdf->SetFont('bookosb','',10);
						$this->hnumis_pdf->MultiCell(195,$l,$tor->term." ".$tor->sy." ".utf8_decode($tor->school_name),0,'L');
						$this->hnumis_pdf->SetFont('arial','',9);
					}

					$str = strlen($course->descriptive_title);
					if (strlen($course->descriptive_title) > 66) {
						$two_liner = TRUE;
					} else {
						$two_liner = FALSE;
					}	
					
					if (!$two_liner) {
						$this->hnumis_pdf->SetX(12);
					} else {
						if ($this->hnumis_pdf->GetY() >= 229) { // 200
							$this->hnumis_pdf->SetFont('arial','I',9);
					  $this->hnumis_pdf->Cell(205,$l,"------------------------ Continued on the Next Page -----------------------",0,0,'C');
							$this->hnumis_pdf->AddPage('P','folio');
							$this->hnumis_pdf->SetFont('bookosb','',10);
							$this->hnumis_pdf->MultiCell(195,$l,$tor->term." ".$tor->sy." ".utf8_decode($tor->school_name),0,'L');
							$this->hnumis_pdf->SetX(12);
						} else {
							//$this->hnumis_pdf->SetXY(12,$this->hnumis_pdf->GetY()+4);
							$this->hnumis_pdf->SetX(12);
						}
					}

					$this->hnumis_pdf->SetFont('arial','B',9);
					$this->hnumis_pdf->Cell(50,$l,trim($course->course_code));
					$this->hnumis_pdf->SetFont('arial','',9);
					$this->hnumis_pdf->MultiCell(100,$l,utf8_decode($course->descriptive_title));
					if (strlen($course->descriptive_title) > 70) {
						$this->hnumis_pdf->Ln();
						$this->hnumis_pdf->SetXY(178,$this->hnumis_pdf->GetY()-8);
					} else {
						$this->hnumis_pdf->SetXY(178,$this->hnumis_pdf->GetY()-4);
					}
						
					if ($course->finals_grade) {
						$this->hnumis_pdf->Cell(6,$l,$course->finals_grade,0,0,'C');
					} else {
						$this->hnumis_pdf->Cell(6,$l,'-',0,0,'C');
					}
					
					$failing_grade = array('5.0','WD','DR','NA','NC','INC','INE','NG',' ','wd','dr','na','nc','inc','ine','ng','IP');

					if (!$course->finals_grade OR (in_array($course->finals_grade,$failing_grade) AND $course->what_school == 'hnu')) {
						$this->hnumis_pdf->Cell(23,$l,'-',0,0,'C');
					} else {
						if ($course->credit_units < 0) {
							$this->hnumis_pdf->Cell(23,$l,'('.abs($course->credit_units).')',0,0,'C');
						} else {
							$this->hnumis_pdf->Cell(23,$l,$course->credit_units,0,0,'C');
						}
					}
					$this->hnumis_pdf->Ln();
					//$l = $l-1;

				}
				
				
				//$this->hnumis_pdf->Ln();

				$statement = $this->Student_Model->getGraduatedStatement($tor->student_histories_id,$tor->other_schools_id);
				//print_r($statement); die();
				if ($statement) {
					if ($statement->graduation_statement) {	
						//$this->hnumis_pdf->Ln();
						//if ($this->hnumis_pdf->GetY() >= 222) { //Graduated statement must not cut
							//$this->hnumis_pdf->AddPage();
						//}

						/* Added by Toyet Amores 3.23.2018 to protect 
						   Graduation Statement from breaking */
						$len_statement = strlen($statement->graduation_statement);
						$statement_lines = (int) $len_statement / 100;
						$statement_pdf_lines = $statement_lines * 4;  //5; // changed Toyet 11.27.2018; 12.12.2018
						$check_bottom_lines = $this->hnumis_pdf->GetY()+$statement_pdf_lines;

						/*  ?> <script>alert("<?Php echo 'GetY='.$this->hnumis_pdf->GetY().' chkbtm='.$check_bottom_lines; ?>")</script> <?php  */
						//log_message("INFO", print_r("STUD I.D.  = ".$student->idno,TRUE) );
						//log_message("INFO", print_r("GET Y      = ".$this->hnumis_pdf->GetY(),TRUE) );
						//log_message("INFO", print_r("ST LINES   = ".$statement_lines,TRUE) );
						//log_message("INFO", print_r("pdf LINES  = ".$statement_pdf_lines,TRUE) );
						//log_message("INFO", print_r("chk bottom = ".$check_bottom_lines,TRUE) );

						if ($check_bottom_lines>=233 and $this->hnumis_pdf->GetY()<234) { // 225

							$this->hnumis_pdf->SetFont('arial','I',9);

								// closing part...  
								/*  ?> <script>alert("<?Php echo $statement_lines; ?>")</script> <?php */
							if ($check_bottom_lines >= 230) { // 225
						 		$this->hnumis_pdf->Cell(210,$l,"------------------------ Continued on the Next Page -----------------------",0,0,'C');
							}
							//$this->hnumis_pdf->Ln(); // 205
							if ($this->hnumis_pdf->GetY() < 234) { // 233 // 225
								$this->hnumis_pdf->AddPage('P','folio');
							}
						}

						$this->hnumis_pdf->SetFont('bookosb','',9);
						
						switch ($statement->what_school) {
							case 'hnu':
								if ($statement->max_yr_level < 4 AND $statement->colleges_id != 10) { //Graduate School is still GRADUATED
									$this->hnumis_pdf->Cell(30,$l,'COMPLETED: ');
								} else {
									$this->hnumis_pdf->Cell(30,$l,'GRADUATED: ');
								}
								break;
							case 'other':
								if ($statement->max_yr_level == 'C') { 
									$this->hnumis_pdf->Cell(30,$l,'COMPLETED: ');
								} else {
									$this->hnumis_pdf->Cell(30,$l,'GRADUATED: ');
								}
								break;
						}
						
						$this->hnumis_pdf->SetX(38);
	
											
						/*
						Graduation Statement will be automated once all options will be discussed   
						 
						if ($statement->what_school == 'hnu') {
							if ($stud_tor['grant_status']) {
								$statement = $statement->graduation_statement." ".$statement->program_name.". Exempted from Special Order - A PAASCU Accredited and by virtue of the grant of autonomy status in accordance with CHED Memorandum Order No. 32, series of 2001.";
							} else {
								$statement = $statement->graduation_statement." ".$statement->program_name.".";
							}
						} else {
							$statement = $statement->graduation_statement;
						}
						
						$this->hnumis_pdf->MultiCell(153,$l,$statement);
						*/
						$this->hnumis_pdf->SetFont('arial','B',9);
						$this->hnumis_pdf->MultiCell(168,$l,$statement->graduation_statement);
							
						//$this->hnumis_pdf->Ln();
					}
					//$this->hnumis_pdf->Ln();
				} 
			}
		
		}
		
		$this->hnumis_pdf->SetAutoPageBreak(TRUE,10);
		$this->hnumis_pdf->SetX(0);
		$this->hnumis_pdf->SetFont('arial','I',9);
		$this->hnumis_pdf->Cell(205,$l,"------------------------- End of Student's Records ------------------------",0,0,'C');
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetX(10);
		$l = 3;
		$this->hnumis_pdf->SetDrawColor(0,0,0);
		$this->hnumis_pdf->SetLineWidth(0.1);
		$this->hnumis_pdf->Line(10,$this->hnumis_pdf->GetY()+2,205,$this->hnumis_pdf->GetY()+2);
		
		$this->hnumis_pdf->set_last_page(TRUE);
		
		$this->hnumis_pdf->Ln();
		
		$this->hnumis_pdf->SetFont('arial','',8);
		$this->hnumis_pdf->Cell(30,$l,'Grading System',0,0,'L',false);
		$this->hnumis_pdf->SetX(35);
		$this->hnumis_pdf->Cell(150,$l,'1.0-1.5 (90%-100%) Excellent; 1.6-2.0 (89%-85%) Very Good; 2.1-2.5 (84%-80%) Good; 2.6-3.0 (79%-75%) Passed;',0,0,'L',false);
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,' ');
		$this->hnumis_pdf->SetX(35);
		$this->hnumis_pdf->Cell(150,$l,'5.0 (Below 75%) Failure: must repeat; WD - Withdrawn Subject; DR - Dropped; INE - Incomplete Examination;',0,0,'L',false);
		
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,' ');
		$this->hnumis_pdf->SetX(35);
		$this->hnumis_pdf->Cell(150,$l,'INC - Incomplete Requirement; NC - No Credit; NA - Never Attended',0,0,'L',false);
		$this->hnumis_pdf->Ln();
		
		$this->hnumis_pdf->SetXY(35,$this->hnumis_pdf->GetY()+2);
		$this->hnumis_pdf->Cell(150,$l,'One collegiate unit of credit is one hour lecture or recitation each week for a period of complete semester of 18 units');
		$this->hnumis_pdf->Ln();
		
		$this->hnumis_pdf->SetXY(35,$this->hnumis_pdf->GetY()+2);
		$this->hnumis_pdf->Cell(150,$l,'Three hours of laboratory work each week or a total of 54 hours a semester are regarded as equivalent also to one unit of credit.');
		$this->hnumis_pdf->Ln();
		
		$this->hnumis_pdf->SetXY(35,$this->hnumis_pdf->GetY()+2);
		$this->hnumis_pdf->MultiCell(150,$l,'The semestral average grade of a student is computed by multiplying the number of units assigned to a course by the grade earned and the product is divided by the total units earned for the semester.');
		//if ($this->hnumis_pdf->GetY() >= 267) {
			//$this->hnumis_pdf->AddPage();
		//}
		$this->hnumis_pdf->Ln();
		
		$this->hnumis_pdf->SetFont('bookosb','',11);
		$this->hnumis_pdf->Cell(25,$l,"REMARKS: ",0,0,'L');
		$this->hnumis_pdf->Cell(30,$l,strtoupper($stud_tor['remarks']),0,0,'L');
		
		$this->hnumis_pdf->set_remarks_linenum($this->hnumis_pdf->GetY());
		
		$this->hnumis_pdf->Output();
		
	}
	
	function Generate_Special_Order_PDF($student, $s_order) {
		
		//print_r($s_order); die();
		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		$this->load->helper('mis_helper');
		
		$this->hnumis_pdf->WithHeader(FALSE);
		$this->hnumis_pdf->WithFooter(FALSE);
		$this->hnumis_pdf->AliasNbPages();
		$this->hnumis_pdf->AddPage('P','letter');
		$this->hnumis_pdf->SetDisplayMode('fullwidth');
		
		$this->hnumis_pdf->SetTextColor(0,0,0);
		$l = 5;
		$from_top = 20;
		
		$this->hnumis_pdf->Image(base_url('assets/img/hnu_logo_tor.jpg'),40,15,25,31,'JPG');
		$this->hnumis_pdf->SetY($from_top+5);
		$this->hnumis_pdf->Image(base_url('assets/img/hnu.png'),75,19,85,10,'PNG');
		$this->hnumis_pdf->SetFont('times','',14);
		$this->hnumis_pdf->Cell(216,$l+8,'Tagbilaran City',0,2,'C');
		$this->hnumis_pdf->SetY($from_top+20);
		$this->hnumis_pdf->SetFont('times','B',12);
		$this->hnumis_pdf->Cell(214,$l,'OFFICE OF THE REGISTRAR',0,2,'C');
		$this->hnumis_pdf->SetXY(150,$from_top+30);
		$this->hnumis_pdf->SetFont('times','',12);
		$m1 = substr($s_order->date_issued,5,2);
		$d1 = substr($s_order->date_issued,8,2);
		$y1 = substr($s_order->date_issued,0,4);
		//print($s_order->date_issued); die();
		//print($m1.'/'.$d1.'/'.$y1); die();
		$this->hnumis_pdf->Cell(10,$l,DATE('d F, Y',mktime(0,0,0,$m1,$d1,$y1)),0,2);
		$this->hnumis_pdf->SetXY(30,$from_top+40);
		$this->hnumis_pdf->Cell(40,$l,'TO WHOM IT MAY CONCERN:');
		$this->hnumis_pdf->SetXY(30,$from_top+50);
		if ($student->gender == 'M') {
			$title="Mr.";
		} elseif($student->gender == 'F') {
			$title="Ms.";
		} else {
			$title="";
		}
		
		$student_neym = $title." ".ucwords(utf8_decode(mb_strtoupper($student->fname)))." ".substr($student->mname,0,1).". ".ucwords(utf8_decode(mb_strtoupper($student->lname)));
		//$student_neym = $title." ".$student->fname." ".substr($student->mname,0,1).". ".$student->lname;
		//$student_neym = suffixize($student_neym);
		//$student_neym = strtoupper(suffixize($student_neym));
		$student_neym = strtoupper(suffixize($student_neym));
		
		$this->hnumis_pdf->SetStyle("p", "times", "N", 12, "0,0,0",15);
		$this->hnumis_pdf->SetStyle("b", "times", "B", 12, "0,0,0");
		$this->hnumis_pdf->SetStyle("i", "times", "I", 12, "0,0,0");
		
		if ($s_order->level) {
			$accredited_level = ', Level '.$s_order->level;
		} else {
			$accredited_level ='';
		}
		
		if (!isset($s_order->description)) { //checks if graduate from other school (DWCT)
			$s_order->description = $s_order->program;
		}
		
		$statement = $this->Student_Model->getGraduatedStatement($s_order->student_histories_id,$s_order->other_schools_id);

		switch ($statement->what_school) {
			case 'hnu':
				if ($statement->max_yr_level < 4 AND $statement->colleges_id != 10) { //Graduate School is still GRADUATED
					$grad_type_txt = "completed";
					$grad_label = "title";
				} else {
					$grad_type_txt = "graduated";
					$grad_label = "degree";
				}
			break;
			
			case 'other':
				if ($statement->max_yr_level == 'C') { 
					$grad_type_txt = "completed";
					$grad_label = "title";
				} else {
					$grad_type_txt = "graduated";
					$grad_label = "degree";
				}
			break;
		}
		
		//$grad_type_txt = $s_order->grad_type == 'C' ? 'completed' : 'graduated';

		//$grad_label = $s_order->grad_type == 'C' ? 'title' : 'degree';

		$content1 = "<p> </p><p>THIS IS TO CERTIFY that per records on file in this office,  <b>"
										.$student_neym."</b>, $grad_type_txt with the $grad_label of "
						.$s_order->description." as of ".$s_order->grad_date.";</p>
					<p>That the medium of instruction used in this University is the English language.</p>";
						
		$content2 = "<p>THIS IS TO CERTIFY FURTHER that said student is exempted from the requirements of Special Order
					 by virtue of the accredited status".$accredited_level." of Holy Name University, Tagbilaran City
					 wherein the ".$s_order->college_name." is one of the accredited College by the <i>"
					 .$s_order->accreditor."</i> (".$s_order->accreditor_acronym.") as certified to by the <i>Federation of Accrediting
					 Agencies of the Philippines</i> (FAAP) under the provisions of Paragraphs 5a and 5b of DECS
					 order No. 36, series of 1984.</p>";
					 		
		$content3a = "<p>THIS IS TO CERTIFY FURTHERMORE that the exemption from the issuance of Special Order is
					 granted by virtue of the Autonomous Status in accordance with CHED Memorandum Order No.
					 32, series of 2001.</p>";

		$content3b = "<p>THIS IS TO CERTIFY FURTHER that the said student is exempted from the requirements of Special Order 
					by virtue of Autonomy Status in accordance with the CHED Memorandum Order No. 32, series of 2001.</p>";
		
		$content4 = "<p>THIS CERTIFICATION is issued upon request of <b>".$student_neym."</b> for whatever legal purpose it may serve.</p>
					<p>Given this ".DATE('jS \d\a\y \o\f F, Y',mktime(0,0,0,$m1,$d1,$y1))." at Tagbilaran City, Bohol, Philippines.</p>";
		
		if ($s_order->accreditor) {
			$content1 = $content1 . $content2;
			
			if ($s_order->grant_status =='Y') {
				$content1 = $content1 . $content3a;
			}
		} else {
			if ($s_order->grant_status =='Y') {
				$content1 = $content1 . $content3b;
			}	
		}
		
		
		$content1 = $content1 . $content4;
					
		$this->hnumis_pdf->WriteTag(163,$l,$content1, 0, "J", 0, 0);
		
		$this->hnumis_pdf->SetXY(140,$this->hnumis_pdf->GetY()+26);
		$this->hnumis_pdf->SetFont('times','B',12);
		$this->hnumis_pdf->Cell(40,$l,'PROF. VICTORIA C. MILLANAR',0,2,'C');
		$this->hnumis_pdf->SetFont('times','',12);
		$this->hnumis_pdf->Cell(40,$l,'University Registrar',0,2,'C');
		$this->hnumis_pdf->SetXY(25,$this->hnumis_pdf->GetY()+16);
		$this->hnumis_pdf->Cell(40,$l,'NOT VALID WITHOUT',0,2,'C');
		$this->hnumis_pdf->Cell(40,$l,'SCHOOL SEAL',0,2,'C');
		
		
		
		$this->hnumis_pdf->Output();
		
	}
	
	//ADDED: 05/31/14 genes
	function download_enrollment_summary_to_csv() {
		//$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/College_Model');
		$this->load->model('hnumis/Enrollments_Model');
		$this->load->model('hnumis/Programs_Model');
		$this->load->model('academic_terms_model');
		
		$colleges = $this->College_Model->ListColleges();
		
		$filename = "College Enrollment Summary ".$this->input->post('show_current_term'). ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		fputcsv ($output, array('COLLEGE ENROLLMENT SUMMARY'));
		fputcsv ($output, array($this->input->post('show_current_term')));
		fputcsv ($output, array(''));
		fputcsv ($output, array(''));

		fputcsv($output, array('Colleges & Programs','','1st Year', '', '', '2nd Year', '', '', 
							'3rd Year','','','4th Year', '', '','5th Year', '', '','Grand Total', '', ''));
		
		fputcsv($output, array('','','M','F','Total','M','F','Total','M','F','Total','M','F','Total','M','F','Total','M','F','Total'));

		foreach($colleges AS $college) {
			fputcsv($output, array($college->name));
		
			$programs = $this->Enrollments_Model->ListEnrollmentSummary($college->id, $this->input->post('academic_terms_id'));
			
			foreach ($programs AS $program) {
				$total = 0;
				$total = $program->m1 + $program->f1 +
						$program->m2 + $program->f2 +
						$program->m3 + $program->f3 +
						$program->m4 + $program->f4 +
						$program->m5 + $program->f5 ;
				if ($total) {
					fputcsv($output, array('',$program->abbreviation,$program->m1,$program->f1,'',
											$program->m2,$program->f2,'',
											$program->m3,$program->f3,'',
											$program->m4,$program->f4,'',
											$program->m5,$program->f5,''));
				}
			}
			fputcsv ($output, array(''));
		}
	}
		
	
	private function College_Enrollment_Summary($colleges,$current_term,$selected_term, $total_units, $total_enrollees, $offerings) {
		$this->load->library('PHPExcel');

		$current_sy = $current_term->term.' '.$current_term->sy;
		$filename = "downloads/College Enrollment Summary ".$current_sy.".xls";
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setTitle("College Enrollment Summary: ".$current_sy);
		
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		
		$objPHPExcel->getActiveSheet()->getPageMargins()
			->setTop(1)
			->setRight(0.4)
			->setLeft(0.4)
			->setBottom(1);
		
		$objPHPExcel->getActiveSheet()->getPageSetup()
			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
			->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
		
		$objPHPExcel->getActiveSheet()->getHeaderFooter()
			->setOddHeader('&L&"-,Bold Italic"Holy Name University - COLLEGE')
			->setOddFooter('&L&B&IRun Date: &D &T' . '&RPage &P of &N');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'COLLEGE ENROLLMENT SUMMARY - '.$current_sy);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
			->setSize(14);
		
		$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(36);
		
		for ($col = 'B'; $col != 'T'; $col++) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(7.5);
		}
		
		$objPHPExcel->getActiveSheet()->getStyle('A3:V4')
			->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		$styleArray = array(
				'borders' => array(
						'inside'     => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array(
										'argb' => '00000000'
								)
						),
						'outline'     => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array(
										'argb' => '00000000'
								)
						)
				)
		);
		
		$objPHPExcel->getActiveSheet()->getStyle('A3:S4')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:S4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()->setRGB('B8B8B8');
		$objPHPExcel->getActiveSheet()->getStyle('A3:S4')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(21);
		$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(21);
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'COLLEGES & PROGRAMS');
		$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', '1st Year');
		$objPHPExcel->getActiveSheet()->mergeCells('E3:G3');
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', '2nd Year');
		$objPHPExcel->getActiveSheet()->mergeCells('H3:J3');
		$objPHPExcel->getActiveSheet()->SetCellValue('H3', '3rd Year');
		$objPHPExcel->getActiveSheet()->mergeCells('K3:M3');
		$objPHPExcel->getActiveSheet()->SetCellValue('K3', '4th Year');
		$objPHPExcel->getActiveSheet()->mergeCells('N3:P3');
		$objPHPExcel->getActiveSheet()->SetCellValue('N3', '5th Year');
		$objPHPExcel->getActiveSheet()->mergeCells('Q3:S3');
		$objPHPExcel->getActiveSheet()->SetCellValue('Q3', 'TOTAL');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'M');
		$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'F');
		$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'TOTAL');
		$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'M');
		$objPHPExcel->getActiveSheet()->SetCellValue('F4', 'F');
		$objPHPExcel->getActiveSheet()->SetCellValue('G4', 'TOTAL');
		$objPHPExcel->getActiveSheet()->SetCellValue('H4', 'M');
		$objPHPExcel->getActiveSheet()->SetCellValue('I4', 'F');
		$objPHPExcel->getActiveSheet()->SetCellValue('J4', 'TOTAL');
		$objPHPExcel->getActiveSheet()->SetCellValue('K4', 'M');
		$objPHPExcel->getActiveSheet()->SetCellValue('L4', 'F');
		$objPHPExcel->getActiveSheet()->SetCellValue('M4', 'TOTAL');
		$objPHPExcel->getActiveSheet()->SetCellValue('N4', 'M');
		$objPHPExcel->getActiveSheet()->SetCellValue('O4', 'F');
		$objPHPExcel->getActiveSheet()->SetCellValue('P4', 'TOTAL');
		$objPHPExcel->getActiveSheet()->SetCellValue('Q4', 'M');
		$objPHPExcel->getActiveSheet()->SetCellValue('R4', 'F');
		$objPHPExcel->getActiveSheet()->SetCellValue('S4', 'TOTAL');

		$students = array();
		
		$row=5;
		$my_row=array();
		
  		foreach($colleges AS $college) {
			$programs = $this->Enrollments_Model->ListEnrollmentSummary_tata($college->id, $selected_term);
						
			if ($programs) {
				$students[$college->id] = array('id'=>$college->id,
												'name'=>$college->name);
				$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':S'.$row);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':S'.$row)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setRGB('EEEEEE');
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':S'.$row)
					->getBorders()->getAllBorders()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $college->name);
				
				$row++;
				$row_start = $row;
				
				foreach ($programs AS $program) {
					$program_total = 0;
					
					$program_total = $program->m1 + $program->f1 +
									$program->m2 + $program->f2 +
									$program->m3 + $program->f3 +
									$program->m4 + $program->f4 +
									$program->m5 + $program->f5;
					
					if ($program_total > 0) {

						$students[$college->id][$college->id][]=array(
							'college_id'=>$college->id,
							'name'=>$college->name,
							'id'=>$program->id,
							'abbreviation'=>$program->abbreviation,
							'm1'=>$program->m1,
							'f1'=>$program->f1,
							'm2'=>$program->m2,
							'f2'=>$program->f2,
							'm3'=>$program->m3,
							'f3'=>$program->f3,
							'm4'=>$program->m4,
							'f4'=>$program->f4,
							'm5'=>$program->m5,
							'f5'=>$program->f5);
						
						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	
						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row.':S'.$row)->getBorders()
							->getAllBorders()
							->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							
						$objPHPExcel->getActiveSheet()
							->SetCellValue('A'.$row, '       '.$program->abbreviation);

						$objPHPExcel->getActiveSheet()
							->getStyle('B'.$row.':S'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							
						$objPHPExcel->getActiveSheet()
							->SetCellValue('B'.$row, $program->m1);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('C'.$row, $program->f1);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('D'.$row, '=SUM(B'.$row.':C'.$row.')'); 
						$objPHPExcel->getActiveSheet()
							->SetCellValue('E'.$row, $program->m2);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('F'.$row, $program->f2);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('G'.$row, '=SUM(E'.$row.':F'.$row.')'); 
						$objPHPExcel->getActiveSheet()
							->SetCellValue('H'.$row, $program->m3);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('I'.$row, $program->f3);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('J'.$row, '=SUM(H'.$row.':I'.$row.')'); 
						$objPHPExcel->getActiveSheet()
							->SetCellValue('K'.$row, $program->m4);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('L'.$row, $program->f4);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('M'.$row, '=SUM(K'.$row.':L'.$row.')'); 
						$objPHPExcel->getActiveSheet()
							->SetCellValue('N'.$row, $program->m5);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('O'.$row, $program->f5);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('P'.$row, '=SUM(N'.$row.':O'.$row.')'); 
						$objPHPExcel->getActiveSheet()
							->SetCellValue('Q'.$row, $program->m1+$program->m2+$program->m3+$program->m4+$program->m5);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('R'.$row, $program->f1+$program->f2+$program->f3+$program->f4+$program->f5);
						$objPHPExcel->getActiveSheet()
							->SetCellValue('S'.$row, '=SUM(Q'.$row.':R'.$row.')'); 
						
						//print('=SUM(Q'.$row.':R'.$row.')<br>');
							
						$row++;
					}	
					//die();
				}

				$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row.':S'.$row)->getFont()->setBold(true);
				
				$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row)
					->getAlignment()
					->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	
				$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row.':S'.$row)->getBorders()
					->getAllBorders()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							
				$objPHPExcel->getActiveSheet()
					->SetCellValue('A'.$row, 'College Total:');
					
				$objPHPExcel->getActiveSheet()
					->getStyle('B'.$row.':S'.$row)
					->getAlignment()
					->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				for($col='B'; $col!='T'; $col++) {
					if ($row_start > $row-1) {
						$row_start=$row-1;
					}
					
					$objPHPExcel->getActiveSheet()
						->SetCellValue($col.$row, '=SUM('.$col.$row_start.':'.$col.($row-1).')');
				}
				$my_row[]=$row; //save rows that contains Total
				//print('=SUM('.$col.$row_start.':'.$col.($row-1).')<br>');
				$row++;	
			}
  		}
  		//die();
  		
  		$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row.':S'.$row)->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(21);
					
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':S'.$row)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setRGB('EEEEEE');
					
		$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row)
					->getAlignment()
					->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	
		$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row.':S'.$row)->getBorders()
					->getAllBorders()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							
		$objPHPExcel->getActiveSheet()
					->SetCellValue('A'.$row, 'GRAND TOTAL:');
					
		$objPHPExcel->getActiveSheet()
					->getStyle('B'.$row.':S'.$row)
					->getAlignment()
					->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
		for($col='B'; $col!='T'; $col++) {
			$sum="";
			foreach($my_row AS $k=>$v) {
				$sum = $sum.$col.$v.'+';
			}
			$sum = $sum.'0';

			$objPHPExcel->getActiveSheet()
				->SetCellValue($col.$row, '=SUM('.$sum.')');
			//print($sum.'<br>'); 
		}
		
		$row=$row+2;
		$row_start = $row;

  		$objPHPExcel->getActiveSheet()
					->getStyle('B'.$row.':B'.$row+5)->getFont()->setBold(true);
  		 		
  		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'TOTAL STUDENTS ENROLLED:');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, '=S'.($row-2));
		$row++;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'TOTAL PAY UNITS:');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getNumberFormat()->setFormatCode('#,###,##0.00');
		if ($total_units->total_paying) {
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $total_units->total_paying);
			$r = '='.$total_units->total_paying.'/B'.($row-1);
		} else {
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row,0);
			$r = '=0'.($row-1);			
		}
		$row++;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'AVERAGE PAY UNITS/STUDENT:');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getNumberFormat()->setFormatCode('####0.00');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $r);
		$row++;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'TOTAL LOAD UNITS:');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getNumberFormat()->setFormatCode('#,###,##0.00');
		if ($total_units->total_load) {
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $total_units->total_load);
			$r = '='.$total_units->total_load.'/B'.($row-3);
		} else {
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 0);
			$r = '=0'.($row-3);			
		}
		$row++;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'AVERAGE LOAD UNITS/STUDENT:');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getNumberFormat()->setFormatCode('####0.00');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $r);
		$row++;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'TOTAL OFFERINGS:');
		if ($offerings) {
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $offerings);
			if ($total_enrollees) {
				$r = '='.$total_enrollees->total_enrollees/$offerings;
			} else {
				$r = '=0';
			}
		} else {
			$r = 0;
		}
		$row++;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'AVERAGE LOAD UNITS/STUDENT:');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getNumberFormat()->setFormatCode('####0.00');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $r);

		$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row_start.':B'.$row)->getBorders()
					->getAllBorders()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		
		if ($offerings) {
			$objWriter->save($filename,__FILE__);
		}
		
		return $students;
		
	}

}
//end of reports_model.php
