<?php

	class colleges_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		public function colleges() {

			$this->CI->load->model('hnumis/college_model');
		
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_programs');
				
			switch ($action) {
				case 'list_programs':
		
					$data['programs'] = $this->CI->college_model->programs_from_college($this->CI->input->post('colleges_id'), 'O');

					$my_return['output'] = $this->CI->load->view('common/select_programs', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
			}
		}
		
	}
	
?>	