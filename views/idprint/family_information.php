<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='family_info') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?> 		
		<form id="student_family_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_family_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<div class="control-group formSep">
					<label for="fathers_name" class="control-label">Father's Name:</label>
					<div class="controls" id="place_of_birth_controls">
						<input type="text" name="fathers_name" id="fathers_name" class="input-xlarge" value="
<?php 
															if(isset($family_info['fathers_name'])) echo $family_info['fathers_name'];
															elseif (isset($fathers_name)) echo $fathers_name;
															else echo "";
?>" />
					</div>
				</div>

				<div class="control-group formSep">
					<label for="fathers_number" class="control-label">Father's Contact Number:</label>
					<div class="controls" id="fathers_number">
						<input type="text" name="fathers_number" id="fathers_number" class="input-xlarge" value="
<?php 
															if(isset($family_info['fathers_number'])) echo $family_info['fathers_number'];
															elseif (isset($fathers_number)) echo $fathers_number;
															else echo "";
?>" />
					</div>
				</div>
				
				
				
				
				<div class="control-group formSep">
					<label for="fathers_address" class="control-label">Father's Address:</label>
					<div class="controls" id="fathers_address_controls"><div style="margin-top:6px" id="fathers_address_control_text">
						<strong>
<?php 
															if(isset($family_info['fathers_address'])) echo $family_info['fathers_address'];
															elseif (isset($fathers_address)) echo $fathers_address;
															else echo "";
?>
						</strong>&nbsp;<small><a href="javascript://" id="fathers_address_control"  class="edit_toggle" >Edit</a></small></div>
						<div id="fathers_address_control_controls"></div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="mothers_name" class="control-label">Mother's Name:</label>
					<div class="controls" id="place_of_birth_controls">
						<input type="text" name="mothers_name" id="mothers_name" class="input-xlarge" value="
<?php 
															if(isset($family_info['mothers_name'])) echo $family_info['mothers_name'];
															elseif (isset($mothers_name)) echo $mothers_name;
															else echo "";
?>" />
					</div>
				</div>

				<div class="control-group formSep">
					<label for="fathers_number" class="control-label">Mother's Contact Number:</label>
					<div class="controls" id="mothers_number">
						<input type="text" name="mothers_number" id="mothers_number" class="input-xlarge" data-mask="999-9999999" value="
<?php 
															if(isset($family_info['mothers_number'])) echo $family_info['mothers_number'];
															elseif (isset($mothers_number)) echo $mothers_number;
															else echo "";
?>" />
					</div>
				</div>				
				
				<div class="control-group formSep">
					<label for="mothers_address" class="control-label">Mother's Address:</label>
					<div class="controls" id="mothers_address_controls">
						<div style="margin-top:6px" id="mothers_address_control_text"><strong>
<?php 
															if(isset($family_info['mothers_address'])) echo $family_info['mothers_address'];
															elseif (isset($mothers_address)) echo $mothers_address;
															else echo "";
?>						
							</strong>&nbsp;<small><a href="javascript://" id="mothers_address_control"  class="edit_toggle" >Edit</a></small></div>
						<div id="mothers_address_control_controls"></div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="num_brothers" class="control-label">Number of Brothers:</label>
					<div class="controls" id="num_brothers_controls">
						<input type="text" name="num_brothers" id="num_brothers" class="input-xlarge span1" value="
<?php 
															if(isset($family_info['num_brothers'])) echo $family_info['num_brothers'];
															elseif (isset($num_brothers)) echo $num_brothers;
															else echo "0";
?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="num_sisters" class="control-label">Number of sisters:</label>
					<div class="controls" id="num_sisters_controls">
						<input type="text" name="num_sisters" id="num_sisters" class="input-xlarge span1" value="
<?php 
															if(isset($family_info['num_sisters'])) echo $family_info['num_sisters'];
															elseif (isset($num_sisters)) echo $num_sisters;
															else echo "0";
?>" />
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button id="update_family_information" class="btn btn-primary" type="submit">Update Family Information</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>

