<?php

	class student_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		
		/*
		 * @added: 30/01/2018
		 * @author: genes
		 */
		public function enrolment_management() {
		
			$this->CI->load->model('hnumis/academicyears_model');
			$this->CI->load->model('hnumis/bed/prospectus_model');
			$this->CI->load->model('hnumis/bed/bed_student_model');
			$this->CI->load->model('places_model');
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':
									
					break;
					
				case 'change_level':
		
					$yr_level = NULL;
					
					if ($this->CI->input->post('level_id')) {
						$yr_level = $this->CI->input->post('level_id');
					} 
					
					$data['year_levels'] 	= $this->CI->year_level_model->ListYearLevels($yr_level);
					
					$my_return['output'] 	= $this->CI->load->view('bed/pulldown_menu/select_year_levels_with_none', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'search_student':
					
					$data['lname'] = $this->CI->input->post('lastname');
					$data['fname'] = $this->CI->input->post('firstname');
					$data['mname'] = $this->CI->input->post('middlename');

					$data['students'] = $this->CI->bed_student_model->SearchStudent($data);
					$data['percent']  = "99%";
					
					if ($data['students']) {
						$my_return['found'] = TRUE;
 					} else {
						$my_return['found'] = FALSE;
					}
										
					$my_return['output'] = $this->CI->load->view('bed/student/list_of_students', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'register_student':
					
					if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
						
						$data['lname']         		= sanitize_text_field($this->CI->input->post('lastname'));
						$data['fname']         		= sanitize_text_field($this->CI->input->post('firstname'));
						$data['mname']         		= sanitize_text_field($this->CI->input->post('middlename'));
						$data['dbirth']        		= sanitize_text_field($this->CI->input->post('dbirth'));
						$data['gender']        		= sanitize_text_field($this->CI->input->post('gender'));
						$data['student_type']  		= 'G';
						$data['lrn']     	   		= sanitize_text_field($this->CI->input->post('lrn'));
						$data['enrolment_type']     = sanitize_text_field($this->CI->input->post('enrolment_type'));
						$data['status'] 	 	   	= 'active';
						
						$data['levels_id']			= sanitize_text_field($this->CI->input->post('levels_id'));
						$data['year_level']			= sanitize_text_field($this->CI->input->post('year_level'));
						$data['academic_years_id']	= sanitize_text_field($this->CI->input->post('acad_years_id'));
						$data['prospectus_id']		= sanitize_text_field($this->CI->input->post('prospectus_id'));
						$data['inserted_by']       	= $this->CI->session->userdata('empno');
						$data['encoded_by']       	= $this->CI->session->userdata('empno');

						//for emergency info
						$data['emergency_notify']     	= sanitize_text_field($this->CI->input->post('emergency_fname'))." ".sanitize_text_field($this->CI->input->post('emergency_lname'));
						$data['emergency_relation']     = sanitize_text_field($this->CI->input->post('emergency_relation'));
						$data['emergency_telephone']    = sanitize_text_field($this->CI->input->post('emergency_contact'));
						$data['emergency_email_address']= NULL;
						$data['street_name']    		= sanitize_text_field($this->CI->input->post('street_name'));
						$data['barangays_id']    		= sanitize_text_field($this->CI->input->post('brgy_id'));
					
						$this->CI->db->trans_begin();

						$this->CI->bed_student_model->registerStudent_ToBED($data);
						
						if ($this->CI->db->trans_status() === FALSE) {
							$this->CI->content_lib->set_message('Error adding student to database!', 'alert-error');
							$this->CI->db->trans_rollback();
						} else {
							$this->CI->db->trans_commit();
							$this->CI->content_lib->set_message('Student successfully added to database with ID Number: <a href="'. site_url($this->CI->uri->segment(1).'/student/' . $this->CI->bed_student_model->get_students_idno()) .'">' . $this->CI->bed_student_model->get_students_idno() . "</a>", 'alert-success');
						}
					
					} else {
						$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');	
					}
					
					break;
					
			}

			$data['countries']      = $this->CI->places_model->ListCountries();
			$data['provinces']      = $this->CI->places_model->fetch_results(137,'provinces');
			$data['towns']      	= $this->CI->places_model->fetch_results(49,'towns');
			$data['academic_years'] = $this->CI->academicyears_model->ListAcadYears_for_NewStud_Basic_Ed();
			$data['yr_levels'] 		= $this->CI->prospectus_model->ListProspectuses();
			
			$this->CI->content_lib->enqueue_footer_script('date_picker');
			$this->CI->content_lib->enqueue_header_style('date_picker');
			$this->CI->content_lib->enqueue_body_content('bed/student/new_student_form', $data);
			$this->CI->content_lib->content();
								
		}


		public function process_student_action($idnum=NULL) {
			
			$this->CI->load->model('hnumis/bed/bed_student_model');
			$this->CI->load->model('academic_terms_model');
			$this->CI->load->model('hnumis/academicyears_model');
			$this->CI->load->model('hnumis/bed/section_model');
			$this->CI->load->model('hnumis/student_model');
			
			if (!$this->CI->student_model->student_is_basic_ed($idnum)) {
				redirect($this->CI->uri->segment(1), 'refresh');			
			}
		
			$data['idnum'] = $idnum;

			$acad_year		= $this->CI->academicyears_model->current_academic_year(); 
			
			$academic_years = $this->CI->academic_terms_model->student_inclusive_academic_terms($idnum, TRUE);
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
				
			switch ($action) {
							
				case 'mainpage':
						
					$my_return = NULL;

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;
						
				case 'update_enrollment_status':
				
					$latest_history = $this->CI->bed_student_model->get_StudentHistory_id($idnum, $acad_year->id); 
					
					if (!$latest_history) {
						$latest_history = $this->CI->bed_student_model->get_StudentHistory_id($idnum, $academic_years[0]->academic_years_id); 							
					}
			
					$data1['basic_ed_histories_id']	= $latest_history->id;
					$data1['enrollment_status']		= $this->CI->input->post('enrollment_status');
					
					$my_return['success'] = FALSE;
					
					if ($this->CI->bed_student_model->updateEnrolmmentStatus($data1)) {
						$my_return['success'] = TRUE;
					}
					
					$student = $this->CI->bed_student_model->getStudentInfo($latest_history->id); 
					
					$data['students_idno'] 		= $student->students_idno;
					$data['enrollment_status']	= $student->enrollment_status;
					
					$my_return['output'] = $this->CI->load->view('bed/student/tabs/enrollment_status', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;
					
				case 'assign_section':
				
					$data1['basic_ed_histories_id']	= $this->CI->input->post('basic_ed_histories_id');
					$data1['bed_block_sections_id']	= $this->CI->input->post('section_id');
				
					$my_return['success'] = FALSE;
					
					if ($this->CI->bed_student_model->updateStudentSection($data1)) {
						$my_return['success'] = TRUE;
					}
					
					$data3['student'] = $this->CI->bed_student_model->getStudentInfo($this->CI->input->post('basic_ed_histories_id'));
					
					$data2['students_idno'] 		= $idnum;
					$data2['enrolled_status']		= $this->CI->input->post('enrolled_status');
					$data2['basic_ed_histories_id']	= $this->CI->input->post('basic_ed_histories_id');
					$data2['acad_program_groups_id']= $this->CI->input->post('acad_program_groups_id');
					$data2['academic_years_id']		= $this->CI->input->post('academic_years_id');
					$data2['section_name'] 			= $data3['student']->description."-".$data3['student']->section_name;
						
					$data2['sections']				= $this->CI->section_model->ListSections($this->CI->input->post('acad_program_groups_id'),$this->CI->input->post('academic_years_id'));

					$my_return['output'] = $this->CI->load->view('bed/student/tabs/assign_section', $data2, TRUE);

					$my_return['student_info'] = $this->CI->load->view('bed/student/student_info_header', $data3, TRUE);
					

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;
					
					
			}
			
		}


		//This is called during student paying registration at Teller
		public function process_student_history($students_idno=NULL, $teller_codes_id=NULL) {

			$this->CI->load->model('hnumis/bed/bed_student_model');
			$this->CI->load->model('hnumis/academicyears_model');
			
			$deferred_teller_codes 	= array(329,330,331);
			$current_teller_codes 	= array(82,104,123,295);
			
			$histories = $this->CI->bed_student_model->checkStudentHistory($students_idno);

			$current_acad_year = $this->CI->academicyears_model->current_academic_year();
							
			if ($histories) 
			{ //there should be basic ed history or else return false
				
				if(count($histories) == 1) 
				{ //student has only 1 history
				
					//NOTE: During student registration by enrolling officer,
					// it is assumed that student is assigned to a 'current' or 'incoming' academic year only
					switch ($histories[0]->academic_year_status) 
					{
						case 'previous':
						
							//academic_years_id can be 'previous' if student only registers in previous year but did not continue to enrol/pay registration, so update also academic_year_id
							if (in_array($teller_codes_id, $current_teller_codes)) {
								//if paying current teller codes, get current acad year
								$academic_years_id = $current_acad_year->id;
							} else {
								//if paying deferred_teller_codes, get incoming acad year
								$incoming_acad_year = $this->CI->academicyears_model->incoming_academic_year();
								
								if ($incoming_acad_year) {
									$academic_years_id = $incoming_acad_year->id;
								} else { //no more incoming acad year so use current acad year 
									$academic_years_id = $current_acad_year->id;
								}								
							}

							$this->CI->bed_student_model->enrollStudent($histories[0]->id, $academic_years_id);
							
							break;
							
						case 'current':

							if (in_array($teller_codes_id, $current_teller_codes)) {
								$academic_years_id = $current_acad_year->id;
							} else {
								$incoming_acad_year = $this->CI->academicyears_model->incoming_academic_year();
									
								if ($incoming_acad_year) {
									$academic_years_id = $incoming_acad_year->id;
								} else { //no more incoming acad year so use current acad year 
									$academic_years_id = $current_acad_year->id;
								}								
							}
					
							$prospectus = $this->getNextProspectus($histories[0]);
							
							if ($prospectus) {
								$data['students_idno'] 		= $students_idno;
								$data['academic_years_id'] 	= $academic_years_id;
								$data['prospectus_id'] 		= $prospectus->id;
								$data['levels_id'] 			= $prospectus->levels_id;
								$data['year_level'] 		= $prospectus->year_level;
								$data['status'] 			= "active";
								$data['enrolment_type'] 	= $histories[0]->enrollment_type;
								$data['inserted_by'] 		= $this->CI->session->userdata('empno');
								
								$this->CI->bed_student_model->AddStudent_ToHistories($data);
								
								$history_id = $this->CI->bed_student_model->get_basic_ed_histories_id();
								
								$this->CI->bed_student_model->enrollStudent($history_id);
								
							} else {
								
								return FALSE;
							
							}

							break;
							
						case 'incoming':

							$this->CI->bed_student_model->enrollStudent($histories[0]->id);
							
							break;
							
					}
					
					return TRUE;
										
				} 
				else 
				{ //if student has more than 1 histories, so student already enrolled before

					if (in_array($teller_codes_id, $current_teller_codes)) {
						$academic_years_id = $current_acad_year->id;
					} else {
						$incoming_acad_year = $this->CI->academicyears_model->incoming_academic_year();
							
						if ($incoming_acad_year) {
							$academic_years_id = $incoming_acad_year->id;
						} else { //no more incoming acad year so use current acad year 
							$academic_years_id = $current_acad_year->id;
						}								
					}
					
					$prospectus = $this->getNextProspectus($histories[0]);
					//print_r($histories[0]); die();		
					if ($prospectus) {
						$data['students_idno'] 		= $students_idno;
						$data['academic_years_id'] 	= $academic_years_id;
						$data['prospectus_id'] 		= $prospectus->id;
						$data['levels_id'] 			= $prospectus->levels_id;
						$data['year_level'] 		= $prospectus->year_level;
						$data['status'] 			= "active";
						$data['enrolment_type'] 	= $histories[0]->enrollment_type;
						$data['inserted_by'] 		= $this->CI->session->userdata('empno');
						
						$this->CI->bed_student_model->AddStudent_ToHistories($data);
						
						$history_id = $this->CI->bed_student_model->get_basic_ed_histories_id();
						
						$this->CI->bed_student_model->enrollStudent($history_id);
						
						return TRUE;
					} else {
						return FALSE;
					}
					
				}					
								
			} 
			else 
			{
				return FALSE;
			}
			
		}

		
		private function getNextProspectus($student=NULL) {
			
			$this->CI->load->model('hnumis/bed/prospectus_model');
			$this->CI->load->model('hnumis/bed/year_level_model');
			//print_r($student); die();
			$yr_level = $student->year_level;
			
			if ($student->year_level < $student->max_yr_level) {
				
				$yr_level++;
				$acad_program_groups_id = $student->acad_program_groups_id;
			
			} else {
				
				$yr_level = 1;
				$nextorder = $this->CI->year_level_model->getNextBEDProgramGroups($student->order_no+1); 
				
				$acad_program_groups_id = $nextorder->acad_program_groups_id;
				
			}
//		print($yr_level." ".$acad_program_groups_id); die();	
			$prospectus = $this->CI->prospectus_model->getNextProspectus($yr_level, $acad_program_groups_id); 
				
			if ($prospectus) {
			
				return $prospectus;
			
			} else {
				
				//prospectus has not created yet for the next calculated prospectus; 
				//should not happen; must create prospectus first before student is enrolled 
				return FALSE; 
			}
			
		}
		
	}
	
?>
