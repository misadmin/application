<?php 

class Common_model extends CI_Model {
	
	public function religions () {
		$sql = "
			SELECT
				id, religion
			FROM religions
			";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;	
	}
	
	public function citizenships () {
		$sql = "
			SELECT
				id, name as citizenship
			FROM citizenships
			";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
	}

	public function featured_gf () {
		$base = base_url($this->config->item('student_images_folder'));
		$sql = "
			SELECT concat('{$base}/',substring(idno,1,3),'/',idno,'.jpg') as featured_image  
			FROM students
			WHERE is_featured='Y'
			order by rand()
			limit 1				
			";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
	}

	//Added by Tatskie July 24, 2013
	function add_feedback($empno,$comment,$submitted_by, $recipient_id) {
		$query = "
			insert into feedbacks (".$submitted_by.",comment, recipient_id, submitted)
			values ('{$empno}','" . mysql_real_escape_string($comment) . "', $recipient_id, now())
		";
		$result = $this->db->query($query);
		return $this->db->insert_id();
	}
	
	//Added by Tatskie July 25, 2013
	function feedback_counter($idno){
		$sql = "
			select count(f.id) as feedback_count 
			from feedbacks f 
			where date(f.submitted) between DATE_SUB(curdate(), INTERVAL 5 MONTH) and curdate()
			 	and f.students_idno = '{$idno}'
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->row()->feedback_count;
		else
			return FALSE;
	}

	public function get_feedbacks($recipient_id=FALSE, $student=FALSE){
		if (!$student){ 
			$sql = "
				select f.id,f.id as fbid, 
					if(!isnull(f.employees_empno),f.employees_empno,s.idno) as idnum,
					if(!isnull(f.employees_empno),cap_first(concat_ws(' ',e.fname,e.lname)), 
						concat(s.fname,', ',s.lname,' ',ap.abbreviation,' ',h.year_level)) as `name`,
					f.`comment`,f.submitted,
					if(f.resolved='Y','Resolved','Unresolved') as `status`, developers_remark
				from feedbacks f
				left join students  s on s.idno = f.students_idno
				left join (select h1.* from student_histories h1 
								left join student_histories h2 on h2.students_idno = h1.students_idno and h1.academic_terms_id < h2.academic_terms_id
								where h2.students_idno is null
							) h on h.students_idno = s.idno
				left join prospectus p on p.id = h.prospectus_id
				left join academic_programs ap on ap.id = p.academic_programs_id
				left join employees e on e.empno = f.employees_empno
				where f.resolved !='Y' "
				. ($recipient_id ? "and recipient_id='$recipient_id' " : "")	
				."order by f.id desc
				limit 100
			";
		}else{
			$sql = "
				select f.id,f.id as fbid, f.recipient_id as idnum,
					cap_first(concat(e.lname,', ',e.fname)) as `name`,
					f.`comment`,f.submitted,
					if(f.resolved='Y','Resolved','Unresolved') as `status`, developers_remark
				from feedbacks f								
				left join employees e on e.empno = f.recipient_id
				where f.resolved !='Y' and f.students_idno = {$this->db->escape($recipient_id)}  
				order by f.id desc
				limit 100
			";							
		}
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			return FALSE;
		}
	}
	
	function count_unresolveds($recipient_id){
		$sql = "
				select count(id) unresolveds 
				from feedbacks f
				where resolved !='Y' and students_idno = {$this->db->escape($recipient_id)}				
			";
		$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			return FALSE;
		}
	}
	
	public function list_recipients(){
		$sql = "
				select distinct e.empno as recipient_id, cap_first(concat(e.lname,', ', e.fname)) as recipient 
				from roles r 
				join employees e on e.empno = r.employees_empno and r.role!='faculty'
				order by recipient				
			";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			return FALSE;
		}		
	}


	public function list_recipients_for_students($prospectus_id){
		$sql = "
				select distinct e.empno as recipient_id, cap_first(concat(e.lname,', ', e.fname)) as recipient
				from employees e
				join roles r on r.employees_empno = e.empno and r.role = 'dean'
				join colleges c on c.id = r.colleges_id and r.colleges_id = c.id
				join academic_programs ap on ap.colleges_id = c.id
				join prospectus p on p.academic_programs_id = ap.id and p.id = {$this->db->escape($prospectus_id)}							
				order by recipient				
			";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			return FALSE;
		}
	}
	
	
	
	public function delete_feedbacks($data){
		if (!(is_array($data) AND count($data)>0))
			return 0;
		$imploded_data = implode(',',$data);
		$sql="
			delete from feedbacks where id in ({$imploded_data}) and resolved='Y'
		";
		$this->db->query($sql);
		return $this->db->affected_rows();		
	}

	public function mark_resolved($data){
		if (!(is_array($data) AND count($data)>0))
			return 0;
		$imploded_data = implode(',',$data);
		$sql="
			update feedbacks set resolved = 'Y' where id in ({$imploded_data}) 
		";
		$this->db->query($sql);
		return $this->db->affected_rows();	
	}
	
	public function unresolve($data){
		if (!(is_array($data) AND count($data)>0))
			return 0;
		$imploded_data = implode(',',$data);
		$sql="
			update feedbacks set resolved = 'N' where id in ({$imploded_data})
		";
		$this->db->query($sql);
		return $this->db->affected_rows();	
	}

	public function dev_remark($id,$developers_remark){
		$sql="
			update feedbacks set developers_remark = {$this->db->escape($developers_remark)} 
				where id = '{$id}'
		";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}	
	
	public function schools ($level, $school=""){
		$this->db->select('id, school')
			->from('schools')
			->where("school LIKE '%{$school}%'", null, FALSE)
			->where('levels_id', $level)
			->order_by('school')
			->limit(15);
		$query = $this->db->get();
		if($query->num_rows() > 0)
			return $query->result(); else
			return array();	
	}
	
	function user_logs($user_id, $role, $session_id, $in=true){
		
		if ($in === true){
			$sql = "
					insert into user_logs (`user_id`, `role`, `session_id`, `ip_address`, `time_in`) 
					values ('{$user_id}', '{$role}', '{$session_id}', '{$this->input->ip_address()}', now())
			";
			$result = $this->db->query($sql);			
			return $this->db->insert_id();				
		}else{
			$sql="
				update user_logs set time_out = now() 
				where session_id = '{$session_id}' 
				limit 1;
			";
			$this->db->query($sql);
			return $this->db->affected_rows();				
		}
		
	}
	
}