<?php 

class Posting extends MY_Controller {
	
	public $error_message = '';
	
	function __construct(){
		parent::__construct();
		$this->common->need_auth();

		$this->userinfo = $this->session->all_userdata();
		
		//print_r($this->userinfo);die();
		
		if ($this->session->userdata('role')=='posting'){		
			$this->navbar_data['menu'] = array(
					'Students'	=> 'student',
					'Transactions' => array(
							'Batch Adjustments'	=> 'batch',
							'Batch Posting'		=> 'batch_adjustment_posting',						
							'Batch Write-off'	=> 'write_off',						
							),
					'Reports' => array(
							'Accounts Receivables' => array(
									'A/R Transactions' 		=> 'ar_balance_ver2',
									'Schedule of A/R'		=> 'ar_balances',
									'Schedule of Deposits'	=> 'ar_deposits',										
									),
									'Assessment' => array(
											'Group' 				=> 'assessment_report_group',
											'College' 				=> 'assessment_report_college',
											'Summary for College' 	=> 'assessment_summary',										
											'Summary for Basic Ed.' => 'assessment_summary_basic_ed',
											'Basic Ed. - Group' 	=> 'assessment_basic_ed_group',
											'Unposted Assessments'	=> 'unposted_assessment',
											'Senior High - By Strand' => 'shs_assessment_report_strand',
									),
									'Enrollment Summary' => array(
											'Basic Ed' => 'enrollment_summary_basic_ed',
											'College' 	=> 'enrollment_summary',										
											'Senior High School'=> 'shs_enrollment_summary',
											),
							'Course Offerings' => 'course_offerings',
							'Others' => array(
									'Withdrawals v2' 						=> 'total_withdrawals',
									'Students\' Balances'					=> 'students_balances',
									'Added Subjects After Assessments'		=> 'added_subjects_after_assessments',
									'List of Mismatched ISIS/MIS Balances' 	=> 'mismatches',
									'List of Students with Multiple IDs' 	=> 'list_students_with_more_than_one_idno',		
									'List of Mismatched Assessments - Basic Ed ' => 'mistmatched_assessments_bed'
									),
					)
				);
		}	
		
		
		$this->content_lib->set_navbar_content('', $this->navbar_data);

		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/College_Model');
		$this->load->model('hnumis/Student_Model');
		$this->load->model('hnumis/Enrollments_model');
		$this->load->model('teller/assessment_model');
		$this->load->model('hnumis/Programs_Model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('accounts/Accounts_model');
		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		//$this->load->library('balances_lib');
		

		
	}	
	
	public function student(){
		
	if ($this->common->nonce_is_valid($this->input->post('nonce'))){
		$this->content_lib->set_title('Posting | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
	
		if ( ! empty($query)){
	
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
	
			$this->load->library('pagination_lib');
	
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
	
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
	
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
	
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('teller/teller_model');
			$this->load->model('hnumis/Programs_Model');
			$this->load->model('hnumis/Student_model');
			$this->load->model('hnumis/Prospectus_Model');
			$this->load->model('financials/Scholarship_Model');
			$this->load->model('ledger_model');
			$this->load->model('hnumis/enrollments_model');
			$this->load->helper('student_helper');
			$this->load->library('shs/shs_student_lib');
			$this->load->model('hnumis/shs/shs_student_model');
				
			$result = $this->student_common_model->my_information($idnum);	
			$result2 = $this->teller_model->get_student($idnum);
			//$this->content_lib->enqueue_footer_script('printelement'); // print element plugin...
			$this->content_lib->set_title ('Posting | ' . $idnum);
			//$bank_codes = $this->input->post('bank_codes');
	
			
			//added by tatskie 2014-07-07 (to fix error in posting, usting not available)
			$this->load->model('extras_model');
			$heck = $this->extras_model->get_payers_id($idnum);				
			$current_academic_terms_obj = $this->Academic_terms_model->getCurrentAcademicTerm();
			$bank_codes = $this->teller_model->get_bank_codes();
			$ledger_data = $this->teller_model->get_ledger_data($heck[0]->payers_id);

			//log_message("INFO", print_r($ledger_data,true)); // Toyet 5.19.2018

			$unposted_students_payments = $this->teller_model->get_unposted_students_payment($heck[0]->payers_id);
			$number_of_periods = ($current_academic_terms_obj->term=='Summer' ? 2 : 4);
			$this->load->library('balances_lib',
					array(
							'ledger_data'=>$ledger_data,
							'unposted_students_payments'=>$unposted_students_payments,
							'current_academic_terms_obj'=>$current_academic_terms_obj,
							'period'=>$this->academic_terms_model->what_period(),
							'period_dates'=>$this->academic_terms_model->period_dates(),
					));			
			
			$tab = 'assessment';
	
			//We're going to use the datepicker plugin...
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');
			$this->content_lib->enqueue_header_style('qtip');
			$this->content_lib->enqueue_footer_script('qtip');
			
			$this->load->library('tab_lib');
			
			switch ($action = $this->input->post('action')){	
				case 'writeoff':
					$this->load->model('posting/posting_model');
					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
						if ($updated = $this->posting_model->write_off($this->input->post('id_to_writeoff'))  AND $updated>0)
							$this->content_lib->set_message('Record successfully Written-Off!','alert-success');
						else
							$this->content_lib->set_message('Unable to mark as write-off!','alert-error');
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}						
					break;

				case 'make_adjustments':
					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
		
						$adj['date'] 		= $this->input->post('date_taken');
						$adj['payers_id'] 	= $result2->payers_id;
						$adj['academic_programs_id'] = $result2->ap_id; //added by tatskie Aug 23, 2014
						//$adj['year_level'] 	= $result2->year_level; //added by tatskie Aug 23, 2014
						$adj['type'] 		= $this->input->post('type');
						$adj['description'] = sanitize_text_field($this->input->post('description')); 
						$adj['amount'] 		= sanitize_text_field($this->input->post('amount')); 
						$adj['remark'] 		= sanitize_text_field($this->input->post('remarks'));   
						$adj['empno'] 		= $this->session->userdata('empno');
						$term 				= $this->Academic_terms_model->getCurrentAcademicTerm();
						$adj['term'] 		= $term->id;
						$levelsid_yearlevel = $this->input->post('chargeto_term');
						log_message("INFO",print_r($levelsid_yearlevel,true));
						$adj['levels_id'] 	= substr($levelsid_yearlevel,0,strpos($levelsid_yearlevel,'/'));
						log_message("INFO",print_r($adj['levels_id'],true));		
						$adj['year_level'] 	= substr($levelsid_yearlevel,strpos($levelsid_yearlevel,'/')+1);
						log_message("INFO",print_r($adj['year_level'],true));		
						$tab 				= $this->input->post('tab');
						
						//log_message("INFO", print_r($adj,true));
						//print_r($adj); 
						//die();	
						//print_r($this->session->userdata('empno')); die();
						
						if ($this->ledger_model->add_adjustment($adj)) {
							$this->content_lib->set_message("Adjustment successfully made!", "alert-success");
							 
						} else
							$this->content_lib->set_message("Error found while making an adjustment!", "alert-error");
		
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
						$this->index();
					}
					break;
					
					case 'make_adjustments_addedSubject':
						if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					
							$adj['date'] 		= $this->input->post('date_taken');
							$adj['payers_id'] 	= $result2->payers_id;
							$adj['type'] 		= $this->input->post('type');
							$adj['description'] = $this->input->post('description');
							$string 			= $this->input->post('tuitionSel');
							$separatedString 	= explode("   |   ",$string);
							$adj['amount'] 		= str_replace(",", "", $separatedString[0]);
							$adj['id'] 			= $separatedString[1];
							$enrollment_info 	= $this->enrollments_model->GetEnrollmentInfo($separatedString[1]);
							$adj['remark'] 		= $enrollment_info->course_code;
							$adj['empno'] 		= $this->session->userdata('empno');
							$term 				= $this->Academic_terms_model->getCurrentAcademicTerm();
							$adj['term'] 		= $term->id;
							$tab 				= $this->input->post('tab');
									
							if ($this->ledger_model->add_AddedSubject_adjustment($adj)) {
								$this->content_lib->set_message("Subject successfully added!", "alert-success");
					
							} else
								$this->content_lib->set_message("Error found while Adding subject!", "alert-error");
					
						} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							$this->index();
						}
						break;

				case 'delete_adjustments':
						if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
				
								$adj_id = $this->input->post('adjustment_id');
								
								if ($this->ledger_model->delete_adjustment($adj_id))
									$this->content_lib->set_message("Adjustment successfully deleted!", "alert-success"); else
									$this->content_lib->set_message("Error found while deleting an adjustment!", "alert-error");
				
							} else {
								$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
								$this->index();
							}
							break;
				
				case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;				

				case 'generate_student_certificate'	:
								$this->load->model('hnumis/Reports_Model');
								$this->load->library('tab_lib');
								
								if ($this->input->post('student_histories_id')) {
									$student_histories_id = $this->input->post('student_histories_id');
									$data['name'] = $result->lname.", ".$result->fname." ".$result->mname;
									$data['course_year'] = $this->input->post('course_year');
									$data['term'] = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));

									$data['courses'] = $this->Student_model->ListMyTuitionFees_Basic($student_histories_id);
									if ($data['courses']) {
										$data['tuition_rate'] = $data['courses'][0]->rate;
									} else {
										$data['tuition_rate'] = 0;
									}
	
									$data['matriculation'] = $this->Student_model->getMyMatriculationFee($student_histories_id);
									$data['miscs'] = $this->Student_model->ListMyMiscellaneous_Fees($student_histories_id);
									$data['other_fees'] = $this->Student_model->ListMyOther_Fees($student_histories_id);
									$data['addtl_other_fees'] = $this->Student_model->ListMyAddtlOther_Fees($student_histories_id);
									$data['lab_fees'] = $this->Student_model->ListMyLaboratory_Fees($student_histories_id);
									if ($this->input->post('reason') == 'other') {
										$data['reason'] = $this->input->post('other_reason');
									} else {
										$data['reason'] = "whatever legal purpose it may serve";
									}

									$this->Reports_Model->generate_student_certificate_pdf($data);
									return;
									
								}  else {
									$tab = 'certificate';
								}
					
								break;				

				default:
					break;	
			}
	
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$grade_level = array(11,12);

				if (in_array($result->year_level,$grade_level)) { //student is SHS
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname)  . ", "  . $result->fname . " ". $result->mname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> "Grade ".$result->year_level." ".$result->section_name,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address'	=>$result->full_city_address,
							'phone_number'	=>$result->phone_number,
							'section'	=>$result->section,
					);
				} else {
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname).', '. ucwords(strtolower($result->fname)) .' '.  substr($result->mname,0,1). (empty($result->mname)?'':'.'),
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> $result->year_level,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address' =>$result->full_city_address,
							'phone_number'		=>$result->phone_number,
							'section'			=>$result->section,
							'bed_status' => $result->bed_status,
					);
				}
				
				
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				//tab contents
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...
		
				//operations on privilege
				
				$this->load->library('tab_lib');
				
				//Operations to view the assessment and ledger
				$this->load->library('print_lib');
				
				$assessment_values = array();
				
				$ledger_data = array();
				$ledger_data = $this->teller_model->get_ledger_data($result2->payers_id);
				$this->load->model('academic_terms_model');
				
				//$this->tab_lib->enqueue_tab('Teller', 'teller/teller', $teller_values, 'teller', TRUE);
				$this->load->model('accounts/accounts_model');
				$this->load->model('hnumis/enrollments_model');
				$this->load->model('accounts/fees_schedule_model');
				
				$period_now = $this->academic_terms_model->what_period();

				//assessment details
				$acontent = array(
						'idnumber'	=>$result->idno,
						'familyname'=>$result->lname,
						'firstname'	=>$result->fname,
						'middlename'=>$result->mname,
						'level'		=>$result->year_level,
						'course'	=>$result->abbreviation,
						'due_now' 	=> '',
						'succeeding_dues'	=> '7,756.87'
				);
				
				//assessment form
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$student_inclusive_terms    = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
				if ( ! empty($student_inclusive_terms) )
					$current_history            = $this->shs_student_model->get_StudentHistory_id($idnum, $student_inclusive_terms[0]->id); //print_r($current_history); die();
				else
					$current_history =  NULL;
				
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
					$hist_id = $selected_history_id;
						
				} else {
					$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
					/*EDITED: 9/28/2016 by genes
					* change of academic term variable 					
					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					*/					

					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					//$other_courses_payments = $this->teller_model->other_courses_payments($student_inclusive_terms[0]->id, $result->year_level);
					//$laboratory_fees = $this->teller_model->all_laboratory_fees($student_inclusive_terms[0]->id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
					$hist_id = $result->student_histories_id;
						
				}
				//print_r($dcourses); die();
				
				$assessment_date = $this->Student_Model->get_student_assessment_date($hist_id);				
				
				if (!$assessment_date){
					$transaction_date2=array();
					$assessment_date2[0]= (object)array('transaction_date' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . " + 1 day") ));
					$assessment_date = $assessment_date2;
				}
				
				
				$lab_courses = array();
				$courses = array();
				//print_r($dcourses); die();
					if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab' && empty($course->enrollments_history_id)) {
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
									);
						} 
						$courses[] = array(
								'id'=>$course->id,
								'name'=>$course->course_code,
								'units'=>$course->credit_units,
								'pay_units'=>$course->paying_units,
								're_enrollments_id'=>$course->re_enrollments_id,
								'withdrawn_on'=>$course->withdrawn_on,
								'assessment_id'=>$course->assessment_id,
								'post_status'=>$course->post_status,
								'enrollments_history_id'=>$course->enrollments_history_id,
								'transaction_date'=>$course->transaction_date,								
								);  
					}
				}
				
				$credit_this_term = 0;
				
				
				if ($student_inclusive_terms) { //Added this condition since there will be no output if the student belongs to the Basic Ed- Amie
					foreach($ledger_data as $item){
						if($item['academic_terms_id'] == $student_inclusive_terms[0]->id){
							if($item['transaction_detail'] == 'COLLEGE REGISTRATION'){
								$credit_this_term += $item['credit'];
							}
						}
					}
				}
				
				if ($action=='schedule_current_term') {
					$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $this->input->post('academic_term')); 

					$tab = 'assessment';

					$selected_term = $this->input->post('academic_term');
				} else {
					if ( ! empty($student_inclusive_terms) )
						$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $student_inclusive_terms[0]->id); //print_r($current_history); die();
					else
						$current_history = NULL;

					$selected_term = '';
				}

				
				if (is_college_student($idnum)) {
					$grade_level = array(11,12);
					
					if (in_array($result->year_level,$grade_level)) { //student is SHS
					
						$assess = $this->shs_student_lib->StudentAssessments($current_history); 
					
						$shs_assessment = $this->accounts_model->student_assessment($current_history->id);
					
						$this->tab_lib->enqueue_tab('Assessment', 'shs/student/student_assessment',
							array( 
									'assess'=>$assess,
									'academic_terms'=>$student_inclusive_terms,
									'selected_term'=>$selected_term,
									'student_inclusive_terms' =>$student_inclusive_terms,
									'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
									'courses'=>$courses,
									'ledger_data'=>$this->balances_lib->all_transactions(),
									'invoice_data'=>$this->balances_lib->invoice_data('Pre-Final'),
									'shs_assessment'=> $shs_assessment,
									'current_academic_term_obj'=>$current_academic_terms_obj,
									'student_details'=>$acontent,
								), 'assessment', $tab=='assessment', FALSE);

					} else {
						//MODIFIED: 6/26/15 by genes							
						$affiliated_fees = $this->fees_schedule_model->ListStudentAffiliated_Fees($hist_id);
											
						$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment', 
							array(
									'assessment_date'=>$assessment_date[0]->transaction_date,								
									'other_courses_payments'=>$other_courses_payments,
									'assessment'=> $assessment,
									'courses'=>$courses,
									'lab_courses'=>$lab_courses,
									'ledger_data'=>$ledger_data,
									'affiliated_fees'=>$affiliated_fees,
									'credit_this_term'=>$credit_this_term,
									'student_inclusive_terms' =>$student_inclusive_terms,
									'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
									'student_details'=>$acontent,
									'current_academic_term_obj'=> isset($current_academic_terms_obj) ? $current_academic_terms_obj :"",
							), 
									'assessment', $tab=='assessment', FALSE);
					}
				} else {
					$this->load->model('basic_ed/assessment_basic_ed_model');
					$this->load->model('basic_ed/basic_ed_enrollments_model');						
					$basic_ed = $this->basic_ed_enrollments_model->getLatestHistoryInfo($idnum);
					$this->load->model('basic_ed/basic_ed_model', 'bed_model');
					$config = array(
							'ledger_data'			=> $ledger_data,
							'unposted_transactions'	=> $unposted_students_payments,
							'periods'				=> $this->bed_model->exam_dates($result2->year_level, $result2->levels_id),
							'current_academic_year' => $this->bed_model->academic_year(),
							'bed_status' 			=> $result->bed_status,
							'current_nth_exam'      => $this->bed_model->current_exam($result2->year_level, $result2->levels_id),
							'total_number_of_exams' => count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id)),
					);
					$this->load->library('basic_ed_balances', $config);

					if ($basic_ed)			
						$this->tab_lib->enqueue_tab ('Assessment', 'teller/assessment_basic_ed',
								array(
										'tuition_basic'=>$this->assessment_basic_ed_model->getTuitionRateBasic($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
										'basic_ed_fees'=>$this->assessment_basic_ed_model->ListBasic_Ed_Fees($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
										'post_status'=>$this->assessment_basic_ed_model->CheckIfPosted($basic_ed->id),
										'basic_ed_sections_id' => $basic_ed->basic_ed_sections_id,
										'stud_status'=>TRUE,
										'current_nth_exam'      => $this->bed_model->current_exam($result2->year_level, $result2->levels_id),
										'total_number_of_exams' => count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id)),
										'due_this_exam'         => $this->basic_ed_balances->due_this_exam2(),
							), 
								'assessment', $tab=='assessment', FALSE);
				}
				//enqueue ledger
				$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$ledger_data,
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date,
								'fired_from_posting'=>true,
								
								
						),
								'ledger', FALSE);

				
				
				//*******************end of operations to view assessment and ledger

				//Added: april 1, 2013 by Amie
				$trans_types = $this->ledger_model->list_teller_codes();
				$unposted_adjustments = $this->ledger_model->get_unposted_adjustments($result->idno, $this->session->userdata('empno'));
				
				
				$this->tab_lib->enqueue_tab('Adjustments', '', array(), 'adjustments', FALSE);
				
				//log_message("INFO",print_r($dcontent,true)); // Toyet 8.16.2018

				$is_bed = FALSE;
				if(!empty($dcontent['bed_status'])) //=='active')
					$is_bed = TRUE;
				else
					$is_bed = FALSE;

				$_terms = "";
				$_terms = $this->academic_terms_model->stud_incl_acad_terms_levels_id($dcontent['idnum'],$is_bed);

				$this->tab_lib->enqueue_tab('Add', 'posting/make_adjustments',
						array('adjustment_type'=>$trans_types,
							  'student'=>$dcontent,
							  '_terms'=>$_terms),
						      'make_adjustments', FALSE, 'adjustments'
						       );
				
								
				//Added: January 20, 2014 by Isah, updated 3/31/2014
				$cuurent_term = $this->academic_terms_model->getCurrentAcademicTerm();
				$current_student_history =   $this->Student_Model->getEnrollStatus($idnum, $cuurent_term->id);	 
				if($current_student_history){
					$date_assessed = $this->ledger_model->getAssessedDate($current_student_history->id);
					//print_r($date_assessed); die();
					if($date_assessed){
						$listAddedSubjects = $this->Enrollments_model->listAddedSubjects($result->student_histories_id,$date_assessed->transaction_date);
						
						
						$student_info = $this->Student_Model->getStudentInfo($result->student_histories_id);
						$tuition_basic_rate = $this->Student_Model->getTuitionRateBasic($student_info->acad_program_groups_id, $student_info->academic_terms_id, $student_info->year_level);
						$cur_aca_yr = $this->AcademicYears_Model->current_academic_year();
						$ctr = 0;
						if($listAddedSubjects){
							foreach($listAddedSubjects as $subject){
								
								$course_info = $this->Courses_Model->getCourseInfo($subject->id);
								$lab_amount = $this->Accounts_model->get_lab_fee($course_info->id, $cur_aca_yr->id);
								$lab_fee = 0;
								if($lab_amount){
									$lab_fee = $lab_amount->rate;
								}
								$tuition_others_rate = $this->Student_Model->getTuitionOthersRate($subject->id, $student_info->academic_terms_id, $student_info->year_level);
								if($tuition_others_rate){
									$tuition = $tuition_others_rate->rate * $course_info->paying_units + $lab_fee;
								}else{
									$tuition = $tuition_basic_rate->rate * $course_info->paying_units + $lab_fee;
								}
								$subjects[$ctr]['tuition'] = $tuition; 	
								$subjects[$ctr]['course_code'] = $subject->course_code;
								$subjects[$ctr]['index'] = $ctr;
								$subjects[$ctr]['id'] = $subject->id;
								$ctr++;
							}
						}
						//print_r($subjects); die();
						if($listAddedSubjects) {
							$ctr = 0;
							foreach($subjects as $added){
								$subjects[$ctr]['tuition'] = number_format($added['tuition'],2);
								$ctr++;
							}
							//print_r($subjects); die();
							$this->tab_lib->enqueue_tab('Add Enrolled Subject', 'posting/make_addedSubject_adjustment',
									array('addedSubjects'=>$subjects, 'adjustment_type'=>$trans_types, 
											'student'=>$dcontent),
									'make_addedSubject_adjustment', FALSE, 'adjustments');
						}
					}
				}
			
				//Added: April 12, 2013 by Amie
				if($unposted_adjustments) {
					$this->tab_lib->enqueue_tab('View', 'posting/delete_adjustments', 
							array('adjustments'=>$unposted_adjustments, 
								  'student'=>$dcontent,
								  'login_user'=>$this->session->userdata('empno')),	
							'delete_adjustments', FALSE, 'adjustments');
				}	


					if (!$this->input->post('academic_terms_id')) {	
						$data['courses'] = $this->Student_model->ListMyTuitionFees_Basic($result->student_histories_id);
						if ($data['courses']) {
							$data['tuition_rate'] = $data['courses'][0]->rate;
						} else {
							$data['tuition_rate'] = 0;						
						}
	
						$data['matriculation'] = $this->Student_model->getMyMatriculationFee($result->student_histories_id);
						$data['miscs'] = $this->Student_model->ListMyMiscellaneous_Fees($result->student_histories_id);
						$data['other_fees'] = $this->Student_model->ListMyOther_Fees($result->student_histories_id);
						$data['addtl_other_fees'] = $this->Student_model->ListMyAddtlOther_Fees($result->student_histories_id);
						$data['lab_fees'] = $this->Student_model->ListMyLaboratory_Fees($result->student_histories_id);
						$data['name'] = $result->lname.", ".$result->fname." ".$result->mname;
						$data['course_year'] = $result->abbreviation."-".$result->year_level;
						$data['student_histories_id'] = $result->student_histories_id;
				
						//$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
						$data['academic_terms'] = $this->academic_terms_model->student_inclusive_academic_terms($result->idno);
						
						$data['term'] = $this->academic_terms_model->getCurrentAcademicTerm();
						//$data['current_term'] = $data['term']->id;

					} else {

						$student_histories_id = $this->Student_model->getEnrollStatus($result->idno,$this->input->post('academic_terms_id'));
						
						if (!$student_histories_id) {
							$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
							$data['current_term'] = $this->input->post('academic_terms_id');
						} else {

							$data['courses'] = $this->Student_model->ListMyTuitionFees_Basic($student_histories_id->id);
							if ($data['courses']) {
								$data['tuition_rate'] = $data['courses'][0]->rate;
							} else {
								$data['tuition_rate'] = 0;
							}
		
							$data['matriculation'] = $this->Student_model->getMyMatriculationFee($student_histories_id->id);
							$data['miscs'] = $this->Student_model->ListMyMiscellaneous_Fees($student_histories_id->id);
							$data['other_fees'] = $this->Student_model->ListMyOther_Fees($student_histories_id->id);
							$data['addtl_other_fees'] = $this->Student_model->ListMyAddtlOther_Fees($student_histories_id->id);
							$data['lab_fees'] = $this->Student_model->ListMyLaboratory_Fees($student_histories_id->id);
							$data['name'] = $result->lname.", ".$result->fname." ".$result->mname;
							$data['course_year'] = $student_histories_id->abbreviation."-".$student_histories_id->year_level;
							$data['student_histories_id'] = $student_histories_id->id;
					
							$data['academic_terms'] = $this->academic_terms_model->student_inclusive_academic_terms($result->idno, TRUE);

							$data['term'] = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
	
							//$data['current_term'] = $this->input->post('academic_terms_id');
						
						}

						$tab='certificate';
					}	
					
					//log_message("INFO",print_r($data,true)); // Toyet 6.13.2018

					$this->tab_lib->enqueue_tab('Certificate', 'accounts/student_certificate', $data, 'certificate', $tab=='certificate', FALSE);

				$tab_content = $this->tab_lib->content();
				$this->content_lib->enqueue_body_content("", $tab_content);}
						
		}

		$this->content_lib->content();

	} else {
		$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
		$this->index();
	}	
	
}
	
	//Added: April 12, 2013 by Amie
	public function batch_adjustment_posting() {
		$this->load->model('ledger_model');
		
		$data['adjustments'] = $this->ledger_model->get_all_unposted_adjustments();
		
		if(!($data['adjustments'])) {
			$this->content_lib->set_message("No unposted adjustments!", 'alert-error');
			$this->index();
		} else {
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
				$data['users'] = $this->ledger_model->ListEmployeesWhoCreateAdjustment();
				$data['login_user']  = $this->session->userdata('empno');

				switch ($step) {
					case 1:
						if ($this->ledger_model->getEmployeeWhoCreateAdjustment($this->session->userdata('empno'))) {
							$data['adjustments'] = $this->ledger_model->get_all_unposted_adjustments($this->session->userdata('empno'));
							$data['selected_id'] = $this->session->userdata('empno');
						} else {
							$data['adjustments'] = $this->ledger_model->get_all_unposted_adjustments();
							$data['selected_id'] = 0;
						}
						
						$this->content_lib->enqueue_body_content('posting/view_unposted_adjustments', $data);
						
						break;
					case 'change_user':
						
						if ($this->input->post('user_id') == 0){
							$data['adjustments'] = $this->ledger_model->get_all_unposted_adjustments();
						} else {
							$data['adjustments'] = $this->ledger_model->get_all_unposted_adjustments($this->input->post('user_id'));
						}
						
						$data['selected_id'] = $this->input->post('user_id');
						$this->content_lib->enqueue_body_content('posting/view_unposted_adjustments', $data);
						
						break;
					case 2:
						if($this->ledger_model->post_adjustments($this->session->userdata('empno')))
								$this->content_lib->set_message("Adjustments successfully posted!", "alert-success"); else 
								$this->content_lib->set_message("Error while posting batch adjustments!", "alert-error");
						break;
					case 3: //deletes a specific adjustment
						$this->load->model('ledger_model');
						
						$this->ledger_model->delete_adjustment(($this->input->post('adjustment_id')));
						$this->view_adjustments();
						
						break;
				}
				
			} else { 
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error'); 
			}
			$this->content_lib->content();	
		}
		
	}
	
	//Added: September 7, 2013 by Amie
	public function view_adjustments() {
		$this->load->model('ledger_model');
		
		$data['adjustments'] = $this->ledger_model->get_all_unposted_adjustments($this->session->userdata('empno'));
		
		if(!($data['adjustments'])) {
			$this->content_lib->set_message("No unposted adjustments!", 'alert-error');
			$this->index(); }
		else { 
			//$this->session->set_userdata('adj',$data['adjustments']);
			
			$this->content_lib->enqueue_body_content('posting/view_unposted_adjustments', $data);
		}
	}
	
	
	public function batch() {

		$this->content_lib->set_title('Posting | Student | ' . $this->config->item('application_title'));
		$this->load->model('ledger_model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('hnumis/offerings_model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		
		if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
			//an input is found...
			//nonce is valid...
			//todo: call model to add student...
			$adj['amount'] = sanitize_text_field($this->input->post('amount', TRUE));
			$batch_type = sanitize_text_field($this->input->post('batch_type', TRUE));
			$current = $this->academic_terms_model->getCurrentAcademicTerm();
			$adj['date'] = $this->input->post('date_taken');
			$adj['type'] = $this->input->post('type');
			$adj['description'] = $this->input->post('description');
			$adj['remark'] = $this->input->post('remarks');
			$adj['empno'] = $this->session->userdata('empno');
			$term = $this->academic_terms_model->getCurrentAcademicTerm();
			$adj['term'] = $term->id;

			if ($batch_type == 'O') {
				$adj['offering'] = sanitize_text_field($this->input->post('offering', TRUE));
				$parallelno = $this->offerings_model->get_parallelno($adj['offering'], $current->id);
				$this->load->model('hnumis/enrollments_model');
				
				if ($parallelno) {
					$parallel_offerings = $this->offerings_model->ListParallel($parallelno->parallel_no);
					if ($parallel_offerings) {
						foreach($parallel_offerings as $po) {
							$payers = $this->enrollments_model->list_enrollees($po->course_offerings_id);
							if ($payers) {
								foreach($payers as $payer) {
									$adj['payers_id'] = $payer->id;
									$this->ledger_model->add_adjustment($adj);
								}
							}
						}
					}
				} else {
					$payers = $this->enrollments_model->list_enrollees($this->input->post('offering'));
					if ($payers) {
						foreach($payers as $payer) {
							$adj['academic_programs_id'] = $payer->academic_programs_id; 
							$adj['year_level'] = $payer->year_level;
							$adj['payers_id'] = $payer->id;
							$this->ledger_model->add_adjustment($adj);
							//@FIXME: there should be error trapping here and inform the user if there is any											
						}
					}
				}
				$this->content_lib->set_message("Adjustment successfully made!", "alert-success");
			} else if ($batch_type == 'B') {
	
			//batch type is for basic ed sections
				$adj['level'] = sanitize_text_field($this->input->post('level', TRUE));
				$adj['year'] = sanitize_text_field($this->input->post('year', TRUE));
				$adj['section'] = sanitize_text_field($this->input->post('section', TRUE));
				
				$this->load->model('basic_ed/basic_ed_enrollments_model');

				$payers = $this->basic_ed_enrollments_model->list_enrollees($adj['level'], $adj['year'], $adj['section'],$current->academic_years_id );
				
				if($payers) {
					foreach($payers as $payer) {
						$adj['levels_id'] = $payer->levels_id; //added by tatskie Aug 23, 2014
						$adj['year_level'] = $payer->year_level; //added by tatskie Aug 23, 2014						
						$adj['payers_id'] = $payer->id;
						$this->ledger_model->add_adjustment($adj, 'N', NULL , NULL, TRUE );
						//@FIXME: there should be error trapping here and inform the user if there is any
					}
					$this->content_lib->set_message("Adjustment successfully made!", "alert-success");
				} else {
					$this->content_lib->set_message("No enrollee for the chosen section", "alert-error");
				}
			}
		} 
		
		//We're going to use the datepicker plugin...
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');
			$this->content_lib->enqueue_header_style('qtip');
			$this->content_lib->enqueue_footer_script('qtip');
		
		$data['trans_types'] = $this->ledger_model->list_teller_codes();
		$current_term = $this->academic_terms_model->getCurrentAcademicTerm();
		$data['offerings'] = $this->offerings_model->ListAllOfferings($current_term->id);
		$data['levels'] = $this->basic_ed_sections_model->ListBasicEd();
		
		$this->content_lib->enqueue_body_content('posting/make_batch_adjustments', $data);
		$this->content_lib->content();
		
	}
	
	
	public function enrollment_summary() {
	
		$this->load->model('hnumis/Reports_Model');
		
		$this->Reports_Model->enrollment_summary();

	}
	

	public function assessment_summary() {
	
		$this->load->model('hnumis/Reports_Model');
		
		$this->Reports_Model->assessment_summary();

	}
		

	public function enrollment_summary_basic_ed() {
	
		$this->load->model('basic_ed/Reports_Model');
		
		$this->Reports_Model->enrollment_summary_basic_ed();

	}


	public function assessment_summary_basic_ed() {
	
		$this->load->model('basic_ed/Reports_Model');
		
		$this->Reports_Model->assessment_summary_basic_ed();

	}
	
	public function Print_adjustments_pdf($empno=NULL) {
			$this->load->model('ledger_model');
			if ($empno) {
				$adj = $this->ledger_model->get_all_unposted_adjustments($empno);
				$get_user = $this->ledger_model->getEmployeeWhoCreateAdjustment($empno);
				$user = $get_user->transact_by;
			} else {
				$adj = $this->ledger_model->get_all_unposted_adjustments();
				$user = "All Users";
			}
				
			$this->hnumis_pdf->AliasNbPages();
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->set_HeaderTitle('ADJUSTMENTS By: '.$user);	
			$this->hnumis_pdf->AddPage();
			$this->hnumis_pdf->SetDisplayMode('fullpage');
			$this->hnumis_pdf->SetDrawColor(165,165,165);

	
			$w=array(20,10,20,55,55,18,18);
			
			$this->hnumis_pdf->SetFont('times','B',16);
			$this->hnumis_pdf->Ln();
						
			$this->hnumis_pdf->SetFont('times','B',9);
			$this->hnumis_pdf->SetFillColor(37,106,2);
			$this->hnumis_pdf->SetTextColor(253,253,253);
			$header=array('Trans. Date','Adj. ID','ID No.','Name','Description','Debit','Credit');

			for($i=0;$i<count($header);$i++)
				$this->hnumis_pdf->Cell($w[$i],8,$header[$i],1,0,'C',true);
			$this->hnumis_pdf->Ln();

			$this->hnumis_pdf->SetTextColor(0,0,0);
			$this->my_pdf->SetFont('times','',10);
			
			
			if ($adj) {
				$debit=0;
				$credit=0;
				foreach($adj AS $adjustment) {
					$this->hnumis_pdf->Cell($w[0],8,$adjustment['transaction_date'],1,0,'C');
					$this->hnumis_pdf->Cell($w[1],8,$adjustment['adjustment_id'],1,0,'C');
					$this->hnumis_pdf->Cell($w[2],8,$adjustment['idno'],1,0,'C');
					$this->hnumis_pdf->Cell($w[3],8,utf8_decode($adjustment['name']),1,0,'L');				
					$this->hnumis_pdf->Cell($w[4],8,$adjustment['description'],1,0,'L');		
					if ($adjustment['adjustment_type'] == 'Debit') {
						$this->hnumis_pdf->Cell($w[5],8,number_format($adjustment['amount'],2,'.',','),1,0,'L');				
						$this->hnumis_pdf->Cell($w[6],8,'',1,0,'L');
						$debit += $adjustment['amount'];
					} else if ($adjustment['adjustment_type'] == 'Credit') {
						$this->hnumis_pdf->Cell($w[5],8,'',1,0,'L');				
						$this->hnumis_pdf->Cell($w[6],8,number_format($adjustment['amount'],2,'.',','),1,0,'L');
						$credit += $adjustment['amount'];
					}
					$this->hnumis_pdf->Ln();
				}
				
							
			}
			
			$this->hnumis_pdf->Output();
		}


	public function assessment_report_group() {	
		
		$this->load->library('posting_lib');
		
		$this->posting_lib->assessment_report_group();

	}		


	public function assessment_report_college() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->assessment_report_college();
	
	}
	
	
	//Added: April 27, 2013 by Amie
	public function view_balance() {
		
		$this->content_lib->set_title('Posting | View Balance | ' . $this->config->item('application_title'));
		$this->load->model('hnumis/college_model');
		$this->load->model('teller/teller_model');
		$this->load->model('hnumis/programs_model');
		$this->load->model('teller/teller_model');
		$this->load->model('Col_Students_Model');

		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
		
		switch ($step) {
			case 1:
				$data['college'] = $this->college_model->all_colleges();
				$this->content_lib->enqueue_body_content('posting/view_balance', $data);
				$this->content_lib->content();
				break;
			case 2:
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$college = sanitize_text_field($this->input->post('colleges', TRUE));
					$data['college'] = $this->college_model->getCollegeName($college);
					
					$program = sanitize_text_field($this->input->post('programs', TRUE));
					$data['program'] = $this->programs_model->getProgram($program);
									
					$data['year'] = sanitize_text_field($this->input->post('year', TRUE));
					$data['class_payers'] = $this->programs_model->list_payers_by_program($program, $data['year']);
									
					$this->content_lib->enqueue_body_content('posting/view_student_balance', $data);
					$this->content_lib->content();
				}
				break;
	
		}
	
	}

	public function ar_balance(){
		$this->load->library('ar_balance');
		$this->ar_balance->index();
	}

	public function ar_balance_basic(){
		$this->load->library('ar_balance');
		$this->ar_balance->basic();
	}
	
	public function ar_balance_college(){
		$this->load->library('ar_balance');
		$this->ar_balance->college();
	}
	
	
	//delete this later    //who commented this??-Toyet 10.12.2017
	public function ar_balance_ver2(){
		$this->load->library('ar_balance_ver2');	
		$this->ar_balance_ver2->index();
	}
			
	
	//delete this later    //who commented this??-Toyet 10.12.2017
	public function ar_balance_action_ver2(){
		$this->load->library('ar_balance_ver2');
		$this->ar_balance_ver2->ar_balance_ver2();
	}

	//delete this later    //who commented this??-Toyet 10.12.2017
	public function ar_balance_action_summary_ver2(){
		//echo 'here';die();
		$this->load->library('ar_balance_ver2');
		$this->ar_balance_ver2->ar_balance_summary_ver2();
	}

	// added Toyet 10.12.2017
	public function ar_balance_action_ver2_raw(){
		$this->load->library('ar_balance_ver2');
		$this->ar_balance_ver2->ar_balance_ver2_raw();
	}
	
	public function ar_transactions(){
		$this->load->library('ar_transactions_lib');
		$this->ar_transactions_lib->index();
	}
	
	public function ar_transactions_details(){
		$this->load->library('ar_transactions_lib');
		$this->ar_transactions_lib->ar_transactions_details();
	}
	
	public function ar_transactions_summary(){
		$this->load->library('ar_transactions_lib');
		$this->ar_transactions_lib->ar_transactions_summary();
	}
		
	
	public function students_balances(){
		$this->load->library('students_balances');
		$this->students_balances->index();
	}

	public function added_subjects_after_assessments(){
		$this->load->library('extras_lib');
		$this->extras_lib->added_subjects_after_assessments();
	}
	
	public function mismatches(){
		$this->load->library('extras_lib');
		$this->extras_lib->mismatches();		
	}
	
	
	public function ar_balances(){
		$this->load->library('extras_lib');
		$this->extras_lib->ar_schedule();
	}

	public function ar_deposits(){
		$this->load->library('extras_lib');
		$this->extras_lib->ar_schedule(TRUE);
	}

	public function total_withdrawals() {
		$this->load->library('extras_lib'); //partially done but found out there is already same report for this accountant
		$this->extras_lib->total_withdrawals();
	}
	
	public function list_students_with_more_than_one_idno(){
		$this->load->library('extras_lib');
		$this->extras_lib->list_students_with_more_than_one_idno();
	}

	public function write_off(){
		// to the previous developer - what are the details and purposes
		//    of such detail for this table?
		//    how do we generate new updates for this table?
		// 
		//   -Toyet 5.12.2018
		// Discovery:  
		// write_off_table is a pre-generated table
		//             supposedly containing the pre-determined
		//             accounts for write-off.  
		//             Criteria for writeoff:
		//             1. has balance above zero
		//             2. last transaction is greater than May 31
		//                three years ago
		//             3. has not been previously written-off or
		//                if has been written-off but became active
		// write_off_ending is the cut-off date to use
		$config = array(
					'write_off_table'  => 'writeoff_2016',
					'write_off_ending' => '2016-05-31',
				);
		$this->session->set_userdata($config);
		$this->load->library('extras_lib');
		$this->extras_lib->write_off($config);
	}

	/**
	 * added by Tatskie Aug 11, 2014
	 */	
	function course_offerings(){
		$this->load->library('../controllers/dean');
		$this->dean->all_offerings();		
	}
	

	//ADDED: 5/31/14 genes
	function download_enrollment_summary_to_csv() {
		$this->load->model('hnumis/Reports_Model');
	
		$this->Reports_Model->download_enrollment_summary_to_csv();
	
	}
	
	/*
	 * @ADDED: 9/25/14
	* @author: genes
	*/
	public function assessment_basic_ed_group() {
	
		$this->load->library('posting_lib');
	
		$this->posting_lib->assessment_basic_ed_group();
	
	}


	/*
	 * @NOTE: copied from audit
	 */
	public function unposted_assessment(){
	
		$this->load->model('teller/assessment_model');
		$this->load->model('academic_terms_model');
	
		$term = $this->academic_terms_model->getCurrentAcademicTermID();
		$data['list'] = $this->assessment_model->listunpostedassessment($term->current_academic_term_id);
	
		$this->content_lib->enqueue_body_content('audit/list_unposted_assessment', $data);
	
		$this->content_lib->content();
	}
	
	/*
	 * @ADDED: 11/19/14
	 * @author: genes
	 */
	function list_allocations() {
		$this->load->model('hnumis/Offerings_Model');

		$data['allocations'] = $this->Offerings_Model->ListAllocation($this->input->get('offer_id'));
		$data['block_names'] = $this->Offerings_Model->ListBlockOfferings($this->input->get('offer_id'));
		
		$this->load->view('dean/list_of_allocations_from_modal', $data);
	}

	/*
	* ADDED: 4/11/15
	* @author: genes
	* @description: displays students enrolled from enrollment summary modal
	*/
	function get_basic_ed_students() {
		$this->load->model('basic_ed/basic_ed_enrollments_model');
		
		$data['students'] = $this->basic_ed_enrollments_model->list_enrollees($this->input->get('prog_id'), $this->input->get('yr_level'), 0, $this->input->get('selected_year'));
		//print_r($data); die();
		$this->load->view('rsclerk/list_of_students_from_modal', $data);
		
	}

	/*
	* ADDED: 6/10/14
	* @author: genes
	* @description: displays students enrolled from enrollment summary modal
	*/
	function get_students() {
		$this->load->model('hnumis/Enrollments_Model');
		
		$data['students'] = $this->Enrollments_Model->masterlist($this->input->get('selected_term'), $this->input->get('prog_id'), $this->input->get('yr_level'), TRUE);
		
		$this->load->view('dean/list_of_students_from_modal', $data);
		
	}
	
	public function shs_assessment_report_strand() {
		$this->load->library('shs/assessment_lib');
	
		$this->assessment_lib->shs_assessment_report_strand();
			
	}

	public function shs_enrollment_summary() {
		$this->load->library('shs/reports_lib');
	
		$this->reports_lib->enrollment_summary();
			
	}

	/* Added by Toyet 7.27.2018 */
	function mistmatched_assessments_bed($term=469) {
		$this->load->model('student_common_model');
		$this->load->model('financials/Payments_Model');
		$this->load->model('Academic_terms_model');
		$this->load->model('basic_ed/Basic_Ed_Enrollments_Model');
		$this->load->model('financials/Scholarship_Model');
		$this->load->model('accounts/Accounts_model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('basic_ed/assessment_basic_ed_model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('hnumis/faculty_model');
		$this->load->model('basic_ed/Reports_Model');

		$academic_terms_id = 469;
		$academic_years_id = 119;
		$levels_array = array( 1, 2, 3, 4, 5, 11 );

		$eval_student = array();
		$mismatched = array();
		$course_prefix = "";

		foreach($levels_array as $row => $level):

			switch ($level) {
				case 1:
					$year_array = array( 1 );
					$course_prefix = "Kinder ";
					break;
				case 2:
					$year_array = array( 1 );
					$course_prefix = "Kinder ";
					break;
				case 3:
					$year_array = array( 1, 2, 3, 4, 5, 6 );
					$course_prefix = "Grade ";
					break;
				case 4:
					$year_array = array( 7, 8, 9, 10 );
					$course_prefix = "HS ";
					break;
				case 5:
					$year_array = array( 7, 8, 9, 10 );
					$course_prefix = "HS ";
					break;
				case 11:
					$year_array = array( 1 );
					$course_prefix = "Pre-School ";
					break;
			}

			foreach($year_array as $row => $year):

				$students_for_term = $this->Basic_Ed_Enrollments_Model->list_enrollees($level, $year, $section=NULL, $academic_years_id);
				log_message("INFO",print_r($course_prefix,true));
				if(count($students_for_term)>0):
					foreach($students_for_term as $enrollee){
						$basic_ed_fees = $this->assessment_basic_ed_model->ListBasic_Ed_Fees($level, $academic_years_id, $year);
						$tuition_basic = $this->assessment_basic_ed_model->getTuitionRateBasic($level, $academic_years_id, $year);
						$post_status   = $this->assessment_basic_ed_model->CheckIfPosted($enrollee->hist_id);
						if($post_status) {
							$posted_assessment = $this->assessment_basic_ed_model->getPostedAssessment_BEd($enrollee->hist_id);
						} else {
							$posted_assessment[0]->assessed_amount = 0.00;
						}
						//log_message("INFO","".print_r($tuition_basic,true));
						//log_message("INFO","BASIC ED FEES => ".print_r($basic_ed_fees,true));
						$misc_fees = 0.00;
						foreach($basic_ed_fees as $key => $val){
							//log_message("INFO",print_r($key,true));
							//log_message("INFO",print_r($val,true));
							$misc_fees += $val->rate;
						}

						if(round($posted_assessment[0]->assessed_amount,2)<>round($tuition_basic->rate+$misc_fees,2)){
							$eval_student[] = array(
								'students_idno'=>$enrollee->students_idno,
								'name'=>$enrollee->name,
								'course_year'=>$course_prefix.(string)$year,
								'section'=>$enrollee->section,
								'posted_assessment'=>round($posted_assessment[0]->assessed_amount,2),
								'supposed_assessment'=>round($tuition_basic->rate+$misc_fees,2),
							);
						}
					}
				endif;
			endforeach;
		endforeach;

		$this->content_lib->enqueue_body_content('developers/list_mismatched_assessments',
	                        array('students'=>$eval_student,
	                              'list_title'=>"BASIC EDUCATION" )
	                    );
	   	$this->content_lib->content();
	}

	
}
