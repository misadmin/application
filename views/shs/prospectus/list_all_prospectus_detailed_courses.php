<style>
	#container{width:100%;}
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>

			<div style="overflow:auto; margin-bottom:20px; width:100%;">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:15%;">Type</td>
								<td class="shs_header" style="width:10%;">Course<br>Code</td>
								<td class="shs_header" style="width:40%;">Descriptive Title</td>
								<td class="shs_header" style="width:5%;">Credit<br>Units</td>
								<td class="shs_header" style="width:15%;">Prerequisites</td>
								<td class="shs_header" style="width:5%;">Add<br>Prereq.</td>
								<td class="shs_header" style="width:5%;">Update<br>Type</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($detailed_courses) {
									foreach($detailed_courses AS $course) {
							?>
										<tr>	
											<td style="vertical-align:middle; font-weight:bold; color:#26437C;"><?php print($course->type_description); ?></td>
											<td style="vertical-align:middle; text-align:center;"><?php print($course->course_code); ?></td>
											<td style="vertical-align:middle;"><?php print($course->descriptive_title); ?></td>
											<td style="vertical-align:middle; text-align:center;"><?php print($course->credit_units); ?></td>
											<td>
												<?php 
													if (isset($course->prereq_courses)) {
														foreach($course->prereq_courses AS $prereq_course) {
												?>
														<div style="float:left; width:90%;">
															<?php print($prereq_course->pre_req); ?>
														</div>
														<div>
												<a href="<?php print($prereq_course->prerequisites_id); ?>" class="del_prerequisite_course" >
													<div id="show_prereq_trash_icon_<?php print($prereq_course->prerequisites_id); ?>" >
														<i class="icon-trash"></i>
													</div>
												</a>
														</div>
												<?php
														}
													}
												?>
											</td>
											<td style="vertical-align:middle; text-align:center;">
												<?php
													if (($year_level > 11) OR ($year_level >= 11 AND $pros_term > 1))  {
												?>
														<a href="<?php print($course->id); ?>" 
															descriptive_title="<?php print($course->descriptive_title); ?>"
															pros_term="<?php print($pros_term); ?>"
															year_level="<?php print($year_level); ?>"
															class="add_prerequisite" >
															<div id="show_pencil_prereq_icon_<?php print($course->id); ?>" >
																<i class="icon-pencil"></i>
															</div>
														</a>
												<?php
													}
												?>
											</td>
											<td style="vertical-align:middle; text-align:center;">
												<a href="<?php print($course->id); ?>" 
													descriptive_title="<?php print($course->descriptive_title); ?>"
													subject_types_id="<?php print($course->subject_types_id); ?>"
													class="update_type" >
													<div id="show_edit_icon_<?php print($course->id); ?>" >
														<i class="icon-edit"></i>
													</div>
												</a>
											</td>
											<td style="vertical-align:middle; text-align:center;">
												<a href="<?php print($course->id); ?>" 
													courses_id="<?php print($course->courses_id); ?>"
													class="del_prospectus_term_course" >
													<div id="show_trash_icon_<?php print($course->id); ?>" >
														<i class="icon-trash"></i>
													</div>
												</a>
											</td>
										</tr>
							<?php
									}
								}
							?>	
						</tbody>
					</table>
			</div>

			