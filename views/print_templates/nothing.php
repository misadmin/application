<div id="grades_to_print" style="display:none;"><?php echo chr(191).chr(189).chr(67).chr(33).chr(239).chr(191).chr(189).chr(58);?>
06120934
ARACO, ZENITH G
4 BSHRM
1st Sem SY 2013-2014
�EStudent's Grade�F

------------- ------ --------------------------- ------- ------- ------- -------
Catalog #              Descriptive Title         Units  Pre-lim Mid-trm  Final
------------- ------ --------------------------- ------- ------- ------- -------
HRM Prac 2   A       Hotel On-the-Job Training  10.0                      1.2
------------- ------ --------------------------- ------ ------- -------- -------
�G    GRADUATED:October 13, 2013 from the Four-Year Course in Hotel and
Restaurant Management leading to the degree of BACHELOR OF
SCIENCE IN HOTEL AND RESTAURANT MANAGEMENT (BSHRM). Exempted
from Special Order by virtue of the grant of autonomy status in
accordance with CHED Memorandum Order No. 32, series of 2001.
�H------------- ------ --------------------------- ------ ------- -------- -------
� Student's Grade. Printed using Altair ISIS. Dec 13, 2013 10:35�</div>
<script>
function print(content) {
    var dapplet = document.jzebra;
    if (dapplet != null) {
       /* dapplet.append('\x1B\x40');*/
       dapplet.append(content);
       dapplet.print();
	}
}
    
$(document).ready(function(){
	var content_to_print = $('#grades_to_print').text();
	var vapplet = document.jzebra;
	if (vapplet != null) {
		// Searches for default printer
		vapplet.findPrinter();
	}
	print(content_to_print);
});
</script>