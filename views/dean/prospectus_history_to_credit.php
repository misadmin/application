<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>


<div style="background:#FFF; width:90%; margin-left:40px;">
<div style="width:100%; margin-bottom:10px;">
<span style="margin-left:5px; color:#FF0000; font-weight: bold;">NOTE: Grades with asterisk(*) are from CREDITED COURSES</span>

<?php /*  Add a button to display the scanned TOR */?>

</div>
<?php $display_note = false;?>
<?php
	if ($prospectus_terms) {
		foreach ($prospectus_terms AS $term) {
?>


<div style="width:100%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td align="left" style="font-size:16px; font-weight:bold;"><?php print($term->y_level." - ".$term->term); ?>
      <a class="add_course" href="<?php print($term->id); ?>" style="text-decoration:none; font-size:12px; font-weight:bold; color:#666; font-family:Verdana, Geneva, sans-serif;"></a></td>
    </tr>
</table>
</div>



<div style="width:100%; margin-bottom:40px;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover table-condensed" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="15%" class="head" style="vertical-align:middle;">Catalog No.</th>
      <th width="30%" class="head" style="vertical-align:middle;">Descriptive Title</th>
      <th width="7%" class="head" style="vertical-align:middle;">Cut-off Grade</th>
      <th width="7%" class="head" style="vertical-align:middle;">No. of Retakes</th>
      <th width="27%" class="head" style="vertical-align:middle;">Pre-requisites</th>
      <th width="7%" class="head" style="vertical-align:middle;">Credit Units</th>
      <th width="7%" class="head" style="vertical-align:middle;">GRADE</th>
      </tr>
    </thead>
    <?php
		$courses = $this->Prospectus_Model->ShowProspectusGrades($term->id,$student['idnum']);
		//print_r($courses); die();
		if ($courses) {
			foreach ($courses AS $course) {
				
				$credited_courses = $this->Student_Model->ListCreditedCourses($student['idnum'],$course->prospectus_courses_id);

				if (!is_null($course->grade)) {
	?>
					<tr style="background:#FFFFD9">
					  <td style="text-align:left; vertical-align:middle; "><?php print($course->course_code); ?></td>
					  <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->cutoff_grade); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->num_retakes); ?></td>
					  <td style="text-align:left; vertical-align:middle; ">
					  <?php 

					  $prereqs = $this->Prospectus_Model->ListPrereq_Courses($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->course_code.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  	print("<br>");
					  }
					  $prereqs = $this->Prospectus_Model->ListPrereq_YrLevel($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->pre_req.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  		}
					  
					  ?></td>
					  
					  <td style="text-align:center; vertical-align:middle; ">
					  <?php 
					  		if ($course->is_bracketed == 'Y') {
					  			print("(".$course->credit_units.")"); 
					  		} else {
					  			print($course->credit_units); 
							}
						?></td>
					  <td align="center" style="text-align:center; vertical-align:middle; ">
						   <?php 
								if (strpos($course->grade,'*')){
							?>
							<a href="#" data-toggle="modal" data-target="#modal_show_credit_<?php print($course->prospectus_courses_id); ?>" 
								data-project-id='<?php //print($credited_courses); ?>'>
									<i class="icon-zoom-in"></i></a>
									
						  <!-- modal for showing credited course -->
      
						      <div class="modal hide fade" id="modal_show_credit_<?php print($course->prospectus_courses_id); ?>" >
						 			<form method="post" id="remove_course_form_<?php print($course->prospectus_courses_id); ?>">
						   				<input type="hidden" name="action" value="remove_credited_course" />
						   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							   		<div class="modal-header">
							       		<button type="button" class="close" data-dismiss="modal">�</button>
							       		<h3>Credited Course</h3>
							   		</div>
							   		<div class="modal-body" style="text-align:center;">
							   				<div style="text-align:left; font-weight: bold; font-size:16px;">
							   					<?php 
													print($course->course_code." - ".$course->descriptive_title);
													print("<br><i>".$course->credit_units." Unit/s</i>");
							   					?>
							   				</div>
							   				<table class="table table-condensed table-hover" style="width:100%; border:1px;">
							   				<thead>
							   					<tr>
							   					<?php 
							   						if ($can_credit) {
							   					?>
								   					<th style="width:5%;  text-align:center; ">&nbsp;</th>
								   					<th style="width:10%; text-align:center; vertical-align: middle; ">Catalog No.</th>
								   					<th style="width:50%; text-align:center; vertical-align: middle; ">Descriptive Title</th>
							   					<?php 
							   						} else {	
							   					?>
							   						<th style="width:10%; text-align:center; vertical-align: middle; ">Catalog No.</th>
								   					<th style="width:55%; text-align:center; vertical-align: middle; ">Descriptive Title</th>
							   					<?php 
							   						}
							   					?>
							   					<th style="width:10%; text-align:center; vertical-align: middle;">Units</th>
							   					<th style="width:10%; text-align:center; vertical-align: middle; ">Final Grade</th>
							   					<th style="width:15%; text-align:center; vertical-align: middle; ">Where Taken?</th>
							   					</tr>	
							   				</thead>
											<?php 
												if ($credited_courses) {
														foreach($credited_courses AS $ccourse) {
															if ($can_credit) {
																print("<tr><td style=\"text-align:center; vertical-align:middle; \">
																		<input type=\"checkbox\" name=\"r[]\" 
																		value=".$ccourse->id."></td>");
															}
															print("<td style=\"text-align:center; vertical-align:middle;\">".$ccourse->cat_no."</td>");
															print("<td style=\"vertical-align:middle;\">".$ccourse->descriptive_title."</td>");
															print("<td style=\"text-align:center; vertical-align:middle;\">".$ccourse->units."</td>");
															print("<td style=\"text-align:center; vertical-align:middle;\">".$ccourse->final_grade."</td>");
															print("<td style=\"text-align:center; vertical-align:middle;\">".$ccourse->taken_where."</td>");
															print("</tr>");
														}
												}
											?>   				
							   			</table>
							   			
					   				</div>
								   <div class="modal-footer">
								   		<?php 
								   			if ($can_credit) {
								   		?>
							    		<input type="submit" class="btn btn-danger" id="remove_credited_<?php print($course->prospectus_courses_id); ?>" value="REMOVE COURSE!">
								   		<?php 
								   			}	
								   		?>
								   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							       </div>
							       </form>
						    </div>		
						    
						    <script>
								$('#remove_credited_<?php print($course->prospectus_courses_id); ?>').bind('click', function(event){
									event.preventDefault();
									var confirmed = confirm("Continue to remove credited course?");
						    
									if (confirmed){
									
										$('#remove_course_form_<?php print($course->prospectus_courses_id); ?>').submit();
									}
								});
							</script>
						    
    					 <!-- End of showing credited course  -->
									
									
							<?php 
								}else{
									print($course->grade);
								}



							?>
					  </td>
	  			</tr>
	<?php
				} else {
						$my_code="";
						if ($course->elective == 'Y') {
							$pattern = '/,/';
							$subject = $course->elective_grade;
							$my_code = preg_split($pattern, $subject);
						}
	?>
					<tr <?php if (isset($my_code[1]) AND ($my_code[1] != 'HNU*')) { print("class=\"success\""); }?>>
					  <td style="text-align:left; vertical-align:middle; ">
						
							<?php 
								if (!$can_credit) {
							  		if (isset($my_code[1]) AND ($my_code[1] != 'HNU*')) { 
							  			foreach($my_code AS $k=>$v) {
							  				$num = $k/3; //get only the 0,3,6... array
					  						if (floor($num) == $num) {
					  							print($course->course_code.' <i>['.$my_code[$k].']</i>');
					  						}
							  			} 
									} else {
						  				print($course->course_code); 
									}
									//print($course->course_code);
								} else {
									if ($course->elective == 'Y') {
										if (isset($my_code[1]) AND ($my_code[1] != 'HNU*')) { 
					  						foreach($my_code AS $k=>$v) {
					  							$num = $k/3; //get only the 0,3,6... array
					  							if (floor($num) == $num) {
				  									print($course->course_code.' <i>['.$my_code[$k].']</i><br>');
					  							}  
					  						} 
										} else {
							?>

          					<a href="#" data-toggle="modal" data-target="#modal_credit_course_<?php print($course->prospectus_courses_id); ?>" 
								data-project-id='<?php print($course); ?>'><?php print($course->course_code); ?></a>
						      <div class="modal hide fade" id="modal_credit_course_<?php print($course->prospectus_courses_id); ?>" >
						 		<form method="post" id="credit_course_form_<?php print($course->prospectus_courses_id); ?>">
						   				<input type="hidden" name="action" value="credit_course" />
						   				 <input type="hidden" name="prospectus_courses_id" value= "<?php print($course->prospectus_courses_id);?>"/>
						   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							   		<div class="modal-header">
							       		<button type="button" class="close" data-dismiss="modal">�</button>
							       		<h3>Credit Course</h3>
							   		</div>
							   		<div class="modal-body">
		 								<div style="text-align:left; font-weight: bold; font-size:16px;">
											<?php 
												print($course->course_code." - ".$course->descriptive_title);
												print("<br><i>".$course->credit_units." Unit/s</i>");
											?>
										</div>
										<table cellpadding="2" cellspacing="0" 
												class="table table-striped table-condensed table-hover" style="width:100%; border:none;">
											<tr>
							  					<td style="width:20%;" align="left" valign="middle">Taken at HNU?</td>
							  					<td style="width:2%;" align="left" valign="middle">:</td>
							  					<td style="width:78%;" align="left" valign="middle">
							  					<input name="taken_type" type="radio" class="form" 
													onClick="document.getElementById('hnu_<?php print($course->id); ?>').style.display = 'table';
									 							document.getElementById('other_school_<?php print($course->id); ?>').style.display = 'none'; 
									 							return;" 
																value="Y" checked="checked"/> Yes
							  					<input name="taken_type" type="radio" class="form" 
													onClick="document.getElementById('hnu_<?php print($course->id); ?>').style.display = 'none';
									 							document.getElementById('other_school_<?php print($course->id); ?>').style.display = 'table'; 
									 							return;" 
																value="N" /> No
							  					</td>
											</tr>
											<tr>
												<td colspan="3">
									  				<table class="table" id="hnu_<?php print($course->id); ?>" style="width:100%; display: block;">
									    				<tr>
									      					<td width="19%" align="left">Course</td>
									      					<td width="2%" align="left">:</td>
									      					<td width="79%" align="left"><select name="enrollments_id" id="enrollments_id" class="form">
									        				<?php
																foreach($courses_passed AS $course_pass) {
																	print("<option value=".$course_pass->enrollments_id.">".$course_pass->course_code." - ".$course_pass->descriptive_title."</option>");	
																}
															?>  </select></td>
									    				</tr>
									  				</table>
											
									  				<table class="table" id="other_school_<?php print($course->id); ?>" style="width:100%; display: none;">
									      				<tr>
									        				<td align="left">								
																<?php
																	if ($other_schools) {
																		foreach ($other_schools AS $other) {
																			print("<span style='font-size:18px; padding-right:20px;'>".$other->program."</span><br>");
																			print("<span style='font-size:14px; font-style:italic;'>".$other->term." - ".$other->school."</span><br>");
																			print("<span style='font-size:14px; font-style:italic;'>".$other->school_address."</span>");
																?>        				
									        				
																 <div style="width:100%; margin-bottom:10px;">
																  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
																    <thead>
																    <tr>
																      <th width="10%" class="head" style="vertical-align:middle;">&nbsp;</th>
																      <th width="10%" class="head" style="vertical-align:middle;">Catalog No.</th>
																      <th width="35%" class="head" style="vertical-align:middle;">Descriptive Title</th>
																      <th width="6%" class="head" style="vertical-align:middle;">Units</th>
																      <th width="6%" class="head" style="vertical-align:middle;">Final Grade</th>
																      <th width="37%" class="head" style="vertical-align:middle;">Remarks</th>
																      </tr>
																    </thead>
																    <?php
																		$courses = $this->OtherSchools_Model->ListCourses($other->id);
																		if ($courses) {
																			foreach ($courses AS $course_other) {
																	?>
																    <tr>
																      <td style="text-align:center; vertical-align:middle; "><input type="checkbox" name="c[]" value="<?php print($course_other->id); ?>"></td>
																      <td style="text-align:left; vertical-align:middle; "><?php print($course_other->catalog_no); ?> </td>
																      <td style="text-align:left; vertical-align:middle; "><?php print($course_other->descriptive_title); ?></td>
																      <td style="text-align:center; vertical-align:middle; "><?php print($course_other->units); ?></td>
																      <td style="text-align:center; vertical-align:middle; "><?php print($course_other->final_grade); ?></td>
																      <td style="text-align:center; vertical-align:middle; "><?php print($course_other->remarks); ?></td>
																      </tr>
																    <?php
																			}
																		}
																	?>
																
																</table>
																</div>       				
									        				
									        				<?php 
									        						}
									        					}	
									        				?>
									        				</td>
									      				</tr>
									   				</table>
   				</td>
   				</tr>
   				
   				</table>
   				
   				</div>
								   <div class="modal-footer">
							    		<input type="submit" class="btn btn-primary" value="CREDIT COURSE!">
								   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							       </div>
								</form>
						    </div>		

							<?php 				
										}	
									} else {
							?>
						  <!-- modal for crediting course -->
      
          					<a href="#" data-toggle="modal" data-target="#modal_credit_course_<?php print($course->prospectus_courses_id); ?>" 
								data-project-id='<?php print($course); ?>'><?php print($course->course_code); ?></a>

						      <div class="modal hide fade" id="modal_credit_course_<?php print($course->prospectus_courses_id); ?>" >
						 		<form method="post" id="credit_course_form_<?php print($course->prospectus_courses_id); ?>">
						   				<input type="hidden" name="action" value="credit_course" />
						   				 <input type="hidden" name="prospectus_courses_id" value= "<?php print($course->prospectus_courses_id);?>"/>
						   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							   		<div class="modal-header">
							       		<button type="button" class="close" data-dismiss="modal">�</button>
							       		<h3>Credit Course</h3>
							   		</div>
							   		<div class="modal-body">
		 								<div style="text-align:left; font-weight: bold; font-size:16px;">
											<?php 
												print($course->course_code." - ".$course->descriptive_title);
												print("<br><i>".$course->credit_units." Unit/s</i>");
											?>
										</div>
										<table cellpadding="2" cellspacing="0" 
												class="table table-striped table-condensed table-hover" style="width:100%; border:none;">
											<tr>
							  					<td style="width:20%;" align="left" valign="middle">Taken at HNU?</td>
							  					<td style="width:2%;" align="left" valign="middle">:</td>
							  					<td style="width:78%;" align="left" valign="middle">
							  					<input name="taken_type" type="radio" class="form" 
													onClick="document.getElementById('hnu_<?php print($course->id); ?>').style.display = 'table';
									 							document.getElementById('other_school_<?php print($course->id); ?>').style.display = 'none'; 
									 							return;" 
																value="Y" checked="checked"/> Yes
							  					<input name="taken_type" type="radio" class="form" 
													onClick="document.getElementById('hnu_<?php print($course->id); ?>').style.display = 'none';
									 							document.getElementById('other_school_<?php print($course->id); ?>').style.display = 'table'; 
									 							return;" 
																value="N" /> No
							  					</td>
											</tr>
											<tr>
												<td colspan="3">
									  				<table class="table" id="hnu_<?php print($course->id); ?>" style="width:100%; display: block;">
									    				<tr>
									      					<td width="19%" align="left">Course</td>
									      					<td width="2%" align="left">:</td>
									      					<td width="79%" align="left"><select name="enrollments_id" id="enrollments_id" class="form">
									        				<?php
																foreach($courses_passed AS $course_pass) {
																	print("<option value=".$course_pass->enrollments_id.">".$course_pass->course_code." - ".$course_pass->descriptive_title."</option>");	
																}
															?>  </select></td>
									    				</tr>
									  				</table>
											
									  				<table class="table" id="other_school_<?php print($course->id); ?>" style="width:100%; display: none;">
									      				<tr>
									        				<td align="left">								
																<?php
																	if ($other_schools) {
																		foreach ($other_schools AS $other) {
																			print("<span style='font-size:18px; padding-right:20px;'>".$other->program."</span><br>");
																			print("<span style='font-size:14px; font-style:italic;'>".$other->term." - ".$other->school."</span><br>");
																			print("<span style='font-size:14px; font-style:italic;'>".$other->school_address."</span>");
																?>        				
									        				
																 <div style="width:100%; margin-bottom:10px;">
																  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
																    <thead>
																    <tr>
																      <th width="10%" class="head" style="vertical-align:middle;">&nbsp;</th>
																      <th width="10%" class="head" style="vertical-align:middle;">Catalog No.</th>
																      <th width="35%" class="head" style="vertical-align:middle;">Descriptive Title</th>
																      <th width="6%" class="head" style="vertical-align:middle;">Units</th>
																      <th width="6%" class="head" style="vertical-align:middle;">Final Grade</th>
																      <th width="37%" class="head" style="vertical-align:middle;">Remarks</th>
																      </tr>
																    </thead>
																    <?php
																		$courses = $this->OtherSchools_Model->ListCourses($other->id);
																		if ($courses) {
																			foreach ($courses AS $course_other) {
																	?>
																    <tr>
																      <td style="text-align:center; vertical-align:middle; "><input type="checkbox" name="c[]" value="<?php print($course_other->id); ?>"></td>
																      <td style="text-align:left; vertical-align:middle; "><?php print($course_other->catalog_no); ?> </td>
																      <td style="text-align:left; vertical-align:middle; "><?php print($course_other->descriptive_title); ?></td>
																      <td style="text-align:center; vertical-align:middle; "><?php print($course_other->units); ?></td>
																      <td style="text-align:center; vertical-align:middle; "><?php print($course_other->final_grade); ?></td>
																      <td style="text-align:center; vertical-align:middle; "><?php print($course_other->remarks); ?></td>
																      </tr>
																    <?php
																			}
																		}
																	?>
																
																</table>
																</div>       				
									        				
									        				<?php 
									        						}
									        					}	
									        				?>
									        				</td>
									      				</tr>
									   				</table>
   				</td>
   				</tr>
   				
   				</table>
   				
   				</div>
								   <div class="modal-footer">
							    		<input type="submit" class="btn btn-primary" value="CREDIT COURSE!">
								   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							       </div>
								</form>
						    </div>		
    					 <!-- End of modal crediting course -->
						<?php 
									}
							}
						?>
						 
					</td>
					  <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->cutoff_grade); ?></td>
					  <td style="text-align:center; vertical-align:middle; "><?php print($course->num_retakes); ?></td>
					  <td style="text-align:left; vertical-align:middle; ">
					  <?php 

					  $prereqs = $this->Prospectus_Model->ListPrereq_Courses($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->course_code.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  	print("<br>");
					  }
					  $prereqs = $this->Prospectus_Model->ListPrereq_YrLevel($course->prospectus_courses_id);
					  if ($prereqs) {
					  	$str_prereq = "";
					  	foreach ($prereqs AS $prereq) {
					  		$str_prereq = $str_prereq.$prereq->pre_req.", ";
					  	}
					  	$str_prereq = substr_replace($str_prereq ,"",-2);
					  	print($str_prereq);
					  		}
					  
					  ?></td>
					  
					  <td style="text-align:center; vertical-align:middle; ">
					  <?php 
					  		if ($course->is_bracketed == 'Y') {
					  			print("(".$course->credit_units.")"); 
					  		} else {
					  			print($course->credit_units); 
							}
						?></td>
					  <td style="text-align:center; vertical-align:middle; ">
					  	<?php
					  		if (isset($my_code[1]) AND ($my_code[1] != 'HNU*')) { 
					  			foreach($my_code AS $k=>$v) {
					  				$num = $k/3; //get only the 0,3,6... array
					  				if (floor($num) == $num) {
					  					print($my_code[$k+1]."<br>");
					  				}
					  			} 
							} else {
				  				print("&nbsp;"); 
							}
					  	?>	
						</td>
					</tr>
	<?php
				}
			}
		}
	?>
</table>
</div>

<?php
		}
	}
?>
</div>


<?php if ($display_note) echo '<div><span style="color:red"><b>NOTE:</b> <i>Grades with asterisk(*) are from CREDITED COURSES</i></span></div>';?>

