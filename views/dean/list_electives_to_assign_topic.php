<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>
<div style="background:#FFF; width:90%;">
<div style="width:100%; margin-bottom:40px;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover table-condensed" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr style="height: 40px;">
      <th width="10%" class="head" style="vertical-align:middle;">Year & Semester</th>
      <th width="10%" class="head" style="vertical-align:middle;">Course Code</th>
      <th width="35%" class="head" style="vertical-align:middle;">Descriptive Title/Assigned Topics</th>
      <th width="6%" class="head" style="vertical-align:middle;">Credit Units</th>
      <th width="6%" class="head" style="vertical-align:middle;">Assign Course </th>
      </tr>
    </thead>
    <?php
		if ($electives) {
			foreach ($electives AS $elective) {
	?>
    <tr>
      <td style="text-align:left; vertical-align:middle; "><?php print($elective->y_level.' - '.$elective->term); ?></td>
      <td style="text-align:left; vertical-align:middle; "><?php print($elective->course_code); ?></td>
      <td style="text-align:left; vertical-align:middle; ">
      <?php 
      		print($elective->descriptive_title."<br>");
			$topics = $this->Electives_Model->ListElectiveTopic($elective->id);
			
			if ($topics) {
				print("<span style='padding-left:20px; font-weight:bold;'>Assigned Topics:</span><br>");		
				
				foreach($topics AS $topic) {
					print("<span style='padding-left:30px; font-style:italic;'>".$topic->course_code." - ".$topic->descriptive_title."</span><br>");		
				}
			}
      ?>
      </td>
      <td style="text-align:center; vertical-align:middle; "><?php print($elective->credit_units); ?></td>
      <td style="text-align:center; vertical-align:middle; ">
	  	<a class="detail_course" href="<?php print($elective->id); ?>" ><i class="icon-edit"></i></a>		</td>
      </tr>
    <?php
			}
		}
	?>
</table>
</div>
</div>



  <form id="detail_course2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" />
</form>

<script>
	$('.detail_course').click(function(event){
		event.preventDefault(); 
		var prospectus_courses_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prospectus_courses_id',
		    value: prospectus_courses_id,
		}).appendTo('#detail_course2');
		$('#detail_course2').submit();
	});
</script>
