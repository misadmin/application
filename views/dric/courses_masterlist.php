<div style="margin-top:10px">
<h2 class="heading"> Courses Masterlist (<?php print("SY ".$term->sy." ".$term->term); ?> )</h2>
<h2> <a href="download" class="btn btn-link pull-right" id="download_list"><i class="icon-download-alt"></i> <strong>Download</strong></a> </h2>	

<table class="table table-striped table-bordered">
	<thead >
		<tr align="center">
			<th>Catalog No.</th>
			<th>Description</th>
			<th>Pay Units</th>
			<th>Load Units</th>
			<th>Contact Hours</th>
			<th>College</th>
			<th>Flags</th>
		</tr>
	</thead>
	<tbody> 
<?php if($courses){ ?>
	<?php foreach ($courses as $course): ?>
		<tr>
			<td><?php print($course->course_code); ?></td>
			<td><?php print($course->descriptive_title); ?></td>
			<td><?php echo $course->paying_units; ?></td>
			<td><?php echo $course->credit_units; ?></td>
			<td><?php echo $course->contact_hours; ?></td>
			<td><?php print($course->college_code); ?></td>
			<td><?php print($course->course_type); ?></td>
		</tr>
<?php 
	endforeach; ?>
<?php } ?>
	</tbody>
</table>

<form method="post" action="<?php echo site_url("{$role}/download_courses_masterlist_csv"); ?>" id="download_list_form">
			<input type="hidden" name="term_id" value="<?php echo $term->id; ?>" />
			<input type="hidden" name="term" value="<?php echo $term->term; ?>" />
			<input type="hidden" name="sy" value="<?php echo $term->sy; ?>" />
</form>

<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
		
</div> 
