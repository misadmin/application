<h2 class="heading">Edit Scholarship</h2>

<div class="fluid span12">
	<form id ="delete_scholarship" action="<?php echo site_url('accounts/list_privileges');?>" method="post">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="delete_stud_privilege" />
		<input type="hidden" name="scholarship_id" value="<?php print($scholarship_id); ?>" />
				
		<fieldset>
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Scholarship Code</label>
					<div class="controls">
						<input disabled name="scholarship_code" type="text" id="scholarship" maxlength="15" value="<?php print($scholarship_code); ?>" style="width:100px;" >
					</div>
				
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Scholarship</label>
					<div class="controls">
						<input disabled name="scholarship" type="text" id="scholarship" maxlength="30" value="<?php print($scholarship); ?>" style="width:400px;" >
					</div>
				</div>
				
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit" name="edit_privilege" id="delete_privilege">Delete Scholarship</button>
					</div>
				</div>
			
		</fieldset>
		
	</form>
</div>

<script>
$('#delete_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to delete scholarship?");
    
	if (confirmed){
		
		$('#delete_scholarship').submit();
	}
});
</script>
