<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>



<div style="background:#FFF; width:40%; text-align:center; margin:auto;">
  					
  
<div style="width:95%;">
<table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  	<tr class="head" align="center">
  		<td colspan="3" class="head" style="text-align:left; padding:10px;">  CREDITED COURSES </td>
    </tr>
</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-hover" style="margin:10px;">
	   <thead>
   			<tr>
    			<th width="79%" class="header">Credited Courses</th>
    			<th width="9%" class="header">Grade</th>
			    <th width="9%" class="header">Taken at HNU?</th>
			    <th width="12%" class="header">Delete</th>
			</tr>
	   </thead>
		<?php
			if ($credited) {
				foreach ($credited AS $course) {
		?>
  			<tr>
			    <td><?php print($course->notes); ?></td>
			    <td><?php print($course->grade); ?></td>
			    <td><?php print($course->taken_here); ?></td>
			    <td style="text-align:center; vertical-align:middle;">
			      <a class="delete_course" href="<?php print($course->id); ?>" my_id="<?php print($course->id); ?>">
			        <img src="<?php print(base_url('assets/img/icon-recycle.gif')); ?>" width="20" height="20" align="absmiddle" style="vertical-align:middle; border:none" />
		          </a></td>
		  </tr>
		<?php
				}
			}
		?>
 
  <thead>
 <tr>
    <th colspan="4" class="header" style="text-align:left">&nbsp;
 	
	</th>
    </tr>
  </thead>
</table>
</div>
    </td>
    </tr>
    </table>
    </div>

</body>


<form id="delete_course" action="<?php echo site_url("dean/creditcourse")?>" method="post">
  <input type="hidden" name="step" value="3" />

</form>

<script>
	$('.delete_course').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete the course?');

		if (confirmed){
			var credited_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'credited_id',
		    value: credited_id,
		}).appendTo('#delete_course');
		$('#delete_course').submit();
		} 
		
	});
</script>
  