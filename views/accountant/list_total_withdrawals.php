 
<h2 class="heading"> LIST OF STUDENTS WHO TOTALLY WITHDREW (as of <?php echo date('M-d-Y'); ?> )</h2>	

<table class="table table-striped table-bordered">
	<thead >
		<tr align="center">
			<th>No.</th>
			<th>ID No.</th>
			<th>Last Name</th>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Program</th>
			<th>Year Level</th>
			<th>Withdrawn On</th>
		</tr>
	</thead>
	<tbody> 
<?php if($total_withdrawals){ 
			$num=1; ?>
<?php foreach ($total_withdrawals as $tw): ?>
		<tr>
			<td><?php echo $num; ?></td>
			<td><?php echo $tw->idno; ?></td>
			<td><?php echo $tw->lname; ?></td>
			<td><?php echo $tw->fname; ?></td>
			<td><?php echo $tw->mname; ?></td>
			<td><?php echo $tw->abbreviation; ?></td>
			<td><?php echo $tw->year_level; ?></td>
			<td><?php echo $tw->withdrawn_on; ?></td>
		</tr>
<?php 
		$num++;
	endforeach; ?>
<?php } ?>
	</tbody>
</table>
