			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; ">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:15%; height:30px;">Time</td>
								<td class="shs_header" style="width:17%; height:30px;">Monday</td>
								<td class="shs_header" style="width:17%; height:30px;">Tuesday</td>
								<td class="shs_header" style="width:17%; height:30px;">Wednesday</td>
								<td class="shs_header" style="width:17%; height:30px;">Thursday</td>
								<td class="shs_header" style="width:17%; height:30px;">Friday</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($class_schedules) {
									foreach($class_schedules AS $class_sched) {
							?>			
										<tr>
											<td style="text-align:center;"><?php print($class_sched->time_sched); ?></td>
											<td><?php print($class_sched->mon); ?></td>
											<td><?php print($class_sched->tue); ?></td>
											<td><?php print($class_sched->wed); ?></td>
											<td><?php print($class_sched->thu); ?></td>
											<td><?php print($class_sched->fri); ?></td>
										</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>
