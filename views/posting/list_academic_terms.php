<div style="float:left; width:auto;">
<form action="<?php 
	
	switch ($this->uri->segment(2)) {
		case 'enrollment_summary':
			print(site_url($this->uri->segment(1).'/enrollment_summary'));
			break;
		case 'assessment_summary':
			print(site_url($this->uri->segment(1).'/assessment_summary'));
			break;
	}
	?>" method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="2" name="step" />
	
	
	<div class="formSep">
		<h3>Select Academic Term</h3>
	</div> 

		<fieldset>
			<div class="control-group formSep">
				<label for="" class="control-label">Academic Term: </label>
					<div class="controls">
						<select id="academic_terms_id" name="academic_terms_id">
							<!--  <option value="">-- Select Academic Term --</option>  -->
          	<?php
				foreach($academic_terms AS $academic_term) {
					print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");	
				}
			?>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">Continue!</button>
					</div>
				</div>	
		</fieldset>
	</form>           
</div>