<div class="row-fluid span12">
	<div class="form-horizontal">
		<fieldset id="master_list_fieldset">
			<div class="control-group formSep">
				<label class="control-label" for="academic_terms">Academic Term</label>
				<div class="controls">
					<select name="academic_term" class="print_selector">
						<option value="">-- Select Academic Term --</option>
							<?php foreach ($academic_terms as $academic_term): ?>
							<?php if(!is_null($academic_term->term)): ?>
							<option value="<?php echo $academic_term->id; ?>"><?php echo $academic_term->term . "-" . $academic_term->sy; ?></option>
							<?php endif; ?>
							<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="group_by">Group By</label>
				<div class="controls">
					<select name="group_by" class="print_selector">
						<option value="">-- Select Group By --</option>
							<?php 
							$group_bys = array(
									'all'=>'All',
									'academic_groups'=>'Academic Groups',
									'college'=>'College',
									'program'=>'Program',
									'gender'=>'Gender',
							);
							foreach ($group_bys as $key => $group_by): ?>
							<option value="<?php echo $key; ?>"><?php echo $group_by; ?></option>
							<?php endforeach; ?>
					</select>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div id="submit_container" style="display:none">
	<div class="control-group formSep">
		<div class="controls">
			<a href="#" class="btn btn-primary" id="master_list_submit">Submit</a>
		</div>
	</div>
</div>
<script>
$().ready(function(){
	var submit_container = $('#submit_container').html();
	$('#master_list_fieldset select[name="group_by"]').on('change', function(){
		switch($(this).val()){
			case 'all'	:
				$('#master_list_fieldset').append(submit_container).show('slow');
				break;
			case 'academic_groups':
				
				break; 
		}
	});

	$(document).on('click', '#master_list_submit', function(){
		var academic_terms_id = $('#master_list_fieldset select[name="academic_term"]').val();
		var group_by = $('#master_list_fieldset select[name="group_by"]').val();

		var url = "<?php echo site_url('printer/masterlist/'); ?>";
		url += "/" + academic_terms_id + "/"+ group_by;

		console.log(url);
		window.open(url, 'printgrade', "height=300,width=400");
	});
});
</script>