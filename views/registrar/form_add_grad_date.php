<script type="text/javascript">
function hide(obj)
  {
	return false;
    var obj1 = document.getElementById(obj);
     // console.log("style: "/*+typeof(obj1.style)*/);
      if(typeof(obj1)=='object')
    	  obj1.style.display = 'none'; else
          return false;
      
  }
function show(obj)
  {
	  return false;
      var obj1 = document.getElementById(obj);
      //console.log("style: "+typeof(obj1.style));
      if(typeof(obj1)=='object')
    	  obj1.style.display = 'table';
          return false;
      
  }

function show_permission()
{
	show('permission');
}
function hide_permission()
{
	hide('permission');
}
</script>


	<body onLoad="hide_permission();">
	<h2 class="heading" style="margin-top:60px;">Set Graduation Date</h2>
	<div class="row-fluid">
		<div class="span12">
			<div class="form-horizontal">
				<form id="addTown" method="post" action="" class="form-horizontal">
					<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="step" value="2" />
				
				<div class="control-group formSep">
					<div class="control-label">Academic Term:</div>
					<div class="controls">
						<select name="acad_term_id" id="acad_term_id">
								<option value="">---Select Academic Term---</option>											
									<?php						
										foreach($academic_terms AS $academic_term) {
												echo "<option value=".$academic_term->id.">" . $academic_term->term . " ".  $academic_term->sy ."</option>";	
										}
									?>					
						</select> 
					</div>
				</div>
			
				<div class="control-group formSep">
						<label for="grad_date" class="control-label">Graduation Date:</label>
						<div class="controls">
							<div class="input-append date" id="da1" data-date-format="yyyy-mm-dd">
								<input type="text" name="grad_date" id="grad_date" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</div>
				</div>
			
			<div class="control-group formSep">
				<div class="control-label">Program</div>
				<div class="controls">
					<input type="checkbox" value="Y" name="all_program" id="selectall_program" checked> ALL Programs<br>										
					<?php 					
						foreach($colleges AS $college) {
					?>
						<fieldset>
						<input type="checkbox" value="<?php print($college->id); ?>" class="selectedId_college" checked>
						<span><a href="#" onclick="showStuff('<?php print($college->id); ?>');return false;" ><?php print($college->name." (+)"); ?></a></span>
						<br> 
						<span id="<?php print($college->id);?>" style="display: none;">
						<?php 
							$programs=$this->Programs_model->ListAcaProgramsbyCollege($college->id);
							foreach ($programs AS $program) {
						?>
							<div style="margin-left: 40px;">
							<input type="checkbox" value="<?php print($program->id);?>" class="selectedId_program" 
									name="programs[]" checked>
							<a href="#" onclick="hideStuff('<?php print($college->id); ?>');return false;">
							<?php print($program->abbreviation." (-)"); ?></a>
							</div>
					<?php 		
							}
					?>
					</span>
					</fieldset>
					<?php 
						}
					?>									
				</div>
			</div>					
			<div class="control-group formSep">
					<div class="control-label"></div>
					<div class="controls">
						 <button class="btn btn-success" type="submit">Add Graduation Date!</button> 
					</div>
			</div>
			</form>
			</div>
		</div>
	</div>
	</body>


<script type="text/javascript">
	function showStuff(id) {
		document.getElementById(id).style.display='block';
	}

	function hideStuff(id) {
		document.getElementById(id).style.display='none';
	}

</script>

<script>
	$(document).ready(function () {
	    $('#selectall_program').click(function () {
	        $('.selectedId_college').prop('checked', this.checked);
	        $('.selectedId_program').prop('checked', this.checked);
		});
	
	    $('.selectedId_college').change(function () {
	        var check = ($('.selectedId_college').filter(":checked").length == $('.selectedId_college').length);
	        $('#selectall_program').prop("checked", check);
	        $(this).closest('fieldset').find(':checkbox').prop('checked', this.checked);
	    });
	    $('.selectedId_program').change(function () {
	        var check = ($('.selectedId_program').filter(":checked").length == $('.selectedId_program').length);
	        $('#selectall_program').prop("checked", check);
	    });
});
</script>

<script>
	$(document).ready(function(){
		$('#da1').datepicker();
	
	});
</script>

<script>
	$(document).ready(function(){
		$('#da2').datepicker();
	
	});
</script>

<script>
	$(document).ready(function(){
		$('#da3').datepicker();
	
	});
</script>

<script>
$(function () {
    $('.checkall').on('click', function () {
        $(this).closest('fieldset').find(':checkbox').prop('checked', this.checked);
		show_permission();
    });
});
</script>

<script>
$('.uncheck').click(function(){
      $('.checkall').attr('checked', false);
	  hide_permission();
})
</script>





  