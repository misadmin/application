
<div id="show_detailed_fees">
	<div>
		<h3 style="color:#02650F;"><?php print($my_program['track_name']); ?></h3>
		<h3 style="color:#011B9F; font-style:italic;"><?php print($my_program['description']); ?></h3>
		<h4><?php print($my_program['sy_semester']); ?></h4>
	</div>
	
	<div style="width:100%; margin-top:10px;">	
		<div style="width:65%; overflow:auto; float:left; ">
			<table class="table table-hover table-bordered table-striped table-condensed" >
				<thead>
					<tr style="text-align:center; font-size:14px; font-weight:bold; height:40px;" class="shs_header" >
						<td class="shs_header" style="width:70%; font-size:14px; ">Item</td>
						<td class="shs_header" style="width:15%; font-size:14px; ">Grade 11</td>
						<td class="shs_header" style="width:15%; font-size:14px; ">Grade 12</td>
					</tr>
				</thead>
				<tbody>
				<?php 
					$total_g11=0; $total_g12=0;
					if ($schedule_items) {
						foreach($schedule_items AS $item) {
				?>
							<tr>
								<td colspan="3" style="font-weight:bold; color:#890404;">
									<?php print($item->fees_group); ?>
								</td>
							</tr>
							<?php 
								if (isset($item->fees_subgroups)) {
										$subtotal_g11=0;
										$subtotal_g12=0;
									foreach($item->fees_subgroups AS $subgroup) {
							?>
										<tr>
											<td style="padding-left:40px;">
												<?php print($subgroup->description); ?>
											</td>
											<td style="text-align:right;">
												<div style="float:right;">
													<div style="float:left;">
														<?php print(number_format($subgroup->gr11,2)); ?>
													</div>
													<?php 
														$subtotal_g11 = $subtotal_g11 + $subgroup->gr11;
														if ($can_update AND ($subgroup->gr11 > 0)) {
													?>
														<div style="float:left; padding-left:5px;">
															<a href="<?php print($subgroup->fees_schedule_id11); ?>" 
																track_name="<?php print($my_program['track_name']); ?>"
																description="<?php print($my_program['description']); ?>"
																sy_semester="<?php print($my_program['sy_semester']); ?>"
																fee_description="<?php print($subgroup->description); ?>"
																grade_level="Grade 11"
																rate="<?php print($subgroup->gr11); ?>"
																class="update_fee_schedule" >
																<div id="show_edit_icon_<?php print($subgroup->fees_schedule_id11); ?>" >
																	<i class="icon-edit"></i>
																</div>
															</a>
														</div>
														<div style="float:left; padding-left:5px;">
															<a href="<?php print($subgroup->fees_schedule_id11); ?>" 
																can_update="<?php print($can_update); ?>"
																class="delete_fee_sched" >
																<div id="show_trash_icon_<?php print($subgroup->fees_schedule_id11); ?>" >
																	<i class="icon-trash"></i>
																</div>
															</a>
														</div>
													<?php
														}
													?>
												</div>		
											</td>
											<td style="text-align:right;">
												<div style="float:right;">
													<div style="float:left;">
														<?php print(number_format($subgroup->gr12,2)); ?>
													</div>
													<?php 
														$subtotal_g12 = $subtotal_g12 + $subgroup->gr12;
														if ($can_update AND ($subgroup->gr12 > 0)) {
													?>
														<div style="float:left; padding-left:5px;">
															<a href="<?php print($subgroup->fees_schedule_id12); ?>" 
																track_name="<?php print($my_program['track_name']); ?>"
																description="<?php print($my_program['description']); ?>"
																sy_semester="<?php print($my_program['sy_semester']); ?>"
																fee_description="<?php print($subgroup->description); ?>"
																grade_level="Grade 12"
																rate="<?php print($subgroup->gr12); ?>"
																can_update="<?php print($can_update); ?>"
																class="update_fee_schedule" >
																<div id="show_edit_icon_<?php print($subgroup->fees_schedule_id12); ?>" >
																	<i class="icon-edit"></i>
																</div>
															</a>
														</div>
														<div style="float:left; padding-left:5px;">
															<a href="<?php print($subgroup->fees_schedule_id12); ?>" 
																can_update="<?php print($can_update); ?>"
																class="delete_fee_sched" >
																<div id="show_trash_icon_<?php print($subgroup->fees_schedule_id12); ?>" >
																	<i class="icon-trash"></i>
																</div>
															</a>
														</div>
													<?php
														}
													?>
												</div>	
											</td>
										</tr>
							<?php
									}
								}
								
								if ($item->fees_groups_id != 9) {
							?>
									<tr>
										<td style="font-weight:bold; text-align:right;">
											Total for <?php print($item->fees_group); ?>:
										</td>
										<td style="font-weight:bold; text-align:right; border-top:solid 1px;  border-bottom:solid 1px;">
											<?php print("P ".number_format($subtotal_g11,2)); $total_g11= $total_g11 + $subtotal_g11; ?>
										</td>
										<td style="font-weight:bold; text-align:right; border-top:solid 1px;  border-bottom:solid 1px;">
											<?php print("P ".number_format($subtotal_g12,2)); $total_g12= $total_g12 + $subtotal_g12; ?>
										</td>								
									</tr>
				<?php
								}
						}
					}
				?>
					<tr>
						<td style="font-weight:bold; text-align:right;">
							TOTAL:
						</td>
						<td style="font-weight:bold; text-align:right; border-top:solid 1px;  border-bottom:solid 1px;">
							<?php print("P ".number_format($total_g11,2)); ?>
						</td>
						<td style="font-weight:bold; text-align:right; border-top:solid 1px;  border-bottom:solid 1px;">
							<?php print("P ".number_format($total_g12,2)); ?>
						</td>								
					</tr>
				</tbody>
			</table>
		</div>

		
		<div style="float:right; width:30%; ">
			<?php 
				$data['lab_items'] = $lab_items;
				$data['can_update'] = $can_update;
				$this->load->view('shs/fees_schedule/lab_fees_details', $data);
			?>		
		</div>

	</div>
</div>	


	
<script>
	$('.delete_fee_sched').click(function(event){
		event.preventDefault(); 
		$('#show_error_msg').html('');

		var confirmed = confirm('Are you sure you want to delete this fee?');

		if (confirmed){
			
			var fees_schedule_id       = $(this).attr('href');
			var can_update             = $(this).attr('can_update');

			var element1               = $("option:selected", "#academic_terms_id");
			var academic_terms_id      = element1.val();
			var sy                     = element1.attr('sy');
			var academic_years_id      = element1.attr('academic_years_id');
			var element2               = $("option:selected", "#academic_programs_id");
			var academic_programs_id   = element2.val();
			var acad_program_groups_id = element2.attr('acad_program_groups_id');
			var description            = element2.attr('description');
			var group_name             = element2.attr('group_name');

			$('#show_trash_icon_'+fees_schedule_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/schedule_for_shs');?>',
				data: {"academic_terms_id":academic_terms_id,
						"academic_years_id": academic_years_id,
						"academic_programs_id": academic_programs_id,
						"acad_program_groups_id": acad_program_groups_id,
						"fees_schedule_id": fees_schedule_id,
						"can_update": can_update,
						"sy": sy,
						"description": description,
						"group_name": group_name,
						"step": 'delete_fee_schedule'},
				dataType: 'json',
				success: function(response) {

					$('#show_detailed_fees').html(response.output); 	

					$('#show_trash_icon_'+fees_schedule_id).html("<i class='icon-trash'></i>")
				}
			});
			
		}

	});
</script>



<script>
	$('.update_fee_schedule').click(function(event){
		event.preventDefault();
		
		$('#modal_update_fee_schedule').modal('show');
		
		$('#fees_schedule_id_modal').val($(this).attr('href'));
		$('#track_name_fee_sched_modal').html($(this).attr('track_name'));
		$('#description_fee_sched_modal').html($(this).attr('description'));
		$('#sy_semester_fee_sched_modal').html($(this).attr('sy_semester'));
		$('#fee_description_modal').val($(this).attr('fee_description'));
		$('#grade_level_modal').val($(this).attr('grade_level'));
		$('#rate_fee_sched_modal').val($(this).attr('rate'));
		$('#can_update_fee_sched_modal').val($(this).attr('can_update'));
		
	});
</script>				


	<div id="modal_update_fee_schedule" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_fee_schedule_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Fee Schedule</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="fees_schedule_id_modal"></div>
						<div id="can_update_fee_sched_modal"></div>
						<?php														
							$this->load->view('shs/fees_schedule/modal_view/update_fee_schedule_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#update_fee_schedule_form').submit(function(e) {
		e.preventDefault();
		
		var fees_schedule_id  = $('#fees_schedule_id_modal').val();
		var rate              = $('#rate_fee_sched_modal').val();
		var can_update        = $('#can_update_fee_sched_modal').val();

		var element1               = $("option:selected", "#academic_terms_id");
		var academic_terms_id      = element1.val();
		var sy                     = element1.attr('sy');
		var academic_years_id      = element1.attr('academic_years_id');
		var element2               = $("option:selected", "#academic_programs_id");
		var academic_programs_id   = element2.val();
		var acad_program_groups_id = element2.attr('acad_program_groups_id');
		var description            = element2.attr('description');
		var group_name             = element2.attr('group_name');
		
		$('#modal_update_fee_schedule').modal('hide');
		
		$('#show_edit_icon_'+fees_schedule_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/schedule_for_shs'); ?>",
			data: {"fees_schedule_id": fees_schedule_id,
					"rate": rate,
					"can_update": can_update,
					"sy": sy,
					"description": description,
					"group_name": group_name,
					"academic_terms_id": academic_terms_id,
					"academic_years_id": academic_years_id,
					"academic_programs_id": academic_programs_id,
					"acad_program_groups_id": acad_program_groups_id,
					"step": "update_fee_schedule" },
			dataType: 'json',
			success: function(response) {													

				$('#show_detailed_fees').html(response.output); 
				
				$('#show_edit_icon_'+fees_schedule_id).html("<i class='icon-edit'></i>")
				
			}
		});
		
	});
</script>				
	