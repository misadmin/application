<?php
	if (!$withdraw_per) {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td style="text-align:center; font-weight:bold;">CANNOT WITHDRAW, NO WITHDRAWAL SCHEDULE YET!</td>
  </tr>
</table>
<?php
	} else {
?>
<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function show_permission()
{
	show('permission');
}
function hide_permission()
{
	hide('permission');
}
</script>

<body onLoad="hide_permission();">
<div style="width:100%;">
	<span style="color:red"><b>NOTE:</b> <i>For Total Withdrawal, please tick the topmost check box</i></span>
</div>
<div class="clearfix">
<form method="post" name="wdraw" id="withdraw_form">
<?php echo $this->common->hidden_input_nonce(); ?>
<input type="hidden" name="action" value="withdraw_enrolled_courses" />
<input type="hidden" name="what_period" value="<?php print($what_period); ?>" />
<input type="hidden" name="history_id" value="<?php print($history_id) ?>" />
<fieldset>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
			  <th width="3%" style="vertical-align:middle; text-align:center;">
			  	<input type="checkbox" name="TotalWithdraw" value="Y" class="checkall" /></th>
				<th width="9%" style="vertical-align:middle; text-align:center;">Catalog No. (Section)</th>
				<th width="22%" style="vertical-align:middle; text-align:center;">Descriptive Title</th>
				<th width="23%" style="vertical-align:middle; text-align:center;">Schedule</th>
				<th width="15%" style="vertical-align:middle; text-align:center;">Teacher</th>
				<th width="5%" style="vertical-align:middle; text-align:center;">Units</th>
				<th width="7%" style="vertical-align:middle; text-align:center;">Prelim Grade</th>
				<th width="9%" style="vertical-align:middle; text-align:center;">Midterm Grade</th>
				<th width="7%" style="vertical-align:middle; text-align:center;">Final Grade</th>
			</tr>
		</thead>
	
<?php
 
	$count = 0;

	 if(is_array($schedules) && count($schedules) > 0) {

		foreach($schedules as $schedule): 
			//if(($schedule->prelim_grade != 'WD') AND ($schedule->midterm_grade != 'WD') AND ($schedule->finals_grade !=	'WD')){
 
			$count = $count + 1;
?>
			<tr>
				<td align="center"><input type="checkbox" name="<?php print("course[$schedule->id]"); ?>" value="Y" class="uncheck" /></td>
				<td><?php print($schedule->course_code); ?></td>
				<td><?php echo $schedule->descriptive_title; ?></td>
				<td>
<?php
			$my_offering_slots = $this->Offerings_Model->ListOfferingSlots($schedule->course_offerings_id);
			
			foreach($my_offering_slots[0] AS $my_slots) {
				print($my_slots->tym." ".$my_slots->days_day_code." | <span style=\"color:green; font-weight:bold;\" >Rm.".$my_slots->room_no."/".
						$my_slots->bldg_name."</span> <br>");  
			}
?>
			    </td>
				<td><?php echo $schedule->emp_name; ?></td>
				<td><?php echo $schedule->credit_units; ?></td>
				<td><?php echo $schedule->prelim_grade; ?></td>
				<td><?php echo $schedule->midterm_grade; ?></td>
				<td><?php echo $schedule->finals_grade; ?></td>
			</tr>
<?php 
			//} 
		endforeach; 
?>
	<?php if($count > 0) { ?>
		<tr>
       	 	 <td colspan="9" valign="middle">
       	 	   <table width="100%" border="0" cellspacing="0" cellpadding="0" id="permission">
                 <tr>
                   <td width="20%">Permission Type </td>
                   <td width="2%">:</td>
                   <td width="78%"><select name="withdraw_stat">
                     <option value="MED" selected="selected">With Medical Certificate</option>
               		 <option value="WP">Total Withdraw</option>
                   </select>                   </td>
                 </tr>
               </table>
       	 	   <p>
       	 	     <input type="submit" name="totWithdraw" id="totWithdraw" value="Withdraw!"  class="btn 
			 	btn-warning" />       	 	 
   	 	       </p>
       	 	   </td>
        </tr>
				<?php } else { ?>
				<tr>
				  <td colspan="9"> No Courses found for this term</td>
			   </tr>
				<?php }	?>
				

	<?php } else { ?>
				<tr>
				  <td colspan="9">No Courses found for this term</td>
			   </tr>
				<?php }	?>
	</table>
</fieldset>	
</form>

</div>
</body>
<?php } ?>

<script>
$(function () {
    $('.checkall').on('click', function () {
        $(this).closest('fieldset').find(':checkbox').prop('checked', this.checked);
		show_permission();
    });
});
</script>

<script>
$('.uncheck').click(function(){
      $('.checkall').attr('checked', false);
	  hide_permission();
})
</script>

<script>
	$('#totWithdraw').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm("Continue to withdraw/remove course/s?");

	if (confirmed){
	   	$('#withdraw_form').submit();
	}		
});
</script>

