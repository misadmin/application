
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Course</div>
			<div class="col2a">
				<select class="form-control" style="width:150px; height:auto;" id="course_id_add_modal" >
					<?php 
						if ($shs_courses) {
							foreach ($shs_courses AS $course) {
					?>		
								<option value="<?php print($course->id); ?>" ><?php print($course->descriptive_title); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Course Type</div>
			<div class="col2a">
				<select class="form-control" style="width:150px; height:auto;" id="stype_add_modal" >
					<?php 
						if ($s_types) {
							foreach ($s_types AS $stype) {
					?>		
								<option value="<?php print($stype->id); ?>" ><?php print($stype->description); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>
