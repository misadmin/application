
		<select name="town_id" id="town_id" class="form-control" style="width:auto;">
			<option value="">--Please Select--</option>
		<?php
			foreach ($towns AS $town) {
				print("<option value=".$town->id.">".$town->name."</option>");
			}
		?>
		</select>


	<script>

		$("select#town_id").change(function(){

			var town_id = $("select#town_id option:selected").attr('value'); 
			var type    = '<?php print($type); ?>';

			$('#brgy_group').show();
			$("#brgy_group").html( "" );
			$('#register_student').prop("disabled", true);
			
			if (town_id.length > 0 ) { 
					
				$.ajax({
					type: "POST",
					url: "<?php echo site_url($this->uri->segment(1).'/extract_places');?>",
					data: {
							"action": 'list_brgys',
							"type": type,
							"town_id":town_id
							},
					cache: false,
					beforeSend: function () { 
						$('#brgy_group').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");
					},
					success: function(html) {    
						$("#brgy_group").html( html );
						$("#street_name").focus();
						
						if (required_has_value()) {
							$('#register_student').prop("disabled", false);
						} else {
							$('#register_student').prop("disabled", true);
						}
						
					}
				});
			}
		});
		
		function isEmpty(v) {
			if (v === '' || v === null) {
				return true;
			} else {
				return false;
			}
		}
		
		
		function required_has_value() {
			
			var lname			= $('#lname').val();
			var fname			= $('#fname').val();
			var mname			= $('#mname').val();
			var dbirth			= $('#dbirth').val();
			var gender			= $("select#gender option:selected").attr('value');
			var level_id		= $("select#level_id option:selected").attr('value');
			var year_level_id	= $("select#academic_programs_id option:selected").attr('value');
			var emergency_lname		= $('#emergency_lname').val();
			var emergency_fname		= $('#emergency_fname').val();
			var emergency_contact	= $('#emergency_contact').val();
			var town_id				= $("select#town_id option:selected").attr('value');

			if (level_id != 0 && year_level_id != 0 && town_id.length > 0) {
				if (isEmpty(lname) || isEmpty(fname) || isEmpty(mname) || isEmpty(dbirth) || isEmpty(emergency_lname) || isEmpty(emergency_fname) || isEmpty(emergency_contact)) {
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
				
		}
		

	</script>
		