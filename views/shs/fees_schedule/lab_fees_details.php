

	<div style="width:100%; float:left; overflow:auto; height:280px;">
		<table class="table table-bordered table-striped table-hover" >
			<thead>
				<tr style="background-color: #C0BFBF; font-weight: bold;">
					<td style="width:60%; text-align:center;">
						LABORATORY FEES
					</td>
					<td style="width:40%; text-align:center;">
						RATE
					</td>
					<?php 
						if ($can_update) {
					?>
							<td style="text-align:center;">
								Edit
							</td>
							<td style="text-align:center;">
								Del.
							</td>				
					<?php 
						}
					?>
				</tr>
			</thead>
			
			<?php 
				if ($lab_items) {	
					foreach ($lab_items AS $lab_item) {
			?>
						<tr>
							<td style='padding-left: 30px;'><?php print($lab_item->course_code); ?></td>
							<td style='text-align: right;'><?php print($lab_item->rate); ?></td>
					<?php
						if ($can_update) {					
					?>		
							<td style="text-align: center;">
								<a href="<?php print($lab_item->laboratory_fees_id); ?>" 
									track_name="<?php print($my_program['track_name']); ?>"
									description="<?php print($my_program['description']); ?>"
									sy_semester="<?php print($my_program['sy_semester']); ?>"
									course_code="<?php print($lab_item->course_code); ?>"
									rate="<?php print($lab_item->rate); ?>"
									can_update="<?php print($can_update); ?>"
									class="update_lab_fee" >
									<div id="show_edit_icon_<?php print($lab_item->laboratory_fees_id); ?>" >
										<i class="icon-edit"></i>
									</div>
								</a>
							</td>	 		
							<td style="text-align: center;">
								<a href="<?php print($lab_item->laboratory_fees_id); ?>" 
									can_update="<?php print($can_update); ?>"
									class="delete_lab_fee" >
									<div id="show_trash_icon_<?php print($lab_item->laboratory_fees_id); ?>" >
										<i class="icon-trash"></i>
									</div>
								</a>
							</td>
					<?php 
						}
					?>
						</tr>			
			
			<?php
					}
				}
			?>

		</table>
	</div>		


<script>
	$('.delete_lab_fee').click(function(event){
		event.preventDefault(); 
		$('#show_error_msg').html('');

		var confirmed = confirm('Are you sure you want to delete this Laboratory fee?');

		if (confirmed){
			
			var lab_fee_id             = $(this).attr('href');
			var can_update             = $(this).attr('can_update');

			var element1               = $("option:selected", "#academic_terms_id");
			var academic_terms_id      = element1.val();
			var sy                     = element1.attr('sy');
			var academic_years_id      = element1.attr('academic_years_id');
			var element2               = $("option:selected", "#academic_programs_id");
			var academic_programs_id   = element2.val();
			var acad_program_groups_id = element2.attr('acad_program_groups_id');
			var description            = element2.attr('description');
			var group_name             = element2.attr('group_name');

			$('#show_trash_icon_'+lab_fee_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/schedule_for_shs');?>',
				data: {"academic_terms_id":academic_terms_id,
						"academic_years_id": academic_years_id,
						"academic_programs_id": academic_programs_id,
						"acad_program_groups_id": acad_program_groups_id,
						"lab_fee_id": lab_fee_id,
						"can_update": can_update,
						"sy": sy,
						"description": description,
						"group_name": group_name,
						"step": 'delete_lab_fee'},
				dataType: 'json',
				success: function(response) {

					$('#show_detailed_fees').html(response.output); 	

					$('#show_trash_icon_'+lab_fee_id).html("<i class='icon-trash'></i>")
				}
			});
			
		}

	});
</script>


<script>
	$('.update_lab_fee').click(function(event){
		event.preventDefault();
		
		$('#modal_update_lab_fee').modal('show');
		
		$('#lab_fee_modal').val($(this).attr('href'));
		$('#track_name_modal').html($(this).attr('track_name'));
		$('#description_modal').html($(this).attr('description'));
		$('#sy_semester_modal').html($(this).attr('sy_semester'));
		$('#course_code_modal').val($(this).attr('course_code'));
		$('#rate_modal').val($(this).attr('rate'));
		$('#can_update_modal').val($(this).attr('can_update'));
		
	});
</script>				


	<div id="modal_update_lab_fee" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_lab_fee_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Laboratory Fee</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="lab_fee_modal"></div>
						<div id="can_update_modal"></div>
						<?php														
							$this->load->view('shs/fees_schedule/modal_view/update_lab_fee_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#update_lab_fee_form').submit(function(e) {
		e.preventDefault();
		
		var lab_fee_id  = $('#lab_fee_modal').val();
		var rate        = $('#rate_modal').val();
		var can_update  = $('#can_update_modal').val();;

		var element1               = $("option:selected", "#academic_terms_id");
		var academic_terms_id      = element1.val();
		var sy                     = element1.attr('sy');
		var academic_years_id      = element1.attr('academic_years_id');
		var element2               = $("option:selected", "#academic_programs_id");
		var academic_programs_id   = element2.val();
		var acad_program_groups_id = element2.attr('acad_program_groups_id');
		var description            = element2.attr('description');
		var group_name             = element2.attr('group_name');
		
		$('#modal_update_lab_fee').modal('hide');
		
		$('#show_edit_icon_'+lab_fee_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/schedule_for_shs'); ?>",
			data: {"lab_fee_id": lab_fee_id,
					"rate": rate,
					"can_update": can_update,
					"sy": sy,
					"description": description,
					"group_name": group_name,
					"academic_terms_id": academic_terms_id,
					"academic_years_id": academic_years_id,
					"academic_programs_id": academic_programs_id,
					"acad_program_groups_id": acad_program_groups_id,
					"step": "update_lab_fee" },
			dataType: 'json',
			success: function(response) {													

				$('#show_detailed_fees').html(response.output); 
				
				$('#show_edit_icon_'+lab_fee_id).html("<i class='icon-edit'></i>")
				
			}
		});
		
	});
</script>				

