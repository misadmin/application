<?php

	class Matriculation_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function AddCollege_Matriculation($data) 
		{
			foreach($data['academic_programs'] as $program)
			{
				if($program->status == 'OFFERED'){ 
					$query = "INSERT INTO matriculation_fees (id, academic_programs_id, academic_years_id, rate, yr_level) 
   							      VALUES ('',
										  ".$program->id.",
										  ".$data['academic_years_id'].",
										  {$this->db->escape($data['rate'])},
										  {$this->db->escape($data['yr_level'])})";
					$this->db->query($query);
				}						
			}						
			
			$id = @mysql_insert_id();
			
			return $id;
			
		}
				
		
		function AddGraduate_Matriculation($data) 
		{
			$query = "INSERT INTO matriculation_fees (id, academic_programs_id, academic_years_id, rate, yr_level) 
   				      VALUES ('',
						  	  {$this->db->escape($data['programs_id'])},
					  	      {$this->db->escape($data['academic_years_id'])},
						      {$this->db->escape($data['rate'])},
						      {$this->db->escape($data['yr_level'])})";
				
			$this->db->query($query);					
				
			$id = @mysql_insert_id();
			
			return $id;
		}
		
}

//end of college_model.php