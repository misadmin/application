<?php 
	$prospectusterms_pdf = json_encode($prospectusterms);
?>
<h2 class="heading">List of Prospectus</h2>

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding:3px; margin-bottom:20px;">
	
	<form id="change_data" method="POST">
	<table style="width:100%;" >
	<tr>
	<td style="text-align:left; font-weight:bold; ">
		<a class="download_prospectus" href=""
			prospectusterms='<?php print($prospectusterms_pdf); ?>'
			prospectus_id = "<?php print($selected_prospectus); ?>" ><i class="icon-download-alt"></i>Download to PDF</a> 
	</td>
	<td style="text-align:right; vertical-align:middle;">
			<select name="prospectus_id" class="change_prospectus" >
				<?php
					foreach($prospectuses AS $prospectus) {
				?>
						<option value="<?php print($prospectus->id); ?>" <?php if ($prospectus->id == $selected_prospectus) { print("selected"); } ?>>
								<?php print($prospectus->abbreviation.' - '.$prospectus->effective_year); ?> </option>	
				<?php 
					}
				?>					
			</select>
			<select name="college_id" class="change_college" style="width:auto;">
				<?php
					foreach($colleges AS $college) {
				?>
						<option value="<?php print($college->id); ?>" <?php if ($college->id == $selected_college) { print("selected"); } ?>>
								<?php print($college->college_code); ?> </option>	
				<?php 
					}
				?>					
			</select>
							
	</td>
	</tr>
	</table>						
	</form>
	
</div>	

<div style="padding-left:20px;">
	<?php 
		if ($prospectusterms) {
			$pros_term = 0;
			$max_bracket = "";
			foreach($prospectusterms AS $pterm) {
				if ($pros_term != $pterm->prospectus_terms_id) {
					
					if ($max_bracket) {
	?>
						<tr>
							<td colspan="2" style="text-align:right; font-weight:bold;background-color: #B0F7B7;">TOTAL:</td>
							<td style="text-align:center; font-weight:bold;background-color: #B0F7B7;"><?php print($max_bracket); ?></td>
							<td style="text-align:center;background-color: #B0F7B7;"></td>
							<td style="text-align:center;background-color: #B0F7B7;"></td>
							<td style="text-align:center;background-color: #B0F7B7;"></td>
						</tr>	
					</table>
	<?php 
					} 
					$pros_term = $pterm->prospectus_terms_id;
	?>
					<table class="table table-condensed table-bordered" style="width:75%;">
						<thead>
							<tr>
								<th colspan="6" style="font-weight:bold; font-size:14px;"><?php print($pterm->term.' '.$pterm->y_level); ?></th>
							</tr>
							<tr>
								<th style="text-align:center; vertical-align:middle; width:15%;">Course Code</th>
								<th style="text-align:center; vertical-align:middle; width:42%;">Descriptive Title</th>
								<th style="text-align:center; vertical-align:middle; width:7%;">Credit<br>Units</th>
								<th style="text-align:center; vertical-align:middle; width:7%;">Cut-off<br>Grade</th>
								<th style="text-align:center; vertical-align:middle; width:8%;">No. of<br>Retakes</th>
								<th style="text-align:center; vertical-align:middle; width:21%;">Pre-requisites</th>
							</tr>
						</thead>
						<tr>
							<td><?php print($pterm->course_code); ?></td>
							<td><?php print($pterm->descriptive_title); ?></td>
							<td style="text-align:center;"><?php print($pterm->credit_units); ?></td>
							<td style="text-align:center;"><?php print($pterm->cutoff_grade); ?></td>
							<td style="text-align:center;"><?php print($pterm->num_retakes); ?></td>
							<td><?php print($pterm->pre_req); ?></td>
						</tr>
				<?php 
				} else {
						$max_bracket = $pterm->max_credit_units.'/'.$pterm->max_bracket_units;
				?>		
						<tr>
							<td><?php print($pterm->course_code); ?></td>
							<td><?php print($pterm->descriptive_title); ?></td>
							<td style="text-align:center;"><?php print($pterm->credit_units); ?></td>
							<td style="text-align:center;"><?php print($pterm->cutoff_grade); ?></td>
							<td style="text-align:center;"><?php print($pterm->num_retakes); ?></td>
							<td><?php print($pterm->pre_req); ?></td>
						</tr>
				<?php 
				}
			}
	?>
						<tr>
							<td colspan="2" style="text-align:right; font-weight:bold;background-color: #B0F7B7;">TOTAL:</td>
							<td style="text-align:center; font-weight:bold;background-color: #B0F7B7;"><?php print($max_bracket); ?></td>
							<td style="text-align:center;background-color: #B0F7B7;"></td>
							<td style="text-align:center;background-color: #B0F7B7;"></td>
							<td style="text-align:center;background-color: #B0F7B7;"></td>
						</tr>	
				</table>
	
	<?php 		
		}
	?>

</div>

<script>
	$(document).ready(function(){
		$('.change_prospectus').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'display_selected_program',
			}).appendTo('#change_data');

			$('<input>').attr({
				type: 'hidden',
				name: 'type',
				value: 'prospectus',
			}).appendTo('#change_data');
			
			$('#change_data').submit();
		});
	});
</script> 


<script>
	$(document).ready(function(){
		$('.change_college').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'display_selected_program',
			}).appendTo('#change_data');

			$('<input>').attr({
				type: 'hidden',
				name: 'type',
				value: 'college',
			}).appendTo('#change_data');
			
			$('#change_data').submit();
		});
	});
</script> 


  <form id="download_prospectus_to_pdf" method="post" target="_blank">
  <input type="hidden" name="action" value="download_to_pdf" />
</form>
<script>
	$(document).ready(function(){
  
		$('.download_prospectus').click(function(event){
			event.preventDefault(); 
			var prospectusterms = $(this).attr('prospectusterms');
			var prospectus_id = $(this).attr('prospectus_id');
			
			$('<input>').attr({
				type: 'hidden',
				name: 'prospectusterms',
				value: prospectusterms,
			}).appendTo('#download_prospectus_to_pdf');

			$('<input>').attr({
				type: 'hidden',
				name: 'prospectus_id',
				value: prospectus_id,
			}).appendTo('#download_prospectus_to_pdf');

			$('#download_prospectus_to_pdf').submit();
			
		});

		
	});
</script>

