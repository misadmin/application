
	<h2 class="heading">List of Unposted Assessments - Senior High School</h2>

	<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 35px; margin-bottom:10px; padding:2px;">
			<div style="float:right; overflow:auto;">
				<div id="show_change_data_icon" style="float:left; padding-right:8px; margin-top:7px;">
				</div>
				<div style="float:left; padding-top:8px; padding-right:10px;" class="b2">
					<a href="#" style="text-decoration: none; color:#4E4D4D; font-size:12px; "						
						class="download_unposted_assessments_pdf" >
						<div style="float:right; padding-left:3px; padding-top:2px;">Download to PDF</div>
						<div id="excel_labels" style="float:right;">
							<img src="<?php print(base_url('assets/images/pdf_icon.png')); ?>" style="height:20px;" /> 
						</div>
					</a>			
				</div>
			</div>
	</div>
	
			<div style="width:80%; overflow:auto; margin-top:15px; ">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:5%;">Count</td>
								<td class="shs_header" style="width:10%;">ID<br>No.</td>
								<td class="shs_header" style="width:20%;">Lastname</td>
								<td class="shs_header" style="width:20%;">Firstname</td>
								<td class="shs_header" style="width:20%;">Middlename</td>
								<td class="shs_header" style="width:20%;">Strand</td>
								<td class="shs_header" style="width:5%;">Grade<br>Level</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($students) {
									$cnt=1;
									foreach($students  AS $student) {
							?>			
										<tr>
											<td style="text-align:right;"><?php print($cnt); ?>.</td>
											<td style="text-align:center;"><?php print($student->idno); ?></td>
											<td><?php print($student->lname); ?></td>
											<td><?php print($student->fname); ?></td>
											<td><?php print($student->mname); ?></td>
											<td><?php print($student->abbreviation); ?></td>
											<td style="text-align:center;"><?php print($student->year_level); ?></td>
										</tr>
							<?php
										$cnt++;
									}
								}
							?>
						</tbody>
					</table>
			</div>

			
			
<form id="download_unposted_assessments_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_unposted_assessments_pdf" />
</form>

<script>
	$('.download_unposted_assessments_pdf').click(function(event){
		event.preventDefault(); 
		var students = '<?php print(json_encode($students)); ?>';

		$('<input>').attr({
			type: 'hidden',
			name: 'students',
			value: students,
		}).appendTo('#download_unposted_assessments_form');

		$('#download_unposted_assessments_form').submit();
		
	});
</script>
			