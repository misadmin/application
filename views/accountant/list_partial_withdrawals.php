 
<h2 class="heading"> LIST OF STUDENTS WHO WITHDREW AT LEAST 1 COURSE (as of <?php echo date('M-d-Y'); ?> )</h2>	

<table class="table table-striped table-bordered">
	<?php if ($partial_withdrawals) {
				$num = 1;		
			 foreach($partial_withdrawals as $pw) {  ?>
					<tr align="center">
							<th colspan="5" class="head" style="text-align:left; padding:10px;">  <?php print($num.'. '.$pw->idno.' '.$pw->name.' '.$pw->abbreviation.' '.$pw->year_level); ?> </th>
					</tr>
				<?php  $courses = $this->enrollments_model->listCoursesWithdrawn($pw->history_id);
					if ($courses) { ?>
						<?php foreach($courses as $co) { ?>
								<tr>
									<td style="text-align:right"><?php echo $co->course_code; ?></td>
									<td><?php echo $co->withdrawn_on; ?></td>
								</tr>
						<?php }
					}
					$num++;
			
			 } 
		}?>
</table>