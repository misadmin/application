<h3 class="heading"><?php echo $title; ?></h3>
<div class="row-fluid">	  

	<?php if (isset($AlertMsg)): ?>
		<div class="alert <?php if ($AlertType == 'Success') { echo "alert-success"; } else { echo "alert-error"; }?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
				 <?php echo $AlertMsg; ?>
		</div>
	<?php endif; ?>

</div>
