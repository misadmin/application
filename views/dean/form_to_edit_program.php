<body onLoad="document.create.abbreviations.focus()">

<form action="<?php echo site_url('dean/academics');?>" method="post" name="create">
 <input type="hidden" name="step" value= "3"/>
 <input type="hidden" name="acad_program_id" value= "<?php print($acad_program_id);?>"/>
<?php $this->common->hidden_input_nonce(FALSE); ?>
  <?php //echo validation_errors(); ?>
   
  <table style="width:50%; margin-left:20px; border:solid; border-color:#999; border-width:1px; vertical-align:middle;">
<tr>
  <td colspan="3" style="background:#093; padding:8px; color:#CCC; font-weight:bold; font-size:16px;">Edit Program</td>
  </tr>
  <?php
  	if ($this->input->post('departments_id')) {
  ?>
<tr>
  <td style="padding-top:10px; padding-left:10px; vertical-align:top; ">Department</td>
  <td style="padding-top:10px; vertical-align:top; ">:</td>
  <td style="padding-top:10px; vertical-align:top; "><?php if($department_id){
  															 print($department_name);
															}?></td>
</tr>
<?php 
	}
?>
<tr>
        	<td style="padding-top:10px; padding-left:10px; vertical-align:top; ">Abbreviation</td>
        	<td style="padding-top:10px; vertical-align:top; ">:</td>
            <td style="padding-top:10px; vertical-align:top; "><input name="abbreviations" type="text" class="form1" id="abbreviations" 
			          value = "<?php print($abbreviation);?>" style="width:50px;"/></td>
    </tr>
    	<tr>
    	  <td style="vertical-align:top; padding-left:10px; ">Description</td>
    	  <td style="vertical-align:top; ">:</td>
    	  <td style="vertical-align:top; "><input type="text" name="description" id="description" value = "<?php print($description);?>"
		  			class="form1" style = "width: 500px;"/></td>
      </tr>
    	<tr>
    	  <td style="vertical-align:top; padding-left:10px; ">Group</td>
    	  <td style="vertical-align:top; ">:</td>
    	  <td style="vertical-align:top; ">
          <select name="acad_program_groups_id" id="acad_program_groups_id" "class="form">
    	    <?php
				foreach($groups AS $group) {
					if ($group->id == $group_id) { //'Same as Program' is the default
						print("<option value=".$group->id." selected>".$group->group_name."</option>");	
					} else {
						print("<option value=".$group->id.">".$group->group_name."</option>");
					}
				}
			?>
  	    </select></td>
  	  </tr>
    	<tr>
        	<td align="left" valign="middle">&nbsp;</td>
        	<td align="left" valign="middle">&nbsp;</td>
        	<td align="left" valign="middle"><input type="submit" value="Edit!" /></td>
        </tr>
  </table>
</form>
