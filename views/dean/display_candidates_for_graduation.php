<style>
th.header {
    text-align:center; 
    vertical-align:middle; 
    height: 35px;
}

</style>
<?php 
	$stud_pdf = json_encode($students);
?>
<h2 class="heading">Candidates for Graduation - <?php print($show_current_term); ?></h2>

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding:3px; margin-bottom:20px;">
	
	<form id="change_data" method="POST">
	<table style="width:100%;" >
	<tr>
	<td style="text-align:left; font-weight:bold; ">
		<a class="download_candidates" href="" 
			students='<?php print($stud_pdf); ?>' 
			selected_term="<?php print($show_current_term); ?>" ><i class="icon-download-alt"></i>Download to PDF</a> 
	</td>
	<td style="text-align:right; vertical-align:middle;">
			<select name="academic_terms_id" class="go_change">
					<?php 
						foreach($terms AS $my_term) {
					?>
							<option value="<?php print($my_term->id); ?>" <?php if ($my_term->id == $selected_term) { print("selected"); } ?>>
								<?php print($my_term->term.' '.$my_term->sy); ?> </option>
					<?php 	
						}
					?>
			</select>
			<select name="academic_programs_id" class="go_change">
						<option value="0">All Programs</option>
					<?php 
						foreach($programs AS $my_program) {
					?>
							<option value="<?php print($my_program->academic_programs_id); ?>" <?php if ($my_program->academic_programs_id == $selected_program) { print("selected"); } ?>>
								<?php print($my_program->abbreviation); ?> </option>
					<?php 	
						}
					?>
			</select>

	</td>
	</tr>
	</table>						
	</form>
	
</div>	

<div style="padding-left:20px;">
	<table class="table table-bordered table-hover table-striped" style="width:70%;">
		<thead style="font-weight: bold;">
			<th style="width:5%;" class="header" style="text-align:right;" >Count</th>
			<th style="width:10%;" class="header" >ID No.</th>
			<th style="width:58%;" class="header" >Name</th>
			<th style="width:10%;" class="header" >Gender</th>
			<th style="width:17%;" class="header" >Program</th>
		</thead>
		<?php
			$cnt=0;
			if ($students) {
				foreach ($students AS $student) {
					$cnt++;
		?>
			<tr>
				<td style="text-align:right;"><?php print($cnt); ?>.</td>
				<td style="text-align:center;"><?php print($student->idno); ?></td>
				<td><?php print($student->neym); ?></td>
				<td style="text-align:center;"><?php print($student->gender); ?></td>
				<td style="text-align:center;"><?php print($student->abbreviation); ?></td>
			</tr>
		<?php 		
				} 
			}
		?>

	</table>
</div>

<script>
	$(document).ready(function(){
		$('.go_change').bind('change', function(){
			show_loading_msg();
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'display_selected',
			}).appendTo('#change_data');
			$('#change_data').submit();
		});
	});
</script>


  <form id="download_candidates_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_to_pdf" />
</form>
<script>
	$(document).ready(function(){
  
		$('.download_candidates').click(function(event){
			event.preventDefault(); 
			var students = $(this).attr('students');
			var selected_term = $(this).attr('selected_term');
	
			$('<input>').attr({
				type: 'hidden',
				name: 'students',
				value: students,
			}).appendTo('#download_candidates_form');

			$('<input>').attr({
				type: 'hidden',
				name: 'selected_term',
				value: selected_term,
			}).appendTo('#download_candidates_form');
			
			$('#download_candidates_form').submit();
			
		});

		
	});
</script>


