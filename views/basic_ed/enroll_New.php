<?php 
		//if ($stud_status == 'active' AND !$from_enroll){
		//	print("<h3>Student Already Enrolled</h3>");
 		//} else {
			if (isset($message) && $tab=='9') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>

<style>
	div.enroll {
		font-weight: bold;
		margin: 3px;
		vertical-align: text-bottom;
	}
	
	span.e1 {
		float: left;
		width: 130px;
	}
</style>

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; ">
	<form method="post" id="enroll_new_stud">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "enroll_student"/>

	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" >
	<input type="hidden" name="levels_id" value="<?php echo (!empty($academic_years) ? $academic_years[0]->id : ''); ?>" >

	<?php 
		if (!$academic_years AND !$from_enroll) {
	?>
	<div class="control-group formSep" style="margin-top:5px; color: #FF0000;">
		<h3>Student may have already enrolled or has not registered yet!</h3>
	</div>
	<?php 
		}
	?>	  
	<div style="margin-bottom:20px; margin-left:20px;">
	<h3>Student Payments</h3>
	  <table width="75%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:90%; padding:0px; ">
	    <thead>
	      <tr>
	         <th width="8%" style="text-align:center;">OR No.</th>
			 <th width="12%" style="text-align:center;">Payment Date</th>
			 <th width="25%" style="text-align:center;">Particulars</th>
	         <th width="10%" style="text-align:center;">Amount Paid</th>
	         <th width="10%" style="text-align:center;">Status</th>
	       	 <th width="25%" style="text-align:center;">Teller</th>         
		  </tr>
	    </thead>
	    <?php
			if ($payments_info) {
				foreach ($payments_info AS $payment) {
		?>
	    <tr>
	      <td style="text-align:left;"><?php print($payment->receipt_no); ?></td>
		  <td style="text-align:center;"><?php print($payment->payment_date); ?></td>
		 <td> <?php print($payment->description); ?></td>	
	      <td style="text-align:right;"><?php print number_format($payment->receipt_amount,2); ?></td>
	      <td style="text-align:center;"><?php echo $payment->status; ?></td>
	   	  <td style="text-align:left;"><?php echo $payment->stud_name == NULL? $payment->emp_name : $payment->stud_name; ?> </td>
	    </tr>
	    <?php
				}
			}else{ ?>
			 <tr> 
			    <td colspan="6">No payments yet</td>
			</tr>	
			<?php }   ?>
	  </table>

	<?php 
		if ($academic_years) {
	?>
	  <div class="control-group formSep">
	   <h3> ENROLL STUDENT/PUPIL for:</h3>
	  </div>

	<div class="enroll">
			<span class="e1">School Year:</span> 
			<select name="basic_ed_histories_id" style="padding:0px;" id="acad_yrs">
					<?php 
						foreach($academic_years AS $acad_yr) {
							print("<option value=".$acad_yr->basic_ed_histories_id." yl_name=\"".$acad_yr->yr_level."\" 
									levels_id=\"".$acad_yr->levels_id."\" >".$acad_yr->sy."</option>");
						}
					?>
			</select>
	</div>

	<div class="enroll">
			<span class="e1">Year and Level:</span>
			<span id="yr_level_name" style="font-weight:bold; font-size:14px; color:#077929;"><?php print($academic_years[0]->yr_level); ?> </span>
	</div>
	
	<?php
		if ($academic_years[0]->levels_id == "12") {
	?>			
			<div class="enroll my_level">
				<span class="e1" style="padding-top:7px;">Strand:</span> 
				<select name="strand_id" style="padding:0px;">
					<?php 
						foreach($strands AS $strand) {
							print("<option value=".$strand->id." >".$strand->strand_code."</option>");
						}
					?>
				</select>
			</div>
	<?php
		}
	?>

	<div class="control-group formSep">
		 <button class="btn btn-primary " type="submit" >ENROLL Student!</button>
		</div>
	<?php 
		} 
	?>
		
	  </div>
	</form>
 </div> 
 <?php 
	//}
 ?>
 
<script>
$('.enroll_me').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to remove Graduated Program?");

	if (confirmed){
		var id = $(this).attr('href');
		
		$('#enroll_new_stud').submit();
	}		
});
</script>

 <script>
	$(document).ready(function(){
		$("#acad_yrs").bind('change', function(){
            var element = $("option:selected", this);
            var myTag = element.attr("yl_name");
			var levels_id = element.attr("levels_id");

			$('<input>').attr({
				type: 'hidden',
				name: 'levels_id',
				value: levels_id,
			}).appendTo('#enroll_new_stud');

        	if (levels_id == '12') {
            	$('.my_level').show();
 			} else {
            	$('.my_level').hide();				
			}
            
            $('#yr_level_name').text(myTag);
 			
		});
	});
</script> 

 
 <script>
$("#acad_yrs1").change(function(){
            var element = $("option:selected", this);
            var myTag = element.attr("yl_name");
			var levels_id = element.attr("levels_id");

			$('<input>').attr({
				type: 'hidden',
				name: 'levels_id',
				value: levels_id,
			}).appendTo('#promotion');
		
        	if (levels_id == '12') {
            	$('.my_level').show();
 			} else {
            	$('.my_level').hide();				
			}
            
            $('#yr_level_name').text(myTag);
        });
</script>