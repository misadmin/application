<div style="width:auto;">
<div style="text-align:right">
<a class="download_assess" href="" academic_terms_id="<?php print($term->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>
 <table width="100%" border="0" cellspacing="0" cellpadding="1" class="table table-bordered" id="table-summary">
 	<thead>
  <tr>
    <td colspan="8" align="center" style="text-align:center; font-weight:bold; font-size:16px;">BASIC EDUCATION<br />ASSESSMENT SUMMARY<br /> 
	<?php
		print($acad_yr->sy);
	 ?></td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td style="width:22%; text-align:center;">DEPARTMENT</td>
    <td style="width:11%; text-align:center;">1st Year </td>
    <td style="width:11%; text-align:center;">2nd Year </td>
    <td style="width:11%; text-align:center;">3rd Year </td>
    <td style="width:11%; text-align:center;">4th Year </td>
    <td style="width:11%; text-align:center;">5th Year </td>
    <td style="width:11%; text-align:center;">6th Year </td>
    <td style="width:11%; text-align:center;">TOTAL</td>
  </tr>
	</thead>
  <?php 
		$grand_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'assess6'=>0,'total'=>0);
  		foreach($programs AS $program) {
			
			$groups = $this->Assessment_Model->ListGroupAssessmentSummary($program->id, $academic_terms_id);

			$total = 0;
			if ($groups) {
				foreach($groups AS $group) {
					$total = $group->assess1+$group->assess2+$group->assess3+$group->assess4+$group->assess5+$group->assess6; 
					$grand_total['assess1'] = $grand_total['assess1'] + $group->assess1;
					$grand_total['assess2'] = $grand_total['assess2'] + $group->assess2;
					$grand_total['assess3'] = $grand_total['assess3'] + $group->assess3;
					$grand_total['assess4'] = $grand_total['assess4'] + $group->assess4;
					$grand_total['assess5'] = $grand_total['assess5'] + $group->assess5;
					$grand_total['assess6'] = $grand_total['assess6'] + $group->assess6;
					$grand_total['total'] = $grand_total['total'] + $total;
  ?>
  <tr>
    <td style="text-align:left; padding-left:30px;"><?php print($program->abbreviation); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($group->assess1,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($group->assess2,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($group->assess3,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($group->assess4,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($group->assess5,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($group->assess6,2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($total,2)); ?></td>
  </tr>
  <?php
  				}
  			}
		}
   ?>
<tr style="background:#EAEAEA;">
    <td style="text-align:right; font-weight:bold;">GRAND TOTAL:</td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess1'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess2'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess3'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess4'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess5'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess6'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['total'],2)); ?></td>
  </tr>
</table>
<div style="text-align:right">
<a class="download_assess" href="" academic_terms_id="<?php print($term->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>

</div>

  <form id="download_assess_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="3" />
</form>
<script>
$(document).ready(function(){
   
   $('.download_assess').click(function(event){
		event.preventDefault(); 
		var academic_terms_id = $(this).attr('academic_terms_id');
			
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_terms_id',
			value: academic_terms_id,
		}).appendTo('#download_assess_summary_form');
		$('#download_assess_summary_form').submit();
		
	});
	var offset = $('.navbar').height();
    $("#table-summary").stickyTableHeaders({fixedOffset: offset-1});
    $('#table-summary').stickyTableHeaders('updateWidth');
	
});
	
</script>
