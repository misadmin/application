
<div id="show_extracted_items">
	<div class="tabbable" style="width:99%; margin:0 auto; margin-top:15px;" >
		<?php 
			switch ($this->session->userdata('role')) {
				
				case 'eofficer':
		?>
					<ul class="nav nav-tabs">
						<li class="dropdown <?php if($active_tab == 'enrollment_status') print("active"); ?>" >
							<a href="#enrol_status" data-toggle="tab">Enrollment Status</a>
						</li>
					</ul>

					<div class="tab-content" >
						<div class="tab-pane <?php if($active_tab == 'enrollment_status') print("active"); ?>" id="enrol_status">
							<?php
								$this->load->view('bed/student/tabs/enrollment_status');
							?>
						</div>
					</div>
					
		<?php 
				break;
				
				case 'rsclerk':
		?>
					<ul class="nav nav-tabs">
						<li class="dropdown <?php if($active_tab == 'assign_section') print("active"); ?>" >
							<a href="#assign_section" data-toggle="tab">Assign Section</a>
						</li>
					</ul>

					<div class="tab-content" >
						<div class="tab-pane <?php if($active_tab == 'assign_section') print("active"); ?>" id="assign_section">
							<?php
								$this->load->view('bed/student/tabs/assign_section');
							?>
						</div>
					</div>

		<?php
					break;
					
			}
		?>
		
		
	</div>

</div>
	