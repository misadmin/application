<?php $genders = $this->config->item('genders'); ?>
<style>
.pagination {
	height: auto;
}
.pagination > ul > li > a{
	width: 24px; /* changed from 27 */
	text-align: center;
	padding: 0;
}
</style>
<div class="row-fluid">
	<h2 class="heading">Employee Search</h2>
	<div class="pagination">
		<ul>
<?php for($count = ord('A'); $count<=ord('Z'); $count++): ?>
			<li <?php if(isset($letter) && $letter == chr($count)) echo 'class="active"'?>><a href="<?php echo chr($count); ?>"><?php echo chr($count); ?></a></li>
<?php endfor; ?>
		</ul>
	</div>
	<div class="row-fluid clearfix">
		<table class="table table-striped table-bordered mediaTable activeMediaTable" id="MediaTable-0">
			<thead>
				<tr>
					<th>No.</th>
					<th>Employee ID</th>
					<th>Employee Name</th>
					<th>Gender</th>
				</tr>
			</thead>
			<tbody>
<?php if(isset($results) && count($results) > 0): $count=0; ?>
<?php foreach ($results as $result): $count++; ?>
				<tr>
					<td><?php echo $count; ?></td>
					<td><a href="<?php echo site_url("{$role}/faculty/{$result->empno}"); ?>"><?php echo $result->empno; ?></a></td>
					<td><?php echo $result->fullname; ?></td>
					<td><?php if( ! empty($result->gender)) echo $genders[$result->gender];?></td>
				</tr>
<?php endforeach; ?>
<?php else: ?>
				<tr><td colspan="4">No Results Found</td></tr>
<?php endif;?>
			</tbody>
		</table>
	</div>
</div>
<form method="post" action="" id="search_by_letter">
</form>
<script>
$('.pagination a').click(function(event){
	event.preventDefault(); 
	var letter = $(this).attr('href');
			
	$('<input>').attr({
	    type: 'hidden',
	    name: 'letter',
	    value: letter,
	}).appendTo('#search_by_letter');
		$('#search_by_letter').submit();
});
</script>