<style>
.void {color: #ccc;}
td.right-align{text-align:right;}
table.summary td {border:1px solid #DCDCDC;}
table.transactions td {border:1px solid #DCDCDC;}
.heading{border-bottom: 1px solid #DCDCDC;}
div.spacer{height:20}
</style>
<?php $count = 0;?>
<?php foreach ($records as $machine_ip => $value) : $count++; ?>
<h3 class="heading">Machine No: <?php echo $machine_ip; ?></h3>
<div class="spacer"></div>
<table class="summary"  cellspacing="0" cellpadding="5" style="width:250;">
	<tr>
		<td colspan="2" style="text-align:center;font-weight:bold;">Payment Methods Summary</td>
	</tr>
	<tr>
		<td>Payment Method</td>
		<td>Total</td>
	</tr>
<?php $machine_total = 0; ?>
<?php foreach($payment_methods[$machine_ip] as $method=>$total): ?>
		<tr>
			<td><?php echo $method; ?></td>
			<td class="right-align"><?php echo number_format($total, 2); $machine_total+=$total; ?></td>
		</tr>
<?php endforeach; ?>
		<tr>
			<td class="right-align">Total</td>
			<td class="right-align"><strong><?php echo number_format($machine_total, 2); ?></strong></td>
		</tr>		
</table>
<div class="spacer"></div>
<table <?php if($count==1){echo 'id="header"'; } ?> class="transactions" cellspacing="0" cellpadding="3" style="">
	<tr>
		<td style="text-align:center;font-weight:bold;">OR Number</td>
		<td style="text-align:center;font-weight:bold;">OR Date</td>
		<td style="text-align:center;font-weight:bold;">Teller ID</td>
		<td style="text-align:center;font-weight:bold;">ID Number </td>
		<td style="text-align:center;font-weight:bold;">Payor</td>
		<td style="text-align:center;font-weight:bold;">Code</td>
		<td style="text-align:center;font-weight:bold;">Amount</td>
	</tr>
<?php foreach ($value as $teller_code => $transactions): ?>
		<tr><td colspan="7"><?php echo $teller_code; ?> (<?php echo $transactions['name']; ?>)</td></tr>
<?php $code_amount_total = 0; ?>
<?php foreach ($transactions['transactions'] as $transaction): ?>
		<tr <?php echo ($transaction->status=='void' ? 'class="void"' : ""); ?>>
			<td><?php echo $transaction->receipt_no; ?></td>
			<td><?php echo $transaction->transaction_date; ?><?php echo ($transaction->status=='void' ? ' (VOIDED)' : ""); ?></td>
			<td><?php echo $transaction->teller; ?></td>
			<td><?php echo $transaction->payor_id; ?></td>
			<td><?php echo $transaction->payor; ?></td>
			<td><?php echo $transaction->teller_code; ?></td>
			<td class="right-align"><?php echo number_format($transaction->whole_amount, 2); $code_amount_total+=(!in_array($transaction->status, array('void', 'deleted')) ? $transaction->whole_amount : 0); ?></td>
		</tr>
<?php endforeach; ?>
		<tr>
			<td colspan="5">&nbsp;</td>
			<td><?php echo $teller_code; ?> Sub Total:</td>
			<td style="text-align:right"><strong><?php echo number_format($code_amount_total, 2); ?></strong></td>
		</tr>
<?php endforeach; ?>
	</table>
<?php endforeach; ?>