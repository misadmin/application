<?php 
class Extras_lib {
	protected $CI;
	
	public function __construct(){
		$this->CI =& get_instance();
	}
	 
	public function added_subjects_after_assessments(){
		$this->CI->load->library('tab_lib');
		$this->CI->load->model('extras_model');
		$this->CI->content_lib->enqueue_footer_script('validate');
		
		if (($data = $this->CI->extras_model->list_last_assessment_dates())==false){
			$this->CI->content_lib->set_message('Unable to list dates!'.$this->CI->db->last_query(),'alert-error');
		}else{
			$adds = array();				
			foreach ($data as $row){				
				if ($subjects =  $this->CI->extras_model->list_added_subjects($row->transaction_date, $row->history_id)){
					foreach ($subjects as $subject){		
						$adds[] = array(
										'transaction_date' 	=> $row->transaction_date, 
										'history_id' 		=> $row->history_id,						
										'idno' 				=> $row->idno, 
										'student'			=> $row->student,  
										'abbreviation' 		=> $row->abbreviation, 
										'year_level'  		=> $row->year_level,
										'course_code' 		=> $subject->course_code,
										'section_code' 		=> $subject->section_code,
										'date_enrolled' 	=> $subject->date_enrolled,
										'enrolled_by_id' 	=> $subject->enrolled_by_id,
										'enrolled_by_name' 	=> $subject->enrolled_by_name,								
										'remark' 			=> $subject->remark,								
						);
					}							
				}
			}
		}
		
		$data['items'] = $adds;
		$this->CI->content_lib->enqueue_body_content("extras/subjects_added_after_assesments_posted",$data);
		$this->CI->content_lib->content();					
	}

	
	public function mismatches(){
		$this->CI->load->model('extras_model');
		$this->CI->load->model('teller/teller_model');
		$this->CI->load->model('hnumis/college_model');
		$data = array();
		$data['colleges'] = $this->CI->college_model->all_colleges();
		$this->CI->content_lib->enqueue_footer_script('validate');
		
		if ($this->CI->input->post('nonce') && $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {			
			//list students with college and program submitted
			
			$mismatch_counter = 0;
			$balances = array(); 
			
			$students = $this->CI->extras_model->list_students($this->CI->input->post('college'), $this->CI->input->post('program'));
			//print_r($students);die();
			if ($students){
				foreach ($students  as $student){	
					$isis_balances = $this->CI->extras_model->list_isis_balances2($student->students_idno);
					if ($isis_balances){
						//print_r($isis_balances);die();
						foreach ($isis_balances as $isis_balance){
							$isis_bal = floatval($isis_balance->balance);
							if ($payers_id = $this->CI->extras_model->get_payers_id($isis_balance->stud_id)){
								$mis_balance =0;
								$mis_transactions = $this->CI->teller_model->get_ledger_data($payers_id[0]->payers_id, TRUE, TRUE);
								foreach ($mis_transactions as $mis_transaction){
									$mis_balance = $mis_balance + $mis_transaction['debit'] - $mis_transaction['credit'];
								}
								//print_r($isis_bal);die();
								if (round($mis_balance,2) !== round($isis_bal,2)){
									$mismatch_counter++;
									$balances[]=array(
											'stud_id'=>$isis_balance->stud_id,
											'college_code'=>$student->college_code,
											'program'=>$student->abbreviation,
											'isis_balance'=>$isis_bal,
											'mis_balance'=>$mis_balance,
											'remark'=>'',
									);
								}
									
							}else{
								if ($isis_bal !==0){	
									$balances[]=array(
											'stud_id'=>$isis_balance->stud_id,
											'college_code'=>$student->college_code,
											'program'=>$student->abbreviation,
											'isis_balance'=>$isis_bal,
											'mis_balance'=>null,
											'remark'=>'Not found in MIS',
									);
								}
							}
							$isis_bal = 0;
						}					
					}else{						
						//print_r($this->CI->db->last_query());die();						
					}
				}
			}else{
				//print_r($this->CI->db->last_query());die();			
				//die("sowee!");
			}
			$data['balances']=$balances;
			//print_r($balances);die();
			//$this->CI->content_lib->enqueue_body_content('extras/mismatches_view', $data);
		}else{
			//$this->CI->content_lib->enqueue_body_content('extras/mismatches_index', $data);
		}		
		$this->CI->content_lib->enqueue_body_content('extras/mismatches_index', $data);
		$this->CI->content_lib->content();
	
	}


	public function total_withdrawals(){
		//if ($this->CI->session->userdata('empno') !== '00404')
		//	die('under construction');
		$this->CI->load->model('extras_model');
		$data['rows'] =  $this->CI->extras_model->list_students_with_total_withdrawals();
		$this->CI->content_lib->enqueue_body_content('extras/total_withdrawals_view', $data);
		$this->CI->content_lib->content();
	}
	
	
	public function enrollments_vs_withdrawals(){
		$data = array();			
		$top_year = date('Y');
		$end_yr=array();
		for ($i = $top_year; $top_year - $i < 10; $i-- ){
			$end_yr[]=$i;
		}
		$data['end_yr'] = $end_yr;			
		$this->CI->load->model('extras_model');			
		if ($this->CI->input->post('nonce') && $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {	
			$data['rows'] =  $this->CI->extras_model->list_enrolled_vs_withdrawals($this->CI->input->post('end_yr'));
			//print_r($data['rows']);die();
		}			
		$this->CI->content_lib->enqueue_body_content('extras/enrolled_vs_withdrawals_view',$data);		
		$this->CI->content_lib->content();		
	}
	
	public function ar_schedule($deposits_only=false){
		//if ($this->CI->session->userdata('empno') !== '00404') die('under construction');
		$this->CI->load->model('extras_model');		
		$this->CI->load->model('hnumis/college_model');
		$this->CI->load->model('accountant/acct_reports_model');
		$this->CI->load->model('teller/teller_model');
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');
		$end_date = $this->CI->input->post('end_date');
		//var_dump($deposits_only);die();
		$data = array();
		$data['title'] = $deposits_only == TRUE ? 'Schedule of Deposits ' : 'Schedule of A/R Balances ' ;
		//print_r($data['title']);die();
		$depts = $this->CI->college_model->all_colleges();
		$colleges=array();
		foreach ($depts as $dept){
			$colleges[floatval($dept->id)] = $dept->name;
		}				
		$data['colleges'] = $colleges;  
		//print_r($data['colleges']);die();
		
		if ($this->CI->input->post('nonce') && $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {
			$date = $end_date;
			$data['date'] = $date;
			if ( ! ($current_end_year = $this->CI->extras_model->get_current_end_year()) ) 
				die('No current academic year');
			
			$students = $this->CI->extras_model->list_students2($this->CI->input->post('top_levels'),$this->CI->input->post('sub_level'), $current_end_year[0]->end_year );
			//print_r($students);die();
			$balances = array();
			//$deposits = array();
			
			$csv = array();
			if ($students){
				$program = "";
				$yr_level = "";
				$sub_total=0;
				$iteration = 0;
				$sub_total_pos = 0;
				$sub_total_neg = 0; 
				foreach ($students as $student){
						$sy = ((int)$student->end_year - 1).'-'. substr($student->end_year,2,2);
						
						$payers_id = $this->CI->extras_model->get_payers_id($student->students_idno);
						
						if ($payers_id){
							$ledger_data = $this->CI->teller_model->get_ledger_data($payers_id[0]->payers_id,TRUE, FALSE, $date, TRUE );
							//$ledger_data = $this->CI->teller_model->get_ledger_data_balance_only($student->students_idno,TRUE, FALSE, $date, TRUE );
								
							//$payers_id, $balance_only = FALSE, $before_linog = FALSE, $cut_off = FALSE, $exclude_written_off = FALSE
							//if (count($ledger_data)>0){ print_r($ledger_data);die(); }
							//print_r($ledger_data);die();
							if ($ledger_data){
								$balance = 0;
								foreach ($ledger_data as $ledger_entry){
									$balance = $balance + $ledger_entry['debit'] - $ledger_entry['credit'];
								}
	
								if (round((float)$balance,2) !== 0.00){
										$iteration++;
										
										if ($deposits_only){
												
												if (round((float)$balance,2) < 0.00){																		
													$balances[] = array(
														'students_idno'=>$student->students_idno,
														'student'=>$student->student,
														'abbreviation'=>$student->abbreviation,
														'year_level'=>$student->year_level,
														'sy'=>$sy,
														'levels_id'=>$student->levels_id,
														'balance'=>$balance,);
													$sub_total_neg = $sub_total_neg + round($balance,2);												
												}												
										}else{ 
																						
												if (round((float)$balance,2) > 0.00){
													$balances[] = array(
														'students_idno'=>$student->students_idno,
														'student'=>$student->student,
														'abbreviation'=>$student->abbreviation,
														'year_level'=>$student->year_level,
														'sy'=>$sy,
														'levels_id'=>$student->levels_id,
														'balance'=>$balance,);
													$sub_total_pos = $sub_total_pos + round($balance,2);												
												}												
										}
	
										$program  = trim($student->abbreviation);
										$yr_level = trim($student->year_level);										
										$sub_total = $deposits_only ? $sub_total_neg : $sub_total_pos;
																				
								} //(round((float)$balance,2) !== 0.00)
								
							} //end of $ledger_data 
							
						} //end of $payers_id					
								
				} //end foreach($students)
					
			} //endif ($students)
			
			
			
			
			

			$data['rows'] =  $balances;
			//print_r($data);die();

			if ($this->CI->input->post('print_to') == 'CSV'){
				
				if (count($balances) > 0){ 
					foreach ($balances as $row){
						if ($row['abbreviation'] != 'Sub Total')
							$csv[] =array(
									$row['sy'],
									$row['students_idno'],
									$row['student'],
									$row['abbreviation'],
									$row['year_level'],
									$row['balance'],);
					};
					$file_name = $this->CI->session->userdata('lname') . '_' . ($deposits_only ? "sked_deposits" : "sked_ar") . '_' . str_replace('-','',$end_date) .  '.csv';
					header('Content-Type: text/csv');
					header('Content-Disposition: attachment;filename='.$file_name);
					echo $this->makeCSV($csv); die();
				}
				

/*				$this->load->library('PHPExcel');
				$filename = "downloads/ar_schedule_of_" . $deposits_only ? "deposits":"Receivables" . ".xls";
				$objPHPExcel = new PHPExcel();				
				$objPHPExcel->getProperties()->setTitle($filename);				
				$objPHPExcel->setActiveSheetIndex(0);				
				$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);				
				redirect(site_url("downloads/".$filename));
*/				
				
			}
							
		} //end nonce
		
		$this->CI->content_lib->enqueue_body_content('extras/ar_schedule_view', $data);
		$this->CI->content_lib->content();
				
	}

	
	function makeCSV($table){
		$csv="";
		foreach($table as $r){
			$csv .= implode(",", $r).",\n";
		}
		return	$csv;
	}
	
	function list_students_with_more_than_one_idno(){
		$data=array();
		$this->CI->load->model('extras_model');
		$lists = $this->CI->extras_model->list_students_with_more_than_one_idno();
		if (!$lists){
			$this->CI->content_lib->set_message("Sorry, but I'm not able to get the list (: ","alert-success");
		}else{			
			$data['rows']=$lists;
			$this->CI->content_lib->enqueue_body_content('extras/multiple_idnos_view', $data);
		}
		$this->CI->content_lib->content();
		
	}

	
	function search_receipt(){
		$this->CI->load->model('extras_model');
		$this->CI->content_lib->enqueue_header_style('date_picker');
		$this->CI->content_lib->enqueue_footer_script('date_picker');
		$this->CI->content_lib->enqueue_footer_script('validate');	
		$data = array();
		$data['rows']=array();
		
		//&& $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))
		if ($this->CI->input->post('nonce')){
			$data['chk1'] =	$this->CI->input->post('chk1');
			$data['chk2'] =	$this->CI->input->post('chk2');
			$data['chk3'] =	$this->CI->input->post('chk3');
			$data['chk4'] =	$this->CI->input->post('chk4');
			$data['chk5'] =	$this->CI->input->post('chk5');
			$data['teller_code'] = sanitize_text_field($this->CI->input->post('teller_code'));
			$data['ornum'] = sanitize_text_field($this->CI->input->post('ornum'));
			$data['start_date'] = sanitize_text_field($this->CI->input->post('start_date'));;
			$data['end_date'] = sanitize_text_field($this->CI->input->post('end_date'));			
			$data['idno'] = sanitize_text_field($this->CI->input->post('idnum'));
			$data['machine_ip'] = sanitize_text_field($this->CI->input->post('machine_ip'));
			$receipts = $this->CI->extras_model->search_receipt($data['ornum'], $data['start_date'], $data['end_date'], $data['idno'], $data['teller_code'], $data['machine_ip'], $data['chk1'], $data['chk2'], $data['chk3'], $data['chk4'], $data['chk5']);
			if ($receipts){
				$data['rows']=$receipts;
			}  
		}
		$this->CI->content_lib->enqueue_body_content('extras/search_receipt_view', $data);
		$this->CI->content_lib->content();
	}
	
	
	function paying_units(){
		//if ($this->CI->session->userdata('empno') !== '00404') die('under construction');
		
		$this->CI->load->model('extras_model');
		$this->CI->load->model('hnumis/college_model');
		$data = array();
		$data['colleges'] = $this->CI->college_model->all_colleges();
		$this->CI->content_lib->enqueue_footer_script('validate');
		
		$ar_terms = array('1'=>'1st Sem', '2'=>'2nd Sem','3'=>'Summer');
		
		$data['rows']=array();
		
		$top_year = date('Y');
		$end_yr=array();
		for ($i = $top_year; $top_year - $i < 8; $i-- ){
			$end_yr[]=$i;
		}
		$data['end_yr'] = $end_yr;
		
		
		
		if ($this->CI->input->post('nonce') && $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {
			$list = array();
			$program_id = $this->CI->input->post('program');
			$yr_level = $this->CI->input->post('yr_level');
			$end_year = $this->CI->input->post('end_yr');
			$term 	  = $this->CI->input->post('term');
			$list = $this->CI->extras_model->paying_units($program_id, $yr_level, $end_year, $term );
			//print_r($list );die();
			if ($list){				
				$data['rows']=$list;
			}

			if ($this->CI->input->post('print_to') == 'CSV'){
				$prog_chosen="";
				$total_pay_units=0;
				$key=0;
				if (count($list) > 0){
					foreach ($list as $row){
						$key++;
						$total_pay_units += $row->pay_units;						
						$csv[] =array(
								$row->end_year-1 .'-' . $row->end_year,
								$ar_terms[$row->term],
								$key,
								$row->idno,
								$row->student,
								$row->program,
								round($row->pay_units,2));
						$prog_chosen = substr($row->program,2);
					}
					$csv[] = array('','','','','Average Paying Units',round($total_pay_units/$key,2));

					$file_name = $this->CI->session->userdata('lname') . '_paying_units_' . $prog_chosen .  '.csv';
					header('Content-Type: text/csv');
					header('Content-Disposition: attachment;filename='.$file_name);
					echo $this->makeCSV($csv); die();
				}
			
			}
			
		}		
		
		$this->CI->content_lib->enqueue_body_content('extras/paying_units_view', $data);
		$this->CI->content_lib->content();
	
	}
	
	function tf_summary(){
		//if ($this->CI->session->userdata('empno') !== '00404') die('under construction');
		$this->CI->load->model('extras_model');
		$this->CI->content_lib->enqueue_footer_script('validate');
		$data = array();
		$term = array('1'=>'1st Sem','2'=>'2nd Sem','3'=>'Summer');
		$data['term'] = $term;	
		$data['rows'] = array();
		$con_term = $this->CI->config->item('terms');
		
		if ($this->CI->input->post('nonce') && $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {
			$list = array();
			$year_start = $this->CI->input->post('year_start');				
			$year_end = $this->CI->input->post('year_end');
			
			$reeds = $this->CI->extras_model->list_reed_fees_total($year_start, $year_end);
				if ($reeds){
					$reed_total=array();
					foreach($reeds as $reed){
						$reed_total[$reed->end_year . '-' . $reed->term . '-' . $reed->college_code] = array('reed' => $reed->reed_total, 'reed_wd' => $reed->reed_total_wd);
					}
				}
			//print_r($reed_total);die();
			$data['reed_rows']=$reed_total;
			
			//print_r($reed_total);die();
			
			$list  = $this->CI->extras_model->list_tuition_fees_total($year_start, $year_end);
			//print_r($list );die();
			if ($list){
				$data['rows']=$list;
			}
			if ($this->CI->input->post('print_to') == 'CSV'){
				$prog_chosen="";
				$total_tuition = 0;
				$key=0;
				if (count($list) > 0){
					foreach ($list as $row){
						$key++;
						$total_tuition  += $row->tuition_total;
						$reed_key = $row->end_year . '-' . $row->term . '-' . $row->college_code;						
						$csv[] =array(
								$key,
								$row->end_year-1 . ' - ' . $row->end_year,
								$con_term[$row->term],
								$row->college_code ,
								round($row->tuition_total,2),
								round($row->tuition_total_wd,2),
								isset($reed_total[$reed_key]['reed']) ? round($reed_total[$reed_key]['reed'],2) : '0.00',
								isset($reed_total[$reed_key]['reed_wd']) ? round($reed_total[$reed_key]['reed_wd'],2) : '0.00'
						);
					}
	
					$file_name = $this->CI->session->userdata('lname') . '_tf_summary_' . $prog_chosen .  '.csv';
					header('Content-Type: text/csv');
					header('Content-Disposition: attachment;filename='.$file_name);
					echo $this->makeCSV($csv); die();
				}
					
			} 
				
		}
	
		$this->CI->content_lib->enqueue_body_content('extras/tf_summary_view', $data);
		$this->CI->content_lib->content();
	
	}
	
	
	function write_off($config){
		$data = array();
		$is_col = array();
		$this->CI->load->model('extras_model');
		$this->CI->load->model('teller/teller_model');
		$this->CI->load->model('posting/posting_model');
		if ($depts = $this->CI->extras_model->list_levels_and_deparments()){
			$data['depts'] = $depts;
		}else{ 
			$data['depts'] = array('0'=> (object)array('id'=>'0','level'=>'Empty or Error on Query')); //damn! this should be the right programming practice		 
		}
		
		
		
		if ($this->CI->input->post('nonce') && $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {
			
			$action =  $this->CI->input->post('action');
			
			switch ($action){
				case 'Generate-List' :
					//print_r($this->CI->input->post('wo_type'));die();
					$department = $this->CI->input->post('department') == 'Unknown' ? '' : $this->CI->input->post('department');
					if (!($wo = $this->CI->extras_model->write_off_list($department, $this->CI->input->post('wo_type'), $config['write_off_ending']) )){
						$wo = array();
					}
					//print_r($wo);die();
					$data['wos'] = $wo;
					
						
					if ($this->CI->input->post('print_to') == 'CSV'){
						if (count($wo) > 0){
							$csv = array();
							$csv[] = array($this->CI->input->post('wo_type'),"", "","","","","");
							$csv[] = array("ID No","Lastname, Firstname","Yr Level","Department","Last Transaction","Amount");
							foreach ($wo as $row){
								$csv[] =
								array($row->idno,
										$row->student,
										$row->year_level,
										$row->department,
										$row->last_transaction,
										- $row->balance);
							}
							$prog_chosen = $this->CI->input->post('department');
							$file_name = $this->CI->session->userdata('lname') . '_for_write_off_' . $prog_chosen .  '.csv';
							header('Content-Type: text/csv');
							header('Content-Disposition: attachment;filename='.$file_name);
							echo $this->makeCSV($csv); die();
						}else{
							$this->CI->content_lib->set_message('No record found! CSV file not created.');
						}
					}
								
					break;
					
					
				case 'Write-Off-obsolete' :								
					$result_message = array();
					$payer_ids = $this->CI->input->post('check');
					$trans = $this->CI->input->post('trans');
					if (is_array($payer_ids) AND count($payer_ids)>0 )  {

						foreach ($payer_ids as $payer_id){
							$payers_transactions = $this->CI->posting_model->get_payers_transactions($payer_id, $trans[$payer_id]);
							if ($payers_transactions){
								
								/** find active rows, write-off, mark writeoff_2012 done. stop until not active row. **/								
								$row=0;
								$this->CI->db->trans_start();
								$has_errors = false;
								while ($row < count($payers_transactions) && $payers_transactions[$row]->isis_status=='active' && $has_errors == false ){	
									$write_off = 0;									
									if ( !($write_off = $this->CI->posting_model->write_off_payments($payers_transactions[$row]->payments_id)) ){
										$this->CI->db->trans_rollback();
										$has_errors = true;
									} 
									$row++;
								}
								
								
								if ($has_errors === false){
									if ( !($done = $this->CI->posting_model->mark_writeoff_table_done($payer_id)) ){
										$this->CI->db->trans_rollback();
										$result_message[] = array('idno' => $payers_transactions[0]->idno,'student' => $payers_transactions[0]->student,'remark'=>'Failed');
									}else{
										$this->CI->db->trans_complete();
										$result_message[] = array('idno' => $payers_transactions[0]->idno,'student' => $payers_transactions[0]->student,'remark'=>'Success');
									}
								} 
								
							}
							
						} //end of foreach
					}
					$data['result_message'] = $result_message;
					$this->CI->content_lib->enqueue_body_content('extras/writeoff_result',$data);						
					$this->CI->content_lib->content();
					die();
					break;


				case 'Write-Off' :								
					$result_message = array();
					$payer_ids = $this->CI->input->post('check');
					$trans = $this->CI->input->post('trans');
					if (is_array($payer_ids) AND count($payer_ids)>0 )  {

						foreach ($payer_ids as $payer_id){
							$payers_transactions = $this->CI->posting_model->get_payers_transactions_for_mis($payer_id, $trans[$payer_id]);
							if ($payers_transactions){
								
								/** find active rows, write-off, mark writeoff_2012 done. stop until not active row. **/								
								$row=0;
								$this->CI->db->trans_start();
								$has_errors = false;
								while ($row < count($payers_transactions) && $payers_transactions[$row]->status=='active' && $has_errors == false ){	
									$write_off = 0;
									if ( !($write_off = $this->CI->posting_model->write_off_ledger_for_mis($payers_transactions[$row]->ledger_id)) ){
										$this->CI->db->trans_rollback();
										$has_errors = true;
									} 
									$row++;
								}
								
								
								if ($has_errors === false){
									if ( !($done = $this->CI->posting_model->mark_writeoff_table_done($payers_transactions[0]->idno)) ){
										$this->CI->db->trans_rollback();
										$result_message[] = array('idno' => $payers_transactions[0]->idno,'student' => $payers_transactions[0]->student,'remark'=>'Failed');
									}else{
										$this->CI->db->trans_complete();
										$result_message[] = array('idno' => $payers_transactions[0]->idno,'student' => $payers_transactions[0]->student,'remark'=>'Success');
									}
								} 
								
							}
							
						} //end of foreach
					}
					$data['result_message'] = $result_message;
					$this->CI->content_lib->enqueue_body_content('extras/writeoff_result',$data);						
					$this->CI->content_lib->content();
					die();
					break;
			}
			
			
		} //end of if nonce
		$this->CI->content_lib->enqueue_body_content('extras/writeoff_view',$data);		
		$this->CI->content_lib->content();				
	
	}
	
}	
