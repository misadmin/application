<?php 

/**
 * v0.3 - Added method set column_id, this filters what data to be inserted on buffer
 * v0.4 - (April 11, 2013) All data will be inserted to buffer, the column id filters will be effective only in content() method where all the data is to be displayed
 * v0.5 - (April 24, 2013) Added insert free row method
 * v0.6 - (April 24, 2013) Added trigger_on_last child function, enables any function to be executed in the last data in table, usefull for computing total amount
 */

/**
 * This library creates a table automatically and saves time and effort:)
 * @author Kevin Felisilda 6128492
 * @author Aljun Bajao 6247912
 *
 */
class Table_lib implements Table_lib_Interface {
	/**
	 * This holds the table attributes
	 * @var Array
	 */
	private $tattributes;

	/**
	 * This holds the data for table heading
	 * @var Array
	 */
	private $thead;
	
	/**
	 * This holds the data for table body
	 * @var Array
	 */
	private $tbody;
	
	/**
	 * This specifies which column names are available
	 * @var Array
	 */
	private $column_ids;
	
	/**
	 * Create table_lib instance
	 * <code>void enqueue_text_input ([mixed $table_attributes])</code>
	 * <pre class="sunlight-highlight-php">
	 * <?php
	 * $this->load->library('table_lib');
	 * </pre>
	 * @param Array $table_attributes
	 */
	
	public function __construct($table_attributes=NULL){
		if (!is_null($table_attributes))
			$this->set_attributes($table_attributes);

		$this->thead = array();
		$this->tbody = array();
		$this->column_names = array();
	}
	
	/**
	 * This function sets the tables attributes
	 * <code>void set_attributes($table_attributes)</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * <?php
	 * 
	 * //Inline Attributes
	 * $this->table_lib->set_attributes(array('class'=>'table','id'=>'table1'));
	 * 
	 * //Putting your attributes to an array
	 * $attributes = array('class' => 'table table-condensed',
	 *                     'id' => 'students',
	 *                     'width' => '100%'
	 * 					  );
	 * 
	 * $this->table_lib->set_attributes($attributes);
	 * </pre>
	 * 
	 * @param Array $table_attributes
	 */
	public function set_attributes($table_attributes){
		$this->tattributes = $table_attributes;
	}
	
	/**
	 * <code>void set_column_id($ids [, $clear=FALSE])</code>
	 * This function is used to specify the column names of the table
	 * If column names are set, adding data to the table can be
	 * organized simply by identifying the data name and column name
	 * 
	 * @param Array $names
	 * @param Boolean $clear Default False
	 */
	private function set_column_id($ids,$clear=FALSE){
		if ($clear){ //clear all column names and start adding again
			$this->column_ids = array();
		}
		if (is_array($ids)){
			foreach($ids as $id){
				$this->column_ids[] = $id;
			}
		} else {
			$this->column_ids[] = $ids;
		}
	}
	
	/**
	 * This function sets the header row
	 * 
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * <?php
	 * 
	 * //simple header with attributes
	 * $head_row1 = array(
	 *                   array("value"=>"Students",
	 *                         "attributes"=>array("class"=>"span12",
	 *                                             "colspan"=>"3")
	 *                         )
	 *                   );
	 * 
	 * //main header with identifier and attributes
	 * //id serves as the basis for the data to be inserted properly
	 * $head_row2 = array(
	 *                   array("id"=>"id",
	 *                         "value"=>"ID Number",
	 *                         "attributes" => array('id'=>'id_header','style'=>'width:15%')
	 *                   ),
	 *                   array("id"=>"name",
	 *                         "value"=>"Names",
	 *                         "attributes" => array('id'=>'name_header')
	 *                   ),
	 *                   array("id"=>"gender",
	 *                         "value"=>"Gender"
	 *                   )
	 *                 );
	 * 
	 * $this->table_lib->insert_head_row($head_row1);
	 * $this->table_lib->insert_head_row($head_row2);
	 * </pre>
	 * 
	 * @param Array $rows
	 */
	public function insert_head_row($head_row){
		if (is_array($head_row)){
			$row = array();
			foreach($head_row as $head_column){
				if (isset($head_column['id']) AND !empty($head_column['id'])){
					$this->set_column_id($head_column['id']);
				}
				$column = array();
				$column['id'] = isset($head_column['id']) ? $head_column['id'] : '';
				$column['value'] = isset($head_column['value']) ? $head_column['value'] : '';
				$column['attributes'] = isset($head_column['attributes']) ? $head_column['attributes'] : array();
				array_push($row, $column);
			}
			array_push($this->thead, $row);
		} else {
			array_push($this->thead, array(array('value'=>$head_row,'attributes'=>array())));
		}
	}
	
	/**
	 * Inserts data on table
	 * 
	 * Example
	 * <pre class="sunlight-highlight-php">
	 * <?php
	 * //inserts a data array
	 * $data_array = array(
	 * 		array(
	 * 			"id"=>"6128492",
	 * 			"name"=>"Kevin Felisilda",
	 * 			"gender"=>"M",
	 * 			"password"=>"qwerty"
	 *          ),
	 * 		array(
	 * 			"id"=>"6247912",
	 * 			"name"=>"Aljun",
	 * 			"gender"=>"M",
	 * 			"password"=>"akoako"
	 *          )
	 * 		);
	 * 
	 * $this->table_lib->insert_data($data);
	 * 
	 * //inserts a single object
	 * class Student_test {
	 *     public $id;
	 * 	   public $name;
	 *     public $gender;
	 *     public $password;
	 * 		
	 *     function __construct($info){
	 *        $this->id = isset($info['id']) ? $info['id'] : '';
	 *        $this->name = isset($info['name']) ? $info['name'] : '';
	 *        $this->gender = isset($info['gender']) ? $info['gender'] : '';
	 *        $this->password = isset($info['password']) ? $info['password'] : '';
	 * 	   }
	 * }
	 * 
	 * $student1 = new Student_test(
	 *                      array("id"=>"6128492",
	 *                            "name"=>"Kevin Felisilda",
	 *                            "gender"=>"M",
	 *                            "password"=>"qwerty"
	 *                           ));
	 * $student2 = new Student_test(
	 *                      array("id"=>"6247912",
	 *                            "name"=>"Aljun",
	 *                            "gender"=>"M",
	 *                            "password"=>"akoako"
	 *                           ));
	 *                           
	 * $this->table_lib->insert_data($student1);
	 * $this->table_lib->insert_data($student2);
	 * 
	 * //inserts array of object
	 * $students = array($student1,$student2);
	 * $this->table_lib->insert_data($students);
	 * </pre>
	 * 
	 * @param Arrays $rows
	 * @param Boolean $clear_data
	 * @return Boolean, Integer
	 */
	public function insert_data($rows,$clear_data=FALSE){
		//check if $rows is a single class object
		if (is_object($rows)){
			$rows = get_object_vars($rows);
		}
		//check if $rows is a multidimension array and if it contains objects instead of array
		if (is_array($rows) AND count(is_array($rows)) > 0 AND isset($rows[0]) AND is_object($rows[0])){
			$temp = $rows;
			$rows = array();
			foreach($temp as $row){
				if (is_object($row)){
					$rows[] = get_object_vars($row);
				}
			}
		}
		//process $rows and merge it to $this->tbody (buffer)
		if (is_array($rows) AND count(is_array($rows)) > 0 AND count($this->column_ids) > 0){
			if ($clear_data){
				$this->tbody = array();
			}
			if (isset($rows[0])){
				foreach ($rows as $row){
					$current_row = array();
					$current_row['attributes'] = $this->format_row($row);
					//filter data to match with column_is to be inserted in buffer
					/* disabled as of april 11, 13
					foreach ($this->column_ids as $column){
						$current_row['data'][$column] = isset($row[$column]) ? $row[$column] : '';
					}
					*/
					
					//not anymore filter data to be inserted in buffer instead the "content" method is responsible with it
					$current_row['data'] = $row;
					array_push($this->tbody, $current_row);
				}
			} else {
				$current_row = array();
				$current_row['attributes'] = $this->format_row($rows);
				foreach ($this->column_ids as $column){
					$current_row['data'][$column] = isset($rows[$column]) ? $rows[$column] : '';
				}
				array_push($this->tbody, $current_row);
			}
			return count($this->tbody);
		}
		return FALSE;
	}
	
	/**
	 * Added April 14, 2013
	 * 
	 * Insert Free Row to the table
	 * 
	 * Example
	 * <pre class="sunlight-highlight-php">
	 * <?php
	 * $this->table_lib->insert_free_row('&lt;tr&gt;&lt;td colspan="3"&gt;Total:&lt;/td&gt;&lt;td&gt;{$total}&lt;/td&gt;&lt;/tr&gt;');
	 * </pre>
	 * @param String $row
	 */
	public function insert_free_row($row){
		$free_row = array('@@type' => 'free_row',
							 '@@row_content' => $row);
		array_push($this->tbody, $free_row);
	}
	
	/**
	 * 
	 * @param unknown_type $row
	 * @return string
	 */
	public function format_row($row){
		return '';
	}
	
	/**
	 * 
	 * @param unknown_type $row
	 * @return string
	 */
	public function format_data_default($row, $key){
		return $row[$key];
	}
	
	public function format_cell_default($row, $key){
		return '';
	}
	
	/**
	 * Clears all data in the table
	 */
	public function clear_all_data(){
		$this->tattributes = array();
		$this->thead = array();
		$this->tbody = array();
		$this->column_ids = array();
	}
	
	/**
	 * This function draws the table
	 * @return String
	 */
	public function content($return=FALSE){
		$table = '<table'.$this->parse_attributes($this->tattributes).'>';
		//concat thead
		if (count($this->thead) > 0){
			$table .= '<thead>';
			foreach ($this->thead as $thead_row){
				$table .= '<tr>';
				foreach ($thead_row as $column){
					$table .= '<th'.$this->parse_attributes($column['attributes']).'>'.$column['value'].'</th>';
				}
				$table .= '</tr>';
			}
			$table .= '</thead>';
		}
		//concat tbody
		if (count($this->tbody) > 0){
			//echo '<pre>';
			//print_r($this->tbody);
			//die();
			$table .= '<tbody>';
			foreach ($this->tbody as $tbody_row){
				//check for free row date
				if (isset($tbody_row['@@type']) AND $tbody_row['@@type'] == 'free_row'){
					$table .= $tbody_row['@@row_content'];
				}
				else {
					
					$table .= '<tr'.$this->parse_attributes($tbody_row['attributes']).'>';
					//display directly the data to table > tr element
					/*
					foreach ($tbody_row['data'] as $column_id => $column_value){
						$data = '';
						$attributes = '';
						if (method_exists($this,'format_data_'.$column_id)){
							$data = call_user_func(array($this, 'format_data_'.$column_id), $tbody_row['data']);
						} else {
							$data = $this->format_data_default($tbody_row['data'], $column_id);
						}
						if (method_exists($this,'format_cell_'.$column_id)){
							$attributes = call_user_func(array($this, 'format_cell_'.$column_id), $tbody_row['data']);
						} else {
							$attributes = $this->format_cell_default($tbody_row['data'], $column_id);
						}
						$table .= '<td'.$this->parse_attributes($attributes).'>'.$data.'</td>';
					}*/
					/**
					 * updated april 11, 2013
					 * filter before displaying the data to table > tr element, column_ids are used in filtering
					 */
					foreach ($this->column_ids as $column){
						$data = '';
						$attributes = '';
						$raw = FALSE;
						if (method_exists($this,'format_data_'.$column)){
							$data = call_user_func(array($this, 'format_data_'.$column), $tbody_row['data']);
						}
						else if (method_exists($this,'format_raw_data_'.$column)){
							$data = call_user_func(array($this, 'format_raw_data_'.$column), $tbody_row['data']);
							$raw = TRUE;
						}
						else {
							$data = $this->format_data_default($tbody_row['data'], $column);
						}
						if (method_exists($this,'format_cell_'.$column)){
							$attributes = call_user_func(array($this, 'format_cell_'.$column), $tbody_row['data']);
						}
						else {
							$attributes = $this->format_cell_default($tbody_row['data'], $column);
						}
						if ($raw){
							$table .= $data;
						}
						else{
							$table .= '<td'.$this->parse_attributes($attributes).'>'.$data.'</td>';	
						}
					}
					$table .= '</tr>';
				}
			}
			if (method_exists($this,'trigger_on_last')){
				$data = call_user_func(array($this, 'trigger_on_last'));
			}
			$table .= '</tbody>';
		}
		//concat closing tag
		$table .= '</table>';
		
		if ($return) return $table;
		else echo $table;
	}
	
	/**
	 * This function will concatinate all the atrributes
	 * @param Array or String $attributes
	 * @return String
	 */
	private function parse_attributes($attributes){
		if (is_array($attributes)){
			$string = '';
			foreach($attributes as $key => $value){
				switch($key){
					case 'class':
						$concat = ' ';
						break;
					case 'style':
						$concat = ';';
						break;
					default:
						$concat = ',';
						break;
				}
				$value = (is_array($value)) ? implode($concat,$value) : $value;
				$string .= $key.'="'.$value.'" ';
			}
			$attributes = trim($string);
		}
		return (!empty($attributes) ? ' '.$attributes : '');
	}
	
}

interface Table_lib_interface {
	
	public function set_attributes($attributes); //<table attributes>
	public function insert_head_row($head_row); // $head_row = array(array("id"=>"id", "value"=>"ID Number", "attributes" =>array()), array("id"=>"name", "value"=>"Names", "attributes" =>array())
	public function insert_data($rows);
	
	public function format_data_default($row, $key); //return value of the cell return $row[$key];
	public function format_cell_default($row, $key); //return attribute to call return 'class="error"';
	public function format_row($row); //add attributes to tr... if ($row['id'] < 2) return ' class="error"';
	//public function content($return);// if return = false will echo content; if $return == TRUE return content 
	//child class...
	
	//public function format_data_id ($row) {return $row['id'];}
	//public function format_data_name($row);
}