<?php 

class Pagination_lib {
	private $CI;
	
	public function __construct(){
		$this->CI =& get_instance();
	}
	public function pagination ($base_url, $total=10, $cur_page=1){
	
		$this->CI->load->library('pagination');
		$config = array(
				'base_url'			=> $base_url,
				'uri_segment'		=> 2,
				//'page_query_string' => TRUE,
				'use_page_numbers'	=> TRUE,
				'total_rows'		=> $total,
				'full_tag_open'		=> '<div class="pagination"><ul>',
				'full_tag_close' 	=> '</ul></div>',
				'num_tag_open'		=> '<li>',
				'num_tag_close'		=> '</li>',
				'prev_link'			=> 'Prev',
				'next_link'			=> 'Next',
				'prev_tag_open'		=> '<li>',
				'prev_tag_close'	=> '</li>',
				'last_tag_open'		=> '<li>',
				'last_tag_close'	=> '</li>',
				'cur_tag_open'		=> '<li class="active"><a href="#">',
				'cur_tag_close'		=> '</a></li>',
				'next_tag_open'		=> '<li>',
				'next_tag_close'	=> '</li>',
				'per_page'			=> $this->CI->config->item('results_to_show_per_page'),
				'first_link'		=> 'First',
				'first_tag_open'	=> '<li>',
				'first_tag_close'	=> '</li>',
				'last_link'			=> 'Last',
				'last_tag_open'		=> '<li>',
				'last_tag_close'	=> '</li>',
				);
		
		$this->CI->pagination->initialize($config);
		$this->CI->pagination->cur_page = $cur_page;
		
		$return = $this->CI->pagination->create_links();
		//$return = str_replace('<li><a href="/' . $cur_page . '">', '<li class="active"><a href="#">', $return);
		return $return;
	}
}