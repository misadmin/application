<?php 

?><table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Exam Name</th>
			<th>Exam Description</th>
			<th>Raw Score</th>
			<th>Percentile</th>
			<th>Exam Taken</th>
			<th>Term and SY Taken</th>
		</tr>
	</thead>
	<tbody> 
<?php if (is_array($exams_taken) && count($exams_taken) > 0 ): ?>
<?php foreach ($exams_taken as $exam): ?>
		<tr>
			<td><a href="javascript://" title="Click to edit this test result" class="exam_row closed" id="<?php echo $exam->test_result_id; ?>"><?php echo $exam->abbreviation; ?></a></td>
			<td><?php echo $exam->description; ?></td>
			<td><?php echo $exam->raw_score; ?></td>
			<td><a href="#" class="ttip_r" title="<?php echo $exam->comment; ?>"><?php echo $exam->percentile; ?></a></td>
			<td><?php echo $exam->date_taken; ?></td>
			<td><?php echo $exam->term; ?> <?php echo $exam->sy; ?></td>
		</tr>
		<tr class="edit_row" id="edit<?php echo $exam->test_result_id; ?>">
			<td colspan="6"><?php echo $exam->comment; ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="6">No Exam Taken Yet</td>
		</tr>
<?php endif; ?>
	</tbody>
</table>
<script>
$(document).ready(function(){
	$('.edit_row').each(function(){
		$(this).hide();
	});
	
	$('.exam_row').click(function(){
		var id = $(this).attr('id');

		$('.edit_row').each(function(){
			$(this).hide();
		});
		
		if ($(this).hasClass('closed')) {
			$(this).removeClass('closed');
			$(this).addClass('opened');
			$('#edit'+id).show();
		} else {
			$(this).removeClass('opened');
			$(this).addClass('closed');
		}
	});		
});
</script>