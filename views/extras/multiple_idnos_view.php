<style>
	table.balances 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
</style>


<div>
<div><br></div>
<div class="fluid span6"> 
<h4>List of Student With Multiple ID Numbers</h4>


<table id="t_id" class="table table-bordered table-striped balances table-condensed">
<thead>
	<tr>
	<th width="10%"></th>
	<th width="40%">Name</th>
	<th width="40%">ID Numbers</th>
	<th width="10%">Counts</th>
	</tr>
</thead>

<?php
	//echo "here";die(); 
	$row_num = 0;
	foreach($rows as $row){
		$row_num++;
?>
		<tr >
		<td class="center"><?php echo $row_num .'.'; ?> </td>
		<td class="left"><?php echo $row->student; ?></td>
		<td class="left"><?php echo $row->idno; ?></td>
		<td class="center"><?php echo $row->counts; ?></td>
		</tr>		

<?php 		
	}
?>




</table>
</div>
</div>

<script>
	$(document).ready(function(){
		$("#t_id").stickyTableHeaders();
	    $("#t_id").stickyTableHeaders('updateWidth');
	});
</script>	
