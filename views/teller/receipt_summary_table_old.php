<?php 

class rs_table extends Table_lib{
	
	public $cur_code;
	public $cur_machine_ip;
	public $cur_sub_total;
	public $voided_total;
	//public $valid_total;
	public $grand_total;
	public $min_or_number;
	public $max_or_number;
	
	private $is_first_entry;
	
	public function __construct(){
		parent::__construct();
		$this->cur_code = '';
		$this->cur_machine_ip = '';
		$this->cur_sub_total = 0;
		$this->grand_total = 0;
		$this->voided_total = 0;
		$this->min_or_number = 0;
		$this->max_or_number = 0;
		//$this->valid_total = 0;
		$this->is_first_entry = true;
	}
	
	public function format_raw_data_transaction_date($row){
		
		$str = '';
		
		//determine max and min O.R Number
		if ($this->is_first_entry){
			$this->min_or_number = $row['receipt_no'];
			$this->max_or_number = $row['receipt_no'];
		}
		else {
			if ($row['receipt_no'] < $this->min_or_number)
				$this->min_or_number = $row['receipt_no'];
			if ($row['receipt_no'] > $this->max_or_number)
				$this->max_or_number = $row['receipt_no'];
		}
		
		//print sub total
		if ($this->cur_code != $row['teller_code']){
			if ($this->is_first_entry){
				$this->is_first_entry = false;
			}
			else {
				$str .= '<td colspan="3" style="text-align:right">'.$this->cur_code.' Sub Total</td><td style="text-align:right"><strong>'.number_format($this->cur_sub_total,2).'</strong></td><td colspan="3"></td></tr><tr>';
				$this->cur_sub_total = 0;
			}
		}
		
		//update total and subtotals
		$this->cur_sub_total += $row['receipt_amount'];
		$this->grand_total += $row['receipt_amount'];
		if ($row['status'] == 'void'){
			$this->voided_total += $row['receipt_amount'];
		}
		
		//print machine ip header
		if ($this->cur_machine_ip != $row['machine_ip']){
			$str .= '<td colspan="7" style="border-bottom:solid 3px #000;padding-top:10px;font-size:1.2em;font-weight:bold">Machine No: '.$row['machine_ip'].'</td></tr><tr>';
			$this->cur_machine_ip = $row['machine_ip'];
		}
		
		//print teller code header
		if ($this->cur_code != $row['teller_code']){
			$str .= '<td colspan="7" style="background:#eee;font-size:1.1em;font-weight:bold" class="header">&nbsp;&nbsp;'.$row['teller_code'].' <span style="font-weight:normal">('.$row['description'].')</span></td></tr><tr>';
			$this->cur_code = $row['teller_code'];
		}
		
		$str .= '<td>&nbsp;&nbsp;&nbsp;&nbsp;'.$row['transaction_date'].'</td>';
		
		return $str;
	}
	
	public function trigger_on_last(){
		$this->is_first_entry = true;
	}
	
	public function format_data_receipt_amount($row){
		$str = '';
		if ($row['status'] == 'void'){
			$str .= '(VOID) ';
		}
		$str .= number_format($row['receipt_amount'],2);
		return $str;
	}
	
	public function format_cell_receipt_amount($row){
		return 'style="text-align:right;margin"';
	}
}

$rs_table = new rs_table();

$rs_table->set_attributes(array('class'=>'table table-condensed table-hover','id'=>'rs_table'));

$rs_table->insert_head_row(
array(
	array("id"=>"transaction_date",
		  "value"=>"O.R. Date - Time"
		 ),
	array("id"=>"receipt_no",
		  "value"=>"O.R. No."
		 ),
	array("id"=>"teller",
		  "value"=>"Teller"
		 ),
	array("id"=>"receipt_amount",
		  "value"=>"Amount",
		  "attributes"=>array("style"=>"text-align:right")
		 ),
	array("id"=>"teller_code",
		  "value"=>"Code"
		 ),
	array("id"=>"idno",
		  "value"=>"I.D. #"
		 ),
	array("id"=>"stud",
		  "value"=>"Name"
		 ),
	)
);

/*===============================================================================
 * Display Header
 ================================================================================*/

echo '<h2 class="heading">Receipt Summary <small> as of '.$date.'</small></h2>';

if (isset($records) AND is_array($records) AND count($records) > 0){
	$rs_table->insert_data($records);
	$rs_table->content(TRUE);
	$rs_table->insert_free_row('<td colspan="3" style="text-align:right">'.$rs_table->cur_code.' Sub Total</td><td style="text-align:right"><strong>'.number_format($rs_table->cur_sub_total,2).'</strong></td><td colspan="3"></td></tr><tr>');
	$rs_table->insert_free_row('<td colspan="3" style="text-align:right;padding-top:10px;border-top:solid 3px #000;margin-top:20px">GRAND TOTAL</td><td style="text-align:right;padding-top:10px;border-top:solid 3px #000;"><strong>'.number_format($rs_table->grand_total,2).'</strong></td><td style="text-align:right;padding-top:10px;border-top:solid 3px #000;" colspan="3"></td></tr><tr>');
	echo '<div class="row-fluid"><div class="pull-left" style="width:250px;font-size:15px;font-weight:bold;line-height: 20px;">Grand Total: <span class="pull-right">'.number_format($rs_table->grand_total,2).'</span></div><div class="pull-left" style="width:350px;margin-left:77px;font-size:15px;font-weight:bold;">OR#s: &nbsp;&nbsp;&nbsp;&nbsp;'.$rs_table->min_or_number.'-'.$rs_table->max_or_number.'</div></div>';
	echo '<div class="row-fluid"><div style="width:250px;font-size:15px;font-weight:bold;line-height: 20px;">Voided Total: <span class="pull-right">'.number_format($rs_table->voided_total,2).'</span></div></div>';
	echo '<div class="row-fluid"><div style="width:250px;font-size:15px;font-weight:bold;line-height: 20px;">Valid Total: <span class="pull-right">'.number_format($rs_table->grand_total - $rs_table->voided_total,2).'</span></div></div>';
	echo '<br>';
	$rs_table->content();
}
else{
	$rs_table->content();
	echo '<p>No records</p>';
}