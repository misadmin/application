<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding: 2px; margin-bottom:20px;">

	<table style="width:100%; height:auto;">
	<tr>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:20%; padding:3px;">
	<!-- <a class="download_enroll" href="" academic_years_id="<?php print($acad_yr->id); ?>"><i class="icon-download-alt"></i>Download to PDF</a> -->
	<a href="<?php print(base_url('downloads/Basic Education Enrollment Summary '.$acad_yr->sy.'.xls')); ?>" class="btn btn-link" id="download_summary">
	<i class="icon-download-alt"></i> Download as Excel</a>
	</td>
	<td style="text-align:right; vertical-align:top;">
		<form id="change_sy" method="POST">

			<select name="academic_year_id" id="academic_year_id">
				<?php
					foreach($school_years AS $school_year) {
				?>
						<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->sy); ?> </option>	
				<?php 
					}
				?>					
			</select>
							
	</form>
	</td>
	</tr>
	</table>						
</div>
<div style="margin-bottom: 15px; font-size:14px; color:#FF0000;">
<span style="font-weight: bold;">NOTE:</span> <span style="font-style: italic;">Click Total to view List of Students Enrolled</span>
</div>

<div style="width:auto;">
 <table width="150%" border="0" cellspacing="0" cellpadding="1" class="table table-bordered">
 	<thead>
  <tr>
    <td colspan="40" align="center" style="text-align:center; font-weight:bold; font-size:16px;">BASIC EDUCATION<br />
      ENROLLMENT SUMMARY<br /> 
	<?php
		print($acad_yr->sy);
	 ?></td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td rowspan="2" style="width:16%; text-align:center; vertical-align:middle;">DEPARTMENT </td>
    <td colspan="3" style="width:12%; text-align:center;">1</td>
    <td colspan="3" style="width:12%; text-align:center;">2</td>
    <td colspan="3" style="width:12%; text-align:center;">3</td>
    <td colspan="3" style="width:12%; text-align:center;">4</td>
    <td colspan="3" style="width:12%; text-align:center;">5</td>
    <td colspan="3" style="width:12%; text-align:center;">6</td>
    <td colspan="3" style="width:12%; text-align:center;">7</td>
    <td colspan="3" style="width:12%; text-align:center;">8</td>
    <td colspan="3" style="width:12%; text-align:center;">9</td>
    <td colspan="3" style="width:12%; text-align:center;">10</td>
    <td colspan="3" style="width:12%; text-align:center;">11</td>
    <td colspan="3" style="width:12%; text-align:center;">12</td>
    <td colspan="3" style="width:12%; text-align:center;">DEPARTMENT TOTAL</td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="2%" style="text-align:center; width:3%;">F</td>
    <td width="4%" style="text-align:center; width:3%;">Total</td>
    <td width="2%" style="text-align:center; width:3%;">M</td>
    <td width="9%" style="text-align:center; width:3%;">F</td>
    <td width="11%" style="text-align:center; width:3%;">Total</td>
  </tr>
	</thead>
  <?php
  		if (isset($students)) { 
  			foreach($students AS $student) {
  			
  				if ($student['id'] != 0) {

   ?>
  <tr>
    <td style="text-align:left; padding-left:30px;"><?php print($student['level']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m1']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f1']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total1']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id1_<?php print($student['id']); ?>" />
  			<?php print($student['total1']); ?></a>

			<div class="modal hide fade" id="modal_id1_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="1" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent1_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id1_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent1_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent1_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total1']); 
				}
			?>

	<!--  -->    	
    </td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m2']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f2']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total2']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id2_<?php print($student['id']); ?>" />
  			<?php print($student['total2']); ?></a>

			<div class="modal hide fade" id="modal_id2_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="2" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent2_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id2_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent2_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent2_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total2']); 
				}
			?>

	<!--  -->    	
    </td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m3']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f3']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total3']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id3_<?php print($student['id']); ?>" />
  			<?php print($student['total3']); ?></a>

			<div class="modal hide fade" id="modal_id3_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="3" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent3_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id3_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent3_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent3_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total3']); 
				}
			?>

	<!--  -->    	
	</td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m4']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f4']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total4']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id4_<?php print($student['id']); ?>" />
  			<?php print($student['total4']); ?></a>

			<div class="modal hide fade" id="modal_id4_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="4" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent4_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id4_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent4_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent4_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total4']); 
				}
			?>

	<!--  -->    	
    </td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m5']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f5']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total5']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id5_<?php print($student['id']); ?>" />
  			<?php print($student['total5']); ?></a>

			<div class="modal hide fade" id="modal_id5_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="5" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent5_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id5_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent5_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent5_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total5']); 
				}
			?>

	<!--  -->    	
	 </td>
	<td style="text-align:center; color:#0000CC;"><?php print($student['m6']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f6']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total6']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id6_<?php print($student['id']); ?>" />
  			<?php print($student['total6']); ?></a>

			<div class="modal hide fade" id="modal_id6_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="6" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent6_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id6_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent6_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent6_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total6']); 
				}
			?>

	<!--  -->    	
    </td>
	<td style="text-align:center; color:#0000CC;"><?php print($student['m7']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f7']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total7']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id7_<?php print($student['id']); ?>" />
  			<?php print($student['total7']); ?></a>

			<div class="modal hide fade" id="modal_id7_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="7" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent7_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id7_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent7_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent7_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total7']); 
				}
			?>

	<!--  -->    	
	<td style="text-align:center; color:#0000CC;"><?php print($student['m8']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f8']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total8']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id8_<?php print($student['id']); ?>" />
  			<?php print($student['total8']); ?></a>

			<div class="modal hide fade" id="modal_id8_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="8" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent8_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id8_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent8_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent8_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total8']); 
				}
			?>

	<!--  -->    	
	<td style="text-align:center; color:#0000CC;"><?php print($student['m9']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f9']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total9']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id9_<?php print($student['id']); ?>" />
  			<?php print($student['total9']); ?></a>

			<div class="modal hide fade" id="modal_id9_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="9" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent9_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id9_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent9_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent9_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total9']); 
				}
			?>

	<!--  -->    	
	<td style="text-align:center; color:#0000CC;"><?php print($student['m10']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f10']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total10']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id10_<?php print($student['id']); ?>" />
  			<?php print($student['total10']); ?></a>

			<div class="modal hide fade" id="modal_id10_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="10" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent10_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id10_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent10_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent10_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total10']); 
				}
			?>

	<!--  -->    	
    </td>
	<td style="text-align:center; color:#0000CC;"><?php print($student['m11']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f11']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total11']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id11_<?php print($student['id']); ?>" />
  			<?php print($student['total11']); ?></a>

			<div class="modal hide fade" id="modal_id11_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="11" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent11_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id11_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent11_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent11_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total11']); 
				}
			?>

	<!--  -->    	
    </td>
	<td style="text-align:center; color:#0000CC;"><?php print($student['m12']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f12']); ?></td>
    <td style="text-align:center; font-weight:bold;">
	<!--  -->

	<?php 
		if ($student['total12']) {
	?>
		<a href="#" data-toggle="modal" data-target="#modal_id12_<?php print($student['id']); ?>" />
  			<?php print($student['total12']); ?></a>

			<div class="modal hide fade" id="modal_id12_<?php print($student['id']); ?>" 
				selected_year="<?php print($selected_year); ?>" 
				prog_id="<?php print($student['id']); ?>" 
				yr_level="12" style="width:750px; margin-left: -375px;">
			   	<div class="modal-header">
			       <button type="button" class="close" data-dismiss="modal">�</button>
			       <h3>List of Students</h3>
			   	</div>
				<div class="modal-body">            
					<div id="modalContent12_<?php print($student['id']); ?>" >
						text here
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</div>
			 
			<script>
				$('#modal_id12_<?php print($student['id']); ?>').on('show', function(){
					  var prog_id = $(this).attr('prog_id');
					  var selected_year = $(this).attr('selected_year');
					  var yr_level = $(this).attr('yr_level');
					  $('#modalContent12_<?php print($student['id']); ?>').html('loading...')
				
					  $.ajax({
					      cache: false,
					      type: 'GET',
					      url: '<?php echo site_url($this->uri->segment(1).'/get_basic_ed_students');?>',
					      data: {prog_id: prog_id,selected_year: selected_year,yr_level: yr_level},
					      success: function(data) {
					        $('#modalContent12_<?php print($student['id']); ?>').html(data); //this part to pass the var
					      }
					  });
					})
			</script>						
			 
			<?php
				} else {
					print($student['total12']); 
				}
			?>

	<!--  -->    	
    </td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['total_m']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['total_f']); ?></span></td>
    <td style="text-align:center; font-weight:bold;"><?php print($student['dept_total']); ?></td>
    
    
  </tr>
  <?php 
  		} else { 
  ?>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td style="text-align:right;">GRAND TOTAL: </td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m1']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f1']); ?></td>
    <td style="text-align:center;"><?php print($student['total1']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m2']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f2']); ?></td>
    <td style="text-align:center;"><?php print($student['total2']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m3']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f3']); ?></td>
    <td style="text-align:center;"><?php print($student['total3']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m4']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f4']); ?></td>
    <td style="text-align:center;"><?php print($student['total4']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m5']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f5']); ?></td>
    <td style="text-align:center;"><?php print($student['total5']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m6']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f6']); ?></td>
    <td style="text-align:center;"><?php print($student['total6']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m7']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f7']); ?></td>
    <td style="text-align:center;"><?php print($student['total7']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m8']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f8']); ?></td>
    <td style="text-align:center;"><?php print($student['total8']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m9']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f9']); ?></td>
    <td style="text-align:center;"><?php print($student['total9']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m10']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f10']); ?></td>
    <td style="text-align:center;"><?php print($student['total10']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m11']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f11']); ?></td>
    <td style="text-align:center;"><?php print($student['total11']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($student['m12']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($student['f12']); ?></td>
    <td style="text-align:center;"><?php print($student['total12']); ?></td>
    <td style="text-align:center;"><span style="text-align:center; color:#0000CC;"><?php print($student['total_m']); ?></span></td>
    <td style="text-align:center;"><span style="text-align:center; color:#FF0000;"><?php print($student['total_f']); ?></span></td>
    <td style="text-align:center;"><?php print($student['grand_total']); ?></td>
  </tr>
   <?php
	  			}	
	  		}
  		}
  ?>
</table>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding: 2px; margin-bottom:20px;">

	<table style="width:100%; height:auto;">
	<tr>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:20%; padding:3px;">
	<!-- <a class="download_enroll" href="" academic_years_id="<?php print($acad_yr->id); ?>"><i class="icon-download-alt"></i>Download to PDF</a> -->
	<a href="<?php print(base_url('downloads/Basic Education Enrollment Summary '.$acad_yr->sy.'.xls')); ?>" class="btn btn-link" id="download_summary">
	<i class="icon-download-alt"></i> Download as Excel</a>
	</td>
	<td style="text-align:right; vertical-align:top;">&nbsp;</td>
	</tr>
	</table>						
</div>

</div>


<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 2,
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>



  <form id="download_enroll_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.download_enroll').click(function(event){
		event.preventDefault(); 
		var academic_years_id = $(this).attr('academic_years_id');
			
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_years_id',
			value: academic_years_id,
		}).appendTo('#download_enroll_summary_form');
		$('#download_enroll_summary_form').submit();
		
	});
</script>
