<?php 

class student_clearance extends Print_lib {
	private $page_width;
	private $page_height;
	private $overhead;
	private $name;
	private $idnumber;
	private $course_year;
	private $ledger_data;
	private $lab_courses;
	private $offices_clearance;
	private $print_count_clearance;
	
	private $header;
	
	public function __construct($idnumber, $name, $course_year, $ledger_data, $lab_courses=array(),$print_count_final,$is_SHS){
		$this->overhead = 11;
		
		$this->idnumber = $idnumber;
		$this->name = $name;
		$this->course_year = $course_year;
		$this->ledger_data = $ledger_data;
		$this->print_count_clearance = $print_count_final;
		////log_message("INFO", "CONSTRUCT   ===> ".print_r($this->print_count_final,true)); // Toyet 10.2.2018
		
		$this->header = array(
				array('id'=>'date', 'name'=>'Date', 'width'=>12, 'align'=>'center'),
				array('id'=>'transaction', 'name'=>'Transaction', 'width'=>30, 'align'=>'left'),
				array('id'=>'reference', 'name'=>'Reference', 'width'=>17, 'align'=>'right'),
				array('id'=>'debit', 'name'=>'DR/Charges', 'width'=>15, 'align'=>'right'),
				array('id'=>'credit', 'name'=>'CR/Payments', 'width'=>15, 'align'=>'right'),
				array('id'=>'balance', 'name'=>'Balance', 'width'=>15, 'align'=>'right'),
				array('id'=>'clearance', 'name'=>'Clearance', 'width'=>26, 'align'=>'left'),
		);

		if($is_SHS){
			$offices_clearance = array(
					'Lib',
					'Bkstore',
					"SSG",
					"SAO",
					"Asst Principal",
					"GH/WS/Acad Scholar",); // added ra for working scholar: TODO: filter only for actual working
		} else {
			$offices_clearance = array(
					'Lib',
					'Bkstore',
					"CSG",
					"SAO",
					"Dean",
					"GH/WS/Acad Scholar",); // added ra for working scholar: TODO: filter only for actual working
		}


		$this->offices_clearance = array_merge($offices_clearance, $lab_courses);
		$clearance_lines = array();
		foreach ($this->offices_clearance as $office){
			$clearance_lines[] = str_pad($office.":", $this->header[6]['width'], " ", STR_PAD_RIGHT);
			$clearance_lines[] = str_pad("", $this->header[6]['width'], "_", STR_PAD_RIGHT);
		}
		$this->offices_clearance = $clearance_lines;
		$this->lab_courses = $lab_courses;
		
		$this->page_width = 136;
		$this->page_height = 33;
	}
	
	public function align_right($string){
		return parent::align_right($string, $this->page_width);
	}
	public function header (){
		$content = $this->align_right($this->idnumber)."\n";
		$content .= $this->align_right($this->name)."\n";
		$content .= $this->align_right($this->course_year)."\n";
		$content .= $this->bold().$this->align_right('STUDENT CLEARANCE'). $this->regular()."\n";
		return $content;
	}
	public function clearance_ledger (){
		$header_double_lines = '';
		$header = "";
		$running_balance = 0;
		$ledger_data = array();
		//prepare ledger data...
		foreach($this->ledger_data as $key=>$ledge){
			$running_balance += $ledge->debit - $ledge->credit;//debit balance...
			$ledger_data[] = array(
					'type'=>'ledger_data',
					'date'=>$ledge->transaction_date,
					'transaction'=>$ledge->transaction_detail,
					'reference'=>isset($ledge->reference_number) && !empty($ledge->reference_number) ? (is_numeric($ledge->reference_number) ? 'OR#' . $ledge->reference_number: $ledge->reference_number): '',
					'debit'=>number_format((float)$ledge->debit, 2),
					'credit'=>number_format((float)$ledge->credit, 2),
					'balance'=>($running_balance < 0 ? "(" . number_format(abs($running_balance), 2) . ")" : number_format($running_balance, 2)),
			);
		}
		$ledger_data[] = array(
				'type'=>'horiz_line',
				);
		$ledger_data[] = array(
				'type'=>'final',
				'data'=>number_format(($running_balance > 0 ? $running_balance : 0), 2),
				);
		$ledger_data[] = array(
				'type' => 'none',
				'value'=> '',
		);
		$ledger_data[] = array(
				'type' => 'none',
				'value'=> '',
				);
		$ledger_data[] = array(
				'type' => 'none',
				'value'=> '_______________________',
				);
		$ledger_data[] = array(
				'type' => 'none',
				'value'=> '    Accounts Clerk    ',
		);
		if(count($ledger_data) > count($this->offices_clearance))
			$max_count = count($ledger_data); else
			$max_count = count($this->offices_clearance);
		//log_message("INFO",print_r($this->offices_clearance,true));
		////log_message("INFO",print_r($ledger_data,true));
		$need_extra_lab_space = false;
		$start_extra_lab_space = false;
		$pointer_extra_lab_space = 1;
		if(count($this->offices_clearance)>22){
			$pointer_extra_lab_space = 21;
			$need_extra_lab_space = true;
			$max_count = 22;
			$extra_count = count($this->offices_clearance) - $max_count;
			////log_message("INFO",print_r($extra_count,true));
		}
		
		foreach ($this->header as $head){
			$head = (object)$head;
			$pad_with = $head->id=='balance' ? "|":" ";
			$header_double_lines .= str_pad('', $head->width, '=', STR_PAD_BOTH).$pad_with;
			$header .= str_pad($head->name, $head->width, ' ', STR_PAD_BOTH). $pad_with;
		}
		$all_header = "{$header_double_lines}\n{$header}\n{$header_double_lines}\n";
		$content = $all_header;
		for ($count=0; $count<$max_count; $count++){
			if (isset($ledger_data[$count]['type']) && $ledger_data[$count]['type']=='ledger_data'){
				foreach ($this->header as $head){
					$head = (object)$head;
					switch ($head->align){
						case 'center'	: $pad=STR_PAD_BOTH; break;
						case 'right'	: $pad=STR_PAD_LEFT; break;
						default			: $pad=STR_PAD_RIGHT; break;
					}
					$pad_with = $head->id=='balance' ? "|" : " ";
					if($head->id!='clearance'){
						$content .= (isset($ledger_data[$count][$head->id]) ? str_pad(substr($ledger_data[$count][$head->id], 0, $head->width), $head->width, " ", $pad) . $pad_with :
								str_pad("", $head->width+1, " ", STR_PAD_BOTH));
					} else {
						$content .= isset($this->offices_clearance[$count]) ? $this->offices_clearance[$count] : "";
					}	
				}
				$content .= "\n";
			} elseif (isset($ledger_data[$count]['type']) && $ledger_data[$count]['type']=='horiz_line'){
				foreach ($this->header as $head){
					$head = (object)$head;
					switch ($head->align){
						case 'center'	: $pad=STR_PAD_BOTH; break;
						case 'right'	: $pad=STR_PAD_LEFT; break;
						default			: $pad=STR_PAD_RIGHT; break;
					}
					$pad_with = $head->id=='balance' ? "|" : "-";
					if($head->id!='clearance'){
						$content .= str_pad("", ($head->width), "-").$pad_with;
					} else {
						$content .= isset($this->offices_clearance[$count]) ? $this->offices_clearance[$count] : "";
					}
				}
				$content .= "\n";
			} elseif(isset($ledger_data[$count]['type']) && $ledger_data[$count]['type']=='final'){
				$total_pad = 0;
				foreach($this->header as $head){
					if (!in_array($head['id'], array('clearance'))){
						$total_pad += $head['width']+1; 
					}
				}
				$content .= $this->bold().str_pad("Please pay this amount for FINALS:  P " . $ledger_data[$count]['data'], $total_pad-1, " ", STR_PAD_LEFT). $this->regular()."|";
				$content .= isset($this->offices_clearance[$count]) ? $this->offices_clearance[$count] : "";
				$content .= "\n";
			} elseif(isset($ledger_data[$count]['type']) && $ledger_data[$count]['type']=='none'){
				$total_pad = 0;
				foreach($this->header as $head){
					if (!in_array($head['id'], array('clearance'))){
						$total_pad += $head['width']+1; 
					}
				}
				$content .= str_pad($ledger_data[$count]['value'], $total_pad-1, " ", STR_PAD_LEFT)."|";
				$content .= isset($this->offices_clearance[$count]) ? $this->offices_clearance[$count] : "";
				$content .= "\n";
				$start_extra_lab_space = true;
			} else { 
				$total_pad = 0;
				foreach($this->header as $head){
					if (!in_array($head['id'], array('clearance'))){
						$total_pad += $head['width']+1;
					}
				}
				if($start_extra_lab_space){
					if($need_extra_lab_space){ 
						$edging = "|";
						if($pointer_extra_lab_space==21)
							$edging = " ";
						$content .= str_pad("", $total_pad-1-27, " ", STR_PAD_LEFT).$edging;
						$content .= isset($this->offices_clearance[$pointer_extra_lab_space]) ? $this->offices_clearance[$pointer_extra_lab_space] : "";
						$content .= "|";
						$content .= isset($this->offices_clearance[$count]) ? $this->offices_clearance[$count] : "";
						$pointer_extra_lab_space = $pointer_extra_lab_space+1;
						if($pointer_extra_lab_space>=count($this->offices_clearance)){
							$need_extra_lab_space = false;
							$start_extra_lab_space = false;
						}
					} else {
						$content .= str_pad("", $total_pad-1, " ", STR_PAD_LEFT)."|";
						$content .= isset($this->offices_clearance[$count]) ? $this->offices_clearance[$count] : "";			
					}
				} else {
					$content .= str_pad("", $total_pad-1, " ", STR_PAD_LEFT)."|";
					$content .= isset($this->offices_clearance[$count]) ? $this->offices_clearance[$count] : "";			
				}
				$content .= "\n"; 
			}
		}
		return $content;
	}
	
	public function footer (){
		$content = "\n";
		$content .= "Prints:[<span id='final_counter'>".$this->print_count_clearance."</span>]";
		$content .= $this->line_feed();
		$content .= "Student Clearance. Printed by HNUMIS. " . date('l, F j, Y h:i:s A');
		return $content;
	}

	public function content (){
		$content = $this->init().$this->condensed();
		$content .= $this->set_page_length_in_lines($this->page_height);
		$content .= $this->header();
		
		foreach (explode("\n", $this->clearance_ledger()) as $key=>$line){
			$content .= $line . "\n";
		}
		for($count = 0; $count <= ($this->page_height - ($this->overhead+$key+1)); $count++){
			$content .= "\n";
		}
		$content .= $this->footer().$this->no_condensed().$this->char_form_feed();
		return $content;
	}
}
?><div id="print_clearance" style="display:none;"><?php 
if(!isset($print_count_final))
	$print_count_final = 0;
$clearance = new student_clearance($idnumber, $name, $course_year, $ledger_data, $lab_courses, $print_count_final, $is_SHS);
echo $clearance->content();
?></div>
