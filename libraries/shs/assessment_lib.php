<?php

	class assessment_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		private $student_count = 0;
		private $posting_msg   = 0;

		function get_student_count() {
			return $this->student_count;
		}
		
		function get_posting_msg() {
			return $this->posting_msg;
		}

		
		public function assessment_posting_shs() {
		
			$this->CI->load->model('hnumis/AcademicYears_Model');
			$this->CI->load->model('hnumis/shs/track_strand_model');
			$this->CI->load->model('hnumis/shs/shs_student_model');
			$this->CI->load->model('hnumis/shs/shs_assessments_model');
			$this->CI->load->model('hnumis/shs/section_model');
			$this->CI->load->model('teller/assessment_model');
			
			$data['terms_sy'] = $this->CI->AcademicYears_Model->getCurrentAcademicTerm();
			$data['strands']  = $this->CI->track_strand_model->ListStrandsByLatestProspectus();
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':
		
					break;
					
				case 'post_assessment':
				
					if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){

						$data['academic_terms_id'] = $data['terms_sy']->id; 
						
						foreach ($this->CI->input->post('strands') AS $k=>$strand) {
				
						//log_message("INFO",'STRANDS'); // Toyet 6.28.2018
						//log_message("INFO",print_r($this->CI->input->post('strands'),true)); // Toyet 6.28.2018
							$data['prospectus_id'] = $strand;
							$data['academic_programs_id'] = $strand; //value of $strand here is replace with
						                                         //academic_programs_id to get the actual
											//prospectus assigned to student during
											//enrollment - Toyet 6.28.2018							



							foreach ($this->CI->input->post('grade_level') AS $k=>$year_level) {
								$data['year_level'] = $year_level;
								
								if ($this->post_assessments($data)) {
									$this->CI->content_lib->set_message('No. of assessed students: '.$this->student_count, 'alert-success');								
								} else {
									$this->CI->content_lib->set_message('Error on Posting assessments!<br>'.$this->posting_msg, 'alert-error');
								}							
							}	
						}
					
					} else {
						$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');	
					}
					
					break;

					
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/assessment_posting',$data);
			$this->CI->content_lib->content();
				
		}
		
		
		private function post_assessments($data) {
							
			$student_histories = $this->CI->shs_assessments_model->ListStudentsToAssess($data);
			//print_r($student_histories); die();				
			if ($student_histories) {

				$this->CI->db->trans_start();

				foreach ($student_histories AS $student) {
					
					$this->student_count++;
					
					$data['acad_program_groups_id'] = $student->acad_program_groups_id;
					$data['academic_terms_id']      = $data['terms_sy']->id;
					$data['grade_level']            = $student->year_level;
					$data['block_sections_id']      = $student->block_sections_id;
					$data['academic_years_id']      = $data['terms_sy']->academic_years_id;
					
					$basic      = $this->CI->shs_student_model->getStudentTuitionBasicRate($data);
					$pay_units  = $this->CI->shs_student_model->getTotalPayingUnits($student->student_histories_id);
					$misc_fee   = $this->CI->shs_student_model->getMyMisc_Fee($data);
					$lab_fees   = $this->CI->shs_student_model->getMyLabFees($data['terms_sy']->academic_years_id, $student->student_histories_id);
					
					$data['assessed_amount']      = ($basic->rate*$pay_units->p_units) + $misc_fee->rate + $lab_fees->rate ;
					$data['student_histories_id'] = $student->student_histories_id;
					$data['employees_empno']      = $this->CI->session->userdata('empno');
					$data['year_level']           = $student->year_level;
					$data['academic_programs_id'] = $student->academic_programs_id;
					
					
					if ($this->CI->shs_assessments_model->AddAssessment($data)) {
					
						$data['assessments_id'] = $this->CI->shs_assessments_model->get_assessments_id();
						
						$courses = $this->CI->section_model->ListCoursesAssigned($student->block_sections_id); 
						
						if ($courses) {
							foreach ($courses AS $course) {
								$data['course_code']  = $course->course_code;
								$data['paying_units'] = $course->paying_units;
								$data['rate']         = $basic->rate;
								$data['encoded_by']   = $this->CI->session->userdata('empno');
								
								if (!$this->CI->shs_assessments_model->AddAssessment_History_Courses($data)) {
									$this->posting_msg = "ERROR in adding data to assessment hitory courses table!";
									$this->CI->db->trans_rollback();
									return FALSE;
								}
								
								$data['student_history_id']  = $student->student_histories_id;
								$data['course_offerings_id'] = $course->course_offerings_id;
								$data['enrolled_by']         = $course->enrolled_by;
								$data['date_enrolled']       = $course->date_enrolled;
								$data['post_status']         = "posted";
								
								if (!$this->CI->shs_assessments_model->AddCourse_toEnrollments($data)) {
									$this->posting_msg = "ERROR in adding data to enrollments table!";
									$this->CI->db->trans_rollback();
									return FALSE;
								}
							}
						}
						
						$misc_fees = $this->CI->shs_student_model->ListMiscFees($data); 
						
						if ($misc_fees) {
							foreach($misc_fees AS $misc_fee) {
								$data['fees_groups_id'] = $misc_fee->fees_groups_id;
								
								if ($misc_fee->fees_subgroups) {							
									foreach($misc_fee->fees_subgroups AS $fee_subgroup) {
										$data['description'] = $fee_subgroup->description;
										$data['rate']        = $fee_subgroup->rate;
								
										if (!$this->CI->shs_assessments_model->AddAssessment_History_Fees($data)) {
											$this->posting_msg = "ERROR in adding data to assessment hitory fee table!";
											$this->CI->db->trans_rollback();
											return FALSE;
										}
									}
								}
							}
						} 
						
						$lab_fees = $this->CI->section_model->ListLabFees($data);
						
						if ($lab_fees) {
							foreach ($lab_fees AS $lab_fee) {
								$data['course_code']  = $lab_fee->course_code;
								$data['rate']         = $lab_fee->rate;
								$data['encoded_by']   = $this->CI->session->userdata('empno');
								
								if (!$this->CI->shs_assessments_model->AddAssessment_History_Lab($data)) {
									$this->posting_msg = "ERROR in adding data to assessment hitory lab table!";
									$this->CI->db->trans_rollback();
									return FALSE;
								}
							}
						}
						
					} else {
						$this->posting_msg = "ERROR in adding data to assessment table!";
						$this->CI->db->trans_rollback();
						return FALSE;
					}
					
				}
			}
			
			$this->CI->db->trans_complete();
			return TRUE;
			
		}
		

		public function shs_assessment_report_strand() {

			$this->CI->load->model('hnumis/AcademicYears_Model');
			$this->CI->load->model('hnumis/shs/track_strand_model');
			$this->CI->load->model('hnumis/shs/shs_assessments_model');
			$this->CI->load->model('academic_terms_model');

			$data['academic_terms'] = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
			$data['strands']        = $this->CI->track_strand_model->ListStrandsByLatestProspectus();
		    $data['current_term']   = $this->CI->academic_terms_model->getCurrentAcademicTerm();
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':
		
					break;
		
				case 'generate_report':
				
					if ($this->CI->input->post('grade_level')) {
						$grade_level = "Grade ".$this->CI->input->post('grade_level');
					} else {
						$grade_level = "All Grade Levels";
					}
					
					if ($this->CI->input->post('strand_id') == "All") {
						$description  = "All Strands";
						$abbreviation = "All Strands";
					} else {
						$description  = $this->CI->input->post('description');
						$abbreviation = $this->CI->input->post('abbreviation');
					}
					
					$students = $this->CI->shs_assessments_model->getStudentsEnrolled($this->CI->input->post('academic_terms_id'), $this->CI->input->post('strand_id'), $this->CI->input->post('grade_level'));
					
					$data = array(
								'num_students'=>$students->num_students,
								'grade_level'=>$grade_level,
								'description'=>$description,
								'abbreviation'=>$abbreviation,
								'term_sy'=>$this->CI->input->post('term_sy'),
							);
							
					$data['tuition_fees'] = $this->CI->shs_assessments_model->ListTuitionFees($this->CI->input->post('academic_terms_id'), $this->CI->input->post('strand_id'), $this->CI->input->post('grade_level'));

					$data['misc_fees']    = $this->CI->shs_assessments_model->ListAssessedMisc($this->CI->input->post('academic_terms_id'), $this->CI->input->post('strand_id'), $this->CI->input->post('grade_level'));

					$data['lab_fees']     = $this->CI->shs_assessments_model->ListAssessedLab($this->CI->input->post('academic_terms_id'), $this->CI->input->post('strand_id'), $this->CI->input->post('grade_level'));
					
					$this->CI->content_lib->enqueue_body_content('shs/reports/assessment_report_strand',$data);
					$this->CI->content_lib->content();

					return;
				
				case 'download_to_pdf':

					if ($this->CI->input->post('grade_level') != 0) {
						$tuition['title'] = $this->CI->input->post('abbreviation').'-'.$this->CI->input->post('grade_level').' ['.$this->CI->input->post('term_sy').']';						
					} else {
						$tuition['title'] = $this->CI->input->post('abbreviation').' ('.$this->CI->input->post('grade_level').') ['.$this->CI->input->post('term_sy').']';
					}
					
					$tuition['num_students'] = $this->CI->input->post('num_students');
					$tuition['tuition_fees'] = json_decode($this->CI->input->post('tuition_fees'));
					$tuition['misc_fees']    = json_decode($this->CI->input->post('misc_fees'));
					$tuition['lab_fees']     = json_decode($this->CI->input->post('lab_fees'));
					
					$this->generate_assessment_report_strand_pdf($tuition);
					
					return;
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/assessment_report_form',$data);
			$this->CI->content_lib->content();
				
		}


		public function shs_unposted_assessments() {
			$this->CI->load->model('hnumis/shs/shs_assessments_model');
			$this->CI->load->model('academic_terms_model');

		    $current_term     = $this->CI->academic_terms_model->getCurrentAcademicTerm();
			$data['students'] = $this->CI->shs_assessments_model->ListUnpostedAssessments($current_term->id);
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
			switch ($action) {
				case 'mainpage':
		
					break;
						
				case 'download_unposted_assessments_pdf':

					$data['students'] = json_decode($this->CI->input->post('students'));
					
					$this->generate_unposted_assessments_report_pdf($data);
					
					return;
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/reports/list_of_unposted_students',$data);
			$this->CI->content_lib->content();
			
		}
		
	
		
/**********************************************
*	PRIVATE FUNCTIONS
**********************************************/
		
		private function generate_assessment_report_strand_pdf($tuition) {

			$this->CI->load->library('my_pdf');
			$this->CI->load->library('hnumis_pdf');
			$l = 4;
		
			$this->CI->hnumis_pdf->AliasNbPages();		
			$this->CI->hnumis_pdf->set_HeaderTitle('POSTED ASSESSMENT REPORT FOR: '.$tuition['title']);
			$this->CI->hnumis_pdf->AddPage('P','folio');
			
			$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->CI->hnumis_pdf->SetDrawColor(200,200,200);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();
			
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->SetTextColor(10,10,10);

			$this->CI->hnumis_pdf->SetFont('times','B',12);
			$this->CI->hnumis_pdf->SetFillColor(200,200,200);
			
			$this->CI->hnumis_pdf->Cell(200,$l+2,'# of Students: '.$tuition['num_students'],0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->SetFillColor(255,255,255);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();

	  		$total_tuition = 0;
			
			if ($tuition['tuition_fees']) {
	  			$basic_total = 0;
	  			foreach ($tuition['tuition_fees'] AS $t_fee) {
					//$this->CI->hnumis_pdf->SetX(14);
					$this->CI->hnumis_pdf->Cell(145,$l,"Tuition Fee [".$t_fee->paying_units.' @ '.$t_fee->amt.'] ('.$t_fee->num_students.' students)',0,0,'L',true);
			  		$basic_total += ($t_fee->paying_units * $t_fee->amt);
					$this->CI->hnumis_pdf->Cell(22,$l,number_format(($t_fee->paying_units * $t_fee->amt),2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();
				}
				$total_tuition += $basic_total;	  
				//$this->CI->hnumis_pdf->SetX(14);
				$this->CI->hnumis_pdf->SetFont('times','B',10);
				$this->CI->hnumis_pdf->Cell(100,$l,'Tuition Fee',0,0,'L',true);
				$this->CI->hnumis_pdf->Cell(92,$l,number_format($basic_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->SetFont('times','',10);
			}

			$this->CI->hnumis_pdf->Ln();
			
			
			if ($tuition['misc_fees']) {
				$total_misc = 0;
				foreach($tuition['misc_fees'] AS $misc) { 
					//$this->CI->hnumis_pdf->SetX(14);						
					$this->CI->hnumis_pdf->SetFont('times','B',10);
					$this->CI->hnumis_pdf->Cell(200,$l+2,$misc->fees_group,0,0,'L',true);
					$this->CI->hnumis_pdf->SetFont('times','',10);
					$this->CI->hnumis_pdf->Ln();
					if ($misc->fees_groups) {
						foreach($misc->fees_groups AS $fgroup) {
							$this->CI->hnumis_pdf->SetX(14);						
							$this->CI->hnumis_pdf->Cell(145,$l,$fgroup->description." @ ".$fgroup->amt." (".$fgroup->num_students." students)",0,0,'L',true);
							$total_misc += $fgroup->num_students*$fgroup->amt;
							$this->CI->hnumis_pdf->Cell(18,$l,number_format($fgroup->num_students*$fgroup->amt,2),0,0,'R',true);
							$this->CI->hnumis_pdf->Ln();
						}
					}			
					$total_tuition += $total_misc; 
					$this->CI->hnumis_pdf->SetFont('times','B',10);
					$this->CI->hnumis_pdf->Cell(192,$l,number_format($total_misc,2),0,0,'R',true);
					$this->CI->hnumis_pdf->SetFont('times','',10);
					$total_misc = 0; 
					$this->CI->hnumis_pdf->Ln();
				}
			}

			
			
			if ($tuition['lab_fees']) {
				//$this->CI->hnumis_pdf->SetX(14);						
				$this->CI->hnumis_pdf->SetFont('times','B',10);
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Laboratory Fees:',0,0,'L',true);
				$this->CI->hnumis_pdf->SetFont('times','',10);
				$this->CI->hnumis_pdf->Ln();
	  			$lab_total = 0;
	  			foreach($tuition['lab_fees'] AS $lab_fee) {
					$this->CI->hnumis_pdf->SetX(14);						
					$this->CI->hnumis_pdf->Cell(145,$l,$lab_fee->course_code." @ ".$lab_fee->rate." (".$lab_fee->num_students." students)",0,0,'L',true);
					$lab_total += $lab_fee->rate*$lab_fee->num_students;
					$this->CI->hnumis_pdf->Cell(18,$l,number_format(($lab_fee->rate*$lab_fee->num_students),2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();				
				}
				$total_tuition += $lab_total;
				$this->CI->hnumis_pdf->SetFont('times','B',10);
				$this->CI->hnumis_pdf->Cell(192,$l,number_format($lab_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->SetFont('times','',10);
				$this->CI->hnumis_pdf->Ln();
			}
			
			
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->SetFont('times','B',12);
			$this->CI->hnumis_pdf->SetFillColor(200,200,200);
			$this->CI->hnumis_pdf->Cell(100,$l+2,'TOTAL Assessment: ',1,0,'L',true);
			$this->CI->hnumis_pdf->Cell(92,$l+2,number_format($total_tuition,2),1,0,'R',true);
			$this->CI->hnumis_pdf->Cell(9,$l+2,'',1,0,'R',true);

			$this->CI->hnumis_pdf->Output();
	
		}
		

		private function generate_unposted_assessments_report_pdf($data) {

			$this->CI->load->library('my_pdf');
			$this->CI->load->library('hnumis_pdf');
			$l = 4;
		
			$this->CI->hnumis_pdf->AliasNbPages();		
			$this->CI->hnumis_pdf->set_HeaderTitle('UNPOSTED ASSESSMENTS REPORT: SENIOR HIGH SCHOOL');
			$this->CI->hnumis_pdf->AddPage('P','folio');
			
			$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->CI->hnumis_pdf->SetDrawColor(200,200,200);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();
			
			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->SetTextColor(10,10,10);

			$this->CI->hnumis_pdf->SetFillColor(200,200,200);
			
			$this->CI->hnumis_pdf->SetY(30);
			$this->CI->hnumis_pdf->SetX(5);

			$header1 = array('Count','ID No.','Name','Strand','Grade Level');

			$w1 = array(15,25,80,55,25);
			
			for($i=0;$i<count($w1);$i++)
				$this->CI->hnumis_pdf->Cell($w1[$i],8,$header1[$i],1,0,'C',true);

			$this->CI->hnumis_pdf->Ln();
			$l=7;

			$this->CI->hnumis_pdf->SetFillColor(255,255,255);
			$this->CI->hnumis_pdf->SetTextColor(0,0,0);
			$this->CI->hnumis_pdf->SetFont('Arial','',9);

			if ($data['students']) {
				$cnt=1;
				foreach($data['students'] AS $student) {
					$this->CI->hnumis_pdf->SetX(5);
					$this->CI->hnumis_pdf->Cell($w1[0],$l,$cnt.'.',1,0,'R',true);
					$this->CI->hnumis_pdf->Cell($w1[1],$l,$student->idno,1,0,'C',true);
					$this->CI->hnumis_pdf->Cell($w1[2],$l,$student->lname.', '.$student->fname,1,0,'L',true);
					$this->CI->hnumis_pdf->Cell($w1[3],$l,$student->abbreviation,1,0,'L',true);
					$this->CI->hnumis_pdf->Cell($w1[4],$l,$student->year_level,1,0,'C',true);
					$this->CI->hnumis_pdf->Ln();
					$cnt++;
				}
			}
			
			
			$this->CI->hnumis_pdf->Output();
	
		}
		
	}
	
?>
