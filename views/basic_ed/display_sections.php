<h2 class="heading" style="margin-top:20px;">Class List <?php echo $academicyear->sy?></h2>
<h2 class="heading" style="margin-top:20px;"> <?php echo $level->level ?> </h2>

<form action="<?php echo site_url('rsclerk/class_list');?>" method="post" id="class_list_form">
	<input type="hidden" name="step" value="display_class_list" />
 	<input type="hidden" name="academicyear" value="<?php echo $academicyear->id ?>">
	<input type="hidden" name="level" value="<?php echo $level->id ?>">

		<?php if ($advisers) { 	?>
			<div style="width:60%; margin-bottom:40px; margin-left:40px;">
			  <table id="advisers_list" border="0" cellspacing="0" class="table table-condensed table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
			    <thead>
				    <tr>
				      <th width="10%" style="vertical-align:middle; text-align:center;">Grade/<br>Year Level</th>
				      <th width="30%" style="vertical-align:middle; text-align:center;">Section Name</th>
				   	  <th width="37%"  style="vertical-align:middle; text-align:center;">Adviser</th>
				      <th width="13%"  style="vertical-align:middle; text-align:center;">Room</th>
				      <th width="10%"  style="vertical-align:middle; text-align:center;">View<br>Details</th>
				    </tr>
			    </thead>		
				<?php foreach ($advisers AS $adviser) { ?>
					    <tr>
					      <td style="text-align:center; vertical-align:middle; "><?php print($adviser->yr_level); ?> </td>
					      <td style="text-align:left; vertical-align:middle; "><?php print($adviser->section_name); ?> </td>
					      <td style="text-align:left; vertical-align:middle; "><?php print($adviser->name); ?> </td>
					      <td style="text-align:center; vertical-align:middle; "><?php print($adviser->room); ?> </td>
					      <td style="text-align:center; vertical-align:middle;">
      						<a class="printlist" href="<?php print($adviser->yr_level); ?>" 
      							basic_ed_sections_id="<?php print($adviser->basic_ed_sections_id); ?>"
      							section_name="<?php print($adviser->section_name); ?>"
      							class_adviser="<?php print($adviser->name); ?>"
      							room="<?php print($adviser->room); ?>" 
      							acad_sy="<?php print($academicyear->sy); ?>" >
        					<i class="icon-zoom-in"></i></a></td>					
							</td>
					   	</tr>
				<?php } ?>
			  </table>
			</div>
		<?php } ?>
</form>

<script>
	$('.printlist').click(function(event){
		event.preventDefault(); 
		var yr_level = $(this).attr('href');
		var basic_ed_sections_id = $(this).attr('basic_ed_sections_id');
		var section_name = $(this).attr('section_name');
		var class_adviser = $(this).attr('class_adviser');
		var room = $(this).attr('room');
		var acad_sy = $(this).attr('acad_sy');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'yr_level',
		    value: yr_level ,
		}).appendTo('#class_list_form');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'basic_ed_sections_id',
		    value: basic_ed_sections_id ,
		}).appendTo('#class_list_form');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'section_name',
		    value: section_name ,
		}).appendTo('#class_list_form');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'class_adviser',
		    value: class_adviser ,
		}).appendTo('#class_list_form');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'room',
		    value: room ,
		}).appendTo('#class_list_form');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'acad_sy',
		    value: acad_sy ,
		}).appendTo('#class_list_form');

		$('#class_list_form').submit();
	});
</script>



