<?php
class Student_Grades extends print_lib {
	private $content="";
	private $width = 78;
	public function __construct($headers){
		$this->set_table_header ($headers);
		$this->append_content($this->init());
		$this->append_content($this->set_page_length_in_lines(33));
	}

	public function add_student_data ($header_information, $data){
		$title_len = 0;
		$row_count = 0;
		$increment = 0;
		$right_limit = 28;
		$page_lines_max = 33;
		$subject_rows = 0;

		foreach($data as $row){

			$title_len = strlen($row['title']);

			$increment1 = $title_len / $right_limit;
			$increment2 = $increment1 - intval($increment1);
			$subject_rows = $subject_rows + intval($increment1);

			if($increment1<=2){
				if($increment2>0 and $increment2<1)
					$subject_rows++;
			}
		}

		//log_message("INFO",print_r(count($header_information['graduated_text']),true));
		$header_rows = 8;
		$footer_rows = 7;
		$graduated_rows = count($header_information['graduated_text']);
		$row_count = $subject_rows + $graduated_rows + 
		             $header_rows + $footer_rows;
		//log_message("INFO", $header_information['fullname'].'==>'.print_r($row_count,true)); 
		if($row_count>33){
			$page_lines_max = 66;
		} else {
			$page_lines_max = 33;
		}

		$this->set_data($data);
		$data = $header_information;
		$this->append_content ($this->align_right($data['idnumber']));
		$this->append_content ($this->align_right($data['fullname']));
		$this->append_content ($this->align_right($data['year_level'] . " " . $data['course_that_term']));
		$this->append_content ($this->align_right($data['term_that_time']));
		$this->append_content ($this->bold() . $this->align_right('Student Grades'));
		$table =  $this->regular(). $this->draw_table();
		//$row_count = count(explode("\n", $table));
		$this->append_content($table);

		if(count($header_information['graduated_text'])>0){
			foreach($header_information['graduated_text'] as $row){				
				$this->append_content($row . "\n");
			}
		}
		$lines_left = $page_lines_max - $row_count;
		//if($page_lines_max==66)
		//	$lines_left += 2;

// edited by ra 103014, Toyet 1.8.2019
		$this->content .= "\n";
		$this->append_content($this->align_right('____________________'));
		$this->append_content(str_pad('', 58, ' ') . str_pad('Registrar', 20, ' ', STR_PAD_BOTH) . "\n");

		if($lines_left>0){
			$this->content .= "\n";
			$this->append_content($this->footer());	
			for ($count = 0; $count < $lines_left-1; $count++){
				//$this->content .= ($count+1)."\n";
				$this->content .= "\n";
			}
		} else {
			$this->append_content($this->footer());	
		}
		// $this->content .= "> lines_left=".$lines_left."/page_lines_max=".
		// 				  $page_lines_max."/row_count=".$row_count."/subject_rows=".$subject_rows.
		// 				  "/graduated_rows=".$graduated_rows."\n";
		$this->content .= ">\n";

	}

	public function footer (){
		$content = "Student's Grades printed by HNUMIS. " . date(DATE_RFC822) . "\n";
		//$content .= $this->char_form_feed();
		$content .= $this->line_feed();  // Toyet 8.6.2018
		return $content;
	}
	public function align_right($line){
		return str_pad($line, $this->width, ' ', STR_PAD_LEFT) . "\n";
	}
	public function append_content($content){
		////log_message("INFO",print_r($content,true)); // Toyet 8.6.2018
		$this->content .= $content;
	}
	public function draw_table($header=TRUE,$footer=TRUE){
		$table = "";
		$border = "";
		$dcontent = "";
		$def = array();
		$grade_rows = array();
		foreach ($this->headers as $header){
			$border .= str_pad('', floor($header['width'] * $this->width/100), '-', STR_PAD_RIGHT) . " ";
			$dcontent .= str_pad($header['name'],floor($header['width'] * $this->width/100), ' ', STR_PAD_RIGHT) . " ";
			$def[] = ceil($header['width'] * $this->width/100);
		}
		foreach ($this->data as $row){
			$grade_rows[] = array(
					(object)array('content' => $row['catalog'], 'align'=>'left',),
					(object)array('content' => $row['section'], 'align'=>'center',),
					(object)array('content' => $row['title'], 'align'=>'left',),
					(object)array('content' => $row['units'], 'align'=>'center',),
					(object)array('content' => $row['prelim'], 'align'=>'center',),
					(object)array('content' => $row['midterm'], 'align'=>'center',),
					(object)array('content' => $row['finals'], 'align'=>'center',)
			);
		}
		if($header){
			$table = $border ."\n" . $dcontent . "\n" . $border . "\n";
		}
		$table .= $this->table($def, $grade_rows); //. "\n" . $border. "\n";
		if($footer){
			$table .= $border. "\n";
		}
		return $table;

	}
	public function content($return=FALSE){
		if($return)
			return $this->content; else
			echo $this->content;
	}

	public function stringToLines($text,$width=80){
		$line = ""; 
		$converted = array(); 
		$text_array = explode(' ',$text); 
		//log_message("INFO","TEXT ARRAY  ==>  ".print_r($text_array,true)); // Toyet 11.24.2018
		// as of 12.11.2018, the value of $text_array is correct - Toyet 12.11.2018
		$Len = 0; 
		$_i = 0; 
		$_iMax = count($text_array); 
		foreach ($text_array as $word){ 
			$_i++; 
			$Len = strlen($line); 
			$Len += strlen($word); 
			if($Len<$width){ 
				$line .= $word.' '; 
			} else { 
				$Len = 0;
				if($_i<=$_iMax){
					$converted[] = $line;
					$line = $word.' ';
				} else {
					$line .= $word;
					$converted[] = $line;
				}
				
			}	
			//log_message("INFO","LINE  ==>  ".print_r($line,true)); // Toyet 12.11.2018
		}
		$converted[] = $line; // add the last line generated: added by Toyet 12.11.2018
		//log_message("INFO","CONVERTED  ==>  ".print_r($converted,true)); // Toyet 11.24.2018
		//die();
		return $converted;
	}
}
////log_message("INFO", print_r($students,true)); // Toyet 8.6.2018
$headers = array(
				array('id'=>'catalog', 'name'=>'Catalog #', 'width'=>20),
				array('id'=>'section', 'name'=>"", 'width'=>5),
				array('id'=>'title', 'name'=>"Descriptive Title", 'width'=>35),
				array('id'=>'units', 'name'=>"Units", 'width'=>10),
				array('id'=>'prelim', 'name'=>"Prelim", 'width'=>10),
				array('id'=>'midterm', 'name'=>"Midterm", 'width'=>10),
				array('id'=>'finals', 'name'=>"Finals", 'width'=>10)
		);
?>
<div id="to_print" style="display:none;"><?php
$student_grades_print = new Student_Grades($headers);
//print_r($students); die();
$graduated_program_text = array();
foreach ($students as $idno => $grades){
	$data = array();
	$graduated_program_text = array();
	if($graduated_text[$idno]){
		$graduated_program_text = $student_grades_print->stringToLines("Graduated: ".$graduated_text[$idno]);
		//log_message("INFO",print_r($graduated_program_text,true)); // Toyet 11.26.2018
	}
		$header_information = array(
			'idnumber'=>$idno,
			'fullname'=>$grades[0]->fullname,
			'year_level'=>$grades[0]->year_level,
			'course_that_term'=>$grades[0]->academic_program,
			'term_that_time'=>$grades[0]->term . " SY " . $grades[0]->sy,
			'graduated_text'=>$graduated_program_text,
		);
	//print_r($grades); die(0); 
	foreach($grades as $grade){
		$data[] = array(
				'catalog'=>$grade->course_code,
				'section'=>$grade->section_code,
				'title'=>$grade->descriptive_title,
				'units'=>$grade->units,
				'prelim'=>$grade->prelim_grade,
				'midterm'=>$grade->midterm_grade,
				'finals'=>$grade->finals_grade,
		);
	}

	$student_grades_print->add_student_data($header_information, $data);
}
echo $student_grades_print->content(TRUE);

?></div>
