
<?php 
	$with_priv_pdf=json_encode($with_privileges);
	// we need to replace (') single quotes into (`) back tic as JSON does not accept it - Toyet 12.19.2018
	$with_priv_pdf=str_replace("'","`",$with_priv_pdf);
	//log_message("INFO",print_r($with_priv_pdf,true)); // Toyet 12.19.2018
?>
<h2 class="heading">Students With Privileges</h2>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; 
	border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px; vertical-align:middle;">
<div style="float:left; margin-top:7px; margin-left:20px;"> 
<?php if ($with_privileges) { ?>
	  <a class="download_scholar_list" href="" 
		  with_priv='<?php print($with_priv_pdf); ?>'><i class="icon-download-alt"></i>Download to PDF</a>
	  <a class="download_csv_scholar_list" href="" 
		  with_priv='<?php print($with_priv_pdf); ?>'><i class="icon-download-alt"></i>Download to CSV</a>
<?php } ?>	 
  </div>
<div style="float:right;">   
	<form id="change_sy" method="POST">
					<select name="academic_term" id="academic_term"" style="width: auto;">
						<?php $_limit = 1;
							foreach($select_terms AS $_term){
						?>
							<option value=<?php print($_term->id); 
							               if($_term->id == $current_selected_term_id) { 
							               	  print(" selected");
							               }?> ><?php print($_term->term.' '.$_term->sy); ?></option> 
					<?php 
							$_limit++;
							if($_limit>=10){
								break;
							}
				    } ?>

					</select>
				
					<select name="privilege_items_id" id="privilege_items_id" style="width: auto;">
					<?php 
						foreach($privilege_items AS $item) {
					?>
							<option value="<?php print($item->id); ?>" <?php if ($item->id == $selected_item) { print("selected"); } ?>  >
								<?php print($item->scholarship_code." - ".$item->scholarship); ?> </option>
					<?php 	
						}
					?>
					</select>
					
					<span style="padding-left:40px; ">Posted:
					<select name="posted" id="posted_id" style="width: auto;">
						<option value="All" <?php if ($posted == 'All') { print("selected"); } ?>>
								All</option>
						<option value="N" <?php if ($posted == 'N') { print("selected"); } ?>>
								No</option>
						<option value="Y" <?php if ($posted == 'Y') { print("selected"); } ?>>
								Yes</option>
					</select></span>
					
	</form>	
</div>					
</div>

<h3><?php print($privilege->scholarship); ?></h3>

<h4><?php 
	foreach($select_terms as $_term){
		if($_term->id == $current_selected_term_id){
			$_acad_term = $_term->term.' '.$_term->sy;
			print($_term->term.' '.$_term->sy);
		}
	}
?></h4><br />

<div>
<?php
	if ($with_privileges) {
?>
  <table class="table table-bordered table-striped table-hover" id="viewprivileges">
	<thead>
	<tr style="background-color: #EAEAEA; font-weight: bold;">
        <td style="text-align: center;"></td>
        <td style="text-align: center;">ID No.</td>
        <td style="text-align: center;">Name</td>
        <td style="text-align: center;">Course/Year</td>
        <td style="text-align: center;">Tuition Fee Discount %</td>
        <td style="text-align: center;">Total Discount</td>
        <td style="text-align: center;">Remarks</td>
        <td style="text-align: center;">POSTED</td>
        </tr>
    </thead>
    <?php
    	$x = 1;
		foreach ($with_privileges AS $priv) {
	?>
    <tr>
    	<td style="text-align: center;"><?php print($x); ?></td>
        <td style="text-align: center;"><?php print($priv->students_idno); ?></td>
        <td style="text-align: left;"><?php print($priv->lname.", ".$priv->fname); ?></td>
    	<td style="text-align: left;"><?php print($priv->abbreviation."-".$priv->year_level); ?></td>
        <td style="text-align: right;"><?php print(($priv->discount_percentage * 100)."%"); ?></td>
        <td style="text-align: right;"><?php print(number_format($priv->discount_amount,2)); ?></td>
        <td style="text-align: left;"><?php print($priv->remark); ?></td>
        <td style="text-align: center;"><?php print($priv->posted); ?></td>
        </tr>
	<?php $x++;
	 } ?>

  </table>
<?php 
	} else {
?>
	<div style="font-size: 15px; font-weight: bold; margin-top: 10px;"><?php print("No Students availed on this scholarship!"); ?> </div>
<?php 	
	} 
?> 

</div>
  
 <form id="download_scholar_list_pdf" method="post" target="_blank">
    <input type="hidden" name="step" value="download_to_pdf"/>
    <input type="hidden" name="scholar_desc" value="<?php print($privilege->scholarship); ?>" />
    <input type="hidden" name="acad_term" value="<?php print($_acad_term); ?>" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>

 <form id="download_csv_scholar_list" method="post" target="_blank">
    <input type="hidden" name="step" value="download_to_csv"/>
    <input type="hidden" name="scholar_desc" value="<?php print($privilege->scholarship); ?>" />
    <input type="hidden" name="acad_term" value="<?php print($_acad_term); ?>" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>

<script>
	$(document).ready(function(){
		$('#viewprivileges').dataTable( {
		    "aoColumnDefs": [{ "bSearchable": true, "aTargets": [ 1,2,3,4,5,6 ] }],
			"iDisplayLength": 100,
		    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
	} );
</script>

<script>
	$('.download_scholar_list').click(function(event){
		event.preventDefault(); 
		var with_priv = $(this).attr('with_priv');
		$('<input>').attr({
			type: 'hidden',
			name: 'with_priv',
			value: with_priv,
		}).appendTo('#download_scholar_list_pdf');
		$('#download_scholar_list_pdf').submit();		
	});
</script>

<script>
	$('.download_csv_scholar_list').click(function(event){
		event.preventDefault(); 
		var with_priv = $(this).attr('with_priv');
		$('<input>').attr({
			type: 'hidden',
			name: 'with_priv',
			value: with_priv,
		}).appendTo('#download_csv_scholar_list');
		$('#download_csv_scholar_list').submit(); 
	});
</script>


<script>
	$('.privilege').click(function(event){
		event.preventDefault(); 
		var  privilege = $(this).attr('href');		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'privilege',
		    value: privilege,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>


<script>
	$(document).ready(function(){
		$('#privilege_items_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 2,
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>


<script>
	$(document).ready(function(){
		$('#posted_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 4,
			}).appendTo('#change_sy');
			$('<input>').attr({
				type: 'hidden',
				name: 'selected_term',
				value: $('#academic_term').val(),
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>

<script>
	$(document).ready(function(){
		$('#academic_term').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 4,
			}).appendTo('#change_sy');
			$('<input>').attr({
				type: 'hidden',
				name: 'selected_term',
				value: $(this).val(),
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});	
</script>
