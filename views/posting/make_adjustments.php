<?php //log_message("INFO", print_r($student,true)); // Toyet 8.16.2018 
      //log_message("INFO", print_r($_terms,true)); // Toyet 8.16.2018 
      //if($is_bed)
      //	log_message("INFO", "IS BED IS TRUE"); // Toyet 8.16.2018 
      //else
      //	log_message("INFO", "IS BED IS FALSE"); // Toyet 8.16.2018 
?>
<div class="fluid span12">
<form id="new_adjustment_form" method="post" action="" class="form-horizontal">
  <input type="hidden" name="action" value="make_adjustments" />
  <input name="tab" value="adjustments" type="hidden" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>	
	
			<fieldset>
				
				<div class="control-group formSep">
					<label for="date_taken" class="control-label">Date of Adjustment</label>
					<div class="controls">
						<div class="input-append date" id="dp2" data-date-format="yyyy-mm-dd">
							<input type="text" name="date_taken" id="date_taken" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>

				<div class="control-group formSep">
					<label for="chargeto" class="control-label">Term Tag</label>
					<div class="controls">
						<select id="chargeto_term" name="chargeto_term" class="span5">
						<?php 
							$select_term = $_terms;
							$i = 0;
							foreach ($select_term as $row){ 
								$is_bed = in_array($row->levels_id,array(1,2,3,4,5,11));
								if($is_bed==FALSE) { ?>
									<option value="<?php echo $row->levels_id.'/'.$row->year_level; ?>"><?php echo $row->student_term.' '.$row->sy.' - '.$row->program.' '.$row->year_level; ?></option>
								<?php } else { ?>
									<option value="<?php echo $row->levels_id.'/'.$row->year_level; ?>"><?php echo 'SY '.$row->sy.' - '.$row->program.' '.$row->year_level; ?></option>
						<?php }
						    $i +=1 ;
						    if($i==6 and count($select_term)>6)
						    	break;
						     } ?>
						</select>
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="adjustment" class="control-label">Adjustment Description</label>
					<div class="controls">
						<select id="description" name="description" class="span6">
						<?php 
							foreach ($adjustment_type as $type): ?>
									<option value="<?php echo $type->description2; ?>"> <?php echo $type->description2; ?></option>
						<?php endforeach; ?>
						</select>
					</div>
				</div>
						
				<div class="control-group formSep">
					<label for="reference" class="control-label">Amount</label>
					<div class="controls">
						<input type="text" style="text-align:right" class="money input-xlarge span2" id="amount" name="amount" placeholder="Amount"/> 				
					</div>									
				</div>
			
				<div class="control-group formSep">
					<label for="adjustment_type" class="control-label">Adjustment Type</label>
					<div class="controls">
						<input name="type" type="radio" class="form" value="Debit"/>  Debit
		    		 	<input name="type" type="radio" class="form" value="Credit"/> Credit 
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Remarks</label>
					<div class="controls">
						<textarea id="remarks" name="remarks" class="input-xlarge field span12" rows="6" placeholder="Please limit your remark to a maximum of 200 characters :)" maxlength="200"></textarea>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<button class="adjust" type="submit">Adjust Student Ledger</button>
					</div>
				</div>
			</fieldset>
</form>
</div>
<script>
	$(document).ready(function(){
		$('#dp2').datepicker();
	
	});
	</script>


<script>
$(document).ready(function(){
	$('#new_adjustment_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			amount: { required: true, minlength: 1 },
			type: {required: true},
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	$(".money").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});
</script>

