<h2 class="heading">Edit Scholarship</h2>

<div class="fluid span12">
	<form id ="edit_scholarship" action="<?php echo site_url('accounts/list_privileges');?>" method="post">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="edit_privilege" />
		<input type="hidden" name="scholarship_id" value="<?php print($scholarship_id); ?>" />
				
		<fieldset>
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Scholarship Code</label>
					<div class="controls">
						<input name="scholarship_code" type="text" id="scholarship" maxlength="15" value="<?php print($scholarship_code); ?>" style="width:100px;" >
					</div>
				
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Scholarship</label>
					<div class="controls">
						<input name="scholarship" type="text" id="scholarship" maxlength="30" value="<?php print($scholarship); ?>" style="width:400px;" >
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Maximum units (1st-3rd Year)</label>
					<div class="controls">
						<input name="max_undergrad" type="text" id="max_undergrad" maxlength="30" value="<?php print($max_under); ?>" style="width:100px;" >
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Maximum units (4th/5th Year)</label>
					<div class="controls">
						<input name="max_grad" type="text" id="max_grad" maxlength="30" value="<?php print($max_grad); ?>" style="width:100px;" >
					</div>
					
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit" name="edit_privilege" id="edit_privilege">Edit Scholarship</button>
					</div>
				</div>
			
		</fieldset>
		
	</form>
</div>

<script>
$('#edit_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update scholarship?");
    
	if (confirmed){
		
		$('#edit_scholarship').submit();
	}
});
</script>
