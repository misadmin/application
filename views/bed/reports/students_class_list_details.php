

	<div style="margin-top:10px">

		<table class="table table-striped table-bordered table-condensed"; style="width:70%">
			<thead >
				<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
					<td class="shs_header" width="5%"  style="vertical-align:middle; text-align:center;">No.</td>
					<td class="shs_header" width="15%" style="vertical-align:middle; text-align:center;">ID<br>Number</td>
					<td class="shs_header" width="50%" style="vertical-align:middle; text-align:center;">Name</td>
					<td class="shs_header" width="5%"  style="vertical-align:middle; text-align:center;">Gender</td>
					<td class="shs_header" width="25%"  style="vertical-align:middle; text-align:center;">Section</td>
				</tr>
			</thead>
			<tbody> 

			
			<?php 
				if($students) {
					$count = 0;
					foreach ($students as $student): ?>
					<tr>
						<?php $count++;?>
						<td style="text-align:right;"><?php print($count); ?>.</td>
						<td style="text-align:center;"><a href="<?php print(base_url($this->uri->segment(1)."/student")."/".$student->idno); ?>"><?php print($student->idno); ?></a></td>
						<td><?php print($student->name); ?></td>
						<td style="text-align:center;"><?php print($student->gender); ?></td>
						<td style="text-align:left;"><?php print($student->description." ".$student->section_name); ?></td>						
					</tr>
		<?php
					endforeach;
				} ?>
		</tbody>
		</table>
	</div> 
