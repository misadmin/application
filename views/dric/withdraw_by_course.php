<div class="clearfix">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th width="13%" valign = "middle" align = "middle"><div align="center">Catalog Number (Section)</div></th>
				<th width="15%" valign = "middle" align = "middle"><div align="center">Descriptive Title</div></th>
				<th width="20%" valign = "middle" align = "middle"><div align="center">Schedule</div></th>
				<th width="20%" valign = "middle" align = "middle"><div align="center">Teacher</div></th>
				<th width="8%" valign = "middle" align = "middle"><div align="center">Units</div></th>
				<th width="8%" valign = "middle" align = "middle"><div align="center">Prelim Grade</div></th>
				<th width="8%" valign = "middle" align = "middle"><div align="center">Midterm Grade</div></th>
				<th width="8%" valign = "middle" align = "middle"><div align="center">Final Grade</div></th>
			</tr>
		</thead>
	
		<tbody>
	<?php $count = 0;
	 if(is_array($schedules) && count($schedules) > 0): ?>
		<?php foreach($schedules as $schedule): if(($schedule->prelim_grade != 'WD') AND ($schedule->midterm_grade != 'WD') AND 
					($schedule->finals_grade != 'WD')){ $count = $count+1; ?>
			<tr>
				<td><div align="left"><a class="withdraw_course" href="<?php print($schedule->id);?>"><?php print($schedule->course_code); ?>
				  </a>
				  </div></td>
				<td><div align="left"><?php echo $schedule->descriptive_title; ?></div></td>
				<td> <div align="left">
				  <?php
					$my_offering_slots = $this->Offerings_Model->ListOfferingSlots($schedule->course_offerings_id);
					foreach($my_offering_slots[0] AS $my_slots) {
						print($my_slots->tym." ".$my_slots->days_day_code." | <span style=\"color:green; font-weight:bold;\" >Rm.".$my_slots->room_no.
								"/".$my_slots->bldg_name."</span> <br>");  
					}
		?>		
			    </div></td>
				<td><div align="left"><?php echo $schedule->emp_name; ?></div></td>
				<td><div align="center"><?php echo $schedule->credit_units; ?></div></td>
				<td><div align="center"><?php echo $schedule->prelim_grade; ?></div></td>
				<td><div align="center"><?php echo $schedule->midterm_grade; ?></div></td>
				<td><div align="center"><?php echo $schedule->finals_grade; ?></div></td>
			</tr>
	<?php } ?>		
	<?php endforeach;  ?>
	<?php if($count > 0) { ?>
	<form id="totalWithdraw" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
	<input type="hidden" name="action" value="total_withdraw" />
	<input type="hidden" name="history_id" value="<?php print($history_id) ?>" />
	<tr>
         <td 
		 	colspan="8" valign="middle"><input type="submit" name="totWithdraw" id="totWithdraw" value="Total Withdrawal!"  class="btn btn-success" />		 		</td>
   </tr>
	</form>	
	<?php }else{ ?> <tr>
				<td colspan="5">No Courses for withdrawal for this term</td>
			</tr>
			<?php } ?>

	<?php  else: ?>
			<tr>
				<td colspan="5">No Courses found for this term</td>
			</tr>
	<?php endif; ?>
		</tbody>
	</table>
	
	
	<script>
	$('#totWithdraw').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm("Continue to withdraw ALL presently enrolled courses?");

	if (confirmed){
	   	$('#totalWithdraw').submit();
	}		
});
</script>
	
	
</div>

<form id="withdrawC" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="withdrawCourse" />
	</form>			

<script>
	$('.withdraw_course').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm("Continue to withdraw course?");

	if (confirmed){
	     var enrollment_id = $(this).attr('href');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'enrollment_id',
		    value: enrollment_id,
		}).appendTo('#withdrawC');
		$('#withdrawC').submit();
	}		
});
</script>