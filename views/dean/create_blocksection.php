<!--form action="" method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	 	<input type="hidden" name="step" value="2" />
	
	  <?php echo validation_errors(); 
	  ?>
	
	<div >
	
	<h2 class="heading">Create Block Sections</h2>
	<form class="form-horizontal">
	<?php if (isset($message) AND $message != ""): ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $message ?>	
	</div>
	<?php endif ?>
		 <div class="control-group formSep">
		  <label class="control-label" for="academic-program">Academic Program&nbsp:&nbsp</label>
			  <div class="controls">
				  <select name="academic_program" id="academic_program_id" class="span3">
			    	<?php
							foreach($academic_program AS $prog) {
								print("<option value=".$prog->id.">".$prog->abbreviation." [ ".$prog->effective_year." ]</option>");	
							}
						?>
			  	   </select>
			  </div>
		</div>
			
		<div class="control-group ">
			<div class="span5">
		  <label class="control-label" for="year-level">Year Level&nbsp:&nbsp</label>
				  <div class="controls">
						<select name="yearlevel" id="yearlevel" class="span3">
					    <option value="1" selected="selected">1</option>
					    <option value="2">2</option>
					    <option value="3">3</option>
					    <option value="4">4</option>
					    <option value="5">5</option>
					  </select>
					  </div>
				</div>
			</div>
			
			<div class="form-actions">
				<a href="#" onclick="$('alert').show()">
					<button class="btn btn-success" type="submit"">Create</button>
				</a>
			</div>
	</form>
	</div>
</form-->
<h2 class="heading">Create Block Sections</h2>
<?php
$this->form_lib->set_id('create_blocksection');
$this->form_lib->add_control_class('formSep');
$this->form_lib->set_attributes(array('method'=>'post','action'=>'#','class'=>'form-horizontal'));
$this->form_lib->enqueue_hidden_input('step',2);
$this->form_lib->enqueue_select_input2('academic_program','Academic Program','{id}','{abbreviation} [{effective_year}]',$academic_program,'','','span2');
$yearlevel = array(
				array('year'=>'1'),
				array('year'=>'2'),	
				array('year'=>'3'),
				array('year'=>'4'),
				array('year'=>'5'),	
			);
$this->form_lib->enqueue_select_input2('yearlevel','Year Level&nbsp:&nbsp','{year}','{year}',$yearlevel,'','','span1');
$this->form_lib->set_submit_button('Create');
if (isset($message) AND $message != ""){
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$message.'</div>';
}

$this->form_lib->content(FALSE);