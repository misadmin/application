<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>

<div style="background:#FFF; width:100%;" align="center">
<div style="width:100%; margin-bottom:40px;" align="center">	
<form action="<?php echo site_url("cashier/post_receipts")?>" method="post" id="post_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="3" name="step" />
	
<table border="0" cellspacing="0" id="tablepost" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
	 <thead> 
	 	
		<tr class="head">
  			<th colspan="8" class="head" style="text-align:center; padding:10px;">  SUMMARY OF RECEIPTS FOR POSTING (<?php echo $date1; ?>) </th>
  	   </tr>
    
    <tr>
      <th width="5%" class="head" style="vertical-align:middle;">OR No.</th>
	  <th width="10%" class="head" style="vertical-align:middle;">Transaction Date</th>
	  <th width="5%" class="head" style="vertical-align:middle;">Machine IP</th>
	  <th width="5%" class="head" style="vertical-align:middle;">Teller ID</th>
	  <th width="5%" class="head" style="vertical-align:middle;">ID No.</th>
	   <th width="20%" class="head" style="vertical-align:middle;">NAME</th>
      <th width="35%" class="head" style="vertical-align:middle;">Description</th>
      <th width="10%" class="head" style="vertical-align:right;">Amount</th>
	</tr>
    </thead>
    <?php
		if ($receipts) {
			$receipts_count = count($receipts);
			$this->session->set_flashdata('receipts_count',$receipts_count);
			$sum = 0;
			foreach ($receipts AS $receipt) {
				echo '<input type="hidden" name="id_to_post[]" value=" ' .  $receipt->id . '" >';
				
	?>
    <tr>
      <td style="text-align:right; vertical-align:middle; "> <?php print($receipt->receipt_no); ?> </td>
	   <td style="text-align:left; vertical-align:middle; "><?php print($receipt->transaction_date); ?></td>
	   <td style="text-center; vertical-align:middle; "><?php print($receipt->machine_ip); ?></td>
	   <td style="text-align:right; vertical-align:middle; "><?php print($receipt->teller_id); ?></td>
	   <td style="text-align:right; vertical-align:middle; "><?php print($receipt->idno); ?></td>
	   <td style="text-align:left; vertical-align:middle; "><?php print($receipt->name); ?></td>
      <td style="text-align:left; vertical-align:middle; "><?php print($receipt->description); ?></td>
      <td style="text-align:right; vertical-align:right; "><?php print(number_format($receipt->receipt_amount,2,'.',',')); ?></td>
	  <?php $sum += $receipt->receipt_amount ?>
    </tr>
    <?php
			} ?>
			
		<tr>
			<th colspan=4 style="text-align:right; vertical-align:middle;">Number of Payments to be posted</th>
			<td style="text-align:right; vertical-align:right; "><?php echo number_format($receipts_count,0,'',','); ?></td>
      		<th colspan=2 style="text-align:right; vertical-align:middle;">TOTAL </th>
		    <td style="text-align:right; vertical-align:right; "><?php echo number_format($sum,2,'.',','); ?></td>
        </tr>			
	<?php 	}
	?>
	
	<tr>
		<th colspan="8" align="center"> <input type="button" id="post_receipt" value="Post Receipts!" /> </th>
	</tr>
</table>
</form>
</div>
</div>

<script>
$(document).ready(function(){
    $("#tablepost").stickyTableHeaders();
    $("#tablepost").stickyTableHeaders('updateWidth');
	
	
	$('#post_receipt').bind('click', function(event){	
		event.preventDefault();
		var confirmed = confirm("Are you sure you want to post the payments?");
	
		if (confirmed){
			$('#post_form').submit();
		}		
	});
	
});

</script>