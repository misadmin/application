<h3 class="heading">Teller's Terminal Controller</h3>
<div class="row-fluid">
	<?php echo $terminal_table; ?>
</div>
<div class="row-fluid">
	<a id="modal_editor_switch" href="#modal_editor" role="button" class="btn btn-primary" data-toggle="modal">Add This Terminal</a>
	<a id="unmark_switch" href="#" class="btn btn-danger" style="display:none">Unmark Terminal</a>
</div>
<div id="modal_editor" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Terminal Editor</h3>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" method="post" id="mark_terminal_form" action="<?php echo site_url('developer/handler'); ?>" >
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<?php action_form('mark_terminal', FALSE)?>
			<input type="hidden" name="id" value="" /> 
			<div class="control-group formSep">
				<label for="terminal_name" class="control-label">Name of Terminal</label>
				<div class="controls">
					<input type="text" class="input-large" name="terminal_name" value="<?php echo ""; ?>" />
				</div>
			</div>
			<div class="control-group  formSep">
				<label for="terminal_details" class="control-label">Other Details</label>
				<div class="controls">
					<textarea name="terminal_details"></textarea>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button type="submit" id="mark_button" class="btn btn-primary">Mark this terminal</button>
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>
<script>
var terminal_id=<?php echo $terminal_id; ?>;
var marked_terminal=<?php echo ($terminal_marked ? 'true' : 'false'); ?>;

$().ready(function(){
	if(marked_terminal){
		$('#modal_editor_switch').hide();
		$('#unmark_switch').show();
	}
	$("#mark_terminal_form").validate({
		onkeyup:false,
		errorClass:'help-inline',
		validClass:'text-success',
		rules:{
			terminal_name: {required:true,},
		},
		highlight:function(element,errorClass){
			$(element).parent().parent().addClass('error');
		},
		unhighlight:function(element,errorClass){
			$(element).parent().parent().removeClass('error');
		},
		errorElement:"span"}
	);
	$('#mark_terminal_form').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			url     : $(this).attr('action'),
			type    : 'POST',
			datatype: 'json',
			data    : $(this).serialize(),
			success : function(response) {
				if(response.success){
					$.gritter.add({
                        title:	'Success',
                        text:	response.message,
                        sticky: false,
                        time: 2000
                    });
                    var content = '';
                    terminal_id = response.id;
                    jQuery.each(response.terminals, function(index, item){
                        if(item.id==terminal_id){
                            $('#alert-container').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>This Terminal is MARKED: '+item.name+'</div>').show('slow');
                        }
						content+='<tr class="terminal_row" data-id="'+item.id+'"><td>'+(index+1)+'</td><td>'+item.name+'</td><td>'+item.description+'</td><td style="text-align:center"><a href="#" class="edit_terminal" data-id="'+item.id+'" data-name="'+item.name+'" data-description="'+item.description+'" title="Edit this terminal"><i class="icon-edit"></i></a>&nbsp;<a href="#" class="delete_terminal" data-id="'+item.id+'" title="Delete this terminal"><i class="icon-remove"></i></a></td></tr>';
                    });
                    $('#terminal_table tbody').html(content);
                    $('#modal_editor_switch').hide();
                    $('#unmark_switch').show();
                    $('input[name="nonce"]').val(response.nonce);   
				} else {
					$.gritter.add({
                        title:	'Error',
                        text:	response.message,
                        sticky: false,
                        time: 2000
                    });
                    $('input[name="nonce"]').val(response.nonce);   
				}
			},
			error: function(){
				$.gritter.add({
                    title:	'Error',
                    text:	'Error found while marking this terminal.',
                    sticky: false,
                    time: 2000
                });
			}
		});
		$('#modal_editor').modal('hide');
	});
	$('#modal_editor').on('hide', function(){
		$('#mark_terminal_form input[name="action"]').val('mark_terminal');
		$('#mark_terminal_form input[name="id"]').val('0');
		$('#mark_terminal_form input[name="terminal_name"]').val('');
		$('#mark_terminal_form [name="terminal_details"]').val('');
		$('#mark_button').html('Mark this Terminal');
	});	
});
</script>