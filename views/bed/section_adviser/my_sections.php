

	<h2 class="heading">Basic Education - Section Adviser</h2>

		<div id="show_term_sy" style="font-size:18px; font-weight:bold; margin-bottom:10px; color:#030B3D;"></div>
		<div id="show_section_grade" style="font-size:25px; font-weight:bold; margin-bottom:10px; color:#030B3D;"></div>

		<div style="float:left; text-align:right; display:none; width:60%;" id="download_pdf" >	</div>		
		<div id="show_my_students" style="width:50%; display:none;"> </div>
		<div id="show_section_schedules" style="width:80%; display:none;"> </div>
		<div id="show_my_subjects" style="width:80%; display:none;"> </div>
		<div id="show_msg" style="width:80%; font-size:18px; font-weight:bold; color:#AF0707; margin-top:20px;"> </div>
		
			<div style="padding:3px; overflow:auto; width:80%; display:block;" id="section_list" >
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:15%;">School Year</td>
								<td class="shs_header" style="width:54%;">Grade Level/<br>Section</td>
								<td class="shs_header" style="width:5%;">Room</td>
								<td class="shs_header" style="width:8%;">No. of<br>Students</td>
								<td class="shs_header" style="width:8%;">No. of<br>Subjects</td>
								<td class="shs_header" style="width:5%;">Submit<br>Attendance</td>
								<td class="shs_header" style="width:5%;">Class<br>Schedule</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($sections) {
									foreach($sections AS $section) {
							?>
							<tr>
										<td style="text-align:center;"><?php print($section->sy); ?></td>
										<td style="text-align:left;"><?php print($section->description.' - '.$section->section_name); ?></td>
										<td style="text-align:center;"><?php print($section->room_no); ?></td>
										<td style="text-align:center;">		
											<!--<a href="<?php print($section->block_sections_id); ?>" 
												sy="<?php print($section->sy); ?>" 
												section_grade="<?php print($section->abbreviation.' - '.$section->section_name.' ['.$section->room_no.']'); ?>"
												grade_section="<?php print('Grade '.$section->abbreviation.'-'.$section->section_name); ?>"
												description="<?php print($section->description); ?>" 
												room_no="<?php print($section->room_no); ?>" 
												abbreviation="<?php print($section->abbreviation); ?>"
												class="my_students" >
											</a>-->
												<div id="show_stud_icon_<?php print($section->block_sections_id); ?>" >
													<?php print($section->num_students); ?>
												</div>
										</td>
										<td style="text-align:center;">
											<!--<a href="<?php print($section->block_sections_id); ?>" 
												sy="<?php print($section->sy); ?>" 
												section_grade="<?php print($section->abbreviation.' - '.$section->section_name.' ['.$section->room_no.']'); ?>"
												grade_section="<?php print('Grade '.$section->abbreviation.'-'.$section->section_name); ?>"
												description="<?php print($section->description); ?>" 
												room_no="<?php print($section->room_no); ?>" 
												abbreviation="<?php print($section->abbreviation); ?>"
												class="my_subjects" >
											</a>-->
												<div id="show_subj_icon_<?php print($section->block_sections_id); ?>" >
													<?php print($section->num_subjects); ?>
												</div>
										</td>
										<td style="text-align:center;">
											<!-- <a href="<?php print($section->block_sections_id); ?>" 
												sy="<?php print($section->sy); ?>" 
												section_grade="<?php print($section->abbreviation.' - '.$section->section_name.' ['.$section->room_no.']'); ?>"
												grade_section="<?php print('Grade '.$section->abbreviation.'-'.$section->section_name); ?>"
												description="<?php print($section->description); ?>" 
												room_no="<?php print($section->room_no); ?>" 
												abbreviation="<?php print($section->abbreviation); ?>"
												class="extract_students_for_attendance" >
											</a> -->
												<div id="show_list_icon_<?php print($section->block_sections_id); ?>" >
													<i class="icon-list"></i>
												</div>
										</td>
										<td style="text-align:center;">
											<a href="<?php print($section->block_sections_id); ?>" 
												sy="<?php print("Academic Year: ".$section->sy); ?>" 
												section_grade="<?php print($section->description.' - '.$section->section_name.' ['.$section->room_no.']'); ?>"
												room_no="<?php print($section->room_no); ?>" 
												abbreviation="<?php print($section->abbreviation); ?>"
												class="my_schedules" >
												<div id="show_zoom_icon_<?php print($section->block_sections_id); ?>" >
													<i class="icon-zoom-in"></i>
												</div>
											</a>
										</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>
	
		
<script>
	$('.my_students').click(function(event){
		event.preventDefault(); 
			
		var section_id    = $(this).attr('href');
		var term_sy       = $(this).attr('term_sy');
		var section_grade = $(this).attr('section_grade');
		var description   = $(this).attr('description');
		var grade_section = $(this).attr('grade_section');
		var room_no       = $(this).attr('room_no');
		var abbreviation  = $(this).attr('abbreviation');

			$('#show_stud_icon_'+section_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/my_section');?>',
				data: { "section_id": section_id,
						"term_sy": term_sy,
						"description": description,
						"grade_section": grade_section,
						"room_no": room_no,
						"abbreviation": abbreviation,
						action: 'extract_section_students'},
				dataType: 'json',
				success: function(response) {				

					$('#show_term_sy').html(term_sy); 	
					$('#show_section_grade').html(section_grade); 	
					$('#show_description').html(description); 	
					$('.heading').html('Senior High School - Class List'); 	
					$('#section_list').hide();
					
					$('#download_pdf').html(response.download); 	
					$('#download_pdf').show();
					$('#show_my_students').html(response.output); 
					$('#show_my_students').show();
					

				}
			});
		

	});
</script>

		
<script>
	$('.my_schedules').click(function(event){
		event.preventDefault(); 
			
		var section_id		= $(this).attr('href');
		var sy				= $(this).attr('sy');
		var section_grade	= $(this).attr('section_grade');

			$('#show_zoom_icon_'+section_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/my_section');?>',
				data: { "section_id": section_id,
						action: 'extract_section_schedule'},
				dataType: 'json',
				success: function(response) {				

					$('#show_term_sy').html(sy); 	
					$('#show_section_grade').html(section_grade); 	
					$('.heading').html('Basic Education - Class Schedule'); 	
					$('#section_list').hide();
					
					$('#show_section_schedules').show();
					$('#show_section_schedules').html(response.output); 	
				}
			});
		

	});
</script>
			


			
			
<script>
	$('.my_subjects').click(function(event){
		event.preventDefault(); 
			
		var section_id = $(this).attr('href');
		var term_sy = $(this).attr('term_sy');
		var section_grade = $(this).attr('section_grade');
		var description = $(this).attr('description');

			$('#show_subj_icon_'+section_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/my_section');?>',
				data: { "section_id": section_id,
						action: 'extract_section_subjects'},
				dataType: 'json',
				success: function(response) {				

					$('#show_term_sy').html(term_sy); 	
					$('#show_section_grade').html(section_grade); 	
					$('#show_description').html(description); 	
					$('.heading').html('Senior High School - Class Courses'); 	
					$('#section_list').hide();
					
					$('#show_my_subjects').html(response.output); 
					$('#show_my_subjects').show();
				}
			});
		

	});
</script>


<script>
	$('.extract_students_for_attendance').click(function(event){
		event.preventDefault(); 
			
		var section_id        = $(this).attr('href');
		var academic_terms_id = $(this).attr('academic_terms_id');
		var term_sy           = $(this).attr('term_sy');
		var section_grade     = $(this).attr('section_grade');
		var description       = $(this).attr('description');

			$('#show_list_icon_'+section_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/my_section');?>',
				data: { "section_id": section_id,
						"academic_terms_id": academic_terms_id,
						action: 'extract_students_for_attendance'},
				dataType: 'json',
				success: function(response) {				

					$('#show_term_sy').html(term_sy); 	
					$('#show_section_grade').html(section_grade); 	
					$('#show_description').html(description); 	
					$('.heading').html('Senior High School - Submit Attendance'); 	
					$('#section_list').hide();
					
					if (response.success) {
						$('#show_my_students').html(response.output);
						$('#show_my_students').show();						
					} else {
						$('#show_msg').html('ERROR: SHS Chair has not assigned Number of School Days yet!');
					}

				}
			});
		

	});
</script>
