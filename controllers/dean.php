<?php

class Dean extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('form_lib');
		$this->userinfo = $this->session->all_userdata();
		
		//print_r($this->userinfo);die();
		
		$this->content_lib->set_title ('Dean | ' . $this->config->item('application_title'));
		if ($this->session->userdata('role')=='dean'){		
			$this->navbar_data['menu'] = array(
							'Students'=>'student',
							'Faculty'=>'faculty',
							'Offerings'	=> array(
									'College Offerings' => 'offerings',
									'Offerings Per College' => 'all_offerings',
									),			
							'Courses'	=> array(
									'List Courses' => 'courses'
									),					
							'Academics'=> array(
									'Create Block Section'	=> 'create_blocksection',
									'View Block Sections'	=> 'view_blocksection',
									'Assign Elective Course' => 'assign_elective_topic',
									'Academic Programs'	=> 'academics',
									'Create New Program' => 'create_program',
									'Prospectus' => array (
															$this->userinfo['college_code'].' Prospectus' => 'prospectus',
															'View Prospectuses' => 'view_prospectus',
															)
									),
							'Reports'=> array(
									'Enrollment Summary' => 'enrollment_summary',
									'Exception Report' => 'exception_report',
									'MasterList' => 'masterlist',
									'Dean\'s List Students' => 'deans_list',
									'Summary of Grade Submission' => 'grade_submission',
									'Enrollment by Program-Course' => 'enroll_by_program_course',
									'Candidates for Graduation' => 'candidates',
									),						
						);

				if($this->session->userdata('college_id')==9){
					$this->navbar_data['menu']['Reports']['List Of NSTP Students 1ST SEM SY 2018-2019'] = 'nstp_list';
					$this->navbar_data['menu']['Reports']['List Of NSTP Students 2ND SEM SY 2018-2019'] = 'nstp_list2';
				}
			}
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		
		// choose only what is needed...
		$this->load->model('hnumis/College_Model');
		$this->load->model('hnumis/Programs_Model');
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('hnumis/Rooms_Model');
		$this->load->model('hnumis/Offerings_Model');
		$this->load->model('hnumis/Faculty_Model');
		$this->load->model('hnumis/Student_Model');
		$this->load->model('hnumis/Enrollments_model');
		$this->load->model('hnumis/Prospectus_Model');
		$this->load->model('hnumis/BlockSection_Model');
		$this->load->model('financials/Payments_Model');
		
		$this->load->library('form_validation');
		$this->load->library('my_pdf');
		$this->load->helper('student_helper');
	}
	
	
	public function student(){
		$this->content_lib->set_title('Dean | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		
		if ( ! empty($query)){
			
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
			
			$this->load->library('pagination_lib');
			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
		
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('hnumis/Prospectus_Model');
			$this->load->model('hnumis/enrollments_model');
			$this->load->model('teller/teller_model');
			
			$this->content_lib->set_title ('Dean | ' . $idnum);
			$result = $this->student_common_model->my_information($idnum);	
			$result2 = $this->teller_model->get_student($idnum);
			
					
			$tab = 'assessment';
			$my_tab = array_fill(0,21,FALSE);
			$my_tab[0]= TRUE;
				
			switch ($action = $this->input->post('action')){
				case 'advise_academic_program' :
										//$aterm = $this->AcademicYears_Model->getCurrentAcademicTerm();
										$aterm = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
										
										if ($this->Student_Model->UpdateStudentProspectus($aterm->id, $idnum, $this->input->post('prospectus_id'), $this->input->post('yr_level')))
											$this->content_lib->set_message("Student Successfully Advised", "alert-success"); else
											$this->content_lib->set_message("Error advising student academic terms", "alert-error");
										$my_tab = array_fill(0,21,FALSE);
										$my_tab[13]= TRUE;
										$my_tab[14]= TRUE;
										break;
				case 'advise_max_units'			:
										$academic_terms_id = $this->input->post('academic_term_id');
										$max_units = $this->input->post('max_units');
										if ($this->Student_Model->setMaxUnits($idnum, $academic_terms_id, $max_units))
											$this->content_lib->set_message("Student Successfully Advised", "alert-success"); else
											$this->content_lib->set_message("Error found while advising student", "alert-error");
										$my_tab = array_fill(0,21,FALSE);
										$my_tab[13]= TRUE;
										$my_tab[15]= TRUE;
										break;
				//Added: January 13, 2013 by Amie
				case 'assign_student_to_blocksection':
										//$academic_terms_id = $this->Academic_terms_model->getCurrentAcademicTerm();
										//$academic_terms_id = $this->Academic_terms_model->current_academic_term ($this->enrollments_model->) 
										//$academic_terms_id->id = 457;
										$academic_terms_id = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
										$blocksection = $this->input->post('blocksection');
										//print_r($blocksection); die();
										if ($this->Student_Model->assign_block($idnum, $academic_terms_id->id, $blocksection, $result->prospectus_id, $result->year_level))
											$this->content_lib->set_message("Student Successfully Advised", "alert-success"); else
											$this->content_lib->set_message("Error found while advising student", "alert-error");
										break;
				//Added: January 13, 2013 by Amie
				case 'unblock_student':
										//$academic_terms_id = $this->Academic_terms_model->getCurrentAcademicTerm();
										//$academic_terms_id->id = 457;
										$academic_terms_id = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
										if ($this->Student_Model->unblock_student($idnum, $academic_terms_id->id))
											$this->content_lib->set_message("Student Successfully Unblocked from the Section!", "alert-success"); else
											$this->content_lib->set_message("Error found while blocking student!", "alert-error");
										$my_tab = array_fill(0,21,FALSE);
										$my_tab[17]= TRUE;
										break;
				//Added: 1/25/2013 By:Genes
				case 'advise_course':
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
										$advised_courses_data['student_histories_id'] = $this->input->post('student_histories_id');
										
										if ($this->input->post('course_type') == 'P') {
											$advised_courses_data['courses_id']    = $this->input->post('courses_id1');
										} else {
											$advised_courses_data['courses_id']    = $this->input->post('courses_id2');
										}										
										
										
										$status = $this->Student_Model->AddAdvisedCourse($advised_courses_data);
										
										if($status){
											$this->content_lib->set_message("Course successfully advised!", "alert-success"); 
										}else{
											$this->content_lib->set_message("Error in advising a course!", "alert-error");
										}
										
								}else {
										$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
									
								}
								$my_tab = array_fill(0,21,FALSE);
								$my_tab[13]= TRUE;
								$my_tab[16]= TRUE;
								
								break;
										
				case 'delete_advised_course':						
							
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
										$status = $this->Student_Model->DeleteAdvisedCourse($this->input->post('advised_course_id'));
										
										if($status){
												$this->content_lib->set_message("Advised course deleted!", "alert-success"); 
										}else{
												$this->content_lib->set_message("Error deleting advised course!", "alert-error");
										}	
								}else {
										$this->content_lib->set_message("Error in submitting form", 'alert-error');	
										
								}
								$my_tab = array_fill(0,21,FALSE);
								$my_tab[13]= TRUE;
								$my_tab[16]= TRUE;
								
								break;
									
				case 'edit_credited_course_form':						
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									$credited_course = $this->Student_Model->getCreditedCourse($this->input->post('credited_course_id'));
									$data['id'] = $credited_course->id;
									$data['couse_code'] = $credited_course->course_code;
									$data['grade'] = $credited_course->grade;
									$data['is_taken_here'] =  $credited_course->taken_here;
									$data['notes'] =  $credited_course->notes;
									$data['idno'] =  $credited_course->students_idno;
									
								//	$this->tab_lib->enqueue_tab('Prospectus', 'dean/form_to_remove_credited_course', $data, 'prospectus', ($tab=='prospectus'));
								$this->content_lib->enqueue_body_content('dean/form_to_remove_credited_course',$data);
									//$this->content_lib->content();
								}else {
										$this->content_lib->set_message("Error in submitting form", 'alert-error');	
										
								}	
								break;						
										
				case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'financials';
								$my_tab = array_fill(0,21,FALSE);
								$my_tab[2]= TRUE;
								$my_tab[3]= TRUE;
								break;												
				
										
			case 'create_remarks':
												
				                
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									    $remarks = $this->input->post('remarks');
									    $view_rights = $this->input->post('view_rights');
			 						    $empno = $this->session->userdata('empno');
			 						   
			 						    if($view_rights){
			 						    	$vrights = "Private";
			 						    }else{
			 						    	$vrights = "Public";
			 						    }   

			 						   // print($vrights);
			 						   // die();
									if($remarks != NULL){		
											$status = $this->Student_Model->createRemarks($idnum, $empno, $remarks, $vrights);
										
									}else{ 
											$status = 0;
									}
								
									if($status){
										$this->content_lib->set_message("Remarks created!", "alert-success");
									}else{
										$this->content_lib->set_message("Error in creating remarks!", "alert-error");
										}
								}else {
										$this->content_lib->set_message("Error in submitting form", 'alert-error');
									
								}
								$my_tab = array_fill(0,21,FALSE);
								$my_tab[18]= TRUE;
								$my_tab[19]= TRUE;
								
								break;
								
						case 'edit_remarks':
											
										if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
											$remarks_id = $this->input->post('remark_id');
											$remark = $this->input->post('remark');
											
											if($this->input->post('view_rights') == 1){
												$view_rights = 'Private';
											}else{
												$view_rights = 'Public';
											}
											$status = $this->Student_Model->editRemark($remarks_id, $remark, $view_rights);
											
											//print($remark_info['remark']);
											//die();
											if($status){
												$this->content_lib->set_message("Successful in editing the remark!", "alert-success");
												//$this->content_lib->content();
											}else{
												$this->content_lib->set_message("Error in editing the remark!", "alert-error");
											}
										}else {
											$this->content_lib->set_message("Error in submitting form", 'alert-error');
												
										}
									
										$my_tab = array_fill(0,21,FALSE);
										$my_tab[18]= TRUE;
										$my_tab[20]= TRUE;
										break;
										
						case 'delete_remark':
												
											if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
												$remarks_id = $this->input->post('remark_id');
												
												$status = $this->Student_Model->deleteRemark($remarks_id);
											//	print($status);
												//die();
												
												if($status){
													$this->content_lib->set_message("Successful in deleting the remark!", "alert-success");
												}else{
													$this->content_lib->set_message("Error in deleting the remark!", "alert-error");
												}
											}else {
												$this->content_lib->set_message("Error in submitting form", 'alert-error');
										
											}

											$my_tab = array_fill(0,21,FALSE);
											$my_tab[18]= TRUE;
											$my_tab[20]= TRUE;
											
											break;
						case 'generate_class_schedule':
								$this->load->model("hnumis/Reports_model");

								$student['idno']= $result->idno;
								$student['fname']= $result->fname;
								$student['lname']= $result->lname;
								$student['mname']= $result->mname;
								$student['yr_level']= $result->year_level;
								$student['abbreviation']= $result->abbreviation;
								$student['max_bracket_units']= $this->input->post('max_units');
								$student['student_histories_id']= $this->input->post('student_histories_id');
								
								$selected_term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
								
								$this->Reports_model->generate_student_class_schedule_pdf($this->input->post('academic_terms_id'), $student, $selected_term);
								
								return;
								
						case 'credit_course': //saves credited courses to credited_courses table
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									if ($this->input->post('taken_type') == 'Y') {
										$student_data['students_idno'] 				= $result->idno;
										$student_data['prospectus_courses_id'] 		= $this->input->post('prospectus_courses_id');
										$student_data['enrollments_id'] 			= $this->input->post('enrollments_id');
										$student_data['other_schools_enrollments_id'] 	= 'NULL';
										$student_data['credited_by'] 				= $this->session->userdata('empno');
										
										$this->Student_Model->creditcourse($student_data);
									} else {
										if ($this->input->post('c')) {
											foreach ($this->input->post('c') AS $k=>$v) {
												$student_data['students_idno'] 				= $result->idno;
												$student_data['prospectus_courses_id'] 		= $this->input->post('prospectus_courses_id');
												$student_data['enrollments_id'] 			= 'NULL';
												$student_data['other_schools_enrollments_id'] 	= $v;
												$student_data['credited_by'] 				= $this->session->userdata('empno');
												
												$this->Student_Model->creditcourse($student_data);
											}
										}
									}
									
									$this->content_lib->set_message("Credited course added!", "alert-success");
										
								} else {
									$this->content_lib->set_message("Error in submitting form", 'alert-error');
								}
								
								$my_tab = array_fill(0,21,FALSE);
								$my_tab[9]= TRUE;
								$my_tab[10]= TRUE;
								
								break;			

						case 'remove_credited_course': //removes credited courses
										
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									//print_r($this->input->post('r')); die();
									if ($this->input->post('r')) {
										foreach ($this->input->post('r') AS $k=>$v) {
											$status = $this->Student_Model->Remove_credited_course($v);
											
											if($status){
												$this->content_lib->set_message("Credited course deleted!", "alert-success");
											}else{
												$this->content_lib->set_message("Error deleting credited course!", "alert-error");
											}
										}
									}
									
								} else {
									$this->content_lib->set_message("Error in submitting form", 'alert-error');
								}
								
								$my_tab = array_fill(0,21,FALSE);
								$my_tab[9]= TRUE;
								$my_tab[10]= TRUE;
								
								break;
						case 'update_year_level';
								if ($this->Student_Model->update_student_history($this->input->post('history_id'),
										array('year_level'=>$this->input->post('new_yr_level')))) {
									$this->content_lib->set_message('Year Level Successfully updated!', 'alert-success');
								} else {
									$this->content_lib->set_message('ERROR: Year Level NOT Updated!', 'alert-error');
								}
								
								break;
								
				default:
					break; 
				
			}
			//$result = $this->student_common_model->my_information($idnum);
					
			if ($result != FALSE) {
				//print_r(json_decode($result->meta));die();
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$grade_level = array(11,12);

				if (in_array($result->year_level,$grade_level)) { //student is SHS
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname)  . ", "  . $result->fname . " ". $result->mname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> "Grade ".$result->year_level." ".$result->section_name,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address'	=>$result->full_city_address,
							'phone_number'	=>$result->phone_number,
							'section'	=>$result->section,
					);
				} else {
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> $result->fname. " " . ucfirst(substr($result->mname,0,1)) . ". " . $result->lname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> $result->year_level,
							'full_home_address'	=> $result->full_home_address,
							'full_city_address'	=> $result->full_city_address,
							'phone_number'		=> $result->phone_number,
							'section'			=> $result->section,						
					);
				}
				
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}					
				
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...
				$data['prospectus']= $this->Programs_Model->ListPrograms($this->session->userdata('college_id'),'O');
				$data['year_level'] = $result->year_level;
				$data['academic_program'] =$result->abbreviation;
				$data['idnum'] = $idnum;
				$data['academic_program_id'] = $result->ap_id;
				
				$this->load->model('hnumis/Student_model');
				
				//$current_term = $this->Academic_terms_model->current_academic_term();
				//$current_term = new StdClass();
				//$current_term->id = 457;
				$current_term = $this->academic_terms_model->current_academic_term($this->Enrollments_model->academic_term_for_enrollment_date());
				$grades = $this->Enrollments_model->student_grades($idnum);
				
				$blocksections = $this->BlockSection_Model->ListSectionsProgram($data['year_level'],$data['academic_program_id']);
				//print_r($blocksections);die();
				$current = $this->Academic_terms_model->getCurrentAcademicTerm();
				$history_id = $this->Student_Model->getEnrollStatus($idnum, $current_term->id);
				$data4 = $this->Student_Model->getCheckWithSummmer($result->prospectus_id, $data['year_level']);
				$data5 = $this->AcademicYears_Model->getCurrentAcademicTerm();
			
				if($data5->term == "Summer" AND !$data4) {
					$max = $this->Student_Model->getMaxInSummmer($idnum, $current_term->id);
					if ($max) {
						$max_units = $max->max_units; 
					} else {
						$max_units = 9.0; 
					}
				} else {
					if ($history_id) {			
							
						$data2 = $this->Student_Model->getMaxUnits($history_id->id,$current_term->id);
						//print_r($data2);
						//die();
						if ($data2) {
							//echo "here";
							$max_units = $data2->max_units_to_enroll; 
						} else {
							//echo "there";
							//add code to retrieve advised units from max_units table
							//$max_units = $data2->max_units_to_enroll; 
							$max_units = $this->Student_Model->get_max_units_from_max_units($idnum, $current_term->id);
								
							
						}
						//die();
					} else {
						$max_units = 0; //student is not enrolled and max_units cannot be retrieved 					
					}
				} 
			
/*				if($max_units ==0){
					$this->content_lib->set_message("No Max Units!", 'alert-error');
					$this->content_lib->content();
					return;
				}*/
				
				//Added 1/25/2013 By: Genes	
				$CourseFromProspectus = $this->Prospectus_Model->ListCoursesToBeAdvised($idnum, $result->prospectus_id,$result->year_level);

				$CourseFromCourses    = $this->Courses_Model->ListAllCoursesToBeAdvised();
				//print_r($history_id); die();
				if($history_id){
					$AdvisedCourses = $this->Student_Model->ListAdvisedCourses($history_id->id);
				}else{
					$AdvisedCourses = array();
				}
				//print_r($AdvisedCourses); die();
				//Added: Feb. 13, 2013 by Amie
				$prospectus_id = $this->Prospectus_Model->student_prospectus_ids($idnum);
				$prospectus = $this->Prospectus_Model->student_prospectus($prospectus_id);
				
				//Added: Feb. 19, 2013 by Amie
				$block = $this->Student_Model->get_blocksection($idnum,$current_term->id);
				
				
				//added January 4, 2013 (psychometrician exams results)
				$this->load->model('psychometrician_model');
				$this->load->model('financials/Tuition_Model');
				$student_exams = $this->psychometrician_model->student_exams($idnum);
				$this->load->library('tab_lib');
				
			     $this->load->library('print_lib');
				
				
				//This looks dirty... but this will work... make schedule be set using a pull down menu
				//added by: justing 3/15/2012
				$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum); //terms this student is 	
																											//enrolled...
				if ($action=='schedule_current_term') {
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $this->input->post('academic_term'));
					//$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $this->input->post('academic_term'));
					$tab = 'schedule';
					$selected_term = $this->input->post('academic_term');
					$my_tab = array_fill(0,21,FALSE);
					$my_tab[1]= TRUE;
				} else {
					if (is_college_student($idnum)) {
						$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $academic_terms[0]->id);
					} else {
						$schedule_courses = NULL;
					}
					//$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_term->id);
					//$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum,  $current_term->id);
					$tab = 'student_info';
					$selected_term = '';					
				}

				//print_r($schedule_courses);die();
				
				
				//display assessment of student
				$result = $this->student_common_model->my_information($idnum);	
				$result2 = $this->teller_model->get_student($idnum);
				$assessment_values = array();
				
				$ledger_data = array();
				$ledger_data = $this->teller_model->get_ledger_data($result2->payers_id);
				//print_r($ledger_data);die();
				$this->load->model('academic_terms_model');
				
				//$this->tab_lib->enqueue_tab('Teller', 'teller/teller', $teller_values, 'teller', TRUE);
				$this->load->model('accounts/accounts_model');
				$this->load->model('hnumis/enrollments_model');
				$this->load->model('hnumis/AcademicYears_Model');
				$this->load->model('hnumis/OtherSchools_Model');
				$this->load->model('accounts/fees_schedule_model');
				
				$period_now = $this->academic_terms_model->what_period();
				
				
				//assessment details
				$acontent = array(
						'idnumber'	=>$result->idno,
						'familyname'=>$result->lname,
						'firstname'	=>$result->fname,
						'middlename'=>$result->mname,
						'level'		=>$result->year_level,
						'course'	=>$result->abbreviation,
						'due_now' 	=> '',
						'succeeding_dues'	=> '7,756.87'
				);
				
				//assessment form
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();

				$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
				//assessment form
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
					$tab = 'financials';
					
					$hist_id = $selected_history_id;
						
					
				} else {
					if (is_college_student($idnum)) {
						$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
						/*EDITED: 9/28/2016 by genes
						* change of academic term variable 
						$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
						$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
						*/
						
						$other_courses_payments = $this->teller_model->other_courses_payments($student_inclusive_terms[0]->id, $result->year_level);
						$laboratory_fees = $this->teller_model->all_laboratory_fees($student_inclusive_terms[0]->id); //
						$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
						
						$hist_id = $result->student_histories_id;
					}	
				}

				if (is_college_student($idnum)) {
					$assessment_date = $this->Student_Model->get_student_assessment_date($hist_id);	

					if (!$assessment_date){
						$transaction_date2=array();
						$assessment_date2[0]= (object)array('transaction_date' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . " + 1 day") ));
						$assessment_date = $assessment_date2;
					}
				}
				
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$lab_courses = array();
				$courses = array();
			
				if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab' && empty($course->enrollments_history_id)) {
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
									);
						} 
						$courses[] = array(
								'id'=>$course->id,
								'name'=>$course->course_code,
								'units'=>$course->credit_units,
								'pay_units'=>$course->paying_units,
								're_enrollments_id'=>$course->re_enrollments_id,
								'withdrawn_on'=>$course->withdrawn_on,
								'assessment_id'=>$course->assessment_id,
								'post_status'=>$course->post_status,
								'enrollments_history_id'=>$course->enrollments_history_id,
								'transaction_date'=>$course->transaction_date,
								
								);  
					}
				}
				//end of code to display assessment of student
				$this->tab_lib->enqueue_tab ('Student Information', 'common/student_information_full', $result, 'student_info', 
											$my_tab[0]);

				if (is_college_student($idnum)) {
					if (in_array($result->year_level,$grade_level)) { //student is SHS

					} else {
						if ($this->input->post('academic_term')) {
							$selected_term = $this->input->post('academic_term');
							$terms=$this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_term')); 
							$acad_term=$terms->term;
						} else {
							$selected_term = $academic_terms[0]->id;
							$acad_term = $current_academic_terms_obj->term;
						}

						$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $selected_term);
						if ($selected_history) {
							$student_units = $this->Student_model->getUnits($selected_history, $acad_term);
							$student_history_id=$selected_history->id;
						} else {
							$student_units['max_bracket_units']=0;
							$student_history_id=0;
						}
										
						$acad_term = $this->AcademicYears_Model->getAcademicTerms($selected_term);
				 
							$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', array('schedules'=>$schedule_courses,
													'assessment_date'=>$assessment_date,
													'academic_terms'=>$academic_terms,
													'academic_terms_id'=>$selected_term,
													'max_units'=>$student_units['max_bracket_units'],					
													'student_histories_id'=>$student_history_id), 'schedule', $my_tab[1]);

							$this->tab_lib->enqueue_tab('Financials',
													'',
													array(),
													'financials',
													$my_tab[2]);

					}							
				
				}
				

/**
 * 
 * temporarily disabled by Tata as requested by Rey on Jun 15, 2015 
 * 
 * 				
**/				//ADDED: 6/11/15 by Genes
				//MODIFIED: 6/26/15 by genes							
				if (is_college_student($idnum) AND !in_array($result->year_level,$grade_level)) {
				$affiliated_fees = $this->fees_schedule_model->ListStudentAffiliated_Fees($hist_id);

				$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment_view_for_students', 
						array(
								'assessment_date'=>$assessment_date[0]->transaction_date,								
								'other_courses_payments'=>$other_courses_payments,
								'assessment'=> $assessment,
								'courses'=>$courses,
								'affiliated_fees'=>$affiliated_fees,
								'lab_courses'=>$lab_courses,
								'student_inclusive_terms' =>$student_inclusive_terms,
								'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
								'student_details'=>$acontent
						), 
								'assessment', $my_tab[3], 'financials');

				$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$ledger_data,
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date
						),
								'ledger', $my_tab[4], 'financials');
								
				$payments = $this->Payments_Model->ListStudentPayments($result2->payers_id);
			
								
				$this->tab_lib->enqueue_tab ('Payments', 'student/list_student_payments', array('payments_info'=>$payments), 'payments',
											$my_tab[5],'financials');		

					$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', FALSE);
				
					$this->tab_lib->enqueue_tab ('Exam Results', 'student/psych_exam_results_with_form', array('exams_taken'=>$student_exams), 'exam', FALSE);
					
					$prospectus_history = $this->Prospectus_Model->ListProspectusTerms($prospectus_id);
					
	
					$this->tab_lib->enqueue_tab('Prospectus',
									'',
									array(),
									'prospectus',
									$my_tab[9]);
								
					//ADDED: 3/25/13 
					//EDITED: 1/10/14 by genes
					//only students of the college can be credited by dean
					if ($this->userinfo['college_id'] == $result->colleges_id) {
						$can_credit = TRUE;
					} else {
						$can_credit = FALSE;
					}
					$this->tab_lib->enqueue_tab ('Current Prospectus', 'dean/prospectus_history_to_credit', 
													array(
														  'can_credit'=>$can_credit,
														  'prospectus_terms'=>$prospectus_history, 
														  'student'=>$dcontent,
														  'courses_passed'=>$this->Enrollments_model->getPassedCourses($idnum, $prospectus_id),
														  'other_schools' => $this->OtherSchools_Model->ListStudentOtherSchools($idnum),
													), 'prospectus_history',
													$my_tab[10],'prospectus');
					
					
					//TEMPORARILY REMOVED
					/*
 					 $credited_courses =  $this->Student_Model->ListCreditedCourses($idnum);
 
					 $this->tab_lib->enqueue_tab ('Credited Courses', 'student/list_credited_courses', 
													array('credited_courses'=>$credited_courses), 'list_credited_courses',
													FALSE,'prospectus');								
					*/
					//Added: October 28, 2013 by Amie
					$programs_taken =  $this->Student_Model->ListPastPrograms($idnum);
					//die();
					$this->tab_lib->enqueue_tab ('Program History', 'student/list_programs_taken',
							array('programs_taken'=>$programs_taken), 'list_programs_taken',
							FALSE,'prospectus');
					
					$acad_id = $this->Prospectus_Model->getProspectus($result->prospectus_id);

					$current = $this->AcademicYears_Model->getCurrentAcademicTerm();

					$student['prospectus_id'] = $result->prospectus_id;
					$student['yr_level'] = $result->year_level;
					$student['idno'] = $result->idno;
					$student['max_yr_level'] = $result->max_yr_level;
					$student['student_histories_id'] = $result->student_histories_id;
										
					$yr_level_data = $this->Student_Model->AssessYearLevel($student);
									
					$this->tab_lib->enqueue_tab ('Year Level Assessment', 
						'student/yr_level_assessment', 
						array(
							'current'=>$current, 
							'data'=>$yr_level_data, 
							'current_yr_level'=>$this->ordinalSuffix($result->year_level),
							'my_yr_level'=>$result->year_level,
							'student_histories_id'=>$result->student_histories_id), 
						'yr_assessment', FALSE, 'prospectus');
												
					//todo: the following tabs should not be outputted to user when the student is not enrolled in the current semester...
					//added: December 18, 2012...
					if ($this->Student_model->student_is_enrolled($idnum)) {
						$this->tab_lib->enqueue_tab('Advisements',
									'',
									array(),
									'advisements',
									$my_tab[13]);
	
						$this->tab_lib->enqueue_tab("Advise Program", "dean/advise_academic_program", $data, "advise", $my_tab[14], 'advisements');
						//This tab is to be present only when the student is under the dean's college...
						if ($this->session->userdata('college_id')==$dcontent['college_id']) { 
							$this->tab_lib->enqueue_tab ("Advise Max Units", "dean/advise_max_units", array('student'=>$dcontent, 'max_units'=>
														$max_units, 'current_term'=>$current_term->id), "advise_max_units", $my_tab[15], 'advisements');
							$this->tab_lib->enqueue_tab ("Advise Course", "dean/advise_course", array('CourseFromProspectus'=>$CourseFromProspectus
															,'CourseFromCourses'=>$CourseFromCourses, 'AdvisedCourses'=>$AdvisedCourses, 	
															'student_histories_id'=>(isset($history_id->id) ? $history_id->id : ''), 'student'=>$dcontent), "advise_course", $my_tab[16], 'advisements');
							//added: January 12, 2013 by Amie
							$this->tab_lib->enqueue_tab ("Block/Unblock", "dean/assign_student_to_blocksection", array('student'=>$dcontent, 
														'blocksection'=>$blocksections, 'block'=>$block, 'current_term'=>$current), 
														"assign_student_to_blocksection", $my_tab[17]);
						}
								
					}
					
					//Updated: July 15, 2013
					//allows the dean to input a remark for a specific student
					$student_college_Id = $this->Student_Model->getSutdentCollegeId($idnum);
					if ($this->session->userdata('college_id')==$student_college_Id->colleges_id) {
						$this->tab_lib->enqueue_tab('Remarks',
								'',
								array(),
								'remarks',
								$my_tab[18]);
						$this->tab_lib->enqueue_tab ("Create", "dean/create_remarks_form", array('current_term'=>$current_term->id),
								"create_remarks", $my_tab[19], 'remarks');
						$remarks_list = $this->Student_Model->ListRemarks($idnum);
						//print_r($remarks_list);
						//die();
						$this->tab_lib->enqueue_tab ("View", "dean/view_remarks", array('all_remarks'=>$remarks_list),
								"view_remarks", $my_tab[20], 'remarks');
					}
					
				}
				$tab_content = $this->tab_lib->content();
				$this->content_lib->flush_sidebar();
				$this->content_lib->enqueue_body_content("", $tab_content);

			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}
		
		//what will happen if there is no query... and the result is not numeric?
		if ( empty($query) && ! is_numeric($idnum)) {
				
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$data = array();
			$this->load->model('hnumis/College_model');
			$data['colleges'] = $this->College_model->all_colleges();
			$this->load->model('student_common_model');
				
			if ($this->input->post('what')=='search_by_program'){
				$total = count ($this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), TRUE));
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				$results = $this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), FALSE, $page, $this->config->item('results_to_show_per_page'));
				$this->load->library('pagination_lib');
				$data['pagination'] = $this->pagination_lib->pagination('', $total, $page);
				$data['results'] = $results;
				$this->load->model('hnumis/Programs_model');
				$this->load->model('hnumis/College_model');
				$data['query'] = $this->Programs_model->getProgram($this->input->post('program'))->abbreviation . " " . $this->input->post('year_level');
				$data['programs'] = $this->College_model->programs_from_college($this->input->post('college'));
				$data['college'] = $this->input->post('college');
				$data['program'] = $this->input->post('program');
				$data['year_level'] = $this->input->post('year_level');
				$data['end'] = $end;
				$data['total'] = $total;
				$data['start'] = $start;
			}
			$this->content_lib->enqueue_body_content('common/search_result_by_program', $data);
		}
		
		$this->content_lib->content();		
	}

	
	private function ordinalSuffix( $n )
	{
		return $n.date('S',mktime(1,1,1,1,( (($n>=10)+($n>=20)+($n==0))*10 + $n%10) ));
	}
	
	
	public function academics (){
		$this->load->model('hnumis/Programs_Model');
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
		switch ($step) {
		case 1:	
			$distinct = $this->input->post('filter') == 'all' ? FALSE : TRUE;
			$this->load->model('hnumis/Programs_Model');
			$programs = $this->Programs_Model->all_programs($this->session->userdata('college_id'), '', $distinct);
			$data = array('programs'=>$programs, 'college'=>$this->userinfo['college_name']);
			$this->content_lib->enqueue_body_content('dean/academic_programs', $data);
			$this->content_lib->enqueue_sidebar_widget('dean/academic_program_filter','', 'Filter Academic Programs', 'in');
			$this->content_lib->content();
			break;
			
		case 2:	//opens the form to edit a program
			$academic_program_id = $this->input->post('id');
			$academic_details = $this->Programs_Model->getProgram($academic_program_id);
		  	$data['groups'] = $this->Programs_Model->ListProgramGroupbyCollege($academic_details->colleges_id);
			$department = $this->Programs_Model->getDepartment($academic_details->departments_id);
			$data['acad_program_id'] = $academic_details->id;
			$data['department_name'] = $department->department;
			$data['department_id'] = $academic_details->departments_id;
			$data['abbreviation'] = $academic_details->abbreviation;
			$data['description'] = $academic_details->description;
			$data['group_id'] = $academic_details->acad_program_groups_id;
			//print_r($data['group_name']);
			//die();
			$this->content_lib->enqueue_body_content('dean/form_to_edit_program', $data);
			$this->content_lib->content();
			//print_r($data['acad_program_id']);
			//die();
			break;
			
		case 3:	//updates a program
		
			$academic_program_id = $this->input->post('acad_program_id');
			$abbreviation = $this->input->post('abbreviations');
			$description = $this->input->post('description');
			$group_id = $this->input->post('acad_program_groups_id');
			$status = $this->Programs_Model->updateAcademicProgram($academic_program_id, $abbreviation, $description, $group_id);
			if($status){
					$this->content_lib->set_message("Academic Program successfully updated!", "alert-success"); 
					
				}else{
					$this->content_lib->set_message("Error found while updating the academic program!", "alert-error");
				}
			$this->content_lib->content();
			//print_r($data);
			//die();
			break;
			
			}	
	}
	
	public function create_program(){
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);

		$this->load->library('form_lib');
		
		$this->load->model('hnumis/Programs_Model');
		$this->load->model('hnumis/College_Model');
		$this->load->model('hnumis/Programs_Model');
	
		$data['groups'] = $this->Programs_Model->ListProgramGroups();

		if ($this->input->post('departments_id')) {
			//Departments ID is set...
			//todo: place this as a hidden field on the create_program form instead of saving it as session...
			$this->session->set_userdata('departments_id',$this->input->post('departments_id'));
			$data['department_name'] = $this->College_Model->getDepartment($this->input->post('departments_id'));
			$this->content_lib->enqueue_body_content('dean/create_program', $data);
			$this->content_lib->content();
			
			return TRUE;
		} 
		
		if ( ! $this->input->post('departments_id') && ! $this->input->post('abbreviations')) { //going to create a program
			//Departments ID is not set && abbreviations is not SET...
			$data['departments'] = $this->College_Model->ListCollegeDepartments($this->session->userdata('college_id'));
			//echo '<pre>';	
			//print_r($data);
			//die();	
			if ($data['departments'] != NULL) {
				$this->content_lib->enqueue_body_content('dean/list_department', $data);
			} else {
				$this->session->set_userdata('departments_id',0);
				$this->content_lib->enqueue_body_content('dean/create_program', $data);
			}
			$this->content_lib->content();
			return TRUE;
		} 
		
		//todo: server side and browser based validation
		//change the data variable to associative array...
		$data = array();
		$data['departments_id'] 		= $this->session->userdata('departments_id');
		$data['colleges_id']    		= $this->session->userdata('college_id');
		$data['abbreviations'] 			= $this->input->post('abbreviations');
		$data['description']    		= $this->input->post('description');
		$data['acad_program_groups_id']	= $this->input->post('acad_program_groups_id');
		$data['status']         		= "F";
		$data['max_yr_level']			= $this->input->post('max_yr_level');
		$data['inserted_by']			= $this->userinfo['empno'];
		
		if ($this->Programs_Model->AddProgram($data))
			$this->content_lib->set_message('Academic Program Added!', 'success'); else
			$this->content_lib->set_message('Academic Program NOT Added!', 'error');
		$this->content_lib->content();
		
	}
	

		public function offerings(){
	
			$term_checker = $this->academic_terms_model->current_academic_term($this->Enrollments_model->academic_term_for_enrollment_date());
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			$im_here = ($this->session->userdata('data_offerings') ? $this->session->userdata('data_offerings') : FALSE); 

			if ($im_here){
				if ($term_checker->id > $this->session->userdata('data_offerings')->academic_terms_id and !in_array($step,array('1','2','4'))) {
					$this->content_lib->set_message("Too late to do this action. No changes were made.", 'alert-error');
					$step=1;
				}   
			}
				
			switch ($step) {
				case 1:
					$term = $this->academic_terms_model->current_academic_term($this->Enrollments_model->academic_term_for_enrollment_date());
					$this->session->set_userdata('academic_terms_id',$term->id);					
					$this->list_schedule_to_select();
					break;

				case 2: //show course info details

					$this->session->set_userdata('course_offerings_id', $this->input->post('offering_id'));
					$this->session->set_userdata('courses_id', $this->input->post('courses_id'));
					$this->session->set_userdata('parallel_no', $this->input->post('parallel_no'));
	
					$data = $this->CollateData();
					$this->session->set_userdata('parallel_offerings_id', $data['parallel_offerings_id']);
					$data['owner_colleges_id'] = $this->session->userdata('college_id');
					$this->session->set_userdata('academic_terms_id',$data['offerings']->academic_terms_id);
	
					$data['active'] = "tab4";

					$this->content_lib->enqueue_body_content('dean/course_actions',$data);
					$this->content_lib->content();
					break;
	
				case 4: //save data of assigned faculty
					
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
							$offerings = $this->Offerings_Model->ListParallel_Offerings('',$this->session->userdata('parallel_no'),FALSE);
							//print_r($offerings); die();
							if ($offerings) {
								foreach ($offerings AS $offering) {
									$this->Offerings_Model->AssignFaculty($this->input->post('employee_empno'),$offering->course_offerings_id);
								}
							} else {
								$this->Offerings_Model->AssignFaculty($this->input->post('employee_empno'),$this->session->userdata('course_offerings_id'));
							}

							$data = $this->CollateData();
							$data['active'] = "tab5";

							$this->content_lib->set_message("Faculty successfully assigned to schedule!", 'alert-success');

							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');

							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}
						
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;

				/*case 6: //from Change Schedule Slot
					if ($this->input->post('rooms_id')) { //the schedule is changed
						$this->select_vacant_room($this->session->userdata('courses_id'), $this->session->userdata('academic_terms_id'), $this->input->post('rooms_id'), $this->input->post('course_offerings_slots_id'));
					} else { //this is a new schedule
						$this->select_vacant_room($this->session->userdata('courses_id'), $this->session->userdata('academic_terms_id'));
					}
					break;
				*/
				
				case 'update_schedule': //for update schedule
				
					/* NOTE: in updating schedule, two things to check if conflict:
					*	1. check if updated schedule is in conflict with new assigned room 
					*	2. check if updated schedule is in conflict with enrolled students
					*   - Genes 2/2/2017
					*/

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						$this->load->model('hnumis/Enrollments_Model');
						$this->load->model('trans_model');
						
						$start_time = strtotime($this->input->post('start_time_update'));
						$hr         = date('h', strtotime($this->input->post('num_hr_update')));
						$min        = date('i', strtotime($this->input->post('num_hr_update')));
						$end_time   = date("H:i:00", strtotime('+'.$hr.' hours +'.$min.' minutes', $start_time));
						$start_time = date("H:i:00", strtotime($this->input->post('start_time_update')));
						
						$data['academic_terms_id']	       = $this->input->post('academic_terms_id');
						$data['course_offerings_id']       = $this->input->post('course_offerings_id');
						$data['start_time'] 		       = $start_time;
						$data['end_time'] 			       = $end_time;
						$data['parallel_offerings_id']     = $this->session->userdata('parallel_offerings_id');
						$data['course_offerings_slots_id'] = $this->input->post('course_offerings_slots_id');

						$data['days_day_code'] 		 = $this->input->post('day_code_update');
						$data['rooms_id']   		 = $this->input->post('room_id');
						$data['check_conflict']  	 = TRUE; //this is for conflict on offering slot but not on room occupancy
						$data['no_offering']	  	 = TRUE;
						$conflict_students 			 = FALSE;


						if ($this->Offerings_Model->NotDissolved($this->input->post('course_offerings_id'))) { 
							
							//$this->trans_model->start_trans();
							$success_trans = TRUE;
							
							$day_names = $this->Rooms_Model->ListDayNames($this->input->post('day_code_update'));

							foreach ($day_names AS $day) {
								$extracted_days[] = $day->day_name;
							}
												
							$d1   = json_encode($extracted_days);
							$days = str_replace(array('"','[',']'),array("'","(",")"),$d1);
												
							if ($this->Rooms_Model->CheckIfRoomOccupied($this->input->post('academic_terms_id'), $start_time, $end_time, $days, $this->input->post('room_id'), $this->input->post('old_rooms_id'))) {
								$success_trans = FALSE;
								$this->content_lib->set_message("ERROR: Room already occupied!", 'alert-error');
								
							} else {
								
								$offerings_slots = $this->Offerings_Model->getCourseOfferings_Slot($this->input->post('course_offerings_slots_id')); 

								$data1['academic_terms_id'] = $this->input->post('academic_terms_id');
								$data1['rooms_id']          = $offerings_slots->rooms_id;
								$data1['days_day_code']     = $offerings_slots->days_day_code;
								$data1['start_time']        = $offerings_slots->start_time;
								$data1['end_time']          = $offerings_slots->end_time;
							
								$parallel_offerings_slots = $this->Offerings_Model->ListParallel_CourseOfferingsSlots($data1);

								if ($parallel_offerings_slots) { //if there are parallel offerings

									foreach ($parallel_offerings_slots AS $offering) {
										$data['course_offerings_id']       = $offering->course_offerings_id;
										$data['course_offerings_slots_id'] = $offering->course_offerings_slots_id;

										//check students affected by change if conflict on every parallel offering
										$conflict_students = $this->Enrollments_Model->ListStudentsEnrolled_WithConflict($data);
											
										if ($conflict_students) {
											$success_trans = FALSE;
											$this->content_lib->set_message("ERROR: Schedule cannot be updated because some schedules of enrolled students are in conflict!", 'alert-error');
											break;
										} 	
									
										if (!$this->Rooms_Model->ClearRoomsOccupancy($offering->course_offerings_slots_id)) {
											$success_trans = FALSE;
											$this->content_lib->set_message("ERROR in Clearing Room Blocking!", 'alert-error');
											break;
										} 
											
										if (!$this->Offerings_Model->UpdateCourseOfferings_Slot($data)) { 
											$success_trans = FALSE;
											$this->content_lib->set_message('Error Message: '.$this->db->_error_message().'<br>'.$this->db->last_query(), 'alert-error');
											break;
										} 
												
										if (!$this->Rooms_Model->AddRoomOccupancy($data)) {
											$success_trans = FALSE;
											$this->content_lib->set_message("ERROR in Adding Room Blocking!", 'alert-error');
											break;
										}
										
									} //end of foreach parallel offerings	

								} else { //no parallel offerings
									
									//check students affected by change if conflict
									$conflict_students = $this->Enrollments_Model->ListStudentsEnrolled_WithConflict($data);
										
									if ($conflict_students) {
										$success_trans = FALSE;
										$this->content_lib->set_message("ERROR: Schedule cannot be updated because some schedules of enrolled students are in conflict!", 'alert-error');
										
									} else {		

										if (!$this->Rooms_Model->ClearRoomsOccupancy($this->input->post('course_offerings_slots_id'))) {
											
											$success_trans = FALSE;
											$this->content_lib->set_message("ERROR in Clearing Room Blocking!", 'alert-error');
										
										} else {
											
											if (!$this->Offerings_Model->UpdateCourseOfferings_Slot($data)) { 
												
												$success_trans = FALSE;
												$this->content_lib->set_message('Error Message: '.$this->db->_error_message().'<br>'.$this->db->last_query(), 'alert-error');
											
											} else {
												
												if (!$this->Rooms_Model->AddRoomOccupancy($data)) {
													$success_trans = FALSE;
													$this->content_lib->set_message("ERROR in Adding Room Blocking!", 'alert-error');
												} else { 
													$success_trans = TRUE;
													$this->content_lib->set_message("Schedule successfully updated!", 'alert-success');
												} //end of add room occupancy
												
											} //end of updating course offering slot
											
										} //end of clearing room occupancy

									} //end of checking conflict students
									
								} //end of checking if there are parallel offerings
								
							} //end of checking room occupied

							if ($success_trans) {
								$this->trans_model->commit_trans();
								$this->content_lib->set_message("Schedule successfully updated!", 'alert-success');
							} else {
								$this->trans_model->rollback_trans();
							}

							$data = $this->CollateData();
							
							if ($conflict_students) {
								$data['conflict_students'] = $conflict_students;
							}
							
							$data['active'] = "tab1";
							
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();

						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
								
						}
						
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
				
				case 'offer_new_schedule_slot':
				
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						$this->load->model('hnumis/Enrollments_Model');
						$this->load->model('hnumis/rooms_model');
						$this->load->model('trans_model');
						
						$start_time = strtotime($this->input->post('start_time'));
						$hr         = date('h', strtotime($this->input->post('num_hr')));
						$min        = date('i', strtotime($this->input->post('num_hr')));
						$end_time   = date("H:i", strtotime('+'.$hr.' hours +'.$min.' minutes', $start_time));
						$start_time = date("H:i", strtotime($this->input->post('start_time')));
						
						$data['academic_terms_id']	   = $this->input->post('academic_terms_id');
						$data['course_offerings_id']   = $this->input->post('course_offerings_id');
						$data['start_time'] 		   = $start_time;
						$data['end_time'] 			   = $end_time;
						$data['parallel_offerings_id'] = $this->session->userdata('parallel_offerings_id');

						$data['days_day_code'] 		 = $this->input->post('day_code');
						$data['rooms_id']   		 = $this->input->post('room_id');
						$data['check_conflict']  	 = TRUE;
						$data['no_offering']	  	 = TRUE;
						$conflict_students 			 = FALSE;
						
						$this->trans_model->start_trans();
							
						$parallel  = $this->Offerings_Model->getOffering($this->input->post('course_offerings_id'));
						$offerings = $this->Offerings_Model->ListParallel_Offerings('',$parallel->parallel_no,FALSE);
								
						if ($offerings) { //if there are parallel offerings
								
							foreach ($offerings AS $offering) {
								$data['course_offerings_id'] = $offering->course_offerings_id;
								$data['check_conflict']  	 = FALSE;
										
								$room_result = $this->Offerings_Model->AddOfferingSlot($data);
									
								if ($room_result['conflict']) {
										
									$this->trans_model->rollback_trans();
										
									$error_msg = "Schedule slot NOT offered because in conflict with:<br>";
						
									foreach($room_result['schedules'] AS $schedule) {
										$sched = $schedule->course_code."[".$schedule->section_code."] ".
												$schedule->start_time."-".$schedule->end_time." ".
												$schedule->days_day_code." ".$schedule->room_no."<br>";
										$error_msg .= $sched;
									}
									
									$this->content_lib->set_message($error_msg, 'alert-error');
									break;
								} else {

									if (!$room_result['rm_blocking']) {
										$this->trans_model->rollback_trans();
										$this->content_lib->set_message('Error Message: '.$this->rooms_model->get_my_query().'<br>'.$this->db->last_query(), 'alert-error');
									} else {
										$this->trans_model->commit_trans();
										$this->content_lib->set_message("Schedule slot successfully offered!", 'alert-success');
									}
								}
							}
						
						} else { //no parallel offering
								
								$room_result = $this->Offerings_Model->AddOfferingSlot($data);
								
								if ($room_result['conflict']) {
									
									$this->trans_model->rollback_trans();
										
									$error_msg = "Schedule slot NOT offered because in conflict with:<br>";
									foreach($room_result['schedules'] AS $schedule) {
											$sched = $schedule->course_code."[".$schedule->section_code."] ".
													$schedule->start_time."-".$schedule->end_time." ".
													$schedule->days_day_code." ".$schedule->room_no."<br>";
											$error_msg .= $sched;
									}

									$this->content_lib->set_message($error_msg, 'alert-error');
								} else {
									if (!$room_result['rm_blocking']) {
										$this->trans_model->rollback_trans();
										$this->content_lib->set_message('Error Message: '.$this->rooms_model->get_my_query().'<br>'.$this->db->last_query(), 'alert-error');
									} else {
										$this->trans_model->commit_trans();
										$this->content_lib->set_message("Schedule slot successfully offered!", 'alert-success');
									}
								}
						}

						$data = $this->CollateData();
							
						$data['active'] = "tab1";
							
						$this->content_lib->enqueue_body_content('dean/course_actions',$data);
						$this->content_lib->content();
				
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
				
					break;
					
				case 'create_parallel_offering': //saves data of parallel subject

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						$data = array();
		
						$s_code = $this->Offerings_Model->getLastSection($this->input->post('courses_id'),$this->session->userdata('academic_terms_id'));
						//print_r($s_code); die();
						//generate the last section
						if ($s_code) {
							$data['section_code'] = $this->NewSectionCode($s_code->section_code);
						} else {
							$data['section_code'] = "A";
						}

						$data['academic_terms_id']	= $this->input->post('academic_terms_id');
						$data['courses_id']        	= $this->input->post('courses_id');
						$data['employees_empno']   	= $this->session->userdata('data_offerings')->employees_empno;
						$data['max_students']   	= $this->session->userdata('data_offerings')->max_students;
						$data['additional_charge']  = $this->session->userdata('data_offerings')->additional_charge;
						$data['status'] 			= $this->session->userdata('data_offerings')->status;
							
		
						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {

							$this->db->trans_start();
							
							$parallel_result = $this->Offerings_Model->AddParallelOffering($data); //inserts record to course_offerings table
							
							if (!$parallel_result['added']) {
								
								$this->db->trans_rollback();
								$this->content_lib->set_message("ERROR in Adding Parallel Offering!", 'alert-error');
							} else {		
								$data['course_offerings_id'] = $parallel_result['course_offerings_id'];
								$data['no_offering']	  	= TRUE;
							
								$offerings_slots = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
								
								foreach($offerings_slots[0] AS $slot) {
									$data['rooms_id']	    = $slot->rooms_id;
									$data['days_day_code']  = $slot->days_day_code;
									$data['start_time']	    = $slot->start_time;
									$data['end_time']	    = $slot->end_time;
									$data['check_conflict'] = FALSE;
									if ($slot->start_time != '0') {
										$data['sched_type'] = 'fixed';
									} else {
										$data['sched_type'] = 'flexible';										
									}
									
									$room_result = $this->Offerings_Model->AddOfferingSlot($data);
									
									if ($room_result['conflict']) {
										
										$this->db->trans_rollback();
										
										$error_msg = "Course NOT offered because in conflict with:<br>";
										foreach($room_result['schedules'] AS $schedule) {
											$sched = $schedule->course_code."[".$schedule->section_code."] ".
													$schedule->start_time."-".$schedule->end_time." ".
													$schedule->days_day_code." ".$schedule->room_no."<br>";
											$error_msg .= $sched;
										}
										$this->content_lib->set_message($error_msg, 'alert-error');
										break ;
									} else {
										if (!$room_result['rm_blocking']) {
											$this->db->trans_rollback();
											$this->content_lib->set_message("ERROR in Adding Room Blocking!", 'alert-error');
											break 2;
										}
									}
								}

								if ($this->Offerings_Model->AddParallelOfferings($this->session->userdata('course_offerings_id'), $room_result['course_offerings_id'])) {
									$this->db->trans_complete();
									$this->content_lib->set_message("Parallel course successfully offered!", 'alert-success');
								} else {
									$this->db->trans_rollback();
									$this->content_lib->set_message("Error creating parallel course!", 'alert-error');
								}
								
							}
							$data = $this->CollateData();
								
							$data['active'] = "tab4";
							
							$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
							$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
							
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}
	
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
						
				case 'dissolve_offering': //dissolve offering

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					
						if ($this->Offerings_Model->NotDissolved($this->input->post('course_offerings_id'))) {
		
							$this->load->model('teller/assessment_model');
							$this->load->model('hnumis/enrollments_model');
							$this->load->model('hnumis/Student_model');
							$this->load->model('ledger_model');
							$error_msg="";
							
							$parallel  = $this->Offerings_Model->getOffering($this->input->post('course_offerings_id'));
							$offerings = $this->Offerings_Model->ListParallel_Offerings('',$parallel->parallel_no,FALSE);
							$cnt = 0;
							if ($offerings) { //for course offerings with parallel
								foreach ($offerings AS $offering) {
									//get students enrolled in dissolved courses
									$course_offerings_id[] = $offering->course_offerings_id;
									$cnt++;
								}
							} else { //for course offerings with NO parallel
								//get students enrolled in dissolved courses
								$course_offerings_id[] = $this->input->post('course_offerings_id');
								$cnt++;
							}
							
							$offerings_for_adjustments = $course_offerings_id; //for auto adjust of dissolve courses

							$course_offerings_id = json_encode($course_offerings_id);
							$course_offerings_id = str_replace(array('"','[',']'),array("'","(",")"),$course_offerings_id);
		
							$data['students'] = $this->Student_Model->AllEnrollees($course_offerings_id);

							$this->db->trans_start();
							
							//delete records in parallel_offerings table
							if (!$this->Offerings_Model->DeleteParallelOffering($offerings_for_adjustments,$cnt,$this->session->userdata('parallel_no'))) {
								$error_msg = "ERROR deleting parallel offering!";
							} else {
								//change status='D' on all course_offerings including parallel
								if (!$this->Offerings_Model->DissolveOffering($offerings_for_adjustments)) {
									$error_msg = "ERROR dissolving offering!";
								} else {
									
									//clear room occupancy
									if (!$this->Rooms_Model->ClearRoomsOccupancy_ByOfferingsID($this->input->post('course_offerings_id'))) {
										$error_msg = "ERROR: Unable to clear room occupancy!";
									
									} else { 	
									
										//copy records from enrollments to enrollments_history 
										if (!$this->Student_Model->TransferDissolvedToHistory($this->session->userdata('empno'), $course_offerings_id)) {
											$error_msg = "ERROR transferring dissolved data to history!";
										} else {
										
											$current_academic_term = $this->academic_terms_model->getCurrentAcademicTerm();
											$adj['term']   = $current_academic_term->id;
											$adj['date']   = date('Y-m-d H:i:s');
											$adj['type']   = "Credit";
											$adj['empno']  = $this->session->userdata('empno');
											$adj['remark'] = "";
												
											foreach ($offerings_for_adjustments AS $k=>$v) {
												$students = $this->enrollments_model->list_enrollees($v);
												if ($students) {
													foreach($students AS $stud) {
														$offered_course = $this->Offerings_Model->getOffering($v);
														$adj['description'] = "DISSOLVED: ".$offered_course->course_code.'-'.$offered_course->section_code;
														
														if ($this->assessment_model->CheckStudentHasAssessment($stud->student_histories_id)) {
															$tuition_rate_basic = $this->Student_model->getTuitionRateBasic($stud->acad_program_groups_id,
																	$stud->academic_terms_id, $stud->year_level);
															
															//check if course is under tuition_others eg: REED, CWTS
															$tuition_other = $this->assessment_model->getTuitionFee_Others($stud->student_histories_id, $offered_course->courses_id);
															if ($tuition_other) {
																$adj['amount'] = $tuition_other->amt;
															} else {
																$adj['amount'] = $tuition_rate_basic->rate * $offered_course->paying_units;
															}
															
															$adj['academic_programs_id']= $stud->academic_programs_id ;
															$adj['year_level'] = $stud->year_level ;
															$adj['payers_id'] = $stud->id;
																						
															//add records to adjustments
															if (!$this->ledger_model->add_adjustment($adj,'Y',$this->session->userdata('empno'),date('Y-m-d H:i:s'))) {
																$error_msg = "ERROR in writing to ledger!";
																break 2;
															}
																												
															//if course is lab
															$lab = $this->assessment_model->getLab_Fee($stud->student_histories_id, $offered_course->courses_id);

															if ($lab) {
																$adj['description'] = "DISSOLVED (Lab. Fee): ".$offered_course->course_code.'-'.$offered_course->section_code;
																$adj['amount'] = $lab->rate;
																//add records to adjustments
																if (!$this->ledger_model->add_adjustment($adj,'Y',$this->session->userdata('empno'),date('Y-m-d H:i:s'))) {
																	$error_msg = "ERROR in writing to ledger!";
																	break 2;
																}
															}
														}

														
														//delete record from enrollments table
														if (!$this->Student_model->WithdrawCourse($stud->enrollments_id)) {
															$error_msg = "ERROR in deleting student enrollments!";
															break 2;
														}
														
														//update re_enrollments table
														if (!$this->enrollments_model->UpdateRe_enrollments($stud->enrollments_id)) {
															$error_msg = "ERROR in updating student re_enrollments record!";
															break 2;
														}
													}
												}
											}
											
											//delete records in block_course_offerings
											if (!$this->Offerings_Model->UnblockOfferings($offerings_for_adjustments)) {
												$error_msg = "ERROR in unblocking offerings!";
											} else {
												//delete records from allocations
												if (!$this->Offerings_Model->DeallocateOfferings($course_offerings_id)) {
													$error_msg = "ERROR in deallocating offerings!";
												} else { 	
													$this->db->trans_complete();
													$this->content_lib->set_message("Section successfully dissolved!", 'alert-success');
												} //end deallocating
											} //end unblocking 
										} //end tranferring
									}
								}
								
							}

							if ($this->db->trans_status() === FALSE){
								$this->db->trans_rollback();
								$this->content_lib->set_message($error_msg.' :'.$stud->enrollments_id, 'alert-error');
							}
							
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
		
							$this->session->set_userdata('academic_term',$this->input->post('academic_term'));
							$this->session->set_userdata('teacher',$this->input->post('teacher'));
							$this->session->set_userdata('course',$this->input->post('course'));
							
							$this->session->set_userdata('dissolve_students',$data['students']);
							
							$data['parallel_offerings'] = json_decode($this->input->post('parallel_offerings'));
							$data['offering_slots'] = json_decode($this->input->post('offering_slots'));
							$data['dissolve_students'] = json_decode($this->input->post('dissolve_students'));
							$data['academic_term'] = $this->input->post('academic_term');
							$data['teacher'] = $this->input->post('teacher');
							
							$this->list_schedule_to_select(TRUE,$data);
		
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}
						
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
	
				case 12: //saves data to either blocksection_course_offering or allocations

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
			
						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
								
							switch ($this->input->post('allocation_type')) {
								case 'blocks':
									$this->Offerings_Model->AssignBlockToOffering($this->input->post('blocksection'),
									$this->session->userdata('course_offerings_id'));
									break;
								case 'program_groups':
									$programs = $this->Programs_Model->ListAcadPrograms($this->input->post('groups'));
									foreach($programs AS $program) {
		
										//print($this->input->post('yearlevel1'));	die();
										if ($this->input->post('yearlevel1') == '0') {
											$num_yr = $this->Programs_Model->getNumber_of_Years($program->id);
											//print_r($num_yr);	die();
											for($yr=1; $yr <= $num_yr->max_yr_level; $yr++) {
												$this->Offerings_Model->allocate_offering($program->id, $yr,
														$this->session->userdata('course_offerings_id'));
											}
										} else {
											$this->Offerings_Model->allocate_offering($program->id,
													$this->input->post('yearlevel1'),
													$this->session->userdata('course_offerings_id'));
										}
									}
									break;
								case 'programs':
									if ($this->input->post('yearlevel') == '0') {
										$num_yr = $this->Programs_Model->getNumber_of_Years($this->input->post('id'));
										for($yr=1; $yr <= $num_yr->max_yr_level; $yr++) {
											$this->Offerings_Model->allocate_offering($this->input->post('id'), $yr,
													$this->session->userdata('course_offerings_id'));
										}
									} else {
										$this->Offerings_Model->allocate_offering($this->input->post('id'), $this->input->post('yearlevel'),
												$this->session->userdata('course_offerings_id'));
									}
									break;
										
							}
								
							$data = $this->CollateData();
							$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
							$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
		
							$data['active'] = "tab3";
		
							$this->content_lib->set_message("Course Offering successfully allocated!", 'alert-success');
		
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
					
					break;
	
				case 13: //unblock course offering
						
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
							$this->BlockSection_Model->UnblockOffering($this->input->post('block_id'));
		
							$data = $this->CollateData();
							$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
							$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
							$data['active'] = "tab3";
		
							$this->content_lib->set_message("Course offering successfully unblocked!", 'alert-success');
								
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
	
				case 14: //deallocate course offering
						
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
							$this->Offerings_Model->DeallocateOffering($this->input->post('alloc_id'));
		
							$data = $this->CollateData();
							$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
							$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
							$data['active'] = "tab3";
		
							$this->content_lib->set_message("Course offering successfully deallocated!", 'alert-success');
								
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
						
				case 16: //updates class size
	
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
							$offerings = $this->Offerings_Model->ListParallel_Offerings('',$this->session->userdata('parallel_no'),FALSE);
							if ($offerings) {
								foreach ($offerings AS $offering) {
									$this->Offerings_Model->Update_MaxStudents($offering->course_offerings_id, $this->input->post('max_stud'));
								}
							} else {
								$this->Offerings_Model->Update_MaxStudents($this->session->userdata('course_offerings_id'), $this->input->post('max_stud'));
							}
								
							$data = $this->CollateData();
							$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
							$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
		
							$data['active'] = "tab2";
		
							$this->content_lib->set_message("Class size successfully updated!", 'alert-success');
								
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
						
				case 'delete_offering': //remove parallel offering, delete offering

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						$this->load->model('trans_model');
						
						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
		
							$course_offerings_id[] = $this->input->post('course_offerings_id');						
		
							$course_offerings_id = json_encode($course_offerings_id);
							$course_offerings_id = str_replace(array('"','[',']'),array("'","(",")"),$course_offerings_id);						

							$this->trans_model->start_trans();
							
							$this->Enrollments_model->DeleteStudents($course_offerings_id);
							
							if (!$this->Rooms_Model->ClearRoomsOccupancy_ByOfferingsID($this->input->post('course_offerings_id'))) {
									
								$this->trans_model->rollback_trans();
								$this->content_lib->set_message("ERROR: Unable to clear room occupancy!", 'alert-error');

							} else {
								
								if (!$this->Offerings_Model->UnblockOfferings($course_offerings_id, TRUE)){
								
									$this->trans_model->rollback_trans();
									$this->content_lib->set_message("ERROR: Unable to unblock course while deleting the mother offering!", 'alert-error');								

								} else{					
							
									$this->Offerings_Model->DeallocateOfferings($course_offerings_id);
								
									if (!$this->Offerings_Model->DeleteCourseOffering($this->input->post('course_offerings_id'))){
										
										$this->trans_model->rollback_trans();
										$this->content_lib->set_message("ERROR: Unable to delete course offering!", 'alert-error');
										
									}else{
										
										if (!$this->Offerings_Model->DeleteRemainingParallel($this->input->post('parallel_no'))){
											
											$this->trans_model->rollback_trans();
											$this->content_lib->set_message("ERROR: Unable to delete remaining paralle offerings!", 'alert-error');										
										
										}else{
																				
											$this->trans_model->commit_trans();
											$this->content_lib->set_message("Course offering successfully removed!", 'alert-success');
										}	
									}

									
								}
							}

							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');	
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select(TRUE);
							
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
						
				case 18: //shows the form for edit additinal charge
					$this->show_form_to_edit_additional_charge($this->input->post('status'));
					break;
						
				case 19: //updates additional charge
	
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
							$offerings = $this->Offerings_Model->ListParallel_Offerings('',$this->session->userdata('parallel_no'),FALSE);
							if ($offerings) {
								foreach ($offerings AS $offering) {
									$this->Offerings_Model->Update_AddCharge($offering->course_offerings_id, $this->input->post('status'));
								}
							} else {
								$this->Offerings_Model->Update_AddCharge($this->session->userdata('course_offerings_id'), $this->input->post('status'));
							}
								
							$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
							$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
								
							$this->content_lib->set_message("Additional charge successfully updated!", 'alert-success');
								
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
						
				case 20: //download pdf of affected students to dissolve
					$this->load->library('registrar_lib');
					
					$parallel_offerings = json_decode($this->input->post('parallel_offerings'));
					$offering_slots = json_decode($this->input->post('offering_slots'));
					$dissolve_students = str_replace("\'", "'", $this->input->post('dissolve_students'));
					$dissolve_students = json_decode($dissolve_students);
					//print_r($dissolve_students); die();
					$this->registrar_lib->Print_DissolveStudents_pdf($dissolve_students,
							$this->input->post('academic_term'),
							$this->input->post('teacher'),
							$parallel_offerings,
							$offering_slots);
					break;
						
				case 22:
					$this->list_schedule_to_select();
					break;
					
				case 'delete_offerings_slot': //delete schedule offerings slot

					$this->load->model('hnumis/rooms_model');
						
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {

							$this->db->trans_start();
							
							$data['course_offerings_slots_id'] = $this->input->post('course_offerings_slots_id');
							$data['rooms_id']                  = $this->input->post('rooms_id');
							$data['days_day_code']             = $this->input->post('days_day_code');
							$data['start_time']                = $this->input->post('start_time');
							$data['end_time']                  = $this->input->post('end_time');
							
							$parallel_offerings_slots = $this->Offerings_Model->ListParallel_CourseOfferingsSlots($data);

							if ($parallel_offerings_slots) { //if there are parallel by course offerings slots 
								
								foreach ($parallel_offerings_slots AS $offering) {
									if (!$this->rooms_model->ClearRoomsOccupancy($offering->course_offerings_slots_id)) {
										$error_msg = "ERROR: Clearing of room schedule NOT successful!";								
										break;
									}
									
									if (!$this->Offerings_Model->DeleteOfferings_Slot($offering->course_offerings_slots_id)) {
										$error_msg = "ERROR: Deleting offerings slot NOT successful!";
										break;
									}									
								}

								if ($this->db->trans_status() === FALSE){
									$this->db->trans_rollback();
									$this->content_lib->set_message($error_msg, 'alert-error');
								} else {
									$this->db->trans_complete();
									$this->content_lib->set_message("Schedule slot successfully deleted!", 'alert-success');								
								}
								
							} else { //no parallel offering
							
								if ($this->rooms_model->ClearRoomsOccupancy($this->input->post('course_offerings_slots_id'))) {
									if ($this->Offerings_Model->DeleteOfferings_Slot($this->input->post('course_offerings_slots_id'))) {
										$this->db->trans_complete();
										$this->content_lib->set_message("Schedule slot successfully deleted!", 'alert-success');								
									} else {
										$this->db->trans_rollback();
										$this->content_lib->set_message('ERROR: Deleting offerings slot NOT successful!', 'alert-error');
									}
								} else {
									$this->db->trans_rollback();
									$this->content_lib->set_message('ERROR: Clearing of room schedule NOT successful!', 'alert-error');								
								}
							
							}
							
							$data = $this->CollateData();
							$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
							$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
							$data['active'] = "tab1";
						
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
						
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
						
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}
						
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;

				// added(copied) by Toyet 3.2.2018
				case 'step_update_isrequested': //updates isrequested field

					////log_message("INFO", "XXXX dean/offerings()");
	
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
							$offerings = $this->Offerings_Model->ListParallel_Offerings('',$this->session->userdata('parallel_no'),FALSE);

							$isrequested = $_POST['isrequested_value'];
							$course_offerings_id = $_POST['course_offerings_id'];
							$this->Offerings_Model->Update_IsRequested($isrequested,$course_offerings_id);						
							$data = $this->CollateData();
							$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
							$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
		
							$data['active'] = "tab9";
		
							$this->content_lib->set_message("Course Offering status successfully updated!", 'alert-success');
								
							$this->content_lib->enqueue_body_content('dean/course_actions',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
		
							$this->session->unset_userdata('course_offerings_id');
							$this->session->unset_userdata('courses_id');
							$this->session->unset_userdata('parallel_no');
		
							$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
							$this->session->set_userdata('academic_terms_id',$term->id);
							$this->list_schedule_to_select();
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
					
				default:

					break;
						
			}
	
		}
		
	
	private function CollateData() {
	
		//data for offer parallel course
		$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
		$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
		$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
		$this->session->set_userdata('data_offerings', $data['offerings']);
		$data['courses']= $this->Courses_Model->ListAllCourses();
		
		$list_parallel_offerings = $this->Offerings_Model->ListParallel_Offerings($data['offerings']->id,$data['offerings']->parallel_no,FALSE);
		//print_r($data['offerings']); die();
		
		//data for offer schedule slots
		$data['days'] = $this->Offerings_Model->ListDays();
		$this->session->set_userdata('courses_id',$data['offerings']->courses_id);
		
		//data for assigned faculty
		$data['faculty'] = $this->Faculty_Model->ListAllFaculty();
			
		//data for allocation
		$data['blocksection']   = $this->BlockSection_Model->ListAllSections();
		$data['programs'] = $this->Programs_Model->all_programs_colleges();
		$data['program_groups'] = $this->Programs_Model->ListProgramWithGroups();
		$data['allocated_blocks'] = $this->Offerings_Model->ListBlockOfferings($this->session->userdata('course_offerings_id'));
		$data['allocation'] = $this->Offerings_Model->ListAllocation($this->session->userdata('course_offerings_id'));
	
		//data for dissolve section
		$offerings = $this->Offerings_Model->ListParallel_Offerings('',$this->session->userdata('parallel_no'),FALSE);
		$cnt = 0;
		if ($offerings) {
			foreach ($offerings AS $offering) {
				//get students enrolled in dissolved courses
				$course_offerings_id[] = $offering->course_offerings_id;
				$cnt++;
			}
		} else {
			//get students enrolled in dissolved courses
			$course_offerings_id[] = $this->session->userdata('course_offerings_id');
			$cnt++;
		}
			
		$course_offerings_id = json_encode($course_offerings_id);
		$course_offerings_id = str_replace(array('"','[',']'),array("'","(",")"),$course_offerings_id);
		$data['students'] = $this->Student_Model->AllEnrollees($course_offerings_id);
		$data['parallel_offerings_id'] = $course_offerings_id;
	
		return $data;
	
	}
	
	public function faculty(){
		$this->load->library('faculty_lib');
		$this->faculty_lib->faculty(TRUE);
	}
	
	
	

/***********************************************************************************************/

/***********************************************************************************************/
	
	//PRIVATE methods...
	private function list_terms_courses($academic_terms_id) {
		
		if (empty($academic_terms_id)) {
			$acad_term_id = $this->academic_terms_model->current_academic_term();
			$acad_id = $acad_term_id->id;
		} else {
			$acad_id = $academic_terms_id;
		}
		
		$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms();
		$data['offerings'] = $this->Offerings_Model->ListOfferings($acad_id,$this->session->userdata('college_id'));
		$data['acad_id'] = $acad_id;
		
		$this->content_lib->enqueue_body_content('dean/offer_coarses_schedule_step1',$data);
		$this->content_lib->enqueue_body_content('dean/list_offered_schedules',$data);
		$this->content_lib->content();
	}
	
	private function input_courses($academic_terms_id) {
			
		$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
		$data['courses'] = $this->Courses_Model->ListCourses($this->userinfo['college_id'],'A');
		$data['rooms'] = $this->Rooms_Model->ListRooms();
		$data['days'] = $this->Offerings_Model->ListDays();
		//$academic_terms = $this->academic_terms_model->current_academic_term();
		$data['offerings'] = $this->Offerings_Model->ListOfferings($academic_terms_id,$this->session->userdata('college_id'));
			
		$this->content_lib->enqueue_body_content('dean/input_offering_schedules',$data);
		$this->content_lib->enqueue_body_content('dean/list_offered_schedules',$data);
		$this->content_lib->content();
			
	}
	

	
	/*private function input_new_slot() {
			
		$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));

		$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
		$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
		$data['days']           = $this->Offerings_Model->ListDays();
	
		$this->session->set_userdata('courses_id',$data['offerings']->courses_id);
			
		$this->content_lib->enqueue_body_content('dean/input_new_slot',$data);
		$this->content_lib->content();
			
	}*/
	
	
	private function select_offering_faculty() {
		
		$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));

		$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
		$data['faculty'] 		= $this->Faculty_Model->ListAllFaculty();	
		$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
		
		$this->content_lib->enqueue_body_content('dean/assign_faculty_to_schedule',$data);
		$this->content_lib->content();
			
	}

		public function prospectus() {
		
		if ($this->common->nonce_is_valid($this->input->post('nonce'))) {	
			$step = ($this->input->post('step') ?  $this->input->post('step') : 2);
		
			switch ($step) {
				case 2:
					$this->list_prospectus();
					break;
		
				case 3: //form to create prospectus
					$data['programs'] = $this->Programs_Model->ListPrograms($this->session->userdata('college_id'),'OF');
					//print_r($data); die();
					$this->content_lib->enqueue_body_content('dean/create_prospectus_form',$data);
					$this->content_lib->content();
					break;
				case 4: //save prospectus
					$data['academic_programs_id'] = $this->input->post('academic_programs_id');
					$data['effective_year'] = $this->input->post('effective_year');
					$data['status'] = 'I';
						
					$this->Prospectus_Model->AddProspectus($data);
					redirect('dean/prospectus');
		
					break;
				case 5:
					$this->session->set_userdata('prospectus_id',$this->input->post('prospectus_id'));
					$this->list_prospectus_terms();
		
					break;
				case 6: //form to create prospectus term
					$data['prospectus'] = $this->Prospectus_Model->getProspectus($this->session->userdata('prospectus_id'));
						
					$this->content_lib->enqueue_body_content('dean/create_prospectus_term_form',$data);
					$this->content_lib->content();
					break;
				case 7: //save prospectus terms data
					$data['prospectus_id'] = $this->session->userdata('prospectus_id');
					$data['year_level']    = $this->input->post('year_level');
					$data['term']          = $this->input->post('term');
					$data['inserted_by']   = $this->userinfo['empno'];
						
					$this->Prospectus_Model->AddProspectusTerm($data);
					$this->list_prospectus_terms();
		
					break;
				
				/*transfferred to modal	
				case 8: //form to add prospectus courses
					$this->session->set_userdata('prospectus_terms_id',$this->input->post('prospectus_terms_id'));
		
					$data['prospectus'] = $this->Prospectus_Model->getProspectus($this->session->userdata('prospectus_id'));
					$data['term']       = $this->Prospectus_Model->getProspectusTerm($this->session->userdata('prospectus_terms_id'));
					$data['courses']    = $this->Courses_Model->ListAllCourses();
						
					$this->content_lib->enqueue_body_content('dean/create_prospectus_course_form',$data);
					$this->content_lib->content();
					break;
				*/
					
				//case 9:
				case 'add_prospectus_course': //save prospectus courses data and Update max_bracketed_units and max_credit_units of prospectus terms 
					//$data['prospectus_terms_id'] = $this->session->userdata('prospectus_terms_id');
					$data['prospectus_terms_id'] = $this->input->post('term_id');
					$data['courses_id']    = $this->input->post('courses_id');
					$data['elective']      = $this->input->post('elective');
					$data['is_major']      = $this->input->post('is_major');
					$data['is_bracketed']  = $this->input->post('is_bracketed');
					
					if ($this->input->post('cutoff_grade')) {
						$data['cutoff_grade']  = $this->input->post('cutoff_grade');
					} else {
						$data['cutoff_grade']  = NULL;
					}
					
					if ($this->input->post('num_retakes')) {
						$data['num_retakes']  = $this->input->post('num_retakes');
					} else {
						$data['num_retakes']  = NULL;
					}
						
					$units = $this->Courses_Model->RetrieveCourse($this->input->post('courses_id'));

					if ($this->Prospectus_Model->AddProspectusCourse($data)) {
						if ($this->Prospectus_Model->UpdateProspectusTermUnits($this->input->post('term_id'))) {
							$this->content_lib->set_message("Prospectus Course successfully added!", 'alert-success');
						} else {
							$this->content_lib->set_message("Prospectus Course not added!", 'alert-error');
						}
					} else {
						$this->content_lib->set_message("Prospectus Course not added!", 'alert-error');
					}
					//redirect('dean/prospectus');
					$this->list_prospectus_terms();
		
					break;
				case 10: //form to show prerequisites details
					$this->session->set_userdata('prospectus_courses_id',$this->input->post('prospectus_courses_id'));

					$this->list_courses_prereq();

					break;
				case 11: //save prerequisites data
					$this->load->model('hnumis/Prerequisites_Model');

					$data['prospectus_courses_id'] = $this->session->userdata('prospectus_courses_id');
					$data['prereq_type']  = $this->input->post('prereq_type');
					$data['courses_id']   = $this->input->post('prospectus_courses_id');
					$data['yr_level']     = $this->input->post('yr_level');
					$data['grade']  	  = $this->input->post('grade');
					
					//retrieve prerequisites of the inputted prospectus_courses_id
					$data['prereqs']  = $this->Prospectus_Model->ListPrereq_ByCourse($this->input->post('prospectus_courses_id'));
						
					//print_r($data['prereqs']);
					//die();
					$this->Prerequisites_Model->AddPrerequisites($data);
		
					//$this->list_prospectus_terms();
					$this->list_courses_prereq();
		
					break;
				case 'delete_prospectus_course': //delete prospectus course
					
					$this->load->model('hnumis/Credited_Courses_Model');
					$this->load->model('accounts/Fees_Schedule_Model');
						
					//$course = $this->Prospectus_Model->getProspectusCourse($this->input->post('prospectus_courses_id'));
					//$unit = -abs($course->credit_units);
					
					$this->db->trans_start();
					
					if ($this->Credited_Courses_Model->DeletedCredited_Courses($this->input->post('prospectus_courses_id'))) {
						if ($this->Fees_Schedule_Model->DeleteFees_Schedule_Prospectus($this->input->post('prospectus_courses_id'))) {
							if ($this->Prospectus_Model->DeleteProspectusCourse($this->input->post('prospectus_courses_id'))) {
								$this->Prospectus_Model->UpdateProspectusTermUnits($this->input->post('prospectus_terms_id'));
								$this->db->trans_complete();
								$this->content_lib->set_message("Prospectus Course successfully deleted!", 'alert-success');
							} else {
								$this->db->trans_rollback();
								$this->content_lib->set_message("Error deleting the Prospectus Course!", 'alert-error');
							}	
						} else {
							$this->db->trans_rollback();
							$this->content_lib->set_message("ERROR: Course has already assigned a Fee Schedule!", 'alert-error');
						}	
					} else {
						$this->db->trans_rollback();
						$this->content_lib->set_message("ERROR: Course has already been credited to a student!", 'alert-error');
					}
					
					$this->list_prospectus_terms();
		
					break;
				case 13: //delete prerequisite detail
					$this->load->model('hnumis/Prerequisites_Model');
					
					$this->Prerequisites_Model->DeletePrerequisite($this->input->post('prerequisites_id'),$this->input->post('prereq_type'));

					$this->list_courses_prereq();
		
					break;
				case 14:
					$this->list_prospectus_terms();
					break;
		
				case 'edit_prospectus_course':
				
						$data['prospectus_course_id'] = $this->input->post('prospectus_course_id');
						$data['elective']      = $this->input->post('elective');
						$data['is_major']      = $this->input->post('is_major');
						$data['is_bracketed']  = $this->input->post('is_bracketed');

						if($this->input->post('cutoff_grade')){
							$data['cutoff_grade']  = $this->input->post('cutoff_grade');
						} else {	
							$data['cutoff_grade']  = NULL;
						}
						if($this->input->post('num_retakes')){
							$data['num_retakes']  = $this->input->post('num_retakes');
						}else {	
							$data['num_retakes']  = NULL;
						}
						
						$this->db->trans_start();
											
						if ($this->Prospectus_Model->UpdateProspectusCourse($data)) {
							$this->Prospectus_Model->UpdateProspectusTermUnits($this->input->post('prospectus_terms_id'));
							$this->db->trans_complete();
							$this->content_lib->set_message("Prospectus course successfully updated!", "alert-success"); 
						} else {
							$this->db->trans_rollback();
							$this->content_lib->set_message("Error found while updating prospectus course!", "alert-error");
						}
						
						$this->list_prospectus_terms();
			
					break;
				default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					break;
			}
			}else {
						$this->content_lib->set_message("Error in submitting form", 'alert-error');	
				   	}
		}
		
		
		private function select_vacant_room($courses_id, $academic_terms_id, $rooms_id=FALSE, $course_offerings_slots_id=FALSE) {
			
			$hr1  = substr($this->input->post('start_time'),0,2);
			$min1 = substr($this->input->post('start_time'),3,2);
			$hr2  = substr($this->input->post('num_hr'),0,2);
			$min2 = substr($this->input->post('num_hr'),3,2);
			
			$start_time = date('H:i', mktime($hr1,$min1,0,0,0,0));
			$end_time	= date('H:i', mktime($hr1+0+$hr2,$min1+$min2,0,0,0,0));
			
			$start_time2 = date('h:iA', mktime($hr1,$min1,0,0,0,0));
			$end_time2	= date('h:iA', mktime($hr1+0+$hr2,$min1+$min2,0,0,0,0));
				
			$this->session->set_userdata('sched_type',$this->input->post('sched_type'));
			$this->session->set_userdata('start_time',$start_time);
			$this->session->set_userdata('end_time',$end_time);
			//$this->session->set_userdata('courses_id',$courses_id);
			//$this->session->set_userdata('days_day_code',$this->input->post('day_code'));
			//$this->session->set_userdata('max_students',$this->input->post('max_students'));
			//$this->session->set_userdata('additional_charge',$this->input->post('additional_charge'));
		
			//NOTE: 5/2/15 by genes
			//sessions do not work correctly; session memory might be the cause
			$data['days_day_code'] = $this->input->post('day_code');
			$data['courses_id'] = $this->input->post('courses_id');
			$data['max_students'] = $this->input->post('max_students');
			$data['additional_charge'] = $this->input->post('additional_charge');
		
			
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			$data['course']         = $this->Courses_Model->RetrieveCourse($courses_id);
			$data['schedule']       = $start_time2." - ".$end_time2." ".$this->input->post('day_code');
		
			$day_names = $this->Rooms_Model->ListDayNames($this->input->post('day_code'));
		
			foreach ($day_names AS $day) {
				$days[] = $day->day_name;
			}
			$d1 = json_encode($days);
			$d2 = str_replace(array('"','[',']'),array("'","(",")"),$d1);
		
			if ($this->input->post('sched_type') == 'flexible') { //flexible time is from 7:30AM - 10:30PM
				$start_time = date('H:i', mktime(0,0,0,0,0,0));
				$end_time	= date('H:i', mktime(0,0,0,0,0,0));
				$this->session->set_userdata('start_time',$start_time);
				$this->session->set_userdata('end_time',$end_time);
				$data['schedule']       = $start_time." - ".$end_time." ".$this->input->post('day_code');
				
				$data['bates_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
						'07:30', '22:30', $d2,3);
				//$data['main_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
					//	'07:30', '22:30', $d2,1);
				$data['scanlon_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
						'07:30', '22:30', $d2,9);
				$data['freina_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
						'07:30', '22:30', $d2,7);
				$data['other_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
						'07:30', '22:30', $d2,11);
			} else {
				
				if (!$rooms_id) { //new schedule
					$data['bates_building'] 	= $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
													$start_time, $end_time, $d2,3);
					//$data['main_building'] 		= $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
					//								$start_time, $end_time, $d2,1);
					$data['scanlon_building'] 	= $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
													$start_time, $end_time, $d2,9);
					$data['freina_building'] 	= $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
													$start_time, $end_time, $d2,7);
					$data['other_building'] 	= $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
													$start_time, $end_time, $d2,11);
					$data['old_days_day_code'] 	= "";
					$data['old_start_time'] 	= "";
						
				} else { //the schedule is changed
					$data['bates_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
							$start_time, $end_time, $d2,3,$rooms_id,$this->input->post('course_offerings_slots_id'));
					//$data['main_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
					//		$start_time, $end_time, $d2,1,$rooms_id,$this->input->post('course_offerings_slots_id'));
					$data['scanlon_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
							$start_time, $end_time, $d2,9,$rooms_id,$this->input->post('course_offerings_slots_id'));
					$data['freina_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
							$start_time, $end_time, $d2,7,$rooms_id,$this->input->post('course_offerings_slots_id'));
					$data['other_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
							$start_time, $end_time, $d2,11,$rooms_id, $course_offerings_slots_id);
					//data for change schedule
					$data['rooms_id'] 				   = $rooms_id;
					$data['old_bldgs_id'] 			   = $this->input->post('old_bldgs_id');
					$data['course_offerings_slots_id'] = $course_offerings_slots_id;
					$data['old_days_day_code'] 		   = $this->input->post('old_days_day_code');
					$data['old_start_time'] 		   = $this->input->post('old_start_time');
					
					//$this->session->set_userdata('old_days_day_code',$this->input->post('old_days_day_code'));
					//$this->session->set_userdata('old_start_time',$this->input->post('old_start_time'));
					
					//$this->session->set_userdata('course_offerings_slots_id',$this->input->post('course_offerings_slots_id'));
					//$this->session->set_flashdata('course_offerings_slots_id',$this->input->post('course_offerings_slots_id'));
					//$this->session->keep_flashdata('course_offerings_slots_id');
					//print_r($this->session->userdata); die();
				}
			}
			
			$data['offerings'] = $this->Offerings_Model->ListOfferings($academic_terms_id,$this->session->userdata('college_id'));
			
			$this->content_lib->enqueue_body_content('dean/select_vacant_room',$data);
			$this->content_lib->enqueue_body_content('dean/list_offered_schedules',$data);
			$this->content_lib->content();
	}
				
		private function list_prospectus() {
				
			$data['title'] = "Prospectus";
			$data['prospectus'] = $this->Prospectus_Model->ListProspectus($this->session->userdata('college_id'));
			$data['terms_sy'] = $this->AcademicYears_Model->getCurrentAcademicTerm();
		
			$this->content_lib->enqueue_body_content('dean/header_title',$data);
			$this->content_lib->enqueue_body_content('dean/list_prospectus_to_select',$data);
			$this->content_lib->content();
		
		}
		
		private function list_prospectus_terms() {
				
			$prospectus = $this->Prospectus_Model->getProspectus($this->session->userdata('prospectus_id'));
			$data['title'] = $prospectus->description." - ".$prospectus->effective_year;
			$data['prospectus_terms'] = $this->Prospectus_Model->ListProspectusTerms($this->session->userdata('prospectus_id'));
			$data['terms_sy'] = $this->AcademicYears_Model->getCurrentAcademicTerm();
			$data['all_courses']  = $this->Courses_Model->ListAllCourses();
				
			////log_message("INFO", print_r($data,true)); //Toyet 5.3.2018

			$this->content_lib->enqueue_body_content('dean/header_title',$data);
			$this->content_lib->enqueue_body_content('dean/list_prospectus_terms',$data);
			$this->content_lib->content();
		
		}
		

		private function list_courses_prereq() {
				
			$data['prospectus'] = $this->Prospectus_Model->getProspectus($this->session->userdata('prospectus_id'));
			$data['courses']    = $this->Prospectus_Model->getProspectusCourse($this->session->userdata('prospectus_courses_id'));
			$data['term']       = $this->Prospectus_Model->getProspectusTerm($data['courses']->prospectus_terms_id);
			
			//retrieve all prerequisites for display
			$data['prereqs']    = $this->Prospectus_Model->ListAllPrereq($this->session->userdata('prospectus_courses_id'));

			//retrieve all courses allowed as prerequisites
			$data['prereq_allowed'] = $this->Prospectus_Model->ListPrereqAllowed($data['term']->y_level1, $data['term']->term1, $this->session->userdata('prospectus_id'));
						
			$this->content_lib->enqueue_body_content('dean/create_prereq_form',$data);
			$this->content_lib->content();
		
		}
		
		private function list_schedule_to_select($DissolveStudents=FALSE,$data=NULL) {
			//print_r($this->session->all_userdata());die();
		
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/Courses_model');
			
			$data['terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
			
			if ($this->input->post('academic_terms_id')) {
				$academic_terms_id = $this->input->post('academic_terms_id');
			} else {
				$this->load->model('academic_terms_model');
				$academic_terms = $this->academic_terms_model->current_academic_term();
				$academic_terms_id = $academic_terms->id;
			}
			
			if ($DissolveStudents) {
				$data['dissolve_students'] = TRUE;
			}

			$data['offerings'] = $this->Offerings_Model->ListOfferings($academic_terms_id,$this->session->userdata('college_id'));
			$data['terms_sy'] = $this->AcademicYears_Model->getAcademicTerms($academic_terms_id);
		
			$data['selected_term'] = $academic_terms_id;
			
			//print_r($data); die();
		
			$this->content_lib->enqueue_footer_script('data_tables');
			$this->content_lib->enqueue_body_content('dean/list_schedule_to_select',$data);
			$this->content_lib->content();
		}

		
		//Added: 01/09/2013
		//Modified: 01/12/2013
		public function create_blocksection(){
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
					$this->list_academicprograms(1);
					break;
				case 2:
				    $oldsection = $this->BlockSection_Model->GetLastSection($this->input->post('academic_program'), $this->input->post('yearlevel'));
					//$newsection = $this->NewSectionCode($oldsection);
					if ($oldsection) {
						$code = $this->NewSectionCode($oldsection->section_name);
					} else {
						$code = "A";
					}
					$this->BlockSection_Model->add_blocksection($this->input->post('academic_program'),$this->input->post('yearlevel'),$code);
					$this->session->set_flashdata('message','Block Section Successfully Added!');
					redirect('dean/create_blocksection');
					break;
							
				default:
					//$this->content_lib->enqueue_body_content('dean/add_blocksection',$data);

					//$this->content_lib->content();
					//break;
			}
			
			//$this->load->view('dean/footer');
			
		}

		
		//Added: 01/09/2013
		private function list_academicprograms($flag=null) {
			$this->load->library('form_lib');
			
			$data['academic_program'] = $this->Programs_Model->ListPrograms($this->session->userdata('college_id'),'O'); //edited 01/28/2013
			//$data['blocksection'] = $this->BlockSection_Model->ListAllSections();
			switch($flag) {
				case 1:	$data['message'] = $this->session->flashdata('message');
						//$data['form_lib'] =& $this->form_lib;
						//echo '<pre>test';
						//print_r(get_class_methods($this->form_lib));
						//return;
						$this->content_lib->enqueue_body_content('dean/create_blocksection',$data); 
						$this->content_lib->content();
						break;
				case 2: $this->content_lib->enqueue_body_content('dean/view_blocksection',$data); 
						$this->content_lib->content();
						break;
			}
			
		}
		
		//Added: 1/10/13
		public function allocate_section() {
			$step = ($this->input->post('step') ?  $this->input->post('step') : 2);
				
			switch ($step) {
				case 2: //shows course offerings for allocation
					$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
					$this->session->set_userdata('academic_terms_id',$term->id);
					$this->list_schedule_to_select();
					break;
				case 3: //shows the form for allocation				
					$offering_id = $this->input->post('offering_id');
					$this->session->set_userdata('course_offerings_id', $offering_id);
					
					$this->show_form_to_allocate_offering();
					break;
				case 4: //saves data to either blocksection_course_offering or allocations
					
					if ($this->input->post('allocation_type') == 'blocks') {
						$this->Offerings_Model->AssignBlockToOffering($this->input->post('blocksection'),$this->session->userdata('course_offerings_id'));
						redirect('dean/dean'); }
					else {
						$this->Offerings_Model->allocate_offering($this->input->post('id'), $this->input->post('yearlevel'), $this->session->userdata('course_offerings_id'));
						redirect('dean/dean'); 	}
					
					$this->show_form_to_allocate_offering();
					break;
				case 5:	//deallocate the course from a specific group			
					$offering_id = $this->input->post('offering_id');
					$this->session->set_userdata('course_offerings_id', $offering_id);
					//$this->session->set_userdata('academic_terms_id',$this->input->post('academic_terms_id'));
					$this->list_offerings_block('U');
					break;
				case 6:
					$this->BlockSection_Model->UnblockOffering($this->session->userdata('course_offerings_id'));
					redirect('dean/dean');
					break;
							
				default:
					$this->session->unset_userdata('academic_terms_id');
	
					$this->list_terms_courses('');
					break;
			}
			
			//$this->load->view('dean/footer');

		}
		
		//Added: March 9, 2013 by Amie
		private function show_form_to_allocate_offering() {
					
			$data['blocksection']   = $this->BlockSection_Model->ListAllSections();
			$data['programs'] = $this->Programs_Model->all_programs_colleges();
			$data['program_groups'] = $this->Programs_Model->ListProgramWithGroups();
			$blocks = $this->Offerings_Model->ListBlockOfferings($this->session->userdata('course_offerings_id'));
			$data['allocated_blocks'] = $blocks;
			$allocation = $this->Offerings_Model->ListAllocation($this->session->userdata('course_offerings_id'));
			$data['allocation'] = $allocation;
						
			$this->content_lib->enqueue_body_content('dean/assign_blocksection',$data); 
			$this->content_lib->content();
		}
		
		
		
		private function NewSectionCode($section_code) {
			$s = 0;
			$s1="";
			$s2="";
			$code="";
				
			$s = strlen($section_code);
							
			if ($s == 1 AND ($section_code != 'Z')) {
				$code = CHR(ORD($section_code) + 1);
			} elseif ($s == 1 AND ($section_code == 'Y')) {
				$code = 'Z';
			} elseif ($s == 1 AND ($section_code == 'Z')) {
				$code = 'AA';
			} elseif ($s > 1) {
				$s1 = substr($section_code,0,1);
				$s2 = substr($section_code,1,1);					
				if ($s1 != 'Z') {
					if ($s2 != 'Z') {
						$code = $s1.CHR((ORD($s2) + 1));
					} else {
						$s1 = CHR(ORD($s1) + 1);
						$s2 = 'A';						
						$code = $s1.$s2;
					}
				} else {
					if ($s2 != 'Z') {
						$code = $s1.CHR((ORD($s2) + 1));
					} 
				}							
			}
			
			return $code ;
			
		}
		
		
		//Added: January 14, 2013 by Amie
		//edited: january 18, 2013 by Amie
		public function view_blocksection(){
			$step = ($this->input->get_post('step') ?  $this->input->get_post('step') : 1);
				
			switch ($step) {
				case 1:
					$this->list_academicprograms(2);
					break;
				case 2:
					$current = $this->academic_terms_model->getCurrentAcademicTerm(); 
				    $data['blocks'] = $this->BlockSection_Model->ListBlocks($this->input->get_post('academic_program'),$this->input->post('yearlevel'),$current->id);
					$data['academic_terms'] = $current;
					$data['academic_program'] = $this->Programs_Model->getProgram($this->input->get_post('academic_program'));
					$data['yearlevel'] = $this->input->post('yearlevel');
					
					$this->content_lib->enqueue_body_content('dean/list_blocksections',$data); 
					$this->content_lib->content();
					break;
				case 3:
					$block_id = $this->input->post('id');
					$current = $this->academic_terms_model->getCurrentAcademicTerm(); 
//					$this->session->set_userdata('block_id', $block_id);
					$data['block'] = $this->BlockSection_Model->getSection($block_id);
					$data['blocklist'] = $this->Student_Model->ListBlockStudents($this->input->post('id'), $current->id);
			
					$this->content_lib->enqueue_body_content('dean/view_blocklist',$data); 
					$this->content_lib->content();
					break;
				
							
				default:
					//$this->content_lib->enqueue_body_content('dean/add_blocksection',$data);

					//$this->content_lib->content();
					//break;
			}
			
			//$this->load->view('dean/footer');
			
		}
		
		//Added: Feb. 9, 2013 by Amie
/*		public function creditcourse(){
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
		    
			$this->load->model('hnumis/Student_Model');

   	
			switch ($step) {
				case 1: //selects a specific course from the prospectus
					$this->session->set_userdata('prospectus_courses_id', $this->input->post('prospectus_courses_id'));
					$this->session->set_userdata('idnum', $this->input->post('idnum'));
					$this->session->set_userdata('courses_id', $this->input->post('courses_id'));
					//$this->session->set_userdata('course_notes', $this->input->post('course_code')." - ".$this->input->post('course_desc'));
					$this->show_form_to_add_credited_courses();
					break;
				case 2:  //saves data to credited_courses
					
					$enrollment = $this->Enrollments_model->getCourseInfo($this->input->post('enrollments_id'));
					if($this->input->post('taken_type') == 'Y'){
					    $notes = $enrollment->course_code." - ".$enrollment->descriptive_title;
						$grade = $enrollment->finals_grade;
						$this->session->set_userdata('course_notes', $this->input->post('course_code')." - ".$this->input->post('course_desc'));
					} else {
						$notes = $this->input->post('course');
						$grade = $this->input->post('outside_hnu_grade');
					}
					$this->Student_Model->creditcourse($this->session->userdata('idnum'),$this->session->userdata('prospectus_courses_id'),$grade,$this->input->post('taken_type'),$notes);
					$this->content_lib->set_message("Course successfully credited!", 'alert-success');

					$this->show_form_to_add_credited_courses();
					break;
				case 3: //deletes a credited course
				   
					$this->Student_Model->Remove_credited_course($this->input->post('credited_id'));
					
					$this->show_form_to_add_credited_courses();
					break;
					
				case 4:  //displays student prospectus
					$this->display_prospectus();
			
				default:
			}
		
		
		}

				
		//Added: Feb. 16, 2013 by Amie
		public function show_form_to_add_credited_courses() {

			$this->load->model('hnumis/Enrollments_model');
			
			$prospectus_id = $this->Prospectus_Model->student_prospectus_ids($this->session->userdata('idnum'));
			$data['courses_passed'] = $this->Enrollments_model->getPassedCourses($this->session->userdata('idnum'), $prospectus_id);
			$data['course'] = $this->Courses_Model->getCourseDetails($this->session->userdata('courses_id'));
			
			
			//$this->session->set_userdata('pc_id', $data['course']->id);
			
			$courses = $this->Student_Model->view_creditedcourses($this->session->userdata('idnum'), $this->session->userdata('prospectus_courses_id'));
			$data['credited'] = $courses;
			
			$this->content_lib->enqueue_body_content('dean/creditcourse',$data); 
			$this->content_lib->content();
		}
*/		
		
		public function download_student_list_csv (){
			$year_level = $this->input->post('year_level');
			$program = $this->input->post('program');
		
			$this->load->model('student_common_model');
			$records = $this->student_common_model->search_by_program ($program, $year_level, TRUE);
			$filename = $records[0]->abbr . "_" . $year_level . ".csv";
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=' . $filename);
		
			// create a file pointer connected to the output stream
			$output = fopen('php://output', 'w');
		
			// output the column headings
			fputcsv($output, array('No.', 'Name', 'Gender', 'Course and Year Level'));
			$count = 0;
			foreach ($records as $record){
				$count++; 
				fputcsv ($output, array($count, iconv( 'utf-8', 'utf-8', $record->fullname), $record->gender, $record->abbr . " ". $record->year_level));
			}
		}
		
		//Added: 01/26/2013 By: genes
		public function assign_elective_topic() {

			$this->load->model('hnumis/Electives_Model');
					
			$step = $this->input->post('step') ?  $this->input->post('step') : 1;
				
			switch ($step) {
				case 1: //display programs
					$this->list_prospectus_to_create_elective();
					break;
				case 2: //display elective courses on a selected program
					if ($this->input->post('prospectus_id')) {
						$this->session->set_userdata('prospectus_id',$this->input->post('prospectus_id'));
					}

					$this->list_electives_to_assign_topic();
					break;
				case 3: //display form to add topic
					if ($this->input->post('prospectus_courses_id')) {
						$this->session->set_userdata('prospectus_courses_id',$this->input->post('prospectus_courses_id'));
					}

					$this->show_form_to_create_elective_topic();

					break;
				case 4: //save topic to a selected elective
					$data['prospectus_courses_id'] = $this->session->userdata('prospectus_courses_id');
					$data['topic_courses_id']      = $this->input->post('topic_courses_id');

					$this->Electives_Model->AddElectiveTopic($data);
		
					$this->show_form_to_create_elective_topic();
		
					break;
				case 5: //delete elective topic
					$this->Electives_Model->DeleteElectiveTopic($this->input->post('topic_id'));
					
					$this->show_form_to_create_elective_topic();
		
					break;
				case 6: //display form to add topic prerequisites
					$this->session->set_userdata('elective_courses_id',$this->input->post('topic_id'));

					$this->show_form_to_create_topic_prereq();

					break;
				case 7: //save prerequisite to selected topic
					$data['elective_courses_id'] = $this->session->userdata('elective_courses_id');

					$data['prereq_type']     	    = $this->input->post('prereq_type');
					$data['prospectus_courses_id']  = $this->input->post('prospectus_courses_id');
					$data['yr_level']   			= $this->input->post('yr_level');

					//retrieve prerequisites of the inputted prospectus_courses_id
					$data['prereqs']  = $this->Prospectus_Model->ListPrereq_ByCourse($this->input->post('prospectus_courses_id'));

					$this->Electives_Model->AddTopicPrerequisite($data);
		
					$this->show_form_to_create_topic_prereq();
		
					break;
				case 8: //delete elective topic prereq
					$this->Electives_Model->DeleteElectiveTopicPrereqs($this->input->post('elective_topic_prereq_id'));
					
					$this->show_form_to_create_topic_prereq();
		
					break;
			}
		
		}


		private function list_prospectus_to_create_elective() {
				
			$data['title'] = "Assign Elective Course";
			$data['prospectus'] = $this->Prospectus_Model->ListProspectus($this->session->userdata('college_id'));
			$data['terms_sy'] = $this->AcademicYears_Model->getCurrentAcademicTerm();
		
			$this->content_lib->enqueue_body_content('dean/header_title',$data);
			$this->content_lib->enqueue_body_content('dean/list_prospectus_to_create_elective',$data);
			$this->content_lib->content();
		
		}
				
		private function list_electives_to_assign_topic() {
				
			$prospectus = $this->Prospectus_Model->getProspectus($this->session->userdata('prospectus_id'));
			$data['title'] = $prospectus->description." - ".$prospectus->effective_year;
			$data['terms_sy'] = $this->AcademicYears_Model->getCurrentAcademicTerm();

			$data['electives'] = $this->Prospectus_Model->ListElectives($this->session->userdata('prospectus_id'));
		
			$this->content_lib->enqueue_body_content('dean/header_title',$data);
			$this->content_lib->enqueue_body_content('dean/list_electives_to_assign_topic',$data);
			$this->content_lib->content();
		
		}

		private function show_form_to_create_elective_topic() {
				
			//$this->load->model('hnumis/Electives_Model');

			$data['prospectus'] = $this->Prospectus_Model->getProspectus($this->session->userdata('prospectus_id'));
			$data['courses']    = $this->Prospectus_Model->getProspectusCourse($this->session->userdata('prospectus_courses_id'));
			
			$data['college_courses'] = $this->Courses_Model->ListAllCourses();			
			//$data['college_courses'] = $this->Courses_Model->ListCourses($this->session->userdata('college_id'),'A');
			$data['topics']          = $this->Electives_Model->ListElectiveTopic($this->session->userdata('prospectus_courses_id'));

						
			$this->content_lib->enqueue_body_content('dean/form_to_create_elective_topic',$data);
			$this->content_lib->content();
		
		}

		private function show_form_to_create_topic_prereq() {
				
			$data['course'] = $this->Prospectus_Model->getProspectusCourse($this->session->userdata('prospectus_courses_id'));
			$data['elective_topic'] = $this->Electives_Model->getTopicInfo($this->session->userdata('elective_courses_id'));
			
			$data['prospectus_courses'] = $this->Prospectus_Model->ListCoursesToBeTaken($this->session->userdata('prospectus_id'));
			$data['topic_prereqs'] = $this->Electives_Model->ListTopicPrereqs($this->session->userdata('elective_courses_id'));

						
			$this->content_lib->enqueue_body_content('dean/form_to_create_elective_topic_prereq',$data);
			$this->content_lib->content();
		
		}

		private function show_form_to_create_parallel() {
				
			$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
			$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
			$data['courses']        = $this->Courses_Model->ListAllCourses();
			
			$this->session->set_userdata('data_offerings', $data['offerings']);
						
			$this->content_lib->enqueue_body_content('dean/form_to_create_parallel_course',$data);
			$this->content_lib->content();
		
		}

		private function show_form_to_edit_class_size($enrollee, $max_stud) {
					
			$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
			$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
			$data['enrollee']       = $enrollee;
			$data['max_stud']       = $max_stud;
			
						
			$this->content_lib->enqueue_body_content('dean/form_to_edit_class_size',$data); 
			$this->content_lib->content();
		}

		
	private function show_form_to_edit_additional_charge($status) {
					
			$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
			$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
			$data['status']       = $status;
			
			$this->content_lib->enqueue_body_content('dean/form_to_edit_additional_charge',$data); 
			$this->content_lib->content();
		}

		
		
	public function courses() {
				
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'list_courses');
				
			switch ($action) {
				case 'list_courses':
					$this->list_courses();
					break;
					
				case 'add_course':
					$data['course_code'] = $this->input->post('course_code');
					$data['group_id'] = $this->input->post('group_id');
					$data['type_id'] = $this->input->post('type_id');
					$data['college_id'] = $this->session->userdata('college_id');
					$data['descriptive_title'] = $this->input->post('descriptive_title');
					$data['credit_units'] = $this->input->post('credit_units');
					$data['paying_units'] = $this->input->post('paying_units');
					$data['service_course'] = $this->input->post('service_course');

					//print_r($data); die();
					if($this->Courses_Model->AddNewCourse($data)) {
						$this->content_lib->set_message("New Course Added! <strong>".$data['course_code']."</strong>", 'alert-success');
					}
					else{
						$errorMsg = $this->db->_error_message();
						if(strpos($errorMsg,"Duplicate entry")!==FALSE){
							$errorMsg = "Duplicate entry for course code - ".$data['course_code'];
						} else {
							$errorMsg = "Error adding course!";
						}
						$this->content_lib->set_message($errorMsg, 'alert-error');
					}
					
					$this->list_courses();
						
					break;
					
				/* moved to dean/create course
				
				
				case 2: //form to create new course
					$data['types'] = $this->Courses_Model->ListCourseTypes();
					//$data['groups'] = $this->Courses_Model->ListCourseGroups();
				
					$this->content_lib->enqueue_body_content('dean/create_course_form',$data);
					$this->content_lib->content();
					break;
				*/

				/* moved to dean/create_course by aljun & kevin
				 * 
				case 3: //save new course
				  
					$data['course_code'] = $this->input->post('course_code');
					$data['group_id'] = $this->input->post('group_id');
					$data['type_id'] = $this->input->post('type_id');
					$data['college_id'] = $this->session->userdata('college_id');
					$data['descriptive_title'] = $this->input->post('descriptive_title');
					$data['credit_units'] = $this->input->post('credit_units');
					$data['paying_units'] = $this->input->post('paying_units');
									
					
					if($id = $this->Courses_Model->AddNewCourse($data)){
						$this->content_lib->set_message("New Course Added! <strong>".$data['course_code']."</strong>", 'alert-success');
						//$this->content_lib->enqueue_body_content('dean/list_courses'); //replaced with next line
						$this->list_courses();
						//$this->content_lib->content();	
					}
					else{
						$this->content_lib->set_message("Error Adding Course!", 'alert-error');
						$this->content_lib->content();
					}
					break;
				 */
					
			/*case 4: //form to update a course
	
					//print_r($this->input->post()); die();
					$course_id = $this->input->post('course_id');
					$this->session->set_userdata('course_id',$course_id);
					
			
					$data['course'] = $this->Courses_Model->getCourseDetails($course_id);
					
					//print_r($data['course']);die();
					$data['type'] = $this->Courses_Model->getCourseType($course_id);
					$data['group'] = $this->Courses_Model->getCourseGroup($course_id);
					$data['types'] = $this->Courses_Model->ListCourseTypes();
					$data['groups'] = $this->Courses_Model->ListCourseGroups();
					$data['course_id'] = $course_id;
					
					$this->load->library('form_lib');
					$this->content_lib->enqueue_body_content('dean/form_to_edit_course',$data);
					$this->content_lib->content();
						
					break;
			*/		
			case 'update_course': // update a course
					
					$data['courses_id']   	   = $this->input->post('courses_id');
					$data['type_id']      	   = $this->input->post('type_id');
					$data['descriptive_title'] = $this->input->post('descriptive_title');
					$data['credit_units'] 	   = $this->input->post('credit_units');
					$data['paying_units'] 	   = $this->input->post('paying_units');
					$data['service_course']	   = $this->input->post('service_course');
					$data['empno'] 			   = $this->session->userdata('empno');
					
					if ($this->Courses_Model->updateCourse($data)) {
						$this->content_lib->set_message("Course successfully updated!", 'alert-success');
					} else {
						$this->content_lib->set_message("ERROR Course NOt updated!", 'alert-error');
					}

					$this->list_courses();
					//$this->content_lib->content();
			 		break;
						
			default:
					//$this->session->unset_userdata('academic_terms_id');
		
					//$this->list_terms_courses();
					$this->list_courses();
					break;
			}
			
		}
		
	public function create_course(){
			$this->load->library('form_validation');
			
			if ($this->form_validation->run('create_course') == TRUE)
			{
				$data['course_code'] = $this->input->post('course_code');
				$data['group_id'] = $this->input->post('group_id');
				$data['type_id'] = $this->input->post('type_id');
				$data['college_id'] = $this->session->userdata('college_id');
				$data['descriptive_title'] = $this->input->post('descriptive_title');
				$data['credit_units'] = $this->input->post('credit_units');
				$data['paying_units'] = $this->input->post('paying_units');
				
				
				if($id = $this->Courses_Model->AddNewCourse($data)){
					$this->content_lib->set_message("New Course Added! <strong>".$data['course_code']."</strong>", 'alert-success');
				}
				else{
					$this->content_lib->set_message("Error Adding Course!", 'alert-error');
				}
			}
			
			$data['types'] = $this->Courses_Model->ListCourseTypes();
			$data['groups'] = $this->Courses_Model->ListCourseGroups();
			
			$this->content_lib->enqueue_body_content('dean/create_course_form',$data);
			$this->content_lib->content();
		}

				
		private function list_courses(){
			
			$this->load->library('table_lib');
			
			$data['title'] = "Courses";
			$college_id = $this->session->userdata('college_id');
			$data['courses'] = $this->Courses_Model->ListCollegeCourses($college_id);
			$data['terms_sy'] = $this->AcademicYears_Model->getCurrentAcademicTerm();

			$data['types'] = $this->Courses_Model->ListCourseTypes();
			$data['groups'] = $this->Courses_Model->ListCourseGroups();
			//print_r($data['courses']); die();						
			//$this->content_lib->enqueue_body_content('dean/header_title',$data);
			$this->content_lib->enqueue_body_content('dean/list_allcourses',$data);
			$this->content_lib->enqueue_footer_script('data_tables');
			$this->content_lib->content();
		}
		
	  public function enrollment_summary() {
	
			$this->load->model('hnumis/Reports_Model');
		
			$this->Reports_Model->enrollment_summary();

		}
		
		public function exception_report() {
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('academic_terms_model');
			$this->load->model('hnumis/faculty_model');
			$this->load->model('hnumis/courses_model');
			$this->load->model('hnumis/enrollments_model');

			
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1: //display a list of academic terms
					$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
					$data['college_id'] = $this->session->userdata('college_id');
					
					$this->content_lib->enqueue_body_content('dean/search_faculty',$data);
					$this->content_lib->content();
					break;
				case 2:
					$data['academic_term'] = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms'));
					$data['faculty'] = $this->faculty_model->get_faculty($this->input->post('faculty'));

					$this->content_lib->enqueue_body_content('dean/exception_report',$data);
					$this->content_lib->content();
					break;
			}
		}
		
		//Added: May 6, 2013 by Amie
		public function masterlist() {	
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('academic_terms_model');
			$this->load->model('hnumis/enrollments_model');
			$this->load->model('hnumis/programs_model');
			
				
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				
				case 1: //display a list of academic terms
					$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
					$data['program'] = $this->programs_model->ListAcaProgramsbyCollege($this->session->userdata('college_id'));
					
					$this->content_lib->enqueue_body_content('dean/search_program',$data);
					$this->content_lib->content();
					break;
				case 2:
					$data['term'] = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms'));
					$data['program'] = $this->programs_model->getProgram($this->input->post('program'));
					$data['level'] = $this->input->post('yearlevel');
					$data['list'] = $this->enrollments_model->masterlist($data['term']->id, $data['program']->id, $data['level']);
					
					$this->content_lib->enqueue_body_content('dean/masterlist',$data);
					$this->content_lib->content();
					break;
				case 4: //prints the masterlist to pdf file
					$program = $this->programs_model->getProgram($this->input->post('program'));
					$level = $this->input->post('level');
					$term = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms'));
					$list = $this->enrollments_model->masterlist($term->id, $program->id, $level);
					
					$this->generate_masterlist($term, $program, $level);
					
					break;
			}
		}	
	
	//Added: May 8, 2013 by Amie
	//generates the pdf file for the masterlist of students
	function generate_masterlist($term, $program, $level) {
		
		$this->load->library('hnumis_pdf');
		$list = $this->enrollments_model->masterlist($term->id, $program->id, $level);
				
		$this->hnumis_pdf->AliasNbPages();
		$this->hnumis_pdf->set_HeaderTitle('MASTER LIST OF STUDENTS '.$program->description." - ".$level." ".$term->term." ".$term->sy);
		
		$this->hnumis_pdf->AddPage('P','letter');
		$this->hnumis_pdf->SetDisplayMode('fullpage');
		$this->hnumis_pdf->SetDrawColor(165,165,165);
		

		$w=array(10,20,110,10);
			
		$this->hnumis_pdf->SetFont('times','B',16);
		$this->hnumis_pdf->Ln();
						
		$this->hnumis_pdf->SetFont('times','B',9);
		$this->hnumis_pdf->SetFillColor(37,106,2);
		$this->hnumis_pdf->SetTextColor(253,253,253);
		$header=array('No.','ID No.','NAME','Gender');

			for($i=0;$i<count($header);$i++)
				$this->hnumis_pdf->Cell($w[$i],8,$header[$i],1,0,'C',true);
			$this->hnumis_pdf->Ln();

			$this->hnumis_pdf->SetTextColor(0,0,0);
			$this->hnumis_pdf->SetFont('times','',10);
			
			
			if ($list) {
				$num=0;
				$male=0;
				$female=0;
				foreach($list AS $l) {
					$num++;
					$this->hnumis_pdf->Cell($w[0],8,$num,1,0,'C');
					$this->hnumis_pdf->Cell($w[1],8,$l->idno,1,0,'C');
					$this->hnumis_pdf->Cell($w[2],8,$l->name,1,0,'L');
					$this->hnumis_pdf->Cell($w[3],8,$l->gender,1,0,'C');				
					if ($l->gender == 'M')
						$male++;
					else $female++;
					$this->hnumis_pdf->Ln();
				}
				
				$this->hnumis_pdf->set_HeaderTitle('Number of Male Students:',' : ',$male);	
				$this->hnumis_pdf->Ln();
				$this->hnumis_pdf->set_HeaderTitle('Number of Female Students:',' : ',$female);	
				$this->hnumis_pdf->Ln();
			}
			
			$this->hnumis_pdf->Output();	
	}
	
	//Added: May 8, 2013 by Amie
	//view dean's list students
	function deans_list() {
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/enrollments_model');
		$this->load->model('hnumis/programs_model');
		$this->load->model('hnumis/prospectus_model');

		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
		switch ($step) {
			case 1: //display a list of academic terms
				$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
				$data['program'] = $this->programs_model->ListAcaProgramsbyCollege($this->session->userdata('college_id'));
				
				$this->content_lib->enqueue_body_content('dean/search_program',$data);
				$this->content_lib->content();
				break;
			case 2:
				$term = $this->input->post('academic_terms');
				$program = $this->input->post('program');
				$year = $this->input->post('yearlevel');
				$students = $this->enrollments_model->masterlist($term, $program, $year);
	
				if ($students) {
					$candidates = array();
					$with_failure = FALSE;
					$max_credit = $this->prospectus_model->get_max_credit_units($program, $year, $term);
					
									
					foreach ($students as $s) {
						$grades = $this->enrollments_model->student_grades_given_academic_term($s->idno, $term);
												
						if ($grades) {
							foreach ($grades as $g) {
								if (($g->finals_grade > 2.5) || (!is_numeric($g->finals_grade))) {
									$with_failure = TRUE;
									break;
								}
							}
						
							if (!$with_failure) {
								$totalunits = 0;
								$sum = 0;
								$cwts = 0;
								foreach ($grades as $g) {
									if ((substr($g->course_code,0,4) != 'CWTS') && (substr($g->course_code,0,4) != 'ROTC') && (substr($g->course_code,0,2) != 'MS') && (substr($g->course_code,0,4) != 'NSTP')) {
										$sum += ($g->finals_grade * $g->credit_units);
										$totalunits += $g->credit_units;
									} //else $cwts = $g->credit_units;
								}
								 
								//if (($totalunits) >= $max_credit->max_credit_units) {
									$average = $sum/$totalunits;
									if ($average <= 1.6000) {
										$candidates[] = array('idno'=>$s->idno, 'name'=>$s->name, 'average'=>$average);
									}
								//}
							}
						}
						$with_failure = FALSE;
					}
					
					if ($candidates) {
						$data['candidates'] = $candidates;
						$data['term'] = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms'));
						$data['program'] = $this->programs_model->getProgram($this->input->post('program'));
						$data['level'] = $this->input->post('yearlevel');
										
						$this->content_lib->enqueue_body_content('dean/deans_list',$data);
						$this->content_lib->content();
					} else {
						$this->content_lib->set_message("No Dean's List Students", "alert-error");
						$this->content_lib->content();
					}
				}
				break;
		}
	}
	
	function check_if_no_failures($idno, $term) {
	
	}
	
	function grade_submission() {
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/faculty_model');
		$this->load->model('hnumis/courses_model');
		$this->load->model('hnumis/offerings_model');
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
		switch ($step) {
			case 1: //display a list of academic terms
				$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
							
				$this->content_lib->enqueue_body_content('dean/list_academic_terms',$data);
				$this->content_lib->content();
				break;
			case 2:
				$data['academic_term'] = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms_id'));
				$data['faculty'] = $this->faculty_model->list_faculty_by_college_term($this->input->post('academic_terms_id'),$this->session->userdata('college_id'));
				
				$this->content_lib->enqueue_body_content('dean/grade_submission_report',$data);
				$this->content_lib->content();
				break;
		}
	}


	
	function all_offerings() {
		//Note: This method is also used by Audit, Accountant, Evaluator and Posting (by Tatskie Aug 11, 2014)
		$this->load->model('academic_terms_model');
		$acad_terms = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
		$colleges = $this->College_Model->ListColleges();
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
	
		switch ($step) {
			case 1:
				$current_term = $this->academic_terms_model->getCurrentAcademicTerm();
					
				$offerings = $this->Offerings_Model->ListOfferings($current_term->id,$this->userinfo['college_id']);
	
				$data = array(
						"selected_term"=>$current_term->id,
						"terms"=>$acad_terms,
						"selected_college"=>$this->userinfo['college_id'],
						"colleges"=>$colleges,
						"offerings"=>$offerings,
				);
	
				break;
	
			case 2:
				
				if ($this->input->post('colleges_id') != 0) {
					$offerings = $this->Offerings_Model->ListOfferings($this->input->post('academic_terms_id'),$this->input->post('colleges_id'));
				} else {
					$offerings = $this->Offerings_Model->ListOfferings($this->input->post('academic_terms_id'));
				}
	
				$data = array(
						"selected_term"=>$this->input->post('academic_terms_id'),
						"terms"=>$acad_terms,
						"selected_college"=>$this->input->post('colleges_id'),
						"colleges"=>$colleges,
						"offerings"=>$offerings,
				);
									
				break;
			/*case 3:
	
				$acad_terms = $this->academic_terms_model->ListAcademicTerms($this->input->post('academic_year_id'));
				$term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
					
				$offerings = $this->Offerings_Model->ListOfferings($this->input->post('academic_terms_id'));
	
				$data = array(
						"school_years"=>$school_years,
						"terms"=>$acad_terms,
						"selected_year"=>$this->input->post('academic_year_id'),
						"selected_term"=>$this->input->post('academic_terms_id'),
						"offerings"=>$offerings,
						"term"=>$term,
						"academic_terms_id"=>$acad_terms[0]->id,
				);
	
				break;
			*/		
		}
	
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->content_lib->enqueue_footer_script('data_tables_tools');		
		$this->content_lib->enqueue_body_content('dean/list_offerings',$data);
		$this->content_lib->content();
	}
	
	//Added:  7/24/2013
	function remarks(){
		switch ($action = $this->input->post('action')){
			case 'get_Remarks':
					
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					//$remarks = $this->input->post('new_remarks');
					$remarks_id = $this->input->post('remark_id');
	
					if($remarks_id){
						$remark = $this->Student_Model->getRemark($remarks_id);
							
					}
					//print_r($remark);
					//die();
					$remark_info['id'] = $remark->id;
					$remark_info['date'] = $remark->remark_date;
					$remark_info['remark'] = $remark->remark;
					$remark_info['view_right'] = $remark->view_rights;
					//print($remark_info['remark']);
					//die();
					if($remark){
						$this->content_lib->enqueue_body_content('dean/form_to_edit_remarks',$remark_info);
						$this->content_lib->content();
					}else{
						$this->content_lib->set_message("Error in creating remarks!", "alert-error");
					}
				}else {
					$this->content_lib->set_message("Error in submitting form", 'alert-error');
	
				}
					
				break;
					
					
		}
	
	}


	//ADDED: 5/31/14 genes
	function download_enrollment_summary_to_csv() {
		$this->load->model('hnumis/Reports_Model');
	
		$this->Reports_Model->download_enrollment_summary_to_csv();
	
	}
	
	/*
	* ADDED: 6/10/14
	* @author: genes
	* @description: displays students enrolled from enrollment summary modal
	*/
	function get_students() {
		$this->load->model('hnumis/Enrollments_Model');
		
		$data['students'] = $this->Enrollments_Model->masterlist($this->input->get('selected_term'), $this->input->get('prog_id'), $this->input->get('yr_level'), TRUE);
		
		$this->load->view('dean/list_of_students_from_modal', $data);
		
	}
	

	/*
	* @ADDED: 8/28/14
	* @author: genes
	* @description: displays students enrolled from dean offerings
	* @edited: include parallel same with student enroll
	*/
	function list_enrolled_students() {
		$this->load->model('hnumis/Student_Model');
		
		$data['students'] = $this->Student_Model->ListEnrollees_Including_Parallel($this->input->get('offer_id'),$this->input->get('parallel_no'));
		$this->load->view('dean/list_of_students_from_modal', $data);
	
	}
	
	function show_course_details() {
		$this->load->model('hnumis/Courses_Model');

		$data['course'] = $this->Courses_Model->getCourseDetails($this->input->get('courses_id'));
		$data['types'] = $this->Courses_Model->ListCourseTypes();
				
		$this->load->view('dean/form_to_edit_course', $data);
	}
	
	/*
	 * @ADDED: 11/19/14
	 * @author: genes
	 */
	function list_allocations() {
		$this->load->model('hnumis/Offerings_Model');

		$data['allocations'] = $this->Offerings_Model->ListAllocation($this->input->get('offer_id'));
		$data['block_names'] = $this->Offerings_Model->ListBlockOfferings($this->input->get('offer_id'));
		
		$this->load->view('dean/list_of_allocations_from_modal', $data);
	}
	
	/*
	 * @author: genes
	 * @date: 7/18/2014
	 */
	function enroll_by_program_course() {
		
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Offerings_Model');
		$this->load->model('hnumis/Enrollments_Model');
		
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'display_all');
		
		$acad_terms = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
		$enrollees = array();
		$num_courses = 0;
		
		switch ($action) {
			
			case 'display_all':
				$current_term = $this->academic_terms_model->getCurrentAcademicTerm();
				$offerings=$this->Offerings_Model->ListOfferings($current_term->id,$this->session->userdata('college_id'));
				
				//print_r($offerings); die();
				if ($offerings) {
					foreach ($offerings AS $offer) {
						if ($offer->status == 'ACTIVE') {
							$code = $offer->course_code.'-'.$offer->section_code;
							$enrollees[$code] = $this->Enrollments_Model->Count_Enrollees_by_program_course($offer->id);
							//$enrollees[$offer->employees_empno] = $offer->emp_name;
						}	
					}
				}
				//print_r($enrollees); die();
				
				if ($enrollees) {
					ksort($enrollees);
					$num_courses = count($enrollees);
				}
				
				$show_current_term = $current_term->term.' '.$current_term->sy;
				
				$this->CreateEnrollmentReportsforExcel($enrollees,$show_current_term);
				
				//print_r($enrollees);die();
				
				$data = array(
						"terms"=>$acad_terms,
						"selected_term"=>$current_term->id,
						"enrollees"=>$enrollees,
						"num_courses"=>$num_courses,
						"show_current_term" => $show_current_term,
				);
				
				break;
				
			case 'display_change_sy':
				$term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
				$offerings=$this->Offerings_Model->ListOfferings($this->input->post('academic_terms_id'),$this->session->userdata('college_id'));

				if ($offerings) {
					foreach ($offerings AS $offer) {
						if ($offer->status == 'ACTIVE') {
							$code = $offer->course_code.'-'.$offer->section_code;
							$enrollees[$code] = $this->Enrollments_Model->Count_Enrollees_by_program_course($offer->id);
						}
					}
				}
				
				if ($enrollees) {
					ksort($enrollees);
					$num_courses = count($enrollees);
				}	

				$show_current_term = $term->term.' '.$term->sy;
				
				$this->CreateEnrollmentReportsforExcel($enrollees, $show_current_term);
				
				$data = array(
						"terms"=>$acad_terms,
						"selected_term"=>$this->input->post('academic_terms_id'),
						"enrollees"=>$enrollees,
						"num_courses"=>$num_courses,
						"show_current_term" => $show_current_term,
				);
				
				break;
		}
		
		$this->content_lib->enqueue_body_content('dean/enrollment_by_program_course',$data);
		$this->content_lib->content();
		
	}
	
	
	/*
	 * @author: genes
	 * @date: 7/19/2014
	 * $description: for excel reports of enrollment by program-course
	 * 
	 */
	private function CreateEnrollmentReportsforExcel($enrollees, $term) {
		$this->load->library('PHPExcel');
		
		$filename = "downloads/Enrollment by Program and Course ".$term.".xls";
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setTitle("Enrollment by Program and Course: ".$term);
		
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', "Enrollment by Program and Course: ".$term);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
		->setSize(14);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4.5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5.5);
		
		$num=3;
		
		if ($enrollees) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, 'Total No. of Courses Offered: '.count($enrollees));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$num)->getFont()->setBold(true)->setSize(12);
				
			$cnt=1;
			$num++;
			foreach($enrollees AS $k=>$v) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, $cnt.'. '.$k);
				$cnt++;
				$num++;
				if ($v) {
					foreach($v AS $prog) {
						$objPHPExcel->getActiveSheet()->SetCellValue('B'.$num, $prog->abbreviation);
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, $prog->stud);
						$num++;
					}
				}
			}
		}
		
		$objWriter->save($filename,__FILE__);
		return;
		
	}
	
	
	function view_prospectus() {
		$this->load->library('prospectus_lib');
	
		$this->prospectus_lib->view_prospectus($this->session->userdata('college_id'));
	}

	
	function candidates() {
		$this->load->library('graduates_lib');
	
		$this->graduates_lib->view_candidates($this->session->userdata('college_id'));
	}

	
	function course_schedules() {
		$this->load->library('dean_lib');
	
		$this->dean_lib->offer_course_schedules();
	}
	
		// Copied from controllers\developer
		// Copied from controllers\developer
		function nstp_list() {
			$this->load->library('nstp_lib');
			$this->nstp_lib->index($term=469);
		}

		function nstp_list2() {
			$this->load->library('nstp_lib');
			$this->nstp_lib->index(470);
		}

	
}	

?>
