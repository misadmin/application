	<div class="control-group formSep">
		<label class="control-label">Level</label>
		<div class="controls">
		   <select id="select_level" name="levels" >
				<option value="">--Select Level--</option>
				<?php 
					foreach($levels as $level) { 
				?>
					<option value="<?php print($level->id); ?>" >
								<?php print($level->level); ?></option>
				<?php 
					}
				?>
			</select>
		</div>
	</div>
	
	<div class="my_level" style="display:none;">	 
		<div class="control-group formSep ">
			<label class="control-label">Year Level</label>
		    <div class="controls">
				<div class="preschool" style="display:none;">	
					<select name="pre_yr_level">
						<option value="1" checked>Preschool 1</option>
					</select>
				</div>
		    	<div class="kinder" style="display:none;">	
					<select name="kinder_yr_level">
						<option value="1" checked>Kindergarten 1</option>
					</select>
		    	</div>
				<div class="gs" style="display:none;">
					<select name="gs_yr_level">
						<option value="1" checked>Grade 1</option>
						<option value="2">Grade 2</option>
						<option value="3">Grade 3</option>
						<option value="4">Grade 4</option>
						<option value="5">Grade 5</option>
						<option value="6">Grade 6</option>
					</select>
				</div>
				<div class="hd" style="display:none;">
					<select name="jh_yr_level">
						<option value="7">Grade 7</option>
						<option value="8">Grade 8</option>
						<option value="9">Grade 9</option>
						<option value="10">Grade 10</option>
					</select>
				</div>
				<div class="senior" style="display:none;">
					<select name="sh_yr_level">
						<option value="11">Grade 11</option>
						<option value="12">Grade 12</option>
					</select>
				</div>
	    	</div>
		</div>   
	 
		<div class="control-group">
			<div class="controls">
				<button class="btn btn-success" type="submit"><?php print($button); ?></button>
			</div>
		</div>	
	</div>

	
 <script>
	 $(document).ready(function (e) {
    	$('#select_level').change(function () {
        	if ($(this).val() == '') {
            	$('.my_level').hide();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.senior').hide();
			} 

        	if ($(this).val() == '11') {
            	$('.my_level').show();
        		$('.preschool').show();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.senior').hide();
			} 

    		if ($(this).val() == '1') {
            	$('.my_level').show();
    			$('.preschool').hide();
    			$('.kinder').show();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.senior').hide();
			} 

        	if ($(this).val() == '3') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').show();
            	$('.hd').hide();
            	$('.senior').hide();
			} 

        	if ($(this).val() == '4') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').show();
            	$('.senior').hide();
			} 

        	if ($(this).val() == '12') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.senior').show();
			} 
			
    	});
	});
 </script>	