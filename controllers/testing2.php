<?php

class Testing2 extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$menu = array(
				'role'=>'testing2',
				'name'=>'Foo Bar',
				'menu'=>array(
						'Student'=>'student',
						'Faculty'=>'faculty',
						'Pull Down'=>array('Here'=>'here'),
						'Testing'=>array('Table Lib'=>'test_table_lib',
										 'Form Lib'=>'test_form_lib',
										 'Metrobank Form'=>'metrobank_form',
										 
										)
						)
		);
		$this->content_lib->set_navbar_content('', $menu);
	}
	public function index(){
		$this->load->library('print_lib');
		//$this->print_lib->test();
		$this->content_lib->content();
	}
	
	public function phpinfo(){
		echo phpinfo();
	}
	
	public function add_payers(){
		
		$this->load->library('form_lib');
		$this->content_lib->enqueue_body_content('accounts/add_payers_form','');
		$this->content_lib->content();
	}
	
	public function ar_balance_college(){
		$this->load->library('table_lib');
		$this->load->model('accountant/accountant_model');
		// 6 college, 7 graduate school
		$data = $this->accountant_model->ar_balance_college();
		
		//echo '<pre>';
		//print_r($data);
		//die();
		$this->content_lib->enqueue_header_script('date_picker');
		$this->content_lib->enqueue_body_content('accountant/ar_running_balance', array('data'=>$data));
		$this->content_lib->content();
	}
	
	public function ar_balance_basic(){
		$this->load->library('table_lib');
		$this->load->model('accountant/accountant_model');
		
		$start_date = $this->input->post('start_date') ? $this->input->post('start_date') : date('Y-m-01');
		$end_date = $this->input->post('end_date') ? $this->input->post('end_date') :date('Y-m-d');
		
		// 2 = Kinder,3 = Grade School ,4 = High School Day,5 = Night
		$data = $this->accountant_model->ar_balance_basic_ed($start_date,$end_date);
		//$data = array();
		//echo '<pre>';
		//print_r($data);
		//die();

		$this->content_lib->enqueue_header_style('date_picker');
		$this->content_lib->enqueue_footer_script('date_picker');
		$this->content_lib->enqueue_footer_script('validate');
		$this->content_lib->enqueue_body_content('accountant/ar_running_balance', array('data'=>$data,'start_date'=>$start_date,'end_date'=>$end_date));
		$this->content_lib->content();
	}
	
	public function test_table_lib (){
		$this->load->library('table_lib');
		$this->load->model('hnumis/enrollments_model');
		
		$students = $this->enrollments_model->class_record("23373");
	 
     //Putting your attributes to an array
     $attributes = array('class' => 'table table-condensed',);
     $this->table_lib->set_attributes($attributes);
     $this->table_lib->insert_head_row (array(
     							array(
     								'id'=>'idno',
     								'value'=>'ID Number',
     								),
     							array(
     								'id'=>'lname',
     								'value'=>'Name',
     								),
     							));
     $this->table_lib->insert_data($students);
     $this->content_lib->enqueue_body_content('', $this->table_lib->content(TRUE));
     $this->content_lib->content();
	}
	
	public function test_form_lib(){
		$this->load->library('form_lib');
		
		$this->form_lib->set_attributes(array('method'=>'post','action'=>'#','class'=>'form-horizontal'));
		$this->form_lib->enqueue_hidden_input('language','english'); //,array('style'=>array('visibility:hidden','display:none'))
		$this->form_lib->enqueue_hidden_input('id','06128492');
		//$this->form_lib->enqueue_text_input(array('name'=>'idno','label'=>'ID Number','rules'=>'required','placeholder'=>'ID Number'));
		//$this->form_lib->enqueue_text_input(array('name'=>'idno','label'=>'ID Number','rules'=>array('required:true','minlength:6')));
		//$this->form_lib->set_rule('idno','required','true','Please Enter a Valid ID Number');
		//$this->form_lib->enqueue_text_input('fname','First Name', 'First Name','',FALSE);
		//$this->form_lib->enqueue_text_input('lname','Last Name', 'Last Name','',FALSE);
		$this->form_lib->enqueue_text_input('username','Username', 'Username','',TRUE);
		$this->form_lib->enqueue_text_input('email','Email', 'Email Address','',TRUE);
		
		$this->form_lib->enqueue_password_input(array('name'=>'password','label'=>'Password','rules'=>array('required','minlength:3'),'placeholder'=>'******'));
		$this->form_lib->enqueue_password_input('passwordconfirm','Confirm Password','******','',FALSE,'equalTo:password');
		//                                     ($data,            $label='',     $placeholder='', $value='', $required=FALSE, $prepend='', $append='', $rules=''){
		
		//$this->form_lib->enqueue_textarea(array('name'=>'address','label'=>'Address','placeholder'=>'Address','prepend'=>'<i class="icon-home"></i>','class'=>'input-xlarge'));
		
		$this->form_lib->set_submit_button('Add Student');
		$this->form_lib->set_reset_button('Reset','btn btn-link');
		
		//$name, $label='', $placeholder='', $value='', $required='FALSE', $prepend='', $append='', $attributes=array()
		//$this->content_lib->enqueue_body_content('', '<div class="container" style="min-height:400px"><div class="row-fluid"><div class="span12">'.$this->form_lib->content()).'</div></div></div>';
		$this->content_lib->enqueue_body_content('','<div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Register</a></li>
    <li><a href="#tab2" data-toggle="tab">Login</a></li>
    <li><a href="#tab3" data-toggle="tab">Add Girlfriend</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab1"><div class="span6">');
		$this->content_lib->enqueue_body_content('', $this->form_lib->content().'</div>');
		$this->form_lib->set_id('form2');
		$this->form_lib->set_rule('fname','required');
		$this->form_lib->set_rule('lname','required');
		$this->form_lib->set_rule('email','required');
		$this->form_lib->set_submit_button('Add Record','btn btn-primary');
		$this->form_lib->set_reset_button('Reset');
		$this->content_lib->enqueue_body_content('', '<div class="span6">'.$this->form_lib->content().'</div>');
		$this->content_lib->enqueue_body_content('', '</div>
    <div class="tab-pane" id="tab2">');
		$this->form_lib->clear_form();
		$this->form_lib->set_id('login');
		$this->form_lib->enqueue_text_input('id','Username');
		$this->form_lib->enqueue_text_input('password','Password');
		$this->form_lib->set_rule('id','required');
		$this->form_lib->set_rule('password','required');
      	$this->form_lib->enqueue_free_line('<div class="controls">Register <a href="#">Here</a></div>');
		$this->form_lib->set_submit_button('Login','btn btn-primary');
		$this->content_lib->enqueue_body_content('', '<div class="span12">'.$this->form_lib->content().'</div>');
		
      	$this->content_lib->enqueue_body_content('', '</div><div class="tab-pane" id="tab3">');
      	$this->form_lib->clear_form();
		$this->form_lib->set_id('girlfriend');
      	$this->form_lib->enqueue_text_input(array('name'=>'id','label'=>'ID Number you wish to be ur GF','class'=>'input-small'));
      	$this->form_lib->set_rule('id','required');
      	$this->form_lib->enqueue_textarea(array('name'=>'comments','label'=>'Your Comments', 'rules'=>array('required','minlength:3'),'class'=>'input-xlarge'));
      	//$this->form_lib->enqueue_radio_button(array('name'=>'status','options'=>array('single'=>'Single','in_relationship'=>'In a Relationship')));
      	$this->form_lib->enqueue_radio_button(array('name'=>'contract','options'=>array('1m'=>'1 Month','6m'=>'6 Months','1y'=>'1 Year','2y'=>'2 Years'),'label'=>'Contract'));
      	$this->form_lib->enqueue_select_input('currentstatus',array('single'=>'Single','married'=>'Married','complicated'=>'It\'s Complicated'),'Current Status');
      	$this->form_lib->enqueue_checkbox('serious','agree','I accept the license agreement','',TRUE);
      	$this->form_lib->set_submit_button('Add Girlfriend','btn btn-warning');
		$this->form_lib->set_reset_button('I change my mind');
		$this->form_lib->enqueue_script("alert('Hello World');");
		$this->form_lib->enqueue_script(array("$('#{form_id}-submit').bind('click', function() { alert('submitting form') } );",
			                                  "$('#{form_id}-reset').bind('click', function() { alert('form cleared') } );"
			                                 ));
		$this->content_lib->enqueue_body_content('', '<div class="span12">'.$this->form_lib->content().'</div>');
   		$this->content_lib->enqueue_body_content('', '</div>
  </div>
</div>');
		$this->content_lib->content();
	}
	
	public function pad_test(){
		echo str_pad('The quick brown fox', 22, '*', STR_PAD_BOTH);
	}
	
	public function test_form_lib2(){
		$this->load->library('form_lib');
		
		$password_input = array('name' => 'password',
				'label' => 'Password',
				'rules' => array('required','minlength:3'),
				'placeholder' => '******'
		);
		$this->form_lib->set_attributes(array('method'=>'post','action'=>'','class'=>'form-horizontal'));
		$this->form_lib->add_control_class('formSep');
		$this->form_lib->enqueue_text_input(array('name'=>'username', 'label'=>'Username', 'rules'=>array('required'), 'class'=>'input-xlarge search-query'));
		$this->form_lib->enqueue_password_input($password_input);
		$this->form_lib->set_submit_button('Submit');
		$this->form_lib->set_reset_button('Reset');
		$this->content_lib->enqueue_body_content('', $this->form_lib->content());
		
		$this->content_lib->content();
	}
	
	public function test_student_history(){
		$this->load->model('hnumis/student_model');
		
		print_r ($this->student_model->assess_student_year_level_by_id('6129700'));
	}
	
	public function metrobank_form(){
		$this->content_lib->enqueue_body_content('cashier/metrobank_deposit_slip','');
		$this->content_lib->content();
	}
	
	public function testme(){		
		$this->load->model('academic_terms_model');
		$mydata = $this->academic_terms_model->getCurrentAcademicTermID();
		$this->output->set_output($mydata->current_academic_term_id);		
	}
	
	public function testme2(){
		$this->load->model('student_common_model');
		if ($idno = $this->student_common_model->get_idno('padora','darlyn','yecyec')){
			echo $idno;
			return;
	}else {
			echo "no idno";
			die();
		}
	}
	
	
	public function test_insert(){
		$code='';
		$name='yxxx xxx xxx8';
		$this->load->model('teller/teller_model');
		$data['code']=$code;
		$data['name']=$name;
		if ( $this->teller_model->insert_bank($data) )
			echo "yahoo!";
		else
			echo "failed oi";
		die();
		
	}


  	function checkdate(){
  		$this->load->model('hnumis/AcademicYears_Model');
  		$this->load->model('hnumis/Student_Model');
  		$this->load->model('hnumis/Enrollments_Model');
  		
  		$current  = $this->AcademicYears_Model->getCurrentAcademicTerm(); 
  		if (!$current ){ 
  			echo "false current"; 
  			return;
  		}  		
/*  		$status   = $this->Student_Model->getEnrollStatus('6234004',$current->id);  
  		if (!$status){
  			echo "false status";
  			return;  				
  		}*/		
  		$schedule = $this->Enrollments_Model->getEnrollmentSchedule($current->id,4);  		
  		if (!$schedule){
  			echo "false schedule";
  			return;
  		}
  		
  		$start_date = strtotime($schedule->start_date);
  		$end_date = strtotime($schedule->end_date);
  		//$d1 = $schedule->dd; 
  		$end_date1 = mktime(23,59,59,$schedule->mm,$schedule->dd,$schedule->yy);

  		$current_date = strtotime(date('Y-m-d h:m:s'));
  		
  		
  		print_r($current);
  		
  		echo '<br>';
  		echo 'current: ' . date("M d, Y H:m:s",$current_date). " ".$current_date ; echo "<p>";
  		echo 'start: ' . date("M d, Y H:m:s",$start_date)." ".$start_date;echo "<p>";
  		echo 'end: ' . date("M d, Y H:i:s",$end_date1)." ".$end_date1;
  		die;
  		
  	}


}
