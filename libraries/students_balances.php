<?php 
class students_balances {
	protected $CI;
	public function __construct(){
		$this->CI =& get_instance();
	}
	
	public function index(){
		$this->CI->content_lib->enqueue_footer_script('data_tables');
		$this->CI->load->model('accountant/accountant_model');
		if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {
			$stud_start = $this->CI->input->post('stud_start') ? $this->CI->input->post('stud_start') :'a' ;
			$stud_end = $this->CI->input->post('stud_end') ? $this->CI->input->post('stud_end') : 'b';
				
			
			$payers_ids = $this->CI->accountant_model->get_payers_ids($stud_start,$stud_end);
		}
		$counter = 0;
		if ($payers_ids){
			//print_r($payers_ids);die();
			$balances = array();
			foreach($payers_ids as $payers_id){
				$ledger_entries = $this->CI->accountant_model->get_balance($payers_id->id);
				if ($ledger_entries){
					foreach ($ledger_entries as $ledger_entry){
						if ($ledger_entry['balance'] != 0){
							$balances[$payers_id->id]['idno'] = $payers_id->idno;
							$balances[$payers_id->id]['student'] = $payers_id->student;
							$balances[$payers_id->id]['abbreviation'] = $payers_id->abbreviation;
							$balances[$payers_id->id]['year_level'] = $payers_id->year_level;
							$balances[$payers_id->id]['balance'] = $ledger_entry['balance'];
						}
					}
				}
			}
			$data['balances']=$balances;
			$this->CI->content_lib->enqueue_body_content('accountant/students_balances',$data);
				
		}else{
			$this->CI->content_lib->set_message('<h3>No records found!</h3>','Error');
		}
		$this->CI->content_lib->content();
	}


	
	
	
}
?>