 <?php
class Printer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mass_printing_model', 'printer');
        //$this->common->need_auth();        $this->load->library('print_lib');

    }
    public function index()
    {
        //$this->load->model('')
        $this->load->library('print_lib');
        $this->content_lib->content();
    }
    public function class_list()
    {
    }
    public function masterlist($group_by)
    {
        $master_list = $this->printer->master_list(455);
        $content     = $this->load->view('mass_printing_templates/masterlist', array(
            'master_list' => $master_list
        ), TRUE);
        $this->load->view('mass_printing_templates/container_jzebra', array(
            'title' => 'Master List',
            'content' => $content
        ));
    }
    public function grades($academic_terms_id, $program_id, $year_level)
    {
        $students_grades = $this->printer->grades($academic_terms_id, $program_id, $year_level);
        $students_graduated_text = array();

        ////log_message("INFO", "STUDENTS_GRADES  == > ".print_r($students_grades,true)); // Toyet 8.6.2018
        $this->load->library('print_lib');
        $this->load->model('hnumis/student_model');
        $students = array();
        foreach ($students_grades as $students_grade) {
             if (isset($students[$students_grade->idno]))
                $students[$students_grade->idno][] = $students_grade;
            else
                $students[$students_grade->idno] = array(
                    $students_grade
                );
                $students_graduated_text[$students_grade->idno] = $this->student_model->List_Graduated_Program_ByTerm($students_grade->idno,FALSE,$academic_terms_id);

        } //$students_grades as $students_grade

        ////log_message("INFO", print_r($students,true)); // Toyet 8.6.2018
        ////log_message("INFO", print_r($students_graduated_text,true)); // Toyet 11.24.2018
        $content = $this->load->view('mass_printing_templates/grades', array(
            'students' => $students,
            'graduated_text' => $students_graduated_text,
        ), TRUE);
        ////log_message("INFO","OUTSIDE  =>  ".print_r($content,true)); // Toyet 8.6.2018
        $this->load->view('mass_printing_templates/container_jzebra', array(
            'title' => 'Grades Printing',
            'content' => $content
        ));
    }
    public function enrollment_list($academic_terms_id, $program_id, $year_level)
    {
        $students        = $this->printer->enrollment_list($academic_terms_id, $program_id, $year_level);
        $student_courses = array();
        foreach ($students as $key => $student) {
            if ($key == 0) {
                $sy               = $student->sy;
                $term             = $student->academic_term;
                $academic_program = $student->program;
                $year_level       = $student->year_level;
            } //$key == 0
            if (isset($student_courses[$student->idno])) {
                $student_courses[$student->idno]['courses'][] = (object) array(
                    'course' => $student->course_code,
                    'credit_units' => $student->credit_units,
                    'is_bracketed' => $student->is_bracketed
                );
            } //isset($student_courses[$student->idno])
            else {
                $student_courses[$student->idno]['courses'] = array(
                    (object) array(
                        'course' => $student->course_code,
                        'credit_units' => $student->credit_units,
                        'is_bracketed' => ($student->is_bracketed == 'Y' ? TRUE : FALSE)
                    )
                );
                $student_courses[$student->idno]['info']    = (object) array(
                    'fullname' => $student->fullname,
                    'idno' => $student->idno,
                    'gender' => $student->gender
                );
            }
        } //$students as $key => $student
        $this->load->library('print_lib');
        $content = $this->load->view('mass_printing_templates/enrollment_list', array(
            'student_courses' => $student_courses,
            'academic_term' => $term,
            'sy' => $sy,
            'academic_program' => $academic_program,
            'year_level' => $year_level
        ), TRUE);
        $this->load->view('mass_printing_templates/container_jzebra', array(
            'title' => 'Enrollment List Printing',
            'content' => $content
        ));
    }
    public function offerings()
    {
    }
    public function form9($students_idno, $date_graduated)
    {
        $this->load->model('dric/students_model');
        $this->load->library('print_lib.php');
        $this->session->set_userdata('last_form9_date', $date_graduated);
        $student_info = $this->students_model->student_form9_information($students_idno);
        $requirements = $this->students_model->student_form9_requirements($students_idno);
        $prelim_educ  = $this->students_model->student_form9_prelim_educ($students_idno);
        $meta         = json_decode($student_info->meta);
        $dcourses     = $this->students_model->student_form9_all_courses($students_idno);
        $lines        = array();
        if ($dcourses) {
            $term = '';
            foreach ($dcourses as $course) {
                if ($term != $course->academic_term . ", " . $course->school_year . " - " . $course->school) {
                    $term    = $course->academic_term . ", " . $course->school_year . " - " . $course->school;
                    $lines[] = (object) array(
                        'content' => '',
                        'rating' => '',
                        'credit' => '',
                        'underlined' => FALSE,
                        'from_hnu' => '',
                        'academic_term' => ''
                    );
                    $lines[] = (object) array(
                        'content' => $this->print_lib->bold() . str_pad(substr($term, 0, 60), 60, " ", STR_PAD_RIGHT) . $this->print_lib->regular(),
                        'rating' => '',
                        'credit' => '',
                        'underlined' => TRUE,
                        'from_hnu' => '',
                        'academic_term' => ''
                    );
                    if (strlen($term) > 60) {
                        $lines[] = (object) array(
                            'content' => $this->print_lib->bold() . str_pad(substr($term, 60, 60), 60, " ", STR_PAD_RIGHT) . $this->print_lib->regular(),
                            'rating' => '',
                            'credit' => '',
                            'underlined' => TRUE,
                            'from_hnu' => '',
                            'academic_term' => ''
                        );
                    } //strlen($term) > 60
                } //$term != $course->academic_term . ", " . $course->school_year . " - " . $course->school
                if (strlen($course->course_code) > 15 || strlen($course->descriptive_title) > 44) {
                    $lines[] = (object) array(
                        'content' => str_pad(substr($course->course_code, 0, 15), 15, ' ', STR_PAD_RIGHT) . " " . str_pad(substr(trim($course->descriptive_title), 0, 44), 44, ' ', STR_PAD_RIGHT),
                        'rating' => $course->finals_grade,
                        'credit' => $course->credit_units,
                        'underlined' => FALSE,
                        'from_hnu' => ($course->from_hnu == '1'),
                        'academic_term' => $course->academic_term_id
                    );
                    $lines[] = (object) array(
                        'content' => str_pad(substr(trim($course->course_code), 15, 15), 15, ' ', STR_PAD_RIGHT) . " " . str_pad(substr(trim($course->descriptive_title), 44, 44), 44, ' ', STR_PAD_RIGHT),
                        'rating' => '',
                        'credit' => '',
                        'underlined' => FALSE,
                        'from_hnu' => '',
                        'academic_term' => ''
                    );
                } //strlen($course->course_code) > 15 || strlen($course->descriptive_title) > 44
                else {
                    $lines[] = (object) array(
                        'content' => str_pad(substr($course->course_code, 0, 15), 15, ' ', STR_PAD_RIGHT) . " " . str_pad(trim($course->descriptive_title), 44, ' ', STR_PAD_RIGHT),
                        'rating' => $course->finals_grade,
                        'credit' => $course->credit_units,
                        'underlined' => FALSE,
                        'from_hnu' => ($course->from_hnu == '1'),
                        'academic_term' => $course->academic_term_id
                    );
                }
            } //$dcourses as $course
        } //$dcourses
        $guardian_parent = isset($meta->family_info->fathers_name) && !empty($meta->family_info->fathers_name) ? $meta->family_info->fathers_name : (isset($meta->family_info->mothers_name) && !empty($meta->family_info->mothers_name) ? $meta->family_info->mothers_name : (isset($meta->student_emergency_info->emergency_notify) ? $meta->student_emergency_info->emergency_notify : ''));
        $content         = $this->load->view('print_templates/form9', array(
            'student_info' => $student_info,
            'requirements' => $requirements->requirements,
            'elementary' => $prelim_educ['elementary'],
            'highschool' => $prelim_educ['highschool'],
            'guardian_parent' => $guardian_parent,
            'date_of_graduation' => $date_graduated,
            'courses' => $lines,
            'last_sem' => $course->academic_term_id
        ), TRUE);
        $this->load->view('mass_printing_templates/container_jzebra', array(
            'title' => 'Form IX',
            'content' => $content
        ));
    }
    public function form19($academic_terms_id = 0, $academic_program = 0, $year_level = 0)
    {
        $this->load->model('dric/enrollments_model');
        $student_data = $this->enrollments_model->students_courses_and_grades($academic_terms_id, $academic_program, $year_level);
        $content      = $this->load->view('mass_printing_templates/form19', array(
            'data' => $student_data
        ), TRUE);
        $this->load->view('mass_printing_templates/container_jzebra', array(
            'title' => 'Form 19 Printing',
            'content' => $content
        ));
    }


    public function basic_ed($what = '', $level = '', $year_level = '', $section = '')
    {

        if (!empty($section)) {

        $year_level_array = array(11,12);

        if(in_array($year_level,$year_level_array)) {

            $this->load->model('student_common_model');
            $this->load->model('teller/teller_model');
            $this->load->model('academic_terms_model');
            $this->load->model('accounts/accounts_model');
            $this->load->model('hnumis/enrollments_model');
            $this->load->library('shs/shs_student_lib');
            $this->load->model('hnumis/student_model');
            $this->load->model('hnumis/shs/shs_student_model');
            $this->load->model('hnumis/shs/shs_enrollments_model');
            $this->load->library('print_lib');
            $this->load->library('custom_loader');

            //$idnum = "06802151";
            $current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
            $academic_terms_id = $current_academic_terms_obj->id;
            ////log_message("INFO", print_r($current_academic_terms_obj,true));
            //die();
            log_message("INFO", "WHAT       => ".print_r($what,true));
            log_message("INFO", "LEVEL      => ".print_r($level,true));
            log_message("INFO", "YEAR LEVEL => ".print_r($year_level,true));
            log_message("INFO", "SECTION    => ".print_r($section,true));

            $idnos = $this->shs_enrollments_model->IDs_For_EnrolledSHS($academic_terms_id,$year_level,$level,$section);
            ////log_message("INFO", "SHS ID NOS  ==>> ".print_r($idnos,true)); 
            $clearance_content = "";

            $i = 1;
            $imax = count($idnos);
            foreach($idnos as $idno):

                $idnum = $idno->students_idno;

                        $result = $this->student_common_model->my_information($idnum);
                        $result2 = $this->teller_model->get_student($idnum);
                        ////log_message("INFO", print_r($result,true));
                        ////log_message("INFO", print_r($result2,true));

                        //assessment details
                        $acontent = array(
                            'idnumber'  =>$result->idno,
                            'familyname'=>$result->lname,
                            'firstname' =>$result->fname,
                            'middlename'=>$result->mname,
                            'level'     =>$result->year_level,
                            'course'    =>$result->abbreviation,
                            'section_name'=>$result->section_name,
                        );

                        $student_details = $acontent;
                        $hist_id = $result->id;

                        $current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
                        $student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum);

                        $assessment_values = array();
                        $ledger_data = array();
                        $ledger_data = $this->teller_model->get_ledger_data($result2->payers_id);

                        $period_now = $this->academic_terms_model->what_period();
                        $term = $this->academic_terms_model->what_period();
                        $sy =  $current_academic_terms_obj->sy;

                        $unposted_students_payments = $this->teller_model->get_unposted_students_payment($result2->payers_id);
                        $number_of_periods = ($current_academic_terms_obj->term=='Summer' ? 2 : 4);
                        $earliest_enrollment_schedule = $this->academic_terms_model->earliest_enrollment_schedule();
                        $student_is_enrolled = $this->student_model->student_is_enrolled($idnum);
                        $dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok

                        $lab_courses = array();
                        $courses = array();

                        if(!empty($dcourses)){
                            foreach ($dcourses as $course) {
                                //NOTE: change by Genes 12/1/2016
                                //if ( (int)$course->paying_units != 0){

                                if ( $course->post_status != "deleted"){
                                //if ( $course->paying_units != 0){
                                //  Filtering out zero paying units courses causes
                                //  such course to be excluded in the listing.  This
                                //  would create confusion in the part of the finance dept
                                //  And so, instead, just filter out deleted subjects--added condition 6.28.2018 Toyet
                                //  As of 5.30.2018, this filtering is disabled.
                                //  By: Toyet 5.30.2018
                                    if ($course->type_description == 'Lab' && empty($course->enrollments_history_id)) {
                                        $lab_courses[] = array(
                                            'name'=>$course->course_code,
                                            'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
                                            );
                                    }
                                    $courses[] = array(
                                        'id'=>$course->id,
                                        'name'=>$course->course_code,
                                        'units'=>$course->credit_units,
                                        'pay_units'=>($course->post_status=='dissolved' ? ' ': $course->paying_units),
                                        're_enrollments_id'=>$course->re_enrollments_id,
                                        'withdrawn_on'=>$course->withdrawn_on,
                                        'assessment_id'=>$course->assessment_id,
                                        'post_status'=>$course->post_status,
                                        'enrollments_history_id'=>$course->enrollments_history_id,
                                        'transaction_date'=>$course->transaction_date
                                    );
                                }
                            }
                        }

                        $my_acad_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum); //terms this student is

                        $current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $my_acad_terms[0]->id); 
                        $shs_assessment = $this->shs_student_lib->StudentAssessments($current_history); 
                        ////log_message("INFO", print_r($shs_assessment,true)); 

                        $cost_per_unit = $shs_assessment['tuition_basic']->rate;

                        $tuition_fees_content = array();  //reset

                        $tuition_fees_content[0] = array(
                                'fee_type'=>'TUITION ' . $term . ' SY ' . $sy . ' @ ' . number_format($cost_per_unit, 2),
                        );

                            if (is_array($courses) && !empty($courses)){
                                foreach ($courses as $course){ //$total_tuition += ($course['pay_units'] * $cost_per_unit); $total_pay_units += $course['pay_units']; $total_units += $course['units'];
                                        if (!$course['withdrawn_on']) {
                                            $tuition_fees_content[count($tuition_fees_content)] = array(
                                                    'subject' => $course['name'],
                                                    'units' => $course['units'],
                                                    'hours' => $course['pay_units'],
                                                    'amount' => (isset($other_courses_payments[$course['id']]) && array_key_exists($course['id'], $other_courses_payments) ? $other_courses_payments[$course['id']] : $cost_per_unit) * $course['pay_units'],
                                            );
                                        }
                                }
                            }

                        $miscellaneous_fees_content = array();  //reset

                        $gtotal = 0;

                        if ($shs_assessment['misc_fees']) {
                            foreach($shs_assessment['misc_fees'] AS $misc_fee) {
                                $total=0;
                                if ($misc_fee->fees_subgroups) {
                                    foreach($misc_fee->fees_subgroups AS $fee_subgroup) {
                                        $total = $total + $fee_subgroup->rate; 
                                        $misc_subtotal[$misc_fee->fees_group] = $total;
                                }
                                $miscellaneous_fees_content[] = array(
                                        'description'=>$misc_fee->fees_group,
                                        'amount'=>$total,
                                    );
                                $gtotal = $gtotal + $total;
                                }
                            }
                        }

                        if ($shs_assessment['lab_fees']) {
                            $total=0;
                            foreach($shs_assessment['lab_fees'] AS $lab_fee) {
                                $total = $total + $lab_fee->rate;
                            }
                            $miscellaneous_fees_content[] = array(
                                    'description'=>'Laboratory Fees',
                                    'amount'=>$total,
                                );
                            $gtotal = $gtotal + $total;
                        }

                        $period = $this->academic_terms_model->what_period();


                        $this->custom_loader->library('balances_lib',
                                array(
                                        'ledger_data'=>$ledger_data,
                                        'unposted_students_payments'=>$unposted_students_payments,
                                        'current_academic_terms_obj'=>$current_academic_terms_obj,
                                        'period'=>$this->academic_terms_model->what_period(),
                                        'period_dates'=>$this->academic_terms_model->period_dates(),
                                ),'balances_lib'); 

                        $trans_after_assess = $this->balances_lib->transAfterAssessment();
                        $balance_before_assessment = $this->balances_lib->balance_before_assessment;

                        ////log_message("INFO","TRANS AFTER ASSESS => ".print_r($trans_after_assess,true)); 
                        ////log_message("INFO","BALANCE B4 ASSESS  => ".print_r($balance_before_assessment,true)); 

                        $selected_history_id = "";
                        $assessed_value = 0;
                        $ass_content = "";

                        if($i==1){
                            $withDivStart = "YES";
                        } else {
                            $withDivStart = "NO";
                        }

                        if($i<$imax) {
                            $withDivEnd = "NO";
                        } else {
                            $withDivEnd = "YES";
                        }
                        $i += 1;

                        $payable_prelim = 0;

                        if($what=='clearances'){
                            // special treatment for lab courses
                            // although this is a bad practice, but due to time constraints
                            // i have to employ this...
                            $lab_courses_names = array();
                            foreach($lab_courses as $labs){
                                $lab_courses_names[] = $labs['name'];
                            }
                            ////log_message("INFO",print_r($lab_courses_names,true));
                            $clearance_content .= $this->load->view('print_templates/student_clearance_shs', 
                                                array(
                                                        'ledger_data'=>$this->balances_lib->current_term_transactions(),
                                                        'idnumber'=>$result2->idno,
                                                        'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
                                                        'name'=>($result2->lname.", ".$result2->fname." ".substr($result2->mname,0,1)),
                                                        'course_year'=>($result2->year_level." ".$result2->abbreviation),
                                                        'payable' => $this->balances_lib->payable_for_current_period,
                                                        'payment_period'=>'Finals',
                                                        'lab_courses'=>$lab_courses_names,
                                                        'section_name'=>$student_details['section_name'],
                                                        'hist_id'=>$hist_id,
                                                        'is_SHS'=>TRUE,
                                                    ),
                                                TRUE);
                            //die();
                        } else {

                            if($period=="Prelim"){
                                $ass_content += $this->load->view('print_templates/student_assessment_shs',
                                    array(
                                            'selected_history_id'=>$selected_history_id,
                                            'current_academic_term_obj'=>$current_academic_terms_obj,
                                            'term'=>$current_academic_terms_obj->term,
                                            'tuition_fees_content'=>isset($tuition_fees_content) ? $tuition_fees_content : array(),
                                            'miscellaneous_fees_content'=>isset($miscellaneous_fees_content) ? $miscellaneous_fees_content : array(),
                                            'idnumber'=>$student_details['idnumber'],
                                            'name'=>$student_details['familyname'].', '.$student_details['firstname'].' '.$student_details['middlename'],
                                            'level'=>$student_details['level'],
                                            'course_year'=>$student_details['course'].'-'.$student_details['level'].' ('.$student_details['section_name'].')',
                                            'payable_midterm'=>(isset($payable_midterm) ? $payable_midterm : 0),
                                            'payable_prelim'=>(isset($payable_prelim) ? $payable_prelim : 0),
                                            'bal_b4_assessment'=>$balance_before_assessment,
                                            'assessed_value'=>$assessed_value,
                                            'trans_after_assess'=>$trans_after_assess,
                                            'period' => "Exam #1",
                                            'withDivStart'=>$withDivStart,
                                            'withDivEnd'=>$withDivEnd
                                    )
                                );
                            } else {
                                $ass_content += $this->load->view('print_templates/invoice',
                                    array(
                                            'period'=>$period,
                                            'div_id'=>'print_midterm_assessment',
                                            'ledger_data'=>$this->balances_lib->current_term_transactions(),
                                            'payable' => $this->balances_lib->payable_for_current_period,
                                            'idnumber'=>$result2->idno,
                                            'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
                                            'name'=>$student_details['familyname'].', '.$student_details['firstname'].' '.$student_details['middlename'],
                                            'course_year'=>$student_details['course'].'-'.$student_details['level'],
                                            'section_name'=>$student_details['section_name'],
                                            TRUE,
                                            'withDivStart'=>$withDivStart,
                                            'withDivEnd'=>$withDivEnd
                                    )
                                );
                            }
                        }

                $this->custom_loader->unload_library('balances_lib');
            endforeach;  //foreach($idnos as $idno):

        } else {
            // mass printing basic_ed  Toyet 9.24.2018
            $this->load->model('hnumis/student_model'); //added by tatskie
            $this->load->model('academic_terms_model');
            $this->load->library('print_lib');
            $this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments');
            $enrollees = $this->enrollments->current_enrollees($level, $year_level, $section);

            $this->load->model('teller/teller_model');
            $this->load->model('hnumis/academic_terms_model');
            $this->load->model('hnumis/academicyears_model', 'AcademicYears_model');
            $this->load->model('basic_ed/assessment_basic_ed_model');

            $current_acadYear           = $this->AcademicYears_model->current_academic_year();
            $current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
            $students                   = array();
            $this->load->model('basic_ed/basic_ed_model', 'bed_model');  // added by Toyet
            $this->load->library('basic_ed_balances');

            foreach ($enrollees as $enrollee) {
                $ledger_data       = $this->teller_model->get_ledger_data($enrollee->payers_id);
                $unposted_payments = $this->teller_model->get_unposted_students_payment($enrollee->payers_id);
                $config            = array(
                    'ledger_data' => $ledger_data,
                    'unposted_transactions' => $unposted_payments,
                    'periods' => $this->bed_model->exam_dates($year_level, $level),
                    'current_academic_year' => $this->bed_model->academic_year(),
                    'bed_status' => $enrollee->bed_status,
                    'current_nth_exam' => $this->bed_model->current_exam($year_level, $level),
                    'total_number_of_exams' => count($this->bed_model->exam_dates($year_level, $level))
                );
                $this->basic_ed_balances->__construct($config);
                if ($this->basic_ed_balances->error == TRUE) {
                    $this->content_lib->set_message($this->basic_ed_balances->error_message, 'alert-error');
                    $this->content_lib->content();
                    return;
                } //$this->basic_ed_balances->error == TRUE

                $students[$enrollee->idno] = (object) array(
                    'basic_info' => $enrollee,
                    'ledger_data' => $this->basic_ed_balances->current_year_transactions(),
                    'exam_name' => 'Exam #'.$this->bed_model->current_exam($year_level, $level),
                    'payable' => $this->basic_ed_balances->payable_for_current_period,
                    'bal_b4_assessment' => $this->basic_ed_balances->previous_year_credit_balance,
                    'current_year_transactions' => $this->basic_ed_balances->current_year_transactions );
            } //$enrollees as $enrollee

            ////log_message("INFO",print_r($students,true)); // Toyet 11.6.2018

            $exam_name = 'Exam #'.$config['current_nth_exam'];

            if($config['current_nth_exam']==1){

            ////log_message("INFO", 'STUDENT-HISTORIES-ID => '.print_r($students,true)); // Toyet 7.21.2018
            $ass_content = "";
            $i = 1;
            $iLen = count($enrollees);

            foreach ($enrollees as $enrollee) {
                ////log_message("INFO", 'SINGLE ACCESS => ' . print_r($students[$enrollee->idno]->basic_info->student_histories_id, true)); // Toyet 7.21.2018
                $exam_name         = 'Exam #';
                $clearance_content = $this->load->view('mass_printing_templates/basic_ed_clearance', array(
                    'students' => $students,
                    'offices' => array(
                        'Library',
                        'Bookstore',
                        'Principal',
                        'Accounts Clerk'
                    )
                ), TRUE);
                /*
                $ass_content        = $this->load->view('mass_printing_templates/basic_ed_ass', array(
                'students'       => $students,
                'exam_number' =>  $exam_name . $this->bed_model->current_exam($year_level, $level),
                'year_start_date' => $current_academic_terms_obj->year_start_date,
                'current_acad_year_id'=>$current_acadYear->id,
                'due'     => 0 ,
                'can_print_statement'=>TRUE,
                'div_id'=>'statement',
                ), TRUE);
                */
                //inserted/changed by Toyet 7.21.2018
                $basic_ed_fees     = $this->assessment_basic_ed_model->ListBasic_Ed_Fees($level, $current_acadYear->id, $year_level);
                $tuition_fee       = 0;
                ////log_message("INFO", print_r($basic_ed_fees,true)); // Toyet 7.23.2018

	            $tuition_fees_content = array();
    	        $miscellaneous_fees_content = array();
                $trans_after_assess = 0;

                foreach ($students[$enrollee->idno]->current_year_transactions as $ledger_row) {
                    if ($ledger_row->transaction_detail == "ASSESSMENT") {
                        $tuition_fee = $ledger_row->debit;
                    } else {
                        $trans_after_assess = $trans_after_assess + round(($ledger_row->debit-$ledger_row->credit),2);
                    }
                } //$students[$enrollee->idno]->ledger_data as $ledger_row

                //$students[$enrollee->idno]->payable = $tuition_fee / $config['total_number_of_exams'];
                ////log_message("INFO", print_r($students[$enrollee->idno]->payable,true)); // Toyet 7.23.2018

                $g_total                      = 0;
                $group                        = "";
                $item_total                   = 0;
                $new                          = FALSE;
                if ($basic_ed_fees) {
                    foreach ($basic_ed_fees AS $basic_fee) {
                        if ($group != $basic_fee->fees_group) {
                            $group = $basic_fee->fees_group;
                            if ($new) {
                                $new                          = FALSE;
                                $miscellaneous_fees_content[] = array(
                                    'fee_type' => $old_group,
                                    'amount' => $item_total
                                );
                            } //$new
                            $g_total    = $g_total + $item_total;
                            $item_total = 0;
                            $new        = TRUE;
                        } //$group != $basic_fee->fees_group
                        else {
                            $old_group = $basic_fee->fees_group;
                        }
                        $item_total = $item_total + $basic_fee->rate;
                    } //$basic_ed_fees AS $basic_fee
                    $miscellaneous_fees_content[] = array(
                        'fee_type' => $old_group,
                        'amount' => $item_total
                    );
                    $g_total                      = $g_total + $item_total;

                } //$basic_ed_fees

                ////log_message("INFO", print_r($g_total,true)); // Toyet 7.23.2018
                ////log_message("INFO", print_r($tuition_fee,true)); // Toyet 7.23.2018
                $tuition_fees_content[]       = array(
                    'subject' => 'Basic Tuition',
                    'units' => ' ',
                    'hours' => ' ',
                    'amount' => $tuition_fee-$g_total
                );

                $bal_b4_assessment = 0;
                $payable_prelim    = 0;
                $term              = 0;

                if($i==1) {
                	$withDivStart = "YES";
                	$withDivEnd = "NO";
                } else {
	               	$withDivStart = "NO";
                	$withDivEnd = "NO";
                }
                if($i==$iLen){
	               	$withDivStart = "NO";
                	$withDivEnd = "YES";
                }

                ////log_message("INFO",print_r($i,true));
                ////log_message("INFO",print_r($withDivStart,true));
                ////log_message("INFO",print_r($withDivEnd,true));

                ////log_message("INFO", "[".$enrollee->idno."] TRANS AFTER ASSESS => ".print_r($trans_after_assess,true)); // Toyet Toyet 7.24.2018

                //if($trans_after_assess>=$students[$enrollee->idno]->payable){
                //    $students[$enrollee->idno]->payable = 0;
                //} else {
                //    $students[$enrollee->idno]->payable = $students[$enrollee->idno]->payable + $trans_after_assess;
                //}
                $students[$enrollee->idno]->payable = $this->basic_ed_balances->payable_for_current_period;
                
                $bal_b4_assessment = $students[$enrollee->idno]->bal_b4_assessment;
                if(abs($trans_after_assess)>=$bal_b4_assessment)
                    $bal_b4_assessment = 0;
                ////log_message("INFO", "[".print_r($enrollee->idno,true)."] BB4Ass =>".print_r($bal_b4_assessment,true)); // Toyet 7.25.2018
                ////log_message("INFO", "[".print_r($enrollee->idno,true)."] =>".print_r($students[$enrollee->idno]->current_year_transactions,true)); // Toyet 7.25.2018
                ////log_message("INFO", "[".print_r($enrollee->idno,true)."] =>".print_r($students[$enrollee->idno]->payable,true)); // Toyet 7.25.2018

                if($config['current_nth_exam']==1){
                    $ass_content += $this->load->view('print_templates/student_assessment_bed', array(
                        'idnumber' => $enrollee->idno,
                        'level' => $year_level,
                        'course' => $section,
                        'name' => ($students[$enrollee->idno]->basic_info->lname . ", " . $students[$enrollee->idno]->basic_info->fname . " " . substr($students[$enrollee->idno]->basic_info->mname, 0, 1)),
                        'course_year' => $students[$enrollee->idno]->basic_info->yr_level . " " . $students[$enrollee->idno]->basic_info->level . " " . $students[$enrollee->idno]->basic_info->section,
                        'period' => $students[$enrollee->idno]->exam_name,
                        'payable' => $students[$enrollee->idno]->payable,
                        'ledger_data' => $students[$enrollee->idno]->ledger_data,
                        'tuition_fee' => $tuition_fee-$g_total,
                        'tuition_fees_content' => $tuition_fees_content,
                        'miscellaneous_fees_content' => $miscellaneous_fees_content,
                        'bal_b4_assessment' => $bal_b4_assessment,
                        'payable_prelim' => $payable_prelim,
                        'term' => $term,
                        'withDivStart'=>$withDivStart,
                        'withDivEnd'=>$withDivEnd
                    ));
                } else {
                    // just skip, actual printing is done after the loop for all enrollees
                    // because, mass_printing_templates/basic_ed_ass has a loop of its own
                    // ...  Toyet 9.24.2018
                }

                $i += 1; 

            } // foreach($enrollees as $enrollee){ 
        } //if(in_array($year_level,$year_level_array)) {

            if($config['current_nth_exam']!=1){
                  $ass_content = $this->load->view('mass_printing_templates/basic_ed_ass', array(
                            'students'    => $students,
                            'exam_number' => $exam_name,
                            'year_start_date' => $current_academic_terms_obj->year_start_date,
                            'current_acad_year_id'=>$current_acadYear->id,
                            'can_print_statement'=>TRUE
                            ), TRUE);
                  $ass_content = '<div id="to_print" style="display:none;">' . $ass_content . '</div>';
            }

        } //if($level in $level_array)
        } //!empty($section)

        ////log_message("INFO", print_r($what,true));
        ////log_message("INFO", print_r($ass_content,true));
        ////log_message("INFO", print_r($clearance_content,true));
        switch ($what) {
            case 'clearances':
                // SHS clearances have already been prepared.  so, go directly to 
                // assigning the value to the <div>, otherwise, prepare for Basic_Ed clearance... Toyet 3.6.2019
                if(!in_array($year_level,$year_level_array)){
                     $clearance_content = $this->load->view('mass_printing_templates/basic_ed_clearance', array(
                                                             'students' => $students,
                                                             'offices' => array(
                                                             'Library',
                                                             'Bookstore',
                                                             'Principal',
                                                             'Accounts Clerk' ) ), TRUE);
                }
                $content = '<div id="assessment_to_print" style="display:none;" >' . $clearance_content . "</div>";
                break;
            case 'statements':
                $content = $ass_content;
                break;
        } //$what
        $this->load->view('mass_printing_templates/container', array(
            'title' => 'Basic Ed ' . ucfirst($what) . ' Printing',
            'content' => $content
        ));
    }
} 
