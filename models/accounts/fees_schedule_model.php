<?php 
class fees_schedule_model extends MY_Model {


	
	/**
	 * This function  retrieves all the fees group
	 * @return unknown|boolean
	 */
	//UPDATED: 5/7/14 by genes excludes laboratory
	public function get_feesgroups() {
		$sql = "
			SELECT 
				fg.id, fg.fees_group
			FROM 
				fees_groups fg
			WHERE 
				id != 5
			ORDER BY
				fees_group
		";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}
		return false;	
	}

	
	//ADDED: 8/8/13 by genes
	/**
	 * This function adds a new fees schedule
	 * @param array $fees_data (representing a new entry in the table)
	 * @return Ambigous <boolean, unknown>
	 */
	function AddFeesSchedule($fees_data,$office='College') {
	
		$status = array();
		
		if ($office == 'College') {
			$query = "
					INSERT INTO
						fees_schedule 
							( 
								fees_subgroups_id, 
								acad_program_groups_id,
								academic_terms_id,
								levels_id,
								yr_level,
								rate,
								posted,
								inserted_by,
								inserted_on
							)
					VALUES (						
								{$this->db->escape($fees_data['fees_subgroups_id'])},
								{$this->db->escape($fees_data['acad_program_groups_id'])},
								{$this->db->escape($fees_data['academic_terms_id'])},
								{$this->db->escape($fees_data['levels_id'])},
								{$this->db->escape($fees_data['yr_level'])},
								{$this->db->escape($fees_data['rate'])},
								{$this->db->escape($fees_data['posted'])},
								{$this->session->userdata('empno')}, 
								now()
							)
					";
		} else { //basic ed:check if fees schedule already encoded
			$query = "SELECT
							a.id
						FROM
							fees_schedule AS a
						WHERE
							a.fees_subgroups_id = {$this->db->escape($fees_data['fees_subgroups_id'])}
							AND a.academic_terms_id = {$this->db->escape($fees_data['academic_terms_id'])}
							AND a.levels_id = {$this->db->escape($fees_data['levels_id'])} 
							AND a.yr_level = {$this->db->escape($fees_data['yr_level'])}";
			//print($query); die();
			$result = $this->db->query($query);
			
			if($result->num_rows() > 0){
				return FALSE;	
			} else {
				$query = "
					INSERT INTO
						fees_schedule 
							( 
								fees_subgroups_id, 
								academic_terms_id,
								levels_id,
								yr_level,
								rate,
								posted,
								inserted_by,
								inserted_on
							)
					VALUES (						
								{$this->db->escape($fees_data['fees_subgroups_id'])},
								{$this->db->escape($fees_data['academic_terms_id'])},
								{$this->db->escape($fees_data['levels_id'])},
								{$this->db->escape($fees_data['yr_level'])},
								{$this->db->escape($fees_data['rate'])},
								{$this->db->escape($fees_data['posted'])},
								{$this->session->userdata('empno')}, 
								now()
							)
					";	
			//	print($query); die();
			}
		}

	//die($query);
		if ($this->db->query($query)) {
			$status['error']                  = FALSE;
		} else {
			if ($office == 'College') {	
				$status['fees_subgroups_id'] 	  = $fees_data['fees_subgroups_id'];
				$status['acad_program_groups_id'] = $fees_data['acad_program_groups_id'];
				$status['academic_terms_id']      = $fees_data['academic_terms_id'];
				$status['levels_id']              = $fees_data['levels_id'];
				$status['yr_level']               = $fees_data['yr_level'];
				$status['error']                  = TRUE;
			} else {
				$status['fees_subgroups_id'] 	  = $fees_data['fees_subgroups_id'];
				$status['academic_terms_id']      = $fees_data['academic_terms_id'];
				$status['levels_id']              = $fees_data['levels_id'];
				$status['yr_level']               = $fees_data['yr_level'];
				$status['error']                  = TRUE;
			}
		}

		return $status;
	}

	
	//ADDED: 04/13/2014 by genes
	function DeleteFeesSchedule($fees_schedule_id) {
		$query = "DELETE
		FROM
		fees_schedule
		WHERE
		id = {$this->db->escape($fees_schedule_id)} ";
			
		//print($query); die();
		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	//ADDED: 04/13/2014 by genes
	function DeleteLaboratoryFees($laboratory_fees_id) {
		$query = "DELETE 
					FROM 
						laboratory_fees 
					WHERE
						id = {$this->db->escape($laboratory_fees_id)} ";
			
		//print($query); die();
		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	
	//ADDED: 04/13/2014 by genes
	function DeleteTuitionOthers($tuition_others_id) {
		$query = "DELETE 
					FROM 
						tuition_others 
					WHERE
						id = {$this->db->escape($tuition_others_id)} ";
			
		//print($query); die();
		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function UpdateFeesSchedule($data) {
		$q1 = NULL;
			
		$q1 = "UPDATE 
					fees_schedule
			 	SET 
			 		rate = {$this->db->escape($data['rate'])},
			 		updated_by =  {$this->session->userdata('empno')},
					updated_on = now()
				WHERE 
					id = {$this->db->escape($data['fees_schedule_id'])}";
			
		if ($this->db->query($q1)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
	
	//ADDED: 04/21/14 
	function UpdateLaboratoryFees($data) {
		$q1 = NULL;
			
		$q1 = "UPDATE 
					laboratory_fees
			 	SET 
			 		rate = {$this->db->escape($data['rate'])},
			 		updated_by =  {$this->session->userdata('empno')},
					updated_on = now()
			 	WHERE 
					id = {$this->db->escape($data['laboratory_fees_id'])}";
			
		if ($this->db->query($q1)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
	
	//ADDED: 04/21/14 
	function UpdateTuitionOtherFees($data) {
		$q1 = NULL;
			
		$q1 = "UPDATE 
					tuition_others
			 	SET 
			 		rate = {$this->db->escape($data['rate'])},
			 		updated_by =  {$this->session->userdata('empno')},
					updated_on = now()
				WHERE 
					id = {$this->db->escape($data['tuition_others_id'])}";
			
		if ($this->db->query($q1)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
	
	/**
	 * This function retrieves the incoming and current academic years
	 * @return unknown|boolean
	 */
	public function get_school_years() {		
		$sql="
			select ay.id, concat_ws('-',ay.end_year-1,ay.end_year) as school_year,status				
			from academic_years ay
			where ay.`status` in ('incoming','current')		
			order by school_year
		";	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}
		return false;	
	}	

	/**
	 * This function retrieves the fees group id and description
	 * @param tinyint $fees_groups_id
	 * @return unknown|boolean
	 */
	public function get_fees_subgroups($fees_groups_id=null) {
		$sql = "
			SELECT
				fs.id, fs.description
			FROM
				fees_subgroups fs
			WHERE 
				fs.fees_groups_id = '{$fees_groups_id}'
			ORDER BY
				description
		";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}
		return false;
	}
	
	/**
	 * This function retrieves all fees subgroups
	 * @return NULL
	 * EDITED: 6/26/15 by genes
	 */
	function ListFeesSubGroups($fees_groups_id=NULL) {

		$result = NULL;
	/* edited by RA 041017 to display options for fees_subgroups */
	
		/* if ($fees_groups_id) {
			$q = "SELECT
						a.*
					FROM
						fees_subgroups AS a
					WHERE
						a.fees_groups_id = {$this->db->escape($fees_groups_id)}
					ORDER BY
							a.id";
		} else { */
			$q = "SELECT
						a.*
					FROM
						fees_subgroups AS a
					ORDER BY
							a.id";
	/*	} */
				
		$query = $this->db->query($q);
				
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
		
	}

	//ADDED: 8/7/13 by genes
	/**
	 * This function retrieves the id and description of a given fees subgroup
	 * @param smallint $fees_subgroups_id
	 * @return NULL
	 */
	function getFeesSubGroup($fees_subgroups_id) {
		$result = null;
	
		$q1 = "SELECT
					a.id,
					a.description,
					b.fees_group
				FROM
					fees_subgroups AS a,
					fees_groups AS b
				WHERE
					a.id = {$this->db->escape($fees_subgroups_id)}
					AND a.fees_groups_id=b.id";
		//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query->num_rows() > 0){
			$result = $query->row();
		}
	
		return $result;
	
	}
	
	//ADDED: 8/10/13 by genes
	//EDITED: 8/18/13 change GROUP BY a.description
	/**
	 * This function retrieves details of academic programs with fees
	 * @param smallint $academic_terms_id
	 * @return NULL
	 */
	function ListAcadPrograms_with_fees($academic_terms_id) {
		$result = NULL;
		
		$q = "SELECT
					a.id,
					a.abbreviation,
					a.description,
					b.acad_program_groups_id,
					c.abbreviation AS group_abbreviation,
					b.fees_subgroups_id
				FROM
					academic_programs AS a,
					fees_schedule AS b,
					acad_program_groups AS c
				WHERE
					a.acad_program_groups_id=b.acad_program_groups_id
					AND a.acad_program_groups_id=c.id
					AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
					AND a.status='O'
				GROUP BY
					a.description
				ORDER BY
					a.abbreviation";
		
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
		
	}

	
	//ADDED: 8/10/13 by genes
	//method updated: 4/13/2014 by genes
	//updated so that fees schedule id can be retrieved
	/**
	 * This function retrieves the fees group and description for a given academic term and program group
	 * @param smallint $academic_terms_id (represents the academic term)
	 * @param tinyint $acad_program_groups_id (represents the academic program group)
	 * @return NULL
	 */
	function ListProgramItems($academic_terms_id,$acad_program_groups_id) {
		$result = NULL;
		
		$q = "SELECT
					fees_group,
					description,
					MAX(IF(yr_level = 1, rate, NULL)) y1,
					MAX(IF(yr_level = 2, rate, NULL)) y2,
					MAX(IF(yr_level = 3, rate, NULL)) y3,
					MAX(IF(yr_level = 4, rate, NULL)) y4,
					MAX(IF(yr_level = 5, rate, NULL)) y5,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE 
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=1)
					AS fee1,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE 
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=2)
					AS fee2,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE 
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=3)
					AS fee3,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE 
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=4)
					AS fee4,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE 
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=5)
					AS fee5
				FROM
					(SELECT
							fgs.weight,
							fgs.fees_group,
							fg.description,
							fs.yr_level,
							fs.rate,
							fs.id,
							fs.fees_subgroups_id
						FROM
							fees_schedule fs
							JOIN fees_subgroups as fg ON fs.fees_subgroups_id = fg.id
							JOIN fees_groups AS fgs ON fgs.id=fg.fees_groups_id
							JOIN academic_programs AS ap ON ap.acad_program_groups_id=fs.acad_program_groups_id
						WHERE 
							fs.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND fs.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND ap.status='O'
						GROUP BY
							fs.yr_level, fg.id
					) test
				GROUP BY
					description
				ORDER BY
					weight, description";
		
		//print($q); die();
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
		$result = $query->result();
		}
			
		return $result;
		
	}
	
	
	//ADDED: 8/30/13 by genes
	//EDITED: 4/22/14 includes fees id 
	/**
	 * This function retrieves the fees group and description applicable to the Basic Ed
	 * @param smallint $academic_terms_id (represents the academic term)
	 * @param tinyint $levels_id (represents the level)
	 * @return NULL
	 */
	function ListBasic_Ed_Items($academic_terms_id,$levels_id) {
		$result = NULL;
	
		$q = "SELECT
					fees_group,
					description,
					MAX(IF(yr_level = 1, rate, NULL)) y1,
					MAX(IF(yr_level = 2, rate, NULL)) y2,
					MAX(IF(yr_level = 3, rate, NULL)) y3,
					MAX(IF(yr_level = 4, rate, NULL)) y4,
					MAX(IF(yr_level = 5, rate, NULL)) y5,
					MAX(IF(yr_level = 6, rate, NULL)) y6,
					MAX(IF(yr_level = 7, rate, NULL)) y7,
					MAX(IF(yr_level = 8, rate, NULL)) y8,
					MAX(IF(yr_level = 9, rate, NULL)) y9,
					MAX(IF(yr_level = 10, rate, NULL)) y10,
					MAX(IF(yr_level = 11, rate, NULL)) y11,
					MAX(IF(yr_level = 12, rate, NULL)) y12,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=1)
					AS fee1,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=2)
					AS fee2,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=3)
					AS fee3,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=4)
					AS fee4,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=5)
					AS fee5,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=6)
					AS fee6,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=7)
					AS fee7,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=8)
					AS fee8,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=9)
					AS fee9,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=10)
					AS fee10,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=11)
					AS fee11,
					(SELECT
							a.id
						FROM
							fees_schedule a
						WHERE
							a.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.acad_program_groups_id IS NULL
							AND a.fees_subgroups_id = test.fees_subgroups_id
							AND a.yr_level=12)
					AS fee12
			FROM
				(SELECT
						fgs.weight,
						fgs.fees_group,
						fg.description,
						fs.yr_level,
						fs.rate,
						fs.fees_subgroups_id
					FROM
						fees_schedule fs
						JOIN fees_subgroups as fg ON fs.fees_subgroups_id = fg.id
						JOIN fees_groups AS fgs ON fgs.id=fg.fees_groups_id
					WHERE
						fs.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND fs.levels_id = {$this->db->escape($levels_id)}
					GROUP BY
						fs.yr_level, fg.id
				) test
				
			GROUP BY
				description
			ORDER BY
				weight, description";
	
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
	
	return $result;
	
	}
	
	
		
	//ADDED: 8/18/13 by genes
	//EDITED: 4/26/14 to include electives
	/**
	 * This function retrieves all laboratory items of a given academic year and program
	 * @param smallint $academic_years_id (represents the academic year)
	 * @param mediumint $academic_programs_id
	 * @return NULL
	 */
	function ListLaboratoryItems($academic_years_id, $academic_programs_id) {
		$result = NULL;
	
		$q = "(SELECT
					a.id AS laboratory_fees_id,
					e.course_code AS course_code,
					c.year_level,
					FORMAT(a.rate,2) AS rate
				FROM
					laboratory_fees AS a,
					prospectus AS b,
					prospectus_terms AS c,
					prospectus_courses AS d,
					courses AS e
				WHERE
					a.courses_id = d.courses_id
					AND d.prospectus_terms_id = c.id
					AND c.prospectus_id = b.id
					AND a.courses_id = e.id
					AND a.academic_years_id = {$this->db->escape($academic_years_id)}
					AND b.academic_programs_id = {$this->db->escape($academic_programs_id)}
			)
			UNION
			(SELECT
					b.id AS laboratory_fees_id,
					c.course_code AS course_code,
					e.year_level,
					FORMAT(b.rate,2) AS rate
				FROM
					elective_courses AS a,
					laboratory_fees AS b,
					courses AS c,
					prospectus_courses AS d,
					prospectus_terms AS e,
					prospectus AS f
				WHERE
					a.topic_courses_id = b.courses_id
					AND c.id = b.courses_id
					AND a.prospectus_courses_id = d.id
					AND d.prospectus_terms_id = e.id
					AND e.prospectus_id = f.id
					AND b.academic_years_id = {$this->db->escape($academic_years_id)}
					AND f.academic_programs_id = {$this->db->escape($academic_programs_id)}
			)		
			ORDER BY
				course_code ";
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
	return $result;
	
	}
	
	
	/*
	 * ADDED: 6/11/15 by genes
	 * MODIFIED: 6/25/15 by genes
	 */
	function ListAffiliated_Items($acad_terms_id, $acad_programs_id) {
		$result = NULL;
		
		$q = "SELECT 
					a.id AS affiliation_id,
					f.course_code,
					a.description,
					FORMAT(a.rate,2) AS rate
				FROM
					fees_affiliation AS a,
					fees_affiliation_courses AS e,
					courses AS f,
					prospectus_courses AS b,
					prospectus_terms AS c,
					prospectus AS d
				WHERE
					a.id=e.fees_affiliation_id
					AND e.courses_id=f.id
					AND f.id=b.courses_id
					AND b.prospectus_terms_id=c.id
					AND c.prospectus_id=d.id
					AND d.academic_programs_id = {$this->db->escape($acad_programs_id)}
					AND a.academic_terms_id = {$this->db->escape($acad_terms_id)} 
				GROUP BY
					e.fees_affiliation_id,e.courses_id 
				ORDER BY
					f.course_code, a.description ";
		//die($q);
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
		
	}
	
	/*
	 * ADDED: 6/25/15 by genes
	 */
	function ListAffiliationFees($acad_yr_id) {
		$result = NULL;
		
		$q = "SELECT
					a.id,
					a.description,
					FORMAT(a.rate,2) AS rate,
					c.academic_years_id,
					c.id AS academic_terms_id 
				FROM
					fees_affiliation AS a,
					academic_terms AS c
				WHERE
					a.academic_terms_id=c.id
					AND c.academic_years_id = {$this->db->escape($acad_yr_id)}
				GROUP BY
					a.description,
					a.rate
				ORDER BY
					a.description";
		//die($q);
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
		
	}
	

	/*
	 * ADDED: 11/11/15 by genes
	 */
	function getAffilFees($fees_affiliation_id) {
		$result = NULL;
		
		$q = "SELECT
					a.id,
					a.description,
					a.rate
				FROM
					fees_affiliation AS a
				WHERE
					a.id = {$this->db->escape($fees_affiliation_id)} ";
		//die($q);
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
		
		return $result;
		
	}


	/*
	 * ADDED: 11/11/15 by genes
	 */
	function ListAffilFees($acad_yr_id,$affil) {
		$result = NULL;
		
		$q = "SELECT
					a.id,
					a.description,
					a.rate
				FROM
					fees_affiliation AS a,
					academic_terms AS b
				WHERE
					a.academic_terms_id = b.id
					AND b.academic_years_id = {$this->db->escape($acad_yr_id)}
					AND a.description = {$this->db->escape($affil->description)} 
					AND a.rate = {$this->db->escape($affil->rate)} ";
		//die($q);
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
		
	}
	
	//ADDED: 8/20/13 by genes
	//EDITED: 4/27/14 to include elective courses
	/**
	 * This function retrieves all other tuition fees for a given term and academic program
	 * @param smallint $terms_id (represents the academic term)
	 * @param tinyine $academic_programs_id (represents the academic program)
	 * @return NULL
	 */
	function ListOtherTuition_Fees($terms_id, $academic_programs_id) {
		$result = NULL;
	
		/*$q = "SELECT
					tuition_others_id,
					course_code,
					FORMAT(rate,2) AS rate,
					FORMAT(MAX(IF(yr_level = 1, rate, NULL)),2) y1,
					FORMAT(MAX(IF(yr_level = 2, rate, NULL)),2) y2,
					FORMAT(MAX(IF(yr_level = 3, rate, NULL)),2) y3,
					FORMAT(MAX(IF(yr_level = 4, rate, NULL)),2) y4,
					FORMAT(MAX(IF(yr_level = 5, rate, NULL)),2) y5,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=1
							AND aa.courses_id = test.courses_id)
					AS fee1,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=2
							AND aa.courses_id = test.courses_id)
					AS fee2,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=3
							AND aa.courses_id = test.courses_id)
					AS fee3,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=4
							AND aa.courses_id = test.courses_id)
					AS fee4,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=5
							AND aa.courses_id = test.courses_id)
					AS fee5
				FROM
					((SELECT
							a.id AS tuition_others_id,
							e.course_code AS course_code,
							a.yr_level,
							a.rate,
							a.courses_id
						FROM
							tuition_others AS a,
							prospectus AS b,
							prospectus_terms AS c,
							prospectus_courses AS d,
							courses AS e
						WHERE
							a.courses_id = d.courses_id
							AND d.prospectus_terms_id = c.id
							AND c.prospectus_id = b.id
							AND a.courses_id = e.id
							AND a.academic_terms_id IN (".$terms_id.")
							AND b.academic_programs_id = {$this->db->escape($academic_programs_id)}
					) 
					UNION
					(SELECT
							b.id AS tuition_others_id,
							c.course_code AS course_code,
							b.yr_level,
							b.rate,
							b.courses_id
						FROM
							elective_courses AS a,
							tuition_others AS b,
							courses AS c,
							prospectus_courses AS d,
							prospectus_terms AS e,
							prospectus AS f
						WHERE
							a.topic_courses_id = b.courses_id
							AND c.id = b.courses_id
							AND a.prospectus_courses_id = d.id
							AND d.prospectus_terms_id = e.id
							AND e.prospectus_id = f.id
							AND b.academic_terms_id IN (".$terms_id.")
							AND f.academic_programs_id = {$this->db->escape($academic_programs_id)}
					)) test
				GROUP BY
					course_code
				ORDER BY
					course_code ";
		*/
		
		$q = "SELECT
					tuition_others_id,
					course_code,
					FORMAT(rate,2) AS rate,
					FORMAT(MAX(IF(yr_level = 1, rate, NULL)),2) y1,
					FORMAT(MAX(IF(yr_level = 2, rate, NULL)),2) y2,
					FORMAT(MAX(IF(yr_level = 3, rate, NULL)),2) y3,
					FORMAT(MAX(IF(yr_level = 4, rate, NULL)),2) y4,
					FORMAT(MAX(IF(yr_level = 5, rate, NULL)),2) y5,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=1
							AND aa.courses_id = test.courses_id)
					AS fee1,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=2
							AND aa.courses_id = test.courses_id)
					AS fee2,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=3
							AND aa.courses_id = test.courses_id)
					AS fee3,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=4
							AND aa.courses_id = test.courses_id)
					AS fee4,
					(SELECT
							aa.id 
						FROM
							tuition_others AS aa
						WHERE
							aa.academic_terms_id = {$this->db->escape($terms_id)}
							AND aa.yr_level=5
							AND aa.courses_id = test.courses_id)
					AS fee5
				FROM
					((SELECT
							a.id AS tuition_others_id,
							e.course_code AS course_code,
							a.yr_level,
							a.rate,
							a.courses_id
						FROM
							tuition_others AS a,
							courses AS e
						WHERE
							a.courses_id = e.id
							AND a.academic_terms_id IN (".$terms_id.")
					) 
					UNION
					(SELECT
							b.id AS tuition_others_id,
							c.course_code AS course_code,
							b.yr_level,
							b.rate,
							b.courses_id
						FROM
							tuition_others AS b,
							courses AS c
						WHERE
							b.courses_id = c.id
							AND b.academic_terms_id IN (".$terms_id.")
					)) test
				GROUP BY
					course_code
				ORDER BY
					course_code ";
		
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
	
		return $result;
	
	}
	
		
	//ADDED: 8/17/13 by genes
	/**
	 * This function retrieves laboratory courses with no fees
	 * @param smallint $academic_years_id
	 * @return NULL
	 */
	function ListLab_with_no_fees($academic_years_id) {
		$result = NULL;
		
		$q = "SELECT
					a.id,
					a.course_code
				FROM
					courses AS a
				WHERE
					a.id NOT IN (SELECT 
										b.courses_id
									FROM
										laboratory_fees AS b,
										prospectus_courses AS c,
										prospectus_terms AS d,
										prospectus AS e
									WHERE
										b.academic_years_id = {$this->db->escape($academic_years_id)} 
										AND b.courses_id=c.courses_id
										AND c.prospectus_terms_id=d.id
										AND d.prospectus_id=e.id
										AND e.status='A' )
					AND a.course_types_id = 11
				ORDER BY
					a.course_code ";
		
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
		$result = $query->result();
		}
			
		return $result;
		
	}

	//ADDED: 8/20/13 by genes
	/**
	 * This function retrieves courses not under tuition others
	 * @param smallint $academic_terms_id
	 * @return NULL
	 */
	function ListCourses_not_in_tuition_others($academic_terms_id) {
		$result = NULL;
	
		$q = "SELECT
					a.id,
					a.course_code
				FROM
					courses AS a
				WHERE
					a.id NOT IN (SELECT
										b.courses_id
									FROM
										tuition_others AS b,
										prospectus_courses AS c,
										prospectus_terms AS d,
										prospectus AS e
									WHERE
										b.academic_terms_id = {$this->db->escape($academic_terms_id)}
										AND b.courses_id=c.courses_id
										AND c.prospectus_terms_id=d.id
										AND d.prospectus_id=e.id
										AND e.status='A' 
								)
				ORDER BY
					a.course_code ";
	
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
	
	}

	
	//ADDED: 6/25/15 by genes
	/**
	 * This function retrieves courses not under fees_affiliation_courses
	 * @param smallint $academic_terms_id
	 * @return NULL
	 */
	function ListCourses_not_in_affiliation_courses($academic_years_id) {
		$result = NULL;
	
		$q = "SELECT
					a.id,
					a.course_code
				FROM
					courses AS a
				WHERE
					a.id NOT IN (SELECT
										b.courses_id
									FROM
										fees_affiliation AS a,
										fees_affiliation_courses AS b,
										academic_terms AS c
									WHERE
										a.id = b.fees_affiliation_id
										AND a.academic_terms_id = c.id
										AND c.academic_years_id = {$this->db->escape($academic_years_id)}
									GROUP BY
										b.courses_id
								)
				ORDER BY
					a.course_code ";
	
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
	
	}
	
	//ADDED: 8/18/13 by genes
	/**
	 * This function adds a new laboratory fee 
	 * @param array $fees_data (contains details of the new laboratory fee
	 * @return Ambigous <boolean, unknown>
	 */
	function AddLaboratory_Fees($fees_data) {
		$query = "
				INSERT INTO
					laboratory_fees (
							academic_years_id,
							levels_id,
							courses_id,
							rate,
							posted,
							inserted_by,
							inserted_on
						)
					VALUES (
							{$this->db->escape($fees_data['academic_years_id'])},
							{$this->db->escape($fees_data['levels_id'])},
							{$this->db->escape($fees_data['courses_id'])},
							{$this->db->escape($fees_data['rate'])},
							{$this->db->escape($fees_data['posted'])},
							{$this->session->userdata('empno')},
							now() 
						)
				";
						
		$result = $this->db->query($query);	
		$my_last_id = $this->db->insert_id();
		return $my_last_id ? $my_last_id : false;
	}
	

	//ADDED: 8/20/13 by genes
	/**
	 * This function adds a new row to other tuition fees
	 * @param array $fees_data (contains details of the new other tuition fee
	 * @return Ambigous <boolean, unknown>
	 */
	function AddOther_Tuition_Fees($fees_data) {	
		$query = "
				INSERT INTO
					tuition_others (
						academic_terms_id,
						levels_id,
						yr_level,
						courses_id,
						rate,
						posted,
						inserted_by,
						inserted_on
					)
				VALUES (
						{$this->db->escape($fees_data['academic_terms_id'])},
						{$this->db->escape($fees_data['levels_id'])},
						{$this->db->escape($fees_data['yr_level'])},
						{$this->db->escape($fees_data['courses_id'])},
						{$this->db->escape($fees_data['rate'])},
						{$this->db->escape($fees_data['posted'])},
						{$this->session->userdata('empno')}, 
						now()
				)
			";			
		$result = $this->db->query($query);
		$my_last_id = $this->db->insert_id();
		return $my_last_id ? $my_last_id : false;
	}

	
	//Added: 9/7/2013
	/**
	 * This function retrieves the rate of a given laboratory course
	 * @param mediumint $course_id (represents the laboratory course)
	 * @param smallint $current_academic_year_id (represents the current academic year)
	 * @return NULL
	 */
	function getLabFees($course_id, $current_academic_year_id) {
		$result=NULL;
		$sql = "
			SELECT
				rate
			FROM
				laboratory_fees
			WHERE 
				academic_years_id = {$this->db->escape($current_academic_year_id)}
				AND courses_id = {$this->db->escape($course_id)}
		";

		$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
		return $result;
	}

	/*
	 * ADDED: 6/26/15 by genes
	 */
	function get_last_weight() {
		
		$result=NULL;
		
		$sql = "SELECT
					weight
				FROM
					fees_groups
				ORDER BY
					weight DESC
				LIMIT 1	";
	
		$query = $this->db->query($sql);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
	}
	
	/**
	 * This function retrieves all courses with laboratory fees
	 * @param smallint $academic_year_id (represents the academic year
	 * @return NULL
	 */
	function ListCourses_with_Lab_Fees($academic_year_id) {
		$result = NULL;
		
		$q = "SELECT
						a.id,
						a.course_code,
						a.descriptive_title,
						FORMAT(b.rate,2) AS rate
					FROM
						courses AS a,
						laboratory_fees AS b
					WHERE
						a.id=b.courses_id
						AND b.academic_years_id = {$this->db->escape($academic_year_id)}
					ORDER BY
						a.course_code ";
		
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
		
	}
	
	
	/*
	 * Added: 10/29/2014
	 * by: genes
	 */
	function DeleteFees_Schedule_Prospectus($prospectus_courses_id) {
		$query = "DELETE FROM
						fees_schedule_prospectus_courses
					WHERE
						prospectus_courses_id = {$this->db->escape($prospectus_courses_id)} ";
			
		//print($query); die();
		
		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}

	/*
	 * ADDED: 6/26/15 by genes
	 */
	function Delete_Subgroup($id) {
		$q = "DELETE FROM
					fees_subgroups
				WHERE
					id = {$this->db->escape($id)} ";
		
		if ($this->db->query($q)) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
	//ADDED: 6/25/15 by genes
	function Add_Fees_Group($data) {
		
		$query = "INSERT INTO
					fees_groups 
						(fees_group,
						weight,
						inserted_by,
						inserted_on)
					VALUES (
						{$this->db->escape($data['fees_group'])},
						{$this->db->escape($data['weight'])},
						{$this->session->userdata('empno')},
						now()
				) ";
	//die($query);
		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
	
	}
	
	//ADDED: 6/25/15 by genes
	function Add_Fees_SubGroup($data) {
		
		$query = "INSERT INTO
					fees_subgroups 
						(fees_groups_id,
						description,
						inserted_by,
						inserted_on,
						remark)
					VALUES (
						{$this->db->escape($data['fees_groups_id'])},
						{$this->db->escape($data['description'])},
						{$this->session->userdata('empno')},
						now(),
						{$this->db->escape($data['remark'])}
				) ";

		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
	
	}
	
	//ADDED: 6/25/15 by genes
	function AddAffiliation_Fees($data) {	
		$query = "
				INSERT INTO
					fees_affiliation (
						academic_terms_id,
						description,
						rate,
						posted,
						inserted_by,
						inserted_on
					)
				VALUES (
						{$this->db->escape($data['academic_terms_id'])},
						{$this->db->escape($data['description'])},
						{$this->db->escape($data['rate'])},
						{$this->db->escape($data['posted'])},
						{$this->session->userdata('empno')}, 
						now()
				)
			";	

		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}
	
	//ADDED: 6/25/15 by genes
	function AssignAffiliation_to_Course($data) {
		
		$query = "INSERT INTO
						fees_affiliation_courses (
							fees_affiliation_id,
							courses_id)
					VALUES (
						{$this->db->escape($data['fees_affiliation_id'])},
						{$this->db->escape($data['courses_id'])}
					) ";
	
		if ($this->db->query($query)) {
			return TRUE;
		} else {
			return FALSE;
		}
	
	}
	
	/*
	 * ADDED: 6/11/15 by genes
	 * MODIFIED: 6/25/15 by genes
	 * New Additional fee for affiliated fees
	 */
	function ListStudentAffiliated_Fees($student_history_id) {
		$result = NULL;
		
		$q = "SELECT
						a.description,
						a.rate
					FROM
						fees_affiliation AS a,
						fees_affiliation_courses AS f,
						enrollments AS c,
						course_offerings AS d,
						student_histories AS e
					WHERE
						a.id=f.fees_affiliation_id
						AND f.courses_id=d.courses_id
						AND c.course_offerings_id = d.id
						AND e.academic_terms_id=d.academic_terms_id
						AND a.academic_terms_id=e.academic_terms_id
						AND e.id = c.student_history_id
						AND e.id = {$this->db->escape($student_history_id)} 
					GROUP BY
						a.description,
						a.rate 
				";
		
		//die($q);
		
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
		
	}
}
?>
