<?php

class Ric extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->content_lib->set_title ('Evaluator | ' . $this->config->item('application_title'));
		$this->navbar_data['menu'] = array(
				'Student'	=>'student',
				'Faculty' => 'faculty',
				'Course Offerings'=>'offerings',
				);
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		$this->load->library('form_validation');
	}
	
	public function student(){
		
		$this->load->model('hnumis/Student_Model');
		$this->load->model('hnumis/Prospectus_Model');
		$this->load->model('hnumis/Offerings_Model');
		$this->load->helper('student_helper');
		
		$this->content_lib->set_title('Ric | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		
		if ( ! empty($query)){
			
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
			
			$this->load->library('pagination_lib');
			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				echo "No result found for that query";
			}
		}
		
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/Programs_Model');
			$this->load->model('hnumis/Courses_Model');
			$this->load->model('hnumis/Enrollments_model');
			$this->load->model('psychometrician_model');
			$this->load->model('hnumis/Student_model');
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/Requirements_Model');
			$this->load->model('hnumis/OtherSchools_Model');
			$this->load->model('accounts/fees_schedule_model');
			$this->load->model('financials/Tuition_Model');
			$this->load->library('tab_lib');
			
			//We're going to use the datepicker plugin...
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');
			$this->content_lib->enqueue_header_style('qtip');
			$this->content_lib->enqueue_footer_script('qtip');
			
			$active_tab = false;			
			//prepare contents...
			$current_term = $this->Academic_terms_model->current_academic_term();
			$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_term->id);
			$grades = $this->Enrollments_model->student_grades($idnum);
			
			$student_academic_terms = $this->Academic_terms_model->student_inclusive_academic_terms ($idnum);
			
			$all_exams = $this->psychometrician_model->all_exams(FALSE);
			$student_exams = $this->psychometrician_model->student_exams($idnum);
			
			$student_exams_id_arrays = array();
			if ($student_exams && count($student_exams) > 0) {
				foreach ($student_exams as $a){
					$student_exams_id_arrays[] = $a->id;
				}
			}
			
			//Let us perform activities first specified by $action
			//We're going to do this first before outputting the student profile
			//Note: enqueue the tab contents here... but don't feed that yet to the body.
			$result = $this->student_common_model->my_information($idnum);
				
			switch ($this->input->post('action')){
				
				case 'generate_class_schedule':
									//print_r($this->input->post());  die();
									$this->load->model("hnumis/Reports_model");
								
									$student['idno']= $result->idno;
									$student['fname']= $result->fname;
									$student['lname']= $result->lname;
									$student['mname']= $result->mname;
									$student['yr_level']= $result->year_level;
									$student['abbreviation']= $result->abbreviation;
									$student['max_bracket_units']= $this->input->post('max_units');
									$student['student_histories_id']= $this->input->post('student_histories_id');
								
									$selected_term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
								
									$this->Reports_model->generate_student_class_schedule_pdf($this->input->post('academic_terms_id'), $student, $selected_term);
								
									return;
				case 'add_requirement':
									if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
									
										$req['idno']= $idnum;
										$req['date'] = $this->input->post('date_submitted');
										$req['req_id'] = $this->input->post('req_id');
										$req['empno'] = $this->session->userdata('empno');
										
									
										if ($this->Requirements_Model->add_requirement($req))
											$this->content_lib->set_message("Requirement successfully added!", "alert-success"); else
											$this->content_lib->set_message("Error found while adding a requirement!", "alert-error");
									} else {
										$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
										$this->index();
									}
									break; 
				case 'delete_requirement':
									if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
										$req_id = $this->input->post('req_id');
									
										if ($this->Requirements_Model->delete_requirement($req_id))
												$this->content_lib->set_message("Requirement successfully removed!", "alert-success"); else
												$this->content_lib->set_message("Error found while removing a requirement!", "alert-error");
									
									} else {
											$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
											$this->index();
									}
									break;
									
				case 'add_other_school_details': //adding of school TOR

					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
												
						$academicYearId = $this->input->post('academic_years_id');
						$studentIdno= $idnum;
						$termId = $this->input->post('term_id');
						$schoolId = $this->input->post('schools_id');
						$program = sanitize_text_field($this->input->post('program'));
														
						if ($this->OtherSchools_Model->insertOtherSchoolsInfo($academicYearId, $studentIdno, $termId, $schoolId, $program))
							$this->content_lib->set_message("Other Schools details successfully added!", "alert-success"); else
							$this->content_lib->set_message("Error found while adding Other Schools details!", "alert-error");
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
					break;
						
				case 'add_other_school_course':
					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
											$otherSchoolenrollId = $this->input->post('other_school_enroll_id');
											
											$catalogNo= sanitize_text_field($this->input->post('catalog_no'));
											$descriptiveTitle = sanitize_text_field($this->input->post('desc_title'));
											$units = sanitize_text_field($this->input->post('units'));
											$finalGrade = sanitize_text_field($this->input->post('fin_grade'));
											$remarks = sanitize_text_field($this->input->post('remarks'));
											//print_r($catalogNo."<br>".$descriptiveTitle."<br>".$units."<br>".$finalGrade."<br>".$remarks); die();																
											if ($this->OtherSchools_Model->insertOtherSchoolsCourse($otherSchoolenrollId, $catalogNo, $descriptiveTitle, $units, $finalGrade, $remarks))
												$this->content_lib->set_message("Other Schools course successfully added!", "alert-success"); else
												$this->content_lib->set_message("Error found while adding Other Schools course !", "alert-error");
					} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							$this->index();
					}
					
					break;
					
					
				case 'delete_other_school_course':
											
						if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
							if ($this->OtherSchools_Model->DeleteOtherSchoolCourse($this->input->post('course_id'))) {
								$this->content_lib->set_message("Other school course successfully removed!", "alert-success");
							} else {
								$this->content_lib->set_message("Error found while removing other school course!", "alert-error");
							}
						} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							$this->index();
						}
					
						break;
						
				case 'edit_other_school_course':
						if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
							$otherSchoolenrollId = $this->input->post('other_school_course_id');
											$catalogNo= sanitize_text_field($this->input->post('catalaog_no'));
											$descriptiveTitle = sanitize_text_field($this->input->post('descriptive_title'));
											$units = sanitize_text_field($this->input->post('units'));
											$finalGrade = sanitize_text_field($this->input->post('fgrade'));
											$remarks = sanitize_text_field($this->input->post('remarks'));
								
							if ($this->OtherSchools_Model->updatetOtherSchoolsCourse($otherSchoolCourseId, $catalogNo, $descriptiveTitle, $units, $finalGrade, $remarks))
								$this->content_lib->set_message("Other Schools course successfully added!", "alert-success"); else
								$this->content_lib->set_message("Error found while adding Other Schools course!", "alert-error");
						} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							$this->index();
						}
							
						break;
				
				case 'delete_school_tor':
					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
									if ($this->OtherSchools_Model->DeleteOtherSchoolTOR($this->input->post('other_school_enrollments_id'))) {
										$this->content_lib->set_message("Other Schools TOR successfully removed!", "alert-success"); 
									} else {
										$this->content_lib->set_message("Error found while removing Other Schools TOR!", "alert-error");
									}	
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
										
					break;
					
				case 'generate_tor_pdf': //generates TOR in PDF form

					$this->load->model("hnumis/Reports_Model");
					
					$tor['remarks'] = $this->input->post('remarks');
					$tor['grant_status'] = $this->input->post('grant_status');
					
					$result->employee = $this->session->userdata('fname')." ".$this->session->userdata('lname');
					$this->Reports_Model->Generate_TOR_PDF($result,$tor);
					
					return;

				case 'add_graduated_student':
					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
						$student['taken_type'] = $this->input->post('taken_type');
						$student['student_histories_id'] = $this->input->post('student_histories_id');
						$student['other_schools_id'] = $this->input->post('other_schools_id');
							
						$student['graduation_statement'] = sanitize_text_field($this->input->post('graduated_statement'));

							//print_r($student); die();
						if ($this->Student_Model->AddStudentGraduated($student)) 
							$this->content_lib->set_message("Other Schools course successfully added!", "alert-success"); else
							$this->content_lib->set_message("Error found while adding Other Schools course !", "alert-error");
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
							
					break;
							
				default:		
						
					break;
					
			   }	
				$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $current_term->id);
								
				if ($selected_history) {
					$student_units = $this->Student_model->getUnits($selected_history, $current_term->term);
					$student_history_id=$selected_history->id;
				} else {
					$student_units['max_bracket_units']=0;
					$student_history_id=0;
				}
						
				$acad_term = $this->AcademicYears_Model->getAcademicTerms($current_term->id);
				$tab = $this->process_student_activities($idnum);
								
				$action = $this->input->post('action');
				if ($action=='schedule_current_term') {
					$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $this->input->post('academic_term'));
					$current_term = $this->Academic_terms_model->current_academic_term($this->input->post('academic_term'));
					$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $this->input->post('academic_term'));
					$tab['term'] = $current_term->term . " SY " . $current_term->sy;
					$tab['tab'] = 'schedule';
					$selected_term = $this->input->post('academic_term');
				} else {
					$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_term->id);
					$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $current_term->id);
					$selected_term = '';
				}
				$tab['idnumber'] = $idnum;
				$tab['courses'] = $courses;
				$tab['tab'] = 'sif';

				//print_r($tab); die();
				$this->tab_lib->set_tab_container_name('student_tab');
								
								$this->tab_lib->enqueue_tab(
										'Basic Info',
										'dric/student_information_form',
										$tab,
										'sif',
										($tab['tab'] =='sif' || empty($tab['tab']) ? TRUE : FALSE)
								);
								$this->tab_lib->enqueue_tab('Other Info',
										'',
										array(),
										'information',
										($tab['tab'] =='sia' || $tab['tab'] =='family_info' || $tab['tab'] =='educ_info' || $tab['tab'] =='suporting_persons' || $tab['tab'] =='emergency_info' || empty($tab['tab']) ? TRUE : FALSE)
								);
								
								$this->tab_lib->enqueue_tab(
										'Address Info',
										'dric/student_info_addresses',
										$tab,
										'sia',
										($tab['tab'] =='sia' ? TRUE : FALSE),
										'information'
								);
								//print_r($tab);die();
								$this->tab_lib->enqueue_tab(
										'Family Info',
										'dric/family_information',
										$tab,
										'family_info',
										($tab['tab'] =='family_info' ? true : false),
										'information'
								);
								$this->tab_lib->enqueue_tab(
										'Educational Info',
										'dric/educational_information',
										$tab,
										'educ_info',
										($tab['tab'] =='educ_info' ? TRUE : FALSE),
										'information'
								);
								$this->tab_lib->enqueue_tab(
										'Support of Education',
										'dric/supporting_persons_information',
										$tab,
										'supporting_persons',
										($tab['tab'] =='supporting_persons' ? TRUE : FALSE),
										'information'
								);
								$this->tab_lib->enqueue_tab(
										'Emergency Contact Info',
										'dric/emergency_contact',
										$tab,
										'emergency_info',
										($tab['tab'] =='emergency_info' ? TRUE : FALSE),
										'information'
								);
								
														
								$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', array('schedules'=>$courses,
										'academic_terms'=>'',
										'assessment_date'=>$assessment_date,
										'academic_terms_id'=>$current_term->id,
										'max_units'=>$student_units['max_bracket_units'],
										'student_histories_id'=>$student_history_id), 'schedule', FALSE);
								
								/*$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', array('schedules'=>$courses,
										'academic_terms_id'=>$current_term->id,
										'max_units'=>$student_units['max_bracket_units'],					
										'student_histories_id'=>$student_history_id), 'schedule', FALSE);
								*/
								$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', FALSE);
							
								if (is_college_student($idnum)){

									$result = $this->student_common_model->my_information($idnum);	

									$prospectus = $this->Prospectus_Model->student_prospectus($this->Prospectus_Model->student_prospectus_ids($result->idno));
									$prospectus_history = $this->Prospectus_Model->ListProspectusTerms($result->prospectus_id);
										
									//Added: Feb. 13, 2013 by Amie
									//$this->tab_lib->enqueue_tab ('Prospectus', 'student/viewprospectus', array('student'=>$dcontent, 'prospectus'=>$prospectus,
									//								'prospectus_id'=>$prospectus_id), 'prospectus', FALSE);
								
									$this->tab_lib->enqueue_tab('Prospectus',
											'',
											array(),
											'prospectus',
											FALSE);
								
									$this->tab_lib->enqueue_tab ('My Prospectus', 'student/prospectus', array('prospectus'=>$prospectus), 'prospectus', FALSE, 'prospectus');
									$this->tab_lib->enqueue_tab ('Prospectus History', 'student/prospectus_history', 
											array('prospectus_terms'=>$prospectus_history,
											'student_idno'=>$result->idno), 
											'prospectus_history_tab', FALSE, 'prospectus');
									
									//Added: November 12, 2013 by Amie
									/*
									$credited_courses =  $this->Student_Model->ListCreditedCourses($idnum);
										
									$this->tab_lib->enqueue_tab ('Credited Courses', 'student/list_credited_courses',
											array('credited_courses'=>$credited_courses), 'list_credited_courses',
											FALSE,'prospectus');
									*/
									//Added: October 28, 2013 by Amie
									$programs_taken =  $this->Student_Model->ListPastPrograms($idnum);
									//print_r($programs_taken);
									//die();
									$this->tab_lib->enqueue_tab ('Program History', 'student/list_programs_taken',
											array('programs_taken'=>$programs_taken), 'list_programs_taken',
											FALSE,'prospectus');
										
									$acad_id = $this->Prospectus_Model->getProspectus($result->prospectus_id);
									//print_r($result); die();
									//$this->userinfo['max_yr_level']=$this->Programs_Model->getProgram($acad_id->academic_programs_id);
									$current = $this->AcademicYears_Model->getCurrentAcademicTerm();
									$student['prospectus_id'] = $result->prospectus_id;
									$student['yr_level'] = $result->year_level;
									$student['idno'] = $result->idno;
									$student['max_yr_level'] = $result->max_yr_level;
										
									$data = $this->Student_Model->AssessYearLevel($student);
									
									$this->tab_lib->enqueue_tab ('Year Level Assessment', 'student/yr_level_assessment', array('current'=>$current, 'data'=>$data, 'current_yr_level'=>$this->ordinalSuffix($result->year_level)), 'assessment', FALSE, 'prospectus');

																		
									//Added: January 3, 2014 by Isah
									//The following allows the user to encode, courses taken from other schools
									$academic_years = $this->AcademicYears_Model->ListAcadYears();
									$academic_terms =  $this->OtherSchools_Model->listTermsOther();
									$other_schools = $this->OtherSchools_Model->ListStudentOtherSchools($idnum);
									$country_groups = $this->OtherSchools_Model->listCountries();
									$province_groups = $this->OtherSchools_Model->listProvinces();
									$town_groups = $this->OtherSchools_Model->listTowns();
									$schools_list = $this->OtherSchools_Model->listOtherSchools();
									
									$student_tor = $this->Student_Model->List_Student_TOR($student);
									
									$this->tab_lib->enqueue_tab('TOR',
											'',
											array(),
											'tor',
											FALSE);
									
									$this->tab_lib->enqueue_tab ('Other Schools', 'ric/list_other_schools_enrollments',
											array('other_schools'=>$other_schools, 'academic_years'=>$academic_years, 'academic_terms'=>$academic_terms,
													'province_groups'=>$province_groups, 'country_groups'=>$country_groups,
													'town_groups'=>$town_groups, 'schools_list'=>$schools_list),
											 		'list_other_schools_enrollments', $active_tab, 'tor');
																		
									$hnu_graduated = $this->Prospectus_Model->ListStudentSY($idnum);
									$other_graduated = $this->OtherSchools_Model->ListStudentSY($idnum);
									
									$this->tab_lib->enqueue_tab ('Display TOR', 'ric/display_tor',
											array('student_tor'=>$student_tor,
													'hnu_graduated'=>$hnu_graduated,
													'other_graduated'=>$other_graduated,
													),
											 		'display_tor', FALSE, 'tor');
											 		
								}
								
								$tab_content = $this->tab_lib->content();
								
			
			//let's prepare to output the student's profile...
			$result = $this->student_common_model->my_information($idnum);
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->mname. " " . $result->lname,
						'course'	=> $result->abbreviation,
						'level'		=> $result->year_level,
						'full_home_address'	=> $result->full_home_address,
						'full_city_address' => $result->full_city_address,
						'phone_number' 		=> $result->phone_number,
						'section' 			=> $result->section,
				);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				
				//We feed the profile to the body...
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				
				//Then we feed the tab content to the body...
				$this->content_lib->enqueue_body_content("", $tab_content);
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				echo "No student found with that ID Number...";
			}
		}
		
		//what will happen if there is no query... and the result is not numeric?
		$this->content_lib->content();		
	}


	public function faculty(){
		$this->load->library('faculty_lib');
		$this->faculty_lib->faculty(TRUE);
	}
	
	
	
	public function download_student_list_csv (){
		$year_level = $this->input->post('year_level');
		$program = $this->input->post('program');
	
		$this->load->model('student_common_model');
		$records = $this->student_common_model->search_by_program ($program, $year_level, TRUE);
		$filename = $records[0]->abbr . "_" . $year_level . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
		fputcsv($output, array('No.', 'Name', 'Gender', 'Course and Year Level'));
		$count = 0;
		foreach ($records as $record){
			$count++;
			fputcsv ($output, array($count, $record->fullname, $record->gender, $record->abbr . " ". $record->year_level));
		}
	}
	
	
	private function ordinalSuffix( $n )
	{
		return $n.date('S',mktime(1,1,1,1,( (($n>=10)+($n>=20)+($n==0))*10 + $n%10) ));
	}
	

	private function process_student_activities($idnum){
	
		//added: 01/07/2013:
		$this->load->model('common_model');
		$this->load->model('Places_model');
		$religions = $this->common_model->religions();
		$citizenships = $this->common_model->citizenships();
		$countries = $this->Places_model->countries();
		$philippine_id = $this->Places_model->country_id('Philippines');
		$provinces = $this->Places_model->fetch_results($philippine_id, 'provinces');
	
		$this->load->model('student_common_model');
	
		switch ($this->input->post('action')){
			case 'add_college_history'		:
				if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					$this->load->model('hnumis/Student_model');
					$this->load->model('hnumis/Prospectus_model');
					$program_id = $this->input->post('program');
					$prospectus_id = ($this->Prospectus_model->prospectus_from_program($program_id) ? $this->Prospectus_model->prospectus_from_program($program_id) : NULL);
	
					$history = $this->Student_model->insert_student_history($idnum, $prospectus_id, 0, 1, FALSE); //last should be FALSE and will only be TRUE once paid...
					if ($history){
						$return1 = array('tab'=>'sif', 'message'=>"Successfully moved student to college.", 'severity'=>'alert-success');
					} else {
						$return1 = array('tab'=>'sif', 'message'=>"Error found while moving student to college.", 'severity'=>'alert-error');
					}
				} else {
					$return1 = array('tab'=>'sif', 'message'=>$this->config->item('nonce_error_message'), 'severity'=>'alert-error');
				}
				break;

			case 'update_student_information' :
	
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					//todo: server side validation...
					$data['fname'] = strip_tags($this->input->post('firstname', TRUE));
					$data['lname'] = strip_tags($this->input->post('familyname', TRUE));
					$data['mname'] = strip_tags($this->input->post('middlename', TRUE));
					$data['dbirth'] = $this->input->post('birthdate');
					$data['gender'] = $this->input->post('gender');
					$data['civil_status'] = $this->input->post('civil_status');
					$data['citizenships_id'] = $this->input->post('citizenship');
					$data['religions_id'] = $this->input->post('religion');
					$data['phone_number'] = $this->input->post('phone_number');
	
					if ($this->student_common_model->update_information ($idnum, $data))
						$return1 = array('tab'=>'sif', 'message'=>'Student information updated.', 'severity'=>'alert-success'); else
						$return1 = array('tab'=>'sif', 'message'=>'Error Found. Student information not updated.', 'severity'=>
								'alert-error');
				} else {
					$return1 = array('tab'=>'sif', 'message'=>'Nonce is invalid. Student information not updated', 'severity'=>
							'alert-error');
				}
				break;
			case 'update_student_address_information' :
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					$student_address = $this->student_common_model->student_address($idnum);
					//print_r($student_address);die();
					$data = array('students_idno' =>$idnum,);
	
					// Note: for addresses we're going to update only when the country is set...
					// Home Address...
					if ($this->input->post('home_address_country')){
						$data['home_address']		= sanitize_text_field($this->input->post('home_address'));
						$data['home_barangays_id']	= $this->input->post('home_address_barangay_id');
						$data['home_towns_id']		= $this->input->post('home_address_town');
						$data['home_provinces_id']	= $this->input->post('home_address_province');
						$data['home_countries_id']	= $this->input->post('home_address_country');
					}
	
					// Birth Place...
					if ($this->input->post('place_of_birth_country')){
						$data['birth_address']		= sanitize_text_field($this->input->post('place_of_birth_address'));
						$data['birth_barangays_id']	= 0;
						$data['birth_towns_id']		= $this->input->post('place_of_birth_town_id');
						$data['birth_provinces_id']	= $this->input->post('place_of_birth_province');
						$data['birth_countries_id']	= $this->input->post('place_of_birth_country');
					}
					// City Address...
					if ($this->input->post('city_address_barangay_id')){
						switch ($this->input->post('city_address_barangay_id')) {
							case 'none'	:
							case '0'	: 	$city_address = '';
							$city_address_country = '';
							$city_address_province = '';
							$city_address_town = '';
							$city_address_barangay = '';
							break;
							case 'same_as_home' :
								$city_address = ($this->input->post('home_address') ? $this->input->post('home_address') : isset($student_address->home_address)?$student_address->home_address:'' );
								$city_address_country = ($this->input->post('home_address_country') ? $this->input->post('home_address_country') : $student_address->home_countries_id);
								$city_address_province = ($this->input->post('home_address_province') ? $this->input->post('home_address_province') : $student_address->home_provinces_id);
								$city_address_town = ($this->input->post('home_address_town') ? $this->input->post('home_address_town') : $student_address->home_towns_id);
								$city_address_barangay = ($this->input->post('home_address_barangay_id') ? $this->input->post('home_address_barangay_id') : ( isset($student_address->home_barangays_id) ? $student_address->home_barangays_id : ""));
								break;
							default:
								$city_address = sanitize_text_field($this->input->post('city_address'));
								$city_address_country = $this->input->post('city_address_country');
								$city_address_province = $this->input->post('city_address_province');
								$city_address_town = $this->input->post('city_address_town');
								$city_address_barangay = $this->input->post('city_address_barangay_id');
								break;
						}
						$data['city_address']		= $city_address;
						$data['city_barangays_id']	= $city_address_barangay;
						$data['city_towns_id']		= $city_address_town;
						$data['city_provinces_id']	= $city_address_province;
						$data['city_countries_id']	= $city_address_country;
					}
	
					//update stay in city field...
					$stay_with_update = $this->student_common_model->update_information ($idnum, array('stay_with'=>$this->input->post('stay_in_city')));
	
					if ($this->student_common_model->update_address($data) OR $stay_with_update)
						$return1 = array('tab'=>'sia', 'message'=>'Student Address information updated.', 'severity'=>'alert-success'); else
						$return1 = array('tab'=>'sia', 'message'=>'Student Address information NOT updated.', 'severity'=>'alert-error');
				} else {
					$return1 = array('tab'=>'sia', 'message'=>'Nonce is invalid. Address not updated', 'severity'=>'alert-error');
				}
				break;
			case 'update_student_educational_information' :
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					//print_r($this->input->post());die();
					//print_r($data);die();
					$my_information = $this->student_common_model->my_information($idnum);
					$meta = json_decode($my_information->meta, TRUE);
						
						
					//$data = array('students_idno' =>$idnum,);
					$meta['educational_background']=array(
							'Primary'		=>$this->input->post('Primary'),
							'Primary_sy'	=>$this->input->post('Primary_sy'),
							'Intermediate'  =>$this->input->post('Intermediate'),
							'Intermediate_sy'  =>$this->input->post('Intermediate_sy'),
							'Secondary'  =>$this->input->post('Secondary'),
							'Secondary_sy'  =>$this->input->post('Secondary_sy'),
					);
					/* 									[primary_school_year] => prim sy
					 [intermediate_school] => intermed
					[intermediate_school_year] => intermed sy
					[secondary_school] => seconda
					[secondary_school_year] => second sy
					[secondary_school_address_country] => 137
					[secondary_school_address_province] => 59
					[secondary_school_address_town] => 1199
					[secondary_school_address_barangay_id] => 32087
					[secondary_school_address] => 123
					[last_school] => if transferee
					[last_school_year] => transterrr sy
					)
					*/									//print_r($meta);die();
					$data['meta'] = json_encode($meta);
					//print_r($data);die();
					if ($this->student_common_model->update_information ($idnum, $data))
						$return1 = array('tab'=>'educ_info', 'message'=>'Educational Information Updated', 'severity'=>'alert-success');
					else
						$return1 = array('tab'=>'educ_info', 'message'=>'Error Updating Educational Information.', 'severity'=>'alert-error');
				} else {
					$return1 = array('tab'=>'educ_info', 'message'=>'Nonce Error. Updating Educational Information not done.', 'severity'=>'alert-error');
				}
				break;
			case 'update_supporting_information'			:
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					$supporting_information_name = $this->input->post('supporting_information_name');
					$supporting_information_occupation = $this->input->post('supporting_information_occupation');
					$supporting_information_relation = $this->input->post('supporting_information_relation');
					$supporting_information_address = $this->input->post('supporting_information_address');
	
					$my_information = $this->student_common_model->my_information($idnum);
					$meta = json_decode($my_information->meta, TRUE);
	
					$support_information = (isset($meta['support_information']) ? $meta['support_information'] : "");
	
					foreach ($supporting_information_name as $key => $val) {
						$support_information[$key] = array(
								'name' 			=> $supporting_information_name[$key],
								'occupation'	=> $supporting_information_occupation[$key],
								'relation'		=> $supporting_information_relation[$key],
								'address'		=> $supporting_information_address[$key],
						);
					}
	
					$meta['support_information'] = $support_information;
	
					$data = array('meta'=>json_encode($meta));
					if ($this->student_common_model->update_information ($idnum, $data))
						$return1 = array('tab'=>'supporting_persons', 'message'=>'Student Support information updated.', 'severity'=>'alert-success');
					else
						$return1 = array('tab'=>'supporting_persons', 'message'=>'Error Updating Student Support information.', 'severity'=>'alert-error');
				} else {
					$return1 = array('tab'=>'supporting_persons', 'message'=>'Nonce Error. Updating Student Support information not done.', 'severity'=>'alert-error');
				}
				break;
			case 'delete_support_information'		:
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					$id = (int)$this->input->post('id');
					$my_information = $this->student_common_model->my_information($idnum);
						
					$meta = json_decode($my_information->meta, TRUE);
					$support_information = $meta['support_information'];
	
					unset($support_information[$id]);
					$meta['support_information'] = $support_information;
	
					$data = array('meta'=>json_encode($meta));
					if ($this->student_common_model->update_information ($idnum, $data))
						$return1 = array('tab'=>'supporting_persons', 'message'=>'Student Support information deleted.' , 'severity'=>'alert-success'); else
						$return1 = array('tab'=>'supporting_persons', 'message'=>'Error Deleting Student Support information.' , 'severity'=>'alert-error');
				} else {
					$return1 = array('tab'=>'supporting_persons', 'message'=>'Nonce Error. Deleting Student Support information not done.' , 'severity'=>'alert-error');
				}
				break;
			case 'update_student_family_information' :
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					$fathers_name = strip_tags($this->input->post('fathers_name'));
					$fathers_number = strip_tags($this->input->post('fathers_number'));
					$mothers_name = strip_tags($this->input->post('mothers_name'));
					$mothers_number = strip_tags($this->input->post('mothers_number'));
					$num_brothers = (int)$this->input->post('num_brothers');
					$num_sisters = (int)$this->input->post('num_sisters');
	
					$student_address = $this->student_common_model->student_address($idnum);
	
					if ($this->input->post('fathers_address_country') == 'same_as_home') {
						$fathers_address_country = $student_address->home_countries_id;
						$fathers_address_province = $student_address->home_provinces_id;
						$fathers_address_town = $student_address->home_towns_id;
						$fathers_address_barangay = $student_address->home_barangays_id;
						$fathers_address = $student_address->home_address;
					} else {
						$fathers_address_country = (is_numeric($this->input->post('fathers_address_country')) ? $this->input->post('fathers_address_country') : FALSE);
						$fathers_address_province = (is_numeric($this->input->post('fathers_address_province')) ? $this->input->post('fathers_address_province') : FALSE);
						$fathers_address_town = (is_numeric($this->input->post('fathers_address_town')) ? $this->input->post('fathers_address_town') : FALSE);
						$fathers_address_barangay = (is_numeric($this->input->post('fathers_address_barangay_id')) ? $this->input->post('fathers_address_barangay_id') : FALSE);
						$fathers_address = $this->input->post('fathers_address');
					}
	
					if ($this->input->post('mothers_address_country') == 'same_as_home') {
						$mothers_address_country = $student_address->home_countries_id;
						$mothers_address_province = $student_address->home_provinces_id;
						$mothers_address_town = $student_address->home_towns_id;
						$mothers_address_barangay = $student_address->home_barangays_id;
						$mothers_address = $student_address->home_address;
					} else {
						$mothers_address_country = (is_numeric($this->input->post('mothers_address_country')) ? $this->input->post('mothers_address_country') : FALSE);
						$mothers_address_province = (is_numeric($this->input->post('mothers_address_province')) ? $this->input->post('mothers_address_province') : FALSE);
						$mothers_address_town = (is_numeric($this->input->post('mothers_address_town')) ? $this->input->post('mothers_address_town') : FALSE);
						$mothers_address_barangay = (is_numeric($this->input->post('mothers_address_barangay_id')) ? $this->input->post('mothers_address_barangay_id') : FALSE);
						$mothers_address = $this->input->post('mothers_address');
					}
	
					$this->load->model('places_model');
					$this->load->model('student_common_model');
	
					$my_information = $this->student_common_model->my_information($idnum);
					$meta = json_decode($my_information->meta, TRUE);
	
					//$meta['fathers_address'] = (isset($meta['fathers_address']) ? $meta['fathers_address'] : "");
					//$meta['mothers_address'] = (isset($meta['mothers_address']) ? $meta['mothers_address'] : "");
	
					$fathers_address = trim($fathers_address . "\n" . $this->places_model->parse_address($fathers_address_country, $fathers_address_province, $fathers_address_town, $fathers_address_barangay));
					$fathers_address = (! empty($fathers_address) ? $fathers_address : $meta['family_info']['fathers_address'] );
	
					$mothers_address = trim($mothers_address . "\n" . $this->places_model->parse_address($mothers_address_country, $mothers_address_province, $mothers_address_town, $mothers_address_barangay));
					$mothers_address = (! empty($mothers_address) ? $mothers_address : $meta['family_info']['mothers_address'] );
	
					$meta['family_info']['fathers_name'] = $fathers_name;
					$meta['family_info']['fathers_number'] = $fathers_number;
					$meta['family_info']['mothers_name'] = $mothers_name;
					$meta['family_info']['mothers_number'] = $mothers_number;
					$meta['family_info']['fathers_address'] =  $fathers_address;
					$meta['family_info']['mothers_address'] =  $mothers_address;
					$meta['family_info']['num_brothers'] = $num_brothers;
					$meta['family_info']['num_sisters'] = $num_sisters;
						
					//print_r($meta);die();
	
					$data = array('meta'=>json_encode($meta));
					//print_r($data);die();
					if ($this->student_common_model->update_information ($idnum, $data))
						$return1 = array('tab'=>'family_info', 'message'=>'Student Family information updated.', 'severity'=>'alert-success'); else
						$return1 = array('tab'=>'family_info', 'message'=>'Error Updating Student Family information.', 'severity'=>'alert-error');
	
				} else {
					$return1 = array('tab'=>'family_info', 'message'=>'Nonce error. Student family information NOT updated.', 'severity'=>'alert-error');
				}
				break;
			case 'update_student_emergency_information' :
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					$emergency_address_country = (is_numeric($this->input->post('emergency_address_country')) ? $this->input->post('emergency_address_country') : FALSE);
					$emergency_address_province = (is_numeric($this->input->post('emergency_address_province')) ? $this->input->post('emergency_address_province') : FALSE);
					$emergency_address_town = (is_numeric($this->input->post('emergency_address_town')) ? $this->input->post('emergency_address_town') : FALSE);
					$emergency_address_barangay = (is_numeric($this->input->post('emergency_address_barangay_id')) ? $this->input->post('emergency_address_barangay_id') : FALSE);
					$emergency_address = $this->input->post('emergency_address');
	
					$this->load->model('places_model');
					$this->load->model('student_common_model');
					$my_information = $this->student_common_model->my_information($idnum);
					$meta = json_decode($my_information->meta, TRUE);
					$emergency_address = trim($emergency_address . "\n" . $this->places_model->parse_address($emergency_address_country, $emergency_address_province, $emergency_address_town, $emergency_address_barangay));
					$emergency_address = (! empty($emergency_address) ? $emergency_address : $meta['emergency_address'] );
	
					$meta['emergency_address'] =  $emergency_address;
					$meta['emergency_notify'] = $this->input->post('emergency_notify', TRUE);
					$meta['emergency_telephone'] = $this->input->post('emergency_telephone', TRUE);
					$meta['emergency_email_address'] = $this->input->post('emergency_email_address', TRUE);
	
					$data = array('meta'=>json_encode($meta));
					if ($this->student_common_model->update_information ($idnum, $data))
						$return1 = array('tab'=>'emergency_info', 'message'=>'Student Emergency information updated.', 'severity'=>'alert-success'); else
						$return1 = array('tab'=>'emergency_info', 'message'=>'Error Updating Student Emergency information.', 'severity'=>'alert-error');
				} else {
					$return1 = array('tab'=>'emergency_info', 'message'=>'Nonce error. Student Emergency information NOT updated.', 'severity'=>'alert-error');
				}
				break;
			case 'edit_sif'			:
				$return1 = array('tab'=>'sif', 'message'=>'Student Information Updated', 'severity'=>'alert-success');
				break;
	
	

				//todo: what else can he do...
			default			:
	
				$return1 = array('tab'=>'sif');
				break;
		}
		$my_information = $this->student_common_model->my_information($idnum);
		//print_r($my_information);die();
	
		if ($my_information) {
			$meta = json_decode($my_information->meta, TRUE);
			//print_r($meta);die();
			$return = array(
					'firstname'		=>$my_information->fname,
					'familyname'	=>$my_information->lname,
					'middlename'	=>$my_information->mname,
					'course'		=>$my_information->abbreviation,
					'religions'		=>$religions,
					'citizenships'	=>$citizenships,
					'religions_id'	=>$my_information->religions_id,
					'citizenships_id'=>$my_information->citizenships_id,
					'birthdate'		=>$my_information->dbirth,
					'gender'		=>$my_information->gender,
					'civil_status'	=>$my_information->civil_status,
					'countries'		=>$countries,
					'place_of_birth'=>$my_information->full_birth_address,
					'philippine_id'	=>$philippine_id,
					'provinces'		=>$provinces,
					'home_address'	=>$my_information->full_home_address,
					'city_address'	=>$my_information->full_city_address,
					'level'			=>$my_information->year_level,
					'full_birth_address'	=> $my_information->full_birth_address,
					'full_city_address'		=> $my_information->full_city_address,
					'full_home_address'		=> $my_information->full_home_address,
					'stay_with'		=> $my_information->stay_with,
					'phone_number'	=> $my_information->phone_number,
					'section'		=> $my_information->section,
	
			);
			$return = array_merge ($return, $return1);
			if (is_array($meta)){
				$return = array_merge($return, $meta);
				//print_r($return);die();
			}
			//print_r($return);die();
			return $return;
	
		} else {
			return FALSE;
		}
	
	}
	public function offerings(){
		$this->content_lib->set_title ('Evaluator | Offerings | ' . $this->config->item('application_title'));
		$limit = array();
		$show_only = ($this->session->userdata('show_only') ? $this->session->userdata('show_only') : 'all');
		$show_college = ($this->session->userdata('show_college') ? $this->session->userdata('show_college') : '');
		$show_program = ($this->session->userdata('show_program') ? $this->session->userdata('show_program') : '');
		$current_academic_term = ($this->session->userdata('show_academic_term') ? $this->session->userdata('show_academic_term') : '');
		$limit = array('college'=>$show_college, 'program'=>$show_program);
	
		//process actions here...
		if ($action = $this->input->post('action')) {
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				switch ($action){
					case 'limit_results' :
						$limit = array(
						'college'=>$this->input->post('college'),
						'program'=>$this->input->post('programs'),
						);
						$show_college = $this->input->post('college');
						$show_program = $this->input->post('programs');
						$show_only = $this->input->post('offering_status');
						$show_only = ( ! empty($show_only) ? $show_only : 'all' );
						$this->session->set_userdata(array('show_only'=>$show_only));
						$this->session->set_userdata(array('show_college'=>$this->input->post('college')));
						$this->session->set_userdata(array('show_program'=>$this->input->post('programs')));
						break;
					case 'set_academic_term' :
						$current_academic_term = $this->input->post('academic_term');
						$this->session->set_userdata(array('show_academic_term'=>$current_academic_term));
						break;
				}
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}
		}
		$page = $this->input->post('page') ? $this->input->post('page') : 1;
		$this->load->model('hnumis/Courses_model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/College_model');
		$this->load->model('hnumis/AcademicYears_model');
	
		$academic_terms = $this->academic_terms_model->academic_terms(6);
		$colleges = $this->College_model->all_colleges();
	
		$current_term = ( ! empty($current_academic_term) ? $current_academic_term : $this->academic_terms_model->getCurrentAcademicTerm()->id);
	
		$courses_offered = $this->Courses_model->list_courses_with_limit($limit, $current_term, $show_only, $page, $this->config->item('results_to_show_per_page'));
		//$courses_offered = $this->Courses_model->list_courses_with_limit($limit, 163, $show_only, $page, $this->config->item('results_to_show_per_page'));
		$this->load->library('pagination_lib');
	
		$pagination = "";
		$total = $courses_offered['total_rows'];
		if (! empty($courses_offered)){
			$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
			$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
					? $total
					: ($start + (int)$this->config->item('results_to_show_per_page') - 1));
			$pagination = $this->pagination_lib->pagination('', $courses_offered['total_rows'], $page);
		} else {
			$start = 0;
			$page = 1;
			$end = 0;
		}
	
		$content = array(
				'courses_offered'=>$courses_offered['result'],
				'page'=>$page,
				'start'=>$start,
				'end'=>$end,
				'total'=>$courses_offered['total_rows'],
				'pagination'=> $pagination,
				'colleges'=>$colleges,
				'show_college'=>$show_college,
				'show_program'=>$show_program,
				'academic_terms'=>$academic_terms,
				'current_academic_term'=>$current_term,
		);
		$this->content_lib->enqueue_body_content('evaluator/course_offerings', $content);
		$this->content_lib->content();
	}
	
	public function download_offerings (){
	
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){
			$limit = array(
					'college'=>$this->input->post('college'),
					'program'=>$this->input->post('programs'),
			);
			$show_only = $this->input->post('offering_status');
			$show_only = ( ! empty($show_only) ? $show_only : 'all' );
	
			$this->load->model('hnumis/Courses_model');
			$this->load->model('academic_terms_model');
	
			$current_term = ($this->input->post('academic_term') ? $this->input->post('academic_term') : $this->academic_terms_model->getCurrentAcademicTerm()->id);
			$records = $this->Courses_model->list_courses_with_limit($limit, $current_term, $show_only, 1, 100000);
			$filename = "course_offerings.csv";
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=' . $filename);
	
			//create a file pointer connected to the output stream
			$output = fopen('php://output', 'w');
	
			// output the column headings
			fputcsv($output, array('Catalog ID', 'Section', 'Descriptive Title', 'Schedule', 'Room', 'Enrolled | Max', 'Teacher', 'Status'));
			//print_r($records);
			foreach ($records['result'] as $record){
				fputcsv ($output, array($record->course_code, $record->section_code, $record->descriptive_title, str_replace('<br />', ' ', $record->schedule), $record->room, $record->enrolled_count . ' | ' . $record->max_enrollment_count, $record->teacher, $record->status));
			}
		}
	}
	
	public function class_list(){
		if ( $this->input->post()){
			if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
				$this->load->model('hnumis/Enrollments_model');
				$students = $this->Enrollments_model->class_record($this->input->post('course_offerings_id'));
	
				$class = array(
						'catalog_id'=>($students ? $students[0]->course_code : $this->input->post('course_code')),
						'description'=>($students ? $students[0]->descriptive_title : $this->input->post('descriptive_title')),
						'section_code'=>($students ? $students[0]->section_code : $this->input->post('section')),
						'term'=>$this->input->post('term'),
						'teacher'=>$this->input->post('teacher'),
						'schedule'=>$this->input->post('schedule'),
						'students'=>$students,
				);
				$this->content_lib->enqueue_body_content('dric/class_list', array('course_offering'=>$class));
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}
		} else {
			$this->content_lib->set_message('You are not allowed to go directly to this link.', 'alert-error');
		}
		$this->content_lib->content();
	}
	
	public function alphalist(){
		$this->content_lib->set_title ('Evaluator | Alpha List | ' . $this->config->item('application_title'));
		$this->content_lib->content();
	}
	
	/****** *****/
	public function enrollment_summary() {
		$this->content_lib->set_title ('Evaluator | Enrollment Summary | ' . $this->config->item('application_title'));
		$this->load->model('hnumis/Reports_Model');
	
		$this->Reports_Model->enrollment_summary();
	
	}
	
	
	
}