<div style="padding-top:20px; padding-left:20px;">
<h2 class="heading">Accredited Programs</h2>

<table style = "width:70%;" id="offerings" class="table table-bordered table-condensed">
    <thead>
    <tr>
      <th width="20%" class="header" style="text-align:center; vertical-align:middle;">Program</th>
      <th width="15%" class="header" style="text-align:center; vertical-align:middle;">Accreditor</th>
       <th width="10%" class="header" style="text-align:center; vertical-align:middle;">Level</th>
       <th width="15%" class="header" style="text-align:center; vertical-align:middle;">Date Approved</th>
       <th width="15%" class="header" style="text-align:center; vertical-align:middle;">Start Date</th>
       <th width="15%" class="header" style="text-align:center; vertical-align:middle;">End Date</th>
      <th width="10%" class="header" style="text-align:center; vertical-align:middle;">Delete</th>
       </tr>
    </thead>
    <?php
	    if($accredited) { 
			foreach ($accredited AS $program) {
	?>
		<tr>
				 <td style="text-align:left; vertical-align:middle; "><?php print($program->abbreviation); ?></td>
				 <td style="text-align:center; vertical-align:middle; "><?php print($program->acronym); ?> </td>
				 <td style="text-align:center; vertical-align:middle; "><?php print($program->level); ?> </td>
				 <td style="text-align:center; vertical-align:middle; "><?php print($program->date_approved); ?> </td>
				 <td style="text-align:center; vertical-align:middle; "><?php print($program->start_date); ?> </td>
				 <td style="text-align:center; vertical-align:middle; "><?php print($program->end_date); ?> </td>
				 <td style="text-align:center; vertical-align:middle; "><i class="icon-trash"></i></td>
		</tr>
   	<?php
			}
	 } else {	
	?>
	<tr>
		 <td style="text-align:center; vertical-align:middle; "> No Grudation Schedule</td>
		 
	</tr>
<?php 
	}
?>	
</table>
</div>

