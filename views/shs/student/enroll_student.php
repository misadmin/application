<style>
	div#msg_header_enroll {
		border-radius: 5px; 
		background-color: #D7D4D4; 
		border: solid; 
		border-width: 1px; 
		border-color: #B7B6B6; 
		text-align:left; 
		padding-left: 10px;
		margin:0 auto;
		margin-bottom:20px;
		font-family: arial;
		font-size: 14px;
		width: 95%;
		height: 35px;
		line-height: 35px;
		font-weight:bold;
		color:#B50303;
</style>

<div class="row-fluid" >
	<div id="msg_header_enroll" style="display:none;">
	</div>
	
		<div style="padding-left:10px; width:99%;">
			<div style="float:left; padding-top:5px;">
				<h3>Student Payments</h3>
			</div>
		</div>
		<div style="width:99%; overflow:auto; margin:0 auto; padding-top:10px; height:200px; border:solid 1px #DBDBDB; ">
			<table class="table table-hover table-bordered table-striped" >
				<thead>
					<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
						<td class="shs_header" style="width:10%;">OR<br>Number</td>
						<td class="shs_header" style="width:15%;">Payment<br>Date</td>
						<td class="shs_header" style="width:30%;">Particulars</td>
						<td class="shs_header" style="width:15%;">Amount<br>Paid</td>
						<td class="shs_header" style="width:10%;">Status</td>
						<td class="shs_header" style="width:20%;">Teller</td>
					</tr>
				</thead>
				<tbody>
					<?php
						if ($payments) {
							foreach($payments AS $payment) {
					?>			
								<tr>
									<td style="text-align:center;"><?php print($payment->receipt_no); ?></td>
									<td style="text-align:center;"><?php print($payment->payment_date); ?></td>
									<td><?php print($payment->description); ?></td>
									<td style="text-align:right;"><?php print(number_format($payment->receipt_amount,2)); ?></td>
									<td style="text-align:center;"><?php print($payment->status); ?></td>
									<td><?php print($payment->emp_name); ?></td>
								</tr>
					<?php 
							}
						}
					?>
				</tbody>
			</table>
		</div>
		
		<div class="form-horizontal" style="margin-top:10px;">
			<form id="proceed_to_enroll_student_form" class="form-horizontal">
				<div class="control-group formSep">
					<label for="grade_level" class="control-label">Enroll For</label>
					<div class="controls">
						<select id="acad_terms_to_enroll_id" name="academic_terms_id" class="form-control" >
							<option value="0">--Select Term--</option>
							<?php 
								if ($terms_to_enroll) {
									foreach ($terms_to_enroll AS $term) {
							?>
									<option value="<?php print($term->student_histories_id); ?>" ><?php print($term->term.' '.$term->sy); ?></option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<div class="controls" style="float:left; padding-right:10px;">
						<input type="submit" class="btn btn-danger" name="proceed_to_enroll_student" id="proceed_to_enroll_student" value="Enroll Student!" disabled />
					</div>
					<div style="float:left; padding-right:8px; margin-top:5px;" id="show_enroll_icon">
					</div>		
				</div>
			</form>	
		</div>

</div>


<script>
	$('#proceed_to_enroll_student_form').submit(function(e) {
		e.preventDefault();		
		
		var student_histories_id = $('#acad_terms_to_enroll_id').val();
		var student_idno         = "<?php print($idnum); ?>";
		
		var confirmed = confirm('Are you sure you want to enroll the student?');

		if (confirmed){
		
			$('#show_enroll_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
					
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+student_idno,
				data: { student_histories_id: student_histories_id,
						action: 'enroll_student'},
				dataType: 'json',
				success: function(response) {				

					$('#show_extracted_items').html(response.output); 	
						
					$('h4.media-heading').html(response.student_info);
					
					$('#show_enroll_icon').html("");

					if (response.success) {
						$('#msg_header_enroll').show();
						$('#msg_header_enroll').html("Student has been enrolled successfully! Student can now be assigned to a section!");
					}
				}
			});
				
		}
	});
	
</script>	


<script>
$('#acad_terms_to_enroll_id').bind('click', function(event){
	event.preventDefault();

		var element       = $("option:selected", "#acad_terms_to_enroll_id");
		var acad_terms_id = element.val();

		if (acad_terms_id != 0) {
			$('#proceed_to_enroll_student').prop("disabled", false);
		} else {
			$('#proceed_to_enroll_student').prop("disabled", true);
		}
});
	
</script>


