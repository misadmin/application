		<?php 
				//use to print pdf for dissolve students
				$list_parallel_offerings = $this->Offerings_Model->ListParallel_Offerings($offerings->id,$offerings->parallel_no,FALSE);
				if (!$list_parallel_offerings) {
					$list_parallel_offerings=json_encode($offerings);	
				} else {
					$list_parallel_offerings=json_encode($list_parallel_offerings);
				}
				$list_dissolve_students=json_encode($students);
				$list_dissolve_students = preg_replace("/'/", "\&#39;", $list_dissolve_students);
				
				$offering_slots_dissolve=json_encode($offering_slots); 
				
		?>

<h2 class="heading">Course Offering</h2>
<?php
	$enrollees = 0;
	if ($offerings->enrollee_parallel) {
	  	 $enrollees = $offerings->enrollee_parallel;
	} else {
	  	 $enrollees = $offerings->enrollee;	
	}
?>
<h3><?php print($academic_terms->term." ".$academic_terms->sy); ?></h3>
<h2>
<?php print($offerings->course_code."[".$offerings->section_code."] - ".$offerings->descriptive_title); ?>
</h2>
<h4>
	<?php
		$offering_slots = $this->Offerings_Model->ListOfferingSlots($offerings->id,FALSE);
			foreach($offering_slots[0] AS $slots) {
				print("<span style='padding-right:5px;'>".$slots->tym."</span>");
				print("<span style='padding-right:10px;'>".$slots->days_day_code."</span>");
				print("<span style=\"color:green; font-weight:bold;\">Rm.".$slots->room_no."/".$slots->bldg_name."</span><br>");
			}
	?>
	Faculty: <?php print($offerings->employees_empno." ".$offerings->emp_name); ?>
</h4>

<div class="tabbable" style="padding-top: 20px;">
	<ul class="nav nav-tabs">
		<li <?php if ($active=="tab1") print("class=\"active\""); ?>><a href="#tab1" data-toggle="tab">Move Students to Other Schedule</a></li>
		<li <?php if ($active=="tab2") print("class=\"active\""); ?>><a href="#tab2" data-toggle="tab">Dissolve Offering</a></li>
	</ul>
			       
	<div class="tab-content">
		<div class="tab-pane <?php if ($active=="tab1") print("active"); ?>" id="tab1" style="padding-left:20px;">
 
 			<form id="transfer_form" method="post">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="step" value="move_students" />
				<input type="hidden" name="offering_id" value="<?php print($offerings->id); ?>" />
	
				<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; ">
 	
			  	<table class="table table-bordered table-hover" style="width:60%; padding:0px; margin:0px; margin-left:20px;">
				  	<thead>
					    <tr style="height:30px; background:#060; color:#FFF; font-weight:bold;">
						      <th width="7%" style="text-align:center"></th>
							  <th width="7%" style="text-align:center"></th>
							  <th width="15%" style="text-align:center">ID No.</th>
							  <th width="48%" style="text-align:center">Name</th>
						      <th width="18%" style="text-align:center">Course</th>
						      <th width="12%" style="text-align:center">Year</th>
					    </tr>
				    </thead>
			    	<?php
				    	$cnt = 1;
						if ($students) {
							foreach($students AS $enrollee) {
					?>
			    	<tr>
					      <td align="center" valign="top"><?php print($cnt.".");?></td>
						  <td><input type="checkbox" name="<?php print("newlist[$enrollee->stud_histories_id]"); ?>" value="Y" class="uncheck"/> </td>
						  <td style="text-align:center" valign="top"><?php print($enrollee->idno); ?></td>
					      <td><?php print($enrollee->neym); ?></td>
					      <td align="left" valign="top"><?php print($enrollee->abbreviation); ?></td>
					      <td style="text-align:center" valign="top"><?php print($enrollee->year_level); ?></td>
			    	</tr>
				    <?php
							$cnt = $cnt + 1;}
						}
					?>
			  	</table>
				</div>
				
				<div style="padding-top:20px; padding-left:20px;">
					<table>
					<tr>
						<td style="padding-right:10px;"><input type="submit" name="move" id="move" value="Move to>>"  class="btn btn-success" /></td>
						<td style="padding-top:10px;"><select name="section" id="new_offering_id" style="width: auto; ">
						<?php 
							foreach($new_offerings as $new_offering) {
								print("<option value= $new_offering->new_offering_id> ".$new_offering->course_code. " [ " .$new_offering->section_code." ] - ".$new_offering->start_time." - ". $new_offering->end_time." ". $new_offering->days_day_code." </option>");
							}
						?>
					</select></td>
					</tr>
					</table>
				</div>
			</form>
			
      	</div>
      	
		<div class="tab-pane <?php if ($active=="tab2") print("active"); ?>" id="tab2" style="padding-left:20px;">
				<a href="#" class="btn btn-info" id="download_affected_students" 
				dissolve_students='<?php print($list_dissolve_students); ?>'
				academic_term="<?php print($academic_terms->term." ".$academic_terms->sy); ?>"
				teacher="<?php print($offerings->employees_empno." ".$offerings->emp_name); ?>"
				parallel_offerings='<?php print($list_parallel_offerings); ?>'
				offering_slots='<?php print($offering_slots_dissolve); ?>'
				><i class="icon-arrow-down icon-white"></i> Download Affected Students</a> 
   			<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#dissolve_offering"><i class="icon-file icon-white"></i> List Affected Students</a>
		
 
 <!--  MODAL bodies    -->


<div class="modal hide fade" id="dissolve_offering">
	<form action="<?php echo site_url('registrar/all_offerings'); ?>" method="post" name="dissolve_offering" id="dissolve_section_form" />
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="dissolve_students" value='<?php print($list_dissolve_students); ?>'>
		<input type="hidden" name="academic_term" value="<?php print($academic_terms->term." ".$academic_terms->sy); ?>">
		<input type="hidden" name="teacher" value="<?php print($offerings->employees_empno." ".$offerings->emp_name); ?>">
		<input type="hidden" name="parallel_offerings" value='<?php print($list_parallel_offerings); ?>'>
		<input type="hidden" name="offering_slots" value='<?php print($offering_slots_dissolve); ?>'>
		<input type="hidden" name="step" value="dissolve_offering" />
	   <div class="modal-header">
	       <button type="button" class="close" data-dismiss="modal">�</button>
	       <h3>LIST OF STUDENTS</h3>
	   </div>
   <div class="modal-body">
		<table class="table table-striped table-hover" style="width:100%;">
			<thead>
				<tr style="font-weight:bold; background:#EAEAEA;">
					<th style="width:5%;">&nbsp;</th>
					<th style="width:10%; text-align:center; vertical-align:middle;">ID No.</th>
					<th style="width:60%; vertical-align:middle;">Name</th>
					<th style="width:20%; text-align:left; vertical-align:middle;">Course</th>
					<th style="width:5%; text-align:center;">Year<br>Level</th>					
				</tr>
			</thead>
   			<?php 
				if ($students) {
					$cnt=1;
					foreach($students AS $student) {
						print("<tr><td style=\"text-align:center;\">".$cnt."</td>");
						print("<td style=\"text-align:center;\">".$student->idno."</td>");
						print("<td>".$student->neym."</td>");
						print("<td>".$student->abbreviation."</td>");
						print("<td style=\"text-align:center;\">".$student->year_level."</td></tr>");
						$cnt++;
					}
				}
			?>
    </table>
  </div>
   <div class="modal-footer">
	   <input type="submit" name="button" id="dissolve_section" value="Dissolve Offering!" class="btn btn-danger" />
       <a href="#" class="btn" data-dismiss="modal">Close</a>
   </div>
   </form>
</div>

<!--   END OF MODAL BODIES -->
   			
   			
   		</div>
       	
   	</div>
</div>





<script>
	$('#dissolve_section').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to dissolve the section?');

		if (confirmed){
			$('#dissolve_section_form').submit();
		} 
		
	});
</script>


<form id="dissolve_students_form" action="<?php echo site_url("registrar/{$this->uri->segment(2)}")?>" method="post" target="_blank">
  <input type="hidden" name="step" value="download_dissolved_students" />
</form>
<script>
	$('#download_affected_students').click(function(event){
		event.preventDefault(); 
		var dissolve_students = $(this).attr('dissolve_students');
		var academic_term = $(this).attr('academic_term');
		var teacher = $(this).attr('teacher');
		var parallel_offerings = $(this).attr('parallel_offerings');
		var offering_slots = $(this).attr('offering_slots');

		$('<input>').attr({
			type: 'hidden',
			name: 'academic_term',
			value: academic_term,
		}).appendTo('#dissolve_students_form');		
		$('<input>').attr({
			type: 'hidden',
			name: 'teacher',
			value: teacher,
		}).appendTo('#dissolve_students_form');		
		$('<input>').attr({
			type: 'hidden',
			name: 'parallel_offerings',
			value: parallel_offerings,
		}).appendTo('#dissolve_students_form');			
		$('<input>').attr({
			type: 'hidden',
			name: 'offering_slots',
			value: offering_slots,
		}).appendTo('#dissolve_students_form');			
		$('<input>').attr({
		    type: 'hidden',
		    name: 'dissolve_students',
		    value: dissolve_students,
		}).appendTo('#dissolve_students_form');

		$('#dissolve_students_form').submit();
	});
</script>

