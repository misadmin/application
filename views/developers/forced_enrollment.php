<style>
	th.heads {
		text-align:center; 
		vertical-align:middle;
	}
	
	span#small {
		font-size: 15px;
		color: #A20101;
		font-weight: bold;
	}

</style>
<h2 class="heading">Forced Enrollment<br>
<span id="small">WARNING! This module does not check conflicts and allocations!</span></h2>
<div class="span10" style="width:95%; margin-left:0px;">

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">

<form id="change_sy" method="POST">
	    <select name="colleges_id" id="colleges_id" style="width:auto;">
				<option value="0" >All Colleges</option>
	    		<?php
					foreach($colleges AS $college) {
						if ($college->id == $selected_college) {
							print("<option value=\"$college->id\" selected>$college->college_code</option>");
						} else {
							print("<option value=\"$college->id\">$college->college_code</option>");
						}
					}
				?>
		</select>

</form>	
</div>


<table id="offerings" class="table table-bordered table-condensed">
    <thead>
    <tr>
      <th width="13%" class="head">Catalog ID [Section]</th>
      <th width="32%" class="head">Descriptive Title</th>
      <th width="5%" class="head">Credit Units</th>      
      <th width="20%" class="head">Time/Room</th>
      <th width="5%" class="head">Enrolled</th>
      <th width="20%" class="head">Faculty</th>
      <th width="5%" class="head">Status</th>
    </tr>
    </thead>
<?php
		if ($offerings) {
			foreach ($offerings AS $offer) {
				echo '<tr>';
				if ($offer->status == 'ACTIVE') {
					print("<td style='text-align:left; vertical-align:middle;'><a href='".$offer->id."' class='forced_enroll'>".$offer->course_code." [".trim($offer->section_code)."]</a></td>");
				} else {
					echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->course_code." [".trim($offer->section_code)."]";					
				}
				echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->descriptive_title.' '.$offer->status;
				echo '<td style="text-align:center; vertical-align:middle; ">' . $offer->credit_units;       
				
				$offering_slots = $this->Offerings_Model->ListOfferingSlots($offer->id);
				echo '<td style="text-align:left; vertical-align:middle; ">';
				if ($offering_slots) {
					foreach($offering_slots[0] AS $slots) {
						echo $slots->tym .' ' . $slots->days_day_code . ' ['.$slots->room_no.']<br>';
					}
					/*echo '<td style="text-align:left; vertical-align:middle; ">';
					foreach($offering_slots[0] AS $slots) {
						echo $slots->bldg_name !== 'Main' ? str_pad($slots->room_no, 4, "0", STR_PAD_LEFT): $slots->room_no ;
						echo '<br>';
					}*/
				} else {
					echo '<td style="text-align:center; vertical-align:middle; ">';
				}
				
      			echo '<td style="text-align:center; vertical-align:middle; ">';
				if ($offer->enrollee_parallel) { 
				 	 print($offer->enrollee_parallel."/".$offer->max_students); 
				} else { 
					print($offer->enrollee."/".$offer->max_students);  	  
				} 
				echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->employees_empno . ' ' . $offer->emp_name;

 				echo '<td style="text-align:center; vertical-align:middle; ">' . ucfirst(strtolower($offer->status)); 
			}
		}
?>
</table>
</div>

<form id="forced_enrollment_form" method="post">
  <input type="hidden" name="action" value="forced_enroll_student" />
  <?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$(document).ready(function(){
  
		$('.forced_enroll').click(function(event){
			event.preventDefault(); 
			var confirmed = confirm("Enroll this course?");

			if (confirmed){
				var course_offerings_id = $(this).attr('href');
		
				$('<input>').attr({
					type: 'hidden',
					name: 'course_offerings_id',
					value: course_offerings_id,
				}).appendTo('#forced_enrollment_form');
				
				$('#forced_enrollment_form').submit();
				
			}
			
		});

		
	});
</script>

<script>
	$(document).ready(function(){
		$('#colleges_id').bind('change', function(){
			show_loading_msg();
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'change_forced_college',
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>


<script>
$().ready(function() {
	$('#offerings').dataTable( {
	    "aoColumnDefs": [{ "bSearchable": false, "aTargets": [ 1,2,3,4,5,6 ] }],
		"iDisplayLength": 50,
	    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],	    
    });
    $("#offerings").stickyTableHeaders();
    $('#offerings').stickyTableHeaders('updateWidth');
	
	
})
</script>