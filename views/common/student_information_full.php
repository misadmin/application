<?php $my_meta = json_decode($meta, TRUE); 
//print_r($my_meta);die();
?>

<div class="row-fluid">
	<div class="span12" style="min-height: 300px;">
		<div class="row-fluid"><span class="pull-right"><select id="selector"></select></span></div>
		<div class="vcard clearfix">
			<ul id="basic_info" class="info">
<?php foreach ($this->config->item('student_basic_info') as $key=>$info_description): ?>
<?php if (isset($$key)): ?>
				<li>
					<span class="item-key"><?php echo $info_description; ?></span>
					<div class="vcard-item"><?php if($key=='gender') { $genders = $this->config->item('genders'); echo $genders[$$key];} else echo $$key; ?></div>
				</li>
<?php endif; ?>
<?php endforeach; ?>
			</ul>
			<ul id="address_info" class="info">
<?php foreach ($this->config->item('student_address_info') as $key=>$info_description): ?>
<?php if (isset($$key)): ?>
				<li>
					<span class="item-key"><?php echo $info_description; ?></span>
					<div class="vcard-item"><?php echo $$key; ?></div>
				</li>
<?php endif; ?>
<?php endforeach; ?>
			</ul>
			<ul id="family_info" class="info">
<?php foreach ($this->config->item('student_family_info') as $key=>$info_description): ?>
				<li>
					<span class="item-key"><?php echo $info_description; ?></span>
					<div class="vcard-item"><?php echo isset($my_meta['family_info'][$key]) ? nl2br(str_replace(array("\r\n\r\n\r\n", "\r\n\r\n"), "", $my_meta['family_info'][$key])) : ""; ?></div>
				</li>
<?php endforeach; ?>
			</ul>
			<ul id="supporting_education" class="info">
<?php if (isset($my_meta['support_information']) && is_array($my_meta['support_information'])): ?>
<?php foreach ($my_meta['support_information'] as $support_val): ?>
				<li class="v-heading"><?php echo $support_val['name']; ?></li>
<?php foreach ($this->config->item('student_support_info') as $key=>$info_description): ?>
<?php if (isset($support_val[$key])): ?>
				<li>
					<span class="item-key"><?php echo $info_description; ?></span>
					<div class="vcard-item"><?php echo $support_val[$key]; ?></div>
				</li>
<?php endif; ?>
<?php endforeach; ?>
<?php endforeach; ?>
<?php endif; ?>
			</ul>
			<ul id="emergency_info" class="info">
<?php foreach ($this->config->item('student_emergency_info') as $key=>$info_description): ?>
<?php if (isset($my_meta[$key])): ?>
				<li>
					<span class="item-key"><?php echo $info_description; ?></span>
					<div class="vcard-item"><?php echo $my_meta[$key]; ?></div>
				</li>
<?php endif; ?>
<?php endforeach; ?>
			</ul>

			<ul id="educational_background" class="info">
<?php if (isset($my_meta['educational_background']) && is_array($my_meta['educational_background'])):?>
<?php foreach ($my_meta['educational_background'] as $key=>$info_description): ?>
			<?php 
				if (substr($key,-2)!=='sy')
					echo '<li><span class="item-key">'.$key . ':</span><div class="vcard-item">' . $info_description.', ';
				else 
					echo $info_description . '</div></li>'; 
			?>
<?php endforeach; ?>
<?php endif; ?>
			</ul>
			
			
			
		</div>
	</div>
</div>
<script>
var info = <?php echo json_encode(array('basic_info'=>'Basic Information', 'address_info'=>"Address Information", 'family_info'=>"Family Information", 'supporting_education'=>'Persons Supporting Education', 'emergency_info'=>'Emergency Contact Information', 'educational_background'=>'Educational Background')) . ";\n"; ?>
var selector_content = '';

$.each(info, function(key, value){
	selector_content = selector_content + '<option value="' + key + '">' + value + '</option>';
});

$(document).ready(function(){
	$.each(info, function(key, value){
		$('#' + key).hide();
	});
	
	$('#basic_info').show();
	
	$('#selector').html(selector_content);
	
	$('#selector').live('change', function(){
		var target = $(this).val();
		
		$.each(info, function(key, value){
			$('#' + key).hide();
			console.log(key);
		});
		$('#' + target).show();
	});		
});
</script>
				