<div class="formSep" style = "margin-left:30px; margin-top:30px">
	<h2><?php print($title); ?></h2>
</div> 

<div style="text-align:center; width:100%;">
		
				
		<?php
			if ($transfer_cutOff_dates) {
		?>
			<div style="width:35%; margin-bottom:40px; margin-left:40px;">
			  <table id="section_list" border="0" cellspacing="0" class="table table-condensed table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
			    <thead>
			    <tr>
			      <th width="15%" style="vertical-align:middle; text-align:center;">School Year</th>
			      <th width="15%" style="vertical-align:middle; text-align:center;">Cut-Off Date</th>
			     
			   <!-- <th width="5%"  style="vertical-align:middle; text-align:center;">Delete</th>  -->
			       <th width="5%"  style="vertical-align:middle; text-align:center;">Edit</th>
			      </tr>
			    </thead>		
		<?php 
				foreach ($transfer_cutOff_dates AS $transfer_cutOff_date) {
		?>
		    <tr>
		      <td style="text-align:left; vertical-align:middle; "><?php print($transfer_cutOff_date->sy); ?> </td>
		      <td style="text-align:left; vertical-align:middle; "><?php print($transfer_cutOff_date->withdrawal_cutoff_date); ?> </td>
		      
		      <td style="text-align:center; vertical-align:middle; ">
			 		<a href="#" data-toggle="modal" data-target="#modal_edit_schedule_<?php print($transfer_cutOff_date->id); ?>" 
								data-project-id='<?php print($transfer_cutOff_date); ?>'><i class="icon-edit"></i></a>

		      
				  <!-- modal for EDITING enrollment schedules -->
				      
				      <div class="modal hide fade" id="modal_edit_schedule_<?php print($transfer_cutOff_date->id); ?>" >
				 		<form method="post" id="edit_schedule_form_<?php print($transfer_cutOff_date->id); ?>">
				   				<input type="hidden" name="action" value="update_cutOff_date" />
				   				 <input type="hidden" name="schedule_id" value= "<?php print($transfer_cutOff_date->id);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
				   		<div class="modal-header">
				       		<button type="button" class="close" data-dismiss="modal">�</button>
				       		<h3>Edit Cut-off Date</h3>
				   		</div>
				   		<div class="modal-body">
				 			<div style="text-align:center;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									
									<tr>
										<td>School Year</td>
										<td>:</td>
										<td> <?php print($transfer_cutOff_date->sy); ?> </td>
									</tr>
									
									<tr>
										<td>Start Date</td>
										<td>:</td>
										<td><input type="date" name="cutOffD" value="<?php print($transfer_cutOff_date->withdrawal_cutoff_date); ?>"></td>
									</tr>		
									
									
								</table>
							</div>
				   		</div>
					   <div class="modal-footer">
				    		<input type="submit" class="btn btn-primary" id="edit_schedule_<?php print($transfer_cutOff_date->id); ?>" value="EDIT DATE">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
						</form>
				    </div>		
				     <!-- End of EDITING a course from other schools -->
								
			</td>
		   </tr>
							
		<?php
				}
		?>
		</table>
		</div>
		<?php 		
			}
		?>
		 
</div>	

<form id="edit_sched" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="update_schedule" />
		
</form>			


<script>
	$('.edit_schedule').click(function(event){
		event.preventDefault(); 
	
		     var schedule_id = $(this).attr('href');
		
	  		$('<input>').attr({
			    type: 'hidden',
			    name: 'schedule_id',
			    value: schedule_id,
			}).appendTo('#edit_sched');
			$('#edit_sched').submit();
		
	});
</script>


<form id="delete_religion_form" method="post" >
	<input type="hidden" name="action" value="delete_religion" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>			

<script>
	$('.delete_religion').bind('click', function(event){
		event.preventDefault();
		var confirmed = confirm("Continue to delete religion?");
							
		if (confirmed){
			var id = $(this).attr('href');
								
			$('<input>').attr({
			    type: 'hidden',
			    name: 'religion_id',
			    value: id,
			}).appendTo('#delete_religion_form');
			$('#delete_religion_form').submit();
		}		
	});
</script>






<script>
$().ready(function() {
	$('#religion_list').dataTable( {
	    "aoColumnDefs": [{ "bSearchable": true, "aTargets": [ 1,2] }],
		"iDisplayLength": 50,
	    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],	    
    });
    $("#religion_list").stickyTableHeaders();
    $('#religion_list').stickyTableHeaders('updateWidth');
	
	
})
</script>
 
