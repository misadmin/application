<?php echo (isset($clearance) ? $clearance : '')?>
<?php echo (isset($midterm_invoice) ? $midterm_invoice : ''); ?>
<?php echo (isset($prefinal_invoice) ? $prefinal_invoice : ''); ?>
<?php if(!isset($withDivStart)) {
		 $withDivStart = 'YES'; //added by Toyet 7.21.2018
	  }
	  if(!isset($withDivEnd)) {
	  	 $withDivEnd = 'YES'; //added by Toyet 7.21.2018 
	  } ?>
<?php
if(in_array($this->session->userdata('role'), $this->config->item('roles_allowed_to_print_assessment'))):

//$name = $familyname . ", " . $firstname . " " . substr($middlename, 0, 1) . ".";
//$course_year = $level . " " . $course;

$table_size = array(
		'width'=>60,
		'height'=>count($tuition_fees_content) < 17 ? 17 : count($tuition_fees_content)
);
$length = array('fee_type'=>12, 'subject'=>20, 'units'=>6, 'hours'=>8, 'amount'=>12);
$numbers = array(0=>'units', 1=>'hours', 2=>'amount');

$tuition_fees_header = array(
		'fee_type'=>'Fee Type',
		'subject'=>'Subject',
		'units'=>'Units',
		'hours'=>'Hrs',
		'amount'=>'Amount'
);

$tuition_fees_content[] = array(
		'subject'=>'',
		'units'=>'____',
		'hours'=>'_____',
		'amount'=>'_________'
);

$tuition_fees_content[] = array(
		'subject'=>'Sub-total:',
		'units'=>0,
		'hours'=>0,
		'amount'=>0
);

$total_assessment = 0;

$tuition_fees = array();
$tuition_fees[] = '+' . str_pad('', $table_size['width'], '-') . '+';
$temp_head = '| ';
foreach($tuition_fees_header as $key=>$val){
	$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
	$temp_head .= str_pad($val, $length[$key], ' ', $str_pad);
}

$tuition_fees[] = str_pad($temp_head, $table_size['width']+1, ' ') . '|';
$tuition_fees[] = $tuition_fees[0];

$totals = array();

//log_message("INFO", print_r($tuition_fees_content, true)); // Toyet 6.6.2018

foreach($tuition_fees_content as $row=>$value){
	$output_total = FALSE;
	$i = count($tuition_fees);
	$tuition_fees[$i] = '| ';
	foreach($value as $key=>$val){

		//log_message("INFO",print_r($val,true)); // Toyet 6.28.2018

		if($key == 'subject' && $val == $tuition_fees_content[count($tuition_fees_content)-1][$key]) $output_total = TRUE;
		if($key == 'subject' && $val == ''){
			if(count($tuition_fees) < $table_size['height']-2){
				$tuition_fees[$i] .= str_pad('', $table_size['width']-1, ' ') . '|';
				for($c = count($tuition_fees); $c < $table_size['height']-3; $c++){
					$tuition_fees[] = '|' . str_pad('', $table_size['width'], ' ') . '|';
				}
				$i = $c;
				$tuition_fees[$i] = '| ';
			}
		}
		if($key == 'fee_type'){
			$tuition_fees[$i] .= str_pad($val, $table_size['width']-2, ' ');
		} else {
			$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
			if($key == 'subject') {
				$tuition_fees[$i] .= str_pad('', $length['fee_type'], ' ');
			}
			if(in_array($key, $numbers)){
				if(is_numeric($val)){
					$totals[$key] = isset($totals[$key]) ? $totals[$key] : 0;
					$totals[$key] += $val;
					$val = number_format($val, in_array($key, array('units')) ? 1 : 2);
				}
				if($output_total) {
					$val = number_format($totals[$key], in_array($key, array('units')) ? 1 : 2);
					if($key == 'amount') $total_assessment += $totals[$key];
				}

			}
			$tuition_fees[$i] .= str_pad(substr($val, 0, $length[$key]), $length[$key], ' ', $str_pad);
		}
	}
	$tuition_fees[$i] = str_pad($tuition_fees[$i], $table_size['width']+1, ' ') . '|';
}

$tuition_fees[] = $this->print_lib->condensed();
$tuition_fees[count($tuition_fees)-1] = $tuition_fees[0];

//print_r($tuition_fees);

//////////////////////////////////////////////////////////// TABLE 2 ////////////////////////////////////

//$table_size = array('width'=>60, 'height'=>16);
$length = array('fee_type'=>42, 'description'=>24, 'amount'=>12);
//$numbers = array(0=>'units', 1=>'hours', 2=>'amount');

$miscellaneous_fees_header = array(
		'fee_type'=>'Fee Type',
		'description'=>'Description',
		'amount'=>'Amount'
);

$miscellaneous_fees_content[] = array(
		'description'=>'',
		'amount'=>'_________'
);

$miscellaneous_fees_content[] = array(
		'description'=>'Sub-total:',
		'amount'=>0
);

$miscellaneous_fees = array();
$miscellaneous_fees[] = '+' . str_pad('', $table_size['width'], '-') . '+';
$temp_head = '| ';
foreach($miscellaneous_fees_header as $key=>$val){
	$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
	$temp_head .= str_pad($val, $key == 'fee_type' ? $length[$key]-25 : $length[$key], ' ', $str_pad);
}

$miscellaneous_fees[] = str_pad($temp_head, $table_size['width']+1, ' ') . '|';
$miscellaneous_fees[] = $miscellaneous_fees[0];

$totals = array();
foreach($miscellaneous_fees_content as $row=>$value){
	$output_total = FALSE;
	$i = count($miscellaneous_fees);
	$miscellaneous_fees[$i] = '| ';
	foreach($value as $key=>$val){
		if($key == 'description' && $val == $miscellaneous_fees_content[count($miscellaneous_fees_content)-1][$key]){
			$output_total = TRUE;
		}
		$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
		if($key == 'description') {
			$miscellaneous_fees[$i] .= str_pad('', $length['fee_type']-24, ' ');
		}
		//if()
		if(in_array($key, $numbers)){
			if(is_numeric($val)){
				$totals[$key] = isset($totals[$key]) ? $totals[$key] : 0;
				$totals[$key] += $val;
				$val = number_format($val, in_array($key, array('units', 'hours')) ? 1 : 2);
			}
			if($output_total) {
				$val = number_format($totals[$key], in_array($key, array('units', 'hours')) ? 1 : 2);
				if($key == 'amount') $total_assessment += $totals[$key];
			}

		}
		$miscellaneous_fees[$i] .= str_pad(substr($val, 0, $length[$key]), $length[$key], ' ', $str_pad);
	}
	$miscellaneous_fees[$i] = str_pad($miscellaneous_fees[$i], $table_size['width']+1, ' ') . '|';
}
if(count($miscellaneous_fees) < count($tuition_fees)-3){
	for($c = count($miscellaneous_fees); $c < count($tuition_fees)-3; $c++){
		$miscellaneous_fees[] = '|' . str_pad('', $table_size['width'], ' ') . '|';
	}
}
$sub_total[1] = $totals['amount'];
$miscellaneous_fees[] = $miscellaneous_fees[0];
$miscellaneous_fees[] = $this->print_lib->no_condensed();
$miscellaneous_fees[count($miscellaneous_fees)-1] .= str_pad('TOTAL ASSESSMENT:', ($table_size['width']/2)-10, ' '); 
$miscellaneous_fees[count($miscellaneous_fees)-1] .= str_pad(number_format($total_assessment, 2), 12, ' ', STR_PAD_LEFT);
$miscellaneous_fees[count($miscellaneous_fees)-1] .= $this->print_lib->condensed();
$miscellaneous_fees[] = str_pad('', $table_size['width']+2, '-');

//print_r($miscellaneous_fees);

/////////////////////////////////////////////////////// TABLE 1 AND 2 READY FOR PRINTING //////////////////////

$print_content = "";
for($i = 0; $i < $table_size['height']; $i++){
	$print_content .= $tuition_fees[$i] . " " . $miscellaneous_fees[$i] . "\n";
}
$print_content = trim($print_content);

?>

<?php
	if ($withDivStart=="YES") {
		echo '<div id="assessment_to_print" style="display:none;">';
		echo $this->print_lib->init(); 
	}
?>

<?php 
//echo $this->print_lib->init(); 
if($this->session->userdata('printed') == 'yes')
	echo $this->print_lib->set_page_length_in_lines(33); else {
	echo $this->print_lib->set_page_length_in_lines(34);
	$this->session->set_userdata('printed', 'yes');
}
echo "\n";
echo $this->print_lib->align_right($idnumber) . "\n";?>
<?php echo $this->print_lib->align_right($name) . "\n"; ?>
<?php echo $this->print_lib->align_right($course_year) ; ?>
<?php //echo print_lib::align_right($term) . "\n"; ?>
<?php //echo print_lib::align_right('Assessment Test Print') . "\n"; ?>
<?php echo $this->print_lib->condensed(); ?>

<?php echo $print_content; ?>

<?php echo $this->print_lib->no_condensed(); ?>
<?php echo "\n"; ?>
<?php 

	//edited   Toyet 11.23.2017
	if(!isset($bal_b4_assessment)){
		$bal_b4_assessment = 0;
	}

	$bal_b4_assessment = round($bal_b4_assessment,2);
	//log_message("INFO", 'Balance B4 Assessment'); 
	//log_message("INFO", print_r($bal_b4_assessment,true)); // Toyet 6.28.2018
	$payable_prelim = 0;

	if($bal_b4_assessment<=0){
		if($payable+$bal_b4_assessment<=0){
			$payable = 0;
		} else {
			$payable = $payable+$bal_b4_assessment;
		}
		echo str_pad("Due for {$period}:", ($table_size['width']*1.2)-12, ' ') . str_pad(number_format(ceil($payable), 2), 10, ' ', STR_PAD_LEFT) . " *\n";
	} else {
		echo str_pad("Due for {$period}:(".number_format($payable, 2)."+Bal=".number_format(ceil($bal_b4_assessment), 2).')', ($table_size['width']*1.2)-12, ' ') . str_pad(number_format(ceil($payable+$bal_b4_assessment), 2), 10, ' ', STR_PAD_LEFT) . " *\n";
	}	

?>
<?php echo $this->print_lib->no_condensed();
      echo str_pad('', $table_size['width']*1.2, '-'); ?>

<?php echo $this->print_lib->condensed(); ?>
<?php echo "*Amount may change depending on payments received and other charges incurred during the semester.  For details of your \n";?>
<?php echo "assessment and payments, login to your HNUMIS account or visit the Finance Office.\n"; ?>
<?php echo "\nStudent Assessment. Printed using HNUMIS. " . date(DATE_RFC822) . $this->print_lib->no_condensed();
echo "\n";
echo "\n";
echo "\n";
echo "\n";
//echo "\n";
//echo "\n"; 

if($withDivEnd=="YES") {
		echo '</div>';
	}

 endif; ?>
