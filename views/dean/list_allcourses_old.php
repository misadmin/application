<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
</style>
<div style="background:#FFF; width:70%;">
<div style="width:100%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td align="right">
    <a class="add_course" href="#" style="text-decoration:none; font-size:12px; font-weight:bold; color:#666; font-family:Verdana, Geneva, sans-serif;">
    <img src="<?php print(base_url('assets/img/new_prospectus.gif')); ?>" width="25" height="27" align="absmiddle" style="vertical-align:middle; border:none" /> Create Course </a></td>
  </tr>
</table>

</div>
<div style="width:100%;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="17%" class="header" style="vertical-align:middle;">Course Code </th>
      <th width="39%" class="header" style="vertical-align:middle;">Descriptive Title </th>
      <th width="14%" class="header" style="vertical-align:middle;">Credit Units </th>
      <th width="17%" class="header" style="vertical-align:middle;">Paying Units </th>
      <th width="13%" class="header"  style="vertical-align:middle;">Details</th>
      </tr>
    </thead>
    <?php
		if ($courses) {
			foreach ($courses AS $course) {
	?>
    <tr>
      <td style="text-align:left; vertical-align:middle; "><?php print($course->course_code); ?></td>
      <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($course->credit_units); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($course->paying_units); ?></td>
      <td style="text-align:center; vertical-align:middle; ">
      <a class="edit_course" href="<?php print($course->id); ?>">
      <img src="<?php print(base_url('assets/img/prospectus_details.gif')); ?>" width="20" height="20" style="vertical-align:bottom; border:none" /></a></td>
      </tr>
    <?php
			}
		}
	?>
</table>
</div>
  </div>
  
<form id="add_course" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="2" />
</form>
<script>
	$('.add_course').click(function(event){
		event.preventDefault(); 
		var add_course = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'add_course',
		    value: add_course,
		}).appendTo('#prospectus2');
		$('#add_course').submit();
	});
</script>


  <form id="edit_course" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="4" />
</form>

<script>
	$('.edit_course').click(function(event){
		event.preventDefault(); 
		var course_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_id',
		    value: course_id,
		}).appendTo('#edit_course');
		$('#edit_course').submit();
	});
</script>
  
  
  
  <form id="srcform2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.course_id').click(function(event){
		event.preventDefault(); 
		var course_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_id',
		    value: course_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>

 