<form id="srcform" action="<?php echo site_url("/{$role}/employee/")?>" method="post">
	<?php echo $this->common->hidden_input_nonce(); ?>
	<input type="text" name="q" id="q" class="wide"/>
	<input type="submit" name="submit" value="Search" />
</form>
<script>
	var options, a;
		jQuery(function(){
		   options = { 
				serviceUrl:'<?php echo site_url("/ajax/search_faculty"); ?>',
				minChars:2,
			};
		   a = $('#q').autocomplete(options);
		   $('#q').focus();
		});
</script>
<style>
.autocomplete {
	background-color: #F7F7F7;
	border: 1px solid #EEEEEE;
}
.autocomplete .selected {
	background-color: #CECECE;
}
</style>