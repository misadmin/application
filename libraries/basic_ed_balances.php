<?php

class Basic_ed_balances {
	public $all_transactions;
	public $ledger_data;
	public $unposted_transactions;
	public $bad_debt = 0;
	public $invoice_items = array();
	public $current_academic_year;
	public $current_year_transactions;
	public $previous_year_transactions;
	public $previous_year_credit_balance = 0;
	public $running_credit_balance;
	public $error = FALSE;
	public $error_message;
	public $has_current_year_assessment=FALSE;
	public $current_nth_exam = 1;
	public $total_number_of_exams = 6;
	public $current_year_assessment = 0;
	public $credit_before_assessment = 0;
	public $payable_per_period = 0;  // added Toyet 9.24.2018
	public $credit_after_assessment = 0; // added Toyet 9.24.2018
	public $debit_after_assessment = 0; // added Toyet 9.24.2018
	public $payable_for_current_period_1 = 0; // added Toyet 10.1.2018
	public $payable_for_current_period = 0; // added Toyet 10.1.2018

	
	public function __construct( $config=null ){
		$this->error = FALSE;
		if(! is_null($config)){
			$this->current_nth_exam = $config['current_nth_exam'];
			$this->total_number_of_exams = $config['total_number_of_exams'];
			$this->ledger_data = $config['ledger_data'];
			$this->unposted_transactions = $config['unposted_transactions'];
			$this->all_transactions = array_merge ($config['ledger_data'], $config['unposted_transactions']);

			usort($this->all_transactions, array($this, 'compare_array_by_date_key'));
			
			$this->periods = $config['periods']; //exams...

			$this->current_academic_year = $config['current_academic_year'];
			$previous_year_transactions = array();
			$current_year_transactions = array();
			$previous_year_credit_balance = 0;
			$before_current_year_assessment = TRUE;
			$credit_before_assessment = 0;
			$date_of_asssessment = null;
			$debit_before_assessment = 0;
			$credit_after_assessment = 0;
			$debit_after_assessment = 0;
			$this->bed_status = $config['bed_status'];
			$running_credit_balance = 0; //credit_balance;
			$bad_debt = 0;
			foreach ($this->all_transactions as $transaction){
				$running_credit_balance += $transaction['debit'] - $transaction['credit'];
				if( $transaction['transaction_date'] >= $this->current_academic_year->start_date ){
					//This is a current year transaction...
					$current_year_transactions[] = (object)$transaction;
					if($transaction['transaction_detail']=='ASSESSMENT'){
						$this->has_current_year_assessment = TRUE;
						$this->current_year_assessment = $transaction['debit'];
						$date_of_asssessment = $transaction['transaction_date'];
					} else {
						if($date_of_asssessment == null){
							$credit_before_assessment += $transaction['credit'];
						}
					}	
				} else {
					//Transaction is from previous year...
					$previous_year_credit_balance += $transaction['debit'] - $transaction['credit'];
				}
				if( $transaction['status']=='written_off'){
					//this is a bad debt...
					$bad_debt += $transaction['debit'] - $transaction['credit']; //debit...
				}
				if( $transaction['transaction_detail']=='BAD DEBT'){
					//this is a payment for bad debt (bad debt recovery)
					$bad_debt += $transaction['debit'] - $transaction['credit'];
				}
			}

			$invoice_items = array();
			if ($previous_year_credit_balance < 0){
				//has items from last school years that were unpaid.
				$payable_per_period = abs($previous_year_credit_balance)/count($this->periods);
				foreach ($this->periods as $period){
					$invoice_items[] = array(
							'debit' => $payable_per_period,
							'credit' => 0,
							'transaction_date' => $period->start_date,
							'transaction_detail' => 'PREVIOUS YEAR BALANCE (EXAM ' . $period->exam_number . ')',
							'period' => $period->exam_number,
					);
				}
			}
			$before_assessment = TRUE;
			$debits_after_asess = array();
			foreach ($current_year_transactions as $transaction){
				$transaction = (array)$transaction;
				if($transaction['debit'] > 0 && $before_assessment ){
					//we divide this by the number of exams the student has
					$payable_per_period = $transaction['debit'] / (count($this->periods));
					foreach ($this->periods as $period){
						$invoice_items[] = array(
								'debit' => $payable_per_period,
								'credit' => 0,
								'transaction_date' => $period->start_date,
								'transaction_detail' => $transaction['transaction_detail'] . ' (Exam ' . $period->exam_number . ')',
								'period' => $period->exam_number,
						);
					}
				}
				if($transaction['credit'] > 0){
					$invoice_items[] = (array)$transaction;					
				}

				if($transaction['debit'] > 0 && !$before_assessment){
					$debit_after_assessment += $transaction['debit'];

					$periods_remaining = count($this->periods);
					$start_date = (int)str_replace("-", "", $this->current_academic_year->start_date);
					$_prev_end_date = $this->periods[0]->end_date;
					foreach ($this->periods as $period){
						$transaction_date = (int)str_replace("-", "", $transaction['transaction_date']);
						if($transaction_date >= $start_date && $transaction_date <= (int)str_replace("-", "", $period->start_date)){
							$payable_per_period = $transaction['debit'] / $periods_remaining;
							$invoice_items[] = array( 
									'debit' => $payable_per_period,
									'credit' => 0,
									'transaction_date' => $period->start_date,
									'transaction_detail' => $transaction['transaction_detail'] . ' (Exam ' . $period->exam_number . ')',
									'period' => $period->exam_number,
							);
						} else {
							$periods_remaining--;
						}

						//a debit transaction... this should be divided between the remaining exams...
						//log_message("INFO", print_r("trace...".$transaction['transaction_date'].' '.$period->start_date.' '.$period->end_date,true));

						if($transaction['transaction_date']<=$period->end_date && $transaction['transaction_date']>=$_prev_end_date){
							$_divisor = $this->total_number_of_exams-$period->exam_number+1;
							$debits_after_asess[] = array(
								'nth_exam'=>$period->exam_number,
								'amount'=>$transaction['debit']/$_divisor,
							);
						}
						$_prev_end_date = $period->end_date;
					}										
				}
				if($transaction['credit'] > 0 && !$before_assessment){
					$credit_after_assessment += $transaction['credit'];
				}
				if($transaction['transaction_detail']=='ASSESSMENT'){
					$before_assessment = FALSE;
					$this->current_year_assessment = $transaction['debit'];
					$this->payable_per_period = $this->current_year_assessment / $this->total_number_of_exams;
				}
			}

			//log_message("INFO","INVOICE ITEMS   ===>".print_r($invoice_items,true));
			//log_message("INFO","this->PERIODS   ===>".print_r($this->periods,true));
			//log_message("INFO","debits_after_asess   ===>".print_r($debits_after_asess,true));
			
			usort($invoice_items, array($this, 'compare_array_by_date_key'));
			$this->bad_debt = $bad_debt;
			$this->invoice_items = $invoice_items;
			$this->previous_year_credit_balance = $previous_year_credit_balance;
			$this->current_year_transactions = $current_year_transactions;
			$this->running_credit_balance = $running_credit_balance;
			$this->credit_before_assessment = $credit_before_assessment;
			$this->credit_after_assessment = $credit_after_assessment;
			$this->debit_after_assessment = $debit_after_assessment;


			// Current Period Payable
			// Current Period Payable
			$this->payable_for_current_period_1 = $this->payable_per_period * $this->current_nth_exam;
			$this->payable_for_current_period = $this->payable_for_current_period_1 - 
												$this->credit_before_assessment - 
												$this->credit_after_assessment;

			//previous years balance
			if($this->previous_year_credit_balance<0){
				$this->payable_for_current_period = $this->payable_for_current_period - abs($this->previous_year_credit_balance);
			} else {
				$this->payable_for_current_period = $this->payable_for_current_period + abs($this->previous_year_credit_balance);
			}
			//add new debits divided by the number of remaining periods
			//$new_debits_divisor = count($this->periods) - $this->current_nth_exam;
			//$this->payable_for_current_period = $this->payable_for_current_period + ($this->debit_after_assessment/$new_debits_divisor);
			foreach($debits_after_asess as $row){
				$this->payable_for_current_period = $this->payable_for_current_period + ($row['amount']*($this->current_nth_exam-$row['nth_exam']+1));
			}

			if($this->payable_for_current_period<0)
				$this->payable_for_current_period = 0;

			if($this->current_nth_exam!=count($this->periods)){
				$this->payable_for_current_period = ceil($this->payable_for_current_period);
			} else {
				$this->payable_for_current_period = $this->ledger_balance();
			}
			// Current Period Payable
			// Current Period Payable

			//log_message("INFO","BAD DEBT            ==> ".print_r(round($this->bad_debt,2),true)); // Toyet 10.1.2018 
			//log_message("INFO","INVOICE ITEMS       ==> ".print_r($this->invoice_items,true)); // Toyet 10.1.2018 
			//log_message("INFO","PREV YR CREDIT BAL  ==> ".print_r(round($this->previous_year_credit_balance,2),true)); // Toyet 10.1.2018 
			//log_message("INFO","CURR YR TRANs       ==> ".print_r($this->current_year_transactions,true)); // Toyet 10.1.2018 
			//log_message("INFO","CURR YR TRANs       ==> ".print_r($this->current_year_transactions(),true)); // Toyet 10.1.2018 
			//log_message("INFO","RUNNG CREDIT BAL    ==> ".print_r($this->running_credit_balance,true)); // Toyet 10.1.2018 
			//log_message("INFO","CREDT BEFORE ASSESS ==> ".print_r(round($this->credit_before_assessment,2),true)); // Toyet 10.1.2018 
			//log_message("INFO","CURRENT ASSESSMENTS ==> ".print_r(round($this->current_year_assessment,2),true)); // Toyet 10.1.2018 
			//log_message("INFO","CREDT AFTER ASSESS  ==> ".print_r(round($this->credit_after_assessment,2),true)); // Toyet 10.1.2018 
			//log_message("INFO","DEBIT AFTER ASSESS  ==> ".print_r(round($this->debit_after_assessment,2),true)); // Toyet 10.1.2018 
			//log_message("INFO","PAYABLE PER PERIOD  ==> ".print_r(round($this->payable_per_period,2),true)); // Toyet 10.1.2018 
			//log_message("INFO","CURRENT NTH EXAM    ==> ".print_r($this->current_nth_exam,true)); // Toyet 10.1.2018 
			//log_message("INFO","TOTAL # OF EXAMS    ==> ".print_r($this->total_number_of_exams,true)); // Toyet 10.1.2018 
			//log_message("INFO","PAYABLE FOR CURRENT_1 ==> ".print_r($this->payable_for_current_period_1,true)); // Toyet 10.1.2018 
			//log_message("INFO","PAYABLE FOR CURRENT ==> ".print_r($this->payable_for_current_period,true)); // Toyet 10.1.2018 
		}
	}
	
	public function invoice_items($exam_number){
		$items = array();
		$start_date = (int)str_replace("-", "", $this->current_academic_year->start_date);
		foreach ($this->periods as $period){
			$end_date = (int)str_replace("-", "", $period->end_date);
			if($period->exam_number == $exam_number){
				break;
			}
		}		
		
		foreach ($this->invoice_items as $transaction){
			$transaction = (object)$transaction;
			if($transaction->debit > 0){
				$transaction_date = (int)str_replace("-", "", $transaction->transaction_date);
				if($transaction_date > $start_date && $transaction_date < $end_date){
					$items[] = $transaction;
				}
			}
			if($transaction->credit > 0){
				$items[] = $transaction;
			}	
		}
		return $items; 
	}
	
	public function payable_for_exam ($exam_number){
		$invoices = $this->invoice_items($exam_number);
		$debit_balance = 0;
		foreach ($invoices as $item){
			$debit_balance += $item->debit - $item->credit;
		}
		if ($this->has_current_year_assessment)
			return $debit_balance > 0 ? $debit_balance : 0;
		else
			return "Not Calculated";
	}
	
	public function ledger_balance(){
		$debit_balance = 0;
		foreach($this->all_transactions as $transaction){
			$transaction = (object)$transaction;
			$debit_balance += $transaction->debit - $transaction->credit;
		}
		return ($debit_balance > 0 ? $debit_balance : 0);
	}

	function due_this_exam( $number_of_exams = 6, $current_exam_number = 1 )
	{
		// $due_this_exam = $this->ledger_balance();
		// // prevent division by zero  - Toyet 6.20.2018
		// if(( $number_of_exams + 1 - $current_exam_number )>0) {
		// 	$due_this_exam = $this->ledger_balance() / ( $number_of_exams + 1 - $current_exam_number );
		// 	$due_this_exam = $due_this_exam < 1 ? 0 : $due_this_exam;
		// 	$due_this_exam = $number_of_exams == $current_exam_number ? $due_this_exam : ceil($due_this_exam);
		// }
		$due_this_exam = $this->current_year_assessment / $number_of_exams * $current_exam_number;
		return $due_this_exam;
	}

	function due_this_exam2()
	{
		$ledger_balance = $this->ledger_balance();
		$exam_divisor = $this->total_number_of_exams() + 1 - $this->current_nth_exam();
		if($exam_divisor<>0){
			$due_this_exam = $ledger_balance / ( $this->total_number_of_exams() + 1 - $this->current_nth_exam() );
		} else {
			$due_this_exam = $ledger_balance;
		}

		$due_this_exam = $due_this_exam < 1 ? 0 : $due_this_exam;
		$due_this_exam = $this->total_number_of_exams() == $this->current_nth_exam() ? $due_this_exam : ceil($due_this_exam);
		return $due_this_exam;
	}

	public function all_transactions(){
		return $this->all_transactions;
	}
	
	public function bad_debt(){
		return ($this->bad_debt > 0 ? $this->bad_debt : 0);
	}
	
	public function balance_previous_year(){
		return $this->previous_year_credit_balance < 0 ? $this->previous_year_credit_balance : 0;
	}
	
	public function debit_balance(){
		return ($this->running_credit_balance < 0 ? abs($this->running_credit_balance) : 0);
	}
	
	public function current_year_transactions (){
		$transactions_this_year = array_merge(array((object)array(
				'transaction_date'=>'',
				'debit'=>($this->previous_year_credit_balance > 0 ? $this->previous_year_credit_balance : ''),
				'credit'=>($this->previous_year_credit_balance < 0 ? abs($this->previous_year_credit_balance) : ''),
				'transaction_detail'=>'FORWARDED BALANCE>>>',
		)), $this->current_year_transactions);
		return $transactions_this_year;
	}
	
	private function compare_object_by_date_property($a, $b){
		if($a->transaction_detail=='Forwarded Balance'){
			return -1;
		}
		if($b->transaction_detail=='Forwarded Balance'){
			return 1;
		}
		if($a->transaction_date == $b->transaction_date)
			return 0;
		return $a->transaction_date < $b->transaction_date ? -1 : 1;
	}
	
	private function compare_array_by_date_key($a, $b){
		if($a['transaction_detail']=='Forwarded Balance'){
			return -1;
		}
		if($b['transaction_detail']=='Forwarded Balance'){
			return 1;
		}
		if($a['transaction_date'] == $b['transaction_date'])
			return 0;
		return $a['transaction_date'] < $b['transaction_date'] ? -1 : 1;
	}

	public function current_nth_exam(){
		return $this->current_nth_exam;
	}

	public function total_number_of_exams(){
		return $this->total_number_of_exams;
	}

}
