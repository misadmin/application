<?php

if(!isset($withDivStart))
	$withDivStart = "YES";
if(!isset($withDivEnd))
	$withDivEnd = "YES";
if(!isset($section_name))
	$section_name = "";
if(!isset($print_count))
	$print_count = 1;

////log_message("INFO","withDivStart =>".print_r($withDivStart,true));
////log_message("INFO","withDivEnd =>".print_r($withDivEnd,true));

$ledger['header'] = array(
		array('id'=>'date', 'name'=>'Date', 'width'=>9),
		array('id'=>'transaction', 'name'=>'Transaction', 'width'=>26),
		array('id'=>'reference', 'name'=>'Reference', 'width'=>18),
		array('id'=>'debit', 'name'=>'DR/Charges', 'width'=>11),
		array('id'=>'credit', 'name'=>'CR/Payments', 'width'=>11),
		array('id'=>'balance', 'name'=>'Balance', 'width'=>11),
);
$idnumber = isset($idnumber) ? $idnumber : "";
$payment_period = isset($payment_period) ? $payment_period : "";
$ledger['data'] = array();
if(isset($ledger_data) && is_array($ledger_data) && count($ledger_data) > 0){
	$running_balance = 0;

	$my_due = 0; $my_debits =0; $my_credits =0; $my_ass = 0; $my_cr = 0;
	$my_debit_div4 = 0; $my_debit_div3 = 0; $my_debit_div2 = 0; $my_debit_div1 = 0; //this are debits and non-ass
	
	foreach($ledger_data as $key=>$ledge){
		
		$my_ass 	+= (($ledge->transaction_detail == 'ASSESSMENT') ? $ledge->debit : 0 ) ; 
		$my_debits  += (($ledge->transaction_detail != 'ASSESSMENT') ? $ledge->debit : 0) ;
		
		if ($ledge->transaction_detail == 'COLLEGE REGISTRATION' && $ledge->academic_terms_id >= 456 ){
			$my_cr += $ledge->credit; 
		}else{
			$my_credits += $ledge->credit ;
		} 

		
		$running_balance += $ledge->debit - $ledge->credit;
		$ledger['data'][] = array(
					'date'=>$ledge->transaction_date,
					'transaction'=>$ledge->transaction_detail,
					'reference'=>isset($ledge->reference_number) && !empty($ledge->reference_number) ? "Ref: ".$ledge->reference_number : '',
					'debit'=>$ledge->debit,
					'credit'=>$ledge->credit,
					'balance'=>$running_balance,
				);
		
	}
	
	
	if ($my_credits>=800 && $my_cr == 0){
		$my_cr = 800;
		$my_credits = $my_credits - 800;
	}elseif ($my_cr > 800){
		$my_credits +=  ($my_cr - 800);
		$my_cr = 800;
	}else{
		
	}
	
	
	$factor=3;
	$my_due = ( (($my_ass - $my_cr)/4) + ($my_debits/4)  ) * $factor ;
	//print_r($my_due);die();
	
	$my_due = $my_due - $my_credits ;			
	$my_due = ($my_due > 0) ? ceil($my_due) : 0;
	
	
}
$this->print_lib->set_ledger_title('');
$this->print_lib->set_table_header($ledger['header']);
$this->print_lib->set_data($ledger['data']);
?>

<?php
//this should be placed outside the html tag or body to be accessible
//echo sprintf($this->config->item('jzebra_applet'), base_url()); 
if($withDivStart=="YES")
	echo '<div id="'.$div_id.'" style="display:none;">';
?>

<?php 
echo $this->print_lib->init();
if($this->session->userdata('printed') == 'yes')
	echo $this->print_lib->set_page_length_in_lines(33); else {
	echo $this->print_lib->set_page_length_in_lines(33);
	$this->session->set_userdata('printed', 'yes');
}
echo "\n";
?><?php echo $this->print_lib->no_condensed();
        echo $this->print_lib->align_right($idnumber) . "\n";?>
<?php echo $this->print_lib->align_right($name) . "\n"; ?>
<?php echo $this->print_lib->align_right((isset($course_year) ? $course_year : "").' ('.$section_name.')') . "\n"; ?>
<?php echo $this->print_lib->bold() . $this->print_lib->align_right('Statement of Account') . $this->print_lib->regular() . "\n"; 
	$this->print_lib->bold();
?>
<?php echo $this->print_lib->draw_ledger(146, $period, $payable, "=", TRUE); ?>
<?php 
	for ($i = 1; $i < (13-count($this->print_lib->data)); $i++){
		echo "\n";
	}
?>
<?php echo $this->print_lib->condensed(); ?>
<?php echo "Prints:[<span id='{$period}_counter' style='display:none;'>".$print_count."</span>]"; ?>
<?php echo "\nStudent Statement of Account for {$period} printed by HNUMIS. " . date(DATE_RFC822) . "\n"; ?>
<?php echo "\n\n\n"; 
	if($withDivEnd=="YES")
		echo '</div>';
?>