<?php

	class Reports_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

		function enrollment_summary_basic_ed() {
		
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('basic_ed/basic_ed_sections_model');
	
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:	
					$this->list_academic_years();				
					break;
				case 2:	
					$this->create_enrollment_summary_basic_ed($this->input->post('academic_years_id'), 0);				
					break;
				case 3:
					$this->create_enrollment_summary_basic_ed($this->input->post('academic_years_id'), 1);				
					break;
			}	

		}

		
		function assessment_summary_basic_ed() {
		
	
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:	
					$this->list_academic_terms();				
					break;
				case 2:	
					$this->create_assessment_summary($this->input->post('academic_terms_id'), 0);				
					break;
				case 3:	
					$this->create_assessment_summary($this->input->post('academic_terms_id'), 1);				
					break;
			}	

		}

	
		function list_academic_years() {
	
			$data['academic_years'] = $this->AcademicYears_Model->ListAcadYears();
	
			$this->content_lib->enqueue_body_content('posting/list_academic_years',$data);
			$this->content_lib->content();
				
		}
	

		function create_enrollment_summary_basic_ed($academic_years_id, $value) {
		
			$data['acad_yr'] = $this->AcademicYears_Model->GetAcademicYear($academic_years_id);
	
			$data['basic_eds'] = $this->basic_ed_sections_model->ListColleges();
			if ($value == 0) {
				$this->content_lib->enqueue_body_content('posting/enrollment_summary',$data);
				$this->content_lib->content();
			} else {
				$this->generate_enrollment_summary_pdf($academic_terms_id, $data);
			}
		
		}
	
	
		function create_assessment_summary($academic_terms_id, $value) {
		
			$data['term'] = $this->AcademicYears_Model->getAcademicTerms($academic_terms_id);
	
			$data['academic_terms_id'] = $academic_terms_id;
			$data['programs'] = $this->Programs_Model->ListProgramGroups();
			if ($value == 0) {
				$this->content_lib->enqueue_body_content('posting/assessment_summary',$data);
				$this->content_lib->content();
			} else {
				$this->generate_assessment_summary_pdf($academic_terms_id, $data);
			}
		
		}


		function generate_enrollment_summary_pdf($academic_terms_id, $data) {

			$this->load->library('my_pdf');
			$this->load->library('hnumis_pdf');
		
			$this->hnumis_pdf->AliasNbPages();		
			$this->hnumis_pdf->set_HeaderTitle('ENROLLMENT SUMMARY - '.$data['term']->term." ".$data['term']->sy);
			$this->hnumis_pdf->AddPage('L','folio');
			
			$this->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->hnumis_pdf->SetDrawColor(165,165,165);
			$this->hnumis_pdf->Ln();

			$this->hnumis_pdf->SetFont('times','B',14);
			
			$this->hnumis_pdf->SetFont('times','B',10);
			$this->hnumis_pdf->SetFillColor(116,116,116);
			$this->hnumis_pdf->SetTextColor(253,253,253);
			
			$this->hnumis_pdf->Cell(50,15,'College',1,0,'C',true);

			$r = 1;
			$x = 60;
			$y = 35;

			$header1 = array('1st Year','2nd Year','3rd Year','4th Year','5th Year','GRAND TOTAL');

			$w1 = array(48,48,48,48,48,48);
			
			for($i=0;$i<count($w1);$i++) 
				$this->hnumis_pdf->Cell($w1[$i],10,$header1[$i],1,0,'C',true);
				
						
			$header2 = array('M','F','TOTAL','M','F','TOTAL','M','F','TOTAL','M','F','TOTAL',
									'M','F','TOTAL','M','F','TOTAL');

			for($i=0;$i<count($header2);$i++) {
				$this->hnumis_pdf->SetXY($x,$y);
				$this->hnumis_pdf->MultiCell(16,5,$header2[$i],1,'C',true);
				
				$x = $x + 16;
				$r++;
				if ($r > 18) {
					$r=1;
					$x=10;
					$y = $y + 28;
				} 
			}

			$w=array(50,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16);
			
			$this->hnumis_pdf->SetFont('Arial','',10);
			$this->hnumis_pdf->SetTextColor(0,0,0);
			$grand_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
										'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
										'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
										'total_m'=>0,'total_f'=>0,'grand_total'=>0);
			$program_total = 0;
			foreach ($data['colleges'] as $college) {
				$programs = $this->Enrollments_Model->ListEnrollmentSummary($college->id, $academic_terms_id);
				
				$total1 = 0;$total2 = 0;$total3 = 0;$total4 = 0;$total5 = 0;$total_m = 0;$total_f = 0;$col_total = 0;
				
				$college_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
										'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
										'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
										'total_m'=>0,'total_f'=>0);

				$this->hnumis_pdf->SetFillColor(200,200,200);
				$this->hnumis_pdf->Cell(338,8,$college->name,1,0,'L',true);
				$this->hnumis_pdf->SetFillColor(255,255,255);
				$this->hnumis_pdf->Ln();
				$this->hnumis_pdf->SetFont('Arial','',9);
				
				foreach ($programs AS $program) {
					$this->hnumis_pdf->Cell($w[0],8,'     '.$program->abbreviation,1,0,'L',true);
					
					$college_total['m1'] = $college_total['m1'] + $program->m1; 
					$this->hnumis_pdf->Cell($w[1],8,$program->m1,1,0,'C',true);
					$college_total['f1'] = $college_total['f1'] + $program->f1;								
					$this->hnumis_pdf->Cell($w[2],8,$program->f1,1,0,'C',true);
					$total1 = $program->m1 + $program->f1; 
					$college_total['total1'] = $college_total['total1'] + $total1;
					$this->hnumis_pdf->Cell($w[3],8,$total1,1,0,'C',true);
					
					$college_total['m2'] = $college_total['m2'] + $program->m2; 
					$this->hnumis_pdf->Cell($w[4],8,$program->m2,1,0,'C',true);
					$college_total['f2'] = $college_total['f2'] + $program->f2;								
					$this->hnumis_pdf->Cell($w[5],8,$program->f2,1,0,'C',true);
					$total2 = $program->m2 + $program->f2; 
					$college_total['total2'] = $college_total['total2'] + $total2;
					$this->hnumis_pdf->Cell($w[6],8,$total2,1,0,'C',true);

					$college_total['m3'] = $college_total['m3'] + $program->m3; 
					$this->hnumis_pdf->Cell($w[7],8,$program->m3,1,0,'C',true);
					$college_total['f3'] = $college_total['f3'] + $program->f3;								
					$this->hnumis_pdf->Cell($w[8],8,$program->f3,1,0,'C',true);
					$total3 = $program->m3 + $program->f3; 
					$college_total['total3'] = $college_total['total3'] + $total3;
					$this->hnumis_pdf->Cell($w[9],8,$total3,1,0,'C',true);

					$college_total['m4'] = $college_total['m4'] + $program->m4; 
					$this->hnumis_pdf->Cell($w[10],8,$program->m4,1,0,'C',true);
					$college_total['f4'] = $college_total['f4'] + $program->f4;								
					$this->hnumis_pdf->Cell($w[11],8,$program->f4,1,0,'C',true);
					$total4 = $program->m4 + $program->f4; 
					$college_total['total4'] = $college_total['total4'] + $total4;
					$this->hnumis_pdf->Cell($w[12],8,$total4,1,0,'C',true);

					$college_total['m5'] = $college_total['m5'] + $program->m5; 
					$this->hnumis_pdf->Cell($w[13],8,$program->m5,1,0,'C',true);
					$college_total['f5'] = $college_total['f5'] + $program->f5;								
					$this->hnumis_pdf->Cell($w[14],8,$program->f5,1,0,'C',true);
					$total5 = $program->m5 + $program->f5; 
					$college_total['total5'] = $college_total['total5'] + $total5;
					$this->hnumis_pdf->Cell($w[15],8,$total5,1,0,'C',true);

					$total_m = $program->m1 + $program->m2 + $program->m3 + $program->m4 + $program->m5; 
					$college_total['total_m'] = $college_total['total_m'] + $total_m;
					$this->hnumis_pdf->Cell($w[16],8,$total_m,1,0,'C',true);
					$total_f = $program->f1 + $program->f2 + $program->f3 + $program->f4 + $program->f5; 
					$college_total['total_f'] = $college_total['total_f'] + $total_f;					
					$this->hnumis_pdf->Cell($w[17],8,$total_f,1,0,'C',true);
					$program_total = $total_m + $total_f; 
					$col_total = $col_total + $program_total;					
					$this->hnumis_pdf->Cell($w[18],8,$program_total,1,0,'C',true);
					$this->hnumis_pdf->Ln();
				}
				$this->hnumis_pdf->Cell($w[0],8,'College Total :',1,0,'R',true);
				$this->hnumis_pdf->Cell($w[1],8,$college_total['m1'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[2],8,$college_total['f1'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[3],8,$college_total['total1'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[4],8,$college_total['m2'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[5],8,$college_total['f2'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[6],8,$college_total['total2'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[7],8,$college_total['m3'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[8],8,$college_total['f3'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[9],8,$college_total['total3'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[10],8,$college_total['m4'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[11],8,$college_total['f4'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[12],8,$college_total['total4'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[13],8,$college_total['m5'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[14],8,$college_total['f5'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[15],8,$college_total['total5'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[13],8,$college_total['total_m'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[14],8,$college_total['total_f'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[15],8,$col_total,1,0,'C',true);
				$grand_total['m1'] = $grand_total['m1'] + $college_total['m1'];
				$grand_total['f1'] = $grand_total['f1'] + $college_total['f1'];
				$grand_total['m2'] = $grand_total['m2'] + $college_total['m2'];
				$grand_total['f2'] = $grand_total['f2'] + $college_total['f2'];
				$grand_total['m3'] = $grand_total['m3'] + $college_total['m3'];
				$grand_total['f3'] = $grand_total['f3'] + $college_total['f3'];
				$grand_total['m4'] = $grand_total['m4'] + $college_total['m4'];
				$grand_total['f4'] = $grand_total['f4'] + $college_total['f4'];
				$grand_total['m5'] = $grand_total['m5'] + $college_total['m5'];
				$grand_total['f5'] = $grand_total['f5'] + $college_total['f5'];
				$grand_total['total1'] = $grand_total['total1'] + $college_total['total1'];
				$grand_total['total2'] = $grand_total['total2'] + $college_total['total2'];
				$grand_total['total3'] = $grand_total['total3'] + $college_total['total3'];
				$grand_total['total4'] = $grand_total['total4'] + $college_total['total4'];
				$grand_total['total5'] = $grand_total['total5'] + $college_total['total5'];
				$grand_total['total_m'] = $grand_total['total_m'] + $college_total['total_m'];
				$grand_total['total_f'] = $grand_total['total_f'] + $college_total['total_f'];
				$grand_total['grand_total'] = $grand_total['grand_total'] + $col_total;

				$this->hnumis_pdf->Ln();
			
			}
			$this->hnumis_pdf->SetFillColor(202,202,202);
			$this->hnumis_pdf->Cell($w[0],8,'Grand Total :',1,0,'R',true);
			$this->hnumis_pdf->Cell($w[1],8,$grand_total['m1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[2],8,$grand_total['f1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[3],8,$grand_total['total1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[4],8,$grand_total['m2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[5],8,$grand_total['f2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[6],8,$grand_total['total2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[7],8,$grand_total['m3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[8],8,$grand_total['f3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[9],8,$grand_total['total3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[10],8,$grand_total['m4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[11],8,$grand_total['f4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[12],8,$grand_total['total4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[13],8,$grand_total['m5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[14],8,$grand_total['f5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[15],8,$grand_total['total5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[13],8,$grand_total['total_m'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[14],8,$grand_total['total_f'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[15],8,$grand_total['grand_total'],1,0,'C',true);
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->Output();
		}	


		function generate_assessment_summary_pdf($academic_terms_id, $data) {

			$this->load->library('my_pdf');
			$this->load->library('hnumis_pdf');
		
			$this->hnumis_pdf->AliasNbPages();		
			$this->hnumis_pdf->set_HeaderTitle('ASSESSMENT SUMMARY - '.$data['term']->term." ".$data['term']->sy);
			$this->hnumis_pdf->AddPage('P','letter');
			
			$this->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->hnumis_pdf->SetDrawColor(165,165,165);
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->SetFont('times','B',10);
			$this->hnumis_pdf->SetFillColor(116,116,116);
			$this->hnumis_pdf->SetTextColor(253,253,253);
			
			$header1 = array('PROGRAMS','1st Year','2nd Year','3rd Year','4th Year','5th Year','6th Year','TOTAL');

			$w1 = array(40,23,23,23,23,23,23,23);
			
			for($i=0;$i<count($w1);$i++) 
				$this->hnumis_pdf->Cell($w1[$i],8,$header1[$i],1,0,'C',true);
				
			$this->hnumis_pdf->Ln();
						
			$this->hnumis_pdf->SetFont('Arial','',8);
			$this->hnumis_pdf->SetTextColor(0,0,0);

			$grand_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'assess6'=>0,'total'=>0);

			$this->hnumis_pdf->SetFillColor(255,255,255);
	
			foreach ($data['programs'] as $program) {
				
				$groups = $this->Assessment_Model->ListGroupAssessmentSummary($program->id, $academic_terms_id);
				
				$total = 0;
				if ($groups) {
					foreach($groups AS $group) {
						$total = $group->assess1+$group->assess2+$group->assess3+$group->assess4+$group->assess5+$group->assess6; 
						$grand_total['assess1'] = $grand_total['assess1'] + $group->assess1;
						$grand_total['assess2'] = $grand_total['assess2'] + $group->assess2;
						$grand_total['assess3'] = $grand_total['assess3'] + $group->assess3;
						$grand_total['assess4'] = $grand_total['assess4'] + $group->assess4;
						$grand_total['assess5'] = $grand_total['assess5'] + $group->assess5;
						$grand_total['assess6'] = $grand_total['assess6'] + $group->assess6;
						$grand_total['total'] = $grand_total['total'] + $total;
						
						$this->hnumis_pdf->Cell($w1[0],8,$program->abbreviation,1,0,'L',true);
						$this->hnumis_pdf->Cell($w1[1],8,number_format($group->assess1,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[2],8,number_format($group->assess2,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[3],8,number_format($group->assess3,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[4],8,number_format($group->assess4,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[5],8,number_format($group->assess5,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[6],8,number_format($group->assess6,2),1,0,'R',true);
						$this->hnumis_pdf->Cell($w1[7],8,number_format($total,2),1,0,'R',true);
						$this->hnumis_pdf->Ln();
					}
				}

			}
			$this->hnumis_pdf->SetFillColor(202,202,202);
			$this->hnumis_pdf->Cell($w1[0],8,'GRAND TOTAL: ',1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[1],8,number_format($grand_total['assess1'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[2],8,number_format($grand_total['assess2'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[3],8,number_format($grand_total['assess3'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[4],8,number_format($grand_total['assess4'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[5],8,number_format($grand_total['assess5'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[6],8,number_format($grand_total['assess6'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[7],8,number_format($grand_total['total'],2),1,0,'R',true);
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->Output();
		}	
		

}
//end of student_model.php