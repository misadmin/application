<?php 
	$headers = array(
				array('id'=>'catalog', 'name'=>'#', 'width'=>10),
				array('id'=>'idno', 'name'=>"Stud ID", 'width'=>25),
				array('id'=>'name', 'name'=>"Name", 'width'=>65),
				array('id'=>'gender', 'name'=>"Gender", 'width'=>20),
				array('id'=>'subject', 'name'=>"Subjects Enrolled", 'width'=>40),
				array('id'=>'unit', 'name'=>"Units", 'width'=>10),
	);
	$this->print_lib->set_table_header($headers);
?><div id="to_print" style="display:none;"><?php 
$lines = array();
echo $this->print_lib->init();

$lines[] =  $this->print_lib->bold() . "ENROLLMENT LIST" . $this->print_lib->regular() . "\n";
$lines[] = "Name of Institution: " . $this->print_lib->bold() . "Holy Name University" . $this->print_lib->regular() . "\n";
$lines[] = "Address: " . $this->print_lib->bold() . "Tagbilaran City" . $this->print_lib->regular() . " Tel. No. " . $this->print_lib->bold() . "(038)4123432, (038)4123763, (038)5019817" . $this->print_lib->regular() . "\n";
$lines[] = "Institutional Identifier: " . $this->print_lib->bold() . "07040" . $this->print_lib->regular() . "\n";
$lines[] = "SY and Sem/Tri: " . $this->print_lib->bold() . $sy . "  [" . $academic_term . "]" . $this->print_lib->regular() . "\n";
$lines[] = "Course/Program: " . $this->print_lib->bold() . $academic_program . $this->print_lib->regular() . "\n";
$lines[] = "Year Level: " . $this->print_lib->bold() . $year_level . $this->print_lib->regular() . "\n";
$lines[] = $this->print_lib->condensed() . "\n";
$lines[] = "---------------------------------------------------------------------------------------------------------------------------------------\n";
$contents = explode("\n", $this->print_lib->draw_table());
foreach ($contents as $content){
	$lines[] = $content; 
}
$lines[] = "\n---------------------------------------------------------------------------------------------------------------------------------------\n";
$max_line_count = 60;
$student_count = 1;
foreach ($student_courses as $key => $student_course){
	$course_count = 1;
	foreach($student_course['courses'] as $course) {
		if($course_count==1)
			$lines[] = str_pad($student_count,4, " ", STR_PAD_LEFT) . "   " . str_pad($student_course['info']->idno, 14, " ", STR_PAD_BOTH) . "   " . str_pad(substr(utf8_decode($student_course['info']->fullname), 0, 52), 52," ", STR_PAD_RIGHT) . "  " . ($student_course['info']->gender == "M" ? "  Male" : ($student_course['info']->gender == "F" ? "Female" : "      ") ) . "           " . (str_pad(substr($course->course, 0, 24), 24, " ", STR_PAD_RIGHT )) . "     " . (str_pad(($course->is_bracketed == "Y" ? "({$course->credit_units})" : $course->credit_units), 10, " ", STR_PAD_BOTH)) . "\n" ;
		else
			$lines[] = str_pad("", 95, " ", STR_PAD_LEFT) . str_pad(substr($course->course, 0, 24), 24, " ", STR_PAD_RIGHT) . "     " . (str_pad(($course->is_bracketed == "Y" ? "({$course->credit_units})" : $course->credit_units), 10, " ", STR_PAD_BOTH)) . "\n" ;
		$course_count++;
	}
	$lines[] = "---------------------------------------------------------------------------------------------------------------------------------------\n";
	$student_count++;
}

$line_count = 0;
$page_line = 0;
$page = 1;
while ($line_count < count($lines)){
	if($page_line < $max_line_count) {
		if ($page_line == 0){
			echo "\n\n\n\n\n";
			$page_line = 5;
			if($page > 1){
				echo "\n\n\n";
			}
		}
		echo $lines[$line_count];
		$page_line++;
	} else {
		$page_line = 0;
		echo $lines[$line_count];
		echo "\n";
		echo "page: " . $page . " Course/Program: " .  $this->print_lib->bold() . $year_level . " " . $academic_program . $this->print_lib->regular() . "\n";
		$page++;
	}	
	$line_count++;	
}
echo "\n\n";
echo "page: " . $page . " Course/Program: " .  $this->print_lib->bold() . $year_level . " " . $academic_program . $this->print_lib->regular() . "\n";
echo $this->print_lib->no_condensed();
echo $this->print_lib->char_form_feed();
?></div>