<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='3') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?> 
		<form id="student_emergency_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_emergency_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<div class="control-group formSep">
					<label for="emergency_notify" class="control-label">Person to Notify:</label>
					<div class="controls" id="emergency_notify_controls">
						<input type="text" name="emergency_notify" id="emergency_notify" class="input-xlarge" value="<?php if(isset($emergency_notify)) echo $emergency_notify; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="emergency_telephone" class="control-label">Telephone/Mobile No.:</label>
					<div class="controls" id="primary_school_controls">
						<input type="text" name="emergency_telephone" id="emergency_telephone" class="input-xlarge" value="<?php if(isset($emergency_telephone)) echo $emergency_telephone; ?>" />
					</div>
				</div>
				<!--  <div class="control-group formSep">
					<label for="emergency_email_address" class="control-label">Email Address:</label>
					<div class="controls" id="emergency_email_address_controls">
						<input type="text" name="emergency_email_address" id="emergency_email_address" class="input-xlarge" value="<?php if(isset($emergency_email_address)) echo $emergency_email_address; ?>" />
					</div>
				</div> 
				-->
				<div class="control-group formSep">
					<label for="emergency_address" class="control-label">Address:</label>
					<div class="controls" id="emmergency_address_controls">
						<div style="margin-top:6px" id="emergency_address_control_text"><strong><?php echo isset($emergency_address)?$emergency_address:""; ?></strong>&nbsp;<small><a href="javascript://" id="emergency_address_control"  class="edit_toggle" >Edit</a></small></div>
						<div id="emergency_address_control_controls"></div>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button id="update_educational_information" class="btn btn-primary" type="submit">Update Emergency Contact Information</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script>
$('#student_emergency_information_form').validate({
	onkeyup: false,
	errorClass: 'error',
	validClass: 'valid',
	rules: {
		emergency_notify: { required: true},
		emergency_email_address: { email: true},
	},
	highlight: function(element) {
		$(element).closest('div').addClass("f_error");
					setTimeout(function() {
						boxHeight()
					}, 200)
				},
	unhighlight: function(element) {
		$(element).closest('div').removeClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	errorPlacement: function(error, element) {
		$(element).closest('div').append(error);
	}
});
</script>


