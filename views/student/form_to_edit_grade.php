<?php //echo 'form to edit '. $update_history; die();?>
<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
</style>


<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">
	
</div>

  <div style="background:#FFF; ">
    <table width="30%" align="center" cellpadding="0" cellspacing="0" class="head1" style="width:42%;">
    <form action="<?php echo site_url('registrar/student/' . $this->uri->segment(3)); ?>" method="post" id="edit_grade_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value= "edit_grade"/>
  <input type="hidden" name="enrollment_id" value= "<?php print($enrollment_id) ?>"/>
  <input type="hidden" name="period" value= "<?php print($period) ?>"/>
  <input type="hidden" name="prev_grade" value= "<?php echo $prev_grade ?>"/>
  <input type="hidden" name="update_history" value= "<?php echo $this->db->escape(htmlspecialchars($update_history)); ?>"/>
  
  
	  <tr class="head">
        <td colspan="3" class="head"> EDIT GRADE </td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside" style="width:100%;">
           <tr>
            <td width="99" valign="middle">Term</td>
            <td width="8" valign="middle">:</td>
            <td width="109" valign="middle"> <?php print($term->term." ".$term->sy); ?> </td>
          </tr>	   
		  <tr>
            <td valign="middle">Catalog No. </td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print($enrollment_info->course_code); ?></td>
		  </tr>
		   <tr>
            <td valign="middle"> Time </td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print($enrollment_info->start_time."-".$enrollment_info->end_time); ?></td>
          </tr>
		    <tr>
            <td valign="middle"> Period </td>
            <td valign="middle">:</td>
            <td valign="middle" style="font-weight:bold;"><?php print($period); ?></td>
          </tr>
		   
		   <tr>
		     <td valign="middle">Previous Grade </td>
		     <td valign="middle">:</td>
		     <td valign="middle" style="font-weight:bold;"><?php 
			 $grades = array('INE', 'INC', 'NA','-','NC','inc', 'ine');
			 if(in_array($prev_grade, $grades)){
			  	print($prev_grade);
			 }else{	
			 	print(number_format($prev_grade,1)); 
			  }?></td>
			   	
	        </tr>
			 <tr>
		     <td valign="middle">New Grade </td>
		     <td valign="middle">:</td>
		    <td><input name="grade" type="text" class="form" style="font-family:Verdana, Geneva, sans-serif; font-size:11px; width:20px;" /></td>
	        </tr>
		
		  <tr>
  				<td align ="center"><input type="submit" name="edit_grade" id="edit_grade" value="Edit Grade!" /></td>
		</tr>
        </table></td>
      </tr>
  </form>
    </table>

  </div>
  
 <script>
$('#edit_grade').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to edit grade of student?");

	if (confirmed){
		$('#edit_grade_form').submit();
	}		
});
</script>

