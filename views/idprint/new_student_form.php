
<style>
	req {
		color: red;
		font-weight: bold;
		font-size: 18px;
	}
	
	.emergency_info {
		border: 1px solid #b7b4b7;
		background-color: #e4e4e4;
		border-radius: 5px;
	}
</style>

<h2 class="heading">Create a New Student Account</h2>

	<div class="row-fluid" style="width:50%; float:left;">
		<?php if(isset($message)): ?>
		<div class="alert alert-<?php echo $severity; ?>">
				<a class="close" data-dismiss="alert">&times;</a>
				<?php echo $message; ?>
		</div>
		<?php endif; ?>

		<div class="form-horizontal">
			<fieldset>
				<div class="control-group formSep">
					<label for="familyname" class="control-label">Family Name:<req>*</req> </label>
					<div class="controls">
						<div style="float:left;">
							<input type="text" name="familyname" id="lastname" placeholder="Family Name" class="input-xlarge search_student" autofocus />
						</div>
						<div style="float:left; padding-top:5px; padding-left:10px; font-weight:bold; color:#BA0303;" id="show_search_code_icon">
							
						</div>
					</div>
				</div>

				<div class="control-group formSep">
					<label for="firstname" class="control-label">First Name:<req>*</req> </label>
					<div class="controls">
						<input type="text" name="firstname" id="firstname" placeholder="First Name" class="input-xlarge search_student" value="<?php if(isset($firstname)) echo $firstname; ?>" autofocus />
					</div>
				</div>

				<div class="control-group formSep">
					<label for="middlename" class="control-label">Middle Name:<req>*</req> </label>
					<div class="controls">
						<input type="text" name="middlename" id="middlename"  placeholder="Middle Name" class="input-xlarge search_student"  value="<?php if(isset($middlename)) echo $middlename; ?>" />
					</div>
				</div>

				<div class="control-group formSep">
					<label for="dbirth" class="control-label">Date of Birth:<req>*</req> </label>
					<div class="controls">
						<div class="input-append date" id="student_dbirth" data-date-format="yyyy-mm-dd" >
							<input type="text" name="dbirth" id="dbirth" class="span6 search_student" readonly />
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="gender" class="control-label">Gender: </label>
					<div class="controls">
						<select id="gender" name="gender">
							<?php foreach ($this->config->item('genders') as $key=>$val): ?>
							<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

							<div class="emergency_info">
								<div class="control-group formSep">
									<label class="control-label" style="font-weight:bold;">Emergency Information:</label>
									<div class="controls">

									</div>
								</div>
								<div class="control-group formSep">
									<label for="emergency_notify" class="control-label">Person to Notify:<req>*</req></label>
									<div class="controls">
										<input type="text" name="emergency_notify" id="emergency_notify"  placeholder="Person to Notify" class="input-xlarge"  />
									</div>
								</div>
								<div class="control-group formSep">
									<label for="emergency_relation" class="control-label">Relation:</label>
									<div class="controls">
										<input type="text" name="emergency_relation" id="emergency_relation"  placeholder="Relationship to Person" class="input-xlarge"  />
									</div>
								</div>
								<div class="control-group formSep">
									<label for="emergency_telephone" class="control-label">Contact No.:<req>*</req></label>
									<div class="controls">
										<input type="text" name="emergency_telephone" id="emergency_telephone"  placeholder="Contact No." class="input-xlarge"  />
									</div>
								</div>
								<div class="control-group formSep">
									<label for="emergency_address" class="control-label">
										Address:<req>*</req><br>
										<button class="btn btn-primary new_address" style="font-size:11px; height:22px;padding:1px;">New Address?</button>
									</label>
									<div class="controls">
										<div id="country_group" style="float:left; padding-left:5px;">
											<?php 
												$data1['countries'] = $countries;
												$data1['type']      = "new_student";
												$this->load->view('common/places/select_countries', $data1);
											?>	
										</div>

										<div id="province_group" style="float:left; padding-left:5px;"></div> 

										<div id="town_group" style="float:left; padding-left:5px;"></div> 
								 
										<div id="brgy_group" style="float:left; padding-left:5px; padding-top:10px;"></div> 
										
									</div>
								</div>
									
							</div>

				<div class="control-group formSep">
					<label for="college" class="control-label">College: </label>
					<div class="controls">
						<select id="colleges" name="college" class="form-controls">
							<option value="">-- Select College --</option>
							<?php foreach ($colleges as $college): ?>
							<option value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="control-group formSep selected_programs" style="display:none;">
					<label for="programs" class="control-label">Program:<req>*</req> </label>
					<div class="controls" id="college_programs">

					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-success" name="register_student" id="register_student" value="Register Student!" disabled />
					</div>
				</div>	
			</fieldset>
		</div>
	</div>

	<div id="list_of_students" style="width:45%; float:left; margin:0 auto;">

	</div>

	
<script>
	$(".search_student").blur(function(){
			
		var lastname   = $('#lastname').val();
		var firstname  = $('#firstname').val();
		var middlename = $('#middlename').val();

		$('#show_search_code_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/search_student'); ?>",
			data: {"lastname": lastname,
					"firstname": firstname,
					"middlename": middlename,						
					"action": "search_student" },
			dataType: 'json',
			success: function(response) {													
				if (response.found) {
					$('#show_search_code_icon').html('Student of the same possible name found!'); 
					$('#list_of_students').html(response.output);
				} else {
					$('#show_search_code_icon').html(''); 	
					$('#list_of_students').html('');						
				}
			}
		});
		
	});
	
</script>

<script>
	$(document).ready(function() {
		$('#student_dbirth').datepicker();
		
	});
</script>


<script>
	$('#colleges').change(function(event){
		event.preventDefault();

		var colleges_id = $("select#colleges option:selected").attr('value'); 

		if (!colleges_id) {
			$('#register_student').prop("disabled", true);			
		}
		
		$.ajax({
			type: "POST",
			url: "<?php echo site_url($this->uri->segment(1).'/colleges');?>",
			data: {
					"action": 'list_programs',
					"colleges_id": colleges_id
					},
			cache: false,
			dataType: 'json',
			success: function(response) {
				if (colleges_id) {
					$(".selected_programs").show();
					$("#college_programs").html(response.output);
				} else {
					$(".selected_programs").hide();
				}
			}
		});
		
		
	});
</script>

<script>
	$("#lastname").keyup(function(){
		
		var lastname      = $('#lastname').val();
		var firstname     = $('#firstname').val();
		var middlename    = $('#middlename').val();
		var dbirth        = $('#dbirth').val();
		var acad_terms_id = $("select#academic_terms_id option:selected").attr('value'); 
		var e_person      = $('#emergency_notify').val();
		var e_contact     = $('#emergency_telephone').val();
		var e_address     = $("select#brgy_id option:selected").attr('value'); 

		var req_array     = [lastname,firstname,middlename,dbirth,e_person,e_contact,e_address,acad_terms_id];
		
			if (required_isEmpty(req_array) || lastname.trim().length == 0) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}

	});
	
	$("#firstname").keyup(function(){

		var lastname   = $('#lastname').val();
		var firstname  = $('#firstname').val();
		var middlename = $('#middlename').val();
		var dbirth     = $('#dbirth').val();
		var program_id = $("select#program_id option:selected").attr('value'); 
		var e_person   = $('#emergency_notify').val();
		var e_contact  = $('#emergency_telephone').val();
		var e_address  = $("select#brgy_id option:selected").attr('value'); 

		var req_array  = [lastname,firstname,middlename,dbirth,e_person,e_contact,e_address,program_id];

			if (required_isEmpty(req_array) || firstname.trim().length == 0) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}

	});

	$("#middlename").keyup(function(){
		
		var lastname   = $('#lastname').val();
		var firstname  = $('#firstname').val();
		var middlename = $('#middlename').val();
		var dbirth     = $('#dbirth').val();
		var program_id = $("select#program_id option:selected").attr('value'); 
		var e_person   = $('#emergency_notify').val();
		var e_contact  = $('#emergency_telephone').val();
		var e_address  = $("select#brgy_id option:selected").attr('value'); 

		var req_array  = [lastname,firstname,middlename,dbirth,e_person,e_contact,e_address,program_id];

			if (required_isEmpty(req_array) || middlename.trim().length == 0) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}

	});

	$("#dbirth").bind("keyup change", function(e){
		
		var lastname   = $('#lastname').val();
		var firstname  = $('#firstname').val();
		var middlename = $('#middlename').val();
		var dbirth     = $('#dbirth').val();
		var program_id = $("select#program_id option:selected").attr('value'); 
		var e_person   = $('#emergency_notify').val();
		var e_contact  = $('#emergency_telephone').val();
		var e_address  = $("select#brgy_id option:selected").attr('value'); 

		var req_array  = [lastname,firstname,middlename,dbirth,e_person,e_contact,e_address,program_id];

			if (required_isEmpty(req_array) || dbirth.trim().length == 0) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}

	});

	$("#emergency_notify").keyup(function(){
		
		var lastname   = $('#lastname').val();
		var firstname  = $('#firstname').val();
		var middlename = $('#middlename').val();
		var dbirth     = $('#dbirth').val();
		var program_id = $("select#program_id option:selected").attr('value'); 
		var e_person   = $('#emergency_notify').val();
		var e_contact  = $('#emergency_telephone').val();
		var e_address  = $("select#brgy_id option:selected").attr('value'); 

		var req_array  = [lastname,firstname,middlename,dbirth,e_person,e_contact,e_address,program_id];
		
			if (required_isEmpty(req_array) || e_person.trim().length == 0) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}

	});

	$("#emergency_telephone").keyup(function(){
		
		var lastname   = $('#lastname').val();
		var firstname  = $('#firstname').val();
		var middlename = $('#middlename').val();
		var dbirth     = $('#dbirth').val();
		var program_id = $("select#program_id option:selected").attr('value'); 
		var e_person   = $('#emergency_notify').val();
		var e_contact  = $('#emergency_telephone').val();
		var e_address  = $("select#brgy_id option:selected").attr('value'); 

		var req_array  = [lastname,firstname,middlename,dbirth,e_person,e_contact,e_address,program_id];
		
			if (required_isEmpty(req_array) || e_contact.trim().length == 0) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}

	});
	
	function required_isEmpty(req_array) {
		
		var aLen; 
		aLen = req_array.length;
		
		for (i = 0; i < aLen; i++) {	

			if (!req_array[i]) {
				return true;
			}
		}
		
		return false;
	}
		
</script>


<script>
	$('.new_address').click(function(event){
		
		$("#province_group").hide();
		$("#town_group").hide();
		$("#brgy_group").hide();
		$('#register_student').prop("disabled", true);
		$("#country_id option:first").attr("selected", true);

		$('.show_address_form').hide();
		
		$('#modal_new_address').modal('show');
		
		$.ajax({
			type: "POST",
			url: "<?php echo site_url($this->uri->segment(1).'/update_address');?>",
			data: {"action": 'list_countries'},
			cache: false,
			dataType: 'json',
			success: function(response) {    
				$("#select_country").html(response.output);
			}
		});
		
	});
		
</script>


	<div id="modal_new_address" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Add Address</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php														
							$this->load->view('address/show_address_form');
						?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary click_add_address">Add Address!</button>
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
			</div>
		</div>
	</div>

<script>

	$('.click_add_address').click(function(event){
				
		$('#modal_new_address').modal('hide');
		
		var country_id   =  $("select#select_country option:selected").attr('value'); 
		var province_id  = $("select#select_province option:selected").attr('value'); 
		var town_id      = $("select#select_town option:selected").attr('value'); 
		var brgy_id      = $("select#select_brgy option:selected").attr('value'); 

		var country_name = $("select#select_country option:selected").attr('country_name'); 

		$("#province_group").show();
		$("#town_group").show();
		$("#brgy_group").show();
		
		if (country_id == 'new_country') {
			var country_name  = $('#new_country_name').val();
			var province_name = $('#new_state_name1').val();
			var town_name     = $('#new_suburb_name1').val();
			var brgy_name     = $('#new_street_name1').val();

			$.ajax({
				type: "POST",
				url: "<?php echo site_url($this->uri->segment(1).'/update_address');?>",
				data: {"action": 'add_country',
						"country_name": country_name,
						"province_name": province_name,
						"town_name": town_name,
						"brgy_name": brgy_name},
				cache: false,
				dataType: 'json',
				success: function(response) {    
					$("#country_group").html(response.my_countries);
					$("#province_group").html(response.my_provinces);
					$("#town_group").html(response.my_towns);
					$("#brgy_group").html(response.my_brgys);

					$("#country_id").val(response.country_id);
					$("#province_id").val(response.province_id);
					$("#town_id").val(response.town_id);
					$("#brgy_id").val(response.brgy_id);
					$("#street_name").focus();

				}
			});

		} else if (province_id == 'new_province') {
			if (country_name == 'Philippines') {
				var province_name = $('#new_province_name').val();
				var town_name     = $('#new_town_name').val();
				var brgy_name     = $('#new_brgy_name').val();				
			} else {
				var province_name = $('#new_state_name2').val();
				var town_name     = $('#new_suburb_name2').val();
				var brgy_name     = $('#new_street_name2').val();
			}

			$.ajax({
				type: "POST",
				url: "<?php echo site_url($this->uri->segment(1).'/update_address');?>",
				data: {"action": 'add_province',
						"country_id": country_id,
						"province_name": province_name,
						"town_name": town_name,
						"brgy_name": brgy_name},
				cache: false,
				dataType: 'json',
				success: function(response) {    
					$("#country_group").html(response.my_countries);
					$("#province_group").html(response.my_provinces);
					$("#town_group").html(response.my_towns);
					$("#brgy_group").html(response.my_brgys);

					$("#country_id").val(response.country_id);
					$("#province_id").val(response.province_id);
					$("#town_id").val(response.town_id);
					$("#brgy_id").val(response.brgy_id);
					$("#street_name").focus();

				}
			});
			
		} else if (town_id == 'new_town') {
			if (country_name == 'Philippines') {
				var town_name     = $('#new_town_name2').val();
				var brgy_name     = $('#new_brgy_name2').val();				
			} else {
				var town_name     = $('#new_suburb_name3').val();
				var brgy_name     = $('#new_street_name3').val();
			}

			$.ajax({
				type: "POST",
				url: "<?php echo site_url($this->uri->segment(1).'/update_address');?>",
				data: {"action": 'add_town',
						"country_id": country_id,
						"province_id": province_id,
						"town_name": town_name,
						"brgy_name": brgy_name},
				cache: false,
				dataType: 'json',
				success: function(response) {    
					$("#country_group").html(response.my_countries);
					$("#province_group").html(response.my_provinces);
					$("#town_group").html(response.my_towns);
					$("#brgy_group").html(response.my_brgys);

					$("#country_id").val(response.country_id);
					$("#province_id").val(response.province_id);
					$("#town_id").val(response.town_id);
					$("#brgy_id").val(response.brgy_id);
					$("#street_name").focus();

				}
			});
			
		} else if (brgy_id == 'new_brgy') {
			if (country_name == 'Philippines') {
				var brgy_name = $('#new_brgy_name3').val();				
			} else {
				var brgy_name = $('#new_street_name4').val();
			}

			$.ajax({
				type: "POST",
				url: "<?php echo site_url($this->uri->segment(1).'/update_address');?>",
				data: {"action": 'add_brgy',
						"country_id": country_id,
						"province_id": province_id,
						"town_id": town_id,
						"brgy_name": brgy_name},
				cache: false,
				dataType: 'json',
				success: function(response) {    
					$("#country_group").html(response.my_countries);
					$("#province_group").html(response.my_provinces);
					$("#town_group").html(response.my_towns);
					$("#brgy_group").html(response.my_brgys);

					$("#country_id").val(response.country_id);
					$("#province_id").val(response.province_id);
					$("#town_id").val(response.town_id);
					$("#brgy_id").val(response.brgy_id);
					$("#street_name").focus();

				}
			});
			
		}
		
	});

</script>


	<form id="new_student_form" method="POST" class="form-horizontal">
		<?php echo $this->common->hidden_input_nonce(); ?>
	</form>

	<script>
		$('#register_student').click(function(e){
			e.preventDefault();

			var familyname  = $('#lastname').val();
			var firstname   = $('#firstname').val();
			var middlename  = $('#middlename').val();
			var dbirth      = $('#dbirth').val();
			var gender      = $('#gender').val();
			var e_person    = $('#emergency_notify').val();
			var e_relation  = $('#emergency_relation').val();
			var e_contact   = $('#emergency_telephone').val();
			var e_address   = $("select#brgy_id option:selected").attr('value'); 
			var st_name     = $('#street_name').val();

			var gender          = $("select#gender option:selected").attr('value'); 
			var academic_year   = $("select#academic_year option:selected").attr('value'); 
			var levels          = $("select#academic_terms_id option:selected").attr('value'); 
			var pre_yr_level    = $("select#pre_yr_level option:selected").attr('value'); 
			var kinder_yr_level = $("select#kinder_yr_level option:selected").attr('value'); 
			var gs_yr_level     = $("select#gs_yr_level option:selected").attr('value'); 
			var jh_yr_level     = $("select#jh_yr_level option:selected").attr('value'); 

				var confirmed = confirm('Continue to register new student?');

				if (confirmed){

					$('<input>').attr({
						type: 'hidden',
						name: 'familyname',
						value: familyname,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'firstname',
						value: firstname,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'middlename',
						value: middlename,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'dbirth',
						value: dbirth,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'gender',
						value: gender,
					}).appendTo('#new_student_form');		

					$('<input>').attr({
						type: 'hidden',
						name: 'e_person',
						value: e_person,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'e_relation',
						value: e_relation,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'e_contact',
						value: e_contact,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'e_address',
						value: e_address,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'street_name',
						value: st_name,
					}).appendTo('#new_student_form');		

					$('<input>').attr({
						type: 'hidden',
						name: 'academic_year',
						value: academic_year,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'levels',
						value: levels,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'pre_yr_level',
						value: pre_yr_level,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'kinder_yr_level',
						value: kinder_yr_level,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'gs_yr_level',
						value: gs_yr_level,
					}).appendTo('#new_student_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'jh_yr_level',
						value: jh_yr_level,
					}).appendTo('#new_student_form');		
					
					$('#new_student_form').submit();
				}
		
		});
		
	</script>
	