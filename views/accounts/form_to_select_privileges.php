<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
	width:100%;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>
 
<form method="post" id="list_priviliges">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value= "form_to_list_priviliges"/>
    <table align="center" cellpadding="0" cellspacing="0" class="head1" style="width:42%;">
      <tr class="head">
        <td colspan="3" class="head"> List of Privileges </td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside">	   
		  <tr>
            <td width="55" valign="middle">Privilege </td>
            <td width="4" valign="middle">:</td>
            <td width="122" valign="middle">
	   <select name="privilege_id" class="form">
		  <?php 
		  		foreach($privilege_items as $privilege_item){
					print("<option value=".$privilege_item->id.">".$privilege_item->scholarship."</option>");	
				
				}
		  ?>
	 </select>		</td>
        </tr>
   		   <td colspan="3" valign="middle" align="middle"><div align="center">
   		     <input type="submit" name="button2" id="list_priviliges" value="Continue!"  class="btn btn-success" />
	        </div></td>
   		 </tr>
        </table></td>
      </tr>
    </table>
</form>
 
