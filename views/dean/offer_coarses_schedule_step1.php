<h4 class="heading">Select Academic Term</h4>
<div class="row-fluid">
	<div class="span7">
		<form action="<?php 
			switch ($this->uri->segment(2)) {
				case 'offer_course_schedules':
					print(site_url('dean/offer_course_schedules'));
					break;
				case 'assign_faculty_to_schedule':
					print(site_url('dean/assign_faculty_to_schedule'));
					break;
				case 'dissolve_section':
					print(site_url('dean/dissolve_section'));
					break;
				case 'offer_schedule_slots':
					print(site_url('dean/offer_schedule_slots'));
					break;
				case 'view_course_offerings':
					print(site_url('dean/view_course_offerings'));
					break;
				case 'view_class_list':
					print(site_url('dean/view_class_list'));
					break;
				case 'masterlist':
					print(site_url('dean/masterlist'));
					break;
					
			}
			?>" method="post" class="form-horizontal">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" value="2" name="step" />
			
			
		<?php echo validation_errors();   ?>
		
			<fieldset>
				<div class="control-group">
					<label for="" class="control-label">Academic Term: </label>
						<div class="controls">
							<select id="academic_terms_id" name="academic_terms_id">
								<!--  <option value="">-- Select Academic Term --</option>  -->
		          	<?php
					foreach($academic_terms AS $academic_term) {
						if ($acad_id) {
							print("<option value=".$academic_term->id);
							if ($acad_id == $academic_term->id) {
								print(" selected");
							}
							print(">".$academic_term->term." ".$academic_term->sy."</option>");
						} else {
							print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");
						}	
					}
				?>
							</select>
						</div>
					</div>
					<div class="form-actions">
							<button class="btn btn-success" type="submit">Continue >></button>
					</div>	
			</fieldset>
		</form>
	</div>
<!-- </div> -->