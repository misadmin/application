
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Level</div>
			<div class="col2a">
				<input type="text" class="form-control" id="section_group_name" style="width:150px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Section</div>
			<div class="col2a">
				<input type="text" class="form-control" id="section_name" style="width:150px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Subject <span class="my_notes">*</span></div>
			<div class="col2a" id="schedule_courses_modal">

			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Time</div>
			<div class="col2a">
				<input type="time" id="start_time_modal" value="07:30" style="width:auto;" /> - 
				<input type="time" id="end_time_modal" value="08:30" style="width:auto;" />
			</div>
		</div>
		<div style="width:100%;">
			<div class="col1">Day</div>
			<div class="col2a" style="font-size:12px;">
				<div style="float:left; padding-top:3px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code1" value="Mon" class="day_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Monday
					</div>
				</div>
				<div style="float:left; padding-top:3px; padding-left:8px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Tue" class="day_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Tuesday
					</div>
				</div>
				<div style="float:left; padding-top:3px; padding-left:8px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Wed" class="day_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Wednesday
					</div>
				</div>
				<div style="float:left; padding-top:3px; padding-left:8px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Thu" class="day_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Thursday
					</div>
				</div>
				<div style="float:left; padding-top:3px; padding-left:8px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Fri" class="day_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Friday
					</div>
				</div>
			</div>
		</div>
		<div style="clear:both; width:100%; font-size:12px; font-weight:bold; color:#E40808; padding-top:15px;">
			* Subjects must be added first in the proper prospectus before they can be selected. Click <a href="<?php print(base_url('principal/prospectus_management')); ?>" class="btn btn-success">here</a> to open Propectus Management.
		</div>
		
	</div>


<script>
	$("#start_time_modal").blur(function(){
		
		var timeElements = $('#start_time_modal').val().split(":");    
		var theHour = parseInt(timeElements[0]);
		var theMintute = timeElements[1];
		var newHour = theHour + 1;
		
		if (newHour < 10) {
			var newHour1 = "0" + newHour;
		} else {
			var newHour1 = newHour;			
		}

		$('#end_time_modal').val(newHour1 + ":" + theMintute);
		
	});
</script>