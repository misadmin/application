<style>
	table.receipts 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
	tr.sub_total {font-weight: bold;}
</style>

<h5>Generate data for <?php echo $this->session->userdata('write_off_ending'); ?> write-off:</h5>
<div class="loading"></div>
<div id="table_here" class="span8">

<form id="frm_search" method="post" action=""  class="form-inline">
<?php 
	echo $this->common->hidden_input_nonce();
	$ar_wo_types = array('Deposit','Receivable',); 
?>

<fieldset>
	<div>
		    <div class="controls">
		    	<label for="department">Department: </label>
					<select id="department" name="department">
						<option value="">-- Select Department --</option>
						<?php foreach ($depts as $dept): ?>
							<option <?php echo ($dept->department==$this->input->post('department') ? 'selected="selected"' : ''); ?> value="<?php echo $dept->department; ?>"><?php echo $dept->department; ?></option>
						<?php endforeach; ?>
						<option <?php echo ('Unknown'==$this->input->post('department') ? 'selected="selected"' : ''); ?> value="Unknown">Unknown</option>						
					</select><br/>
				<label for="deposit_ar" style="width:70px">Type:</label>
					<select id="wo_type" name="wo_type">
						<?php foreach ($ar_wo_types as $wo_type): ?>
							<option <?php echo ($wo_type==$this->input->post('wo_type') ? 'selected="selected"' : ''); ?> value="<?php echo $wo_type; ?>"><?php echo $wo_type; ?></option>
						<?php endforeach; ?>					
					<!--<option value="Deposit">Deposit</option>	
						<option value="Receivable">Receivable</option>
					 -->	
					</select><br/>										
				<label for="department" style="width:70px">Target:</label>
					<select id="print_to" name="print_to">
						<option value="Screen">Screen</option>	
						<option value="CSV">CSV</option>	
					</select>					
				<button type="submit" name="action" value="Generate-List"  class="btn">Submit</button>
			</div>
	</div>

</fieldset>
</form>
<hr>


<?php 
	if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce')) && $this->input->post('action')=='Generate-List') {

?>
<form id="frm_writeoff" method="post">
<?php 
	echo $this->common->hidden_input_nonce();
?>


<table id="receipts_t" class="table table-condensed table-bordered table-striped receipts">
<thead>
	<tr>
	<th><?php if (count((array)$wos) > 0){ ?>
	 	<input class="checkboxes" type="checkbox" id="ckbCheckAll" />
	 	<?php } ?>
	</th>
	<th></th>
	<th>Department</th>	
	<th>Yr Level</th>		
	<th>ID No</th>
	<th>Student</th>
	<th>Last Transaction</th>
	<th><?php echo ($this->input->post('wo_type') == 'Deposit' ? 'Deposit':'Receivable'); ?> </th>
	</tr>
</thead>

<?php
	if (count($wos) < 1) {
		echo '<td class="center" colspan="8">No record found!</td>';
	}else{
		$row_num = 0;
		//print_r($rows);die();
		foreach($wos as $row){
			$row_num++;
			list($month, $day, $year) = explode(' ', $row->last_transaction);
			$last_transaction = $year; 
			//.'-'.$month.'-'.$day; 									
?>
			<input type="hidden" name="trans[<?php echo $row->payers_id;?>]" value="<?php echo $row->last_transaction2;?>" />
			<tr>
			<td style="text-align: center"><input class="checkboxes" type="checkbox" name="check[]" value="<?php echo $row->payers_id; ?>"></td>						
			<td class="center"><?php echo $row_num ?>
			<td class="left"><?php echo $row->department; ?> </td>
			<td class="center"><?php echo $row->year_level; ?> </td>
			<td class="center"><a target="_blank" href="<?php echo site_url($this->session->userdata('role') .  "/student/" . str_pad($row->idno,8,"0",STR_PAD_LEFT)); ?> ">  <?php echo $row->idno; ?> </a></td>
			<td class="left"><?php echo $row->student; ?> </td>
			<td class="center"><?php echo $row->last_transaction2; ?> </td>			
			<td class="right"><?php echo ($row->balance < 0 ? number_format(abs($row->balance),2) :  "(".number_format($row->balance,2).")" ); ?> </td>
			</tr>		

<?php
		} 		
	}
?>

</table>

<?php 
	if (count((array)$wos) > 0){ 
?>
	<input type="submit" name="action" value="Write-Off" class="btn btn-primary">
<?php 
	} 
?>



</form>
<?php } 
?>






</div>


<script>
$(document).ready(function(){
	$("#receipts_t").stickyTableHeaders();
    $("#receipts_t").stickyTableHeaders('updateWidth');
	
	$('#frm_search').validate({
		submitHandler: function(form) {

			if ($('#print_to').val()!='CSV')
				$('.loading').delay(5).fadeIn(100);
			
			form.submit();
		}
	});

    $("#ornum").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });	

    $("#ckbCheckAll").click(function () {
        $(".checkboxes").attr('checked', this.checked);
    });
    
	
		
});






</script>