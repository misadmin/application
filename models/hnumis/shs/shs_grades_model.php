<?php

	class Shs_grades_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		
		function Update_Midterm_Grades($enrollments_id=NULL,$midterm_grade=NULL) {
			$q1 = "UPDATE 
							enrollments
						SET 
							midterm_grade = {$this->db->escape($midterm_grade)}
						WHERE
							id = {$this->db->escape($enrollments_id)} ";
					
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			

		}
		

		function Update_Finals_Grades($enrollments_id=NULL,$finals_grade=NULL) {
			$q1 = "UPDATE 
							enrollments
						SET 
							finals_grade = {$this->db->escape($finals_grade)}
						WHERE
							id = {$this->db->escape($enrollments_id)} ";
					
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			

		}

		
		function Save_GradeSubmissionDate($period=NULL,$course_offerings_id=NULL) {
			
			switch ($period) {
				
				case 'Midterm':
				
					$q1 = "INSERT INTO
								grade_submission_dates (course_offerings_id,
														midterm_date)
								VALUES (
									{$this->db->escape($course_offerings_id)},
									NOW() )
							ON DUPLICATE KEY UPDATE 
									midterm_date = NOW() ";
									
					break;

				case 'Finals':
				
					$q1 = "INSERT INTO
								grade_submission_dates (course_offerings_id,
														finals_date)
								VALUES (
									{$this->db->escape($course_offerings_id)},
									NOW() ) 
							ON DUPLICATE KEY UPDATE 
									finals_date = NOW() ";
									
					break;
					
			}
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		}
		

		function Save_GradeConfirmDate($data) {
			
			switch ($data['period']) {
				
				case 'Midterm':
				
					$q1 = "INSERT INTO
								shs_course_offerings_confirmation (course_offerings_id,
																	midterm_confirm_by,
																	midterm_confirm_date)
								VALUES (
									{$this->db->escape($data['course_offerings_id'])},
									{$this->db->escape($data['midterm_confirm_by'])},
									NOW() )
							ON DUPLICATE KEY UPDATE 
									midterm_confirm_by = {$this->db->escape($data['midterm_confirm_by'])},
									midterm_confirm_date = NOW() ";
									
					break;

				case 'Finals':
				
					$q1 = "INSERT INTO
								shs_course_offerings_confirmation (course_offerings_id,
																	finals_confirm_by,
																	finals_confirm_date)
								VALUES (
									{$this->db->escape($data['course_offerings_id'])},
									{$this->db->escape($data['finals_confirm_by'])},
									NOW() ) 
							ON DUPLICATE KEY UPDATE 
									finals_confirm_by = {$this->db->escape($data['finals_confirm_by'])},
									finals_confirm_date = NOW() ";
									
					break;
					
			}
			
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		}

			
		function List_Student_Grades ($students_idno=NULL) {	
			$result = null;
			
			$q1 = "SELECT 
							a.academic_terms_id,
							CASE b.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
							END AS term, 
							CONCAT(c.end_year-1,'-',c.end_year) AS sy
						FROM 
							student_histories AS a,
							academic_terms AS b,
							academic_years AS c
						WHERE 
							a.academic_terms_id = b.id 
							AND b.academic_years_id = c.id
							AND a.year_level IN (11,12) 
							AND a.students_idno = {$this->db->escape($students_idno)} 
						ORDER BY 
							c.end_year DESC,
							b.term DESC ";
			//die($q1);
			//log_message("INFO", "List_Student_Grades  ==> ".print_r($q1,true)); // Toyet 11.9.2018

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();

				//log_message("INFO", "RESULT ==>  ".print_r($result,true));  // Toyet 11.9.2018

				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$myresult = $this->List_MyGrades($students_idno, $res->academic_terms_id);
						
						if ($myresult) {
							$result[$cnt]->grades = $myresult;
						}									
						$cnt++;
					}
				}	
			} 
			
			return $result;
			
		}		
		
		
		function List_MyGrades($students_idno=NULL, $academic_terms_id=NULL) {
			$result = null;
			
			$q1 = "SELECT distinct
							e.course_code,
							e.descriptive_title,
							CONCAT(f.lname,', ',f.fname) AS teacher,
							IF(d.midterm_confirm_date,b.midterm_grade,NULL) AS midterm_grade,
							IF(d.finals_confirm_date,b.finals_grade,NULL) AS finals_grade,
							d.midterm_confirm_date,
							d.finals_confirm_date,
							g.midterm_date,
							g.finals_date,
							b.id AS enrollments_id,
							IF(d.midterm_confirm_date AND d.finals_confirm_date,(b.midterm_grade+b.finals_grade)/2,NULL) AS final_grade
						FROM student_histories AS a
						left join enrollments AS b on b.student_history_id=a.id
						left join course_offerings AS c on c.id=b.course_offerings_id
						LEFT JOIN shs_course_offerings_confirmation AS d ON d.course_offerings_id = c.id
						LEFT JOIN grade_submission_dates AS g ON g.course_offerings_id = c.id 
						LEFT JOIN employees AS f ON f.empno = c.employees_empno 
						left join courses AS e on e.id=c.courses_id 
						left join prospectus_courses AS j on j.courses_id=e.id 
						left join prospectus_terms AS i on i.id=j.prospectus_terms_id 
						left join shs_type_prospectus_courses AS k on k.prospectus_courses_id=j.id 
						WHERE a.students_idno = {$this->db->escape($students_idno)} 
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
						ORDER BY 
							k.subject_types_id,
							e.course_code ";
			//die($q1);

			//log_message("INFO","LIST MY GRADES ==> ".print_r($q1,true)); // Toyet 11.9.2018

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		function List_Grades_For_Report_Cards($students_idno=NULL, $academic_terms_id=NULL) {
			$result = null;

			//log_message("INFO",print_r($academic_terms_id,true)); // Toyet 11.9.2018
			
			$q1 = "(SELECT 
							a.id, 
							a.description
						FROM 
							shs_subject_types AS a
						WHERE 
							a.id=1)
					UNION 
					(SELECT 
							a.id, 
							'Applied and Specialized Subjects'
						FROM 
							shs_subject_types AS a
						WHERE a.id in (2,3)
					LIMIT 1)
					UNION
					(SELECT 
							a.id, 
							'Others'
						FROM 
							shs_subject_types AS a
						WHERE 
							a.id = 4
					LIMIT 1)";

			// $q1 = "SELECT a.id, 
			// 			  a.description
			// 	   FROM shs_subject_types AS a";

			//die($q1);
			log_message("INFO","INDIVIDUAL STUDENTS GRADES");  // Toyet 1.21.2019
			log_message("INFO",print_r($q1,true));  // Toyet 1.21.2019

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();

				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$myresult = $this->Report_Card_Details($students_idno, $academic_terms_id, $res->id);
						
						if ($myresult) {
							$result[$cnt]->grades = $myresult;
						}									
						$cnt++;
					}
				}	
			} 

			//log_message("INFO", print_r($result,true)); //Toyet 1.23.2019
			
			return $result;
			
			
		}
		
		private function Report_Card_Details($students_idno=NULL, $academic_terms_id=NULL, $subject_types_id=NULL) {
			$result = null;
			
			switch ($subject_types_id) {
				case 1:
						$q1 = "SELECT 
										e.descriptive_title,
										IF(d.midterm_confirm_date,b.midterm_grade,NULL) AS midterm_grade,
										IF(d.finals_confirm_date,b.finals_grade,NULL) AS finals_grade,
										IF(d.midterm_confirm_date AND d.finals_confirm_date,(b.midterm_grade+b.finals_grade)/2,NULL) AS final_grade,
										h.remark
									FROM student_histories a
										left join enrollments AS b on b.student_history_id=a.id
										left join enrollments_remarks h on h.enrollments_id=b.id
										left join course_offerings AS c on c.id=b.course_offerings_id
										LEFT JOIN shs_course_offerings_confirmation AS d ON d.course_offerings_id = c.id
										LEFT JOIN grade_submission_dates AS g ON g.course_offerings_id = c.id 
										left join courses AS e on e.id=c.courses_id 
										left join prospectus_courses AS j on j.courses_id=e.id 
										left join prospectus_terms AS i on i.id=j.prospectus_terms_id 
										left join shs_type_prospectus_courses AS k on k.prospectus_courses_id=j.id 
									WHERE 
										a.students_idno = {$this->db->escape($students_idno)} 
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND k.subject_types_id = {$this->db->escape($subject_types_id)}
									GROUP BY 
										e.course_code ";
						break;
				case 2:
						$q1 = "SELECT 
										e.descriptive_title,
										IF(d.midterm_confirm_date,b.midterm_grade,NULL) AS midterm_grade,
										IF(d.finals_confirm_date,b.finals_grade,NULL) AS finals_grade,
										IF(d.midterm_confirm_date AND d.finals_confirm_date,(b.midterm_grade+b.finals_grade)/2,NULL) AS final_grade,
										h.remark
									FROM student_histories a
										left join enrollments AS b on b.student_history_id=a.id
										left join enrollments_remarks h on h.enrollments_id=b.id
										left join course_offerings AS c on c.id=b.course_offerings_id
										LEFT JOIN shs_course_offerings_confirmation AS d ON d.course_offerings_id = c.id
										LEFT JOIN grade_submission_dates AS g ON g.course_offerings_id = c.id 
										left join courses AS e on e.id=c.courses_id 
										left join prospectus_courses AS j on j.courses_id=e.id 
										left join prospectus_terms AS i on i.id=j.prospectus_terms_id 
										left join shs_type_prospectus_courses AS k on k.prospectus_courses_id=j.id 
									WHERE 
										a.students_idno = {$this->db->escape($students_idno)} 
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND k.subject_types_id in (2,3)
									GROUP BY 
										e.course_code ";
						break;	

				case 4:
						$q1 = "SELECT 
									e.descriptive_title,
									IF(d.midterm_confirm_date,b.midterm_grade,NULL) AS midterm_grade,
									IF(d.finals_confirm_date,b.finals_grade,NULL) AS finals_grade,
									IF(d.midterm_confirm_date AND d.finals_confirm_date,(b.midterm_grade+b.finals_grade)/2,NULL) AS final_grade,
									h.remark
									FROM student_histories a
										left join enrollments AS b on b.student_history_id=a.id
										left join enrollments_remarks h on h.enrollments_id=b.id
										left join course_offerings AS c on c.id=b.course_offerings_id
										LEFT JOIN shs_course_offerings_confirmation AS d ON d.course_offerings_id = c.id
										LEFT JOIN grade_submission_dates AS g ON g.course_offerings_id = c.id 
										left join courses AS e on e.id=c.courses_id 
										left join prospectus_courses AS j on j.courses_id=e.id 
										left join prospectus_terms AS i on i.id=j.prospectus_terms_id 
										left join shs_type_prospectus_courses AS k on k.prospectus_courses_id=j.id 
									WHERE 
										a.students_idno = {$this->db->escape($students_idno)} 
										AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
										AND k.subject_types_id >= {$this->db->escape($subject_types_id)}
								GROUP BY 
									e.course_code ";
						break;	
			}
			
			//die($q1);
			//log_message("INFO","REPORT CARD"); // Toyet 11.9.2018 
			//log_message("INFO",print_r($q1,true)); // Toyet 11.9.2018 

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		

		function Add_ReportCard_Remark($data) {

			$q1 = "INSERT INTO
							enrollments_remarks (
												enrollments_id,
												remark,
												encoded_by)
							VALUES (
								{$this->db->escape($data['enrollments_id'])},
								{$this->db->escape($data['remark'])},
								{$this->db->escape($data['encoded_by'])}) 
							ON DUPLICATE KEY UPDATE 
								remark = {$this->db->escape($data['remark'])},
								encoded_by = {$this->db->escape($data['encoded_by'])} ";
					
			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		
		}

		
	}

?>	