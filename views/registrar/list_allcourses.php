<?php 

/************************************************************************
 * TABLE LIB CHILD CLASS DEF
 ************************************************************************/

class Coarse_Table extends Table_lib{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function format_data_details($row){
		return '<a class="edit_course" href="'.$row['id'].'"><i class="icon-edit"></i>';
	}
}

$coarse_table = new Coarse_Table();

/************************************************************************
 * TABLE HEADER
************************************************************************/

$coarse_table->set_attributes(array('class'=>'table table-bordered table-hover','id'=>'all-coarses'));

$coarse_table->insert_head_row(array(
								array(	'id'=>'course_code',
										'value'=>'Course Code',
										'attributes' => array('width'=>'17%')
								),
								array(	'id'=>'descriptive_title',
										'value'=>'Descriptive Title',
										'attributes' => array('width'=>'39%')
								),
								array(	'id'=>'credit_units',
										'value'=>'Credit Units',
										'attributes' => array('style'=>'text-align:right')
								),
								array(	'id'=>'paying_units',
										'value'=>'Paying Units',
										'attributes' => array('style'=>'text-align:right')
								),
								array(	'id'=>'type_description',
										'value'=>'Course Type',
										'attributes' => array('style'=>'text-align:left')
								),
								array(
										'id'=>'details',
										'value'=>'Details',
										'attributes' => array('width'=>'8%','style'=>'text-align:center')
								)
						));

?>

<h2 class="heading"><?php echo $title ?></h2>

<div class="row-fluid">
	<div class="span10">
		<div class="row-fluid">
			<div class="span12">
				<!-- <div class="well pull-left" style="border-radius:0;padding:5px 17px">
					For Term : <?php echo $terms_sy->term." ".$terms_sy->sy ?>
				</div> -->
				<?php if (isset($dissolve_students)): ?>
					<a href="<?php echo site_url("dean/Print_DissolveStudents_pdf")?>" target="_blank" class="btn btn-link" style="line-height:28px">List Students <i class="icon-download-alt"></i></a>
				<?php endif; ?>
			</div>
		</div>
		<style>
			table > tbody > tr > td:nth-child(3),
			table > tbody > tr > td:nth-child(4) {
				text-align:right
			}
			table > tbody > tr > td:nth-child(6) {
				text-align:center;
			}
			table > tbody > tr > td:nth-child(6) > a > img{
				min-width:14px
			}
		</style>
		<div class="row-fluid">
			<div class="span12">
			<?php if($courses AND is_array($courses)): ?>
			<?php $coarse_table->insert_data($courses) ?>
			<?php $coarse_table->content() ?>
			<?php else: ?>
			<div class="alert">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>No Record!</strong> Try Refreshing this page.
			</div>
			<?php endif ?>
			</div>
		</div>
	</div>
</div>

<!-- div style="background:#FFF; width:70%;">
<div style="width:100%;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td align="right">
    <a class="add_course" href="#" style="text-decoration:none; font-size:12px; font-weight:bold; color:#666; font-family:Verdana, Geneva, sans-serif;">
    <img src="<?php print(base_url('assets/img/new_prospectus.gif')); ?>" width="25" height="27" align="absmiddle" style="vertical-align:middle; border:none" /> Create Course </a></td>
  </tr>
</table>

</div>
<div style="width:100%;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="17%" class="header" style="vertical-align:middle;">Course Code </th>
      <th width="39%" class="header" style="vertical-align:middle;">Descriptive Title </th>
      <th width="14%" class="header" style="vertical-align:middle;">Credit Units </th>
      <th width="17%" class="header" style="vertical-align:middle;">Paying Units </th>
      <th width="13%" class="header"  style="vertical-align:middle;">Details</th>
      </tr>
    </thead>
    <?php
		if ($courses) {
			foreach ($courses AS $course) {
	?>
    <tr>
      <td style="text-align:left; vertical-align:middle; "><?php print($course->course_code); ?></td>
      <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($course->credit_units); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($course->paying_units); ?></td>
      <td style="text-align:center; vertical-align:middle; ">
      <a class="edit_course" href="<?php print($course->id); ?>">
      <img src="<?php print(base_url('assets/img/prospectus_details.gif')); ?>" width="20" height="20" style="vertical-align:bottom; border:none" /></a></td>
      </tr>
    <?php
			}
		}
	?>
</table>
</div>
  </div -->
 
<form id="edit_course" action="<?php echo site_url("registrar/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="4" />
</form>
<form id="srcform2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" />
</form>
<script>
$().ready(function() {
	$('.course_id').click(function(event){
		event.preventDefault(); 
		var course_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_id',
		    value: course_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
	$('.edit_course').click(function(event){
		event.preventDefault(); 
		var course_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_id',
		    value: course_id,
		}).appendTo('#edit_course');
		$('#edit_course').submit();
	});
	$('#all-coarses').dataTable( {
	    "aoColumnDefs": [
	    { "bSortable": false, "aTargets": [ 4 ] },
	    { "bSearchable": false, "aTargets" : [ 2,3,4 ]}
	                   ] } );
});
</script>