<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='password') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
		<form id="passwords_form" method="post" action="" class="form-horizontal">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" name="action" value="change_password" />
			<fieldset>
				<div class="control-group formSep">
					<label for="newpassword1" class="control-label">New Password</label>
					<div class="controls">
						<h2 id="pass_generated" class="span3" style="font-family:serif"><?php if(isset($password)) echo $password;?></h2>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button id="generate_password" class="btn" type="button">Generate Password</button>
						<button id="update_password" class="btn btn-primary" disabled="disabled" type="submit">Update Password</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script>
	$('#generate_password').click(function(){
		var new_password = generatepass(<?php echo $this->config->item('password_length'); ?>);
		$('#pass_generated').text(new_password);
		$('#update_password').removeAttr('disabled');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'password',
		    value: new_password,
		}).appendTo('#passwords_form');
	});
	
	var keylist = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@%^&*';
	function generatepass(length){
		temp='';
		for (i=0; i<length; i++)
			temp+=keylist.charAt(Math.floor(Math.random()*keylist.length));
		return temp
	}
</script>