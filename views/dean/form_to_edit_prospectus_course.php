<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<form action="<?php echo site_url('dean/prospectus');?>" method="post" id = "edit_prospectus_course_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="16" />
	<input type="hidden" name="prospectus_course_id" value="<?php print($prospectus_course_details->id); ?>" />

 <div style="background:#FFF;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:45%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head">
  EDIT PROSPECTUS COURSE
  </td>
  </tr>
  <tr>
    <td colspan="3">
  
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
<tr>
  <td colspan="3" align="left" valign="middle" style="font-weight:bold; font-size:14px;">
  <?php 
  		print($prospectus->description." - ".$prospectus->effective_year); 
  		print("<br>".$term->y_level." - ".$term->term); 
  ?></td>
  </tr>
<tr>
  <td width="71" align="left" valign="middle">Course</td>
  <td width="3" align="left" valign="middle">:</td>
  <td width="126" align="left" valign="middle">
  <?php
  		print($prospectus_course_details->course_code);	
  ?>
  </select></td>
</tr>
<tr>
  <td align="left" valign="middle">Elective?</td>
  <td align="left" valign="middle">:</td>
  <td align="left" valign="middle">
  <?php if($prospectus_course_details->elective == 'N'){ ?>
  		<input type="radio" name="elective" id="radio" value="Y"/>
    	Yes 
      	<input name="elective" type="radio" id="radio2" value="N" checked="checked" />
      	No
	<?php } else{ ?>	
	    <input type="radio" name="elective" id="radio" value="Y" checked="checked" />
    	Yes 
      	<input name="elective" type="radio" id="radio2" value="N" />
      	No
	<?php } ?>	
  </td>
</tr>
<tr>
  <td align="left" valign="middle">Major?</td>
  <td align="left" valign="middle">:</td>
  <td align="left" valign="middle">
    <?php if($prospectus_course_details->is_major == 'Y'){ ?>
		<input name="is_major" type="radio" id="radio3" value="Y" checked="checked" />
    		Yes
	   	<input name="is_major" type="radio" id="radio4" value="N" />
    	    No
	<?php }else{ ?>			
		<input name="is_major" type="radio" id="radio3" value="Y" />
    		Yes
	   	<input name="is_major" type="radio" id="radio4" value="N" checked="checked" />
    		No
	<?php } ?>	
	</td>
</tr>
<tr>
  <td align="left" valign="middle">Bracketed?</td>
  <td align="left" valign="middle">:</td>
  <td align="left" valign="middle">
  <?php if($prospectus_course_details->is_bracketed == 'Y'){ ?>
 	 <input name="is_bracketed" type="radio" id="radio5" value="Y" checked="checked"/>
   		 Yes
   	 <input name="is_bracketed" type="radio" id="radio6" value="N" />
   		 No
  <?php } else { ?>	 
      <input name="is_bracketed" type="radio" id="radio5" value="Y" />
   		 Yes
   	 <input name="is_bracketed" type="radio" id="radio6" value="N" checked="checked"/>
   		 No
  
   <?php } ?>
	 </td>
</tr>
<tr>
  <td>Cut-off Grade </td>
  <td>:</td>
  <td><input name="cutoff_grade" type="text" class="form" id="cutoff_grade" size="5" maxlength="5" 
  			value = <?php  if($prospectus_course_details->cutoff_grade){
							 print($prospectus_course_details->cutoff_grade);
							
							}else {print('');} ?>	 ></td>
</tr>
<tr>
  <td>Number of Retakes </td>
  <td>:</td>
  <td><input name="num_retakes" type="text" class="form" id="cutoff_grade" size="5" maxlength="5" 
      value = <?php  print($prospectus_course_details->num_retakes);?> ></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><input type="submit" name="edit_prospectus_course" id="edit_prospectus_course" value="Edit Course details!"  class="btn btn-success" /></td>
</tr>
    </table>
    </td>
    </tr>
    </table>
  </div>

</form>

<script>
$('#edit_prospectus_course').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update prospectus course?");

	if (confirmed){
		$('#edit_prospectus_course_form').submit();
	}
});
</script>
