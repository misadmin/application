<h2 class="heading">Transcript of Records from Other Schools</h2>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">
		

 </div>

<?php
	if ($other_schools) {
		foreach ($other_schools AS $other) {
?>
<div style="width:100%; margin-bottom:5px; ">
<table border="0" cellspacing="0" cellpadding="3" style="width:100%;">
  <tr>
    <td width="72%" align="left" style="font-size:16px; font-weight:bold;">
    	<?php 
    		print("<span style='font-size:18px; padding-right:20px;'>".$other->program."</span><br>");
    		print("<span style='font-size:14px; font-style:italic;'>".$other->term." - ".$other->school."</span><br>");
    		print("<span style='font-size:14px; font-style:italic;'>".$other->school_address."</span>");	
     	?>
     </td>
  </tr>
</table>

</div>

<div style="width:100%; margin-bottom:40px;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="10%" class="head" style="vertical-align:middle;">Catalog No.</th>
      <th width="35%" class="head" style="vertical-align:middle;">Descriptive Title</th>
      <th width="6%" class="head" style="vertical-align:middle;">Units</th>
      <th width="6%" class="head" style="vertical-align:middle;">Final Grade</th>
      <th width="37%" class="head" style="vertical-align:middle;">Remarks</th>
    </tr>
    </thead>
    <?php
		$courses = $this->OtherSchools_Model->ListCourses($other->id);
		if ($courses) {
			foreach ($courses AS $course) {
	?>
    <tr>
      <td style="text-align:left; vertical-align:middle; "><?php print($course->catalog_no); ?> </td>
      <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($course->units); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($course->final_grade); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($course->remarks); ?></td>
      
      
      </tr>
    <?php
			}
		}
	?>

</table>
</div>
<?php
	}
}
?>
   
