<?php

	class shs_student_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		
		/*
		 * @added: 11/2/2016
		 * @author: genes
		 * @NOTE: this function is performed by rsclerk
		 */
		
		public function new_student() {
		
			$this->CI->load->model('hnumis/shs/track_strand_model');
			$this->CI->load->model('hnumis/shs/shs_student_model');
			$this->CI->load->model('academic_terms_model');
				
			$data['strands']      = $this->CI->track_strand_model->ListStrandsByLatestProspectus();
			$data['acad_terms']   = $this->CI->academic_terms_model->academic_terms_incoming();
			$data['current_term'] = $this->CI->academic_terms_model->getCurrentAcademicTerm();

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
				
			switch ($action) {
				case 'mainpage':
							
						break;
						
				case 'add_new_student':

					if ($this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))){
						
						$data['lname']         = $this->CI->input->post('lastname');
						$data['fname']         = $this->CI->input->post('firstname');
						$data['mname']         = $this->CI->input->post('middlename');
						$data['dbirth']        = $this->CI->input->post('dbirth');
						$data['gender']        = $this->CI->input->post('gender');
						$data['student_type']  = 'H';

						$data['academic_terms_id'] = $this->CI->input->post('academic_terms_id');
						$data['prospectus_id']     = $this->CI->input->post('prospectus_id');
						$data['year_level']        = $this->CI->input->post('grade_level');
						$data['inserted_by']       = $this->CI->session->userdata('empno');
						
						//for the phone_numbers table
						$data['phone_type']	   = 'mobile';
						$data['owner_type']	   = 'student';
						$data['phone_number']  = $this->CI->input->post('contact_no');
						$data['is_primary']	   = 'Y';
						
						$this->CI->db->trans_start();

						if ($this->CI->shs_student_model->AddNewStudent_ToSHSStudent($data)) {
							$this->CI->db->trans_complete();
							$this->CI->content_lib->set_message('Student successfully added to database with ID Number: <a href="'. site_url('rsclerk/student/' . $this->CI->shs_student_model->get_students_idno()) .'">' . $this->CI->shs_student_model->get_students_idno() . "</a>", 'alert-success');
						} else {
							$this->CI->db->trans_rollback();
							$this->CI->content_lib->set_message('Error adding student to database!', 'alert-error');
						}
					
					} else {
						$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');	
					}
					
					break;

				case 'search_student':
					
					$data['lname'] = $this->CI->input->post('lastname');
					$data['fname'] = $this->CI->input->post('firstname');
					$data['mname'] = $this->CI->input->post('middlename');

					$data['students'] = $this->CI->shs_student_model->SearchStudent($data);
					$data['percent']  = "99%";
					
					if ($data['students']) {
						$my_return['found'] = TRUE;
 					} else {
						$my_return['found'] = FALSE;
					}
										
					$my_return['output'] = $this->CI->load->view('shs/student/list_of_students', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'search_lrn': 
					
					$data['lrn'] = $this->CI->input->post('lrn');

					$data['students'] = $this->CI->shs_student_model->SearchLRN($data);
					$data['percent']  = "99%";
					
					if ($data['students']) {
						$my_return['found'] = TRUE;
 					} else {
						$my_return['found'] = FALSE;
					}
										
					$my_return['output'] = $this->CI->load->view('shs/student/list_of_students', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
			}
				
			$this->CI->content_lib->enqueue_body_content('shs/student/new_student_form', $data);
			$this->CI->content_lib->content();
				
		}
		
		
		function process_student_action($idnum) {
			
			$this->CI->load->model('hnumis/shs/shs_student_model');
			$this->CI->load->model('Academic_terms_model');
			$this->CI->load->model('teller/teller_model');
			$this->CI->load->model('financials/Payments_Model');
			$this->CI->load->model('hnumis/shs/track_strand_model');
			$this->CI->load->model('hnumis/shs/section_model');
			$this->CI->load->model('hnumis/shs/shs_grades_model');
			$this->CI->load->model('hnumis/shs/behavior_model');
			$this->CI->load->model('hnumis/shs/attendance_model');
			$this->CI->load->model('hnumis/prospectus_model');

			$data['idnum']      = $idnum;
			$data['term']       = $this->CI->Academic_terms_model->current_academic_term(); 
			$current_history    = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $data['term']->id);  

			$data['can_update_values'] = FALSE; //used to check if can update behavior values; default is false			

			if ($this->CI->shs_student_model->is_SHS_Student($idnum)) {
				$data['show_register_tab'] = TRUE;
			} else {
				$data['show_register_tab'] = FALSE;
			}

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
				
			switch ($action) {
							
				case 'mainpage':
						
					break;
							
				case 'register_old_student':
					$data['students_idno']     = $this->CI->input->post('students_idno');
					$data['academic_terms_id'] = $this->CI->input->post('academic_terms_id');
					$data['year_level']        = $this->CI->input->post('grade_level');
					$data['prospectus_id']     = $this->CI->input->post('prospectus_id');
					$data['inserted_by']       = $this->CI->session->userdata('empno');
					
					if ($this->CI->shs_student_model->AddStudent_ToHistories($data)) {
						$my_return['success']      = TRUE;
						$current_history           = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $this->CI->input->post('academic_terms_id'));  
						$stud_data['student']      = $this->CI->shs_student_model->getStudentInfo($current_history->id);
						$my_return['student_info'] = $this->CI->load->view('shs/student/student_info_header', $stud_data, TRUE);
						$data['is_registered']     = TRUE;
					} else {
						$my_return['success']      = FALSE;
						$data['is_registered']     = FALSE;
						$data['is_enrolled']       = FALSE;	
						$data['is_sectioned']      = FALSE;
					}
					
					$current_history     = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $this->CI->input->post('academic_terms_id'));  

					$data['active_tab']  = "enrollment";
					$data['active_tab1'] = "register";
					$data['is_enrolled']   = FALSE;	
					$data['is_sectioned']  = FALSE;
					$data['school_days']   = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('academic_terms_id'));
					$data['present_days']  = $this->CI->attendance_model->ListPresentDays($this->CI->input->post('academic_terms_id'), $current_history->id);
					$data['grade_reports'] = $this->CI->shs_grades_model->List_Grades_For_Report_Cards($idnum, $this->CI->input->post('academic_terms_id')); 

					$data['my_student']    = array('name'=>$stud_data['student']->lname_first,
													'gender'=>$stud_data['student']->gender,
													'age'=>$stud_data['student']->age,
													'grade'=>$current_history->grade,
													'section'=>$current_history->section,
													'lrn'=>$current_history->lrn,
													'block_adviser'=>$current_history->block_adviser,
												);		

					break;
					
				case 'enroll_student':
						
					if ($this->CI->shs_student_model->UpdateCan_Enroll_StudentHistories($this->CI->input->post('student_histories_id'))) {
						$my_return['success']      = TRUE;
						$stud_data['student']      = $this->CI->shs_student_model->getStudentInfo($this->CI->input->post('student_histories_id'));
						$my_return['student_info'] = $this->CI->load->view('shs/student/student_info_header', $stud_data, TRUE);
						$data['is_registered']     = TRUE;
						$data['is_enrolled']       = TRUE;	
					} else {
						$my_return['success']      = FALSE;
						$data['is_registered']     = FALSE;
						$data['is_enrolled']       = FALSE;	
						$data['is_sectioned']      = FALSE;
					}

					$current_history       = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $stud_data['student']->academic_terms_id);  
					
					$data['active_tab']    = "enrollment";
					$data['active_tab1']   = "enroll_student";
					$data['is_sectioned']  = FALSE;
					$data['school_days']   = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('term_id'));
					$data['present_days']  = $this->CI->attendance_model->ListPresentDays($this->CI->input->post('term_id'), $current_history->id);
					$data['grade_reports'] = $this->CI->shs_grades_model->List_Grades_For_Report_Cards($idnum, $this->CI->input->post('term_id')); 

					$data['my_student']    = array('name'=>$stud_data['student']->lname_first,
													'gender'=>$stud_data['student']->gender,
													'age'=>$stud_data['student']->age,
													'grade'=>$current_history->grade,
													'section'=>$current_history->section,
													'lrn'=>$current_history->lrn,
													'block_adviser'=>$current_history->block_adviser,
												);		
					
					break;

				case 'assign_to_section':
						
					$data['student_histories_id'] = $this->CI->input->post('student_history_id');
					$data['block_sections_id']    = $this->CI->input->post('block_sections_id');
					
					if ($this->CI->shs_student_model->AssignToSection_StudentHistories($data)) {
						$my_return['success']      = TRUE;
						$stud_data['student']      = $this->CI->shs_student_model->getStudentInfo($this->CI->input->post('student_history_id'));
						$my_return['student_info'] = $this->CI->load->view('shs/student/student_info_header', $stud_data, TRUE);
						$data['is_registered']     = TRUE;
						$data['is_enrolled']       = TRUE;	
						$data['is_sectioned']      = TRUE;
					} else {
						$my_return['success']      = FALSE;
						$data['is_registered']     = FALSE;
						$data['is_enrolled']       = FALSE;	
						$data['is_sectioned']      = FALSE;
						$my_return['student_info'] = NULL;
					}

					$current_history     = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $stud_data['student']->academic_terms_id);  

					$data['active_tab']  = "enrollment";
					$data['active_tab1'] = "assign_section";
					
					$data['school_days']   = $this->CI->attendance_model->ListSchoolDays($stud_data['student']->academic_terms_id);
					$data['present_days']  = $this->CI->attendance_model->ListPresentDays($stud_data['student']->academic_terms_id, $current_history->id);
					$data['grade_reports'] = $this->CI->shs_grades_model->List_Grades_For_Report_Cards($idnum, $stud_data['student']->academic_terms_id); 

					$data['my_student']    = array('name'=>$stud_data['student']->lname_first,
													'gender'=>$stud_data['student']->gender,
													'age'=>$stud_data['student']->age,
													'grade'=>$current_history->grade,
													'section'=>$current_history->section,
													'lrn'=>$current_history->lrn,
													'block_adviser'=>$current_history->block_adviser,
												);
					break;

				case 'transfer_to_section':
						
					$data['student_histories_id'] = $this->CI->input->post('student_history_id');
					$data['block_sections_id']    = $this->CI->input->post('block_sections_id');
					
					if ($this->CI->shs_student_model->AssignToSection_StudentHistories($data)) {
						$my_return['success']      = TRUE;
						$stud_data['student']      = $this->CI->shs_student_model->getStudentInfo($this->CI->input->post('student_history_id'));
						$my_return['student_info'] = $this->CI->load->view('shs/student/student_info_header', $stud_data, TRUE);
					} else {
						$my_return['success'] = FALSE;
					}

					$current_history     = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $stud_data['student']->academic_terms_id);  
					$data['active_tab']  = "transfer_student";
					$data['active_tab1'] = "to_section";
					
					$data['is_registered']     = TRUE;
					$data['is_enrolled']       = TRUE;	
					$data['is_sectioned']      = TRUE;
					
					break;
					
				case 'search_block_sections':
					$data['to_sections'] = $this->CI->section_model->ListSections($this->CI->input->post('grade_level'),$this->CI->input->post('academic_terms_id'),$this->CI->input->post('strand_id'));

					$my_return['output'] = $this->CI->load->view('shs/tracks/list_of_sections', $data, TRUE);
						
					echo json_encode($my_return,JSON_HEX_APOS);
					
					return;

				case 'transfer_to_strand':
						
					$data['student_histories_id'] = $this->CI->input->post('student_history_id');
					$data['prospectus_id']        = $this->CI->input->post('prospectus_id');
					$data['block_sections_id']    = $this->CI->input->post('block_sections_id');
					
					if ($this->CI->shs_student_model->AssignToStrand_StudentHistories($data)) {
						$my_return['success']      = TRUE;
						$stud_data['student']      = $this->CI->shs_student_model->getStudentInfo($this->CI->input->post('student_history_id'));
						$my_return['student_info'] = $this->CI->load->view('shs/student/student_info_header', $stud_data, TRUE);
					} else {
						$my_return['success'] = FALSE;
					}

					$current_history     = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $stud_data['student']->academic_terms_id);  
					$data['active_tab']  = "transfer_student";
					$data['active_tab1'] = "to_strand";
					
					$data['is_registered']     = TRUE;
					$data['is_enrolled']       = TRUE;	
					$data['is_sectioned']      = TRUE;

					break;
					
				case 'change_term_for_class_sched':
				
					$current_history            = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $this->CI->input->post('term_id'));  
				    $data['class_schedules']    = $this->CI->shs_student_model->ListClassSchedules($current_history->id); 
					$data['block_section_data'] = $this->CI->section_model->getSection($current_history->block_sections_id); 
					
					$my_return['output'] = $this->CI->load->view('shs/student/student_class_schedules_with_teacher', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;
					
				case 'change_term_for_assessment':

					$current_history = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $this->CI->input->post('term_id'));  
					$data['assess']  = $this->StudentAssessments($current_history);  //This code is for the assessments
				
					$my_return['output'] = $this->CI->load->view('shs/student/student_assessment_details', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;
					
				case 'change_student_grade': //this is performed by Registrar only!

					$this->CI->load->model('hnumis/shs/shs_grades_model');
					
					switch ($this->CI->input->post('period')) {
						
						case 'Midterm':
							if ($this->CI->shs_grades_model->Update_Midterm_Grades($this->CI->input->post('enrollments_id'),$this->CI->input->post('new_grade'))) {
								$my_return['success'] = TRUE;
							} else {
								$my_return['success'] = FALSE;								
							}
							
							break;
							
						case 'Finals':
							if ($this->CI->shs_grades_model->Update_Finals_Grades($this->CI->input->post('enrollments_id'),$this->CI->input->post('new_grade'))) {
								$my_return['success'] = TRUE;
							} else {
								$my_return['success'] = FALSE;								
							}	

							break;
					}
					
					$data['grades']   = $this->CI->shs_grades_model->List_Student_Grades($idnum); 
					$data['can_edit'] = TRUE;
					$data['idnum']    = $idnum;
					
					$my_return['output'] = $this->CI->load->view('shs/registrar/student_grades_for_change', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;
				
				case 'change_term_for_obeserved_values':
					
					$selected_history = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $this->CI->input->post('term_id'));  

					$data['student_histories_id'] = $selected_history->id;
					$data['observed_values']      = $this->CI->behavior_model->ListObservedValues($selected_history->id);
					$data['selected_history'] = $selected_history;
					log_message("INFO",print_r($selected_history,true)); // Toyet 10.24.2018
				
					if ($this->CI->session->userdata('role') == 'classadviser') { 
						
						if ($this->CI->input->post('term_id') == $data['term']->id) {
							$data['can_update_values'] = TRUE;
						} else {
							$data['can_update_values'] = FALSE;
						}

					}

					// Emergency Patch to enable ClassAdviser to encode observed values...
					// Instated: 10.24.2018
					// Remove  : 11.05.2018
					$data['can_update_values'] = TRUE;
					
					$my_return['output'] = $this->CI->load->view('shs/class_adviser/observed_values', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;

				case 'change_term_for_attendance':
					
					$selected_history = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $this->CI->input->post('term_id'));  

					$data['school_days'] = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('term_id'));

					$data['present_days'] = $this->CI->attendance_model->ListPresentDays($this->CI->input->post('term_id'),$selected_history->id);
				
					$my_return['output'] = $this->CI->load->view('shs/class_adviser/attendance', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;

				case 'change_term_for_remarks': //performed by Class Adviser Only!
					
					$data['grades']        = $this->CI->shs_grades_model->List_MyGrades($idnum, $this->CI->input->post('term_id')); 
					$data['selected_term'] = $this->CI->input->post('term_id');

					if ($this->CI->input->post('term_id') == $data['term']->id) {
						$data['can_update_remark'] = TRUE;
					} else {
						$data['can_update_remark'] = FALSE;
					}

					$my_return['output'] = $this->CI->load->view('shs/class_adviser/submit_remarks_form', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;

				case 'assign_lrn':  //performed by rsclerk
						
					$data['students_idno'] = $idnum;
					$data['lrn']           = $this->CI->input->post('lrn');
					$data['encoded_by']    = $this->CI->session->userdata('empno');
								
					if ($this->CI->shs_student_model->Assign_LRN($data)) {
						$my_return['success'] = TRUE;
						$student     = $this->CI->shs_student_model->get_LRN($idnum);
						$data['lrn'] = $student->lrn;
					} else {
						$my_return['success'] = FALSE;								
					}	

					$my_return['output'] = $this->CI->load->view('shs/student/assign_lrn', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);

					return;
					
				case 'change_term_generate_report_card': //performed by Registrar
				
					$selected_history        = $this->CI->shs_student_model->get_StudentHistory_id($idnum, $this->CI->input->post('term_id'));  
					$data['grade_reports']   = $this->CI->shs_grades_model->List_Grades_For_Report_Cards($idnum, $this->CI->input->post('term_id')); 
					$data['observed_values'] = $this->CI->behavior_model->ListObservedValues($selected_history->id);
					$data['school_days']     = $this->CI->attendance_model->ListSchoolDays($this->CI->input->post('term_id'));
					$data['present_days']    = $this->CI->attendance_model->ListPresentDays($this->CI->input->post('term_id'), $selected_history->id);
					$student                 = json_decode($this->CI->input->post('student'));
					//$studentname 			 = utf8_decode($student->name);
					//log_message("INFO",print_r($studentname,true));  //toyet 4.17.2018
					//log_message("INFO",print_r($student->name,true));  //toyet 4.17.2018
					$data['my_student']      = array('name'=>$student->name,
													'gender'=>$student->gender,
													'age'=>$student->age,
													'grade'=>$selected_history->grade,
													'section'=>$selected_history->section,
													'lrn'=>$selected_history->lrn,
													'block_adviser'=>$selected_history->block_adviser,
												);
					$my_return['output'] = $this->CI->load->view('shs/registrar/generate_report_card', $data, TRUE);

					echo json_encode($my_return,JSON_HEX_APOS);
							
					return;
					
				case 'download_report_card':

					$data1['grade_reports']   = json_decode($this->CI->input->post('grade_reports'));
					$data1['observed_values'] = json_decode($this->CI->input->post('observed_values'));
					$data1['school_days']     = json_decode($this->CI->input->post('school_days'));
					$data1['present_days']    = json_decode($this->CI->input->post('present_days'));
					$data1['student']         = json_decode($this->CI->input->post('student'));
					$data1['term_text']       = $this->CI->input->post('term_text');
					$data1['fullwidth']       = FALSE;
					$data1['student_idno']    = $idnum;
					$data1['strand']          = $current_history->strand;
					
					log_message("INFO", '  workbench   =>>'.print_r($data1,true)); //toyet 4.19.2018

					$this->generate_report_card_pdf($data1);
					
					return;

				case 'prospectus_to_use':

					$student_hist_id = $this->CI->input->post('student_history_id');
					$prospectus_id = $this->CI->input->post('prospectus_id');

					//log_message("INFO",$student_hist_id.' '.$prospectus_id);

					$this->CI->shs_student_model->saveNewProspectusID($student_hist_id,$prospectus_id);

					return;

				case 'withdraw_enrollment':

					//log_message("INFO","nakaabot man...");

					$student_hist_id = $this->CI->input->post('student_history_id');
					$idnum = $this->CI->input->post('students_idno');

					//log_message("INFO",'TOTALLY WITHDRAWN  ==> '.$student_hist_id.' '.$idnum);

					$this->CI->shs_student_model->totallyWithdrawEnrollment($student_hist_id);

					return;
			}
			
			$data['term']       = $this->CI->academic_terms_model->getCurrentAcademicTerm(); 

			$result2            = $this->CI->teller_model->get_student($idnum);  
			$data['payments']   = $this->CI->Payments_Model->ListStudentPayments($result2->payers_id); 
			$data['strands']    = $this->CI->track_strand_model->ListStrandsByLatestProspectus();
			
			$data['acad_terms'] = $this->CI->shs_student_model->My_AcadTerms_toRegister($idnum);

			$data['terms_to_enroll']    = $this->CI->shs_student_model->ListTermsToEnroll($idnum);

			$data['block_sections']     = NULL;
			$data['current_register']   = FALSE;
					
			$data['student_histories_id'] = $current_history->id;

			if ($current_history->academic_terms_id == $data['term']->id) {
				$data['current_register'] = TRUE;
			}

			if($current_history->can_enroll == 'Y') {
				$data['is_enrolled']           = TRUE;
				$data['to_sections']           = $this->CI->section_model->ListSections($current_history->year_level,$current_history->academic_terms_id,$data['strands'][0]->academic_programs_id);
				$data['current_prospectus_id'] = $current_history->prospectus_id;
			} else {
				$data['is_enrolled'] = FALSE;
				$data['to_sections'] = NULL;
				$data['current_prospectus_id'] = NULL;				
			}
						
			if (is_null($current_history->block_sections_id)) {
				$data['is_sectioned'] = FALSE;
				$data['block_sections'] = $this->CI->section_model->ListSections($current_history->year_level,$current_history->academic_terms_id,$current_history->academic_programs_id);
			} else {
				$data['is_sectioned'] = TRUE;
				$data['transfer_sections'] = $this->CI->section_model->ListSections($current_history->year_level,$current_history->academic_terms_id,$current_history->academic_programs_id);
				$data['current_section_id']= $current_history->block_sections_id; 
			}

			$data['year_level']        = $current_history->year_level;
			$data['academic_terms_id'] = $current_history->academic_terms_id;
			$data['current_strand_id'] = $current_history->prospectus_id;
				
			$data['class_schedules']   = $this->CI->shs_student_model->ListClassSchedules($current_history->id); 
			$data['grades']            = $this->CI->shs_grades_model->List_Student_Grades($idnum); 

			$data['observed_values']   = $this->CI->behavior_model->ListObservedValues($current_history->id);
			
			$data['assess']            = $this->StudentAssessments($current_history);  //This code is for the assessments
			
			$data['student_type']      = "shs";
			
			$data['academic_terms'] = $this->CI->academic_terms_model->student_inclusive_academic_terms($idnum);
			$data['selected_term']  = $current_history->id;
			$data['show_pulldown']  = TRUE;

			if ($this->CI->session->userdata('role') == 'shschair') { 
				
				if ($current_history->academic_terms_id == $data['term']->id) {
					$data['can_update_values'] = TRUE;
				} 

			}

			$my_return['output'] = $this->CI->load->view('shs/student_management', $data, TRUE);

			echo json_encode($my_return,JSON_HEX_APOS);
					
			return;

		}


		public function StudentAssessments($student_history=NULL) {
			
			$this->CI->load->model('hnumis/shs/shs_student_model');
			$this->CI->load->model('hnumis/shs/section_model');
			
			//$student_histories = $this->CI->shs_student_model->ListStudentHistories($students_idno); //extract previous and current
			
			$data['assess_record'] = $this->CI->shs_student_model->checkIf_IsAssessed($student_history->id);

			if ($student_history) {
				$data['acad_program_groups_id'] = $student_history->acad_program_groups_id;
				$data['academic_terms_id']      = $student_history->academic_terms_id;
				$data['grade_level']            = $student_history->year_level;	
				$data['academic_years_id']		= $student_history->academic_years_id;
				$data['block_sections_id']		= $student_history->block_sections_id;
				
				if ($data['assess_record']) { //student already assessed
					$data['courses']  = $this->CI->shs_student_model->ListMyAssesed_Courses($student_history->id); 
					$data['lab_fees'] = $this->CI->shs_student_model->ListMyAssesed_LabFees($student_history->id); 					
				} else {
					$data['courses']  = $this->CI->section_model->ListCoursesAssigned($student_history->block_sections_id); 
					$data['lab_fees'] = $this->CI->section_model->ListLabFees($data); 
				}
			} else {
				$data['acad_program_groups_id'] = NULL;
				$data['academic_terms_id']      = NULL;
				$data['grade_level']            = NULL;	
				$data['courses']                = NULL;
				$data['lab_fees']               = NULL;
			}
				
			if ($data['assess_record']) { //student already assessed
				$data['student_histories_id'] = $student_history->id;
				$data['tuition_basic']        = new stdClass();
				$data['tuition_basic']->rate  = $data['courses'][0]->rate;
				$data['misc_fees']            = $this->CI->shs_student_model->ListMyAssessed_MiscFees($student_history->id); 
				$data['assessed_amount']      = $data['assess_record']->assessed_amount;   //Toyet 11.23.2017
			} else {
				$data['tuition_basic']        = $this->CI->shs_student_model->getStudentTuitionBasicRate($data); 
				$data['misc_fees']            = $this->CI->shs_student_model->ListMiscFees($data); 
				$data['assessed_amount']      = 0;   //Toyet 11.23.2017
			}
			
			return $data;
			
		}	


		
		private function generate_report_card_pdf($data) {

			//log_message("INFO", '1 ==> '.print_r($data,true)); // Toyet 7.23.2018

			ob_start();

			$this->CI->load->library('my_pdf');
			$this->CI->load->library('hnumis_pdf');

			$new_size = array(215.9,165.1); //this is the size for a halfpage longbond

			$this->CI->hnumis_pdf->WithHeader(FALSE);		
			$this->CI->hnumis_pdf->WithFooter(FALSE);		
			$this->CI->hnumis_pdf->SetAutoPageBreak(false);
			
			$this->CI->hnumis_pdf->AliasNbPages();		
			$this->CI->hnumis_pdf->AddPage('P',$new_size);
			
			$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->CI->hnumis_pdf->SetDrawColor(200,200,200);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();
			
			$this->headings($data);
			
			$this->name_and_section($data);

			$this->grades($data);

			$this->attendance($data);

			$this->side_descriptors($data);
			
			$this->CI->hnumis_pdf->AddPage('P',$new_size);
			
			$this->observed_values($data); 
			
			$this->footings($data);
			
			$this->CI->hnumis_pdf->Output("Report Card - ".$data['student_idno'].'.pdf','I');
	
			ob_end_flush(); 	
		}

		
		public function headings($data){

			$font_size = 9;
						
			$l=5;

			$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			$this->CI->hnumis_pdf->SetFillColor(255,255,255);

			if ($data['fullwidth']) {
				$y1 = $this->CI->hnumis_pdf->GetY()+80;
			} else {
				$y1 = 7;
			}
			
			$this->CI->hnumis_pdf->SetXY(7,$y1);
			$this->CI->hnumis_pdf->Cell(25,$l,'Form 138-B',0,0,'L',true);

			$this->CI->hnumis_pdf->Image(base_url('assets/img/hnulogo100px.png'),60,$y1,13,17,'PNG');

			$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			$this->CI->hnumis_pdf->SetXY(90,$y1);
			$this->CI->hnumis_pdf->Cell(25,4,'HOLY NAME UNIVERSITY',0,2,'C',true);
			$this->CI->hnumis_pdf->Cell(25,4,'Basic Education Department',0,2,'C',true);
			$this->CI->hnumis_pdf->Cell(25,4,'Janssen Heights, Tagbilaran City',0,2,'C',true);
			$this->CI->hnumis_pdf->SetFont('arial','B',$font_size);
			$this->CI->hnumis_pdf->Cell(25,4,'REPORT CARD',0,2,'C',true);
			$this->CI->hnumis_pdf->Cell(25,4,$data['term_text'],0,2,'C',true);
			
			$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			$this->CI->hnumis_pdf->SetXY(178,$y1);
			$this->CI->hnumis_pdf->Cell(20,4,'Grading System Used:',0,2,'C',true);
			$this->CI->hnumis_pdf->SetFont('arial','B',$font_size);
			$this->CI->hnumis_pdf->Cell(20,4,'K to 12 BEP',0,2,'C',true);
			$this->CI->hnumis_pdf->SetXY(171,$y1+12);
			$this->CI->hnumis_pdf->Cell(20,4,'LRN: '.$data['student']->lrn,0,2,'L',true);
					
		}
		
		
		public function name_and_section($data) {

			$font_size = 9.5;

			if ($data['fullwidth']) {
				$y1 = $this->CI->hnumis_pdf->GetY()+7;
			} else {
				$y1 = 32;
			}
			
			$this->CI->hnumis_pdf->SetXY(7,$y1);
			$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			$this->CI->hnumis_pdf->Cell(30,5,'Name:',0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('arial','BU',$font_size);

			//changed toyet 4.27.2018
			//  catch ñ at this point only, seems to cause error when passed to
			//  a javascript function if string is not encoded as utf8 by utf8_encode()
			$this->CI->hnumis_pdf->Cell(135,5,utf8_decode($data['student']->name),0,0,'L',true);

			$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			$this->CI->hnumis_pdf->Cell(10,5,'Sex:',0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('arial','B',$font_size);

			switch ($data['student']->gender) {
				case 'M':
					$gender = "MALE";
					break;
				
				case 'F':
					$gender = "FEMALE";
					break;
					
				default:
					$gender = NULL;
					break;
			}

			$this->CI->hnumis_pdf->Cell(20,5,$gender,0,1,'L',true);
			
			if(isset($data['strand'])){
				$edited_grade = substr($data['student']->grade,0,10).' '.$data['strand'];
			} else {
				$edited_grade = substr($data['student']->grade,0,10).' '.$data['student']->strand;
			}
			//$len = $this->CI->hnumis_pdf->GetStringWidth($data['student']->grade) + 2;
			$data['student']->grade = $edited_grade;

			$len = $this->CI->hnumis_pdf->GetStringWidth($data['student']->grade) + 2;
		
			$this->CI->hnumis_pdf->SetX(7);
			$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			$this->CI->hnumis_pdf->Cell(30,5,'Grade & Section:',0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('arial','B',$font_size);
			$this->CI->hnumis_pdf->Cell($len,5,$data['student']->grade,0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('arial','BI',$font_size);
			$this->CI->hnumis_pdf->Cell(135-$len,5,$data['student']->section,0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			$this->CI->hnumis_pdf->Cell(10,5,'Age:',0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('arial','B',$font_size);
			$this->CI->hnumis_pdf->Cell(20,5,$data['student']->age,0,0,'L',true);
		
		}

		
		public function grades($data) {

			if (isset($data['grade_reports'][0]->grades)) {

				$w = array(83,87,111,123,87,99);
				
				if ($data['fullwidth']) {
					$y1 = $this->CI->hnumis_pdf->GetY()+8;
				} else {
					$y1 = 43;
				}

				$this->CI->hnumis_pdf->SetFont('arial','B',9);
				$this->CI->hnumis_pdf->SetDrawColor(155,155,155);
				$this->CI->hnumis_pdf->SetXY(7,$y1);
				
				//added by Toyet 7.23.2018
				if(substr($data['term_text'],0,3)=='1st') {
					$term_quarter1 = '1'; 
					$term_quarter2 = '2'; 
				} else {
					$term_quarter1 = '3'; 
					$term_quarter2 = '4'; 
				}

				$this->CI->hnumis_pdf->MultiCell(82,10,'LEARNING AREAS',1,'C',true);
				$this->CI->hnumis_pdf->SetXY($w[1],$this->CI->hnumis_pdf->GetY()-10);
				$this->CI->hnumis_pdf->MultiCell(25,5,'Quarter',1,'C',true);
				$this->CI->hnumis_pdf->SetXY($w[2],$this->CI->hnumis_pdf->GetY()-5);
				$this->CI->hnumis_pdf->MultiCell(12,5,'Final Grade',1,'C',true);
				$this->CI->hnumis_pdf->SetXY($w[3],$this->CI->hnumis_pdf->GetY()-10);
				$this->CI->hnumis_pdf->MultiCell(17,10,'Remarks',1,'C',true);
				$this->CI->hnumis_pdf->SetXY($w[4],$this->CI->hnumis_pdf->GetY()-5);
				//$this->CI->hnumis_pdf->MultiCell(12,5,'3',1,'C',true);
				$this->CI->hnumis_pdf->MultiCell(12,5,$term_quarter1,1,'C',true);  //changed Toyet 7.23.2018
				$this->CI->hnumis_pdf->SetXY($w[5],$this->CI->hnumis_pdf->GetY()-5);
				//$this->CI->hnumis_pdf->MultiCell(12,5,'4',1,'C',true);
				$this->CI->hnumis_pdf->MultiCell(12,5,$term_quarter2,1,'C',true);  //changed Toyet 7.23.2018
				$this->CI->hnumis_pdf->SetFont('arial','',9);
				
				$l = 5;

				$w = array(80,12,12,12,17);

				$midterm_total      = 0;
				$finals_total       = 0;
				$show_ave_midterm   = TRUE;
				$show_ave_finals    = TRUE;
				$num_subj           = 0;
				$total_final_grades = NULL;
				
				foreach($data['grade_reports'] AS $report) {
					$this->CI->hnumis_pdf->SetX(7);
					$this->CI->hnumis_pdf->SetFillColor(218,218,218);
					$this->CI->hnumis_pdf->SetFont('arial','BI',9);
					$this->CI->hnumis_pdf->Cell(133,$l,STRTOUPPER($report->description),1,1,'L',true);
					$this->CI->hnumis_pdf->SetFillColor(255,255,255);
					$this->CI->hnumis_pdf->SetFont('arial','',9);
					
					if (isset($report->grades)) {
						foreach($report->grades AS $grade) {
							$this->CI->hnumis_pdf->SetX(7);

							$desc_len = $this->CI->hnumis_pdf->GetStringWidth($grade->descriptive_title);

							// auto-detect Font to use   -depending on length of descriptive_title
							// auto-detect 1 or 2 lines  -depending on length of descriptive_title
							switch(TRUE) {
								case ($desc_len<81): 
									$this->CI->hnumis_pdf->SetFont('arial','',9);
									break;
								case ($desc_len>80 && $desc_len<93):
									$this->CI->hnumis_pdf->SetFont('arial','',8.5);
									break;
								case ($desc_len>92):
									$this->CI->hnumis_pdf->SetFont('arial','I',9);
									break;
							}

							if($desc_len>92){								
								$this->CI->hnumis_pdf->Cell($w[0],$l,'',0,0,'L',true);
								$orig_line = $this->CI->hnumis_pdf->GetY();
								$_col = $this->CI->hnumis_pdf->GetX();
								$this->CI->hnumis_pdf->SetX(7);
								$this->CI->hnumis_pdf->MultiCell($w[0],$l,$grade->descriptive_title,1,'L');
								$this->CI->hnumis_pdf->SetXY($_col,$orig_line);
								$this->CI->hnumis_pdf->Cell($w[1],$l,'',1,0,'C',true);
								$this->CI->hnumis_pdf->Cell($w[2],$l,'',1,0,'C',true);
								$this->CI->hnumis_pdf->Cell($w[3],$l,'',1,0,'C',true);
								$this->CI->hnumis_pdf->Cell($w[4],$l,'',1,0,'C',true);
								$this->CI->hnumis_pdf->Ln();
								$this->CI->hnumis_pdf->SetX($_col);
							} else {
								$this->CI->hnumis_pdf->Cell($w[0],$l,$grade->descriptive_title,1,0,'L',true);
							}
							$this->CI->hnumis_pdf->Cell($w[1],$l,$grade->midterm_grade,1,0,'C',true);
							$this->CI->hnumis_pdf->Cell($w[2],$l,$grade->finals_grade,1,0,'C',true);
							$this->CI->hnumis_pdf->SetFont('arial','B',9);
							
							if ($grade->final_grade) {
								$this->CI->hnumis_pdf->Cell($w[3],$l,round($grade->final_grade),1,0,'C',true);
								$total_final_grades += round($grade->final_grade);
								
								if (round($grade->final_grade) >= 74.5) {
									$this->CI->hnumis_pdf->Cell($w[4],$l,'Prom',1,1,'C',true);
								} else {
									$this->CI->hnumis_pdf->SetTextColor(255,1,1);
									$this->CI->hnumis_pdf->Cell($w[4],$l,'Failed',1,1,'C',true);
									$this->CI->hnumis_pdf->SetTextColor(0,0,0);
								}
								
							} else {
								$this->CI->hnumis_pdf->Cell($w[3],$l,'',1,0,'C',true);							
								$this->CI->hnumis_pdf->Cell($w[4],$l,'',1,1,'C',true);
							}							
							
							$this->CI->hnumis_pdf->SetFont('arial','',9);
							
							if ($grade->midterm_grade) {
								$midterm_total += $grade->midterm_grade;
							} else {
								$show_ave_midterm = FALSE;
							}

							if ($grade->finals_grade) {
								$finals_total += $grade->finals_grade;
							} else {
								$show_ave_finals = FALSE;
							}
							
							$num_subj++;	
						}
					}
					
				}	
			
				$this->CI->hnumis_pdf->SetX(7);
				$this->CI->hnumis_pdf->SetFont('arial','B',9);
				$this->CI->hnumis_pdf->Cell($w[0],$l,'GENERAL AVERAGE',1,0,'C',true);
				
				if ($show_ave_midterm  AND $num_subj) {
					$ave_midterm = ($midterm_total/$num_subj);
					$this->CI->hnumis_pdf->Cell($w[1],$l,number_format($ave_midterm,2),1,0,'C',true);
				} else {
					$this->CI->hnumis_pdf->Cell($w[1],$l,'',1,0,'C',true);			
				}
				
				if ($show_ave_finals  AND $num_subj) {
					$ave_finals = ($finals_total/$num_subj);
					$this->CI->hnumis_pdf->Cell($w[2],$l,number_format($ave_finals,2),1,0,'C',true);
				} else {
					$this->CI->hnumis_pdf->Cell($w[2],$l,'',1,0,'C',true);
				}
				
				if ($show_ave_midterm AND $show_ave_finals AND $num_subj) {
					$this->CI->hnumis_pdf->Cell($w[3],$l,number_format(($total_final_grades/$num_subj),2),1,0,'C',true);
				} else {
					$this->CI->hnumis_pdf->Cell($w[3],$l,'',1,0,'C',true);				
				}
				
				$this->CI->hnumis_pdf->Cell($w[4],$l,'',1,0,'C',true);
			
			}	
		}


		public function side_descriptors($data) {

			if (isset($data['grade_reports'][0]->grades)) {
			
				$desc = array(array('Outstanding','90-100','Passed'),
							array('Very Satisfactory','85-89','Passed'),
							array('Satisfactory','80-84','Passed'),
							array('Fairly Satisfactory','75-79','Passed'),
							array('Did not Meet Expectations','Below 75','Failed'),						
						);
				
				$l = 6;
				
				if ($data['fullwidth']) {
					$y1 = $data['y_descriptor'];
				} else {
					$y1 = 53;
				}

				$this->CI->hnumis_pdf->SetFont('arial','B',8);
				$this->CI->hnumis_pdf->SetXY(142,$y1);
				$this->CI->hnumis_pdf->SetFillColor(218,218,218);
				
				$this->CI->hnumis_pdf->Cell(33,7,'Descriptors',1,0,'L',true);
				$this->CI->hnumis_pdf->MultiCell(14,3.5,'Grading Scale',1,'C',true);
				$this->CI->hnumis_pdf->SetXY(189,$this->CI->hnumis_pdf->GetY()-7);
				$this->CI->hnumis_pdf->Cell(14,7,'Remarks',1,2,'L',true);
				$this->CI->hnumis_pdf->SetFont('arial','',7.7);
				
				$this->CI->hnumis_pdf->SetFillColor(255,255,255);
				
				foreach ($desc AS $k=>$v) {
					$this->CI->hnumis_pdf->SetX(142);
					$this->CI->hnumis_pdf->Cell(33,$l,$v[0],1,0,'L',true);
					$this->CI->hnumis_pdf->Cell(14,$l,$v[1],1,0,'C',true);
					$this->CI->hnumis_pdf->Cell(14,$l,$v[2],1,1,'C',true);
				}

				$this->CI->hnumis_pdf->SetFont('arial','I',7.7);
				$this->CI->hnumis_pdf->SetXY(141,$y1+39);
				$this->CI->hnumis_pdf->Cell(33,$l,'*Not included in the computation of academic rating.',0,0,'L',true);
			
			}
		}
		
		
		public function attendance($data) {
			
			$l = 5;
			$font_size = 8;
			
			$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			
			if ($data['school_days']) {
				$my_cnt = 50 + (count($data['school_days'])*10) + 14; //Attendance Record printed at the center of the table
			} else {
				$my_cnt = 5;
			}
			
			if ($data['fullwidth']) {
				$y1 = $data['y_attendance'];
			} else {
				$y1 = 125;
			}

			$_line = $this->CI->hnumis_pdf->GetY();
			$this->CI->hnumis_pdf->SetXY(7,$_line);
			
			$w = array(50,10);
						
			if ($data['school_days']) {
				$this->CI->hnumis_pdf->SetXY(7,$_line+12);
				$this->CI->hnumis_pdf->SetFont('arial','B',$font_size);
				$this->CI->hnumis_pdf->Cell($my_cnt,$l,'ATTENDANCE RECORD',0,2,'C',true);
				$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
				$this->CI->hnumis_pdf->SetFillColor(218,218,218);
				$this->CI->hnumis_pdf->SetFont('arial','B',$font_size);
				$this->CI->hnumis_pdf->Cell($w[0],$l,'',1,0,'C',true);
				
				foreach ($data['school_days'] AS $sday) {
					$this->CI->hnumis_pdf->Cell($w[1],$l,$sday->month_report,1,0,'C',true);
				}
				
				$this->CI->hnumis_pdf->Cell(14,$l,'TOTAL',1,1,'C',true);
				$this->CI->hnumis_pdf->SetFillColor(255,255,255);
				$this->CI->hnumis_pdf->SetFont('arial','',$font_size);
			}
			
			$this->CI->hnumis_pdf->SetX(7);

			if ($data['school_days']) {
				$total_schooldays=0;
				$this->CI->hnumis_pdf->Cell($w[0],$l,'School Days Per Month',1,0,'L',true);
				
				foreach ($data['school_days'] AS $sday) {
					$this->CI->hnumis_pdf->Cell($w[1],$l,$sday->num_days,1,0,'C',true);
					$total_schooldays += $sday->num_days;
				}
				
				$this->CI->hnumis_pdf->Cell(14,$l,$total_schooldays,1,1,'C',true);
			}

			$this->CI->hnumis_pdf->SetX(7);

			if ($data['present_days']) {
				$total_presentdays=0;
				$this->CI->hnumis_pdf->Cell($w[0],$l,'School Days Present',1,0,'L',true);
				
				foreach ($data['present_days'] AS $pday) {
					$this->CI->hnumis_pdf->Cell($w[1],$l,$pday->days_present,1,0,'C',true);
					$total_presentdays += $pday->days_present;
				}
				
				$this->CI->hnumis_pdf->Cell(14,$l,$total_presentdays,1,1,'C',true);
			}
			
			$this->CI->hnumis_pdf->SetX(7);

			if ($data['present_days']) {
				$total_tardydays=0;
				$this->CI->hnumis_pdf->Cell($w[0],$l,'Days Tardy',1,0,'L',true);
				
				foreach ($data['present_days'] AS $pday) {
					$this->CI->hnumis_pdf->Cell($w[1],$l,$pday->days_tardy,1,0,'C',true);
					$total_tardydays += $pday->days_tardy;
				}
				
				$this->CI->hnumis_pdf->Cell(14,$l,$total_tardydays,1,1,'C',true);
			}

		}

		
		public function observed_values($data) {

			$w = array(51,101,13,13,13,13);
			
			$this->CI->hnumis_pdf->SetFont('arial','B',9);
			$this->CI->hnumis_pdf->SetDrawColor(155,155,155);

			if ($data['fullwidth']) {
				$y1 = $this->CI->hnumis_pdf->GetY()-$data['y_values']-5;
			} else {
				$y1 = 9-3;
			}

			//added by Toyet 7.23.2018
			if(substr($data['term_text'],0,3)=='1st') {
				$term_quarter1 = '1'; 
				$term_quarter2 = '2'; 
			} else {
				$term_quarter1 = '3'; 
				$term_quarter2 = '4'; 
			}

			$this->CI->hnumis_pdf->SetXY(175,$y1);
			$this->CI->hnumis_pdf->Cell(60,5,'LRN: '.$data['student']->lrn,0,1,'L',true);

			$this->CI->hnumis_pdf->SetXY(7,$y1+7);

			$this->CI->hnumis_pdf->SetFillColor(218,218,218);
			$this->CI->hnumis_pdf->MultiCell(array_sum($w)-6,8,"REPORT ON LEARNER'S OBSERVED VALUES",1,'C',true);
			$this->CI->hnumis_pdf->SetFillColor(255,255,255);
			$this->CI->hnumis_pdf->SetXY(7,$this->CI->hnumis_pdf->GetY());
			$this->CI->hnumis_pdf->MultiCell($w[0],12,'Core Values',1,'C',true);
			$this->CI->hnumis_pdf->SetXY($w[0],$this->CI->hnumis_pdf->GetY()-12);
			$this->CI->hnumis_pdf->MultiCell($w[1],12,'Behavior Statements',1,'C',true);
			$this->CI->hnumis_pdf->SetXY($w[0]+100,$this->CI->hnumis_pdf->GetY()-12);
			$this->CI->hnumis_pdf->MultiCell(54,6,'Quarter',1,'C',true);
			$this->CI->hnumis_pdf->SetXY($w[0]+100,$this->CI->hnumis_pdf->GetY());
			$this->CI->hnumis_pdf->MultiCell(13.5,6,' ',1,'C',true);
			$this->CI->hnumis_pdf->SetXY($w[0]+100+13.5,$this->CI->hnumis_pdf->GetY()-6);
			$this->CI->hnumis_pdf->MultiCell(13.5,6,' ',1,'C',true);
			$this->CI->hnumis_pdf->SetXY($w[0]+100+13.5+13.5,$this->CI->hnumis_pdf->GetY()-6);
			//$this->CI->hnumis_pdf->MultiCell(13.5,6,'3','C',true);
			$this->CI->hnumis_pdf->MultiCell(13.5,6,$term_quarter1,1,'C',true);
			$this->CI->hnumis_pdf->SetXY($w[0]+100+13.5+13.5+13.5,$this->CI->hnumis_pdf->GetY()-6);
			//$this->CI->hnumis_pdf->MultiCell(13.5,6,'4',1,'C',true);
			$this->CI->hnumis_pdf->MultiCell(13.5,6,$term_quarter2,1,'C',true);

			$this->CI->hnumis_pdf->SetFont('arial','',9);
			$this->CI->hnumis_pdf->SetXY(7,$this->CI->hnumis_pdf->GetY());

			if ($data['observed_values']) {
				$num =1;
				foreach($data['observed_values'] AS $ovalue) {
					
					if ($ovalue->behaviors) {
						$h    = COUNT($ovalue->behaviors)*10;
					} else {
						$h    = 10;
					}
					
					$this->CI->hnumis_pdf->SetX(7);
					$this->CI->hnumis_pdf->MultiCell(44,$h,$num.". ".$ovalue->description,1,'L',false);
					$this->CI->hnumis_pdf->SetXY(51,$this->CI->hnumis_pdf->GetY()-$h);
					
					if ($ovalue->behaviors) { 
						foreach($ovalue->behaviors AS $behavior) {
							
							$this->CI->hnumis_pdf->SetX(51);
							
							if ($this->CI->hnumis_pdf->GetStringWidth($behavior->behavior_statement) > 90) {
								$h1 = 5;
							} else {
								$h1 =10;
							}
							
							$this->CI->hnumis_pdf->MultiCell(100,$h1,$behavior->behavior_statement,1,'L',false);
							$this->CI->hnumis_pdf->SetXY(151,$this->CI->hnumis_pdf->GetY()-10);
							$this->CI->hnumis_pdf->MultiCell(13.5,10,'',1,'C',false);
							$this->CI->hnumis_pdf->SetXY(151+13.5,$this->CI->hnumis_pdf->GetY()-10);
							$this->CI->hnumis_pdf->MultiCell(13.5,10,'',1,'C',false);
							$this->CI->hnumis_pdf->SetXY(151+13.5+13.5,$this->CI->hnumis_pdf->GetY()-10);
							$this->CI->hnumis_pdf->MultiCell(13.5,10,$behavior->midterm_marking,1,'C',false);
							$this->CI->hnumis_pdf->SetXY(151+13.5+13.5+13.5,$this->CI->hnumis_pdf->GetY()-10);
							$this->CI->hnumis_pdf->MultiCell(13.5,10,$behavior->finals_marking,1,'C',false);

						}
						
					}

					$num++;
				}
			}
			
		}
		

		public function footings($data) {

			$this->CI->hnumis_pdf->SetXY(20,$this->CI->hnumis_pdf->GetY()+4);
			$this->CI->hnumis_pdf->Cell(170,12,'',1,1,'L',false);
			$y = $this->CI->hnumis_pdf->GetY();

			$this->CI->hnumis_pdf->SetXY(22,$this->CI->hnumis_pdf->GetY()-11);
			$this->CI->hnumis_pdf->SetFont('arial','B',9);
			$this->CI->hnumis_pdf->Cell(50,5,'Marking/Non-Numercial Rating:',0,0,'L',false);
			$this->CI->hnumis_pdf->SetFont('arial','',9);
			$this->CI->hnumis_pdf->Cell(50,5,'AO - Always Observed',0,0,'L',false);
			$this->CI->hnumis_pdf->Cell(50,5,'RO - Rarely Observed',0,1,'L',false);
			$this->CI->hnumis_pdf->SetXY(72,$this->CI->hnumis_pdf->GetY());
			$this->CI->hnumis_pdf->Cell(50,5,'SO - Sometimes Observed',0,0,'L',false);
			$this->CI->hnumis_pdf->Cell(50,5,'NO - Not Observed',0,1,'L',false);
			
			$this->CI->hnumis_pdf->SetXY(7,$y+4);
			$this->CI->hnumis_pdf->SetFont('arial','I',8);
			$this->CI->hnumis_pdf->Cell(40,5,'Eligible for Admission to:',0,0,'L',false);
			$this->CI->hnumis_pdf->Cell(60,5,'','B',0,'L',false);
			$this->CI->hnumis_pdf->SetX(120);
			$this->CI->hnumis_pdf->Cell(20,5,'','B',0,'L',false);
			$y = $this->CI->hnumis_pdf->GetY();
			
			$this->CI->hnumis_pdf->SetXY(7,$y+11);
			$this->CI->hnumis_pdf->Cell(30,5,'Lacks Subjects in:',0,0,'L',false);
			$this->CI->hnumis_pdf->Cell(70,5,'','B',0,'L',false);
			$this->CI->hnumis_pdf->SetXY(120,$y+8);
			$this->CI->hnumis_pdf->MultiCell(35,4,'Cancellation of Eligibility to Transfer/Admitted in:',0,'L',false);
			$this->CI->hnumis_pdf->SetXY(155,$y+10);
			$this->CI->hnumis_pdf->Cell(15,5,'','B',0,'L',false);
			$this->CI->hnumis_pdf->Cell(30,5,'Date:',0,0,'L',false);

			$this->CI->hnumis_pdf->SetXY(60,$this->CI->hnumis_pdf->GetY()+16);
			$this->CI->hnumis_pdf->SetFont('arial','B',8.5);
			$this->CI->hnumis_pdf->Cell(15,4,$data['student']->block_adviser,0,2,'C',false);
			$this->CI->hnumis_pdf->SetFont('arial','I',8);
			$this->CI->hnumis_pdf->Cell(15,4,'Section Adviser',0,2,'C',false);

			$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->SetFont('arial','B',8.5);
			$this->CI->hnumis_pdf->Cell(15,4,'DR. PRISCIANO S. LEGITIMAS',0,2,'C',false);
			$this->CI->hnumis_pdf->SetFont('arial','I',8);
			$this->CI->hnumis_pdf->Cell(15,4,'Principal',0,2,'C',false);
		
		}

	}
	
?>