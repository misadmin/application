
<div class="row-fluid" >
<h2 class="heading">Assessment Posting - Senior High School</h2>
	<div class="span12">
		<div class="form-horizontal">
			<form method="POST" id="post_assessment" >
				<input type="hidden" name="action" value="post_assessment" />
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<div class="control-group formSep">
					<div class="control-label">School Term</div>
					<div class="controls">
						<div style="float:left; padding-top:5px; font-weight:bold;">
							<?php print($terms_sy->term." ".$terms_sy->sy); ?>
						</div>
					</div>
				</div>
				<div class="control-group formSep">
					<div class="control-label">Track</div>
					<div class="controls">
						<div style="float:left;">
							<input type="checkbox" value="Y" name="all_program" id="selectall_strands" checked />
						</div>
						<div style="float:left; padding-top:3px; padding-left:7px;">
							ALL Strands
						</div>											
						<?php 					
							foreach($strands AS $strand) {
						?>
								<div style="clear:both; float:left;">

								<!-- Previously (below):
								     value="<?php print($strand->id); ?>" 
								     is used, this is the prospectus_id
								  -->
									<input type="checkbox" 
										value="<?php print($strand->academic_programs_id); ?>" 
										class="strand_id" 
										name="strands[]" 
										checked />
								</div>
								<div style="float:left; padding-top:3px; padding-left:7px;">
									<?php print($strand->description); ?>
								</div>
								
						<?php 
							}
						?>									
					</div>
				</div>
				<div class="control-group formSep" id="college">
					<div class="control-label">Grade Level</div>
					<div class="controls">
						<div style="float:left;">
							<input type="checkbox" value="Y" name="all_yr" id="selectall_yr" checked>
						</div>
						<div style="float:left; padding-top:3px; padding-left:7px;">
							ALL Grade Levels
						</div>
						<div style="clear:both; float:left;">
							<input type="checkbox" value="11" name="grade_level[]" class="selectedId_yr" checked>
						</div>
						<div style="float:left; padding-top:3px; padding-left:7px;">
							Grade 11
						</div>
						<div style="clear:both; float:left;">
							<input type="checkbox" value="12" name="grade_level[]" class="selectedId_yr" checked>
						</div>
						<div style="float:left; padding-top:3px; padding-left:7px;">
							Grade 12
						</div>										
					</div>
				</div>
				<div class="control-group formSep">
					<div class="control-label"></div>
					<div class="controls">
						<div style="float:left;">
							<input type="submit" value="Post Assessment!" class="btn btn-primary">		
						</div>
						<div style="float:left; padding-left:10px; padding-top:5px;" id="show_submit_form_icon">
						
						</div>
					</div>
				</div>
			</form>
		</div>				
	</div>
</div>


<script>
	$(document).ready(function () {
	    $('#selectall_strands').click(function () {
	        $('.strand_id').prop('checked', this.checked);
		});
	
	    $('.strand_id').change(function () {
	        var check = ($('.strand_id').filter(":checked").length == $('.strand_id').length);
	        $('#selectall_strands').prop("checked", check);
	    });
	});
</script>


<script>
	$(document).ready(function () {
	    $('#selectall_yr').click(function () {
	        $('.selectedId_yr').prop('checked', this.checked);
	    });
	
	    $('.selectedId_yr').change(function () {
	        var check = ($('.selectedId_yr').filter(":checked").length == $('.selectedId_yr').length);
	        $('#selectall_yr').prop("checked", check);
	    });
	});
</script>


<script>
	$(document).ready(function(){
		$('#post_assessment').submit(function() {
		
			var strands_selected = $('input:checkbox:checked.strand_id').map(function () {
				return this.value;
			}).get(); 
			
			var grades_selected = $('input:checkbox:checked.selectedId_yr').map(function () {
				return this.value;
			}).get(); 

			if (jQuery.isEmptyObject(strands_selected)) {
				alert('At least 1 strand must be selected!');
				return false;
			} 
			
			if (jQuery.isEmptyObject(grades_selected)) {
				alert('At least 1 Grade Level must be selected!');
				return false;
			} 

			var confirmed = confirm('Are you sure you want to continue posting?');

			if (!confirmed){
				return false;
			}
			
		});	

	});
	
</script>


