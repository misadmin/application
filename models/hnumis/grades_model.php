<?php

	class Grades_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListStudentGrades($academic_terms_id,$students_idno) {
			$result = null;
			
			$q = "
				 (SELECT
				 		e.id,
				 		c.course_code,
				 		c.descriptive_title,
				 		a.section_code,
				 		e.course_offerings_id,
				 		c.credit_units,
				 		c.paying_units,
				 		if ((k.is_bracketed = 'N') OR (k.is_bracketed IS NULL),'N','Y') AS is_bracketed,
				 		UPPER(CONCAT(h.lname,', ',h.fname)) AS emp_name,
				 		e.prelim_grade,
				 		e.midterm_grade,
				 		e.finals_grade,
				 		b.start_time,
				 		CASE l.day_name
					 		WHEN 'Mon' THEN 1
					 		WHEN 'Tue' THEN 2
					 		WHEN 'Wed' THEN 3
					 		WHEN 'Thu' THEN 4
					 		WHEN 'Fri' THEN 5
					 		WHEN 'Sat' THEN 6
					 		WHEN 'Sun' THEN 7
				 		END AS my_day
				 	FROM
				 		course_offerings_slots AS b,
				 		days AS l,
				 		courses AS c,
				 		rooms AS d,
				 		student_histories AS f,
				 		enrollments AS e 
				 			LEFT JOIN course_offerings AS a ON a.id=e.course_offerings_id 
				 			LEFT JOIN prospectus_courses AS k ON a.courses_id=k.courses_id
				 			LEFT JOIN col_faculty AS g ON a.employees_empno=g.employees_empno
				 			LEFT JOIN employees AS h ON g.employees_empno=h.empno
				 	WHERE a.id=b.course_offerings_id
				 		AND b.days_day_code=l.day_code
				 		AND c.id=a.courses_id
				 		AND d.id=b.rooms_id
				 		AND e.student_history_id=f.id
				 		AND a.id=b.course_offerings_id
				 		AND a.academic_terms_id={$this->db->escape($academic_terms_id)}
				 		AND f.students_idno={$this->db->escape($students_idno)}
				 		AND a.status='A'
						AND e.status='Active'
				 	GROUP BY e.course_offerings_id)
				
				UNION
				
				(SELECT
						e.id,
						c.course_code,
						c.descriptive_title,
						a.section_code,
						e.course_offerings_id,
						c.credit_units,
						c.paying_units,
						k.is_bracketed,
						UPPER(CONCAT(h.lname,', ',h.fname)) AS emp_name,
						e.prelim_grade,
						e.midterm_grade,
						e.finals_grade,
						m.start_time,
						CASE l.day_name
								WHEN 'Mon' THEN 1
								WHEN 'Tue' THEN 2
								WHEN 'Wed' THEN 3
								WHEN 'Thu' THEN 4
								WHEN 'Fri' THEN 5
								WHEN 'Sat' THEN 6
								WHEN 'Sun' THEN 7
							END AS my_day
						FROM
							course_offerings_slots AS b,
							days AS l,
							courses AS c,
							prospectus_courses AS k,
							elective_courses AS j,
							rooms AS d,
							enrollments AS e,
							student_histories AS f,
							course_offerings_slots AS m,
							course_offerings AS a 
							LEFT JOIN col_faculty AS g ON a.employees_empno=g.employees_empno
							LEFT JOIN employees AS h ON g.employees_empno=h.empno
						WHERE a.id=b.course_offerings_id
							AND b.days_day_code=l.day_code
							AND c.id=j.topic_courses_id
							AND k.id=j.prospectus_courses_id
							AND c.id=a.courses_id
							AND d.id=b.rooms_id
							AND a.id=e.course_offerings_id
							AND e.student_history_id=f.id
							AND a.id=m.course_offerings_id
							AND a.academic_terms_id={$this->db->escape($academic_terms_id)}
							AND f.students_idno={$this->db->escape($students_idno)}
							AND a.status='A'
							AND e.status='Active'
				GROUP BY 
						e.course_offerings_id)
					
				ORDER BY my_day, start_time";
			
					
			
			//print_r($q); die();
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		function getGrade($courses_id,$students_idno) {
			$result = null;
			
			$q1 = "SELECT a.id, a.finals_grade
						FROM 
							enrollments AS a, course_offerings AS b, student_histories AS c
						WHERE
							a.course_offerings_id=b.id
							AND c.id=a.student_history_id
							AND	b.courses_id={$this->db->escape($courses_id)}
							AND c.students_idno={$this->db->escape($students_idno)}
						ORDER BY a.id DESC LIMIT 1";
			//print($q1);
			//die();
									
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		}
		
		public function update_grades ($period, $data, $course_offerings_id){
			
			$sql = "
				INSERT into 
					enrollments(`id`, `{$period}_grade`)
				VALUES
				";
			
			foreach ($data as $key=>$val){
				$sql .= "({$this->db->escape($key)}, {$this->db->escape($val)}),\n";
			}
			$sql = rtrim($sql, ",\n") . "\n";
			$sql .= "ON DUPLICATE KEY UPDATE
						`{$period}_grade`=VALUES(`{$period}_grade`)
					";
			
			if ($this->db->query($sql)){
				$sql =  "
						INSERT into 
							grade_submission_dates (`course_offerings_id`, `{$period}_date`)
						VALUES ({$this->db->escape($course_offerings_id)}, NOW())
						ON DUPLICATE KEY UPDATE
							`{$period}_date`=VALUES(`{$period}_date`)
						";
				return $this->db->query($sql);
			} else {
				return FALSE;
			}
		}
		
		/**
		 * Method: course_is_under_faculty
		 * 
		 * validates whether students and courses are under faculty. data is an array of enrollments ID...
		 * 
		 * @param unknown_type $course_offerings_id
		 * @param unknown_type $faculty_id
		 * @param unknown_type $data
		 * @return string
		 */
		public function course_is_under_faculty ($course_offerings_id, $faculty_id, $data){
			$sql = "SELECT
						count(co.id) as idcount
					FROM
						course_offerings co
					LEFT JOIN
						enrollments e
					ON
						co.id=e.course_offerings_id
					WHERE
						co.employees_empno={$this->db->escape($faculty_id)}
					AND
						e.id
					IN
						(";
			foreach ($data as $key=>$val){
				$sql.= "$key,";
			}
			$sql = rtrim($sql, ",") . ")";	
			
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0)
				return TRUE; else
				return FALSE;
		}
		
		//ADDED: 7/25/13 by genes
		//extract submission date of given course_offerings_id
		public function getGradesSubmissionDate($course_offerings_id) {
			$result = null;
				
			$q1 = "SELECT 
						DATE_FORMAT(a.prelim_date,'%b %d, %Y %h:%i:%s %p') AS prelim_date,
						DATE_FORMAT(a.midterm_date,'%b %d, %Y %h:%i:%s %p') AS midterm_date,
						DATE_FORMAT(a.finals_date,'%b %d, %Y %h:%i:%s %p') AS finals_date
					FROM
						grade_submission_dates AS a
					WHERE
						a.course_offerings_id={$this->db->escape($course_offerings_id)} ";
				
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
			$result = $query->row();
			}
				
			return $result;
		}
		
		public function grades_for_period ($academic_term_id){
			$this->db->select ('sh.students_idno, pn.phone_number, e.prelim_grade, e.midterm_grade, e.finals_grade, c.course_code')
				->from('enrollments e')
				->join('course_offerings co', 'e.course_offerings_id=co.id', 'left')
				->join('courses c', 'c.id=co.courses_id', 'left')
				->join('student_histories sh', 'sh.id=e.student_history_id', 'left')
				->join('(SELECT DISTINCT students_idno,  phone_number, phone_type FROM phone_numbers ORDER BY students_idno) pn', 'pn.students_idno=sh.students_idno', 'left')
				->where('co.academic_terms_id', $academic_term_id)
				->where('NOT ISNULL(e.finals_grade)', null, FALSE)
				->where('pn.phone_type', 'mobile')
				->limit(5000, 60000);
			$query = $this->db->get ();
			if($query->num_rows() > 0)
				return $query->result(); else
				return FALSE;
		}
		
		//Justing 3/29/2013
		public function course_code_phone_number_grade($period, $data, $course_offering_id){
			$enrollments_id = array();
			foreach ($data as $key=>$val){
				$enrollments_id[] = $key;
			}
			$query = $this->db->select("pn.phone_number, {$period}_grade, h.students_idno, c.course_code", FALSE)
				->from("enrollments e")
				->join("student_histories h", "e.student_history_id=h.id", 'left')
				->join("(SELECT DISTINCT students_idno, phone_number FROM phone_numbers) pn", "h.students_idno=pn.students_idno", 'LEFT')
				->join("course_offerings co", 'e.course_offerings_id=co.id', 'left')
				->join('courses c', 'c.id=co.courses_id', 'left')
				->where(sprintf("e.id IN (%s)", implode(",", $enrollments_id)), null, FALSE)->get();
			if($query->num_rows() > 0)
				return $query->result(); else
				return FALSE;
		}
	}


//end of grades_model.php