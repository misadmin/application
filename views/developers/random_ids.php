<?php 
class Random_Ids {
	
	protected $CI;
	public function __construct(){
		$this->CI =& get_instance();
	}
	
	public function index(){
		$this->CI->load->library('table_lib');
		$this->CI->load->model('hnumis/College_model');
		$this->CI->load->model('developers/random_ids_model');
		$t30ids = array();
		$allcolleges = $this->CI->College_model->all_colleges();
		$data = array();
		foreach($allcolleges as $college) {
			$t30ids = $this->CI->random_ids_model->fetch_30ids($college->college_code);
			$data = array_merge($data,$t30ids);
		}

		$datestamp = date("Y_m_d_hisa");
		$filename = "downloads/RandomID_".$datestamp.".xls";
		$this->CI->random_ids_model->CreateRandomIDsforExcel($data,$filename,$datestamp);

		$this->CI->content_lib->enqueue_body_content('developers/random_ids', 
								array( 'data' => $data,
									   'filename' => $filename,
								 ) );
		$this->CI->content_lib->content();
	}
}

