<div class="span9">
  <h3 class="heading"><strong>Payments during: <select name="terms_id" class="form">
              <?php
					foreach($terms AS $term) {
						print("<option value=".$term->id.">".$term->term."."-".$term->sy</option>");	
					}
				?>
            </select>
		
		<table width="63%">
			<thead>
				<tr>
					<th width="26%" style="text-align: left">Payment Date</th>
					<th width="51%" style="text-align: center">Description </th>
					<th width="23%" style="text-align: center">Amount</th>
	     		</tr>
			</thead>
			<tbody>
				<?php foreach ($payments_info as $payment){ ?>
				<tr>
					<td><?php print($payment->payment_date); ?></td>
					<td style="text-align: left"><?php print($payment->description); ?></td>
					<td style="text-align: left"><?php echo number_format($payment->receipt_amount, 2); ?></td>
			   </tr>
			   <?php } ?>	
		</tbody>
  </table>

</div>

