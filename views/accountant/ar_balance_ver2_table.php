<?php
/**
 * Actual display of AR Transactions (details only) pertaining to
 * or with relation to the Student's Subsidiary Ledger.
 * @author   From the original team of IRMC
 *           Edited: 2017 October-November by Toyet Amores
 * @param    $data             - the array containing the data to display
 *			 $start_date       - starting date of transactions to get
 *			 $end_date         - ending date of transactions to get
 			 $tab              - (seems to be unused)
 			 $levels_array     - 
 			 $year_level_array - 
 * @return   None.
*/
?>
<style>
	.table 
	th.center {text-align:center; vertical-align:middle;}
	td.right {text-align:right}
	td.center {text-align:center}
	tr.trhide {display:none}
	.green  {font-color:green}
</style>

<div class="span8 row-fluid"">
<div class="heading">
<h2>A/R Transactions Details<h3>For Period: <?php echo  (isset($start_date) AND isset($end_date)) ? $start_date.' to '.$end_date : " "; ?></h3></h2>
	<div id="ar_balance_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="arBalanceModal" aria-hidden="true"
		style="width:850px; left:350px;">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    <h3 id="ar_balance_modal_label">Details</h3>
		  </div>
		  <div class="modal-body">
			    <table id="ar-details-table" class="table table-bordered">
			    <thead>
			    <tr>
			    	<th>#</th>
			    	<th>ID Number</th>
			    	<th>Name</th>
			    	<th>Course/Year</th>
			    	<th>Date</th>			    	
			    	<th id="ar_balance_modal_tran_type">Debit</th>
			    	<th>Reference</th>
			    </tr>
			    </thead>
			    <tbody id="ar_balance_modal_content">
			    
			    </tbody>
			    </table>
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  </div>
	</div>

	<table id="ar-table" class="table table-condensed table-bordered table-hovered table-striped">
		<thead>
			<th class="center">Prog Group</th>
			<th class="center"> * </th>
			<th class="center"> </th>
			<th class="center">Code</th>
			<th class="center">Description</th>
			<th class="center">Students</th>
			<th class="center">Debit</th>
			<th class="center">Credit</th>
		</thead>		
<?php
	$levels_id = "";
	$group_name="";
	$year_level="";
	$total_for_group_name_credit=0;
	$total_for_group_name_debit=0;	
	$iteration = 0;
	$total_for_abbreviation_credit=0;
	$total_for_abbreviation_debit=0;
	$row_num = 0;
	$studs_count = 0;
	
	//log_message('info','iterating $data');

	foreach ($data as $row){
		$dumb_section=array('Grade 1','Grade 2','Grade 3','Grade 4','Grade 5','Grade 6','Grade 7','Grade 8','(',')');
		//$trimed_abbreviation = str_replace($dumb_section,"",$row->abbreviation);  	
		$iteration++;
		$row_num++;
		$studs_count += $row->stud;
		
		echo $row->group_name=='Sample' ? '<tr class="trhide">' : '<tr>' ;
		
		if ($levels_id != $row->levels_id) {
			echo "<td class ='center' colspan=8><h4>" . $levels_array[$row->levels_id] . "</h4></tr>" ; 
			echo $row->group_name=='Sample' ? '<tr class="trhide">' : '<tr>' ;
		}

		if ($group_name !=  $row->group_name) {
			$row_num =  1;				
			echo "<td colspan=8><i><b>" . $row->group_name . ":</b></i></tr>" ;				
			echo $row->group_name=='Sample' ? '<tr class="trhide">' : '<tr>' ;
		}
		
		$checkArray = array(1,2,3,4,5,11,12);
		if (in_array($levels_id,$checkArray) and $year_level != $row->year_level) {
			echo "<td class ='center' colspan=8><h5>" . $row->year_level . "</h5></tr>" ; 
			echo $row->group_name=='Sample' ? '<tr class="trhide">' : '<tr>' ;
		}

		$total_for_group_name_credit += $row->credit;
		$total_for_group_name_debit += $row->debit;
		$total_for_abbreviation_credit += $row->credit;
		$total_for_abbreviation_debit += $row->debit;
		
?>
		<td width="10%">
		<td width="5%" class="center"><?php echo $row_num .'.'; ?></td>
		<td width="4%"><code><?php echo $row->trangroup; ?></code>
		<td width="30%"><?php echo $row->code; ?>		
		<td width="30%"><?php echo $row->description; ?>
		<td width="8%" class="center" >
			<a target="_blank" class="students-list btn btn-link"
				data-levels_id   = "<?php echo $row->levels_id; ?>"			 
				data-group_name  = "<?php echo $row->group_name; ?>"			 
				data-year_level  = "<?php echo $row->year_level; ?>"	
				data-trangroup   = "<?php echo $row->trangroup; ?>" 
				data-deskription = "<?php echo $row->description; ?>"
				data-tran_code   = "<?php echo $row->code; ?>" 
				data-tran_type   = "<?php echo $row->debit > 0 ? 'debit' : 'credit'; ?>"	
			 	><?php echo $row->stud; ?></a>
		<td width="10%" class="right"><?php echo number_format($row->debit,2); ?>
		<td width="10%" class="right"><?php echo number_format($row->credit,2); ?>
<?php
		$levels_id = $row->levels_id;
		$group_name = $row->group_name;
		$year_level = $row->year_level;
} //end of foreach
?>

		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="right"><br>Total(s)</td>
			<td width="8%" class="center"><font color="#0182b2" >Transaction(s)<br><?php echo number_format($studs_count,0); ?></font></td>
			<td width="10%" class="right"><br><?php echo number_format($total_for_group_name_debit,2); ?></td>
			<td width="10%" class="right"><br><?php echo number_format($total_for_group_name_credit,2); ?></td>
		</tr>

	</table>
</div>
</div>

<script>
	$(document).ready(function(){
		$("#ar-table").stickyTableHeaders();
		$("#ar-table").stickyTableHeaders('updateWidth');
		//.bind() is deprecated, using .on() instead - Toyet 1.24.2018
		//$('.students-list').bind('click', function(){
		$('.students-list').on('click', function(){
			show_loading_msg();
			$.post('<?php echo site_url('ajax/ar_details/') ?>',
				{
					'levels_id'  : $(this).data('levels_id'),
					'group_name' : $(this).data('group_name'),
					'year_level' : $(this).data('year_level'),
					'tran_code'	 : $(this).data('tran_code'),
					'tran_type'	 : $(this).data('tran_type'),
					'trangroup'	 : $(this).data('trangroup'),
					'deskription': $(this).data('deskription'),
					'start_date' : <?php echo $this->db->escape($start_date); ?>,
					'end_date'   : <?php echo $this->db->escape($end_date); ?>,
					'summary'    : <?php echo 'false'; ?>,
				},
			  function(data){
			    if (data.type == 'Success'){
				    hide_loading_msg();
				    //$('#ar_balance_modal_label').text(data.heading);
				    $('#ar_balance_modal_label').text(data.heading);
				    $('#ar_balance_modal_tran_type').text(data.tran_type);
					var str = '';
					var amount = 0.00;
					var total = 0.00;
				    $.each(data.data, function (i,item){
						str += '<tr><td class="center">'+(i+1)+'</td><td class="center"><a href="student/'+item.idno+'" target="_blank">'
						+item.idno+'</a></td><td>'+item.student+'</td><td class="left">'+item.course_year+'</td><td class="center">'
						+item.transaction_date+'</td><td class="right">'+item.amount+'</td><td class="center">'+item.ref+'</td></tr>';
						amount = parseFloat(item.amount.replace(',',''));
						total = total + amount ;
					});
					str += '<tr><td></td><td></td><td></td><td></td><td>TOTAL</td><td class="right">'+numberWithCommas(total.toFixed(2))+'</td><td></td></tr>';
					$('#ar_balance_modal_content').html(str);
					$('#ar_balance_modal').modal('show');
			    }
			    else {
			    	hide_loading_msg();
			    	show_sticky_msg(data.type,data.message);
			    }
			  }
			,"json");
		});
		
	});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

</script>