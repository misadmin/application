<h2 class="heading">Create Course</h2>
<?php
$this->form_lib->set_id('create_course_form');
$this->form_lib->add_control_class('formSep');
$this->form_lib->set_attributes(array('method'=>'post','action'=>'','class'=>'form-horizontal'));
$this->form_lib->enqueue_hidden_input('nonce',$this->common->nonce());
$this->form_lib->enqueue_text_input('course_code','Course Code','',set_value('course_code'),FALSE,'span2');
$this->form_lib->enqueue_text_input('descriptive_title','Descriptive Title','',set_value('descriptive_title'),FALSE,'span4');
$this->form_lib->enqueue_text_input('credit_units','Credit Units','',set_value('credit_units'),FALSE,'span1');
$this->form_lib->enqueue_text_input('paying_units','Paying Units','',set_value('paying_units'),FALSE,'span1');

$this->form_lib->enqueue_select_input2('type_id','Course Type','{id}','{type_description}',$types,'','','span2');
$this->form_lib->enqueue_select_input2('group_id','Course Group','{id}','{group_name}',$groups,'','','span2');

$this->form_lib->set_submit_button('Create');

if (form_error('course_code')) $this->form_lib->enqueue_text_helper('course_code',form_error('course_code'),'error');
if (form_error('descriptive_title')) $this->form_lib->enqueue_text_helper('descriptive_title',form_error('descriptive_title'),'error');
if (form_error('credit_units')) $this->form_lib->enqueue_text_helper('credit_units',form_error('credit_units'),'error');
if (form_error('paying_units')) $this->form_lib->enqueue_text_helper('paying_units',form_error('paying_units'),'error');
if (form_error('type_id')) $this->form_lib->enqueue_text_helper('type_id',form_error('type_id'),'error');
if (form_error('group_id')) $this->form_lib->enqueue_text_helper('group_id',form_error('group_id'),'error');

$this->form_lib->content(FALSE);