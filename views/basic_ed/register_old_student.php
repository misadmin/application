<?php 
/*	$levels = array(
                        11 => 'Preschool',
			1 => 'Kindergarten',
			3 => 'Grade School',
			4 => 'High School',
	);
*/
/*	$yrs[11] = new StdClass();
	$yrs[3] = new StdClass();
	
	$yrs[11]->id=11;
	$yrs[11]->my_yr=1;
	$yrs[3]->id=3;
	$yrs[3]->my_yr=1;
*/
/*	
	$yrs = array();
	$pre=1;
	$kin=1;
	$gr=1;
	$hs=7;
	
	for($n=1; $n<15; $n++) {
		
		$yrs[$n] = new StdClass();
		
		if ($n == 1) {
			$yrs[$n]->id=11;	
			$yrs[$n]->my_yr=$pre;
			$pre++;
		} 
		if ($n == 2) {
			$yrs[$n]->id=1;	
			$yrs[$n]->my_yr=$kin;
			$kin++;
		} 
		if ($n>2 AND $n<9) {
			$yrs[$n]->id=3;
			$yrs[$n]->my_yr=$gr;
			$gr++;
		}
		if ($n>8 AND $n<17) {
			$yrs[$n]->id=4;
			$yrs[$n]->my_yr=$hs;
			$hs++;
		}
		
	}


	
	//print_r($levels);
	//print("<p>");
	//print_r($yrs); die();
*/	
	

?>

<?php //if ($current_history){
		//print("<h3>Student Already Registered</h3>");
// }else {?>
 <?php if (isset($message) && $tab=='7') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:5px; ">
 <form method="post" id="register_old_student" name="register_old_student">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "register_old_student"/>
	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" > 
	<!--  <input type="hidden" name="academic_year" value="<?php print($sy_id); ?>" > --> 
	<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
	   <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    <!-- <strong><font face="Verdana, Arial, Helvetica, sans-serif"> REGISTER STUDENT/PUPIL for SY <?php print($sy);?></font></strong> -->
		    <strong><font face="Verdana, Arial, Helvetica, sans-serif"> REGISTER STUDENT/PUPIL for:</font></strong>
		    </div>
		   </td>
 	 </tr>
	</table>	

		  
  <div style="margin-left:20px; width:90%; margin-bottom:20px; ">

	<div class="control-group formSep">
		<label class="control-label">School Year</label>
		<div class="controls">
		    <select id="acad_yr" name="academic_year" class="form">
				<option value="">-- Select School Year --</option>
				<?php foreach ($academic_years as $acad_yr): ?>
						<option value="<?php echo $acad_yr->id; ?>"><?php echo $acad_yr->sy; ?></option>
					<?php endforeach; ?>
			</select>
		</div>
	</div>
  
	<?php
		$data = array("button"=>"Register STUDENT/PUPIL!");
		$this->load->view("basic_ed/includes/select_yr_and_level",$data);
	?>
		  
	</div>
 	 
  
</form>
 </div>
 
 <?php //}?>
 
 
 <script>
	 $(document).ready(function (e) {
    	$('#select_level').change(function () {
        	if ($(this).val() == '') {
            	$('.my_level').hide();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.shigh').hide();
			} 

        	if ($(this).val() == '11') {
            	$('.my_level').show();
        		$('.preschool').show();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.shigh').hide();
			} 

    		if ($(this).val() == '1') {
            	$('.my_level').show();
    			$('.preschool').hide();
    			$('.kinder').show();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.shigh').hide();
			} 

        	if ($(this).val() == '3') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').show();
            	$('.hd').hide();
            	$('.shigh').hide();
			} 

        	if ($(this).val() == '4') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').show();
            	$('.shigh').hide();
			} 

        	if ($(this).val() == '12') {
            	$('.my_level').show();
        		$('.preschool').hide();
        		$('.kinder').hide();
            	$('.gs').hide();
            	$('.hd').hide();
            	$('.shigh').show();
			} 
    	});
	});
 </script>
 