<div class="row-fluid">
	<h3 class="heading"><small>Search Result for:</small> <strong><?php echo $query; ?></strong></h3>
	<div class="search_panel clearfix">
<?php if (isset($results) && count($results) > 0): ?>		
<?php foreach ($results as $result): ?>
	<div class="search_item clearfix">
		<div class="thumbnail pull-left"><a href="<?php echo site_url("{$role}/faculty/{$result['idno']}"); ?>"><img src="<?php echo $result['image']; ?>" style="width:100px"></a></div>
		<div class="search_content">
			<div style="padding-left:10px"><h4><?php echo $result['fullname']?></h4></div>
			<div style="padding-left:10px"><a href="<?php echo site_url("{$role}/faculty/{$result['idno']}"); ?>"><?php echo $result['idno']; ?></a></div>
		</div>
	</div>
<?php endforeach; ?>
<?php endif; ?>
	</div>
</div>