<div style="padding-left:20px;">
<?php
	
	if ($basic_ed_sections_id == 166 OR $basic_ed_sections_id == 175) { 	//check if student is self-contained
		print("<h3>Student under Self-Contained Section. No Assessment is required!</h3>");
	} elseif (!$stud_status) {
		print("<h3>Student is INACTIVE! Assessment NOT available!</h3>"); //check if student is inactive 
	} else {

?>

<h3>
<?php 
	if ($post_status) {
		print("ASSESSMENT POSTED ON: ".$post_status->transaction_date);
	} else {
		print("ASSESSMENT UNPOSTED!");
	}
?>
</h3>
<table class="table table-condensed table-hover" style="width:70%;">
	<tr>
		<td style="width:40%; font-weight:bold;">Tuition Basic</td>
		<td style="width:30%;">&nbsp;</td>
		<td style="text-align:right; width:30%; font-weight:bold;"><?php print(number_format($tuition_basic->rate,2)); ?></td>
	</tr>
	<?php 
		$g_total=$tuition_basic->rate;
		$group="";
		$item_total=0;
		$new = FALSE;
		if ($basic_ed_fees) {
			foreach($basic_ed_fees AS $basic_fee) {
				if ($group != $basic_fee->fees_group) {
					$group = $basic_fee->fees_group;						
					if ($new) {
						$new = FALSE;
	?>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align:right; font-weight:bold;">TOTAL <?php print($old_group); ?>:</td>
			<td style="text-align:right; font-weight:bold;"><?php print(number_format($item_total,2)); ?></td>
		</tr>
	<?php 
					}
	?>
		<tr>
			<td colspan="3" style="font-weight:bold;"><?php print($basic_fee->fees_group); ?></td>
		</tr>
		<tr>
			<td style="padding-left:30px;"><?php print($basic_fee->description); ?></td>
			<td style="text-align:right;"><?php print(number_format($basic_fee->rate,2)); ?></td>
			<td>&nbsp;</td>
		</tr>
	<?php 
					$g_total = $g_total + $item_total;
					$item_total=0;
					$new = TRUE;
				} else {
					$old_group = $basic_fee->fees_group;
	?>
		<tr>
			<td style="padding-left:30px;"><?php print($basic_fee->description); ?></td>
			<td style="text-align:right;"><?php print(number_format($basic_fee->rate,2)); ?></td>
			<td>&nbsp;</td>
		</tr>
	
	<?php 
				}
				$item_total=$item_total + $basic_fee->rate;
			}
			$g_total = $g_total + $item_total;
	?>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align:right; font-weight:bold;">TOTAL <?php print($old_group); ?>:</td>
			<td style="text-align:right; font-weight:bold;"><?php print(number_format($item_total,2)); ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align:right; font-weight:bold;">GRAND TOTAL:</td>
			<td style="text-align:right; font-weight:bold;"><?php print(number_format($g_total,2)); ?></td>
		</tr>
	<?php 
		}
	?>

</table>

<?php 
	}

?>



</div>