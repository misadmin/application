<div style="text-align:center;">
<table cellpadding="2" cellspacing="0" class="table table-striped table-condensed table-hover" style="width:100%;">
 	<thead>
  	<tr style="font-weight:bold; background:#EAEAEA;">
    	<td width="8%" style="text-align:center; vertical-align:middle;">No.</td>
    	<td width="12%" style="text-align:center; vertical-align:middle;">ID No.</td>
    	<td width="43%" style="text-align:center;">Name</td>
    	<td width="37%" style="text-align:center;">Year-Section</td>
    </tr>
	</thead>
	<tbody>
  <?php
		$cnt = 0;
		if ($students) {
			foreach($students AS $student) {
				$cnt++;
  ?>
  
  <tr>
    <td style="text-align:center;"><?php print($cnt); ?></td>
    <td style="text-align:center;"><?php print($student->students_idno); ?></td>
    <td style="text-align:left;"><?php print($student->name); ?></td>
    <td style="text-align:left;"><?php print($student->year_level.' - '.$student->section); ?></td>
    </tr>
	<?php
			}
		}
	?> 
  </tbody>   
</table>
</div>
