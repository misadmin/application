 <?php if (isset($message) && $tab=='15') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>

<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;"> 
	  <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    	<strong><font face="Verdana, Arial, Helvetica, sans-serif"> UPDATE GPA</font></strong>
		   </div>
		   </td>
 	 </tr>
 	 
</table>	

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666;  margin-top:10px;">
  <table width="60%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:60%; padding:0px; margin:10px;">
    <thead>
      <tr>
         <th width="10%" style="text-align:center;">School Year</th>
		<th width="10%" style="text-align:center;">Year Level</th>
		 <th width="10%" style="text-align:center;">GPA</th>
		  <th width="10%" style="text-align:center;">Update</th>
       
       </tr>
    </thead>
    <?php
    
	if ($gpas) {
			foreach ($gpas AS $gpa) {
	?>
    <tr>
      <td style="text-align:left;"><?php print($gpa->sy); ?></td>
	  <td style="text-align:center;"><?php print($gpa->yr_level); ?></td>
	  <td style="text-align:center;"><?php print number_format($gpa->gpa,2); ?></td>
	  
      <td style="text-align:center; vertical-align:middle; ">
			 		<a href="#" data-toggle="modal" data-target="#modal_edit_gpa_<?php print($gpa->id); ?>" 
								data-project-id='<?php print($section); ?>'><i class="icon-edit"></i></a>
	    <!-- modal for EDITING GPA -->
		   
		    <div class="modal hide fade" id="modal_edit_gpa_<?php print($gpa->id); ?>" >
				<form method="post" id="edit_gpa_form_<?php print($gpa->id); ?>">
					<input type="hidden" name="action" value="update_gpa" />
			    	 <input type="hidden" name="gpa_id" value= "<?php print($gpa->id);?>"/>
				 	<?php $this->common->hidden_input_nonce(FALSE); ?>
				 <div class="modal-header">
				 	<button type="button" class="close" data-dismiss="modal">�</button>
				      		<h3>Edit GPA</h3>
				 </div>
				 <div class="modal-body">
				 			<div style="text-align:center;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									
									<tr>
										<td style="width:85px;"><b> School Year </b></td>
										<td style="width:10px;" >:</td>
										<td> <strong> <?php print($gpa->sy);?> </strong></td>
									</tr>	
									
									<tr>
										<td style="width:85px;"> <b> Year Level </b></td>
										<td style="width:10px;">:</td>
										<td> <b>  <?php print($gpa->yr_level);?> </b></td>
									</tr>	
									
									<tr>
										<td style="width:85px;"> <b> GPA </b></td>
										<td style="width:10px;">:</td>
										<td><input type="text" name="gpa" style="width:40px;" value="<?php print number_format($gpa->gpa,2); ?>"></td>
									</tr>	
									
								</table>
							</div>
				</div>
				<div class="modal-footer">
				    		<input type="submit" class="btn btn-primary" id="edit_gpa_<?php print($gpa->id); ?>" value="EDIT GPA">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
			</form>
		</div>		
				     <!-- End of EDITING GPA -->
								
			</td>
		</tr>		
			<?php 		
			}
		?>
	<?php 		
			}else{
		?>	
		<tr>
		    <td colspan = "4">
		  <b>  No GPAs Yet </b>
		    </td>
		</tr>  
		<?php }?>  
	</table>
	
	</div>
   		
<form id="edit_gp" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="update_gpa" />
		
</form>			


<script>
	$('.edit_gpa').click(function(event){
		event.preventDefault(); 
	
		     var gpa_id = $(this).attr('href');
		
	  		$('<input>').attr({
			    type: 'hidden',
			    name: 'gpa_id',
			    value: gpa_id,
			}).appendTo('#edit_gp');
			$('#edit_gp').submit();
		
	});
</script>