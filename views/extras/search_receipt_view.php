<style>
	table.receipts 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
	tr.sub_total {font-weight: bold;}
</style>

<h5>Search Receipt</h5>
<div class="loading"></div>
<div id="table_here" class="span11">

<form id="search_receipt" method="post" action=""  class="form-inline">
<?php echo $this->common->hidden_input_nonce(); ?>

<fieldset>
	<div>
		<input type="checkbox" id="chk1" name="chk1" checked="checked" value="1" />
		<label class="control-label" for="start_date">From:
		    <div class="controls date" id="date_entry" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-d'); ?>">
		    	<input style="width:90px;" type="text" id="start_date" name="start_date" placeholder="YYYY-MM-DD" value="<?php echo $this->input->post('start_date') ? $this->input->post('start_date') : date('Y-m-d')?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			</div>
		</label>
		<label class="control-label" for="end_date">To:
		    <div class="controls date" id="date_entry2" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-d'); ?>">
		    	<input style="width:90px;" type="text" id="end_date" name="end_date" placeholder="YYYY-MM-DD" value="<?php echo $this->input->post('end_date') ? $this->input->post('end_date') : date('Y-m-d')?>" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			</div>
		</label>
	</div>
	<div>
		<input type="checkbox" id="chk2" name="chk2" checked="checked" value="1" onchange="toggleDisabled2(this.checked)" />
		<label class="control-label" for="ornum">O.R. No.:
			<div>
				<input type="text" id="ornum" name="ornum" style="width:90px;">
			</div>
		</label>
		<input type="checkbox" id="chk3" name="chk3" checked="checked" value="1" onchange="toggleDisabled3(this.checked)" />
		<label class="control-label" for="idnum">Payor:
			<div>
				<input type="text" id="idnum" name="idnum" style="width:90px;">
			</div>
		</label>
	</div>	

	<div>
		<input type="checkbox" id="chk4" name="chk4" checked="checked" value="1" onchange="toggleDisabled4(this.checked)" />
		<label class="control-label" for="teller_code">Teller Code:
			<div>
				<input type="text" id="teller_code" name="teller_code" style="width:90px;">
			</div>
		</label>
		<input type="checkbox" id="chk5" name="chk5" checked="checked" value="1" onchange="toggleDisabled5(this.checked)" />
		<label class="control-label" for="machine_ip">Machine No.:
			<div>
				<input type="text" id="machine_ip" name="machine_ip" style="width:90px;">
			</div>
		</label>
		<button type="submit" class="btn">Search</button>
	</div>	
	
</fieldset>
</form>
<hr>


<?php 
	if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {

?>
<table id="receipts_t" class="table table-condensed table-bordered table-striped receipts">
<thead>
	<tr>
	<th></th>
	<th>TransDate</th>	
	<th>Post Date</th>		
	<th>Description</th>
	<th>Machine</th>
	<th>Receipt No.</th>
	<th>Amount</th>
	<th>Payor ID</th>
	<th>Payor</th>
	<th>Teller ID</th>
	<th>Teller</th>
	</tr>
</thead>

<?php
	if (count($rows) < 1) {
		echo '<td class="center" colspan=11>No record found!</td>';
	}else{
		$row_num = 0;
		//print_r($rows);die();
		foreach($rows as $row){
			$row_num++;
			
?>
			<tr>
			<td class="center"><?= $row_num ?>
			<td class="center"><?= $row->transaction_date ?> </td>
			<td class="center"><?= !empty($row->posted_on)?$row->posted_on : 'Unposted' ?> </td>
			<td class="left"><?= $row->description ?> </td>
			<td class="left"><?= $row->machine_ip ?> </td>
			<td class="center"><?= $row->receipt_no ?> </td>			
			<td class="right"><?= number_format($row->receipt_amount,2) ?> </td>			
			<td class="center"><?= $row->payor_id ?> </td>			
			<td class="left"><?= $row->payor ?> </td>			
			<td class="center"><?= $row->teller_id ?> </td>			
			<td class="left"><?= $row->teller ?> </td>			
			</tr>		

<?php
		} 		
	}
?>

</table>

<?php } 
?>






</div>


<script>
$(document).ready(function(){
	$("#receipts_t").stickyTableHeaders();
    $("#receipts_t").stickyTableHeaders('updateWidth');

	$('#date_entry').datepicker();
	$('#date_entry2').datepicker();
	
	$('#search_receipt').validate({
		submitHandler: function(form) {
			$('.loading').delay(5).fadeIn(100);
			form.submit();
		}
	});

    $("#ornum").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });	

    $('#search_receipt input[type="checkbox"]').attr('checked', false);	    
    $('#search_receipt input[type="text"]').attr('disabled', true);	    
    $('#start_date').attr('disabled', false);	    
    $('#end_date').attr('disabled', false);	    
    
		
});



function toggleDisabled2(_checked) {
    document.getElementById('ornum').disabled = _checked ? false : true;
    // if (document.getElementById('chk2').value !== 1){ document.getElementById('ornum').value=""; }        	
}

function toggleDisabled3(_checked) {
    document.getElementById('idnum').disabled = _checked ? false : true;
    //if (document.getElementById('chk3').value !== 1){ document.getElementById('idnum').value="";	}        	
}

function toggleDisabled4(_checked) {
    document.getElementById('teller_code').disabled = _checked ? false : true;
    //if (document.getElementById('chk4').value !== 1){ document.getElementById('teller_code').value=""; }        	
}

function toggleDisabled5(_checked) {
    document.getElementById('machine_ip').disabled = _checked ? false : true;
    //if (document.getElementById('chk5').value !== 1){ document.getElementById('machine_ip').value=""; }        	
}



</script>