<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
	width:100%;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

  <div style="background:#FFF; ">
  
<?php
	if (!isset($assessment)) {
?>  
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td style="text-align:center; font-weight:bold;">CANNOT POST STUDENT'S PRIVILEGE YET BECAUSE STUDENT ASSESSMENT HAS NOT BEEN POSTED YET! </td>
  </tr>
</table>
<?php
	} elseif ($privilege_availed->posted == 'Y') {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td style="text-align:center; font-weight:bold;">STUDENT'S PRIVILEGE ALREADY POSTED! </td>
  </tr>
</table>
<?php
	} else {
?>  
<form method="post" id="post_privilege_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value= "form_to_post_privileges"/>
  <?php
	if (isset($privilege_availed)) {
  ?>
  <input type="hidden" name="privilege_id" value="<?php print($privilege_availed->id); ?>" > 
	<?php
		}
	?>  
    <table align="center" cellpadding="0" cellspacing="0" class="head1" style="width:42%;">
      <tr class="head">
        <td colspan="3" class="head"> POST PRIVILEGES</td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside">
           <tr>
            <td width="129" valign="middle">Term</td>
            <td width="3" valign="middle">:</td>
            <td width="63" valign="middle"> <?php print($term->term." ".$term->sy); ?> </td>
          </tr>	   
		  <tr>
            <td valign="middle">Privilege</td>
            <td valign="middle">:</td>
            <td valign="middle"><?php print($scholarship->scholarship); ?></td>
		  </tr>
		   <tr>
		     <td valign="middle">Units</td>
		     <td valign="middle">:</td>
		     <td valign="middle"><?php print($paying_units); ?></td>
	        </tr>
			<tr>
		     <td valign="middle">Tuition</td>
		     <td valign="middle">:</td>
		     <td valign="middle"><?php print(number_format($tuition,2)); ?></td>
	        </tr>
		<tr>
            <td valign="middle"> Tuition Percentage  </td>
            <td valign="middle">:</td>
            <td valign="middle"><?php print($tuition_detail->discount_percentage); ?> % </td>
          </tr>
			<tr>
		     <td valign="middle">Tuition Discount</td>
		     <td valign="middle">:</td>
		     <td valign="middle"><?php print($tuition_disc); ?></td>
	        </tr>
			
			<?php
				if ($other_privileges) {
			?>
		   <tr>
		     <td colspan="3" valign="middle"><strong><u> Other Privileges</u></strong></td>
	        </tr>
			<?php
					$total = $tuition_disc;
					foreach($other_privileges AS $other) {
			?>
		   <tr>
		     <td valign="middle" style="padding-left:15px;"><?php if($other->weight != 1){ print($other->fees_group); ?></td>
		     <td valign="middle">:</td>
		     <td align="left" valign="middle" style="padding-right:5px;">
			 		<?php 
						print(number_format($other->discount_amount,2)); 
						$total = $total + $other->discount_amount;
			 		 ?></td>
	        </tr>
			<?php
			 	       }	
					}
				}
				
			?>
		     <tr>
		     <td align="right" valign="middle" style="padding-left:15px;"><strong>TOTAL DISCOUNT</strong></td>
		     <td valign="middle">:</td>
		     <td align="left" valign="middle" style="padding-right:5px;"><strong><?php print(number_format($total,2));?></strong></td>
	       </tr>
		   
		     <tr>
            <td valign="middle"> Remarks </td>
            <td valign="middle">:</td>
            <td valign="left"> <input name="Remarks" type="text" id="Remarks" size="50" maxlength="50" style="width:200px; height:12px;" ></td>
          </tr>
		   
		   <tr>
		      <td colspan="3" valign="middle"><input type="submit" name="action" id="post_privilege" value="POST!"  class="btn btn-success" /></td>
			</tr>
			  
        </table></td>
      </tr>
    </table>
  </form>
<?php			
	}
?>

  </div>

<script>
$('#post_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue posting privilege?");

	if (confirmed){
		$('#post_privilege_form').submit();
	}		
});
</script>