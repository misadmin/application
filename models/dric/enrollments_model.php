<?php
class Enrollments_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}
	public function students_courses_and_grades($academic_terms_id, $academic_program, $year_level){
		$this->db->select("
				s.idno,
				ap.abbreviation as program_abbreviation,
				sh.year_level,
				CONCAT(IF(at.term='1', 'First Sem', IF(at.term='2', 'Second Sem', IF(at.term='3','Summer', ''))), ' SY ', (ay.end_year-1), '-', ay.end_year) as academic_term, 
				CONCAT(s.lname, ', ', s.fname, IF(ISNULL(s.mname), '', CONCAT(' ', SUBSTR(s.mname, 1, 1), '.'))) as fullname,
				GROUP_CONCAT(DISTINCT CONCAT('[', c.course_code, ':', e.finals_grade, ':', 				
					if(!isnull(eo.id), eo.credit_units, c.credit_units), 					
					':' , 
					if(!isnull(eo.id), eo.is_bracketed, pc.is_bracketed),
					']') ORDER BY c.course_code) as course_grade_units"
				, FALSE)
				->from('enrollments e')
				->join('enrollments_override eo','eo.enrollments_id=e.id', 'left')				
				->join('student_histories sh','sh.id=e.student_history_id', 'left')
				->join('course_offerings co','e.course_offerings_id=co.id', 'left')
				->join('courses c', 'co.courses_id=c.id', 'left')
				->join('students s', 's.idno=sh.students_idno', 'left')
				->join('prospectus p', 'p.id=sh.prospectus_id', 'left')
				->join('academic_programs ap', 'ap.id=p.academic_programs_id', 'left')
				->join('prospectus_courses pc', 'pc.courses_id=c.id', 'left')
				->join('academic_terms at', 'sh.academic_terms_id=at.id', 'left')
				->join('academic_years ay', 'at.academic_years_id=ay.id', 'left')
				->where('sh.academic_terms_id', $academic_terms_id)
				->where('ap.id', $academic_program)
				->where('sh.year_level', $year_level)
				->group_by('s.idno')
				->order_by('s.lname, s.fname');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$return = array();
			$row = $query->row();
			$return['program_details'] = (object)array(
					'program_abbreviation'=>$row->program_abbreviation,
					'year_level'=>$row->year_level,
					'academic_term'=>$row->academic_term
					);
			$return['result'] = array();
			foreach($query->result() as $row){
				preg_match_all('!(\[(.*):(.*):(.*):(Y|N)\])[,]{0,1}!', str_replace(',', ",\n", $row->course_grade_units), $courses_enrolled, PREG_SET_ORDER);
				$total_units = 0;
				$courses = array();
				foreach($courses_enrolled as $course){
					$courses[] = (object)array(
							'course'=>$course[2],
							'final_grade'=>$course[3],
							'credit_units'=>($course[5]=='Y' ? '(' . $course[4] . ')' : $course[4]),
							);
					$total_units += ($course[5]!='Y' ? $course[4] : 0); 
				}
				$return['result'][$row->idno] = (object)array(
						'student_details'=>(object)array(
								'fullname'=>$row->fullname,
								'id_number'=>$row->idno,
								'total_units'=>$total_units),
						'courses'=>$courses);
			}
			//print_r ($return); die();
			return $return;
		} else {
			return FALSE;
		}
	}
}