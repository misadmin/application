<div class="row-fluid">
	<div class="span12">
	<b>Note! </b> <br>
	<i>Recent grades are found on the top most.</i> <br>
	<i>Failed courses or courses with no final grades yet are highlighted in red.</i> <br>
	<br><br>
	</div>
</div>
<?php if (isset($terms) && count($terms) > 0): 
		//print_r($terms); die();	
?>
<?php foreach($terms as $term): 
?>
<div class="row-fluid">
	<div class="span12">
		<table class="table table-bordered table-hover table-condensed table-grades mediaTable activeMediaTable" style="margin-bottom:20px !important">
			<thead>
				<tr>
					<th colspan="6"><?php echo $term['term']; ?> [<?php echo $term['program'] . " " . $term['year_level']; ?>] <?php echo $term['school_year']; ?></th>
					<th style="text-align:right">
						<?php 
							if ($this->uri->segment(1) == "dric") {
								$grade_term= json_encode($term);
						?>
						<a style="font-weight: normal;" class="download_grade" href="" 
							grade_term='<?php print($grade_term); ?>'>Download Grade as PDF<i class="icon-download-alt"></i></a>
						<?php 
							} else {
								print("&nbsp;");
							}
						?>
					</th>
				</tr>
				<tr>
					<th width="15%">Catalog Number</th>
					<th width="30%">Descriptive Title</th>
					<th width="5%">Credit Units</th>
					<th width="6%">Prelim</th>
					<th width="6%">Midterm</th>
					<th width="6%">Finals</th>
					<th width="32%">Teacher</th>
				</tr>
			</thead>
			<tbody>
				<?php $credit_units = 0; ?>
				<?php if(isset($term['courses']) && count($term['courses']) > 0): ?>
				<?php
					//print_r($term['courses']); die();
					foreach($term['courses'] as $subject): ?>
						<?php // <tr  if ($subject['course']->finals_grade > 3.0 || ! is_numeric ($subject['course']->finals_grade)) echo 'class="error" '; ?>
						<?php 
						echo '<tr';
						if (is_numeric($subject['course']->finals_grade)){
							/*if ($subject['course']->finals_grade < 1.5){
								echo ' class="success"';
							}
							else */
							if ($subject['course']->finals_grade > 3.0){
								echo ' class="error"';
							}
						}
						else {
							echo ' class="error"';
						}
						echo '>';
						?>
							<td><?php 
									echo $subject['course']->course_code . ' ('.$subject['course']->section_code . ')'
									. ($this->userinfo['empno']=='404' ? ' '.$subject['course']->enrollments_id:'')	; ?></td>
							<td><?php echo $subject['course']->descriptive_title; ?></td>
							<?php 
								if ($subject['course']->is_bracketed == 'Y') {
							?>
							<td style="text-align:center;"><?php echo "(".strtoupper($subject['course']->credit_units).")" ?></td>
							<?php 
								} else {
									if ($subject['course']->finals_grade <= 3.0 && is_numeric($subject['course']->finals_grade)) {
										$credit_units += $subject['course']->credit_units;
									}	
							?>
							<td style="text-align:center;"><?php echo strtoupper($subject['course']->credit_units) ?></td>
							<?php 
								}
							?>
							<td style="text-align:center;"><?php echo strtoupper($subject['course']->prelim_grade) ?></td>
							<td style="text-align:center;"><?php echo strtoupper($subject['course']->midterm_grade) ?></td>
							<td style="text-align:center;"><strong><?php echo strtoupper($subject['course']->finals_grade) ?></strong></td>
							<td><?php echo $subject['course']->instructor; ?></td>
							<?php 
							?>
						</tr>
				<?php endforeach; ?>
				<tr>
					<td> &nbsp; </td>
					<td style="text-align:right;"> TOTAL CREDIT UNITS </td>
					<td style="text-align:center;"> <strong><?php echo number_format($credit_units,1); ?></strong> </td>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>
<?php endforeach; ?>
<style>
table.table-grades th:nth-child(1){width:15%}
table.table-grades th:nth-child(2){width:35%}
table.table-grades th:nth-child(3){width:5%}
table.table-grades th:nth-child(4){width:6%}
table.table-grades th:nth-child(5){width:6%}
table.table-grades th:nth-child(6){width:6%}
table.table-grades th:nth-child(7){width:27%}

</style>
<script>
$().ready(function(){
var offset = $('.navbar').height();
//$('.stickyHeader').stickyTableHeaders({fixedOffset: offset-1});
});
</script>
<?php else: ?>
	<h3>No Records Found</h3>
<?php endif;?>


  <form id="download_grade_form" method="post" target="_blank">
  <input type="hidden" name="action" value="generate_grade" />
  </form>
<script>
$(document).ready(function(){
   
   $('.download_grade').click(function(event){
		event.preventDefault(); 
		var grade_term = $(this).attr('grade_term');
		$('<input>').attr({
			type: 'hidden',
			name: 'grade_term',
			value: grade_term,
		}).appendTo('#download_grade_form');
		
		$('#download_grade_form').submit();
		
	});
	
	
});
	
</script>

