<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>

<div id="show_section_management_page" >
	<h2 class="heading">Section Management</h2>
	<div id="rounded_header" style="width:<?php print($various['page_width1']); ?>; font-size:12px; <?php print($various['page_align']); ?> margin-top:15px; padding:0px;">
		<div id="container">
			<div id="left" >
				<div id="show_error_msg" style="padding-left:10px; padding-top:10px; color:#F92424; font-weight:bold; font-size:14px;">
					
				</div>
			</div>
			<div id="right">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<?php 
					if ($various['can_edit']) {
				?>
				<div style="float:left; padding-top:4px; padding-right:4px; font-size:14px;" >
					<a href="#" class="btn btn-success add_section" >
						<i class="icon-plus-sign" style="margin-top:2px;"></i> New Section
					</a>
				</div>
				<?php	
					}
				?>
					<div style="float:left; padding-top:5px;" class="b2">
						<select name="acad_program_groups_id" class="form-control change_select" style="width:auto; height:auto;" id="acad_program_groups_id">
							<?php 
								if ($levels) {
									foreach ($levels AS $level) {
							?>		
										<option value="<?php print($level->id); ?>" ><?php print($level->group_name); ?></option>
							<?php
									}
								}
							?>
						</select>
					</div>
					<div style="float:left; padding-top:5px;" class="b2">
						<select name="academic_years_id" class="form-control change_select" style="width:auto; height:auto;" id="academic_years_id">
							<?php 
								if ($years) {
									foreach ($years AS $year) {
							?>		
										<option value="<?php print($year->id); ?>" 
											yr_text="<?php print($year->sy); ?>"
											<?php if ($acad_year_id == $year->id) print("selected"); ?>>
											<?php print($year->sy); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
				
			</div>
		</div>
	</div>

	
	<div style="width:<?php print($various['page_width2']); ?>; overflow:auto; <?php print($various['page_align']); ?> margin-top:15px;" id="show_extracted_items" >
		<?php
			$data = array(
					"sections"=>$sections,
			);
			
			if ($various['can_edit']) {
				$this->load->view('bed/sections/list_all_sections',$data);			
			} else {
				$this->load->view('bed/sections/list_all_sections_for_reports',$data);							
			}
		?>
	</div>
</div>


	<script>
			$('.change_select').change(function(){
				
				var acad_program_groups_id 	= $("select#acad_program_groups_id option:selected").attr('value');
				var academic_years_id		= $("select#academic_years_id option:selected").attr('value');
				
				$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

				$.ajax({
					cache: false,
					type: 'POST',
					url: "<?php echo site_url($this->uri->segment(1).'/section_management'); ?>",
					data: {"acad_program_groups_id": acad_program_groups_id,
							"academic_years_id": academic_years_id,
							"action": "change_select" },
					dataType: 'json',
					success: function(response) {													

						$('#show_extracted_items').html(response.output); 

						$('#show_add_icon').html("")
											
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
					
				});

			});
			
	</script> 


	<script>
		$('.add_section').click(function(event){
			
			var acad_program_groups_id	= $("select#acad_program_groups_id option:selected").attr('value');
			var academic_year			= $("select#academic_years_id option:selected").attr('yr_text');
			var level_text 				= $("select#acad_program_groups_id option:selected").text();
			
			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/section_management'); ?>",
				data: {"acad_program_groups_id": acad_program_groups_id,
						"action": "extract_year_levels" },
				dataType: 'json',
				success: function(response) {													

					$('#modal_new_section').modal('show');
					
					$("#year_text_modal").val(academic_year);
					$("#level_text").val(level_text);
					$("#section_name_modal").val('');
					$("#section_name_modal").focus();

					$('#year_levels').html(response.output); 
					
					$('#show_add_icon').html("");
					
										
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
					
			});
			
		});
	</script>				


	<div id="modal_new_section" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_section_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">New Section</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="term_id_modal"></div>
						<?php
							$this->load->view('bed/sections/modal_view/new_section_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Section!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<script>
		$('#add_section_form').submit(function(e) {
			e.preventDefault();
			
			var acad_program_groups_id	= $("select#acad_program_groups_id option:selected").attr('value');
			var academic_programs_id	= $("select#academic_programs_id option:selected").attr('value');
			var academic_years_id		= $("select#academic_years_id option:selected").attr('value');
			var section_name 			= $('#section_name_modal').val();
			var year_level				= $("select#academic_programs_id option:selected").attr('year_level');
						
			$('#modal_new_section').modal('hide');
			
			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/section_management'); ?>",
				data: {"acad_program_groups_id": acad_program_groups_id,
						"academic_programs_id": academic_programs_id,
						"academic_years_id": academic_years_id,
						"year_level": year_level,
						"section_name": section_name,
						"action": "add_section" },
				dataType: 'json',
				success: function(response) {													
																			
					$('#show_extracted_items').html(response.output); 
					
					$('#show_add_icon').html('');
					
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
			
		});
	</script>				



<!--This script forces the select to go back to default once Back button of browser is pressed-->
<script>
$(document).ready(function () {
    $("select").each(function () {
        $(this).val($(this).find('option[selected]').val());
    });
})
</script>