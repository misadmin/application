<div style="float:left; width:auto; margin-top:10px">
<form action="" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="set_transferOut_date" />
	
	
<div class="formSep" style = "margin-left:30px">
	<h2><?php print($title); ?></h2>
</div> 
	<fieldset>
		<div class="control-group formSep" style = "margin-left:30px">
				<label class="control-label"> <b> Academic Year &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </b> </label>
					<div class="controls">
						<select id="academic_year_id" name="academic_year_id">
						<?php
							foreach($academic_years AS $academic_year) {
								print("<option value=".$academic_year->id.">".$academic_year->start_year."-".$academic_year->end_year."</option>");	
							}
						?>
						</select>
					</div>
		</div>
	
		<div class="control-group formSep" style = "margin-left:30px">
		    <label class="control-label" for="dateS"><b>Date <i>(mm-dd-yyyy) </i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b></label>
		    <div class="controls">
			    <input type="date" name="transDate" value="<?php echo date('Y-m-d'); ?>" >
	    	</div>
	    </div>
	    
	     
		<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">Set Date!</button>
					</div>
		</div>	
	</fieldset>
</form>

<script>
$('#levels').change(function(){
	var level_id = $('#levels option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/year"); ?>/?level=" + level_id,
		dataType: "json",
		success: function(data){
			var doptions = make_options_year(data);
			$('#year').html(doptions);
		}	
	});	
});


function make_options_year (data){
var doptions = '<option value="">-- Select Year --</option>';
for (var i = 0; i < data.length; i++) {
	doptions = doptions 
	 	+ '<option value="'
	 	+ data[i].yr_level
	 	+ '">'
	 	+ data[i].yr_level
	 	+ '</option>';
}
return doptions;
}

function make_options_section (data){
var doptions = '<option value="">-- Select Section --</option>';
for (var i = 0; i < data.length; i++) {
	doptions = doptions 
	 	+ '<option value="'
	 	+ data[i].id
	 	+ '">'
	 	+ data[i].section
	 	+ '</option>';
}
return doptions;
}

</script>