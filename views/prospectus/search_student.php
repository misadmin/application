<p>
<?php 
	if (isset($error)) {
		print($error);
	}
?>
<form action="<?php echo site_url('prospectus/search_student');?>" method="post">
<?php $this->common->hidden_input_nonce(FALSE); ?>
<?php echo validation_errors(); ?>
	<table>
    	<tr>
        	<td>ID No.:</td>
            <td><input type="text" name="idno" /></td>
        </tr>
    	<tr>
        	<td colspan="2"><input type="submit" value="Search!" class="btn btn-primary"/></td>
        </tr>
    </table>
</form>