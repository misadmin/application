<style>
.data-group {width:100%;padding:10px;}
.data-group .data-label {width:30%;float:left}
.data-group .data-content {width:70%;float:right;font-weight:bold;}
.data-group .data-content a {padding:5px;margin: 0px 5px 1px 0px;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;font-size:0.7em;background-color:#CCFFCC}
.danger {background-color:#f2dede!important;color:#7A1A1A!important;}
.heading {margin-top: 20px;}
</style>
<div class="row-fluid">
	<div class="span11">
		<div class="row-fluid">
			<h5 class="heading">Primary Educational Info
				<span class="pull-right">
					<a href="#" class="add" data-what="primary"><i class="icon-plus"></i>Add Prim. Educ.</a>
				</span>
			</h5>
		</div>
		<?php if(isset($primary)): ?>
		<?php foreach ($primary as $school):  list($sy_start, $sy_end) = explode("-", $school->school_year); ?>
		<div class="row-fluid">
			<div class="data-group">
				<div class="data-label">School</div>
				<div class="data-content"><?php echo $school->school; ?></div>
				<div class="data-label">School Year</div>
				<div class="data-content"><?php echo $school->school_year; ?></div>
				<div class="data-label"></div>
				<div class="data-content">
					<a href="#" class="edit_prelim_educ" data-level_id="3" data-level="Primary" data-id="<?php echo $school->student_educations_id; ?>" data-school="<?php echo $school->school; ?>" data-sy_start="<?php echo $sy_start; ?>" data-sy_end="<?php echo $sy_end; ?>" >Edit</a> 
					<a class="danger delete_prelim_educ" href="#" data-id="<?php echo $school->student_educations_id; ?>">Delete</a></div>
			</div>
		</div>
		<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid">
	<div class="span11">
		<div class="row-fluid">
			<h5 class="heading">Intermediate Educational Info
				<span class="pull-right">
					<a href="#" class="add" data-what="intermediate"><i class="icon-plus"></i>Add Interm. Educ.</a>
				</span>
			</h5>
		</div>
		<?php if(isset($intermediate)): ?>
		<?php foreach ($intermediate as $school):  list($sy_start, $sy_end) = explode("-", $school->school_year); ?>
		<div class="row-fluid">
			<div class="data-group">
				<div class="data-label">School</div>
				<div class="data-content"><?php echo $school->school; ?></div>
				<div class="data-label">School Year</div>
				<div class="data-content"><?php echo $school->school_year; ?></div>
				<div class="data-label"></div>
				<div class="data-content">
					<a href="#" class="edit_prelim_educ" data-level_id="3" data-level="Intermediate" data-id="<?php echo $school->student_educations_id; ?>" data-school="<?php echo $school->school; ?>" data-sy_start="<?php echo $sy_start; ?>" data-sy_end="<?php echo $sy_end; ?>" >Edit</a>  
					<a class="danger delete_prelim_educ" href="#" data-id="<?php echo $school->student_educations_id; ?>">Delete</a></div>
			</div>
		</div>
		<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid">
	<div class="span11">
		<div class="row-fluid">
			<h5 class="heading">Secondary Educational Info
				<span class="pull-right">
					<a href="#" class="add" data-what="secondary"><i class="icon-plus"></i>Add Sec. Educ.</a>
				</span>
			</h5>
		</div>
		<?php if(isset($secondary)): $year_levels = array(1=>'1st Year', 2=>'2nd Year', 3=>'3rd Year', 4=>'4th Year', 5=>'5th Year', 6=>'6th Year', 7=>'7th Year', 8=>'8th Year'); $count = 1; ?>
		<?php foreach ($secondary as $school): list($sy_start, $sy_end) = explode("-", $school->school_year); ?>
		<div class="row-fluid">
			<div class="data-group">
				<div class="data-label">School</div>
				<div class="data-content"><?php echo $school->school; ?></div>
				<div class="data-label">Year Level</div>
				<div class="data-content"><?php echo $year_levels[$count]; ?></div>
				<div class="data-label">School Year</div>
				<div class="data-content"><?php echo $school->school_year; ?></div>
				<div class="data-label"></div>
				<div class="data-content">
					<a href="#" class="edit_prelim_educ" data-level_id="4" data-level="Secondary" data-id="<?php echo $school->student_educations_id; ?>" data-school="<?php echo $school->school; ?>" data-sy_start="<?php echo $sy_start; ?>" data-sy_end="<?php echo $sy_end; ?>" >Edit</a> 
					<a class="danger delete_prelim_educ" href="#" data-id="<?php echo $school->student_educations_id; ?>">Delete</a></div>
			</div>
		</div>
		<?php $count++; endforeach; ?>
		<?php endif; ?>
	</div>
</div>
<div id="add_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form id="add_prelim_educ_form" class="form-horizontal" method="post">
		<input type="hidden" name="action" value="insert_preliminary_education" />
		<input type="hidden" name="level" value="" />
		<input type="hidden" name="se_id" value="" />
		<?php echo $this->common->hidden_input_nonce(); ?>
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 id="add_modal_label">Modal header</h3>
		</div>
		<div class="modal-body">
			<fieldset>
				<div class="control-group formSep">
					<label class="control-label">
						School
					</label>
					<div class="controls"><input type="text" class="input-xlarge" name="school" id="school"/></div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">
						School Year
					</label>
					<div class="controls"><input type="text" class="input-small" name="sy_start"/> - <input type="text" class="input-small" name="sy_end"/></div>
				</div>
			</fieldset>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>
<form id="delete_prelim_educ_form" method="post">
	<?php echo $this->common->hidden_input_nonce(); ?>
	<input type="hidden" name="action" value="delete_preliminary_education" />
</form>
<script>
$().ready(function(){
	$('.add').on('click', function(){
		var modal_title = "";
		var has_year_level = true;
		var level_id = 3;
		var level = '';
		switch ($(this).data('what')){
			case 'primary' :
				modal_title = "Add Primary School";
				has_year_level = false;
				level_id = 3;
				level = 'Primary';
				break;
			case 'intermediate' :
				modal_title = "Add Intermediate School";
				has_year_level = false;
				level_id = 3;
				level = 'Intermediate';
				break;
			case 'secondary' :
				modal_title = "Add Secondary School";
				has_year_level = true;
				level_id = 4;
				level = 'Secondary';
				break;
		}
		options = { 
			serviceUrl:'<?php echo site_url("ajax/schools"); ?>/'+level_id+'/',
			minChars:2,
		};
		$('#school').val('');
		$('#add_prelim_educ_form input[name="level"]').val(level);
		$('#add_prelim_educ_form input[name="action"]').val('insert_preliminary_education');
		a = $('#school').autocomplete(options);
		$('#add_modal_label').html(modal_title);
		$('#add_prelim_educ_form input[name="sy_start"]').val('');
		$('#add_prelim_educ_form input[name="sy_end"]').val('');
		$('#add_modal').modal('show');
	});
	$('.edit_prelim_educ').on('click', function(){
		console.log('here');
		$('#add_modal_label').html('Edit '+$(this).data('level')+' School');
		$('#school').val($(this).data('school'));
		$('#add_prelim_educ_form input[name="level"]').val($(this).data('level'));
		$('#add_prelim_educ_form input[name="action"]').val('edit_preliminary_education');
		$('#add_prelim_educ_form input[name="sy_start"]').val($(this).data('sy_start'));
		$('#add_prelim_educ_form input[name="sy_end"]').val($(this).data('sy_end'));
		$('#add_prelim_educ_form input[name="se_id"]').val($(this).data('id'));
		options = { 
			serviceUrl:'<?php echo site_url("ajax/schools"); ?>/'+$(this).data('level_id')+'/',
			minChars:2,
		};
		a = $('#school').autocomplete(options);
		$('#add_modal').modal('show');
	});	
	$('.delete_prelim_educ').on('click', function(){
		var continue_this = confirm('Are you sure you want to delete this?');

		if (continue_this){
			var id = $(this).data('id');

			$('<input>').attr({
			    type: 'hidden',
			    name: 'id',
			    value: id,
			}).appendTo('#delete_prelim_educ_form');
			$('#delete_prelim_educ_form').submit();
		}	
	});
	$('#add_prelim_educ_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			school: { required: true,},
			sy_start: { required: true,  number: true, min:1900, max:<?php echo date('Y'); ?>},
			sy_end: { required: true,  number: true, min:1900, max:<?php echo date('Y'); ?>}
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
})
</script>
<style>
.autocomplete {
	background-color: #F7F7F7;
	border: 1px solid #EEEEEE;
}
.autocomplete .selected {
	background-color: #CECECE;
}
</style>