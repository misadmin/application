<h2 class="heading">Transcript of Records</h2>
<table class="table table-condensed" style="width:40%; border: solid; border-width: 1px; 
				border-color: #D8D8D8;">
	<form method="POST" target="_blank">
  		<input type="hidden" name="action" value="generate_tor_pdf" />
 	<thead>
		<tr>
			<th colspan="3">Generate TOR to PDF</th>
		</tr>
	</thead>
	<tr>
		<td colspan="3">
			<input type="checkbox" name="grant_status" value="Y" checked="checked" style="margin-right:10px;">Granted Automous Status?
		</td>
	</tr>
	<tr>
		<td>Prepared By</td>
		<td>:</td>
		<td><select name="prepared_by"> 
			<?php 			
				foreach($evaluators AS $eval) {
					print("<option value=\"".$eval->fname." ".$eval->mname." ".$eval->lname."\">".$eval->lname.", ".$eval->fname."</option>");
				}
			?></select>
		</td>
	</tr>	
	<tr>
		<td>Checked By</td>
		<td>:</td>
		<td><select name="checked_by"> 
			<?php 
				foreach($evaluators AS $eval) {
					print("<option value=\"".$eval->fname." ".$eval->mname." ".$eval->lname."\">".$eval->lname.", ".$eval->fname."</option>");
				}
			?></select>
		</td>
	</tr>	
	<tr style="vertical-align:middle">
		<td>Date</td>
		<td>:</td>
		<td><input type="text" name="date_tor" value="<?php print(date('m/d/Y'));?>" style="width:80px;"> <span style="font-style:italic; color:#FF0000;">(Format: mm/dd/yyyy)</span>
		</td>
	</tr>	
	<tr>
		<td>Remarks</td>
		<td>:</td>
		<td><input type="text" name="remarks" style="width:250px;">
		</td>
	</tr>	
	<tr>
		<td colspan="3">
			<input type="submit" value="Proceed >>" class="btn btn-primary">
		</td>
	</tr>	
	</form>
</table>
					
 		<div style="margin-bottom:20px; margin-top:20px;">
		
		<?php
			//print_r($student_tor);die();
			if ($student_tor) {
				foreach ($student_tor AS $s_tor) {
		?>
					<div style="width:100%; margin-bottom:5px; ">
					<table border="0" cellspacing="0" cellpadding="3" style="width:100%;">
					  <tr>
					    <td width="72%" align="left" style="font-size:14px; font-weight:bold;">
					    	<?php 
					    		print("<span style=\"font-size:18px;\">".$s_tor->program_name."</span><br>");
					    		print("<span style=\"font-style:italic;\">".$s_tor->term." ".
										$s_tor->sy." ".$s_tor->school_name."</span>");	
					     	?>
					     </td>
					    </tr>
					</table>
					
					</div>
		 
					<div style="width:100%; margin-bottom:30px; ">
					  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
					    <thead>
						    <tr>
						      <th width="20%" class="head" style="vertical-align:middle;">Course No.</th>
						      <th width="64%" class="head" style="vertical-align:middle;">Descriptive Title</th>
						      <th width="8%" class="head" style="vertical-align:middle;">Final Rating</th>
						      <th width="8%" class="head" style="vertical-align:middle;">Units</th>
						    </tr>
					    </thead>
					    <?php
							$courses = $this->Student_Model->List_Student_TOR_Courses($s_tor->student_histories_id,
												$s_tor->other_schools_id,$s_tor->prospectus_id);
								
							//$courses = $this->Student_Model->List_Student_TOR_Courses(181353,
							//					$s_tor->other_schools_id,161);
					    	//print_r($courses); die();
							
							if ($courses) {
								foreach ($courses AS $course) {
						?>
								    <tr>
								      <td style="text-align:left; vertical-align:middle; "><?php echo $course->course_code . ($this->session->userdata('empno')=='0404' ? ' '. $course->enr_id : ''); ?></td>
								      <td style="text-align:left; vertical-align:middle; "><?php print($course->descriptive_title); ?></td>
								      <td style="text-align:center; vertical-align:middle; ">
								      <?php
								      	if ($course->finals_grade) { 
								      		print($course->finals_grade); 
								      	} else {
											print("-");
										}	
								      ?></td>
								      <td style="text-align:center; vertical-align:middle; ">
								      	<?php
											$failing_grade = array('5.0','WD','DR','NA','NC','INC','INE','NG',' ','IP');
											if (!$course->finals_grade OR (in_array($course->finals_grade,$failing_grade) AND
														$course->what_school == 'hnu')) {
												print("-");
								      		} else {
												if ($course->credit_units < 0) {
													print('('.abs($course->credit_units).')');
												} else {
								      				print($course->credit_units);
								      			}
								      		} 
								      	?></td>
								    </tr>
					    <?php
								}
							}
							
						?>
					
					</table>
					</div>
					
					<?php 
						$statement = $this->Student_Model->getGraduatedStatement($s_tor->student_histories_id,$s_tor->other_schools_id);				
					
						if ($statement) {
					?>
					
							<div style="border-radius: 5px; background-color: #C3FCBD; border: solid; border-width: 1px; border-color: #D8D8D8; height: 60px; padding: 2px; text-align:right; margin-bottom:30px;">
								<table style="font-weight: bold; font-size: 16px; text-align:left; vertical-align: top;">
									<tr>
										<td style="width:30%; vertical-align: top;">GRADUATED: </td>
										<td style="vertical-align: top;">
											<?php 
												/*
												if ($statement->what_school == 'hnu') {
													$grad_statement = $statement->graduation_statement." ".$statement->program_name.".";
												} else {
													$grad_statement = $statement->graduation_statement;
												}*/	
												print($statement->graduation_statement);
											?>
										</td>
									</tr>	
								</table>
							</div> 
					<?php 					
						}
					?>
		<?php
			}
		}
		?>
	</div>
	

  <form id="download_tor_form" method="post" target="_blank">
  <input type="hidden" name="action" value="generate_tor_pdf" />
</form>
<script>
	$('#download_tor').click(function(event){
		event.preventDefault(); 
		var student_tor = $(this).attr('student_tor');
			
		$('<input>').attr({
			type: 'hidden',
			name: 'student_tor',
			value: student_tor,
		}).appendTo('#download_tor_form');
		$('#download_tor_form').submit();
		
	});
</script>

