<?php 
//print_r( get_defined_vars());die();

$this->session->set_userdata('flash_idnumber',$idnumber);
//$this->session->set_userdata('flash_student_name', $familyname . ', ' . $firstname . ' ' . substr($middlename,0,1) . '.' );
$this->session->set_userdata('flash_student_lname', $firstname);
$this->session->set_userdata('flash_student_fname', $familyname);
$this->session->set_userdata('flash_student_m_initial', substr($middlename,0,1) . '.');
$this->session->set_userdata('flash_course',$level .' '.$course);
$this->session->set_userdata('emergency_notify', $emergency_notify);
$this->session->set_userdata('emergency_telephone', $emergency_telephone);
$this->session->set_userdata('emergency_address', $emergency_address);


?>

<div class="row-fluid span12">
<?php if(isset($message) && $tab=='print_shs_id'): ?>
	<div class="alert alert-<?php echo $severity; ?>">
		<a class="close" data-dismiss="alert">&times;</a>
		<?php echo $message; ?>
	</div>
<?php endif; ?>
	<form method="post" id="print_id_form">
	<input type="hidden" name="action" value="print_id" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
		<fieldset>
			<div class="controls">
				<button onclick="previewID()" type="button" class="btn btn-primary btn-large">Preview ID</button>
			</div>
		</fieldset>
	</form>
</div>
<script>
function previewID(){
	window.open("<?php echo site_url('/dric/print_shs_id'); ?>", "_blank","toolbar=no, scrollbars=no, resizable=yes, top=500, left=500, width=400, height=400");
}
</script>				





