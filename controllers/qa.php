<?php

class Qa extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$navigation = array(
						'Students'=>'student',
						'Faculty'=>'faculty',
						'Offerings'=>'all_offerings',					
						'Programs' => array(
								'Offer/Freeze' => 'programs'),
						'Reports' => array(
								'Enrollment Summary' => 'enrollment_summary',
								'List of Courses' => 'courses',
								'Withdrawals'=>'list_withdrawals',
								'List of New Enrollees'=>'list_new_enrollees')
						//'Colleges' => 'colleges'
				);
		
		$this->navbar_data['menu'] = $navigation;
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		$this->content_lib->set_title ('Registrar | ' . $this->config->item('application_title'));
		$this->load->model('hnumis/Enrollments_model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('hnumis/Programs_model');
		$this->load->model('hnumis/College_model');
		$this->load->model('hnumis/AcademicYears_model');
		$this->load->model('Academic_terms_model');
		$this->load->helper('student_helper');
	}

	/*
	 * @updated: 8/29/14 by: genes
	 * @notes: to include dissolve offering
	 */
	function all_offerings() {
	
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Offerings_Model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('academic_terms_model');
		
		$acad_terms = $this->AcademicYears_Model->ListAcademicTerms(FALSE);

		$step = ($this->input->post('step') ?  $this->input->post('step') : 'display_offerings');
	
		switch ($step) {
			case 'display_offerings':
				$current_term = $this->academic_terms_model->getCurrentAcademicTerm();
				$offerings = $this->Offerings_Model->ListOfferings($current_term->id);
	
				$data = array(
						"terms"=>$acad_terms,
						"selected_term"=>$current_term->id,
						"offerings"=>$offerings,
				);
	
					
				break;
				
			case 'display_selected_offerings':
	
				$term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
				
				$offerings = $this->Offerings_Model->ListOfferings($this->input->post('academic_terms_id'));
	
				$data = array(
						"terms"=>$acad_terms,
						"selected_term"=>$this->input->post('academic_terms_id'),
						"offerings"=>$offerings,
				);
	
				break;

			case 'show_tab':

				$this->session->set_userdata('course_offerings_id', $this->input->post('offering_id'));
				$this->session->set_userdata('courses_id', $this->input->post('courses_id'));
				$this->session->set_userdata('parallel_no', $this->input->post('parallel_no'));
				
				//$offering_id = $this->input->post('offering_id');
				//$id = "('".$this->input->post('offering_id')."')";
				//$data['title']="Class List";
				//$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
				//$data['enrollees'] = $this->Student_model->AllEnrollees($id);
				//print_r($data['new_offerings']); die();			
				$data = $this->CollateData();

				$course_info = $this->Enrollments_model->getCourse_id($this->input->post('offering_id'));
				$data['offerings'] = $this->Offerings_Model->getOffering($this->input->post('offering_id'));
				$data['owner_colleges_id'] = $this->session->userdata('college_id');
				$this->session->set_userdata('academic_terms_id',$data['offerings']->academic_terms_id);
				$data['new_offerings'] = $this->Enrollments_model->list_offerings($course_info->id, $data['offerings']->academic_terms_id,
						$this->input->post('offering_id'));
				$data['offering_slots'] = $this->Offerings_Model->ListOfferingSlots($this->input->post('offering_id'));
				
				$data['active'] = "tab1";
				//print_r($data['offering_slots']); die();
				$this->content_lib->enqueue_body_content('registrar/course_actions',$data);
				$this->content_lib->content();
				
				return;

			case 'move_students':
				
				if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					$enrollees = $this->input->post('newlist');

					if ($enrollees) {
						foreach($enrollees as $index => $enrollee){
							$result = $this->Enrollments_model->updateCourseOffering($index,
															$this->input->post('offering_id'),
															$this->input->post('section'), 
															$this->session->userdata('empno'));
						}
						if($result){
							$this->content_lib->set_message("Student/s  Successfully Transferred!", "alert-success");
						}
					} else {
							$this->content_lib->set_message("ERROR in transferring student/s!", "alert-error");
					}
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
				}	

				$current_term = $this->academic_terms_model->getCurrentAcademicTerm();
				$offerings = $this->Offerings_Model->ListOfferings($current_term->id);
				
				$data = array(
						"terms"=>$acad_terms,
						"selected_term"=>$current_term->id,
						"offerings"=>$offerings,
				);
				
				break;
				
			case 'download_dissolved_students': //download pdf of affected students to dissolve
					$this->load->library('registrar_lib');
					$parallel_offerings = json_decode($this->input->post('parallel_offerings'));
					$offering_slots = json_decode($this->input->post('offering_slots'));
					$dissolve_students = str_replace("\'", "'", $this->input->post('dissolve_students'));
					$dissolve_students = json_decode($dissolve_students);
					//print_r($parallel_offerings); die();
						
					$this->registrar_lib->Print_DissolveStudents_pdf($dissolve_students,
							$this->input->post('academic_term'),
							$this->input->post('teacher'),
							$parallel_offerings,
							$offering_slots);
					break;

			case 'dissolve_offering': //dissolve offering
							
				$this->load->model('teller/assessment_model');
				$this->load->model('hnumis/enrollments_model');
				$this->load->model('hnumis/Student_Model');
				$this->load->model('hnumis/Offerings_Model');
				$this->load->model('hnumis/Rooms_Model');
				$this->load->model('ledger_model');
				//print($this->input->post('offering_slots')); die();
				if ($this->Offerings_Model->NotDissolved($this->session->userdata('course_offerings_id'))) {
					
						$error_msg="";
					
						$offerings = $this->Offerings_Model->ListParallel_Offerings('',$this->session->userdata('parallel_no'),FALSE);
						$cnt = 0;
						if ($offerings) { //for course offerings with parallel
							foreach ($offerings AS $offering) {
								//get students enrolled in dissolved courses
								$course_offerings_id[] = $offering->course_offerings_id;
								$cnt++;
							}
						} else { //for course offerings with NO parallel
							//get students enrolled in dissolved courses
							$course_offerings_id[] = $this->session->userdata('course_offerings_id');
							$cnt++;
						}
					
						$offerings_for_adjustments = $course_offerings_id; //for auto adjust of dissolve courses
						$course_offerings_id = json_encode($course_offerings_id);
						$course_offerings_id = str_replace(array('"','[',']'),array("'","(",")"),$course_offerings_id);
						
						//$data['students'] = $this->Student_Model->AllEnrollees($course_offerings_id);
						$this->db->trans_rollback(); //print($this->session->userdata('empno')); die();
						
						$this->db->trans_start();
					
						//delete records in parallel_offerings table
						if (!$this->Offerings_Model->DeleteParallelOffering($offerings_for_adjustments,$cnt,$this->session->userdata('parallel_no'))) {
							$error_msg = "ERROR deleting parallel offering!";
						} else {
							//change status='D' on all course_offerings including parallel
							if (!$this->Offerings_Model->DissolveOffering($offerings_for_adjustments)) {
								$error_msg = "ERROR dissolving offering!";
							} else {
								$course_slots = $this->Offerings_Model->ListSlotsForOcccupancyRemoval($course_offerings_id);
								if ($course_slots) {
									foreach($course_slots AS $slot) {
										//delete records from room_occupancy
										if (!$this->Rooms_Model->ClearRoomOccupancy($slot->id)) {
											$error_msg = "ERROR clearing room block!";
											break 1;
										}
									}
								}
					
								//copy records from enrollments to enrollments_history
								if (!$this->Student_Model->TransferDissolvedToHistory($this->session->userdata('empno'), $course_offerings_id)) {
									$error_msg = "ERROR transferring dissolved data to history!";
								} else {
				
									$current_academic_term = $this->academic_terms_model->getCurrentAcademicTerm();
									$adj['term']   = $current_academic_term->id;
									$adj['date']   = date('Y-m-d H:i:s');
									$adj['type']   = "Credit";
									$adj['empno']  = $this->session->userdata('empno');
									$adj['remark'] = "";
									
									foreach ($offerings_for_adjustments AS $k=>$v) {
										$students = $this->enrollments_model->list_enrollees($v);
										//$this->db->trans_rollback(); print($students); die();
										if ($students) {
											foreach($students AS $stud) {
												$offered_course = $this->Offerings_Model->getOffering($v);
												$adj['description'] = "DISSOLVED: ".$offered_course->course_code.'-'.$offered_course->section_code;
				
												if ($this->assessment_model->CheckStudentHasAssessment($stud->student_histories_id)) {
													$tuition_rate_basic = $this->Student_model->getTuitionRateBasic($stud->acad_program_groups_id,
															$stud->academic_terms_id, $stud->year_level);
														
													//check if course is under tuition_others eg: REED, CWTS
													$tuition_other = $this->assessment_model->getTuitionFee_Others($stud->student_histories_id, $offered_course->courses_id);
													if ($tuition_other) {
														$adj['amount'] = $tuition_other->amt;
													} else {
														$adj['amount'] = $tuition_rate_basic->rate * $offered_course->paying_units;
													}
														
													$adj['academic_programs_id']= $stud->academic_programs_id ;
													$adj['year_level'] = $stud->year_level ;
													$adj['payers_id'] = $stud->id;
				
													//add records to adjustments
													if (!$this->ledger_model->add_adjustment($adj,'Y',$this->session->userdata('empno'),date('Y-m-d H:i:s'))) {
														$error_msg = "ERROR in writing to ledger!";
														break 2;
													}
				
													//if course is lab
													$lab = $this->assessment_model->getLab_Fee($stud->student_histories_id, $offered_course->courses_id);
				
													if ($lab) {
														$adj['description'] = "DISSOLVED (Lab. Fee): ".$offered_course->course_code.'-'.$offered_course->section_code;
														$adj['amount'] = $lab->rate;
														//add records to adjustments
														if (!$this->ledger_model->add_adjustment($adj,'Y',$this->session->userdata('empno'),date('Y-m-d H:i:s'))) {
															$error_msg = "ERROR in writing to ledger!";
															break 2;
														}
													}
												}
				
				
												//delete record from enrollments table
												if (!$this->Student_model->WithdrawCourse($stud->enrollments_id)) {
													$error_msg = "ERROR in deleting student enrollments!";
													break 2;
												}
				
												//update re_enrollments table
												if (!$this->enrollments_model->UpdateRe_enrollments($stud->enrollments_id)) {
													$error_msg = "ERROR in updating student re_enrollments record!";
													break 2;
												}
											}
										}
									}
										
									//delete records in block_course_offerings
									if (!$this->Offerings_Model->UnblockOfferings($offerings_for_adjustments)) {
										$error_msg = "ERROR in unblocking offerings!";
									} else {
										//delete records from allocations
										if (!$this->Offerings_Model->DeallocateOfferings($course_offerings_id)) {
											$error_msg = "ERROR in deallocating offerings!";
										} else {
											$this->db->trans_complete();
											$this->content_lib->set_message("Section successfully dissolved!", 'alert-success');
										} //end deallocating
									} //end unblocking
								} //end tranferring
							}
						}
				
						if ($this->db->trans_status() === FALSE){
							$this->db->trans_rollback();
							$this->content_lib->set_message($error_msg, 'alert-error');
						}
				
						$this->session->unset_userdata('courses_id');
						$this->session->unset_userdata('parallel_no');
				
						$acad_term_id = $this->input->post('academic_term');
					
					} else {
						$this->content_lib->set_message("Course offering already dissolved!", 'alert-error');
					
						$this->session->unset_userdata('course_offerings_id');
						$this->session->unset_userdata('courses_id');
						$this->session->unset_userdata('parallel_no');
					
						$term = $this->AcademicYears_Model->getCurrentAcademicTerm();
						$acad_term_id = $term->id;
					}
		
				$current_term = $this->academic_terms_model->getCurrentAcademicTerm();
				$offerings = $this->Offerings_Model->ListOfferings($current_term->id);
				//$offerings = $this->Offerings_Model->ListOfferings($acad_term_id);
	
				$data = array(
						"terms"=>$acad_terms,
						"selected_term"=>$current_term->id,
						"offerings"=>$offerings,
				);

				break;
							
		}
	
		//print_r($data['offerings']); die();
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->content_lib->enqueue_body_content('registrar/list_offerings',$data);
		$this->content_lib->content();
	}


	/*
	 * NOTE: this is transfered already to all_offerings()
	//Added: December 5, 2013 by Amie
	public function force_enrollment()
	{
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Offerings_Model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('academic_terms_model');
		
			
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
		
		//print($this->input->post('step')); die();
		switch ($step) {
			case 1:
				if ($this->common->nonce_is_valid($this->input->post('nonce'))){	
					$offering_id = $this->input->post('offering_id');
					$course_info = $this->Enrollments_model->getCourse_id($offering_id);
					$id = "('".$offering_id."')";
					$data['title']="Class List";
					$data['offerings'] = $this->Offerings_Model->getOffering($offering_id);
					$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
					$data['offering_slot'] = $this->Offerings_Model->ListOfferingSlots($offering_id);
					$data['enrollees'] = $this->Student_model->AllEnrollees($id);
					$data['new_offerings'] = $this->Enrollments_model->list_offerings($course_info->id, $data['offerings']->academic_terms_id,$offering_id);
					//print_r($data['new_offerings']); die();
					$this->content_lib->enqueue_body_content('registrar/registrar_class_list',$data);
					$this->content_lib->content();
				 } else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
				}
				
				break;
				
			case 2:
				if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					$enrollees = $this->input->post('newlist');
					//print_r($this->input->post('newlist')); die();
					$prev_offering_id = $this->input->post('offering_id');
					$new_offering_id = $this->input->post('section');
					$empno = $this->session->userdata('empno');
					//print($empno); die();
					//print_r($prev_offering_id."<br>".$new_offering_id); die();
					if ($enrollees) {
						foreach($enrollees as $index => $enrollee){
							$result = $this->Enrollments_model->updateCourseOffering($index,$prev_offering_id,$new_offering_id, $empno);
						}
						if($result){
							$this->content_lib->set_message("Student/s  Successfully Transferred!", "alert-success");
						}
					}else {
							$this->content_lib->set_message("Error in transferring student/s", "alert-error");
						}
					
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
				}	
				$this->content_lib->content();
				break;
								
		}
	}
	*/
	
	public function academic_terms(){

		$this->content_lib->set_title ('Academic Terms | Registrar | ' . $this->config->item('application_title'));
		$this->load->model('registrar_model');
		
		$action = $this->input->post('action');
		$message = "";
		
		if ($action) {
			if  ($this->common->nonce_is_valid($this->input->post('nonce'))) {
				//nonce is valid
				switch ($action){
					case 'edit_academic_term'	:
										$data = array();
										$academic_term_start_date = $this->input->post('start_date');
										$academic_term_id = $this->input->post('id');
										//academic terms...
										if ( ! empty($academic_term_start_date))
											$data['start_date'] = $academic_term_start_date;
										
										if ($academic_term_status = $this->input->post('academic_status'))
											$data['status'] = $academic_term_status;
										
										//process academic terms...
										if ( ! $this->registrar_model->update_academic_term($academic_term_id, $data)){
											//An error occurred...
											$message = array('message'=>"Academic term information NOT updated", 'severity'=>'error');
										} else {
											//process enrollment dates
											$enrollment_start_dates = $this->input->post('enroll_start_date');
											$enrollment_end_dates = $this->input->post('enroll_end_date');
											$data = array();
											foreach ($enrollment_start_dates as $key=>$val){
												$data[] = array(
														'id'=>$key,
														'start_date' => $val,
														'end_date'=>$enrollment_end_dates[$key],
														);
											}
											
											if ( ! $this->registrar_model->update_enrollment_schedules ($data)){
												//enrollment dates processing error occurred...
												$message = array('message'=>"Enrollment schedules NOT updated", 'severity'=>'error');
											} else {
												//no error occurred we're going to continue processing
												//process periods
												$data = array();
												$period_start_dates = $this->input->post('period_start_date');
												$period_end_dates = $this->input->post('period_end_date');
												foreach ($period_start_dates as $key=>$val){
													$data[] = array(
															'id'=>$key,
															'start_date'=>$val,
															'end_date'=>$period_end_dates[$key],
															);
												}
												
												if ( ! $this->registrar_model->update_periods($data))
													$message = array('message'=>"Enrollment Periods NOT updated", 'severity'=>'error'); else
													$message = array('message'=>"Academic terms, periods and enrollment schedules updated", 'severity'=>'success');
											}
										}
										
										break;
					case 'show_academic_term'	:
										$academic_term = $this->input->post('id'); 
										break;
					case 'add_academic_year'	:
										if ($this->registrar_model->add_academic_year())
											$message = array('message'=>"Academic year added to system", 'severity'=>'success'); else 
											$message = array('message'=>"Academic year NOT added to system", 'severity'=>'error');
										break;
					
										
					default:
							
							break;
				}
			} else {
				//Nonce is invalid
				$message = array('message' =>'Nonce is invalid', 'severity'=>'error');
			}
		}
		
		$data = $this->registrar_model->current_and_upcoming_terms();
		$this->content_lib->enqueue_footer_script('stepy');
		$this->content_lib->enqueue_header_style('stepy');
		$this->content_lib->enqueue_footer_script('date_picker');
		$this->content_lib->enqueue_header_style('date_picker');
		$this->content_lib->enqueue_body_content('registrar/academic_terms', array('academic_terms'=>$data, 'message'=>$message));
		$this->content_lib->enqueue_sidebar_widget('registrar/academic_terms_widget', array('academic_terms'=>$data), 'Upcoming Academic Terms', 'in');
		$this->content_lib->content();	
	}
	
	
	public function student(){
		
		
		$this->content_lib->set_title('Registrar | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		
		if ( ! empty($query)){
			
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
			
			$this->load->library('pagination_lib');
			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			}
		}
		
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('financials/Tuition_Model');
			
			$result = $this->student_common_model->my_information($idnum);	
			
			$action = $this->input->post('action');
			
			if ($action = $this->input->post('action')) {
			
		
				
					switch ($this->input->post('action')){
					
						case 'upload_tor'	:
							if ($this->common->nonce_is_valid($this->input->post('nonce'))){
									$this->load->library('upload', $this->config->item('tor_upload_config'));
									$this->upload->do_upload('tor');
									
									if ($errors = $this->upload->display_errors()){
										$this->content_lib->set_message('Error seen while uploading TOR: ' . $errors, 'alert-error');
									} else {
										$data = $this->upload->data();
									// include code to get the last page number of the uploaded TOR
									// if result is not NULL, increment the page number else page number is 1
									// include code to insert TOR records to tor_path table 
										$this->content_lib->set_message('TOR Uploaded as: ' . print_r($data, TRUE) , 'alert-success');
									}
							  } else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
								}
									break;
									
					/*		case 'open_edit_grade_form'	:
								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
										$enrollment_id = $this->input->post('enrollment_id');
										$period = $this->input->post('period');
										$prev_grade = $this->input->post('prev_grade');
										$this->edit_grade($enrollment_id, $period, $prev_grade);
										
										
									if($period == 'P'){
											$data['period'] = "Prelim";
										}elseif($period == 'M'){
											$data['period'] = "Midterm";
										}else{
											$data['period'] = "Finals";
										}
										$data['prev_grade'] = $this->input->post('prev_grade');
										$data['term'] = $this->Academic_terms_model->getCurrentAcademicTerm();
										$data['enrollment_info'] = $this->Enrollments_model->GetEnrollmentInfo($data['enrollment_id']);
										$this->content_lib->enqueue_body_content('student/form_to_edit_grade', $data);
										$this->content_lib->content();	
								} else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
								}	
								break;*/
								
								return;
												
						default:
										break; 
						
					}
				
				
			}
			//print($this->input->post('action'));
			//die();				
			$result = $this->student_common_model->my_information($idnum);
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$grade_level = array(11,12);

				if (in_array($result->year_level,$grade_level)) { //student is SHS
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname)  . ", "  . $result->fname . " ". $result->mname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> "Grade ".$result->year_level." ".$result->section_name,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address'	=>$result->full_city_address,
							'phone_number'	=>$result->phone_number,
							'section'	=>$result->section,
					);
				} else {
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> $result->fname . " " . $result->mname ." ".  $result->lname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> $result->year_level,
							'full_home_address'	=> $result->full_home_address,
							'full_city_address'	=> $result->full_city_address,
							'phone_number'		=> $result->phone_number,
							'section'			=> $result->section,
					);
				}
				
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$donctent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}					
												
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...
				$data['year_level'] = $result->year_level;
				$data['academic_program'] =$result->abbreviation;
				$data['idnum'] = $idnum;
				$data['academic_program_id'] = $result->ap_id;
				
				$this->load->model('hnumis/Student_model');
				$this->load->model('hnumis/Courses_Model');
				$this->load->model('hnumis/Enrollments_model');
				$this->load->model('hnumis/AcademicYears_Model');
				$this->load->model('hnumis/Offerings_Model');
				$this->load->library('tab_lib');
				$this->load->model('hnumis/shs/shs_student_model');
				
				$current_term = $this->Academic_terms_model->current_academic_term();
				$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_term->id);
				$grades = $this->Enrollments_model->student_grades($idnum);
				$withgrades = $this->Enrollments_model->student_withgrades($idnum);
				//print_r($withgrades); die();					
				
				//added January 4, 2013 (psychometrician exams results)
				$this->load->model('psychometrician_model');
				$student_exams = $this->psychometrician_model->student_exams($idnum);
				
				//This looks dirty... but this will work... make schedule be set using a pull down menu
				//added by: justing 3/15/2012
				if (is_college_student($idnum)) {
					$academic_terms = $this->Academic_terms_model->student_inclusive_academic_terms($idnum); //terms this student is enrolled...
					$this->load->model('hnumis/Courses_Model');

					if ($action=='schedule_current_term') {
						$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $this->input->post('academic_term')); 
						$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $this->input->post('academic_term'));
						$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $this->input->post('academic_term'));
						$tab = 'schedule';
						$selected_term = $this->input->post('academic_term');
						$terms=$this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_term')); 
						$acad_term=$terms->term;
					} else {
						if ($academic_terms) {
							$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $academic_terms[0]->id); 							
						} else {
							$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $current_term->id); 							
						}
						
						$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $academic_terms[0]->id);
						$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $current_term->id);
						$tab = 'student_info';
						//$selected_term = '';
						$selected_term = $academic_terms[0]->id;
						$acad_term = $current_term->term;
					}
				
					$this->tab_lib->enqueue_tab ('Student Information', 'common/student_information_full', $result, 'student_info', ($tab!='schedule'));
					
					$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $selected_term);
					if ($selected_history) {
						$student_units = $this->Student_model->getUnits($selected_history, $acad_term);
						$student_history_id=$selected_history->id;
					} else {
						$student_units['max_bracket_units']=0;
						$student_history_id=0;
					}
				
				
					$acad_term = $this->AcademicYears_Model->getAcademicTerms($selected_term);
				
				}

				$grade_level = array(11,12);

				if (is_college_student($idnum)) {
					if (in_array($result->year_level,$grade_level)) { //student is SHS

						$this->load->model('hnumis/shs/shs_grades_model');
						
						$grades          = $this->shs_grades_model->List_Student_Grades($idnum); 
						$class_schedules = $this->shs_student_model->ListClassSchedules($current_history->id); 					
						
						$this->tab_lib->enqueue_tab ('Grades', 'shs/student/student_grades_for_change', 
								array('grades'=>$grades,
										'can_edit'=>TRUE,
										'idnum'=>$idnum,
								), 'grades', FALSE);					

						$this->tab_lib->enqueue_tab ('Schedule', 'shs/student/student_class_schedules', 
								array('academic_terms'=>$academic_terms,
									'selected_term'=>$selected_term,
									'class_schedules'=>$class_schedules,
									'show_pulldown'=>TRUE,
									), 
								'schedule', $tab=='schedule');
						
					} else {
						$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', array('schedules'=>$courses, 
													'academic_terms'=>$academic_terms,
													'assessment_date'=>$assessment_date,
													'academic_terms_id'=>$selected_term,
													'max_units'=>$student_units['max_bracket_units'],					
													'student_histories_id'=>$student_history_id),
													 'schedule', ($tab=='schedule'));
					}
				}
				
				$tab_content = $this->tab_lib->content();
				$this->content_lib->enqueue_body_content("", $tab_content);
				
				
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}
		
		//what will happen if there is no query... and the result is not numeric?
		if ( empty($query) && ! is_numeric($idnum)) {
				
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$data = array();
			$this->load->model('hnumis/College_model');
			$data['colleges'] = $this->College_model->all_colleges();
			$this->load->model('student_common_model');
				
			if ($this->input->post('what')=='search_by_program'){
				$total = count ($this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), TRUE));
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				$results = $this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), FALSE, $page, $this->config->item('results_to_show_per_page'));
				$this->load->library('pagination_lib');
				$data['pagination'] = $this->pagination_lib->pagination('', $total, $page);
				$data['results'] = $results;
				$this->load->model('hnumis/Programs_model');
				$this->load->model('hnumis/College_model');
				$data['query'] = $this->Programs_model->getProgram($this->input->post('program'))->abbreviation . " " . $this->input->post('year_level');
				$data['programs'] = $this->College_model->programs_from_college($this->input->post('college'));
				$data['college'] = $this->input->post('college');
				$data['program'] = $this->input->post('program');
				$data['year_level'] = $this->input->post('year_level');
				$data['end'] = $end;
				$data['total'] = $total;
				$data['start'] = $start;
			}
			$this->content_lib->enqueue_body_content('common/search_result_by_program', $data);
		}
		$this->content_lib->content();	
	}

	

	public function faculty(){
		$this->load->library('faculty_lib');
		$this->faculty_lib->faculty(TRUE);
	}
	
	
	
	public function download_student_list_csv (){
		$year_level = $this->input->post('year_level');
		$program = $this->input->post('program');
	
		$this->load->model('student_common_model');
		$records = $this->student_common_model->search_by_program ($program, $year_level, TRUE);
		$filename = $records[0]->abbr . "_" . $year_level . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
		fputcsv($output, array('No.', 'Name', 'Gender', 'Course and Year Level'));
		$count = 0;
		foreach ($records as $record){
			$count++;
			fputcsv ($output, array($count, $record->fullname, $record->gender, $record->abbr . " ". $record->year_level));
		}
	}
	
	public function programs (){

		//process action...
		$college_id = 0;
		if ($action = $this->input->post('action')){
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				$this->load->model('hnumis/Programs_model');
				$college_id = $this->input->post('college');
				switch ($action){
					case 'unoffer_active'	:
						if ($this->Programs_model->update_programs($this->input->post('program_ids'), 'U')){
							$this->content_lib->set_message ('Program(s) are successfully unoffered for First Year students' , 'alert-success');
						} else {
							$this->content_lib->set_message ('Error found while Program(s) were unoffered for First Year students', 'alert-error');
						}
						
						break;
					case 'deactivate_active' :
						if ($ret = $this->Programs_model->update_programs($this->input->post('program_ids'), 'F')){
							$this->content_lib->set_message ('Program(s) are successfully Frozen', 'alert-success');
						} else {
							$this->content_lib->set_message ('Error found while freezing Program(s).', 'alert-error');
						}
						break;
					case 'activate_inactive' :
						if ($this->Programs_model->update_programs($this->input->post('program_ids'), 'O')){
							$this->content_lib->set_message ('Program(s) are successfully offered.' , 'alert-success');
						} else {
							$this->content_lib->set_message ('Error found while Program(s) were offered.', 'alert-error');
						}
						
						break;
					case 'activate_unoffered' :
						if ($this->Programs_model->update_programs($this->input->post('program_ids'), 'O')){
							$this->content_lib->set_message ('Program(s) not offered to First Year students are successfully offered.' , 'alert-success');
						} else {
							$this->content_lib->set_message ('Error found while Program(s) were offered.', 'alert-error');
						}
						
						break;
					
					case 'deactivate_unoffered' :
						if ($this->Programs_model->update_programs($this->input->post('program_ids'), 'F')){
							$this->content_lib->set_message ('Program(s) not offered to First Year Students are successfully Frozen.' , 'alert-success');
						} else {
							$this->content_lib->set_message ('Error found while Program(s) were Frozen.', 'alert-error');
						}
					
						break;
					default:
						
						break;
				}
				
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
			}
		}
		$this->load->model('hnumis/College_model');
		$colleges = $this->College_model->all_colleges();
		$this->load->library('tab_lib');
		$this->tab_lib->enqueue_tab('Offered Programs', 'registrar/active_programs', array('colleges'=>$colleges, 'college_id'=>$college_id), 'active_programs', TRUE);
		$this->tab_lib->enqueue_tab('Frozen Programs', 'registrar/inactive_programs', array(), 'inactive_programs', FALSE);
		$this->tab_lib->enqueue_tab('Programs not Offered for First Years', 'registrar/unoffered_programs', array(), 'unoffered_programs', FALSE);
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->content();
	}
	
	public function prospectus(){
		$program_id = $this->uri->segment(3);
		$prospectus_id = $this->uri->segment(4);
		
		$this->load->model('hnumis/Programs_model');
		$this->load->model('hnumis/Prospectus_model');
		
		$data['academic_program'] = $this->Programs_model->academic_program_info($program_id);
		$data['prospectuses'] = $this->Prospectus_model->prospectuses_from_academic_programs ($program_id);
		
		if ($prospectus_id) {
			$data['prospectus'] = $this->Prospectus_model->student_prospectus($prospectus_id);
		} else {
			if (isset($data['prospectuses'][0]))
				$data['prospectus'] = $this->Prospectus_model->student_prospectus($data['prospectuses'][0]->id);
		}
		
		$this->content_lib->enqueue_body_content('registrar/prospectus', $data);
		$this->content_lib->content();
	}
	
	public function colleges(){
		//$this->content_lib->enqueue_body_content('', );
		$this->content_lib->content();
	}
	
	public function enrollment_summary() {
	
		$this->load->model('hnumis/Reports_Model');
		
		$this->Reports_Model->enrollment_summary();

	}
	
	public function edit_grade() {
	$this->load->model('Academic_terms_model');
	//print_r($this->input->post());die();
	if ($this->common->nonce_is_valid($this->input->post('nonce'))){
			$data['enrollment_id'] = $this->input->post('enrollment_id');
			$period = $this->input->post('period');
			$data['prev_grade'] = $this->input->post('prev_grade');
			$data['update_history'] = $this->input->post('update_history');
				
			if($period == 'P'){
					$data['period'] = "Prelim";
			}elseif($period == 'M'){
					$data['period'] = "Midterm";
			}else{
					$data['period'] = "Finals";
			}
			//$data['prev_grade'] = $this->input->post('prev_grade');
			$data['term'] = $this->Academic_terms_model->getCurrentAcademicTerm();
			$data['enrollment_info'] = $this->Enrollments_model->GetEnrollmentInfo($data['enrollment_id']);
			$this->content_lib->enqueue_body_content('student/form_to_edit_grade', $data);
			$this->content_lib->content();	
	} else {
		$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
	}	
}


		/**
		 * Description:
		 * This method generates a pdf file of the list of students in a particular class handled by the faculty member.
		 */
		function generate_class_schedule_pdf($academic_term_id, $max_units) {
				
			$this->load->model("hnumis/Reports_model");
			$this->load->model("hnumis/AcademicYears_Model");
			$student = $this->userinfo;
			$student['lname'] = $this->input->post('lname');
			$student['fname'] = $this->input->post('fname');
			$student['mname'] = $this->input->post('mname');
			$student['idno'] = $this->input->post('idno');
			$student['yr_level'] = $this->input->post('yr_level');
			$student['abbreviation'] = $this->input->post('abbreviation');
			$student['max_bracket_units']= $this->input->post('max_units');
			$student['student_histories_id']= $this->input->post('student_histories_id');
				
			//print_r($this->userinfo);
			//die();
			$this->userinfo['max_bracket_units'] = $max_units;
			$current = $this->AcademicYears_Model->getCurrentAcademicTerm();
			$this->Reports_model->generate_student_class_schedule_pdf($academic_term_id, $student, $current);
		
		}
		
		
		//Added: 3/11/2014 by Isah
		
		public function courses() {
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
		
			switch ($step) {
				case 1:
					$this->list_courses();
					break;
						
				case 4: //form to update a course
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$course_id = $this->input->post('course_id');
						$this->session->set_userdata('course_id',$course_id);
			
						$data['course'] = $this->Courses_Model->getCourseDetails($course_id);
							
						$data['type'] = $this->Courses_Model->getCourseType($course_id);
						$data['group'] = $this->Courses_Model->getCourseGroup($course_id);
						$data['types'] = $this->Courses_Model->ListCourseTypes();
						$data['groups'] = $this->Courses_Model->ListCourseGroups();
						$data['course_id'] = $course_id;
			
						$this->load->library('form_lib');
			
						$this->content_lib->enqueue_body_content('dean/form_to_edit_course',$data);
						$this->content_lib->content();
					} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							$this->index();
					}
					break;
						
				case 5: // update a course
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){	
						$data['course_id'] = $this->session->userdata('course_id');
						$data['course_code'] = $this->input->post('course_code');
						$data['group_id'] = $this->input->post('group_id');
						$data['type_id'] = $this->input->post('type_id');
						$data['descriptive_title'] =  sanitize_text_field($this->input->post('descriptive_title')); 
						$data['credit_units'] = sanitize_text_field($this->input->post('credit_units'));
						$data['paying_units'] = sanitize_text_field($this->input->post('paying_units'));
						//print_r($data); die();
						$this->Courses_Model->updateCourse($data);
						$this->content_lib->set_message("Course successfully updated!", 'alert-success');
						//$this->content_lib->enqueue_body_content('dean/list_courses'); replaced with next line
						$this->list_courses();
						//$this->content_lib->content();
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}	
					break;
		
				default:
					$this->list_courses();
					break;
			}
			
		
		}
		
		private function list_courses(){
				
			$this->load->library('table_lib');
				
			$data['title'] = "Courses";
			$data['courses'] = $this->Courses_Model->ListCoursesAll();
			//$data['terms_sy'] = $this->AcademicYears_Model->getCurrentAcademicTerm();
		
			//$this->content_lib->enqueue_body_content('dean/header_title',$data);
			$this->content_lib->enqueue_body_content('registrar/list_allcourses',$data);
			$this->content_lib->enqueue_footer_script('data_tables');
			$this->content_lib->content();
		}
		
		public function accreditation() {
			$this->content_lib->enqueue_footer_script('chained');
			
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');
			
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
				switch ($step) {
					case 1:
						if ($this->common->nonce_is_valid($this->input->post('nonce'))){
							$data['colleges'] = $this->College_model->ListColleges();
							$data['programs'] = $this->Programs_model->ListAcaPrograms();
							$data['accreditors'] = $this->Programs_model->listAccreditors();
							//print_r($data['accreditors']); die();
							$this->content_lib->enqueue_body_content('registrar/form_add_accred_level',$data);
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							$this->index();
						}	
						break;
			
					case 2: //form to add accreditation level
						if ($this->common->nonce_is_valid($this->input->post('nonce'))){
							$data['program_id'] = $this->input->post('program_id');
							$data['accreditor_id'] = $this->input->post('accred_id');
							$data['level'] = sanitize_text_field($this->input->post('level'));
							$data['date_approved'] = $this->input->post('date_approved');
							$data['start_date'] = $this->input->post('start_date');
							$data['end_date'] = $this->input->post('end_date');
							
							$add_status = $this->Programs_model->addAccredLevel($data);
							
							if($add_status){
								$this->content_lib->set_message("Accreditation Level Successfully Added!", "alert-success");
							} else {
								$this->content_lib->set_message("Error found while adding a accreditation level!", "alert-error");
							}
							$this->content_lib->content();
						} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							$this->index();
						}
						break;
			
					default:
					
						break;
				}
				
		
		}

		
		public function display_accredited_programs() {
			
			$this->load->model("hnumis/Programs_Model");
						
			$data['accredited'] = $this->Programs_Model->ListAccreditedPrograms();

			$this->content_lib->enqueue_body_content('registrar/display_accredited_programs',$data);
			$this->content_lib->content();
				
		}

		
		public function set_grad_date() {
			//$this->content_lib->enqueue_footer_script('chained');
				
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');
				
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
		
			switch ($step) {
				case 1:
					$this->get_acadterm();
					
					break;
		
				case 2: //form to add graduation dates
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$data['acad_term_id'] = $this->input->post('acad_term_id');
						
						$data['grad_date'] = $this->input->post('grad_date');
						$programs = $this->input->post('programs');
						//print_r($this->input->post('programs')); die();
						foreach($programs AS $k => $v) {
							$add_status = $this->Academic_terms_model->addGradDate($data, $v);
							if(!$add_status){
								$this->db->trans_rollback();
								$add_status = 0;
							}
						}
						
						if($add_status > 0){
							$this->content_lib->set_message("Graduation date Successfully Added!", "alert-success");
							$this->get_acadterm();
						} else {
							$this->content_lib->set_message("Error found while adding Graduation date!", "alert-error");
							$this->index();
						}
					//$this->content_lib->content();
					
						break;
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
				default:
			}
	
		}
		
		//Added: 3/14/2014
		
		public function list_grad_date() {
			//$this->content_lib->enqueue_footer_script('chained');
		
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');
		
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$data['academic_terms'] = $this->AcademicYears_model->ListAcademicTerms(FALSE);
						
						$this->content_lib->enqueue_body_content('registrar/form_input_aca_term',$data);
						$this->content_lib->content();
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
					
					break;
		
				case 2: //list graduation schedule
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$acad_term_id = $this->input->post('acad_term_id');
						$data['grad_schedule'] = $this->Academic_terms_model->listGradDates($acad_term_id);
						$data['academic_term'] = $this->AcademicYears_model->getAcademicTerms($acad_term_id);
						
						$this->content_lib->enqueue_body_content('registrar/list_grad_dates',$data);
						$this->content_lib->content();
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
					break;
		
				case 3: //delete graduation schedule
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$grad_sked_id = $this->input->post('sked_id');
						$delete_status  = $this->Academic_terms_model->deleteGradDate($grad_sked_id);
						
						$data['grad_schedule'] = $this->Academic_terms_model->listGradDates($this->input->post('academic_term_id'));
						$data['academic_term'] = $this->AcademicYears_model->getAcademicTerms($this->input->post('academic_term_id'));
						
						if($delete_status > 0){
							$this->content_lib->set_message("Graduation date Successfully Deleted!", "alert-success");
						} else {
							$this->content_lib->set_message("Error found while deleting Graduation date!", "alert-error");
						}
						$this->content_lib->enqueue_body_content('registrar/list_grad_dates',$data);
						$this->content_lib->content();
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}	
						break;
				default:
			}
	}
		
	private function get_acadterm() {
		//$this->content_lib->enqueue_footer_script('chained');
	
		$this->content_lib->enqueue_footer_script('date_picker');
		$this->content_lib->enqueue_header_style('date_picker');
	
		$data['academic_terms'] = $this->AcademicYears_model->ListAcademicTerms(FALSE);
		$data['colleges'] = $this->College_model->ListColleges();
		$this->content_lib->enqueue_body_content('registrar/form_add_grad_date',$data);
		$this->content_lib->content();
	}
		

	//ADDED: 5/31/14 genes
	function download_enrollment_summary_to_csv() {
		$this->load->model('hnumis/Reports_Model');
	
		$this->Reports_Model->download_enrollment_summary_to_csv();
	
	}
	

	/*
	 * ADDED: 6/10/14
	* @author: genes
	* @description: displays students enrolled from enrollment summary modal
	*/
	function get_students() {
		$this->load->model('hnumis/Enrollments_Model');
	
		$data['students'] = $this->Enrollments_Model->masterlist($this->input->get('selected_term'), $this->input->get('prog_id'), $this->input->get('yr_level'), TRUE);
	
		$this->load->view('dean/list_of_students_from_modal', $data);
	
	}
	
	
	public function list_withdrawals() {
		$this->load->library('../controllers/accountant/');
		$this->accountant->Withdrawals();
	}
	

	public function list_new_enrollees() {
		$this->load->library('registrar_lib');
	
		$this->registrar_lib->list_new_enrollees();
	
	}

	/*
	 * @ADDED: 8/28/14 by: genes
	 * 
	 */
	private function CollateData() {
		$this->load->model('hnumis/Student_Model');
		
		//data for dissolve section
		$data['offerings'] = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
		$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($data['offerings']->academic_terms_id);
		$offerings = $this->Offerings_Model->ListParallel_Offerings('',$this->session->userdata('parallel_no'),FALSE);
		$cnt = 0;
		if ($offerings) {
			foreach ($offerings AS $offering) {
				//get students enrolled in dissolved courses
				$course_offerings_id[] = $offering->course_offerings_id;
				$cnt++;
			}
		} else {
			//get students enrolled in dissolved courses
			$course_offerings_id[] = $this->session->userdata('course_offerings_id');
			$cnt++;
		}
			
		$course_offerings_id = json_encode($course_offerings_id);
		$course_offerings_id = str_replace(array('"','[',']'),array("'","(",")"),$course_offerings_id);
		$data['students'] = $this->Student_Model->AllEnrollees($course_offerings_id);
	
		return $data;
	
	}


	public function process_student_action($idnum) {
		
		$this->load->library('shs/shs_student_lib');
	
		$this->shs_student_lib->process_student_action($idnum);
		
	}

	
}