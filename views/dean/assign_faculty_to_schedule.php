<h2 class="heading">Assign Faculty</h2>
<?php 
$this->form_lib->set_id('assign-faculty');
$this->form_lib->set_attributes(array('method'=>'post','class'=>'form-horizontal'));
$this->form_lib->add_control_class('formSep');
$this->form_lib->enqueue_hidden_input('nonce',$this->common->nonce());
$this->form_lib->enqueue_hidden_input('step','4');
$this->form_lib->enqueue_text_data('Academic Terms',$academic_terms->term." ".$academic_terms->sy);
$this->form_lib->enqueue_text_data('Course [Section]',$offerings->course_code."[".$offerings->section_code."]");
$this->form_lib->enqueue_text_data('Schedule',$ids = implode(', ',array_map(function($item) { return $item->tym." ".$item->days_day_code." [".$item->room_no."]"; }, $offering_slot[0])));
$this->form_lib->enqueue_select_input2('employee_empno','Faculty','{empno}','{neym}',$faculty,'','','span3');
$this->form_lib->set_submit_button('Assign');
$this->form_lib->content(FALSE);
?>