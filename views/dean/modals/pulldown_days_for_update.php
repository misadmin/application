
		<select id="day_code_update" name="day_code_update" style="width:auto;">
	  			<?php
					foreach($days AS $day) {
				?>
						<option value="<?php print($day->day_code); ?>" <?php if ($selected_day == $day->day_code) print("selected"); ?> >
							<?php print($day->day_code); ?>
						</option>
				<?php 
					}
				?>								
		</select>

<script>
	$(document).ready(function(){
		
		$('#day_code_update').bind('change', function(){

			var element    		  = $("option:selected", "#start_time_update");
			var start_time		  = element.val();	
			var element  		  = $("option:selected", "#num_hr_update");
			var num_hr    		  = element.val();
			var element    		  = $("option:selected", "#day_code_update");
			var day_code   		  = element.val();
			var academic_terms_id = $('#academic_terms_id_update').val();
			var old_bldgs_id      = $('#old_bldgs_id_update').val();
			var old_rooms_id      = $('#old_rooms_id_update').val();
			var course_offerings_slots_id = $('#course_offerings_slots_id_update').val();

			$('#rooms_offering_update').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:25px;' />"); 

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/course_schedules'); ?>",
				data: {	"academic_terms_id": academic_terms_id,
						"rooms_id": old_rooms_id,
						"start_time": start_time,
						"num_hr": num_hr,
						"day_code": day_code,
						"course_offerings_slots_id": course_offerings_slots_id,
						"old_bldgs_id": old_bldgs_id,
						"action": "extract_vacant_rooms_for_update" },
				dataType: 'json',
				success: function(response) {													
																			
					$('#rooms_offering_update').html(response.rooms); 
					
				}
			});

		});
		
	});
</script> 		

