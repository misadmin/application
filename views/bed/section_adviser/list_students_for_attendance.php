<style>
	td.attend_header {
		background-color:#C3C3C3;
		font-size:bold;
		text-align:center;
		font-size:12px;
		vertical-align:middle;
	}

</style>		
	<div id="show_list_of_students">		
		<div style="width:100%; overflow:auto; " >
			<form id="submit_attendance_form" method="POST" action="<?php print(site_url($this->uri->segment(1).'/my_section'));?>" >
				<input type="hidden" name="action" value="submit_attendance" />					
				<?php $this->common->hidden_input_nonce(FALSE); ?>

					<table class="table table-hover table-bordered table-striped table-condensed" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" >
								<td class="attend_header" rowspan="2" style="width:8%;">Count</td>
								<td class="attend_header" rowspan="2" style="width:12%;">ID No.</td>
								<td class="attend_header" rowspan="2" style="width:41%;">Name</td>
								<td class="attend_header" rowspan="2" style="width:15%;">No. of<br>School<br>Days</td>
								<td class="attend_header" colspan="2" style="width:24%; text-align:center;">
									<div>
										<div id="show_change_data_icon" style="float:left; padding-top:13px;">
										</div>
										<div style="padding-top:8px;">
											<select style="width:auto;" class="form-control change_month" id="school_days_id" name="school_days_id" >
											<?php 
												if ($months) {
													foreach($months AS $m) {
											?>
													<option value="<?php print($m->id); ?>" 
														<?php if ($school_days_id == $m->id) print("selected"); ?>
														academic_terms_id="<?php print($academic_terms_id); ?>" 
														section_id="<?php print($section_id); ?>" ><?php print($m->school_month); ?></option>
											<?php 
													}
												}
											?>
											</select>
										</div>
									</div>
								</td>
							</tr>
							<tr style="text-align:center; font-size:11px; font-weight:bold;">
								<td class="attend_header" style="width:12%;">Days<br>Present</td>
								<td class="attend_header" style="width:12%;">Days<br>Tardy</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($students) {
									$cnt=1;
									foreach($students  AS $student) {
							?>			
										<tr>
											<td style="text-align:right;"><?php print($cnt); ?>.</td>
											<td style="text-align:center;"><?php print($student->idno); ?></td>
											<td><?php print($student->name); ?></td>
											<td style="text-align:center;"><?php print($student->num_days); ?></td>
											<td style="text-align:center;">
												<input type="text" name="days_present[<?php print($student->student_histories_id); ?>]"
													style="width:45px; height:17px; font-size:14px; text-align:center; font-weight:bold;" 
													class="form-control d_present"
													id="days_present[<?php print($student->student_histories_id); ?>]"
													student_histories_id="<?php print($student->student_histories_id); ?>"
													num_days="<?php print($student->num_days); ?>"
													value="<?php print($student->days_present); ?>" />
											</td>
											<td style="text-align:center;">
												<input type="text" name="days_tardy[<?php print($student->student_histories_id); ?>]"
													style="width:45px; height:17px; font-size:14px; text-align:center; font-weight:bold;" 
													class="form-control d_tardy"
													id="days_tardy[<?php print($student->student_histories_id); ?>]"
													student_histories_id="<?php print($student->student_histories_id); ?>"
													num_days="<?php print($student->num_days); ?>"
													value="<?php print($student->days_tardy); ?>" />
											</td>
										</tr>
							<?php
										$cnt++;
									}
								}
							?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" class="attend_header"></td>
								<td colspan="2" class="attend_header" style="text-align:center;">
									<button class="btn btn-primary submit_attendance" >Submit<br>Attendance!</button>
								</td>
							</tr>
						</tfoot>
					</table>
			</form>
		</div>

	</div>	
		
		
<!-- this script prevents user to press Enter -->
<script>
	$('form input').on('keypress', function(e) {
		return e.which !== 13;
	});
</script>


<script>
	//$(document).ready(function(){
		$('.change_month').bind('change', function(){
			
			var element1          = $("option:selected", "#school_days_id");
			var school_days_id    = element1.val();
			var academic_terms_id = element1.attr('academic_terms_id');
			var section_id        = element1.attr('section_id');
			
			$('#show_change_data_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:18px;' />");

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/my_section'); ?>",
				data: {"school_days_id": school_days_id,
						"academic_terms_id": academic_terms_id,
						"section_id": section_id,
						"action": "change_extract_students_for_attendance" },
				dataType: 'json',
				success: function(response) {													

					$('#show_list_of_students').html(response.output); 
					
					$('#show_change_data_icon').html('');
					
				}
			});

		});
	//});
</script> 
			
			
<script>
	$(".d_present").blur(function(){

		var student_histories_id = $(this).attr('student_histories_id');
		var num_days             = $(this).attr('num_days');
		
		var days_present = $('#days_present\\['+student_histories_id+'\\]').val();

		if (parseFloat(days_present) > parseFloat(num_days)) {
			alert("ERROR: Days present greater than No. of School Days!");
			$('#days_present\\['+student_histories_id+'\\]').val('');
			$('#days_present\\['+student_histories_id+'\\]').focus();
			return false;
		} 
			
		
	});

</script>


<script>
	$(".d_tardy").blur(function(){

		var student_histories_id = $(this).attr('student_histories_id');
		var num_days             = $(this).attr('num_days');
		
		var days_tardy = $('#days_tardy\\['+student_histories_id+'\\]').val();

		if (parseFloat(days_tardy) > parseFloat(num_days)) {
			alert("ERROR: Days tardy greater than No. of School Days!");
			$('#days_tardy\\['+student_histories_id+'\\]').val('');
			$('#days_tardy\\['+student_histories_id+'\\]').focus();
			return false;
		} 
			
		
	});

</script>


<script>
	$('.submit_attendance').click(function(e){
		e.preventDefault();	

		var student_histories_id = $(this).attr('student_histories_id');
		var num_days             = $(this).attr('num_days');

		var confirmed = confirm('Continue to submit your attendance?');

		if (confirmed){
						
			$('#submit_attendance_form').submit();
			
		}
		
	});	
	
</script>
