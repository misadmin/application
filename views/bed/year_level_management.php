
<div id="show_strand_management_page" >
	<h2 class="heading">Year Level Management</h2>
	<div id="rounded_header" style="width:99%; font-size:12px; margin:0 auto; margin-top:15px; padding:0px;">
			<div style="float:right; overflow:auto;">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<div style="float:left; padding-top:6px; " >
					<a href="#" class="btn btn-success add_strand" style="font-size:12px; height:17px; margin-right:8px; margin-bottom:2px;" >
						<i class="icon-plus-sign"></i> New Year Level
					</a>
				</div>
					<div style="float:left; padding-top:5px;" class="b2">
						<select name="track_id" class="form-control change_track" style="width:auto; height:auto;" id="level_id">
							<option value="All" <?php if ($level_id == 0) print("selected"); ?> pos_name="All Levels">All</option>
							<?php 
								if ($levels) {
									foreach ($levels AS $level) {
							?>		
										<option value="<?php print($level->id); ?>" 
											abbreviation="<?php print($level->abbreviation); ?>" 
											<?php if ($level_id == $level->id) print("selected"); ?>>
											<?php print($level->group_name); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
			</div>
	</div>
	
	<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_items" >
		<?php
			$data = array(
					"levels"=>$levels,
					"yr_levels"=>$yr_levels,
			);
			
			$this->load->view('bed/levels/year_level_details',$data);					
		?>
	</div>
</div>



<script>
	$('.add_strand').click(function(event){
		
		$('#abbreviation_modal').focus();

		var level_id = $("select#level_id option:selected").attr('value');
		
		$('#track_id_add_modal').val(level_id);		
		$('#abbreviation_modal').val('');
		$('#description_modal').val('');
		$('#modal_new_strand').modal('show');
	});
</script>				


	<div id="modal_new_strand" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_year_level_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Year Level</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$data['levels'] = $levels;
							$this->load->view('bed/levels/modal_view/new_year_level_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Year Level!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<script>
		$('#add_year_level_form').submit(function(e) {
			e.preventDefault();
			
			var nav_level_id 	= $("select#level_id option:selected").attr('value');
			var level_id    	= $("select#track_id_add_modal option:selected").attr('value');
			var abbreviation	= $("select#track_id_add_modal option:selected").attr('level_abbreviation');
			var num_year		= $("select#track_id_add_modal option:selected").attr('num_year');
			var year_level 		= $('#abbreviation_modal').val();
			var description 	= $('#description_modal').val();

			$('#modal_new_strand').modal('hide');
			
			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/year_level_management'); ?>",
				data: {"level_id": level_id,
						"abbreviation": abbreviation,
						"description": description,
						"year_level": year_level,
						"nav_level_id": nav_level_id,
						"num_year": num_year,
						"action": "add_yr_level" 
					},
				dataType: 'json',
				success: function(response) {													

					$('#show_extracted_items').html(response.output); 
					
					$('#show_add_icon').html('');
					
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
			
		});
	</script>				


	<script>
		$("select#level_id").change(function(){
			
			var level_id = $("select#level_id option:selected").attr('value');

			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/year_level_management'); ?>",
				data: {"level_id": level_id,
						"action": "change_level" },
				dataType: 'json',
				success: function(response) {													

					$('#show_extracted_items').html(response.output); 
					
					$('#show_add_icon').html('');
					
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});

		});
		
	</script> 


