
		   				<table class="table table-condensed" style="width:100%;">
							<tr>
								<td style="Width:25%;">Course Code</td>
								<td style="width:75%; font-weight: bold;"><?php print($course->course_code); ?></td>
							</tr>
							<tr>
								<td>Descriptive Title</td>
								<td><input type="text" name="descriptive_title" style="width:300px;" value="<?php print($course->descriptive_title); ?>" ></td>
							</tr>
							<tr>
								<td>Credit Units</td>
								<td><input type="text" name="credit_units" style="width:80px;" value="<?php print($course->credit_units); ?>"> </td> <!-- disabled></td> -->
							</tr>
							<tr>
								<td>Paying Units</td>
								<td><input type="text" name="paying_units" style="width:80px;" value="<?php print($course->paying_units); ?>"> </td> <!-- disabled></td> -->
							</tr>
							<tr>
								<td>Service Course?</td>
								<td><input type="radio" name="service_course" value="N" 
									<?php if ($course->is_service_course == 'N') print("checked"); ?> > No 
									<input type="radio" name="service_course" value="Y" style="margin-left:20px;" 
									<?php if ($course->is_service_course == 'Y') print("checked"); ?> > Yes
								</td>
							</tr>
							<tr>
								<td>Course Type</td>
								<td><select name="type_id" style="width:auto;">
									<?php 
										foreach($types AS $type) {
											if ($type->id == $course->course_types_id) {
												print("<option value=".$type->id." selected>".$type->type_description."</option>");
											} else {
												print("<option value=".$type->id." >".$type->type_description."</option>");
											}
										}
									?>
									</select>
								</td>
							</tr>
						</table>
