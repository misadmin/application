<?php 
//$userinfo->fname = 'Christian'
//print_r ($userinfo);
//die();

?>
<h2 class="heading">Employee Profile</h2>

<div class="row-fluid">
	<div class="span2">
		<img src="<?php echo $image; ?>" class="img-polaroid" />
	</div>
	<div class="span10">
		<h3><?php echo $userinfo->fname . " " . ucfirst(substr($userinfo->mname, 0, 1)) . ". " . $userinfo->lname;
		if(! empty($userinfo->suffix))
			echo ", " . $userinfo->suffix;
		?></h3>
		<h4>Employee Number: <?php echo $userinfo->empno; ?></h4>
<?php foreach($roles as $role): ?>
		<h4><?php echo $role; ?></h4>
<?php endforeach; ?>
		<address>
			<?php echo $userinfo->street; ?> <?php echo $userinfo->town; ?> <!-- Edited: 5/6/2013, ra -->
  			<!-- <?php echo $userinfo->province; ?><br> -->
	  	</address>
	</div>
</div>


<script>
	$(document).ready(function(){
		var feedbacks_count = <?php echo $feedbacks_count; ?> 
		if (feedbacks_count > 0 ) {
			var redirect_to = <?php echo "'./".$this->userinfo['role']."/tools'"; ?>;	
			$.amaran({
				content  : {'message' : 'You have ' + feedbacks_count + ' unresolved feedbacks!',},
				position : 'bottom right',
				inEffect:'slideRight',
				onClick: function(){ window.location = redirect_to ; },
				delay:120000,
			});
		}
	});
</script>