
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Grade/Section</div>
			<div class="col2a">
				<input type="text" class="form-control" id="description_assign_room_modal" style="width:210px;" disabled />
			</div>
		</div>
		<div style="width:100%; margin-bottom:10px;">
			<div class="col1">Strand</div>
			<div class="col2a">
				<input type="text" class="form-control" id="strand_assign_room_modal" style="width:380px;" disabled />
			</div>
		</div>
		<div style="width:100%; font-size:12px; font-weight:bold; color:#FD4949;">
			Vacant Rooms from 7:30AM - 5:30PM Mon-Fri
		</div>
		<?php
			if ($section->room_no) {
		?>
				<div style="clear:both; width:100%; font-size:12px; font-weight:bold; color:#E40808; padding-top:5px;">
					NOTE: Changing rooms will also change all rooms in the <span style="text-decoration:underline;">Class Schedules</span>!
				</div>
		<?php 
			}
		?>
		<div id="vacant_rooms">
		
		</div>
	</div>
