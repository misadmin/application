<?php
require_once(APPPATH . "/third_party/barcode/BCGcode39.barcode.php");
require_once(APPPATH . "/third_party/barcode/BCGColor.php");
require_once(APPPATH . "/third_party/barcode/BCGBarcode.php");
require_once(APPPATH . "/third_party/barcode/BCGDrawing.php");
require_once(APPPATH . "/third_party/barcode/BCGFontFile.php");

class Barcode {
	private $filetypes;
	private $code_generated;
	private $filetype;
	private $text;
	private $color_white;
	private $color_black;
	
	public function __construct(){
		$this->font_directory = APPPATH . "/third_party/font";
		$this->filetypes = array('PNG' => BCGDrawing::IMG_FORMAT_PNG, 'JPEG' => BCGDrawing::IMG_FORMAT_JPEG, 'GIF' => BCGDrawing::IMG_FORMAT_GIF);
		$this->color_black = new BCGColor(0, 0, 0);
		$this->color_white = new BCGColor(255, 255, 255);
		$this->filetype = 'PNG';
		$this->text = "HNUMIS";
		$this->code_generated = new BCGcode39();
		$this->code_generated->setScale(1);
		$this->code_generated->setBackgroundColor($this->color_white);
		$this->code_generated->setForegroundColor($this->color_black);
		$this->set_font('Arial.ttf');
	}
	
	public function set_text ($text){
		$this->text = $text;
		$this->code_generated->parse($text);
	}
	
	public function set_filetype ($filetype){
		if (in_array(strtoupper($filetype), array('PNG', 'JPEG', 'GIF')))
			$this->filetype = strtoupper($filetype);	
	}
	
	private function set_header ($filetype){
		switch ($filetype){
			case 'PNG':
				header('Content-Type: image/png');
				break;
			case 'JPEG':
				header('Content-Type: image/jpeg');
				break;
			case 'GIF':
				header('Content-Type: image/gif');
				break;
		}
	}
	
	private function set_font ($font_family, $font_size = 8){
		$font = new BCGFontFile($this->font_directory . '/' . $font_family, $font_size);
		$this->code_generated->setFont($font);
	}
	
	public function render ($text='') {
		if (!empty($text))
			$this->set_text($text);
		$this->set_header ($this->filetype);
		$drawing = new BCGDrawing('', $this->color_white);
		$drawing->setBarcode($this->code_generated);
		$drawing->setRotationAngle(0);
		$drawing->setDPI(300);
		$drawing->draw();
		$drawing->finish($this->filetypes[$this->filetype]);
	}
}