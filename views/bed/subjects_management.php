<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>

<div id="show_course_management_page" >
	<h2 class="heading">Subjects Management</h2>
	<div id="rounded_header" style="width:99%; font-size:12px; margin:0 auto; margin-top:15px; padding:0px;">
		<div id="container">
			<div id="left" >
				<div id="show_error_msg" style="padding-left:10px; padding-top:10px; color:#F92424; font-weight:bold; font-size:14px;">
					
				</div>
			</div>
			<div id="right">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<div style="float:left; padding-top:6px; " >
					<a href="#" class="btn btn-success add_course" style="font-size:12px; height:17px; margin-right:8px; margin-bottom:2px;" >
						<i class="icon-plus-sign"></i> New Subject
					</a>
				</div>
			</div>
		</div>
	</div>
	
	<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_items" >
		<?php
			$data = array(
					"subjects"=>$subjects,
			);
			
			$this->load->view('bed/subjects/subjects_details',$data);					
		?>
	</div>
</div>



<script>
	$('.add_course').click(function(event){
		
		$('#show_error_msg').html('');
		
		$('#course_code_modal').val('');
		$('#descriptive_title_modal').val('');
		$('#paying_units_modal').val(1.0);
		$('#credit_units_modal').val(3.0);
		$('#course_types_modal').val(8);
		
		$('#show_search_code_icon').html('');
		
		$('#modal_new_course').modal('show');

	});
</script>				


	<div id="modal_new_course" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_course_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">New Subject</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php
							$this->load->view('bed/subjects/modal_view/new_course_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Subject!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#add_course_form').submit(function(e) {
		e.preventDefault();
		
		var course_code       = $('#course_code_modal').val();
		var descriptive_title = $('#descriptive_title_modal').val();
		var paying_units      = $('#paying_units_modal').val();
		var credit_units      = $('#credit_units_modal').val();
		var course_types_id   = $('#course_types_modal').val();
				
		if (isEmpty(course_code) || isEmpty(descriptive_title) || isEmpty(paying_units) || isEmpty(credit_units)) {
			alert('All fields are required!');
		} else {
			$('#modal_new_course').modal('hide');
			
			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/subject_management'); ?>",
				data: {"course_code": course_code,
						"descriptive_title": descriptive_title,
						"paying_units": paying_units,
						"credit_units": credit_units,
						"course_types_id": course_types_id,					
						"action": "add_course" },
				dataType: 'json',
				success: function(response) {	
					if (response.found) {
						alert('ERROR: Course code already exists!');
					}
					
					$('#show_extracted_items').html(response.output); 
					$('#show_add_icon').html('');
				}
			});
		}
		
	});
	
	
	function isEmpty(v) {
		if (v === '' || v === null) {
			return true;
		} else {
			return false;
		}
	}
	
</script>				



