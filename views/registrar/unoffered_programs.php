<div class="row-fluid span12">
	<form method="post" id="unoffered_programs_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
		<fieldset>
			<div class="control-group">
				<div class="controls">
					<select id="unoffered_colleges" name="college">
						<option value="">-- Select College --</option>
<?php foreach ($colleges as $college): ?>
						<option value="<?php echo $college->id; ?>" <?php if(isset($college_id) && $college_id==$college->id) echo ' selected="selected" '?>><?php echo $college->name; ?></option>
<?php endforeach; ?>
					</select>
					<span id="unoffered_loading_img" style="margin-left:10px;"><img style="width:30px;height:30px" src="<?php echo base_url($this->config->item('loading_image')); ?>" /></span>
				</div>
			</div>
			<div>
				<table class="table table-striped table-bordered mediaTable activeMediaTable" id="unoffered_programs_table">
					<thead>
						<tr>
							<th width="3%"><input type="checkbox" id="unoffered_select_all" /></th>
							<th width="25%">Program Abbreviation</th>
							<th width="72%">Program Name</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="control-group">
				<div class="controls">
					<button class="disabled btn btn-danger submit" id="activate_unoffered">Activate Program(s)</button>
					<button class="disabled btn btn-danger submit" id="deactivate_unoffered">Freeze Program(s)</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script>
$('#unoffered_colleges').bind('change', function(){
	var college_id = $('#unoffered_colleges option:selected').val();
	
	$('#unoffered_loading_img').show();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=U",
		dataType: "json",
		success: function(data){
			var rows = make_rows(data, 'unoffered');
			$('#unoffered_programs_table > tbody').html(rows);
			$('#unoffered_loading_img').hide();
			$('#activate_unoffered').removeClass('disabled');	
			$('#deactivate_unoffered').removeClass('disabled');
		}	
	});
});

$('#unoffered_select_all').bind('change', function(){
	
	if ($(this).is(':checked')){
		$('.unoffered_selector').each(function(){
			$(this).attr('checked', 'checked');
		});
	} else {
		$('.unoffered_selector').each(function(){
			$(this).removeAttr('checked');
		});
	}
});

$('.submit').bind('click', function(event){
	event.preventDefault();
	var what = $(this).attr('id');
	var todo;
	var form;

	switch (what){
		case 'activate_inactive' :
			todo = ' Activate inactive Program(s)';
			form = 'inactive_programs_form';
			break;
		case 'deactivate_active' :
			todo = ' Deactivate Active Program(s)';
			form = 'active_programs_form';
			break;
		case 'unoffer_active' :
			todo = ' Unoffer program(s) for First Year students';
			form = 'active_programs_form';
			break;
		case 'activate_unoffered' :
			todo = ' Activate Programs not offered to First Year Students';
			form = 'unoffered_programs_form';
			break;
		case 'deactivate_unoffered' :
			todo = ' Freeze Programs not offered to First Year Students';
			form = 'unoffered_programs_form';
			break;		
	}

	var confirmed = confirm("You are about to" + todo + ". Click Ok to continue. Otherwise, click Cancel");
	
	if (confirmed){
		$('<input>').attr({
			type: 'hidden',
			name: 'action',
			value: what
		}).appendTo('#' + form);
		$('#' + form).submit();	
	}	
});
</script>