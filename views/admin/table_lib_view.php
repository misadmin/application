<?php //This is not used as of april-4-13 ?>

<div class="row-fluid"><div class="span12">
<table <?php echo $attributes ?>>

	<?php if (is_array($head_rows)): ?>
	<thead>
	<?php foreach($head_rows as $row):?>
		<tr <?php echo (isset($row['attributes']) ? $row['attributes'] : '')?>>
			<?php if (is_array($row)):?>
				<?php foreach($row['data'] as $column):?>
				<?php if (is_array($column)):?>
					<th <?php echo $column['attributes']?>><?php echo $column['value']?></th>
				<?php else:?>
				<th><?php echo $column?></th>
				<?php endif;?>
				<?php endforeach; ?>
			<?php else:?>
			<td><?php echo $row ?></td>
			<?php endif;?>
		</tr>
	<?php endforeach;?>
	</thead>
	<?php endif; ?>
	
	<?php if (is_array($body_rows)): ?>
	<tbody>
	<?php foreach($body_rows as $row):?>
		<tr <?php echo (isset($row['attributes']) ? $row['attributes'] : '')?>>
			<?php if (is_array($row)):?>
				<?php foreach($row['data'] as $column):?>
				<?php if (is_array($column)):?>
					<td <?php echo $column['attributes']?>><?php echo $column['value']?></td>
				<?php else:?>
				<td><?php echo $column?></td>
				<?php endif;?>
				<?php endforeach; ?>
			<?php else:?>
			<td><?php echo $row ?></td>
			<?php endif;?>
		</tr>
	<?php endforeach;?>
	</tbody>
	<?php endif; ?>
	
	</tfoot>
</table></div></div>