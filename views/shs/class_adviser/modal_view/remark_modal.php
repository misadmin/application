<style>
	.c11 {
		float:left;
		width:25%;
		font-size:14px;
		font-weight:bold;
		margin-bottom:8px;
	}
	
	.c21 {
		float:left;
		width:75%;
		font-size:14px;
		color:#064D01;
		font-weight:bold;
		margin-bottom:10px;
	}

</style>

	
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%;">
			<div class="c11">Course Code</div>
			<div class="c21" id="course_code"></div>
		</div>
		<div style="width:100%;">
			<div class="c11">Teacher</div>
			<div class="c21" id="teacher"></div>
		</div>
		<div style="width:100%;">
			<div class="c11">Final Grade</div>
			<div class="c21" id="final_grade"></div>
		</div>
		<div style="width:100%;">
			<div class="c11">Remark</div>
			<div class="c21" >
				<select id="remark" class="form-control" style="width:auto;">
					<option value="NULL">--Select--</option>
					<option value="Prom">Promoted</option>
					<option value="Fail">Failed</option>
				</select>
			</div>
		</div>
	</div>
