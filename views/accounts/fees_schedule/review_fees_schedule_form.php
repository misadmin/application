<h2 class="heading">Fees Schedule - College</h2>

<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<input type="hidden" name="step" value="2">
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						print($academic_year);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Semester Term</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						$academic_terms = json_encode($terms);
						foreach($terms AS $k => $v) {
							$term = $this->Academicyears_model->getAcademicTerms($v);
							print($term->term."<br>");
						}						
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Fees Category</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						print($fees_subgroup->fees_group);					
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Item</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						print($fees_subgroup->description);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Program</div>
				<div class="controls">
					<?php 
						$prog_group = json_encode($programs);
						foreach($programs AS $k => $v) {
							$program_group = $this->Programs_model->getProgramGroup($v);
							print("<b>".$program_group->group_name."</b><br>");
							print("<div style=\"margin-left:40px; font-style:italic;\">".$program_group->prog."</div>");
						}					
					?>								
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Year Level</div>
				<div class="controls" style="font-weight:bold;">
					<?php 
						$y_levels = json_encode($year_levels);
						foreach($year_levels AS $k => $v) {
						$ordinal =date( 'S' , mktime( 1 , 1 , 1 , 1 , $v ) );
						print($v.$ordinal." Year <br>");
						}
					?>										
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label" >Amount</div>
				<div class="controls" style="font-weight:bold;">
					<?php print(number_format($rate,2));?>			
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<button class="btn btn-danger" id="back_to_fees" ><< Back to Fees Schedule</button>
					<button class="btn btn-warning" id="save_fees" fees_subgroups_id="<?php print($fees_subgroup->id); ?>" 
						acad_program_groups='<?php print($prog_group); ?>' year_levels='<?php print($y_levels); ?>' 
						academic_terms='<?php print($academic_terms); ?>' 
						rate="<?php print($rate); ?>">Save Fees Schedule >></button>
				</div>
			</div>
			</div>				
	</div>
</div>

 <form id="back_to_fees_sched_form" method="post">
  <input type="hidden" name="step" value="1" />
</form>

<script>
	$('#back_to_fees').click(function(event){
		event.preventDefault(); 
		
		$('#back_to_fees_sched_form').submit();
	});
</script>


<form id="save_fees_sched_form" method="post">
	<input type="hidden" name="step" value="3" />
</form>

<script>
	$('#save_fees').click(function(event){
		event.preventDefault(); 
		var fees_subgroups_id = $(this).attr('fees_subgroups_id');
		var acad_program_groups = $(this).attr('acad_program_groups');
		var year_levels = $(this).attr('year_levels');
		var academic_terms = $(this).attr('academic_terms');
		var rate = $(this).attr('rate');

		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'fees_subgroups_id',
		   	value: fees_subgroups_id,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'acad_program_groups',
		   	value: acad_program_groups,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'year_levels',
		   	value: year_levels,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'academic_terms',
		   	value: academic_terms,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'rate',
		   	value: rate,
		}).appendTo('#save_fees_sched_form');
		$('#save_fees_sched_form').submit();
	});
</script>



