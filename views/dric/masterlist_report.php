<div style="margin-top:10px">
<h2 class="heading"> MASTERLIST OF STUDENTS (<?php print("SY ".$term->sy." ".$term->term); ?> )</h2>
<h2> <a href="download" class="btn btn-link pull-right" id="download_list"><i class="icon-download-alt"></i> <strong>Download</strong></a> </h2>	

<table class="table table-striped table-bordered">
	<thead >
		<tr align="center">
			<th>No.</th>
			<th>Student ID</th>
			<th>Name</th>
			<th>Course</th>
			<th>Address</th>
		</tr>
	</thead>
	<tbody> 

	
	<?php 
		if($students) {
			$count = 0;
			foreach ($students as $student): ?>
			<tr>
				<?php $count++;?>
				<td><?php print($count); ?></td>
				<td><?php print($student->idno); ?></td>
				<td><?php print($student->sname); ?></td>
				<td><?php print($student->course); ?></td>
				<td><?php echo $student->home_address; ?></td>
			</tr>
<?php
			endforeach;
		} ?>
</tbody>
</table>

<form method="post" action="<?php echo site_url("{$role}/download_masterlist_address_csv"); ?>" id="download_list_form">
			<input type="hidden" name="term_id" value="<?php echo $term->id; ?>" />
			<input type="hidden" name="term" value="<?php echo $term->term; ?>" />
			<input type="hidden" name="sy" value="<?php echo $term->sy; ?>" />
			
</form>

<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
		
</div> 
