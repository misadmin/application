<style>
	div#msg_header_assign {
		border-radius: 5px; 
		background-color: #D7D4D4; 
		border: solid; 
		border-width: 1px; 
		border-color: #B7B6B6; 
		text-align:left; 
		padding-left: 10px;
		margin:0 auto;
		margin-bottom:20px;
		font-family: arial;
		font-size: 14px;
		width: 98%;
		height: 35px;
		line-height: 35px;
		font-weight:bold;
		color:#B50303;
</style>

<div class="row-fluid" >
<?php
	if (!$is_enrolled and $totally_withdrawn=="N") {
?>
		<div id="msg_header_assign"> 
				Student is not enrolled yet!
		</div>
<?php 
    } elseif ($totally_withdrawn=="Y"){
?>
		<div id="msg_header_assign"> 
				Student has totally withdrawn!
		</div>
<?php

	} else {
?>
		<div id="msg_header_assign" style="display:none;">
		</div>

		<div class="form-horizontal">
			<fieldset>
				<form id="proceed_withdraw_enrollment_form" class="form-horizontal">
					<div class="control-group formSep">
						<p>Totally withdraw senior high school enrollment!</p>
					</div>
					<div class="control-group">
						<div class="controls" style="float:left; padding-right:10px;">
							<input type="submit" class="btn btn-danger" name="withdraw_enrollment_btn" id="withdraw_enrollment_btn" value="Go ahead, withdraw!" />
						</div>
						<div style="float:left; padding-right:8px; margin-top:5px;" id="show_withdraw_enrollment_icon">
						</div>		
					</div>
				</form>
			</fieldset>
		</div>
<?php 
	} 
?>

</div>


<script>
	$('#proceed_withdraw_enrollment_form').submit(function(e){
		e.preventDefault();		
		
		var students_idno      = "<?php print($idnum); ?>";
		var student_history_id = "<?php print($student_history_id); ?>";

		$('#show_withdraw_enrollment_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			//alert(students_idno+' / '+student_history_id);

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+students_idno,
				data: { student_history_id: student_history_id,
						action: 'withdraw_enrollment' },
				dataType: 'text',
				success: function(response) {				
					$('#show_withdraw_enrollment_icon').html("");
					$('#msg_header_assign').show();	
					$('#msg_header_assign').html("Student successfully withdrawn totally!");
					alert("Student successfully withdrawn totally!");
				},
			    error: function (request, error) {
			           console.log(arguments);
			           alert(" Can't do because: " + error);
			    },
			});


	});

	function isEmpty(v) {
		if (v === '' || v === null) {
			return true;
		} else {
			return false;
		}
	}

</script>
