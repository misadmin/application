<h2 class="heading">List of All University Offerings</h2>
<div class="span10">

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">

<form id="change_sy" method="POST">
		<select name="academic_terms_id" id="academic_terms_id">
				<?php 
					foreach($terms AS $my_term) {
				?>
						<option value="<?php print($my_term->id); ?>" <?php if ($my_term->id == $selected_term) { print("selected"); } ?>>
							<?php print($my_term->term.' '.$my_term->sy); ?> </option>
				<?php 	
					}
				?>
		</select>
 							
</form>	
</div>


<table id="offerings" class="table table-bordered table-condensed">
    <thead>
    <tr>
      <th width="8%" style="text-align:center; vertical-align:middle;">Catalog ID</th>
      <th width="6%" style="text-align:center; vertical-align:middle;">Section</th>
      <th width="26%" style="text-align:center; vertical-align:middle;">Descriptive Title</th>
      <th width="5%" style="text-align:center; vertical-align:middle;">Units</th>      
      <th width="15%" style="text-align:center; vertical-align:middle;" >Time</th>
      <th width="5%" style="text-align:center; vertical-align:middle;" >Room</th>
      <th width="10%" style="text-align:center; vertical-align:middle;" >Enrolled</th>
      <th width="17%" style="text-align:center; vertical-align:middle;" >Faculty</th>
      <th width="8%" style="text-align:center; vertical-align:middle;" >Status</th>
    </tr>
    </thead>
<?php
//print_r($offerings); die();
/*		if ($offerings) {
			foreach ($offerings AS $offer) {
				echo '<tr>';
				echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->course_code.'</td>';
				if ($offer->status == 'ACTIVE') {
					echo '<td style="text-align:center; vertical-align:middle; "> 
					<a class="class_list" href="'. $offer->id . '" > ' . trim($offer->section_code) .'</a></td>';
				} else {
					print("<td style=\"text-align:center; vertical-align:middle; \">".trim($offer->section_code)."</td>");
				}
				//echo '<td style="text-align:center; vertical-align:middle; ">' . trim($offer->section_code);
				echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->descriptive_title.'</td>';
				echo '<td style="text-align:center; vertical-align:middle; ">' . $offer->credit_units.'</td>';       
			      
				$offering_slots = $this->Offerings_Model->ListOfferingSlots($offer->id);
				echo '<td style="text-align:left; vertical-align:middle; ">';
				if ($offering_slots) {
					foreach($offering_slots[0] AS $slots) {
						echo $slots->tym .' ' . $slots->days_day_code . '<br>';
					}
				  	print("</td>");
					echo '<td style="text-align:center; vertical-align:middle; ">';
					foreach($offering_slots[0] AS $slots) {
						echo $slots->bldg_name !== 'Main' ? str_pad($slots->room_no, 4, "0", STR_PAD_LEFT): $slots->room_no ;
						echo '<br>';
					}
			  		print("</td>");
				}    
      			echo '<td style="text-align:center; vertical-align:middle; ">';
				if ($offer->enrollee_parallel) { 
				 	 print($offer->enrollee_parallel."/".$offer->max_students); 
				} else { 
					print($offer->enrollee."/".$offer->max_students);  	  
				} 
				print("</td>");
				echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->employees_empno . ' ' . $offer->emp_name.'</td>';
				echo '<td style="text-align:center; vertical-align:middle; ">' . ucfirst(strtolower($offer->status).'</td>'); 
				print("</tr>");
			}
		}
*/

if ($offerings) {
	foreach ($offerings AS $offer) {
		$id = "('".$offer->id."')";

		print("<tr>");
		print("<td style=\"text-align:left; vertical-align:middle; \">".$offer->course_code."</td>");
		print("<td style=\"text-align:center; vertical-align:middle; \">");

		if ($offer->status == 'DISSOLVED' OR $this->session->userdata('academic_terms_id') > $selected_term) {
			print($offer->section_code."</td>");
		} else {
			print("<a class=\"class_list\" href=\"".$offer->id. "\" parallel_no=\" " . $offer->parallel_no . "\" courses_id=\"" . $offer->courses_id . "\" > " . $offer->section_code ."</a></td>");
		}
		print("<td style=\"text-align:left; vertical-align:middle; \">" . $offer->descriptive_title."</td>");
		print("<td style=\"text-align:center; vertical-align:middle; \">" . $offer->credit_units."</td>");

		$offering_slots = $this->Offerings_Model->ListOfferingSlots($offer->id);

		if ($offering_slots) {
			print("<td style=\"text-align:left; vertical-align:middle; \">");
			foreach($offering_slots[0] AS $slots) {
				print($slots->tym." ".$slots->days_day_code . "<br>");
			}
			print("</td>");
			print("<td style=\"text-align:left; vertical-align:middle; \">");
			if ($offering_slots) {
				foreach($offering_slots[0] AS $slots) {
					print($slots->room_no . "<br>");
				}
			}
			print("</td>");
		} else {
			print("<td style=\"text-align:left; vertical-align:middle; \"></td>");
			print("<td style=\"text-align:left; vertical-align:middle; \"></td>");
		}
			

			
		print("<td style=\"text-align:center; vertical-align:middle; \">");

		if ($offer->enrollee_parallel) {
			print($offer->enrollee_parallel);
			print("/" . $offer->max_students);

		} else {
			print($offer->enrollee . "/" . $offer->max_students);
		} 
		
		print("</td>");
		print("<td style=\"text-align:left; vertical-align:middle; \">" .$offer->employees_empno ." ". $offer->emp_name."</td>");
		print("<td style=\"text-align:center; vertical-align:middle; \">" . strtolower($offer->status)."</td>"); 
		print("</tr>");

	} //end of: foreach $offerings 
}

?>
</table>
</div>

  <form id="classlist" action="<?php echo site_url("registrar/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="show_tab" />
</form>

<script>
	$('.class_list').click(function(event){
		event.preventDefault(); 
		var offering_id = $(this).attr('href');
		var courses_id = $(this).attr('courses_id');
		var parallel_no = $(this).attr('parallel_no');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'offering_id',
		    value: offering_id,
		}).appendTo('#classlist');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'courses_id',
		    value: courses_id,
		}).appendTo('#classlist');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'parallel_no',
		    value: parallel_no,
		}).appendTo('#classlist');
		
		$('#classlist').submit();
	});
</script>

<script>
	$(document).ready(function(){
		$('#academic_terms_id').bind('change', function(){
			show_loading_msg();
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 'display_selected_offerings',
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>

<script>
$().ready(function() {
	$('#offerings').dataTable( {
	    "aoColumnDefs": [{ "bSearchable": false, "aTargets": [ 1,2,3,4,5,6 ] }],
		"iDisplayLength": 100,
	    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],	    
    });
    $("#offerings").stickyTableHeaders();
    $('#offerings').stickyTableHeaders('updateWidth');
	
	
})
</script>

