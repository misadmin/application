<?php if(isset($footer_scripts) && count($footer_scripts) > 0):?>
<?php foreach($footer_scripts as $script): ?>
	<script src="<?php echo $script; ?>"></script>
<?php endforeach;?>
<?php endif; ?>
<?php if($this->content_lib->html_class() != 'login_page'): ?>
		<footer>
			<div class="row">
				<div class="offset1 span6-condensed pull-right">
					<div class="span2-condensed">
						<center><img src="<?php echo base_url("assets/img/hnulogo60px.png");?>" /></center>
					</div>
					<div class="span4-condensed">
						<p>&copy; 2012 Holy Name University. All Rights Reserved<br />
						Corner Lesage and Gallares Streets<br />
						6300 Tagbilaran City, Bohol, Philippines<br />
						</p>
					</div>				
				</div>
			</div>
		</footer>
<?php endif;?>
		<?php // <!--[if lt IE 9]><script>document.body.innerHTML = "<h1>php echo $this->config->item('unsupported_browser_message') </h1>";</script><![endif]--> ?>
	</body>
</html>