  <?php echo validation_errors(); 
  ?>
  <div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; ">
  <table border="0" cellspacing="0" class="table table-striped table-bordered" style="width:60%; padding:0px; margin-top:20px;">
    <thead>
    <tr>
      <th style="width:15%; text-align:center; vertical-align:middle;">Program</th>
      <th style="width:60%; text-align:center; vertical-align:middle;" >Description</th>
      <th style="width:15%; text-align:center; vertical-align:middle;" >Effective Year</th>
      <th style="width:10%; text-align:center; vertical-align:middle;" >Status</th>
    </tr>
    </thead>
    <?php
		if ($programs) {
			foreach ($programs AS $program) {
	?>
    <tr>
      <td style="text-align:center; vertical-align:middle; "><?php print($program->abbreviation); ?></td>
      <td style="text-align:left; vertical-align:middle; "><?php print($program->description); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($program->effective_year); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($program->status); ?></td>
    </tr>
    <?php
			}
		}
	?>
</table>
  </div>
  <form id="srcform2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.schedule_slot').click(function(event){
		event.preventDefault(); 
		var offering_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'offering_id',
		    value: offering_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>