<?php

class registrar_lib {
	
	/**
	 * Codeigniter Instance
	 * @var unknown_type
	 */
	protected $CI;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}


	/*
	 * @ADDED: 8/7/14
	 * @author: genes
	 */
	function list_new_enrollees() {
		$this->CI->load->model('academic_terms_model');
		$this->CI->load->model('hnumis/enrollments_model');
		$this->CI->load->model('hnumis/Academicyears_model');
		
		$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'display_current');
		
		$acad_terms = $this->CI->Academicyears_model->ListAcademicTerms(FALSE);
		
		
		switch ($action) {
			case 'display_current':
				
				$current_term = $this->CI->academic_terms_model->getCurrentAcademicTerm();
				$enrollees = $this->CI->enrollments_model->List_New_Enrollees($current_term->id);
				
				$this->Create_New_Enrollees_Excel($enrollees,$current_term->term.' '.$current_term->sy);

				$data = array(
						"terms"=>$acad_terms,
						"selected_term"=>$current_term->id,
						"enrollees"=>$enrollees,
						"s_term"=>$current_term->term.' '.$current_term->sy,
					);
					
				break;
						
			case 'display_selected_term': //pulldown menu clicked
				$acad_term = $this->CI->Academicyears_model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$enrollees = $this->CI->enrollments_model->List_New_Enrollees($acad_term->id);
				
				$this->Create_New_Enrollees_Excel($enrollees,$acad_term->term.' '.$acad_term->sy);
				
				$data = array(
						"terms"=>$acad_terms,
						"selected_term"=>$this->CI->input->post('academic_terms_id'),
						"enrollees"=>$enrollees,
						"s_term"=>$acad_term->term.' '.$acad_term->sy,
					);
	
				break;
	
		}
	
		$this->CI->content_lib->enqueue_footer_script('data_tables');
		$this->CI->content_lib->enqueue_body_content('registrar/list_of_new_enrollees',$data);
		$this->CI->content_lib->content();
	
	
	}
	
	
	private function Create_New_Enrollees_Excel($students,$term) {
		$this->CI->load->library('PHPExcel');
		
		$filename = "downloads/New Enrollees ".$term.".xls";
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setTitle("New Enrollees: ".$term);
		
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', "New enrollees during ".$term);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
		->setSize(14);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(41);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(9);

		$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(24);

		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')
		->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
		->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'ID No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', 'Course/Year');
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', 'Gender');
		
		$num=5;
		
		if ($students) {
			foreach($students AS $stud) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, $stud->idno);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$num, $stud->name);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, $stud->abbreviation.'-'.$stud->year_level);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$num, $stud->gender);
				
				$objPHPExcel->getActiveSheet()->getStyle('A'.$num)
				->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$num)
				->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$num++;	
			}
		}
		
		$objWriter->save($filename,__FILE__);
		return;
		
	}
	
	
	function Print_DissolveStudents_pdf($dissolve_students,$academic_term,$teacher,$parallel_offerings,$offering_slots) {
			
		$this->CI->load->library('my_pdf');
		$this->CI->load->library('hnumis_pdf');
		$l = 4;
			
		$this->CI->hnumis_pdf->AliasNbPages();
		$this->CI->hnumis_pdf->set_HeaderTitle('LIST OF STUDENTS OF THE DISSOLVED COURSE');
		$this->CI->hnumis_pdf->AddPage('P','letter');
	
		$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->CI->hnumis_pdf->SetDrawColor(165,165,165);
			
		$this->CI->hnumis_pdf->SetFont('times','',11);
		$this->CI->hnumis_pdf->SetTextColor(10,10,10);
		$this->CI->hnumis_pdf->SetFillColor(255,255,255);
		$this->CI->hnumis_pdf->Ln();
	
		$this->CI->hnumis_pdf->Cell(30,$l,'School Year',0,0,'L',true);
		$this->CI->hnumis_pdf->Cell(100,$l,': '.$academic_term,0,0,'L',true);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Cell(30,$l,'Teacher',0,0,'L',true);
		$this->CI->hnumis_pdf->Cell(100,$l,': '.utf8_decode($teacher),0,0,'L',true);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Cell(30,$l,'Class',0,0,'L',true);
		if ($parallel_offerings) {
			if (is_array($parallel_offerings)) {
				foreach($parallel_offerings AS $parallel) {
					$this->CI->hnumis_pdf->SetX(40);
					$this->CI->hnumis_pdf->Cell(100,$l,': '.
							$parallel->course_code.'['.$parallel->section_code.']'.
							' '.$parallel->descriptive_title,0,0,'L',true);
					$this->CI->hnumis_pdf->Ln();
				}
			} else {
				$this->CI->hnumis_pdf->SetX(40);
				$this->CI->hnumis_pdf->Cell(100,$l,': '.
						$parallel_offerings->course_code.'['.$parallel_offerings->section_code.']'.
						' '.$parallel_offerings->descriptive_title,0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
			}
		}
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Cell(30,$l,'Schedule/Room',0,0,'L',true);

		foreach($offering_slots[0] AS $slot) {
			$this->CI->hnumis_pdf->SetX(40);
			$this->CI->hnumis_pdf->Cell(100,$l,': '.$slot->tym." ".$slot->days_day_code.' / ['.$slot->room_no.'] '.$slot->bldg_name,0,0,'L',true);
			$this->CI->hnumis_pdf->Ln();
		}
		$this->CI->hnumis_pdf->Ln();
	
		$w=array(15,25,70,30,12);
			
		$this->CI->hnumis_pdf->SetFont('times','B',10);
		$this->CI->hnumis_pdf->SetFillColor(116,116,116);
		$this->CI->hnumis_pdf->SetTextColor(253,253,253);
	
		$header=array(' ','ID No.','NAME','Program','Year');
		$this->CI->hnumis_pdf->SetX(22);
	
		for($i=0;$i<count($header);$i++)
			$this->CI->hnumis_pdf->Cell($w[$i],8,$header[$i],1,0,'C',true);
		
		$this->CI->hnumis_pdf->Ln();
	
		$this->CI->hnumis_pdf->SetTextColor(0,0,0);
		$this->CI->hnumis_pdf->SetFont('times','',10);
		$cnt=1;
	
		if ($dissolve_students) {
			foreach($dissolve_students AS $student) {
				$this->CI->hnumis_pdf->SetX(22);
				$this->CI->hnumis_pdf->Cell($w[0],8,$cnt,1,0,'C');
				$this->CI->hnumis_pdf->Cell($w[1],8,$student->idno,1,0,'C');
				$this->CI->hnumis_pdf->Cell($w[2],8,utf8_decode($student->neym),1,0,'L');
				$this->CI->hnumis_pdf->Cell($w[3],8,$student->abbreviation,1,0,'C');
				$this->CI->hnumis_pdf->Cell($w[4],8,$student->year_level,1,0,'C');
				$cnt++;
				$this->CI->hnumis_pdf->Ln();
			}
		}
			
		$this->CI->hnumis_pdf->Output();
				
	}
	
	
	/*
	 function view_exam_dates_basic_ed() {
		$this->CI->load->model('hnumis/AcademicYears_Model');
		$this->CI->load->model('basic_ed/rsclerk_model');
		
		$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 'display_current');

		$school_years = $this->CI->AcademicYears_Model->ListAcadYears();
		$levels = $this->CI->rsclerk_model->ListBasicEdLevels();
		
		//print_r($levels[0]->id); die();
		switch ($step) {
			case 'display_current':
				
				$acad_yr = $this->CI->AcademicYears_Model->GetAcademicYear($school_years[0]->id);
				
				$exam_sched = $this->CI->rsclerk_model->ListExamDates($levels[0]->id,$school_years[0]->id);
				
				//print_r($exam_sched); die();
				$data = array(
						"school_years"=>$school_years,
						"selected_year"=>$school_years[0]->id,
						"acad_yr"=>$acad_yr,
						"levels"=>$levels,
						"selected_level"=>$levels[0]->id,
						"selected_level_desc"=>$levels[0]->level,
						"exam_sched"=>$exam_sched,
				);

				break;
				
			case 'display_selected_term': //pulldown menu clicked
				$acad_yr = $this->CI->AcademicYears_Model->GetAcademicYear($this->CI->input->post('academic_year_id'));
				
				$exam_sched = $this->CI->rsclerk_model->ListExamDates($this->CI->input->post('levels_id'),$this->CI->input->post('academic_year_id'));
				
				$level = $this->CI->rsclerk_model->getBasicEdLevel($this->CI->input->post('levels_id'));
				
				$data = array(
						"school_years"=>$school_years,
						"selected_year"=>$this->CI->input->post('academic_year_id'),
						"acad_yr"=>$acad_yr,
						"levels"=>$levels,
						"selected_level"=>$this->CI->input->post('levels_id'),
						"selected_level_desc"=>$level->level,
						"exam_sched"=>$exam_sched,
				);
				
				break;
		}
		
		$this->CI->content_lib->enqueue_body_content('basic_ed/view_exam_dates',$data);
		$this->CI->content_lib->content();
		
	}
	*/
	
	function update_exam_dates() {
		$this->CI->load->model('hnumis/academicyears_model');
		$this->CI->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
		$this->CI->load->model('basic_ed/rsclerk_model');
		$this->CI->load->model('basic_ed/basic_ed_sections_model');
		
		//$levels = $this->CI->rsclerk_model->ListBasicEdLevels();
		$levels = $this->CI->basic_ed_sections_model->ListBasicLevels();
				
		$school_years = $this->CI->academicyears_model->ListAcadYears();
		
		$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list dates');
		
		switch ($action) {
		
			case 'list dates':// lists examination dates
		
				$acad_yr = $this->CI->academicyears_model->GetAcademicYear($school_years[0]->id);

				$exam_dates = $this->CI->enrollments_model->listExamDates($levels[0]->id,$school_years[0]->id);
				
				if ($acad_yr->start_year == DATE('Y')) {
					$can_edit = TRUE;
				} else {
					$can_edit = FALSE;
				}

				$data = array(
						"school_years"=>$school_years,
						"selected_year"=>$school_years[0]->id,
						"acad_yr"=>$acad_yr,
						"levels"=>$levels,
						"selected_level"=>$levels[0]->id,
						"selected_level_desc"=>$levels[0]->level,
						"exam_dates"=>$exam_dates,
						"can_edit"=>$can_edit,
						
				);
				
				break;

			case 'display_selected_term': //pulldown menu clicked
				$acad_yr = $this->CI->academicyears_model->GetAcademicYear($this->CI->input->post('academic_year_id'));
			
				$exam_dates = $this->CI->enrollments_model->listExamDates($this->CI->input->post('levels_id'),$this->CI->input->post('academic_year_id'));
												
				$level = $this->CI->rsclerk_model->getBasicEdLevel($this->CI->input->post('levels_id'));
				
				if ($acad_yr->start_year == DATE('Y')) {
					$can_edit = TRUE;
				} else {
					$can_edit = FALSE;
				}
				
				$data = array(
						"school_years"=>$school_years,
						"selected_year"=>$this->CI->input->post('academic_year_id'),
						"acad_yr"=>$acad_yr,
						"levels"=>$levels,
						"selected_level"=>$this->CI->input->post('levels_id'),
						"selected_level_desc"=>$level->level,
						"exam_dates"=>$exam_dates,
						"can_edit"=>$can_edit,
				);
				
				break;
				
			case 'update_schedule': //updates selected schedule
		
				if ($this->CI->input->post('nonce') && $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {
					$sked_id = $this->CI->input->post('schedule_id');
						
					$startDate = sanitize_text_field($this->CI->input->post('start_date'));
					$endDate = sanitize_text_field($this->CI->input->post('end_date'));
					//print($startDate."<br>".$endDate); die();
					if($this->CI->enrollments_model->updateExamDate($sked_id, $startDate, $endDate)){
						$this->CI->content_lib->set_message("Exam Date successfully updated!", "alert-success");
					} else {
						$this->CI->content_lib->set_message("Error found while updating Exam Date!", "alert-error");
					}
		
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
				}
		

				$acad_yr = $this->CI->academicyears_model->GetAcademicYear($this->CI->input->post('academic_year_id'));
					
				$exam_dates = $this->CI->enrollments_model->listExamDates($this->CI->input->post('levels_id'),$this->CI->input->post('academic_year_id'));
				
				$level = $this->CI->rsclerk_model->getBasicEdLevel($this->CI->input->post('levels_id'));

				if ($acad_yr->start_year == DATE('Y')) {
					$can_edit = TRUE;
				} else {
					$can_edit = FALSE;
				}
				
				$data = array(
						"school_years"=>$school_years,
						"selected_year"=>$this->CI->input->post('academic_year_id'),
						"acad_yr"=>$acad_yr,
						"levels"=>$levels,
						"selected_level"=>$this->CI->input->post('levels_id'),
						"selected_level_desc"=>$level->level,
						"exam_dates"=>$exam_dates,
						"can_edit"=>$can_edit,
				);
				
				break;
		
			case 'delete_schedule': //deletes selected schedule
		
				if ($this->CI->input->post('nonce') && $this->CI->common->nonce_is_valid($this->CI->input->post('nonce'))) {
					$sked_id = $this->CI->input->post('schedule_id');
		
					if($this->CI->enrollments_model->deleteExamDate($sked_id)){
						$this->CI->content_lib->set_message("Exam Date successfully deleted!", "alert-success");
					} else {
						$this->CI->content_lib->set_message("Error found while deleting Exam Date!", "alert-error");
					}
		
				} else {
					$this->CI->content_lib->set_message($this->CI->config->item('nonce_error_message'), 'alert-error');
				}
		
				$acad_yr = $this->CI->academicyears_model->GetAcademicYear($this->CI->input->post('academic_year_id'));
					
				$exam_dates = $this->CI->enrollments_model->listExamDates($this->CI->input->post('levels_id'),$this->CI->input->post('academic_year_id'));
				
				$level = $this->CI->rsclerk_model->getBasicEdLevel($this->CI->input->post('levels_id'));

				if ($acad_yr->start_year == DATE('Y')) {
					$can_edit = TRUE;
				} else {
					$can_edit = FALSE;
				}
				
				$data = array(
						"school_years"=>$school_years,
						"selected_year"=>$this->CI->input->post('academic_year_id'),
						"acad_yr"=>$acad_yr,
						"levels"=>$levels,
						"selected_level"=>$this->CI->input->post('levels_id'),
						"selected_level_desc"=>$level->level,
						"exam_dates"=>$exam_dates,
						"can_edit"=>$can_edit,
				);
				
				break;
		}

		$this->CI->content_lib->enqueue_body_content('basic_ed/update_exam_dates', $data);
		$this->CI->content_lib->content();
		
	}
	
}	