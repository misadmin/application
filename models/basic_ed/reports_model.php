<?php

	class Reports_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}

			function enrollment_summary_basic_ed() {
		
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('basic_ed/Basic_Ed_Enrollments_Model');
			//$this->load->model('hnumis/Academicyears_model');
				
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			$school_years = $this->AcademicYears_Model->ListAcadYears();
				
			switch ($step) {
				case 1:	
					
					$acad_yr = $this->AcademicYears_Model->GetAcademicYear($school_years[0]->id);

					$basic_eds = $this->Basic_Ed_Enrollments_Model->ListEnrollmentSummary($school_years[0]->id);
					
					$students = $this->Basic_Ed_Enrollment_Summary($basic_eds,$acad_yr->sy);
					
					$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$school_years[0]->id,
							"students"=>$students,
							"acad_yr"=>$acad_yr,
					);
					//print_r($data);die();
					break;
				case 2:	
					
					$acad_yr = $this->AcademicYears_Model->GetAcademicYear($this->input->post('academic_year_id'));
					
					$basic_eds = $this->Basic_Ed_Enrollments_Model->ListEnrollmentSummary($this->input->post('academic_year_id'));

					$students = $this->Basic_Ed_Enrollment_Summary($basic_eds,$acad_yr->sy);
					
					$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$this->input->post('academic_year_id'),
							"students"=>$students,
							"acad_yr"=>$acad_yr,
					);
						
					break;
				case 3:
					$this->create_enrollment_summary($this->input->post('academic_years_id'), 1);				
					return;
			}	
			
			$this->content_lib->enqueue_body_content('posting/enrollment_summary_basic_ed',$data);
			$this->content_lib->content();
				
		}
   				

		function assessment_summary_basic_ed() {
		
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('basic_ed/Assessment_Basic_Ed_Model');
	
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:	
					$this->list_academic_years();				
					break;
				case 2:	
					$this->create_assessment_summary($this->input->post('academic_years_id'), 0, 
														$this->input->post('post_status'));				
					break;
				case 3:	
					$this->create_assessment_summary($this->input->post('academic_years_id'), 1, 
														$this->input->post('post_status'));				
					break;
			}	

		}

	
		function list_academic_years() {
	
			$data['academic_years'] = $this->AcademicYears_Model->ListAcadYears();
	
			$this->content_lib->enqueue_body_content('posting/list_academic_years',$data);
			$this->content_lib->content();
				
		}
	

		function create_enrollment_summary($academic_years_id, $value) {
		
			$data['acad_yr'] = $this->AcademicYears_Model->GetAcademicYear($academic_years_id);

			$data['basic_eds'] = $this->Basic_Ed_Enrollments_Model->ListEnrollmentSummary($academic_years_id);
			if ($value == 0) {
				$this->content_lib->enqueue_body_content('posting/enrollment_summary_basic_ed',$data);
				$this->content_lib->content();
			} else {
				$this->generate_enrollment_summary_pdf($academic_years_id, $data);
			}
		
		}
	
	
		function create_assessment_summary($academic_years_id, $value, $post_status) {
		
			$data['acad_yr'] = $this->AcademicYears_Model->GetAcademicYear($academic_years_id);
	
			$data['departments'] = $this->Assessment_Basic_Ed_Model->ListAssessmentSummary($academic_years_id, 
																							$post_status);
			$data['post_status'] = $post_status;
			if ($value == 0) {
				$this->content_lib->enqueue_body_content('posting/assessment_summary_basic_ed',$data);
				$this->content_lib->content();
			} else {
				$this->generate_assessment_summary_pdf($data);
			}
		
		}


		function generate_enrollment_summary_pdf($academic_years_id, $data) {

			$this->load->library('my_pdf');
			$this->load->library('hnumis_pdf');
		
			$this->hnumis_pdf->AliasNbPages();		
			$this->hnumis_pdf->set_HeaderTitle("BASIC EDUCATION ENROLLMENT SUMMARY - ".$data['acad_yr']->sy);
			$this->hnumis_pdf->AddPage('L','folio');
			
			$this->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->hnumis_pdf->SetDrawColor(165,165,165);
			$this->hnumis_pdf->Ln();
		
			$this->hnumis_pdf->SetFont('times','B',9);
			$this->hnumis_pdf->SetFillColor(202,202,202);
			
			$this->hnumis_pdf->Cell(40,17,'DEPARTMENT',1,0,'C',true);

			$r = 1;
			$x = 50;
			$y = 35;

			$header1 = array('1','2','3','4','5','6','DEPT. TOTAL');

			$w1 = array(42,42,42,42,42,42,42);
			
			for($i=0;$i<count($w1);$i++) 
				$this->hnumis_pdf->Cell($w1[$i],10,$header1[$i],1,0,'C',true);
				
						
			$header2 = array('M','F','TOTAL','M','F','TOTAL','M','F','TOTAL','M','F','TOTAL',
									'M','F','TOTAL','M','F','TOTAL','M','F','TOTAL');

			for($i=0;$i<count($header2);$i++) {
				$this->hnumis_pdf->SetXY($x,$y);
				$this->hnumis_pdf->MultiCell(14,8,$header2[$i],1,'C',true);
				
				$x = $x + 14;
				$r++;
				if ($r > 21) {
					$r=1;
					$x=10;
					$y = $y + 28;
				} 
			}

			$w=array(40,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14);
			
			$this->hnumis_pdf->SetFont('Arial','',9);
			$this->hnumis_pdf->SetTextColor(0,0,0);
			$grand_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,'m6'=>0,
									'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,'f6'=>0,
									'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,'total6'=>0,
									'total_m'=>0,'total_f'=>0,'grand_total'=>0);
			$dept_total = array('total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,'total6'=>0,
									'total_m'=>0,'total_f'=>0,'dept_total'=>0);
		
 			$this->hnumis_pdf->SetFillColor(255,255,255);

 			foreach($data['basic_eds'] AS $basic_ed) {
				$dept_total['total1'] = $basic_ed->m1 + $basic_ed->f1;
				$dept_total['total2'] = $basic_ed->m2 + $basic_ed->f2;
				$dept_total['total3'] = $basic_ed->m3 + $basic_ed->f3;
				$dept_total['total4'] = $basic_ed->m4 + $basic_ed->f4;
				$dept_total['total5'] = $basic_ed->m5 + $basic_ed->f5;
				$dept_total['total6'] = $basic_ed->m6 + $basic_ed->f6;
	
				$dept_total['total_m'] = $basic_ed->m1 + $basic_ed->m2 + $basic_ed->m3 + $basic_ed->m4 + $basic_ed->m5 + $basic_ed->m6;
				$dept_total['total_f'] = $basic_ed->f1 + $basic_ed->f2 + $basic_ed->f3 + $basic_ed->f4 + $basic_ed->f5 + $basic_ed->f6;
				$dept_total['dept_total'] = $dept_total['total_m'] + $dept_total['total_f'];
				
				$grand_total['m1'] = $grand_total['m1'] + $basic_ed->m1; 
				$grand_total['f1'] = $grand_total['f1'] + $basic_ed->f1;
				$grand_total['total1'] = $grand_total['total1'] + $dept_total['total1'];
				$grand_total['m2'] = $grand_total['m2'] + $basic_ed->m2; 
				$grand_total['f2'] = $grand_total['f2'] + $basic_ed->f2;
				$grand_total['total2'] = $grand_total['total2'] + $dept_total['total2'];
				$grand_total['m3'] = $grand_total['m3'] + $basic_ed->m3; 
				$grand_total['f3'] = $grand_total['f3'] + $basic_ed->f3;
				$grand_total['total3'] = $grand_total['total3'] + $dept_total['total3'];
				$grand_total['m4'] = $grand_total['m4'] + $basic_ed->m4; 
				$grand_total['f4'] = $grand_total['f4'] + $basic_ed->f4;
				$grand_total['total4'] = $grand_total['total4'] + $dept_total['total4'];
				$grand_total['m5'] = $grand_total['m5'] + $basic_ed->m5; 
				$grand_total['f5'] = $grand_total['f5'] + $basic_ed->f5;
				$grand_total['total5'] = $grand_total['total5'] + $dept_total['total5'];
				$grand_total['m6'] = $grand_total['m6'] + $basic_ed->m6; 
				$grand_total['f6'] = $grand_total['f6'] + $basic_ed->f6;
				$grand_total['total6'] = $grand_total['total6'] + $dept_total['total6'];
				$grand_total['total_m'] = $grand_total['total_m'] + $dept_total['total_m']; 
				$grand_total['total_f'] = $grand_total['total_f'] + $dept_total['total_f'];
				$grand_total['grand_total'] = $grand_total['grand_total'] + $dept_total['dept_total'];

				$this->hnumis_pdf->Cell($w[0],8,'     '.$basic_ed->level,1,0,'L',true);
					
				$this->hnumis_pdf->Cell($w[1],8,$basic_ed->m1,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[2],8,$basic_ed->f1,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[3],8,$dept_total['total1'],1,0,'C',true);
					
				$this->hnumis_pdf->Cell($w[4],8,$basic_ed->m2,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[5],8,$basic_ed->f2,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[6],8,$dept_total['total2'],1,0,'C',true);

				$this->hnumis_pdf->Cell($w[7],8,$basic_ed->m3,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[8],8,$basic_ed->f3,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[9],8,$dept_total['total3'],1,0,'C',true);

				$this->hnumis_pdf->Cell($w[10],8,$basic_ed->m4,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[11],8,$basic_ed->f4,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[12],8,$dept_total['total4'],1,0,'C',true);

				$this->hnumis_pdf->Cell($w[13],8,$basic_ed->m5,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[14],8,$basic_ed->f5,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[15],8,$dept_total['total5'],1,0,'C',true);

				$this->hnumis_pdf->Cell($w[16],8,$basic_ed->m6,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[17],8,$basic_ed->f6,1,0,'C',true);
				$this->hnumis_pdf->Cell($w[18],8,$dept_total['total6'],1,0,'C',true);

				$this->hnumis_pdf->Cell($w[19],8,$dept_total['total_m'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[20],8,$dept_total['total_f'],1,0,'C',true);
				$this->hnumis_pdf->Cell($w[21],8,$dept_total['dept_total'],1,0,'C',true);
				$this->hnumis_pdf->Ln();
			}

			$this->hnumis_pdf->SetFillColor(202,202,202);
			$this->hnumis_pdf->Cell($w[0],8,'Grand Total :',1,0,'R',true);
			$this->hnumis_pdf->Cell($w[1],8,$grand_total['m1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[2],8,$grand_total['f1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[3],8,$grand_total['total1'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[4],8,$grand_total['m2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[5],8,$grand_total['f2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[6],8,$grand_total['total2'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[7],8,$grand_total['m3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[8],8,$grand_total['f3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[9],8,$grand_total['total3'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[10],8,$grand_total['m4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[11],8,$grand_total['f4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[12],8,$grand_total['total4'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[13],8,$grand_total['m5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[14],8,$grand_total['f5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[15],8,$grand_total['total5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[16],8,$grand_total['m5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[17],8,$grand_total['f5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[18],8,$grand_total['total5'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[19],8,$grand_total['total_m'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[20],8,$grand_total['total_f'],1,0,'C',true);
			$this->hnumis_pdf->Cell($w[21],8,$grand_total['grand_total'],1,0,'C',true);
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->Output();
		}	


		function generate_assessment_summary_pdf($data) {

			$this->load->library('my_pdf');
			$this->load->library('hnumis_pdf');
		
			$this->hnumis_pdf->AliasNbPages();		
			$this->hnumis_pdf->set_HeaderTitle('BASIC EDUCATION ASSESSMENT SUMMARY - '.$data['acad_yr']->sy);
			$this->hnumis_pdf->AddPage('L','folio');
			
			$this->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->hnumis_pdf->SetDrawColor(165,165,165);
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->SetFont('times','B',10);
			$this->hnumis_pdf->SetFillColor(116,116,116);
			$this->hnumis_pdf->SetTextColor(253,253,253);
			
			$header1 = array('DEPARTMENT','1','2','3','4','5','6','7','8','9','10','11','12','TOTAL');

			$w1 = array(35,20,20,20,20,20,20,20,20,20,20,20,20,24);
			
			for($i=0;$i<count($w1);$i++) 
				$this->hnumis_pdf->Cell($w1[$i],8,$header1[$i],1,0,'C',true);
				
			$this->hnumis_pdf->Ln();
						
			$this->hnumis_pdf->SetFont('Arial','',8);
			$this->hnumis_pdf->SetTextColor(0,0,0);
 			$this->hnumis_pdf->SetFillColor(255,255,255);

			$grand_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'assess6'=>0,
								'assess7'=>0,'assess8'=>0,'assess9'=>0,'assess10'=>0,'assess11'=>0,'assess12'=>0,
								'total'=>0);
			foreach($data['departments'] AS $department) {
				
				$total = 0;
	
				$total = $department->assess1+$department->assess2+$department->assess3+$department->assess4+$department->assess5+$department->assess6+
						$department->assess7+$department->assess8+$department->assess9+$department->assess10+$department->assess11+$department->assess12; 
				$grand_total['assess1'] = $grand_total['assess1'] + $department->assess1;
				$grand_total['assess2'] = $grand_total['assess2'] + $department->assess2;
				$grand_total['assess3'] = $grand_total['assess3'] + $department->assess3;
				$grand_total['assess4'] = $grand_total['assess4'] + $department->assess4;
				$grand_total['assess5'] = $grand_total['assess5'] + $department->assess5;
				$grand_total['assess6'] = $grand_total['assess6'] + $department->assess6;
				$grand_total['assess7'] = $grand_total['assess7'] + $department->assess7;
				$grand_total['assess8'] = $grand_total['assess8'] + $department->assess8;
				$grand_total['assess9'] = $grand_total['assess9'] + $department->assess9;
				$grand_total['assess10'] = $grand_total['assess10'] + $department->assess10;
				$grand_total['assess11'] = $grand_total['assess11'] + $department->assess11;
				$grand_total['assess12'] = $grand_total['assess12'] + $department->assess12;
				$grand_total['total'] = $grand_total['total'] + $total;
			
				$this->hnumis_pdf->Cell($w1[0],8,$department->level,1,0,'L',true);
				$this->hnumis_pdf->Cell($w1[1],8,number_format($department->assess1,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[2],8,number_format($department->assess2,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[3],8,number_format($department->assess3,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[4],8,number_format($department->assess4,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[5],8,number_format($department->assess5,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[6],8,number_format($department->assess6,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[7],8,number_format($department->assess7,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[8],8,number_format($department->assess8,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[9],8,number_format($department->assess9,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[10],8,number_format($department->assess10,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[11],8,number_format($department->assess11,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[12],8,number_format($department->assess12,2),1,0,'R',true);
				$this->hnumis_pdf->Cell($w1[13],8,number_format($total,2),1,0,'R',true);
				$this->hnumis_pdf->Ln();
			}
			
			$this->hnumis_pdf->SetFillColor(202,202,202);
			$this->hnumis_pdf->Cell($w1[0],8,'GRAND TOTAL: ',1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[1],8,number_format($grand_total['assess1'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[2],8,number_format($grand_total['assess2'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[3],8,number_format($grand_total['assess3'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[4],8,number_format($grand_total['assess4'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[5],8,number_format($grand_total['assess5'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[6],8,number_format($grand_total['assess6'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[7],8,number_format($grand_total['assess7'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[8],8,number_format($grand_total['assess8'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[9],8,number_format($grand_total['assess9'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[10],8,number_format($grand_total['assess10'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[11],8,number_format($grand_total['assess11'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[12],8,number_format($grand_total['assess12'],2),1,0,'R',true);
			$this->hnumis_pdf->Cell($w1[13],8,number_format($grand_total['total'],2),1,0,'R',true);
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->Output();
		}	
		
		
		function basic_ed_fix() {
			
			$this->load->model('basic_ed/rsclerk_model');
			
			$data['find_students'] = new ArrayObject();
			$data['not_find_students'] = new ArrayObject();
						
			$students = $this->rsclerk_model->ListOldHistory(114);
			
			foreach ($students AS $stud) {
				$find = $this->rsclerk_model->getNewHistory_NoInsert($stud->id);
				if ($find == $stud) {
					$data['find_students']->append($this->rsclerk_model->getFindStudent($find->id));
				} else {
					$this->db->trans_start();
					$find1 = $this->rsclerk_model->getNewHistory($stud->id);
					if ($this->rsclerk_model->InsertNewHistory($stud)) {
						if ($this->rsclerk_model->UpdateNewHistory($stud,$find1)) {
							$this->db->trans_complete();
						} else {
							$this->db->trans_rollback();
							$this->content_lib->set_message("ERROR updating new History!", 'alert-error');
							//$this->index();
						}
					} else {
						$this->db->trans_rollback();
						$this->content_lib->set_message("ERROR inserting new History!", 'alert-error');
						//$this->index();
					}
					
					$data['not_find_students']->append($this->rsclerk_model->getNotFindStudent($find->id));
				}
			}
			//print_r($data['find_students']); die();

			$this->content_lib->enqueue_body_content('basic_ed/student_on_history',$data);
			$this->content_lib->content();
		}

		
		function fix_histories() {
				
			$this->load->model('basic_ed/rsclerk_model');
			$academic_years_id = 115;
			$success = FALSE;
				
			$this->db->trans_start();
			if ($this->rsclerk_model->UpdateToActive($academic_years_id)) {
				$students = $this->rsclerk_model->ListNewStudents($academic_years_id);
		
				foreach($students AS $stud) {
					if (!$this->rsclerk_model->FixAcadYears($stud,$academic_years_id)) {
						$this->db->trans_rollback();
						$this->content_lib->set_message("ERROR updating 115 to active!", 'alert-error');
						$success = FALSE;
						break 2;
					} else {
						$success = TRUE;
					}
				}
				
				$students = $this->rsclerk_model->ListNewStudents(114);
				
				foreach($students AS $stud) {
					if (!$this->rsclerk_model->FixAcadYears($stud,114)) {
						$this->db->trans_rollback();
						$this->content_lib->set_message("ERROR updating 114 to active!", 'alert-error');
						$success = FALSE;
						break 2;
					} else {
						$success = TRUE;
					}
				}
				
		
			} else {
				$this->db->trans_rollback();
				$this->content_lib->set_message("ERROR updating to active!", 'alert-error');
			}
		
			if ($success) {
				$this->db->trans_complete();
				$this->content_lib->set_message("SUCCESS updating!", 'alert-success');
			}
				
			$this->content_lib->enqueue_body_content('basic_ed/fix_histories');
			$this->content_lib->content();
				
		}


		function Create_Class_List_Excel($data) {
			$this->Create_Class_List($data);
				
			return;
		}
		
		/*
		 * EDITED: 7/8/15 by genes
		 */
		private function Create_Class_List($data) {
			$this->load->library('PHPExcel');
		
			/*$level="";
			if ($data['section']->levels_id == 2) {
				$level="Kinder-".$data['section']->year_level;
			} elseif ($data['section']->levels_id == 3) {
				$level="Grade-".$data['section']->year_level;
			} else {
				$level="Year-".$data['section']->year_level;
			}
			*/
				
			$filename = "downloads/Class List - ".$data['section_name']." ".$data['yr_level'].".xls";
			$objPHPExcel = new PHPExcel();
		
			$objPHPExcel->getProperties()->setTitle("Class List - ".$data['section_name']." ".$data['yr_level']);
				
			$objPHPExcel->setActiveSheetIndex(0);
				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
				
			$objPHPExcel->getActiveSheet()->getPageMargins()
			->setTop(1)
			->setRight(0.4)
			->setLeft(0.4)
			->setBottom(1);
				
			$objPHPExcel->getActiveSheet()->getPageSetup()
			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT)
			->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER);
				
			$objPHPExcel->getActiveSheet()->getHeaderFooter()
			->setOddHeader('&L&"-,Bold Italic"Holy Name University - Basic Education')
			->setOddFooter('&L&B&IRun Date: &D &T' . '&RPage &P of &N');
				
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', "Basic Education: ".$data['acad_sy'])
			->getStyle('A1')->getFont()->setBold(true)
			->setSize(14);
			$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'SECTION/LEVEL: '.$data['section_name']." - ".$data['yr_level'])
			->getStyle('A2')->getFont()
			->setSize(13);
			$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'ADVISER: '.$data['class_adviser'])
			->getStyle('A3')->getFont()
			->setSize(13);
			$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'ROOM No.: '.$data['room'])
			->getStyle('A4')->getFont()
			->setSize(13);
				
			$styleArray = array( 'font' => array( 'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE),);
			$objPHPExcel->getActiveSheet()->SetCellValue('A6', 'Male');
			$objPHPExcel->getActiveSheet()->SetCellValue('E6', 'Female');
			$objPHPExcel->getActiveSheet()->getStyle('B7:I7')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->SetCellValue('B7', 'Family Name');
			$objPHPExcel->getActiveSheet()->SetCellValue('C7', 'Given Name');
			$objPHPExcel->getActiveSheet()->SetCellValue('D7', 'Middle Name');
			$objPHPExcel->getActiveSheet()->SetCellValue('G7', 'Family Name');
			$objPHPExcel->getActiveSheet()->SetCellValue('H7', 'Given Name');
			$objPHPExcel->getActiveSheet()->SetCellValue('I7', 'Middle Name');
				
				
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(4);
		
			for ($col = 'B'; $col != 'E'; $col++) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(16);
			}
			for ($col = 'G'; $col != 'J'; $col++) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(16);
			}
				
			$m=0;
			$f=0;
			$col_m=8;
			$col_f=8;

			if ($data['list']) {
				foreach($data['list'] AS $stud) {
					if ($stud->gender == 'M') {
						$m++;
						$objPHPExcel->getActiveSheet()->SetCellValue('A'.$col_m, $m);
						$objPHPExcel->getActiveSheet()->SetCellValue('B'.$col_m, strtoupper($stud->lname));
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$col_m, $stud->fname);
						$objPHPExcel->getActiveSheet()->SetCellValue('D'.$col_m, $stud->mname);
						
						$col_m++;
					} else {
						$f++;
						$objPHPExcel->getActiveSheet()->SetCellValue('F'.$col_f, $f);
						$objPHPExcel->getActiveSheet()->SetCellValue('G'.$col_f, strtoupper($stud->lname));
						$objPHPExcel->getActiveSheet()->SetCellValue('H'.$col_f, $stud->fname);
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$col_f, $stud->mname);
						
						$col_f++;
					}
				}
			}
			$total = $m + $f;
			$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'No. of Students: '.$total)
			->getStyle('C4')->getFont()
			->setSize(13);
				
				
			$objWriter->save($filename,__FILE__);
		
		}
		
		
		private function Basic_Ed_Enrollment_Summary($basic_eds,$sy) {
				
			$this->load->library('PHPExcel');
				
			$filename = "downloads/Basic Education Enrollment Summary ".$sy.".xls";
			$objPHPExcel = new PHPExcel();
		
			$objPHPExcel->getProperties()->setTitle("Basic Education Enrollment Summary: ".$sy);
				
			$objPHPExcel->setActiveSheetIndex(0);
				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', "Basic Education Enrollment Summary : ".$sy);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
			->setSize(14);
			$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(21);
		
			for ($col = 'B'; $col != 'AO'; $col++) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(7.5);
			}
			$objPHPExcel->getActiveSheet()->getStyle('A3:AN4')
			->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
			$styleArray = array(
					'borders' => array(
							'inside'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							),
							'outline'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							)
					)
			);
		
		
			$objPHPExcel->getActiveSheet()->getStyle('A3:AN4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()->setRGB('EAEAEA');
			$objPHPExcel->getActiveSheet()->getStyle('A3:AN4')->getFont()->setBold(true);
				
			$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(21);
			$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(21);
		
			$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'DEPARTMENT');
			$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
			$objPHPExcel->getActiveSheet()->SetCellValue('B3', '1');
			$objPHPExcel->getActiveSheet()->mergeCells('E3:G3');
			$objPHPExcel->getActiveSheet()->SetCellValue('E3', '2');
			$objPHPExcel->getActiveSheet()->mergeCells('H3:J3');
			$objPHPExcel->getActiveSheet()->SetCellValue('H3', '3');
			$objPHPExcel->getActiveSheet()->mergeCells('K3:M3');
			$objPHPExcel->getActiveSheet()->SetCellValue('K3', '4');
			$objPHPExcel->getActiveSheet()->mergeCells('N3:P3');
			$objPHPExcel->getActiveSheet()->SetCellValue('N3', '5');
			$objPHPExcel->getActiveSheet()->mergeCells('Q3:S3');
			$objPHPExcel->getActiveSheet()->SetCellValue('Q3', '6');
			$objPHPExcel->getActiveSheet()->mergeCells('T3:V3');
			$objPHPExcel->getActiveSheet()->SetCellValue('T3', '7');
			$objPHPExcel->getActiveSheet()->mergeCells('W3:Y3');
			$objPHPExcel->getActiveSheet()->SetCellValue('W3', '8');
			$objPHPExcel->getActiveSheet()->mergeCells('Z3:AB3');
			$objPHPExcel->getActiveSheet()->SetCellValue('Z3', '9');
			$objPHPExcel->getActiveSheet()->mergeCells('AC3:AE3');
			$objPHPExcel->getActiveSheet()->SetCellValue('AC3', '10');
			$objPHPExcel->getActiveSheet()->mergeCells('AF3:AH3');
			$objPHPExcel->getActiveSheet()->SetCellValue('AF3', '11');
			$objPHPExcel->getActiveSheet()->mergeCells('AI3:AK3');
			$objPHPExcel->getActiveSheet()->SetCellValue('AI3', '12');
			$objPHPExcel->getActiveSheet()->mergeCells('AL3:AN3');
			$objPHPExcel->getActiveSheet()->SetCellValue('AL3', 'DEPARTMENT TOTAL');
		
			$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('F4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('G4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('H4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('I4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('J4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('K4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('L4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('M4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('N4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('O4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('P4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('Q4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('R4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('S4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('T4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('U4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('V4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('W4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('X4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('Y4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('Z4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('AA4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('AB4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('AC4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('AD4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('AE4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('AF4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('AG4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('AH4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('AI4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('AJ4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('AK4', 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('AL4', 'M');
			$objPHPExcel->getActiveSheet()->SetCellValue('AM4', 'F');
			$objPHPExcel->getActiveSheet()->SetCellValue('AN4', 'TOTAL');
				
			$objPHPExcel->getActiveSheet()->getPageMargins()
			->setTop(1)
			->setRight(0.75)
			->setLeft(0.75)
			->setBottom(1);
				
			$objPHPExcel->getActiveSheet()->getPageSetup()
			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
			->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
				
			$objPHPExcel->getActiveSheet()->getHeaderFooter()
			->setOddHeader('&L&"-,Bold Italic"Holy Name University - Basic Education')
			->setOddFooter('&L&B&IRun Date: &D &T' . '&RPage &P of &N');
				
				
			$students = array();
				
			$grand_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,'m6'=>0,'m7'=>0,'m8'=>0,'m9'=>0,'m10'=>0,'m11'=>0,'m12'=>0,
					'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,'f6'=>0,'f7'=>0,'f8'=>0,'f9'=>0,'f10'=>0,'f11'=>0,'f12'=>0,
					'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,'total6'=>0,'total7'=>0,'total8'=>0,'total9'=>0,
					'total10'=>0,'total11'=>0,'total12'=>0,
					'total_m'=>0,'total_f'=>0,'grand_total'=>0);
			$num=5;
			$cnt=0;
			foreach($basic_eds AS $basic_ed) {
		
				$dtotal1 = $basic_ed->m1 + $basic_ed->f1;
				$dtotal2 = $basic_ed->m2 + $basic_ed->f2;
				$dtotal3 = $basic_ed->m3 + $basic_ed->f3;
				$dtotal4 = $basic_ed->m4 + $basic_ed->f4;
				$dtotal5 = $basic_ed->m5 + $basic_ed->f5;
				$dtotal6 = $basic_ed->m6 + $basic_ed->f6;
				$dtotal7 = $basic_ed->m7 + $basic_ed->f7;
				$dtotal8 = $basic_ed->m8 + $basic_ed->f8;
				$dtotal9 = $basic_ed->m9 + $basic_ed->f9;
				$dtotal10 = $basic_ed->m10 + $basic_ed->f10;
				$dtotal11 = $basic_ed->m11 + $basic_ed->f11;
				$dtotal12 = $basic_ed->m12 + $basic_ed->f12;
				
				$tm = $basic_ed->m1 + $basic_ed->m2 + $basic_ed->m3 + $basic_ed->m4 + $basic_ed->m5 + $basic_ed->m6 + $basic_ed->m7 + $basic_ed->m8 
					+ $basic_ed->m9 + $basic_ed->m10 + $basic_ed->m11 + $basic_ed->m12;
				$tf = $basic_ed->f1 + $basic_ed->f2 + $basic_ed->f3 + $basic_ed->f4 + $basic_ed->f5 + $basic_ed->f6 + $basic_ed->f7 + $basic_ed->f8 
					+ $basic_ed->f9 + $basic_ed->f10 + $basic_ed->f11 + $basic_ed->f12;
				$dt = $tm + $tf;
		
				$grand_total['m1'] = $grand_total['m1'] + $basic_ed->m1;
				$grand_total['f1'] = $grand_total['f1'] + $basic_ed->f1;
				$grand_total['total1'] = $grand_total['total1'] + $dtotal1;
				$grand_total['m2'] = $grand_total['m2'] + $basic_ed->m2;
				$grand_total['f2'] = $grand_total['f2'] + $basic_ed->f2;
				$grand_total['total2'] = $grand_total['total2'] + $dtotal2;
				$grand_total['m3'] = $grand_total['m3'] + $basic_ed->m3;
				$grand_total['f3'] = $grand_total['f3'] + $basic_ed->f3;
				$grand_total['total3'] = $grand_total['total3'] + $dtotal3;
				$grand_total['m4'] = $grand_total['m4'] + $basic_ed->m4;
				$grand_total['f4'] = $grand_total['f4'] + $basic_ed->f4;
				$grand_total['total4'] = $grand_total['total4'] + $dtotal4;
				$grand_total['m5'] = $grand_total['m5'] + $basic_ed->m5;
				$grand_total['f5'] = $grand_total['f5'] + $basic_ed->f5;
				$grand_total['total5'] = $grand_total['total5'] + $dtotal5;
				$grand_total['m6'] = $grand_total['m6'] + $basic_ed->m6;
				$grand_total['f6'] = $grand_total['f6'] + $basic_ed->f6;
				$grand_total['total6'] = $grand_total['total6'] + $dtotal6;
				$grand_total['m7'] = $grand_total['m7'] + $basic_ed->m7;
				$grand_total['f7'] = $grand_total['f7'] + $basic_ed->f7;
				$grand_total['total7'] = $grand_total['total7'] + $dtotal7;
				$grand_total['m8'] = $grand_total['m8'] + $basic_ed->m8;
				$grand_total['f8'] = $grand_total['f8'] + $basic_ed->f8;
				$grand_total['total8'] = $grand_total['total8'] + $dtotal8;
				$grand_total['m9'] = $grand_total['m9'] + $basic_ed->m9;
				$grand_total['f9'] = $grand_total['f9'] + $basic_ed->f9;
				$grand_total['total9'] = $grand_total['total9'] + $dtotal9;
				$grand_total['m10'] = $grand_total['m10'] + $basic_ed->m10;
				$grand_total['f10'] = $grand_total['f10'] + $basic_ed->f10;
				$grand_total['total10'] = $grand_total['total10'] + $dtotal10;
				$grand_total['m11'] = $grand_total['m11'] + $basic_ed->m11;
				$grand_total['f11'] = $grand_total['f11'] + $basic_ed->f11;
				$grand_total['total11'] = $grand_total['total11'] + $dtotal11;
				$grand_total['m12'] = $grand_total['m12'] + $basic_ed->m12;
				$grand_total['f12'] = $grand_total['f12'] + $basic_ed->f12;
				$grand_total['total12'] = $grand_total['total12'] + $dtotal12;
				$grand_total['total_m'] = $grand_total['total_m'] + $tm;
				$grand_total['total_f'] = $grand_total['total_f'] + $tf;
				$grand_total['grand_total'] = $grand_total['grand_total'] + $dt;
		
				$students[] = array('id'=>$basic_ed->id,
						'level'=>$basic_ed->level,
						'm1'=>$basic_ed->m1,
						'f1'=>$basic_ed->f1,
						'total1'=>$dtotal1,
						'm2'=>$basic_ed->m2,
						'f2'=>$basic_ed->f2,
						'total2'=>$dtotal2,
						'm3'=>$basic_ed->m3,
						'f3'=>$basic_ed->f3,
						'total3'=>$dtotal3,
						'm4'=>$basic_ed->m4,
						'f4'=>$basic_ed->f4,
						'total4'=>$dtotal4,
						'm5'=>$basic_ed->m5,
						'f5'=>$basic_ed->f5,
						'total5'=>$dtotal5,
						'm6'=>$basic_ed->m6,
						'f6'=>$basic_ed->f6,
						'total6'=>$dtotal6,
						'm7'=>$basic_ed->m7,
						'f7'=>$basic_ed->f7,
						'total7'=>$dtotal7,
						'm8'=>$basic_ed->m8,
						'f8'=>$basic_ed->f8,
						'total8'=>$dtotal8,
						'm9'=>$basic_ed->m9,
						'f9'=>$basic_ed->f9,
						'total9'=>$dtotal9,
						'm10'=>$basic_ed->m10,
						'f10'=>$basic_ed->f10,
						'total10'=>$dtotal10,
						'm11'=>$basic_ed->m11,
						'f11'=>$basic_ed->f11,
						'total11'=>$dtotal11,
						'm12'=>$basic_ed->m12,
						'f12'=>$basic_ed->f12,
						'total12'=>$dtotal12,
						'total_m'=>$tm,
						'total_f'=>$tf,
						'dept_total'=>$dt);
				
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, $students[$cnt]['level']);
		
				$x=1;
				
				for ($col = 1; $col < 37; ++$col) {
					$col1 = PHPExcel_Cell::stringFromColumnIndex($col);
					//print($col1.$num.' == '. $students[$cnt]['m'.$x].'<br>');
					$objPHPExcel->getActiveSheet()->SetCellValue($col1.$num, $students[$cnt]['m'.$x]);
					$n = $col;
					$col++;
					$col1 = PHPExcel_Cell::stringFromColumnIndex($col);
					//print($col1.$num.' == '.$students[$cnt]['f'.$x].'<br>');
					$objPHPExcel->getActiveSheet()->SetCellValue($col1.$num, $students[$cnt]['f'.$x]);
					$col++;
					$col1 = PHPExcel_Cell::stringFromColumnIndex($col);
					$n1 = PHPExcel_Cell::stringFromColumnIndex($n);
						
					$total = "=SUM(".$n1.$num.":".PHPExcel_Cell::stringFromColumnIndex($col-1).$num.")";
					//print(PHPExcel_Cell::stringFromColumnIndex($col).$num.' == '.$total.'<br>');
					$objPHPExcel->getActiveSheet()->SetCellValue(PHPExcel_Cell::stringFromColumnIndex($col).$num, $total);
					$x++;
				}
				
				//$col++;
				$total_m="=(B$num+E$num+H$num+K$num+N$num+Q$num+T$num+W$num+Z$num+AC$num+AF$num+AI$num)";
				$total_f="=(C$num+F$num+I$num+L$num+O$num+R$num+U$num+X$num+AA$num+AD$num+AG$num+AJ$num)";
				$dept_total = "=SUM(AL$num:AM$num)";
				$objPHPExcel->getActiveSheet()->SetCellValue(PHPExcel_Cell::stringFromColumnIndex($col).$num, $total_m);
				$col++;
				$objPHPExcel->getActiveSheet()->SetCellValue(PHPExcel_Cell::stringFromColumnIndex($col).$num, $total_f);
				$col++;
				$objPHPExcel->getActiveSheet()->SetCellValue(PHPExcel_Cell::stringFromColumnIndex($col).$num, $dept_total);
		
				$num++;
				$cnt++;
			}
			
			$students[] = array('id'=>0,
					'level'=>'Grand Total',
					'm1'=>$grand_total['m1'],
					'f1'=>$grand_total['f1'],
					'total1'=>$grand_total['total1'],
					'm2'=>$grand_total['m2'],
					'f2'=>$grand_total['f2'],
					'total2'=>$grand_total['total2'],
					'm3'=>$grand_total['m3'],
					'f3'=>$grand_total['f3'],
					'total3'=>$grand_total['total3'],
					'm4'=>$grand_total['m4'],
					'f4'=>$grand_total['f4'],
					'total4'=>$grand_total['total4'],
					'm5'=>$grand_total['m5'],
					'f5'=>$grand_total['f5'],
					'total5'=>$grand_total['total5'],
					'm6'=>$grand_total['m6'],
					'f6'=>$grand_total['f6'],
					'total6'=>$grand_total['total6'],
					'm7'=>$grand_total['m7'],
					'f7'=>$grand_total['f7'],
					'total7'=>$grand_total['total7'],
					'm8'=>$grand_total['m8'],
					'f8'=>$grand_total['f8'],
					'total8'=>$grand_total['total8'],
					'm9'=>$grand_total['m9'],
					'f9'=>$grand_total['f9'],
					'total9'=>$grand_total['total9'],
					'm10'=>$grand_total['m10'],
					'f10'=>$grand_total['f10'],
					'total10'=>$grand_total['total10'],
					'm11'=>$grand_total['m11'],
					'f11'=>$grand_total['f11'],
					'total11'=>$grand_total['total11'],
					'm12'=>$grand_total['m12'],
					'f12'=>$grand_total['f12'],
					'total12'=>$grand_total['total12'],
					'total_m'=>$grand_total['total_m'],
					'total_f'=>$grand_total['total_f'],
					'grand_total'=>$grand_total['grand_total']
			);
			
			$objPHPExcel->getActiveSheet()->getStyle('B5:AN'.$num)
			->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$objPHPExcel->getActiveSheet()->getStyle('A3:AN'.$num)->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->getStyle('B'.$num.':AN'.$num)
			->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$num.':AN'.$num)
			->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()->setRGB('EAEAEA');
		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$num.':AN'.$num)->getFont()->setBold(true);
		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$num)
			->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getRowDimension($num)->setRowHeight(21);
				
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, 'GRAND TOTAL :');
			for($cnt=1; $cnt<=39; $cnt++) {
				$objPHPExcel->getActiveSheet()->SetCellValue(PHPExcel_Cell::stringFromColumnIndex($cnt).$num, '=SUM('.PHPExcel_Cell::stringFromColumnIndex($cnt).'5:'.PHPExcel_Cell::stringFromColumnIndex($cnt).'10)');
			}
			$objWriter->save($filename,__FILE__);
			return $students;
		}
		
}
//end of student_model.php