<div class="row-fluid">
<?php if (isset($query)): ?>
	<h2 class="heading"><small>Search Result for:</small> <strong><?php echo $query; ?></strong>
<?php //if (isset($programs) && count($programs) > 0):?>
<?php if (isset($results) && count($results) > 0):?>
	<a href="download" class="btn btn-link pull-right" id="download_list"><i class="icon-download-alt"></i> <strong>Download</strong></a>
<?php else:?>
	<div class="btn btn-link pull-right" onclick="alert('No Result to download')"><i class="icon-download-alt"></i> <strong>Download</strong></div>
<?php endif; ?>
	</h2>
<?php else:?>
	<h2 class="heading">Student Search</h2>
<?php endif;?>
	<div id="search_meta" class="well clearfix">
		<div class="row-fluid">
<?php if (isset($start)): ?>	
			<div class="pull-left span2">Showing <?php echo $start; ?>-<?php echo $end; ?> of <?php echo $total; ?></div>
<?php endif; ?>
			<div class="pull-right span10">
				<form method="post" action="" id="search_by_program">
					<?php $this->common->hidden_input_nonce(FALSE); ?>
					<input type="hidden" name="what" value="search_by_program" />
					<input type="submit" value="Search" class="pull-right btn"/>
					<select id="year_level" name="year_level" class="pull-right" style="margin-right: 5px">
						<option value="0">-- Year Level --</option>
<?php for ($count = 1; $count<=5; $count++): ?>
						<option value="<?php echo $count; ?>" <?php if(isset($year_level) && $year_level==$count) echo ' selected="selected"'; ?>><?php echo $count; ?></option>
<?php endfor;?>
					</select>
					<select name="program" id="programs" class="pull-right" style="margin-right: 5px">
						<option value="0">-- Programs --</option>
<?php if (isset($programs) && count($programs) > 0): ?>
<?php foreach ($programs as $dprogram): ?>
						<option value="<?php echo $dprogram->id; ?>" <?php if(isset($program) && $program==$dprogram->id) echo ' selected="selected"'?>><?php echo $dprogram->abbreviation; ?></option>
<?php endforeach; ?>
<?php endif; ?>
					</select>
					<select name="college" id="colleges" class="pull-right" style="margin-right: 5px">
							<option value="0">-- Colleges --</option>
						<?php foreach ($colleges as $dcollege): ?>
							<option value="<?php echo $dcollege->id; ?>" <?php if(isset($college) && $college==$dcollege->id) echo ' selected="selected"'; ?>><?php echo $dcollege->name; ?></option>
						<?php endforeach; ?>
					</select>
					
					
				</form>
			</div>
		</div>
	</div>
	<div id="error_box" class="alert alert-error" style="display:none">
	</div>
<?php if (isset($pagination)) echo $pagination; ?>
	<div class="clearfix">
		<table class="table table-striped table-bordered mediaTable activeMediaTable" id="MediaTable-0">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="15%">Student ID</th>
					<th width="40%">Student Name</th>
					<th width="20%">Gender</th>
					<th width="20%">Course and Year Level</th>
				</tr>
			</thead>
			<tbody>
<?php if (isset($results) && count($results) > 0): $count = $start; ?>
<?php foreach ($results as $result): ?>
				<tr>
					<td style="text-align: center"><?php echo $count; ?></td>
					<td><a href="<?php echo site_url("{$role}/student/{$result->idno}"); ?>"><?php echo $result->idno; ?></a></td>
					<td><?php echo $result->fullname; ?></td>
					<td><?php echo $result->gender; ?></td>
					<td><?php echo $result->abbr . " " . $result->year_level; ?></td>
				</tr>
<?php $count++; endforeach; ?>
<?php else: ?>
				<tr><td colspan="5">No Results Found</td></tr>
<?php endif;?>
			</tbody>
		</table>
<?php if(isset($program)): ?>
		<form method="post" action="" id="search_by_program_page">
			<input type="hidden" name="what" value="search_by_program" />
			<input type="hidden" name="college" value="<?php echo $college; ?>" />
			<input type="hidden" name="program" value="<?php echo $program; ?>" />
			<input type="hidden" name="year_level" value="<?php echo $year_level; ?>" />
		</form>
		<form method="post" action="<?php echo site_url("{$role}/download_student_list_csv"); ?>" id="download_list_form">
			<input type="hidden" name="program" value="<?php echo $program; ?>" />
			<input type="hidden" name="year_level" value="<?php echo $year_level; ?>" />
		</form>
		<script>
		$('.pagination a').click(function(event){
			event.preventDefault(); 
			var page = $(this).attr('href').substring(1);
			
			$('<input>').attr({
			    type: 'hidden',
			    name: 'page',
			    value: page,
			}).appendTo('#search_by_program_page');
			$('#search_by_program_page').submit();
		});
		$('#download_list').click(function(event){
			event.preventDefault(); 
			
			$('#download_list_form').submit();
		});
	</script>
		</script>
<?php endif; ?>
	</div>
</div>
<script>
jQuery.validator.addMethod("notEqualto", function(value, element, param) {
	console.log(param);
	return this.optional(element) || value != param;
	}, "All search items must be set..."
);
	
$('#search_by_program').validate({
	onkeyup: false,
	errorClass: 'error',
	validClass: 'valid',
	rules: {
		year_level: { required: true, notEqualto: "0"},
		college: { required: true, notEqualto: "0"},
		program: { required: true, notEqualto: "0"}
	},
	
	errorPlacement: function(error, element) {
		$('#error_box').html(error);
		$('#error_box').show();
	}	
});
$('#colleges').change(function(){
	
	var college_id = $('#colleges option:selected').val();
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id,
		dataType: "json",
		success: function(data){
			var doptions = make_options(data);
			$('#programs').html(doptions);
		}	
	});	
});

function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}
</script>