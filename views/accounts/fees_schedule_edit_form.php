<?php

$sample_fees = array(
		//group
		'1'=>array(
				'name'		=>'tuition',
				'subgroups' 	=> array(
						//subgroup
						'100'=>array(
								'name'=>'tuition',
								'values'=>array(
										//years
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'130'=>array(
								'name'=>'tuition2',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						)
				)
		),
		'2'=>array(
				'name'=>'miscellaneous',
				'subgroups' => array(
						'101'=>array(
								'name'=>'misc1',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'102'=>array(
								'name'=>'misc2',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'103'=>array(
								'name'=>'misc3',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						)
				)
		),
		'3'=>array(
				'name'=>'other fees',
				'subgroups' => array(
						'101'=>array(
								'name'=>'other fees1',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'102'=>array(
								'name'=>'other fees2',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						),
						'103'=>array(
								'name'=>'other fees3',
								'values'=>array(
										1=>array(
											1=>100,
											2=>120,
											3=>150
										),
										2=>array(
											1=>100,
											2=>120,
											3=>150
										),
										3=>array(
											1=>100,
											2=>120,
											3=>150
										),
										4=>array(
											1=>100,
											2=>120,
											3=>150
										),
										5=>array(
											1=>100,
											2=>120,
											3=>150
										)
								)
						)
				)
		)
);

$fees = isset($fees) ? $fees : $sample_fees;

//$academic_programs_group = "COE";
$school_year = ($current_academic_year->end_year-1)."-".$current_academic_year->end_year;

$no_of_years = 0;
$no_of_terms = 0;

if(!empty($fees)){
	$years = array();
	$terms = array();
	foreach($fees as $key=>$val){
		foreach($val['subgroups'] as $sub_key=>$sub_val){
			foreach ($sub_val['values'] as $year=>$term_rates){
				$years[] = $year;
				foreach($term_rates as $term=>$rate){
					$terms[] = $term;
				}
			}
		}
	}
	
	$no_of_years = max($years);
	$no_of_terms = max($terms);
}

$terms = array(
		1=>'1st',
		2=>'2nd',
		3=>'Summer'
);

//<input type="text" name="rate[--subgroup id--][--year--][--term--]" value="" />

//print_r($fees);
?>
<h2 class="heading"><?php echo $academic_programs_group . " " . $school_year;?></h2>
<?php if(!empty($fees)): ?>
<table class="table">
	<thead>
		<tr>
			<th></th>
			<?php
				$year = 1;
				while($year <= $no_of_years):
			?>
			<th style="text-align:left; padding-left: 7%;">Year <?php echo $year++?></th>
			<?php endwhile?>
		</tr>
		<tr>
			<th></th>
			<?php
				$year = 1;
				while($year <= $no_of_years): $year++; $term = 1;
			?>
			<th>
				<?php while($term <= $no_of_terms): $term++; ?>
					<span class="span3" style="text-align: left; "><?php echo $terms[$term-1]?></span>
				<?php endwhile; ?>
			</th>
			<?php endwhile; ?>
		</tr>
	</thead>
	<tbody>
	<?php
		foreach($fees as $key=>$val):
	?>
		<tr>
			<td colspan="<?php echo $no_of_years+1?>"><?php echo $val['name'] ?></td>
		</tr>
		<?php foreach($val['subgroups'] as $subgroup_key => $child): ?>
		<tr>
			<td child-id="<?php echo $subgroup_key;?>">&nbsp;&nbsp;<?php echo $child['name']; ?></td>
			<?php
				$year = 1;
				while($year <= $no_of_years):
			?>
			<td>
				<?php
				if(isset($child["values"][$year])):
					$term = 1;
					while($term <= $no_of_terms):
				?>
				<input type="text" class="money" style="width: 4em;text-align:right;" name="rate[<?php echo $subgroup_key; ?>][<?php echo $year; ?>][<?php echo $term; ?>]" value="<?php echo (isset($child["values"][$year][$term]) ? $child["values"][$year][$term] : 0); ?>" />
				<?php
					$term++;
					endwhile;
				?>
			</td>
			<?php
				else:
					echo "0&nbsp0&nbsp0";
				endif;
				$year++;
				endwhile;
			?>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<?php endif; ?>