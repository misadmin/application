<?php 

$this->form_lib->set_id('programs');
//$this->form_lib->set_label('Step1: Create Program');
$this->form_lib->add_control_class('formSep');
$this->form_lib->set_attributes(array('method'=>'post','action'=>'#','class'=>'form-horizontal'));
$this->form_lib->enqueue_hidden_input('nonce',$this->common->nonce());
$this->form_lib->enqueue_text_input('abbreviations','Abbreviation','Abbreviation','',TRUE, 'input-small');
$this->form_lib->enqueue_text_input('description','Description','Description','',TRUE, 'input-xxlarge');
$this->form_lib->enqueue_select_input2('acad_program_groups_id','Group','id','{group_name}',$groups,'254','','span2');
$this->form_lib->set_submit_button('Create');
$this->form_lib->set_reset_button('Reset');
?>

<h2 class="heading"><span>Step1: Create Program</span><span style="color:#d4d4d4;margin-left:20px;font-size:0.9em">Step2: Prospectus</span></h2>
<?php $this->form_lib->content(FALSE);?>