<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>


<form action="<?php echo site_url('dean/add_blocksection');?>" method="post" class="form-horizontal">
<?php $this->common->hidden_input_nonce(FALSE); ?>
 	<input type="hidden" name="step" value="2" />

  <?php echo validation_errors(); 
  ?>

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; ">

<h2 class="heading">View Block Sections</h2>
<form class="form-horizontal">
  <div class="control-group ">
  <label class="control-label" for="academic-program">Academic Program&nbsp:&nbsp</label>
  <div class="controls">
	  <select name="academic_program" id="academic_program_id" class="span2">
    	<?php
				foreach($academic_program AS $prog) {
					print("<option value=".$prog->id.">".$prog->abbreviation." [ ".$prog->effective_year." ]</option>");	
				}
			?>
  </select>
	  </div>
	</div>
	
<div class="control-group formSep ">
  <label class="control-label" for="year-level">Year Level&nbsp:&nbsp</label>
  <div class="controls">
	<select name="yearlevel" id="yearlevel" class="span1">
    <option value="1" selected="selected">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
  </select>
	  </div>
	</div>
	<div class="controls">
	
	<button class="btn btn-success" type="submit">Continue!</button>
	</div>
</form>
 </div>
</form>
</form>

