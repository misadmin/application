<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function show_offering()
{
	show('offering');
	hide('program');
}
function show_program()
{
	hide('offering');
	show('program');
}
</script>

<body onLoad="show_offering();">

<div style="background:#FFF; width:50%; text-align:center; margin:auto;" align="center">
	<form id="view_balance_form" method="post" action="" class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="2" />
	<table height="" align="left" cellpadding="0" cellspacing="0" class="head1" style="width:100%; margin-top:10px;">
  		<tr class="head">
			<td colspan="3" class="head" style="text-align:center; padding:10px;">  View Balance of Students </td>
		</tr>
  		<tr>
			<td width="30%" align="left">College</td>
			<td width="2%" align="left">:</td>
			<td width="68%" align="left">
			<select id="colleges" name="colleges" class="form">
				<option value="">-- Select Program --</option>
			  <?php
					foreach($college AS $col) {
						print("<option value=".$col->id.">". $col->name. "</option>");	
					}  ?> </select> </td>
   	 	</tr>
  		<tr>
			<td width="30%" align="left">Program:</td>
			<td width="2%" align="left">:</td>
			<td width="68%" style="text-align:left;">
			<select id="programs" name="programs" class="form">
				<option value="">-- Select Program --</option>
			</select>  </td>
		</tr>
		<tr>
			<td width="30%" align="left">Year Level:</td>
			<td width="2%" align="left">:</td>
			<td width="68%" style="text-align:left;">
			<select id="year" name="year" class="form">
				<option value="1">First Year </option>
				<option value="2">Second Year </option>
				<option value="3">Third Year </option>
				<option value="4">Fourth Year </option>
				<option value="5">Fifth Year </option>
			</select>  </td>
		</tr>
		<tr align="center"> 
			<td colspan="3"> <button class="adjust" type="submit">View List</button> </td>
		</tr>
    </table>

 </form>
</div>
</body>

<script>
	
$('#colleges').change(function(){
	var college_id = $('#colleges option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
		dataType: "json",
		success: function(data){
			var doptions = make_options(data);
			$('#programs').html(doptions);
		}	
	});	
});

function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}
</script>






  