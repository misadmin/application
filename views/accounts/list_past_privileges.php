 
<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
  <tr>
  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">
    <strong><font face="Verdana, Arial, Helvetica, sans-serif"> PAST PRIVILEGES</font></strong>
   </td>
  </tr>
</table>	

<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; margin-top:10px;">
  <table width="75%" border="0" cellspacing="0" class="table table-bordered table-hover" style="width:90%; padding:0px; margin:10px;">
    <thead>
      <tr>
         <th width="15%" style="text-align:center;">Term/School Year</th>
         <th width="20%" style="text-align:center;">Privileges</th>
    	 <th width="15%" style="text-align:center;">Tuition Discount %</th>
		 <th width="15%" style="text-align:center;">Total Discount</th>
		 <th width="20%" style="text-align:center;">View Details</th>
       
       </tr>
    </thead>
    <?php
    
	if ($past_privileges) {
			$total = 0;
			foreach ($past_privileges AS $privilege) {
				$priv_details = $this->Scholarship_Model->ListPrivilegeAvailedDetails($privilege->id);
	?>
    <tr>
      <td style="text-align:left;"><?php 
     		 $start = $privilege->end_year-1;
          if($privilege->term == 1){
			print("1st Sem ".$start."-".$privilege->end_year);}
     	  else if($privilege->term == 2){
			print("2nd Sem ".$start."-".$privilege->end_year);}	
		  else{
			print("Summer ".$start."-".$privilege->end_year);}?>
	 </td>
      <td style="text-align:left;"><?php print($privilege->scholarship); ?></td>
	  <td style="text-align:right;"><?php print($privilege->discount_percentage * 100); ?></td>
	  <td style="text-align:right;"><?php print number_format($privilege->total_amount_availed,2); ?></td>
	  
      <td style="text-align:center; vertical-align:middle; ">
          <!--    <a class="view_privilege" href="<?php print($privilege->id); ?>"><i class="icon-adjust"></i></a> -->
   		 <a href="#" data-toggle="modal" data-target="#modal_id_delete_<?php print($privilege->id); ?>" 
				data-project-id=''><i class="icon-adjust"></i></a>
      
       <div class="modal hide fade" id="modal_id_delete_<?php print($privilege->id); ?>" >
 		<form method="post" id="delete_privilege_form">
   				<input type="hidden" name="action" value="delete_unposted_privilege" />
   				<input type="hidden" name="privilege_id" value="<?php print($privilege->id); ?>">
   		<div class="modal-header">
       		<button type="button" class="close" data-dismiss="modal">�</button>
       		<h3>Privilege Details</h3>
   		</div>
   		<div class="modal-body">
 			<div style="text-align:center;">
				<table border="0" cellpadding="2" cellspacing="0" 
					class="table table-striped table-condensed table-hover" style="width:100%;">
				 	<thead>
				  	<tr style="font-weight:bold; background:#EAEAEA;">
				    	<td width="50%" style="text-align:center; vertical-align:middle;">Description</td>
				    	<td width="25%" style="text-align:center; vertical-align:middle;">Percentage</td>
				    	<td width="25%" style="text-align:center;">Amount</td>
				    </tr>
					</thead>
					<?php 
						$total=0;
						foreach($priv_details AS $detail) {
							
					?>
					<tr>
					    <td style="text-align:left;"><?php print($detail->fees_group); ?></td>
					    <td style="text-align:center;"><?php print(($detail->discount_percentage*100)."%"); ?></td>
					    <td style="text-align:right;"><?php print(number_format($detail->discount_amount,2)); ?></td>
					</tr>
					<?php 
						$total += $detail->discount_amount;
						}
					?>	
					<thead style ="background-color: #F0F0F0;">
						<tr style ="font-weight: bold; font-size:14px;">
							<td colspan="2" style="text-align:right;">Total :</td>
							<td style="text-align:right;"><?php print(number_format($total,2)); ?></td>
						</tr>
					</thead>
				</table>
			</div>
   		</div>
	   <div class="modal-footer">
    		
	   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
       </div>
		</form>
    </div>
      
      </td>
    </tr>
    <?php
         
	  }?>
		
		 
	<?php }else{ ?>
		 <tr> 
		    <td colspan="6">No privileges yet</td>
		</tr>	
		<?php }   ?>
		
  </table>
</div>

<form id="view_priv" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="view_past_privilege_detail" />
		
	</form>			

<script>
$('.view_privilege').bind('click', function(event){
	event.preventDefault();
	
		var priv_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'priv_id',
		    value: priv_id,
		}).appendTo('#view_priv');
		$('#view_priv').submit();
			
});
</script>
