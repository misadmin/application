<?php if(!$past_privilege_availed){ ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
 		 <tr>
  				  <td style="text-align:center; font-weight:bold;">STUDENT HAS NO PRIVILEGES AVAILED</td>
 		 </tr>
	</table>
<?php }else{ ?>
		<div class="pull-right">
			<form method="post" id="schedule_current_term_form">
				<input type="hidden" value="schedule_current_term" name="action" />
				<?php $this->common->hidden_input_nonce(FALSE); ?>
		<?php if ($role != 'student' && isset($academic_terms) && !empty($academic_terms)): ?>
					<!-- <select name="academic_term" id="academic_term">
		<?php foreach ($academic_terms as $academic_term):?> 
						<option value="<?php echo $academic_term->id; ?>" <?php if($academic_term->id==$selected_term) echo 'selected="selected"'?>><?php echo $academic_term->term . " " . $academic_term->sy; ?></option>
		<?php endforeach; ?>
					</select> -->
		<?php endif; ?>
			</form>
			<script>
				$('#academic_term').bind('change', function(){
					$('#schedule_current_term_form').submit();
				});
			</script>
		</div>
		<div class="clearfix">
		<table width="297">
			<tr>
				<td width="70">Scholarship</td>
				<td width="5">:</td>
				<td width="90"><?php print($past_privilege_availed->scholarship); ?></td>
			</tr>	
		
		</table>
		
			<table class="table table-striped table-bordered" style="width:50%">
				<thead>
					<tr>
						<th width = "175">Discounted Items</th>
						<th width = "100">% Discount</th>
						<th>Amount</th>
					</tr>
				</thead>
			
			<tbody>
			<?php if($past_privilege_availed){
				$total = 0;
			 ?>
			
			<?php 
				if ($past_priv_details) {
					foreach($past_priv_details as $past_priv_detail){ ?>
						<tr>
							 <td width = "79"> <?php print($past_priv_detail->fees_group);?> </td>
							 <td> <?php print($past_priv_detail->discount_percentage * 100);?> </td>
							 <td> <?php print(number_format($past_priv_detail->discount_amount,2));
							 $total = $total  + $past_priv_detail->discount_amount; ?> </td>
						</tr>
			<?php 
					}
				}
			 ?>	
					 <tr>
						 <td>&nbsp;  </td>
						 <td> Total: </td>
						 <td> <?php print(number_format($total,2));?> </td>
					</tr>
			<?php } else{ ?>
					<tr>
						<td colspan="5">No Privileges Yet</td>
					</tr>
			<?php } ?>
			  </tbody>
		  </table>
		</div>
<?php } ?>	