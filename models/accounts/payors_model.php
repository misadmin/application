<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payors_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}
	
	public function add_new_payor($table, $payor_details){
		$this->db->insert($table, $payor_details);
		$insert_id = $this->db->insert_id();
		$this->db->insert('payers', array($this->payers_column($table) => $insert_id));
		return $insert_id;
	}
	
	public function edit_payor($id, $table, $payor_details){
		$this->db->where('id', $id);
		return $this->db->update($table, $payor_details);
	}
	
	public function search_payor($table, $query){
		$this->db->where($query);
		$query = $this->db->get($table);//get_where?
		if ($query && $query->num_rows() > 0)
			return $query->result(); else
			return FALSE;
	}
	
	public function all_non_student_payors (){
		$sql = "SELECT 
					payer_details.payers_id as account_number, 
					payer_details.name as account_name, 
					payer_details.tenant as tenant, 
					payer_details.type as type
				FROM
				((SELECT 
					payers.id as payers_id, 
					offices.office as name,
					'N/A' as tenant,
					'HNU Office' as type 
				FROM payers 
				LEFT JOIN offices 
				ON payers.inhouse_id=offices.id 
				WHERE NOT ISNULL(payers.inhouse_id)
				) UNION
				(SELECT
					payers.id as payers_id,
					other_payers.name as name,
					IF(other_payers.tenant='Y', 'Tenant', 'Non-Tenant') as tenant,
					'Other Payers' as type
				FROM payers
				LEFT JOIN other_payers
				ON payers.other_payers_id=other_payers.id
				WHERE NOT ISNULL(payers.other_payers_id)
				)) AS payer_details 
				ORDER BY `account_name`
				";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			return FALSE;
		}
	}
	
	public function account_details ($account_number){
	$sql = "
			SELECT payers.id as account_number, 
			IF(ISNULL(payers.inhouse_id), IF(ISNULL(payers.other_payers_id), '', other_payers.name), offices.office) AS account_name, 
			IF(ISNULL(payers.inhouse_id), IF(ISNULL(payers.other_payers_id), '', 'In House Payer'), 'Other Payers') AS type
			FROM
				payers
			LEFT JOIN
				offices
			ON 
				offices.id=payers.inhouse_id
			LEFT JOIN
				other_payers
			ON
				other_payers.id=payers.other_payers_id
			WHERE
				payers.id={$this->db->escape($account_number)}
			LIMIT
				1
			";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			if(empty($query->row()->account_name))
				return FALSE;
				return $query->row();
		} else {
			return FALSE;
		}
	}
	
	private function payers_column ($table){
		switch ($table){
			case 'other_payers':
				$column = 'other_payers_id';
				break;
			case 'offices':
				$column = 'inhouse_id';
				break;
			default:
				$column = FALSE;
				break;
		}
		return $column;
	}
}