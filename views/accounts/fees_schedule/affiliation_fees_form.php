
<div class="row-fluid" style="margin-top:20px;">
<h2 class="heading">Other Additional School Fees</h2>
	<div class="span12">
		<div class="form-horizontal">
			<form method="POST" id="affiliated_fees_form" name="MyForm" />
			<input type="hidden" name="action" value="submit_fee" />
			<input type="hidden" name="terms" value='<?php print(json_encode($terms)); ?>' />
				<?php $this->common->hidden_input_nonce(FALSE); ?>
			
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<select name="academic_year_id" id="academic_year_id">
						<?php
							foreach($school_years AS $school_year) {
						?>
							<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->school_year . " (". $school_year->status.")"); ?> </option>	
						<?php 
							}
						?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Description</div>
				<div class="controls">
					<input type="text" name="description" style="width:300px;" autofocus="autofocus" />									
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Rate</div>
				<div class="controls">
					<input type="text" name="rate">									
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<input type="submit" value="Record Fee!" class="btn btn-primary">									
				</div>
				
			</div>
			</form>
			</div>				
	</div>
</div>



<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'mainpage',
			}).appendTo('#affiliated_fees_form');
			$('#affiliated_fees_form').submit();
		});
	});
</script>

