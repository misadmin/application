<?php

	class Offerings_model extends CI_Model {
	
			
  		function __construct() {
        	parent::__construct();
		
   		}

		private function AddOffering($offering_data) 
		{
			if ($offering_data['employees_empno']) {
				$empno = $offering_data['employees_empno'];
			} else {
				$empno = "NULL";
			}
			
			$query = "INSERT INTO 
							course_offerings 
									(academic_terms_id, 
										courses_id, 
										employees_empno,
										section_code, 
										max_students, 
										additional_charge, 
										status, 
										inserted_by, 
										inserted_on) 
							VALUES ({$this->db->escape($offering_data['academic_terms_id'])},
									{$this->db->escape($offering_data['courses_id'])},
									".$empno.",
									{$this->db->escape($offering_data['section_code'])},
									{$this->db->escape($offering_data['max_students'])},
									{$this->db->escape($offering_data['additional_charge'])},
									'A',
									{$this->session->userdata('empno')},
									NOW() )";
			$this->db->query($query);

			//$id = @mysql_insert_id();
			$id = $this->db->insert_id();

			return $id;
			
		}

		
		//ADDED: 4/28/14 by genes
		function AddParallelOffering($offering_data) {
			
			$result = array();
						
			$result['course_offerings_id'] = $this->AddOffering($offering_data);
			
			if ($result['course_offerings_id']) {
				$result['added'] = TRUE;
			} else {
				$result['added'] = FALSE;
			}
			
			return $result;

		}

		
		/*function AddParallelOffering($offering_data) 
		{
			$query = "INSERT INTO 
							course_offerings 
									(id, 
										academic_terms_id, 
										courses_id, 
										employees_empno, 
										section_code, 
										max_students, 
										additional_charge, 
										status, 
										inserted_by, 
										inserted_on) 
							VALUES ('',
									'".$offering_data['academic_terms_id']."',
									'".$offering_data['courses_id']."',
									'".$offering_data['employees_empno']."',
									'".$offering_data['section_code']."',
									'".$offering_data['max_students']."',
									'".$offering_data['additional_charge']."',
									'A', 
									{$this->session->userdata('empno')},
									now()
									)";
	
			$this->db->query($query);
			$id = @mysql_insert_id();
			
			return $id;
			
		}*/


		function AddOfferingSlot($offering_data) {
			
			$this->load->model('hnumis/Rooms_Model');

			//list equivalent days for days_day_code
			$day_names = $this->Rooms_Model->ListDayNames($offering_data['days_day_code']); 
			
			foreach ($day_names AS $day) {
				$days[] = $day->day_name;
			}
			$d1 = json_encode($days);
			$offering_data['days'] = str_replace(array('"','[',']'),array("'","(",")"),$d1);

			
			if ($offering_data['check_conflict']) {
				$schedules=$this->ListConflictOfferings($offering_data);
				
				if ($schedules) {
					$my_result['conflict'] = true;
					$my_result['schedules'] = $schedules;
					return $my_result;
				} else {

					if ($offering_data['no_offering']) {
						$my_result['course_offerings_id'] = $offering_data['course_offerings_id'];
					} else {
						$my_result['course_offerings_id'] = $this->AddOffering($offering_data);
					}	
					
					if ($offering_data['sched_type'] == 'fixed') {
						$query = "INSERT INTO 
										course_offerings_slots 
												(course_offerings_id, 
												rooms_id, 
												days_day_code,	
												start_time,	
												end_time, 
												inserted_by, 
												inserted_on) 
										VALUES ('".$my_result['course_offerings_id']."',
												{$this->db->escape($offering_data['rooms_id'])},
												{$this->db->escape($offering_data['days_day_code'])},
												{$this->db->escape($offering_data['start_time'])},
												{$this->db->escape($offering_data['end_time'])},
												{$this->session->userdata('empno')},
												NOW() )";
					} else {
						$query = "INSERT INTO 
										course_offerings_slots 
												(course_offerings_id, 
												rooms_id, 
												days_day_code,	
												start_time,
												end_time,
												inserted_by, 
												inserted_on) 
										VALUES ('".$my_result['course_offerings_id']."',
												{$this->db->escape($offering_data['rooms_id'])},
												{$this->db->escape($offering_data['days_day_code'])},
												'',
												'',
												{$this->session->userdata('empno')},
												NOW() )";						
					}
					//die($query);
					$this->db->query($query);
					//$my_result['course_offerings_slots_id'] = @mysql_insert_id();
					$my_result['course_offerings_slots_id'] = $this->db->insert_id();
					
					$my_result['conflict'] = false;
				}
				
			} else { //this does not check for conflicts for parallel offerings

				if ($offering_data['no_offering']) {
					$my_result['course_offerings_id'] = $offering_data['course_offerings_id'];
				} else {
					$my_result['course_offerings_id'] = $this->AddOffering($offering_data);
				}
					
				if ($offering_data['sched_type'] == 'fixed') {
					$query = "INSERT INTO
									course_offerings_slots
											(course_offerings_id,
											rooms_id,
											days_day_code,
											start_time,
											end_time,
											inserted_by,
											inserted_on)
									VALUES ('".$my_result['course_offerings_id']."',
											{$this->db->escape($offering_data['rooms_id'])},
											{$this->db->escape($offering_data['days_day_code'])},
											{$this->db->escape($offering_data['start_time'])},
											{$this->db->escape($offering_data['end_time'])},
											{$this->session->userdata('empno')},
											NOW() )";
				} else {
					$query = "INSERT INTO
									course_offerings_slots
											(course_offerings_id,
											rooms_id,
											days_day_code,
											start_time,
											end_time,
											inserted_by,
											inserted_on)
									VALUES ('".$my_result['course_offerings_id']."',
											{$this->db->escape($offering_data['rooms_id'])},
											{$this->db->escape($offering_data['days_day_code'])},
											'',
											'',
											{$this->session->userdata('empno')},
											NOW() )";					
				}
												

				$this->db->query($query);
				//$my_result['course_offerings_slots_id'] = @mysql_insert_id();
				$my_result['course_offerings_slots_id'] = $this->db->insert_id();
																
				$my_result['conflict'] = false;
				
			}
			
			$offering_data['course_offerings_slots_id'] = $my_result['course_offerings_slots_id'];
			
			if (!$this->Rooms_Model->AddRoomOccupancy($offering_data)) {
				$my_result['rm_blocking'] = false;
			} else {
				$my_result['rm_blocking'] = true;
			}

			return $my_result;
			
		}

		private function ListConflictOfferings($offering_data) 
		{
			$result = null;
						
			$q = "SELECT 
						b.course_code,
						c.section_code,
						TIME_FORMAT(e.start_time,'%l:%i%p') AS start_time,
						TIME_FORMAT(e.end_time,'%l:%i%p') AS end_time,
						e.days_day_code,
						f.room_no
					FROM 
						course_offerings AS c, 
						room_occupancy AS d, 
						course_offerings_slots AS e,
						courses AS b,
						rooms AS f,
						room_occupancy_course_offerings_slots AS g
					WHERE
						c.id = e.course_offerings_id
						AND e.id = g.course_offerings_slots_id 
						AND g.room_occupancy_id = d.id 
						AND d.rooms_id = f.id
						AND b.id = c.courses_id
						AND d.academic_terms_id = {$this->db->escape($offering_data['academic_terms_id'])}
						AND (({$this->db->escape($offering_data['start_time'])} BETWEEN ADDTIME(d.from_time,'00:01') 
							AND SUBTIME(d.to_time,'00:01') 
							OR {$this->db->escape($offering_data['end_time'])} BETWEEN ADDTIME(d.from_time,'00:01') 
							AND ADDTIME(d.to_time,'00:01')) 
						AND d.day_name IN ".$offering_data['days'].")
						AND f.status = 'active'
						AND f.id = {$this->db->escape($offering_data['rooms_id'])}
					GROUP BY
						e.id ";
			
			//die($q); 
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function getLastSection($courses_id,$academic_terms_id) 
		{
			$result = null;
			
			$q = "SELECT a.section_code 
					FROM
					     course_offerings AS a
					WHERE 
					    a.courses_id = {$this->db->escape($courses_id)}
						AND a.academic_terms_id={$this->db->escape($academic_terms_id)} 
					ORDER 
						BY a.id DESC LIMIT 1";
			

			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}


		function getConflictEnrollment($students_idno,$academic_terms_id,$start_time,$end_time,$day_name) 
		{
			$result = null;
			
			$q = "SELECT c.id, a.academic_terms_id, a.courses_id, b.rooms_id, a.section_code, 
						b.start_time, b.end_time
					FROM course_offerings AS a, 
						course_offerings_slots AS b, 
						enrollments AS c, 
						student_histories AS d, 
						days AS e 
					WHERE
					 	a.id=b.course_offerings_id 
						AND a.id=c.course_offerings_id 
						AND c.student_history_id=d.id 
						AND b.days_day_code=e.day_code 
						AND a.academic_terms_id={$this->db->escape($academic_terms_id)} 
						AND (
							(
								{$this->db->escape($start_time)} >= b.start_time 
								AND {$this->db->escape($start_time)} < b.end_time
							) 
							OR 
							(
								{$this->db->escape($end_time)}  > b.start_time 
								AND {$this->db->escape($end_time)} < b.end_time)
							) 
						AND a.status='A' 
						AND d.students_idno={$this->db->escape($students_idno)} 
						AND e.day_name={$this->db->escape($day_name)}
						AND c.status='Active' 
					";	
			//print($q);
			//die();
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}

		
		/* UPDATED 3/8/13 By: genes
		 * include data for parallel offerings
		 * @UPDATED: 8/28/14 by: genes
		 * @NOTE: include paying_units
		*/
		function getOffering($id) 
		{
			$result = null;
			
			$q = "SELECT 
							a.id, 
							a.academic_terms_id, 
							a.courses_id, 
							a.section_code, 
							b.course_code,
							b.credit_units,
							b.paying_units,
							d.employees_empno,
							a.max_students,
							a.additional_charge,
							a.status,
							b.descriptive_title,
							UPPER(CONCAT(f.lname,', ',f.fname)) AS emp_name,
							g.parallel_no,
							if(a.additional_charge = 'Y', 'YES', 'NO') as additional_charge,
							(SELECT 
									COUNT(gg.id) AS num_parallel
								FROM 
									parallel_offerings AS gg
								WHERE 
									gg.parallel_no=g.parallel_no) AS cnt,
							 (SELECT 
									COUNT(xx.id)
								FROM
									course_offerings AS aa,
									enrollments AS xx,
									parallel_offerings AS yy
								WHERE 
									aa.id=xx.course_offerings_id
									AND aa.id=yy.course_offerings_id
									AND yy.parallel_no = g.parallel_no) AS enrollee_parallel, 
							 COUNT(x.id) AS enrollee
						FROM 
						    courses AS b, 
							course_offerings AS a LEFT JOIN col_faculty AS d ON a.employees_empno=d.employees_empno 
								LEFT JOIN employees AS f ON f.empno=d.employees_empno 
								LEFT JOIN parallel_offerings AS g ON a.id=g.course_offerings_id
								LEFT JOIN enrollments AS x ON a.id=x.course_offerings_id
						WHERE
						    a.courses_id=b.id 
							AND a.id={$this->db->escape($id)} 
						GROUP BY
							g.parallel_no ";
			
			//print($q);
			//die();
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}

	/**********************************************************************************************************/

		//UPDATED: 3/8/13 By: genes
		//include parallel offerings data
		//UPDATED: 5/18/13 By: genes
		//display all offerings if college id not specified
		//$code is used for searching a course_code
		function ListOfferings($academic_terms_id,$colleges_id=NULL,$code=NULL) {
			$result = null;
			
			$q = "SELECT a.id, 
						 a.academic_terms_id, 
						 a.courses_id, 
						 a.section_code, 
						 a.max_students, 
						 c.course_code,
						 c.credit_units,
						 c.paying_units,
						 c.descriptive_title,
						 d.employees_empno,
						 CONCAT(cap_first(f.lname),', ',cap_first(f.fname)) AS emp_name,
						 (SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa,
								enrollments AS xx,
								parallel_offerings AS yy
							WHERE 
								aa.id=xx.course_offerings_id
								AND aa.id=yy.course_offerings_id
								AND yy.parallel_no = y.parallel_no) AS enrollee_parallel, 
						 COUNT(x.id) AS enrollee,
						 IF(a.status='A','ACTIVE','DISSOLVED') AS status,
						 y.parallel_no 
					FROM 
						courses AS c,
						course_offerings AS a 
						LEFT JOIN col_faculty AS d ON a.employees_empno=d.employees_empno 
							LEFT JOIN employees AS f ON f.empno=d.employees_empno
							LEFT JOIN enrollments AS x ON a.id=x.course_offerings_id
							LEFT JOIN parallel_offerings AS y ON a.id=y.course_offerings_id
					WHERE 
						a.courses_id = c.id 
					  	AND	a.academic_terms_id={$this->db->escape($academic_terms_id)} ";
			
			if ($code) {
				$q .= " AND c.course_code LIKE '".$code."%' ";	
			}
			
			if ($colleges_id) {
				$q .= " AND	c.owner_colleges_id={$this->db->escape($colleges_id)} ";
			}
			
			$q .= "	GROUP BY
					  	a.id
					ORDER BY
						a.id desc /* recently created offering should be displayed on top most */ 			
				";				
			
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		//Added: April 3, 2012 by Amie
		
		function get_parallelno($offerings_id, $academic_term_id) {
			$result = null;
			
			$q = "SELECT po.parallel_no 
						FROM parallel_offerings AS po,
							 course_offerings AS co
					  WHERE po.course_offerings_id = co.id
					  		AND co.id = {$this->db->escape($offerings_id)}
							AND co.academic_terms_id = {$this->db->escape($academic_term_id)}";
		
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			return $result;
		}
	
		function ListParallel($parallelno) {
			$q = "SELECT course_offerings_id
						FROM 	parallel_offerings 
						WHERE	parallel_offerings.parallel_no = {$this->db->escape($parallelno)}";
			
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				return($query->result());
			} 
		}
	
		//Added: April 2, 2013 by Amie
		function ListAllOfferings($academic_terms_id) {
			$result = null;
			
			$q = "SELECT a.id, 
						 b.course_code, 
						 b.credit_units,
						 a.section_code,
						 UPPER(CONCAT(c.lname,', ',c.fname)) AS emp_name,
						 CONCAT(cs.start_time,'-',cs.end_time,' ',cs.days_day_code) AS timeslot
				   FROM 
						courses AS b,
						course_offerings AS a
						LEFT JOIN course_offerings_slots AS cs ON a.id = cs.course_offerings_id
						LEFT JOIN col_faculty AS d ON a.employees_empno=d.employees_empno 
						LEFT JOIN employees AS c ON c.empno=d.employees_empno
							
					WHERE 
						a.courses_id = b.id
						AND a.status = 'A' 
					  	AND	a.academic_terms_id={$this->db->escape($academic_terms_id)}
					GROUP BY 
						cs.course_offerings_id
					ORDER BY
						b.course_code, 
						a.section_code ";	
					
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function ListSlotsForOcccupancyRemoval($course_offerings_id) {
			$result = null;
				
			$q = "SELECT
						a.id
					FROM
						course_offerings_slots AS a
					WHERE
						a.course_offerings_id IN $course_offerings_id ";
		
			$query = $this->db->query($q);
			//print($q); die();
			if($query->num_rows() > 0){
			$result = $query->result();
			}
					
				return $result;
					
		}
		
		
		function ListOfferingSlots($course_offerings_id,$order_data=TRUE) 
		{
			$result = null;
			
			$q = "SELECT 
							a.id,a.course_offerings_id, 
							a.rooms_id, 
							b.room_no, 
							a.start_time, 
							a.end_time,
							if (HOUR(a.start_time) = 0, 'FLEXIBLE TIME',
								CONCAT(DATE_FORMAT(a.start_time,'%h:%i%p'),'-',DATE_FORMAT(a.end_time,'%h:%i%p'))) AS tym, 
							a.days_day_code,
							d.bldg_name,
							CASE c.day_name
								WHEN 'Mon' THEN 1
								WHEN 'Tue' THEN 2
								WHEN 'Wed' THEN 3
								WHEN 'Thu' THEN 4
								WHEN 'Fri' THEN 5
								WHEN 'Sat' THEN 6
								WHEN 'Sun' THEN 7
							END AS my_day,
							d.id AS old_bldgs_id
						FROM 
							course_offerings_slots AS a, 
							rooms AS b,
							days AS c,
							buildings AS d
						WHERE 
							a.rooms_id=b.id 
							AND a.days_day_code=c.day_code
							AND b.buildings_id=d.id
							AND a.course_offerings_id={$this->db->escape($course_offerings_id)} 
						GROUP BY 
							a.id ";
			
			if ($order_data) {
				$q = $q." ORDER BY my_day, a.start_time";
			} else {
				$q = $q." ORDER BY a.id";
			}
						
			//die($q);
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result[] = $query->result();
				$result[] = $query->num_rows(); //ADDED 7/15/13 for generation of PDF
				//$result = $query->result(); //edited 7/16/13 by kevin reason:error in all views that uses ListOfferingSlots() 
											//Noted: 7/17/13 by genes all modules accessing this method must be updated
			} 
					
			return $result;
			
		}


		function AssignFaculty($empno,$course_offerings_id) 
		{
			
			$q = "UPDATE course_offerings 
					SET 
						employees_empno={$this->db->escape($empno)},
						updated_by =  {$this->session->userdata('empno')},
						updated_on = now()
					WHERE
						id={$this->db->escape($course_offerings_id)} ";
			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

		/*
		 * @UPDATED: 8/13/14 by genes
		 */
		function DissolveOffering($course_offerings) {
			
			if ($course_offerings) {
				
				foreach ($course_offerings AS $k=>$v) {
					$q = "UPDATE 
								course_offerings 
							SET 
								status='D',
								updated_by =  {$this->session->userdata('empno')},
								updated_on = now()						
							WHERE
								id=$v";
					//print_r($q); die();
					if (!$this->db->query($q)) {
						return FALSE;
					}
				}
				return TRUE;
			} 	

			return FALSE;
		}

		
		function ListDays() {
			$result = null;
			
			$q = "SELECT day_code,day_name,
					CASE day_name
						WHEN 'Mon' THEN 1 
						WHEN 'Tue' THEN 2 
						WHEN 'Wed' THEN 3 
						WHEN 'Thu' THEN 4
						WHEN 'Fri' THEN 5 
						WHEN 'Sat' THEN 6 
						WHEN 'Sun' THEN 7 
					END AS day_num
					FROM days 
					WHERE status='active'
					GROUP BY day_code
					ORDER BY day_num";
			//print($q);
			//die();
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function getCourseOfferingInfo($course_offerings_id,$prospectus_id=NULL, $advised=NULL, $elective=NULL) {
			$result = null;
			//print("ADVISED: ".$advised);
			//print("<br>ELECTIVE: ".$elective);
			
			if ($advised == 'N' AND $elective == 'N') {
				$q = "SELECT e.credit_units, c.is_bracketed, c.num_retakes
						FROM 
							course_offerings AS a,
							prospectus_terms AS b,
							prospectus_courses AS c,
							courses AS e 
						WHERE 
							a.courses_id = c.courses_id
							AND c.courses_id=e.id 
							AND b.id=c.prospectus_terms_id
							AND a.id={$this->db->escape($course_offerings_id)} 
							AND b.prospectus_id={$this->db->escape($prospectus_id)} ";
			} elseif ($advised == 'N' AND $elective == 'Y' ) {
				$q = "SELECT 
							e.credit_units, 
							'N' AS is_bracketed, 
							NULL AS num_retakes,
							b.prospectus_courses_id
						FROM 
							course_offerings AS a,
							elective_courses AS b,
							courses AS e 
						WHERE
							a.courses_id = e.id
							AND b.topic_courses_id = e.id
							AND a.id={$this->db->escape($course_offerings_id)} ";
			} else {
				$q = "SELECT 
							e.credit_units, 
							'N' AS is_bracketed, 
							NULL AS num_retakes
						FROM 
							course_offerings AS a,
							advised_courses AS b,
							courses AS e 
						WHERE
							a.courses_id = e.id
							AND b.courses_id = e.id
							AND a.id={$this->db->escape($course_offerings_id)} ";
			}
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}

		//Added: 1/10/13
		function AssignBlockToOffering($blocksection, $offering) 
		{
			
			$q = "INSERT into block_course_offerings (`block_sections_id`, `course_offerings_id`, `inserted_by`, `inserted_on`) 					
					VALUES ('{$blocksection}',
						    '{$offering}',
							'{$this->session->userdata('empno')}',
							now()
							)";
			
			//print($q);
			//die();
			$this->db->query($q);
			$id = @mysql_insert_id();
			
			return $id;		
		}
		
		//Added: March 9, 2013 by Amie
		function allocate_offering($program_id, $year_level, $offering) 
		{		

			$q = "INSERT into allocations (academic_programs_id, yr_level, course_offerings_id, inserted_by, inserted_on)
					VALUES ('".$program_id."', 
							'".$year_level."',
						    '".$offering."',
							'{$this->session->userdata('empno')}',
							now()						    		
							)";

			$this->db->query($q);
			$id = @mysql_insert_id();
			
			return $id;		
		}
		
		
		function UnblockOffering($course_offerings_id) {
			$q = "DELETE FROM 
						block_course_offerings 
					WHERE
						course_offerings_id={$this->db->escape($course_offerings_id)} ";
			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

		
		function UnblockOfferings($course_offerings, $from_dean=FALSE) {
			//Tata introduced $from_dean variable on May 30, 2015
			//REASON: this function when called from dean $course_offerings is a json_encoded
			//        but when called by registrar, $course_offerings is an array
			
			
			if ($from_dean){
				
				$q = "
					delete from block_course_offerings
					where course_offerings_id in  $course_offerings
				";
				if (!$this->db->query($q)){
					return FALSE;
				}
				
			}else{
				
				if ($course_offerings) {
					foreach ($course_offerings AS $course_offering) {
						$q = "
								DELETE FROM
									block_course_offerings
								WHERE
									course_offerings_id={$this->db->escape($course_offering)}
						";					
						if (!$this->db->query($q)) {
							return FALSE;
						}		
					}
				} else {
					return FALSE;
				}
			}

			return TRUE;
		}
		
	 	function DeallocateOffering($alloc_id) {
			$q = "DELETE FROM 
						allocations
					WHERE
						id={$this->db->escape($alloc_id)} ";
			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
		function DeallocateOfferings($course_offerings) {
			$q = "DELETE FROM 
						allocations
					WHERE
						course_offerings_id IN $course_offerings ";
			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		
		}
		//Added: March 9, 2013 by Amie
		function ListAllocation($course_offerings_id) {
			$result = null;
			
			$q = "SELECT a.id,
						CONCAT(b.abbreviation,'-',a.yr_level) AS allocation_name
					FROM 
						allocations AS a,
						academic_programs AS b
					WHERE 
						a.academic_programs_id = b.id 
					  	AND	a.course_offerings_id={$this->db->escape($course_offerings_id)}
					ORDER BY
						allocation_name ";				
			
			$query = $this->db->query($q);
					
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
					
		}

		/*
		*	UPDATED: 11/10/15 by genes
		*	NOTE: query on enrollee is changed to SELECT
		*/
		function ListOfferingsThatCanBeTaken($academic_terms_id,$CoursesInProspectus,$AdvisedCourses,$CoursesInElective,$student) {
			$result = null;
			//print_r($CoursesInProspectus); die();
					
			$q = "(SELECT 
						a.id, 
						b.descriptive_title AS descriptive_title,
						b.course_code AS course_code, 
						a.section_code AS section_code, 
						a.max_students, 
						'N' AS advised, 
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa LEFT JOIN enrollments AS xx ON aa.id=xx.course_offerings_id
									LEFT JOIN parallel_offerings AS yy ON aa.id=yy.course_offerings_id 
							WHERE 
								yy.parallel_no = y.parallel_no
								AND aa.status='A') 
						AS enrollee_parallel,
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa,
								enrollments AS xx 
							WHERE 
								aa.status='A'
								AND aa.id=xx.course_offerings_id
								AND a.id=aa.id) 
						AS enrollee,
						-- COUNT(x.id) AS enrollee, 
						a.courses_id, 
						NULL AS mother_code,
						c.is_bracketed, 
						b.credit_units, 
						'N' AS elective,
						y.parallel_no
					FROM 
						courses AS b, 
						prospectus_courses AS c,
						prospectus_terms AS d,
						course_offerings AS a LEFT JOIN enrollments AS x ON a.id=x.course_offerings_id
							LEFT JOIN parallel_offerings AS y ON a.id=y.course_offerings_id
					WHERE
						a.courses_id = b.id 
						AND a.courses_id = c.courses_id
						AND c.prospectus_terms_id=d.id 
						AND d.prospectus_id={$this->db->escape($student['prospectus_id'])}
						AND a.academic_terms_id={$this->db->escape($academic_terms_id)} 
						AND a.courses_id IN $CoursesInProspectus
						AND a.status='A' 
						AND (a.id NOT IN (SELECT 
												x.course_offerings_id 
											FROM 
												allocations AS x)
								OR a.id IN (SELECT 
												x.course_offerings_id 
											FROM 
												allocations AS x
											WHERE 
												x.academic_programs_id = {$this->db->escape($student['academic_programs_id'])}
												AND x.yr_level = {$this->db->escape($student['year_level'])}
												)
								)
						AND a.id NOT IN (SELECT
												xx.course_offerings_id
											FROM
												block_course_offerings AS xx)
					GROUP BY
						a.courses_id, a.section_code 
					ORDER BY
						b.course_code, a.section_code)

				UNION

	(select co.id,c.descriptive_title,c.course_code,co.section_code,co.max_students,
		'Y' AS advised,sum(!isnull(po.id)) as enrollee_parallel, count(e.id) AS enrollee, co.courses_id, null AS mother_code,
		'N' AS is_bracketed, c.credit_units, 'Y' AS elective, po.parallel_no
		from courses c
		join course_offerings co on co.courses_id =c.id and co.`status`='A' 
		and co.courses_id in ('01338','01081') and co.academic_terms_id={$this->db->escape($academic_terms_id)}
		left join enrollments e on e.course_offerings_id = co.id
		left join parallel_offerings AS po ON co.id=po.course_offerings_id 
		group by co.id 
	)									
					UNION
				
				
				(SELECT 
						b.id,
						a.descriptive_title,
						a.course_code,
						b.section_code,
						b.max_students,
						'Y' AS advised,
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa LEFT JOIN enrollments AS xx ON aa.id=xx.course_offerings_id
									LEFT JOIN parallel_offerings AS yy ON aa.id=yy.course_offerings_id 
							WHERE 
								yy.parallel_no = y.parallel_no
								AND aa.status='A') 
						AS enrollee_parallel,
						(SELECT 
								COUNT(ee.id)
							FROM
								enrollments AS ee,
								course_offerings AS cc
							WHERE
								ee.course_offerings_id=cc.id
								AND cc.id=b.id
								AND cc.courses_id=b.courses_id
								AND cc.status = 'A'
								AND cc.academic_terms_id = {$this->db->escape($academic_terms_id)}
							GROUP BY
								cc.courses_id, cc.section_code)
						AS enrollee,
						b.courses_id,
						NULL AS mother_code,
						IF ((pc.is_bracketed = 'N' OR pc.is_bracketed IS NULL),'N','Y') AS is_bracketed,
						a.credit_units, 
						'N' AS elective,
						y.parallel_no
					FROM 
						courses AS a,
						course_offerings AS b LEFT JOIN advised_courses AS c ON b.courses_id=c.courses_id
						LEFT JOIN parallel_offerings AS y ON b.id=y.course_offerings_id 
						LEFT JOIN prospectus_courses AS pc ON pc.courses_id=b.courses_id
					WHERE 
						a.id = b.courses_id
						AND b.status = 'A'
						AND b.courses_id IN $AdvisedCourses
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND c.student_histories_id = {$this->db->escape($student['student_histories_id'])}
						AND (b.id IN (SELECT 
											x.course_offerings_id 
										FROM 
											allocations AS x
										WHERE 
											(x.academic_programs_id = {$this->db->escape($student['academic_programs_id'])}
											AND x.yr_level = {$this->db->escape($student['year_level'])}) 
									)
							OR (b.id NOT IN (SELECT
											xx.course_offerings_id
										FROM
										block_course_offerings AS xx)
							AND b.id NOT IN (SELECT
											xxx.course_offerings_id
										FROM
											allocations AS xxx)
											))
							

/*						AND (b.id NOT IN (SELECT 
											x.course_offerings_id 
										FROM 
											allocations AS x)
									OR b.id IN (SELECT 
											x.course_offerings_id 
										FROM 
											allocations AS x
										WHERE 
											x.academic_programs_id = {$this->db->escape($student['academic_programs_id'])}
											AND x.yr_level = {$this->db->escape($student['year_level'])})
								)						
								AND b.id NOT IN (SELECT
											xx.course_offerings_id
										FROM
										block_course_offerings AS xx)
*/
					GROUP BY
						b.courses_id,b.section_code)			
						
					UNION
					
					(SELECT 
						a.id, 
						b.descriptive_title AS descriptive_title,
						b.course_code AS course_code, 
						a.section_code AS section_code, 
						a.max_students, 
						'N' AS advised, 
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa LEFT JOIN enrollments AS xx ON aa.id=xx.course_offerings_id
								 LEFT JOIN parallel_offerings AS yy ON aa.id=yy.course_offerings_id
							WHERE 
								yy.parallel_no = y.parallel_no
								AND aa.status='A') AS enrollee_parallel,
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa,
								enrollments AS xx 
							WHERE 
								aa.status='A'
								AND aa.id=xx.course_offerings_id
								AND a.id=aa.id) 
						AS enrollee,
						-- COUNT(x.id) AS enrollee, 
						a.courses_id, 
						e.course_code AS mother_code,
						'N' AS is_bracketed, 
						b.credit_units, 
						'Y' AS elective, 
						y.parallel_no
					FROM 
						courses AS b, 
						elective_courses AS c,
						prospectus_courses AS d,
						courses AS e,
						course_offerings AS a LEFT JOIN enrollments AS x ON a.id=x.course_offerings_id 	
							LEFT JOIN parallel_offerings AS y ON a.id=y.course_offerings_id
					WHERE
						b.id = c.topic_courses_id
						AND d.id=c.prospectus_courses_id
						AND e.id=d.courses_id
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND c.prospectus_courses_id IN (SELECT
																aa.id
															FROM
																prospectus_courses AS aa,
																prospectus_terms AS bb
															WHERE
																aa.prospectus_terms_id = bb.id
																AND bb.prospectus_id = {$this->db->escape($student['prospectus_id'])})
																
						AND a.courses_id = b.id  
						AND a.status='A' 
						AND c.topic_courses_id IN $CoursesInElective
						AND (a.id NOT IN (SELECT 
												x.course_offerings_id 
											FROM 
												allocations AS x)
								OR a.id IN (SELECT 
												x.course_offerings_id 
											FROM 
												allocations AS x
											WHERE 
												x.academic_programs_id = {$this->db->escape($student['academic_programs_id'])}
												AND x.yr_level = {$this->db->escape($student['year_level'])}
												)
								)				
						AND a.id NOT IN (SELECT
												xx.course_offerings_id
											FROM
												block_course_offerings AS xx)
					GROUP BY
						a.courses_id, a.section_code)
						
				ORDER BY course_code, section_code  ";
			
			//print($q);die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}


		//Added 01/19/2013
		/*
		*	UPDATED: 11/10/15 by genes
		*	NOTE: query on enrollee is changed to SELECT
		*/
		function ListOfferingsThatCanBeTakenByBlock($academic_terms_id,$CoursesInProspectus,$AdvisedCourses,$CoursesInElective,$prospectus_id,$block_id) {
			$result = null;

			$q = "(SELECT 
						a.id, 
						b.descriptive_title AS descriptive_title,
						b.course_code AS course_code, 
						a.section_code AS section_code, 
						a.max_students, 
						'N' AS advised,
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa LEFT JOIN enrollments AS xx ON aa.id=xx.course_offerings_id
									LEFT JOIN parallel_offerings AS yy ON aa.id=yy.course_offerings_id 
							WHERE 
								yy.parallel_no = y.parallel_no
								AND aa.status='A') AS enrollee_parallel,
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa,
								enrollments AS xx 
							WHERE 
								aa.status='A'
								AND aa.id=xx.course_offerings_id
								AND a.id=aa.id) 
						AS enrollee,
						-- COUNT(x.id) AS enrollee, 
						a.courses_id, 
						NULL AS mother_code,
						c.is_bracketed, 
						b.credit_units, 
						'N' AS elective,
						y.parallel_no
					FROM 
						courses AS b, 
						prospectus_courses AS c,
						prospectus_terms AS d,
						block_course_offerings AS e,
						course_offerings AS a 
						LEFT JOIN enrollments AS x ON a.id=x.course_offerings_id 
						LEFT JOIN parallel_offerings AS y ON a.id=y.course_offerings_id
					WHERE
						a.courses_id = b.id 
						AND b.id=c.courses_id
						AND c.prospectus_terms_id=d.id
						AND e.course_offerings_id=a.id
						AND a.status='A' 
						AND d.prospectus_id={$this->db->escape($prospectus_id)}
						AND a.academic_terms_id={$this->db->escape($academic_terms_id)} 
						AND e.block_sections_id= {$this->db->escape($block_id)}
						AND a.courses_id IN $CoursesInProspectus 
					GROUP BY 
						a.courses_id, a.section_code 
					ORDER BY
						b.course_code, a.section_code)
					
					UNION 

	(select co.id,c.descriptive_title,c.course_code,co.section_code,co.max_students,
		'Y' AS advised,sum(!isnull(po.id)) as enrollee_parallel, count(e.id) AS enrollee, co.courses_id, null AS mother_code,
		'N' AS is_bracketed, c.credit_units, 'Y' AS elective, po.parallel_no
		from courses c
		join course_offerings co on co.courses_id =c.id and co.`status`='A' 
		and co.courses_id in ('01338','01081') and co.academic_terms_id={$this->db->escape($academic_terms_id)}
		left join enrollments e on e.course_offerings_id = co.id
		left join parallel_offerings AS po ON co.id=po.course_offerings_id 
		group by co.id 
	)									
					UNION
					
					(SELECT 
						a.id, 
						b.descriptive_title AS descriptive_title,
						b.course_code AS course_code, 
						a.section_code AS section_code, 
						a.max_students, 
						'Y' AS advised,
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa LEFT JOIN enrollments AS xx ON aa.id=xx.course_offerings_id
									LEFT JOIN parallel_offerings AS yy ON aa.id=yy.course_offerings_id 
							WHERE 
								yy.parallel_no = y.parallel_no
								AND aa.status='A') 
						AS enrollee_parallel,
						(SELECT 
								COUNT(ee.id)
							FROM
								enrollments AS ee,
								course_offerings AS cc
							WHERE
								ee.course_offerings_id=cc.id
								AND cc.id=b.id
								AND cc.courses_id=a.courses_id
								AND cc.status='A'
								AND cc.academic_terms_id = {$this->db->escape($academic_terms_id)}
							GROUP BY
								cc.courses_id, cc.section_code)
						AS enrollee,
						a.courses_id, 
						NULL AS mother_code,
						IF ((pc.is_bracketed = 'N' OR pc.is_bracketed IS NULL),'N','Y') AS is_bracketed, 
						b.credit_units, 
						'N' AS elective,
						y.parallel_no
					FROM 
						courses AS b, 
						course_offerings AS a LEFT JOIN advised_courses AS c ON c.courses_id=a.courses_id  	
							LEFT JOIN parallel_offerings AS y ON a.id=y.course_offerings_id
							LEFT JOIN prospectus_courses AS pc ON pc.courses_id=c.courses_id					
					WHERE
						b.id = c.courses_id 
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND a.courses_id = b.id
						AND a.status='A' 
						AND a.courses_id IN $AdvisedCourses
					GROUP BY 
						a.academic_terms_id,a.courses_id,a.section_code)
					
					UNION
					
					(SELECT 
						a.id, 
						b.descriptive_title AS descriptive_title,
						b.course_code AS course_code, 
						a.section_code AS section_code, 
						a.max_students, 
						'N' AS advised, 
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa LEFT JOIN enrollments AS xx ON aa.id=xx.course_offerings_id
								 LEFT JOIN parallel_offerings AS yy ON aa.id=yy.course_offerings_id
							WHERE 
								yy.parallel_no = y.parallel_no
								AND aa.status='A') AS enrollee_parallel,
						(SELECT 
						 		COUNT(xx.id)
							FROM
								course_offerings AS aa,
								enrollments AS xx 
							WHERE 
								aa.status='A'
								AND aa.id=xx.course_offerings_id
								AND a.id=aa.id) 
						AS enrollee,
						-- COUNT(x.id) AS enrollee, 
						a.courses_id, 
						e.course_code AS mother_code,
						'N' AS is_bracketed, 
						b.credit_units, 
						'Y' AS elective,
						y.parallel_no
					FROM 
						courses AS b, 
						elective_courses AS c,
						prospectus_courses AS d,
						courses AS e,
						block_course_offerings AS ee,
						course_offerings AS a LEFT JOIN enrollments AS x ON a.id=x.course_offerings_id 	
							LEFT JOIN parallel_offerings AS y ON a.id=y.course_offerings_id
					WHERE
						b.id = c.topic_courses_id
						AND d.id=c.prospectus_courses_id
						AND e.id=d.courses_id
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND a.courses_id = b.id  
						AND a.id=ee.course_offerings_id
						AND a.status='A' 
						AND c.topic_courses_id IN $CoursesInElective
						AND ee.block_sections_id={$this->db->escape($block_id)}
					GROUP BY
						a.courses_id, a.section_code)
					ORDER BY course_code, section_code  ";
					
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}
		
		// edited from query above to resolve case idno 6119219	
		// and co.courses_id in ('01338','01081') and co.academic_terms_id=460 
	

	function ListBlockOfferings($course_offerings_id) {
			$result = null;
			
			$q = "SELECT a.id,
						CONCAT(c.abbreviation,'-',b.section_name,'[',b.yr_level,']') AS block_name
					FROM 
						block_course_offerings AS a,
						block_sections AS b,
						academic_programs AS c
					WHERE 
						a.block_sections_id = b.id 
						AND b.academic_programs_id=c.id
					  	AND	a.course_offerings_id={$this->db->escape($course_offerings_id)}
					ORDER BY
						block_name ";				

			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
					
		}

		//ADDED: 3/8/2013 By: genes
		//use to updat max_students for parallel offerings
		function Update_MaxStudents($course_offerings_id, $max_students) {
			$q = "UPDATE course_offerings 
					SET 
						max_students={$this->db->escape($max_students)},
						updated_by =  {$this->session->userdata('empno')},
						updated_on = now()						
					WHERE
						id={$this->db->escape($course_offerings_id)} ";
			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}					
		}
		
		//ADDED: 3/8/2013 By: genes
		//use to add data to parallel_offerings table
		function AddParallelOfferings($course_offerings_id_1, $course_offerings_id_2) {
		
			$parallel_no = $this->getParallel_Num($course_offerings_id_1);
			
			//print_r($parallel_no); die();
			if ($parallel_no->new_offering == 'N') {
				$q2 = "INSERT INTO 
								parallel_offerings 
									(parallel_no, 
									course_offerings_id, 
									inserted_by, 
									inserted_on) 
							VALUES (
									'".$parallel_no->num."',
									'".$course_offerings_id_2."',
									'{$this->session->userdata('empno')}',
									now()											
									)";
				if ($this->db->query($q2)) {
					return TRUE;
				} else {
					return FALSE;
				}					
			} else {
				$q1 = "INSERT INTO 
								parallel_offerings 
									(id, 
									parallel_no, 
									course_offerings_id, 
									inserted_by, 
									inserted_on) 
							VALUES ('',
									'".$parallel_no->num."',
									'".$course_offerings_id_1."',
									'{$this->session->userdata('empno')}',
									now()											
									)";
				
				$q2 = "INSERT INTO 
								parallel_offerings 
									(id, 
									parallel_no, 
									course_offerings_id, 
									inserted_by, 
									inserted_on) 
							VALUES ('',
									'".$parallel_no->num."',
									'".$course_offerings_id_2."', 
									'{$this->session->userdata('empno')}',
									now()						    		
									)";
				if ($this->db->query($q1) AND $this->db->query($q2)) {
					return TRUE;
				} else {
					return FALSE;
				}					
			}
			
		}


		//ADDED: 3/8/2013 By: genes
		//check existence of data
		function getParallel_Num($course_offerings_id) 
		{
			$result = null;
			
			$q = "SELECT 
						a.parallel_no AS num,
						'N' AS new_offering
					FROM 
						parallel_offerings AS a
					WHERE
						a.course_offerings_id={$this->db->escape($course_offerings_id)} ";
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} else {
				$q1 = "SELECT 
						a.parallel_no+1 AS num,
						'Y' AS new_offering
					FROM 
						parallel_offerings AS a
					ORDER BY
						a.parallel_no DESC 
					LIMIT 1";
				
				$query = $this->db->query($q1);
				if($query->num_rows() > 0){
					$result = $query->row();
				} else {
					$q1 = "SELECT 1 AS num, 'Y' AS new_offering";	
					$query = $this->db->query($q1);
					$result = $query->row();
				}
			}
			
			
			return $result;
			
		}

		function ListParallel_Offerings($course_offerings_id, $parallel_no,$exclude=TRUE) {
			$result = null;
			
			if ($exclude) {
				$q = "SELECT 
							CONCAT(c.course_code,' [',a.section_code,']') AS course_section, 
							b.id, 
							a.id AS course_offerings_id,
							c.owner_colleges_id,
							c.course_code,
							a.section_code,
							c.descriptive_title,
							e.college_code,
							c.credit_units,
							f.id AS course_offerings_slots_id
						FROM 
							course_offerings AS a,
							parallel_offerings AS b,
							courses AS c,
							colleges AS e,
							course_offerings_slots AS f
						WHERE 
							a.id = b.course_offerings_id 
							AND a.courses_id=c.id
							AND c.owner_colleges_id=e.id 
							AND a.id = f.course_offerings_id
							AND	b.parallel_no={$this->db->escape($parallel_no)}
							AND b.course_offerings_id != {$this->db->escape($course_offerings_id)}
						ORDER BY
							c.course_code";	
			} else {
				$q = "SELECT 
							CONCAT(c.course_code,' [',a.section_code,']') AS course_section, 
							b.id, 
							a.id AS course_offerings_id,
							c.owner_colleges_id,
							c.course_code,
							a.section_code,
							c.descriptive_title,
							e.college_code,
							c.credit_units,
							f.id AS course_offerings_slots_id
						FROM 
							course_offerings AS a,
							parallel_offerings AS b,
							courses AS c,
							colleges AS e,
							course_offerings_slots AS f
						WHERE 
							a.id = b.course_offerings_id 
							AND a.courses_id=c.id
							AND c.owner_colleges_id=e.id
							AND a.id = f.course_offerings_id
							AND	b.parallel_no={$this->db->escape($parallel_no)}
						ORDER BY
							c.course_code";	
			}

			//print($q); die();
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
							
		}
		
		function DeleteParallelOffering($course_offerings,$cnt,$parallel_no) 
		{
			
			if ($cnt < 3) { 
				//if parallel_no is less than 3, delete all same parallel_no
				//so that NO record will remain without parallel
				$q = "DELETE FROM
							parallel_offerings
						WHERE
							parallel_no = {$this->db->escape($parallel_no)}";
							
			} else {
				if ($course_offerings) {
					foreach ($course_offerings AS $k=>$v) {
						$q = "DELETE FROM
									parallel_offerings
								WHERE
									course_offerings_id=$v";
						if (!$this->db->query($q)) {
							return FALSE;
						}
					}
				} else {
					return FALSE;
				}
			}			

			return TRUE;
		}
		
		
		function NotDissolved($course_offerings_id) {
			$result = null;
			
			$q = "SELECT 
						a.status 
					FROM
					     course_offerings AS a
					WHERE 
					    a.id = {$this->db->escape($course_offerings_id)} 
						AND a.status='A' ";
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
			
		}
	
		
		function DeleteRemainingParallel($parallel_no) {
			
			$q1 = "SELECT *
						FROM
							parallel_offerings
						WHERE
							parallel_no = {$this->db->escape($parallel_no)} ";

			$query = $this->db->query($q1);
			
			if($query AND $query->num_rows() == 1){
				$q = "DELETE FROM
						parallel_offerings
					WHERE
						parallel_no =  {$this->db->escape($parallel_no)}";
						
				if ($this->db->query($q)) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else {
				return TRUE;
			}

		}


		function DeleteCourseOffering($course_offerings_id) 
			{
				
				$q = "
						DELETE FROM
							course_offerings
						WHERE
							id = {$this->db->escape($course_offerings_id)}
				";
				
				//print($q);die();
				if ($this->db->query($q)) {
					return TRUE;
				} else {
					return FALSE;
				}			
			}

		function DeleteOfferings_Slot($course_offerings_slots_id) 
			{
				
				$q = "DELETE FROM
							course_offerings_slots
						WHERE
							id = {$this->db->escape($course_offerings_slots_id)} ";
						
				if ($this->db->query($q)) {
					return TRUE;
				} else {
					return FALSE;
				}			
			}
		
		function Update_AddCharge($course_offerings_id, $status) {
		 
				$q = "UPDATE course_offerings 
						SET 
							additional_charge={$this->db->escape($status)},
							updated_by =  {$this->session->userdata('empno')},
							updated_on = now()						
						WHERE
							id={$this->db->escape($course_offerings_id)} ";
						
				
				if ($this->db->query($q)) {
					return TRUE;
				} else {
					return FALSE;
				}					
		}
		
		//Added: May 11, 2013 by Amie
		//this will retrieve the grade submission dates of faculty members on a given term
		function get_submission_dates($faculty, $term) {
			$result = null;
				
			$q = "SELECT CONCAT(c.course_code,'[',co.section_code,']') as course_code, CONCAT(cos.start_time,'-',cos.end_time,' ',cos.days_day_code) as schedule, gsd.prelim_date, gsd.midterm_date, gsd.finals_date
						FROM courses c
						LEFT JOIN course_offerings as co ON co.courses_id = c.id
						LEFT JOIN course_offerings_slots as cos ON cos.course_offerings_id = co.id
						LEFT JOIN grade_submission_dates as gsd ON gsd.course_offerings_id = co.id
					WHERE 
						co.employees_empno = {$this->db->escape($faculty)}
						AND co.academic_terms_id={$this->db->escape($term)}
					ORDER BY c.course_code";				

				$query = $this->db->query($q);
				
				if($query->num_rows() > 0){
					$result = $query->result();
				} 
				
				return $result;
		}

	
		//ADDED: 10/4/13 by genes
		function getCourseOfferings_Slot($course_offerings_slots_id) {
			$result = null;
				
			$q = "SELECT
						a.id AS course_offerings_slots_id,
						DATE_FORMAT(a.start_time,'%H:%i %p') AS s_time,
						TIME_FORMAT(subtime(end_time,start_time),'%H:%i') AS num_hr,
						a.days_day_code,
						a.rooms_id,
						a.start_time,
						a.end_time,
						b.buildings_id
					FROM
						course_offerings_slots AS a,
						rooms AS b
					WHERE
						a.rooms_id=b.id
						AND a.id = {$this->db->escape($course_offerings_slots_id)}";
					
			$query = $this->db->query($q);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
		
			return $result;
		
		}
	
		//ADDED: 10/4/13 by genes
		/*UPDATED: 10/31/2015 by genes
		/ NOTE: query edited using condition AS id
		*/
		function UpdateCourseOfferings_Slot($data) {
			/*$q = "UPDATE course_offerings_slots
					SET
						rooms_id={$this->db->escape($data['rooms_id'])},
						days_day_code={$this->db->escape($data['days_day_code'])},
						start_time={$this->db->escape($data['start_time'])},
						end_time={$this->db->escape($data['end_time'])},
						updated_by =  {$this->session->userdata('empno')},
						updated_on = now()		
					WHERE
						rooms_id = {$this->db->escape($data['old_rooms_id'])}
						AND days_day_code={$this->db->escape($data['old_days_day_code'])}
						AND start_time={$this->db->escape($data['old_start_time'])} ";
			*/
			$q = "UPDATE course_offerings_slots
					SET
						rooms_id={$this->db->escape($data['rooms_id'])},
						days_day_code={$this->db->escape($data['days_day_code'])},
						start_time={$this->db->escape($data['start_time'])},
						end_time={$this->db->escape($data['end_time'])},
						updated_by =  {$this->session->userdata('empno')},
						updated_on = now()		
					WHERE
						id = {$this->db->escape($data['course_offerings_slots_id'])} ";

			//die($q);			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
					
		}
		
		
		function ListParallel_CourseOfferingsSlots($data) {
			$result = null;
			
			$q = "SELECT 
							b.course_offerings_id,
							a.parallel_no,
							b.id AS course_offerings_slots_id
						FROM 
							parallel_offerings AS a,
							course_offerings_slots AS b,
							course_offerings AS c
						WHERE 
							a.course_offerings_id = b.course_offerings_id 
							AND c.id = b.course_offerings_id 
							AND c.academic_terms_id = {$this->db->escape($data['academic_terms_id'])}
							AND b.rooms_id = {$this->db->escape($data['rooms_id'])}
							AND b.days_day_code = {$this->db->escape($data['days_day_code'])}
							AND b.start_time = {$this->db->escape($data['start_time'])}
							AND b.end_time = {$this->db->escape($data['end_time'])} ";				

			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
	
		
}

//end of Offerings_model.php
