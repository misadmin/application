
		<select name="acad_programs_id" id="acad_programs_id">
		<?php
			if ($programs) {
				foreach($programs AS $program) {
		?>			
					<option value="<?php print($program->id); ?>" 
						abbreviation="<?php print($program->abbreviation); ?>" 
						description="<?php print($program->description); ?>"
						acad_program_groups_id="<?php print($program->acad_program_groups_id); ?>"
						max_yr_level="<?php print($program->max_yr_level); ?>" >
						<?php print($program->abbreviation); ?>
					</option>
		<?php
				}
			}
		?>
		</select>
