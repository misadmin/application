
	<div class="row-fluid" >
		<div id="msg_header_assign" style="display:none;">
		</div>

		<div class="form-horizontal my_enrollment_status">
			<fieldset>
					<div class="control-group">
						<label for="grade_level" class="control-label">Current</label>
						<div class="controls">
							<input type="text" class="form-control" value="<?php print($enrollment_status); ?>" disabled />
						</div>
					</div>
					<div class="control-group">
						<label for="grade_level" class="control-label">Status</label>
						<div class="controls">
							<select id="enrollment_status" name="enrollment_status" class="form-control" >
								<option value="Unblock" <?php if ($enrollment_status == 'Block') print("selected"); ?>>Unblock</option>
								<option value="Block" <?php if ($enrollment_status == 'Unblock') print("selected"); ?>>Block</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<div class="controls" style="float:left; padding-right:10px;">
							<a href="#" class="btn btn-danger" name="click-status-button" id="click-status-button">Update!</a>
						</div>
						<div style="float:left; padding-right:8px; margin-top:5px;" id="show_click_icon">
						</div>		
					</div>
			</fieldset>
		</div>
	</div>

	<script>
		$('#click-status-button').click(function(e){
			e.preventDefault();		
			
			var enrollment_status = $("select#enrollment_status option:selected").attr('value');
			
			var confirmed = confirm('Update enrollment status?');

			if (confirmed){
			
				$('#show_click_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

				$.ajax({
					cache: false,
					type: 'POST',
					url: '<?php print(base_url($this->uri->segment(1)."/process_student_action")."/".$students_idno);?>',
					data: { "enrollment_status": enrollment_status,
							"action": 'update_enrollment_status'
						},
					dataType: 'json',
					success: function(response) {				

						$('.my_enrollment_status').html(response.output); 	

						$('#show_click_icon').html("");
						
						if (response.success) {
							$('#msg_header_assign').show();	
							$('#msg_header_assign').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Status successfully updated!</div>');
						}
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
					
				});
			}
		});

	</script>
