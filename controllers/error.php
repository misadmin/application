<?php

class Error extends CI_Controller {
	
	public function index($error_no = '404'){
		$errors = $this->config->item('errors');
		if (!array_key_exists($error_no, $errors))
			$error_no = '404';
		$message = explode('|',$errors[$error_no]);
		$message[] = $this->session->flashdata('error_message');
		//print_r($message);die();
		$error = array('code'=>$error_no,'message'=>$message);
		$this->load->view('error/error_view',$error);
	}
}

/* End of file error.php */
/* Location: ./application/controllers/error.php */