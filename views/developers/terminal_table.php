<div class="row-fluid" id="alert-container">
<?php if(count($this_terminal) > 0): ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		This Terminal is MARKED: <?php echo $this_terminal->name; ?>
	</div>
<?php endif;?>
</div>
<div class="row-fluid">
<table id="terminal_table" class="table table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>Terminal</th>
			<th>Details</th>
			<th>Control</th>
		</tr>
	</thead>
	<tbody>
<?php foreach($terminals as $key=>$terminal): ?>
		<tr class="terminal_row" data-id="<?php echo $terminal->id; ?>" >
			<td><?php echo ($key+1)?></td>
			<td><?php echo $terminal->name; ?></td>
			<td><?php echo $terminal->description; ?></td>
			<td style="text-align:center"><a href="#" class="edit_terminal" data-id="<?php echo $terminal->id; ?>" data-name="<?php echo $terminal->name; ?>" data-description="<?php echo $terminal->description; ?>" title="Edit this terminal"><i class="icon-edit"></i></a>&nbsp;<a href="#" class="delete_terminal" data-id="<?php echo $terminal->id; ?>" title="Delete this terminal"><i class="icon-remove"></i></a></td>
		</tr>
<?php endforeach; ?>
	</tbody>
</table>
</div>
<form method="post" action="<?php echo site_url('developer/handler'); ?>" id="terminal_hidden_form">
	<input type="hidden" name="action" value="" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
var last
$(document).ready(function(){
	$(document).on('click', '.delete_terminal', function(){
		var to_delete = confirm("Are you sure you want to Remove this terminal?");
		if(to_delete){
			var id=$(this).attr('data-id');
			$('#terminal_hidden_form input[name="action"]').attr('value','delete');
			$('#terminal_hidden_form').append('<input type="hidden" name="id" value="'+id+'" />');
			$.ajax({
				url: $('#terminal_hidden_form').attr('action'),
				type: 'post',
				dataType: 'json',
				data: $('#terminal_hidden_form').serialize(),
				success:function(result){
					console.log(result);
					if(result.success){
						$.gritter.add({
		                    title:	'Success',
		                    text:	result.message,
		                    sticky: false,
		                    time: 2000
		                });
						$('#terminal_table tbody tr[data-id="'+id+'"]').hide('slow', function(){$('#terminal_table tbody tr[data-id="'+id+'"]').remove(); });
						$('input[name="nonce"]').val(result.nonce);
						if(terminal_id==id){
							$('#alert-container').hide('slow', function(){ $('#alert-container').html('')});
							$('#modal_editor_switch').show('slow');
							$('#unmark_switch').hide('slow');
						}
						$('#terminal_hidden_form [name="id"]').remove();
					} else {
						$.gritter.add({
		                    title:	'Error',
		                    text:	result.message,
		                    sticky: false,
		                    time: 2000
		                });
						$('input[name="nonce"]').val(result.nonce);
					}
				},
				error: function(){
					$.gritter.add({
	                    title:	'Error',
	                    text:	'Unknown error found',
	                    sticky: false,
	                    time: 2000
	                });
				} 
			}); 			
			
		} else {
			console.log("NOT deleting");
		}
	});
	$(document).on('click', '.edit_terminal', function(){
		$('#mark_terminal_form input[name="action"]').val('edit');
		$('#mark_terminal_form input[name="id"]').val($(this).attr('data-id'));
		$('#mark_terminal_form input[name="terminal_name"]').val($(this).attr('data-name'));
		$('#mark_terminal_form [name="terminal_details"]').val($(this).attr('data-description'));
		$('#mark_button').html('Update Terminal');
		$('#modal_editor').modal('show');
	});
	
	$(document).on('click', '#unmark_switch', function(){
		var to_unmark = confirm("Are you sure you want to Unmark this terminal?");
		if(to_unmark){
			var id=terminal_id;
			$('#terminal_hidden_form input[name="action"]').attr('value','delete');
			$('#terminal_hidden_form').append('<input type="hidden" name="id" value="'+id+'" />');
			$.ajax({
				url: $('#terminal_hidden_form').attr('action'),
				type: 'post',
				dataType: 'json',
				data: $('#terminal_hidden_form').serialize(),
				success:function(result){
					if(result.success){
						$.gritter.add({
		                    title:	'Success',
		                    text:	result.message,
		                    sticky: false,
		                    time: 2000
		                });
						$('#terminal_table tbody tr[data-id="'+id+'"]').hide('slow', function(){$('#terminal_table tbody tr[data-id="'+id+'"]').remove(); });
						$('input[name="nonce"]').val(result.nonce);
						if(terminal_id==id){
							$('#alert-container').hide('slow', function(){ $('#alert-container').html('')});
							$('#modal_editor_switch').show('slow');
							$('#unmark_switch').hide('slow');
						}
						$('#terminal_hidden_form [name="id"]').remove();
					} else {
						$.gritter.add({
		                    title:	'Error',
		                    text:	result.message,
		                    sticky: false,
		                    time: 2000
		                });
						$('input[name="nonce"]').val(result.nonce);
					}
				},
				error: function(){
					$.gritter.add({
	                    title:	'Error',
	                    text:	'Unknown error found',
	                    sticky: false,
	                    time: 2000
	                });
				} 
			}); 			
			
		}
	});
});
</script>