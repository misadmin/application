<?php

	class Withdrawal_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


    	
    function insertNewWithdrawalSked($data) 
		{
		/*	if($data['term'] != 1){ *
				$query = "INSERT INTO withdrawal_schedule (id, academic_years_id, terms_id, term_id, levels_id, date_start, date_end, 
							percentage) 
   							VALUES ('',
						   	   	 {$this->db->escape($data['school_year_id'])},
								 {$this->db->escape($data['term'])},
								 {$this->db->escape($data['term_id'])},
								 {$this->db->escape($data['level_id'])},
								 {$this->db->escape($data['start_d'])},
								 {$this->db->escape($data['end_d'])},
								 {$this->db->escape($data['percentage'])}
								 )";
			}else {*/
			$query = "INSERT INTO withdrawal_schedule (id, academic_terms_id, terms_id, levels_id, date_start, date_end, 
							percentage) 
   							VALUES ('',
						   	   	 {$this->db->escape($data['academic_terms_id'])},
								 {$this->db->escape($data['term_id'])},
								 {$this->db->escape($data['level_id'])},
								 {$this->db->escape($data['start_d'])},
								 {$this->db->escape($data['end_d'])},
								 {$this->db->escape($data['percentage'])}
				
								 )";
		 
			$this->db->query($query);
			$id = @mysql_insert_id();
			
			return $id;
			
		}
		
		
		function listWithdrawalSkeds($data) {
			$result = 0;
			$q = "SELECT a.id, 
						 b.terms_name,
						 c.level,
						 a.date_start,
						 a.date_end,
						 a.percentage * 100 as percent,
						 CASE d.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
						END AS term, 
							CONCAT(e.end_year-1,'-',e.end_year) AS sy
					FROM
						withdrawal_schedule as a,
						terms as b, 
						levels as c,
						academic_terms as d,
						academic_years as e
					WHERE
						a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])}
						AND a.terms_id = {$this->db->escape($data['term_id'])}
						AND a.levels_id = {$this->db->escape($data['level_id'])}
						AND b.id = a.terms_id
						AND c.id = a.levels_id
						AND d.id = a.academic_terms_id
						AND e.id = d.academic_years_id
					ORDER BY
					    a.date_start";	 
			
			//print_r($q);
			//die();
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		} 
		
		function getWithdrawalSked($withdraw_sked_id) {
			$result = 0;
			$q = "SELECT a.id, 
						 b.terms_name,
						 c.level,
						 a.date_start,
						 a.date_end,
						 a.percentage * 100 as percent,
						 CASE d.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
						END AS term, 
							CONCAT(e.end_year-1,'-',e.end_year) AS sy
					FROM
						withdrawal_schedule as a,
						terms as b, 
						levels as c,
						academic_terms as d,
						academic_years as e
					WHERE
						a.id = {$this->db->escape($withdraw_sked_id)}
						AND b.id = a.terms_id
						AND c.id = a.levels_id
						AND d.id = a.academic_terms_id
						AND e.id = d.academic_years_id ";	 
	
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		
		} 
		
		function updateWithdrawalSked($data){
		
		$q = "UPDATE withdrawal_schedule 
			SET 
				date_start ={$this->db->escape($data['start_d'])},
				date_end ={$this->db->escape($data['end_d'])},
				percentage ={$this->db->escape($data['percentage'])}
			WHERE
				id={$this->db->escape($data['withdraw_sked_id'])}";
			
	//print_r($q);
	//die();
		if ($this->db->query($q)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
	
	//Added 5/2/2013
	
	function getWithdrawalPercentage($academic_term_id) {
			$result = 0;
			$q = "SELECT a.percentage, a.date_start, a.date_end 
						
					FROM
						withdrawal_schedule as a
						
					WHERE
						 Date(NOW()) >= a.date_start
                    	    AND Date(NOW()) <= a.date_end
						 AND a.academic_terms_id = {$this->db->escape($academic_term_id)}";	 
			
		
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		
	}
	
	//Added: 8/27/2013
	
	function deleteWithdrawSked($withdraw_sked_id) {
	$q1 = "DELETE FROM withdrawal_schedule
 	       WHERE id={$this->db->escape($withdraw_sked_id)} ";
	
		if ($this->db->query($q1)) {
		   return TRUE;
		} else {
		   return FALSE;
		}

	}
}

//end of withdrawal_model.php