<div class="row-fluid">
<?php 

	$this->form_lib->set_attributes(array('class'=>'form-vertical', 'method'=>'post'));
	$this->form_lib->enqueue_hidden_input('nonce', $this->common->nonce());
	$this->form_lib->enqueue_hidden_input('action', 'new_feedback');
	$this->form_lib->enqueue_textarea(
		array(
			'name'=>'comment',
			'label'=>'Write down the details of your feedback:',
			'rules'=>array('required','minlength:3'),
			'class'=>'input-xlarge' // modified: xlarge -> large, ra, 8/17/13
		)
	);
	
	$this->form_lib->enqueue_select_input('recipient_id',$recipients,'Send feedback to:');
	
	$this->form_lib->set_submit_button('Submit','btn btn-primary');
	$this->form_lib->content(FALSE);
?>
</div>