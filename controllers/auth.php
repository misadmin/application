<?php 

class Auth extends CI_Controller {
	private $role;
	
	public function __construct(){
		parent::__construct();
		$this->role = $this->uri->segment(1);
		$this->content_lib->set_title('Login Form | ' . $this->config->item('application_title'));
		$this->content_lib->set_html_class('login_page');
		
		$this->referrer = $this->session->flashdata('referrer');
		if (!$this->referrer)
			$this->referrer = $this->input->get_post('referrer');
	}
	
	public function index(){
		$tries = $this->input->cookie("tries", 0);
		if ( $tries >= ($this->config->item('max_login_tries'))) {
			$this->content_lib->set_main_container_view();
			$this->content_lib->enqueue_body_content('auth/login_max_tries');
			$this->content_lib->content();
			return TRUE;
		}
		$cookie = array(
				'name'   => 'tries',
				'value'  => $tries,
				'expire' => 60*60,
		);
		$this->input->set_cookie($cookie);
		$this->show_login_form();
	}
	
	public function login(){
		
		$userid = $this->input->post('username');
		$password = $this->input->post('password');
		//server side validation:
		
		$error = array();
		//if ( ! is_numeric(ltrim($userid, '0')))
		if ( ! is_numeric($userid))
			$error[] = 'Username must be numeric';
		
		if (strlen($userid) > $this->config->item('username_length'))
			$error[] = 'Username is longer than allowed';
		
		$password_filter = "#[' \-]#";
		
		if (preg_match($password_filter, $password))
			$error[] = 'Password Filter Failed';
		
		$tries = $this->input->cookie('tries');
		
		if (count($error) == 0) {
			switch ($this->role) {
				case 'student' :
						$this->load->model ('student_common_model');
						$info = $this->student_common_model->user_is_valid ($userid, $password);
						break;
				default :
					
						$this->load->model ('employee_common_model');
						$info = $this->employee_common_model->user_is_valid($userid, $password, $this->role);
						if (($userid >= 20000) &&  !in_array($this->role, $this->config->item('roles_for_students')))
							$info = false;
						break;
			}
		} else {
			$info = FALSE;
		}

		// added by Tatskie on 2015-02-16 as requested by Rey to enhance app's security 
		$intranet_only_error = false;
		if  ((in_array($this->role, $this->config->item('roles_barred_from_internet_access'))) && !ip_is_private($this->input->ip_address()) ) {
			$intranet_only_error = true;
			$info = FALSE;			
		}
		
		if ( $info ){
			$this->session->set_userdata( $info );
			$this->session->set_userdata( 'logged_in', TRUE);
						
			$this->load->model ('common_model');
			$this->common_model->user_logs($userid, $this->role, $this->session->userdata('session_id'), true);
			
			$cookie = array(
					'name'   => 'tries',
					'value'  => 0,
			);			
			$this->input->set_cookie($cookie);
			if ($this->config->item('return_to_referrer') AND $this->referrer)
				redirect($this->referrer);
			else
				redirect($this->role);
		} else {
			//Lets check if user is over the limit...
			if ( $tries >= ($this->config->item('max_login_tries'))) {
				$data = array (
						'role'	=>$this->role,
				);
				
				
				$this->content_lib->set_main_container_view();
				$this->content_lib->enqueue_body_content('auth/login_max_tries');
				$this->content_lib->content();
				return TRUE;
			}
			$cookie = array(
					'name'   => 'tries',
					'value'  => $tries + 1,
					'expire' => 60*60,
			);
			
			$this->input->set_cookie($cookie);
			if ($intranet_only_error)
				$this->show_login_form('You seem to login outside from HNU\'s intranet!', 'error');
			else
				$this->show_login_form('Error Authenticating your ID and password', 'error');
			
		}
		
	}
	
	public function logout(){		
		$this->load->model ('common_model');		
		$this->common_model->user_logs($this->session->userdata('idno'), $this->role, $this->session->userdata('session_id'), false);		
		
		$this->session->sess_destroy();
		$this->show_login_form ( 'You are successfully logged out of HNU-MIS.', 'success' );
	}
	
	private function show_login_form( $message="", $severity="" ){
		//severity: error, success
		$data = array (
				'role'	=>$this->role,
		);
		if ( !empty($message)) {
			$data['message'] = $message;
			$data['severity'] = $severity;
		}
		//keep referrer address to redirect after logged in
		$data['referrer'] = $this->referrer;
		$this->content_lib->set_main_container_view();//lets not have a main container view...
		$this->content_lib->enqueue_body_content( 'auth/login_form', $data );
		$this->content_lib->content();
	}



}