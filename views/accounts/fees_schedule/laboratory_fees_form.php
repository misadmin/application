
<div class="row-fluid" style="margin-top:20px;">
<h2 class="heading">Laboratory Fees</h2>
	<div class="span12">
		<div class="form-horizontal">
			<form method="POST" id="change_academic_year_form">
			<input type="hidden" name="step" value="2">
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<select name="academic_year_id" id="academic_year_id">
						<?php
							foreach($school_years AS $school_year) {
						?>
							<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->school_year . " (". $school_year->status.")"); ?> </option>	
						<?php 
							}
						?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"> </div>
				<div class="controls" style="border-radius: 5px; border:solid; border-width:1px; border-color: #D8D8D8; width: 30%; ">
					<?php
						if ($laboratories) {
					?>
						<table class="table table-striped" >
							<thead>
								<tr style="background-color: #EAEAEA; font-weight: bold; ">
									<td style="width: 60%; text-align: left;">COURSE CODE</td>
									<td style="width: 40%; text-align: left;">RATE</td>
								<tr>
								
							</thead>
						<?php 
							foreach($laboratories AS $lab) {
						?>
							<tr>
								<td><?php print($lab->course_code); ?></td>
								<td style="text-align: right;">
									<input type="text" name="laboratory[<?php print($lab->id); ?>]">
								</td>
							</tr>
						
						<?php 	
								}
						?>
						
						</table>
						<?php 
							}
						?>					
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<input type="submit" value="Proceed to Review Form >>" class="btn btn-primary">									
				</div>
				
			</div>
			</form>
			</div>				
	</div>
</div>


<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 1,
			}).appendTo('#change_academic_year_form');
			$('#change_academic_year_form').submit();
		});
	});
</script>
