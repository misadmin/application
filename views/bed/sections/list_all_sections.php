
			<div style="padding:3px; overflow:auto;">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:15%;">Level</td>
								<td class="shs_header" style="width:32%;">Year Level - Section</td>
								<td class="shs_header" style="width:23%;">Class Adviser</td>
								<td class="shs_header" style="width:5%;">Room</td>
								<td class="shs_header" style="width:5%;">Download<br>Classlist</td>
								<td class="shs_header" style="width:5%;">Report<br>Card</td>
								<td class="shs_header" style="width:5%;">Enrolled<br>Students</td>
								<td class="shs_header" style="width:5%;">Section<br>Details</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($sections) {
									foreach($sections AS $section) {
										if ($section->strand_status == 'O') {
											$bg = "#FFFFFF";
										} else {
											$bg = "#FF9C9C";
										}
							?>
							<tr>
									<td style="background-color:<?php print($bg); ?>; "><?php print($section->group_name); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($section->description.' - '.$section->section_name); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($section->class_adviser); ?></td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;"><?php print($section->room_no); ?></td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;">
										<a href="#" 
											grade_section="<?php print('Grade '.$section->yr_level.'-'.$section->section_name); ?>"
											strand="<?php print($section->description); ?>"
											class_adviser="<?php print($section->class_adviser); ?>"
											room_no="<?php print($section->room_no); ?>"
											section_id="<?php print($section->id); ?>"
											abbreviation="<?php print($section->abbreviation); ?>"
											class="download_classlist_pdf">
											<img src="<?php print(base_url('assets/images/pdf_icon.png')); ?>" style="height:18px;" /> 
										</a>
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;">
										<!-- <a href="#" 
											grade_section="<?php //print('Grade '.$section->yr_level.'-'.$section->section_name); ?>"
											strand="<?php //print($section->description); ?>"
											class_adviser="<?php //print($section->class_adviser); ?>"
											room_no="<?php //print($section->room_no); ?>"
											section_id="<?php //print($section->id); ?>"
											abbreviation="<?php //print($section->abbreviation); ?>"
											class="download_report_card_pdf">
											<img src="<?php //print(base_url('assets/images/pdf_icon.png')); ?>" style="height:18px;" /> 
										</a> -->
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;"><?php print($section->num_students); ?></td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;">
										<?php 
											if ($section->strand_status == 'O') {
										?>
											<a href="<?php print(site_url($this->uri->segment(1).'/detailed_section_management').'/'.$section->id);?>" >
												<i class="icon-info-sign"></i>
											</a>
										<?php 
											}
										?>
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>;">
										<?php 
											if ($section->strand_status == 'O') {
										?>
											<a href="<?php print($section->id); ?>" class="del_section" room_no="<?php print($section->room_no); ?>">
												<div id="show_trash_icon_<?php print($section->id); ?>" >
													<i class="icon-trash"></i>
												</div>
											</a>
										<?php 
											}
										?>
									</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>


	<script>
		$('.del_section').click(function(event){
			event.preventDefault(); 
			
			var confirmed = confirm('Are you sure you want to delete this section?');

			if (confirmed){
				
				var acad_program_groups_id 	= $("select#acad_program_groups_id option:selected").attr('value');
				var academic_years_id		= $("select#academic_years_id option:selected").attr('value');
				var room_no 				= $(this).attr('room_no');				
				var section_id 				= $(this).attr('href');

				$('#show_trash_icon_'+section_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
				
				$.ajax({
					cache: false,
					type: 'POST',
					url: '<?php echo site_url($this->uri->segment(1).'/section_management');?>',
					data: { "acad_program_groups_id": acad_program_groups_id,
							"academic_years_id": academic_years_id,
							"section_id": section_id,
							"room_no": room_no,
							"action": 'delete_section'},
					dataType: 'json',
					success: function(response) {				
						if (!response.msg) {
							$('#show_error_msg').html('Cannot be removed because section already in use!'); 							
							$('#show_trash_icon_'+section_id).html("<i class='icon-trash'></i>")
						} else {
							$('#show_error_msg').html(''); 	
							$('#show_extracted_items').html(response.output); 	
						}					
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
					
				});
			
			}

		});
	</script>

			
			
<form id="download_classlist_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_classlist_pdf" />
</form>

<script>
	$('.download_classlist_pdf').click(function(event){
		event.preventDefault(); 
		var section_id    = $(this).attr('section_id');
		var grade_section = $(this).attr('grade_section');
		var strand        = $(this).attr('strand');
		var class_adviser = $(this).attr('class_adviser');
		var room_no       = $(this).attr('room_no');
		var abbreviation  = $(this).attr('abbreviation');

		var element2  = $("option:selected", "#select2");
		var term_sy   = element2.attr('term_text');
	
		$('<input>').attr({
			type: 'hidden',
			name: 'section_id',
			value: section_id,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'grade_section',
			value: grade_section,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'strand',
			value: strand,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'class_adviser',
			value: class_adviser,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'room_no',
			value: room_no,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'abbreviation',
			value: abbreviation,
		}).appendTo('#download_classlist_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'term_sy',
			value: term_sy,
		}).appendTo('#download_classlist_form');

		$('#download_classlist_form').submit();
		
	});
</script>
			
			
<form id="download_report_card_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_report_card_pdf" />
</form>

<script>
	$('.download_report_card_pdf').click(function(event){
		event.preventDefault(); 

		var section_id    = $(this).attr('section_id');

		var filename  = $(this).attr('abbreviation')+" "+$(this).attr('grade_section');

		var element2  = $("option:selected", "#select2");
		var term_id   = element2.val();
		var term_text = element2.attr('term_text');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'section_id',
			value: section_id,
		}).appendTo('#download_report_card_form');

		$('<input>').attr({
			type: 'hidden',
			name: 'term_id',
			value: term_id,
		}).appendTo('#download_report_card_form');

		$('<input>').attr({
			type: 'hidden',
			name: 'term_text',
			value: term_text,
		}).appendTo('#download_report_card_form');

		$('<input>').attr({
			type: 'hidden',
			name: 'filename',
			value: filename,
		}).appendTo('#download_report_card_form');

		$('#download_report_card_form').submit();
		
	});
</script>



