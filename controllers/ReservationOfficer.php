<?php

class ReservationOfficer extends MY_Controller {

	public function __construct(){
		parent::__construct();
		//print_r($this->userinfo);die();
		//print_r($this->session->all_userdata());die();
		if ($this->session->userdata('role')=='ReservationOfficer'){		
			$navigation = array(
							'Calendar' => 'calendar',
					);
		}
		
		$this->load->model('av/av_model');
		$data = $this->av_model->get_approval_location();
		$approval_location ="";
		if ($data){
			//$key = 0;
			foreach ($data as $row){
				//$approval_location .= $row['location'] . ($key < (count($data)-1) ? ",":"");
				//$key++;
				$new[]= $row['location'];
			}
			$approval_location = implode(",",$new);
			$approval_location =  $approval_location ;
		}
		
		$this->userinfo['approval_location']=$approval_location;
		//print_r($this->userinfo['approval_location']);die();
		
		
		$this->content_lib->enqueue_header_style('ggpopover');
		$this->content_lib->enqueue_footer_script('ggpopover');
		
		$this->navbar_data['menu'] = $navigation;
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		
		
		
	}
	

	public function calendar(){
		//var_dump($this->approval_location);die();
		
		$this->load->library('av_lib');
		$this->av_lib->calendar();
	}
		

	function list_all_bookings_of_the_day(){
		$activity_date = $this->input->post('date');
		$this->load->library('av_lib');
		$this->av_lib->list_all_bookings_of_the_day($activity_date);
	}
	
	function add_booking(){
		$activity_date = $this->input->post('activity_date');
		$activity_time_s = $this->input->post('activity_time_s');
		$activity_time_e = $this->input->post('activity_time_e');
		$activity_name = $this->input->post('activity_name');
		$equipment = $this->input->post('equipment');
		$size = $this->input->post('group_size');
		$remark = $this->input->post('remark');
		$location = $this->input->post('location');
	
		$this->load->library('av_lib');
		$this->av_lib->add_booking($activity_date,$activity_time_s,$activity_time_e,$activity_name,$equipment,$size,$remark,$location);
	}
	
	function approve_booking(){
		$data = $this->input->post('check');
		$error_approving_count = 0;
		$success_approving_count = 0;
		$conflicts_count = 0;
		foreach ($data as $value){
			$book_data = $this->av_model->list_possible_conflicts($value);
			if (!$book_data){
				$output['type'] = "Error";
				$output['message']="Found errors while searching for possible conflicts!";
				$this->output->set_output(json_encode($output));
				return;				
			}
			$suspects = array(); //suspected conflicts 
			foreach ($book_data as $row){
				$suspects[(int)$row->id]=$row;				
			}

			$counflict_found = false;
			$check_time_end = $suspects[(int)$value]->time_end;
			$check_time_start = $suspects[(int)$value]->time_start;
			$check_location = $suspects[(int)$value]->location; 
				
			foreach ($suspects as $suspect){
				if ($suspect->id != (int)$value ){
					if (strtotime($check_time_end) > strtotime($suspect->time_start) AND strtotime($suspect->time_end) > strtotime($check_time_start) AND $check_location == $suspect->location ){
						$counflict_found = true ;
						$conflicts_count++; 
					}
				}
			}
			
			if (!$counflict_found){
				if ($updated = $this->av_model->approve_booking((int)$value)  AND $updated>0){
					$success_approving_count++;
				}else{
					$error_approving_count++;
				}
			}
				
			
			
		} //end of foreach ($data as $value){
		
		if ($error_approving_count > 0){
			$output['type'] = "Error";
			$output['message']="Some errors were encoutered!";
			$this->output->set_output(json_encode($output));					
		}elseif ($success_approving_count > 0){
			$output['type'] = "Success";
			$output['message']=$success_approving_count. " bookings were successfully approved. \n" 
					. ($conflicts_count  > 0 ? $conflicts_count . " were not approved due to conflict." : "");
			$this->output->set_output(json_encode($output));							
		}elseif ($success_approving_count == 0){
			$output['type'] = "Error";
			$output['message']="No bookings were approved!";
			//$output['message']=$this->db->last_query();
			$this->output->set_output(json_encode($output));				
		}
		return;
		
	}


	function disapprove_booking(){
		$data = implode(',',$this->input->post('check'));
		if ($disapproved = $this->av_model->disapprove_booking($data)  AND $disapproved>0){
			$output['type'] = "Success";
			$output['message'] = $disapproved . " bookings were disapproved!";
		}else{
			$output['type'] = "Error";
			$output['message']="Some errors were encoutered while disapproving bookings!";				
		}
		$this->output->set_output(json_encode($output));
		
		return;
	
	}
	
	function mark_forapproval(){
		$data = implode(',',$this->input->post('check'));
		if ($forapproval = $this->av_model->mark_forapproval($data)  AND $forapproval>0){
			$output['type'] = "Success";
			$output['message'] = $forapproval . " bookings were marked as For Approval!";
		}else{
			$output['type'] = "Error";
			$output['message']="Some errors were encoutered while processing your request!";
		}
		$this->output->set_output(json_encode($output));	
		return;	
	}
	
	function mark_didnotshowup(){
		$data = implode(',',$this->input->post('check'));
		if ($noshow = $this->av_model->mark_didnotshowup($data)  AND $noshow>0){
			$output['type'] = "Success";
			$output['message'] = $noshow . " bookings were marked as No Show!";
			//$output['message'] = $this->db->last_query() ;
		}else{
			$output['type'] = "Error";
			$output['message']="Some errors were encoutered while processing your request!";
		}
		$this->output->set_output(json_encode($output));
		return;
	}

	
	public function booking_history($empno){		
		$this->load->model('av/av_model');
		$data['data'] = $this->av_model->booking_history($empno);
		if (!$data){
			$this->content_lib->set_message("Failed to list booking history!", 'alert-error');
		}else{
			$this->content_lib->enqueue_footer_script('data_tables');
			$this->content_lib->enqueue_body_content('av/booking_history',$data);
		}
			
		$this->content_lib->content();
	}
	
	function cancel_my_booking(){
		$ids_to_cancel = implode(',',$this->input->post('check'));
		$this->load->library('av_lib');
		$this->av_lib->cancel_my_booking($ids_to_cancel);
	}
	
	
} ?>