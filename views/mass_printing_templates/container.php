<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title; ?></title>
		<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
		
		<style>
		#content {width:400px;}
		.btn {
			margin-left:150px;
			margin-top:150px;
			height: 30px;
			width: 100px;
			color: white;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			background-color: #007500;
			background-image: -webkit-gradient(linear,0 0,0 100%,from(#008C00),to(#007500));
			background-image: -webkit-linear-gradient(top,#008C00,#007500);
			background-image: -o-linear-gradient(top,#008C00,#007500);
			background-image: linear-gradient(to bottom,#008C00,#007500);
			background-image: -moz-linear-gradient(top,#008C00,#007500);
			background-repeat: repeat-x;
			border-color: #008C00 #008C00 #007500;
			filter: progid:dximagetransform.microsoft.gradient(startColorstr='#008C00',endColorstr='#007500',GradientType=0);
			filter: progid:dximagetransform.microsoft.gradient(enabled=false);
		}
		.btn.disabled, .btn[disabled] {
			cursor: default;
			background-image: none;
			opacity: .65;
			filter: alpha(opacity=65);
			-webkit-box-shadow: none;
			-moz-box-shadow: none;
			box-shadow: none;
		}
		</style>
	</head>
	<body>
	<?php echo '<h5>'.$title.'</h5>'; ?>
	<div id="content">
		<button class="btn">Print</button>
		<div id="container" style="display:none;">
			<?php echo $content; ?>
		</div>
	</div>

<script>
	$(document).ready(function(){
		$('.btn').click(function(){
		var content_to_print = $('#to_print').text();
		var hiddenElement = document.createElement('a');

		if(!content_to_print)
			content_to_print = $('#assessment_to_print').text();
		if(!content_to_print)
			content_to_print = $('#print_midterm_assessment').text();
		if(!content_to_print)
			content_to_print = $('#print_prefinal_assessment').text();
		if(!content_to_print)
			content_to_print = $('#exam_2').text();
		if(!content_to_print)
			content_to_print = $('#to_print').text();

		hiddenElement.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(content_to_print);
		hiddenElement.target = '_blank';
		hiddenElement.download = 'receipt.txt';
	document.getElementById('container').appendChild(hiddenElement);
		hiddenElement.click();
		});	
	});
</script>
	</body>
</html>
