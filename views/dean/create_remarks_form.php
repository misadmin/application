<div class="fluid span12">
	<form action="<?php echo site_url('dean/student/' . $this->uri->segment(3)); ?>" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="create_remarks" />
		<fieldset>
				<div class="control-group formSep">
					<label for="remarks" class="control-label">Remarks</label>
					<div class="controls">
						<textarea id="remarks" name="remarks" class="input-xlarge"></textarea>
					</div>
				
				</div>
				<div class="control-group formSep">
					<label for="view_rights" class="control-label">View Rights</label>
					<div class="controls">
						
						<input type="radio" name="view_rights" value="1" checked="checked"/> Own use <br>
						
						<input type="radio" name="view_rights" value="0" /> Student
				    </div>
		  		</div>
		  		
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Create Remarks</button>
					</div>
				</div>
		</fieldset>
		<?php 
		//  $this->form_lib->set_id('create_course_form');
		   $this->load->library('form_validation');
		   if (form_error('remarks')) 
		      $this->form_lib->enqueue_text_helper('remarks',form_error('remarks'),'error'); 
		 // $this->form_lib->content(FALSE);
	   ?>
	</form>
</div>


<!--  <h2 class="heading">Create Course</h2> -->

<?php 
/*$this->form_lib->set_id('create_remarks');
$this->form_lib->add_control_class('formSep');
$this->form_lib->set_attributes(array('method'=>'post','action'=>'','class'=>'form-horizontal'));
$this->form_lib->enqueue_hidden_input('nonce',$this->common->nonce());
$this->form_lib->enqueue_text_input('remarks','Remarks','',set_value('remarks'),FALSE,'span6');

$this->form_lib->set_submit_button('Send');

if (form_error('remarks')) $this->form_lib->enqueue_text_helper('remarks',form_error('remarks'),'error');


$this->form_lib->content(FALSE);
*/
?>