<?php

	class AcademicYears_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListAcademicTerms($not_previous=TRUE){ 
			$result = null;
			
			if ($not_previous) {
				//UPDATED: 3/6/2013 By: Genes
				//show only current and upcoming terms
				$q = "
						SELECT a.id,
							 b.id AS academic_years_id,
							CASE a.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
							END AS term, 
							CONCAT(b.end_year-1,'-',b.end_year) AS sy,
							b.end_year
						FROM 
							academic_terms AS a, 
							academic_years AS b 
						WHERE 
							a.academic_years_id=b.id 
							AND a.status != 'previous'
						ORDER BY 
							b.end_year DESC,
							a.term 
				";
			} else {
				$q = "
						SELECT a.id,
							 b.id AS academic_years_id,
							CASE a.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
							END AS term, 
							CONCAT(b.end_year-1,'-',b.end_year) AS sy,
							b.end_year
						FROM 
							academic_terms AS a, 
							academic_years AS b 
						WHERE 
							a.academic_years_id=b.id 
							and a.term>0
						ORDER BY 
							b.end_year DESC,
							a.term desc
				";			
			}
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		//EDITED: 8/18/13 by genes added my_term
		function getAcademicTerms($id=null)	{
			if($id == null) { return null; }
			
			$result = null;
			
			$q = "
					SELECT 
						a.id, 
						b.id AS academic_years_id,
						CASE a.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
						END AS term,
						a.term AS my_term, 
						CONCAT(b.end_year-1,'-',b.end_year) AS sy,
						b.status
					FROM 
						academic_terms AS a, 
						academic_years AS b 
					WHERE 
						a.academic_years_id=b.id 
						AND a.id={$this->db->escape($id)} 
			";
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
			function getCurrentAcademicTerm() {
			$result = null;
			//sorry for the edit... Justing, commented a.sy since this is not on the academic_terms table
/**
			$q1 = "
					SELECT a.academic_years_id, a.id, 
						CASE a.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
							END AS term, 
						CONCAT(b.end_year-1,'-',b.end_year) AS sy
					FROM 
						academic_terms AS a, 
						academic_years AS b
					WHERE 
						a.academic_years_id=b.id 
						AND b.status='current' 
						AND a.status='current' 
					ORDER BY b.end_year DESC, a.term DESC 
					LIMIT 1
			";
**/			
			//query modifie by Tata on May 28, 2015 to sync checking status to academic_terms not academic_years table
			$q1 = "
				SELECT tr.academic_years_id, tr.id,
					CASE tr.term
					WHEN 1 THEN '1st Semester'
					WHEN 2 THEN '2nd Semester'
					WHEN 3 THEN 'Summer'
					END AS term,
					CONCAT(ay.end_year-1,'-',ay.end_year) sy
				FROM academic_terms tr
				JOIN academic_years ay ON tr.academic_years_id = ay.id
				WHERE tr.status='current'
				ORDER BY ay.end_year DESC, tr.term DESC
				LIMIT 1
			";
												
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		
		//ADDED: 02/16/13
		//Edited by Justing...
		//added limit...
		function ListAcademicYears($limit=0) {
			$q = "SELECT 
						a.id,
						a.end_year,
						a.end_year-1 AS start_year
					FROM
						academic_years AS a
					WHERE 
						a.status != 'previous' 
					ORDER BY 
						a.end_year DESC
					";
			
			if ( ! empty ($limit)){
				$q .= " LIMIT {$this->db->escape($limit)}";
			}
			//print_r($q);
			//die();
			$query = $this->db->query($q);
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		//Added: 2/23/2013
		function GetAcademicYear($aca_id) {
			$result = null;
			$q = "SELECT 
						a.id,
						a.end_year,
						a.end_year-1 AS start_year,
						CONCAT('SY ',a.end_year-1,'-',a.end_year) AS sy,
						a.start_date,
						a.status						
					FROM
						academic_years AS a
					WHERE 
						a.id = {$this->db->escape($aca_id)}";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
		
		/*
		 * EDITED: 8/20/15 gy genes
		 * includes CONCAT
		 */
		function current_academic_year(){
			$sql = "
				SELECT 
						ay.id, 
						ay.end_year, 
						ay.start_date,
						CONCAT('SY ',ay.end_year-1,'-',ay.end_year) AS sy
					FROM 
						academic_years ay 
							JOIN academic_terms tr ON tr.academic_years_id = ay.id 
					WHERE 
						tr.`status`='current'
					LIMIT 1
			";	
			$query = $this->db->query($sql);
			if($query && $query->num_rows() > 0){
				return $query->row();
			} else {
				return FALSE;
			}
		}


		//ADDED: Mar 3, 2019 - genes
		function incoming_academic_year(){
			
			$sql = "
				SELECT 
						ay.id 
					FROM 
						academic_years ay  
					WHERE 
						ay.status='incoming'
					ORDER BY 
						ay.id DESC 
					LIMIT 1
			";	
			$query = $this->db->query($sql);
			if($query && $query->num_rows() > 0){
				return $query->row();
			} else {
				return FALSE;
			}
		}
		
	//Added: 4/11/2013
	function getPeriod($curdate) {
			$q = "SELECT 
						a.period
				  FROM
						periods as a
					WHERE 
						{$this->db->escape($curdate)} >= a.start_date 
						 AND {$this->db->escape($curdate)} <= a.end_date  ";
		//	print($q);
		//	die();			
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
	//Added:  4/12/2013	
	function getRefDate($term) {

		// die ($term);
		// added 2 days on INTERVALs below i don't know why it worked by @Tatskie Jun 23, 2015
		$result = NULL;
			if($term == '1st Semester' OR $term == '2nd Semester'){
				$q = "
						SELECT *
				  		FROM
							academic_terms 
						WHERE
							status = 'current'
							AND CURDATE() <= DATE_ADD(start_date, INTERVAL 120 DAY) 
				";
			}else {

				// originally set at 9 DAY ; then 20

				$q = "
						SELECT *
				  		FROM
							academic_terms 
						WHERE
							academic_terms.status = 'current'
							AND CURDATE() <= DATE_ADD(academic_terms.start_date, INTERVAL 60  DAY)   
				";
			}		
			
		//	die($q); 
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
	
	//Added: 4/18/2013
	
		function getTermDate($acadYear_id, $term) {
		$result = NULL;
			$q = "SELECT 
						a.start_date
				  FROM
						academic_terms as a
					WHERE
						a.cademic_years_id = {$this->db->escape($acadYear_id)} 
						AND a.term = {$this->db->escape($acadYear_id)} ";
			//print($q);
			//die();			
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
		function ListAcadYears() {
			$result = NULL;
			
			$q = "SELECT 
						a.id,
						a.end_year,
						CONCAT('SY ',a.end_year-1,'-',a.end_year) AS sy
				  FROM
						academic_years AS a
				  ORDER BY
				  		a.end_year DESC";
			//print($q);
			//die();			
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		/*
		 * @ADDED: 3/19/15 by genes
		 * @NOTE: new basic ed student can be registered if current month is march 
		 */
		function ListAcadYears_for_NewStud_Basic_Ed() {
			$result = NULL;
/**			
			$q = "					
				SELECT  ay.id, CONCAT('SY ',ay.end_year-1,'-',ay.end_year) sy
				FROM academic_years ay
				JOIN academic_terms tr ON tr.academic_years_id = ay.id
				WHERE	tr.status='current' 
			";
			$query = $this->db->query($q);

			if($query && $query->num_rows() > 0){
				$result = $query->result();				
				$num = intval($result[0]->id) + 1;
				print_r($num);die();				
				$q2 = "
						SELECT
							a.id,
							CONCAT('SY ',a.end_year-1,'-',a.end_year) AS sy
						FROM
							academic_years AS a
						WHERE
							a.status='current'
							OR (MONTH(NOW()) IN (3,4,5) AND a.id=".$num.")"
				;
				
				$query2 = $this->db->query($q2);
				
				if($query2 && $query2->num_rows() > 0){
					$result = $query2->result();
				}
			
			} 
**/
			//query revised by Tata on May 25, 2015
			//note current status should depend only on academic_terms not academic_years table 
			//EDITED: 2/1/16 by genes
			//EDITED: 4/14/19 - changed asc to desc
			//include February
			$sql = "
				SELECT  ay.id, CONCAT('SY ',ay.end_year-1,'-',ay.end_year) sy
				FROM academic_years ay
				JOIN academic_terms tr ON tr.academic_years_id = ay.id
				WHERE tr.status in ('current','incoming')
				group by ay.id
				order by ay.id desc 
				limit " . (in_array(date('n'),array(1,2,3,4,5))?2:1)
			;
			$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			return $result;
				
			
		}
		
}


//end of AcademicYears_model.php
