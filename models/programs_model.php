<?php

	class Programs_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListProgramGroups() {
			$result = null;
			
			$q1 = "SELECT * 
						FROM acad_program_groups 
						GROUP BY abbreviation";
			
			$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		function getProgram($program) {
			$result = null;
			
			$q1 = "SELECT abbreviation 
						FROM academic_programs
						WHERE id = {$this->db->escape($program)}";
			
				$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
		public function all_programs ($college_id, $status='', $distinct=TRUE){
			
			$sql = "SELECT
					";
			if ($distinct)
				$sql.= 'DISTINCT(ap.abbreviation),'; else
				$sql.= 'ap.abbreviation,';
			
			$sql .= "
					ap.id,
					ap.description,
					ap.status as program_status,
					p.effective_year,
					p.status as prospectus_status
			FROM
					academic_programs as ap
			LEFT JOIN
					prospectus as p
			ON
					p.academic_programs_id=ap.id
			WHERE
					ap.colleges_id={$this->db->escape($college_id)}
			";
			
			if ($status=='F' || $status=='A')
				$sql.= "AND status='{$status}'
						";
			
			if ($distinct)
				$sql.= "GROUP BY ap.abbreviation
						";
			
			$query = $this->db->query($sql);
			
			if ( $query && $query->num_rows() > 0)
				return $query->result(); 
			else {
				echo $this->db->_error_message() . " " . $sql;
				return FALSE;
			}
		}

		
		/***************************************************************************************************************
		 *                                                                                                              *
		*                                                                                                              *
		****************************************************************************************************************/
		/*NOTE: Changed last 01/26/2013 */
		function ListPrograms($colleges_id, $status) {
			$result = null;
				
			if ($status == 'O') {
				$q1 = "SELECT
							a.description,
							a.abbreviation,
							a.id,
							b.id AS prospectus_id,
							b.effective_year, 
							IF(a.status='O','OFFERED','FROZEN') AS status
						FROM
							academic_programs AS a,
							prospectus AS b
						WHERE
							a.id=b.academic_programs_id 
							AND b.status='A'
							AND a.status='O'
							AND a.colleges_id = {$this->db->escape($colleges_id)}
						ORDER BY 
							a.abbreviation, b.effective_year DESC";
			} else {
				$q1 = "SELECT
							a.description,
							a.abbreviation,
							a.id,
							IF(a.status='O','OFFERED','FROZEN') AS status
						FROM
							academic_programs AS a
						WHERE
							a.colleges_id = {$this->db->escape($colleges_id)}
						ORDER BY 
							a.abbreviation ";
				}				
			
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
			$result = $query->result();
			}
		return $result;
		}
		
		
		/*NOTE: Changes made on 12/30/12*/
		function AddProgram($program_data) {
				
			if ($program_data['departments_id']) {
			$query = "
			INSERT INTO
			academic_programs (id, departments_id, colleges_id, acad_program_groups_id, abbreviation, description, status)
						VALUES (
							'',
							{$this->db->escape($program_data['departments_id'])},
							{$this->db->escape($program_data['colleges_id'])},
							{$this->db->escape($program_data['acad_program_groups_id'])},
							{$this->db->escape($program_data['abbreviations'])},
							{$this->db->escape($program_data['description'])},
							{$this->db->escape($program_data['status'])} )";
			} else {
			$query = "
					INSERT INTO
							academic_programs (id, colleges_id, acad_program_groups_id, abbreviation, description, status)
							VALUES (
							'',
							{$this->db->escape($program_data['colleges_id'])},
							{$this->db->escape($program_data['acad_program_groups_id'])},
							{$this->db->escape($program_data['abbreviations'])},
							{$this->db->escape($program_data['description'])},
							{$this->db->escape($program_data['status'])} )";
			}
			$result = $this->db->query($query);
			return $this->db->insert_id();
		}


}


//program_model.php