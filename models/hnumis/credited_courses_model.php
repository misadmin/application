<?php

	class Credited_Courses_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
			
   		}
   		
   		
		/*
		 * Added: 10/29/2014
		 * by: genes
	 	*/
   		function DeletedCredited_Courses($prospectus_courses_id) {
   			$query = "DELETE FROM
   							credited_courses
   						WHERE
   							prospectus_courses_id = {$this->db->escape($prospectus_courses_id)} ";
   				
   			//print($query); die();
   			
   			if ($this->db->query($query)) {
   				return TRUE;
   			} else {
   				return FALSE;
   			}
   			
   		}
   		
	}
	
 ?> 