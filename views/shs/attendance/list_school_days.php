
			<div style="padding:3px; overflow:auto;">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:64%;">Month-Year</td>
								<td class="shs_header" style="width:20%;">No.<br>of Days</td>
								<td class="shs_header" style="width:8%;">Update</td>
								<td class="shs_header" style="width:8%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($school_days) {
									foreach($school_days AS $sday) {
							?>
							<tr>
									<td style="text-align:center;"><?php print($sday->school_month); ?></td>
									<td style="text-align:center;"><?php print($sday->num_days); ?></td>
									<td style="text-align:center;">
										<a href="<?php print($sday->id); ?>" 
											num_days="<?php print($sday->num_days); ?>"
											school_month="<?php print($sday->school_month); ?>"
											class="update_school_day" >
											<div id="show_update_icon_<?php print($sday->id); ?>" >
												<i class="icon-pencil"></i>
											</div>
										</a>
									</td>
									<td style="text-align:center;">
										<a href="<?php print($sday->id); ?>" class="del_school_day" >
											<div id="show_trash_icon_<?php print($sday->id); ?>" >
												<i class="icon-trash"></i>
											</div>
										</a>
									</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>


<script>
	$('.del_school_day').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete this School Day?');

		if (confirmed){
			
			var school_days_id = $(this).attr('href');
			
			var element2 = $("option:selected", "#select2");
			var term_id  = element2.val();

			$('#show_trash_icon_'+school_days_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/school_days');?>',
				data: { "school_days_id": school_days_id,
						"term_id": term_id,
						action: 'delete_school_day'},
				dataType: 'json',
				success: function(response) {				
					if (!response.msg) {
						$('#show_error_msg').html('Cannot be removed because school day already in use!'); 							
						$('#show_trash_icon_'+school_days_id).html("<i class='icon-trash'></i>")
					} else {
						$('#show_error_msg').html(''); 	
						$('#show_extracted_items').html(response.output); 	
					}					
				}
			});
		
		}

	});
</script>

			

<script>
	$('.update_school_day').click(function(event){
		event.preventDefault();

		var school_days_id = $(this).attr('href');
		var num_days       = $(this).attr('num_days');
		var school_month   = $(this).attr('school_month');

		var element2 = $("option:selected", "#select2");
		$('#update_term_text_modal').val(element2.attr('term_text'));
		
		$('#show_error_msg').html('');

		$('#modal_update_school_day').modal('show');
		
		$('#update_school_day_modal').val(school_days_id);
		$('#update_month_modal').val(school_month);
		$('#update_num_days_modal').val(num_days);

	});
</script>				


	<div id="modal_update_school_day" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_school_day_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update School Day</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="update_school_day_modal"></div>
						<?php														
							$this->load->view('shs/attendance/modal_view/update_school_day_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#update_school_day_form').submit(function(e) {
		e.preventDefault();
		
		var school_days_id = $('#update_school_day_modal').val();
		var num_days       = $('#update_num_days_modal').val();

		var element2 = $("option:selected", "#select2");
		var term_id  = element2.val();

		$('#modal_update_school_day').modal('hide');
		
		$('#show_update_icon_'+school_days_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/school_days'); ?>",
			data: {"school_days_id": school_days_id,
					"term_id": term_id,
					"num_days": num_days,
					"action": "update_school_day" },
			dataType: 'json',
			success: function(response) {													

				$('#show_extracted_items').html(response.output); 
				
				$('#show_update_icon_'+school_days_id).html("<i class='icon-pencil'></i>")
				
			}
		});
		
	});
</script>				

