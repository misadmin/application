<div style="margin-top:20px; width:70%">
	<h3>List of Mismatched Assessments</h3>
	<h3><?php echo $list_title; ?></h3>
</div>
<div style="margin-top:20px; width:70%">
	<table class="table table-condensed table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="4%" style="text-align:center;">ID NO</th>
				<th width="20%">STUDENT NAME</th>
				<th width="8%">COURSE</th>
				<th width="10%">SECTION</th>
				<th width="8%" style="text-align:center;">POSTED ASSESSMENT</th>
				<th width="8%" style="text-align:center;">SUPPOSED ASSESSMENT</th>
				<th width="8%" style="text-align:center;">DISCREPANCY</th>
			</tr>
		</thead>
	<?php foreach($students as $row) {
		echo '<tbody>';
		echo '  <tr>';
		echo '    <td style="text-align:center;">'.$row['students_idno'].'</td>';
		echo '    <td>'.$row['name'].'</td>';
		echo '    <td>'.$row['course_year'].'</td>';
		echo '    <td>'.$row['section'].'</td>';
		echo '    <td style="text-align:right">'.number_format($row['posted_assessment'],2).'</td>';
		echo '    <td style="text-align:right">'.number_format($row['supposed_assessment'],2).'</td>';
		if($row['posted_assessment']<>$row['supposed_assessment']){
			echo '    <td style="text-align:right; "><font color="red">'.number_format($row['supposed_assessment']-$row['posted_assessment'],2).'</font></td>';
		} else {
			echo '    <td></td>';
		}
		echo '  </tr>';
		echo '</tbody>';
	} ?>
	</table>
</div>