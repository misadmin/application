


<form id = "edit_remark_form" action="<?php echo site_url('dean/remarks/' . $this->uri->segment(3)); ?>" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="get_Remarks" />

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="20%" style="text-align: center; ">Date Created/Updated</th>
			<th width="60%" style="text-align: center; ">Remarks</th>
			<th width="10%" style="text-align: left; ">Edit</th>
			<th width="10%" style="text-align: left; ">Delete</th>
  		</tr>
	</thead>
	<tbody> 
<?php if (is_array($all_remarks) && count($all_remarks) > 0 ): ?>
<?php foreach ($all_remarks as $remark): ?>
		<tr>
			<td><?php echo $remark->remark_date; ?></td>
			<td><?php echo $remark->remark; ?></td>
			<td style="text-align:left; vertical-align:middle; ">
               <a class="edit_remarks" href="<?php print($remark->id); ?>"><i class="icon-edit"></i></a></td>
            <td style="text-align:left; vertical-align:middle; ">
               <a class="delete_remarks" href="<?php print($remark->id); ?>"><img src="<?php print(base_url('assets/img/icon-recycle.gif'));?>" /></a>
                    		  
		     </td>
		</tr>
		
		
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="6">No remarks yet</td>
		</tr>
<?php endif; ?>
	</tbody>

</table>
</form>

<script>
	$('.edit_remarks').click(function(event){
		event.preventDefault(); 
		var remark_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'remark_id',
		    value: remark_id,
		}).appendTo('#edit_remark_form');
		$('#edit_remark_form').submit();
	});
</script>

<form id="delete_Rem" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="delete_remark" />
		
	</form>			

<script>
$('.delete_remarks').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to remove remark?");

	if (confirmed){
		var remark_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'remark_id',
		    value: remark_id,
		}).appendTo('#delete_Rem');
		$('#delete_Rem').submit();
	}		
});
</script>