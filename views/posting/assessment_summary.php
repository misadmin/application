<div style="width:auto; margin-top:20px;">
<div style="text-align:right">
<a class="download_assess" href="" academic_terms_id="<?php print($term->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>
 <table width="100%" border="0" cellspacing="0" cellpadding="1" class="table table-bordered">
 	<thead>
  <tr>
    <td colspan="8" align="center" style="text-align:center; font-weight:bold; font-size:16px;">ASSESSMENT SUMMARY<br /> 
	<?php
		print($term->term." ".$term->sy);
	 ?></td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td style="width:22%; text-align:center;">PROGRAMS</td>
    <td style="width:13%; text-align:center;">1st Year </td>
    <td style="width:13%; text-align:center;">2nd Year </td>
    <td style="width:13%; text-align:center;">3rd Year </td>
    <td style="width:13%; text-align:center;">4th Year </td>
    <td style="width:13%; text-align:center;">5th Year </td>
    <td style="width:13%; text-align:center;">TOTAL</td>
  </tr>
	</thead>
  <?php 
		$grand_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'total'=>0);
		$sub_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'total'=>0);
		$total_assessment = array();
		$grad_school = FALSE;
		//print_r($program); die();
		foreach($programs AS $program) {
			
			//$groups = $this->Assessment_Model->ListGroupAssessmentSummary($program->id, $academic_terms_id);
			$groups   =  $this->Assessment_Model->getTuitionFeeEnrolledSummary($academic_terms_id, $program->id);
			//print_r($groups); die();
			$t_others =  $this->Assessment_Model->getTuitionFee_OthersSummary($academic_terms_id, $program->id);
			//print_r($t_others); die();
			$matri    =  $this->Assessment_Model->getMatriculationSummary($academic_terms_id, $program->id);
			//print_r($matri); die();
			$misc     =  $this->Assessment_Model->getMiscSummary($academic_terms_id, $program->id);
			//print_r($misc); die();
			$other_school_fees =  $this->Assessment_Model->getOtherSchoolFeesSummary($academic_terms_id, $program->id);
			//print_r($other_school_fees); die();
			$add_other_fees    =  $this->Assessment_Model->getAddtnlOtherFeesSummary($academic_terms_id, $program->id);
			//print_r($add_other_fees); die();
			$lab_fees =  $this->Assessment_Model->getLaboratoryFeesSummary($academic_terms_id, $program->id);
			
			$total = $total1 = $total2 = $total3 = $total4 = $total5 = 0;
			
			$total1 = $groups->amt1+$t_others->t_others1+$matri->matri1+$misc->m1+$other_school_fees->osf1+
							$add_other_fees->add_other_fee1+$lab_fees->lab_fee1; 
			$total2 = $groups->amt2+$t_others->t_others2+$matri->matri2+$misc->m2+$other_school_fees->osf2+
							$add_other_fees->add_other_fee2+$lab_fees->lab_fee2; 
			$total3 = $groups->amt3+$t_others->t_others3+$matri->matri3+$misc->m3+$other_school_fees->osf3+
							$add_other_fees->add_other_fee3+$lab_fees->lab_fee3; 
			$total4 = $groups->amt4+$t_others->t_others4+$matri->matri4+$misc->m4+$other_school_fees->osf4+
							$add_other_fees->add_other_fee4+$lab_fees->lab_fee4; 
			$total5 = $groups->amt5+$t_others->t_others5+$matri->matri5+$misc->m5+$other_school_fees->osf5+
							$add_other_fees->add_other_fee5+$lab_fees->lab_fee5; 
			$total = $total1 + $total2 + $total3 + $total4 + $total5;
							
			$grand_total['assess1'] = $grand_total['assess1'] + $total1;
			$grand_total['assess2'] = $grand_total['assess2'] + $total2;
			$grand_total['assess3'] = $grand_total['assess3'] + $total3;
			$grand_total['assess4'] = $grand_total['assess4'] + $total4;
			$grand_total['assess5'] = $grand_total['assess5'] + $total5;
			$grand_total['total'] = $grand_total['total']     + $total;
					
			$total_assessment[] = array($program->abbreviation=>array($total1,$total2,$total3,$total4,$total5,$program->levels_id));
			
			//checks if it is Grad School
			if ($program->levels_id == 7 AND !$grad_school) {
				$grad_school = TRUE;
  ?>
			<tr style="background:#EAEAEA;">
			    <td style="text-align:right; font-weight:bold;">SUB TOTAL:</td>
			    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess1'],2)); ?></td>
			    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess2'],2)); ?></td>
			    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess3'],2)); ?></td>
			    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess4'],2)); ?></td>
			    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess5'],2)); ?></td>
			    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['total'],2)); ?></td>
			  </tr>
<?php 
				$sub_total = array('assess1'=>0,'assess2'=>0,'assess3'=>0,'assess4'=>0,'assess5'=>0,'total'=>0);
			}
			
			$sub_total['assess1'] = $sub_total['assess1'] + $total1;
			$sub_total['assess2'] = $sub_total['assess2'] + $total2;
			$sub_total['assess3'] = $sub_total['assess3'] + $total3;
			$sub_total['assess4'] = $sub_total['assess4'] + $total4;
			$sub_total['assess5'] = $sub_total['assess5'] + $total5;
			$sub_total['total']   = $sub_total['total']   + $total;
			
?>    
  <tr>
    <td style="text-align:left; padding-left:30px;"><?php print($program->abbreviation); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($total1,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($total2,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($total3,2)); ?></td>
    <td style="text-align:right; color:#FF0000;"><?php print(number_format($total4,2)); ?></td>
    <td style="text-align:right; color:#0000CC;"><?php print(number_format($total5,2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($total,2)); ?></td>
  </tr>
	<?php
 		}
 		
 		$total_assessment = json_encode($total_assessment);
 		//print_r($total_assessment);
   ?>
<tr style="background:#EAEAEA;">
	    <td style="text-align:right; font-weight:bold;">SUB TOTAL:</td>
		<td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess1'],2)); ?></td>
		<td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess2'],2)); ?></td>
	    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess3'],2)); ?></td>
	    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess4'],2)); ?></td>
	    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['assess5'],2)); ?></td>
	    <td style="text-align:right; font-weight:bold;"><?php print(number_format($sub_total['total'],2)); ?></td>
  </tr>

<tr style="background:#EAEAEA;">
    <td style="text-align:right; font-weight:bold;">GRAND TOTAL:</td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess1'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess2'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess3'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess4'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['assess5'],2)); ?></td>
    <td style="text-align:right; font-weight:bold;"><?php print(number_format($grand_total['total'],2)); ?></td>
  </tr>
</table>
<div style="text-align:right">
<a class="download_assess" href="" total_assessment='<?php print($total_assessment); ?>'  academic_terms_id="<?php print($term->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>

</div>

  <form id="download_assess_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.download_assess').click(function(event){
		event.preventDefault(); 
		var academic_terms_id = $(this).attr('academic_terms_id');
		var total_assessment = $(this).attr('total_assessment');
			
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_terms_id',
			value: academic_terms_id,
		}).appendTo('#download_assess_summary_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'total_assessment',
			value: total_assessment,
		}).appendTo('#download_assess_summary_form');
		$('#download_assess_summary_form').submit();
		
	});
</script>
