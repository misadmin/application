<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Smsprivileges_Model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }
    public function privileges( $idnumber ){
    	$column = ($idnumber < 20000 ? 'employees_empno' : 'students_idno');
    	$table = ($idnumber < 20000 ? 'employees_sms_privileges' : 'students_sms_privileges');
    	$query = $this->db->select('privileges')->from($table)->where($column, $idnumber)->get();
    	if ($query->num_rows() > 0){
    		$privileges = json_decode($query->row()->privileges);
    		if (is_array($privileges))
    			return $privileges;
    	}
    	return array();
    }
    public function update_privileges( $idnumber, $privileges=array() ){
    	$column = ($idnumber < 20000 ? 'employees_empno' : 'students_idno');
    	$table = ($idnumber < 20000 ? 'employees_sms_privileges' : 'students_sms_privileges');
    	$privileges_json = json_encode($privileges);
    	$sql = "INSERT into {$table} (`{$column}`, `privileges`)
    		VALUES (" . $this->db->escape($idnumber) . "," . $this->db->escape($privileges_json) .")
    		ON DUPLICATE KEY UPDATE `{$column}` = " . $this->db->escape($idnumber) . ", `privileges`=" . $this->db->escape($privileges_json) ."
    		";
    	$query = $this->db->query($sql);
    	if ($this->db->affected_rows() < 1)
    		return FALSE; else
    		return TRUE;
    }
}