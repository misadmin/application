		<div style="width:82%; border-radius: 5px; background-color: #EAEAEA; 
				border:solid 1px #D8D8D8; height: 35px; padding: 2px; margin-bottom:20px;">
			<div style="float:right; padding-top:8px; padding-right:10px;" >
				<a href="#" style="text-decoration: none; color:#4E4D4D; font-size:12px; "
					term_sy="<?php print($term_sy); ?>"
					grade_section="<?php print($grade_section); ?>"
					strand="<?php print($strand); ?>"
					room_no="<?php print($room_no); ?>"
					abbreviation="<?php print($abbreviation); ?>"
					students='<?php print(json_encode($students)); ?>'
					class="download_pdf_report" >
					<div style="float:right; padding-left:3px; padding-top:2px;">Download to PDF</div>
					<div id="pdf_labels" style="float:right;">
						<img src="<?php print(base_url('assets/images/pdf_icon.png')); ?>" style="height:20px;" /> 
					</div>
				</a>
			</div>
		</div>

		

<form id="download_class_list_form" method="post" target="_blank">
  <input type="hidden" name="action" value="download_class_list_to_pdf" />
</form>

<script>
	$('.download_pdf_report').click(function(event){
		event.preventDefault(); 

		var term_sy       = $(this).attr('term_sy');
		var grade_section = $(this).attr('grade_section');
		var strand        = $(this).attr('strand');
		var room_no       = $(this).attr('room_no');
		var abbreviation  = $(this).attr('abbreviation');

		var students      = $(this).attr('students');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'term_sy',
			value: term_sy,
		}).appendTo('#download_class_list_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'grade_section',
			value: grade_section,
		}).appendTo('#download_class_list_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'strand',
			value: strand,
		}).appendTo('#download_class_list_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'room_no',
			value: room_no,
		}).appendTo('#download_class_list_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'abbreviation',
			value: abbreviation,
		}).appendTo('#download_class_list_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'students',
			value: students,
		}).appendTo('#download_class_list_form');
		
		$('#download_class_list_form').submit();
		
	});
</script>
					