<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rsclerk_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}
	
	public function basic_ed_levels(){
		;
	}
	

	function ListBasicEdLevels() {
		$result = NULL;
	
		$q = "SELECT
					a.*
				FROM
					levels AS a
				WHERE
					a.id IN (2,3,4,5)
				ORDER BY
					a.id";
		
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
	}
	
	/*function ListBasicEd_GradeLevels() {
	 $result = NULL;
	
	$q = "SELECT
	a.*
	FROM
	basic_ed_histories AS a
	GROUP BY
	a.levels_id, a.yr_level
	ORDER BY
	a.levels_id, a.yr_level";
	
	$query = $this->db->query($q);
	
	if($query && $query->num_rows() > 0){
	$result = $query->result();
	}
		
	return $result;
	}*/
	
	function getBasicEdLevel($levels_id) {
		$result = NULL;
	
		$q = "SELECT
					a.id,
					a.level
				FROM
					levels AS a
				WHERE
					a.id = {$this->db->escape($levels_id)} ";
	
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
	
		}

		
	function ListOfStudentsBasedOnHistory($academic_years_id) {

		$result = NULL;
	
		$q = "SELECT
					a.idno,
					CONCAT(a.lname,', ',a.fname,' ',a.mname) AS neym,
					c.*,
					d.section_name,
					d.gs_section_id,
					d.hs_section_id
				FROM
					students AS a,
					basic_ed_students AS b,
					basic_ed_histories AS c,
					basic_ed_sections AS d
				WHERE
					a.idno=b.students_idno
					AND b.students_idno=c.students_idno
					AND c.basic_ed_sections_id = d.id
					AND c.academic_years_id = {$this->db->escape($academic_years_id)} 
				ORDER BY
					neym ";
	
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
	}

	function ListOldHistory($academic_years_id) {

		$result = NULL;
	
		$q = "SELECT
					c.id,
					c.students_idno,
					c.academic_years_id,
					c.levels_id,
					c.yr_level,
					c.basic_ed_sections_id
				FROM
					students AS a,
					basic_ed_students AS b,
					basic_ed_histories_old AS c
				WHERE
					a.idno=b.students_idno
					AND b.students_idno=c.students_idno
					AND c.academic_years_id = {$this->db->escape($academic_years_id)} 
				ORDER BY
					a.lname,a.fname,a.mname";
	
		$query = $this->db->query($q);
	
		//print($q); die();
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
	}

	function getNewHistory_NoInsert($id) {

		$result = NULL;
	
		$q = "SELECT
					id,
					students_idno,
					academic_years_id,
					levels_id,
					yr_level,
					basic_ed_sections_id
				FROM
					basic_ed_histories_new 
				WHERE
					id = {$this->db->escape($id)} ";
	
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
	}
	
	function getNewHistory($id) {

		$result = NULL;
	
		$q = "SELECT
					*
				FROM
					basic_ed_histories_new 
				WHERE
					id = {$this->db->escape($id)} ";
	
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
	}

	function getFindStudent($id) {

		$result = NULL;
	
		$q = "SELECT
					a.idno,
					CONCAT(a.lname,', ',a.fname,' ',a.mname) AS neym,
					c.*,
					d.section_name,
					d.gs_section_id,
					d.hs_section_id
				FROM
					students AS a,
					basic_ed_students AS b,
					basic_ed_histories_new AS c LEFT JOIN basic_ed_sections AS d ON c.basic_ed_sections_id = d.id
				WHERE
					a.idno=b.students_idno
					AND b.students_idno=c.students_idno
					AND c.id = {$this->db->escape($id)} ";
	
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
	}

	function getNotFindStudent($id) {

		$result = NULL;
	
		$q = "SELECT
					a.idno,
					CONCAT(a.lname,', ',a.fname,' ',a.mname) AS neym,
					c.*,
					d.section_name,
					d.gs_section_id,
					d.hs_section_id
				FROM
					students AS a,
					basic_ed_students AS b,
					basic_ed_histories_old AS c LEFT JOIN basic_ed_sections AS d ON c.basic_ed_sections_id = d.id
				WHERE
					a.idno=b.students_idno
					AND b.students_idno=c.students_idno
					AND c.id = {$this->db->escape($id)} ";
	
		//print($q); die();
		$query = $this->db->query($q);
	
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
	}
	
	function InsertNewHistory($stud) {
		$result = NULL;
		//print_r($stud); die();
		$q1 = "INSERT INTO 
					basic_ed_histories_new 
						(students_idno,
						academic_years_id,
						levels_id,
						yr_level,
						status,
						basic_ed_sections_id,
						inserted_on,
						withdrawn_by,
						withdrawn_on)
					SELECT 
						students_idno,
						academic_years_id,
						levels_id,
						yr_level,
						status,
						basic_ed_sections_id,
						inserted_on,
						withdrawn_by,
						withdrawn_on
					FROM
						basic_ed_histories_new 
					WHERE
						id = {$this->db->escape($stud->id)}";

		//print($q1); die();		
		if ($this->db->query($q1)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	function UpdateNewHistory($stud,$stud_insert) { 
		$q2 = "UPDATE
					basic_ed_histories_new
				SET
					students_idno = {$this->db->escape($stud->students_idno)},
					academic_years_id = {$this->db->escape($stud->academic_years_id)},
					levels_id = {$this->db->escape($stud->levels_id)},
					yr_level = {$this->db->escape($stud->yr_level)},
					status = {$this->db->escape($stud_insert->status)},
					basic_ed_sections_id = {$this->db->escape($stud->basic_ed_sections_id)},
					inserted_on = {$this->db->escape($stud_insert->inserted_on)},
					withdrawn_by = {$this->db->escape($stud_insert->withdrawn_by)},
					withdrawn_on = {$this->db->escape($stud_insert->withdrawn_on)}
				WHERE
					id = {$this->db->escape($stud->id)}";
			
		if ($this->db->query($q2)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	

	function UpdateToActive($academic_years_id) {
		$q2 = "UPDATE
					basic_ed_histories_new
				SET
					status = 'active'
				WHERE
					academic_years_id = {$this->db->escape($academic_years_id)}";
			
		if ($this->db->query($q2)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	function ListNewStudents($academic_years_id) {
		$result = NULL;
		
		if ($academic_years_id == 115) {
			$q = "SELECT
						a.id AS basic_ed_id,
						a.students_idno,
						a.academic_years_id
					FROM
						basic_ed_histories_new AS a
					WHERE
						a.id = (SELECT
										MAX(x.id)
									FROM
										basic_ed_histories_new AS x
									WHERE
										x.academic_years_id = {$this->db->escape($academic_years_id)}
										AND x.students_idno = a.students_idno) ";
		} else {
			$q = "SELECT
						a.id AS basic_ed_id,
						a.students_idno,
						a.academic_years_id
					FROM
						basic_ed_histories_new AS a
					WHERE
						a.id = (SELECT
									MIN(x.id)
								FROM
									basic_ed_histories_new AS x
								WHERE
									x.academic_years_id = {$this->db->escape($academic_years_id)}
									AND x.students_idno = a.students_idno) ";
		}
		
		//print($q); die();
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
		$result = $query->result();
		}
			
		return $result;
	
	}
	
	function FixAcadYears($stud,$academic_years_id) {
		$q2 = "DELETE FROM
						basic_ed_histories_new
					WHERE
						students_idno = {$this->db->escape($stud->students_idno)}
						AND academic_years_id = {$this->db->escape($academic_years_id)}
						AND id != {$this->db->escape($stud->basic_ed_id)}";
		
		if ($this->db->query($q2)) {
			return TRUE;
		} else {
			return FALSE;
		}
	
	}
	
	/*
	 * @ADDED: 9/3/14 
	 * @author: genes
	 */
	function ListExamDates($levels_id,$academic_years_id) {
		$result = NULL;
		
		/*$q = "SELECT
					b.level,
					a.year_level,
					DATE_FORMAT(a.start_date,'%W, %M %e, %Y') AS start_date,
					DATE_FORMAT(a.end_date,'%W, %M %e, %Y') AS end_date
				FROM
					periods_bed AS a,
					levels AS b
				WHERE
					a.levels_id = b.id
					AND b.id IN (1,2,3,4,5)
					AND a.academic_years_id = {$this->db->escape($academic_years_id)} 
				ORDER BY
					b.id, a.year_level, a.start_date";*/
		
		$q = "SELECT
					(SELECT
							if (a.year_level,a.year_level,0) AS yr
						FROM
							periods_bed AS a,
							levels AS b
						WHERE
							a.levels_id = b.id
							AND b.id = c.id
							AND FIND_IN_SET('1',a.year_level)  
							AND a.academic_years_id = {$this->db->escape($academic_years_id)} 
						ORDER BY
							b.id, a.year_level, a.start_date)
					AS y1,
					(SELECT
							if (a.year_level,a.year_level,0) AS yr
					FROM
							periods_bed AS a,
							levels AS b
						WHERE
							a.levels_id = b.id
							AND b.id = c.id
							AND FIND_IN_SET('2',a.year_level)  
							AND a.academic_years_id = {$this->db->escape($academic_years_id)} 
						ORDER BY
							b.id, a.year_level, a.start_date)
					AS y2
				FROM
					levels AS c
				WHERE 
					c.id = {$this->db->escape($levels_id)} ";

		print($q); die();
		$query = $this->db->query($q);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
		
	}
}