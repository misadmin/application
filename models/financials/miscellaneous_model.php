<?php

	class Miscellaneous_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		//Added: 2/16/2013
		function AddMisc_Fee($data) 
		{
			foreach($data['academic_programs'] as $program)
			{
				$query = "INSERT INTO misc_fees (id, academic_years_id, misc_items_id, academic_programs_id, rate, yr_level) 
   						      VALUES ('',
									   {$this->db->escape($data['academic_years_id'])},
									   {$this->db->escape($data['misc_item_id'])},
									   {$this->db->escape($program->id)},
									   {$this->db->escape($data['rate'])},
									   {$this->db->escape($data['yr_level'])})";
				$this->db->query($query);					
			}						
			
			$id = @mysql_insert_id();
			//print($query);
			//die();
			return $id;
	}
	
		//added: 2/16/2013			
		function ListMiscellaneousItems() {
			$result = null;
				
			$q1 = "SELECT
					id,
					description
				   FROM
					  misc_items
				   ORDER BY 
					  description";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}					
		
//Added:2/18/2013

	function AddNew_Fee_Item($item_name) {
	   //wala pa ni nahuman 
		$query = "INSERT INTO misc_items (id, description) 
   						      VALUES ('',
									 {$this->db->escape($item_name)})";
				$this->db->query($query);					
								
			
			$id = @mysql_insert_id();
			//print($query);
			//die();
			return $id;
	}
	
//Added: 3/6/2013

	function ListMiscFees($data) {
			$result = 0;
			$q = "SELECT 
						a.id,
						a.yr_level,
						a.rate,
						b.description, 
						b.abbreviation
													
					FROM
						misc_fees AS a, academic_programs as b
					WHERE 
					    a.misc_items_id = {$this->db->escape($data[misc_item_id])}
						AND b.colleges_id = {$this->db->escape($data[college_id])}
						AND a.academic_programs_id = b.id
						AND a.academic_years_id = {$this->db->escape($data[academic_years_id])}
						AND a.yr_level = {$this->db->escape($data[yr_level])}
						
					ORDER BY
					    b.description, a.yr_level";
			
		//print($q);
	//	die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
	}	
		
	function getMiscillaneousItem($misc_item_id){
	
		$result = null;
		
		$q = "SELECT description
				FROM
					misc_items as a
				WHERE
					a.id = {$this->db->escape($misc_item_id)}";
				
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
			
		return $result;
			
	}		
		
	function getMiscFee($fee_id){
	
		$result = null;
		
		$q = "SELECT a.rate, a.yr_level, a.academic_years_id, b.description 
				FROM
					misc_fees as a, academic_programs as b, academic_years as c
				WHERE
					a.academic_programs_id = b.id
					AND a.id = {$this->db->escape($fee_id)} "; 
			
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
			
		return $result;
			
	}		
		
  function UpdateMiscFee($id, $misc_fee){
  
	$q = "UPDATE misc_fees 
			SET 
				rate={$this->db->escape($misc_fee)} 
			WHERE
				id={$this->db->escape($id)}";
			
	if ($this->db->query($q)) {
		return TRUE;
	} else {
		return FALSE;
	}			
 }
		
		
}

//end of college_model.php