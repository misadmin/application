<style>
	tr.cancelled {text-decoration: line-through;}
</style>


<div class="span5">
<h4>Booking History</h4><hr/>
<table id="history-table" class="table table-condensed table-striped">
	<thead>
		<th></th>
		<th>Booked By</th>
		<th>Date</th>
		<th>Location</th>
		<th>Time</th>
		<th>Status</th>
		<th>&times</th>
	</thead>
	<tbody>
<?php $i=0; ?>
<?php foreach ($data as $row): $i++; ?>
		<tr id="tr<?php echo $row->id; ?>" class="<?php echo $row->status=='cancelled' ? 'cancelled':'' ; ?>">
			<td><?php echo $i ?></td>
			<td><?php echo $row->booked_by ?>
			<td><?php echo $row->date ?></td>
			<td><?php echo $row->location ?></td>
			<td><?php echo $row->time ?></td>
			<td id="c<?php echo $row->id; ?>"><?php echo ucwords($row->status); ?></td>
			<td>
				<input id="check<?php echo $row->id; ?>" class="checkboxes" type="checkbox" name="check[]" value="<?php echo $row->id;?>" />						
			</td>

<?php endforeach; ?> 	

	</tbody>
</table>

</div>


<script>

$(document).ready(function () {		
	$("input.checkboxes").click(function(){
		if ($("input[type='checkbox']").is(":checked")){
			var ans = confirm("The checked booking will be cancelled!");
			if (ans !== true){
				$('.checkboxes').attr('checked', false);				
				return;
			}
		}else{
			return;
		}	
		var clicked_tr = $(':checkbox:checked').closest('tr').attr('id') ;
		show_loading_msg();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url($this->role . '/cancel_my_booking'); ?>",  
			data: $(".checkboxes:checked").serialize(),
			dataType: "json",
			success: function(data){
		    	hide_loading_msg();
				if (data.type == 'Success'){				
					alert(data.message);
					var cancelled_id = data.id; 
					$("#c"+cancelled_id).text("Cancelled");
					$("#"+clicked_tr).removeClass().addClass('cancelled');
				}else{
					show_sticky_msg(data.type,data.message);														
				}	
				$('.checkboxes').attr('checked', false);			
			},
			error: function(data){
		    	hide_loading_msg();	
				$('.checkboxes').attr('checked', false);								
		    	show_sticky_msg(data.type,data.message);		    	
			},
		});
	});
});

</script>