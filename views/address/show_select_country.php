
				<select name="select_country" id="select_country" class="form-control" style="font-style:italic;" >
					<option value="">--Please Select--</option>
					<option value="new_country" style="font-weight:bold;">New Country</option>
					<?php
						foreach ($countries AS $country) {
							print("<option value=".$country->id." country_name=".$country->name.">".$country->name."</option>");
						}
					?>
				</select>

				
<script>
	$(document).ready(function(){

		$("select#select_country").change(function(){

			$('#edit_province_group').hide();
			$('#edit_town_group').hide();
			$('#edit_brgy_group').hide();
			$('#new_country_group').hide();
			$('#new_province_group').hide();
			$('#new_state_group').hide();
			$('#new_town_group').hide();
			$('#new_suburb_group').hide();
			$('#new_brgy_group').hide();
			$('#new_street_group').hide();
			
			$('.show_address_form').show();

			var country_id   = $("select#select_country option:selected").attr('value'); 
			var country_name = $("select#select_country option:selected").attr('country_name'); 

			if (country_id.length > 0 ) { 

				if (country_id == 'new_country') {
					$('#new_country_group').show();
					$('#new_country_name').focus();
				} else {	
						
					$('#edit_province_group').show();
					
					$.ajax({
						type: "POST",
						url: "<?php echo site_url($this->uri->segment(1).'/update_address');?>",
						data: {"country_id": country_id,
								"action": 'list_provinces'},
						cache: false,
						dataType: 'json',
						beforeSend: function () { 
							$('#edit_province_group').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");
						},
						success: function(response) {    
							$("#edit_province_group").html(response.output);
							if (country_name == 'Philippines') {
								$('#province_state').html('Province:');
								$('#new_prov_state').html('New Province');
							} else {
								$('#province_state').html('State:');
								$('#new_prov_state').html('New State');
							}
						}
					});
				}
			}
		});
	});
</script>
				