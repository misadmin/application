<?php

class Scholarship extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->common->need_auth();
		
		$this->userinfo = $this->session->all_userdata();
		
		$navigation = array(
						'Students'=>'student'
				);
		
		$this->navbar_data['menu'] = $navigation;
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		$this->content_lib->set_title ('Students Affairs Office | ' . $this->config->item('application_title'));
	}
	
	public function student(){
			
		$this->content_lib->set_title('Students Affairs Office | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		
		if ( ! empty($query)){
			
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
			
			$this->load->library('pagination_lib');
			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			}
		}
		
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->helper('student_helper');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('teller/teller_model');
			
			$this->content_lib->enqueue_header_style('image_area_select');
			$this->content_lib->enqueue_footer_script('image_area_select');
			$port = substr($idnum, 0, 3);
			$this->content_lib->set_title ('SAO | ' . $idnum);
			
			$result = $this->student_common_model->my_information($idnum);	
			$result2 = $this->teller_model->get_student($idnum);
			$tab = 'student_info';
				
			//if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				
				switch ($action = $this->input->post('action')){
						case 'upload':
										
										switch ($this->input->post('submit')){
										
											case 'Submit Picture'	:
												//a picture is submitted...
												$config = $this->config->item('student_upload_config');
												$config['upload_path'] .= "/{$port}/";
												$config['file_name'] = "{$idnum}.jpg";
														
												$this->load->library('upload', $config);
												$this->upload->do_upload('id_picture');
														
												if ($errors = $this->upload->display_errors()){
													$this->content_lib->set_message('<h4>Error(s) seen while uploading student picture</h4> ' . $errors, 'alert-error');
												} else {
													$data = $this->upload->data();
													if ($x1 = $this->input->post('idx1')){
														$x2 = $this->input->post('idx2');
														$y1 = $this->input->post('idy1');
														$y2 = $this->input->post('idy2');
																
														if($data['image_width'] > 500)
															$correction_factor = $data['image_width']/500; else
															$correction_factor = 1;
															
														$image_lib_config = $this->config->item('image_config');
														$image_lib_config['x_axis'] = round($x1*$correction_factor);
														$image_lib_config['y_axis'] = round($y1*$correction_factor);
														$image_lib_config['width'] = round(($x2 - $x1)*$correction_factor);
														$image_lib_config['height'] = round(($y2 - $y1)*$correction_factor);
														$image_lib_config['source_image'] = $data['full_path'];
														$this->load->library('image_lib', $image_lib_config);
															
														if ( ! $this->image_lib->crop()){
															$this->content_lib->set_message('<h4>Error(s) seen while cropping student image</h4>'.  $this->image_lib->display_errors(), 'alert-error');
														} else {
															$this->content_lib->set_message('Student picture successfully uploaded and cropped...', 'alert-success');
														}
													}
														//todo: width here must be set in the config...
														 
													$width = (isset($image_lib_config['width']) ? $image_lib_config['width'] : $data['image_width']);
													  
													if ($width > 240){
														$image_lib_config = $this->config->item('image_config');
														$image_lib_config['maintain_ratio'] = TRUE;
														$image_lib_config['source_image'] = $data['full_path'];
														$image_lib_config['width'] = 240;
														$this->image_lib->initialize($image_lib_config);
															
														if ( ! $this->image_lib->resize()){
															$this->content_lib->set_message('<h4>Error(s) seen while resizing student image</h4>'.  $this->image_lib->display_errors(), 'alert-error');
														} else {
															$this->content_lib->set_message('Student picture successfully uploaded and resized...', 'alert-success');
														}	
													} else {
														$this->content_lib->set_message('Student picture successfully updated...', 'alert-success');
													}
													
												}
												break;

											case 'Crop Picture'		:
												
												if ($x1 = $this->input->post('idx1')){
													$x2 = $this->input->post('idx2');
													$y1 = $this->input->post('idy1');
													$y2 = $this->input->post('idy2');
												
													
													$correction_factor = 1;
														
													$image_lib_config = $this->config->item('image_config');
													$image_lib_config['x_axis'] = round($x1*$correction_factor);
													$image_lib_config['y_axis'] = round($y1*$correction_factor);
													$image_lib_config['width'] = round(($x2 - $x1)*$correction_factor);
													$image_lib_config['height'] = round(($y2 - $y1)*$correction_factor);
													$image_lib_config['source_image'] = "./assets/img/students/{$port}/{$idnum}.jpg";
													$this->load->library('image_lib', $image_lib_config);
														
													if ( ! $this->image_lib->crop()){
														$this->content_lib->set_message('<h4>Error(s) seen while cropping student image</h4>'.  $this->image_lib->display_errors(), 'alert-error');
													} else {
														$this->content_lib->set_message('Student picture successfully updated...', 'alert-success');
													}
												
												} else {
													$this->content_lib->set_message('Error found. Picture not cropped.', 'alert-error');
												}
												break;
											case 'Submit Signature' :
												
												break;
										}
									
										break;
										
						case 'change_term': //check if pull-down menu in assessment is changed
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;
						case 'generate_class_schedule':
								$this->load->model("hnumis/Reports_model");
								$this->load->model('hnumis/AcademicYears_Model');
								
								$student['idno']= $result->idno;
								$student['fname']= $result->fname;
								$student['lname']= $result->lname;
								$student['mname']= $result->mname;
								$student['yr_level']= $result->year_level;
								$student['abbreviation']= $result->abbreviation;
								$student['max_bracket_units']= $this->input->post('max_units');
								$student['student_histories_id']= $this->input->post('student_histories_id');
								
								$selected_term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
								
								$this->Reports_model->generate_student_class_schedule_pdf($this->input->post('academic_terms_id'), $student, $selected_term);
								
								return;
						default:
							break; 
						
					}
				//} else {
				//	$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
				//}
			
			$result = $this->student_common_model->my_information($idnum);
			if ($result !== FALSE) {
				//a user with that id number is seen...
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname . " " . $result->mname. " " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->year_level,
						'full_home_address'	=> $result->full_home_address,
						'full_city_address' => $result->full_city_address,
						'phone_number' 		=>$result->phone_number,
						'section'			=> $result->section,
				);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}					
				//todo: promote with a test..
				$dcontent['signature'] = base_url('assets/img/signature.png');
				
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...
				$data['year_level'] = $result->year_level;
				$data['academic_program'] =$result->abbreviation;
				$data['idnum'] = $idnum;
				$data['academic_program_id'] = $result->ap_id;
				
				$this->load->model('hnumis/Student_model');
				$this->load->model('hnumis/Courses_Model');
				$this->load->model('hnumis/Enrollments_model');
				$this->load->model('hnumis/AcademicYears_Model');
				$this->load->model('accounts/accounts_model');
				$this->load->model('teller/teller_model');
				$this->load->model('financials/Tuition_Model');
				$this->load->model('hnumis/Offerings_Model');
				
				$current_term = $this->Academic_terms_model->current_academic_term();
				$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_term->id);
				$grades = $this->Enrollments_model->student_grades($idnum);
									
				//added January 4, 2013 (psychometrician exams results)
				$this->load->model('psychometrician_model');
				$student_exams = $this->psychometrician_model->student_exams($idnum);
				$this->load->library('tab_lib');
				
								
				$acad_term = $this->AcademicYears_Model->getAcademicTerms($current_term->id);
				
				$ledger_data = array();
				$ledger_data = $this->teller_model->get_ledger_data($result2->payers_id);

				//assessment details
				/*$acontent = array(
						'idnumber'	=>$result->idno,
						'familyname'=>$result->lname,
						'firstname'	=>$result->fname,
						'middlename'=>$result->mname,
						'level'		=>$result->year_level,
						'course'	=>$result->abbreviation,
						'due_now' 	=> '',
						'succeeding_dues'	=> '7,756.87'
				);
				
				//assessment form
				
				//assessment form
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->Enrollments_model->get_enrolled_courses($selected_history_id); //ok
				} else {
					$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					$dcourses = $this->Enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
				}

				//$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$lab_courses = array();
				$courses = array();
				$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
				
				if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab') {
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
							);
						}
						$courses[] = array(
								'id'=>$course->id,
								'name'=>$course->course_code,
								'units'=>$course->credit_units,
								'pay_units'=>$course->paying_units,
								're_enrollments_id'=>$course->re_enrollments_id,
								'withdrawn_on'=>$course->withdrawn_on,
								'assessment_id'=>$course->assessment_id,
								'post_status'=>$course->post_status,
						);
					}
				}
				*/
				
				$current_term = $this->Academic_terms_model->current_academic_term();
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum);
				
				if ($action=='schedule_current_term') {
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $this->input->post('academic_term'));
					$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $this->input->post('academic_term'));
					$tab = 'schedule';
					$selected_term = $this->input->post('academic_term');
				
				} elseif ($action=='change_term') { //for change in assessment pull-down
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_term->id);
					$tab = 'assessment';
					$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $current_term->id);
					$selected_term = $academic_terms[0]->id;
				
				} else {
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $academic_terms[0]->id);
					$tab = 'student_info';
					$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $academic_terms[0]->id);
					$selected_term = $academic_terms[0]->id;
				}

				$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $selected_term);
				if ($selected_history) {
					$student_units = $this->Student_model->getUnits($selected_history, $selected_term);
					$student_history_id=$selected_history->id;
				} else {
					$student_units['max_bracket_units']=0;
					$student_history_id=0;
				}
				
				$this->tab_lib->enqueue_tab ('Student Information', 'common/student_information_full', $result, 'student_info', $tab=='student_info', FALSE);
				
				/*
				 * Ledger must show only for the SAO Officer
				 * temporary solution for Mam Cuasito (Emp. No: 100)
				 */

				if ($this->userinfo['empno'] == 100) {
					$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
							array(
									'ledger_data'=>$ledger_data,
									'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
									'year_start_date'=>$current_academic_terms_obj->year_start_date
							),
							'ledger', FALSE);
				}
				
				$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', 
						array('schedules'=>$schedule_courses,
							  'assessment_date'=>$assessment_date,
							  'academic_terms'=>$academic_terms,
							  'academic_terms_id'=>$selected_term,
							  'max_units'=>$student_units['max_bracket_units'],
							  'student_histories_id'=>$student_history_id), 
						'schedule', $tab=='schedule');
								
				$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', FALSE);
				$this->tab_lib->enqueue_tab ('Exam Results', 'student/psych_exam_results', array('exams_taken'=>$student_exams), 'exam_results', FALSE);
				$this->tab_lib->enqueue_tab ('Pic and Signature', 'sao/image_upload.php', array('image'=>$dcontent['image'], 'sign_image'=>$dcontent['signature']), 'img', FALSE);
				$this->tab_lib->enqueue_tab ('Comments', 'sao/comments.php', array(), 'comments', FALSE);
				$tab_content = $this->tab_lib->content();
				$this->content_lib->enqueue_body_content("", $tab_content);
				
				
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}
		
		//what will happen if there is no query... and the result is not numeric?
		if ( empty($query) && ! is_numeric($idnum)) {
				
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$data = array();
			$this->load->model('hnumis/College_model');
			$data['colleges'] = $this->College_model->all_colleges();
			$this->load->model('student_common_model');
				
			if ($this->input->post('what')=='search_by_program'){
				$total = count ($this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), TRUE));
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				$results = $this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), FALSE, $page, $this->config->item('results_to_show_per_page'));
				$this->load->library('pagination_lib');
				$data['pagination'] = $this->pagination_lib->pagination('', $total, $page);
				$data['results'] = $results;
				$this->load->model('hnumis/Programs_model');
				$this->load->model('hnumis/College_model');
				$data['query'] = $this->Programs_model->getProgram($this->input->post('program'))->abbreviation . " " . $this->input->post('year_level');
				$data['programs'] = $this->College_model->programs_from_college($this->input->post('college'));
				$data['college'] = $this->input->post('college');
				$data['program'] = $this->input->post('program');
				$data['year_level'] = $this->input->post('year_level');
				$data['end'] = $end;
				$data['total'] = $total;
				$data['start'] = $start;
			}
			$this->content_lib->enqueue_body_content('common/search_result_by_program', $data);
		}
		$this->content_lib->content();	
	}
}