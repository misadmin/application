<?php 
class Migration_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function get_prospectus_courses_id($prospectus_id, $course_code){
		$sql = "
			select pc.id
			from prospectus_courses pc
			join prospectus_terms pt on pt.id = pc.prospectus_terms_id
			join prospectus p on p.id = pt.prospectus_id
			join courses c on pc.courses_id = c.id
			where c.course_code='{$course_code}' and  p.id ='{$prospectus_id}'
		";
		//if ($this->iteration == 4){		
		//	print_r($sql);die();		
		//}	
			
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() >= 0){
			//print_r($query->result());die();
			return $query->result();
		}else{ 
			return false;
		}
	}
	
	public function get_latest_prospectus_id($idno){
		$sql = "
			select h.prospectus_id 
			from student_histories h 
			where h.students_idno = '{$idno}'
			order by h.academic_terms_id desc 
			limit 1
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() >= 0)
			return $query->result();
		else 
			return false;
	}
	
	public function get_subjects_to_be_credited(){
		$sql ="
			select h.students_idno idno, e.id enr_id, e.credit_as course_code
			from other_schools_enrollments e 
			join other_schools_student_histories h on h.id = e.other_schools_student_histories_id
			where !isnull(e.credit_as) and e.credit_as!=''
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() >= 0)
			return $query->result();
		else 
			return false;
	}


	public function get_subjects_to_be_credited_from_here(){
		$sql ="
			select h.students_idno idno, e.id as enr_id, sm.credit_as as course_code
			from hnumis.student_histories h 
			join hnumis.academic_terms tr on tr.id = h.academic_terms_id
			join hnumis.academic_years ay on ay.id = tr.academic_years_id
			join hnumis.enrollments e on e.student_history_id = h.id
			join hnumis.course_offerings co on co.id = e.course_offerings_id
			join hnumis.courses c on c.id = co.courses_id
			join student.marks_credit sm on sm.sy = ay.end_year and tr.term = sm.term 
				and sm.school=0 and sm.catalog_id=0 and sm.catalog_no = c.course_code and sm.stud_id = h.students_idno 				
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() >= 0)
			return $query->result();
		else
			return false;
	}
	
	
	
	public function insert_credited_courses ($data_to_insert){
		$sql = "
				insert into credited_courses (students_idno, prospectus_courses_id, other_schools_enrollments_id) 
			values
				('{$data_to_insert['idno']}', '{$data_to_insert['prospectus_courses_id']}', '{$data_to_insert['os_enr_id']}' );			
				 
		";
		if ($query = $this->db->query($sql)){
			$inserted_id = $this->db->insert_id();
			if ($inserted_id < 1)
				return false; 
			else 
				return $inserted_id; 			
		}else{
			return false;
		}		
	}
	
	
	public function insert_credited_courses_from_here($data_to_insert){
		$sql = "
		insert into credited_courses (students_idno, prospectus_courses_id, enrollments_id)
		values
		('{$data_to_insert['idno']}', '{$data_to_insert['prospectus_courses_id']}', '{$data_to_insert['os_enr_id']}' );
					
		";
		if ($query = $this->db->query($sql)){
		$inserted_id = $this->db->insert_id();
		if ($inserted_id < 1)
			return false;
			else
				return $inserted_id;
		}else{
		return false;
		}
	
	}
	
	function insert_student_history($idno, $academic_terms_id ){
		$sql = "
			insert ignore into student_histories (students_idno, academic_terms_id, prospectus_id) values 
				('{$idno}', '{$academic_terms_id}', '555') 				
		";
		//note: 555 is unknown prospectus
		//print_r($sql);die();
		if ($query = $this->db->query($sql)){
			$inserted_id = $this->db->insert_id();
			if ($inserted_id < 1)
				return false;
			else
				return $inserted_id;
		}else{
			return false;
		}
		
	}
	
	
	function insert_col_students($idno){
		$sql = "
			insert ignore into col_students (students_idno) values ('{$idno}')
		";
		if ($query = $this->db->query($sql)){
			$inserted_id = $this->db->insert_id();
			if ($inserted_id < 1)
				return false;
			else
				return $inserted_id;
		}else{
			return false;
		}
	
	}
	
	function list_courses_from_isis($idno) {		
		$sql = "
			select h.id as history_id, cm.stud_id, cs.students_idno as col_students_idno, concat(s.lname,', ',s.fname) as student, cm.sy, cm.term, 
				cm.catalog_no, sc.section, cm.grade_1, cm.grade_3, cm.final_gr, cm.units
			from student.college_marks cm 
			left join student.classes sc on sc.stud_id = cm.stud_id and sc.sy=cm.sy and sc.term=cm.term and sc.catalog_no=cm.catalog_no			
			join hnumis.academic_years ay on ay.end_year = cm.sy
			join hnumis.academic_terms tr on tr.academic_years_id = ay.id and cm.term = tr.term
			join hnumis.students s on s.idno = cm.stud_id
			left join hnumis.student_histories h on h.students_idno = cm.stud_id and h.academic_terms_id = tr.id
			left join hnumis.col_students cs on cs.students_idno = cm.stud_id			
			where 
				cm.terms = 2 and cm.stud_id > 0 and cm.school = 0 
				and cm.catalog_id = 0 and cm.final_gr !='' 
				and cm.stud_id = '{$idno}'
			order by 
				cm.stud_id, cm.sy, cm.term, cm.catalog_no
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result();
		else
			return false;
		
	}
	
	function get_course_from_mis ($history_id, $catalog_no){
		$sql = "
			select e.id as enrollments_id, e.student_history_id, e.course_offerings_id, e.prelim_grade, e.midterm_grade, e.finals_grade
			from enrollments e 
			left join student_histories h on h.id = e.student_history_id
			left join course_offerings co on co.id = e.course_offerings_id and co.academic_terms_id = h.academic_terms_id
			left join courses c on c.id = co.courses_id
			where e.student_history_id = {$history_id}
			and c.course_code = '{$catalog_no}'				
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result();
		else
			return false;
		
	}
	
	
	public function get_course_offerings_id($catalog_no, $sy, $term, $section){
		$sql ="
			select co.id as course_offerings_id 
			from course_offerings co 
			join courses c on c.id = co.courses_id 
			join academic_terms tr on tr.id = co.academic_terms_id 
			join academic_years ay on ay.id = tr.academic_years_id 	
			where 
				ay.end_year = '{$sy}'
				and tr.term = '{$term}'
				and co.section_code = '{$section}'
				and c.course_code = '{$catalog_no}'
				limit 1		
		";
		//print_r($sql);die();	
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result();
		else
			return false;
		
	}

	public function migrate_isis_to_mis($data, $courses_to_migrate=array()){
		$insertions=0;
		if (!(is_array($data) AND count($data)>0)){
			$this->session->set_userdata('err_msg','No data found!');
			return 0;
		}
		$selected_courses_to_migrate = array();
		//print_r($courses_to_migrate);die();
		foreach($data as $key){
			$selected_courses_to_migrate[] = $courses_to_migrate[$key];
		}
		
		//print_r($selected_courses_to_migrate);die();
		
		if (count($selected_courses_to_migrate) < 1){
			$this->session->set_userdata('err_msg','No selected subjects to migrate!');
			return count($selected_courses_to_migrate);
		}
		
		//get course_offerings_id for each course
		$this->db->trans_start();
		foreach ($selected_courses_to_migrate as $row){
			//print_r($row);die();
			$course_offerings_id = $this->get_course_offerings_id($row->catalog_no, $row->sy, $row->term, $row->section);
			//print_r($course_offerings_id);die();
			if ($course_offerings_id){
				if (!$this->migrate_isis_to_mis_now($course_offerings_id[0]->course_offerings_id,(array)$row)){					
					$this->db->trans_rollback();
					$this->session->set_userdata('err_msg','Failed to get Course Offerings ID for '.$row->catalog_no);
					return 0;								
				}else{
					$insertions++;
				}
			}else{
				if (($acad_term_id = $this->get_academic_term_id($row->sy, $row->term)) AND ($courses_id = $this->get_courses_id($row->catalog_no, $row->units)) ){
					if ( $co_id = $this->insert_course_offerings($acad_term_id[0]->id, $courses_id[0]->id) ){
						if (!$this->migrate_isis_to_mis_now($co_id, (array)$row)){
							$this->db->trans_rollback();
							$this->session->set_userdata('err_msg','Failed to insert enrollment for CO ID: '. $co_id . ' '. implode(',',(array)$row) . '<br>' . $this->db->last_query() . '<br>' . $this->db->_error_message() );
							return 0;
						}else{
							$insertions++;							
						}						
					}else{
						$this->db->trans_rollback();
						$detail = implode('-',array($row->catalog_no, $row->sy, $row->term, $row->section));
						$this->session->set_userdata('err_msg','No MIS course offering for: '. $detail );
						//die("here 3");						
						return 0;						
					}
						
				}
					
				
				
			}			
			
		}
		$this->db->trans_complete();
		return $insertions;
	}
	
	
	function migrate_isis_to_mis_now($course_offerings_id, $data){
		$sql = "
			insert into enrollments (course_offerings_id, student_history_id, prelim_grade, midterm_grade, finals_grade) values 
			('{$course_offerings_id}', '{$data['history_id']}', '{$data['grade_1']}', '{$data['grade_3']}', '{$data['final_gr']}') 
		";		
		//print_r($sql);die();
		if ($query = $this->db->query($sql)){
			$inserted_id = $this->db->insert_id();
			if ($inserted_id < 1)
				return false;
			else
				return $inserted_id;
		}else{
			return false;
		}		
	}
	
	function insert_course_offerings ($academic_terms_id, $courses_id){
		$sql = "
			INSERT INTO `course_offerings` (`academic_terms_id`, `courses_id`, `inserted_by`, `inserted_on`) values
				('{$academic_terms_id}','{$courses_id}','404',now())
		";
		//print_r($sql);die();
		if ($query = $this->db->query($sql)){
			$inserted_id = $this->db->insert_id();
			if ($inserted_id < 1)
				return false;
			else
				return $inserted_id;
		}else{
			return false;
		}
	}
	
	function get_academic_term_id ($sy,$term){
		$sql = "
			select tr.id
			from academic_terms tr 
			join academic_years ay on ay.id = tr.academic_years_id
			where ay.end_year='{$sy}' and tr.term='{$term}'		
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result();
		else
			return false;		
	}
	
	function get_courses_id ($course_code, $units){
		$sql = " select id from courses where course_code='{$course_code}' ";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result();
		else{
			if ($query->num_rows() == 0){ //course_code found in ISIS but not in MIS. now we try to insert this to MIS
				$inserted = $this->insert_unknown_course_code($course_code, $units);
				if ($inserted > 0 ){					
					return $this->get_courses_id($course_code); //stalemate :)					
				}else{
					$this->session->set_userdata('err_msg','<br>' . $course_code . ' is not found in MIS and even failed to insert such.');
					return false;								
				}
				
			}else{
				return false;
			}
		}		
	}
	
	function insert_unknown_course_code($course_code, $units){
		$sql = "
			insert ignore into courses (course_code,courses_groups_id,course_types_id,owner_colleges_id,descriptive_title,credit_units,remark,inserted_by,inserted_on) values 
			('{$course_code}','1','1','0','-', '{$units}' , 'via migrator insertion',{$this->session->userdata('empno')},now())
		";
		if ($query = $this->db->query($sql)){
			$inserted_id = $this->db->insert_id();
			if ($inserted_id < 1)
				return false;
			else
				return $inserted_id;				
		}else{
			return false;
		}
		
	}
	
	function list_room_classes($room_id='61', $academic_term='464'){ // 457
		
		$sql = "
			select co.id as co_id, c.course_code, co.section_code, r.room_no as room, sl.days_day_code as sked, sl.start_time, sl.end_time, e.lname as assigned_by, 
				t.empno as teacher_idno, t.lname as teacher_lname, t.fname as teacher_fname 
			from course_offerings co 
				join course_offerings_slots sl on sl.course_offerings_id = co.id
				join rooms r on r.id = sl.rooms_id
				join courses c on c.id = co.courses_id
				join employees e on e.empno = sl.inserted_by 
				left join employees t on t.empno = co.employees_empno 
			where
				co.`status`='A'  
				and co.academic_terms_id = '464' 
				and r.id = '{$room_id}'
			order by 
				sl.days_day_code, sl.start_time 
		";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() >= 0)
			return $query->result();
		else
			return false;		
		
	}
	
	function list_subject_students($course_offerings_id){
		$sql = "
			select c.course_code, co.section_code, s.idno, s.lname, s.fname, ifnull(t.lname,'unknown') as tlname
			from enrollments e 
				join student_histories h on h.id = e.student_history_id
				join course_offerings co on co.id = e.course_offerings_id 
				join students s on s.idno = h.students_idno
				join courses c on c.id = co.courses_id				
				left join employees t on t.empno = co.employees_empno 				
			where 
				co.id = '{$course_offerings_id}'
			order by 
				s.lname, s.fname
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result();
		else
			return false;
		
	}
	
	
}
