<?php 
class nstp_model extends MY_Model {

	/** 
	 * Bridge function to query a list of nstp students
	 * @author   Toyet Amores
	 * @param    term - term of interest
	 * @return   array with:
	 *             name,
	 *             gender,
	 *             program_and_year,
	 *             birthday,
	 *             barangay,
	 *             town,
	 *             province,
	 *             NSTP subject enrolled,
	 *             final grade
	*/
	function fetch_nstp_list($term=NULL) {

		$sql = "SELECT DISTINCT
				concat(st.lname,', ',st.fname,' ',st.mname) as name,
				       st.gender,
				       concat(ap.abbreviation,' ',sh.year_level) as program_year,
				       st.dbirth as birthday,
				       coalesce(addr.home_address,brgy.name) as barangay,
				       twn.name as town,
				       prv.name as province,
				       cors.course_code,
				       en.finals_grade,
				       (SELECT pn.phone_number FROM phone_numbers pn
						  WHERE pn.students_idno=st.idno 
						  AND pn.is_primary='Y' LIMIT 1) AS phone_number
				from enrollments en
				left join student_histories sh on sh.id=en.student_history_id
				left join students st on st.idno=sh.students_idno
				left join prospectus pros on pros.id=sh.prospectus_id
				left join academic_programs ap on ap.id=pros.academic_programs_id
				left join course_offerings co on co.id=en.course_offerings_id
				left join courses cors on cors.id=co.courses_id
				left join addresses addr on addr.students_idno=st.idno
				left join barangays brgy on brgy.id=addr.home_barangays_id
				left join towns twn on twn.id=brgy.towns_id
				left join provinces prv on prv.name=twn.provinces_id
				where co.academic_terms_id = {$term}
				      and cors.course_code in ( 'ROTC',
												'NSTP 1',
												'NSTP 2',
												'MS 11',
												'MS 12',
												'MS 21',
												'MS 31',
												'MS 32',
												'MS 41',
												'MS 42',
												'CWTS' )
				order by cors.course_code, name; ";

		//log_message("INFO",print_r($sql,true)); // Toyet 8.29.2018
		
		$query = $this->db->query($sql);	
		return ($query ? $query->result() : FALSE);
	}


}

