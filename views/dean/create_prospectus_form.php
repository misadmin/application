<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>
<!-- 
<form action="<?php echo site_url('dean/prospectus');?>" method="post">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="4" />
  <?php echo validation_errors(); 
  ?><div style="background:#FFF; ">
  <table align="center" cellpadding="0" cellspacing="0" style="width:40%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head">
  NEW PROSPECTUS
  </td>
  </tr>
  <tr>
    <td colspan="3">
  
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
<tr>
  <td width="107" align="left" valign="middle">Program</td>
  <td width="6" align="left" valign="middle">:</td>
  <td width="401" align="left" valign="middle">
  					<select name="academic_programs_id" id="select" class="form">
    <?php
				foreach($programs AS $program) {
					print("<option value=".$program->id.">".$program->description."</option>");	
				}
			?>
    </select></td>
</tr>
<tr>
  <td align="left" valign="middle">Effective Year</td>
  <td align="left" valign="middle">:</td>
  <td align="left" valign="middle">
  	<input name="effective_year" type="text" value="<?php print(date('Y')); ?>" class="form" style="font-family:Verdana, Geneva, sans-serif; font-size:12px; width:50px;" id="effective_year" /></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><input type="submit" name="button" id="button" value="Continue!"  class="btn btn-success" /></td>
</tr>
    </table>
    </td>
    </tr>
    </table>
    </div>

</form>
 -->
<h2 class="heading">Create Prospectus</h2>
<?php 
$this->form_lib->set_id('create_prospectus_form');
$this->form_lib->add_control_class('formSep');
$this->form_lib->set_attributes(array('method'=>'post','action'=>'#','class'=>'form-horizontal'));
$this->form_lib->enqueue_hidden_input('step',4);
$this->form_lib->enqueue_select_input2('academic_programs_id','Program','{id}','{description}',$programs,'','','span5');
$this->form_lib->enqueue_text_input('effective_year','Effective Year','','',TRUE,'span1');
$this->form_lib->set_submit_button('Create');
if (isset($message) AND $message != ""){
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$message.'</div>';
}
$this->form_lib->content(FALSE);
