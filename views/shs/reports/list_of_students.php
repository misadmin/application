		
			<div style="width:80%; overflow:auto; margin-top:15px; ">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:5%;">Count</td>
								<td class="shs_header" style="width:10%;">ID<br>No.</td>
								<td class="shs_header" style="width:25%;">Lastname</td>
								<td class="shs_header" style="width:25%;">Firstname</td>
								<td class="shs_header" style="width:20%;">Middlename</td>
								<td class="shs_header" style="width:8%;">Gender</td>
								<td class="shs_header" style="width:7%;">Grade<br>Level</td>
							</tr>
						</thead>
						<tbody>
							<?php

								if ($students) {
									$cnt=1;
									$r_year_level=11;  // Start with Grade 11   Toyet 5.14.2019
									foreach($students  AS $student) {

										if($student->year_level != $r_year_level){
											$cnt=1;
										?>

											</tbody>
										</table>
									<table class="table table-hover table-bordered table-striped" >

										<thead>
											<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
												<td class="shs_header" style="width:5%;">Count</td>
												<td class="shs_header" style="width:10%;">ID<br>No.</td>
												<td class="shs_header" style="width:25%;">Lastname</td>
												<td class="shs_header" style="width:25%;">Firstname</td>
												<td class="shs_header" style="width:20%;">Middlename</td>
												<td class="shs_header" style="width:8%;">Gender</td>
												<td class="shs_header" style="width:7%;">Grade<br>Level</td>
											</tr>
										</thead>

										<?php

											$r_year_level = 12;
										
									} else {
							?>			
										<tr>
											<td style="text-align:right;"><?php print($cnt); ?>.</td>
											<td style="text-align:center;">
												<a href="<?php print(site_url($this->uri->segment(1).'/student')."/".$student->idno);?>" >
													<?php print($student->idno); ?>
												</a>
											</td>
											<td><?php print($student->lname); ?></td>
											<td><?php print($student->fname); ?></td>
											<td><?php print($student->mname); ?></td>
											<td style="text-align:center;"><?php print($student->gender); ?></td>
											<td style="text-align:center;"><?php print($student->year_level); ?></td>
										</tr>
							<?php
											$cnt++;
										}
									}
								}
							?>
						</tbody>
					</table>
			</div>
