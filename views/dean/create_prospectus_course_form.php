<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<form action="<?php echo site_url('dean/prospectus');?>" method="post">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="9" />

 <div style="background:#FFF;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:45%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head">
  ADD PROSPECTUS COURSE
  </td>
  </tr>
  <tr>
    <td colspan="3">
  
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
<tr>
  <td colspan="3" align="left" valign="middle" style="font-weight:bold; font-size:14px;">
  <?php 
  		print($prospectus->description." - ".$prospectus->effective_year); 
  		print("<br>".$term->y_level." - ".$term->term); 
  ?></td>
  </tr>
<tr>
  <td width="138" align="left" valign="middle">Course</td>
  <td width="3" align="left" valign="middle">:</td>
  <td width="106" align="left" valign="middle">
  <select name="courses_id" class="form" id="courses_id">
    <?php
				foreach($courses AS $course) {
					print("<option value=".$course->id.">".$course->course_code."</option>");	
				}
			?>
  </select></td>
</tr>
<tr>
  <td align="left" valign="middle">Elective?</td>
  <td align="left" valign="middle">:</td>
  <td align="left" valign="middle"><input type="radio" name="elective" id="radio" value="Y" />
    Yes 
      <input name="elective" type="radio" id="radio2" value="N" checked="checked" />
      No</td>
</tr>
<tr>
  <td align="left" valign="middle">Major?</td>
  <td align="left" valign="middle">:</td>
  <td align="left" valign="middle"><input name="is_major" type="radio" id="radio3" value="Y" checked="checked" />
    Yes
    <input name="is_major" type="radio" id="radio4" value="N" />
    No</td>
</tr>
<tr>
  <td align="left" valign="middle">Bracketed?</td>
  <td align="left" valign="middle">:</td>
  <td align="left" valign="middle"><input name="is_bracketed" type="radio" id="radio5" value="Y" />
    Yes
    <input name="is_bracketed" type="radio" id="radio6" value="N" checked="checked" />
    No</td>
</tr>
<tr>
  <td>Cut-off Grade </td>
  <td>:</td>
  <td><input name="cutoff_grade" type="text" class="form" id="cutoff_grade" size="5" maxlength="5" /></td>
</tr>
<tr>
  <td>Number of Retakes </td>
  <td>:</td>
  <td><input name="num_retakes" type="text" class="form" id="cutoff_grade" size="5" maxlength="5" /></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><input type="submit" name="button" id="button" value="Continue!"  class="btn btn-success" /></td>
</tr>
    </table>
    </td>
    </tr>
    </table>
    </div>

</form>