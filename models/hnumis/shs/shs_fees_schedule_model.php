<?php

	class Shs_fees_schedule_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListAcadPrograms_with_fees($academic_terms_id) {
			$result = NULL;
			
			$q = "SELECT
						a.id,
						a.abbreviation,
						a.description,
						b.acad_program_groups_id,
						c.group_name,
						b.fees_subgroups_id,
						c.abbreviation AS track_abbrev
					FROM
						academic_programs AS a,
						fees_schedule AS b,
						acad_program_groups AS c
					WHERE
						a.acad_program_groups_id=b.acad_program_groups_id
						AND a.acad_program_groups_id=c.id
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND a.colleges_id = 12 
					GROUP BY 
						a.id
					ORDER BY
						c.id, 
						a.description ";
			//return $q;
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
			
		}
		
		
		function ListFeesSchedule_FeesGroups($data) {
			$result = NULL;
			
			$sql = "SELECT 
							b.fees_groups_id,
							c.fees_group
						FROM 
							fees_schedule AS a,
							fees_subgroups AS b,
							fees_groups AS c 
						WHERE 
							a.fees_subgroups_id = b.id 
							AND b.fees_groups_id = c.id 
							AND a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])} 
							AND a.acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])}  
						GROUP BY 
							c.id 
						ORDER BY 
							c.weight "; 
			//return $sql;				
			$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();

				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$data['fees_groups_id'] = $res->fees_groups_id;
						
						$myresult = $this->ListFeesSchedule_FeesSubGroups($data);
						
						if ($myresult) {
							$result[$cnt]->fees_subgroups = $myresult;
						}									
						$cnt++;
					}
				}	
				
			}
				
			return $result;
			
		}
		

		function ListFeesSchedule_FeesSubGroups($data) {
			$result = NULL;
			
			$q = "SELECT
						fees_group,
						description,
						MAX(IF(yr_level = 11, rate, NULL)) gr11,
						MAX(IF(yr_level = 12, rate, NULL)) gr12,
						(SELECT
								a.id
							FROM
								fees_schedule a
							WHERE 
								a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])}
								AND a.acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])}
								AND a.fees_subgroups_id = test.fees_subgroups_id
								AND a.yr_level=11)
						AS fees_schedule_id11,
						(SELECT
								a.id
							FROM
								fees_schedule a
							WHERE 
								a.academic_terms_id = {$this->db->escape($data['academic_terms_id'])}
								AND a.acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])}
								AND a.fees_subgroups_id = test.fees_subgroups_id
								AND a.yr_level=12)
						AS fees_schedule_id12
					FROM
						(SELECT
								fgs.weight,
								fgs.fees_group,
								fg.description,
								fs.yr_level,
								fs.rate,
								fs.id,
								fs.fees_subgroups_id
							FROM
								fees_schedule fs
								JOIN fees_subgroups as fg ON fs.fees_subgroups_id = fg.id
								JOIN fees_groups AS fgs ON fgs.id=fg.fees_groups_id
							WHERE 
								fs.academic_terms_id = {$this->db->escape($data['academic_terms_id'])}
								AND fs.acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])}
								AND fg.fees_groups_id = {$this->db->escape($data['fees_groups_id'])}  
							GROUP BY
								fs.yr_level, fg.id
						) test
					GROUP BY
						description
					ORDER BY
						weight, description";
			
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
			
		}


		function ListLab_with_no_fees($academic_years_id) {
			$result = NULL;
			
			$q = "SELECT
						a.id,
						a.course_code
					FROM
						courses AS a
					WHERE
						a.id NOT IN (SELECT 
											b.courses_id
										FROM
											laboratory_fees AS b,
											prospectus_courses AS c,
											prospectus_terms AS d,
											prospectus AS e,
											academic_programs AS f,
											block_sections AS g,
											shs_block_sections AS h
										WHERE
											b.academic_years_id = {$this->db->escape($academic_years_id)} 
											AND b.courses_id = c.courses_id
											AND c.prospectus_terms_id = d.id
											AND d.prospectus_id = e.id
											AND e.status = 'A' 
											AND e.academic_programs_id = f.id
											AND f.id = g.academic_programs_id 
											AND g.id = h.block_sections_id )
						AND a.id NOT IN (SELECT 
												b.courses_id 
											FROM 
												laboratory_fees AS b 
											WHERE 
												b.academic_years_id = {$this->db->escape($academic_years_id)} 
												AND b.levels_id = 12)
						AND a.course_types_id = 11 
						AND a.owner_colleges_id = 12
					ORDER BY
						a.course_code ";
			
			//die($q);
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
			$result = $query->result();
			}
				
			return $result;
			
		}


		public function get_school_years() {		
			$sql="SELECT 
							ay.id, 
							concat_ws('-',ay.end_year-1,ay.end_year) as school_year,
							status				
					FROM 
							academic_years ay
					WHERE 
							ay.`status` in ('incoming','current')		
					ORDER BY 
							school_year	";
							
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();
				return $result;
			}

			return false;	

		}	


		function AddLaboratory_Fees($fees_data) {
			$query = "
					INSERT INTO
						laboratory_fees (
								academic_years_id,
								levels_id,
								courses_id,
								rate,
								posted,
								inserted_by,
								inserted_on
							)
						VALUES (
								{$this->db->escape($fees_data['academic_years_id'])},
								{$this->db->escape($fees_data['levels_id'])},
								{$this->db->escape($fees_data['courses_id'])},
								{$this->db->escape($fees_data['rate'])},
								{$this->db->escape($fees_data['posted'])},
								{$this->session->userdata('empno')},
								now() 
							)
					";
							
			$result = $this->db->query($query);	
			$my_last_id = $this->db->insert_id();
			return $my_last_id ? $my_last_id : false;
		}


		function ListLaboratoryItems($academic_years_id, $academic_programs_id) {
			$result = NULL;
		
			$q = "SELECT
						a.id AS laboratory_fees_id,
						e.course_code AS course_code,
						c.year_level,
						FORMAT(a.rate,2) AS rate
					FROM
						laboratory_fees AS a,
						prospectus AS b,
						prospectus_terms AS c,
						prospectus_courses AS d,
						courses AS e
					WHERE
						a.courses_id = d.courses_id
						AND d.prospectus_terms_id = c.id
						AND c.prospectus_id = b.id
						AND a.courses_id = e.id
						AND a.academic_years_id = {$this->db->escape($academic_years_id)}
						AND b.academic_programs_id = {$this->db->escape($academic_programs_id)}
					ORDER BY
						course_code ";
			
			//die($q);
			$query = $this->db->query($q);
		
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}
		
	}


?>

