
<div class="pull-right">
	<form method="post" id="schedule_current_term_form">
		<input type="hidden" value="schedule_current_term" name="action" />
		<?php $this->common->hidden_input_nonce(FALSE); ?>
<?php if ($role != 'student' && isset($academic_terms) && !empty($academic_terms)): ?>
			<select name="academic_term" id="academic_term">
<?php foreach ($academic_terms as $academic_term):?> 
				<option value="<?php echo $academic_term->id; ?>" <?php if($academic_term->id==$academic_terms_id) echo 'selected="selected"'?>><?php echo $academic_term->term . " " . $academic_term->sy; ?></option>
<?php endforeach; ?>
			</select>
<?php endif; ?>
	</form>
	<script>
		$('#academic_term').bind('change', function(){
			$('#schedule_current_term_form').submit();
		});
	</script>
</div>

<div style="margin-bottom:10px;">
<a class="download_class_sched" href="" academic_terms_id="<?php print($academic_terms_id); ?>" 
		student_histories_id="<?php print($student_histories_id); ?>"  
		max_units="<?php print($max_units); ?>" >Download Class Schedule as PDF<i class="icon-download-alt"></i></a>
</div>

<div class="clearfix" style="width:100%">
	<table class="table table-striped table-bordered" style="width:100%">
		<thead>
			<tr>
				<th width="12%">Catalog [Section]</th>
				<th width="30%">Descriptive Title</th>
				<th width="35%">Schedule</th>
				<th width="17%">Teacher</th>
				<th width="5%">Units</th>
				<th width="5%">Re-Enroll</th>
			</tr>
		</thead>
	
		<tbody>
	<?php 
		//print_r($schedules); die();
	if(is_array($schedules) && count($schedules) > 0): ?>
	<?php foreach($schedules as $schedule): ?>
			<tr <?php echo ($schedule['status']=="Withdrawn" ? 'class="error"' : "");?>>
				<td> <a title="<?php echo "Enrolled on ". $schedule['date_enrolled']. " by ".$schedule['enrollee_id'];   
			 					if ($schedule['date_withdrawn']!='') echo "; Withdrawn on ". $schedule['date_withdrawn'] ?>"><?php print($schedule['course_code']." [".$schedule['section_code']."]"); ?> <?php echo ($schedule['status']=="Withdrawn" ? ' [WD]' : "");?></td>
				<td><?php echo $schedule['descriptive_title']; ?></td>
				<td>
				<?php 
						$offering_slots = $this->Offerings_Model->ListOfferingSlots($schedule['id']);
						foreach ($offering_slots[0] AS $offer) {
							$sched = $offer->tym." ".$offer->days_day_code." /[".$offer->room_no."] ".$offer->bldg_name;
							print($sched."<br>");
						}
				
				?>
				</td>
				<td><?php echo $schedule['instructor']; ?></td>
				<td style="text-align: center;">
				<?php 
					if ($schedule['is_bracketed'] == 'Y') {
						print("(".$schedule['credit_units'].")");
					} else {
						print($schedule['credit_units']); 
					}
				?>
				</td>
						
				<td style="text-align:center; vertical-align:middle; ">
				   <?php if(!$schedule['enrollments_id'] && !($schedule['status']=="Withdrawn") && $academic_terms_id == $current_term_id){
				   	?>
				   	    <a class="reenrol" href="<?php print($schedule['enroll_id']); ?>"><i class="icon-adjust"></i></a>
				   <?php } ?>  
				   	    </td>
				   
			</tr>
	<?php endforeach; ?>
	<?php else: ?>
			<tr>
				<td colspan="5">No Courses found for this term</td>
			</tr>
	<?php endif; ?>
		</tbody>
	</table>
</div>

  <form id="download_class_sched_form" method="post" target="_blank">
  <input type="hidden" name="step" value="2" />
  <input type="hidden" name="action" value="generate_class_schedule" />
  </form>
<script>
$(document).ready(function(){
   
   $('.download_class_sched').click(function(event){
		event.preventDefault(); 
		var academic_terms_id = $(this).attr('academic_terms_id');
		var student_histories_id = $(this).attr('student_histories_id');
		var max_units = $(this).attr('max_units');
		
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_terms_id',
			value: academic_terms_id,
		}).appendTo('#download_class_sched_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'student_histories_id',
			value: student_histories_id,
		}).appendTo('#download_class_sched_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'max_units',
			value: max_units,
		}).appendTo('#download_class_sched_form');
		$('#download_class_sched_form').submit();
		
	});
	
	
});
	
</script>

<form id="transact_reenroll" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="re_enroll" />
		
	</form>			

<script>
$('.reenrol').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to trasact re-enrolled course?");

	if (confirmed){
		var enrollment_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'enrollment_id',
		    value: enrollment_id,
		}).appendTo('#transact_reenroll');
		$('#transact_reenroll').submit();
	}		
});
</script>

