
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Academic Year</div>
			<div class="col2a">
				<input type="text" class="form-control" id="year_text_modal" style="width:200px;" disabled />
			</div>
		</div>
		
		<div style="width:100%">
			<div class="col1">Level</div>
			<div class="col2a">
				<input type="text" class="form-control" id="level_text" style="width:200px;" disabled />
			</div>
		</div>
		
		<div style="width:100%">
			<div class="col1">Year Level</div>
			<div class="col2a">
				<div id="year_levels">
				
				</div>
			</div>
		</div>
		
		<div style="width:100%">
			<div class="col1">Section Name</div>
			<div class="col2a">
				<input type="text" class="form-control" id="section_name_modal" style="width:350px;" autofocus />
			</div>
		</div>
		<div style="width:100%; font-size:12px; font-weight:bold; color:#575555; margin-top:10px;">
			NOTE: You can only create sections on Year Levels that are <span style="font-weight:bold; color:#E40808;">Offered</span>!
		</div>
		
	</div>
