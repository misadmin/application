<?php

class posting_lib {
	
	/**
	 * Codeigniter Instance
	 * @var unknown_type
	 */
	protected $CI;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	
	public function assessment_report_group() {
	
		$this->CI->load->model('hnumis/AcademicYears_Model');
		$this->CI->load->model('hnumis/College_Model');
		$this->CI->load->model('academic_terms_model');
		$this->CI->load->model('hnumis/Programs_Model');
		$this->CI->load->model('posting/Tuition_Report_Model');
		
		
		$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 1);
			
		switch ($step) {
			case 1:
				$this->select_group_acad_yr();
				break;
		
			case 2: //from selection of term, group, year level,post status 
				$years_id = $this->CI->AcademicYears_Model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$this->generate_assessment_report($years_id->academic_years_id, $this->CI->input->post('academic_terms_id'),
													$this->CI->input->post('program_groups_id'), 
													$this->CI->input->post('yr_level'), 
													0, 
													$this->CI->input->post('post_status'));
				break;
					
			case 3: //download to PDF
				$years_id = $this->CI->AcademicYears_Model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$this->generate_assessment_report($years_id->academic_years_id, $this->CI->input->post('academic_terms_id'),
													$this->CI->input->post('program_groups_id'), 
													$this->CI->input->post('yr_level'), 
													1, 
													$this->CI->input->post('post_status'));
				break;
				
		}
	
	}
	
	/*
	 * @added: 9/11/14
	 * @author: genes
	 */
	public function assessment_basic_ed_group() {
	
		$this->CI->load->model('hnumis/academicyears_model');
		$this->CI->load->model('basic_ed/rsclerk_model');
		$this->CI->load->model('basic_ed/assessment_basic_ed_model');
		$this->CI->load->model('basic_ed/basic_ed_sections_model');
	
		$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 'select_level');
			
		switch ($step) {
			case 'select_level':
				
				//$levels = $this->CI->rsclerk_model->ListBasicEdLevels();
				$levels = $this->CI->basic_ed_sections_model->ListBasicLevels();
				$school_years = $this->CI->academicyears_model->ListAcadYears();
				
				$data = array(
						"school_years"=>$school_years,
						"levels"=>$levels,
				);
				
				$this->CI->content_lib->enqueue_body_content('basic_ed/select_level_acad_yr',$data);
				$this->CI->content_lib->content();
					
				break;
	
			case 'display_report': 
				switch ($this->CI->input->post('levels_id')) {
					case '0':
						$yr_level = 0;
						break;
					case '11':
						$yr_level = $this->CI->input->post('yr_level_pre');
						break;
					case '1':
						$yr_level = $this->CI->input->post('yr_level_k');
						break;
					case '3':
						$yr_level = $this->CI->input->post('yr_level_gs');
						break;
					case '4':
						$yr_level = $this->CI->input->post('yr_level_hs');
						break;
					case '12':
						$yr_level = $this->CI->input->post('yr_level_shs');
						break;
				}
				//print_r($this->CI->input->post());
				//print("levels_id: ".$this->CI->input->post('levels_id')); 
				//print("<p>".$yr_level); die();
				$this->generate_detailed_basic_ed_assessment($this->CI->input->post('academic_years_id'),
						$this->CI->input->post('levels_id'),
						$yr_level,
						$this->CI->input->post('post_status'));

				break;
					
			case 'download_to_pdf': //download to Excel

				//print_r($this->CI->input->post()); die();
				$tuition=json_decode($this->CI->input->post('tuition_basics'));
				$misc_fees=json_decode($this->CI->input->post('misc_fees'));
				$other_fees=json_decode($this->CI->input->post('other_fees')); 
				$addl_other_fees=json_decode($this->CI->input->post('addl_other_fees')); 
				$acad_years = json_decode($this->CI->input->post('acad_years'));
				$students = json_decode($this->CI->input->post('students'));
				//print_r($this->CI->input->post('addl_other_fees')); die();
				
				$this->generate_assessment_basic_ed_pdf($tuition,
														$misc_fees,
														$other_fees,
														$addl_other_fees,
														$acad_years,
														$students,
														$this->CI->input->post('post_description'),
														$this->CI->input->post('header'));
				break;
		}
	
	}
	
	
	public function assessment_report_college() {	
		
		$this->CI->load->model('hnumis/AcademicYears_Model');
		$this->CI->load->model('hnumis/College_Model');
		$this->CI->load->model('academic_terms_model');
		$this->CI->load->model('posting/Tuition_Report_Model');

		$step = ($this->CI->input->post('step') ?  $this->CI->input->post('step') : 1);
			
		switch ($step) {
			case 1:	
				$this->select_college_acad_yr();				
				break;
				
			case 2:
				$years_id = $this->CI->AcademicYears_Model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$this->generate_assessment_report_college($years_id->academic_years_id, 
															$this->CI->input->post('academic_terms_id'), 
															$this->CI->input->post('colleges_id'), 
															$this->CI->input->post('yr_level'), 
															0,
															$this->CI->input->post('post_status'));
				break;
					
			case 3:
				$years_id = $this->CI->AcademicYears_Model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				$this->generate_assessment_report_college($years_id->academic_years_id, 
															$this->CI->input->post('academic_terms_id'), 
															$this->CI->input->post('colleges_id'), 
															$this->CI->input->post('yr_level'), 
															1,
															$this->CI->input->post('post_status'));
				break;
		}

	}	
	

	private	function generate_assessment_report_college($academic_years_id, $academic_terms_id, $colleges_id, $yr_level, $value, $post_status) {
	
		$data['term'] = $this->CI->AcademicYears_Model->getAcademicTerms($academic_terms_id);	
		$data['students'] = $this->CI->Tuition_Report_Model->getStudentsEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		$data['colleges_id'] = $colleges_id;
		$data['yr_level'] = $yr_level;
		$data['post_status'] = $post_status;
		$data['post_status'] = $post_status;
		
		$data['post_description'] = 'POSTED';
		$data['report_type'] = 'COLLEGE';
		
		switch ($colleges_id) {
			case '0':
					if ($yr_level == 0) {
						$data['header'] = "All Colleges (All Year Levels)";
					} else {
						$data['header'] = $yr_level." - All Colleges";
					}
					break;
			default:
					if ($yr_level == 0) {
						$data['header'] = $data['students']->abbreviation." (All Year Levels)";
					} else {
						$data['header'] = $yr_level." - ".$data['students']->abbreviation;
					}
					break;
		}

		$data['tuition_basics'] = $this->CI->Tuition_Report_Model->getTuitionFeeEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		$data['tuition_others_extracted'] = $this->CI->Tuition_Report_Model->extractOtherFromTuitionFeeEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		$data['tuition_others'] = $this->CI->Tuition_Report_Model->ListTuitionOthersEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);		
		$data['matriculation'] = $this->CI->Tuition_Report_Model->getMatriculationEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		$data['misc_fees'] = $this->CI->Tuition_Report_Model->ListMiscEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		$data['other_fees'] = $this->CI->Tuition_Report_Model->ListOthersEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);

		//ADDED: 7/10/15 by genes
		$data['learning_resources_fees'] = $this->CI->Tuition_Report_Model->ListLearningResourcesEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		$data['student_support_fees'] = $this->CI->Tuition_Report_Model->ListStudentSupportEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		
		$data['addl_other_fees'] = $this->CI->Tuition_Report_Model->ListAdditionalOthersEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);

		//ADDED: 7/15/15 by genes
		$data['affil_fees'] = $this->CI->Tuition_Report_Model->ListAffiliationEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		
		$data['lab_fees'] = $this->CI->Tuition_Report_Model->ListLabFeesEnrolled($academic_years_id, $academic_terms_id, NULL, $colleges_id, $yr_level, $post_status);
		
		if ($value == 0) {
			$this->CI->content_lib->enqueue_body_content('posting/tuition_report',$data);
			$this->CI->content_lib->content();
		} else {
			$this->generate_report_pdf($data);
		}
				
	}
	
	/*
	 * @added: 9/11/14
	 * @author: genes
	 */
	private	function generate_detailed_basic_ed_assessment($academic_years_id, $levels_id, $grade_level, $post_status) {
	
		$this->CI->load->model('academic_terms_model');
		
		$data['acad_years'] = $this->CI->academicyears_model->GetAcademicYear($academic_years_id);
		$data['students'] = $this->CI->assessment_basic_ed_model->getStudentsEnrolled($academic_years_id, $levels_id, $grade_level, $post_status);
		//print_r($data['students']); die();
		$data['levels_id'] = $levels_id;
		$data['grade_level'] = $grade_level;
		$data['post_status'] = $post_status;
		//print_r($data); die();
		
		if ($post_status == 'Y') {
			$data['post_description'] = 'POSTED';
		} else {
			$data['post_description'] = 'UNPOSTED';
		}
		
		if ($data['students']) {
			if ($levels_id == 0) {
				$data['header'] = "All Levels";
			} else {
				$data['header'] = $data['students']->level;
			}
			
			if ($grade_level == 0) {
				$data['header'] = $data['header']." (All Grade Levels)";
			} else {
				$data['header'] = $data['header']." ".$grade_level;
			}
		} else {
			$data['header'] = "No Enrollees for this level!";
		}

		$acad_term = $this->CI->academic_terms_model->ListAcademicTerms($academic_years_id);
		$data['tuition_basics'] = $this->CI->assessment_basic_ed_model->ListTuitionFees($academic_years_id, $acad_term[0]->id, $levels_id, $grade_level, $post_status);
		
		//print_r($data['tuition_basics']); die();
		
		$data['misc_fees'] = $this->CI->assessment_basic_ed_model->ListMiscellaneous($academic_years_id, $acad_term[0]->id, $levels_id, $grade_level, $post_status);
		$data['other_fees'] = $this->CI->assessment_basic_ed_model->ListOtherSchoolFees($academic_years_id, $acad_term[0]->id, $levels_id, $grade_level, $post_status);
		$data['addl_other_fees'] = $this->CI->assessment_basic_ed_model->ListAdditionalOtherFees($academic_years_id, $acad_term[0]->id, $levels_id, $grade_level, $post_status);
	
		$this->CI->content_lib->enqueue_body_content('basic_ed/detailed_assessment_report',$data);
		$this->CI->content_lib->content();
		
		return;
	
	}
	
	
	private	function select_group_acad_yr() {
	
			
		$data['academic_terms'] = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
		$data['program_groups'] = $this->CI->Programs_Model->ListProgramWithGroups();
		$data['current_term'] = $this->CI->academic_terms_model->getCurrentAcademicTerm();
	
		$this->CI->content_lib->enqueue_body_content('posting/select_group_acad_yr',$data);
		$this->CI->content_lib->content();
			
	}
	
	private	function select_college_acad_yr() {
	
			
		$data['academic_terms'] = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
		$data['colleges'] = $this->CI->College_Model->ListColleges();
		$data['current_term'] = $this->CI->academic_terms_model->getCurrentAcademicTerm();
		
		$this->CI->content_lib->enqueue_body_content('posting/select_college_acad_yr',$data);
		$this->CI->content_lib->content();
			
	}

	/*
	 * EDITED: 7/10/15 by genes
	 * added Learning Resources Fee
	 * THIS HAS BEEN MARKED AS WITH A SUFFICE _OLD.  THIS IS TO BE SUPERSEDED BY FUNCTION OF THE SAME
	 * NAME.  THE NEW FUNCTION WILL FETCH DATA FROM THE ASSESSMENTS_HISTORY_COURSES, ASSESSMENTS_HISTORY_FEES,
	 * ASSESSMENTS_HISTORY_LAB TO MAKE SURE THAT THE POSTED ELEMENTS WILL NO LONGER CHANGE. -Toyet 4.5.4019
	 */
	private	function generate_assessment_report_OLD($academic_years_id, $academic_terms_id, $program_groups_id, $yr_level, $value, $post_status) {
	
		$data['term'] = $this->CI->AcademicYears_Model->getAcademicTerms($academic_terms_id);
		$data['students'] = $this->CI->Tuition_Report_Model->getStudentsEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['colleges_id'] = NULL;
		$data['yr_level'] = $yr_level;
		$data['post_status'] = $post_status;
		
		//if ($post_status == 'Y') {
			$data['post_description'] = 'POSTED';
		//} else {
		//	$data['post_description'] = 'UNPOSTED';
		//}
		$data['report_type'] = 'GROUP';
	
		if ($yr_level == 0) {
			$data['header'] = $data['students']->abbreviation." (All Year Levels)";
		} else {
			$data['header'] = $yr_level." - ".$data['students']->abbreviation;
		}
		
		$data['tuition_basics'] = $this->CI->Tuition_Report_Model->getTuitionFeeEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['tuition_others'] = $this->CI->Tuition_Report_Model->ListTuitionOthersEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);

		// added by Toyet 1.11.2019
		$data['tuition_others_extracted'] = $this->CI->Tuition_Report_Model->extractOtherFromTuitionFeeEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);

		$data['matriculation'] = $this->CI->Tuition_Report_Model->getMatriculationEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['misc_fees'] = $this->CI->Tuition_Report_Model->ListMiscEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['other_fees'] = $this->CI->Tuition_Report_Model->ListOthersEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);

		//ADDED: 7/10/15 by genes
		$data['learning_resources_fees'] = $this->CI->Tuition_Report_Model->ListLearningResourcesEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['student_support_fees'] = $this->CI->Tuition_Report_Model->ListStudentSupportEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		
		$data['addl_other_fees'] = $this->CI->Tuition_Report_Model->ListAdditionalOthersEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);

		//ADDED: 7/13/15 by genes
		$data['affil_fees'] = $this->CI->Tuition_Report_Model->ListAffiliationEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		
		$data['lab_fees'] = $this->CI->Tuition_Report_Model->ListLabFeesEnrolled($academic_years_id, $academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
	
		//log_message("INFO", "posting_lib LIBRARY ==>".print_r($data,true)); // Toyet 9.17.2018; 1.11.2019

		if ($value == 0) {
			$this->CI->content_lib->enqueue_body_content('posting/tuition_report',$data);
			$this->CI->content_lib->content();
		} else {
			$this->generate_report_pdf($data);
		}
	
	}


    // ------
	// Full edit to this function which copied from function of the same name.
	// ------
	// THIS NEW FUNCTION WILL FETCH DATA FROM THE ASSESSMENTS_HISTORY_COURSES, ASSESSMENTS_HISTORY_FEES,
	// ASSESSMENTS_HISTORY_LAB TO MAKE SURE THAT THE POSTED ELEMENTS WILL NO LONGER CHANGE. -Toyet 4.5.4019
	//
	private	function generate_assessment_report($academic_years_id, $academic_terms_id, $program_groups_id, $yr_level, $value, $post_status) {
	
		$data['term'] = $this->CI->AcademicYears_Model->getAcademicTerms($academic_terms_id);
		$data['students'] = $this->CI->Tuition_Report_Model->getStudentsEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['colleges_id'] = NULL;
		$data['yr_level'] = $yr_level;
		$data['post_status'] = $post_status;
		
		//if ($post_status == 'Y') {
			$data['post_description'] = 'POSTED';
		//} else {
		//	$data['post_description'] = 'UNPOSTED';
		//}
		$data['report_type'] = 'GROUP';
	
		if ($yr_level == 0) {
			$data['header'] = $data['students']->abbreviation." (All Year Levels)";
		} else {
			$data['header'] = $yr_level." - ".$data['students']->abbreviation;
		}
		
		$data['tuition_basics'] = $this->CI->Tuition_Report_Model->getTuitionFeeEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		
		$data['tuition_others'] = $this->CI->Tuition_Report_Model->ListTuitionOthersEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);

		// added by Toyet 1.11.2019
		$data['tuition_others_extracted'] = $this->CI->Tuition_Report_Model->extractOtherFromTuitionFeeEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);

		$data['matriculation'] = $this->CI->Tuition_Report_Model->getMatriculationEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['misc_fees'] = $this->CI->Tuition_Report_Model->ListMiscEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['other_fees'] = $this->CI->Tuition_Report_Model->ListOthersEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);

		//ADDED: 7/10/15 by genes
		$data['learning_resources_fees'] = $this->CI->Tuition_Report_Model->ListLearningResourcesEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		$data['student_support_fees'] = $this->CI->Tuition_Report_Model->ListStudentSupportEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		
		$data['addl_other_fees'] = $this->CI->Tuition_Report_Model->ListAdditionalOthersEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);

		//ADDED: 7/13/15 by genes
		$data['affil_fees'] = $this->CI->Tuition_Report_Model->ListAffiliationEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
		
		$data['lab_fees'] = $this->CI->Tuition_Report_Model->ListLabFeesEnrolled($academic_years_id, $academic_terms_id, $program_groups_id, NULL, $yr_level, $post_status);
	
		//log_message("INFO", "posting_lib LIBRARY ==>".print_r($data,true)); // Toyet 9.17.2018; 1.11.2019

		if ($value == 0) {
			$this->CI->content_lib->enqueue_body_content('posting/tuition_report',$data);
			$this->CI->content_lib->content();
		} else {
			$this->generate_report_pdf($data);
		}
	
	}



	private function generate_report_pdf($tuition) {

			$this->CI->load->library('my_pdf');
			$this->CI->load->library('hnumis_pdf');
			$l = 4;
		
			$this->CI->hnumis_pdf->AliasNbPages();		
			$this->CI->hnumis_pdf->set_HeaderTitle($tuition['post_description'].' ASSESSMENT REPORT FOR: '.$tuition['header']." [".$tuition['term']->term." ".$tuition['term']->sy."]");
			$this->CI->hnumis_pdf->AddPage('P','folio');
			
			$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->CI->hnumis_pdf->SetDrawColor(200,200,200);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();
			
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->SetTextColor(10,10,10);

			$this->CI->hnumis_pdf->SetFont('times','B',12);
			$this->CI->hnumis_pdf->SetFillColor(200,200,200);
			
			$this->CI->hnumis_pdf->Cell(200,$l+2,'# of Students: '.$tuition['students']->numEnroll,0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->SetFillColor(255,255,255);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();

			$this->CI->hnumis_pdf->Cell(200,$l,'Tuition & REED Fees: '.$tuition['students']->total_units,0,0,'L',true);
			$this->CI->hnumis_pdf->Ln();
	  		$tuition_fee = 0;
	  		$others_total = 0;
	  		$learning_total = 0;
	  		$support_total = 0;
	  		 
	  		if ($tuition['tuition_basics']) {
	  			$basic_total = 0;
	  			foreach ($tuition['tuition_basics'] AS $tuition_basic) {
					$this->CI->hnumis_pdf->SetX(14);
					$this->CI->hnumis_pdf->Cell(100,$l,'Tuition Fee ['.$tuition_basic->total_units." @ ".$tuition_basic->rate.'] ('.$tuition_basic->numEnroll.' students)',0,0,'L',true);
			  		$basic_total =  $basic_total + ($tuition_basic->total_units * $tuition_basic->rate);
					$this->CI->hnumis_pdf->Cell(25,$l,number_format(($tuition_basic->total_units * $tuition_basic->rate),2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($basic_total);	  
				$this->CI->hnumis_pdf->SetX(14);
				$this->CI->hnumis_pdf->Cell(100,$l,'Tuition Fee',0,0,'L',true);
				$this->CI->hnumis_pdf->Cell(50,$l,number_format($basic_total,2),0,0,'R',true);
			}

			$this->CI->hnumis_pdf->Ln();
			
			if ($tuition['tuition_others']) {
				foreach($tuition['tuition_others'] AS $other) {
					$this->CI->hnumis_pdf->SetX(14);
					$this->CI->hnumis_pdf->Cell(100,$l,$other->course_code.' Fee ['.$other->paying_units.
										' @ '.$other->rate.'] ('.$other->enrollees.' students)',0,0,'L',true);
					$total_others = $other->paying_units * $other->rate;
					$tuition_fee = $tuition_fee + $total_others;
					$this->CI->hnumis_pdf->Cell(50,$l,number_format($total_others,2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();
				}
			}		
			//$this->CI->hnumis_pdf->Cell(190,$l,number_format($tuition_fee,2),0,0,'R',true);
			//$this->CI->hnumis_pdf->Ln();

			if (!$tuition['tuition_others'] and $tuition['tuition_others_extracted']) {
				$total_others_extracted = 0;
				$_i = 1;
				$_iLen = sizeof($tuition['tuition_others_extracted']);
				//log_message("INFO",print_r($_iLen,true)); // Toyet 11.22.2018
				foreach($tuition['tuition_others_extracted'] AS $other_other2) {
					$this->CI->hnumis_pdf->SetX(14);
					$this->CI->hnumis_pdf->SetTextColor(255,0,0);
					$this->CI->hnumis_pdf->Cell(100,$l,$other_other2->course_code.' Fee ['.$other_other2->total_units.' @ '.$other_other2->rate.'] ('.$other_other2->numEnroll.' students)',0,0,'L',true);
					$total_others = $other_other2->total_units * $other_other2->rate;
					$total_others_extracted += $total_others;
					$this->CI->hnumis_pdf->Cell(25,$l,number_format($total_others,2),0,0,'R',true);
					if($_i < $_iLen ){
						$this->CI->hnumis_pdf->SetTextColor(0,0,0);
						$this->CI->hnumis_pdf->Ln();
					}
					$_i++;
				}
				$this->CI->hnumis_pdf->SetTextColor(255,0,0);
				$this->CI->hnumis_pdf->Cell(25,$l,number_format($total_others_extracted,2),0,0,'R',true);
				$this->CI->hnumis_pdf->SetTextColor(0,0,0);
				$tuition_fee += $total_others_extracted;
				$this->CI->hnumis_pdf->Cell(36,$l,number_format($tuition_fee,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			} 

			$this->CI->hnumis_pdf->Ln();

	  		if ($tuition['matriculation']) {
	  			$matri_total = 0;
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Matriculation, Misc & Other Fees:',0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
				foreach ($tuition['matriculation'] AS $matri) {
					$this->CI->hnumis_pdf->SetX(14);						
					$this->CI->hnumis_pdf->Cell(100,$l,'Matriculation @ '.number_format($matri->rate,2),0,0,'L',true);
					$matri_total = $matri_total + ($matri->numEnroll*$matri->rate);
					$this->CI->hnumis_pdf->Cell(50,$l,number_format($matri->numEnroll*$matri->rate,2),0,0,'R',true);				
					$this->CI->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($matri_total);
				$this->CI->hnumis_pdf->Cell(190,$l,number_format($matri_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
			
			
			if ($tuition['misc_fees']) {
				$this->CI->hnumis_pdf->SetX(14);						
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Miscellaneous Fees',0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
	  			$misc_total = 0;
	  			foreach($tuition['misc_fees'] AS $misc_fee) {
					$this->CI->hnumis_pdf->SetX(18);						
					$this->CI->hnumis_pdf->Cell(100,$l,$misc_fee->description." @ ".$misc_fee->rate." (".$misc_fee->enrollees." students)",0,0,'L',true);
					$misc_total = $misc_total + ($misc_fee->enrollees*$misc_fee->rate);
					$this->CI->hnumis_pdf->Cell(46,$l,number_format($misc_fee->enrollees*$misc_fee->rate,2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();
				}			
				$tuition_fee = $tuition_fee + ($misc_total);
				$this->CI->hnumis_pdf->Cell(190,$l,number_format($misc_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}

			
	  		if ($tuition['other_fees']) {
				$this->CI->hnumis_pdf->SetX(14);						
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Other School Fees',0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
	  			foreach($tuition['other_fees'] AS $other) {
					$this->CI->hnumis_pdf->SetX(18);						
					$this->CI->hnumis_pdf->Cell(100,$l,$other->description." @ ".$other->rate." (".$other->enrollees." students)",0,0,'L',true);
					$others_total = $others_total + ($other->enrollees*$other->rate);
					$this->CI->hnumis_pdf->Cell(46,$l,number_format($other->enrollees*$other->rate,2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($others_total);
				$this->CI->hnumis_pdf->Cell(190,$l,number_format($others_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}

			if ($tuition['learning_resources_fees']) {
				$this->CI->hnumis_pdf->SetX(14);
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Learning Resources Fees',0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
				foreach($tuition['learning_resources_fees'] AS $learning) {
					$this->CI->hnumis_pdf->SetX(18);
					$this->CI->hnumis_pdf->Cell(100,$l,$learning->description." @ ".$learning->rate." (".$learning->enrollees." students)",0,0,'L',true);
					$learning_total = $learning_total + ($learning->enrollees*$learning->rate);
					$this->CI->hnumis_pdf->Cell(46,$l,number_format($learning->enrollees*$learning->rate,2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($learning_total);
				$this->CI->hnumis_pdf->Cell(190,$l,number_format($learning_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
				
			if ($tuition['student_support_fees']) {
				$this->CI->hnumis_pdf->SetX(14);
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Student Support Services',0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
				foreach($tuition['student_support_fees'] AS $support) {
					$this->CI->hnumis_pdf->SetX(18);
					$this->CI->hnumis_pdf->Cell(100,$l,$support->description." @ ".$support->rate." (".$support->enrollees." students)",0,0,'L',true);
					$support_total = $support_total + ($support->enrollees*$support->rate);
					$this->CI->hnumis_pdf->Cell(46,$l,number_format($support->enrollees*$support->rate,2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($support_total);
				$this->CI->hnumis_pdf->Cell(190,$l,number_format($support_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
				
	  		if ($tuition['addl_other_fees']) {
				$this->CI->hnumis_pdf->SetX(14);						
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Additional Other Fees',0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
	  			$addl_total = 0;
	  			foreach($tuition['addl_other_fees'] AS $addl) {
					$this->CI->hnumis_pdf->SetX(18);						
					$this->CI->hnumis_pdf->Cell(100,$l,$addl->description.' @ '.$addl->rate.' ('.$addl->enrollees.' '.$addl->yr_level.'-Yr students)',0,0,'L',true);
					$addl_total = $addl_total + ($addl->enrollees*$addl->rate);
					$this->CI->hnumis_pdf->Cell(46,$l,number_format($addl->enrollees*$addl->rate,2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($addl_total);
				$this->CI->hnumis_pdf->Cell(190,$l,number_format($addl_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
			
			if ($tuition['lab_fees']) {
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Laboratory Fees:',0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
	  			$lab_total = 0;
	  			foreach($tuition['lab_fees'] AS $lab_fee) {
					$this->CI->hnumis_pdf->SetX(14);						
					$this->CI->hnumis_pdf->Cell(100,$l,$lab_fee->course_code." @ ".$lab_fee->rate." (".$lab_fee->enrollees." students)",0,0,'L',true);
					$lab_total = $lab_total + $lab_fee->fees;
					$this->CI->hnumis_pdf->Cell(50,$l,number_format($lab_fee->fees,2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();				
				}
				$tuition_fee = $tuition_fee + ($lab_total);
				$this->CI->hnumis_pdf->Cell(190,$l,number_format($lab_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
			
			/*
			 * ADDED: 7/15/15 by genes
			 */
			if ($tuition['affil_fees']) {
				$this->CI->hnumis_pdf->Cell(200,$l+2,'Affiliation Fees:',0,0,'L',true);
				$this->CI->hnumis_pdf->Ln();
	  			$affil_total = 0;
	  			foreach($tuition['affil_fees'] AS $affil_fee) {
					$this->CI->hnumis_pdf->SetX(14);						
					$this->CI->hnumis_pdf->Cell(100,$l,$affil_fee->description." @ ".$affil_fee->rate." (".$affil_fee->enrollees." students)",0,0,'L',true);
					$affil_total = $affil_total + $affil_fee->fees;
					$this->CI->hnumis_pdf->Cell(50,$l,number_format($affil_fee->fees,2),0,0,'R',true);
					$this->CI->hnumis_pdf->Ln();				
				}
				$tuition_fee = $tuition_fee + ($affil_total);
				$this->CI->hnumis_pdf->Cell(190,$l,number_format($affil_total,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
			
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->SetFont('times','B',12);
			$this->CI->hnumis_pdf->SetFillColor(200,200,200);
			$this->CI->hnumis_pdf->Cell(100,$l+2,'TOTAL Assessment: ',1,0,'L',true);
			$this->CI->hnumis_pdf->Cell(90,$l+2,number_format($tuition_fee,2),1,0,'R',true);
			$this->CI->hnumis_pdf->Cell(9,$l+2,'',1,0,'R',true);

			$this->CI->hnumis_pdf->Output();
	
	}



	private function generate_assessment_basic_ed_pdf($tuition_basics,
														$misc_fees,
														$other_fees,
														$addl_other_fees,
														$acad_years,
														$students,
														$post_description,
														$header) {
	
		$this->CI->load->library('my_pdf');
		$this->CI->load->library('hnumis_pdf');
		$l = 4;
	
		$this->CI->hnumis_pdf->AliasNbPages();
		$this->CI->hnumis_pdf->set_HeaderTitle($header." ".$acad_years->sy);
		$this->CI->hnumis_pdf->AddPage('P','letter');
			
		$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->CI->hnumis_pdf->SetDrawColor(200,200,200);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Ln();
			
		$this->CI->hnumis_pdf->SetFont('times','',10);
		$this->CI->hnumis_pdf->SetTextColor(10,10,10);
	
		$this->CI->hnumis_pdf->SetFont('times','B',12);
		$this->CI->hnumis_pdf->SetFillColor(200,200,200);
			
		$this->CI->hnumis_pdf->Cell(200,$l+2,'# of Students: '.$students->numEnroll,0,0,'L',true);
		$this->CI->hnumis_pdf->SetFont('times','',10);
		$this->CI->hnumis_pdf->SetFillColor(255,255,255);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Ln();
	
		$tuition_fee = 0;
		$others_total = 0;
		 
		if ($tuition_basics) {
			$basic_total = 0;
			foreach ($tuition_basics AS $tuition_basic) {
				$this->CI->hnumis_pdf->SetX(14);
				$this->CI->hnumis_pdf->Cell(100,$l,'Tuition Fee ['.$tuition_basic->level.'-'.$tuition_basic->yr_level.' @ '.$tuition_basic->rate.'] ('.$tuition_basic->numEnroll.' students)',0,0,'L',true);
		  		$basic_total =  $basic_total + ($tuition_basic->numEnroll * $tuition_basic->rate);
				$this->CI->hnumis_pdf->Cell(25,$l,number_format(($tuition_basic->numEnroll * $tuition_basic->rate),2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
			$tuition_fee = $tuition_fee + ($basic_total);
			$this->CI->hnumis_pdf->SetX(14);
			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->Cell(20,$l,'Tuition Fee',0,0,'L',true);
			$this->CI->hnumis_pdf->Cell(167,$l,number_format($basic_total,2),0,0,'R',true);
			$this->CI->hnumis_pdf->SetFont('times','',10);
				
		}
	
		$this->CI->hnumis_pdf->Ln();
			
		if ($misc_fees) {
			$this->CI->hnumis_pdf->SetX(14);
			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->Cell(200,$l+2,'Miscellaneous Fees',0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->Ln();
			$misc_total = 0;
			foreach($misc_fees AS $misc_fee) {
				$this->CI->hnumis_pdf->SetX(18);
				$this->CI->hnumis_pdf->Cell(100,$l,$misc_fee->description." @ ".$misc_fee->rate." (".$misc_fee->enrollees." students)",0,0,'L',true);
				$misc_total = $misc_total + ($misc_fee->enrollees*$misc_fee->rate);
				$this->CI->hnumis_pdf->Cell(46,$l,number_format($misc_fee->enrollees*$misc_fee->rate,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
			$tuition_fee = $tuition_fee + $misc_total;
			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->Cell(190,$l,number_format($misc_total,2),0,0,'R',true);
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->SetFont('times','',10);
		}
	
			
		if ($other_fees) {
			$this->CI->hnumis_pdf->SetX(14);
			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->Cell(200,$l+2,'Other School Fees',0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->Ln();
			foreach($other_fees AS $other) {
				$this->CI->hnumis_pdf->SetX(18);
				$this->CI->hnumis_pdf->Cell(100,$l,$other->description." @ ".$other->rate." (".$other->enrollees." students)",0,0,'L',true);
				$others_total = $others_total + ($other->enrollees*$other->rate);
				$this->CI->hnumis_pdf->Cell(46,$l,number_format($other->enrollees*$other->rate,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
			$tuition_fee = $tuition_fee + $others_total;
			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->Cell(190,$l,number_format($others_total,2),0,0,'R',true);
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->Ln();
		}
	
		if ($addl_other_fees) {
			$this->CI->hnumis_pdf->SetX(14);
			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->Cell(200,$l+2,'Additional Other Fees',0,0,'L',true);
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->Ln();
			$addl_total = 0;
			foreach($addl_other_fees AS $addl) {
				$this->CI->hnumis_pdf->SetX(18);
				$this->CI->hnumis_pdf->Cell(100,$l,$addl->description.' @ '.$addl->rate.' ('.$addl->enrollees.' students)',0,0,'L',true);
				$addl_total = $addl_total + ($addl->enrollees*$addl->rate);
				$this->CI->hnumis_pdf->Cell(46,$l,number_format($addl->enrollees*$addl->rate,2),0,0,'R',true);
				$this->CI->hnumis_pdf->Ln();
			}
			$tuition_fee = $tuition_fee + $addl_total;
			$this->CI->hnumis_pdf->SetFont('times','B',10);
			$this->CI->hnumis_pdf->Cell(190,$l,number_format($addl_total,2),0,0,'R',true);
			$this->CI->hnumis_pdf->SetFont('times','',10);
			$this->CI->hnumis_pdf->Ln();
		}
			
		$this->CI->hnumis_pdf->SetFont('times','B',11);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->SetFillColor(200,200,200);
		$this->CI->hnumis_pdf->Cell(100,$l+1,'TOTAL Assessment: ',1,0,'L',true);
		$this->CI->hnumis_pdf->Cell(90,$l+1,number_format($tuition_fee,2),1,0,'R',true);
		$this->CI->hnumis_pdf->Cell(9,$l+1,'',1,0,'R',true);
	
		$this->CI->hnumis_pdf->Output();
	
	}
	

	function posted_running($from_type=NULL) {
		
		$this->CI->load->model('hnumis/AcademicYears_Model');
		$this->CI->load->model('hnumis/College_Model');
		$this->CI->load->model('hnumis/programs_model');
		$this->CI->load->model('academic_terms_model');
		$this->CI->load->model('posting/Tuition_Report_Model');

		$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'mainpage');
			
		switch ($action) {
			case 'mainpage':	
			
				$data['academic_terms'] = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
				$data['colleges']       = $this->CI->College_Model->ListColleges();
				$data['current_term']   = $this->CI->academic_terms_model->getCurrentAcademicTerm();
				$data['report_type']    = $from_type;
				
				switch ($from_type) {
					case 'college':
						$this->CI->content_lib->enqueue_body_content('posting/posted_running_form_college',$data);
						break;
						
					case 'col_student':
						$this->CI->content_lib->enqueue_body_content('posting/posted_running_form_college',$data);
						break;

					case 'program':
						$this->CI->content_lib->enqueue_body_content('posting/posted_running_form_program',$data);
						break;

					case 'student':
						$this->CI->content_lib->enqueue_body_content('posting/posted_running_form_program',$data);
						break;
				}		
						
				$this->CI->content_lib->content();
				
				break;
			
			case 'extract_college_programs':

				$data['programs']      = $this->CI->programs_model->ListAcaProgramsbyCollege($this->CI->input->post('colleges_id'));
				
				$my_return['programs'] = $this->CI->load->view('audit/modal_views/select_programs', $data, TRUE);
						
				echo json_encode($my_return,JSON_HEX_APOS);
					
				return;
				
			case 'extract_posted_students':
			
				$this->CI->load->model('hnumis/enrollments_model');
				
				$data['students'] = $this->CI->enrollments_model->ListEnrolled_Posted($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'),$this->CI->input->post('year_level'));
					
				$my_return['students'] = $this->CI->load->view('audit/modal_views/list_of_students', $data, TRUE);
					
				echo json_encode($my_return,JSON_HEX_APOS);
			
				return;

			case 'extract_running_students':
			
				$this->CI->load->model('hnumis/enrollments_model');
				
				$data['students'] = $this->CI->enrollments_model->ListEnrolled_Running($this->CI->input->post('academic_terms_id'),$this->CI->input->post('academic_programs_id'),$this->CI->input->post('year_level'));
					
				$my_return['students'] = $this->CI->load->view('audit/modal_views/list_of_students', $data, TRUE);
					
				echo json_encode($my_return,JSON_HEX_APOS);
			
				return;

			case 'generate_assessment':
				
				$this->CI->load->model('trans_model');
				
				switch ($from_type) {
					case 'college':
						$data['reports'] = $this->CI->Tuition_Report_Model->generatePostedRunningAssessmentReport_ByCollege($this->CI->input->post('academic_terms_id'),$this->CI->input->post('colleges_id')); 

						$data['academic_terms_id'] = $this->CI->input->post('academic_terms_id');
						$data['college_name']      = $this->CI->input->post('college_name');
						$data['college_code']      = $this->CI->input->post('college_code');
						$data['term_sy']           = $this->CI->input->post('term_sy');

						$this->CI->content_lib->enqueue_body_content('posting/assessment_report_on_posted_running_college',$data);
						break;

					case 'col_student':
						$data['reports'] = $this->CI->Tuition_Report_Model->generatePostedRunningAssessmentReport_ByCollegeStudentList($this->CI->input->post('academic_terms_id'),$this->CI->input->post('colleges_id')); 

						$data['academic_terms_id'] = $this->CI->input->post('academic_terms_id');
						$data['college_name']      = $this->CI->input->post('college_name');
						$data['college_code']      = $this->CI->input->post('college_code');
						$data['term_sy']           = $this->CI->input->post('term_sy');

						$this->CI->content_lib->enqueue_body_content('posting/assessment_report_on_posted_running_col_student',$data);
						break;
						
					case 'program':	
						$data['reports'] = $this->CI->Tuition_Report_Model->generatePostedRunningAssessmentReport($this->CI->input->post('academic_terms_id'),$this->CI->input->post('acad_programs_id'),$this->CI->input->post('acad_program_groups_id'),$this->CI->input->post('max_yr_level')); 
				
						$data['academic_terms_id'] = $this->CI->input->post('academic_terms_id');
						$data['acad_programs_id']  = $this->CI->input->post('acad_programs_id');
						$data['college_name']      = $this->CI->input->post('college_name');
						$data['abbreviation']      = $this->CI->input->post('abbreviation');
						$data['description']       = $this->CI->input->post('description');
						$data['term_sy']           = $this->CI->input->post('term_sy');
						$data['max_yr_level']      = $this->CI->input->post('max_yr_level');
					
						$this->CI->content_lib->enqueue_body_content('posting/assessment_report_on_posted_running_program',$data);
						break;
						
					case 'student':
					
						$data['reports'] = $this->CI->Tuition_Report_Model->generatePostedRunningAssessmentReport_ByStudentList($this->CI->input->post('academic_terms_id'),$this->CI->input->post('acad_programs_id'),$this->CI->input->post('acad_program_groups_id'),1); 

						$data['academic_terms_id']      = $this->CI->input->post('academic_terms_id');
						$data['acad_programs_id']       = $this->CI->input->post('acad_programs_id');
						$data['acad_program_groups_id'] = $this->CI->input->post('acad_program_groups_id');
						$data['college_name']           = $this->CI->input->post('college_name');
						$data['abbreviation']           = $this->CI->input->post('abbreviation');
						$data['description']            = $this->CI->input->post('description');
						$data['term_sy']                = $this->CI->input->post('term_sy');
						$data['year_level']             = 1;
						
						for($x=1; $x<=$this->CI->input->post('max_yr_level'); $x++) {
							$yr_levels[$x] = $this->ordinalSuffix($x)." Year";
						}
					
						$data['yr_levels']  = $yr_levels;
						
						$this->CI->content_lib->enqueue_body_content('posting/assessment_report_on_posted_running_student',$data);
						break;
				}
				
				$this->CI->content_lib->content();

				break;
				
			case 'change_yr_level':
			
				$data['reports'] = $this->CI->Tuition_Report_Model->generatePostedRunningAssessmentReport_ByStudentList($this->CI->input->post('academic_terms_id'),$this->CI->input->post('acad_programs_id'),$this->CI->input->post('acad_program_groups_id'),$this->CI->input->post('yr_level')); 
		
				$data['academic_terms_id']      = $this->CI->input->post('academic_terms_id');
				$data['acad_programs_id']       = $this->CI->input->post('acad_programs_id');
				$data['acad_program_groups_id'] = $this->CI->input->post('acad_program_groups_id');
				$data['college_name']           = $this->CI->input->post('college_name');
				$data['abbreviation']           = $this->CI->input->post('abbreviation');
				$data['description']            = $this->CI->input->post('description');
				$data['term_sy']                = $this->CI->input->post('term_sy');
				$data['year_level']             = $this->CI->input->post('yr_level');
			
				$my_return['download'] = $this->CI->load->view('posting/views/download_link', $data, TRUE);
				$my_return['students'] = $this->CI->load->view('posting/views/list_of_students_with_posted_running_per_program', $data, TRUE);
					
				echo json_encode($my_return,JSON_HEX_APOS);
			
				return;
			
			case 'create_posted_running_excel':

				switch ($from_type) {
					case 'college':
						$data['college_name'] = $this->CI->input->post('college_name');
						$data['college_code'] = $this->CI->input->post('college_code');
						$data['term_sy']      = $this->CI->input->post('term_sy');
						$data['reports']      = json_decode($this->CI->input->post('reports'));

						$this->GenerateExcel_Posted_Running_College($data);
						break;
					
					case 'col_student':
						$data['college_name'] = $this->CI->input->post('college_name');
						$data['college_code'] = $this->CI->input->post('college_code');
						$data['term_sy']      = $this->CI->input->post('term_sy');
						$data['reports']      = json_decode($this->CI->input->post('reports'));

						$this->GenerateExcel_Posted_Running_College_Student($data);
						break;

					case 'program':
						$data['college_name'] = $this->CI->input->post('college_name');
						$data['description']  = $this->CI->input->post('description');
						$data['abbreviation'] = $this->CI->input->post('abbreviation');
						$data['term_sy']      = $this->CI->input->post('term_sy');
						$data['max_yr_level'] = $this->CI->input->post('max_yr_level');
						$data['reports']      = json_decode($this->CI->input->post('reports'));
						
						$this->GenerateExcel_Posted_Running_Program($data);
						break;

					case 'student':
						$data['college_name'] = $this->CI->input->post('college_name');
						$data['description']  = $this->CI->input->post('description');
						$data['abbreviation'] = $this->CI->input->post('abbreviation');
						$data['term_sy']      = $this->CI->input->post('term_sy');
						$data['year_level']   = $this->CI->input->post('year_level');
						$data['reports']      = json_decode($this->CI->input->post('reports'));
						
						$this->GenerateExcel_Posted_Running_Student($data);
						break;
						
				}
				
				break;
				
		}
		
		
	}


		private function GenerateExcel_Posted_Running_College($data) {
			$this->CI->load->library('PHPExcel');
			
			$title="College Assessment Summary Report";
			$max_col="Q";
			
			$filename = "downloads/assessment_".$data['college_code'].".xls"; 
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setTitle($title);
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			
			$objPHPExcel->getActiveSheet()->getPageMargins()
			->setTop(0.4)
			->setRight(0.4)
			->setLeft(0.4)
			->setBottom(0.4);
			
			$objPHPExcel->getActiveSheet()->getPageSetup()
			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
			->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title);
			$objPHPExcel->getActiveSheet()->SetCellValue('A2', $data['term_sy']);
			$objPHPExcel->getActiveSheet()->SetCellValue('A3', $data['college_name']);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
				->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)
				->setSize(14);

			$styleArray = array(
					'borders' => array(
							'inside'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							),
							'outline'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							)
					)
			);

			$row  = 5;
			$row1 = 6;
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_col.$row1)->applyFromArray($styleArray);

			$objPHPExcel->getActiveSheet()
				->getStyle('A'.$row.':'.$max_col.$row1)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('EEEEEE');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_col.$row1)
				->getAlignment()
				->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()
				->getStyle('A'.$row.':'.$max_col.$row1)
				->getAlignment()
				->setWrapText(true);

			$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(32);
			$objPHPExcel->getActiveSheet()->getRowDimension($row1)->setRowHeight(35);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'Course/Year Level');
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':A'.$row1);	
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'POSTED');
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);	

			$objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':'.$max_col.$row);	
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'RUNNING BALANCE');
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row1, '# of Students');
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row1, 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row1, '# of Students');
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row1, 'TF');
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row1, 'REED');
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row1, 'CWTS/MS');
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row1, 'Mat.');
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row1, 'Misc.');
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row1, 'OSF');
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row1, 'AOF');
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row1, 'LRF');
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row1, 'SSS');
			$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row1, 'AF');
			$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row1, 'Lab.');
			$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row1, 'TOTAL');
			$objPHPExcel->getActiveSheet()
				->getStyle('B'.$row)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('D99795');
			$objPHPExcel->getActiveSheet()
				->getStyle('B'.$row1.':C'.$row1)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('E6B9B8');
			$objPHPExcel->getActiveSheet()
				->getStyle('E'.$row)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('C2D69A');
			$objPHPExcel->getActiveSheet()
				->getStyle('E'.$row1.':'.$max_col.$row1)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('D7E4BC');

			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12.6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(1.2);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);

			$row=7;

			if ($data['reports']) {

				foreach($data['reports'] AS $programs) {
					if ($programs->for_programs) {
						
						$objPHPExcel->getActiveSheet()->getStyle('A'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						
						$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':C'.$row);	
						$objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':'.$max_col.$row);	

						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row.':C'.$row)->getBorders()
							->getAllBorders()
							->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						$objPHPExcel->getActiveSheet()
							->getStyle('E'.$row.':'.$max_col.$row)->getBorders()
							->getAllBorders()
							->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

						$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true)
							->setSize(11);

						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(18.75);

						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row)
							->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setRGB('BFBFBF');
						$objPHPExcel->getActiveSheet()
							->getStyle('E'.$row)
							->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setRGB('BFBFBF');

						$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row,$programs->abbreviation);
						
						$row++;
						
						foreach($programs->for_programs AS $program) {

							$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':B'.$row)
								->getAlignment()
								->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle('C'.$row)
								->getAlignment()
								->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
							$objPHPExcel->getActiveSheet()->getStyle('E'.$row)
								->getAlignment()
								->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':'.$max_col.$row)
								->getAlignment()
								->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

							$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()
								->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
							$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':'.$max_col.$row)->getNumberFormat()
								->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(18.75);
						
							$objPHPExcel->getActiveSheet()
								->getStyle('A'.$row.':'.$max_col.$row)->getBorders()
								->getAllBorders()
								->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row,$program->year_level);
							$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row,$program->num_posted_students);
							$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row,$program->total_posted);
							$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row,$program->num_running_students);
							$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row,$program->tuition_fee);
							$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row,$program->reed_fee);
							$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row,$program->cwts_fee);
							$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row,($program->num_running_students*$program->mat_rate));
							$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row,($program->num_running_students*$program->misc_rate));
							$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row,($program->num_running_students*$program->osf_rate));
							$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row,($program->num_running_students*$program->aof_rate));
							$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row,($program->num_running_students*$program->lrf_rate));
							$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row,($program->num_running_students*$program->sss_rate));
							$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row,$program->affil_fees);
							$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row,$program->lab_fee);
							$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row,"=SUM(F".$row.":P".$row.")" );  
							
							$row++;
						}
					}
				}
			}
			
			$objPHPExcel->getActiveSheet()->mergeCells('D5:D'.($row-1));	
			$objPHPExcel->getActiveSheet()
				->getStyle('D5')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('393939');

			$objWriter->save($filename,__FILE__);
			
			return;
		}  	


		private function GenerateExcel_Posted_Running_College_Student($data) {

			$this->CI->load->library('PHPExcel');
			$title="College Assessment Summary Report - Student List";
			$max_row="K";

			$filename = "downloads/assessment_".$data['college_code']."_students.xls"; 

			//deletes the old uploaded file
			unlink($filename);

			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setTitle($title);
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			
			$objPHPExcel->getActiveSheet()->getPageMargins()
			->setTop(1)
			->setRight(0.4)
			->setLeft(0.4)
			->setBottom(1);
			
			$objPHPExcel->getActiveSheet()->getPageSetup()
			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
			->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title);
			$objPHPExcel->getActiveSheet()->SetCellValue('A2', $data['term_sy']);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
				->setSize(14);

			
			$styleArray = array(
					'borders' => array(
							'inside'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							),
							'outline'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							)
					)
			);

			$row  = 4;
			$row1 = 5;

			if ($data['reports']) {

				foreach($data['reports'] AS $program) {
					
					if (isset($program->program_students)) {

						$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true)
							->setSize(12);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(22.5);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
							
						$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $program->abbreviation."-".$program->year_level);
						
						$row++; $row1++;
				
						$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_row.$row1)->applyFromArray($styleArray);

						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row.':'.$max_row.$row1)
							->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setRGB('EEEEEE');

						$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_row.$row1)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row.':'.$max_row.$row1)
							->getAlignment()
							->setWrapText(true);
				
						$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_row.$row1)->getFont()->setBold(true);

						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(21.75);
						$objPHPExcel->getActiveSheet()->getRowDimension($row1)->setRowHeight(21.75);
						$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'Count');
						$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':A'.$row1);	
						$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'ID Number');
						$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':B'.$row1);	
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Lastname');
						$objPHPExcel->getActiveSheet()->mergeCells('C'.$row.':C'.$row1);	
						$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'Firstname');
						$objPHPExcel->getActiveSheet()->mergeCells('D'.$row.':D'.$row1);	
						$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Middlename');
						$objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':E'.$row1);	
						$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'POSTED');
						$objPHPExcel->getActiveSheet()->mergeCells('F'.$row.':H'.$row);	
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'RUNNING');
						$objPHPExcel->getActiveSheet()->mergeCells('I'.$row.':K'.$row);	
						$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row1, 'Course');
						$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row1, 'Year');
						$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row1, 'Amount');
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row1, 'Course');
						$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row1, 'Year');
						$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row1, 'Amount');

						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row.':'.$max_row.$row1)
							->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setRGB('C3C3C3');

						$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);
						$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11.5);
						$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18.5);
						$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18.5);
						$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18.5);
						$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12.5);
						$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
						$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
						$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12.5);
						$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
						$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);

						$row++; $row1++;
						$cnt=1;
						
						$row++; $row1++;
						
						foreach($program->program_students AS $student) {
							
							if (number_format($student->amount_posted,2) != number_format($student->amount_running,2)) {
								$objPHPExcel->getActiveSheet()
									->getStyle('A'.$row.':'.$max_row.$row)
									->getFill()
									->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
									->getStartColor()->setRGB('FF8686');						
							}
					
							$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':B'.$row)
									->getAlignment()
									->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
									->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':F'.$row)
									->getAlignment()
									->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
									->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
							$objPHPExcel->getActiveSheet()->getStyle('G'.$row)
									->getAlignment()
									->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
									->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle('H'.$row)
									->getAlignment()
									->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
									->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
							$objPHPExcel->getActiveSheet()->getStyle('I'.$row)
									->getAlignment()
									->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
									->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
							$objPHPExcel->getActiveSheet()->getStyle('J'.$row)
									->getAlignment()
									->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
									->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle('K'.$row)
									->getAlignment()
									->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
									->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

							$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->getNumberFormat()
									->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
							$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->getNumberFormat()
									->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(18.75);
							
							$objPHPExcel->getActiveSheet()
									->getStyle('A'.$row.':'.$max_row.$row)->getBorders()
									->getAllBorders()
									->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row,$cnt.".");
							$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row,$student->idno);
							$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row,$student->lname);
							$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row,$student->fname);
							$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row,$student->mname);
							$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row,$student->course_posted);
							$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row,$student->yr_level_posted);
							$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row,$student->amount_posted);
							$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row,$student->course_running);
							$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row,$student->yr_level_running);
							$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row,$student->amount_running);
								
							$row++; $row1++;
							
							$cnt++;
						}
						
					}
					
					$row++; $row1++;

				}
				
			}
			
			$objWriter->save($filename,__FILE__);
			
			return;
		}  	

		
		private function GenerateExcel_Posted_Running_Program($data) {
			$this->CI->load->library('PHPExcel');
			
			$title="Program Assessment Summary Report";
			$max_row="Q";
			
			$filename = "downloads/assessment_".$data['abbreviation'].".xls";
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setTitle($title);
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			
			$objPHPExcel->getActiveSheet()->getPageMargins()
			->setTop(1)
			->setRight(0.4)
			->setLeft(0.4)
			->setBottom(1);
			
			$objPHPExcel->getActiveSheet()->getPageSetup()
			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
			->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title);
			$objPHPExcel->getActiveSheet()->SetCellValue('A2', $data['term_sy']);
			$objPHPExcel->getActiveSheet()->SetCellValue('A3', $data['description']);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
				->setSize(14);

			$styleArray = array(
					'borders' => array(
							'inside'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							),
							'outline'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							)
					)
			);

			$row  = 7;
			$row1 = 8;
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_row.$row1)->applyFromArray($styleArray);

			$objPHPExcel->getActiveSheet()
				->getStyle('A'.$row.':'.$max_row.$row1)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('EEEEEE');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_row.$row1)
				->getAlignment()
				->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()
				->getStyle('A'.$row.':'.$max_row.$row1)
				->getAlignment()
				->setWrapText(true);

			$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(32);
			$objPHPExcel->getActiveSheet()->getRowDimension($row1)->setRowHeight(35);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'Year Level');
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':A'.$row1);	
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'POSTED');
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);	
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$row.':D'.($row1+$data['max_yr_level']));	
			$objPHPExcel->getActiveSheet()
				->getStyle('D'.$row)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('898787');
			$objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':'.$max_row.$row);	
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'RUNNING BALANCE');
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row1, '# of Students');
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row1, 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row1, '# of Students');
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row1, 'TF');
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row1, 'REED');
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row1, 'CWTS/MS');
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row1, 'Mat.');
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row1, 'Misc.');
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row1, 'OSF');
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row1, 'AOF');
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row1, 'LRF');
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row1, 'SSS');
			$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row1, 'AF');
			$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row1, 'Lab.');
			$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row1, 'TOTAL');
			$objPHPExcel->getActiveSheet()
				->getStyle('B'.$row)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('D99795');
			$objPHPExcel->getActiveSheet()
				->getStyle('B'.$row1.':C'.$row1)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('E6B9B8');
			$objPHPExcel->getActiveSheet()
				->getStyle('E'.$row)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('C2D69A');
			$objPHPExcel->getActiveSheet()
				->getStyle('E'.$row1.':'.$max_row.$row1)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('D7E4BC');

			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6.6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12.6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(4);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);

			$row=9;

			if ($data['reports']) {

				foreach($data['reports'] AS $k=>$v) {
					foreach($v->programs AS $program) {

						$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':B'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						$objPHPExcel->getActiveSheet()->getStyle('C'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$objPHPExcel->getActiveSheet()->getStyle('E'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':'.$max_row.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

						$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()
							->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
						$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':'.$max_row.$row)->getNumberFormat()
							->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(18.75);
					
						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row.':'.$max_row.$row)->getBorders()
							->getAllBorders()
							->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					
						$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row,$program->year_level);
						$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row,$program->num_posted_students);
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row,$program->total_posted);
						$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row,$program->num_running_students);
						$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row,$program->tuition_fee);
						$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row,$program->reed_fee);
						$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row,$program->cwts_fee);
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row,($program->num_running_students*$program->mat_rate));
						$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row,($program->num_running_students*$program->misc_rate));
						$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row,($program->num_running_students*$program->osf_rate));
						$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row,($program->num_running_students*$program->aof_rate));
						$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row,($program->num_running_students*$program->lrf_rate));
						$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row,($program->num_running_students*$program->sss_rate));
						$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row,$program->affil_fees);
						$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row,$program->lab_fee);
						$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row,"=SUM(F".$row.":P".$row.")" );  
						
						$row++;
					}
				}
			}
			
			
			$objWriter->save($filename,__FILE__);
			
			return;
		}  	


		private function GenerateExcel_Posted_Running_Student($data) {

			$this->CI->load->library('PHPExcel');

			$title="Program Assessment Summary Report - Student List";
			$max_row="K";
			
			$filename = "downloads/assessment_".$data['abbreviation']."_".$data['year_level'].".xls";

			//deletes the old uploaded file
			unlink($filename);

			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setTitle($title);
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			
			$objPHPExcel->getActiveSheet()->getPageMargins()
			->setTop(1)
			->setRight(0.4)
			->setLeft(0.4)
			->setBottom(1);
			
			$objPHPExcel->getActiveSheet()->getPageSetup()
			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
			->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title);
			$objPHPExcel->getActiveSheet()->SetCellValue('A2', $data['term_sy']);
			$objPHPExcel->getActiveSheet()->SetCellValue('A3', $data['description']);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)
				->setSize(14);

			$styleArray = array(
					'borders' => array(
							'inside'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							),
							'outline'     => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
									'color' => array(
											'argb' => '00000000'
									)
							)
					)
			);

			$row  = 6;
			$row1 = 7;
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_row.$row1)->applyFromArray($styleArray);

			$objPHPExcel->getActiveSheet()
				->getStyle('A'.$row.':'.$max_row.$row1)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('EEEEEE');

			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_row.$row1)
				->getAlignment()
				->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()
				->getStyle('A'.$row.':'.$max_row.$row1)
				->getAlignment()
				->setWrapText(true);
				
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$max_row.$row1)->getFont()->setBold(true);

			$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(21.75);
			$objPHPExcel->getActiveSheet()->getRowDimension($row1)->setRowHeight(21.75);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'Count');
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':A'.$row1);	
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'ID Number');
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':B'.$row1);	
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Lastname');
			$objPHPExcel->getActiveSheet()->mergeCells('C'.$row.':C'.$row1);	
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'Firstname');
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$row.':D'.$row1);	
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Middlename');
			$objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':E'.$row1);	
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'POSTED');
			$objPHPExcel->getActiveSheet()->mergeCells('F'.$row.':H'.$row);	
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'RUNNING');
			$objPHPExcel->getActiveSheet()->mergeCells('I'.$row.':K'.$row);	
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row1, 'Course');
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row1, 'Year');
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row1, 'Amount');
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row1, 'Course');
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row1, 'Year');
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row1, 'Amount');

			$objPHPExcel->getActiveSheet()
				->getStyle('A'.$row.':'.$max_row.$row1)
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('C3C3C3');

			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12.5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);

			$row=8;

			if ($data['reports']) {

				$cnt=1;
				foreach($data['reports'] AS $student) {

					if (number_format($student->amount_posted,2) != number_format($student->amount_running,2)) {
						$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row.':'.$max_row.$row)
							->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()->setRGB('FF8686');						
					}
					
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':B'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':F'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$row)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->getNumberFormat()
							->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->getNumberFormat()
							->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(18.75);
					
					$objPHPExcel->getActiveSheet()
							->getStyle('A'.$row.':'.$max_row.$row)->getBorders()
							->getAllBorders()
							->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row,$cnt.".");
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row,$student->idno);
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row,$student->lname);
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row,$student->fname);
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row,$student->mname);
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row,$student->course_posted);
					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row,$student->yr_level_posted);
					$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row,$student->amount_posted);
					$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row,$student->course_running);
					$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row,$student->yr_level_running);
					$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row,$student->amount_running);
						
					$row++; $cnt++;
				}
				
			}
			
			$objWriter->save($filename,__FILE__);
			
			return;
		}  	

		
		private function ordinalSuffix( $n ) {
			return $n.date('S',mktime(1,1,1,1,( (($n>=10)+($n>=20)+($n==0))*10 + $n%10) ));
		}
	
}

?>