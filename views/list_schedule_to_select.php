  <?php echo validation_errors(); 
  ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; ">
  <table align="left" cellpadding="2" cellspacing="0" style="width:35%; border:solid; border-width:1px; border-color:#060; margin-top:10px; margin-left:20px;">
<tr>
        	<td width="199">Academic Terms</td>
        	<td width="15">:</td>
            <td width="438"><?php print($academic_terms->term." ".$academic_terms->sy); ?></td>
        </tr>
    </table>
  <br />
  <br />
<br />


  <table border="0" cellspacing="0" class="table table-striped table-bordered" style="width:60%; padding:0px; margin:0px;">
   <thead>
    <tr>
      <th style="width:15%; text-align:center; vertical-align:middle;">Course/Section</th>
      <th style="width:40%; text-align:center; vertical-align:middle;" >Schedule</th>
      <th style="width:10%; text-align:center; vertical-align:middle;" >Enrolled/<br />
        Capacity</th>
      <th style="width:35%; text-align:center; vertical-align:middle;" >Faculty</th>
    </tr>
    </thead>
    <?php
		if ($offerings) {
			foreach ($offerings AS $offer) {
	?>
    <tr>
      <td style="text-align:center; vertical-align:middle; ">
	  <?php 
			switch ($this->uri->segment(2)) {
				case 'assign_faculty_to_schedule':
					print("<a class=\"schedule_slot\" href=\"{$offer->id}\">{$offer->course_code}[{$offer->section_code}]</a>");
					break;
				case 'offer_schedule_slots':
					print("<a class=\"schedule_slot\" href=\"{$offer->id}\">{$offer->course_code}[{$offer->section_code}]</a>");
					break;
				case 'view_class_list':
					print("<a class=\"schedule_slot\" href=".site_url('dean/view_class_list/'.$offer->id).">".$offer->course_code." [".$offer->section_code."]</a>");
					break;
				case 'view_course_offerings':
					print($offer->course_code." [".$offer->section_code."]");
					break;				
			}
		?></td>
      <td style="text-align:left; vertical-align:middle; ">
      	<?php
			$offering_slots = $this->Offerings_Model->ListOfferingSlots($offer->id);
			foreach($offering_slots AS $slots) {
				print($slots->tym." ".$slots->days_day_code." - [".$slots->room_no."]<br>");
			}
		?>
      </td>
      <td style="text-align:center; vertical-align:middle; "><?php 
	  //print($offer->enrollee."/");
	  print($offer->max_students); ?></td>
      <td style="text-align:left; vertical-align:middle; "><?php print($offer->employees_empno." ".$offer->emp_name); ?></td>
    </tr>
    <?php
			}
		}
	?>
  </table>
  </div>
  <form id="srcform2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.schedule_slot').click(function(event){
		event.preventDefault(); 
		var offering_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'offering_id',
		    value: offering_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>