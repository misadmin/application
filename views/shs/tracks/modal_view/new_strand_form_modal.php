
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Track</div>
			<div class="col2a">
				<select class="form-control" style="width:auto; height:auto;" id="track_id_add_modal" >
					<?php 
						if ($tracks) {
							foreach ($tracks AS $track) {
					?>		
								<option value="<?php print($track->id); ?>" ><?php print($track->group_name); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Abbreviation</div>
			<div class="col2a">
				<input type="text" class="form-control" id="abbreviation_modal" style="width:100px;" />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Description</div>
			<div class="col2a">
				<input type="text" class="form-control" id="description_modal" style="width:350px;" />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Max. Year Level</div>
			<div class="col2a">
				<input type="text" class="form-control" id="max_yr_level_modal" style="width:20px;" />
			</div>
		</div>
	</div>
