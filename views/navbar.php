	<header>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container-fluid">
					<a class="brand" href="<?php echo site_url($role); ?>"><img src="<?php echo base_url("assets/img/hnuseal_icon_green.png");?>" alt="logo"/> <?php echo $this->config->item('application_title'); ?></a>
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="nav-collapse collapse">
						<ul class="nav user_menu">
								<li<?php echo (in_array($this->uri->segment(2),array('','index')) ? ' class="active"' : '') ?>><a href="<?php echo site_url("{$role}"); ?>">Home</a></li>
						<?php if (isset($menu) && count($menu) > 0): ?>
							<?php foreach ($menu as $key=>$menuitem): ?>
								<?php if(is_array ($menuitem)): ?>
								<li class="dropdown<?php echo (in_array($this->uri->segment(2),$menuitem) ? ' active' : '') ?>" role="navigation">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $key; ?><b class="caret"></b></a>
									<ul class="dropdown-menu">
									<?php foreach($menuitem as $subkey=>$submenuitem): ?>
										<?php if(is_array($submenuitem)): ?>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $subkey; ?><b class="caret-right"></b></a>
											<ul class="dropdown-menu">
												<?php foreach($submenuitem as $subsubkey => $subsubmenu): ?>
												<li><a href="<?php echo site_url("{$role}/{$subsubmenu}"); ?>"><?php echo $subsubkey; ?></a></li>												
												<?php endforeach; ?>
											</ul>
										</li>
										<?php else: ?>
										<li<?php echo ($this->uri->segment(2) == $submenuitem ? ' class="active"' : '') ?>><a href="<?php echo site_url("{$role}/{$submenuitem}"); ?>"><?php echo $subkey; ?></a></li>
										<?php endif; ?>
									<?php endforeach; ?>
									</ul>
								</li>
								<?php else: ?>
								<li<?php echo ($this->uri->segment(2) == $menuitem ? ' class="active"' : '') ?>><a href="<?php echo site_url("{$role}/{$menuitem}"); ?>"><?php echo $key; ?></a></li>
								<?php endif; ?>
							<?php endforeach; ?>
							<?php endif; ?>
						</ul>
						<ul class="nav user_menu pull-right">
							<li class="dropdown" role="navigation">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $name; ?><b class="caret"></b></a>
								<ul class="dropdown-menu">
									<?php if  ($this->uri->segment(1) != 'student' && (int)$this->userinfo['empno'] < 20000): ?>
										<li class="dropdown" role="navigation">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">Login As<b class="caret"></b></a>									
											<ul class="dropdown-menu">
												<?php foreach ($this->userinfo['roles_list'] as $row): ?>
													<li><a href="<?php echo site_url("{$role}/login_as/$row"); ?>"><?php echo $row; ?></a></li>
												<?php endforeach; ?>				
											</ul>
										</li>
									<?php endif; ?>
									<?php if ( isset($this->my_sms_privileges) && count($this->my_sms_privileges) > 0):?>
									<li><a href="<?php echo site_url("{$role}/sendsms"); ?>">Send SMS</a></li>
									<?php endif; ?>								
									<li><a href="<?php echo site_url("{$role}/tools"); ?>">Tools</a></li>
									<li class="divider"></li>
									<li><a href="<?php echo site_url("{$role}/logout"); ?>">Log Out</a></li>
								</ul>
							</li>
						</ul>
						<?php if (isset($avatar_image)): //avatar image?>
						<ul class="nav user_menu pull-right">
							<img src="<?php echo $avatar_image ?>" style="
							    max-width: 25px;
							    max-height: 25px;
							    margin-top: 7px;
							    border-radius: 2px;
							">
						</ul>
						<?php endif ?>
						<?php if (isset($college) && $college!="None"): ?>
						<ul class="nav user_menu pull-right">
							<li><a href="<?php echo site_url("{$role}"); ?>"><?php echo ucfirst($role); ?> (<?php echo $college; ?>)</a></li>
						</ul>
						<?php else: ?>
						<ul class="nav user_menu pull-right">
							<li><a href="<?php echo site_url("{$role}"); ?>"><?php echo ucfirst($role); ?></a></li>
						</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</header>
<script>
$(document).ready(function(){
	$('.dropdown-menu li').each(function(){
		var $this = $(this);
		if($this.children('ul').length) {
			$this.addClass('sub-dropdown');
			$this.children('ul').addClass('sub-menu');
		}
	});

	$('.sub-dropdown').on('mouseenter',function(){
		$(this).addClass('active').children('ul').addClass('sub-open');
	}).on('mouseleave', function() {
		$(this).removeClass('active').children('ul').removeClass('sub-open');
	});
});

</script>
