<?php

	class Behavior_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		
		function ListObservedValues($student_histories_id=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.id,
							a.description,
							(SELECT 
									COUNT(*) 
								FROM 
									behavior_statements 
								WHERE 
									core_values_id = a.id
							) AS cnt
						FROM 
							core_values AS a		
						ORDER BY 
							a.id ";
			//return $sql;
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				$result = $query->result();
							
				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						$myresult = $this->ListBehaviorStatements($res->id,$student_histories_id);
						
						if ($myresult) {
							$result[$cnt]->behaviors = $myresult;
						}									
						$cnt++;
					}
				}	
			}
			
			return $result;
			
		}
		
		
		private function ListBehaviorStatements($core_values_id=NULL,$student_histories_id) {
			$result=null; 
			
			$q1 = "SELECT 
							a.id AS behavior_statements_id,
							a.description AS behavior_statement,
							(SELECT 
									x.midterm_marking 
								FROM 
									students_behavior_statements AS x 
								WHERE 
									x.student_histories_id = {$this->db->escape($student_histories_id)}
									AND x.behavior_statements_id = a.id
							) AS midterm_marking,
							(SELECT 
									x.finals_marking 
								FROM 
									students_behavior_statements AS x 
								WHERE 
									x.student_histories_id = {$this->db->escape($student_histories_id)}
									AND x.behavior_statements_id = a.id
							) AS finals_marking
						FROM 
							behavior_statements AS a 
						WHERE 
							a.core_values_id = {$this->db->escape($core_values_id)}
						ORDER BY 
							a.core_values_id,
							a.id ";
			//return $q1;
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->result();			
			}
				
			return $result;
			
		}
		

		function Add_StudentBehaviorStatements($data) {

			switch ($data['period']) {
				
				case 'Midterm':
					
					$q1 = "INSERT INTO
								students_behavior_statements (
														student_histories_id,
														behavior_statements_id,
														midterm_marking,
														encoded_by)
							VALUES (
								{$this->db->escape($data['student_histories_id'])},
								{$this->db->escape($data['behavior_statements_id'])},
								{$this->db->escape($data['marking'])},
								{$this->db->escape($data['encoded_by'])}) 
							ON DUPLICATE KEY UPDATE 
								midterm_marking = {$this->db->escape($data['marking'])},
								encoded_by = {$this->db->escape($data['encoded_by'])} ";
					
					break;
					
				case 'Finals':
					
					$q1 = "INSERT INTO
								students_behavior_statements (
														student_histories_id,
														behavior_statements_id,
														finals_marking,
														encoded_by)
							VALUES (
								{$this->db->escape($data['student_histories_id'])},
								{$this->db->escape($data['behavior_statements_id'])},
								{$this->db->escape($data['marking'])},
								{$this->db->escape($data['encoded_by'])}) 
							ON DUPLICATE KEY UPDATE 
								finals_marking = {$this->db->escape($data['marking'])},
								encoded_by = {$this->db->escape($data['encoded_by'])} ";
					
					break;
					
			}

			//return $q1;
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		
		}
	}
	
?>	