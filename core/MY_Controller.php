<?php 
/**
 * Class MY_Controller
 * 
 * Controller that should be the parent of all employee type controllers.
 * Inherits the CI_Controller class.
 * 
 * @author HNU-MIS Team
 * @package HNU-MIS
 * @subpackage core
 * @since version 1
 */
class MY_Controller extends CI_Controller {
	protected $navigation;
	protected $navbar_data;
	public $userinfo;
	public $role;
	public $status_schedule;
	//date_default_timezone_set('UP8');
	public $my_sms_privileges = array();
	
	public function __construct(){
		parent::__construct();
		$this->common->need_auth(); //if I'm authenticated then I'll proceed else I should be authenticated...
		$this->userinfo = $this->session->all_userdata();
		$this->role = $this->uri->segment(1);
		//print_r($this->userinfo);die();
		
		//the are edited (and commented some lines) by tatskie:
		
		//this will most likely be overriden by children constructor...		
		$this->navbar_data = array(
				'name'			=> $this->userinfo['fname'] . " " . $this->userinfo['lname'],
				'role'			=> $this->role,
				'college' 		=> in_array($this->userinfo['role'],array('dean','faculty')) ? $this->userinfo['college_code'] : NULL,
			);  
		
		//$college = (! is_null($this->userinfo['college_code']) ? $this->userinfo['college_code'] : "");
		//if (! empty($college))
		//	$this->navbar_data['college'] = $college; 
		
		$this->load->model('academic_terms_model');
		$this->load->library('breadcrumb_lib');
		//$this->breadcrumb_lib->enqueue_item('home',humanize($this->role),'./');
		
		$current_academic_term = $this->academic_terms_model->getCurrentAcademicTerm();
		$this->status_schedule = $this->academic_terms_model->what_period() . " &middot; " . $current_academic_term->term . " &middot; SY " . $current_academic_term->sy;
		
		$this->load->model('hrdmo/smsprivileges_model', 'sms_privileges');
		$this->my_sms_privileges = $this->sms_privileges->privileges( $this->session->userdata('empno'));
	}

	/**
	 * Dashboard
	 * 
	 * This will be shown to the user having an employee role.
	 */
	public function index(){
		$this->load->model('common_model');
		
		//$this->content_lib->enqueue_body_content('',$this->breadcrumb_lib->content());		
		//$featured_gf = $this->common_model->featured_gf(); 
		
		$this->load->model('employee_common_model');
		$birthdays = array('results' => $this->employee_common_model->upcoming_birthdays()); 
		$last_logins = array('results' => $this->employee_common_model->last_logins($this->userinfo['empno']));
		$this->content_lib->set_title ('Dashboard | ' . $this->config->item('application_title'));
				
		$data = $this->employee_common_model->my_information($this->userinfo['empno']);
		$working_student = false;
		if ($this->userinfo['empno'] < 20000) {
			$folder = $this->config->item('employee_images_folder');
			$file = FCPATH . $folder . ltrim($this->userinfo['empno'], '0') . ".jpg";
		} else {
			$port = substr($this->userinfo['empno'], 0, 3);
			$folder = $this->config->item('student_images_folder') . "{$port}/";
			$file = FCPATH . $folder . $this->userinfo['empno'] . ".jpg";
			$working_student = true;
		}
		
		if (is_file($file)){
			if ($this->userinfo['empno'] < 20000)
				$image = base_url($folder . ltrim($this->userinfo['empno'], '0') . ".jpg"); else 
				$image = base_url($folder . $this->userinfo['empno'] . ".jpg");
		} else { 
			//$image = base_url($this->config->item('no_image_placeholder_female')); 
			//todo: add gender to employees table...
			$image = base_url($this->config->item('no_image_placeholder_male'));
		}
		
		$data['image'] = $image;
		$data['profile_image_max_width'] = $this->config->item('profile_image_max_width');		
		
		
		
		if ( ($feedbacks_count = $this->common_model->get_feedbacks($this->userinfo['empno'])) == TRUE ){
			$data['feedbacks_count'] = count($feedbacks_count);
			$feedbacks_count = null; unset($feedbacks_count ); //faster and better way to free memory. make this a habit, okay?  
		}else{
			$data['feedbacks_count'] = 0;				
		}		
		
		$recipients = $this->common_model->list_recipients();
		$data['recipients'] = array();		
		$data['recipients'][0]='--Select Recipient--';
		
		if ($recipients){
			foreach ($recipients as $recipient){
				$data['recipients'][$recipient->recipient_id] = $recipient->recipient;
			}	
		}else{
			die('something wrong on your query!');
		}
		
		
		$this->content_lib->enqueue_body_content('common/employee_profile', $data);
		//added by Tatskie for App Users Feedbacking on July 23, 2013
		if ($this->input->post()){
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				if($action = $this->input->post('action')){
					switch ($action){
						case 'new_feedback':
							$comment = sanitize_text_field($this->input->post('comment'));
							$column_to_insert = $working_student ? 'students_idno':'employees_empno';
							if (trim($comment) == '')
								$this->content_lib->set_message('Feedback field is required!', 'alert-error');
								
							else{
								if ( (int)$this->input->post('recipient_id') !== 0 ) {
									if($insert_id = $this->common_model->add_feedback($this->userinfo['empno'],$comment,$column_to_insert, $this->db->escape($this->input->post('recipient_id')) )){
										$this->content_lib->set_message('Feedback successfully submitted!', 'alert-success');
									}else{
										$this->content_lib->set_message('Error in submission!', 'alert-error');
									}
								}else{
									$this->content_lib->set_message('Please select recipient of your feedback!', 'alert-error');
								}
							}
							break;
					}
				}
					
			}else{
				$this->content_lib->set_message('ERROR: Possibly a resubmission of the same form!','alert-error');
			}	
		} 
		
		
		$this->load->library('form_lib');
		$comment = array();

		
		$this->content_lib->enqueue_sidebar_widget('common/policies', $policies, 'Policies and Memoranda', 'in');
		//$this->content_lib->enqueue_header_script('zrssfeed');
		//$this->content_lib->enqueue_sidebar_widget('trial_content',array('feed'=>'http://www.inquirer.net/fullfeed'), 'Recent News', 'in');			
		$this->content_lib->enqueue_sidebar_widget('common/last_logins', $last_logins, 'Your Last Logins:', 'out');
		$this->content_lib->enqueue_sidebar_widget('feedbacks/feedbacks_view', $data, 'Submit Feedback', 'out');
		//$this->content_lib->enqueue_sidebar_widget('feedbacks/fbcomments_view', array(), 'FB Comments', 'out');
		$this->content_lib->enqueue_sidebar_widget('common/birthdays', $birthdays, 'Upcoming Birthdays', 'out');
		//$this->content_lib->enqueue_sidebar_widget('trial', array('image'=>$featured_gf[0]->featured_image), 'Featured Student', 'in');
		
						
		$this->content_lib->content();
		
	}
	
	
	/**
	 * Tools
	 * Aside from the dashboard is the most common "grouped action" of any role...
	 * @param string $action specified action
	 */
	public function tools(){
		
		$action = $this->input->post('action');		
		if (method_exists($this, $action)) {
			//action is a private method...
			call_user_func_array(array($this, $action), array());
		} else {
			//show tools...
			$data = array(
					'name'	=>	$this->userinfo['fname'] . " " . $this->userinfo['lname'],
					'role'	=>	$this->role,
					'errors' => array(),
			);
			$this->content_lib->set_title ('Tools | ' . $this->config->item('application_title'));
			
			
			$this->load->model('common_model');
			
			
			
			switch ($this->input->post('action')){
				case "Delete":
					if ($deleted = $this->common_model->delete_feedbacks($this->input->post('check')) AND $deleted > 0)
						$this->content_lib->set_message("Successfully deleted $deleted rows!","alert-success");
					else
						$this->content_lib->set_message('Unable to delete!','alert-error');
					break;
				case "Mark as Resolved":
					if ($resolved = $this->common_model->mark_resolved($this->input->post('check'))  AND $resolved>0)
						$this->content_lib->set_message('Successfully marked as Resolved!','alert-success');
					else
						$this->content_lib->set_message('Unable to mark as resolved!','alert-error');
					break;
				case "Unresolve":
					if ($resolved = $this->common_model->unresolve($this->input->post('check'))  AND $resolved>0)
						$this->content_lib->set_message('Successfully unresolved!','alert-success');
					else
						$this->content_lib->set_message('Unable to unresolve!','alert-error');
					break;
				case "Remark":
					if ($dev_remark = $this->common_model->dev_remark($this->input->post('id_to_update'),$this->input->post('dev_remark')) AND $dev_remark>0)
						$this->content_lib->set_message("Successfully posted!","alert-success");
					else
						$this->content_lib->set_message("Unable to post response!","alert-error");
					break;
			}
			
			
			
			$this->load->library('tab_lib');				
			
			if ( ($data['data'] = $this->common_model->get_feedbacks($this->userinfo['empno'])) == TRUE ){				
				$feedbacks_count = count($data['data']);
								
				$passwordtab_active = $feedbacks_count > 0 ? FALSE : TRUE ;
								
			}else{
				$passwordtab_active = TRUE;				
			}
			
			$this->load->library('table_lib');
			$this->load->library('form_lib');				
			
			$this->tab_lib->enqueue_tab ('Password Settings', 'common/tools', $data, 'password', $passwordtab_active);
			$this->tab_lib->enqueue_tab ('Feedbacks', 'feedback_view', $data, 'feedbacks', !$passwordtab_active);
				
				
			$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
			$this->content_lib->enqueue_footer_script ('password_strength');
			$this->content_lib->enqueue_footer_script('data_tables');
				
			$this->content_lib->content();
		}
	}
	
public function sendsms( $privilege='' ){
		if( empty($this->my_sms_privileges) ){
			//User has no SMS privileges
			$this->session->set_flashdata('error_message', "You don't have enough privileges to access this feature.");
			redirect('error/401');
		}
		if ( !empty($privilege) && !in_array( $privilege, $this->my_sms_privileges)){
			//privilege specified is NOT a privilege of the user...
			$this->session->set_flashdata('error_message', "You don't have this privilege to access this feature.");
			redirect('error/401');
		}
		
		//process inputs...
		if ( $this->input->post('action') ) {
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				$this->load->library('sms_client', $this->config->item('sms_api_config'));
				$this->load->model('sms_model');
				switch ($this->input->post('action')){
					case 'send_sms_to_anybody' :
						
						
						
						$recepient = substr( str_replace(" ", "", $this->input->post('recepient')), -10 );
						$message = str_replace('[message]', $this->input->post('message'), $this->config->item('sms_message_template'));
						$sticky = ($this->input->post('priority')=='yes');
						if ($return = $this->sms_client->send_message ( $recepient, $message, $sticky)){
							$return = json_decode($return);
							if ($return->success===TRUE)
								$this->content_lib->set_message( "Message sent.", 'alert-success'); else
								$this->content_lib->set_message( "Error was seen while sending message. Message not sent.", 'alert-error');
						} else {
							$this->content_lib->set_message( "Error was seen while sending message. Message not sent.", 'alert-error');
						}
						break;
					case 'send_to_all_current_college_students' :
						$recepients = $this->input->post('recepients');
						$message = $this->input->post('message');
						$sticky = $this->input->post('priority');
						if(empty($message)){
							$this->content_lib->set_message( "You sent an empty message.", 'alert-error');
							break;
						}
						$recepients_numbers = array();
						if ( $recepients == '"all"' ){
							$recepients_numbers = $this->sms_model->phone_numbers_given_program();
						} else {
							$recepients = json_decode($recepients);
							$recepients_numbers = array();
							foreach($recepients as $key => $year_levels){
								foreach($year_levels as $year_level){
									$recepients_numbers = array_merge($recepients_numbers, $this->sms_model->phone_numbers_given_program($key, $year_level));
								}
							}
						}
						$message = str_replace("[message]", $message, $this->config->item('sms_message_template'));
						$return = json_decode($this->sms_client->send_message_to_many($message, $recepients_numbers, $sticky=='yes'));
						if($return->success===TRUE)
							$this->content_lib->set_message( $return->message, 'alert-success'); else 
							$this->content_lib->set_message( $return->message, 'alert-error');
						break;
				}
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
			}
		}
		$this->load->library('tab_lib');
		$this->content_lib->enqueue_footer_script('maskedinput');
		$my_sms_privileges = $this->my_sms_privileges;
		$sms_privileges = $this->config->item('sms_privileges');
		$active_tab = $privilege;
		$tab_count = 0;
		if ( in_array('send_to_anybody', $my_sms_privileges) ){
			$content = 'common/sms_sending/send_to_anybody';
			$this->tab_lib->enqueue_tab ( $sms_privileges['send_to_anybody'], $content, array(), 'send_to_anybody-tab', (empty($active_tab) || $active_tab=='send_to_anybody'), '', site_url("{$this->role}/sendsms/send_to_anybody"));
			$index = array_search('send_to_anybody', $my_sms_privileges);
			unset($my_sms_privileges[$index]);
			$tab_count++;
		}
		foreach ($my_sms_privileges as $my_privilege){
			$content = '';
			if($tab_count==0 || $active_tab == $my_privilege){
				$content = "common/sms_sending/{$my_privilege}";
			}
			$this->tab_lib->enqueue_tab ( $sms_privileges[$my_privilege], $content, $this->sms_content($my_privilege), "{$my_privilege}-tab", ($tab_count==0 || $active_tab==$my_privilege), '', site_url("{$this->role}/sendsms/{$my_privilege}"));
			$tab_count++;
		}
		$this->content_lib->enqueue_body_content('common/sms_sending/sms_container', array(
				'content' => $this->tab_lib->content()
				));
		$this->content_lib->content();
	}
	
	private function change_password(){
		
		$this->load->model('employee_common_model');
		$data = array(
				'name'	=>	$this->userinfo['fname'] . " " . $this->userinfo['lname'],
				'role'	=>	$this->role,
				'title'	=>	'Tools',
		);		
		$old_password = $this->input->post('password');
		$new_password = $this->input->post('newpassword1', TRUE);
		$new_password2 = $this->input->post('newpassword2', TRUE);
		
		//server side validation...
		$errors = array();
		if ( ! $this->common->nonce_is_valid($this->input->post('nonce')))
			$errors[] = 'Nonce is invalid. ';
		
		if ( ! $this->employee_common_model->user_is_valid($this->userinfo['empno'], $old_password, $this->role))
			$errors[] = 'Old password did not match. ';
		
		if ( $new_password != $new_password2 )
			$errors[] = 'Passwords did not match. ';
		
		//regular expression validation...
		$regex = "#[' \-]#";
		if (preg_match($regex, $new_password))
			$errors[] = 'Invalid Characters found. ';
		
		if (count($errors) == 0) {
			$result = $this->employee_common_model->update_information($this->userinfo['empno'], array('password'=>$new_password));
			if ( ! $result)
				$errors[] = 'Error found while updating password. '; 
		}
		
		if ( count($errors) > 0 )
			$data['errors'] = $errors; else 
			$data['success'] = TRUE;
		
		$this->load->library('tab_lib');
			
		$this->tab_lib->enqueue_tab ('Password Settings', 'common/tools', $data, 'password', TRUE);
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->enqueue_footer_script ('password_strength');
		$this->content_lib->content();
	}
	
	public function login_as($new_role){
		//FIXME: this is a security threat. What about if the user adds: login_as/ric when this is NOT one of his/her role. He/She will be able to login as that role. 
		//Check whether that new role is in his/her roles.
		if (!in_array($new_role,$this->userinfo['roles_list'])){
			$this->content_lib->set_message('Your intention to login using a prohibited role is a failure. This illegal activity has been logged!','alert-error');			
			$this->content_lib->content();				
			redirect(site_url($this->userinfo['role']));
		}
		 
		
		if (in_array($new_role,array('dean','faculty'))){			
			$this->load->model('employee_common_model');
			$college_data = $this->employee_common_model->get_college_data($this->userinfo['empno'],$new_role);			
			if ($college_data){				
				$this->session->set_userdata('college_id', $college_data['college_id']);
				$this->session->set_userdata('college_code', $college_data['college_code']);
				$this->session->set_userdata('college_name', $college_data['college_name']);				
			}			
		}		
		$this->session->set_userdata('role',$new_role);
		redirect(site_url($new_role));		
	}
	
	private function sms_content($privilege){
		$this->load->model('hnumis/college_model');
		switch ($privilege){
			case 'send_to_all_current_college_students' :
				$colleges = $this->college_model->all_colleges();
				foreach ($colleges as $college){
					$programs[$college->id] = $this->college_model->programs_from_college($college->id, 'O');
				}
				return array('colleges'=>$colleges, 'programs'=>$programs);
				break;
		}
	}
	
	public function __destruct(){
		$this->db->close();
	}
}



