<form method="post" action="" id="tax_type_form" class="form-inline" style="margin-bottom:0">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<div class="row-fluid">
		<div class="span12">
			<input type="hidden" name="action" value="change_tax_type" />
			<select name="tax_type" id="tax_type">
				<option value="vat" <?php if($tax_type == 'vat') echo 'selected="selected"'; ?>>VAT</option>
				<option value="nonvat" <?php if($tax_type == 'nonvat') echo 'selected="selected"'; ?>>NON-VAT</option>
			</select>
			<span style="font-weight:bold">
			<?php
			if ( !empty($receipt_no))
				echo 'OR# <span id="receipt_no">'. $receipt_no . "</span>";
			else
				echo 'Set OR# <a href="' . site_url('teller/settings') . '">here</a>.';
			?>
			</span>
		</div>
	</div>
</form>
<script>
	$('#tax_type').change(function(){
		$('#tax_type_form').submit();
	});
</script>