<?php 

class Av_model extends CI_Model {
	private $last_error;

	public function __construct(){
		parent::__construct();
	}



	public function list_bookings($year,$month){
		$total_days=cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$daily_data=array();		
		for ($i=1;$i<=$total_days;$i++){
			$date    =  $year.'-'.str_pad($month, 2, '0',STR_PAD_LEFT).'-'.str_pad($i, 2, '0',STR_PAD_LEFT);
			$sql = "
				select for_approval, approved, disapproved, sum(for_approval + approved + disapproved + noshow) as total_bookings
				from (			
					select 
						sum(b.`status`='for approval') for_approval,
						sum(b.`status`='approved') approved,
						sum(b.`status`='disapproved') disapproved,
						sum(b.`status`='no show') noshow	
					from av_bookings b
					where b.activity_date = {$this->db->escape($date)}	
					) t
			";			
			$query = $this->db->query($sql);
			$daily_data[$date] = $query->result();						
		}		
		return $daily_data;
	}
	
	public function list_approved_booking_of_the_day($date,$location){
		$sql = "
			select concat(e.fname,' ',e.lname) as booked_by, time_start, time_end 
			from av_bookings b
			left join employees e on e.empno = b.booked_by 
			where b.status='approved' and b.activity_date = {$this->db->escape($date)}	
			and location = {$this->db->escape($location)}										
		";
		
		$query = $this->db->query($sql);
		if ($query){
			if ($query->num_rows() == 0)
				return 1;
			else			
				return $query->result();
		}else{
			return false;
		}
		
		
	}
	
	public function list_all_bookings_of_the_day($date){
		$sql = "
			SELECT lpad(b.id,4,'0') id, ifnull(b.location,'???') location, b.activity_date, b.activity_name activity, b.time_start, b.time_end,
				if(!isnull(emp.empno),concat(substring(emp.fname,1,1), '. ', emp.lname),'Uknown') as booked_by,
				ifnull(emp.empno,''), empno, ifnull(size,'') size, substr(b.booked_on,1,16) booked_on, b.status,
				ifnull(b.equipment,'') equipment,
				ifnull(b.remark,'') remark
			FROM av_bookings b
			left join employees emp on emp.empno = b.booked_by  
			WHERE b.activity_date = {$this->db->escape($date)}
			order by b.location, b.time_start, b.booked_on
		";
		//return '$sql'; die;		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows()>0)
			//return $query->result();
			return $query;
		else
			return false;
	}
	
	public function add_booking($activity_date,$activity_time_s,$activity_time_e,$activity_name,$equipment,$size,$remark,$location){
		$sql = "
			insert into av_bookings (location,activity_date,time_start,time_end,activity_name,equipment,size,remark,booked_by) values (
			{$this->db->escape($location)},
			{$this->db->escape($activity_date)},
			{$this->db->escape($activity_time_s)},
			{$this->db->escape($activity_time_e)},
			{$this->db->escape($activity_name)},
			{$this->db->escape($equipment)},
			{$this->db->escape($size)},
			{$this->db->escape($remark)},
			{$this->session->userdata('empno')})
		";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}
	

	public function approve_booking($id){
		$sql = "
			update av_bookings set status='approved' where status='for approval' and id = {$id}
			";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function disapprove_booking($data){
		$sql = "
			update av_bookings set status='disapproved' where id in ({$data})
		";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function mark_forapproval($data){
		$sql = "
		update av_bookings set status='for approval' where id in ({$data})
		";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function mark_didnotshowup($data){
		$sql = "
			update av_bookings set status='no show' where status='approved' and id in ({$data}) 
		";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function mark_cancelled($data){
		$sql = "
			update av_bookings set status='cancelled' where status not in ('approved','no show') and id in ({$data})
		";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	
	
	public function list_possible_conflicts($id){
		$sql = "
			select av.id, av.location, av.activity_date, av.time_start, av.time_end, av.booked_by
			from av_bookings av 
			where av.activity_date in (select b.activity_date from av_bookings b where id={$this->db->escape($id)})
			and av.`status`='approved'
			union all
			select id, location, activity_date, time_start, time_end, booked_by
			from av_bookings av2 where id ={$this->db->escape($id)}			
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows()>0)
			return $query->result();
		else
			return false;
		
	}
	
	public function get_approval_location(){
		$sql = "
				select location from av_admins where empno = {$this->db->escape($this->session->userdata('empno'))} 
			";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows()>0)
			return $query->result_array();
		else
			return false;		
	}
	
	
	public function booking_history($empno){
		$sql="
			select b.id, concat(emp.fname,' ',emp.lname) as booked_by, b.activity_date date, b.location, 
				lower(concat(DATE_FORMAT(b.time_start,  '%h:%i%p'),'-',DATE_FORMAT(b.time_end, '%h:%i%p'))) `time`, b.`status`
			from av_bookings b
			left join employees emp on emp.empno = b.booked_by
			where b.booked_by={$this->db->escape($empno)} 			
			order by b.activity_date desc, time			
		";
		$query = $this->db->query($sql);
		if ($query)
			return $query->result();
		else
			return false;		
	}
	
	
}

?>
