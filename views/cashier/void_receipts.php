<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>

<div style="background:#FFF; width:60%; margin-left:20px;">

<div style="width:100%;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td align="left" style="font-size:16px; font-weight:bold;">&nbsp;</td>
    </tr>
</table>

</div>
<div style="width:100%; margin-bottom:40px;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px; ">
	<thead>
		<th colspan=3 style="text-align:center"> RECEIPT DETAILS </th>
	</thead>
	<tr >
		  <td width="26%" >Receipt Number </td>
		  <td width="1%" >:</td>
		  <td width="73%" ><?php print ($details->receipt_no); ?></td>
	</tr>
	<tr >
		  <td >Transaction Date</td>
		  <td >:</td>
		  <td ><?php print($details->date); ?></td>
	</tr>
	<tr >
		  <td >Payee</td>
		  <td >:</td>
		  <td ><?php print($details->name); ?></td>
	</tr>
	<tr >
		  <td >Payment for </td>
		  <td >:</td>
		  <td ><?php print($details->description); ?> </td>
	</tr>
	<tr >
		  <td >Amount</td>
		  <td >:</td>
		  <td style="font-weight:bold;"><?php print(number_format($details->receipt_amount,2,'.',',')); ?></td>
	</tr>
	 <thead>
		<td colspan="3" align="center"> <input id="void_receipt" type="button" name="void_receipt" value="Void Receipt!" /> </td>
	</thead>
	
</table>

</div>

</div>

  

<form action="<?php echo site_url("cashier/void_receipts")?>" method="post" id="void_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="3" name="step" />
	<input type="hidden" name="payment_id" value="<?php echo $details->id?>" />
</form>
	
<script>
$('#void_receipt').click(function(event){
	event.preventDefault();
	var confirmed = confirm("Are you sure you want to void the receipt?");

	if (confirmed){
		$('#void_form').submit();
	}		
});
</script>


