<?php
$this->form_lib->set_id('new_payers');
$this->form_lib->set_attributes(array('method'=>'post','action'=>'#','class'=>'form-horizontal'));
$this->form_lib->enqueue_hidden_input('nonce', $this->common->nonce());
$this->form_lib->enqueue_hidden_input('action', 'add_account', array('id'=>'the-action'));

$accounts_input = array('name' => 'account_name',
		'label' => 'Account Name',
		'rules' => array('required'),
		'placeholder'=> 'Account Name',
		'class'=>'input-xxlarge',
);
$this->form_lib->enqueue_text_input($accounts_input);
$account_type = array(
		'name'=>'account_type',
		'label'=>'Account Type',
		'options'=>$this->config->item('payers')
		);

$this->form_lib->enqueue_select_input($account_type);
$this->form_lib->set_submit_button('Add','btn btn-success');
$this->form_lib->add_control_class('formSep');
?>

<h2 class="heading">Add New Account</h2>
<?php echo $this->form_lib->content() ?>