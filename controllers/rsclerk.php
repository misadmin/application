<?php 

class Rsclerk extends MY_Controller {
	
	private $enroll=FALSE; //variable is used to check if Student->Academics->Enroll is clicked 
	private $enroll_stat = FALSE;
	
	
	public function __construct(){
		parent::__construct();		
		$this->content_lib->set_title ('RSClerk | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_footer_script('validate');
		$this->content_lib->enqueue_footer_script('autocomplete');
		$this->content_lib->enqueue_footer_script('jquery-ui');
		
		if ($this->session->userdata('role')=='rsclerk'){
			$this->navbar_data['menu'] = array(
					'Students'=>array('Students' => 'student',
										'New Student' => array('Basic Education'=>'bed_enrolment',
														'Senior High School' => 'new_shs_student',
										),				
					),
					'Reports'=>array('Students/Pupils Registered-Not Enrolled' => 'registered_not_enrolled',
									'Students/Pupils Enrolled-Not Sectioned' => 'enrolled_not_sectioned',
									'Class List' => 'bed_class_list',
									'Students/Pupils Transferred Out' => 'student_list_transferOut',
									'Students/Pupils Dropped Out' => 'student_list_dropOut',
									'Enrollment Summary' => 'enrollment_summary_basic_ed',
					),	
					'Set Dates'=>array('Transfer Out Cut-off Date' => 'set_cut_off_transfer',
										'Set Examination Dates' => 'set_exam_dates',
										//'View Examination Dates' => 'view_exam_dates',
										'Update Examination Dates' => 'update_exam_dates',
								
					),
					'Updates'=>array('Master File on Sections' => 'add_new_section',
							'Faculty Advisers' => 'academic_sections',
							// 'Update Enrollment Schedule' => 'update_enrollment_date',
							'Update Transfer Out Cut-Off Date' => 'update_cut_off_transfer',
					),
					
					
			);
		}
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		
	}

	
	
	public function student(){
			
		$this->content_lib->set_title('RS Clerk | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		
		//print("Q: ".$this->input->post('q')."<br>".$idnum); die();
		if ( ! empty($query)){
			
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
		
			
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
			
			$this->load->library('pagination_lib');
			
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
				
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else { 
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start, 
							'end'		=>$end, 
							'total'		=>$total, 
							'pagination'=>$pagination, 
							'results'	=>$res, 
							'query'		=>$query
						);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			}
		}
		if (is_numeric($idnum)){
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
			$this->load->model('hnumis/Student_model');
			$this->load->model('teller/teller_model');
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/Terms_Model');
			$this->load->helper('student_helper');
			$this->load->library('tab_lib');
				
			$this->load->library('form_lib');
			$this->content_lib->enqueue_header_style('image_area_select');
			$this->content_lib->enqueue_footer_script('image_area_select');
			$port = substr($idnum, 0, 3);
			$this->content_lib->set_title ('RSClerk | ' . $idnum);
			
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');
			
			$religions 		= $this->Student_model->ListReligions();
			$citizenships 	= $this->Student_model->ListCitizenship();

			$result 		= $this->student_common_model->my_information($idnum);

			if ($result !== FALSE) {
				//a user with that id number is seen...
				$grade_level = array(11,12);

				if ($result->bed_id != NULL) { 
				
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname)  . ", "  . $result->fname . " ". $result->mname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> $result->section_name,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address'	=>$result->full_city_address,
							'phone_number'	=>$result->phone_number,
							'section'	=>$result->section,
					);
				} else {
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> $result->fname . " " . $result->mname . " " . $result->lname,
							'firstname' => $result->fname,
							'familyname' => $result->lname,
							'middlename' => $result->mname,
							'course'	=>'',
							'birthdate' => $result->dbirth,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> $result->year_level,
							'full_home_address'	=> $result->full_home_address,
							'full_city_address' => $result->full_city_address,
							'phone_number' 		=> $result->phone_number,						
							'section' 			=> $result->section,
							'bed_status' 		=> $result->bed_status, //added by tatskie. this is very important for the user to know		
							'religions'			=> 	$religions,
							'citizenships'		=> 	$citizenships,
					);
				}
				
				$data['student_current_history'] = NULL;
				
				if ($result->bed_id == NULL) { 
					if ($data['student_current_history'] = $this->enrollments_model->student_current_history($idnum)){					
						$dcontent['level'] = $data['student_current_history']->department . " " . $data['student_current_history']->yr_level;		
					}
				}
				
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"); 
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else 
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}

				
				//todo: promote with a test..
				$tab_no 				= array_fill(0,26,FALSE);
				$tab_no['password']		= FALSE;
				$tab_no[0] 				= TRUE;
				$dcontent['signature'] 	= base_url('assets/img/signature.png');
				
				
				if (!is_college_student($idnum)) {
					
					if ($data['student_current_history']) {
						$bed_status             = $this->student_common_model->get_my_bed_status($data['student_current_history']->history_id);
						$dcontent['bed_status'] = $bed_status->status;
					} else {
						$bed_status             = NULL;
						$dcontent['bed_status'] = NULL;
					}
					
				}
				/* end here... */
				
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...
			
				
				//see to it that rsclerk activities are for basic_ed students only
				if (!is_college_student($idnum)) 
				{
				
					if ($this->student_model->student_is_basic_ed($idnum)) {

						$this->load->model('common_model');
						$this->load->model('Places_model');
						$this->load->model('hnumis/bed/bed_student_model');
						$this->load->model('hnumis/bed/section_model');
						
						//for basic info, other info, address, emergency, family bground
						$religions 		= $this->common_model->religions();
						$citizenships 	= $this->common_model->citizenships();
						$countries 		= $this->Places_model->countries();
						$philippine_id 	= $this->Places_model->country_id('Philippines');
						$provinces 		= $this->Places_model->fetch_results($philippine_id, 'provinces');
						
						$return1 		= array('tab'=>'0');
						
						$meta = json_decode($result->meta, TRUE);
						
						$return = array(
								'firstname'		=>$result->fname,
								'familyname'	=>$result->lname,
								'middlename'	=>$result->mname,
								'course'		=>$result->abbreviation,
								'religions'		=>$religions,
								'citizenships'	=>$citizenships,
								'religions_id'	=>$result->religions_id,
								'citizenships_id'=>$result->citizenships_id,
								'birthdate'		=>$result->dbirth,
								'gender'		=>$result->gender,
								'civil_status'	=>$result->civil_status,
								'countries'		=>$countries,
								'place_of_birth'=>$result->full_birth_address,
								'philippine_id'	=>$philippine_id,
								'provinces'		=>$provinces,
								'home_address'	=>$result->full_home_address,
								'city_address'	=>$result->full_city_address,
								'level'			=>$result->year_level,
								'full_birth_address'	=> $result->full_birth_address,
								'full_city_address'		=> $result->full_city_address,
								'full_home_address'		=> $result->full_home_address,
								'stay_with'		=> $result->stay_with,
								'phone_number'	=> $result->phone_number,
								'section'		=> $result->section,
				
						);
						
						$studinfo = array_merge ($return, $return1);

						if (is_array($meta)){
							$studinfo = array_merge($studinfo, $meta);				
						}
						//end data for basic info
						
						//data for assign section
						$histories = $this->bed_student_model->checkStudentHistory($idnum);

						$data['students_idno'] 			= $idnum;
						$data['enrolled_status']		= $histories[0]->enrolled_status;
						$data['basic_ed_histories_id']	= $histories[0]->id;
						$data['acad_program_groups_id']	= $histories[0]->acad_program_groups_id;
						$data['academic_years_id']		= $histories[0]->academic_years_id;
						
						if ($histories[0]->section_name) {
							$data['section_name'] = $histories[0]->description."-".$histories[0]->section_name;
						} else {
							$data['section_name'] = NULL;
						}
						
						
						$data['sections']				= $this->section_model->ListSections($histories[0]->acad_program_groups_id,$histories[0]->academic_years_id);
						//end data for assign section
					
						$this->tab_lib->enqueue_tab ('Basic Information', 'rsclerk/student_information_form', $studinfo, 'student_info', $tab_no[0]);

						$this->tab_lib->enqueue_tab('Other Info',
													'',
													array(),
													'otherInfo',
													$tab_no[1]);
					
						$this->tab_lib->enqueue_tab ("Address Info", "rsclerk/student_info_addresses", $studinfo, "student_info_addresses", $tab_no[2], 'otherInfo');
						$this->tab_lib->enqueue_tab ("Emergency Info", "rsclerk/emergency_contact", $studinfo, "emergency_contact",  $tab_no[3], 'otherInfo');
						$this->tab_lib->enqueue_tab ("Family Background", "rsclerk/family_information", $studinfo, "family_information",  $tab_no[4], 'otherInfo');

						$this->tab_lib->enqueue_tab ('Assign Section', 'bed/student/tabs/assign_section', $data, 'assign_section', $tab_no[5]);

						$tab_content = $this->tab_lib->content();
						
						$this->content_lib->enqueue_body_content("", $tab_content);
						
					} else {
						$this->content_lib->set_message('Student record cannot be determine! Please refer to IRMC.', 'alert-error');
					}
					
				} 
				else 
				{	
					//NOTE: senior high students are considered college students because college and shs students share the same histories (student_histories table)
					if (in_array($result->year_level,$grade_level)) { //student is SHS

						$this->load->model('hnumis/shs/shs_student_model');

						$student   = $this->shs_student_model->get_LRN($idnum);
						
						if ($student) {
							$lrn = $student->lrn;
						} else {
							$lrn = NULL;
						}
						
						$my_tab    = array_fill(0,3,FALSE);
						$my_tab[1] = TRUE;
						
						$this->tab_lib->enqueue_tab ('Student Information', 'common/student_information_full', $result, 'student_info',$my_tab[1]);						

						$this->tab_lib->enqueue_tab ('Assign LRN', 'shs/student/assign_lrn', 
																array('students_idno'=>$idnum,
																	'lrn'=>$lrn,
																	), 'assign_lrn',$my_tab[2]);						

						$tab_content = $this->tab_lib->content();
						
						$this->content_lib->enqueue_body_content("", $tab_content);
						
					} else {
						$this->content_lib->set_message('Student records are in College! Refer to Digital-Records in Charge (DRIC)!', 'alert-error');
					}	
				}				
				
				//end of student check for basic_ed
			
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}
		
		//what will happen if there is no query... and the result is not numeric?
		if ( empty($query) && ! is_numeric($idnum)) {
		
		}
		
		if ($this->enroll_stat) {
			$this->content_lib->set_message($this->enroll_msg, 'alert-success');			
			$this->enroll_stat = FALSE;
		} 
		
		$this->content_lib->content();	
	}
	
	
	/*
	 * ADDED: 4/12/15 
	 * by genes
	 * @Updated: 3/24/2019 genes
	 */
	public function enrolled_not_sectioned() {
		$this->load->library('bed/enrolment_lib');
	
		$this->enrolment_lib->enrolled_not_sectioned();
	
	}

	
	/*
	 * ADDED: 4/14/15 
	 * by genes
	 * @Updated: 3/24/2019 genes
	 */
	public function registered_not_enrolled() {
		$this->load->library('bed/enrolment_lib');
	
		$this->enrolment_lib->registered_not_enrolled();
	
	}
	
	
	public function bed_class_list() {
		$this->load->library('bed/enrolment_lib');
	
		$this->enrolment_lib->class_list();		
	}
		
	//Added: April 15, 2014 by Amie
	public function section_list() {
		print('Still on the development phase ....'); die();
		
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('basic_ed/basic_ed_enrollments_model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('hnumis/Terms_Model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of level and year level
				$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears(3);
				$data['levels'] = $this->Terms_Model->ListBasicLevels();
				$this->content_lib->enqueue_body_content('rsclerk/search_academic_year', $data);
				$this->content_lib->content();
				break;
			case 2:
	
				$cur_yr_id = $this->input->post('academic_year');
				$academic_year_info = $this->AcademicYears_Model->GetAcademicYear($cur_yr_id);
				$level = $this->input->post('levels');
				
				$data['students'] = $this->basic_ed_enrollments_model->list_section_summary($data);
				//$this->content_lib->enqueue_body_content('rsclerk/registered_report',$data);
				$this->content_lib->content();
				break;
		}
	}
	
	//Added: 4/12/2014 by Isah
	public function download_basicEd_enrolled_csv (){
		
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
		
		$data['cur_yr_id'] = $this->input->post('sy_id');
		$data['cur_yr'] = $this->input->post('cur_yr');
		$data['end_yr'] = $data['cur_yr'] + 1;
		$data['basic_level'] = $this->input->post('basic_level');
		$data['yr_level'] = $this->input->post('yr_level');
		$data['level_id'] = $this->input->post('level_id');
	
		$records = $this->enrollments_model->ListBasicStudentsPromoted($data);
		//print_r($records); die();
		$filename = "masterListEnrolled_" .$data['basic_level']. $data['yr_level']. "-". $data['cur_yr'] . "-" . $data['end_yr'] . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
		fputcsv($output, array('List of Enrolled Students'));
		if($data['level_id']== 4 OR $data['level_id']== 5){
			switch($data['yr_level']){
				case 1: $yearLevel = '1st'; break;
				case 2: $yearLevel = '2nd'; break;
				case 3: $yearLevel = '3rd'; break;
				case 4: $yearLevel = '4th'; break;
			}
			fputcsv($output, array($yearLevel, $data['basic_level']));
		}else 
			fputcsv($output, array($data['basic_level'], $data['yr_level']));
		fputcsv($output, array(' '));
		fputcsv($output, array('No.', 'ID No.', 'Name', 'Year Level', 'Section', 'Gender'));
	
		$count = 1;
		foreach ($records as $record){
			fputcsv ($output, array($count, $record->students_idno, $record->neym, $data['yr_level'], $record->section_name, $record->gender));
			$count++;
		}
	}
	
	//Added: 5/30/2014 by Isah
	public function download_basicEd_enrolled_not_sectioned_csv (){
	
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$data['cur_yr_id'] = $this->input->post('sy_id');
		$data['cur_yr'] = $this->input->post('cur_yr');
		$data['end_yr'] = $data['cur_yr'] + 1;
		$data['basic_level'] = $this->input->post('basic_level');
		$data['yr_level'] = $this->input->post('yr_level');
		$data['level_id'] = $this->input->post('level_id');
		
		if($data['level_id']== 4 OR $data['level_id']== 5)
			switch($data['yr_level']){
				case 1: $yearLevel = '1st'; break;
				case 2: $yearLevel = '2nd'; break;
				case 3: $yearLevel = '3rd'; break;
				case 4: $yearLevel = '4th'; break;
			}
		$records = $this->enrollments_model->ListBasicStudentsPromotedNotSectioned($data);
		//print_r($records); die();
		$filename = "masterListNotSectioned_" .$data['basic_level']. $data['yr_level']. "-". $data['cur_yr'] . "-" . $data['end_yr'] . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
		
		fputcsv($output, array('List of Enrolled Students But Not Sectioned'));
		if($data['level_id']== 4 OR $data['level_id']== 5){
			switch($data['yr_level']){
				case 1: $yearLevel = '1st'; break;
				case 2: $yearLevel = '2nd'; break;
				case 3: $yearLevel = '3rd'; break;
				case 4: $yearLevel = '4th'; break;
			}
			fputcsv($output, array($yearLevel, $data['basic_level']));
		}else 
			fputcsv($output, array($data['basic_level'], $data['yr_level']));
		fputcsv($output, array(' '));
		fputcsv($output, array('No.', 'ID No.', 'Name', 'Year Level','Gender'));
	
		$count = 1;
		foreach ($records as $record){
			fputcsv ($output, array($count, $record->students_idno, $record->neym, $data['yr_level'], $record->gender));
			$count++;
		}
	}
	
	//Added: 5/30/2014 by Isah
	public function download_basicEd_registered_not_enrolled_csv (){
	
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$data['cur_yr_id'] = $this->input->post('sy_id');
		$data['cur_yr'] = $this->input->post('cur_yr');
		$data['end_yr'] = $data['cur_yr'] + 1;
		$data['basic_level'] = $this->input->post('basic_level');
		$data['yr_level'] = $this->input->post('yr_level');
		$data['level_id'] = $this->input->post('level_id');
	
		$records = $this->enrollments_model->ListBasicStudentsNotPromoted($data);
		//print_r($records); die();
		$filename = "masterListNotEnrolled_" .$data['basic_level']. $data['yr_level']. "-". $data['cur_yr'] . "-" . $data['end_yr'] . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
		fputcsv($output, array('List of Registered Students but Not Enrolled'));
		if($data['level_id']== 4 OR $data['level_id']== 5){
			switch($data['yr_level']){
				case 1: $yearLevel = '1st'; break;
				case 2: $yearLevel = '2nd'; break;
				case 3: $yearLevel = '3rd'; break;
				case 4: $yearLevel = '4th'; break;
			}
			fputcsv($output, array($yearLevel, $data['basic_level']));
		}else
			fputcsv($output, array($data['basic_level'], $data['yr_level']));
		
		fputcsv($output, array(' '));
		fputcsv($output, array('No.', 'ID No.', 'Name', 'Year Level', 'Gender'));
	
		$count = 1;
		foreach ($records as $record){
			fputcsv ($output, array($count, $record->students_idno, $record->neym, $data['yr_level'], $record->gender));
			$count++;
		}
	}
	
	
	//Added: 5/30/2014 by Isah
	public function download_basicEd_transferredOut_csv(){
	
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$data['academic_year_id'] = $this->input->post('sy_id');
		$data['cutOffDate'] = $this->input->post('cutOffDate');
		$data['curdate'] = date('m-d-Y');
		
		$records = $this->enrollments_model->listStudentsTransferredOut($data);
		//print_r($records); die();
		
		$filename = "transferredOutasOf_" .$data['curdate']. ".csv";
		//$filename = "transferredOutasOf_" .$date. ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
		fputcsv($output, array('Cacellation of Enrollment ', $data['curdate']));
		fputcsv($output, array(' '));
		fputcsv($output, array(' '));
		fputcsv($output, array('No.', 'ID No.', 'Name', 'Year Level', 'Date Cancelled', 'Reason'));
	
		$count = 1;
		foreach ($records as $record){
			fputcsv ($output, array($count, $record->student_histories_id, $record->neym, $record->yr_level, $record->withdrawn_on, $record->withdrawn_reason));
			$count++;
		}
	}
	
	//Added: 5/30/2014 by Isah
	public function download_basicEd_droppedOut_csv(){
	
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$data['academic_year_id'] = $this->input->post('sy_id');
		$data['cutOffDate'] = $this->input->post('cutOffDate');
		$data['curdate'] = date('m-d-Y');
	
		$records = $this->enrollments_model->listStudentsDroppeddOut($data);
		//print_r($records); die();
	
		$filename = "droppedOutasOf_" .$data['curdate']. ".csv";
		//$filename = "transferredOutasOf_" .$date. ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
		fputcsv($output, array('List of Dropped-Outs as of ', $data['curdate']));
		fputcsv($output, array(' '));
		fputcsv($output, array(' '));
		fputcsv($output, array('No.', 'ID No.', 'Name', 'Year Level', 'Date Cancelled', 'Reason'));
	
		$count = 1;
		foreach ($records as $record){
			fputcsv ($output, array($count, $record->student_histories_id, $record->neym, $record->yr_level, $record->withdrawn_on, $record->withdrawn_reason));
			$count++;
		}
	}
	
	
	
	//Added:54/30/2014 by Isah
	public function batch_sectioning() {
		//print('Still on the development phase ....'); die();
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('hnumis/Terms_Model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of level and year level
	
				$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears(3);
				$data['levels'] = $this->basic_ed_sections_model->ListBasicLevels();
				//print_r($data['levels']); die();
				$data['title'] = 'Section Students';
				$this->content_lib->enqueue_body_content('basic_ed/year_level_sectioning', $data);
				$this->content_lib->content();
				break;

			case 2:
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$data['cur_yr_id'] = $this->input->post('academic_year');
					$data['prev_id'] = $data['cur_yr_id'] - 1;
					$academic_year_info = $this->AcademicYears_Model->GetAcademicYear($data['cur_yr_id']);
					$data['cur_yr_end'] = $academic_year_info->end_year;
					$data['cur_yr'] = $academic_year_info->start_year;
					$data['level_id'] = $this->input->post('level');
					$data['classSize'] = sanitize_text_field($this->input->post('classSize'));
					switch($data['level_id']){
						case '1': $data['basic_level'] = 'Kindergarten'; break;
						case '2': $data['basic_level'] = 'Kinder'; break;
						case '3': $data['basic_level'] = 'Grade'; break;
						case '4': $data['basic_level'] = 'Year'; break;
						case '5': $data['basic_level'] = 'Year'; break;
						case '11': $data['basic_level'] = 'Preschool'; break;
						case '0': $data['basic_level'] = 'NO Section'; break;
						case '8': $data['basic_level'] = 'K12-Day'; break;
						case '9': $data['basic_level'] = 'K12-Night'; break;
					}
					$data['sections'] = $this->basic_ed_sections_model->ListHischoolSections();
					$data['yr_level'] = $this->input->post('year');
					$data['students'] = $this->enrollments_model->ListBasicStudentsforSectioning($data);
					//print_r($data['sections']); die();
					$this->content_lib->enqueue_body_content('basic_ed/batch_sectioning',$data);
					$this->content_lib->content();
				}else{
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
				break;
				
			case 'auto_section': //autosectioning process
				
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
							$student_list = $this->input->post('classlist');
							//print_r($student_list); die();
							$section = sanitize_text_field($this->input->post('section'));
						
							$status = 1;
							
							if($student_list AND $section){
								$this->db->trans_start();
								foreach($student_list AS $k => $v){
									$status = $this->enrollments_model->section_student($k, $section);
									if(!$status){
										$this->db->trans_rollback();
										$status = 0;
									}	
								}
								//print($status); die();	
								if($status){
										$this->db->trans_complete();
										$this->content_lib->set_message("Auto sectioning successful!", "alert-success");
								} else {
									//$this->db->trans_rollback();
									$this->content_lib->set_message("Error found while sectioning!", "alert-error");
								}
							}else 
								$this->content_lib->set_message("No students selected for sectioning OR No section chosen.", "alert-error");
							
					} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
							$this->index();
					}	
					$this->content_lib->content();
					//$this->index();
					break;
		}
	}
	

	//Added:6/6/2014 by Isah
	public function random_sectioning() {
		//print('Still on the development phase ....'); die();
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('hnumis/Terms_Model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of level and year level
	
				$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears(3);
				$data['levels'] = $this->basic_ed_sections_model->ListBasicLevels();
				//print_r($data['levels']); die();
				$data['title'] = 'Section Students';
				$this->content_lib->enqueue_body_content('basic_ed/year_level_sectioning_random', $data);
				$this->content_lib->content();
				break;
	
			case 2:
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$data['cur_yr_id'] = $this->input->post('academic_year');
					$data['prev_id'] = $data['cur_yr_id'] - 1;
					$academic_year_info = $this->AcademicYears_Model->GetAcademicYear($data['cur_yr_id']);
					$data['cur_yr_end'] = $academic_year_info->end_year;
					$data['cur_yr'] = $academic_year_info->start_year;
					$data['level_id'] = $this->input->post('level');
					$data['numSection'] = sanitize_text_field($this->input->post('numSection'));
					switch($data['level_id']){
						case '1': $data['basic_level'] = 'Kindergarten'; break;
						case '2': $data['basic_level'] = 'Kinder'; break;
						case '3': $data['basic_level'] = 'Grade'; break;
						case '4': $data['basic_level'] = 'Year'; break;
						case '5': $data['basic_level'] = 'Year'; break;
						case '11': $data['basic_level'] = 'Preschool'; break;
						case '0': $data['basic_level'] = 'NO Section'; break;
						case '8': $data['basic_level'] = 'K12-Day'; break;
						case '9': $data['basic_level'] = 'K12-Night'; break;
					}
					$data['sections'] = $this->basic_ed_sections_model->ListHischoolSections();
					$data['yr_level'] = $this->input->post('year');
					$data['students'] = $this->enrollments_model->ListStudentsforRandomSectioning($data);
					//print_r($data['sections']); die();
					$this->content_lib->enqueue_body_content('basic_ed/random_sectioning',$data);
					$this->content_lib->content();
				}else{
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
				break;
	
			case 'random_section': //autosectioning process
	
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$section_list = $this->input->post('sections');
					$data['cur_yr_id'] = $this->input->post('cur_yr_id');
					$data['level_id'] = $this->input->post('level');
					$data['yr_level'] = $this->input->post('yearLevel');
					$status = 1;
					//$students = $this->enrollments_model->ListStudentsforRandomSectioning($data);
					$numElements = count($section_list);
					//print($numElements); die();
					$this->db->trans_start();
					$count = 0;
					foreach($section_list AS $k => $v){
						while($students=$this->enrollments_model->ListStudentsforRandomSectioning($data)){
							//print_r($students); die();
							$studentsCount = count($students);
							$studentsPerclass = round($studentsCount/$numElements);
							//print($studentsPerclass); die();
							$selectedStudentsKeys = array_rand($students, $studentsPerclass);
							//print_r($selectedStudentsKeys); die();
							if($count == $numElements-1)
								$count = 0;
							foreach($section_list AS $k => $v){
								$student_hist_id = $this->enrollments_model->get_random_student($k, $section);
								$status = $this->enrollments_model->section_student($k, $student_hist_id);
								if(!$status){
									$this->db->trans_rollback();
									$status = 0;
								}
							}
						}		
					}	
						//print($status); die();
						if($status){
							$this->db->trans_complete();
							$this->content_lib->set_message("Auto sectioning successful!", "alert-success");
						} else 
							//$this->db->trans_rollback();
							$this->content_lib->set_message("Error found while sectioning!", "alert-error");
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
				$this->content_lib->content();
				//$this->index();
				break;
		}
	}
	
	
	//Added: 4/29/2014 by Isah
	public function add_new_section() {
		
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('hnumis/Terms_Model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model');
		
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of sections, allows adding a new section
				
				$data['levels'] = $this->Terms_Model->ListLevels();
				$data['sections'] = $this->basic_ed_sections_model->ListBasicSections();
				$this->content_lib->enqueue_body_content('basic_ed/form_to_list_sections', $data);
				$this->content_lib->content();
				break;
			
			case 2: //add new section
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$level_id = $this->input->post('level');
					//$data['yrLevel'] = $this->input->post('year');
					$sectionNm = sanitize_text_field($this->input->post('section'));
					if($level_id == 4){
						$section_id =  $this->basic_ed_sections_model->GetSectionId(1)->lastId + 1;
						$level_tag = 1;
					}else{
						$section_id =  $this->basic_ed_sections_model->GetSectionId(0)->lastId + 1;
						$level_tag = 0;
					}
					if($this->basic_ed_sections_model->insertNewSection($section_id, $sectionNm, $level_tag)){
						$this->content_lib->set_message("Section successfully added!", "alert-success");
					} else {
						$this->content_lib->set_message("Error found while adding section!", "alert-error");
					}
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}	
				$data['levels'] = $this->Terms_Model->ListLevels();
				$data['sections'] = $this->basic_ed_sections_model->ListBasicSections();
				$this->content_lib->enqueue_body_content('basic_ed/form_to_list_sections', $data);
				$this->content_lib->content();
				break;
			
			case '3': //edit section
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$data['id'] = $this->input->post('section_id');
					$data['section'] = $this->input->post('section');
					//print_r($data); die();
					if ($this->basic_ed_sections_model->editSection($data)) {
						$this->content_lib->set_message("Section successfully edited!", "alert-success");
					} else {
						$this->content_lib->set_message("Error found while updating section!", "alert-error");
					}
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
				$data['levels'] = $this->Terms_Model->ListLevels();
				$data['sections'] = $this->basic_ed_sections_model->ListBasicSections();
				$this->content_lib->enqueue_body_content('basic_ed/form_to_list_sections', $data);
				$this->content_lib->content();
				break;
		}
	
	}
	
	//Added: 4/29/2014 by Isah
	public function list_sections() {
		
		//NOT Done Yet
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('hnumis/Terms_Model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of level and year level
	
				$data['levels'] = $this->Terms_Model->ListBasicLevels();
				$this->content_lib->enqueue_body_content('rsclerk/form_to_add_new_section', $data);
				$this->content_lib->content();
				break;
			case 2:
				
				
				$data['students'] = $this->Student_model->ListBasicStudentsEnrolled($data);
				//print_r($data); die();
				$this->content_lib->enqueue_body_content('rsclerk/registered_report',$data);
				$this->content_lib->content();
				break;
		}
	
	
	}
	
	//Added: May 15, 2014 by Amie
	public function academic_sections() {
		$this->load->model('hnumis/Terms_Model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('hnumis/faculty_model');
		
			$step = ($this->input->post('step') ?  $this->input->post('step') : 'list_academic_years');
			//print_r($this->input->post()); die();			
			switch ($step) {
				case 'list_academic_years': //display academic years
					$data['academicyears'] = $this->academicyears_model->ListAcademicYears();
					$data['levels'] = $this->basic_ed_sections_model->ListBasicLevels();

					$this->content_lib->enqueue_body_content('rsclerk/list_academic_years',$data);
					$this->content_lib->content();
					break;

				case 'retrieve_advisers' : //retrieve advisers for the selected academic year and allow for adding
					$academic_year = $this->input->post('academic_years_id');
					$level = $this->input->post('levels_id');
					
					$data['academicyear'] = $this->academicyears_model->GetAcademicYear($academic_year);
					$data['level'] = $this->basic_ed_sections_model->getlevel($level);
					$data['advisers'] = $this->basic_ed_sections_model->faculty_advisers($data['academicyear']->id,$data['level']->id);
					$data['sections'] = $this->basic_ed_sections_model->ListBasicSections();
					$data['faculty'] = $this->faculty_model->get_employees();

					$this->content_lib->enqueue_body_content('basic_ed/assign_faculty_advisers',$data);
					$this->content_lib->content();
					break;
				case 'assign_advisers':  //assign faculty adviser
					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
						$data['academic_years_id'] = $this->input->post('academic_years_id');
						$data['basic_ed_sections_id'] = $this->input->post('basic_ed_sections_id');
						$data['employees_empno'] = $this->input->post('employees_empno');
						$data['levels_id'] = $this->input->post('levels_id');
						$data['year_level'] = $this->input->post('year_level');
						$data['room'] = sanitize_text_field($this->input->post('room'));

						if($this->basic_ed_sections_model->create_faculty_adviser($data)){
							$this->content_lib->set_message("Faculty successfully assigned!", "alert-success");
						} else {
							$this->content_lib->set_message("Error found while assigning faculty!", "alert-error");
						}
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						//$this->index();
					}	
					$data['academicyear'] = $this->academicyears_model->GetAcademicYear($this->input->post('academic_years_id'));
					$data['level'] = $this->basic_ed_sections_model->getlevel($this->input->post('levels_id'));
					$data['advisers'] = $this->basic_ed_sections_model->faculty_advisers($this->input->post('academic_years_id'),$this->input->post('levels_id'));
					$data['sections'] = $this->basic_ed_sections_model->ListBasicSections();
					$data['faculty'] = $this->faculty_model->get_employees();
						
					$this->content_lib->enqueue_body_content('basic_ed/assign_faculty_advisers',$data);
					$this->content_lib->content();
					break;
					
				case 'remove_adviser': //removes an adviser
					if ($this->common->nonce_is_valid($this->input->post('nonce'))) {	
						$level = $this->basic_ed_sections_model->getlevel_adviser($this->input->post('adviser_id'));
						
						if($this->basic_ed_sections_model->remove_adviser($this->input->post('adviser_id'))) {
							$this->content_lib->set_message("Faculty successfully unassigned!", "alert-success");
						} else {
							$this->content_lib->set_message("Error found while removing adviser!", "alert-error");
						}
						
					} else {
							$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					}
					
					$academic_year = $this->input->post('academicyear');
					$level = $this->input->post('level');
					
					$data['academicyear'] = $this->academicyears_model->GetAcademicYear($academic_year);
					$data['level'] = $this->basic_ed_sections_model->getlevel($level);
						
					$data['advisers'] = $this->basic_ed_sections_model->faculty_advisers($data['academicyear']->id,$data['level']->id);
					$data['sections'] = $this->basic_ed_sections_model->ListBasicSections();
					$data['faculty'] = $this->faculty_model->get_employees();
							
					$this->content_lib->enqueue_body_content('basic_ed/assign_faculty_advisers',$data);
					$this->content_lib->content();
					
					break;
			}
		}
	

	//Added: 5/22/2014 by Isah
	public function set_enrollment_dates() {
	
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'set dates');
			
		switch ($action) {
	
			case 'set dates':// input enrollment dates
	
				$data['academic_years'] = $this->academicyears_model->ListAcademicYears(2);
				$data['title'] = 'Set Enrollment Schedule';
				$this->content_lib->enqueue_body_content('basic_ed/set_enrollment_date', $data);
				$this->content_lib->content();
				break;
			
			case'insert_enrollment_date': // update enrollment schedule
				
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$acadyear_id = $this->input->post('academic_year_id');
					$startD = sanitize_text_field($this->input->post('startDate'));
					$EndD = sanitize_text_field($this->input->post('endDate'));
					$empno = $this->session->userdata('empno');
					$enrollment_Sked_Info = $this->enrollments_model->getEnrollmentSched($acadyear_id);
					if($enrollment_Sked_Info){
						if(!$enrollment_Sked_Info->start_date AND !$enrollment_Sked_Info->end_date){
							if($this->enrollments_model->updateEnrollmentSched($enrollment_Sked_Info->id, $startD, $EndD)){
								$this->content_lib->set_message("Enrollment Schedule successfully set!", "alert-success");
							} else {
								$this->content_lib->set_message("Error found while setting Enrollment Schedule!", "alert-error");
							}
						}else 
							$this->content_lib->set_message("Error found while setting Enrollment Schedule. Enrollment Schedule has been set already.", "alert-error");
					}else {
						if($this->enrollments_model->insertEnrollmentSched($acadyear_id, $startD, $EndD, $empno)){
							$this->content_lib->set_message("Enrollment Schedule successfully set!", "alert-success");
						} else {
							$this->content_lib->set_message("Error found while setting Enrollment Schedule!", "alert-error");
						}
					} 
						
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}	
	
				$this->content_lib->content();
				break;
		}
	
	}
	
	
	//Added: 5/22/2014 by Isah
	public function update_enrollment_date() {
	
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'list dates');
			
		switch ($action) {
	
			case 'list dates':// lists enrollment dates
	
				$data['enrollment_schedules'] = $this->enrollments_model->listEnrollmentSched();
				$data['title'] = 'Update Enrollment Schedule';
				$this->content_lib->enqueue_body_content('basic_ed/update_enrollment_schedule', $data);
				$this->content_lib->content();
				break;
					
			case 'update_schedule': //updates selected schedule
	
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$sked_id = $this->input->post('schedule_id');
					$startD = sanitize_text_field($this->input->post('startD'));
					$endD = sanitize_text_field($this->input->post('endDate'));
					if($this->enrollments_model->updateEnrollmentSched($sked_id, $startD, $endD)){
						$this->content_lib->set_message("Enrollment Schedule successfully updated!", "alert-success");
					} else {
						$this->content_lib->set_message("Error found while updating Enrollment Schedule!", "alert-error");
					}
					
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
	
				$this->content_lib->content();
				break;
		}
	
	
	}
	
	//Added: 5/26/2014 by Isah
	public function set_cut_off_transfer() {
	
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'set date');
			
		switch ($action) {
	
			case 'set date':// input transfer out cut-off date
	
				$data['academic_years'] = $this->academicyears_model->ListAcademicYears(2);
				$data['title'] = 'Set Transfer Out Cut-Off Date';
				$this->content_lib->enqueue_body_content('basic_ed/set_transferOut_cut_off_date', $data);
				$this->content_lib->content();
				break;
					
			case'set_transferOut_date': // update transfer out cut-off date
	
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$acadyear_id = $this->input->post('academic_year_id');
					$transD = sanitize_text_field($this->input->post('transDate'));
					$tranferDate_Info = $this->enrollments_model->getEnrollmentSched($acadyear_id);
					if($tranferDate_Info){
						if(!$tranferDate_Info->withdrawal_cutoff_date){
							if($this->enrollments_model->setTransferCutOff($transD, $tranferDate_Info->id)){
								$this->content_lib->set_message("Transfer Out Cut_off Date successfully set!", "alert-success");
							} else {
								$this->content_lib->set_message("Error found while setting Transfer Out Cut_off Date!", "alert-error");
							}
						}else
							$this->content_lib->set_message("Error found while setting Transfer Out Cut_off Date!. Transfer Out Cut_off Date has been set already.", "alert-error");
					}else {
						$empno = $this->session->userdata('empno');
						if($this->enrollments_model->insertTransferCutOff($acadyear_id, $transD, $empno)){
								$this->content_lib->set_message("Transfer Out Cut_off Date successfully set!", "alert-success");
							} else {
								$this->content_lib->set_message("Error found while setting Transfer Out Cut_off Date!", "alert-error");
						}
						
					}
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
	
				$this->content_lib->content();
				break;
		}
	
	}
	
	
	//Added: 5/26/2014 by Isah
	public function update_cut_off_transfer() {
	
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'list dates');
			
		switch ($action) {
	
			case 'list dates':// lists enrollment dates
	
				$data['transfer_cutOff_dates'] = $this->enrollments_model->listTransferCutOffDates();
				//print_r($data['transfer_cutOff_dates']); die();
				$data['title'] = 'Update Transfer Out Cut-off Date';
				$this->content_lib->enqueue_body_content('basic_ed/update_transfer_cutoff_date', $data);
				$this->content_lib->content();
				break;
					
			case 'update_cutOff_date': //updates selected schedule
	
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$sked_id = $this->input->post('schedule_id');
					$cutOffD = sanitize_text_field($this->input->post('cutOffD'));
					if($this->enrollments_model->setTransferCutOff($cutOffD, $sked_id)){
						$this->content_lib->set_message("Transfer Out Cut-off Date successfully updated!", "alert-success");
					} else {
						$this->content_lib->set_message("Error found while updating Transfer Out Cut-off Date!", "alert-error");
					}
						
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
	
				$this->content_lib->content();
				break;
		}
	
	}
	
	//Added: 5/26/2014 by Isah
	public function student_list_transferOut() {
		//print('Still on the development phase ....'); die();
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'search academic year');
			
		switch ($action) {
	
			case 'search academic year': //display academic years
	
				$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears(1);
				$data['title'] = 'Cancellation of Enrollment';
				$this->content_lib->enqueue_body_content('basic_ed/search_academic_years', $data);
				$this->content_lib->content();
	
	
				break;
			case 'show list': //lists students who transferred out (within the cut-off date)
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$data['academic_year_id'] = $this->input->post('academic_year_id');
					
					$enrollment_sked_info = $this->enrollments_model->getEnrollmentSched($data['academic_year_id']);
					if($enrollment_sked_info){
						$data['cutOffDate'] = $enrollment_sked_info->withdrawal_cutoff_date;
						$data['datestatus'] = 1;
						$acad_info = $this->AcademicYears_Model->GetAcademicYear($data['academic_year_id']);
						$data['sy'] = $acad_info->sy;
						$data['current_date'] = date('F j, Y');
						$data['student_list'] = $this->enrollments_model->listStudentsTransferredOut($data);
						$data['title'] = 'CANCELLATION OF ENROLLMENT';
						//print_r($data['student_list'] ); die();
						
					}else {
						$data['datestatus'] = 0;
					}
					
					$this->content_lib->enqueue_body_content('basic_ed/students_transferredOut_report',$data);
					$this->content_lib->content();
				
				}else{
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
				
				break;
		}
	}
	
	
	//Added: 5/26/2014 by Isah
	public function student_list_dropOut() {
		//print('Still on the development phase ....'); die();
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'search academic year');
			
		switch ($action) {
	
			case 'search academic year': //display academic years
	
				$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears(1);
				$data['title'] = 'Dropped-Outs';
				$this->content_lib->enqueue_body_content('basic_ed/search_academic_years', $data);
				$this->content_lib->content();
	
	
				break;
			case 'show list': //lists students who transferred out (outside of the cut-off date)
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$data['academic_year_id'] = $this->input->post('academic_year_id');
					$enrollment_sked_info = $this->enrollments_model->getEnrollmentSched($data['academic_year_id']);
					if($enrollment_sked_info){
						$data['cutOffDate'] = $enrollment_sked_info->withdrawal_cutoff_date;
						$acad_info = $this->AcademicYears_Model->GetAcademicYear($data['academic_year_id']);
						$data['sy'] = $acad_info->sy;
						$data['current_date'] = date('F j, Y');
						$data['student_list'] = $this->enrollments_model->listStudentsDroppeddOut($data);
						$data['title'] = 'DROPPED-OUTS';
						$data['datastatus'] = 1;
					}else{
						$data['datastatus'] = 0;
					}	
					//print_r($data['datastatus'] ); die();
					$this->content_lib->enqueue_body_content('basic_ed/students_droppedOut_report',$data);
					$this->content_lib->content();
				}else{
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
				break;
		}
	}
	
	
	
	public function enrollment_summary_basic_ed() {
	
		$this->load->model('basic_ed/Reports_Model');
	
		$this->Reports_Model->enrollment_summary_basic_ed();
	
	}
	
	//Added: May 28, 2014 by Amie
	/*public function class_list() {
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model');
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('hnumis/faculty_model');
		$this->load->model('basic_ed/Reports_Model');
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 'list_academic_years');
		
		switch ($step) {
			case 'list_academic_years' : //display academic years
				$data['academicyears'] = $this->academicyears_model->ListAcademicYears();
				$data['levels'] = $this->basic_ed_sections_model->ListBasicLevels();
				
				$this->content_lib->enqueue_body_content('basic_ed/list_academic_years',$data);
				$this->content_lib->content();
				break;
			case 'display_sections' : //retrieve all created sections for the academic year
				$data['academicyear'] =  $this->academicyears_model->GetAcademicYear($this->input->post('academic_years_id'));
				$data['level']= $this->basic_ed_sections_model->getlevel($this->input->post('level_id'));
				$data['title'] = 'Class List';
				$data['advisers'] = $this->basic_ed_sections_model->faculty_advisers($this->input->post('academic_years_id'),$this->input->post('level_id'));
				//print_r($data['advisers']); die();
				$this->content_lib->enqueue_body_content('basic_ed/display_sections',$data);
				$this->content_lib->content();
				break;
			case 'display_class_list' :
				//$data['section'] = $this->basic_ed_sections_model->getsectiondetails($adviser,$academicyear);
				$data['section_name'] = $this->input->post('section_name');
				$data['yr_level'] = $this->input->post('yr_level');
				$data['class_adviser'] = $this->input->post('class_adviser');
				$data['room'] = $this->input->post('room');
				$data['acad_sy'] = $this->input->post('acad_sy');
				$data['list'] = $this->basic_ed_enrollments_model->generate_list(
																	$this->input->post('academicyear'),$this->input->post('level'),
																	$this->input->post('yr_level'),$this->input->post('basic_ed_sections_id'));

				$this->Reports_Model->Create_Class_List_Excel($data);
				
				$this->content_lib->enqueue_body_content('basic_ed/view_class_list',$data);
				$this->content_lib->content();
				
				break;
		}
	}
	*/

	//Added: May 31, 2014 by Amie
	public function download_section_csv() {
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model');
	
		$academicyear = $this->input->post('academicyear');
		$level = $this->input->post('level');
		$adviser = $this->input->post('adviser');
	
		$section = $this->basic_ed_sections_model->getsectiondetails($adviser,$academicyear);
		$records = $this->basic_ed_enrollments_model->generate_list($academicyear, $section->levels_id, $section->bed_id, $section->year_level);
		//print_r($records); die();
		$filename = $section->section_name.".csv";
		
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		fputcsv($output, array('Grade/Year Level:', $section->year_level));
		fputcsv($output, array('Section:', $section->section_name));
		fputcsv($output, array('Class Adviser:', $section->class_adviser));
		fputcsv($output, array());
		
		// output the column headings
		fputcsv($output, array('Last Name', 'First Name', 'Middle Name', 'Gender'));
	
		$count = 0;
		$countmale=0;
		$countfemale=0;
		foreach ($records as $record){
			fputcsv ($output, array($record->lname, $record->fname, $record->mname, $record->gender));
			if ($record->gender == 'M') {
				$countmale++;
			} else {
				$countfemale++;
			}
			$count++;
		}
		
		fputcsv($output, array());
		fputcsv($output, array('No. of Students', $count));
		fputcsv($output, array('No. of Male Students', $countmale));
		fputcsv($output, array('No. of Female Students', $countfemale));
		
	}

	public function set_exam_dates() {
		//print('Still on the development phase ....'); die();
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('hnumis/Terms_Model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
		$this->content_lib->enqueue_footer_script('chained');
		$this->content_lib->enqueue_footer_script('data_tables');
		
		$action = ($this->input->post('action') ?  $this->input->post('action') : "set school year");
			
		switch ($action) {
	
			case "set school year": //display a list of level and year level
	
				$data['academic_years'] = $this->AcademicYears_Model->ListAcademicYears(3);
				$data['levels'] = $this->basic_ed_sections_model->ListBasicLevels();
				//print_r($data['levels']); die();
				$data['title'] = 'Set Examination Dates';
				$this->content_lib->enqueue_body_content('basic_ed/set_sy_level', $data);
				$this->content_lib->content();
	
	
				break;
			case "set dates":
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$data['cur_yr_id'] = $this->input->post('academic_year');
					$academic_year_info = $this->AcademicYears_Model->GetAcademicYear($data['cur_yr_id']);
					$data['cur_yr_end'] = $academic_year_info->end_year;
					$data['cur_yr']     = $academic_year_info->start_year;
					$data['level_id'] = $this->input->post('level');
					$data['title'] = "Set Examination Dates";
					switch($data['level_id']){
						case '11': 
								$data['yr_level'][] = 1;
									 
								break;
						case '1': 
								$data['yr_level'][] = 1;
									 
								break;
						case '3': 
								for($x=1; $x <= 6; $x++) {
									if ($this->input->post('gs'.$x)) {
										$data['yr_level'][] = $x;
									}
								}
									 
								break;
						case '4': 
								for($x=7; $x <= 10; $x++) {
									if ($this->input->post('hd'.$x)) {
										$data['yr_level'][] = $x;
									}
								}
									 
								break;
						case '12': 
								for($x=11; $x <= 12; $x++) {
									if ($this->input->post('sh'.$x)) {
										$data['yr_level'][] = $x;
									}
								}
									 
								break;
					}
					$data['yr_level'] = json_encode($data['yr_level']);
					$data['num_exams'] = $this->input->post('num_exams');
					$data['start_exam_no'] = $this->input->post('start_exam_no');
					//print_r($data); die();
					$this->content_lib->enqueue_body_content('basic_ed/set_exam_dates',$data);
					$this->content_lib->content();
				}else{
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
				break;
				
			case "insert dates":
					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
						$yr_level = json_decode($this->input->post('yr_level'));
						//print_r($yr_level); die();
						$data['cur_yr_id'] = $this->input->post('academic_year');
						$academic_year_info = $this->AcademicYears_Model->GetAcademicYear($data['cur_yr_id']);
						$data['cur_yr_end'] = $academic_year_info->end_year;
						$data['cur_yr'] = $academic_year_info->start_year;
						$data['level_id'] = $this->input->post('level');
						$year_level_code = $this->input->post('year_level_code');
						
						$start_dates = $this->input->post('start_exam_date');
						$end_dates = $this->input->post('end_exam_date');
						$start_status = 1;
						foreach($start_dates as $start_date){
							if($start_date == NULL)
								$start_status = 0;
						}
						$end_status = 1;
						foreach($end_dates as $end_date){
							if($end_date == NULL)
								$end_status = 0;
						}
						
						$data['empno'] = $this->session->userdata('empno');

						if ($start_status AND $end_status) {
							foreach($start_dates AS $k => $v) {
								$data['start_date'] = $start_dates[$k];
								$data['end_date'] = $end_dates[$k];
								$data['exam_no'] = $k;
								
								foreach($yr_level AS $k1=>$v1) {
									$insert_status = $this->enrollments_model->insertExamDates($data, $v1);
								}
								/* if($year_level_code == 3 OR $year_level_code == 5){
									if($year_level_code == 3){
										for($year_level = 1; $year_level <= 5; $year_level++){
											//print("OK"); die();
											$insert_status = $this->enrollments_model->insertExamDates($data, $year_level);
										}	
									}else{
										for($year_level = 7; $year_level <= 11; $year_level++){
											//print("not OK"); die();
											$insert_status = $this->enrollments_model->insertExamDates($data, $year_level);
										}
									}
								}else{
									switch($year_level_code){
										case 4: $year_level = 6; break;
										case 6: $year_level = 12; break;
										case 7: $year_level = 4; break;
										default:
											$year_level = $year_level_code;
									}
									//print("sample"); die();
									$insert_status = $this->enrollments_model->insertExamDates($data, $year_level);
									
								} */
							}	
						} else {
							$this->content_lib->set_message("Error found while setting Examination dates! Other dates are blank!", "alert-error");
						}
						if($insert_status){
							$this->content_lib->set_message("Examination dates successfully set!", "alert-success");
						}else{
							$this->content_lib->set_message("Error found while setting examination dates!.", "alert-error");
						}					
					}else{
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
					$this->content_lib->content();
					break;
		}
	}
	
	//Added: 7/23/2014 by Isah
	//Updated: 9/4/14 by genes
	function update_exam_dates() {
	 	$this->load->library('registrar_lib');
		$this->registrar_lib->update_exam_dates();
	
	}
	/*public function update_exam_dates() {
	
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 'list dates');
			
		switch ($action) {
	
			case 'list dates':// lists examination dates
	
				
				//print_r($data['transfer_cutOff_dates']); die();
				$cur_yr_id = $this->academicyears_model->current_academic_year();
				$data['exam_dates'] = $this->enrollments_model->listExamDates($cur_yr_id->id);
				$data['title'] = 'Update Examination Dates';
				$data['start_year'] = $cur_yr_id->end_year - 1;
				$data['end_year'] = $cur_yr_id->end_year;
				$this->content_lib->enqueue_body_content('basic_ed/update_exam_dates', $data);
				$this->content_lib->content();
				break;
					
			case 'update_schedule': //updates selected schedule
	
				if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
					$sked_id = $this->input->post('schedule_id');
					
					$startDate = sanitize_text_field($this->input->post('start_date'));
					$endDate = sanitize_text_field($this->input->post('end_date'));
					//print($startDate."<br>".$endDate); die();
					if($this->enrollments_model->updateExamDate($sked_id, $startDate, $endDate)){
						$this->content_lib->set_message("Exam Date successfully updated!", "alert-success");
					} else {
						$this->content_lib->set_message("Error found while updating Exam Date!", "alert-error");
					}
	
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->index();
				}
	
				$this->content_lib->content();
				break;
				
				case 'delete_schedule': //deletes selected schedule
				
					if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
						$sked_id = $this->input->post('schedule_id');
						
						if($this->enrollments_model->deleteExamDate($sked_id)){
							$this->content_lib->set_message("Exam Date successfully deleted!", "alert-success");
						} else {
							$this->content_lib->set_message("Error found while deleting Exam Date!", "alert-error");
						}
				
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}
				
					$this->content_lib->content();
					break;
		}
	
	}*/
	
/************************ PRIVATE METHOD**************************/
	private function list_academic_years() {
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		
		$data['academicyears'] = $this->AcademicYears_Model->ListAcademicYears();
		$data['levels'] = $this->basic_ed_sections_model->ListBasicLevels();
		
		$this->content_lib->enqueue_body_content('rsclerk/list_academic_years',$data);
		$this->content_lib->content();
	}
	
	private function getSections($academic_year) {
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('hnumis/faculty_model');
		
		$data['academicyear'] = $this->academicyears_model->GetAcademicYear($academic_year);
		$data['level'] = $this->basic_ed_sections_model->getlevel($this->session->userdata('level'));
		$data['advisers'] = $this->basic_ed_sections_model->faculty_advisers($data['academicyear']->id,$data['level']->id);
		$data['sections'] = $this->basic_ed_sections_model->ListBasicSections();
		$data['faculty'] = $this->faculty_model->get_employees();
			
		$this->content_lib->enqueue_body_content('basic_ed/assign_faculty_advisers',$data);
		$this->content_lib->content();
			
	}
	/*****************************************************************/

	///////////////////////////////////////////////////////////////////
	// added 6/30/14 by ra
	public function print_id(){
		$this->load->view('rsclerk/print_id/id.html', array());
	}
	
	public function barcode($idnum){
		$idnum = str_pad ($idnum, '0', 8, STR_PAD_LEFT);
		$this->load->library('barcode');
		$this->barcode->render($idnum);
	}
	////////////////////////////////////////////////////////////////

	
	/*
	 * @ADDED: 9/3/14 
	 * @author: genes
	 */
	/*function view_exam_dates() {
		$this->load->library('registrar_lib');
		$this->registrar_lib->view_exam_dates_basic_ed();
		
	}*/



	function ad_sections(){
		$this->load->model('basic_ed/ad_model');
		$data['sections'] = $this->ad_model->list_ad_sections('115');
		$this->content_lib->enqueue_body_content('basic_ed/ad_sections_view',$data);
		$this->content_lib->content();
	}

	function ad_students(){
		$this->content_lib->set_message('under construction','alert-error');
		$this->content_lib->content();
	}
	
	/*
	* ADDED: 4/11/15
	* @author: genes
	* @description: displays students enrolled from enrollment summary modal
	*/
	function get_basic_ed_students() {
		$this->load->model('basic_ed/basic_ed_enrollments_model');
		
		$data['students'] = $this->basic_ed_enrollments_model->list_enrollees($this->input->get('prog_id'), $this->input->get('yr_level'), 0, $this->input->get('selected_year'));
		//print_r($data); die();
		$this->load->view('rsclerk/list_of_students_from_modal', $data);
		
	}


	public function extract_places() {
		$this->load->library('places_lib');
	
		$this->places_lib->extract_places();		
	}
	
	public function new_shs_student() {	
		$this->load->library('shs/shs_student_lib');
	
		$this->shs_student_lib->new_student();
	}

	
	public function process_student_action($idnum=NULL) {
		
		if (!$this->input->post('action')) {
			redirect('rsclerk', 'refresh');
		}
		
		$this->load->library('shs/shs_student_lib');
	
		$this->shs_student_lib->process_student_action($idnum);
		
	}


	public function bed_enrolment() {
		$this->load->library('bed/student_lib');
	
		$this->student_lib->enrolment_management();
	
	}

		
	//NOTE: this function handles all actions for the student tabs basic ed students
	public function process_bed_student_action($idnum=NULL) {

		if (!$this->input->post('action')) {
			redirect('rsclerk', 'refresh');
		}
		
		$this->load->library('bed/student_lib');
	
		$this->student_lib->process_student_action($idnum);
		
	}


}

?>