<?php 
/**
 * Function age_given_birthdate
 * 
 * Returns the age of the person in years given birthdate. Birthdate is in the form yyyy-mm-dd.
 * 
 * @param unknown_type $birthdate
 * @return int age
 */
function age_given_birthdate ($birthdate) {
	//explode the date to get month, day and year
	$birthdate = explode("-", $birthdate);
	//get age from date or birthdate
	$age = (date("md", date("U", mktime(0, 0, 0, $birthdate[2], $birthdate[1], $birthdate[0]))) > date("md") ? ((date("Y")-$birthdate[0])-1):(date("Y")-$birthdate[0]));
	return $age;
}

function sanitize_text_field($str) {
	$filtered = check_invalid_utf8( $str );

	if ( strpos($filtered, '<') !== false ) {
		// This will strip extra whitespace for us.
		$filtered = strip_all_tags( $filtered, true );
	} else {
		$filtered = trim( preg_replace('/[\r\n\t ]+/', ' ', $filtered) );
	}

	$match = array();
	$found = false;
	//get rid of octets
	while ( preg_match('/%[a-f0-9]{2}/i', $filtered, $match) ) {
		$filtered = str_replace($match[0], '', $filtered);
		$found = true;
	}

	if ( $found ) {
		// Strip out the whitespace that may now exist after removing the octets.
		$filtered = trim( preg_replace('/ +/', ' ', $filtered) );
	}

	return $filtered;
}

function sanitize_textarea($str) {
	$filtered = check_invalid_utf8( $str );

	$match = array();
	$found = false;
	//get rid of octets
	while ( preg_match('/%[a-f0-9]{2}/i', $filtered, $match) ) {
		$filtered = str_replace($match[0], '', $filtered);
		$found = true;
	}

	if ( $found ) {
		// Strip out the whitespace that may now exist after removing the octets.
		$filtered = trim( preg_replace('/ +/', ' ', $filtered) );
	}

	return $filtered;
}

function check_invalid_utf8( $string, $strip = false ) {
	$string = (string) $string;

	if ( 0 === strlen( $string ) ) {
		return '';
	}

	// preg_match fails when it encounters invalid UTF8 in $string
	if ( 1 === @preg_match( '/^./us', $string ) ) {
		return $string;
	}

	// Attempt to strip the bad chars if requested (not recommended)
	if ( $strip && function_exists( 'iconv' ) ) {
		return iconv( 'utf-8', 'utf-8', $string );
	}

	return '';
}

function strip_all_tags($string, $remove_breaks = false) {
	$string = preg_replace( '@<(script|style)[^>]*?>.*?@si', '', $string );
	$string = strip_tags($string);

	if ( $remove_breaks )
		$string = preg_replace('/[rnt ]+/', ' ', $string);

	return trim($string);
}

function action_form ($action, $return=TRUE){
	if ($return){
		return '<input type="hidden" name="action" value="' . $action . '" />';	
	} else {
		echo '<input type="hidden" name="action" value="' . $action . '" />';
	}
}


function accronymize($word, $acc_single=TRUE, $ucfirst=TRUE){
	$words = explode(" ", $word);

	if ($acc_single && count($words) == 1){
		return $word;
	}

	$acronym = "";
	foreach ($words as $w) {
		if ($ucfirst)
			$acronym .= ucfirst($w[0]); else
			$acronym .= $w[0];
	}
	return $acronym;
}

function suffixize($fullname){
	$suffix = '';
	$fullname = str_replace(',','',$fullname);
	$sufixes_ar = array('jr.','jr','sr.','sr','i','ii','iii','iv','v','vi','vii','viii','ix','x','svd','md',);
	$words = explode(" ", $fullname);
	
	$i = count($words) - 1;
	$i_when_found = 0;
	$go_ahead=true;
	$found_one = false;
	while ($go_ahead AND $i>=0){
		if (in_array(strtolower(trim($words[$i])), $sufixes_ar)){
			$suffix = $words[$i];
			$i_when_found = $i;
			$go_ahead = false;
			$found_one = true;
		}
		$i--;
	}
	if ($suffix !==''){ //valid suffix exist
		unset($words[$i_when_found]);
		$words =  array_values($words);
	}
	return implode(" ",$words) . ($found_one ? ' ':'') . $suffix;
	
}



/**
 * returns a name in "surname, firstname middle suffix" format given "first middle lastname suffix" format
 * @param unknown $fullname
 */
function l_first($fullname){
	$return='';
	$found_de = false;	
	$fullname = str_replace(',','',$fullname);
	$fullname = trim(preg_replace('/\s+/', ' ', $fullname));
	$sufixes_ar = array('jr.','jr','sr.','sr','i','ii','iii','iv','IV', 'v','vi','vii','viii','ix','x','svd','md',);
	$de_ar = array('de','dela','delos', 'del', 'prasad', 'van', 'de paz','abay');
	$words = explode(" ", $fullname);
	
	
	//clean up
	$key=0;
	foreach ($words as $word){
		$words[$key]=trim($word);
		$key++;
	}
	
	//get suffixes:
	$suffix = '';
	$i = count($words) - 1;
	$i_when_found = 0;
	$go_ahead=true;
	$found_one = false;
	while ($go_ahead AND $i>=0){
		if (in_array(strtolower(trim($words[$i])), $sufixes_ar)){
			$suffix = $words[$i];
			$i_when_found = $i;
			$go_ahead = false;
			$found_one = true;
		}
		$i--;
	}
	if ($suffix !==''){ //valid suffix exist
		unset($words[$i_when_found]);
		$words =  array_values($words);
	}

	//now fname and lname is left. if there is any, suffix was stored at $suffix
	$count = count($words);
	switch ($count){
		case 0:
			break;
		case 1:
			$return = $words[0];
			break;
		case 2:
			$return = $words[1] . ', ' . $words[0]; 
		default:
			//now we search for any existence of de's
			$index = $count - 1;
			$j = 0;
			$go_ahead=true;
			$j_when_found = 0;

			$splice_left ="";
			$splice_right="";

			while ($go_ahead AND $j <= $index){
				if (in_array(strtolower(trim($words[$j])), $de_ar)){
					$j_when_found = $j;			
					$splice_left = implode(' ',array_splice($words, $j));
					$splice_right = implode(' ',array_values($words));						
					$found_de = true;
					$go_ahead = false;
				}
				$j++;
			}
			
			if ($found_de==false){
				$splice_left = $words[$index];
				unset($words[$index]);
				$splice_right = implode(' ',$words);
			}
			$return = 	$splice_left . ', ' . $splice_right;		
			
			break;
	} 
		
	return $return . ' ' . $suffix;

}

/**
 * places suffix after firstname
 * @param unknown $fullname
 * @return string
 */
function l_first2($fullname){
	$return='';
	$found_de = false;
	$fullname = str_replace(',','',$fullname);
	$fullname = trim(preg_replace('/\s+/', ' ', $fullname));
	$sufixes_ar = array('jr.','jr','sr.','sr','i','ii','iii','iv','IV', 'v','vi','vii','viii','ix','x','svd','md',);
	$de_ar = array('de','dela','delos', 'del', 'prasad', 'van', 'de paz','abay');
	$words = explode(" ", $fullname);


	//clean up
	$key=0;
	foreach ($words as $word){
		$words[$key]=trim($word);
		$key++;
	}

	//get suffixes:
	$suffix = '';
	$i = count($words) - 1;
	$i_when_found = 0;
	$go_ahead=true;
	$found_one = false;
	while ($go_ahead AND $i>=0){
		if (in_array(strtolower(trim($words[$i])), $sufixes_ar)){
			$suffix = $words[$i];
			$i_when_found = $i;
			$go_ahead = false;
			$found_one = true;
		}
		$i--;
	}
	if ($suffix !==''){ //valid suffix exist
		unset($words[$i_when_found]);
		$words =  array_values($words);
	}

	//now fname and lname is left. if there is any, suffix was stored at $suffix
	$count = count($words);
	switch ($count){
		case 0:
			break;
		case 1:
			$return = $words[0];
			break;
		case 2:
			$return = $words[1] . ', ' . $words[0];
		default:
			//now we search for any existence of de's
			$index = $count - 1;
			$j = 0;
			$go_ahead=true;
			$j_when_found = 0;

			$splice_left ="";
			$splice_right="";

			while ($go_ahead AND $j <= $index){
				if (in_array(strtolower(trim($words[$j])), $de_ar)){
					$j_when_found = $j;
					$splice_left = implode(' ',array_splice($words, $j));
					$splice_right = implode(' ',array_values($words));
					$found_de = true;
					$go_ahead = false;
				}
				$j++;
			}
				
			if ($found_de==false){
				$splice_left = $words[$index];
				unset($words[$index]);
				$splice_right = implode(' ',$words);
			}
			$return = 	$splice_left . ', ' . $splice_right;
				
			break;
	}

	//print_r($splice_right);die();
	$firstname_middlename	 = explode(" ",$splice_right);
	if (count($firstname_middlename) > 1){
		$middlename = implode(' ',array_splice($firstname_middlename,count($firstname_middlename)-1));
		$firstname = implode(' ',$firstname_middlename);
		return $splice_left . ', '. $firstname .' '. $suffix . ' ' . $middlename; 
	}	
	
	return $return . ' ' . $suffix;		

}


function generateRandomString($length = 15) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}


function ip_is_private ($ip) {
	$pri_addrs = array (
			'10.0.0.0|10.255.255.255', // single class A network
			'172.16.0.0|172.31.255.255', // 16 contiguous class B network
			'192.168.0.0|192.168.255.255', // 256 contiguous class C network
			'169.254.0.0|169.254.255.255', // Link-local address also refered to as Automatic Private IP Addressing
			'127.0.0.0|127.255.255.255' // localhost
	);

	$long_ip = ip2long ($ip);
	if ($long_ip != -1) {

		foreach ($pri_addrs AS $pri_addr) {
			list ($start, $end) = explode('|', $pri_addr);

			// IF IS PRIVATE
			if ($long_ip >= ip2long ($start) && $long_ip <= ip2long ($end)) {
				return true;
			}
		}
	}

	return false;
}

function getBrowser(){
	$u_agent = $_SERVER['HTTP_USER_AGENT'];
	$bname = 'Unknown';
	$platform = 'Unknown';
	$version= "";

	//First get the platform?
	if (preg_match('/linux/i', $u_agent)) {
		$platform = 'linux';
	}
	elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
		$platform = 'mac';
	}
	elseif (preg_match('/windows|win32/i', $u_agent)) {
		$platform = 'windows';
	}

	// Next get the name of the useragent yes seperately and for good reason
	if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
	{
		$bname = 'Internet Explorer';
		$ub = "MSIE";
	}
	elseif(preg_match('/Firefox/i',$u_agent))
	{
		$bname = 'Mozilla Firefox';
		$ub = "Firefox";
	}
	elseif(preg_match('/Chrome/i',$u_agent))
	{
		$bname = 'Google Chrome';
		$ub = "Chrome";
	}
	elseif(preg_match('/Safari/i',$u_agent))
	{
		$bname = 'Apple Safari';
		$ub = "Safari";
	}
	elseif(preg_match('/Opera/i',$u_agent))
	{
		$bname = 'Opera';
		$ub = "Opera";
	}
	elseif(preg_match('/Netscape/i',$u_agent))
	{
		$bname = 'Netscape';
		$ub = "Netscape";
	}

	// finally get the correct version number
	$known = array('Version', $ub, 'other');
	$pattern = '#(?<browser>' . join('|', $known) .
	')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	if (!preg_match_all($pattern, $u_agent, $matches)) {
		// we have no matching number just continue
	}

	// see how many we have
	$i = count($matches['browser']);
	if ($i != 1) {
		//we will have two since we are not using 'other' argument yet
		//see if version is before or after the name
		if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
			$version= $matches['version'][0];
		}
		else {
			$version= $matches['version'][1];
		}
	}
	else {
		$version= $matches['version'][0];
	}

	// check if we have a number
	if ($version==null || $version=="") {$version="?";}

	return array(
			'userAgent' => $u_agent,
			'name'      => $bname,
			'version'   => $version,
			'platform'  => $platform,
			'pattern'    => $pattern
	);
}




