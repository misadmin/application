<div style="auto; text-align:center; padding-top:30px;">
<table border="0" cellpadding="2" cellspacing="0" class="table table-striped table-condensed table-hover" style="width:60%;">
 	<thead>
  <tr style="background:#EAEAEA;">
    <td colspan="4" style="text-align:left; font-weight:bold; font-size:16px;"><?php print($sy."<br>".$program_abbreviation." ".$ylevel); ?></td>
    </tr>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td width="9%" style="text-align:center; vertical-align:middle;">No.</td>
    <td width="16%" style="text-align:center; vertical-align:middle;">ID No.</td>
    <td width="50%" style="text-align:center;">Name</td>
    <td width="25%" style="text-align:center;">Course/Year</td>
    </tr>
	</thead>
  <tr>
  <?php
  		if ($students) {
			$cnt = 0;
  			foreach($students AS $student) {
				$cnt++;
  ?>
    <td style="text-align:center;"><?php print($cnt); ?></td>
    <td style="text-align:center;"><?php print($student->idno); ?></td>
    <td style="text-align:left;"><?php print($student->name); ?></td>
    <td style="text-align:center;"><?php print($student->course_yr); ?></td>
    </tr>
	<?php
			}
		}
	?>
</table>
</div>
