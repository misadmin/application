<div class="span10">
<h3>Student Payments</h3>
  <table class = "table table-bordered table-striped">
    <thead>
      <tr>
        <th width="15%" style="text-align:center;">Payment Date</th>
        <th width="15%" style="text-align:center;">Receipt No</th>
        <th width="35%" style="text-align:center;">Particulars</th>
        <th width="15%" style="text-align:center;">Amount</th>
		<th width="20%" style="text-align:center;">Payment Status</th>
      </tr>
    </thead>
    <?php
		if ($payments_info) {
			foreach ($payments_info AS $payment) {
	?>
    <tr>
      <td style="text-align:center;"><?php print($payment->payment_date); ?></td>
      <td><?php echo $payment->receipt_no ; ?></td>
      <td><?php echo $payment->description; ?></td>
      <td style="text-align:right;"><?php echo number_format($payment->receipt_amount,2); ?></td>
	  <td style="text-align:center;"><?php echo $payment->status ;?></td>
    </tr>
    <?php
			}
		}else{ ?>
		 <tr> 
		    <td colspan="5">No payments yet</td>
		</tr>	
		<?php }   ?>
  </table>
</div>
