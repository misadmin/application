<?php

	class subject_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		
		/*
		 * @added: 01/29/2018
		 * @author: genes
		 */
		
		public function subject_management() {
		
			$this->CI->load->model('hnumis/bed/subject_model');
			
			$data['subjects'] = $this->CI->subject_model->ListSubjects(14);

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_subjects');
			
			switch ($action) {
				case 'list_subjects':
		
					break;
					
				case 'add_course':

					$data['course_code']       = $this->CI->input->post('course_code');
					$data['descriptive_title'] = $this->CI->input->post('descriptive_title');
					$data['paying_units']      = $this->CI->input->post('paying_units');
					$data['credit_units']      = $this->CI->input->post('credit_units');
					$data['course_types_id']   = $this->CI->input->post('course_types_id');
					$data['owner_colleges_id'] = 14;
					$data['courses_groups_id'] = 1;
					$data['inserted_by']  	   = $this->CI->session->userdata('empno');
					
					$search = $this->CI->subject_model->getCourseCode($this->CI->input->post('course_code'));		

					if ($search) {
						$my_return['found'] = TRUE;
					} else {
						$my_return['found'] = FALSE;
						$this->CI->subject_model->AddBEDSubject($data);
					}

					$data['subjects'] = $this->CI->subject_model->ListSubjects(14);
					
					$my_return['output'] = $this->CI->load->view('bed/subjects/subjects_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'delete_course':

					if ($this->CI->subject_model->DeleteSubject($this->CI->input->post('course_id'))) {
						$my_return['msg'] = TRUE;
					} else {
						$my_return['msg'] = FALSE;
					}

					$data['subjects'] = $this->CI->subject_model->ListSubjects(14);
					
					$my_return['output'] = $this->CI->load->view('bed/subjects/subjects_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'update_course':

					$data['id']                = $this->CI->input->post('course_id');
					$data['course_code']       = $this->CI->input->post('course_code');
					$data['descriptive_title'] = $this->CI->input->post('descriptive_title');
					$data['paying_units']      = $this->CI->input->post('paying_units');
					$data['credit_units']      = $this->CI->input->post('credit_units');
					$data['course_types_id']   = $this->CI->input->post('course_types_id');
					$data['updated_by']  	   = $this->CI->session->userdata('empno');
					
					if ($this->CI->subject_model->UpdateSubject($data)) {
						$my_return['msg'] = TRUE;
					} else {
						$my_return['msg'] = FALSE;
					}

					$data['subjects'] = $this->CI->subject_model->ListSubjects(14);
					
					$my_return['output'] = $this->CI->load->view('bed/subjects/subjects_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'search_course_code':
					
					if ($this->CI->input->post('original_course_code')) {
						$search = $this->CI->subject_model->getCourseCode($this->CI->input->post('search_code'),$this->CI->input->post('original_course_code'));
					} else {
						$search = $this->CI->subject_model->getCourseCode($this->CI->input->post('search_code'));
					}
					
					if ($search) {
						$my_return['found'] = TRUE;
					} else {
						$my_return['found'] = FALSE;
					}

					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
			}
				
			$this->CI->content_lib->enqueue_body_content('bed/subjects_management',$data);
			$this->CI->content_lib->content();
				
		}

		
	}
	
?>