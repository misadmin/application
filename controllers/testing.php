<?php

class Testing extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$menu = array(
				'role'=>'dean',
				'name'=>'John Doe',
				'menu'=>array(
						'Student'=>'student',
						'Faculty'=>'faculty',
						'Pull Down'=>array('Tuition Report - Group'=>'tuition_report_group',
						'Tuition Report - College'=>'tuition_report_college'))
		);
		$this->content_lib->set_navbar_content('', $menu);
	}
	
	function index(){
		$this->load->model('teller/terminal_model');
		echo "IP: " . $this->input->ip_address(); die();
		if($ret = $this->terminal_model->retrieve('10.10.9.240', 'lQXzNMIHxaNmvAVWd8jfApGD8vfXN0Jx21mfR8RqHrqTxxM0Qv')){
			print_r($ret);
		} else 
			echo "error retrieving terminals";
	}
	
	public function prerequisites_of_prerequisite (){
		
		//201
		$this->load->model('hnumis/Prospectus_model');
		print_r ($this->Prospectus_model->all_courses_with_cascaded_prerequisites (207));
	}
	
	public function add_address(){
		
		$this->load->model('student_common_model');
		print_r ($this->student_common_model->student_address(6129700));
		
		
	}
	
	public function search_employee(){
		$this->load->model('employee_common_model');
		$data = '{"fathers_name":"Alex Reyes","fathers_address":"","mothers_name":"Marissa Casquejo","mothers_address":"","guardians_name":"Jose Ni�o Quimson","guardians_address":"Sitio Trigons San Isidro","spouse_name":"Jose Ni�o Quimson","spouse_address":"Sitio Trigons San Isidro"}';
		print_r (json_decode(utf8_encode($data)));
		//echo $data;
	}

	public function terms(){
		$this->load->model('registrar_model');
		
		print_r ($this->registrar_model->current_and_upcoming_terms());
	}
	
	public function add_academic_year(){
		$this->load->model('registrar_model');
		
		if ($this->registrar_model->add_academic_year())
			echo "Another Academic Year added..."; else
			echo "Error adding academic Year";
	}
	
	public function academic_terms(){
		$this->content_lib->enqueue_header_style('stepy');
		$this->content_lib->enqueue_footer_script('stepy');
		$this->content_lib->enqueue_body_content('registrar/academic_terms');
		$this->content_lib->content();
	}
	
	public function update_academic_terms (){
		$this->load->model('registrar_model');
		
		if ($this->registrar_model->update_academic_terms (244, array('start_date'=>'2013-04-01')))
			echo "Academic Term Updated"; else
			echo "Academic Term NOT updated";
	}
	
	public function update_periods (){
		$this->load->model('registrar_model');
		$data = array(
				'Prelim'=>array('period_id'=>'41',
								'start_date'=>'2013-07-06',
								'end_date'=>'2013-07-20',));
		
		if ($this->registrar_model->update_periods ($data))
			echo "Periods Updated"; else
			echo "Periods NOT updated";
	}
	
	public function update_enrollment_schedules(){
		$this->load->model('registrar_model');
		$data = array(
				0 => Array
                        (   "id" => "00031",
                            "start_date" => '2013-05-15', 
                            "end_date" => '2013-06-20', 
                        ),

                    1 => Array
                        (
                             "id" => "00032",
                            "start_date" => '2013-06-01', 
                            "end_date" => '2013-06-20', 
                        ),
				);
		
		if ($this->registrar_model->update_enrollment_schedules ($data))
			echo "Enrollment Schedules Updated"; else
			echo "Enrollment Schedules NOT updated";
	}
	
	public function regex_test (){
		$regex = '#^(NA|DR|INE|WD|[1-4]\.[0-9]|5\.0)$#i';
		$string = "INE";
		if (preg_match($regex, $string))
			echo "matched"; else
			echo "not matched";
	}
	
	public function browser (){
		echo $_SERVER['HTTP_USER_AGENT'];	
	}
	
	public function test_json(){
		$value = array(array('id'=>20, 'amount'=>300), array('id'=>21, 'amount'=>400));
		
		echo json_encode($value);
	}
	
	public function student_number(){
		$this->load->helper('student_helper');
		$this->load->model('student_common_model');
		
		print_r ($this->student_common_model->last_id_number());
	}
	
	public function test_input(){
		$str = "[{'type':'check','amount':'800','bank_id':'00005','check_date':'03/09/2013','payee':'justin','check_date':'03/09/2013','check_number':'89898'}]";
		
		print_r (json_decode($str));
	}
	
	public function christian_testing(){
		$this->load->library('tab_lib');
		$this->load->library('print_lib');
		
		$this->tab_lib->enqueue_tab('Clearance Print', 'print_templates/student_clearance', array(), 'print_clearance', TRUE);
		//$this->tab_lib->enqueue_tab('Print Grades', 'print_templates/student_grades', array(), 'print_grades', FALSE);
		//$this->tab_lib->enqueue_tab('Lab Fees', 'accounts/laboratory_fees', array(), 'laboratory_fees', FALSE);
		//$this->tab_lib->enqueue_tab('Receipts', 'teller/receipt_report', array(), 'receipts', FALSE);
		//$this->tab_lib->enqueue_tab('Assessment', 'accounts/assessment_table', array(), 'assessment_table', FALSE);
		//$this->tab_lib->enqueue_tab('Fees Schedule', 'accounts/fees_schedule_edit_form', array(), 'fees_schedule_edit_form', FALSE);
		
		//$this->content_lib->enqueue_body_content('accounts/fees_schedule_edit_form', array());
	
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->content();
		echo sprintf($this->config->item('jzebra_applet'), base_url());
	}
	
	public function accounts_sched_form_testing(){
		$this->load->library('tab_lib');
		$this->content_lib->enqueue_body_content('accounts/fees_schedule_form', array());
	
		$this->content_lib->content();
	}
	
	public function accounts_sched_edit_form_testing(){
		$this->load->library('tab_lib');
		$this->content_lib->enqueue_body_content('accounts/fees_schedule_edit_form', array());
	
		$this->content_lib->content();
	}
	
	public function accounts_sched_testing(){
		$this->load->library('tab_lib');
		$this->content_lib->enqueue_body_content('accounts/fees_schedule', array());
	
		$this->content_lib->content();
	}
	
	public function accounts_levels_testing(){
		$this->load->library('tab_lib');
		$this->content_lib->enqueue_body_content('accounts/levels_table', array());
	
		$this->content_lib->content();
	}
	
	public function test_minify(){
		$this->load->library('unit_test');
		$str = '
<table class="table table-bordered">
    {rows}
        <tr>
        <td>{item}</td>
        <td>{result}</td>
        </tr>
    {/rows}
</table>';
		$this->unit->set_template($str);
		
		//Testing with new lines.
		$test = Content_lib::minify("This is a test
			This is a test
				
			This is a      test..");
		$expected = "This is a test This is a test This is a test.";
		$this->unit->run($test, $expected, 'Minify Test #1', 'replace all newlines, tabs and double or multiple spaces with single space');
		
		//Testing with single line comments
		$test = Content_lib::minify("This is a test
				//This is 123 a comment
				This is a test.");
		$expected = "This is a test This is a test.";
		$this->unit->run($test, $expected, 'Minify Test #2', 'single line comments should be removed.');
		
		
		//testing with multiline (doc) comments
		$test = Content_lib::minify("This is a test
				/* This 123 is a doc comment
				Another line
				another */
				This is a test."); 
		$expected = "This is a test This is a test.";
		$this->unit->run($test, $expected, 'Minify Test #3', 'multiple line comments should be removed.');
		
		//testing with multiline (doc) comments on one line
		$test = Content_lib::minify("This is a test
				/* This 123 is a doc comment */
				This is a test."); 
		$expected = "This is a test This is a test.";
		$this->unit->run($test, $expected, 'Minify Test #4', 'multiple line comments on one line should be removed.');
		
		$test = TRUE;
		$expected = FALSE;
		$this->unit->run($test, $expected, 'Test if equal');
		$this->content_lib->enqueue_body_content('', $this->unit->report());
		$this->content_lib->content();
	}
	
	public function table_lib_test(){
		$data = array(
			array(
				"id"=>"1",
				"name"=>"Kevin Felisilda",
				"gender"=>"male"	),
			array(
				"id"=>"2",
				"name"=>"Rey Agunod",
				"gender"=>"male"),
			array(
				"id"=>"3",
				"name"=>"Justin",
				"gender"=>"male"),
				);
		
		$this->load->library('table_lib');
		
		$this->content_lib->enqueue_body_content("developers/test_table", array("data"=>$data));
		//$this->table_lib->set_table_attributes(array("id"=>"table_id"));
		//$this->table_lib->set_thead_row(array("id", "name", "gender"));
		
		$this->content_lib->content();
		//print_r ($this->table_lib->content());	
	}
	
	public function table_lib_unit_testing(){
		$this->load->library('table_lib');
		
		$this->load->library('unit_test');
		$str = '
<table class="table table-bordered">
    {rows}
        <tr>
        <td>{item}</td>
        <td>{result}</td>
        </tr>
    {/rows}
</table>';
		$this->unit->set_template($str);
		$this->table_lib->set_attributes(array("id"=>"table_id","class"=>"table"));
		
		$test = Content_lib::minify($this->table_lib->content());
		$expected = Content_lib::minify('<table id="table_id" class="table"></table>');
		$this->unit->run($test, $expected, 'Table with attributes');
		
		//=============================================================================================================
		
		$this->table_lib->clear_all_data();
		$this->table_lib->insert_head_row(array(array("id"=>"name","value"=>"Name"),array("id"=>"gender","value"=>"Gender")));
		$this->table_lib->insert_data(array("name"=>"Kevin","gender"=>"M"));
		$test = Content_lib::minify($this->table_lib->content());
		
		$expected = Content_lib::minify('<table><thead><tr><th>Name</th><th>Gender</th></tr></thead><tbody><tr><td>Kevin</td><td>M</td></tr></tbody></table>');
		$this->unit->run($test, $expected, 'Table with data');
		$this->content_lib->enqueue_body_content('', $this->unit->report());
		$this->content_lib->content();
	}
	
	public function period_test(){
		$this->load->model('academic_terms_model');
		$this->content_lib->enqueue_body_content('', $this->academic_terms_model->what_period(mktime(12, 0, 0, 12, 12, 2012)));
		$this->content_lib->content();
	}
	
	public function reg_exp (){
		$remarks = 'Admit until: Prelims 0.282123';
		
		if (preg_match('!Admit until: (Prelims|Midterms|Pre-Finals|Finals)!', $remarks, $matches)){
			$last_payment = $matches[0][0];
			print_r ($matches);
		}
	}

/***************************************************
test for posting
*****************************************************/

	public function tuition_report_group() {	
		
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/College_Model');
			$this->load->model('academic_terms_model');
			$this->load->model('hnumis/Programs_Model');
			$this->load->model('posting/Tuition_Report_Model');

	
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:	
					$this->select_group_acad_yr();				
					break;
				
				case 2:
					$years_id = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
					$this->generate_tuition_report($years_id->academic_years_id, $this->input->post('academic_terms_id'), 
													$this->input->post('program_groups_id'), $this->input->post('yr_level'), 0);
					break;
					
				case 3:
					$years_id = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
					$this->generate_tuition_report($years_id->academic_years_id, $this->input->post('academic_terms_id'), 
													$this->input->post('program_groups_id'), $this->input->post('yr_level'), 1);
					break;
			}

	}	


	public function tuition_report_college() {	
		
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/College_Model');
			$this->load->model('academic_terms_model');
			//$this->load->model('hnumis/Programs_Model');
			$this->load->model('posting/Tuition_Report_Model');

	
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
			switch ($step) {
				case 1:	
					$this->select_college_acad_yr();				
					break;
				
				case 2:
					$years_id = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
					$this->generate_tuition_report_college($years_id->academic_years_id, $this->input->post('academic_terms_id'), 
													$this->input->post('colleges_id'), $this->input->post('yr_level'), 0);
					break;
					
				case 3:
					$years_id = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
					$this->generate_tuition_report_college($years_id->academic_years_id, $this->input->post('academic_terms_id'), 
													$this->input->post('colleges_id'), $this->input->post('yr_level'), 1);
					break;
			}

	}	


	private	function select_group_acad_yr() {
	
			
		$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
		$data['program_groups'] = $this->Programs_Model->ListProgramWithGroups();
	
		$this->content_lib->enqueue_body_content('posting/select_group_acad_yr',$data);
		$this->content_lib->content();
			
	}


	private	function select_college_acad_yr() {
	
			
		$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
		$data['colleges'] = $this->College_Model->ListColleges();
	
		$this->content_lib->enqueue_body_content('posting/select_college_acad_yr',$data);
		$this->content_lib->content();
			
	}


	private	function generate_tuition_report($academic_years_id, $academic_terms_id, $program_groups_id, $yr_level, $value) {
	
		$data['term'] = $this->AcademicYears_Model->getAcademicTerms($academic_terms_id);	
		$data['students'] = $this->Tuition_Report_Model->getStudentsEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level);

		$data['tuition_basics'] = $this->Tuition_Report_Model->getTuitionFeeEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level);

		$data['tuition_others'] = $this->Tuition_Report_Model->ListTuitionOthersEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level);
		$data['matriculation'] = $this->Tuition_Report_Model->getMatriculationEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level);
		$data['misc_fees'] = $this->Tuition_Report_Model->ListMiscEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level);
		$data['other_fees'] = $this->Tuition_Report_Model->ListOthersEnrolled($academic_terms_id, $program_groups_id, NULL, $yr_level);
		$data['lab_fees'] = $this->Tuition_Report_Model->ListLabFeesEnrolled($academic_years_id, $academic_terms_id, $program_groups_id, NULL, $yr_level);
		
		if ($value == 0) {
			$this->content_lib->enqueue_body_content('posting/tuition_report',$data);
			$this->content_lib->content();
		} else {
			$this->generate_report_pdf($data);
		}
				
	}


	private	function generate_tuition_report_college($academic_years_id, $academic_terms_id, $colleges_id, $yr_level, $value) {
	
		$data['term'] = $this->AcademicYears_Model->getAcademicTerms($academic_terms_id);	
		$data['students'] = $this->Tuition_Report_Model->getStudentsEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level);

		$data['tuition_basics'] = $this->Tuition_Report_Model->getTuitionFeeEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level);

		$data['tuition_others'] = $this->Tuition_Report_Model->ListTuitionOthersEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level);
		$data['matriculation'] = $this->Tuition_Report_Model->getMatriculationEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level);
		$data['misc_fees'] = $this->Tuition_Report_Model->ListMiscEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level);
		$data['other_fees'] = $this->Tuition_Report_Model->ListOthersEnrolled($academic_terms_id, NULL, $colleges_id, $yr_level);
		$data['lab_fees'] = $this->Tuition_Report_Model->ListLabFeesEnrolled($academic_years_id, $academic_terms_id, NULL, $colleges_id, $yr_level);
		
		if ($value == 0) {
			$this->content_lib->enqueue_body_content('posting/tuition_report',$data);
			$this->content_lib->content();
		} else {
			$this->generate_report_pdf($data);
		}
				
	}
	
	
	
	private function generate_report_pdf($tuition) {

			$this->load->library('my_pdf');
			$this->load->library('hnumis_pdf');
			$l = 4;
		
			$this->hnumis_pdf->AliasNbPages();		
			$this->hnumis_pdf->set_HeaderTitle('TUITION REPORT FOR: '.$tuition['students']->year_level." - ".$tuition['students']->abbreviation.
													" [".$tuition['term']->term." ".$tuition['term']->sy."]");
			$this->hnumis_pdf->AddPage('P','letter');
			
			$this->hnumis_pdf->SetDisplayMode('fullwidth');
			$this->hnumis_pdf->SetDrawColor(200,200,200);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			
			$this->hnumis_pdf->SetFont('times','',10);
			$this->hnumis_pdf->SetTextColor(10,10,10);

			$this->hnumis_pdf->SetFont('times','B',12);
			$this->hnumis_pdf->SetFillColor(200,200,200);
			
			$this->hnumis_pdf->Cell(200,$l+2,'# of Students: '.$tuition['students']->numEnroll,0,0,'L',true);
			$this->hnumis_pdf->SetFont('times','',10);
			$this->hnumis_pdf->SetFillColor(255,255,255);
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();

			$this->hnumis_pdf->Cell(200,$l,'Tuition & REED Fees: '.$tuition['students']->total_units,0,0,'L',true);
			$this->hnumis_pdf->Ln();
	  		$tuition_fee = 0;
	  		if ($tuition['tuition_basics']) {
	  			$basic_total = 0;
	  			foreach ($tuition['tuition_basics'] AS $tuition_basic) {
					$this->hnumis_pdf->SetX(14);
					$this->hnumis_pdf->Cell(100,$l,'Tuition Fee ['.$tuition_basic->total_units." @ ".$tuition_basic->rate.']',0,0,'L',true);
			  		$basic_total =  $basic_total + ($tuition_basic->total_units * $tuition_basic->rate);
					$this->hnumis_pdf->Cell(25,$l,number_format(($tuition_basic->total_units * $tuition_basic->rate),2),0,0,'R',true);
					$this->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($basic_total);	  
				$this->hnumis_pdf->SetX(14);
				$this->hnumis_pdf->Cell(100,$l,'Tuition Fee',0,0,'L',true);
				$this->hnumis_pdf->Cell(50,$l,number_format($basic_total,2),0,0,'R',true);
			}

			$this->hnumis_pdf->Ln();
			
			if ($tuition['tuition_others']) {
				foreach($tuition['tuition_others'] AS $other) {
					$this->hnumis_pdf->SetX(14);
					$this->hnumis_pdf->Cell(100,$l,$other->course_code.' Fee ['.$other->paying_units.
										' @ '.$other->rate.']',0,0,'L',true);
					$total_others = $other->paying_units * $other->rate;
					$tuition_fee = $tuition_fee + $total_others;
					$this->hnumis_pdf->Cell(50,$l,number_format($total_others,2),0,0,'R',true);
					$this->hnumis_pdf->Ln();
				}
			}		
			$this->hnumis_pdf->Cell(190,$l,number_format($tuition_fee,2),0,0,'R',true);
			$this->hnumis_pdf->Ln();

	  		if ($tuition['matriculation']) {
	  			$matri_total = 0;
				$this->hnumis_pdf->Cell(200,$l+2,'Matriculation, Misc & Other Fees:',0,0,'L',true);
				$this->hnumis_pdf->Ln();
				foreach ($tuition['matriculation'] AS $matri) {
					$this->hnumis_pdf->SetX(14);						
					$this->hnumis_pdf->Cell(100,$l,'Matriculation @ '.number_format($matri->rate,2),0,0,'L',true);
					$matri_total = $matri_total + ($matri->numEnroll*$matri->rate);
					$this->hnumis_pdf->Cell(50,$l,number_format($matri->numEnroll*$matri->rate,2),0,0,'R',true);				
					$this->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($matri_total);
				$this->hnumis_pdf->Cell(190,$l,number_format($matri_total,2),0,0,'R',true);
				$this->hnumis_pdf->Ln();
			}
			
			
			if ($tuition['misc_fees']) {
				$this->hnumis_pdf->SetX(14);						
				$this->hnumis_pdf->Cell(200,$l+2,'Miscellaneous Fees',0,0,'L',true);
				$this->hnumis_pdf->Ln();
	  			$misc_total = 0;
	  			foreach($tuition['misc_fees'] AS $misc_fee) {
					$this->hnumis_pdf->SetX(18);						
					$this->hnumis_pdf->Cell(100,$l,$misc_fee->description." @ ".$misc_fee->rate,0,0,'L',true);
					$misc_total = $misc_total + ($tuition['students']->numEnroll*$misc_fee->rate);
					$this->hnumis_pdf->Cell(46,$l,number_format($tuition['students']->numEnroll*$misc_fee->rate,2),0,0,'R',true);
					$this->hnumis_pdf->Ln();
				}			
				$tuition_fee = $tuition_fee + ($misc_total);
				$this->hnumis_pdf->Cell(190,$l,number_format($misc_total,2),0,0,'R',true);
				$this->hnumis_pdf->Ln();
			}

	  		if ($tuition['other_fees']) {
				$this->hnumis_pdf->SetX(14);						
				$this->hnumis_pdf->Cell(200,$l+2,'Other School Fees',0,0,'L',true);
				$this->hnumis_pdf->Ln();
	  			$others_total = 0;
	  			foreach($tuition['other_fees'] AS $other) {
					$this->hnumis_pdf->SetX(18);						
					$this->hnumis_pdf->Cell(100,$l,$other->description." @ ".$other->rate,0,0,'L',true);
					$others_total = $others_total + ($tuition['students']->numEnroll*$other->rate);
					$this->hnumis_pdf->Cell(46,$l,number_format($tuition['students']->numEnroll*$other->rate,2),0,0,'R',true);
					$this->hnumis_pdf->Ln();
				}
				$tuition_fee = $tuition_fee + ($others_total);
				$this->hnumis_pdf->Cell(190,$l,number_format($others_total,2),0,0,'R',true);
				$this->hnumis_pdf->Ln();
			}
			$this->hnumis_pdf->Ln();
			
			if ($tuition['lab_fees']) {
				$this->hnumis_pdf->Cell(200,$l+2,'Laboratory Fees:',0,0,'L',true);
				$this->hnumis_pdf->Ln();
	  			$lab_total = 0;
	  			foreach($tuition['lab_fees'] AS $lab_fee) {
					$this->hnumis_pdf->SetX(14);						
					$this->hnumis_pdf->Cell(100,$l,$lab_fee->course_code." @ ".$lab_fee->rate,0,0,'L',true);
					$lab_total = $lab_total + $lab_fee->fees;
					$this->hnumis_pdf->Cell(50,$l,number_format($lab_fee->fees,2),0,0,'R',true);
					$this->hnumis_pdf->Ln();				
				}
				$tuition_fee = $tuition_fee + ($lab_total);
				$this->hnumis_pdf->Cell(190,$l,number_format($lab_total,2),0,0,'R',true);
				$this->hnumis_pdf->Ln();
			}
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->Ln();
			$this->hnumis_pdf->SetFont('times','B',12);
			$this->hnumis_pdf->SetFillColor(200,200,200);
			$this->hnumis_pdf->Cell(100,$l+2,'TOTAL Assessment: ',1,0,'L',true);
			$this->hnumis_pdf->Cell(90,$l+2,number_format($tuition_fee,2),1,0,'R',true);
			$this->hnumis_pdf->Cell(9,$l+2,'',1,0,'R',true);

			$this->hnumis_pdf->Output();
	
	}






/***************************************************
END test for posting
*****************************************************/


}