<?php
$levels = array(
		'1'=>array(
				'name'=>'kinder'
				),
		'2'=>array(
				'name'=>'grade school'
				),
		'3'=>array(
				'name'=>'high school(day)'
				),
		'4'=>array(
				'name'=>'high school(night)'
				),
		'5'=>array(
				'name'=>'college',
				'children'=>array(
					'51'=>'subject fee',
					'52'=>'laboratory fee'
				)
		),
		'6'=>array(
				'name'=>'graduate school',
				'children'=>array(
						'61'=>'subject fee',
						'62'=>'laboratory fee'
				)
		)
);

//print_r($college);
$academic_groups['college'] = $college;
$academic_groups['graduate school'] = $graduate_school;
//print_r($levels);

?>
<script>
	$( document ).ready(function() {
		$(".sub").hide();
		$(".graduateschool_group").hide;
		$("tr.level").bind('click', function() {
			var g = $(this).attr('id');
			console.log(g);
	    	$("."+g+"_group").toggle("slow");
	    });
	});
</script>

<table class="table">
	<thead>
		<tr>
			<th width="35%">Levels</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($levels as $key=>$val):?>
		<tr class="level" id="<?php echo $key; ?>">
			<td level-id="<?php echo $key; ?>"><?php echo $val['name']; ?></td>
			<?php if($val['name'] == 'college' || $val['name'] == 'graduate school'): ?>
			<td>&nbsp;</td>
		</tr>
			<?php foreach($academic_groups[$val['name']] as $acad_key=>$acad_val): ?>		
		<tr class="sub <?php echo $key; ?>_group">
			<td id="<?php echo $acad_key;?>">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $acad_val;?></td>
			<td><a href="#" class="view_fees_schedule" acad-name="<?php echo $acad_val;?>" acad-key="<?php echo $acad_key; ?>">View</a></td>
		</tr>
		<?php
				endforeach;
			else:
		?>
			<td><a>View</a></td>
		</tr>
		<?php
			endif;
			if(isset($val['children'])):
				foreach($val['children'] as $child_key=>$child_val):
			?>
		<tr>
			<td child-id="<?php echo $child_key?>">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $child_val; ?></td>
			<td><a
				<?php if($child_val == 'laboratory fee') echo "class='view_lab_fees'"; ?>
					 lab-id="<?php echo $child_key?>">View</a></td>
		</tr>
			<?php 
				endforeach;
			endif; ?>
	<?php endforeach;?>
	</tbody>
</table>
<form method="post" id="view_fees_schedule">
<?php action_form('show_fees_schedule', FALSE); ?>
<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<form method="post" id="view_lab_fees">
<?php action_form('show_lab_fees', FALSE); ?>
<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('.view_lab_fees').bind('click', function(event){
		var lab_id = $(this).attr('lab-id');
		
		console.log(lab_id);

		$('<input>').attr({
		    type: 'hidden',
		    name: 'lab-id',
		    value: lab_id,
		}).appendTo('#view_lab_fees');

		$('#view_lab_fees').submit();
		
	});
	
	$('.view_fees_schedule').bind('click', function(event){
		event.preventDefault();
		var acad_key = $(this).attr('acad-key');
		var acad_program_group = $(this).attr('acad-name');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'acad_key',
		    value: acad_key,
		}).appendTo('#view_fees_schedule');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'acad_name',
		    value: acad_program_group,
		}).appendTo('#view_fees_schedule');
		
		$('#view_fees_schedule').submit();
	});	
</script>