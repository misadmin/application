 
<?php //print_r($provinces); die(); ?>

<style>
	req {
		color: red;
		font-weight: bold;
		font-size: 18px;
	}
</style>

<div class="row-fluid" style="width:100%; float:left;">

		<div class="form-horizontal">
			<fieldset>
				<div class="control-group formSep">
					<label for="emergency_notify" class="control-label">Person to Notify:<req>*</req></label>
					<div class="controls" id="emergency_notify_controls">
						<input type="text" name="emergency_notify" id="emergency_notify" class="input-xlarge" value="<?php if(isset($emergency_notify)) echo $emergency_notify; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="emergency_relation" class="control-label">Relation to Person:</label>
					<div class="controls" id="emergency_relation_controls">
						<input type="text" name="emergency_relation" id="emergency_relation" class="input-xlarge" value="<?php if(isset($emergency_relation)) echo $emergency_relation; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="emergency_telephone" class="control-label">Telephone/Mobile No.:<req>*</req></label>
					<div class="controls" id="primary_school_controls">
						<input type="text" name="emergency_telephone" id="emergency_telephone" class="input-xlarge" value="<?php if(isset($emergency_telephone)) echo $emergency_telephone; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="emergency_email_address" class="control-label">Email Address:</label>
					<div class="controls" id="emergency_email_address_controls">
						<input type="text" name="emergency_email_address" id="emergency_email_address" class="input-xlarge" value="<?php if(isset($emergency_email_address)) echo $emergency_email_address; ?>" />
					</div>
				</div>
								<div class="control-group formSep">
									<label for="emergency_address" class="control-label">
										Address:<req>*</req><br>
										<button class="btn btn-primary new_address" style="font-size:11px; height:22px;padding:1px;">New Address?</button>
									</label>
									<div class="controls">
										<div id="country_group" style="float:left; padding:5px;">
											<?php 
												$data1['countries'] = $countries;
												$data1['type']      = "update_student";
												$this->load->view('common/places/select_countries', $data1);
											?>	
										</div>
										<div id="province_group" style="float:left; padding:5px; display:none;">
											<?php 
												if ($provinces) {
													$data1['provinces'] = $provinces;
													$this->load->view('common/places/select_provinces', $data1);
												}
											?>	
										</div> 
										<div id="town_group" style="float:left; padding:5px;">
											<?php 
												if ($towns) {
													$data1['towns'] = $towns;
													$this->load->view('common/places/select_towns', $data1);
												}
											?>	
										</div> 
										<div id="brgy_group" style="float:left; padding:5px; ">
											<?php 
												if ($barangays) {
													$data1['brgys'] = $barangays;
													$this->load->view('common/places/select_brgys', $data1);
												}
											?>	
										</div> 
									</div>
								</div>
				<div class="control-group">
					<div class="controls">
						<!--NOTE: use register_student as id to match with jquery id in views: common/places/select_countries -->
						<input type="submit" class="btn btn-success" name="register_student" id="register_student" value="Update Emergency Contact Information!" disabled />
					</div>
				</div>
			</fieldset>
		</div>
</div>

<script>
	$(document).ready(function() {
		
		var e_person  = $('#emergency_notify').val();
		var e_contact = $('#emergency_telephone').val();
		var e_address = $("select#brgy_id option:selected").attr('value'); 

		$('#country_id').val('<?php print($countries_id); ?>');
		$('#province_id').val('<?php print($provinces_id); ?>');
		$('#town_id').val('<?php print($towns_id); ?>');
		$('#brgy_id').val('<?php print($barangays_id); ?>');
		$('#street_name').val('<?php print($street_name); ?>');

		<?php 
			if ($barangays_id) {
		?>
				$('#province_group').show();
				$('#town_group').show();
				$('#brgy_group').show();
		<?php
			}
		?>
		var req_array = [e_person,e_contact,e_address];
		
		if (required_isEmpty(req_array)) {
			$('#register_student').prop("disabled", true);
		} else {
			$('#register_student').prop("disabled", false);
		}

		
		function required_isEmpty(req_array) {
			
			var aLen; 
			aLen = req_array.length;
			
			for (i = 0; i < aLen; i++) {	

				if (!req_array[i]) {
					return true;
				}
			}
			
			return false;
		}
		
	});
</script>

<script>
	$("#emergency_notify").keyup(function(){
		
		var e_person   = $('#emergency_notify').val();
		var e_contact  = $('#emergency_telephone').val();
		var e_address  = $("select#brgy_id option:selected").attr('value'); 

		var req_array  = [e_person,e_contact,e_address];
		
			if (required_isEmpty(req_array) || e_person.trim().length == 0) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}

	});

	$("#emergency_telephone").keyup(function(){
		
		var e_person   = $('#emergency_notify').val();
		var e_contact  = $('#emergency_telephone').val();
		var e_address  = $("select#brgy_id option:selected").attr('value'); 

		var req_array  = [e_person,e_contact,e_address];
		
			if (required_isEmpty(req_array) || e_contact.trim().length == 0) {
				$('#register_student').prop("disabled", true);
			} else {
				$('#register_student').prop("disabled", false);
			}

	});

	function required_isEmpty(req_array) {
		
		var aLen; 
		aLen = req_array.length;
		
		for (i = 0; i < aLen; i++) {	

			if (!req_array[i]) {
				return true;
			}
		}
		
		return false;
	}
	
</script>

	

		<form id="student_emergency_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_emergency_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
		</form>
		
	<script>
		$('#register_student').click(function(e){
			e.preventDefault();

			var e_person      = $('#emergency_notify').val();
			var e_relation    = $('#emergency_relation').val();
			var e_contact     = $('#emergency_telephone').val();
			var email_address = $('#emergency_email_address').val();
			
			var e_address   = $("select#brgy_id option:selected").attr('value'); 
			var st_name     = $('#street_name').val();

				var confirmed = confirm('Continue to update Emergency Information?');

				if (confirmed){

					$('<input>').attr({
						type: 'hidden',
						name: 'emergency_notify',
						value: e_person,
					}).appendTo('#student_emergency_information_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'emergency_relation',
						value: e_relation,
					}).appendTo('#student_emergency_information_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'emergency_telephone',
						value: e_contact,
					}).appendTo('#student_emergency_information_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'emergency_email_address',
						value: email_address,
					}).appendTo('#student_emergency_information_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'emergency_address',
						value: e_address,
					}).appendTo('#student_emergency_information_form');		
					$('<input>').attr({
						type: 'hidden',
						name: 'street_name',
						value: st_name,
					}).appendTo('#student_emergency_information_form');		
					
					$('#student_emergency_information_form').submit();
				}
		
		});
		
	</script>
		
		