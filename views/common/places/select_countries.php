

		<select name="country_id" id="country_id" class="form-control" >
			<option value="">--Please Select--</option>
			<?php
				foreach ($countries AS $country) {
			?>
					<option value="<?php print($country->id); ?>" country_name="<?php print($country->name); ?>" ><?php print($country->name); ?></option>
			<?php
				} 
			?>
		</select>

		
<script>

		$("select#country_id").change(function(){

			var country_id = $("select#country_id option:selected").attr('value'); 
			var type       = '<?php print($type); ?>';

			$("#province_group").hide();
			$("#town_group").hide();
			$("#brgy_group").hide();
			$('#register_student').prop("disabled", true);
				
			if (country_id.length > 0 ) { 

				$('#province_group').show();
				
				$.ajax({
					type: "POST",
					url: "<?php echo site_url($this->uri->segment(1).'/extract_places');?>",
					data: {
							"action": 'list_provinces',
							"type": type,
							"country_id": country_id
							},
					cache: false,
					beforeSend: function () { 
						$('#province_group').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");
					},
					success: function(html) {    
						$("#province_group").html( html );
					}
				});
			}
		});

</script>
		