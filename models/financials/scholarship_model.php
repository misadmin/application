<?php

	class Scholarship_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


	    function AddScholarshipItem($data) 
		{
			$query = "INSERT INTO scholarships (id, scholarship_code, scholarship, max_unit_grad, max_unit_undergrad) 
   						VALUES ('',
						   	   	 {$this->db->escape($data['scholarship_code'])},
								 {$this->db->escape($data['scholarship'])},
								 {$this->db->escape($data['max_grad'])},
								 {$this->db->escape($data['max_under'])})";
			$this->db->query($query);
			$id = @mysql_insert_id();
			//print($id);
		//	die();
			return $id;
			
		}

	function ListPrivileges() {
			$result = null;
				
			$q1 = "SELECT
					id,
					scholarship_code,
					scholarship, 
					max_unit_grad,
					max_unit_undergrad
				   FROM
					scholarships
				   ORDER BY 
					  scholarship";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}					

	
	function ListPrivilegesNotTaken($student_histories_id, $bed = NULL) {
		$result = null;
		if(!$bed){
		$q1 = "SELECT
					id,
					scholarship_code,
					scholarship
				FROM
					scholarships
				WHERE
					id NOT IN (SELECT 
									scholarships_id 
										FROM
											privileges_availed
										WHERE
											student_histories_id={$this->db->escape($student_histories_id)})
					AND id != 186
				   ORDER BY
					  scholarship";
		}else {
			$q1= "SELECT
					id,
					scholarship_code,
					scholarship
			FROM
				scholarships
			WHERE
				id NOT IN (SELECT
							scholarships_id
								FROM
									privileges_availed
								WHERE
									basic_ed_histories_id={$this->db->escape($student_histories_id)})
				AND id != 186
				ORDER BY scholarship";
		}
			
		//print($q1);
	//	die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
	}
	
	
	function getScholarship($privilege_availed_id){
	
		$result = null;
		
		$q = "SELECT 
					a.scholarship,
					b.scholarships_id,
					b.posted
				FROM
					scholarships as a,
					privileges_availed as b
				WHERE
					b.id = {$this->db->escape($privilege_availed_id)}
					AND b.scholarships_id = a.id";
	//	print_r($q);
	   // die();		
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
		
		
		return $result;
			
	}		
	
	function getTuitionPrivilege($privilege_availed_id){
	
		$result = null;
		
		$q = "SELECT 
		         (a.discount_percentage * 100) as discount_percentage,
			a.discount_amount
			 FROM
				privileges_availed_details as a
			 WHERE
				a.privileges_availed_id = {$this->db->escape($privilege_availed_id)}
				AND a.fees_groups_id = 9";
				
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
		//print_r($query);
	   // die();
		
		return $result;
			
	}		

	
	function AddStudentPrivilege($data, $bed = NULL) {		
		   if(!$bed){	
				$query = "
					INSERT into privileges_availed (student_histories_id, scholarships_id, academic_programs_id, year_level) VALUES (
					{$this->db->escape($data['student_history_id'])},
					{$this->db->escape($data['privilege_id'])},
					{$this->db->escape($data['academic_programs_id'])},
					{$this->db->escape($data['year_level'])} 
					)
				";
		   }else {
			   	$query = "
			   		INSERT into privileges_availed (basic_ed_histories_id, scholarships_id,levels_id,year_level) VALUES (
			   		{$this->db->escape($data['student_history_id'])},
			   		{$this->db->escape($data['privilege_id'])},
			   		{$this->db->escape($data['levels_id'])},
			   		{$this->db->escape($data['year_level'])}
					)
				";
		   }					
			//print($query); die();
		    //log_message("INFO", print_r($query,true)); // Toyet 7.3.2018 
			$this->db->query($query);
				
			$id = @mysql_insert_id();
			return $id;
	}	
		
	//UPDATED: 4/4/2013
	function AddPrivilegeDetails($privilege_availed_id, $data) {
		
		$query = "INSERT into privileges_availed_details 
					( privileges_availed_id,
					  fees_groups_id,
					  discount_percentage,
					  discount_amount)
					VALUES ({$this->db->escape($privilege_availed_id)},
							{$this->db->escape($data['fees_groups_id'])},
							{$this->db->escape($data['discount_percentage'])},
							{$this->db->escape($data['discount_amount'])})";
							
		$this->db->query($query);
			
		$id = @mysql_insert_id();

		$q = "UPDATE 
					privileges_availed 
				SET 
					total_amount_availed = total_amount_availed + {$this->db->escape($data['discount_amount'])}
				WHERE
					id={$this->db->escape($privilege_availed_id)}";

		$this->db->query($q);

		//log_message("INFO", print_r($query,true)); // Toyet 7.3.2018 
		//log_message("INFO", print_r($q,true)); // Toyet 7.3.2018 
		
		return $id;					
	}


	//Added: 3/9/2013
	function ListStudentsWithPrivileges($aca_term_id, $privilege_id) {
			$result = null;
				
			$q1 = "SELECT
						a.id,
						a.discount_percentage,
						a.discount_amount,
						a.students_idno,
						a.approve_status,
						b.lname,
						b.fname,
						c.year_level,
						e.abbreviation		
				  FROM
					  privileges_availed_details as a,
					  students as b,
					  student_histories as c,
					  prospectus as d,
					  academic_programs as e
				  WHERE
				  	  a.academic_terms_id = {$this->db->escape($aca_term_id)}
					  AND a.scholarships_id = {$this->db->escape($privilege_id)}
					  AND a.students_idno = c.students_idno
					  AND c.academic_terms_id = {$this->db->escape($aca_term_id)}
					  AND c.students_idno = b.idno
					  AND c.prospectus_id = d.id
					  AND d.academic_programs_id = e.id	  
				   GROUP BY
				      a.students_idno
				   ORDER BY 
					  b.lname";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}					

	
		
	function getStudentPrivilege($privilege_id){
	
		$result = null;
		
		$q = "SELECT 
				a.id,
				a.discount_percentage,
				a.discount_amount,
				a.students_idno,
				a.approve_status,
				b.lname,
				b.fname,
				c.year_level,
				e.abbreviation,	
				f.scholarship	
		
			FROM
				privileges_availed as a,
				students as b,
				student_histories as c,
				prospectus as d,
				academic_programs as e,
				scholarships as f
				
			WHERE
				a.id = {$this->db->escape($privilege_id)}
				AND a.scholarships_id = f.id
				AND a.students_idno = c.students_idno
			    AND a.academic_terms_id = c.academic_terms_id
			    AND c.students_idno = b.idno
				AND c.prospectus_id = d.id
				AND d.academic_programs_id = e.id";
				
				
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
		//print_r($query);
	   // die();
		
		return $result;
			
	}		
	
	//Added: 3/11/2013
	
	function UpdatePrivilege($id, $privilege_id, $priv_disc){
		$q = "UPDATE privileges_availed 
			SET 
				scholarships_id ={$this->db->escape($privilege_id)},
				discount_percentage = {$this->db->escape($priv_disc)} 
			WHERE
				id={$this->db->escape($id)}";
			
	//print_r($q);
	//die();
		if ($this->db->query($q)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}

	
	
	function ListPrivilegeAvailedDetails($privilege_availed_id) {
			$result = null;
				
			$q1 = "SELECT
					a.id,
					b.fees_group, 
					a.discount_amount,
					b.weight,
					a.discount_percentage,
					b.id as fees_group_id					
				   FROM
					privileges_availed_details as a,
					fees_groups as b
				   WHERE
				    a.privileges_availed_id = {$this->db->escape($privilege_availed_id)} 
					AND a.fees_groups_id = b.id
				   ORDER BY 
					 b.weight";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}				
	
	function PostPrivileges($privilege_availed_id, $remarks, $user_id) {
	
		$q1 = "UPDATE 
					privileges_availed 
				SET 
					remark = {$this->db->escape($remarks)},
					posted = 'Y',
					posted_on = NOW(),
					posted_by = $user_id
				WHERE
					id = {$this->db->escape($privilege_availed_id)} ";
					
		if ($this->db->query($q1)) {
			return TRUE;
		} else {
			return FALSE;
		}	
	
	}	
	
	function DeletePrivilegeDetails($privilege_availed_details_id, $discount_amount, $privilege_availed_id) {
			
			$q1 = "DELETE FROM 
						privileges_availed_details 
					WHERE
						id = {$this->db->escape($privilege_availed_details_id)} ";
			
			$query1 = $this->db->query($q1);
			
			$q2 = "UPDATE 
					privileges_availed 
				SET 
					total_amount_availed = total_amount_availed - {$this->db->escape($discount_amount)}
				WHERE
					id={$this->db->escape($privilege_availed_id)}";
			
			$query2 = $this->db->query($q2);
		//	print($q);
			//die();
			if ($query1 AND $query2) {
				return TRUE;
			} else {
				return FALSE;
			}			
	}
	
	function DeletePrivilegeAvailed($privilege_availed_id) {
	
		$q = "DELETE FROM 
						privileges_availed 
					WHERE
						id = {$this->db->escape($privilege_availed_id)} ";
			
			$query1 = $this->db->query($q);
			if ($query1) {
				return TRUE;
			} else {
				return FALSE;
			}			
	}
	
	//Added: 4/25/2013
	
	function getPrivilegeAvailedInfo($privilege_availed_id){
	
		$result = null;
		
		$q = "SELECT 
				a.id,
				a.total_amount_availed,
				a.scholarships_id, a.posted,
				b.discount_percentage,
				b.discount_amount, 
				c.scholarship
			 FROM privileges_availed as a 
			 	LEFT JOIN privileges_availed_details as b on a.id = b.privileges_availed_id,
				scholarships as c
			WHERE 
				a.scholarships_id = c.id
				AND b.privileges_availed_id = {$this->db->escape($privilege_availed_id)}
				AND b.fees_groups_id = 9";

		//print_r($q);
	   // die();		
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
		
		
		return $result;
	}	
	
		function EditPrivilegeAvailed($privilege_availed_id, $new_total_disc, $new_tuition_disc,																				
									$tuition_disc_rate, $scholarship_id){
									
		$q1 = "UPDATE privileges_availed 
			SET 
				scholarships_id ={$this->db->escape($scholarship_id)},
				total_amount_availed = {$this->db->escape($new_total_disc)}
			WHERE
				id={$this->db->escape($privilege_availed_id)}";
			
		$q2 = "UPDATE privileges_availed_details 
			SET 
				discount_percentage ={$this->db->escape($tuition_disc_rate)},
				discount_amount = {$this->db->escape($new_tuition_disc)}
			WHERE
				privileges_availed_details.privileges_availed_id={$this->db->escape($privilege_availed_id)}
				AND fees_groups_id = 9";

		if ($this->db->query($q1) AND $this->db->query($q2)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
	
	//Added 4/26/2013
	
	/*function listPastPrivileges($idnum, $acad_term_id){
			$result = null;
				
			$q1 = "SELECT
					a.id, 
					f.scholarship,
					a.total_amount_availed,
					CASE d.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
					END AS term, 
					CONCAT(e.end_year-1,'-',e.end_year) AS sy					
				   FROM
					 student_histories as c 
					   LEFT JOIN academic_terms as d ON c.academic_terms_id = d.id
						 LEFT JOIN academic_years as e ON d.academic_years_id = e.id,
					 privileges_availed as a,
					 scholarships as f
				   WHERE
				    c.students_idno = {$this->db->escape($idnum)}
					AND a.student_histories_id = c.id
					AND a.scholarships_id = f.id
					AND d.status = 'current'
					AND e.status = 'current'
					AND a.posted = 'N'
				  ";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}	*/	
	
	
	function getPastPrivileges($idnum, $acad_term_id){
			$result = null;
				
			$q1 = "SELECT
					a.id, 
					f.scholarship,
					a.total_amount_availed
			      FROM
					 student_histories as c,
					 privileges_availed as a,
					 scholarships as f
				   WHERE
				    c.students_idno = {$this->db->escape($idnum)}
					AND c.academic_terms_id = {$this->db->escape($acad_term_id)}
					AND a.student_histories_id = c.id
					AND a.scholarships_id = f.id
					AND a.posted = 'Y'
				  ";
					
		//	print($q1);
		//	die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
		return $result;
	}	
	
	function getPastPrivilegesDetails($priv_availed_id){
			$result = null;
				
			$q1 = "SELECT
					b.discount_percentage,
					b.discount_amount,
					b.fees_groups_id,
					g.fees_group,
					a.total_amount_availed,
					f.scholarship
			   FROM
     				 privileges_availed as a
					   LEFT JOIN privileges_availed_details	as b on a.id = b.privileges_availed_id,
					 fees_groups as g,
					 scholarships as f
					 
				   WHERE
				    b.privileges_availed_id = {$this->db->escape($priv_availed_id)}
					AND a.scholarships_id = f.id
					AND b.fees_groups_id = g.id";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}					
		
	function getPrivilegesDetailInfo($priv_availed_details_id){
			$result = null;
				
			$q1 = "SELECT
					a.discount_amount, a.privileges_availed_id
			   FROM
     				 privileges_availed_details as a
			   WHERE
				    a.id = {$this->db->escape($priv_availed_details_id)}
				";
					
			//print($q1);
			//die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
		return $result;
	}		
	
	//Added: 6/6/2013

		function ListStudentsAvailed($privilge_id, $current_term_id, $posted=NULL) {
			$result = null;
				
			if ($posted == 'All') {
				$q1 = "SELECT
						  a.lname, 
						  a.fname, 
						  a.mname, 
						  b.year_level, 
						  e.abbreviation, 
						  a.idno as students_idno, 
						  c.total_amount_availed as discount_amount, 
						  f.discount_percentage,
						  c.remark,
						  IF(c.posted='Y','Yes','No') AS posted
					   FROM
						  students as a, 
						  student_histories as b, 
						  privileges_availed as c, 
						  prospectus as d, 
						  academic_programs as e, 
						  privileges_availed_details as f
					   WHERE
					   	  	c.scholarships_id = {$this->db->escape($privilge_id)}
							AND c.student_histories_id = b.id
							AND b.academic_terms_id = {$this->db->escape($current_term_id)}
							AND b.students_idno = a.idno
							AND b.prospectus_id = d.id
							AND d.academic_programs_id = e.id
							AND c.id = f.privileges_availed_id
							AND f.fees_groups_id = 9
							
					   ORDER BY 
						  a.lname, a.fname";
			} else {
				$q1 = "SELECT
						a.lname,
						a.fname,
						a.mname,
						b.year_level,
						e.abbreviation,
						a.idno as students_idno,
						c.total_amount_availed as discount_amount,
						f.discount_percentage,
						c.remark,
						IF(c.posted='Y','Yes','No') AS posted
					FROM
						students as a,
						student_histories as b,
						privileges_availed as c,
						prospectus as d,
						academic_programs as e,
						privileges_availed_details as f
					WHERE
						c.scholarships_id = {$this->db->escape($privilge_id)}
						AND c.student_histories_id = b.id
						AND b.academic_terms_id = {$this->db->escape($current_term_id)}
						AND c.posted = {$this->db->escape($posted)}
						AND b.students_idno = a.idno
						AND b.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND c.id = f.privileges_availed_id
						AND f.fees_groups_id = 9
					
					ORDER BY
						a.lname, a.fname";	
			}					
			//print($q1); die();	
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		return $result;
	}	
	
	//Added 6/7/2013
	function getScholarshipInfo($scholarship_id){
	
		$result = null;
		
		$q = "SELECT 
					a.scholarship, a.scholarship_code, a.max_unit_grad, a.max_unit_undergrad
				FROM
					scholarships as a
				WHERE
					a.id = {$this->db->escape($scholarship_id)}";
	//	print_r($q);
	   // die();		
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		} 
		
		
		return $result;
			
	}				

	
	//Added: 9/10/2013
	
	function updateScholarship($scholarship_id, $scholarship_code, $scholarship, $max_grad, $max_undergrad){
			
		$q1 = "UPDATE scholarships
		SET
		 	scholarship_code = {$this->db->escape($scholarship_code)},
		 	scholarship = {$this->db->escape($scholarship)},
		 	max_unit_grad = {$this->db->escape($max_grad)},
		 	max_unit_undergrad = {$this->db->escape($max_undergrad)}
		WHERE
			id={$this->db->escape($scholarship_id)}";
			
	
		if ($this->db->query($q1)) {
		return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	function deletescholarship($privilege_id) {
	
		$q = "DELETE 
				FROM scholarships
		WHERE
		id = {$this->db->escape($privilege_id)} ";
			
		$query1 = $this->db->query($q);
		if ($query1) {
		return TRUE;
	} else {
			return FALSE;
		}
	}
	
	//Added: 9/14/2103
	
	function ListUnpostedPrivileges($history_id, $bed = NULL) {
		$result = null;
		if(!$bed){
		$q1 = "	SELECT a.id,
				       b.scholarship,
				       b.id as scholarship_id,
				 	   a.total_amount_availed,
					   c.discount_percentage,
					   c.fees_groups_id 
			    FROM privileges_availed as a
					LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
				scholarships as b 
				WHERE
					 a.student_histories_id = {$this->db->escape($history_id)}
				     AND a.scholarships_id = b.id 
				     AND c.fees_groups_id = 9
				     AND a.posted = 'N'";
		}else {
			$q1 = "	SELECT a.id,
			b.scholarship,
			b.id as scholarship_id,
			a.total_amount_availed,
			c.discount_percentage,
			c.fees_groups_id
			FROM privileges_availed as a
			LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
			scholarships as b
			WHERE
			a.basic_ed_histories_id = {$this->db->escape($history_id)}
			AND a.scholarships_id = b.id
			AND c.fees_groups_id = 9
			AND a.posted = 'N'";
		}
//	print($q1); die();	
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
	}
	
	//Added:9/14/2013
	
	function getMaxUnitsDiscount($scholarship_id){
	
		$result = null;
	
		$q = "SELECT
		     a.max_unit_grad, a.max_unit_undergrad
		FROM
			scholarships as a
		WHERE
			a.id = {$this->db->escape($scholarship_id)}
			
			";
		//print($q); die();
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
		$result = $query->row();
		}
	
	
		return $result;
			
		}
	
		
 //Added: 9/16/2013
 
		function ListPostedPrivileges($history_id) {
			$result = null;
		
			$q1 = "	SELECT a.id,
			b.scholarship,
			a.total_amount_availed,
			a.posted,
			c.discount_percentage,
			c.discount_amount
			FROM privileges_availed as a
			LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
			scholarships as b
			WHERE
			a.student_histories_id = {$this->db->escape($history_id)}
			AND a.scholarships_id = b.id
			AND c.fees_groups_id = 9
			AND a.posted = 'Y'";
			//print($q1); die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
	}
	
//Added: 9/17/2013
	function AddCashDiscount($data, $bed=NULL) {
	
		if(!$bed)
			$id = $this->AddStudentPrivilege($data);
		else 
			$id  = $this->AddStudentPrivilege($data, 1);
			$id2 = $this->AddPrivilegeDetails($id, $data);
		if($id && $id2){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
//Added: 9/18/2013

	function ListPastPrivileges($idnum, $bed = NULL){
		$result = null;
	if(!$bed){
		$q1 = "SELECT 
				a.id,
				f.scholarship, 
				a.total_amount_availed, 
				b.term, 
				d.end_year,
				g.discount_percentage
			 FROM student_histories as c 
					LEFT JOIN academic_terms as b ON c.academic_terms_id = b.id,
			 	privileges_availed as a LEFT JOIN privileges_availed_details as g ON a.id = g.privileges_availed_id,
			 	scholarships as f,
				academic_years as d
			WHERE
				c.students_idno = {$this->db->escape($idnum)} 
				AND a.student_histories_id = c.id 
				AND g.fees_groups_id = 9
				AND a.scholarships_id = f.id 
				AND b.academic_years_id = d.id
				AND a.posted = 'Y'";
	}else{
		$q1 = "SELECT
				a.id,
				f.scholarship,
				a.total_amount_availed,
				d.end_year,
				g.discount_percentage
			FROM basic_ed_histories as c,
				 privileges_availed as a LEFT JOIN privileges_availed_details as g ON a.id = g.privileges_availed_id,
					scholarships as f,
				academic_years as d
			WHERE
				c.students_idno = {$this->db->escape($idnum)}
				AND a.basic_ed_histories_id = c.id
				AND g.fees_groups_id = 9
				AND a.scholarships_id = f.id
				AND c.academic_years_id = d.id
				AND a.posted = 'Y'";
	}	
			
	//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
		$result = $query->result();
	}
	return $result;
	}
	
	//Added: 10/25/2013
	
	function ListUnpostedPrivilegesWdoutCash($history_id, $bed=NULL) {
		$result = null;
	   
	  if(!$bed){
		$q1 = "	SELECT a.id,
		b.scholarship,
		a.total_amount_availed,
		c.discount_percentage,
		c.discount_amount
		FROM privileges_availed as a
		LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
		scholarships as b
		WHERE
		a.student_histories_id = {$this->db->escape($history_id)}
		AND a.scholarships_id = b.id
		AND c.fees_groups_id = 9
		AND a.scholarships_id != 186
		AND a.posted = 'N'";
	  }else{
	  	$q1 = "	SELECT a.id,
	  	b.scholarship,
	  	a.total_amount_availed,
	  	c.discount_percentage,
	  	c.discount_amount
	  	FROM privileges_availed as a
	  	LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
	  	scholarships as b
	  	WHERE
	  	a.basic_ed_histories_id = {$this->db->escape($history_id)}
	  	AND a.scholarships_id = b.id
	  	AND c.fees_groups_id = 9
	  	AND a.scholarships_id != 186
	  	AND a.posted = 'N'";
	  }	
	//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
		$result = $query->result();
	}
	return $result;
	}
	
	function ListUnpostedCashDiscount($history_id, $bed = NULL) {
		$result = null;
		if(!$bed){
			$q1 = "	SELECT a.id,
			b.scholarship,
			a.total_amount_availed,
			c.discount_percentage
			FROM privileges_availed as a
			LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
			scholarships as b
			WHERE
			a.student_histories_id = {$this->db->escape($history_id)}
			AND a.scholarships_id = b.id
			AND c.fees_groups_id = 9
			AND a.scholarships_id = 186
			AND a.posted = 'N'";
		}else {
			$q1 = "	SELECT a.id,
			b.scholarship,
			a.total_amount_availed,
			c.discount_percentage
			FROM privileges_availed as a
			LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
			scholarships as b
			WHERE
			a.basic_ed_histories_id = {$this->db->escape($history_id)}
			AND a.scholarships_id = b.id
			AND c.fees_groups_id = 9
			AND a.scholarships_id = 186
			AND a.posted = 'N'";
		}
		//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
		$result = $query->result();
		}
		return $result;
	}
	
	function ListPrivilegesWdoutCash($history_id, $bed= NULL) {
		$result = null;
	  if(!$bed){
		$q1 = "	SELECT a.id,
					b.scholarship,
					a.total_amount_availed,
					c.discount_percentage,
					c.discount_amount
			FROM privileges_availed as a
				LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
				scholarships as b
			WHERE
				a.student_histories_id = {$this->db->escape($history_id)}
				AND a.scholarships_id = b.id
				AND c.fees_groups_id = 9
				AND a.scholarships_id != 186";
	  }else {
	  	$q1 = "	SELECT a.id,
	  	b.scholarship,
	  	a.total_amount_availed,
	  	c.discount_percentage,
	  	c.discount_amount
	  	FROM privileges_availed as a
	  	LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
	  	scholarships as b
	  	WHERE
	  	a.basic_ed_histories_id = {$this->db->escape($history_id)}
	  	AND a.scholarships_id = b.id
	  	AND c.fees_groups_id = 9
	  	AND a.scholarships_id != 186";
	  }
	  	
	  
		//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
		$result = $query->result();
		}
		return $result;
	}
	
	//Added: 3/8/2014 by Isah
	function LisAddedCashDiscount($history_id) {
		$result = null;
	
		$q1 = "	SELECT a.id,
		b.scholarship,
		a.total_amount_availed,
		c.discount_percentage
		FROM privileges_availed as a
		LEFT JOIN privileges_availed_details as c on a.id = c.privileges_availed_id,
		scholarships as b
		WHERE
		a.student_histories_id = {$this->db->escape($history_id)}
		AND a.scholarships_id = b.id
		AND c.fees_groups_id = 9
		AND a.scholarships_id = 186	";
		//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query && $query->num_rows() > 0){
		$result = $query->result();
		}
		return $result;
	}
	
	function getPostedCash($history_id, $bed=NULL){
	
		$result = null;
		if(!$bed){
			$q = "SELECT a.id,
					a.scholarships_id,
					a.total_amount_availed
				FROM privileges_availed as a
				WHERE
					a.student_histories_id = {$this->db->escape($history_id)}
					AND a.scholarships_id = 186	
					AND a.posted = 'Y'";
		}else{
			$q = "SELECT a.id,
			a.scholarships_id,
			a.total_amount_availed
			FROM privileges_availed as a
			WHERE
			a.basic_ed_histories_id = {$this->db->escape($history_id)}
			AND a.scholarships_id = 186
			AND a.posted = 'Y'";
		}
		//print_r($q);
		//die();
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
	
	
		return $result;
	}


	/*
	 * ADDED: 8/20/15 by genes
	*/
	function ListBasicEdStudentsAvailed($privilge_id, $acad_yr_id, $posted=NULL) {
		$result = null;
				
		if ($posted == 'All') {
			$q1 = "SELECT
					  a.lname, 
					  a.fname, 
					  a.mname, 
					  a.idno as students_idno,
					  CONCAT(d.level,'-',b.yr_level) AS stud_level, 
					  c.total_amount_availed as discount_amount, 
					  f.discount_percentage,
					  c.remark,
					  IF(c.posted='Y','Yes','No') AS posted
				   FROM
					  students as a, 
					  basic_ed_histories as b,
					  levels AS d, 
					  privileges_availed as c, 
					  privileges_availed_details as f
				   WHERE
						c.scholarships_id = {$this->db->escape($privilge_id)}
						AND c.basic_ed_histories_id = b.id
						AND b.academic_years_id = {$this->db->escape($acad_yr_id)}
						AND b.students_idno = a.idno
						AND d.id=b.levels_id
						AND c.id = f.privileges_availed_id						
				   ORDER BY 
					  a.lname, a.fname";
		} else {
			$q1 = "SELECT
					a.lname,
					a.fname,
					a.mname,
					CONCAT(d.level,'-',b.yr_level) AS stud_level, 
					a.idno as students_idno,
					c.total_amount_availed as discount_amount,
					f.discount_percentage,
					c.remark,
					IF(c.posted='Y','Yes','No') AS posted
				FROM
					students as a,
					basic_ed_histories as b,
					levels AS d, 
					privileges_availed as c,
					privileges_availed_details as f
				WHERE
					c.scholarships_id = {$this->db->escape($privilge_id)}
					AND c.basic_ed_histories_id = b.id
					AND b.academic_years_id = {$this->db->escape($acad_yr_id)}
					AND c.posted = {$this->db->escape($posted)}
					AND b.students_idno = a.idno
					AND d.id=b.levels_id
					AND c.id = f.privileges_availed_id
				
				ORDER BY
					a.lname, a.fname";	
		}					
		//print($q1); die();	
		$query = $this->db->query($q1);
			
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
	}	
		
	
}

//end of scholarship_model.php