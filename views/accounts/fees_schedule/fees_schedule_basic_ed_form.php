<h2 class="heading">Fees Schedule - Basic Education</h2>
<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<form method="POST" id="change_academic_year_form" name="MyForm" >
			<input type="hidden" name="step" value="2">
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<select name="academic_year_id" id="academic_year_id">
						<?php
							foreach($school_years AS $school_year) {
						?>
							<option value="<?php print($school_year->id); ?>" <?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->school_year . " (". $school_year->status.")"); ?> </option>	
						<?php 
							}
						?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Fees Category</div>
				<div class="controls">
					<select name="fees_group_id" id="fees_groups">
						<!--  <option value="">Select Fees Group</option> --> 											
<?php						
						foreach($fees_groups AS $fees_group) {
							echo "<option value=". $fees_group->id . ">" . $fees_group->fees_group ."</option>\n";	
						}
?>					
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Item</div>
				<div class="controls">
					<select name="fees_subgroups" id="fees_subgroups">						
						<!-- <option value="">Select Item</option> -->											
<?php 					
						if (isset($fees_subgroups)){
							foreach($fees_subgroups AS $fees_subgroup) {
								print("<option value=".$fees_subgroup->id." class=".$fees_subgroup->fees_groups_id.">" . $fees_subgroup->description ."</option>\n");
							}
						}
?>									
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Level</div>
				<div class="controls">		
					<select name="basic_levels" id="basic_levels">						
					<?php 					
						if (isset($basic_levels)){
							foreach($basic_levels AS $level) {
								print("<option value=".$level->id." >" . $level->level ."</option>\n");
							}
						}
					?>									
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Grade Level</div>
				<div class="grade_level" id="2" style="display: block; margin-left: 180px;">
					<input type="checkbox" name="k[1]" value="Kinder 1" checked> Kinder 1<br>
					<input type="checkbox" name="k[2]" value="Kinder 2" checked> Kinder 2<br>
				</div>
				<div class="grade_level" id="3" style="display: none; margin-left: 180px;">
					<input type="checkbox" name="gs[1]" value="Grade 1" checked> Grade 1<br>
					<input type="checkbox" name="gs[2]" value="Grade 2" checked> Grade 2<br>
					<input type="checkbox" name="gs[3]" value="Grade 3" checked> Grade 3<br>
					<input type="checkbox" name="gs[4]" value="Grade 4" checked> Grade 4<br>
					<input type="checkbox" name="gs[5]" value="Grade 5" checked> Grade 5<br>
					<input type="checkbox" name="gs[6]" value="Grade 6" checked> Grade 6<br>
				</div>
				<div class="grade_level" id="4" style="display: none; margin-left: 180px;">
					<input type="checkbox" name="hs_d[1]" value="1st Year" checked> 1st Year<br>
					<input type="checkbox" name="hs_d[2]" value="2nd Year" checked> 2nd Year<br>
					<input type="checkbox" name="hs_d[3]" value="3rd Year" checked> 3rd Year<br>
					<input type="checkbox" name="hs_d[4]" value="4th Year" checked> 4th Year<br>
				</div>
				<div class="grade_level" id="5" style="display: none; margin-left: 180px;">
					<input type="checkbox" name="hs_n[1]" value="1st Year" checked> 1st Year<br>
					<input type="checkbox" name="hs_n[2]" value="2nd Year" checked> 2nd Year<br>
					<input type="checkbox" name="hs_n[3]" value="3rd Year" checked> 3rd Year<br>
					<input type="checkbox" name="hs_n[4]" value="4th Year" checked> 4th Year<br>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Amount</div>
				<div class="controls">
					<input type="text" name="rate">									
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<input type="submit" value="Proceed to Review Form >>" class="btn btn-primary">									
				</div>
				
			</div>
			</form>
			</div>				
	</div>
</div>


<script language="JavaScript" type="text/javascript" src=<?php print(base_url('assets/js/chained_select/')."/jquery.min.js"); ?> ></script>
<script language="JavaScript" type="text/javascript" src=<?php print(base_url('assets/js/chained_select/')."/jquery.chained.min.js"); ?> charset="utf-8"></script>

<script type="text/javascript" charset="utf-8">
          $(function(){
              $("#fees_subgroups").chained("#fees_groups"); 
          });
</script>


<script>
$(function() {
    $('#basic_levels').change(function(){
        $('.grade_level').hide();
        $('#' + $(this).val()).show();
    });
});

</script>


<script>
	$(document).ready(function () {
	    $('#selectall_yr').click(function () {
	        $('.selectedId_yr').prop('checked', this.checked);
	    });
	
	    $('.selectedId_yr').change(function () {
	        var check = ($('.selectedId_yr').filter(":checked").length == $('.selectedId_yr').length);
	        $('#selectall_yr').prop("checked", check);
	    });
	});
</script>


<script type="text/javascript">
	function showStuff(id) {
		document.getElementById(id).style.display='block';
	}

	function hideStuff(id) {
		document.getElementById(id).style.display='none';
	}

	function validateForm()
	{
		var x=document.forms["MyForm"]["rate"].value;
		var x2=document.forms["MyForm"]["fees_group_id"].value; 
		
		
		if (x2==null || x2=="")  {
			  alert("Please select Fees Category!");
			  document.forms["MyForm"]["fees_group_id"].focus();
			  return false;
		}

		
		if (x==null || x=="")  {
		  alert("Please input rate!");
		  document.forms["MyForm"]["rate"].focus();
		  return false;
		}
	}
	
</script>


<script>
	$(document).ready(function(){
		$('#academic_year_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 1,
			}).appendTo('#change_academic_year_form');
			$('#change_academic_year_form').submit();
		});
	});
</script>
