<?php 

?><table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="10%">Date Created</th>
			<th width="60%">Remarks</th>
  		</tr>
	</thead>
	<tbody> 
<?php if (is_array($all_remarks) && count($all_remarks) > 0 ): ?>
<?php foreach ($all_remarks as $remark): ?>
		<tr>
			<td><?php echo $remark->remark_date; ?></td>
			<td><?php echo $remark->remark; ?></td>
		</tr>

<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="6">No remarks yet</td>
		</tr>
<?php endif; ?>
	</tbody>
</table>

