<?php

	class Shs_faculty_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
	
		function List_MySHS_ClassSchedules($empno=NULL, $academic_terms_id=NULL) {
			$result = null;
			
			$q1 = "SELECT 
						d.block_sections_id,
						a.courses_id,
						e.abbreviation,
						d.section_name,
						f.course_code,
						f.descriptive_title,
						f.credit_units,
						c.yr_level AS grade_level,
						c.id,
						IF (d.rooms_id,h.room_no,'') AS room_no,
						(SELECT 
								COUNT(DISTINCT(x.id)) 
							FROM 
								student_histories AS x,
								enrollments AS y
							WHERE 
								x.id = y.student_history_id
								AND x.block_sections_id = c.id 
						) AS num_enrollees,
						a.id AS course_offerings_id,
						CONCAT(g.lname,', ',g.fname) AS block_adviser,
						DATE_FORMAT(i.midterm_date,'%b/%e/%Y @%h:%i%p') AS midterm_date,
						DATE_FORMAT(i.finals_date,'%b/%e/%Y @%h:%i%p') AS finals_date,
						DATE_FORMAT(j.midterm_confirm_date,'%b/%e/%Y @%h:%i%p') AS midterm_confirm_date,
						DATE_FORMAT(j.finals_confirm_date,'%b/%e/%Y @%h:%i%p') AS finals_confirm_date
					FROM 
						course_offerings AS a
							LEFT JOIN grade_submission_dates AS i ON i.course_offerings_id = a.id
							LEFT JOIN shs_course_offerings_confirmation AS j ON a.id = j.course_offerings_id,
						block_course_offerings AS b,
						block_sections AS c, 
						shs_block_sections AS d 
							LEFT JOIN employees AS g ON d.block_adviser = g.empno
							LEFT JOIN rooms AS h ON h.id = d.rooms_id,
						academic_programs AS e,
						courses AS f
					WHERE 
						a.id = b.course_offerings_id 
						AND b.block_sections_id = c.id
						AND c.id = d.block_sections_id 
						AND c.academic_programs_id = e.id
						AND a.courses_id = f.id
						AND a.employees_empno = {$this->db->escape($empno)} ";
			
			if ($academic_terms_id) {	
				$q1 .= " AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
			}	
			
			$q1 .= " GROUP BY 
						f.id,
						c.id 
					ORDER BY 
						c.yr_level,
						e.abbreviation,
						d.section_name	";
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();

				$cnt=0;
				if ($result) {
					foreach($result AS $res) {
						
						$myresult = $this->ListTimeSched($res->block_sections_id, $res->courses_id);
						
						if ($myresult) {
							$result[$cnt]->time_sched = $myresult;
						}									
						$cnt++;
					}
				}	
			} 
			
			return $result;
			
		}
		

		private function ListTimeSched($block_sections_id, $courses_id) {
			$result = NULL;	

			$sql = "SELECT 
							CASE d.day_name
								WHEN 'Mon' THEN 1
								WHEN 'Tue' THEN 2
								WHEN 'Wed' THEN 3
								WHEN 'Thu' THEN 4
								WHEN 'Fri' THEN 5
							END AS day_id,
							CONCAT('<div style=\'width:150px; float:left;\'>',TIME_FORMAT(d.from_time,'%h:%i%p'),'-',TIME_FORMAT(d.to_time,'%h:%i%p'),' ',d.day_name,'</div>[',c.room_no,']') AS course_sched
						FROM 
							rooms AS c,
							room_occupancy AS d,
							room_occupancy_course_offerings_slots AS e,
							block_course_offerings AS f,
							course_offerings AS g,
							course_offerings_slots AS h
						WHERE 
							c.id = d.rooms_id 
							AND d.id = e.room_occupancy_id
							AND f.course_offerings_id = g.id 
							AND g.id = h.course_offerings_id
							AND h.id = e.course_offerings_slots_id
							AND g.courses_id = {$this->db->escape($courses_id)} 
							AND f.block_sections_id = {$this->db->escape($block_sections_id)} 
						ORDER BY 
							day_id,
							d.from_time	";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}

		
		function ListFaculty_Inclusive_Terms($empno=NULL) {
			$result = null;
			
			$q1 = "SELECT 
						d.id,
						CASE d.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
						END AS term, 
						CONCAT(e.end_year-1,'-',e.end_year) AS sy
					FROM 
						course_offerings AS a,
						block_course_offerings AS b,
						shs_block_sections AS c,
						academic_terms AS d,
						academic_years AS e
					WHERE 
						a.id = b.course_offerings_id 
						AND b.block_sections_id = c.block_sections_id
						AND a.academic_terms_id = d.id 
						AND d.academic_years_id = e.id 
						AND a.employees_empno = {$this->db->escape($empno)} 
					GROUP BY 
						d.id 
					ORDER BY 
						e.end_year DESC,
						d.term ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

	
		function List_MyStudents($section_id=NULL, $courses_id=NULL, $order_by='by_name') {
			$result = null;
			
			$q1 = "SELECT 
							a.idno,
							a.lname,
							a.fname,
							a.mname,
							a.gender,
							b.year_level,
							CONCAT(a.lname,', ',a.fname,' ',UPPER(LEFT(a.mname,1)),'.') AS name,
							f.id AS enrollments_id,
							f.midterm_grade,
							f.finals_grade,
							d.course_offerings_id
						FROM 
							students AS a,
							student_histories AS b,
							block_sections AS c,
							block_course_offerings AS d,
							course_offerings AS e,
							enrollments AS f
						WHERE 
							a.idno = b.students_idno
							AND b.block_sections_id = c.id 
							AND c.id = d.block_sections_id
							AND d.course_offerings_id = e.id 
							AND e.id = f.course_offerings_id 
							AND b.id = f.student_history_id 
							AND b.block_sections_id = {$this->db->escape($section_id)} 
							AND e.courses_id = {$this->db->escape($courses_id)} ";
			
			switch ($order_by) {
				
				case 'by_name':
					$q1 .= " ORDER BY 
								name ";
					break;
					
				case 'by_gender':
					$q1 .= " ORDER BY 
								gender,
								name ";
					break;
			
			}
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function Adviser_Confirm($course_offerings_id=NULL) {
			$result = null;
			
			$q1 = "SELECT 
						a.course_offerings_id,
						DATE_FORMAT(a.midterm_confirm_date,'%b/%e/%Y @%h:%i%p') AS midterm_confirm_date,
						DATE_FORMAT(a.finals_confirm_date,'%b/%e/%Y @%h:%i%p') AS finals_confirm_date,
						b.midterm_date AS midterm_submitted_date,
						b.finals_date AS finals_submitted_date
					FROM
						shs_course_offerings_confirmation AS a 
							LEFT JOIN grade_submission_dates AS b ON a.course_offerings_id = b.course_offerings_id
					WHERE 
						a.course_offerings_id = {$this->db->escape($course_offerings_id)} ";
			
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}


		function CheckIfMyStudent($empno=NULL, $students_idno=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.id
						FROM 
							course_offerings AS a,
							enrollments AS b,
							student_histories AS c
						WHERE 
							a.id = b.course_offerings_id 
							AND b.student_history_id = c.id
							AND a.employees_empno = {$this->db->escape($empno)} 
							AND c.students_idno = {$this->db->escape($students_idno)}
						LIMIT 1 ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
		
		}


		function ListAcad_Terms_WithTeachers() {
			$result = null;
			
			$q1 = "SELECT 
						d.id,
						CASE d.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
						END AS term, 
						CONCAT(e.end_year-1,'-',e.end_year) AS sy,
						CASE d.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
						END AS student_term						
					FROM 
						course_offerings AS a,
						block_course_offerings AS b,
						shs_block_sections AS c,
						academic_terms AS d,
						academic_years AS e,
						employees AS f
					WHERE 
						a.id = b.course_offerings_id 
						AND b.block_sections_id = c.block_sections_id
						AND a.academic_terms_id = d.id 
						AND d.academic_years_id = e.id 
						AND a.employees_empno = f.empno
					GROUP BY 
						d.id 
					ORDER BY 
						e.end_year DESC,
						d.term ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function ListTeachers($academic_terms_id=NULL, $with_grade_submission=FALSE) {

			$result = null;
			
			$q1 = "SELECT 
						a.employees_empno,
						CONCAT(d.lname,', ',d.fname) AS name
					FROM 
						course_offerings AS a,
						block_course_offerings AS b,
						shs_block_sections AS c,
						employees AS d
					WHERE 
						a.id = b.course_offerings_id 
						AND b.block_sections_id = c.block_sections_id
						AND d.empno = a.employees_empno 
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
					GROUP BY 
						a.employees_empno
					ORDER BY 
						d.lname,
						d.fname, 
						d.mname ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){

				$result = $query->result();
				
				if ($with_grade_submission) {
					
					$cnt=0;
					if ($result) {
						foreach($result AS $res) {
							$myresult = $this->List_MySHS_ClassSchedules($res->employees_empno, $academic_terms_id);
							
							if ($myresult) {
								$result[$cnt]->grades = $myresult;
							}									
							$cnt++;
						}
					}
					
				}
				
			} 
			
			return $result;
			
		}
		
	}
	
?>