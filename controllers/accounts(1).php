<?php

class Accounts extends MY_Controller {

	public function __construct(){

		parent::__construct();

    // In lieu of the JZEBRA Java printing plugin which support was cutoff
	// by almost all browsers, this directory is created automatically when
	// Teller Role logins to house the new raw printing utility developed
	// inhouse by HNU IRMC.  The new utility, TEXTPRINTER, runs in the background
	// acting as a windows service that waits for the file receipt.txt to be
	// saved in this auto-created folder.  It then prints the text file and
	// erases it.  -Toyet 10.16.2017

		if ($this->session->userdata('role')=='accounts'){
			$this->navbar_data['menu'] =
				array(
					'Students'	=> array(
									'Students'=>'student',
									'College SMS Mass Sending'=>'send_mass_sms/college',
									'Basic Ed Mass Sending'=>'send_mass_sms/basic_ed',
									),
					'Accounts'  => array(
									'Accounts'=>'payors',
									'New Accounts'=>'new_payors',),
					'Fees Schedule' => array(
											'College' => array (
															'Fees Schedule'=>'fees_schedule_college',
															'Laboratory Fees'=>'laboratory_fees',
															'Other Tuition Fees'=>'other_tuition_fees',
															'Other Additional School Fees'=>'affiliation_fees',
															'Assign Affiliation to Course'=>'assign_affiliation_course',
														),
											'Basic Education'=>'fees_schedule_basic_ed',
											'Senior High School'=>array('Fees Schedule'=>'fees_schedule_shs',
																			'Laboratory Fees'=>'lab_fees_shs',
																		)
										),
					'Reports' => array(
										'Privileges' 		=> array(
												'College'			=>	'privileges',
												'Basic Education'	=>	'privileges_basic_ed'
												),
										),
					'Prospectuses' => 'view_prospectus',
					'Privileges'  => array(
									'Student List'=>'privileges',
									'Create New Privilege' => 'new_privilege',
									'View List of Privileges' => 'list_privileges',),
					'Mass Printing'=> array(
									'Basic ED Statements'=>'mass_printing/basic_ed_statements',
									'Basic ED Clearance'=>'mass_printing/basic_ed_clearances'),
					'Fees Management'=> array(
									'Fees Category'=>'fees_category',
									'Schedule by Academic Program'=>'schedule_by_acad_program',
									'Schedule for Basic Education'=>'schedule_for_basic_ed',
									'Schedule for Senior High School'=>'schedule_for_shs',
								),

					);
		}
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		$this->load->helper('student_helper');


		$this->load->model('teller/assessment_model');
		$this->load->model('hnumis/Enrollments_model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('hnumis/Programs_model');
		$this->load->model('hnumis/College_model');
		$this->load->model('hnumis/AcademicYears_model');
		$this->load->model('Academic_terms_model');
		$this->load->model('hnumis/Offerings_Model');


	}

	public function student(){

			$this->content_lib->set_title('Accounts | Student | ' . $this->config->item('application_title'));
			$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
			$query = $this->input->post('q');
			$idnum = $this->uri->segment(3);

			if ( ! empty($query)){

				//A search query occurs... so lets query the student_common_model
				$page = $this->input->post('page') ? $this->input->post('page') : 1;
				$this->load->model('student_common_model');
				$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
				$total = $this->student_common_model->total_search_results($query);

				$this->load->library('pagination_lib');

				if ($results !== FALSE) {
					$pagination = $this->pagination_lib->pagination('', $total, $page);

					$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
					$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
							? $total
							: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
					);

					//A result or several results were found
					if (count($results) > 1){
						//when several results are found... lets show the search result page...
						$res=array();
						foreach ($results as $result){
							$port = substr($result->idno, 0, 3);
							if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
								$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
							else {
								if ($result->gender=='F')
									$image = base_url($this->config->item('no_image_placeholder_female')); else
									$image = base_url($this->config->item('no_image_placeholder_male'));
							}
							$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
						}
						$data = array(
								'start'		=>$start,
								'end'		=>$end,
								'total'		=>$total,
								'pagination'=>$pagination,
								'results'	=>$res,
								'query'		=>$query
						);
						$this->content_lib->enqueue_body_content ('common/search_result', $data);
					} else {
						//var_dump ($results);
						//Only one result is seen... lets show his profile instead.
						//redirect to the user's profile...
						redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
					}
				} else {
					//A result is NOT found...
					$this->content_lib->set_message("No result found for that query", 'alert-error');
				}
			}

			if (is_numeric($idnum)){
				$this->load->model('hnumis/student_model');

				if(!is_college_student($idnum)){
					if ($this->student_model->student_is_basic_ed($idnum)) {
						redirect(site_url("{$this->role}/basic_ed/{$idnum}"));
					}
				}

				//after the search... when results are found....
				$this->load->model('student_common_model');
				$this->load->model('Academic_terms_model');
				$this->load->model('academic_terms_model');
				$this->load->model('financials/Payments_Model');
				$this->load->model('hnumis/Courses_Model');
				$this->load->model('hnumis/AcademicYears_model');
				$this->load->model('hnumis/BlockSection_Model');
				$this->load->model('hnumis/Programs_Model');
				$this->load->model('financials/Scholarship_Model');
				$this->load->model('accounts/Accounts_model');
				$this->load->model('teller/teller_model');
				$this->load->model('hnumis/enrollments_model');
				$this->load->model('teller/assessment_model');
				$this->load->model('hnumis/Withdrawal_Model');
				$this->load->model('hnumis/AcademicYears_Model');
				$this->load->model('academic_terms_model');
				$this->load->model('accounts/fees_schedule_model');
				$this->load->model('hnumis/Student_Model');
				$this->load->model('hnumis/Offerings_Model');
				$this->load->library('shs/shs_student_lib');
				$this->load->model('hnumis/shs/shs_student_model');

				$result = $this->student_common_model->my_information($idnum);
				$result2 = $this->teller_model->get_student($idnum);
				//$this->content_lib->enqueue_footer_script('printelement'); // print element plugin...
				$this->content_lib->set_title ('Accounts | ' . $idnum);

				$current_term = $this->Academic_terms_model->current_academic_term();
				$tab = 'assessment';

				switch ($action = $this->input->post('action')){

					case 'form_to_assign_privileges':

						if ($this->common->nonce_is_valid($this->input->post('nonce'))){
								$current_term = $this->Academic_terms_model->current_academic_term();
								$history = $this->student_model->getEnrollStatus($idnum, $current_term->id);
								if($history){
									$academic_program_group_id = $this->student_model->getAcadGroup($history->id);
									$academic_program_info = $this->student_model->getAcadProgram($history->id);
								}

								$paying_units_basic = $this->student_model->getPayingUnitsBasic($idnum, $current_term->id);
								$paying_units_others = $this->student_model->getPayingUnitsOthers($idnum, $current_term->id);
								$tuition_percentage = sanitize_text_field($this->input->post('TuitionD'));
								(float)$tuition_percentage;
								$re_enrolled_info_basic = $this->student_model->getRe_enrolledUnitsBasic($history->id, $current_term->id);
								$re_enrolled_info_others = $this->student_model->getRe_enrolledUnitsOthers($history->id, $current_term->id);
								//print_r($re_enrolled_info_basic); print("<br>"); print_r($re_enrolled_info_others); die();
								$tuition_fee_basic = $this->student_model->getMyTuitionFee_Basic($history->id, $academic_program_group_id->
													acad_program_groups_id, $current_term->id );
								$tuition_fee_others = $this->student_model->getMyTuitionFee_Others($history->id, $current_term->id);
								$withCWTS = $this->student_model->checkIfWithCWTS($history->id, $current_term->id);
								//log_message("INFO", print_r($withCWTS,true)); // Toyet 7.3.2018

								//In case academic_terms_id >= 469, check if rate is 0 and assign
								//the $tuition_fee_basic_rate for the CWTS courses -Toyet 7.3.2018
								$tuition_fee_basic_rate = $this->student_model->getTuitionRateBasic($academic_program_group_id->acad_program_groups_id, $current_term->id, $result->year_level);
								if($withCWTS){
									//log_message("INFO", print_r($tuition_fee_basic_rate,true)); // Toyet 7.3.2018
									foreach ($withCWTS as $key => $value){
										//log_message("INFO", print_r($key,true)); // Toyet 7.3.2018
										//log_message("INFO", print_r($value,true)); // Toyet 7.3.2018
										if($value==0) $withCWTS->rate = $tuition_fee_basic_rate->rate;
									}
									//log_message("INFO", print_r($withCWTS,true)); // Toyet 7.3.2018
								}


								$data1['student_history_id'] = $history->id;
								$data1['privilege_id'] = sanitize_text_field($this->input->post('privilege_id'));
								//print($data1['privilege_id']); die();

								$privilege = $this->student_model->Check_if_with_Privilege($history->id);



						//calculate tuition fee discount
						//$data2 - the data are used to insert into privileges_availed_details table
								$data2['discount_percentage'] = $tuition_percentage;
								if($tuition_fee_basic){
									if($tuition_fee_basic->tuition_fee != 0){
										$tuition_fee_basic = $tuition_fee_basic->tuition_fee;
									}
								}else {
									$tuition_fee_basic = 0;
								}

								if($tuition_fee_others){
									if($tuition_fee_others->tuition_fee == 0){
										$tuition_fee_others = 0;
									}else {
										if($withCWTS){
											//print($tuition_fee_others->tuition_fee); die();
											$tuition_fee_others = $tuition_fee_others->tuition_fee - ($withCWTS->rate * $withCWTS->paying_units);
										}else{
											$tuition_fee_others = $tuition_fee_others->tuition_fee;
										}
									}
								}else {
									$tuition_fee_others = 0;
								}

								if($paying_units_basic->total_paying_units_enrolled == 0){
									$paying_units_basic = 0;
								}else {
									$paying_units_basic = $paying_units_basic->total_paying_units_enrolled;

								}

								if($paying_units_others->total_paying_units_enrolled == 0){
									$paying_units_others = 0;
								}else {
									$paying_units_others = $paying_units_others->total_paying_units_enrolled;
									if($withCWTS){
										$paying_units_others = $paying_units_others - $withCWTS->paying_units;
									}

								}

								$max_units_info = $this->Scholarship_Model->getMaxUnitsDiscount($this->input->post('privilege_id'));

								if($re_enrolled_info_basic){
									$privilege_units_basic =  $paying_units_basic - $re_enrolled_info_basic->total_paying_units_enrolled;
								}else{
									$privilege_units_basic =  $paying_units_basic;
								}

								if($re_enrolled_info_others){
									if($paying_units_others > $re_enrolled_info_others->total_paying_units_enrolled){
										$privilege_units_others =  $paying_units_others - $re_enrolled_info_others->total_paying_units_enrolled;
									}else{
										$privilege_units_others = 0;
									}
								}else{
									$privilege_units_others =  $paying_units_others;
								}

								//calculations for withdrawn subjects
								$student_info = $this->Student_Model->getStudentInfo($history->id);
								$cur_aca_yr = $this->AcademicYears_Model->current_academic_year();
								$tuition_basic_rate = $this->Student_Model->getTuitionRateBasic($student_info->acad_program_groups_id, $student_info->academic_terms_id, $student_info->year_level);
								$withWithdrawals = $this->student_model->checkIfWithWithdrawals($history->id);
								$lab_fee_withdrawn = 0;
								if($withWithdrawals){
									$units_basic_withdrawn = 0;
									$units_others_withdrawn = 0;
									foreach($withWithdrawals as $withdraw){
										$course_info = $this->Courses_Model->getCourseInfo($withdraw->id);
										$lab_amount = $this->Accounts_model->get_lab_fee($course_info->id, $cur_aca_yr->id);

										if($lab_amount){
											$lab_fee_withdrawn += $lab_amount->rate;
										}
										$tuition_others_rate = $this->Student_Model->getTuitionOthersRate($withdraw->id, $student_info->academic_terms_id, $student_info->year_level);
										if($tuition_others_rate){
											$units_others_withdrawn += $course_info->paying_units;
										}else{
											$units_basic_withdrawn += $course_info->paying_units;
										}
									}
									$privilege_units_basic = $privilege_units_basic - $units_basic_withdrawn;
									$privilege_units_others = $privilege_units_others - $units_others_withdrawn;
								}
							//	print_r($lab_fee_withdrawn); die();

								$tuition_discount_info = $this->tuitionDiscount($tuition_fee_basic, $tuition_fee_others,
									                                $paying_units_basic, $paying_units_others,
																	$history->year_level, $max_units_info, $academic_program_info,
																	$privilege_units_basic, $privilege_units_others, $data1['privilege_id']);

								//print_r($privilege_units_others); die();
								$data2['discount_percentage'] = ($data2['discount_percentage']/100);
								$data2['discount_amount'] = $tuition_discount_info[0] * $data2['discount_percentage'] ;
								$data2['fees_groups_id'] = 9;

								$data1['academic_programs_id'] = $result->academic_programs_id;
								$data1['year_level'] = $result->year_level;

								//print_r($data2);die();

								//inserts tuition discount
								$privilege_availed_id = $this->Scholarship_Model->AddStudentPrivilege($data1);
								//@FIXME: this should be done using transaction

								if ($privilege_availed_id){
									$priv_detail_id = $this->Scholarship_Model->AddPrivilegeDetails($privilege_availed_id, $data2);
								}else{
									$priv_detail_id = false;
								}




								//calculate laboratory fee discount
								$labfees = 0;
								$data2['discount_percentage'] = 1;
								$data2['discount_amount'] = 0;
								if(isset($_POST['5'])){
									$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id);

									//print_r($dcourses); die();
									$cur_aca_yr = $this->AcademicYears_model->current_academic_year();
									if(!empty($dcourses)){
										$lab_courses = array();
										foreach ($dcourses as $course) {
											if ($course->type_description == 'Lab' AND $course->enrolled_status == 'current') {
													$lab_amount = $this->Accounts_model->get_lab_fee($course->id, $cur_aca_yr->id);
													$lab_courses[] = array(
														'name'=>$course->course_code,
														'amount'=> $lab_amount->rate,
													);
											}
										}

									}

									//print_r($lab_courses); die();
										$total_lab_fees = 0;
										if($lab_courses){
											foreach ($lab_courses as $lab_course) {
												$total_lab_fees += (float)$lab_course['amount'];
											}
										}
									  $total_lab_fees = $total_lab_fees - $lab_fee_withdrawn;
									 // print($total_lab_fees); die();
									  $cur_aca_yr = $this->AcademicYears_model->current_academic_year();
									  if($total_lab_fees) {
										 $data2['discount_amount'] = $total_lab_fees;
										 $data2['fees_groups_id'] = 5;

										//inserts laboratory discount, if there is any
										//print_r($data2);die();
										 $priv_detail_id = $this->Scholarship_Model->AddPrivilegeDetails($privilege_availed_id, $data2);
									  } else {
										  $priv_detail_id = NULL;
									  }
								}


								//inserts other privileges
								$fees_groups = $this->Accounts_model->ListOtherFeesGroups();
								foreach($fees_groups as $fees_group){
									$fees = 0;
									$data2['discount_amount'] = 0;
									if(isset($_POST[$fees_group->id])) {
									   $fees = $this->Accounts_model->getFees($history->id, $fees_group->id);
									   if($fees->total_fee) {
											 $data2['discount_amount'] = $fees->total_fee;
											 $data2['fees_groups_id'] = $fees_group->id;
											 //inserts other privileges discount, if there is any
											 $priv_detail_id = $this->Scholarship_Model->AddPrivilegeDetails($privilege_availed_id, $data2);

									   } else {
										 	 $priv_detail_id = NULL;
				 					   }
								    }
							    }

								if($privilege_availed_id){
									$this->content_lib->set_message("Privilege Successfully Added!", "alert-success");
								} else {
									$this->content_lib->set_message("Error found while adding a privilege!", "alert-error");
								}

							} else {
								$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
								$this->index();
						}

							break;



					case 'delete_unposted_privilege':

												if ($this->common->nonce_is_valid($this->input->post('nonce'))){

													$status = $this->Scholarship_Model->DeletePrivilegeAvailed($this->input->post('privilege_id'));
													if($status){
														$this->content_lib->set_message("Privilege Successfully Deleted!", "alert-success");
													}else {
														$this->content_lib->set_message("Error found while deleting a privilege!", "alert-error");
													}
												} else {
													$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
													$this->index();
												}

												break;
					case 'form_to_add_cash_discount':

													if ($this->common->nonce_is_valid($this->input->post('nonce'))){
														//$tuition_fee = $this->input->post('tuition_fee');
														$tuition_discounts = $this->input->post('tuition_for_cash_discount');

														if($tuition_discounts == 0){
															$tuition_discounts = $this->input->post('tuition_fee');
														}
														//print($tuition_discounts); die();

														$data['discount_percentage'] = $this->input->post('CashDisc')/100;
														$data['discount_amount'] = $tuition_discounts * $data['discount_percentage'];
														$data['student_history_id']= $this->input->post('student_histories_id');
														$data['academic_programs_id']= $this->input->post('academic_programs_id');
														$data['year_level']= $this->input->post('year_level');
														$data['privilege_id'] = 186;
														$data['fees_groups_id'] = 9;

														//inserts the cash discount to both privileges_availed and privileges_availed_details tables
														$status = $this->Scholarship_Model->AddCashDiscount($data);
														//print($status); die();

														if($status){
															$this->content_lib->set_message("Cash Discount Successfully Added!", "alert-success");
														}else {
															$this->content_lib->set_message("Error found while processing cash discount!", "alert-error");
														}
													} else {
														$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
														$this->index();
													}

													break;



					case 'view_past_privilege_detail':
												if ($this->common->nonce_is_valid($this->input->post('nonce'))){
													$privilege_id = $this->input->post('priv_id');
													$privilege_info = $this->Scholarship_Model->getPrivilegeAvailedInfo($privilege_id);

													$privilege_details_info = $this->Scholarship_Model->ListPrivilegeAvailedDetails($privilege_id);

													$this->list_privileges_availed($privilege_info, $privilege_details_info,2);

												} else {
													$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
													$this->index();
												}

											break;

					case 'privilege_enroll' :
											if ($this->common->nonce_is_valid($this->input->post('nonce'))){

												$this->load->model('hnumis/Student_Model');
												$this->load->model('hnumis/enrollments_model');

												//$term = $this->Academic_terms_model->getCurrentAcademicTerm($this->enrollments_model->academic_term_for_enrollment_date());//Note(Justin): Add academic term if on enrollment date...
												$term = $this->Academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());

												$history = $this->student_model->getStudentHistory($result->student_histories_id);

												$data['idno'] = $result->idno;
												$data['academic_terms_id'] = $term->id;
												$data['prospectus_id'] = $result->prospectus_id;
												$data['block_sections_id'] = $history->block_sections_id;
												$data['yr_level'] = $result->year_level;
												$data['max_yr_level'] = $result->max_yr_level;
												$data['student_histories_id'] = $history->id;

												$student = $this->Student_Model->AssessYearLevel($data);

												//needed data for adding student history
												$data['year_level'] = $student['year_level'];
												$data['students_idno'] = $result->idno;
												//print_r($data); die();
												if ($history->academic_terms_id != $term->id) { //for old students who have already past records in
																								//student_histories table
													$data['inserted_by'] = $this->session->userdata('empno');
													$this->student_model->AddHistory_ByPrivilege($data);
												} else { //for new students created by dric
													$data['id'] = $result->student_histories_id;
													$this->student_model->AllowStudenToEnroll($data);
													//FIXME: this will still display "Successfully enrolled student via privilege" even if update fails
												}
												$this->content_lib->set_message('Successfully enrolled student via privilege!', 'alert-success');

												/*if ($return = $this->teller_model->privilege_enroll_student($idnum))
													$this->content_lib->set_message('Successfully enrolled student via privilege.', 'alert-success'); else
													$this->content_lib->set_message('Error in enrolling student.', 'alert-error');*/
											} else {
												$this->content_lib->set_message('You are not allowed to resubmit this form.', 'alert-error');
											}

											break;

					case 'edit_privileges':

								$tuition_discount_info = $this->tuitionDiscount($this->input->post('tuition_fee'),$this->input->post('paying_units')
														,$this->input->post('history_id'));
								$data['term'] = $this->Academic_terms_model->getCurrentAcademicTerm();
								$data['privilege'] = $this->Scholarship_Model->getScholarship($this->input->post('privilege_availed_id'));

								$data['paying_units'] = $tuition_discount_info[1];
								$data['tuition_fee'] = $this->input->post('tuition_fee');
								$data['history_id'] = $this->input->post('history_id');
								$tuition_info = $this->Scholarship_Model->getTuitionPrivilege($this->input->post('privilege_availed_id'));
								$data['privilege_items'] = $this->Scholarship_Model->ListPrivileges();
								$data['tuition_percentage'] = $tuition_info->discount_percentage;
								$data['amount'] = $tuition_info->discount_amount;
								$data['other_privileges'] = $this->Scholarship_Model->ListPrivilegeAvailedDetails($this->input->post(
																											'privilege_availed_id'));
								$this->content_lib->enqueue_body_content('accounts/form_to_edit_privileges', $data);
								$this->content_lib->content();
								break;

					case 'cancel_privilege':

								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
								   $privilege_detail_info = $this->Scholarship_Model->getPrivilegesDetailInfo($this->input->post('privilege_details_id'));
								   $disc_amount = $privilege_detail_info->discount_amount;
								   $privilege_availed_id = $privilege_detail_info->privileges_availed_id;
									$this->Scholarship_Model->DeletePrivilegeDetails($this->input->post('privilege_details_id'),$disc_amount,
																					$privilege_availed_id);
									$this->content_lib->set_message('Privilege detail removed!', 'alert-success');

								} else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
									$this->index();
								}

								break;

					case 'delete_privilege_availed':

								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
									$stat = $this->Scholarship_Model->DeletePrivilegeAvailed($this->input->post('privilege_availed_id'));
									if($stat){
									  $this->content_lib->set_message('Privilege removed!', 'alert-success');
									}

								} else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
									$this->index();
								}

								break;

					case 'change_term':

								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;


					case 'generate_student_certificate'	:
								$this->load->model('hnumis/Reports_Model');
								$this->load->library('tab_lib');

								if ($this->input->post('student_histories_id')) {
									$student_histories_id = $this->input->post('student_histories_id');
									$data['name'] = utf8_decode($result->lname.", ".$result->fname." ".$result->mname);
									$data['course_year'] = $this->input->post('course_year');
									$data['term'] = $this->AcademicYears_model->getAcademicTerms($this->input->post('academic_terms_id'));

									$data['courses'] = $this->student_model->ListMyTuitionFees_Basic($student_histories_id);
									if ($data['courses']) {
										$data['tuition_rate'] = $data['courses'][0]->rate;
									} else {
										$data['tuition_rate'] = 0;
									}

									$data['matriculation'] = $this->student_model->getMyMatriculationFee($student_histories_id);
									$data['miscs'] = $this->student_model->ListMyMiscellaneous_Fees($student_histories_id);
									$data['other_fees'] = $this->student_model->ListMyOther_Fees($student_histories_id);
									$data['addtl_other_fees'] = $this->student_model->ListMyAddtlOther_Fees($student_histories_id);
									$data['lab_fees'] = $this->student_model->ListMyLaboratory_Fees($student_histories_id);

									/*
									 * ADDED: 6/30/15 by genes
									*/
									$data['learning_resources'] = $this->student_model->getLearningResources_Fees($student_histories_id);
									$data['student_support'] = $this->student_model->getStudentSupport_Fees($student_histories_id);
									$data['affiliated_fees'] = $this->fees_schedule_model->ListStudentAffiliated_Fees($student_histories_id);

									$data['accounts_clerk'] = strtoupper($this->session->userdata('fname').' '.substr($this->session->userdata('mname'),0,1).'. '.$this->session->userdata('lname'));
									if ($this->input->post('reason') == 'other') {
										$data['reason'] = $this->input->post('other_reason');
									} else {
										$data['reason'] = "whatever legal purpose it may serve";
									}

									$this->Reports_Model->generate_student_certificate_pdf($data);
									return;

								}  else {
									$tab = 'certificate';
								}

								break;

					case 'generate_class_schedule':

								$this->load->model("hnumis/Reports_model");
								$student['idno']= $result->idno;
								$student['fname']= $result->fname;
								$student['lname']= $result->lname;
								$student['mname']= $result->mname;
								$student['yr_level']= $result->year_level;
								$student['abbreviation']= $result->abbreviation;
								$student['max_bracket_units']= $this->input->post('max_units');
								$student['student_histories_id']= $this->input->post('student_histories_id');

								$selected_term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));

								$this->Reports_model->generate_student_class_schedule_pdf($this->input->post('academic_terms_id'), $student, $selected_term);

								break;

					case 're_enroll'	:
								//print("re-enroll"); die();
					         	if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					         		$current_term = $this->Academic_terms_model->current_academic_term();
					         		$current_academic_year = $this->AcademicYears_Model->current_academic_year();
					         		$history = $this->student_model->getEnrollStatus($idnum, $current_term->id);
					         		if($history){
					         			$academic_program_group_id = $this->student_model->getAcadGroup($history->id);
					         		}
					         		$tuition_fee_basic = $this->student_model->getMyTuitionFee_Basic($history->id, $academic_program_group_id->
					         				acad_program_groups_id, $current_term->id );

					         		$tuition_fee_others = $this->student_model->getMyTuitionFee_Others($history->id, $current_term->id);

					         		$paying_units_basic = $this->student_model->getPayingUnitsBasic($idnum, $current_term->id);

					         		$paying_units_others = $this->student_model->getPayingUnitsOthers($idnum, $current_term->id);

					         		if($tuition_fee_others){
					         			$tf_others = $tuition_fee_others->tuition_fee;
					         		}else {
					         			$tf_others = 0;
					         		}

					         		if($paying_units_others->total_paying_units_enrolled == 0){
					         			$payUnits_others = 0;
					         			$rateOthers = 0;
					         		}else {
					         			$payUnits_others = $paying_units_others->total_paying_units_enrolled;
					         			$rateOthers = (float)$tuition_fee_others->tuition_fee/(float)$paying_units_others->total_paying_units_enrolled;
					         		}

					         		$rateBasic = (float)$tuition_fee_basic->tuition_fee/(float)$paying_units_basic->total_paying_units_enrolled;

					         		$enrollments_id = $this->input->post('enrollment_id');

									$course_info = $this->Courses_Model->getCourseInfo($enrollments_id);

									$course_other = $this->Courses_Model->checkIfCourseOther($course_info->id);

									$fee = $this->Accounts_model->MatriFee($history->id, 1);
									$matriculation = $fee->total_fee;


									if($course_other){
										$tuition = $rateOthers * $course_info->paying_units;
										if($course_info->type_description != 'Lab'){
											$amount_credited = $tuition - $matriculation;
										}else {
											$labfee_info = $this->fees_schedule_model->getLabFees($course_info->id, $current_academic_year->id);
											$labfee = $labfee_info->rate;
											$amount_credited = $tuition - $matriculation + $labfee;

									   	}
									}else{
										$tuition = $rateBasic * $course_info->paying_units;
										if($course_info->type_description != 'Lab'){
											$amount_credited = $tuition - $matriculation;


										}else {
											$labfee_info = $this->fees_schedule_model->getLabFees($course_info->id, $current_academic_year->id);
											$labfee = $labfee_info->rate;
											$amount_credited = $tuition - $matriculation + $labfee;


										}
									}
									//print_r($history); die();
									$status = $this->student_model->insert_reenrollment($enrollments_id, $history->academic_programs_id, $history->year_level, $amount_credited, $course_info->course_code);
									if($status){
										$this->content_lib->set_message('Re-enrollment transaction complete!', 'alert-success');
									} else {
										$this->content_lib->set_message('ERROR: Re-enrollment transaction NOT complete!', 'alert-error');
									}

								} else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
									//$this->index();
								}
								break;

						default:

								break;

			}

			if ($result !== FALSE) {

				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$grade_level = array(11,12);

				if (in_array($result->year_level,$grade_level)) { //student is SHS
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname)  . ", "  . $result->fname . " ". $result->mname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> "Grade ".$result->year_level." ".$result->section_name,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address'	=>$result->full_city_address,
							'phone_number'	=>$result->phone_number,
							'section'	=>$result->section,
					);
				} else {
					$dcontent = array(
							'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
							'idnum'		=> $idnum,
							'name'		=> strtoupper($result->lname)  . ", "  . $result->fname . " ". $result->mname,
							'course'	=> $result->abbreviation,
							'college_id'=> $result->colleges_id,
							'college'	=> $result->college_code,
							'level'		=> $result->year_level,
							'full_home_address'	=>$result->full_home_address,
							'full_city_address'	=>$result->full_city_address,
							'phone_number'	=>$result->phone_number,
							'section'	=>$result->section,
					);
				}
				
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
				//tab contents

				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...
				$this->load->library('tab_lib');
				$this->load->library('print_lib');

				//code to view ledger and assessment

				$assessment_values = array();
				$ledger_data = array();
				$ledger_data = $this->teller_model->get_ledger_data($result2->payers_id);
				$this->load->model('academic_terms_model');

				//$this->tab_lib->enqueue_tab('Teller', 'teller/teller', $teller_values, 'teller', TRUE);
				$this->load->model('accounts/accounts_model');
				$this->load->model('hnumis/enrollments_model');
				$this->load->library('shs/shs_student_lib');

				$period_now = $this->academic_terms_model->what_period();

				//assessment details
				$acontent = array(
					'idnumber'	=>$result->idno,
					'familyname'=>$result->lname,
					'firstname'	=>$result->fname,
					'middlename'=>$result->mname,
					'level'		=>$result->year_level,
					'course'	=>$result->abbreviation,
				);
			//assessment form
			$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();

			$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum);

			//assessment form
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok

					$hist_id = $selected_history_id;

				} else {
					$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
					/*EDITED: 9/28/2016 by genes
					* change of academic term variable 
					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					*/
					
					$other_courses_payments = $this->teller_model->other_courses_payments($student_inclusive_terms[0]->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($student_inclusive_terms[0]->id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok

					$hist_id = $result->student_histories_id;
				}
				//print_r($dcourses);die();
				$assessment_date = $this->Student_Model->get_student_assessment_date($hist_id);

				if (!$assessment_date){
					$transaction_date2=array();
					$assessment_date2[0]= (object)array('transaction_date' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . " + 1 day") ));
					$assessment_date = $assessment_date2;
				}


				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$lab_courses = array();
				$courses = array();

				if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						//NOTE: change by Genes 12/1/2016
						//if ( (int)$course->paying_units != 0){

						if ( $course->post_status != "deleted"){
						//if ( $course->paying_units != 0){
						//  Filtering out zero paying units courses causes
						//  such course to be excluded in the listing.  This
						//  would create confusion in the part of the finance dept
						//  And so, instead, just filter out deleted subjects--added condition 6.28.2018 Toyet
						//  As of 5.30.2018, this filtering is disabled.
						//  By: Toyet 5.30.2018
							if ($course->type_description == 'Lab' && empty($course->enrollments_history_id)) {
								$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
									);
							}
							$courses[] = array(
								'id'=>$course->id,
								'name'=>$course->course_code,
								'units'=>$course->credit_units,
								'pay_units'=>($course->post_status=='dissolved' ? ' ': $course->paying_units),
								're_enrollments_id'=>$course->re_enrollments_id,
								'withdrawn_on'=>$course->withdrawn_on,
								'assessment_id'=>$course->assessment_id,
								'post_status'=>$course->post_status,
								'enrollments_history_id'=>$course->enrollments_history_id,
								'transaction_date'=>$course->transaction_date
							);
						}
					}
				}

			   //log_message("INFO", print_r($courses,true)); // Toyet 6.4.2018

			   $my_acad_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum); //terms this student is

			   if ($action=='schedule_current_term') {
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $this->input->post('academic_term'));
					$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $this->input->post('academic_term')); 
					if ($this->input->post('shs_assess')) {
						$tab = 'assessment';
					} else {
						$tab = 'schedule';
					}
					$selected_term = $this->input->post('academic_term');
				} else {
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $my_acad_terms[0]->id);
					$current_history  = $this->shs_student_model->get_StudentHistory_id($idnum, $my_acad_terms[0]->id); //print_r($current_history); die();
					//NOTE: i checked if $tab is existing because i also have a list-menu (tab=certificate)
					//      for certificate - genes 5/7/13
					if (!isset($tab)) {
						$tab = 'assessment';
					}
					$selected_term = '';
				}

				$credit_this_term = 0;


				if ($student_inclusive_terms) { //Added this condition since there will be no output if the student belongs to the Basic Ed- Amie
					foreach($ledger_data as $item){
						if($item['academic_terms_id'] == $student_inclusive_terms[0]->id){
							if($item['transaction_detail'] == 'COLLEGE REGISTRATION'){
								$credit_this_term += $item['credit'];
							}
						}
					}
				}
				$unposted_students_payments = $this->teller_model->get_unposted_students_payment($result2->payers_id);
				$number_of_periods = ($current_academic_terms_obj->term=='Summer' ? 2 : 4);
				$earliest_enrollment_schedule = $this->academic_terms_model->earliest_enrollment_schedule();
				$student_is_enrolled = $this->student_model->student_is_enrolled($idnum);

				/*
				$dcourses = $this->enrollments_model->get_enrolled_courses($history_id->id);
				$cur_aca_yr = $this->AcademicYears_model->current_academic_year();
				if(!empty($dcourses)){
					$lab_courses = array();
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab') {
							$lab_amount = $this->Accounts_model->get_lab_fee($course->id, $cur_aca_yr->id);
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=> (isset($lab_amount->rate) ? $lab_amount->rate : 0),
							);
						}
					}
				}*/

				$laboratory_courses = array();
				foreach($lab_courses as $lab){
					$laboratory_courses[] = $lab['name'];
				}

				$this->load->library('balances_lib',
						array(
								'ledger_data'=>$ledger_data,
								'unposted_students_payments'=>$unposted_students_payments,
								'current_academic_terms_obj'=>$current_academic_terms_obj,
								'period'=>$this->academic_terms_model->what_period(),
								'period_dates'=>$this->academic_terms_model->period_dates(),
						),'balances_lib');

				//ADDED: 6/11/15 by Genes
				$affiliated_fees = $this->fees_schedule_model->ListStudentAffiliated_Fees($hist_id);

				$grade_level = array(11,12);
				
				//log_message("INFO", "BEFORE anything else...");  // Toyet 6.20.2018
				//log_message("INFO", print_r($grade_level));  // Toyet 6.20.2018

				if (is_college_student($idnum)) {
				
					if (in_array($result->year_level,$grade_level)) { //student is SHS
					
						$assess = $this->shs_student_lib->StudentAssessments($current_history); 

						//log_message('INFO',"$courses =>".print_r($courses,true));  //Toyet 11.23.2017
						
						$shs_assessment = $this->accounts_model->student_assessment($current_history->id);

						$this->tab_lib->enqueue_tab('Assessment', 'shs/student/student_assessment',
							array( 
									'assess'=>$assess,
									'academic_terms'=>$student_inclusive_terms,
									'selected_term'=>$selected_term,
									'student_inclusive_terms' =>$student_inclusive_terms,
									'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
									'courses'=>$courses,
									'ledger_data'=>$this->balances_lib->all_transactions(),
									'invoice_data'=>$this->balances_lib->invoice_data('Prelim'),
									'shs_assessment'=> $shs_assessment,
									'current_academic_term_obj'=>$current_academic_terms_obj,
									'student_details'=>$acontent,
									'credit_this_term'=>$credit_this_term,
									'period'=>$this->academic_terms_model->what_period(),    //added Toyet 11.23.2017
									'clearance'=>$this->load->view('print_templates/college_clearance', 
												array(
														'ledger_data'=>$this->balances_lib->current_term_transactions(),
														'idnumber'=>$result2->idno,
														'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
														'name'=>($result2->lname.", ".$result2->fname." ".substr($result2->mname,0,1)),
														'course_year'=>($result2->year_level." ".$result2->abbreviation),
														//'payable'=>$this->balances_lib->balance_for_period('Final', FALSE),
														'payable' => $this->balances_lib->due_this_period('Final'),
														'payment_period'=>'Finals',
														'lab_courses'=>$laboratory_courses ),
												TRUE),
									'midterm_invoice'=>$this->load->view('print_templates/invoice',
												array(
													'period'=>'Midterm',
													'div_id'=>'print_midterm_assessment',
													'ledger_data'=>$this->balances_lib->current_term_transactions(),
													'idnumber'=>$result2->idno,
													//'payable'=>$this->balances_lib->balance_for_period('Midterm', TRUE),
													'payable' => $this->balances_lib->due_this_period('Midterm'),
													'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
													'name'=>($result2->lname.", ".$result2->fname." ".substr($result2->mname,0,1)),
													'course_year'=>($result2->year_level." ".$result2->abbreviation),
													),
												TRUE),
									'prefinal_invoice'=>$this->load->view('print_templates/invoice',
												array(
													'period'=>'Pre-final',
													'div_id'=>'print_prefinal_assessment',
													'ledger_data'=>$this->balances_lib->current_term_transactions(),
													//'payable'=>$this->balances_lib->balance_for_period('Pre-Final', TRUE),
													'payable' => $this->balances_lib->due_this_period('Pre-Final'),
													'idnumber'=>$result2->idno,
													'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
													'name'=>($result2->lname.", ".$result2->fname." ".substr($result2->mname,0,1)),
													'course_year'=>($result2->year_level." ".$result2->abbreviation),
														),
												TRUE),									
									
								), 'assessment', $tab=='assessment', FALSE);
					} else {

						$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment',
								array(
										'assessment_date'=>$assessment_date[0]->transaction_date,
										'other_courses_payments'=>$other_courses_payments,
										'assessment'=> $assessment,
										'courses'=>$courses,
										'lab_courses'=>$lab_courses,
										'affiliated_fees'=>$affiliated_fees,
										'student_inclusive_terms' =>$student_inclusive_terms,
										'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
										'student_details'=>$acontent,
										'ledger_data'=>$this->balances_lib->all_transactions(),
										'invoice_data'=>$this->balances_lib->invoice_data('Prelim'),
										'current_academic_term_obj'=>$current_academic_terms_obj,
										'credit_this_term'=>$credit_this_term,
										'clearance'=>$this->load->view('print_templates/college_clearance',
												array(
														'ledger_data'=>$this->balances_lib->current_term_transactions(),
														'idnumber'=>$result2->idno,
														'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
														'name'=>($result2->lname.", ".$result2->fname." ".substr($result2->mname,0,1)),
														'course_year'=>($result2->year_level." ".$result2->abbreviation),
														//'payable'=>$this->balances_lib->balance_for_period('Final', FALSE),
														'payable'=>$this->balances_lib->due_this_period('Final'),
														'payment_period'=>'Finals',
														'lab_courses'=>$laboratory_courses ),
												TRUE),
										'midterm_invoice'=>$this->load->view('print_templates/invoice',
												array(
													'period'=>'Midterm',
													'div_id'=>'print_midterm_assessment',
													'ledger_data'=>$this->balances_lib->current_term_transactions(),
													'idnumber'=>$result2->idno,
													//'payable'=>$this->balances_lib->balance_for_period('Midterm', TRUE),
													'payable'=>$this->balances_lib->due_this_period('Midterm'),
													'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
													'name'=>($result2->lname.", ".$result2->fname." ".substr($result2->mname,0,1)),
													'course_year'=>($result2->year_level." ".$result2->abbreviation),
													),
												TRUE),
										'prefinal_invoice'=>$this->load->view('print_templates/invoice',
												array(
													'period'=>'Pre-final',
													'div_id'=>'print_prefinal_assessment',
													'ledger_data'=>$this->balances_lib->current_term_transactions(),
													'payable'=>$this->balances_lib->due_this_period('Pre-Final'),
													//'payable'=>$this->balances_lib->balance_for_period('Pre-Final', TRUE),
													'idnumber'=>$result2->idno,
													'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
													'name'=>($result2->lname.", ".$result2->fname." ".substr($result2->mname,0,1)),
													'course_year'=>($result2->year_level." ".$result2->abbreviation),
														),
												TRUE),

								),
									'assessment', $tab=='assessment', FALSE);
					}
				} else {

					$this->load->model('basic_ed/assessment_basic_ed_model');
					$this->load->model('basic_ed/basic_ed_enrollments_model');
					
					$basic_ed = $this->basic_ed_enrollments_model->getLatestHistoryInfo($idnum);

					//log_message("INFO", print_r($basic_ed));  // Toyet 6.20.2018

					if ($basic_ed)
						$this->tab_lib->enqueue_tab ('Assessment', 'teller/assessment_basic_ed',
								array(
										'tuition_basic'=>$this->assessment_basic_ed_model->getTuitionRateBasic($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
										'basic_ed_fees'=>$this->assessment_basic_ed_model->ListBasic_Ed_Fees($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
										'post_status'=>$this->assessment_basic_ed_model->CheckIfPosted($basic_ed->id),
										'basic_ed_sections_id' => $basic_ed->basic_ed_sections_id,
										'stud_status'=>TRUE,
							), 
								'assessment', $tab=='assessment', FALSE);
				}

				//$Invoice_PrelimData = $this->balances_lib->invoice_data('Prelim');
				//log_message('INFO',"INVOICE_DATA =>".print_r($Invoice_PrelimData[1]->debit,true));  //Toyet 11.23.2017

				//enqueue ledger
				$this->load->library('form_lib');
				$this->load->library('table_lib');
				$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$this->balances_lib->all_transactions(),
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date,
						),
								'ledger', FALSE);

			//view payments
				$payments = $this->Payments_Model->ListStudentPayments($result2->payers_id);
				$this->tab_lib->enqueue_tab ('Payments', 'student/list_student_payments2', array('payments_info'=>$payments), 'payments',
											 $tab=='payments',FALSE);

			//for class schedule

			   $academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum); //terms this student is
				  																				//enrolled...
				if ($this->input->post('academic_term')) {
			   		$selected_term = $this->input->post('academic_term');
			   		$terms=$this->AcademicYears_model->getAcademicTerms($this->input->post('academic_term'));
			   		$acad_term=$terms->term;

				} else {
					$selected_term = $academic_terms[0]->id;
					$acad_term = $current_academic_terms_obj->term;
				}


				$selected_history = $this->student_model->get_StudentHistory_id($idnum, $selected_term);
				//print_r($selected_history);die();

				if ($selected_history) {
					$student_units = $this->student_model->getUnits($selected_history, $acad_term);
					$student_history_id=$selected_history->id;
				} else {
					$student_units['max_bracket_units']=0;
					$student_history_id=0;
				}

				$acad_term = $this->AcademicYears_Model->getAcademicTerms($selected_term);

				//print($current_term->id."<br>".$selected_term); die();

				//$list_reenrolled_courses = $this->Courses_Model->listReEnrolledCourses($idnum, $acad_term->id);
				if(is_college_student($idnum))
					if (in_array($result->year_level,$grade_level)) { //student is SHS

						$class_schedules = $this->shs_student_model->ListClassSchedules($current_history->id); 					
						
						$this->tab_lib->enqueue_tab ('Schedule', 'shs/student/student_class_schedules', 
								array('academic_terms'=>$academic_terms,
									'selected_term'=>$selected_term,
									'class_schedules'=>$class_schedules,
									'show_pulldown'=>TRUE,
									), 
								'schedule', $tab=='schedule', FALSE);

					} else { 
						$this->tab_lib->enqueue_tab ('Schedule', 'accounts/schedule', array('schedules'=>$schedule_courses,
												'academic_terms'=>$academic_terms,
												'academic_terms_id'=>$selected_term,
												'max_units'=>$student_units['max_bracket_units'],
												'student_histories_id'=>$student_history_id,
												'current_term_id'=>$current_term->id), 'schedule', $tab=='schedule', FALSE);
					}

				//for privileges
				$current_term = $this->Academic_terms_model->current_academic_term();
				$paying_units_Basic = $this->student_model->getPayingUnitsBasic($idnum, $current_term->id);
				$paying_units_others = $this->student_model->getPayingUnitsOthers($idnum, $current_term->id);

				$history_id = $this->student_model->getEnrollStatus($idnum, $current_term->id);

				//assigns privilege to student
				//student can be given privilege ONLY if he already has course/s enrolled
				if($paying_units_Basic OR $paying_units_others ){
					if (($paying_units_Basic->total_paying_units_enrolled OR  $paying_units_others->total_paying_units_enrolled)
						       AND $history_id->id){
						$MyPrivilege = $this->student_model->Check_if_with_Privilege($history_id->id);

					$assessment = $this->assessment_model->CheckStudentHasAssessment($history_id->id);
				//	if($assessment){
				 			$this->tab_lib->enqueue_tab('Privileges',
									'',
									array(),
									'privileges',
									FALSE);

				 			$data['prospectus']= $this->Programs_Model->ListPrograms($this->session->userdata('college_id'),'O');
							$data['year_level'] = $result->year_level;
							$data['academic_program'] =$result->abbreviation;
							$data['idnum'] = $idnum;
							$data['academic_program_id'] = $result->ap_id;

							$current = $this->Academic_terms_model->getCurrentAcademicTerm();

							$privileges = $this->Scholarship_Model->ListPrivilegesNotTaken($history_id->id);
							//$privileges = $this->Scholarship_Model->ListPrivileges();
							$privilege_availed = $this->student_model->Check_if_with_Privilege($history_id->id);
							//$other_privileges = $this->Accounts_model->ListFeesGroups($history_id->id);
							$other_privileges = $this->Accounts_model->ListAllFeesGroups();
							if($history_id){
								$academic_program_group_id = $this->student_model->getAcadGroup($history_id->id);
							}
							$tuition_fee_basic = $this->student_model->getMyTuitionFee_Basic($history_id->id, $academic_program_group_id->
													acad_program_groups_id,	$current_term->id );

							$withCWTS = $this->student_model->checkIfWithCWTS($history_id->id, $current_term->id);
							//log_message("INFO", print_r($withCWTS,true)); // Toyet 7.3.2018

							//In case academic_terms_id >= 469, check if rate is 0 and assign
							//the $tuition_fee_basic_rate for the CWTS courses -Toyet 7.3.2018
							$tuition_fee_basic_rate = $this->student_model->getTuitionRateBasic($academic_program_group_id->acad_program_groups_id, $current_term->id, $result->year_level);
							
							if($withCWTS){
								//log_message("INFO", print_r($tuition_fee_basic_rate,true)); // Toyet 7.3.2018
								foreach ($withCWTS as $key => $value){
									//log_message("INFO", print_r($key,true)); // Toyet 7.3.2018
									//log_message("INFO", print_r($value,true)); // Toyet 7.3.2018
									if($value==0) $withCWTS->rate = $tuition_fee_basic_rate->rate;
								}
								//log_message("INFO", print_r($withCWTS,true)); // Toyet 7.3.2018
							}

							$tuition_fee_others = $this->student_model->getMyTuitionFee_Others($history_id->id, $current_term->id);

							//log_message("INFO", "tuition_fee_others"); // Toyet 7.3.2018
							//log_message("INFO", print_r($tuition_fee_others,true)); // Toyet 7.3.2018
							if($tuition_fee_others->tuition_fee==NULL)
								$tuition_fee_others->tuition_fee = 0;

							if($withCWTS){
								$tuition_fee = $tuition_fee_basic->tuition_fee + $tuition_fee_others->tuition_fee - ($withCWTS->rate * $withCWTS->paying_units);
							}else{
								$tuition_fee =     $tuition_fee_basic->tuition_fee + $tuition_fee_others->tuition_fee;
							}

							//checks if the student has enrolled a laboratory course
							if(!empty($lab_courses)){
								$with_lab = TRUE;
							}
							else{
								$with_lab = FALSE;
							}

							//log_message("INFO", print_r($tuition_fee_basic,true)); // Toyet 7.3.2018
							//log_message("INFO", print_r($tuition_fee,true)); // Toyet 7.3.2018
							//end of code to check if student has enrolled a laboratory course
							$this->tab_lib->enqueue_tab ("Assign", "accounts/form_to_assign_privileges", array('student_idno'=>$idnum,
																'term'=> $current,'history_id'=>$history_id, 'privilege_items'=>$privileges,
															'other_privileges'=>$other_privileges, 'privilege_availed'=>$privilege_availed,
															'tuition_fee_basic'=>$tuition_fee_basic, 'tuition_fee_others'=>$tuition_fee_others
															,'assessed'=>$assessment, 'with_lab'=>$with_lab,
														     'tuition_fee'=>$tuition_fee, ), "form_to_assign_privileges", FALSE, 'privileges');

						//allows the user to view/delete privilege details
						 //this is available ONLY if privilege has been assigned and NOT posted yet
					     /*	if ($MyPrivilege AND ($tuition_fee_basic OR $tuition_fee_others)) {
						    $paying_units_basic = $this->student_model->getPayingUnitsBasic($idnum, $current_term->id);
							$paying_units_others = $this->student_model->getPayingUnitsOthers($idnum, $current_term->id);
							if($tuition_fee_others){
								if($tuition_fee_others->tuition_fee == 0){
									$tuition_fee_others = 0;
								}else {
									$tuition_fee_others = $tuition_fee_others->tuition_fee;
								}
							}

							if($paying_units_others->total_paying_units_enrolled == 0){
								$paying_units_others = 0;
							}else {
								$paying_units_others = $paying_units_others->total_paying_units_enrolled;
							}
							$max_units_info = $this->Scholarship_Model->getMaxUnitsDiscount($data1['privilege_id']);
							$tuition_discount_info = $this->tuitionDiscount($tuition_fee_basic->tuition_fee, $tuition_fee_others,
														$paying_units_basic->total_paying_units_enrolled, $paying_units_others,
														$history_id->year_level, $max_units_info);
							$privilege  = $this->Scholarship_Model->getScholarship($MyPrivilege->id);
							$paying_units = $tuition_discount_info[1];
							$tuition_info = $this->Scholarship_Model->getTuitionPrivilege($MyPrivilege->id);
							$tuition_percentage = $tuition_info->discount_percentage;
							$other_privileges = $this->Scholarship_Model->ListPrivilegeAvailedDetails($MyPrivilege->id);
							$tuition = $tuition_discount_info[0];
							foreach($other_privileges as $other_privilege){
								if($other_privilege->weight == 1){
									  $tuition_disc = $other_privilege->discount_amount;
									  $tuition_group = $other_privilege->fees_group;
								}
							}


							$this->tab_lib->enqueue_tab ("View/Delete Details", "accounts/display_assigned_privileges",
														array('student_idno'=>$idnum, 'term'=>$current,'tuition_percentage'=>$tuition_percentage,
															'paying_units'=>$paying_units, 'other_privileges'=>$other_privileges,
															'privilege'=>$privilege, 'tuition_disc'=> $tuition_disc, 'tuition'=>$tuition,
															'tuition_group'=> $tuition_group), "display_assigned_privileges", FALSE, 'privileges');
						     $this->tab_lib->enqueue_tab ("Delete", "accounts/form_to_delete_privilege", array('student_idno'=>$idnum, 'term'=>$current,
															'tuition_percentage'=>$tuition_percentage, 'paying_units'=>$paying_units,
															'other_privileges'=>$other_privileges,
															'privilege'=>$privilege), "form_to_delete_privilege", FALSE, 'privileges');
					    	}	*/


						//The following code allows the user to input cash discount

						if ($tuition_fee_basic OR $tuition_fee_others) {
						       $paying_units_basic = $this->student_model->getPayingUnitsBasic($idnum, $current_term->id);
							   $paying_units_others_info = $this->student_model->getPayingUnitsOthers($idnum, $current_term->id);
							if($tuition_fee_others){
								if($tuition_fee_others->tuition_fee == 0){
										$tuition_fee_others = 0;
								}else {
									$tuition_fee_others = $tuition_fee_others->tuition_fee;
							}
						}

						if($paying_units_others->total_paying_units_enrolled == 0){
							$paying_units_others = 0;
						}else {
							$paying_units_others = $paying_units_others->total_paying_units_enrolled;
						}

						$withCWTS = $this->student_model->checkIfWithCWTS($history_id->id, $current_term->id);
						//log_message("INFO", print_r($withCWTS,true)); // Toyet 7.3.2018

						//In case academic_terms_id >= 469, check if rate is 0 and assign
						//the $tuition_fee_basic_rate for the CWTS courses -Toyet 7.3.2018
						$tuition_fee_basic_rate = $this->student_model->getTuitionRateBasic($academic_program_group_id->acad_program_groups_id, $current_term->id, $result->year_level);
						//log_message("INFO", print_r($tuition_fee_basic_rate,true)); // Toyet 7.3.2018
						if($withCWTS){
							foreach ($withCWTS as $key => $value){
								//log_message("INFO", print_r($key,true)); // Toyet 7.3.2018
								//log_message("INFO", print_r($value,true)); // Toyet 7.3.2018
								if($value==0) $withCWTS->rate = $tuition_fee_basic_rate->rate;
							}
							//log_message("INFO", print_r($withCWTS,true)); // Toyet 7.3.2018
						}


						if($withCWTS){
							$tuition_fee = $tuition_fee_basic->tuition_fee + $tuition_fee_others - ($withCWTS->rate * $withCWTS->paying_units);
						}else{
							$tuition_fee =     $tuition_fee_basic->tuition_fee + $tuition_fee_others;
						}

						$tuition_discount = 0;
						if($MyPrivilege){
							$tuition_discount_info = $this->Scholarship_Model->ListPostedPrivileges($history_id->id);
							if($tuition_discount_info){
								foreach($tuition_discount_info as $t_dicount){
									$tuition_discount += $t_dicount->discount_amount;
								}
							}
						}

						$list_tuition_privileges_availed = $this->Scholarship_Model->ListPrivilegesWdoutCash($history_id->id);
						//print_r($list_tuition_privileges_availed); die();
						$total_tuition_withdrawn = 0;
						$withWithdrawals = $this->student_model->checkIfWithWithdrawals($history_id->id);
						if($withWithdrawals){
							$total_tuition_withdrawn = 0;
							$student_info = $this->Student_Model->getStudentInfo($history_id->id);
							$tuition_basic_rate = $this->Student_Model->getTuitionRateBasic($student_info->acad_program_groups_id, $student_info->academic_terms_id, $student_info->year_level);
							$total_basic_tuition_withdrawn = 0;
							$total_others_tuition_withdrawn = 0;
							foreach($withWithdrawals as $withdrawn){
								$course_info = $this->Courses_Model->getCourseInfo($withdrawn->id);
								$tuition_others_rate = $this->Student_Model->getTuitionOthersRate($withdrawn->id, $student_info->academic_terms_id, $student_info->year_level);
								if($tuition_others_rate){
									$total_others_tuition_withdrawn += $tuition_others_rate->rate * $course_info->paying_units;
								}else{
									$total_basic_tuition_withdrawn += $tuition_basic_rate->rate * $course_info->paying_units;
								}
								$total_tuition_withdrawn = $total_others_tuition_withdrawn + $total_basic_tuition_withdrawn;
							}
						}
						$tuition_fee = 	$tuition_fee - 	$total_tuition_withdrawn;

						$total_tuition_discount = 0;
						$tuition_for_cash_discount = 0;
						if($list_tuition_privileges_availed){
							foreach ($list_tuition_privileges_availed as $tuition_disc){
								$total_tuition_discount += $tuition_disc->discount_amount;
							}
							$tuition_for_cash_discount = $tuition_fee - $total_tuition_discount;
						}else{
							$tuition_for_cash_discount = $tuition_fee;
						}

						//print($tuition_for_cash_discount); die();
						//The following code lists all unposted privileges of the current sem
						$unpostedPrivileges = $this->Scholarship_Model->ListUnpostedPrivileges($history_id->id);

						$this->tab_lib->enqueue_tab ("List Unposted Privileges", "accounts/list_unposted_privileges",
								array('privileges'=>$unpostedPrivileges, 'tuition_discount'=>$tuition_fee,
										'tuition_for_cash_discount'=>$tuition_for_cash_discount), "list_unposted_privileges", FALSE, 'privileges');


					//	the following code allows the user to input cash discount to a student
						$cash_privileges_added = $this->Scholarship_Model->LisAddedCashDiscount($history_id->id);
						//print_r($cash_privileges_added); die();
						if($cash_privileges_added){
							$withCashPriv = TRUE;
						}else{
							$withCashPriv = FALSE;
						}

						$this->tab_lib->enqueue_tab ("Cash Discount", "accounts/form_to_input_cash_discount",
								array('privileges'=>$list_tuition_privileges_availed, 'student_histories_id'=> $history_id->id, 'assessment'=>$assessment,
										'total_tuition_discount'=>$total_tuition_discount, 'tuition_fee'=>$tuition_fee,
										'tuition_for_cash_discount'=>$tuition_for_cash_discount,
										'academic_programs_id'=>$result->academic_programs_id,
										'year_level'=>$result->year_level,
										'withCashPriv'=>$withCashPriv), "form_to_input_cash_discount", FALSE, 'privileges');

				   }
			// }
		 }

	}

	//the following code allows the user to view past privileges

			/*	if ($action=='schedule_current_term') {
					$past_privilege_availed = $this->Scholarship_Model->ListPastPrivileges($idnum, $this->input->post('academic_term'));
					$tab = 'list_student_privileges';
					$selected_term = $this->input->post('academic_term');
				} else {
					$past_privilege_availed = $this->Scholarship_Model->ListPastPrivileges($idnum, $current_term->id);
					$tab = 'assessment';
					$selected_term = '';
				}
				//print_r($past_privilege_availed); die();
				$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum);
				$selected_term = $this->input->post('academic_term');
				*/
				$past_privileges_availed = $this->Scholarship_Model->ListPastPrivileges($idnum);
			    if($past_privileges_availed){
			    	foreach($past_privileges_availed as $past_privilege){
					    $past_privileges_details = $this->Scholarship_Model->getPastPrivilegesDetails($past_privilege->id);
			    	}
			    }


		$this->tab_lib->enqueue_tab ("View Past Privileges", "accounts/list_past_privileges", array('past_privileges' => $past_privileges_availed,
											), 'list_past_privileges', FALSE, 'privileges');


		//allow student to enroll
				//insert condition to check if student is assigned a program
				if(is_college_student($idnum))
					$this->tab_lib->enqueue_tab('Allow to Enroll', 'accounts/privilege_enroll', array('history_id'=>$history_id), 'privilege', FALSE);

		//displays total withdrawal computation
				if($history_id){
				//	print($history_id->id);
				//	die();
					    $current_term = $this->Academic_terms_model->current_academic_term();
					   // print($current_term->id);
					   // die();
						$assessment = $this->assessment_model->CheckStudentHasAssessment($history_id->id);
						//print_r($assessment);
						//die();
						if($assessment){
						$academic_program_group_id = $this->student_model->getAcadGroup($history_id->id);

						$tuition_fee_basic = $this->student_model->getMyTuitionFee_Basic($history_id->id, $academic_program_group_id->
														acad_program_groups_id, $current_term->id);
						//print($tuition_fee_basic->tuition_fee); die();
						$tuition_fee_others = $this->student_model->getMyTuitionFee_Others($history_id->id, $current_term->id);

						if($tuition_fee_others){
							$tuition_others = $tuition_fee_others->tuition_fee;
						}else{
							$tuition_others = 0;
						}

						$tuition = $tuition_fee_basic->tuition_fee + $tuition_others;
					//	print_r($tuition); die();
						$fee = $this->Accounts_model->getFees($history_id->id, 1);
						$matriculation = $fee->total_fee;
						$fee = $this->Accounts_model->getFees($history_id->id, 2);
						$miscellaneous = $fee->total_fee;
						$fee = $this->Accounts_model->getFees($history_id->id, 3);
						$other_fees = $fee->total_fee;
						/*
						*  ADDED: 12/10/15 by genes
						*  fees for Learning Resources Fee and Student Support Services
						*  Affiliated Fees are also included here..
						*  NOTE: affiliated fees are computed above; used in assessment
						*/
						$fee = $this->Accounts_model->getFees($history_id->id, 10);
						$learning_resources = $fee->total_fee;
						$fee = $this->Accounts_model->getFees($history_id->id, 11);
						$student_support = $fee->total_fee;

						$other_add_fees = 0;

						if ($affiliated_fees) {
							foreach ($affiliated_fees AS $a_fee) {
								$other_add_fees += $a_fee->rate;
							}
						}
						/*
						*  end..
						*/

						$fee = $this->Accounts_model->getFees($history_id->id, 5);
						$dcourses = $this->enrollments_model->get_enrolled_courses($history_id->id);
						//print_r($dcourses); die();
						$cur_aca_yr = $this->AcademicYears_model->current_academic_year();
						if(!empty($dcourses)){
							$lab_courses = array();
							foreach ($dcourses as $course) {
								if ($course->type_description == 'Lab' AND $course->enrolled_status == 'current') {
										$lab_amount = $this->Accounts_model->get_lab_fee($course->id, $cur_aca_yr->id);
										$lab_courses[] = array(
															'name'=>$course->course_code,
															'amount'=>(isset($lab_amount->rate) ? $lab_amount->rate : 0),
														);
									}
							}
						}
						$total_lab_fees = 0;
						if($lab_courses){
						foreach ($lab_courses as $lab_course) {
							$total_lab_fees += (float)$lab_course['amount'];
							}
						}
						$student_info = $this->student_model->getStudentInfo($history_id->id);
						$payers = $this->teller_model->get_student($student_info->students_idno);
						$ledger_data = $this->teller_model->get_ledger_data($payers->payers_id);
						//$ledger_data = $this->teller_model->listStudentLedgerEntries($payers->payers_id);
						$running_balance_tuition = 0;
						$running_balance = 0;
						$runnung_balance_not_tuition = 0;
						$amount_paid = 0;
						$total_debit = 0;
						foreach($ledger_data as $ledge){
							$running_balance +=  $ledge['debit'] - $ledge['credit'];
							$amount_paid += $ledge['credit'];
							$total_debit += $ledge['debit'];
						/*	if(!($ledge['transaction_detail'] == 'COLLEGE - TUITION FEE' OR $ledge['transaction_detail'] == 'Assessment'
							OR $ledge['transaction_detail'] == 'COLLEGE REGISTRATION')) {
								$runnung_balance_not_tuition += $ledge['debit'] - $ledge['credit'];
							}*/
							$running_balance = round($running_balance,5);
							if($running_balance == 0){
								$amount_paid = 0;
								$total_debit = 0;
							}
						}

						//UPDATED: 12/11/15 by genes
						//$total_assessment = $tuition + $total_lab_fees + $matriculation + $miscellaneous + $other_fees;
						$total_assessment = $tuition + $total_lab_fees + $matriculation + $miscellaneous + $other_fees
											+ $learning_resources + $student_support + $other_add_fees;

						$other_payable = $total_debit - $total_assessment;
						$posted_privilege_availed = $this->student_model->GetPostedPrivilege($history_id->id);
						/*print("TOTAL DEBIT:".$total_debit);
						print("<p>TOTAL CREDIT:".$amount_paid);
						print("<p>RUNNING BALANCE:".$running_balance);
						print("<p>TOTAL ASSESS:".$total_assessment);
						print("<p>OTHER PAYABLE:".$other_payable);
						die();
						*/
					//****This is not included *******
						//adjustment is made if the privilege has been posted already
						if($posted_privilege_availed){
							$privilege_info = $this->Scholarship_Model->getPrivilegeAvailedInfo($posted_privilege_availed->id);
							$amount_paid = $amount_paid - $privilege_info->total_amount_availed;
						}

					//End of not included **********
						$withdraw_percentage = $this->Withdrawal_Model->getWithdrawalPercentage($current_term->id);
						//print_r($withdraw_percentage); die();
						if($withdraw_percentage){
							$this->tab_lib->enqueue_tab('Withdrawal Computation', 'accounts/form_to_view_withdrawal_payments',
													array('tuition_fee'=>$tuition, 'withdraw_percentage' => $withdraw_percentage->percentage,
													'matriculation'=>$matriculation, 'miscellaneous'=>$miscellaneous, 'other_fee'=>$other_fees,
													'learning_resources'=>$learning_resources,'student_support'=>$student_support,
													'other_add_fees'=>$other_add_fees,
													'lab_fee'=>$total_lab_fees, 'amount_paid'=>$amount_paid, 'other_payable'=>$other_payable),
													'view_withdraw_computation', FALSE);
						}
					}
				}
			//Printing
					$result2 = $this->teller_model->get_student($result->idno);
					$ledger_data = $this->balances_lib->all_transactions();//$this->teller_model->get_ledger_data($result2->payers_id);
					$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
					$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id);
					$lab_courses = array();

					if(!empty($dcourses)){
						foreach ($dcourses as $course) {
							if ($course->type_description == 'Lab') {
								$lab_courses[] = $course->course_code;
							}
						}
					}

					$data = array(
							'idnumber'=>$result->idno,
							'name'=>$result->lname . ", " . $result->fname . " " . $result->mname,
							'course_year'=>$result->abbreviation . " " . $result->year_level,
							'payment_period'=>'Finals',
							'ledger_data'=>$ledger_data,
							'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
							'lab_courses'=>$lab_courses,
					);
					/*
					if(is_college_student($idnum))
						$this->tab_lib->enqueue_tab('Clearance', 'print_templates/student_clearance', $data, 'print_shortcut', FALSE);
					*/
					if (!$this->input->post('academic_terms_id')) {

						$data['courses'] = $this->student_model->ListMyTuitionFees_Basic($result->student_histories_id);
						if ($data['courses']) {
							$data['tuition_rate'] = $data['courses'][0]->rate;
						} else {
							$data['tuition_rate'] = 0;
						}
						//print_r($data['courses'] ); die();
						$data['matriculation'] = $this->student_model->getMyMatriculationFee($result->student_histories_id);
						$data['miscs'] = $this->student_model->ListMyMiscellaneous_Fees($result->student_histories_id);
						$data['other_fees'] = $this->student_model->ListMyOther_Fees($result->student_histories_id);
						$data['addtl_other_fees'] = $this->student_model->ListMyAddtlOther_Fees($result->student_histories_id);
						$data['lab_fees'] = $this->student_model->ListMyLaboratory_Fees($result->student_histories_id);
						$data['name'] = $result->lname.", ".$result->fname." ".$result->mname;
						$data['course_year'] = $result->abbreviation."-".$result->year_level;
						$data['student_histories_id'] = $result->student_histories_id;

						/*
						 * ADDED: 6/30/15 by genes
						 */
						$data['learning_resources'] = $this->student_model->getLearningResources_Fees($result->student_histories_id);
						$data['student_support'] = $this->student_model->getStudentSupport_Fees($result->student_histories_id);
						$data['affiliated_fees'] = $this->fees_schedule_model->ListStudentAffiliated_Fees($result->student_histories_id);

						//$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
						$data['academic_terms'] = $this->academic_terms_model->student_inclusive_academic_terms($result->idno);

						$data['term'] = $this->academic_terms_model->getCurrentAcademicTerm();
						//$data['current_term'] = $data['term']->id;

					} else {

						$student_histories_id = $this->student_model->getEnrollStatus($result->idno,$this->input->post('academic_terms_id'));

						if (!$student_histories_id) {
							$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
							$data['current_term'] = $this->input->post('academic_terms_id');
						} else {

							$data['courses'] = $this->student_model->ListMyTuitionFees_Basic($student_histories_id->id);
							if ($data['courses']) {
								$data['tuition_rate'] = $data['courses'][0]->rate;
							} else {
								$data['tuition_rate'] = 0;
							}

							$data['matriculation'] = $this->student_model->getMyMatriculationFee($student_histories_id->id);
							$data['miscs'] = $this->student_model->ListMyMiscellaneous_Fees($student_histories_id->id);
							$data['other_fees'] = $this->student_model->ListMyOther_Fees($student_histories_id->id);
							$data['addtl_other_fees'] = $this->student_model->ListMyAddtlOther_Fees($student_histories_id->id);
							$data['lab_fees'] = $this->student_model->ListMyLaboratory_Fees($student_histories_id->id);
							$data['name'] = $result->lname.", ".$result->fname." ".$result->mname;
							$data['course_year'] = $student_histories_id->abbreviation."-".$student_histories_id->year_level;
							$data['student_histories_id'] = $student_histories_id->id;

							/*
							 * ADDED: 6/30/15 by genes
							 */
							$data['learning_resources'] = $this->student_model->getLearningResources_Fees($student_histories_id->id);
							$data['student_support'] = $this->student_model->getStudentSupport_Fees($student_histories_id->id);
							$data['affiliated_fees'] = $this->fees_schedule_model->ListStudentAffiliated_Fees($student_histories_id->id);

							$data['academic_terms'] = $this->academic_terms_model->student_inclusive_academic_terms($result->idno);

							$data['term'] = $this->AcademicYears_model->getAcademicTerms($this->input->post('academic_terms_id'));

							//print_r($data['term']); die();

						}

						$tab='certificate';

					}
					/* Please spray some pesticide to this code part... this is buggy!!!!*/
					//print_r($data); die();
					if(is_college_student($idnum)){
						if (!$this->input->post('academic_terms_id')) {
							//print($data['academic_terms'][0]->student_sy); die();
							$data['term'] = new StdClass();
							$data['sy'] = new StdClass();
							$data['term']->id = $data['academic_terms'][0]->id;
							$data['term']->term = $data['academic_terms'][0]->student_term;
							$data['term']->sy = $data['academic_terms'][0]->student_sy;

						}
						//print_r($data['affiliated_fees']); die();
						$this->tab_lib->enqueue_tab('Certificate', 'accounts/student_certificate', $data, 'certificate', $tab=='certificate', FALSE);
					}
					//Added: December 7, 2013; viewing of statement of account for Basic Ed students
					//$current_acadYear = $this->AcademicYears_model->current_academic_year();
					//for basic education
					$current_acadYear = $this->AcademicYears_model->current_academic_year();
					//var_dump($current_acadYear);die();
					//print_r($idnum);die();
					if (!in_array($result->year_level,$grade_level)) { //student is not SHS

						if($this->student_model->isBasicEd($idnum, $current_acadYear->id)){
							
							$basicEd_info = $this->student_model->get_basicEdLevel($idnum, $current_acadYear->id);
							if(($basicEd_info->level == 'Kinder' AND $basicEd_info->yr_level == 2) OR
									($basicEd_info->level == 'GS' AND $basicEd_info->yr_level == 6) OR
									(($basicEd_info->level == 'HS-Day' OR $basicEd_info->level == 'HS-Night') AND $basicEd_info->yr_level == 4) ){
								$numExam = 5;
							}else{
								$numExam = 6;
							}

							$charges = 0;
							$payments = 0;
							$running_balance = 0;
							$additional = 0;
							//print_r($ledger_data); die();

							foreach($ledger_data as $ledger){
								/*print($ledger['academic_terms_id']); die();
								$trans_year = (int)(substr($ledger['transaction_date'],0,4));
								if ($this->academic_terms_model->is_withinAcadYear($ledger['academic_terms_id'],$current_acadYear->id, $trans_year)) {
									if(($ledger['transaction_detail'] == 'ELEMENTARY REGISTRATION') OR ($ledger['transaction_detail'] == 'ELEMENTARY - TUITION FEE')
											OR ($ledger['transaction_detail'] == 'HIGH SCHOOL - REG. FEE') OR ($ledger['transaction_detail'] == 'HIGH SCHOOL-TUITION FEE')
											OR ($ledger['transaction_detail'] == 'KINDERGARTEN-REG. FEE') OR ($ledger['transaction_detail'] == 'KINDERGARTEN-TUITION FEE')
											OR ($ledger['transaction_detail'] == 'TUITION TRANSFER - CREDIT') OR ($ledger['transaction_detail'] == 'FAC. & EMPLOYEES PRIV.')
											OR ($ledger['transaction_detail'] == 'BROTHERS/SISTERS PRIV.')){
										$payments+= $ledger['credit'];
									} else {
										if ($ledger['transaction_detail'] != 'ASSESSMENT') {
											$additional += $ledger['debit'];
										}
									}
								}*/
								$running_balance +=  $ledger['debit'] - $ledger['credit'];
								//$running_balance = round($running_balance,5);

								/*if($running_balance == 0){
								 $charges = 0;
								$payments = 0;
								}*/
							}


							$payablePerExam = ($basicEd_info->assessed_amount - 500) / $numExam;
							$due['Ex1'] = $payablePerExam;
							$due['Ex2'] = $payablePerExam * 2;
							$due['Ex3'] = $payablePerExam * 3;
							$due['Ex4'] = $payablePerExam * 4;
							$due['Ex5'] = $payablePerExam * 5;
							$due['Ex6'] = $payablePerExam * 6;

							$other_charges = 0;
							$expected_balance_before_exam5 =  ($basicEd_info->assessed_amount - 500) - $due['Ex4'];
							if($running_balance > $expected_balance_before_exam5){
								$other_charges = $running_balance - $expected_balance_before_exam5;
							}
							if($running_balance <= $payablePerExam){
								$payable_exam5 = $other_charges/2;
							}else{
								$excess = $running_balance - $payablePerExam;
								if($excess < $payablePerExam){
									$payable_exam5 = $excess + $other_charges/2;
								}else{
									$payable_exam5 = ($running_balance + $other_charges) / 2;
								}
							}


							if($numExam == 6){
								$due['Ex6'] =  $payablePerExam * 6;
							}

							$payable = 	$running_balance/$numExam;
							//print($payments); die();
							//$payable = ($due['Ex5'] + ($additional/2)) - ($payments - 500);

							//print($due['Ex5']."<br>".$payments."<br>".$payable); die();
							/*$balance = 	$charges - $payments;
							 if($numExam == 6){
							$payable =
							}else{
							$payable = $balance/2;
							}*/
							//print("Charges->".$charges."<br>"."Payments->".$payments."<br>Balance->".$balance."<br>Payable ->".$payable); die();

						}

						if($this->student_model->isBasicEd($idnum, $current_acadYear->id)){
							$this->tab_lib->enqueue_tab('Statement of Account', 'accounts/basic_ledger',
									array(
											'ledger_data'=>$ledger_data,
											'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
											'year_start_date'=>$current_academic_terms_obj->year_start_date,
											'due'=>$payable,
									),
									'basic_ledger', FALSE);
						}
					}

					$tab_content = $this->tab_lib->content();
					$this->content_lib->enqueue_body_content("", $tab_content);
				}

			}

			$this->content_lib->content();
			//this should be placed here, outside the html tag or body to be accessible
			echo sprintf($this->config->item('jzebra_applet'), base_url());
	}

	public function basic_ed($idnum){
		$this->load->model('student_common_model');
		$this->load->model('financials/Payments_Model');
		$this->load->model('Academic_terms_model');
		$this->load->model('basic_ed/Basic_Ed_Enrollments_Model');
		$this->load->model('financials/Scholarship_Model');
		$this->load->model('accounts/Accounts_model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('basic_ed/assessment_basic_ed_model');


		$result = $this->student_common_model->my_information($idnum);
		$this->content_lib->set_title ('Accounts | Basic Ed | ' . $idnum);
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');

		switch ($action = $this->input->post('action')){

			case 'form_to_assign_privileges':

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$current_term = $this->Academic_terms_model->current_academic_term();
						$tuition_percentage = sanitize_text_field($this->input->post('TuitionD'));

						//print($tuition_percentage); die();
						$tuition_fee = sanitize_text_field($this->input->post('tution_fee'));

						$data1['student_history_id'] = sanitize_text_field($this->input->post('history_id'));
						$data1['privilege_id'] = sanitize_text_field($this->input->post('privilege_id'));
						$data1['levels_id'] = $result->levels_id;
						$data1['year_level'] = $result->year_level;

						$privilege = $this->Student_model->Check_if_with_Privilege($data1['student_history_id'], 1);
						//print($data1['student_history_id']); die();
						//insert privilege to privileges_availed table

						//print_r($result);die();

						$privilege_availed_id = $this->Scholarship_Model->AddStudentPrivilege($data1, TRUE);
						//print_r($privilege_availed_id); die();
						//inserts tuition discount
						if($privilege_availed_id){
							$data2['fees_groups_id'] = 9;
							$data2['discount_percentage'] = (float)$tuition_percentage/100;
							$data2['discount_amount'] = $tuition_fee * $data2['discount_percentage'];
							$priv_detail_id = $this->Scholarship_Model->AddPrivilegeDetails($privilege_availed_id, $data2);
						}

						//inserts other privileges
						$fees_groups = $this->Accounts_model->ListOtherFeesGroups();
						$other_priv_availed = $this->input->post('other_priv');
						if($other_priv_availed){
							$student_info = $this->Basic_Ed_Enrollments_Model->getLatestHistoryInfo($this->input->post('stud_id'));
							foreach($other_priv_availed as $priv_id => $priv){
								$fees = 0;
								$data2['discount_amount'] = 0;
								$fees = $this->Accounts_model->getFeesBed($student_info->academic_terms_id, $priv_id, $student_info->levels_id, $student_info->yr_level);
								//print_r($fees); die();
								if($fees->total_fee) {
									$data2['discount_amount'] = $fees->total_fee;
									$data2['fees_groups_id'] = $priv_id;
									$data2['discount_percentage'] = 1.00;

									//inserts other privileges discount, if there is any
									$priv_detail_id = $this->Scholarship_Model->AddPrivilegeDetails($privilege_availed_id, $data2);

								} else {
									$priv_detail_id = NULL;
								}

							}
						}

						if($privilege_availed_id){
							$this->content_lib->set_message("Privilege Successfully Added!", "alert-success");
						} else {
							$this->content_lib->set_message("Error found while adding a privilege!", "alert-error");
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;

				case 'delete_unposted_privilege':

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){

						$status = $this->Scholarship_Model->DeletePrivilegeAvailed($this->input->post('privilege_id'));
						if($status){
							$this->content_lib->set_message("Privilege Successfully Deleted!", "alert-success");
						}else {
							$this->content_lib->set_message("Error found while deleting a privilege!", "alert-error");
						}
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
				case 'form_to_add_cash_discount':

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						//$tuition_fee = $this->input->post('tuition_fee');
						$tuition_discounts = $this->input->post('tuition_for_cash_discount');

						if($tuition_discounts == 0){
							$tuition_discounts = $this->input->post('tuition_fee');
						}
						//print($tuition_discounts); die();

						$data['discount_percentage'] = $this->input->post('CashDisc')/100;
						$data['discount_amount'] = $tuition_discounts * $data['discount_percentage'];
						$data['student_history_id']= $this->input->post('student_histories_id');
						$data['privilege_id'] = 186;
						$data['fees_groups_id'] = 9;

						//inserts the cash discount to both privileges_availed and privileges_availed_details tables
						$status = $this->Scholarship_Model->AddCashDiscount($data, 1);
						//print($status); die();

						if($status){
							$this->content_lib->set_message("Cash Discount Successfully Added!", "alert-success");
						}else {
							$this->content_lib->set_message("Error found while processing cash discount!", "alert-error");
						}
					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;

				case 'view_past_privilege_detail':
					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$privilege_id = $this->input->post('priv_id');
						$privilege_info = $this->Scholarship_Model->getPrivilegeAvailedInfo($privilege_id);

						$privilege_details_info = $this->Scholarship_Model->ListPrivilegeAvailedDetails($privilege_id);

						$this->list_privileges_availed($privilege_info, $privilege_details_info,2);

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;
			}


		if ($result !== FALSE) {
			//a user with that id number is seen...
			$port = substr($idnum, 0, 3);
			$dcontent = array(
					'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
					'idnum'		=> $idnum,
					'name'		=> $result->fname . " " . $result->lname,
					'course'	=> $result->abbreviation,
					'college_id'=> $result->colleges_id,
					'college'	=> $result->college_code,
					'level'		=> $result->year_level,
					'full_home_address'	=>$result->full_home_address,
					'full_city_address'	=>$result->full_city_address,
					'phone_number'	=>$result->phone_number,
					'section'	=>$result->section,
					'bed_status' => $result->bed_status,
			);
			if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
				$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
			else {
				if ($result->gender == 'F')
					$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
					$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
			}
			//student profile:
			//print"hello hello"; die();
			$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
			//model calls:
			$this->load->model('teller/teller_model');
			$this->load->model('hnumis/academic_terms_model');
			$this->load->model('hnumis/academicyears_model', 'AcademicYears_model');
			$this->load->model('hnumis/student_model');

			$this->load->library('print_lib');
			$current_acadYear = $this->AcademicYears_model->current_academic_year();
			$result2 = $this->teller_model->get_student($idnum);
			$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
			$ledger_data = array();
			$ledger_data = $this->teller_model->get_ledger_data($result2->payers_id);
			$unposted_payments = $this->teller_model->get_unposted_students_payment($result2->payers_id);

			$this->load->model('basic_ed/basic_ed_model', 'bed_model');
			$config = array(
					'ledger_data'			=> $ledger_data,
					'unposted_transactions'	=> $unposted_payments,
					'periods'				=> $this->bed_model->exam_dates($result2->year_level, $result2->levels_id),
					'current_academic_year' => $this->bed_model->academic_year(),
					'bed_status' 			=> $result->bed_status,
					'current_nth_exam'      => $this->bed_model->current_exam($result2->year_level, $result2->levels_id),
					'total_number_of_exams' => count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id)),
			);
			//print_r($config);die();
			//'current_period'		=> $this->bed_model->current_exam($result->year_level, $result->levels_id),


			$this->load->library('basic_ed_balances', $config);
			if($this->basic_ed_balances->error == TRUE){
				$this->content_lib->set_message($this->basic_ed_balances->error_message, 'alert-error');
				$this->content_lib->content();
				return;
			}

			$basicEd_info = $this->student_model->get_basicEdLevel($idnum, $current_acadYear->id);

			if($basicEd_info){
				$show_invoice = TRUE;
			} else {
				$show_invoice = FALSE;
				$numExam = 1;
				$exams = 1;
			}

			$this->load->library('tab_lib');
			$tab = isset($tab) ? $tab : 'assessment';

			$basic_ed = $this->Basic_Ed_Enrollments_Model->getLatestHistoryInfo($idnum);
			//print_r($idnum);die();

			$stud_status = FALSE;
			//if ($basic_ed && $basic_ed->status == 'active') {
			if ($basic_ed) {
				$stud_status = TRUE;

				$this->tab_lib->enqueue_tab ('Assessment', 'teller/assessment_basic_ed',
						array(
								'tuition_basic'=>$this->assessment_basic_ed_model->getTuitionRateBasic($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
								'basic_ed_fees'=>$this->assessment_basic_ed_model->ListBasic_Ed_Fees($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
								'post_status'=>$this->assessment_basic_ed_model->CheckIfPosted($basic_ed->id),
								'basic_ed_sections_id' => $basic_ed->basic_ed_sections_id,
								'stud_status'=>$stud_status,
								'current_nth_exam'    => $this->basic_ed_balances->current_nth_exam(),
								'due_this_exam'       => $this->basic_ed_balances->due_this_exam2(),
							),
								'assessment', $tab=='assessment', FALSE);
			}
			//print_r($this->basic_ed_balances->payable_for_exam($this->bed_model->current_exam($result2->year_level, $result2->levels_id)));die();
			//print_r($this->bed_model->current_exam($result2->year_level, $result2->levels_id));die();
			$this->tab_lib->enqueue_tab('Ledger', 'accounts/basic_ledger',
					array(
							'ledger_data'=>$this->basic_ed_balances->all_transactions(),
							'exam_number'=>"Exam #" . $this->bed_model->current_exam($result2->year_level, $result2->levels_id),
							'year_start_date'=>$current_academic_terms_obj->year_start_date,
							// 'due'=>($this->bed_model->current_exam($result2->year_level, $result2->levels_id) == count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id))) ?
							// 	$this->basic_ed_balances->payable_for_exam($this->bed_model->current_exam($result2->year_level, $result2->levels_id)) :
							// 	ceil($this->basic_ed_balances->payable_for_exam($this->bed_model->current_exam($result2->year_level, $result2->levels_id))),
							'due' => $this->basic_ed_balances->due_this_exam(count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id)), $this->bed_model->current_exam($result2->year_level, $result2->levels_id)),
							'can_print_statement'=>TRUE,
							'error'=>$this->basic_ed_balances->error,
							'error_message'=>$this->basic_ed_balances->error_message,
					),
					'basic_ledger', ($tab=='basic_ledger'?TRUE:FALSE));



			$payments = $this->Payments_Model->ListStudentPayments($result2->payers_id);

			$this->tab_lib->enqueue_tab ('Payments', 'student/list_student_payments2', array('payments_info'=>$payments), 'payments', $tab=='payments',FALSE);

			//the following process privileges
			$current_sy = $this->Academic_terms_model->current_academic_year();
			$current_term = $this->Academic_terms_model->current_academic_term();
			$history_id = $this->Basic_Ed_Enrollments_Model->getHistoryInfo($idnum, $current_sy->academic_years_id);
			//@FIXME: what if $history_id does not get any result?


			if ($result->bed_status == 'active'){

					$assessment = $this->Basic_Ed_Enrollments_Model->checkIfAssessed($history_id->id);
					$privileges = $this->Scholarship_Model->ListPrivilegesNotTaken($history_id->id, 1);

					//print_r($privileges); die();
					$privilege_availed = $this->student_model->Check_if_with_Privilege($history_id->id, 1);
					$other_privileges = $this->Accounts_model->ListAllFeesGroups();
					$tuition_fee = $this->Basic_Ed_Enrollments_Model->getMyTuitionFee($current_sy->academic_years_id, $history_id->levels_id, $history_id->yr_level);
					if($tuition_fee)
						$tuition_fee = $tuition_fee->rate;
					else
						$tuition_fee = 0;

					$this->tab_lib->enqueue_tab('Privileges', '', array(), 'privileges', FALSE);

					$this->tab_lib->enqueue_tab ("Assign", "accounts/form_to_assign_privileges_bed",
							array(
									'student_idno'=>$idnum,
									'sy'=> $current_sy->sy,
									'history_id'=>$history_id->id,
									'privilege_items'=>$privileges,
									'other_privileges'=>$other_privileges,
									'privilege_availed'=>$privilege_availed,
									'tuition_fee'=>$tuition_fee,
									'assessed'=> $assessment,
								),
							"form_to_assign_privileges_bed", FALSE, 'privileges');


					//lists unposted privilegs
					$unpostedPrivileges = $this->Scholarship_Model->ListUnpostedPrivileges($history_id->id, 1);
					$list_tuition_privileges_availed = $this->Scholarship_Model->ListPrivilegesWdoutCash($history_id->id, 1);
					$total_tuition_disc = 0;
					$tuition_for_cash_discount = $tuition_fee;
					if($list_tuition_privileges_availed){
						foreach($list_tuition_privileges_availed as $tuition_discount){
							$total_tuition_disc += $tuition_discount->discount_amount;
						}
						$tuition_for_cash_discount = $tuition_fee - $total_tuition_disc;
					}

					//print($tuition_fee); die();
					$this->tab_lib->enqueue_tab ("List Unposted Privileges", "accounts/list_unposted_privileges",
							array(
									'privileges'=>$unpostedPrivileges,
									'tuition_discount'=>$tuition_fee,
									'tuition_for_cash_discount'=>$tuition_for_cash_discount
								),
							"list_unposted_privileges", FALSE, 'privileges');


					//assigns cash discount
					$cash_privileges_added = $this->Scholarship_Model->LisAddedCashDiscount($history_id->id);
					if($cash_privileges_added){
						$withCashPriv = TRUE;
					}else{
						$withCashPriv = FALSE;
					}

					$this->tab_lib->enqueue_tab ("Cash Discount", "accounts/form_to_input_cash_discount_bed",
							array(
									'privileges'=>$list_tuition_privileges_availed,
									'student_histories_id'=> $history_id->id,
									'assessment'=>$assessment,
									'total_tuition_discount'=>$total_tuition_disc,
									'tuition_fee'=>$tuition_fee,
									'tuition_for_cash_discount'=>$tuition_for_cash_discount,
									'withCashPriv'=>$withCashPriv,
									'sy'=>$current_sy->sy
								),
							"form_to_input_cash_discount_bed", FALSE, 'privileges');


					$past_privileges_availed = $this->Scholarship_Model->ListPastPrivileges($idnum, 1);
					//print_r($past_privileges_availed); die();
					if($past_privileges_availed){
						foreach($past_privileges_availed as $past_privilege){
							$past_privileges_details = $this->Scholarship_Model->getPastPrivilegesDetails($past_privilege->id);
						}
					}


					$this->tab_lib->enqueue_tab ("View Past Privileges", "accounts/list_past_privileges_bed",
								array('past_privileges' => $past_privileges_availed),
								'list_past_privileges', FALSE, 'privileges');


					if($show_invoice){
						$this->content_lib->enqueue_body_content('print_templates/invoice',
								array(
									'div_id'=>'statement',
									'idnumber'=>$idnum,
									'name'=>($result->lname.", ".$result->fname." ".substr($result->mname,0,1)),
									'course_year'=>$result->year_level." ".$result->abbreviation." ".$result->section,
									'period'=>'Exam #' . $this->bed_model->current_exam($result2->year_level, $result2->levels_id),
									// 'payable'=>($this->bed_model->current_exam($result2->year_level, $result2->levels_id) == count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id))) ?
									// 		$this->basic_ed_balances->payable_for_exam($this->bed_model->current_exam($result2->year_level, $result2->levels_id)) :
									// 		ceil($this->basic_ed_balances->payable_for_exam($this->bed_model->current_exam($result2->year_level, $result2->levels_id))),
									'payable' => $this->basic_ed_balances->due_this_exam(count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id)), $this->bed_model->current_exam($result2->year_level, $result2->levels_id)),
									'ledger_data'=>$this->basic_ed_balances->current_year_transactions() )
								);

						$this->content_lib->enqueue_body_content('print_templates/student_clearance',
								array(
										'ledger_data'=>$this->basic_ed_balances->current_year_transactions(),
										'idnumber'=>$idnum,
										'semester_start_date'=>$current_academic_terms_obj->year_start_date,
										'name'=>($result->lname.", ".$result->fname." ".substr($result->mname,0,1)),
										'course_year'=>$result->year_level." ".$result->abbreviation." ".$result->section,
										// 'payable'=>$this->basic_ed_balances->debit_balance(),
										'payable' => $this->basic_ed_balances->due_this_exam(count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id)), count($this->bed_model->exam_dates($result2->year_level, $result2->levels_id))),
										'payment_period'=>'Finals',
										'clearance'=>array('Library', 'Bookstore', 'Principal', 'Accounts Clerk'),
										'basic_ed'=>TRUE,
										'lab_courses'=>array() ),
								TRUE);
					}


			} //endof if($result->bed_status == 'active')


			$this->content_lib->enqueue_body_content("", $this->tab_lib->content());

			$this->content_lib->content();

		} //endof if ($result !== FALSE)

		echo sprintf($this->config->item('jzebra_applet'), base_url());

	}


	public function fees_schedule (){
		$this->load->library('tab_lib');
		$this->load->model('accounts/accounts_model');
		$this->load->model('hnumis/academicyears_model');

		if($action = $this->input->post('action')){
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				$this->load->model('accounts/accounts_model');
				switch ($action){
					case 'new_group' :
								if ($this->accounts_model->insert_new_group(sanitize_text_field($this->input->post('description'))))
									$this->content_lib->set_message('Successfully added new fees group. ', 'alert-success'); else
									$this->content_lib->set_message('Error(s) encountered while inserting new fees group.', 'alert-error');
								break;
					case 'new_subgroup' :
								if ($this->accounts_model->insert_new_subgroup(sanitize_text_field($this->input->post('parent_id')), sanitize_text_field($this->input->post('description'))))
									$this->content_lib->set_message('Successfully added new fees subgroup. ', 'alert-success'); else
									$this->content_lib->set_message('Error(s) encountered while inserting new fees subgroup.', 'alert-error');
								break;
					case 'show_fees_schedule' :
								$fees_schedule = $this->accounts_model->fetch_fees_given_group($this->academicyears_model->current_academic_year()->id, $this->input->post('acad_key'));
								break;
					case 'show_lab_fees' :
								$lab_fees = array();//$this->accounts_model->fetch_fees_given_group($this->academicyears_model->current_academic_year()->id, $this->input->post('acad_key'));
								break;
					default:
								break;
				}
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'),'alert-error');
			}
		}

		if (isset($fees_schedule)) {
			$this->load->model('hnumis/academicyears_model');
			$current_academic_year = $this->academicyears_model->current_academic_year();
			$this->content_lib->enqueue_body_content('accounts/fees_schedule_edit_form',
						array(
								'current_academic_year'=>$current_academic_year,
								'fees'=>$fees_schedule,
								'academic_programs_group'=>$this->input->post('acad_name')
						));
		}

		if (isset($lab_fees)) {
			//$this->load->model('hnumis/academicyears_model');
			//$current_academic_year = $this->academicyears_model->current_academic_year();
			$this->content_lib->enqueue_body_content('accounts/laboratory_fees',
						array());
		} else {
			$groups = $this->accounts_model->all_fees_groups_subgroups();
			$fees_schedule = $this->accounts_model->fetch_fees_given_group($this->academicyears_model->current_academic_year()->id, 22);
			$college = $this->accounts_model->academic_programs_group(6);
			$graduate_school = $this->accounts_model->academic_programs_group(7);
			$this->tab_lib->enqueue_tab('Academic Programs Group', 'accounts/levels_table', array('college'=>$college,'graduate_school'=>$graduate_school), 'levels_table', TRUE);
			$this->tab_lib->enqueue_tab('Fees Schedule Form', 'accounts/fees_schedule_form', array(), 'fees_schedule_form', FALSE);
			$this->tab_lib->enqueue_tab('Fees Schedule', 'accounts/fees_schedule', array('fees'=>$fees_schedule), 'fees_schedule', FALSE);
			$this->tab_lib->enqueue_tab('Fees Schedule Edit', 'accounts/fees_schedule_edit_form', array('fees'=>$fees_schedule), 'fees_schedule_edit_form', FALSE);
			$this->tab_lib->enqueue_tab('Fees Group', 'accounts/fees_group', array('groups'=>$groups), 'fees_group', FALSE);
			$this->tab_lib->enqueue_tab('Levels', 'accounts/levels_table', array(), 'levels', FALSE);

			$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		}
		$this->content_lib->content();
	}

   //this function calculates the tuition fee for discount
	private function tuitionDiscount($tuitionB, $tuitionO, $paying_unitsB, $paying_unitsO, $year_level, $maxUnitsDisc, $academic_program_info, $privilege_units_basic, $privilege_units_others, $priv_id){
		//print_r($maxUnitsDisc); die();
		$total_units = $privilege_units_basic + $privilege_units_others;
		if($priv_id == 215){
			$tuitionBasic = $tuitionB;
			$tuitionOthers = $tuitionO;
			$paying_units = $total_units;
		}else{
			if($tuitionB){
				$rateB = $tuitionB/$paying_unitsB;
				//print($rateB); die();
			}else{
				$rateB = 0;
			}

			if($paying_unitsO > 0){
				$rateO = $tuitionO/$paying_unitsO;
			}else{
			    $rateO = 0;
			}

			//print($maxUnitsDisc->max_unit_undergrad); die();
			if($year_level < $academic_program_info->max_yr_level){
				if($maxUnitsDisc->max_unit_undergrad){
					if($total_units < $maxUnitsDisc->max_unit_undergrad){
						   $tuitionBasic = $privilege_units_basic * $rateB;
						   $tuitionOthers = $privilege_units_others * $rateO;
						   $paying_units = $total_units;
			 	    }else{
						  if($privilege_units_others){
							 $tuitionOthers = $privilege_units_others * $rateO;
							 $tuitionBasic = ($maxUnitsDisc->max_unit_undergrad - $privilege_units_others) * $rateB;
							 $paying_units = $maxUnitsDisc->max_unit_undergrad;
							 //print("privilege units others->".$privilege_units_others."<br>rate others->".$rateO."<br> Tution->". $tuitionBasic); die();
						  }else{
							 $tuitionOthers = 0;
							 $tuitionBasic = $maxUnitsDisc->max_unit_undergrad * $rateB;
							 $paying_units = $maxUnitsDisc->max_unit_undergrad;
						  }
					}
				}else{
					$tuitionBasic = $privilege_units_basic * $rateB;
					$tuitionOthers = $privilege_units_others * $rateO;
					$paying_units = $total_units;


				}

		    }else{
			    if($maxUnitsDisc->max_unit_grad){
			    	if($total_units < $maxUnitsDisc->max_unit_grad){
						 $tuitionBasic = $privilege_units_basic * $rateB;
						 $tuitionOthers = $privilege_units_others * $rateO;
						 $paying_units = $total_units;
				   }else{
				   		 if($privilege_units_others){
				   		 	 $tuitionOthers = $privilege_units_others * $rateO;
							 $tuitionBasic = ($maxUnitsDisc->max_unit_grad - $privilege_units_others) * $rateB;
							 $paying_units = $maxUnitsDisc->max_unit_grad;
						  }else{
							 $tuitionOthers = 0;
							 $tuitionBasic = $maxUnitsDisc->max_unit_grad * $rateB;
							 $paying_units = $maxUnitsDisc->max_unit_grad;
						  }
				  }
			    }else{
			    	$tuitionBasic = $privilege_units_basic * $rateB;
					$tuitionOthers = $privilege_units_others * $rateO;
					$paying_units = $total_units;

			    }
			}
		}
	//	print($tuitionBasic); die();

		$totalTuition = $tuitionBasic + $tuitionOthers;
		return array( $totalTuition, $paying_units) ;

	}

	//Added by Justing...
	public function payors ($account_number=0){
		//echo $account_number; die();
		$this->load->model('accounts/payors_model', 'payors');
		$this->load->library('table_lib');

		if (is_numeric($account_number) && $account_number!=0){
			if($data['account'] = $this->payors->account_details($account_number))
				$this->content_lib->enqueue_body_content('accounts/account_details', $data);
			else {
				$this->content_lib->set_message('This account is non-existent', 'alert-error');
				$data = array();
				$data['data'] = $this->payors->all_non_student_payors();
				$this->content_lib->enqueue_body_content('accounts/accounts_list_table', $data);
			}
		} else {
			$data['data'] = $this->payors->all_non_student_payors();
			$this->content_lib->enqueue_body_content('accounts/accounts_list_table', $data);
		}
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->content_lib->content();
	}

	public function new_payors(){
		if ($action = $this->input->post('action')){
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				switch ($action){
					case 'add_account' :
						$this->load->model('accounts/payors_model', 'payors');
						switch ($this->input->post('account_type')){
							case 'offices' :
								$table = 'offices';
								$payor_details = array('office'=>sanitize_text_field($this->input->post('account_name')));
								break;
							case 'other_payers_tenant' :
								$table = 'other_payers';
								$payor_details = array('name'=>sanitize_text_field($this->input->post('account_name')), 'tenant'=>'Y',);
								break;
							case 'other_payers_non_tenant' :
								$table = 'other_payers';
								$payor_details = array('name'=>sanitize_text_field($this->input->post('account_name')), 'tenant'=>'N',);
								break;
						}
						if ($this->payors->add_new_payor($table, $payor_details)){
							$this->content_lib->set_message('Payer Added to Database', 'alert-success');
						} else {
							$this->content_lib->set_message('Error adding payer to Database', 'alert-error');
						}
						break;
					default:
						break;
				}
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}
		}

		$this->load->library('form_lib');
		$this->content_lib->enqueue_body_content('accounts/add_payers_form');
		$this->content_lib->content();
	}

//Added: 6/6/2013

	public function privileges(){

   		$this->load->library('accounts_lib');

   		$this->accounts_lib->privileges();

   }

	public function new_privilege(){

		 $this->content_lib->enqueue_body_content('accounts/form_to_add_new_privilege');

		 //Load models here
		 $this->load->model('financials/Scholarship_Model');
		 $this->load->model('Academic_terms_model');

		 switch ($action = $this->input->post('action')){
		 	case 'new_privilege':

		 		if ($this->common->nonce_is_valid($this->input->post('nonce'))){
		 			$data['scholarship_code'] = sanitize_text_field($this->input->post('pcode'));
		 			$data['scholarship'] = sanitize_text_field($this->input->post('privilege'));

		 			if($this->input->post('max_under') == 0){
		 				$data['max_under'] = NULL;
		 			} else{
		 				$data['max_under'] = sanitize_text_field($this->input->post('max_under'));
		 			}

		 			if($this->input->post('max_grad') == 0){
		 				$data['max_grad'] = NULL;
		 			} else{
		 				$data['max_grad'] = sanitize_text_field($this->input->post('max_grad'));
		 			}


		 			if($data['scholarship_code'] AND $data['scholarship']){
		 				$privilege_id = $this->Scholarship_Model->AddScholarshipItem($data);

		 			}else{
		 				$privilege_id = NULL;
		 			}

		 			if($privilege_id){
		 				$this->content_lib->set_message("Privilege Successfully Added!", "alert-success");
		 				//$this->index();
		 			} else {
		 				$this->content_lib->set_message("Error found while adding a privilege!", "alert-error");
		 				//$this->index();
		 			}

		 		} else {
		 				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
		 				//$this->index();
		 		}

		 		break;


		 }
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->content_lib->content();
	}

	public function list_privileges(){

	   	$this->load->model('financials/Scholarship_Model');

	   	$action = ($this->input->post('action') ?  $this->input->post('action') : 'list_privilege');
		//print($action);
		//die();
		switch ($action){

			case 'list_privilege':

				$data['scholarships'] = $this->Scholarship_Model->ListPrivileges();
				$this->content_lib->enqueue_body_content('accounts/list_privileges', $data);
				break;

			case 'edit_privilege_form':

				if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					$data['scholarship_id'] = sanitize_text_field($this->input->post('priv_id'));

				$scholarshipInfo = 	$this->Scholarship_Model->getScholarshipInfo($data['scholarship_id']);
				$data['scholarship'] = $scholarshipInfo->scholarship;
				$data['scholarship_code'] = $scholarshipInfo->scholarship_code;
				$data['max_grad'] = $scholarshipInfo->max_unit_grad;
				$data['max_under'] = $scholarshipInfo->max_unit_undergrad;

				$this->content_lib->enqueue_body_content('accounts/form_to_edit_scholarship', $data);

				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					//$this->index();
				}

				break;

			case 'edit_privilege':

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$scholarship_id = sanitize_text_field($this->input->post('scholarship_id'));
						$scholarship_code = sanitize_text_field($this->input->post('scholarship_code'));
						$scholarship = sanitize_text_field($this->input->post('scholarship'));
						if($this->input->post('max_grad') == 0){
							$max_grad = NULL ;
						}else{
							$max_grad = sanitize_text_field($this->input->post('max_grad'));
						}

						if($this->input->post('max_undergrad') == 0){
							$max_undergrad = NULL ;
						}else{
							$max_undergrad = sanitize_text_field($this->input->post('max_undergrad'));
						}

						$status = $this->Scholarship_Model->updateScholarship($scholarship_id, $scholarship_code, $scholarship, $max_grad, $max_undergrad);

						if($status){
							$this->content_lib->set_message("Scholarship Successfully updated!", "alert-success");
							$data['scholarships'] = $this->Scholarship_Model->ListPrivileges();
							$this->content_lib->enqueue_body_content('accounts/list_privileges', $data);
						} else {
							$this->content_lib->set_message("Error found while updating a scholarship!", "alert-error");
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');

					}

					break;

		   case 'form_to_delete_stud_privilege':

				if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$data['scholarship_id'] = sanitize_text_field($this->input->post('priv_id'));

				$scholarshipInfo = 	$this->Scholarship_Model->getScholarshipInfo($data['scholarship_id']);
				$data['scholarship'] = $scholarshipInfo->scholarship;
				$data['scholarship_code'] = $scholarshipInfo->scholarship_code;

				$this->content_lib->enqueue_body_content('accounts/form_to_delete_scholarship', $data);

				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					//$this->index();
				}

						break;

			case 'delete_stud_privilege':

					if ($this->common->nonce_is_valid($this->input->post('nonce'))){
						$privilege_id = sanitize_text_field($this->input->post('scholarship_id'));

						$status = $this->Scholarship_Model->deletescholarship($privilege_id);
						if($status){
							$this->content_lib->set_message("Scholarship Successfully deleted!", "alert-success");
							$data['scholarships'] = $this->Scholarship_Model->ListPrivileges();
							$this->content_lib->enqueue_body_content('accounts/list_privileges', $data);
						} else {
							$this->content_lib->set_message("Error found while deleting a scholarship!", "alert-error");
						}

					} else {
						$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
						$this->index();
					}

					break;


		}

		// $this->content_lib->enqueue_body_content('accounts/form_to_select_privileges', $data);

		//$this->content_lib->enqueue_footer_script('data_tables');
		$this->content_lib->content();
	}

	function generate_scholars_list_pdf($scholarship_id) {

		$this->load->model("hnumis/student_model");
		$this->load->model('hnumis/AcademicYears_model');
		$this->load->model('hnumis/Offerings_model');


		$current_term = $current_term = $this->Academic_terms_model->current_academic_term();
		$current_term_id = $current_term->id;
		$with_privileges = $this->Scholarship_Model->ListStudentsAvailed($scholarship_id,$current_term->id);
		$privilege_info = $this->Scholarship_Model->getScholarshipInfo($scholarship_id);
		$term = $current_term;


		$this->load->library('my_pdf');
		$this->load->library('hnumis_pdf');
		$l = 4;

		$this->hnumis_pdf->AliasNbPages();
		$this->hnumis_pdf->set_HeaderTitle('LIST OF SCHOLARS');
		$this->hnumis_pdf->AddPage('P','letter');

		$this->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->hnumis_pdf->SetDrawColor(165,165,165);

		$this->hnumis_pdf->SetFont('times','',10);
		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Ln();

		$this->hnumis_pdf->Cell(30,$l,'School Year',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': '.$term->term.' '.$term->sy,0,0,'L',true);
		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->Cell(30,$l,'Scholarship',0,0,'L',true);
		$this->hnumis_pdf->Cell(100,$l,': '.$privilege_info->scholarship,0,0,'L',true);
		$this->hnumis_pdf->Ln();


		$this->hnumis_pdf->Ln();
		$this->hnumis_pdf->SetFillColor(116,116,116);
		$this->hnumis_pdf->SetTextColor(253,253,253);
		$this->hnumis_pdf->SetX(15);

		$header1 = array('','ID No.','Name of Student','Course/Year','Discount %','Discount');

		$w=array(10,20,70,35,20,25);

		for($i=0;$i<count($w);$i++)
			$this->hnumis_pdf->Cell($w[$i],6,$header1[$i],1,0,'C',true);

		$this->hnumis_pdf->SetTextColor(10,10,10);
		$this->hnumis_pdf->SetFillColor(255,255,255);
		$this->hnumis_pdf->Ln();

		$num = 1;
		foreach($with_privileges AS $student) {
			$this->hnumis_pdf->SetX(15);
			$this->hnumis_pdf->Cell($w[0],$l,$num,1,0,'C',true);
			$this->hnumis_pdf->Cell($w[1],$l,$student->students_idno,1,0,'C',true);
			$this->hnumis_pdf->Cell($w[2],$l,$student->lname.', '.$student->fname,1,0,'L',true);
			$this->hnumis_pdf->Cell($w[3],$l,$student->abbreviation.'-'.$student->year_level,1,0,'L',true);
			$this->hnumis_pdf->Cell($w[4],$l,($student->discount_percentage * 100).'%',1,0,'C',true);
			$this->hnumis_pdf->Cell($w[5],$l,number_format($student->discount_amount,2),1,0,'R',true);

			$this->hnumis_pdf->Ln();
			$num++;
		}

			$this->hnumis_pdf->Output();
	}


	/*
	 * ADDED: 6/26/15 by genes
	 */
	public function fees_category() {
		$this->load->model('accounts/fees_schedule_model');


		if ($this->common->nonce_is_valid($this->input->post('nonce'))){

			$fees_groups = $this->fees_schedule_model->get_feesgroups();
			$weight = $this->fees_schedule_model->get_last_weight();

			$action = ($this->input->post('action') ?  $this->input->post('action') : 'mainpage');

			switch ($action) {
				case 'mainpage':

					$fees_subgroups = $this->fees_schedule_model->ListFeesSubGroups($fees_groups[0]->id);

					$data = array(
							"fees_groups"=>$fees_groups,
							"selected_fee_group"=>$fees_groups[0]->id,
							"fees_subgroups"=>$fees_subgroups,
							"weight"=>$weight,
						);

					break;

				case 'change_pulldown':

					$fees_subgroups = $this->fees_schedule_model->ListFeesSubGroups($this->input->post('fees_groups_id'));

					$data = array(
							"fees_groups"=>$fees_groups,
							"selected_fee_group"=>$this->input->post('fees_groups_id'),
							"fees_subgroups"=>$fees_subgroups,
							"weight"=>$weight,
						);

					break;

				case 'add_fees_group':

					$data['fees_group'] = $this->input->post('fees_group');
					$data['weight'] = $this->input->post('weight') + 1;

					if ($this->fees_schedule_model->Add_Fees_Group($data)) {
						$this->content_lib->set_message('Fees Groups successfully added!', 'alert-success');
					} else {
						$this->content_lib->set_message('ERROR: Fees Groups not added!', 'alert-error');
					}

					$fees_groups = $this->fees_schedule_model->get_feesgroups();
					$fees_subgroups = $this->fees_schedule_model->ListFeesSubGroups($fees_groups[0]->id);

					$data = array(
							"fees_groups"=>$fees_groups,
							"selected_fee_group"=>$fees_groups[0]->id,
							"fees_subgroups"=>$fees_subgroups,
							"weight"=>$weight,
					);

					break;

			case 'add_fees_subgroup':

				$data['description'] = $this->input->post('description');
				$data['remark'] = $this->input->post('remark');
				$data['fees_groups_id'] = $this->input->post('fees_groups_id');

				if ($this->fees_schedule_model->Add_Fees_SubGroup($data)) {
					$this->content_lib->set_message('Fees Groups item successfully added!', 'alert-success');
				} else {
					$this->content_lib->set_message('ERROR: Fees Groups item not added!', 'alert-error');
				}

				$fees_groups = $this->fees_schedule_model->get_feesgroups();
				$fees_subgroups = $this->fees_schedule_model->ListFeesSubGroups($this->input->post('fees_groups_id'));

				$data = array(
						"fees_groups"=>$fees_groups,
						"selected_fee_group"=>$this->input->post('fees_groups_id'),
						"fees_subgroups"=>$fees_subgroups,
						"weight"=>$weight,
				);

				break;

			case 'delete_subgroup':

				if ($this->fees_schedule_model->Delete_SubGroup($this->input->post('fees_subgroups_id'))) {
					$this->content_lib->set_message('Fees Groups item successfully deleted!', 'alert-success');
				} else {
					$this->content_lib->set_message('ERROR: Fees Groups item not deleted!', 'alert-error');
				}

				$fees_subgroups = $this->fees_schedule_model->ListFeesSubGroups($this->input->post('fees_groups_id'));

				$data = array(
						"fees_groups"=>$fees_groups,
						"selected_fee_group"=>$this->input->post('fees_groups_id'),
						"fees_subgroups"=>$fees_subgroups,
						"weight"=>$weight,
				);

				break;
			}

			$this->content_lib->enqueue_body_content('accounts/fees_schedule/fees_category', $data);
			$this->content_lib->content();

		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			$this->index();
		}

	}



	public function fees_schedule_college(){
		$this->load->model('accounts/fees_schedule_model');
		$this->load->model('hnumis/College_model');
		$this->load->model('hnumis/Academicyears_model');
		$this->load->model('hnumis/Programs_model');
		$this->load->model('academic_terms_model');
		$this->load->helper('url');

		$school_years = $this->fees_schedule_model->get_school_years();
		$fees_groups = $this->fees_schedule_model->get_feesgroups();

		if ($this->common->nonce_is_valid($this->input->post('nonce'))){

			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			$selected_year = ($this->input->post('selected_year') ? $this->input->post('selected_year') : $school_years[0]->id);

			switch ($step) {
				case 1:
					//log_message("INFO", print_r($selected_year, true)); // Toyet 5.11.2018
					$data = array(
								"fees_groups"=>$fees_groups,
								"school_years"=>$school_years,
								"fees_subgroups"=>$this->fees_schedule_model->ListFeesSubGroups($fees_groups[0]->id),
								"colleges"=>$this->College_model->all_colleges(),
								"terms"=>$this->academic_terms_model->ListAcademicTerms($selected_year),
								"selected_year"=>$selected_year,
						);

					$this->content_lib->enqueue_body_content('accounts/fees_schedule/fees_schedule_form',$data);
					$this->content_lib->content();

					break;

				case 'change_academic_year':

					$data = array(
								"fees_groups"=>$fees_groups,
								"school_years"=>$school_years,
								"fees_subgroups"=>$this->fees_schedule_model->ListFeesSubGroups($fees_groups[0]->id),
								"colleges"=>$this->College_model->all_colleges(),
								"terms"=>$this->academic_terms_model->ListAcademicTerms($this->input->post('academic_year_id')),
					);

					$my_return['output'] = $this->load->view('shs/fees_schedule/list_terms', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 2:
					$academic_year = $this->Academicyears_model->GetAcademicYear($this->input->post('academic_year_id'));
					//print_r($this->input->post('programs')); die();
					$data = array(
								"academic_year"=>$academic_year->sy,
								"terms"=>$this->input->post('semester'),
								"fee_category"=>$this->input->post('fees_group_id'),
								"fees_subgroup"=>$this->fees_schedule_model->getFeesSubGroup($this->input->post('fees_subgroups')),
								"programs"=>$this->input->post('programs'),
								"year_levels"=>$this->input->post('yr_level'),
								"rate"=>$this->input->post('rate'),
								);
					$this->content_lib->enqueue_body_content('accounts/fees_schedule/review_fees_schedule_form',$data);
					$this->content_lib->content();

					break;
				case 3:
					$acad_program_groups = json_decode($this->input->post('acad_program_groups'));
					$academic_terms = json_decode($this->input->post('academic_terms'));
					$year_levels = json_decode($this->input->post('year_levels'));
					//print_r($academic_terms); die();
					$errors = array();
					//print_r($acad_program_groups); die();
					foreach($acad_program_groups AS $k1=>$acad_program_group_id) {
						foreach($academic_terms AS $k2=>$academic_terms_id) {
							foreach($year_levels AS $k3=>$yr_level) {
								$data['fees_subgroups_id']      = $this->input->post('fees_subgroups_id');
								$data['acad_program_groups_id'] = $acad_program_group_id;
								$data['academic_terms_id']      = $academic_terms_id;
								$data['levels_id']              = 6;
								$data['yr_level']               = $yr_level;
								$data['rate']                   = $this->input->post('rate');
								$data['posted']                 = "Y";

								$status =  $this->fees_schedule_model->AddFeesSchedule($data);

								if ($status['error']) {
									$errors[] = $status;
									//break 3;
								}
							}
						}
					}

					if ($errors) {
						$msg = "Fees Schedule successfully created with errors on: ";
						foreach ($errors AS $k=>$v) {
							foreach ($v AS $k1=>$v1) {
								$msg .= $k1."==".$v1."<br>";
							}
						}
					} else {
						$msg = "Fees Schedule successfully created!";
					}
					$this->content_lib->set_message($msg, 'alert-success');
					$this->index();

					break;
					
				case 'search_fees_subgroups':
					
					$data['fees_subgroups'] = $this->fees_schedule_model->ListFeesSubGroups($this->input->post('fees_groups_id'));

					$my_return['output'] = $this->load->view('shs/fees_schedule/list_fees_subgroups', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
			}

		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			$this->index();
		}

	}


	public function fees_schedule_shs(){
		$this->load->library('shs/fees_schedule_lib');
	
		$this->fees_schedule_lib->create_fees_schedules_shs();

	}


	public function lab_fees_shs(){
		$this->load->library('shs/fees_schedule_lib');
	
		$this->fees_schedule_lib->lab_fees_shs();

	}

	
	public function schedule_for_shs(){
		$this->load->library('shs/fees_schedule_lib');
	
		$this->fees_schedule_lib->view_update_schedule_for_shs();

	}

	
	
	//ADDED: 8/24/13
	public function fees_schedule_basic_ed(){
		$this->load->model('accounts/fees_schedule_model');
		$this->load->model('hnumis/College_model');
		$this->load->model('hnumis/Academicyears_model');
		$this->load->model('basic_ed/rsclerk_model');
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('academic_terms_model');
		$this->load->helper('url');

		if ($this->common->nonce_is_valid($this->input->post('nonce'))){

			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);

			switch ($step) {
				case 1:

					$school_years = $this->fees_schedule_model->get_school_years();

					if ($this->input->post('academic_year_id')) {
						$data = array(
								"fees_groups"=>$this->fees_schedule_model->get_feesgroups(),
								"school_years"=>$school_years,
								"fees_subgroups"=>$this->fees_schedule_model->ListFeesSubGroups(),
								"basic_levels"=>$this->basic_ed_sections_model->ListBasicLevels(),
								"selected_year"=>$this->input->post('academic_year_id'),
						);

					} else {
						$data = array(
								"fees_groups"=>$this->fees_schedule_model->get_feesgroups(),
								"school_years"=>$school_years,
								"fees_subgroups"=>$this->fees_schedule_model->ListFeesSubGroups(),
								"basic_levels"=>$this->basic_ed_sections_model->ListBasicLevels(),
								"selected_year"=>$school_years[0]->id,
						);
					}

					$this->content_lib->enqueue_body_content('accounts/fees_schedule/fees_schedule_basic_ed_form2',$data);
					$this->content_lib->content();

					break;
				case 2:
					$academic_year = $this->Academicyears_model->GetAcademicYear($this->input->post('academic_year_id'));
					switch ($this->input->post('basic_levels')) {
						/*case '2':
							$grade_levels = $this->input->post('k');
							break;
						*/
						case '11':
							$grade_levels = Array(1=>'Preschool 1');
							break;

						case '1':
							$grade_levels = Array(1=>'Kindergarten 1');;
							break;

						case '3':
							$grade_levels = $this->input->post('gs');
							break;
						case '4':
							$grade_levels = $this->input->post('hs_d');
							break;
						case '12':
							$grade_levels = $this->input->post('senior');
							break;
						/*case '5':
							$grade_levels = $this->input->post('hs_n');
							break;
						*/
					}
					//print_r($grade_levels); die();
					$terms=$this->academic_terms_model->ListAcademicTerms($this->input->post('academic_year_id'));

					$data = array(
							"academic_year"=>$academic_year->sy,
							"term"=>$terms[0]->id,
							"fee_category"=>$this->input->post('fees_group_id'),
							"fees_subgroup"=>$this->fees_schedule_model->getFeesSubGroup($this->input->post('fees_subgroups')),
							"basic_level"=>$this->rsclerk_model->getBasicEdLevel($this->input->post('basic_levels')),
							"grade_levels"=>$grade_levels,
							"rate"=>$this->input->post('rate'),
					);
					$this->content_lib->enqueue_body_content('accounts/fees_schedule/review_fees_schedule_basic_ed_form',$data);
					$this->content_lib->content();

					break;
				case 3:
					$grade_levels = json_decode($this->input->post('grade_levels'));
					$errors = array();
					$this->db->trans_start();
					foreach($grade_levels AS $k3=>$v) {
						$data['fees_subgroups_id']      = $this->input->post('fees_subgroups_id');
						$data['academic_terms_id']      = $this->input->post('academic_terms_id');
						$data['acad_program_groups_id'] = NULL;
						$data['levels_id']              = $this->input->post('levels_id');
						$data['yr_level']               = $k3;
						$data['rate']                   = $this->input->post('rate');
						$data['posted']                 = "Y";

						$status =  $this->fees_schedule_model->AddFeesSchedule($data,'BasicEd');

						if ($status['error']) {
							$errors[] = $status;
							//break;
						}

					}


					if ($errors) {
						$msg = "Fees Schedule created with errors on: ";
						foreach ($errors AS $k=>$v) {
							foreach ($v AS $k1=>$v1) {
								$msg .= $k1."==".$v1."<br>";
							}
						}
					} else {
						$msg = "Fees Schedule successfully created!";
					}
					$this->db->trans_complete();
					$this->content_lib->set_message($msg, 'alert-success');
					$this->index();

					break;
			}
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			$this->index();
		}

	}


	//ADDED: 8/20/13 by genes
	public function other_tuition_fees(){
		$this->load->model('accounts/fees_schedule_model');
		//$this->load->model('hnumis/College_model');
		$this->load->model('hnumis/Academicyears_model');
		//$this->load->model('hnumis/Programs_model');
		$this->load->model('hnumis/courses_model');
		$this->load->model('academic_terms_model');
		$this->load->helper('url');

		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);

		switch ($step) {
			case 1:

				$school_years = $this->fees_schedule_model->get_school_years();

				if ($this->input->post('academic_year_id')) {
					$terms = $this->academic_terms_model->ListAcademicTerms($this->input->post('academic_year_id'));
					$selected_year = $this->input->post('academic_year_id');

				} else {
					$terms = $this->academic_terms_model->ListAcademicTerms($school_years[0]->id);
					$selected_year = $school_years[0]->id;
				}

				if ($terms) {
					$selected_term = $terms[0]->id;
				} else {
					$selected_term ="";
				}
				$courses = $this->fees_schedule_model->ListCourses_not_in_tuition_others($selected_term);

				$data = array(
						"school_years"=>$school_years,
						"terms"=>$terms,
						"selected_year"=>$selected_year,
						"selected_term"=>$selected_term,
						"courses"=>$courses,
				);

				$this->content_lib->enqueue_body_content('accounts/fees_schedule/other_tuition_fees_form',$data);
				$this->content_lib->content();

				break;

			case 2:
				$data = array(
							"academic_year"=>$this->input->post('academic_year_id'),
							"terms"=>$this->input->post('semester'),
							"courses_id"=>$this->input->post('courses_id'),
							"year_levels"=>$this->input->post('yr_level'),
							"rate"=>$this->input->post('rate'),
					);
					$this->content_lib->enqueue_body_content('accounts/fees_schedule/review_other_tuition_fees_form',$data);
					$this->content_lib->content();

					break;

			case 3:
				$academic_terms = json_decode($this->input->post('academic_terms'));
				$year_levels = json_decode($this->input->post('year_levels'));

					foreach($academic_terms AS $k2=>$academic_terms_id) {
						foreach($year_levels AS $k3=>$yr_level) {
							$data['academic_terms_id'] = $academic_terms_id;
							$data['levels_id']         = 6;
							$data['yr_level']          = $yr_level;
							$data['courses_id']        = $this->input->post('courses_id');
							$data['rate']              = $this->input->post('rate');
							$data['posted']            = "Y";

							$this->fees_schedule_model->AddOther_Tuition_Fees($data);
							//FIXME: No error trapping here. what if insertion will fail? this will still display "fees created" message.

						}
					}

				$this->content_lib->set_message("Other Tuition Fees created!", 'success');
				$this->index();

				break;
		}

	}


	//ADDED: 6/10/2015 by genes
	public function affiliation_fees(){
		$this->load->model('accounts/fees_schedule_model');
		$this->load->model('hnumis/Academicyears_model');
		$this->load->model('academic_terms_model');

		if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
			$action = ($this->input->post('action') ?  $this->input->post('action') : 'mainpage');

			switch ($action) {
				case 'mainpage':

					$school_years = $this->fees_schedule_model->get_school_years();

					if ($this->input->post('academic_year_id')) {
						$terms = $this->academic_terms_model->ListAcademicTerms($this->input->post('academic_year_id'));
						$selected_year = $this->input->post('academic_year_id');

					} else {
						$terms = $this->academic_terms_model->ListAcademicTerms($school_years[0]->id);
						$selected_year = $school_years[0]->id;
					}

					$data = array(
							"school_years"=>$school_years,
							"terms"=>$terms,
							"selected_year"=>$this->input->post('academic_year_id')
					);

					$this->content_lib->enqueue_body_content('accounts/fees_schedule/affiliation_fees_form',$data);
					$this->content_lib->content();

					break;

				case 'submit_fee':
					$academic_terms = json_decode($this->input->post('terms'));

					//print_r($academic_terms); die();
					foreach($academic_terms AS $acad_term) {
						$data['academic_terms_id'] = $acad_term->id;
						$data['description']       = $this->input->post('description');
						$data['rate']              = $this->input->post('rate');
						$data['posted']            = "Y";

						if (!$this->fees_schedule_model->AddAffiliation_Fees($data)) {
							$this->content_lib->set_message("ERROR: Affiliation Fee not added!", "alert-error");
							$this->affiliation_fees();
							return;
						}
					}

					$this->content_lib->set_message("Affiliation Fees created!", 'success');
					$this->index();

					break;
			}
		}else {
			$this->content_lib->set_message("Error in submitting form", 'alert-error');
	   	}

	}


	public function assign_affiliation_course(){
		$this->load->model('accounts/fees_schedule_model');
		$this->load->model('hnumis/academicyears_model');
		$this->load->model('hnumis/courses_model');
		$this->load->model('academic_terms_model');

		if ($this->common->nonce_is_valid($this->input->post('nonce'))) {

			$school_years = $this->academicyears_model->ListAcadYears();
			$courses = $this->courses_model->ListAllCourses();

			$action = ($this->input->post('action') ?  $this->input->post('action') : 'mainpage');

			//print($this->input->post('action')); die();
			switch ($action) {
				case 'mainpage':

					//$school_years = $this->fees_schedule_model->get_school_years();
					$affiliation_fees = $this->fees_schedule_model->ListAffiliationFees($school_years[0]->id);

					$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$school_years[0]->id,
							"affiliation_fees"=>$affiliation_fees,
							"courses"=>$courses
					);

					break;

				case 'change_sy':

					//$school_years = $this->fees_schedule_model->get_school_years();
					$affiliation_fees = $this->fees_schedule_model->ListAffiliationFees($this->input->post('academic_year_id'));

					$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$this->input->post('academic_year_id'),
							"affiliation_fees"=>$affiliation_fees,
							"courses"=>$courses
					);

					break;

				case 'submit_fee':

					$acad_terms = $this->academic_terms_model->ListAcademicTerms($this->input->post('academic_year_id'));
					$success_fees = array();

					foreach ($acad_terms AS $aterm) {
						foreach($this->input->post('a_fee') AS $k=>$v) {

							$affil = $this->fees_schedule_model->getAffilFees($v);

							$descs = $this->fees_schedule_model->ListAffilFees($this->input->post('academic_year_id'),$affil);

							foreach ($descs AS $affil_fee) {
								$data['courses_id'] = $this->input->post('courses_id');
								$data['fees_affiliation_id'] = $affil_fee->id;

								if ($this->fees_schedule_model->AssignAffiliation_to_Course($data)) {
									$success_fees[] = $affil_fee->description." - ".$affil_fee->rate;
									//$this->content_lib->set_message("ERROR: Assignment not added!", "alert-error");
									//$this->affiliation_fees();
									//return;
								}
							}
						}
					}

					$affiliation_fees = $this->fees_schedule_model->ListAffiliationFees($this->input->post('academic_year_id'));

					$data = array(
							"school_years"=>$school_years,
							"selected_year"=>$this->input->post('academic_year_id'),
							"affiliation_fees"=>$affiliation_fees,
							"courses"=>$courses
					);

					$msg = "The following rates are added: <br>";
					foreach ($success_fees AS $k=>$v) {
						$msg .= $v."<br>";
					}

					$this->content_lib->set_message($msg, 'success');

					break;
			}

			$this->content_lib->enqueue_body_content('accounts/fees_schedule/affiliation_to_course_assignment_form',$data);
			$this->content_lib->content();

		}else {
			$this->content_lib->set_message("Error in submitting form", 'alert-error');
		}

	}


	//ADDED: 8/17/13 by genes
	public function laboratory_fees(){
		$this->load->model('accounts/fees_schedule_model');
		$this->load->model('hnumis/Academicyears_model');
		$this->load->model('hnumis/courses_model');
		$this->load->helper('url');

		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);

		switch ($step) {
			case 1:

				$school_years = $this->fees_schedule_model->get_school_years();

				if ($this->input->post('academic_year_id')) {
					$data = array(
							"school_years"=>$school_years,
							"laboratories"=>$this->fees_schedule_model->ListLab_with_no_fees($this->input->post('academic_year_id')),
							"selected_year"=>$this->input->post('academic_year_id'),
					);

				} else {
					$data = array(
							"school_years"=>$school_years,
							"laboratories"=>$this->fees_schedule_model->ListLab_with_no_fees($school_years[0]->id),
							"selected_year"=>$school_years[0]->id,
					);
				}

				$this->content_lib->enqueue_body_content('accounts/fees_schedule/laboratory_fees_form',$data);
				$this->content_lib->content();

				break;
			case 2:
				$selected_labs="";
				foreach($this->input->post('laboratory') AS $k=>$v) {
					if ($v) {
						$lab = $this->courses_model->getCourseDetails($k);
						$selected_labs[] = array('id'=>$lab->id,'course_code'=>$lab->course_code,'rate'=>$v);
					}
				}
				$data = array(
						"academic_year"=>$this->Academicyears_model->GetAcademicYear($this->input->post('academic_year_id')),
						"laboratories"=>$selected_labs,
				);
				$this->content_lib->enqueue_body_content('accounts/fees_schedule/review_laboratory_fees_form',$data);
				$this->content_lib->content();

				break;
			case 3:
				$laboratories = json_decode($this->input->post('laboratories'));
				foreach ($laboratories AS $lab) {
					$data['academic_years_id'] = $this->input->post('academic_year');
					$data['levels_id']         = 6;
					$data['courses_id']        = $lab->id;
					$data['rate']              = $lab->rate;
					$data['posted']            = "Y";
					$this->fees_schedule_model->AddLaboratory_Fees($data);
					//FIXME: No error trapping here. what if insertion will fail? this will still display "successfully created" message.
				}

				$this->content_lib->set_message("Laboratory Fees successfully created!", 'success');
				$this->index();

				break;
		}
	}


	public function schedule_by_acad_program() {
		$this->load->library('accounts_lib');

		$this->accounts_lib->schedule_by_acad_program();

	}


	public function schedule_for_basic_ed() {
		$this->load->library('accounts_lib');

		$this->accounts_lib->schedule_for_basic_ed();

	}

	//Added: 9/14/2013

	public function list_privileges_availed($privilege_info, $privilege_details_info, $view){

	//	print_r($view);
	//	die();
		$data['total_disc_amount'] = $privilege_info->total_amount_availed;
		$data['privilege'] = $privilege_info->scholarship;
		$data['privileges_details'] = $privilege_details_info;
		$data['privilege_id'] = $privilege_info->id;
		foreach($privilege_details_info as $privilege){
			if($privilege->fees_group_id == 9){
				$data['tuition_percentage'] = $privilege->discount_percentage;
			}
		}

		if($view == 1){
			$this->content_lib->enqueue_body_content('accounts/view_unposted_privilege',$data);
		}else{
			$this->content_lib->enqueue_body_content('accounts/view_past_priv_detail',$data);
		}


	}

	public function mass_basic_ed_clearance() {
		if($this->input->post('section')){
			$this->load->model('hnumis/student_model'); //added by tatskie
			$this->load->library('print_lib');
			$this->load->model('basic_ed/basic_ed_enrollments_model', 'enrollments');
			$enrollees = $this->enrollments->current_enrollees($this->input->post('level'), $this->input->post('year_level'), $this->input->post('section'));

			$this->load->model('teller/teller_model');
			$this->load->model('hnumis/academic_terms_model');
			$this->load->model('hnumis/academicyears_model', 'AcademicYears_model');

			$current_acadYear = $this->AcademicYears_model->current_academic_year();
			$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
			//print_r($current_academic_terms_obj);die();
			$students = array();


			//$current_acadYear = $this->AcademicYears_model->current_academic_year();

			$this->load->model('basic_ed/basic_ed_model', 'bed_model');
			$this->load->library('basic_ed_balances');
			foreach($enrollees as $enrollee){
				$ledger_data = $this->teller_model->get_ledger_data($enrollee->payers_id);
				$unposted_payments = $this->teller_model->get_unposted_students_payment($enrollee->payers_id);
				//echo $this->bed_model->current_exam($result->year_level, $result->levels_id); die();
				//print_r($this->bed_model->exam_dates($result->year_level, $result->levels_id));
				$config = array(
						'ledger_data'			=> $ledger_data,
						'unposted_transactions'	=> $unposted_payments,
						'periods'				=> $this->bed_model->exam_dates($this->input->post('year_level'), $this->input->post('level')),
						'current_academic_year' => $this->bed_model->academic_year(),
						'bed_status' 			=> $enrollee->bed_status,
						'current_nth_exam'      => $this->bed_model->current_exam($this->input->post('year_level'), $this->input->post('level')),
						'total_number_of_exams' => count($this->bed_model->exam_dates($this->input->post('year_level'), $this->input->post('level'))),
				);
				//'current_period'		=> $this->bed_model->current_exam($result->year_level, $result->levels_id),
				$this->basic_ed_balances->__construct($config);
				if($this->basic_ed_balances->error == TRUE){
					$this->content_lib->set_message($this->basic_ed_balances->error_message, 'alert-error');
					$this->content_lib->content();
					return;
				}
				$students[$enrollee->idno] = (object)array(
						'basic_info'=>$enrollee,
						'ledger_data'=>$this->basic_ed_balances->current_year_transactions(),
						'exam_name'=>'Exam #' . $this->bed_model->current_exam($this->input->post('year_level'), $this->input->post('level')),
						'payable'=>$this->basic_ed_balances->payable_for_exam($this->bed_model->current_exam($this->input->post('year_level'), $this->input->post('level'))),
						);
			} //end of foreach

			//die();

			$exam_name = 'Exam #1';

			$clearance_content = $this->load->view('mass_printing_templates/basic_ed_clearance', array('students'=>$students, 'offices'=>array('Library', 'Bookstore', 'Principal', 'Accounts Clerk')), TRUE);
			$ass_content 	   = $this->load->view('mass_printing_templates/basic_ed_ass', array(
										'students' 	  => $students,
										'exam_number' => $exam_name,
										'year_start_date' => $current_academic_terms_obj->year_start_date,
										'current_acad_year_id'=>$current_acadYear->id,
										'due' 	=> 0 ,
										'can_print_statement'=>TRUE,
										'div_id'=>'statement',
								), TRUE);
			/** (($payable_exam1 > 0) ? $payable_exam1 : 0), **/

			//$this->content_lib->enqueue_body_content('accounts/mass_basic_ed', array('for_printing'=>$clearance_content,'for_printing2'=>$ass_content));
			$this->content_lib->enqueue_body_content('accounts/mass_basic_ed', array('for_printing'=>$ass_content));
		} else {
			$this->content_lib->enqueue_body_content('accounts/mass_basic_ed', array());
		}
		$this->content_lib->content();
		echo sprintf($this->config->item('jzebra_applet'), base_url());
	}


	function download_fees_schedule_to_csv() {
		$this->load->library('accounts_lib');

		$this->accounts_lib->download_fees_schedule_to_csv();
	}


	function download_fees_schedule_basic_ed_to_csv() {
		$this->load->library('accounts_lib');

		$this->accounts_lib->download_fees_schedule_basic_ed_to_csv();
	}

	public function send_mass_sms($recepients){
		$this->load->model('academic_terms_model');
		$this->load->model('accounts/sms_mass_sending_model', 'mass_sender');

		$this->content_lib->enqueue_body_content( 'accounts/smsbatch',
				array(
						'recepients'=>$recepients,
						'academic_terms_id'=>$this->academic_terms_model->current_academic_term()->id,
						'end_year'=>$this->academic_terms_model->current_academic_term()->end_year,
						'histories'=>$this->mass_sender->all_historical($recepients),
						)
				);
		$this->content_lib->content();
	}

	public function mass_sms (){
		//echo $this->input->post('action');
		$this->load->model('accounts/sms_mass_sending_model', 'mass_sender');
		switch ($this->input->post('action')){
			case 'init' :
				$academic_terms_id = $this->input->post('recepients') == 'college' ? $this->input->post('academic_terms_id') : null;
				$end_year = $this->input->post('recepients') == 'college' ? null : $this->input->post('end_year');
				if ( $historical_id = $this->mass_sender->new_historical($this->input->post('recepients'), $this->input->post('period'), $academic_terms_id, $end_year) ){
					$output = (object)array(
							'success'=>true,
							'historical_id'=>$historical_id,
							);
				} else {
					$output = (object)array(
							'success'=>false,
							'message'=>'Error found while adding new historical. That assessment/statement may have been sent already.');
				}
				break;
			case 'perform' :
				$historical_id = $this->input->post('historical_id');
				if ( $historical_info = $this->mass_sender->historical($historical_id)){
					if(isset($historical_info->period)){
						$type = strtolower($historical_info->period)=='assessment' ? 'assessment' : 'statement';
						$templates = $this->config->item('accounts_mass_sms_template');
						$template = $templates[$historical_info->recepients][$type];
						if($return = $this->mass_sender->process_mass_sending ($historical_id, $type, $historical_info->recepients, $template)){
							$this->load->library('sms_client', $this->config->item('sms_api_config'));
							if ( $this->sms_client->send_multiple_messages($return->messages) ){
								$this->mass_sender->mark_as_sent($return->bills_id_processed);
							}
							$status = $this->mass_sender->status($historical_id);
							if($status->unsent==0)
								$this->mass_sender->update_historical($historical_id, array('status'=>'done'));
							$output = (object)array(
									'success'=>TRUE,
									'percentage'=>$status->total==0 ? 100 : round($status->sent*100/$status->total),
									'done'=>$status->unsent==0
									);
						}
					}
				} else {
					$output = (object)array(
							'success'=>false,
							'message'=>'The mass sending specified is not known.');
				}
				break;
			case 'stop' :

				break;
		}
		$this->output->set_content_type('application/json')
			->set_output(json_encode($output));
	}

	public function mass_printing($what=''){
			switch($what){
				case 'basic_ed_clearances' :
					$this->content_lib->enqueue_body_content('accounts/mass_basic_ed', array(
						'title'=>'Basic Ed Clearances Mass Printing',
						'print_what'=>$what));
					break;
				case 'basic_ed_statements' :

					$this->content_lib->enqueue_body_content('accounts/mass_basic_ed', array(
						'title'=>'Basic Ed Statements Mass Printing',
						'print_what'=>$what));
					break;
			}
			$this->content_lib->content();
		}

	public function test(){
		$this->load->model('accounts/sms_mass_sending_model', 'mass_sender');
		$templates = $this->config->item('accounts_mass_sms_template');
		$template = $templates['college']['statement'];
		$result = $this->mass_sender->process_mass_sending( 35, 'statement', 'college', $template );
		print_r($result);
	}

/**** added here by RA July 14 2015 **/

	/*
	 * ADDED: 8/20/15 by genes
	 */
	public function privileges_basic_ed(){

		$this->load->library('accounts_lib');

		$this->accounts_lib->privileges_basic_ed();

	}


	function view_prospectus() {
		$this->load->library('accts_prospectus_lib');

		$this->accts_prospectus_lib->view_prospectus($this->session->userdata('college_id'));
	}

	
	public function process_student_action($idnum) {
		
		$this->load->library('shs/shs_student_lib');
	
		$this->shs_student_lib->process_student_action($idnum);
		
	}


	public function cash_receipts(){
		$this->load->library('accounts_lib');
		$this->accounts_lib->cash_receipts();

	}

}
