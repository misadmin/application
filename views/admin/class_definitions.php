<?php 
/*
$class = array(
		'name' => 'Admin',
		'filename' => 'admin.php',
		'description' => 'The God',
		'parent' => 'MY_Controller',
		'attributes' => array(
							array(
									'name'=>'id_number',
									'scope'=>'public',
									'description'=>'identification'
								),
							array(
									'name'=>'fire',
									'scope'=>'protected',
									'description'=>'secret'
							)
						),
		'methods' => array(
							array(
									'name'=>'get_id_number',
									'scope'=>'public',
									'description'=>'get_identification'
								),
							array(
									'name'=>'get_fire_number',
									'scope'=>'private',
									'description'=>'get_identification'
								)
							)
		);
*/
$class = $classes;
?>
    <h1 class="heading">
	    <form id="class_request" method="post" style="display:inline">
		    <input type="hidden" class="hidden" name="page" value="<?php echo isset($page) ? $page : ''?>"></input>
		    <input type="submit" class="btn btn-primary" value="Back" style="margin:0 0 2px"></input>
		</form>
		<?php echo $class['name']?>
	</h1>
    <p class="formSep"><strong>Filename: </strong><?php echo $class['filename']?></p>
    <p class="formSep"><strong>Description: </strong><?php echo $class['description']?></p>
    <p class="formSep"><strong>Parent: </strong><?php echo $class['parent']?></p>
    
   	<h2 class="heading">Attributes:</h2>
   	<?php if (count($class['attributes']) > 0 AND isset($class['attributes'][0]['name'])): ?>
		<div class="accordion" id="attributes">
   		<?php foreach($class['attributes'] as $attribute): ?>
   		<div class="accordion-group">
	   		<div class="accordion-heading">
		      <a class="accordion-toggle" data-toggle="collapse" data-parent="#attributes" href="#att-<?php echo $attribute['name'] ?>">
	      <i class="icon-<?php echo ($attribute['scope'] == 'Private' ? 'minus-sign' : ( $attribute['scope'] == 'Public' ? 'plus-sign' : ( $attribute['scope'] == 'Protected' ? 'remove-sign' :'question-sign'))) ?>"></i> <?php echo $attribute['name'] ?>
	      </a>
	    </div>
	    <div id="att-<?php echo $attribute['name'] ?>" class="accordion-body collapse">
	      <div class="accordion-inner">
	      <p><?php echo $attribute['description'] ?></p>
		      </div>
		    </div>
    	</div>
    	<?php endforeach ?>
	</div>
<?php else: ?>
	<p>None</p>
   	<?php endif ?>
    <br><br>
    <h2 class="heading">Methods Details</h2>
    <p>
    	<?php if (count($class['methods']) > 0 AND isset($class['methods'][0]['name'])): ?>
    	<div class="accordion" id="methods">
   		<?php foreach($class['methods'] as $attribute): ?>
	<div class="accordion-group">
   		<div class="accordion-heading">
	      <a class="accordion-toggle" data-toggle="collapse" data-parent="#methods" href="#met-<?php echo $attribute['name'] ?>">
	      <i class="icon-<?php echo ($attribute['scope'] == 'Private' ? 'minus-sign' : ( $attribute['scope'] == 'Public' ? 'plus-sign' : ( $attribute['scope'] == 'Protected' ? 'remove-sign' :'question-sign'))) ?>"></i> <?php echo $attribute['name'] ?>
	      </a>
	    </div>
	    <div id="met-<?php echo $attribute['name'] ?>" class="accordion-body collapse">
	      <div class="accordion-inner">
	      <p><?php echo $attribute['description'] ?></p>
		      </div>
		    </div>
    	</div>
   		<?php endforeach ?>
    	</div>
    	<?php else: ?>
    	<p>None</p>
    	<?php endif ?>
    </p>
<script type="text/javascript">
		$(document).ready(function(){
			Sunlight.highlightAll()
			});
</script>
<style>
body{
background:#fcfcfc;
}
code{
color:rgb(23, 20, 95);
font:italic 13px/30px Verdana, Geneva, sans-serif;
}
</style>