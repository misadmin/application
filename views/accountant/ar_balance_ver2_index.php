<?php if (isset($message) AND !empty($message)): ?>
<div class="alert alert-success alert-error">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<?php echo $message ?>
</div>
<?php endif; ?>

<ul class="nav nav-tabs" id="ar_balance">
  <li class="active"><a href="#ar_summary" data-toggle="tab">Summary</a></li>
  <li><a href="#ar_details" data-toggle="tab">Details</a></li>
<!--  <li><a href="#ar_details_raw" data-toggle="tab">Raw</a></li>-->
</ul>


<div class="loading"></div>
<div class="span9 row-fluid""><!-- 1 -->
<div class="tab-content"><!-- 2 -->
	<div class="tab-pane active" id="ar_summary"><!-- 3 -->
		<div class=""><!-- 4 -->
			<h2 class="lead">A/R Transactions Summary</h2>			
			<form class="form-vertical" method="post" id="frm_summary" action="ar_balance_action_summary_ver2">
				<input type="hidden" style="display:none" name="nonce" value="<?php echo $this->common->nonce() ?>">
				<fieldset>
				  <div class="control-group error" id="college-date-error">
				  	<div class="controls"></div>
				  </div>
				  <div class="control-group input-append">
				    <label class="control-label" for="start_date">From:</label>
				    <div class="controls date" id="c-start_date" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-01') ?>">
				      <input type="text" id="college-start_date" name="start_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-01')?>">
					  <span class="add-on"><i class="icon-calendar"></i></span>
				    </div>
				  </div>
				  <div class="control-group input-append">
				    <label class="control-label" for="end_date">To:</label>
				    <div class="controls date" id="c-end_date" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-d') ?>">
				      <input type="text" id="college-end_date" name="end_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d')?>">
					 <span class="add-on"><i class="icon-calendar"></i></span>
				    </div>
				  </div>
				  <style>
				  div.btn-group[data-toggle=buttons-radio] input[type=radio] {
					  display:    block;
					  position:   absolute;
					  top:        0;
					  left:       0;
					  width:      107%;
					  height:     115%;
					  opacity:    0;
					  margin:	  -2px;
					}
				  </style>
					<div class="form-actions">
					<button type="submit" class="btn btn-success">Show</button>
					</div>
				</fieldset>
			</form>		
		</div><!-- 4 -->
		<script>
			$(document).ready(function() {
				$('#c-start_date').datepicker();
				$('#c-end_date').datepicker();
				$.validator.addMethod("enddate1", function(value, element) {
					var startdatevalue1 = $('#college-start_date').val();
					return Date.parse(startdatevalue1) <= Date.parse(value);
				},"End Date should be greater than or equal to Start Date");
				$('#frm_summary').validate({
					errorClass:'help-inline',
					onkeyup: false,
					rules:{
						end_date:{enddate1:1},
					},
					highlight:function(element,errorClass){
						$(element).addClass('error');
					},
					unhighlight:function(element,errorClass){
						$(element).removeClass('error');
					},
					errorPlacement: function(error, element) {
					     error.appendTo(element.closest('form').children().children('#college-date-error').children('.controls'));
					},
					errorElement:"span",
					submitHandler: function(form) {
						$('.loading').delay(800).fadeIn(400);
						form.submit();
					}
				});
			});	  
		</script>
	</div><!-- 3 -->

	
	<div class="tab-pane" id="ar_details"><!-- 5-->
		<div class=""><!-- 6 -->
			<h2 class="lead">A/R Transactions Details</h2>			
			<form class="form-vertical" method="post" id="frm_details" action="ar_balance_action_ver2">
				<input type="hidden" style="display:none" name="nonce" value="<?php echo $this->common->nonce() ?>">
				<fieldset>
				  <div class="control-group error" id="college-date-error">
				  	<div class="controls"></div>
				  </div>
				  <div class="control-group input-append">
				    <label class="control-label" for="start_date">From:</label>
				    <div class="controls date" id="cstart_date" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-01') ?>">
				      <input type="text" id="collegestart_date" name="start_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-01')?>" >
					  <span class="add-on"><i class="icon-calendar"></i></span>
				    </div>
				  </div>
				  <div class="control-group input-append">
				    <label class="control-label" for="end_date">To:</label>
				    <div class="controls date" id="cend_date" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-d') ?>">
				      <input type="text" id="collegeend_date" name="end_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d')?>">
					 <span class="add-on"><i class="icon-calendar"></i></span>
				    </div>
				  </div>
				  <style>
				  div.btn-group[data-toggle=buttons-radio] input[type=radio] {
					  display:    block;
					  position:   absolute;
					  top:        0;
					  left:       0;
					  width:      107%;
					  height:     115%;
					  opacity:    0;
					  margin:	  -2px;
					}
				  </style>
					<div class="form-actions">
						<button type="submit" class="btn btn-success">Show</button>
					</div>
				</fieldset>
			</form>
		</div><!-- 6 -->
		<script>
			$(document).ready(function() {
				$('#cstart_date').datepicker();
				$('#cend_date').datepicker();
				$.validator.addMethod("enddate2", function(value, element) {
					var startdatevalue2 = $('#collegestart_date').val();
					return Date.parse(startdatevalue2) <= Date.parse(value);
				},"End Date should be greater than or equal to Start Date.");
				$('#frm_details').validate({
					errorClass:'help-inline',
					onkeyup: false,
					rules:{
						end_date:{enddate2:1},
					},
					highlight:function(element,errorClass){
						$(element).addClass('error');
					},
					unhighlight:function(element,errorClass){
						$(element).removeClass('error');
					},
					errorPlacement: function(error, element) {
					     error.appendTo(element.closest('form').children().children('#college-date-error').children('.controls'));
					},
					errorElement:"span",
					submitHandler: function(form) {
						$('.loading').delay(800).fadeIn(400);
						form.submit();
					}
				});
			});	  
		</script>		
	</div><!-- 5 -->

	<div class="tab-pane" id="ar_details_raw"><!-- 7 -->
		<div class=""><!-- 8 -->
			<h2 class="lead">A/R Transactions Raw </h2>			
			<form class="form-vertical" method="post" id="frm_details_raw" action="ar_balance_action_ver2_raw">
				<input type="hidden" style="display:none" name="nonce" value="<?php echo $this->common->nonce() ?>">
				<fieldset>
				  <div class="control-group error" id="college-date-error">
				  	<div class="controls"></div>
				  </div>
				  <div class="control-group input-append">
				    <label class="control-label" for="start_date">From:</label>
				    <div class="controls date" id="cstart_date3" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-01') ?>">
				      <input type="text" id="collegestart_date3" name="start_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-01')?>" >
					  <span class="add-on"><i class="icon-calendar"></i></span>
				    </div>
				  </div>
				  <div class="control-group input-append">
				    <label class="control-label" for="end_date">To:</label>
				    <div class="controls date" id="cend_date3" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-d') ?>">
				      <input type="text" id="collegeend_date3" name="end_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d')?>">
					 <span class="add-on"><i class="icon-calendar"></i></span>
				    </div>
				  </div>
				  <style>
				  div.btn-group[data-toggle=buttons-radio] input[type=radio] {
					  display:    block;
					  position:   absolute;
					  top:        0;
					  left:       0;
					  width:      107%;
					  height:     115%;
					  opacity:    0;
					  margin:	  -2px;
					}
				  </style>
					<div class="form-actions">
						<button type="submit" class="btn btn-success">Raw Show</button>
					</div>
				</fieldset>
			</form>
		</div><!-- 8 -->
		<script>
			$(document).ready(function() {
				$('#cstart_date3').datepicker();
				$('#cend_date3').datepicker();
				$.validator.addMethod("enddate3", function(value, element) {
					var startdatevalue3 = $('#collegestart_date3').val();
					return Date.parse(startdatevalue3) <= Date.parse(value);
				},"End Date should be greater than or equal to Start Date.");
				$('#frm_details').validate({
					errorClass:'help-inline',
					onkeyup: false,
					rules:{
						end_date:{enddate3:1},
					},
					highlight:function(element,errorClass){
						$(element).addClass('error');
					},
					unhighlight:function(element,errorClass){
						$(element).removeClass('error');
					},
					errorPlacement: function(error, element) {
					     error.appendTo(element.closest('form').children().children('#college-date-error').children('.controls'));
					},
					errorElement:"span",
					submitHandler: function(form) {
						$('.loading').delay(800).fadeIn(400);
						form.submit();
					}
				});
			});	  
		</script>		
	</div><!-- 7 -->

</div><!-- 2 -->
</div><!-- 1 -->