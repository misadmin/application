<div class="row-fluid">
	<form id="new_student" method="post" action="" class="form-horizontal">
	<?php action_form('new_employee', FALSE); ?>
	<?php echo $this->common->hidden_input_nonce(); ?>
		<fieldset>
			<div class="control-group formSep">
				<label for="firstname" class="control-label">First Name: </label>
				<div class="controls">
					<input type="text" name="firstname" id="firstname" placeholder="First Name" class="input-xlarge" value="<?php if(isset($firstname)) echo $firstname; ?>" />
				</div>
			</div>
			<div class="control-group formSep">
				<label for="familyname" class="control-label">Family Name: </label>
				<div class="controls">
					<input type="text" name="familyname" id="familyname" placeholder="Family Name" class="input-xlarge"  value="<?php if(isset($familyname)) echo $familyname; ?>" />
				</div>
			</div>
			<div class="control-group formSep">
				<label for="middlename" class="control-label">Middle Name: </label>
				<div class="controls">
					<input type="text" name="middlename" id="middlename"  placeholder="Middle Name" class="input-xlarge"  value="<?php if(isset($middlename)) echo $middlename; ?>" />
				</div>
			</div>
			<div class="control-group formSep">
				<label for="birthdate" class="control-label">Date of Birth</label>
				<div class="controls">
					<div class="input-append date" id="dp2" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
						<input type="text" name="birthdate" id="birthdate" class="span6" value="<?php  if( ! empty($birthdate)) echo $birthdate; else echo date('Y-m-d'); ?>" readonly/>
						<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
				</div>
			</div>
			<div class="control-group formSep">
				<label for="home_address" class="control-label">Home Address:</label>
				<div class="controls" id="home_address_controls">
<?php if ( ! empty($home_address)): ?>
					<div style="margin-top:6px" id="home_address_control_text"><strong><?php echo $home_address; ?></strong>&nbsp;<small><a href="javascript://" id="home_address_control"  class="edit_toggle" >Edit</a></small></div>
<?php endif; ?>
				<div id="home_address_control_controls"></div>
			</div>
				</div>
			<div class="control-group formSep">
				<label for="gender" class="control-label">Gender </label>
				<div class="controls">
					<select name="gender" id="gender">
	<?php foreach($this->config->item('genders') as $key => $gender): ?>
						<option value="<?php echo $key; ?>"><?php echo $gender; ?></option>
	<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label for="bloodtype" class="control-label">Blood Type </label>
				<div class="controls">
					<select name="bloodtype" id="bloodtype">
	<?php foreach($this->config->item('blood_types') as $bloodtype): ?>
						<option value="<?php echo $bloodtype; ?>"><?php echo $bloodtype; ?></option>
	<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-primary" type="submit">Add New Employee</button>
				</div>
			</div>	
		</fieldset>
	</form>
</div>
<script>
var philippine_id = <?php echo ltrim($philippine_id, '0'); ?>;
var city_id = <?php echo ltrim($this->config->item('city_id'), '0'); ?>;
var countries = <?php echo json_encode($countries); ?>;
var provinces = <?php echo json_encode($provinces); ?>;

$(document).ready(function(){
<?php
/* 
	$controls = array(
				'place_of_birth',
				'home_address',
				'city_address',
				'fathers_address',
				'mothers_address',
				'secondary_school_address',
				'emergency_address',
			);

	foreach ($controls as $control){
		if (empty($$control)) {
			echo "\t$('#{$control}_control_controls').show();\n";
		} else {
			echo "\t$('#{$control}_control_controls').hide();\n";
		}
	}
*/
	echo "\t$('#home_address_control_controls').show();\n";
?>
	show_countries('home_address_country', 'home_address_control_controls', false);
});

$(function(){
	$('#dp2').datepicker();
});

$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Unacceptable values found"
);

$('#new_student').validate({
	onkeyup: false,
	errorClass: 'error',
	validClass: 'valid',
	rules: {
		firstname: { required: true },
		familyname: { required: true },
		college: { required: true },
		program: { required: true }
	},
	highlight: function(element) {
		$(element).closest('div').addClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	unhighlight: function(element) {
		$(element).closest('div').removeClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	
	errorPlacement: function(error, element) {
		$(element).closest('div').append(error);
	}
});

$("#firstname").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
$("#familyname").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
$("#middlename").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });

$('#home_address_country').live('change', function(){
	var country_id = $(this).val();
	
	if (country_id==philippine_id){
		$('#home_address').remove();
		$('#home_address_control_controls').append(show_provinces('home_address_province'));
	} else {
		$('#home_address').remove();
		$('#home_address_province').remove();
		$('#home_address_town').remove();
		$('#home_address_barangay_id').remove();
		$('#home_address_control_controls').append('<input style="margin-left: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="home_address" name="home_address" />');
	}
	
});

$('#home_address_province').live('change', function(){
	var province_id = $(this).val();

	$('#home_address_town').remove();
	$('#home_address_barangay_id').remove();
	$('#home_address').remove();
	show_towns(province_id, 'home_address_town', 'home_address_control_controls');
});

$('#home_address_town').live('change', function(){
	var town_id = $(this).val();
	
	$('#home_address_barangay_id').remove();
	$('#home_address').remove();
	show_barangays(town_id, 'home_address_barangay_id', 'home_address_control_controls', false, false, 'home_address');
});

$('#home_address_barangay_id').live('change', function(){
	
	$('#home_address').remove();
	$('#home_address_control_controls').append('<input style="margin-top: 5px" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="home_address" name="home_address" />');
});

function show_provinces (control_id){
	var content = '<select style="margin-left: 5px" id="' + control_id + '" name="' + control_id + '"><option>--Select Province--</option>';
	$.each(provinces, function(index, value){
		content = content + '<option value="' + value.id + '">' + value.name + '</option>';
	});
	content = content + '</select>';
	return content;
}

function show_towns (province_id, control_id, caller){
	
	$.ajax({
		url: "<?php echo site_url("ajax/towns"); ?>/?id=" + province_id,
		dataType: "json",
		success: function(data){
			var content = '<select style="margin-left: 5px" id="' + control_id + '" name="' + control_id + '"><option value="0">--Select Town--</option>';
					
			$.each(data, function(index, value){
				content = content + '<option value="' + value.id + '">' + value.name + '</option>';
			});
			content = content + '</select>';
			$('#'+caller).append(content);
		}	
	});
}

function show_barangays (town_id, control_id, caller, include_same_as_home, include_none, textinput){
	
	$.ajax({
		url: "<?php echo site_url("ajax/barangays"); ?>/?id=" + town_id,
		dataType: "json",
		success: function(data){
			var content = '<select style="margin-left: 5px" id="' + control_id + '" name="' + control_id + '"><option value="0">--Select Barangay (optional)--</option>';

			if(include_none){
				content = content + '<option value="none">No City Address</option>';
			}

			if (include_same_as_home){
				content = content + '<option value="same_as_home">Same as Home Address</option>';
			}		
			$.each(data, function(index, value){
				content = content + '<option value="' + value.id + '">' + value.name + '</option>';
			});
			content = content + '</select>';

			content = content + '<input style="margin-top:5px;" type="text"  placeholder="House Num, Street Name, etc." class="input-xlarge" id="' + textinput + '" name="' + textinput + '" />';
			$('#'+caller).append(content);
		}	
	});
}

function show_countries (control_id, caller, include_same_as_home){
	
	var content = '<select id="' + control_id + '" name="' + control_id + '"><option value="0">--Select Country--</option>';

	if (include_same_as_home){
		content = content + '<option value="same_as_home">Same as Home</option>';
	}
	
	content = content + '<option value="' + philippine_id + '">Philippines</option>';
	$.each(countries, function(index, value){
		if (value.id != philippine_id) {
			content = content + '<option value="' + value.id + '">' + value.name + '</option>';
		}
	});
	content = content + '</select>';
	$('#'+caller).append(content);
}

</script>