 
<table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class = "head">
  <tr class="head">
  <td colspan="3" class="head" style="text-align:center; padding:10px;">
 LIST OF PROGRAMS TAKEN </td>
  </tr>
</table>	

<table class="table table-striped table-bordered">
	<thead >
		<tr align="center">
			<th>Term</th>
			<th>School Year</th>
			<th>Program</th>
			<th>Year Level</th>
		</tr>
	</thead>
	<tbody> 
<?php if($programs_taken){ ?>
<?php foreach ($programs_taken as $program): ?>
		<tr>
			<td><?php echo $program->term; ?></td>
			<td><?php echo $program->sy; ?></td>
			<td><?php echo $program->abbreviation; ?></td>
			<td><?php echo $program->year_level; ?></td>
		</tr>
<?php endforeach; ?>
<?php } ?>
	</tbody>
</table>

