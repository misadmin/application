<?php 

class Class_list_pdf_lib {
	protected $CI;
	
	public function __construct(){
		$this->CI =& get_instance();
	}
	
	function generate_class_list_pdf($course_offering_id) {
	
		$this->CI->load->model("hnumis/Student_model");
		$this->CI->load->model('hnumis/AcademicYears_model');
		$this->CI->load->model('hnumis/Offerings_model');
	
		$students = $this->CI->Student_model->class_list($course_offering_id);
		$course_offering_details = $students[0];
		
		$this->CI->load->library('my_pdf');
		$this->CI->load->library('hnumis_pdf');
		$l = 4;
	
		$this->CI->hnumis_pdf->AliasNbPages();
		$this->CI->hnumis_pdf->set_HeaderTitle('CLASS LIST');
		$this->CI->hnumis_pdf->AddPage('P','letter');
			
		$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->CI->hnumis_pdf->SetDrawColor(165,165,165);
	
		$this->CI->hnumis_pdf->SetFont('times','',10);
		$this->CI->hnumis_pdf->SetTextColor(10,10,10);
		$this->CI->hnumis_pdf->SetFillColor(255,255,255);
		$this->CI->hnumis_pdf->Ln();
	
		$this->CI->hnumis_pdf->Cell(30,$l,'School Year',0,0,'L',true);
		$this->CI->hnumis_pdf->Cell(100,$l,': '. $course_offering_details->term.' '.$course_offering_details->sy,0,0,'L',true);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Cell(30,$l,'Teacher',0,0,'L',true);
		$this->CI->hnumis_pdf->Cell(100,$l,': ' . $course_offering_details->teacher,0,0,'L',true);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Cell(30,$l,'Class',0,0,'L',true);
		$this->CI->hnumis_pdf->Cell(100,$l,': '.$course_offering_details->descriptive_title,0,0,'L',true);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Cell(30,$l,'Schedule/Room',0,0,'L',true);
		
		$slots = explode(',', $course_offering_details->schedule);
		
		foreach($slots AS $key => $slot) {
			if($key > 0)
				$this->CI->hnumis_pdf->Cell(30,$l,'',0,0,'L',true);
			$this->CI->hnumis_pdf->Cell(100,$l,': '.$slot,0,0,'L',true);
			$this->CI->hnumis_pdf->Ln();
		}
		$l = 5;
	
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->SetFillColor(116,116,116);
		$this->CI->hnumis_pdf->SetTextColor(253,253,253);
		$this->CI->hnumis_pdf->SetX(22);
	
		$header1 = array(' ','ID No.','Name of Student','Sex','Year & Course');
	
		$w=array(10,25,90,8,30);
			
		for($i=0;$i<count($w);$i++)
			$this->CI->hnumis_pdf->Cell($w[$i],6,$header1[$i],1,0,'C',true);
	
		$this->CI->hnumis_pdf->SetTextColor(10,10,10);
		$this->CI->hnumis_pdf->SetFillColor(255,255,255);
		$this->CI->hnumis_pdf->Ln();
	
		$num = 1;
		if(is_array($students) && count($students) > 0 && isset($students[0]->neym)){
			foreach($students AS $student) {
				$this->CI->hnumis_pdf->SetX(22);
				$this->CI->hnumis_pdf->Cell($w[0],$l,$num,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[1],$l,$student->idno,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[2],$l,$student->neym,1,0,'L',true);
				$this->CI->hnumis_pdf->Cell($w[3],$l,$student->gender,1,0,'C',true);
				$this->CI->hnumis_pdf->Cell($w[4],$l,$student->year_level.' '.substr($student->abbreviation, 0, 10) . (strlen($student->abbreviation) > 10 ? "..." : ''),1,0,'C',true);
				$this->CI->hnumis_pdf->Ln();
				$num++;
			}
		}
			$this->CI->hnumis_pdf->Output();
	}
}