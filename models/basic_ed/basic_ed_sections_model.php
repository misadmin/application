<?php

	class basic_ed_sections_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListBasicEd() {
			$result = null;
			
			$q1 = "SELECT
							a.* 
						FROM 
							levels AS a
						WHERE
							a.id IN (1,3,4,5,8,9,11,12)";
		
			//edited a.id IN (1,3,4,5,8,9,11,12);
			//print($q1);
			//die();
			//log_message("INFO", print_r($q1,true));

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			return $result;
		}
		
		//Added: April 23, 2013 by Amie
		function ListAllSections($year_level, $level_id) {
			$result = null;
						
			$q1 = "SELECT distinct s.id,s.section_name as section
					FROM basic_ed_histories h
						join basic_ed_sections s on s.id = h.basic_ed_sections_id
						join academic_years y on y.id = h.academic_years_id
					WHERE 
						h.levels_id = {$this->db->escape($level_id)} and
						h.yr_level = {$this->db->escape($year_level)}
						group by section
						order by section";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			return $result;

		
		}
		
		//Added: April 24, 2013 by Amie
		//Edited: Feb 1, 2019 by Toyet - since SHS records is transferred to student_histories,
		//                               we have to use student_histories table
		function Get_distinct_year($level,$acad_year) {
			$result = null;
			
			if($level<11) {  // Not SHS

				$q1 = "SELECT distinct yr_level
						FROM basic_ed_histories
						WHERE levels_id = {$this->db->escape($level)}
							AND academic_years_id = {$this->db->escape($acad_year)}
						ORDER BY yr_level";

			} else { // assumed SHS

				$q1 = "SELECT distinct sh.year_level as yr_level
						 FROM student_histories sh
					    join academic_terms at on at.id=sh.academic_terms_id
						 WHERE at.academic_years_id = {$this->db->escape($acad_year)}
						       AND sh.year_level in (11,12)
						 ORDER BY sh.year_level";
			} 

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 

			log_message("INFO", print_r($result,true));

			return $result;
		
		}
		
		
		
		/*
		 * @updated: 7/10/14
		 * @by: genes
		 * @notes: excludes 'Self Contained' sections
		 * @notes: post only 'active' status
		 */
		function ListStudentsEnrolled($academic_years_id, $yr_level, $levels_id) {
			$result = null;
			
			$q1 = "SELECT
							a.students_idno,
							a.id AS basic_ed_histories_id
						FROM 
							basic_ed_histories AS a
						WHERE
							a.academic_years_id = {$this->db->escape($academic_years_id)}
							AND a.yr_level = {$this->db->escape($yr_level)}
							AND a.levels_id = {$this->db->escape($levels_id)} 
							AND a.basic_ed_sections_id NOT IN (166,175) 
							AND a.status='active'";
 
							// AND a.students_idno = 6742259";
	
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			return $result;
		}
		
		//Added: May 28, 2014 by Amie
		/*function ListCurrentSections($academic_years, $level) {
			$result = null;
				
			$q1 = "SELECT bed.beds.section_name, a.students_idno,
			a.id AS basic_ed_histories_id
			FROM
			basic_ed_histories AS a
			WHERE
			a.academic_years_id = {$this->db->escape($academic_years_id)}
			AND a.yr_level = {$this->db->escape($yr_level)}
			AND a.levels_id = {$this->db->escape($levels_id)} ";
			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
			$result = $query->result();
			}
			return $result;
		}*/
		
		
		function getMyTuitionFee($academic_years_id, $levels_id, $yr_level) {
			$result = NULL;
				
			$q1 = "SELECT
						a.rate
					FROM
						fees_schedule AS a
					WHERE
						a.academic_terms_id IN (SELECT 
														x.id 
													FROM 
														academic_terms AS x
													WHERE 
														x.academic_years_id = {$this->db->escape($academic_years_id)})
						AND a.levels_id = {$this->db->escape($levels_id)} 
						AND a.yr_level = {$this->db->escape($yr_level)} 
						AND a.fees_subgroups_id=201 ";		
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
		
		}


		function getMyOtherFees($academic_years_id, $levels_id, $yr_level) {
			$result = NULL;	
			$q1 = "SELECT 
							SUM(a.rate) AS rate
						FROM 
							fees_schedule AS a,
							fees_groups AS b,
							fees_subgroups AS c
						WHERE
							a.fees_subgroups_id = c.id
							AND b.id = c.fees_groups_id
							AND a.yr_level = {$this->db->escape($yr_level)} 
							AND a.academic_terms_id IN (SELECT 
														x.id 
													FROM 
														academic_terms AS x
													WHERE 
														x.academic_years_id = {$this->db->escape($academic_years_id)})
							AND a.levels_id = {$this->db->escape($levels_id)} 
							AND b.id != 9 ";
			
		//die($q1);
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
			
			return $result;
		
		}
		
		//Added: April 28, 2014 by Isah
		/*
		 * EDITED: 7/7/15 by genes
		 * ORDER BY section_name
		 */
		function ListBasicSections() {
			$result = null;
		
			$q1 = "SELECT id, IF(gs_section_id, 'Grade School', 'High School') as level, section_name
					FROM basic_ed_sections
					ORDER BY section_name";
			//print($q1); die();
			$query = $this->db->query($q1);
		
			if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
			
		}
		
		//Added: May 16, 2014 by Amie
		function ListLevels() {
			$result = null;
			
			$q1 = "SELECT id, level, rank
					FROM levels
					WHERE rank != 0.0
					ORDER BY rank";
			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			}
			return $result;
		}
		
		//Added: May 27, 2014 by Amie
		function getlevel($level_id) {
			$result = null;
				
			$q1 = "SELECT *
					FROM levels
					WHERE id = {$this->db->escape($level_id)}";
				
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
			return $result;
		}
		
		//Added: May 31, 2014 by Amie
		function getlevel_adviser($adviser) {
			$result = null;
		
			$q1 = "SELECT l.id, sh.year_level, sh.academic_years_id
						FROM levels l, sections_histories sh
						WHERE l.id = sh.levels_id
							AND sh.id = {$this->db->escape($adviser)}";

			
			$query = $this->db->query($q1);
		
			if($query->num_rows() > 0){
			$result = $query->row();
			}
			return $result;
			}
		
		//Added: 5/6/2014 by Isah
		
		function insertNewSection($section_id, $sectionNm, $level_tag){
		
			if($level_tag){
				$sql = " INSERT into
				basic_ed_sections (hs_section_id, section_name, inserted_on)
				VALUE
				({$this->db->escape($section_id)},
				{$this->db->escape($sectionNm)},
				NOW()) ";
				}else {
				$sql = " INSERT into
				basic_ed_sections (gs_section_id, section_name, inserted_on)
				VALUE
				({$this->db->escape($section_id)},
				{$this->db->escape($sectionNm)},
				NOW()) ";
				}
				//print_r($sql);die();
				if($this->db->query($sql)){
				return $this->db->insert_id();
		} else {
			return FALSE;
		}
		}
		
		//Added: 5/6/2014 by Isah
		
		function GetSectionId($num) {
			$result = null;
		
			if($num){
				$q1 = "SELECT MAX(hs_section_id) as lastId FROM basic_ed_sections" ;
			}else{
				$q1 = "SELECT MAX(gs_section_id) as lastId FROM basic_ed_sections" ;
			}	
			//print($q1); die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
		
			return $result;
			}

	//Added:  may 29, 2014 by Amie
	function getsectiondetails($adviser, $academic_year) {
		$result = null;

		$q = "SELECT sh.id, sh.levels_id, sh.academic_years_id, sh.year_level, sh.basic_ed_sections_id as bed_id, sh.room,
					 beds.section_name, CONCAT(e.lname,', ',e.fname, ' ', e.mname) as class_adviser
				 from sections_histories sh, basic_ed_sections as beds, employees e
				where sh.basic_ed_sections_id = beds.id
					and sh.employees_empno = e.empno 
					and sh.id = {$this->db->escape($adviser)}
					and sh.academic_years_id = {$this->db->escape($academic_year)}";
		
		$query = $this->db->query($q);
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
		
		return $result;
	}		
			

	//Added: 5/13/2014

	function editSection($data) {
			
				$q1 = "UPDATE basic_ed_sections
						SET section_name = {$this->db->escape($data['section'])}
						WHERE id = {$this->db->escape($data['id'])}";

				//print($q1); die();
				if ($this->db->query($q1)) {
					return TRUE;
				} else {

					return FALSE;
				}
	}
	
	//Added:  May 16, 2014 by Amie
	/*
	 * EDITED: 7/7/15 by genes
	 */
	function faculty_advisers($academicyear, $level) {
		$result = null;
		
		/*$q1 = "SELECT 
					sh.id, 
					sh.year_level, 
					beds.id as section_id, 
					beds.section_name, 
					CONCAT(e.lname,', ',e.fname, ' ', e.mname) as name, 
					room
				FROM 
					sections_histories sh, 
					basic_ed_sections beds, 
					employees e
				WHERE 
					sh.basic_ed_sections_id = beds.id
					AND sh.academic_years_id = {$this->db->escape($academicyear)}
					AND sh.levels_id = {$this->db->escape($level)}
					AND sh.employees_empno = e.empno		
				ORDER by 
					sh.year_level";
		*/
		$q1 = "SELECT
					c.id,
					a.academic_years_id,
					a.levels_id,
					a.yr_level,
					a.basic_ed_sections_id,
					b.section_name,
					CONCAT(d.lname,', ',d.fname, ' ', d.mname) as name,
					d.empno,
					c.room
				FROM
					basic_ed_histories AS a 
						LEFT JOIN sections_histories AS c ON 
							a.basic_ed_sections_id = c.basic_ed_sections_id
							AND a.academic_years_id = c.academic_years_id
							AND a.levels_id = c.levels_id
						LEFT JOIN employees AS d ON c.employees_empno = d.empno,
					basic_ed_sections b
				WHERE
					a.basic_ed_sections_id = b.id
					AND a.academic_years_id = {$this->db->escape($academicyear)}
					AND a.levels_id = {$this->db->escape($level)}
					AND a.status='active'
				GROUP BY
					b.id
				ORDER BY
					a.yr_level, 
					b.section_name";
		
		//die($q1); 
		$query = $this->db->query($q1);
		
		if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result;
			
	}
	
	//Added: May 16, 2014 by Amie
	/*
	 * EDITED: 7/8/15 by genes
	 */
	function create_faculty_adviser($data) {
		$sql = "INSERT INTO 
						sections_histories 
						( 
							 academic_years_id, 
							 basic_ed_sections_id,
							 employees_empno,
							 levels_id,
							 year_level,
							 room,
							 inserted_on,
							 inserted_by
						)
						VALUES (
							{$this->db->escape($data['academic_years_id'])},
							{$this->db->escape($data['basic_ed_sections_id'])},
							{$this->db->escape($data['employees_empno'])},
							{$this->db->escape($data['levels_id'])},
							{$this->db->escape($data['year_level'])},
							{$this->db->escape($data['room'])},
							now(),
							{$this->session->userdata('empno')}
						)";
		
		//print($sql); die();
		$this->db->query($sql);
		$id = @mysql_insert_id();
		
		return $id;
	}
	
	
	function remove_adviser($adviser_id) {
		$sql = "DELETE FROM sections_histories
				WHERE id = $adviser_id";
		
		if ($this->db->query($sql)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*
	 * EDITED: 7/7/15 by genes
	 */
	function ListBasicLevels() {
		$result = null;
			
			$q1 = "SELECT 
						id, 
						level, 
						rank, 
						type
					FROM 
						levels
					WHERE 
						type = 'basic_ed'
						AND status='active'
					ORDER BY 
						rank";
			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			}
			return $result;
	}
	
	//Added: 5/31/2014 by Isah
	function ListHischoolSections() {
		$result = null;
	
		$q1 = "SELECT id, section_name
				FROM basic_ed_sections as a
				WHERE 
					a.gs_section_id IS NULL 
				ORDER BY section_name";
		//print($q1); die();
		$query = $this->db->query($q1);
	
		if($query->num_rows() > 0){
			$result = $query->result();
		}
		return $result;
			
	}
}


//end of Offerings_model.php
