<?php

	class Class_adviser_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		
		function List_MySections($empno=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.block_sections_id,
							a.section_name,
							d.room_no,
							e.yr_level AS grade_level,
							f.description,
							a.academic_terms_id,
							CASE g.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
							END AS term, 
							CONCAT(h.end_year-1,'-',h.end_year) AS sy,
							(SELECT 
									COUNT(DISTINCT(x.students_idno))
								FROM 
									student_histories AS x 
								WHERE 
									x.block_sections_id = e.id 
							) AS num_students,
							(SELECT 
									COUNT(DISTINCT(y.courses_id))
								FROM 
									course_offerings AS y,
									block_course_offerings AS w 
								WHERE 
									y.id = w.course_offerings_id 
									AND w.block_sections_id = e.id 
							) AS num_subjects,
							f.abbreviation
						FROM 
							shs_block_sections AS a,
							rooms AS d,
							block_sections AS e,
							academic_programs AS f,
							academic_terms AS g,
							academic_years AS h
						WHERE 
							e.id = a.block_sections_id
							AND a.rooms_id = d.id 
							AND e.academic_programs_id = f.id 
							AND a.academic_terms_id = g.id 
							AND g.academic_years_id = h.id 
							AND a.block_adviser = {$this->db->escape($empno)} 					
						ORDER BY 
							h.end_year DESC,
							g.term DESC ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
			
		}


		function ListClassSchedules($block_sections_id=NULL) {

			$result = NULL;	

			$sql = "SELECT 
							CONCAT(TIME_FORMAT(h.from_time,'%h:%i%p'),'-',TIME_FORMAT(h.to_time,'%h:%i%p')) AS time_sched,
							h.from_time,
							h.day_name,
							e.id,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									courses AS q,
									block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Mon'
							) AS mon,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Mon'
									AND p.id = s.course_offerings_id 
									AND s.block_sections_id = {$this->db->escape($block_sections_id)}
							) AS mon_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p 
										LEFT JOIN employees AS u ON p.employees_empno=u.empno,
									courses AS q,
									block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Tue'
							) AS tue,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Tue'
									AND p.id = s.course_offerings_id 
									AND s.block_sections_id = {$this->db->escape($block_sections_id)}
							) AS tue_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p 
										LEFT JOIN employees AS u ON p.employees_empno=u.empno,
									courses AS q,
									block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Wed'
							) AS wed,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Wed'
									AND p.id = s.course_offerings_id 
									AND s.block_sections_id = {$this->db->escape($block_sections_id)}
							) AS wed_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p 
										LEFT JOIN employees AS u ON p.employees_empno=u.empno,
									courses AS q,
									block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Thu'
							) AS thu,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Thu'
									AND p.id = s.course_offerings_id 
									AND s.block_sections_id = {$this->db->escape($block_sections_id)}
							) AS thu_teacher,
							(SELECT 
									q.descriptive_title
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p 
										LEFT JOIN employees AS u ON p.employees_empno=u.empno,
									courses AS q,
									block_course_offerings AS r
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.courses_id = q.id
									AND r.course_offerings_id = p.id 
									AND r.block_sections_id = {$this->db->escape($block_sections_id)} 
									AND m.from_time = h.from_time 
									AND m.day_name = 'Fri'
							) AS fri,
							(SELECT 
									CONCAT(q.lname,', ',q.fname) 
								FROM 
									room_occupancy AS m,
									room_occupancy_course_offerings_slots AS n,
									course_offerings_slots AS o,
									course_offerings AS p,
									employees AS q,
									block_course_offerings AS s
								WHERE 
									m.id = n.room_occupancy_id 
									AND n.course_offerings_slots_id = o.id 
									AND o.course_offerings_id = p.id 
									AND p.employees_empno = q.empno
									AND m.from_time = h.from_time 
									AND m.day_name = 'Fri'
									AND p.id = s.course_offerings_id 
									AND s.block_sections_id = {$this->db->escape($block_sections_id)}
							) AS fri_teacher
						FROM 
							block_course_offerings AS d,
							course_offerings AS e,
							course_offerings_slots AS f,
							room_occupancy_course_offerings_slots AS g,
							room_occupancy AS h
						WHERE 
							d.course_offerings_id = e.id 
							AND e.id = f.course_offerings_id 
							AND f.id = g.course_offerings_slots_id
							AND g.room_occupancy_id = h.id
							AND d.block_sections_id = {$this->db->escape($block_sections_id)} 
						GROUP BY 
							h.from_time
						ORDER BY 
							h.from_time";
			//return $sql;
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}


		function CheckIfMyStudent($empno=NULL, $students_idno=NULL) {
			$result = NULL;	

			$sql = "SELECT 
							a.block_sections_id
						FROM 
							shs_block_sections AS a,
							block_sections AS b,
							student_histories AS c
						WHERE 
							a.block_sections_id = b.id 
							AND b.id = c.block_sections_id
							AND a.block_adviser = {$this->db->escape($empno)} 
							AND c.students_idno = {$this->db->escape($students_idno)}
						LIMIT 1 ";
			//die($sql);
			$query = $this->db->query($sql);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
			
		
		}
		
		
	}
	
?>	