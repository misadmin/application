<?php 

class Accountant extends MY_Controller {
	
	public $error_message = '';
	
	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('role')=='accountant'){
			$this->navbar_data['menu'] =
			array(
					'Students'	=> 'student',						
					'Settings'	=> array('Teller Codes'=>'teller_settings'),
					'Reports' 	=> array(
				  				'Enrollment Summary' => array(
				  						'College' 	=> 'enrollment_summary',
				  						'Basic Ed.' => 'enrollment_summary_basic_ed',				  						
				  						),
				  				'Assessments' => array( 
				  						'Assessment Summary - Basic Ed' => 'assessment_summary_basic_ed',
				  						'Assessment Report - Group' 	=> 'assessment_report_group',
				  						'Assessment Report - College' 	=> 'assessment_report_college',	
				  						'Unposted' 						=> 'unposted_assessments',			  						
				  						),
				  				'Accounts Receivables' => array(
				  						'A/R Transactions' 		=> 'ar_balance_ver2',
				  						'Schedule of A/R'		=> 'ar_balances',
				  						'Schedule of Deposits'	=> 'ar_deposits',				  						
				  						),
				  				'College Offerings' => 'course_offerings',
				  				'Students With Privileges'=>'student_with_privileges',				  		
				  				'Others' => array(
				  						'Students with Written Off Records' => 'list_written_off_records',
				  						'Withdrawal' 						=> 'Withdrawals',
				  						'Withdrawals v2' 					=> 'total_withdrawals',
				  						'Enrollments vs Withdrawals' 		=> 'enrollments_vs_withdrawals',
				  						'Student\'s Balances' 				=> 'student_balances',
				  						'List of Students with Multiple IDs'=> 'list_students_with_more_than_one_idno',
				  						'Tuition Fee per College'			=> 'tf_summary',
				  						),
				  				)
						
			);
		}
		$this->content_lib->set_navbar_content('', $this->navbar_data);		
		$this->content_lib->set_title ('Accountant | ' . $this->config->item('application_title'));
	} 
	
	function teller_settings (){
		
		$this->load->library('tab_lib');
		$this->load->model('teller/teller_model');
		
		//added for breadcrumb april 12,13
		$this->breadcrumb_lib->enqueue_item('settings','Settings','teller_settings');
		if ($this->input->post('action')){
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				switch ($this->input->post('action')){
					case 'update_item' :
								if ($ret = $this->teller_model->update_teller_code($this->input->post()))
									$this->content_lib->set_message('Successfully updated teller code', 'alert-success'); else
									$this->content_lib->set_message('Error seen while updating teller code', 'alert-error');
								break;
					case 'new_teller_code':
								//validate inputs...
								if ($this->input->post('teller_code')=='' && $this->input->post('description')){
									$this->content_lib->set_message('Error found on inputted data. Some required items are empty.', 'alert-error');
								} else{
									if ($ret = $this->teller_model->insert_teller_code($this->input->post()))
										$this->content_lib->set_message('Successfully inserted teller code', 'alert-success'); else
										$this->content_lib->set_message('Error seen while inserting teller code. Possible error could be: Duplicate teller code', 'alert-error');
								}
								break;
								
				}	
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}
		}
		$items = $this->teller_model->all_teller_codes();
		
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->content_lib->enqueue_footer_script('bootstrap-multiselect');
		$this->tab_lib->enqueue_tab('Item Codes', 'teller/teller_codes', array('items'=>$items), 'item_codes', TRUE);
		$this->tab_lib->enqueue_tab('New Item Code', 'teller/new_teller_code', array('items'=>$items), 'new_item_codes', FALSE);
		//enqueue breadcrumb april 12,13
		//$this->content_lib->enqueue_body_content('', $this->breadcrumb_lib->content());
		
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		
		$this->content_lib->content();
	}
	

	public function enrollment_summary() {	
		$this->load->model('hnumis/Reports_Model');	
		$this->Reports_Model->enrollment_summary();	
	}
	
	
	public function enrollment_summary_basic_ed() {	
		$this->load->model('basic_ed/Reports_Model');	
		$this->Reports_Model->enrollment_summary_basic_ed();	
	}
	
	
	public function assessment_summary_basic_ed() {	
		$this->load->model('basic_ed/Reports_Model');
		$this->Reports_Model->assessment_summary_basic_ed();	
	}
	

	public function assessment_report_group() {	
		$this->load->library('posting_lib');	
		$this->posting_lib->assessment_report_group();	
	}
	
	
	public function assessment_report_college() {	
		$this->load->library('posting_lib');	
		$this->posting_lib->assessment_report_college();	
	}
		
	
	public function ar_balance(){
		$this->load->library('ar_balance');
		$this->ar_balance->index();
	}
	
	public function ar_balance_basic(){
		$this->load->library('ar_balance');
		$this->ar_balance->basic();
	}
	
	public function ar_balance_college(){
		$this->load->library('ar_balance');
		$this->ar_balance->college();
	}

	
	public function ar_balance_ver2(){
		$this->load->library('ar_balance_ver2');
		//print_r($this->input->post('action'));die();
		
		$this->ar_balance_ver2->index();
	}

	public function ar_balance_action_ver2(){
		$this->load->library('ar_balance_ver2');
		$this->ar_balance_ver2->ar_balance_ver2();
	}

	public function ar_balance_action_summary_ver2(){
		$this->load->library('ar_balance_ver2');
		$this->ar_balance_ver2->ar_balance_summary_ver2();
	}
			
	
	public function  list_written_off_records(){
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->load->model('accountant/accountant_model');
		if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
			$stud_start = $this->input->post('stud_start') ? $this->input->post('stud_start') :'A' ; 
			$stud_end = $this->input->post('stud_end') ? $this->input->post('stud_end') : 'ZZ'; 
			$data['wos'] = $this->accountant_model->get_students_with_written_off_records($stud_start,$stud_end,5000);				
		}
		if ($data['wos'])		 
			$this->content_lib->enqueue_body_content('accountant/list_written_off_records',$data);
		else 
			$this->content_lib->set_message('<h3>No records found!</h3>','Error');
		$this->content_lib->content();
		
	}

	public function student_balances(){
		//$this->content_lib->enqueue_footer_script('data_tables');
		$this->load->library('students_balances');
		$this->students_balances->index();
	}
	
	
	//Added: October 28, 2013 by Amie
	public function Withdrawals() {
		$this->load->library('tab_lib');
		$this->content_lib->set_title ('Accountant | Withdrawals | ' . $this->config->item('application_title'));
		
		$this->load->model('accountant/accountant_model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/enrollments_model');
		
		$term = $this->academic_terms_model->getCurrentAcademicTermID();
		$data = $this->accountant_model->ListTotalWithdrawal($term->current_academic_term_id);
		$this->tab_lib->enqueue_tab ('Total Withdrawal', 'accountant/list_total_withdrawals', array('total_withdrawals'=>$data),
				'TotalWithdrawal', TRUE);
				
		$partial = $this->accountant_model->ListPartialWithdrawals($term->current_academic_term_id);
		$this->tab_lib->enqueue_tab ('Partial Withdrawal', 'accountant/list_partial_withdrawals', array('partial_withdrawals'=>$partial),
				'PartialWithdrawal', FALSE);
		
		$tab = $this->tab_lib->content();
		
		$this->content_lib->enqueue_body_content('', $tab);
		$this->content_lib->content();
	}
	
	/**
	 * @author: Tatskie on 2015-02-26
	 */
	public function student(){
		$this->load->library('../controllers/audit/');
		$this->audit->student();		
	}

	public function ar_balances(){
		$this->load->library('extras_lib');
		$this->extras_lib->ar_schedule();
	}
	
	public function ar_deposits(){
		$this->load->library('extras_lib');
		$this->extras_lib->ar_schedule(TRUE);
	}
	
	public function total_withdrawals() {
		$this->load->library('extras_lib'); //partially done but found out there is already same report for this accountant
		$this->extras_lib->total_withdrawals();
	}

	public function enrollments_vs_withdrawals() {
		$this->load->library('extras_lib'); 
		$this->extras_lib->enrollments_vs_withdrawals();
	}
	
	public function list_students_with_more_than_one_idno(){
		$this->load->library('extras_lib');
		$this->extras_lib->list_students_with_more_than_one_idno();
	}
	

	/**
	 * @author Tatskie on 2014-08-11	 
	 */
	function course_offerings(){
		$this->load->library('../controllers/dean');
		$this->dean->all_offerings();
	}
		
	
	public function tf_summary(){
		$this->load->library('extras_lib');
		$this->extras_lib->tf_summary();
	}
	
	/*
	 * ADDED: 6/10/14
	* @author: genes
	* @description: displays students enrolled from enrollment summary modal
	*/
	function get_students() {
		$this->load->model('hnumis/Enrollments_Model');
	
		$data['students'] = $this->Enrollments_Model->masterlist($this->input->get('selected_term'), $this->input->get('prog_id'), $this->input->get('yr_level'), TRUE);
	
		$this->load->view('dean/list_of_students_from_modal', $data);
	
	}

	/*
	 * @ADDED: 11/19/14
	 * @author: genes
	 */
	function list_allocations() {
		$this->load->model('hnumis/Offerings_Model');

		$data['allocations'] = $this->Offerings_Model->ListAllocation($this->input->get('offer_id'));
		$data['block_names'] = $this->Offerings_Model->ListBlockOfferings($this->input->get('offer_id'));
		
		$this->load->view('dean/list_of_allocations_from_modal', $data);
	}
	
	/**
	 * @author: Tatskie on 2015-02-26
	 */
	public function unposted_assessments() {
		$this->load->library('../controllers/audit/');
		$this->audit->unposted_assessment();
	}

	/**
	 * @author: Tatskie on 2015-02-26
	 */
	public function student_with_privileges() {
		$this->load->library('../controllers/accounts/');
		$this->accounts->privileges();
	}
	
}