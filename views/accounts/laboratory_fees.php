<?php
$lab_fees = array(
		3=>array(
				6=>array(
						'name'=>'lab1',
						'rate'=>1235
				),
				7=>array(
						'name'=>'lab2',
						'rate'=>1235
				),
				8=>array(
						'name'=>'lab3',
						'rate'=>1235
				),
				9=>array(
						'name'=>'lab4',
						'rate'=>1235
				)
		)
);

//print_r($lab_fees);

?>
<br />
<h3>Laboratory Fees</h3>
<br />
<table class="table">
	<thead>
		<tr>
			<th width="30%">Name</th>
			<th>Fee</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($lab_fees as $lab_fee_id=>$lab_fee): ?>
			<?php foreach($lab_fee as $lab_fee_sub_id=>$lab_fee_sub): ?>
		<tr>
			<td><?php echo $lab_fee_sub['name']?></td>
			<td><input class="money" style="width:5em; text-align:right;" type="text" name="lab_fee[<?php echo $lab_fee_sub_id; ?>]" value="<?php echo number_format($lab_fee_sub['rate'], 2); ?>"/></td>
		</tr>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</tbody>
</table>