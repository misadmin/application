<?php 
	$year_levels = array(
			1 => 'First',
			2 => 'Second',
			3 => 'Third',
			4 => 'Fourth',
			5 => 'Fifth',
			);
	$terms_name = array(
			1 => 'First Semester',
			2 => 'Second Semester',
			3 => 'Summer',
			);
?>
<div class="row-fluid span12">
	<h2 class="heading"><?php echo $academic_program->description . "({$academic_program->abbreviation})"; ?>
		<small class="pull-right">Effective Year: 
			<select name="prospectus_id" id="prospectus_id">
<?php foreach ($prospectuses as $val): ?>
				<option value="<?php echo $val->id; ?>" <?php if($val->id==$this->uri->segment(4)) echo ' selected="selected" '; ?>><?php echo $val->effective_year; ?></option>
<?php endforeach; ?>
			</select>
		</small>
	</h2>
<?php if(isset($prospectus) && count($prospectus) > 0 ): ?>
<?php foreach ($prospectus as $year => $years): ?>
	<div class="row-fluid">
		<h3 class="heading"><?php if (isset($year_levels[$year])) echo $year_levels[$year]; ?> Year</h3>
<?php foreach ($years as $term=>$terms): ?>
		<div class="span5">
			<h4><?php if (isset($terms_name[$term])) echo $terms_name[$term]; ?></h4>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Code</th>
						<th>Descriptive Title</th>
						<th>Units</th>
					</tr>
				</thead>
				<tbody>
<?php foreach($terms as $course):?>
					<tr>
						<td><?php echo $course['course_code']; ?></td>
						<td><?php echo $course['descriptive_title']; ?></td>
						<td><?php echo $course['credit_units']; ?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
		</div>	
<?php endforeach; ?>
	</div>
<?php endforeach; ?>
<?php else: ?>
	<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>No details found for this prospectus.</div>
<?php endif; ?>
</div>
<script>
$('#prospectus_id').change(function(){
	var new_url = "<?php echo site_url('/registrar/prospectus/') . "/{$this->uri->segment(3)}"; ?>/" + $("#prospectus_id option:selected").val();

	window.location = new_url;
});
</script>