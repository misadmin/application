<?php
$no_of_years = 5;
$no_of_terms = 3;

$groups = array(
		'1'=>array(
				'name'		=>'tuition',
				'children' 	=> array(
						'100'=>'tuition'
				)
		),
		'2'=>array(
				'name'=>'miscellaneous',
				'children' => array(
						'101'=>'misc1',
						'102'=>'misc2',
						'103'=>'misc3'
				)
		),
		'3'=>array(
				'name'=>'other fees',
				'children' => array(
						'101'=>'misc1',
						'102'=>'misc2',
						'103'=>'misc3'
				)
		),
);
?>
<script>
$( document ).ready(function() {
	$(".child-key-switch").bind("click", function(event){
		event.preventDefault();
		var child_key_id = $(this).attr("child-key");
		$(".child-key_"+child_key_id).each(function(){
			var value = $(".child-key_"+child_key_id).first().val();
			$(".child-key_"+child_key_id).val(value);
		});
	});
});
</script>

<table class="table">
	<thead>
		<tr>
			<th></th>
			<?php
				$year = 1;
				while($year <= $no_of_years):
			?>
			<th>Year <?php echo $year++?></th>
			<?php endwhile?>
		</tr>
	</thead>
	<tbody>
	<?php foreach($groups as $key=>$val): ?>
		<tr>
			<td colspan="<?php echo $no_of_years+1?>"><?php echo $val['name'] ?></td>
		</tr>
		<?php foreach($val['children'] as $child_key => $child): ?>
		<tr>
			<td child-id="<?php echo $child_key;?>">&nbsp;&nbsp;<?php echo $child; ?></td>
			<?php
				$year = 1;
				while($year <= $no_of_years):
			?>
			<td>
				<?php
					$term = 1;
					while($term <= $no_of_terms):
				?>
				<input type="text" class="child-key_<?php echo $child_key;?>" year="<?php echo $year;?>" term="<?php echo $term;?>" style="width: 2em;">
				<?php 
					$term++;
					endwhile;
				?>
				<br />
				<?php if($year == 1): ?>
				<a class="child-key-switch" child-key="<?php echo $child_key;?>">copy</a>
				<?php endif; ?>
			</td>
			<?php 
				$year++;
				endwhile;
			?>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>