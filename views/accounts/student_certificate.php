<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function show_other_reason()
{
	show('other_reason');
	document.reason_form.other_reason.focus();
}
function hide_other_reason()
{
	hide('other_reason');
}
</script>

<body onLoad="hide_other_reason();">

<div style="width:auto;">
<div>
<form method="post" id="change_academic_term_form">
  <input type="hidden" name="action" value="generate_student_certificate" />
<select name="academic_terms_id" id="academic_terms_id">
<?php
	if ($academic_terms) {
		foreach($academic_terms AS $terms) {
			if ($term->id == $terms->id) {
				print("<option value=\"$terms->id\" selected>$terms->term $terms->sy</option>");
			} else {
				print("<option value=\"$terms->id\" >$terms->term $terms->sy</option>");
			}
		}
	}
?>
</select>
</form>
</div>
<?php
		if (!isset($student_histories_id)) {
			//print("Student not enrolled in the Academic Term!");
		} else {
?>
<div style="padding-top:0px; font-family:Arial, Helvetica, sans-serif; font-size:14px; width:70%; text-align:left;">

  <div style="font-size:12px; font-weight:normal; padding:3px;">
  <form method="post" name="reason_form" target="_blank">
  <input type="hidden" name="action" value="generate_student_certificate" />
  <input type="hidden" name="student_histories_id" value="<?php print($student_histories_id); ?>"  />
  <input type="hidden" name="academic_terms_id" value="<?php print($term->id); ?>"  />
  <input type="hidden" name="course_year" value="<?php print($course_year);?>"  />
  	<table border="0" cellpadding="2" cellspacing="0" class="table" style="width:70%;">
		<tr>
		<td align="left" valign="top" style="width:7%;">Reason</td>
		<td align="left" valign="top" style="width:2%;">:</td>
		<td align="left" valign="top" style="width:91%;"><input name="reason" type="radio" value="Y" checked="checked" onClick="hide_other_reason();" />
		  whatever legal purpose it may serve<br />
		  <input name="reason" type="radio" value="other" onClick="show_other_reason();"/>
		  Others<br />
              <input name="other_reason" type="text" id="other_reason" size="40" maxlength="40" />
            </td>
		</tr>
		<tr>
		<td colspan="3"><input type="submit" name="generate_certificate" value="Generate PDF!" class="btn btn-warning" /></td>
		</tr>
    </table>
  </form>
  </div>
  <br />
  TO WHOM IT MAY CONCERN: 
    <p />
  <p>This is to certify that <b><?php print($name);?></b> is officially enrolled at Holy Name University for <b><?php print($term->term." ".$term->sy); ?></b> taking up <b><?php print($course_year);?></b> and has the following charges:   
  </p>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px;">
    <tr>
      <td colspan="5" style="font-weight:bold;">Tuition Fees: (@ <?php print($tuition_rate); ?>) </td>
      </tr>
    <tr>
      <td width="35%">&nbsp;</td>
      <td width="19%" align="center">Load Units </td>
      <td width="15%" align="center">Pay Units </td>
      <td width="14%" align="right">&nbsp;</td>
      <td width="17%">&nbsp;</td>
    </tr>
	<?php
		$total_load=0;
		$total_paying=0;
		$total_tuition=0;
		if ($courses) {
			foreach($courses AS $course) {
				$total_load += $course->credit_units;
				$total_paying += $course->paying_units;
				$total_tuition += $course->tuition_fee;
	?>
    <tr>
      <td style="padding-left:15px;"><?php print($course->course_code); ?></td>
      <td align="center"><?php print($course->credit_units); ?></td>
      <td align="center"><?php print($course->paying_units); ?></td>
      <td align="right"><?php print(number_format($course->tuition_fee,2)); ?></td>
      <td>&nbsp;</td>
    </tr>
    <?php
			}
	?>
	<tr>
      <td>&nbsp;</td>
      <td align="center" style="border-top:solid; border-width:1px;"><?php print(number_format($total_load,1)); ?></td>
      <td align="center"><?php print($total_paying); ?></td>
      <td align="right">&nbsp;</td>
      <td align="right"><?php print(number_format($total_tuition,2)); ?></td>
    </tr>
	<?php
		}
		
		if ($matriculation and $courses) {
	?>
	<tr>
	  <td colspan="5">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="5" style="font-weight:bold;">Misc. Fees and Other Fees: </td>
	  </tr>
	<tr>
	  <td style="padding-left:15px;"><?php print($matriculation->code); ?></td>
	  <td align="center">&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td align="right"><?php print(number_format($matriculation->rate,2)); ?></td>
	  <td align="right">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="5" align="right">
	    <?php 
	  		$total_tuition += $matriculation->rate;
	  		print(number_format($matriculation->rate,2)); 
		?></td>
	  </tr>
	<tr>
	  <td>&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td align="right">&nbsp;</td>
	  <td align="right">&nbsp;</td>
	  </tr>
	  <?php
	  		}
			
	  		if ($miscs and $courses) {
	  ?>
	<tr>
	  <td colspan="5" style="padding-left:15px;">Miscellaneous Fees </td>
	  </tr>
	  <?php
				$total_misc = 0;
				foreach ($miscs AS $misc) {
					$total_misc += $misc->rate;
	  ?>
	<tr>
	  <td style="padding-left:25px;"><?php print($misc->description); ?></td>
	  <td align="center">&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td align="right"><?php print(number_format($misc->rate,2)); ?></td>
	  <td align="right">&nbsp;</td>
	  </tr>
	  <?php
	  			}
		?>
	<tr>
	  <td colspan="5" align="right" style="padding-left:15px;"><?php 
	  		$total_tuition += $total_misc;
	  		print(number_format($total_misc,2)); 
		?></td>
	  </tr>
	  <?php
	  		}
	  		if ($other_fees and $courses) {
		?>
	<tr>
	  <td colspan="5" style="padding-left:15px;">Other School Fees </td>
	  </tr>
	 <?php
				$total_other = 0;
		 		foreach($other_fees AS $other) {
					$total_other += $other->rate;
	 ?>
	<tr>
	  <td style="padding-left:25px;"><?php print($other->description); ?></td>
	  <td align="center">&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td align="right"><?php print(number_format($other->rate,2)); ?></td>
	  <td align="right">&nbsp;</td>
	  </tr>
	  <?php
	  			}
	  			if (isset($learning_resources)) {
					$total_other += $learning_resources->rate;
	  ?>
	<tr>
		  <td style="padding-left:25px;"><?php print($learning_resources->description); ?></td>
		  <td align="center">&nbsp;</td>
		  <td align="center">&nbsp;</td>
		  <td align="right"><?php print(number_format($learning_resources->rate,2)); ?></td>
		  <td align="right">&nbsp;</td>
	  </tr>
	  <?php 
	  			}
	  			if (isset($student_support)) {
					$total_other += $student_support->rate;
	  ?>
	<tr>
		  <td style="padding-left:25px;"><?php print($student_support->description); ?></td>
		  <td align="center">&nbsp;</td>
		  <td align="center">&nbsp;</td>
		  <td align="right"><?php print(number_format($student_support->rate,2)); ?></td>
		  <td align="right">&nbsp;</td>
	  </tr>
	  <?php 
	  			}
	  ?>
	  <tr>
	  <td colspan="5" align="right" style="padding-left:15px;"><?php 
	  		$total_tuition += $total_other;
	  		print(number_format($total_other,2)); 
		?></td>
	  </tr>
	  <?php
	  		}
	  		if (isset($affiliated_fees)) {
	  ?>
	<tr>
      <td colspan="5" style="padding-left:15px;">Other Additional School Fees</td>
	  </tr>
	 <?php
				$total_other_addtl_fee = 0;
		 		foreach($affiliated_fees AS $affil_fee) {
					$total_other_addtl_fee += $affil_fee->rate;
	 ?>	  
	<tr>
      <td style="padding-left:25px;"><?php print($affil_fee->description); ?></td>
	  <td align="center">&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td align="right"><?php print(number_format($affil_fee->rate,2)); ?></td>
	  <td align="right">&nbsp;</td>
	  </tr>
	  <?php
	  			}
		?>
	<tr>
      <td colspan="5" align="right" style="padding-left:15px;"><?php 
	  		$total_tuition += $total_other_addtl_fee;
	  		print(number_format($total_other_addtl_fee,2)); 
		?></td>
	  </tr>
	  <?php
	  		}
	  		if ($addtl_other_fees and $courses) {
	  ?>
	<tr>
      <td colspan="5" style="padding-left:15px;">Additional Other Fees </td>
	  </tr>
	 <?php
				$total_addtl = 0;
		 		foreach($addtl_other_fees AS $addtl) {
					$total_addtl += $addtl->rate;
	 ?>	  
	<tr>
      <td style="padding-left:25px;"><?php print($addtl->description); ?></td>
	  <td align="center">&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td align="right"><?php print(number_format($addtl->rate,2)); ?></td>
	  <td align="right">&nbsp;</td>
	  </tr>
	  <?php
	  			}
		?>
	<tr>
      <td colspan="5" align="right" style="padding-left:15px;"><?php 
	  		$total_tuition += $total_addtl;
	  		print(number_format($total_addtl,2)); 
		?></td>
	  </tr>
	  <?php
	  		}
	  		if ($lab_fees and $courses) {
		?>
	<tr>
	  <td colspan="5" style="font-weight:bold;">Laboratory Fees </td>
	  </tr>
	  <?php
				$total_lab = 0;
	  			foreach($lab_fees AS $lab_fee) {
					$total_lab += $lab_fee->rate;					
	  ?>
	<tr>
	  <td style="padding-left:15px;"><?php print($lab_fee->course_code); ?></td>
	  <td align="center">&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td align="right"><?php print($lab_fee->rate); ?></td>
	  <td align="right">&nbsp;</td>
	  </tr>
	  <?php
	  			}
	  ?>
	<tr>
	  <td colspan="5" align="right" style="padding-bottom:10px;"><?php 
	  		$total_tuition += $total_lab;
	  		print(number_format($total_lab,2)); 
		?></td>
	  </tr>
	  <?php
	  		}
	?>
	<tr>
	  <td colspan="5" align="right" style="border-top:solid; border-width:1px; font-size:16px; font-weight:bold; border-color:#C9C9C9; padding-top:5px;"><?php print(number_format($total_tuition,2)); ?></td>
	  </tr>
  </table>
</div>
<?php
	}
?>
</div>
</body>

  <form id="download_certificate_form" method="post" target="_blank">
  <input type="hidden" name="action" value="generate_student_certificate" />
</form>
<script>
	$('.download_certificate').click(function(event){
		event.preventDefault(); 
		var student_histories_id = $(this).attr('student_histories_id');
		var academic_terms_id = $(this).attr('academic_terms_id');
		var course_year = $(this).attr('course_year');
			
		$('<input>').attr({
			type: 'hidden',
			name: 'student_histories_id',
			value: student_histories_id,
		}).appendTo('#download_certificate_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_terms_id',
			value: academic_terms_id,
		}).appendTo('#download_certificate_form');
		$('<input>').attr({
			type: 'hidden',
			name: 'course_year',
			value: course_year,
		}).appendTo('#download_certificate_form');
		$('#download_certificate_form').submit();
		
	});
</script>


<script>
	$(document).ready(function(){
		$('#academic_terms_id').bind('change', function(){
			$('#change_academic_term_form').submit();
		});
	});
</script>

