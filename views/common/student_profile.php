<div class="row-fluid">
<h2 class="heading">Student Profile</h2>
</div>
<div class="row-fluid">
	<div class="span12">
		<div class="row-fluid">
			<div class="span8">
				<div class="media" style="margin-bottom: 12px">
					<a class="pull-left" href="#studentImage" role="button" data-toggle="modal">
						<img src="<?php echo $image; ?>" alt="" class="media-object img-polaroid" data-src="holder.js/64x64" style="width:64px;margin-bottom: 12px" />
					</a>
					<div class="media-body">
						<h4 class="media-heading">[<?php echo $idnum; ?>] <?php echo $name; ?> <span > - <?php echo $course; ?> <?php echo $level; ?> <?php echo $section; ?></span></h4>						

<?php if (isset($bed_status) && trim($bed_status)!=='') { //added by tatskie to make user aware of student's status
			if ($bed_status !=='active') { ?>
				<h5><i class="icon-eye-open"></i> <span style="font-size:13px;font-weight:normal;color:red;"><b><?php echo str_replace(array("\r\n\r\n\r\n", "\r\n\r\n", "\n\n\n", "\n\n"), ",", ucfirst($bed_status)); ?>!</b></span></h5>
<?php }else{ ?>				
				<h5><i class="icon-eye-open"></i> <span style="font-size:13px;font-weight:normal;"><?php echo str_replace(array("\r\n\r\n\r\n", "\r\n\r\n", "\n\n\n", "\n\n"), ",", ucfirst($bed_status)); ?></span></h5>
<?php }}; ?>


<?php if (trim($full_home_address)!=='') { ?>
						<h5><i class="icon-home"></i> <span style="font-size:13px;font-weight:normal"><?php echo str_replace(array("\r\n\r\n\r\n", "\r\n\r\n", "\n\n\n", "\n\n"), ",", $full_home_address); ?></span></h5>
<?php }; ?>												
<?php if (trim($full_city_address)!=='') { ?>
						<h5><i class="icon-barcode"></i> <span style="font-size:13px;font-weight:normal"><?php echo str_replace(array("\r\n\r\n\r\n", "\r\n\r\n", "\n\n\n", "\n\n"), ",", $full_city_address); ?></span></h5>
<?php }; ?>						
<?php if (trim($phone_number) !== '') { ?>
						<h5><i class="icon-envelope"></i> <span style="font-size:13px;font-weight:normal"><?php echo $phone_number; ?></span></h5>
<?php }; ?>												
					</div>
					<div id="studentImage" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" style="width:auto !important;left:30px;right: auto;top: 30px;margin: 0 !important;" >
					  <div class="modal-body" style="padding:0">
					    <img src="<?php echo $image ?>" alt="" data-src="holder.js/130x130" style="max-width:<?php echo (isset($profile_image_max_width) ? $profile_image_max_width : '140px') ?>" />
					  </div>
					</div>
				</div>
			</div>
			<div class="span4">
			<?php if(isset($profile_sidebar)): ?>
			<div class="widgets well" style="margin-bottom:0">
			<?php foreach ($profile_sidebar as $widget): ?>
				<?php echo $widget ?>
			<?php  endforeach ?>
			</div>
			<?php endif ?>
			</div>
		</div>
	</div>
</div>