<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
	width:100%;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>
  <div style="background:#FFF; ">
<?php

if($assessed){
	if (!($tuition_fee_basic OR $tuition_fee_others)) {
?>
<div>NO FEE SCHEDULE POSTED YET!</div>

<?php
 //elseif (isset($privilege_availed) AND ($privilege_availed->posted == 'Y')) {
?>  
<!-- 	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td style="text-align:center; font-weight:bold;">STUDENT'S PRIVILEGE ALREADY POSTED!  APPLY FOR ADJUSTMENT ONLY! </td>
  </tr>
</table> -->
<?php
	} else {
?>

<form colspan="6" method="post" id="allow_privilege_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="action" value= "form_to_assign_privileges"/> 
  <?php
	if (isset($privilege_availed)) {
  ?>
  <input type="hidden" name="privilege_id1" value="<?php print($privilege_availed->id); ?>" > 
	<?php
		}
	?>
    <table align="center" cellpadding="0" cellspacing="0" class="head1" style="width:55%;">
      <tr class="head">
        <td colspan="3" class="head"> ASSIGN PRIVILEGE</td>
      </tr>
      <tr>
        <td colspan="3"><table border="0" cellpadding="1" cellspacing="0" class="inside">
           <tr>
            <td width="170" valign="middle">Term</td>
            <td width="8" valign="middle">:</td>
            <td width="87" valign="middle"> <?php print($term->term." ".$term->sy); ?> </td>
          </tr>	   
		  <tr>
            <td valign="middle">Privilege</td>
            <td valign="middle">:</td>
            <td valign="middle">
			
			<select class="form" id="privilege_id" onchange="changeEventHandler(event);">
              <?php
					foreach($privilege_items AS $privilege_item) {
						print("<option value=".$privilege_item->id.">".$privilege_item->scholarship."</option>");	
					}
				?>
            </select>
		</td>
          </tr>
           <tr>
             <td> Tuition Fee </td>
             <td valign="middle">:</td>
             <td><div><p id="display_tuition_fee"><?php echo number_format($tuition_fee,2); ?></p></div>
				<input type="hidden" name="tuition_fee" id="tuition_fee" value="<?php print($tuition_fee); ?>" >
				<input type="hidden" name="paying_units" id="paying_units" value="<?php print($paying_units); ?>" >
				<input type="hidden" name="scholarship_units" id="scholarship_units" value="<?php print($scholarship_units); ?>" >
				<input type="hidden" name="tuition_fee_basic_rate" id="tuition_fee_basic_rate" value="<?php print($tuition_fee_basic_rate); ?>" >
				<input type="hidden" name="year_level" id="year_level" value="<?php print($year_level); ?>" >
				<input type="hidden" name="selected_privilege_id" id="selected_privilege_id" value="227" >
				<input type="hidden" name="withcwts_units" id="withcwts_units" value="<?php print($withcwts_units); ?>" >
             </td>
           </tr>
		   <tr>
            <td valign="middle"> Tuition Percentage (%) </td>
            <td valign="middle">:</td>
            <td valign="middle">
			
			 <input name="TuitionD" type="text" id="TuitionD" size="3" maxlength="3" value = "0" style="width:25px;" >
			
			 </td>
          </tr>
		  <?php
				if ($other_privileges) {
		  ?>
		   <tr>
		     <td valign="middle"><strong><u>Other Privileges</u></strong></td>
	        </tr>
			<?php
					foreach($other_privileges AS $other) {
						if($other->id != 5 OR ($other->id == 5 AND $with_lab)){
			?>
						   <tr>
							 <td valign="middle" style="padding-left:15px;"><?php print($other->fees_group); ?></td>
							 <td valign="middle">:</td>
							 <td valign="middle"><input type="checkbox" name="<?php print($other->id); ?>" value="Y"/></td>
							</tr>
						   <tr>
		   <?php
		   				}
		   			}
		   		}
		   ?>
		     <td colspan="3"><br><input type="submit" name="button2" id="allow_privilege" value="Continue!" class="btn btn-success" /></td>
			 
	        </tr>
        </table></td>
      </tr>
    </table>
</form>
<?php			
		}
}else { ?>
   <div style="color:red";> <i> Assessment NOT posted yet! </i></div>
<?php }?>   
</div>


<script>
$('#allow_privilege').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to assign privilege to student?");

	if (confirmed){
		$('#allow_privilege_form').submit();
	}		
});
</script>

  <form id="srcform3" method="post">
  <input type="hidden" name="action" value= "edit_privileges"/>
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>
<script>
	$('#edit_priv').click(function(event){
		event.preventDefault(); 
		var privilege_availed_id = $(this).attr('href');
		var tuition_fee = $(this).attr('tuition_fee');
		var paying_units = $(this).attr('paying_units');
		var history_id = $(this).attr('history_id');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'tuition_fee',
		    value: tuition_fee,
		}).appendTo('#srcform3');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'privilege_availed_id',
		    value: privilege_availed_id,
		}).appendTo('#srcform3');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'paying_units',
		    value: paying_units,
		}).appendTo('#srcform3');
		$('<input>').attr({
		    type: 'hidden',
		    name: 'history_id',
		    value: history_id,
		}).appendTo('#srcform3');
		$('#srcform3').submit();
	});
</script>

<script>
	function changeEventHandler(event) {
	var selected_privilege_id = event.target.value;
	var paying_units = $('#paying_units').val();
	var scholarship_units = $('#scholarship_units').val();
	var tuition_fee_basic_rate = $('#tuition_fee_basic_rate').val();
	var year_level = $('#year_level').val();
	var withcwts_units = $('#withcwts_units').val();
	var ajaxData = [];
    var tuition_fee = 0;

	$.ajax({
		'url': '<?php echo site_url("ajax/recompute_tuition_byprivilege"); ?>',
		'type': 'post', // performing a POST request
		'data': { selected_privilege_id: selected_privilege_id,
		          paying_units: paying_units,
		          scholarship_units: scholarship_units,
		          tuition_fee_basic_rate: tuition_fee_basic_rate,
		          year_level: year_level,
		          withcwts_units: withcwts_units },
		'dataType':'json',
		'success':function(data){
			ajaxData = data;
			console.log(ajaxData);
			$( "#display_tuition_fee" ).html( ajaxData.tuition_fee );
			$( "#tuition_fee" ).val( ajaxData.tuition_fee );
			$( "#selected_privilege_id" ).val( ajaxData.selected_privilege_id );
		},
		error: function (request, error) {
        	console.log(arguments);
        	alert(" Can't do because: " + error);
    	}
	});
	}
</script>

