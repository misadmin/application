
<h2 class="heading">Fees Category Management</h2>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; 
	border-color: #D8D8D8; height: 35px; padding: 2px; margin-bottom:20px;">
	<table style="width:100%; padding:0px; height:auto;">
		<tr>
		<td style="text-align:left; vertical-align:top; font-weight:bold;">
	    <a href="#" data-toggle="modal" data-target="#modal_add_group"><button class="btn btn-warning" >Add Category</button></a>

		<div class="modal hide fade" id="modal_add_group" >
			<form method="post">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="weight" value="<?php print($weight->weight); ?>" />
				<input type="hidden" name="action" value="add_fees_group" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3>Fees Group</h3>
				</div>
				<div class="modal-body">            
					    <?php 
					    	$this->load->view('accounts/fees_schedule/modals/add_fees_group_form');
					    ?>
				</div>
				<div class="modal-footer">
					<input type="submit" name="button" id="button" value="Proceed!" class="btn btn-success" />
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
		
		
		</td>
		<td style="text-align:right; vertical-align:top;">	
			<form id="change_data" method="POST">
					<select name="fees_groups_id" id="fees_groups">
						<?php
							foreach($fees_groups AS $f_group) {
						?>
							<option value="<?php print($f_group->id); ?>" <?php if ($f_group->id == $selected_fee_group) { print("selected"); } ?>>
								<?php print($f_group->fees_group); ?> </option>	
						<?php 
							}
						?>					
					</select>
			</form>		
		</td>
		</tr>
	</table>
				
</div>

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; 
	border-color: #D8D8D8; height: 30px; padding: 2px; margin-bottom:20px; width:50%; margin-left:20px;">
	<span style="font-size:14px; font-weight:bold;">Category Items</span>
	    <a href="#" data-toggle="modal" data-target="#modal_add_subgroup" style="float:right;">
	    <button class="btn btn-success" >Add Fees Group Item</button></a>

		<div class="modal hide fade" id="modal_add_subgroup" >
			<form method="post">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="fees_groups_id" value="<?php print($selected_fee_group); ?>" />
				<input type="hidden" name="action" value="add_fees_subgroup" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3>Fees Group Item</h3>
				</div>
				<div class="modal-body">            
					    <?php 
					    	$this->load->view('accounts/fees_schedule/modals/add_fees_subgroup_form');
					    ?>
				</div>
				<div class="modal-footer">
					<input type="submit" name="button" id="button" value="Proceed!" class="btn btn-success" />
					<a href="#" class="btn" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
		
		
	
	
</div>

<div>
	<?php 
		if ($fees_subgroups) {
	?>
		<table class="table table-condensed table-bordered table-hover" style="width:50%; margin-left:20px;">
			<thead>
				<tr style="background-color:#DBDADA;">
					<td style="width:50%;">Description</td>
					<td style="width:40%;">Remarks</td>
					<td style="width:5%;">Edit</td>
					<td style="width:5%;">Del.</td>
				</tr>
			</thead>	
	<?php 
			foreach($fees_subgroups AS $sgroup) {
	?>
				<tr>
					<td><?php print($sgroup->description); ?></td>
					<td><?php print($sgroup->remark); ?></td>
					<td style="text-align:center;"><i class="icon-edit"></i></td>
					<td style="text-align:center;">
						<a class="delete_subgroup" href="<?php print($sgroup->id); ?>" >
					<i class="icon-trash"></i></a>
					</td>
				</tr>	
	<?php 
			}
	?>
		</table>
	<?php 		
		}
	?>

</div>


<script>
	$(document).ready(function(){
		$('#fees_groups').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'change_pulldown',
			}).appendTo('#change_data');
			$('#change_data').submit();
		});
	});
</script>


<form id="delete_subgroup_form" method="post">
	  <?php $this->common->hidden_input_nonce(FALSE); ?>
	  <input type="hidden" name="action" value="delete_subgroup" />
	  <input type="hidden" name="fees_groups_id" value="<?php print($selected_fee_group);?>" />
</form>

<script>
	$('.delete_subgroup').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to remove the item?');

		if (confirmed){
			var fees_subgroups_id = $(this).attr('href');
		
			$('<input>').attr({
				type: 'hidden',
				name: 'fees_subgroups_id',
				value: fees_subgroups_id,
			}).appendTo('#delete_subgroup_form');
			$('#delete_subgroup_form').submit();
		}
		
	});
</script>

