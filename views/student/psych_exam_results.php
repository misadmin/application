<?php 

?><table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Exam Name</th>
			<th>Exam Description</th>
			<th>Raw Score</th>
			<th>Percentile</th>
			<th>Exam Taken</th>
			<th>Term and SY Taken</th>
		</tr>
	</thead>
	<tbody> 
<?php if (is_array($exams_taken) && count($exams_taken) > 0 ): ?>
<?php foreach ($exams_taken as $exam): ?>
		<tr>
			<td><?php echo $exam->abbreviation; ?></td>
			<td><?php echo $exam->description; ?></td>
			<td><?php echo $exam->raw_score; ?></td>
			<td><?php echo $exam->percentile; ?></td>
			<td><?php echo $exam->date_taken; ?></td>
			<td><?php echo $exam->term; ?> <?php echo $exam->sy; ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="6">No Exam Taken Yet</td>
		</tr>
<?php endif; ?>
	</tbody>
</table>