<?php 

class Tab_lib {
	
	private $tab_contents=array();
	private $active_tab;
	private $tab_container_name;
	private $tab_children=array();
	private $tab_container_view;
	private $CI;
	
	public function __construct($tab_name=''){
		$this->CI =& get_instance();
		$this->set_tab_container_name($tab_name);
		$this->tab_container_view = $this->CI->config->item('tab_container_view');
	}
	
	public function set_tab_container_name ($name){
		$this->tab_container_name = $name;
	}
	
	public function set_active_tab($view, $tab_content) {
	$content = "";
		if ( ! empty($view)){
			ob_start();
			$this->CI->load->view($view, $tab_content);
			$content = ob_get_contents();
			ob_end_clean();
		}
		$this->active_tab = TRUE;
	}
	
	public function enqueue_tab ($title, $view, $tab_content, $tab_alias, $active=FALSE, $parent=""){
		$content = "";
		if ( ! empty($view)){
			ob_start();
			$this->CI->load->view($view, $tab_content);
			$content = ob_get_contents();
			// ob_end_clean();
			if (ob_get_contents()) ob_end_clean();
		}
		
		if ( empty($parent)) {
			$this->tab_contents[] =	array(
							'title'		=> $title,
							'active'	=> $active,
							'alias'		=> $tab_alias,
							'content'	=> $content,
						);
		} else {
			$this->tab_children[$parent][] = array(
							'title'		=> $title,
							'active'	=> $active,
							'alias'		=> $tab_alias,
							'content'	=> $content,
						);
		}		
	}
	
	public function content(){
		$data = array(
					'tab_id'	=> $this->tab_container_name,
					'contents'	=> $this->tab_contents,
					'children'	=> $this->tab_children,
				);
		
		ob_start();
		$this->CI->load->view($this->tab_container_view, $data);
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}
}