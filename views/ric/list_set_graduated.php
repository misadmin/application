
		<h2 class="heading">Graduated Programs</h2>

		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; 
						border-color: #D8D8D8; padding: 5px; margin-bottom:20px; margin-top:20px; vertical-align:middle;">
				<a href="#" data-toggle="modal" data-target="#modal_set_graduated" class="btn btn-success"
								data-project-id='<?php //print($student_tor); ?>'><i class="icon-plus icon-white"></i> Set Graduated</a>	
		</div>		

		<!-- Modal to Set Graduated -->
 		
		      <div class="modal hide fade" id="modal_set_graduated" >
		 		<form method="post" id="close_student_form">
		   				<input type="hidden" name="action" value="add_graduated_student" />
		   				<?php $this->common->hidden_input_nonce(FALSE); ?>
		   		<div class="modal-header">
		       		<button type="button" class="close" data-dismiss="modal">�</button>
		       		<h3>Add Graduated Details</h3>
		   		</div>
		   		<div class="modal-body">
		 				<table style="width:100%; text-align:left;" cellpadding="2" cellspacing="0">	
		 					<tr>
		 						<td style="width:30%;">Graduated from:</td>
		 						<td style="width:70%;"><input name="taken_type" type="radio" class="form" 
										onClick="document.getElementById('hnu').style.display = 'table'; document.getElementById('other_school').style.display = 'none'; 
									 							return;" 
										value="hnu" checked="checked"/> HNU
							  		<input name="taken_type" type="radio" class="form" 
										onClick="document.getElementById('hnu').style.display = 'none';	document.getElementById('other_school').style.display = 'table'; 
									 							return;" 
										value="other" /> OTHER SCHOOL
								</td>
							</tr>	
							<tr>
								<td style="width:30%;" colspan="2">
									<table class="table" id="hnu" style="display: block; text-align: left; ">
										<tr>
											<td style="vertical-align: top;">School Year Graduated:</td>
											<td style="vertical-align: top;">
												<select name="student_histories_id" style="padding:0px;" id="hnu_program"><?php 
															foreach($hnu_graduated AS $hnu) {
																print("<option value=".$hnu->student_histories_id." program=\"".$hnu->program_name."\" grad_date=\"".$hnu->grad_date."\" so_text=\"".$hnu->so_text."\">".$hnu->term." ".$hnu->sy."</option>");
															}
														?>
													</select>
											</td>
										</tr>	
										<tr>
											<td>&nbsp;</td>
											<td id="hnu_program_name" style="font-weight:bold; font-size:14px; color:#077929;"><?php print($hnu_graduated[0]->program_name); ?> </td>
										</tr>
										<tr style="vertical-align:top;">
											<td>Statement:</td>
											<td style="padding-left:10px;"><textarea name="graduated_statement1" style="width:300px; height:80px;" id="your_textarea_id"><?php print($hnu_graduated[0]->grad_date.' '.$hnu_graduated[0]->so_text); ?></textarea></td>
										</tr>
										<tr>
											<td colspan="2">NOTE: <b>Graduated/Completed</b> is automatically selected by the system.</td>
										</tr>	
								</table>
									
									<table class="table" id="other_school" style="display: none; text-align: left; ">
										<tr>
											<td style="vertical-align: top;">School Year Graduated:</td>
											<td><select name="other_schools_id" id="other_program"><?php 
														foreach($other_graduated AS $other_g) {
															print("<option value=".$other_g->other_schools_id." program=\"".$other_g->program_name."\">".$other_g->term." ".$other_g->sy."</option>");
														}
													?>
												</select>							
											</td>							
										</tr>
										<tr>
											<td>&nbsp;</td>
											<?php 
												if ($other_graduated) {
											?>
											<td id="other_program_name" style="font-weight:bold; font-size:14px; color:#077929;"><?php print($other_graduated[0]->program_name); ?> </td>
											<?php 
												}
											?>
										</tr>
										<tr style="vertical-align:top;">
											<td>
												<select name="grad_type" style="width:auto;">
													<option value="G">Graduated</option>
													<option value="C">Completed</option>
												</select>
											</td>
											<td style="padding-left:10px;"><textarea name="graduated_statement2" style="width:300px; height:80px;"> </textarea></td>
										</tr>
								</table>
									
								</td>
							</tr>
					</table>
		   		</div>
			   <div class="modal-footer">
		    		<input type="submit" class="btn btn-primary" id="add_school" value="ADD GRADUATED DETAILS!">
			   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		       </div>
				</form>
		    </div>				
				
				<!--  End of Modal to set graduated -->		
						
		<div>
		
		<?php
			if ($graduated_programs) {
		?>
			<div style="width:100%; margin-bottom:40px;">
			  <table border="0" cellspacing="0" class="table table-condensed" style="width:100%; padding:0px; margin:0px;">
			    <thead>
			    <tr>
			      <th width="18%" class="head" style="vertical-align:middle;">School Year</th>
			      <th width="34%" class="head" style="vertical-align:middle;">Statement</th>
			      <th width="30%" class="head" style="vertical-align:middle;">Program</th>
			      <th width="8%" class="head" style="vertical-align:middle;">What School</th>
			      <th width="5%" class="head" style="vertical-align:middle;">Edit</th>
			      <th width="5%" class="head" style="vertical-align:middle;">Del</th>
			      </tr>
			    </thead>		
		<?php 
				foreach ($graduated_programs AS $program) {
		?>
		    <tr>
		      <td style="text-align:center; vertical-align:middle; "><?php print($program->term." ".$program->sy); ?> </td>
		      <td style="text-align:left; vertical-align:middle; "><?php print($program->graduation_statement); ?></td>
		      <td style="text-align:left; vertical-align:middle; "><?php print($program->program_name); ?> </td>
		      <td style="text-align:center; vertical-align:middle; "><?php print($program->what_school); ?></td>
		      <td style="text-align:center; vertical-align:middle; ">
			 		<a href="#" data-toggle="modal" data-target="#modal_edit_graduated_<?php print($program->id); ?>" 
								data-project-id=" "><i class="icon-edit"></i></a>
			 		
					<!-- Modal to edit Graduated Details -->
			 		
					      <div class="modal hide fade" id="modal_edit_graduated_<?php print($program->id); ?>" >
					 		<form method="post" id="edit_graduated_form">
					   				<input type="hidden" name="action" value="edit_graduated_student" />
					   				<input type="hidden" name="graduated_id" value="<?php print($program->id); ?>" />
					   				<?php $this->common->hidden_input_nonce(FALSE); ?>
					   		<div class="modal-header">
					       		<button type="button" class="close" data-dismiss="modal">�</button>
					       		<h3>Edit Graduated Details</h3>
					   		</div>
					   		<div class="modal-body">
					   				<?php 
					   					if ($program->what_school == 'HNU') {
					   						$show_hnu='block';
					   						$show_other='none';
					   					} else {
					   						$show_hnu='none';
					   						$show_other='block';
					   					}
					   				?>
					 				<table style="width:100%; text-align:left;" cellpadding="2" cellspacing="0">	
					 					<tr>
					 						<td style="width:30%;">Graduated from:</td>
					 						<td style="width:70%;">
					 							<input name="taken_type" type="radio" class="form" 
													onClick="document.getElementById('hnu_<?php print($program->id); ?>').style.display = 'table'; document.getElementById('other_school_<?php print($program->id); ?>').style.display = 'none'; 
												 							return;" 
													value="hnu" 
													<?php 
														if ($program->what_school == 'HNU') {
															print("checked=\"checked\""); 
														}?>
											/> HNU
										  		<input name="taken_type" type="radio" class="form" 
													onClick="document.getElementById('hnu_<?php print($program->id); ?>').style.display = 'none';	document.getElementById('other_school_<?php print($program->id); ?>').style.display = 'table'; 
												 							return;" 
													value="other" 
													<?php 
														if ($program->what_school == 'OTHER SCHOOL') {
															print("checked=\"checked\""); 
														}?>											
											/> OTHER SCHOOL
											</td>
										</tr>	
										<tr>
											<td style="width:30%;" colspan="2">
												<table id="hnu_<?php print($program->id); ?>" style="display: <?php print($show_hnu); ?>; text-align: left; ">
													<tr>
														<td style="vertical-align: top;">School Year Graduated:</td>
														<td style="vertical-align: top;">
														<select name="student_histories_id" 
														style="padding:0px;" id="hnu_program_edit_<?php print($program->id); ?>"><?php 
																	foreach($hnu_graduated AS $hnu) {
																		if ($hnu->student_histories_id == $program->student_histories_id) {
																			print("<option value=".$hnu->student_histories_id." program1=\"".$hnu->program_name."\" grad_date1=\"".$hnu->grad_date."\" so_text1=\"".$hnu->so_text."\" selected>".$hnu->term." ".$hnu->sy."</option>");
																		} else {
																			print("<option value=".$hnu->student_histories_id." program1=\"".$hnu->program_name."\" grad_date1=\"".$hnu->grad_date."\" so_text1=\"".$hnu->so_text."\">".$hnu->term." ".$hnu->sy."</option>");
																		}
																	}
																?>
															</select>
														</td>							
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td id="hnu_program_name_edit_<?php print($program->id); ?>" style="font-weight:bold; font-size:14px; color:#077929;"><?php print($program->program_name); ?> </td>
													</tr>
													<tr style="vertical-align:top;">
														<td>Statement:</td>
														<td style="padding-left:10px;"><textarea name="graduated_statement1" style="width:300px; height:80px;" id="your_textarea_id_<?php print($program->id); ?>"><?php print($program->graduation_statement); ?></textarea></td>
													</tr>
													<tr>
														<td colspan="2">NOTE: <b>Graduated/Completed</b> is automatically selected by the system.</td>
													</tr>	
												</table>

												<table id="other_school_<?php print($program->id); ?>" style="display: <?php print($show_other); ?>; text-align: left; ">
													<tr>
														<td style="vertical-align: top;">School Year Graduated:</td>
														<td><select name="other_schools_id" id="other_program_<?php print($program->id); ?>"><?php 
																	foreach($other_graduated AS $other_g) {
																		if ($other_g->other_schools_id == $program->other_schools_student_histories_id) {
																			print("<option value=".$other_g->other_schools_id." program=\"".$other_g->program_name."\" selected>".$other_g->term." ".$other_g->sy."</option>");
																		} else {
																			print("<option value=".$other_g->other_schools_id." program=\"".$other_g->program_name."\">".$other_g->term." ".$other_g->sy."</option>");
																		}
																	}
																?>
															</select>							
														</td>							
													</tr>
													<tr>
														<td>&nbsp;</td>
														<?php 
															if ($other_graduated) {
														?>
															<td id="other_program_name_<?php print($program->id); ?>" style="font-weight:bold; font-size:14px; color:#077929;"><?php print($other_graduated[0]->program_name); ?> </td>
														<?php 
															}
														?>
													</tr>
													<tr style="vertical-align:top;">
														<td>
															<select name="grad_type" style="width:auto;">
																	<option value="G">Graduated</option>
																	<option value="C">Completed</option>
																</select>
														</td>	
														<td style="padding-left:10px;"><textarea name="graduated_statement2" style="width:300px; height:80px;"><?php print($program->graduation_statement); ?></textarea></td>
													</tr>
												</table>
											</td>
										</tr>
								</table>
					   		</div>
						   <div class="modal-footer">
					    		<input type="submit" class="btn btn-primary" id="add_school" value="EDIT GRADUATED DETAILS!">
						   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					       </div>
							</form>
					    </div>				
							
							<!--  End of Modal to edit graduated details -->		
						
						<script>
						$("#hnu_program_edit_<?php print($program->id); ?>").change(function(){
				            var element = $("option:selected", this);
				            var myTag = element.attr("program1");
				            var myGradDate = element.attr("grad_date1");
				            var mySOText = element.attr("so_text1");
				            
				            $("#hnu_program_name_edit_<?php print($program->id); ?>").text(myTag);
            				$("#your_textarea_id_<?php print($program->id); ?>").val(myGradDate+' '+mySOText);
				        });
						</script>
			 		
			 		</td>
		      <td style="text-align:center; vertical-align:middle; ">
			 		<a class="delete_program" href="<?php print($program->id); ?>" ><i class="icon-trash"></i></a></td>
		      </tr>
		<?php
				}
		?>
		</table>
		
		</div>
		<?php 		
			}
		?>
		  </div>
		

<form id="delete_program_form" method="post" >
	<input type="hidden" name="action" value="delete_graduated" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>			

<script>
$('.delete_program').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to remove Graduated Program?");

	if (confirmed){
		var id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'graduated_id',
		    value: id,
		}).appendTo('#delete_program_form');
		$('#delete_program_form').submit();
	}		
});
</script>

 
<script>
$("#hnu_program").change(function(){
            var element = $("option:selected", this);
            var myTag = element.attr("program");
            var myGradDate = element.attr("grad_date");
            var mySOText = element.attr("so_text");
            
            $('#hnu_program_name').text(myTag);
            $('#your_textarea_id').val(myGradDate+' '+mySOText);
        });
</script>

<script>
$("#other_program").change(function(){
            var element = $("option:selected", this);
            var myTag = element.attr("program");

            $('#other_program_name').text(myTag);
        });
</script>

