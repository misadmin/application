<div style="float:left; width:auto;">
<form id="search_faculty" action="<?php echo site_url("dean/exception_report")?>" method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="2" name="step" />
	
	
<div class="formSep">
	<h3>Choose a Term and Faculty Member</h3>
</div> 
	<fieldset>
		<div class="control-group formSep">
			<label for="adjustment" class="control-label"> <b> Academic Term: </b> </label>
				<div class="controls">
					<select id="academic_terms" name="academic_terms">
							<!--  <option value="">-- Select Academic Term --</option>  -->
          			<?php
						foreach($academic_terms AS $academic_term) {
							print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");	
						}
					?>
					</select>
				</div>
		</div>
		<div class="control-group formSep">
			<label for="adjustment" class="control-label"><b>Faculty Name:</b></label>
				<div class="controls">
					<select id="faculty" name="faculty">
						<option value="">Select Faculty</option>
					</select>
				</div>
		</div>
		<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">View Report</button>
					</div>
		</div>	
	</fieldset>
</form>

<script>
$('#academic_terms').change(function(){
		var academic_term = $('#academic_terms option:selected').val();
		var college_id = <?php echo $college_id; ?>
		
		$.ajax({
			url: "<?php echo site_url("ajax/academic_term"); ?>/?academic_term=" + academic_term + "&college_id=" + college_id,
			dataType: "json",
			success: function(data){
				var doptions = make_options(data);
				$('#faculty').html(doptions);
			}	
		});	
	});

function make_options (data){
	var doptions = '<option value="">-- Select Faculty --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].empno
		 	+ '">'
		 	+ data[i].name
		 	+ '</option>';
	}
	return doptions;
}

$(document).ready(function(){
	$('#search_faculty').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			faculty: {required: true},
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
});
</script>		