<div class="row-fluid">
	<div class="span12">
<?php if ( isset($errors) && count($errors) > 0): ?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php foreach($errors as $err):?>
		<?php echo $err; ?>
		<?php endforeach; ?>
		</div>
<?php endif; ?>
<?php if (isset($success) && $success) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			Password changed.
		</div>
<?php endif; ?>
		<form id="passwords_form" method="post" action="<?php echo site_url ("{$role}/tools/")?>" class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="change_password" />
			<fieldset>
				<div class="control-group formSep">
					<label for="password" class="control-label">Old Password</label>
					<div class="controls">
						<input type="password" id="password" name="password" class="input-xlarge" value=""/>
					</div>
				</div>
										<div class="control-group formSep">
											<label for="newpassword1" class="control-label">New Password</label>
											<div class="controls">
												<input type="password" id="newpassword1" name="newpassword1" class="input-xlarge" value="" />
											</div>
										</div>	
										<div class="control-group formSep">
											<label for="newpassword2" class="control-label">Repeat New Password</label>
											<div class="controls">
												<input type="password" id="newpassword2" name="newpassword2" class="input-xlarge" value="" />
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button class="btn btn-primary" type="submit">Update Password</button>
											</div>
										</div>
										
									</fieldset>
								</form>
							</div>
						</div>
<script>
$(document).ready(function(){
	$('#passwords_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			newpassword1: { required: true, minlength: 6 },
			newpassword2: { required: true, equalTo: "#newpassword1" }
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	$('#newpassword1').password_strength();
});
</script>
<style>
.password_strength {
	margin-left: 10px;
	padding: 8px;
	display: inline-block;
	}
.password_strength_1 {
	background-color: #fcb6b1;
	}
.password_strength_2 {
	background-color: #fccab1;
	}
.password_strength_3 {
	background-color: #fcfbb1;
	}
.password_strength_4 {
	background-color: #dafcb1;
	}
.password_strength_5 {
	background-color: #bcfcb1;
	}			
</style>