<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/
$config['base_url']	= '';

/*
|--------------------------------------------------------------------------
| Index File
|--------------------------------------------------------------------------
|
| Typically this will be your index.php file, unless you've renamed it to
| something else. If you are using mod_rewrite to remove the page set this
| variable so that it is blank.
|
*/
// $config['index_page'] = 'index.php';
$config['index_page'] = '';
/*
|--------------------------------------------------------------------------
| URI PROTOCOL
|--------------------------------------------------------------------------
|
| This item determines which server global should be used to retrieve the
| URI string.  The default setting of 'AUTO' works for most servers.
| If your links do not seem to work, try one of the other delicious flavors:
|
| 'AUTO'			Default - auto detects
| 'PATH_INFO'		Uses the PATH_INFO
| 'QUERY_STRING'	Uses the QUERY_STRING
| 'REQUEST_URI'		Uses the REQUEST_URI
| 'ORIG_PATH_INFO'	Uses the ORIG_PATH_INFO
|
*/
$config['uri_protocol']	='REQUEST_URI';

/*
 * Use SSL...
 */
$config['use_ssl'] = false;
/*
|--------------------------------------------------------------------------
| URL suffix
|--------------------------------------------------------------------------
|
| This option allows you to add a suffix to all URLs generated by CodeIgniter.
| For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/urls.html
*/

$config['url_suffix'] = '';

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
$config['language']	= 'english';

/*
|--------------------------------------------------------------------------
| Default Character Set
|--------------------------------------------------------------------------
|
| This determines which character set is used by default in various methods
| that require a character set to be provided.
|
*/
$config['charset'] = 'UTF-8';

/*
|--------------------------------------------------------------------------
| Enable/Disable System Hooks
|--------------------------------------------------------------------------
|
| If you would like to use the 'hooks' feature you must enable it by
| setting this variable to TRUE (boolean).  See the user guide for details.
|
*/
$config['enable_hooks'] = TRUE;


/*
|--------------------------------------------------------------------------
| Class Extension Prefix
|--------------------------------------------------------------------------
|
| This item allows you to set the filename/classname prefix when extending
| native libraries.  For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/core_classes.html
| http://codeigniter.com/user_guide/general/creating_libraries.html
|
*/
$config['subclass_prefix'] = 'MY_';


/*
|--------------------------------------------------------------------------
| Allowed URL Characters
|--------------------------------------------------------------------------
|
| This lets you specify with a regular expression which characters are permitted
| within your URLs.  When someone tries to submit a URL with disallowed
| characters they will get a warning message.
|
| As a security measure you are STRONGLY encouraged to restrict URLs to
| as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
|
| Leave blank to allow all characters -- but only if you are insane.
|
| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
|
*/
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';


/*
|--------------------------------------------------------------------------
| Enable Query Strings
|--------------------------------------------------------------------------
|
| By default CodeIgniter uses search-engine friendly segment based URLs:
| example.com/who/what/where/
|
| By default CodeIgniter enables access to the $_GET array.  If for some
| reason you would like to disable it, set 'allow_get_array' to FALSE.
|
| You can optionally enable standard query string based URLs:
| example.com?who=me&what=something&where=here
|
| Options are: TRUE or FALSE (boolean)
|
| The other items let you set the query string 'words' that will
| invoke your controllers and its functions:
| example.com/index.php?c=controller&m=function
|
| Please note that some of the helpers won't work as expected when
| this feature is enabled, since CodeIgniter is designed primarily to
| use segment based URLs.
|
*/
$config['allow_get_array']		= TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger']	= 'c';
$config['function_trigger']		= 'm';
$config['directory_trigger']	= 'd'; // experimental not currently in use

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['log_threshold'] = 0;

/*
|--------------------------------------------------------------------------
| Error Logging Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| application/logs/ folder. Use a full server path with trailing slash.
|
*/
$config['log_path'] = '';

/*
|--------------------------------------------------------------------------
| Date Format for Logs
|--------------------------------------------------------------------------
|
| Each item that is logged has an associated date. You can use PHP date
| codes to set your own date formatting
|
*/
$config['log_date_format'] = 'Y-m-d H:i:s';

/*
|--------------------------------------------------------------------------
| Cache Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| system/cache/ folder.  Use a full server path with trailing slash.
|
*/
$config['cache_path'] = '';

/*
|--------------------------------------------------------------------------
| Encryption Key
|--------------------------------------------------------------------------
|
| If you use the Encryption class or the Session class you
| MUST set an encryption key.  See the user guide for info.
|
*/
$config['encryption_key'] = 'Thequickbrownfoxjumpsoverme';

/*
|--------------------------------------------------------------------------
| Session Variables
|--------------------------------------------------------------------------
|
| 'sess_cookie_name'		= the name you want for the cookie
| 'sess_expiration'			= the number of SECONDS you want the session to last.
|   by default sessions last 7200 seconds (two hours).  Set to zero for no expiration.
| 'sess_expire_on_close'	= Whether to cause the session to expire automatically
|   when the browser window is closed
| 'sess_encrypt_cookie'		= Whether to encrypt the cookie
| 'sess_use_database'		= Whether to save the session data to a database
| 'sess_table_name'			= The name of the session database table
| 'sess_match_ip'			= Whether to match the user's IP address when reading the session data
| 'sess_match_useragent'	= Whether to match the User Agent when reading the session data
| 'sess_time_to_update'		= how many seconds between CI refreshing Session Information
|
*/
$config['sess_cookie_name']		= 'wp_session';
$config['sess_expiration']		= 7200;
$config['sess_expire_on_close']	= TRUE;
$config['sess_encrypt_cookie']	= TRUE;
$config['sess_use_database']	= FALSE;
$config['sess_table_name']		= 'ci_sessions';
$config['sess_match_ip']		= FALSE;
$config['sess_match_useragent']	= TRUE;
$config['sess_time_to_update']	= 300;

/*
|--------------------------------------------------------------------------
| Cookie Related Variables
|--------------------------------------------------------------------------
|
| 'cookie_prefix' = Set a prefix if you need to avoid collisions
| 'cookie_domain' = Set to .your-domain.com for site-wide cookies
| 'cookie_path'   =  Typically will be a forward slash
| 'cookie_secure' =  Cookies will only be set if a secure HTTPS connection exists.
|
*/
$config['cookie_prefix']	= "";
$config['cookie_domain']	= "";
$config['cookie_path']		= "/";
$config['cookie_secure']	= FALSE;

/*
|--------------------------------------------------------------------------
| Global XSS Filtering
|--------------------------------------------------------------------------
|
| Determines whether the XSS filter is always active when GET, POST or
| COOKIE data is encountered
|
*/
$config['global_xss_filtering'] = FALSE;

/*
|--------------------------------------------------------------------------
| Cross Site Request Forgery
|--------------------------------------------------------------------------
| Enables a CSRF cookie token to be set. When set to TRUE, token will be
| checked on a submitted form. If you are accepting user data, it is strongly
| recommended CSRF protection be enabled.
|
| 'csrf_token_name' = The token name
| 'csrf_cookie_name' = The cookie name
| 'csrf_expire' = The number in seconds the token should expire.
*/
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;

/*
|--------------------------------------------------------------------------
| Output Compression
|--------------------------------------------------------------------------
|
| Enables Gzip output compression for faster page loads.  When enabled,
| the output class will test whether your server supports Gzip.
| Even if it does, however, not all browsers support compression
| so enable only if you are reasonably sure your visitors can handle it.
|
| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
| means you are prematurely outputting something to your browser. It could
| even be a line of whitespace at the end of one of your scripts.  For
| compression to work, nothing can be sent before the output buffer is called
| by the output class.  Do not 'echo' any values with compression enabled.
|
*/
$config['compress_output'] = FALSE;

/*
|--------------------------------------------------------------------------
| Master Time Reference
|--------------------------------------------------------------------------
|
| Options are 'local' or 'gmt'.  This pref tells the system whether to use
| your server's local time as the master 'now' reference, or convert it to
| GMT.  See the 'date helper' page of the user guide for information
| regarding date handling.
|
*/
$config['time_reference'] = 'local';


/*
|--------------------------------------------------------------------------
| Rewrite PHP Short Tags
|--------------------------------------------------------------------------
|
| If your PHP installation does not have short tag support enabled CI
| can rewrite the tags on-the-fly, enabling you to utilize that syntax
| in your view files.  Options are TRUE or FALSE (boolean)
|
*/
$config['rewrite_short_tags'] = FALSE;


/*
|--------------------------------------------------------------------------
| Reverse Proxy IPs
|--------------------------------------------------------------------------
|
| If your server is behind a reverse proxy, you must whitelist the proxy IP
| addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
| header in order to properly identify the visitor's IP address.
| Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
|
*/
$config['proxy_ips'] = '';//10.1.1.129';

//HNUMIS configs...
$config['results_to_show_per_page'] = 20;
$config['fiscal_year_start'] = '06-01';
$config['application_title'] = 'HNUMIS'; // from HNU-MIS, ra 8/17/13
$config['application_title_long'] = 'Management Information System';
$config['max_login_tries'] = 9;
$config['keep_cache'] = FALSE;
$config['minify_content'] = false;
$config['minify_exceptions'] = array('teller', 'dric');
$config['username_length'] = 8;
$config['available_scripts'] = array(
		'app'					=> 'assets/js/app.js',
		'jquery' 				=> 'assets/js/jquery.min.js',
		'jquery-ui' 			=> 'assets/js/jquery-ui-1.10.3.custom.min.js',
		'validate'				=> 'assets/js/jquery.validate.js',
		'actual'				=> 'assets/js/jquery.actual.min.js',
		'counter'				=> 'assets/js/jquery.counter.min.js',
		'autocomplete'			=> 'assets/js/jquery.autocomplete.js',
		'bootstrap'				=> 'assets/bootstrap/js/bootstrap.min.js',
		'bootstrap-multiselect' => 'assets/js/bootstrap-multiselect.js',
		'chained'				=> 'assets/js/chained_select/jquery.chained.min.js',
		'password_strength' 	=> 'assets/js/jquery.password_strength.min.js',
		'data_tables' 			=> 'assets/js/jquery.dataTables.min.js',
		'date_picker'			=> 'assets/bootstrap/js/bootstrap-datepicker.min.js',
		'qtip'					=> 'assets/js/jquery.qtip.min.js',
		'stepy'					=> 'assets/js/jquery.stepy.min.js',
		'sticky'				=> 'assets/js/jquery.sticky.js',
		'sticky_table_headers'	=> 'assets/js/jquery.stickytableheaders.min.js',
		'zrssfeed'				=> 'assets/js/jquery.zrssfeed.min.js',
		'printelement'			=> 'assets/js/jquery.printelement.min.js',
		'image_area_select'		=> 'assets/js/jquery.imgareaselect.pack.js',
		'multi_select'			=> 'assets/js/jquery.multi-select.js',
		'maskedinput'			=> 'assets/js/jquery.maskedinput-1.2.2.js', //added by tatskie
		'html2canvas'			=> 'assets/js/html2canvas.js',
		'jqueryhtml2canvas'		=> 'assets/js/jquery.plugin.html2canvas.js',
		'printing'				=> 'assets/js/printing.js',
		'sunlight_php'			=> 'assets/js/sunlight.php.min.js',
		'sunlight'				=> 'assets/js/sunlight.min.js',
		'directPrint'			=> 'assets/js/directPrint.js',
		'gritter'				=> 'assets/js/jquery.gritter.js',
		'amaran'				=> 'assets/js/jquery.amaran.min.js', // added 10/9/14
		'ggpopover' 			=> 'assets/js/ggpopover.js',
);
$config['preload_scripts']	= array ('jquery','jquery-ui', 'bootstrap', 'app', 'autocomplete', 'validate', 'counter', 'actual','sticky_table_headers','amaran');
$config['available_styles'] = array(
		'bootstrap' 			=> 'assets/bootstrap/css/bootstrap.min.css',
		'bootstrap-responsive'	=> 'assets/bootstrap/css/bootstrap-responsive.min.css',
		'style'					=> 'assets/css/style.css',
		'green'					=> 'assets/css/green.css',
		'blue'					=> 'assets/css/blue.css',
		'date_picker'			=> 'assets/bootstrap/css/datepicker.css',
		'qtip'					=> 'assets/css/jquery.qtip.min.css',
		'stepy'					=> 'assets/css/jquery.stepy.css',
		'image_area_select'		=> 'assets/css/imgareaselect-default.css',
		'multi_select'			=> 'assets/css/multi-select.css',
		'sunlight_default'		=> 'assets/css/sunlight.default.min.css',
		'sunlight_dark'			=> 'assets/css/sunlight.dark.min.css',
		'gritter'				=> 'assets/css/jquery.gritter.css',
		'amaran'				=> 'assets/css/amaran.min.css',
		'animate'				=> 'assets/css/animate.min.css',
		'ggpopover'			=> 'assets/css/ggpopover.css',
);

$config['preload_styles']	= array ('bootstrap', 'bootstrap-responsive', 'style', 'green','amaran');
$config['default_header_view'] = 'header';
$config['default_navigation_view'] = 'navbar';
$config['default_body_view'] = 'body';
$config['default_sidebar_widget_wrapper_view'] = 'sidebar_widget_wrapper';
$config['default_sidebar_wrapper_view'] = 'sidebar_wrapper';
$config['default_footer_view'] = 'footer';
$config['favicon'] = 'seal.ico';
$config['student_images_folder'] = 'assets/img/students/';
$config['employee_images_folder'] = 'assets/img/employees/';
$config['seal'] = 'assets/img/hnuseal.png'; 
$config['no_image_placeholder_female'] = 'assets/img/placeholder-female.jpg';
$config['no_image_placeholder_male'] = 'assets/img/placeholder-male.jpg';
$config['tab_container_view'] = 'common/tab_container_view';
$config['mysql_date_format'] = '%M %e, %Y';
$config['mysql_month_day_format'] = '%M %e';
$config['terms'] = array(
		'1'=>'First Semester',
		'2'=>'Second Semester',
		'3'=>'Summer');
$config['genders'] = array(
		'M'=>'Male',
		'F'=>'Female',
		''=>' ');
$config['civil_statuses'] = array(
		'Single',
		'Married',
		'Divorced',
		'Widowed',
		'Widower'
		);
$config['country_id'] = 137;
$config['province_id'] = 49;
$config['city_id'] = 960; //this should be based on what is in the database...
$config['stay_in_city'] = array (
				'N' => 'Not Staying in Tagbilaran',
				'P' => 'Home with parents',
				'F' => 'Staying with relatives/family friends',
				'A' => 'Apartment/Dormitory/Boarding House',
				
		);

$config['student_basic_info'] = array(
			'fname' 			=> 'First Name', 
			'mname' 			=> 'Middle Name',
			'lname'				=> 'Family Name',
			'dbirth_formatted'	=> 'Date of Birth',
			'gender'			=> 'Gender',
			'civil_status' 		=> 'Civil Status',
			'abbreviation'		=> 'Academic Program',
			'year_level'		=> 'Current Year Level',
			'citizenship'		=> 'Citizenship',
			'religion'			=> 'Religion',
			'phone_number'		=> 'phone_number',
		);
$config['student_address_info'] = array(
			'full_birth_address' => 'Birth Place',
			'full_home_address'		=> 'Home Address',
			'full_city_address'		=> 'City Address',
			);

$config['student_family_info']	= array(
			'fathers_name'		=> 'Father\'s Name',
			'fathers_number'	=> 'Father\'s Number',
			'fathers_address'	=> 'Father\'s Address',
			'mothers_name'		=> 'Mother\'s Name',
			'mothers_number'	=> 'Mother\'s Number',
			'mothers_address'	=> 'Mother\'s Address',
			'num_brothers'		=> 'Number of Brothers',
			'num_sisters'		=> 'Number of Sisters',
			);

$config['student_support_info']	= array(
			'occupation'		=> 'Occupation',
			'relation'			=> 'Relation',
			'address'			=> 'Address',
		);

$config['educational_background']	= array(
		'primary'		=> 'Primary',
		'intermediate'	=> 'Intermediate',
		'secondary'		=> 'Secondary',
);


$config['student_emergency_info']	= array(
		'emergency_notify'		=> 'Person to Notify',
		'emergency_address'		=> 'Address',
		'emergency_telephone'	=> 'Telephone Number',
		'emergency_email_address' => 'Email Address',
);
$config['blood_types']	= array(
		'A+','A-','B+','B-','AB+','AB-','0+','O-'
		);

$config['periods'] = array(
		'prelim', 'midterm', 'pre-Final', 'final');
$config['periods_to_be_graded'] = array(
		'prelim', 'midterm', 'finals');
$config['enrollment_year_levels'] = array(
			0 => 'New Enrollees',
			1 => 'First Year',
			2 => 'Second Year',
			3 => 'Third Year',
			4 => 'Fourth Year',
			5 => 'Fifth Year',
			6 => 'Graduate School'
		);
//Grade specific Regex... to find out if errors in the grade entries exists...
$config['basic_grade_regex'] = "/^(NA|DR|INE|WD|[1-4]\.[0-9]|5\.0)$/i";
$config['finals_grade_regex'] = "/^(NA|IP|DR|INC|WD|NC|[1-2]\.[0-9]|3\.0|5\.0)$/i";

$config['currency_sign'] = 'Php';
$config['finance_allowed_ips'] = array('10.1.1.107', '10.1.1.103', '10.1.1.105', 'fe80::e5f5:95f5:fd1d:199f', 'fe80::75ee:5581:1af2:97be', 'fe80::71f1:f354:d64d:138e', '::1', '127.0.0.1', '10.1.1.101','10.1.1.102');

$config['nonce_error_message'] = "An error was seen while processing submitted data. Please don't resubmit forms. Thank you.";

$config['tor_upload_config'] = array(
								'upload_path' => './assets/img/tors/',
								'allowed_types' => 'gif|jpg|png',
								'max_width'  => '2000',
								'max_height'  => '2000',
								);
$config['student_upload_config'] = array(
								'upload_path' => './assets/img/students/',
								'allowed_types' => 'gif|jpg|png',
								'max_width'  => '2000',
								'max_height'  => '2000',
								'overwrite'		=> TRUE,
);
$config['tor_folder_url'] = 'assets/img/tors/';

$config['image_config'] = array(
	'image_library' => 'gd2',
	'create_thumb' => FALSE,
	'maintain_ratio' => FALSE,
	);

$config['profile_image_max_width'] = '150'; //in terms of pixels

$config['roles'] = array(
		'accountant',
		'accounts',
		'audit',
		'cashier',
		'dean',
		'developer',
		'dric',
		'faculty',
		'hrdmo',
		'posting',
		'psychometrician',
		'registrar',
		'rsclerk',
		'sao',
		'teller',
		'evaluator',
		'counselor',
		'studentwin',
		'ric',
		'migrator',
		'gharvest',
		'ReservationOfficer',
		'office',
		'shschair',
		'scholarship',
		'classadviser',
		'qa',
		'president',
		'teller2',
		'principal',
		'sectionadviser',
		'eofficer',
		);
$config['roles_with_offices'] = array('sao');
$config['roles_with_colleges'] = array('dean', 'faculty');
$config['roles_for_students'] = array('teller', 'sao', 'dric', 'developer', 'posting', 'accounts', 'evaluator','gharvest'); // ra added posting accounts 12/2/13, added evaluator 02/13/2014, added gharvest 9/13/14
// $config['roles_for_students'] = array('teller', 'sao', 'dric', 'developer','studentwin','evaluator');
$config['roles_barred_from_internet_access'] = array('audit','cashier','posting'); // evaluator
$config['password_length'] = 7; //generated password length...
$config['loading_image'] = 'assets/img/loading.gif';

$config['ajax_strict'] = FALSE;

$config['payment_method_keys'] = array(
		'cash'		=> array(
					'amount'),
		'check'		=> array(
					'amount',
					'bank_id',
					'check_date',
					'payee',
					'check_number'),
		'credit_card'	=> array(
					'amount',
					'credit_card_company',
					'credit_card_number',
					'credit_card_owner',
					'credit_card_expiry'),
		'debit_card'	=> array(
					'amount',
					'debit_card_company',
					'debit_card_number',
					'debit_card_owner',
					'debit_card_expiry',),
		'bank_payment'	=> array(
					'amount',
					'bank_code',
					'date_received',
					'depositor',));

$config['payment_methods'] = array(
		'cash' => 'Cash',
		'check' => 'Check',
		'credit_card' => 'Credit Card',
		'debit_card' => 'Debit Card',
		'bank_payment'=> 'Bank',
		);

$config['jzebra_applet'] = '
<applet name="jzebra" code="jzebra.PrintApplet.class" archive="%sassets/jzebra.jar" width="1px" height="1px">
	<param name="printer" value="zebra">
</applet>		';

$config['roles_allowed_to_print_assessment'] = array('accounts', 'posting');
$config['percentage_of_total_units'] = 100;
$config['tuition_factor'] = 0.0312;
$config['minify_exceptions'] = array('teller', 'registrar', 'sao', 'posting', 'accounts', 'cashier');
$config['teller_code_CR'] = 26;
$config['payers'] = array('offices'=>'Offices', 'other_payers_tenant'=>'Other Payers - Tenant', 'other_payers_non_tenant' => 'Other Payers - Non Tenant');

$config['allowed_browsers'] = array('chrome'=>'14',
									'firefox'=>'5',
									'safari'=>'5',
									'opera'=>'10',
									'internet explorer'=>'7');

$config['unsupported_browser_message'] = "Unsupported Browser! Please use an updated browser, thank you";

$config['errors'] = array(
						'401' => 'Unauthorized Access',
						'404' => 'Not Found|Document or file requested by the client was not found.',
						'500' => 'Internal Server Error|Request unsuccessful because of an unexpected condition encountered by the server.'
						);
$config['last_isis_db']='2013-10-18';
$config['student_max_feedback']=5;
$config['employee_max_feedback']=20;
//return to previous page after logging in
$config['return_to_referrer'] = true;
$config['log_benchmark'] = TRUE;
$config['show_student_on_hover'] = TRUE;

$config['big_logo'] = 'assets/img/hnuseal.png';

$config['teller_code_levels'] = array(
			1=>'Kindergarten',
			2=>'Kinder',
			3=>'Grade School',
			4=>'HS-Day',
			5=>'HS-Night',
			6=>'College',
			7=>'Graduate School',
			8=>'K-12 Day',
			9=>'K-12 Night',
			11=>'Preschool',
			12=>'Senior High',	
			99=>'Other Payors',
			);
$config['sms_api_config'] = array(
			'client_id'=>'hnumis',
			'api_key'=>'eu5WgIUpxpWbntIdsfhnf75gdQwIwl5CyryyOykOJYCUaW4IzUNSVRyJEXtv',
			// 'sms_api_url'=>'http://sms.hnu.edu.ph/sms-server/api/',
			// 'sms_api_url'=>'http://10.1.1.190/sms-server/api/',
			'sms_api_url'=>'http://10.2.3.202/sms-server/api/',
			//'sms_api_url'=>'http://10.2.3.211/sms-server/api/',
			//'sms_api_url'=>'http://http://10.1.22.200/sms-server/api/',
			);


/** $config['sms_api_config'] = array(
			'client_id'=>'hnumis',
			'api_key'=>'eu5WgIUpxpWbntIdsfhnf75gdQwIwl5CyryyOykOJYCUaW4IzUNSVRyJEXtv',
			'sms_api_url'=>'http://10.1.22.12/sms-server/api/'
			); **/

/* End of file config.php */
/* Location: ./application/config/config.php */
