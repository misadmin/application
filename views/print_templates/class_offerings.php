<div id="offerings_to_print" style="display:none;">
<?php
$max_line_count = 62;
$headers = array(
		array('id'=>'catalog_id', 'name'=>"Catalog ID", 'width'=>15),
		array('id'=>'section', 'name'=>"Section", 'width'=>10),
		array('id'=>'title', 'name'=>"Descriptive Title", 'width'=>65),
		array('id'=>'schedule', 'name'=>"Schedule", 'width'=>25),
		array('id'=>'room', 'name'=>"Room", 'width'=>10),
		array('id'=>'enrolled', 'name'=>"En'led/Max", 'width'=>15),
		array('id'=>'teacher', 'name'=>"Teacher", 'width'=>20),
		array('id'=>'Status', 'name'=>"Status", 'width'=>10),
); 
$this->print_lib->set_table_header ($headers);
echo $this->print_lib->bold() . "Course Offerings" . $this->print_lib->regular() . "\n";
echo $this->print_lib->condensed() . "\n---------------------------------------------------------------------------------------------------------------------------------------\n";
echo $this->print_lib->draw_table();
echo "\n---------------------------------------------------------------------------------------------------------------------------------------\n";
$line_count = 5;

foreach($courses_offered['result'] as $offered){
	$schedule = explode('<br />', $offered->schedule);
	$room_exploded = explode(" ", $offered->room);
	$room_exploded[(count($room_exploded) - 1)] = "";
	$room = implode('', $room_exploded);
	echo str_pad(substr(trim($offered->course_code), 0, 15), 15) . str_pad(substr(trim($offered->section_code), 0, 6), 6) . str_pad(substr(trim($offered->descriptive_title), 0, 45), 45, " ", STR_PAD_RIGHT) . " " . str_pad(substr($schedule[0], 0, 23), 23) . " " . str_pad(substr($room, 0, 10), 10) . " " . str_pad(($offered->enrolled_count . "/". $offered->max_enrollment_count), 6) . " " . str_pad(substr($offered->teacher, 0, 16), 16) . " " . substr($offered->status, 0, 9) . "\n";
	$line_count++;
	if($line_count >= $max_line_count){
		$line_count = 1;
		echo "\n\n\n\n\n";
	}
}
echo $this->print_lib->no_condensed();
?></div>
<script>
function print(content) {
    var dapplet = document.jzebra;
    if (dapplet != null) {
       dapplet.setEndOfDocument("P1\n");
       dapplet.append('\x1B\x40');
       dapplet.append(content);
       dapplet.print();
	}
}

$().ready(function(){
	$('#print_offerings').on('click', function(){
		var content_to_print = $('#offerings_to_print').text();
		var vapplet = document.jzebra;
		if (vapplet != null) {
			// Searches for default printer
			vapplet.findPrinter();
		}
		print(content_to_print);		
	});
});
</script>
