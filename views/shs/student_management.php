
<div id="show_extracted_items">
	<div class="tabbable" style="width:99%; margin:0 auto; margin-top:15px;" >
		<ul class="nav nav-tabs">
			<?php 
				if ($student_type == 'shs') {
			?>
					<li class="dropdown <?php if($active_tab == 'class_schedules') print("active"); ?>" >
						<a href="#class_schedules" data-toggle="tab">Schedules</a>
					</li>
					<li class="dropdown <?php if($active_tab == 'report_card') print("active"); ?>" >
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Report Card <b class="caret"></b></a>
						<ul class="dropdown-menu" id="report_card">
							<li <?php if($active_tab1 == 'student_grades') print("class='active'"); ?> >
								<a href="#student_grades" data-toggle="tab">Grades</a>
							</li>
							<li <?php if($active_tab1 == 'student_attendance') print("class='active'"); ?> >
								<a href="#student_attendance" data-toggle="tab">Attendance</a>
							</li>
							<li <?php if($active_tab1 == 'student_values') print("class='active'"); ?> >
								<a href="#student_values" data-toggle="tab">Observed Values</a>
							</li>
							<li <?php if($active_tab1 == 'generate_report_card') print("class='active'"); ?> >
								<a href="#report_card_pdf" data-toggle="tab">Generate Report Card</a>
							</li>
						</ul>
					</li>	
					<li class="dropdown <?php if($active_tab == 'assessment') print("active"); ?>" >
						<a href="#assessment" data-toggle="tab">Assessment</a>
					</li>

			<?php 
				}
			?>
			<li class="dropdown <?php if($active_tab == 'enrollment') print("active"); ?>" >
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Enrollment <b class="caret"></b></a>
				<ul class="dropdown-menu" id="enrollment">
					<?php 
						if ($show_register_tab) {
					?>
							<li <?php if($active_tab1 == 'register') print("class='active'"); ?> ><a href="#register" data-toggle="tab">Register</a></li>
					<?php 
						}
					?>
					
					<?php 
						if ($student_type == 'shs') {
					?>
						<li <?php if($active_tab1 == 'enroll_student') print("class='active'"); ?>><a href="#enroll_student" data-toggle="tab">Enroll</a></li>
						<li <?php if($active_tab1 == 'assign_section') print("class='active'"); ?>><a href="#assign_section" data-toggle="tab">Assign Section</a></li>
						<li <?php if($active_tab1 == 'prospectus_used') print("class='active'"); ?>><a href="#prospectus_used" data-toggle="tab">Prospectus Used</a></li>
						<li <?php if($active_tab1 == 'withdraw_enrollment') print("class='active'"); ?>><a href="#withdraw_enrollment" data-toggle="tab">Withdraw Enrollment</a></li>
					<?php 
						}
					?>
				</ul>			
			</li>
			<?php 
				if ($student_type == 'shs') {
					if (!$assess['assess_record']) {
			?>
					<li class="dropdown <?php if($active_tab == 'transfer_student') print("active"); ?>" >
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Transfer Student <b class="caret"></b></a>
						<ul class="dropdown-menu" id="transfer_student">
							<li <?php if($active_tab1 == 'to_section') print("class='active'"); ?> ><a href="#to_section" data-toggle="tab">To Another Section</a></li>
							<li <?php if($active_tab1 == 'to_strand') print("class='active'"); ?>><a href="#to_strand" data-toggle="tab">To Another Strand</a></li>
						</ul>			
					</li>
			<?php 
					}
			?>
					<li class="dropdown <?php if($active_tab == 'change_pwd') print("active"); ?>" >
						<a href="#change_pwd" data-toggle="tab">Change Password</a>
					</li>
			<?php 
				}
			?>
		</ul>
		
	
		<div class="tab-content" >
			<?php 
				if ($student_type == 'shs') {
			?>
					<div class="tab-pane <?php if($active_tab == 'assessment') print("active"); ?>" id="assessment">
						<?php
							$this->load->view('shs/student/student_assessment');
						?>
					</div>
					<div class="tab-pane <?php if($active_tab == 'class_schedules') print("active"); ?>" id="class_schedules">
						<?php
							
							$this->load->view('shs/student/student_class_schedules');
						?>
					</div>
					
					<div class="tab-pane <?php if($active_tab == 'student_grades') print("active"); ?>" id="student_grades">
						<?php
							$this->load->view('shs/student/student_grades');
						?>
					</div>
					<div class="tab-pane <?php if($active_tab == 'student_attendance') print("active"); ?>" id="student_attendance">
						<?php
							$this->load->view('shs/class_adviser/attendance_mainpage');
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'student_values') print("active"); ?>" id="student_values">
						<?php
							$this->load->view('shs/class_adviser/observed_values_mainpage');
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'generate_report_card') print("active"); ?>" id="report_card_pdf">
						<?php
							$this->load->view('shs/registrar/generate_report_card_mainpage');
						?>
					</div>
					
					<div class="tab-pane <?php if($active_tab1 == 'to_section') print("active"); ?>" id="to_section">
						<?php
							$this->load->view('shs/student/transfer_to_section');
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'to_strand') print("active"); ?>" id="to_strand">
						<?php
							$data = array(
										"academic_terms_id" => $academic_terms_id, 
										"strands"=>$strands,
										"to_sections"=>$to_sections,
										);
										
							$this->load->view('shs/student/transfer_to_strand', $data);
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'change_pwd') print("active"); ?>" id="change_pwd">
						<?php
							$this->load->view('student/password');
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'enroll_student') print("active"); ?>" id="enroll_student">
						<?php
							$data=array(
										'payments'=>$payments,
										'idnum'=>$idnum,
										'terms_to_enroll'=>$terms_to_enroll,
										'is_registered'=>$is_registered,
										'is_enrolled'=>$is_enrolled,								
									);
							$this->load->view('shs/student/enroll_student', $data);
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'assign_section') print("active"); ?>" id="assign_section">
						<?php
							$data=array(
										'block_sections'=>$block_sections,
										'idnum'=>$idnum,
										'is_registered'=>$is_registered,
										'is_enrolled'=>$is_enrolled,								
										'is_sectioned'=>$is_sectioned,	
										'student_history_id'=>$student_histories_id,	
									);
							$this->load->view('shs/student/assign_section', $data);
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'prospectus_used') print("active"); ?>" id="prospectus_used">
						<?php
							$data=array(
										'block_sections'=>$block_sections,
										'idnum'=>$idnum,
										'is_registered'=>$is_registered,
										'is_enrolled'=>$is_enrolled,								
										'is_sectioned'=>$is_sectioned,	
										'student_history_id'=>$student_histories_id,
										'prospectus_list'=>$prospectus_list,
										'prospectus_assigned'=>$prospectus_assigned[0]->prospectus_id,
									);
							$this->load->view('shs/student/prospectus_used', $data);
						?>
					</div>
					<div class="tab-pane <?php if($active_tab1 == 'withdraw_enrollment') print("active"); ?>" id="withdraw_enrollment">
						<?php
							$data=array(
										'block_sections'=>$block_sections,
										'idnum'=>$idnum,
										'is_registered'=>$is_registered,
										'is_enrolled'=>$is_enrolled,								
										'is_sectioned'=>$is_sectioned,	
										'student_history_id'=>$student_histories_id,
										'prospectus_list'=>$prospectus_list,
										'prospectus_assigned'=>$prospectus_assigned[0]->prospectus_id,
										'totally_withdrawn'=>$totally_withdrawn,
									);
							$this->load->view('shs/student/withdraw_enrollment', $data);
						?>
					</div>
			<?php 
				}
			?>
			
				<div class="tab-pane <?php if($active_tab1 == 'register') print("active"); ?>" id="register">
					<?php
						$data=array(
									'strands'=>$strands,
									'acad_terms'=>$acad_terms,
									'current_term'=>$term,
									'is_registered'=>$is_registered,
									'idnum'=>$idnum,
								);
						$this->load->view('shs/student/register_student', $data);
					?>
				</div>

		</div>

	</div>

</div>
	