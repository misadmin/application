<?php

class Student extends CI_Controller {
	
	private $navigation;
	private $role;
	private $userinfo;
	private $ConflictEnrollmentID;
	
	/**
	 * Please put a description
	 * @var very_unknown_type
	 */
	private $CannotContinue = TRUE;
	
	private $student_info;
	
	private $AlertMsg;
	private $AlertType;
	private $assessment;
	private $student_image;
	public $status_schedule;
	private $new_enrollee = FALSE;
	
	function __construct(){		
		parent::__construct();
		$this->common->need_auth();

		$this->userinfo = $this->session->all_userdata(); //print_r($this->userinfo); die();
		$this->role = $this->uri->segment(1); // a student can have other roles other than a student...
		
		$this->content_lib->set_title ('Student | ' . $this->config->item('application_title'));
		$navbar_data = array(
				'name'			=> $this->userinfo['fname'] . " " . $this->userinfo['lname'],
				'role'			=> $this->role,
			); //removed by tatskie: 'roles' => $this->session->userdata('roles'),				
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('hnumis/Student_Model');
		$this->load->model('hnumis/Enrollments_Model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('hnumis/Prospectus_Model');
		$this->load->model('hnumis/Offerings_Model');
		$this->load->model('hnumis/Grades_Model');
		$this->load->model('hnumis/Rooms_Model');
		$this->load->model('hnumis/Programs_Model');
		$this->load->model('teller/assessment_model');
		$this->load->model('teller/teller_model');
		$this->load->model('ledger_model');
		$this->load->model('accounts/Accounts_model');
		$this->load->model('col_students_model');
		$this->load->helper('student_helper');
		
		$this->remarks = $this->Student_Model->CheckForNewRemarks($this->userinfo['idno']);
		$this->load->model('academic_terms_model');
		$current_academic_term = $this->academic_terms_model->current_academic_term();

		$grade_level = array(11,12);

		if(!is_college_student($this->userinfo['idno'])) { 
			$navbar_data['menu'] = array(
					'Financials'	=>'financials',
			);
		} else {

			if (in_array($this->userinfo['yr_level'],$grade_level)) { //student is SHS
				$navbar_data['menu'] = array(
						'Academics'		=> array(
												'My Academics' => 'academics',
												//'Prospectus'	=> 'view_prospectus'
											),
						'Financials'	=>'financials',	
				);		
			} else {
				$navbar_data['menu'] = array(
						'Enrollment'	=>'enrollment_check',
						'Offerings' 	=>'offerings',
						'Academics'		=> array(
												'My Academics' => 'academics',
												'Prospectus'	=> 'view_prospectus'
											),
						'Financials'	=>'financials',
							
				);
			}
		}
		
		$this->content_lib->set_navbar_content('', $navbar_data);
		$this->status_ = $this->academic_terms_model->what_period() . " &middot; " . $current_academic_term->term . " &middot; SY " . $current_academic_term->sy;
		$this->load->model('student_common_model');
		$this->student_info = $this->student_common_model->my_information($this->userinfo['idno']);
		
		//this is used for avatar$this->userinfo['idno']
		$port = substr($this->userinfo['idno'], 0, 3);
		
		if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$this->userinfo['idno']}.jpg"))
			$this->student_image = base_url($this->config->item('student_images_folder') . "{$port}/{$this->userinfo['idno']}.jpg");
		else {
			if ($this->student_info->gender == 'F')
				$this->student_image = base_url($this->config->item('no_image_placeholder_female')); else
				$this->student_image = base_url($this->config->item('no_image_placeholder_male'));
		}
		
		$this->content_lib->set_avatar_image($this->student_image);
		//print_r($this->session->all_userdata()); die();
		
	}
	
	
	public function index (){
		$this->load->helper('student_helper');
		$this->load->model('common_model');		
		$this->content_lib->set_title ('Student | Dashboard | ' . $this->config->item('application_title'));

		$grade_level = array(11,12);

		if (in_array($this->student_info->year_level,$grade_level)) { //student is SHS
			$dcontent = array(
					'image'		=> $this->student_image,
					'idnum'		=> $this->userinfo['idno'],
					'name'		=> $this->student_info->fname ." " . $this->student_info->mname. " " . $this->student_info->lname,
					'course'	=> $this->student_info->abbreviation,
					'level'		=> " Grade ".$this->student_info->year_level." ".$this->student_info->section_name,
					'full_home_address'	=> $this->student_info->full_home_address,
					'full_city_address'	=> $this->student_info->full_city_address,
					'religion'	=> $this->student_info->religion,
					'phone_number'=>$this->student_info->phone_number,
			);
		} else {
			$dcontent = array(
					'image'		=> $this->student_image,
					'idnum'		=> $this->userinfo['idno'],
					'name'		=> $this->student_info->fname ." " . $this->student_info->mname. " " . $this->student_info->lname,
					'course'	=> $this->student_info->abbreviation,
					'level'		=> $this->student_info->year_level,
					'full_home_address'	=> $this->student_info->full_home_address,
					'full_city_address'	=> $this->student_info->full_city_address,
					'religion'	=> $this->student_info->religion,
					'phone_number'=>$this->student_info->phone_number,
				
			);
		}
		
		
		$dcontent['error_msg'] = "";
		if (!$this->CannotContinue) {
			$dcontent['error_msg'] = "You cannot enroll anymore because one of your required subjects has a failing grade. See your dean as soon as possible!";
		}
		
		if ($this->remarks) {
			$dcontent['error_msg'] = "Please tick your Messages tab to view new messages!";
		}
		
		$dcontent['profile_image_max_width'] = $this->config->item('profile_image_max_width');
		
		
		//added by tatskie:				
		if ( ($count_unresolveds = $this->common_model->count_unresolveds($this->userinfo['idno'])) == TRUE ){
			$dcontent['unresolveds'] = $count_unresolveds[0]->unresolveds;
			$count_unresolveds = null; unset($count_unresolveds );
		}else{
			$dcontent['unresolveds'] = 0;
		}
		
		
		
		$this->content_lib->enqueue_body_content('student/dashboard', $dcontent);
		$this->load->library('tab_lib');
		$this->tab_lib->enqueue_tab("My Information", 'common/student_information_full', $this->student_info, 'student_info', TRUE);
		$all_remarks = $this->Student_Model->ListNewRemarks($this->userinfo['idno']);
		
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){		
			if($action = $this->input->post('action')){			
				if ($feedback_count = $this->common_model->feedback_counter($this->userinfo['idno'])){
					if ($feedback_count >= $this->config->item('student_max_feedback')){
						$this->content_lib->set_message('Maximum allowed number of feedbacks is reached', 'alert-error');										
						$this->content_lib->content();
						return;
					}
				}
				switch ($action){
					case 'new_feedback':
						$comment = sanitize_text_field($this->input->post('comment'));
						if (trim($comment) == '')
							$this->content_lib->set_message('Feedback field is required!', 'alert-error');
						else{
							if ( (int)$this->input->post('recipient_id') !== 0 ) {									
								if($insert_id = $this->common_model->add_feedback($this->userinfo['idno'], $comment, 'students_idno',$this->input->post('recipient_id'))){
									$this->content_lib->set_message('Feedback successfully submitted!</br>', 'alert-success');
								}else{
									$this->content_lib->set_message('Error in submission!', 'alert-error');
								}
							}else{
								$this->content_lib->set_message('Please select recipient of your feedback!', 'alert-error');								
							}
						}
						break;
				}
			}		
		}else{
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');				
		}
		$this->load->library('form_lib');
		//$comment = array();
		
		$last_logins = array('results' => $this->student_common_model->last_logins($this->userinfo['idno']));
		
		$feedback_count = $this->common_model->feedback_counter($this->userinfo['idno']);
		$feedback_status = $this->config->item('student_max_feedback') - $feedback_count  . "/" . $this->config->item('student_max_feedback');		


		// the comments feature in SAO is displayed here.  however, 
		// the value of $all_remarks is overwritten with $all_comments
		// as of 3.19.2019, $all_remarks doesn't seem to have a value - Toyet 3.19.2019
		$all_comments = $this->col_students_model->get_student_comments($this->userinfo['idno']);
		foreach ($all_comments as $row){
			$all_remarks[] = (object) array( "remark_date"=>$row->term.'<br>'.$row->year,
								             "remark"=>$row->comments ); 
		}

		$this->tab_lib->enqueue_tab("Messages", 'student/view_remarks', array('all_remarks'=>$all_remarks), 'view_remarks', FALSE);
		$this->content_lib->enqueue_sidebar_widget('common/last_logins', $last_logins, 'Your Last Logins:', 'in');		
		if ($feedback_count < $this->config->item('student_max_feedback')){
			
			//print_r($this->userinfo);die();			
			
			$recipients = $this->common_model->list_recipients_for_students($this->userinfo['prospectus_id']);
			
			$data_fb['recipients'] = array();
			
			$data_fb['recipients'][0]='--Select Recipient--';
			if ($recipients) {
				foreach ($recipients as $recipient){
					$data_fb['recipients'][$recipient->recipient_id] = $recipient->recipient;
				}
			}
				
			
			$this->content_lib->enqueue_sidebar_widget('feedbacks/feedbacks_view', $data_fb, 'Submit Feedback [Allowed: '.$feedback_status.']:', 'in');
			
		}				
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content().'<br><br><br><br>');
		$this->content_lib->content();
	}
	
	public function not_allowed(){
		$this->content_lib->set_message('Sorry, but you re not allowed to access this item at the moment!<br>' . $this->session->userdata('not_allowed_to_enroll_message'), 'alert-error');
		$this->content_lib->content();		
	}
	
	public function academics (){
		
		$this->load->library('tab_lib');
		$this->load->model('financials/Tuition_Model');
		$this->content_lib->set_title ('Student | Academics | ' . $this->config->item('application_title'));
		
		$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($this->session->userdata('idno'));
		
		//print($this->input->post('academic_term')); die();
		if ($this->input->post('academic_term')) {
			$academic_terms_id = $this->input->post('academic_term');
			$terms=$this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_term')); 
			$acad_term=$terms->term;
		} else {
			$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($this->session->userdata('idno'));
			$academic_terms_id = $academic_terms[0]->id;
			$acad_term = $academic_terms[0]->id;
		}
		
		
		$courses = $this->Courses_Model->student_courses_from_academic_terms ($this->session->userdata('idno'), $academic_terms_id);
		
		$grades = $this->Enrollments_Model->student_grades($this->session->userdata('idno'));

		$prospectus = $this->Prospectus_Model->student_prospectus($this->Prospectus_Model->student_prospectus_ids($this->session->userdata('idno')));
		
		$prospectus_history = $this->Prospectus_Model->ListProspectusTerms($this->session->userdata('prospectus_id'));
		$my_tab = array_fill(0,7,FALSE);
		$my_tab[0]= TRUE;
		
		
		$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
		switch ($step) {
			case 1:
				break;
			case 2:
				$this->generate_class_schedule_pdf($this->input->post('academic_terms_id'), $this->input->post('max_units'));
				return;
			case 3:
				if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
					if ($this->Student_Model->update_student_history($this->input->post('history_id'), 
							array('year_level'=>$this->input->post('new_yr_level')))) {
						$this->content_lib->set_message('Year Level Successfully updated!', 'alert-success');	
					} else {
						$this->content_lib->set_message('ERROR: Year Level NOT Updated!', 'alert-error');	
					} 
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
				}
				$my_tab = array_fill(0,7,FALSE);
				$my_tab[3]= TRUE;
				$my_tab[6]= TRUE;
				
				break;					
		}
		
		$student = new stdClass();
		$student->prospectus_id = $this->userinfo['prospectus_id'];
		$student->year_level = $this->userinfo['yr_level'];
		$student->students_idno = $this->userinfo['idno'];
		//$student->academic_terms_id = $this->userinfo['academic_terms_id'];
		$student->academic_terms_id = $academic_terms_id;
		$student->id = $this->userinfo['student_histories_id'];
		
		$student_units = $this->Student_Model->getUnits($student, $academic_terms_id);
		$assessment_date_acad = $this->Tuition_Model->getAssessmentDate($this->session->userdata('idno'),$academic_terms_id );

		$this->student_info = $this->student_common_model->my_information($this->userinfo['idno']);

		/*
		 * ADDED: 4/17/15 by genes
		 */
		$s_history = $this->Student_Model->get_StudentHistory_id($this->userinfo['idno'], $academic_terms_id);

		if ($s_history) {
			$s_units = $this->Student_Model->getUnits($s_history, $acad_term);
			$s_history_id=$s_history->id;
		} else {
			$s_units['max_bracket_units']=0;
			$s_history_id=0;
		}
		/*
		 * ADDED: 4/17/15
		 * up to here only
		 */
		
		if (!$student_units) {
			$student_units['max_bracket_units']=0;
		}
			
		$grade_level = array(11,12);

		if (in_array($this->student_info->year_level,$grade_level)) { //student is SHS

			$this->load->model('hnumis/shs/shs_student_model');
			$this->load->model('hnumis/shs/shs_grades_model');
			$this->load->model('hnumis/shs/section_model');

			$class_schedules = $this->shs_student_model->ListClassSchedules($s_history_id); 
			$grades          = $this->shs_grades_model->List_Student_Grades($this->userinfo['idno']); 
			//print_r($s_history); die();
			$block_section   = $this->section_model->getSection($s_history->block_sections_id); 

			$this->tab_lib->enqueue_tab ('Schedule', 'shs/student/student_class_schedules', 
								array('academic_terms'=>$academic_terms,
									'selected_term'=>$acad_term,
									'class_schedules'=>$class_schedules,
									'idnum'=>$this->userinfo['idno'],
									'show_pulldown'=>TRUE,
									'block_section_data'=>$block_section,
									), 
								'schedule', $my_tab[0]);

			$this->tab_lib->enqueue_tab ('Grades', 'shs/student/student_grades', array('grades'=>$grades), 'grades', FALSE);					
							
			//$this->tab_lib->enqueue_tab ('My Prospectus', 'student/prospectus', array('prospectus'=>$prospectus), 'prospectus', $my_tab[4], 'prospectus');
							
		} else {

			$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', 
							array('schedules'=>$courses, 
								'assessment_date'=>$assessment_date_acad, 
								'academic_terms'=>$academic_terms,
								'academic_terms_id'=>$academic_terms_id, 
								'max_units'=>$s_units['max_bracket_units'],
								'student_histories_id'=>$s_history_id,
							), 'schedule', $my_tab[0]);

			$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', $my_tab[2]);

		
			$this->tab_lib->enqueue_tab('Prospectus',
				'',
				array(),
				'prospectus',
				$my_tab[3]);
			$this->tab_lib->enqueue_tab ('My Prospectus', 'student/prospectus', array('prospectus'=>$prospectus), 'prospectus', $my_tab[4], 'prospectus');
			$this->tab_lib->enqueue_tab ('Prospectus History', 'student/prospectus_history', 
											array('prospectus_terms'=>$prospectus_history, 
												 'student_idno'=>$this->userinfo['idno']), 
											'prospectus_history_tab', $my_tab[5], 'prospectus');
										
			$current = $this->AcademicYears_Model->getCurrentAcademicTerm();
			
			//print_r($this->userinfo); die();
			$acad_id = $this->Prospectus_Model->getProspectus($this->userinfo['prospectus_id']);
			$max_yr = $this->Programs_Model->getProgram($acad_id->academic_programs_id);
			
			$this->userinfo['max_yr_level'] = $max_yr->max_yr_level;
			
			$data = $this->Student_Model->AssessYearLevel($this->userinfo);
			
			$this->tab_lib->enqueue_tab ('Year Level Assessment', 'student/yr_level_assessment', 
											array(
												'current'=>$current, 
												'data'=>$data, 
												'current_yr_level'=>$this->ordinalSuffix($this->student_info->year_level),
												'my_yr_level'=>$this->student_info->year_level,
												'student_histories_id'=>$this->userinfo['student_histories_id']
											), 'assessment', $my_tab[6], 'prospectus');

		}									

		$tab = $this->tab_lib->content();
		
		$this->content_lib->enqueue_body_content('', $tab);
		$this->content_lib->content();
		
		
	}
	
	
	private function ordinalSuffix( $n )
	{
	  return $n.date('S',mktime(1,1,1,1,( (($n>=10)+($n>=20)+($n==0))*10 + $n%10) ));
	}
	
	
	public function offerings() {

		$this->content_lib->enqueue_footer_script('data_tables');
		$this->DisplayCourses(TRUE);

	}
	
	
	public function enroll(){
		if($this->session->userdata('enrollment_checked') != 'successful'){
			redirect(site_url('student/enrollment_check'));
		}
		
		$this->content_lib->enqueue_footer_script('data_tables');
		
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){

			$this->content_lib->set_title ('Student | Enrollment | ' . $this->config->item('application_title'));
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
			
				switch ($step) {
					case 1://when he opens
						$this->DisplayCourses();
						break;
					case 2://student enrolls a course
						//print_r($this->session->all_userdata()); die();
						if ($this->StudentOKToEnroll($this->input->post('course_offerings_id'),$this->input->post('courses_id'),
														$this->session->userdata('prospectus_id'),$this->input->post('advised'),$this->input->post('elective'))) {					
							$data['students_idno']  = $this->userinfo['idno'];
							$data['academic_terms_id'] = $this->session->userdata('academic_terms_id');
							$data['course_offerings_id']   = $this->input->post('course_offerings_id');	
							$data['ip_address']          = $this->input->ip_address();	
							$data['max_enrolee']   = $this->input->post('max_enrolee');
							$data['parallel_no']   = $this->input->post('parallel_no');
							
							/*
							 * EDITED: 4/9/15
							 * NOTE: we see to it that student_histories_id must be current
							 * $isAssessed = $this->assessment_model->CheckStudentHasAssessment($this->session->userdata('student_histories_id')); 
							 */
							
							$this->load->model('hnumis/enrollments_model');
							$current_academic_term = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
							$current_student_history = $this->Student_Model->get_StudentHistory_id($this->userinfo['idno'], $current_academic_term->id);
							$isAssessed = $this->assessment_model->CheckStudentHasAssessment($current_student_history->id);

							if(!$isAssessed){
								$result = $this->Student_Model->EnrollStudentToOffering($data);
								if($result){
									$this->AlertMsg = "Successfully Enrolled!";
									$this->AlertType = "Success";
								}else{
									$this->AlertMsg = "Course not enrolled. Class is full.";
									$this->AlertType = "Error";
								}
							}else{
								//$this->db->trans_start();
								$result = $this->Student_Model->EnrollStudentToOffering($data, 'added');
								
								if($result){
									$student_info = $this->Student_Model->getStudentInfo($this->session->userdata('student_histories_id'));
									//print_r($student_info );die();
									$adj['date'] = DATE("Y/m/d");
									$payer_info = $this->teller_model->get_student($student_info->students_idno);
									//print_r($payer_info);die(); 
									$adj['payers_id'] = $payer_info->payers_id;
									$adj['academic_programs_id'] = $payer_info->ap_id; //added by tatskie Aug 23, 2014
									$adj['year_level'] = $payer_info->year_level; //added by tatskie Aug 23, 2014								
									$adj['type'] = "Debit";
									$tuition_basic_rate = $this->Student_Model->getTuitionRateBasic($student_info->acad_program_groups_id, $student_info->academic_terms_id, $student_info->year_level);
									$tuition_others_rate = $this->Student_Model->getTuitionOthersRate($result, $student_info->academic_terms_id, $student_info->year_level);
									$course_info = $this->Courses_Model->getCourseInfo($result);
									$adj['description'] = "ADD SUBJECT: " . $course_info->course_code;
									$cur_aca_yr = $this->AcademicYears_Model->current_academic_year();
									$lab_amount = $this->Accounts_model->get_lab_fee($course_info->id, $cur_aca_yr->id);
									//print_r($lab_amount); die();
									$lab_fee = 0;
									if($lab_amount){
										$lab_fee = $lab_amount->rate;
									}
									if($tuition_others_rate){
										$adj['amount'] = $tuition_others_rate->rate * $course_info->paying_units + $lab_fee;
									}else{
										$adj['amount'] = $tuition_basic_rate->rate * $course_info->paying_units + $lab_fee;
									}
									
									$adj['remark'] = 'Added by Student: ID no. '. $student_info->students_idno;
									$adj['empno'] = $student_info->students_idno;
									$term = $this->academic_terms_model->getCurrentAcademicTerm();
									$adj['term'] = $term->id;
									//$data, $posted = 'N', $posted_by = NULL, $posting_date = NULL, $bed = FALSE
									$postAdded = $this->ledger_model->add_adjustment($adj, 'Y', $student_info->students_idno, DATE("Y/m/d H:i:s"), FALSE);
									//print_r($postAdded); die();
									if($postAdded){
										$this->db->trans_complete();
										$this->AlertMsg = "Successfully Enrolled!";	
										$this->AlertType = "Success";
									}else{
										$this->db->trans_rollback();
										$this->AlertMsg = "Error in adjusting...";
										$this->AlertType = "Error";
									}
								}else{
									$this->AlertMsg = "Course not enrolled. Class is full.";
									$this->AlertType = "Error";
									$this->db->trans_rollback();
								}
							}
						}						
						$this->DisplayCourses();						
						break;
						
					case 3: //withdraw Course by Student
						//print_r($this->session->userdata); die();
						$this->db->trans_start();
						$stud_data['enrollments_id']  = $this->input->post('enrollments_id');
						$stud_data['students_idno']   = $this->session->userdata('idno');
						
 						if ($this->Student_Model->TransferDeletedToHistory($stud_data)) {
							if ($this->Student_Model->WithdrawCourse($this->input->post('enrollments_id'))) {
								$this->db->trans_complete();
								$this->AlertMsg = "Schedule Successfully Withdrawn!";
								$this->AlertType = "Success";
							} else {
								$this->db->trans_rollback();
								$this->AlertMsg = "ERROR: course not withdrawn!";
								$this->AlertType = "Error";
							}
						} else {
							$this->db->trans_rollback();
							$this->AlertMsg = "ERROR: course not copied to history! Please try again!";
							$this->AlertType = "Error";
						} 
						
						$this->DisplayCourses();					
						break;
						
					case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								$tab = 'assessment';
								break;	
				}
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');	
			$this->index();
		}
						
	}
	
	
	public function financials (){
		$this->content_lib->set_title ('Student | Financials | ' . $this->config->item('application_title'));
		$this->load->model('accounts/accounts_model');
		$this->load->model('teller/teller_model');
		$this->load->model('hnumis/enrollments_model');
		$this->load->model('accounts/fees_schedule_model');
		$this->load->library('shs/shs_student_lib');
		$this->load->model('hnumis/shs/shs_student_model');
		
		$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
		$result = $this->teller_model->get_student($this->userinfo['idno']);
		$ledger_data = $this->teller_model->get_ledger_data($result->payers_id);
		$unposted_students_payments = $this->teller_model->get_unposted_students_payment($result->payers_id);
		$this->load->library('balances_lib',
				array(
						'ledger_data'=>$ledger_data,
						'unposted_students_payments'=>$unposted_students_payments,
						'current_academic_terms_obj'=>$current_academic_terms_obj,
						'period'=>$this->academic_terms_model->what_period(),
						'period_dates'=>$this->academic_terms_model->period_dates(),
				));

		/*$port = substr($this->userinfo['idno'], 0, 3);
		
		$stud_img = FCPATH . $this->config->item('student_images_folder') . "{$port}/{$this->userinfo['idno']}.jpg"; 
		
		if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$this->userinfo['idno']}.jpg"))
			$this->student_image = base_url($this->config->item('student_images_folder') . "{$port}/{$this->userinfo['idno']}.jpg");
		else {
			if ($this->student_info->gender == 'F')
				$this->student_image = base_url($this->config->item('no_image_placeholder_female')); else
				$this->student_image = base_url($this->config->item('no_image_placeholder_male'));
		}
		
		print("<img src=".$stud_img.">");*/
		
		$tab = 'assessment';
		$result = $this->teller_model->get_student($this->userinfo['idno']);
		switch ($action = $this->input->post('action')){
			
		  		case 'change_term'		:
								$selected_history = explode("|", $this->input->post('history_id'));
								$selected_history_id = $selected_history[0];
								$selected_term_id = $selected_history[1];
								//print($result->student_histories_id);
								//die();
								$tab = 'assessment';
								
								break;	
		}
		//$assessment = $this->accounts_model->student_assessment($this->session->userdata('student_histories_id'), $this->session->userdata('yr_level'));
		
	
		$this->load->library('tab_lib');
		
		//$result = $this->teller_model->get_student($this->userinfo['idno']);
		
		//assessment form
		$inclusive_academic_term = $this->academic_terms_model->student_inclusive_academic_terms ($this->userinfo['idno']);
		
		$inclusive_academic_term = $inclusive_academic_term ? $inclusive_academic_term[0] : FALSE;
		$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
		$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
		$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id);
		
		//$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id);
		if(isset($selected_history_id)){
			$assessment = $this->accounts_model->student_assessment($selected_history_id);
			$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
			$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
			$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
			
			$hist_id = $selected_history_id;
				
			
		} else {
			$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
			$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
			$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
			$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
			
			$hist_id = $result->student_histories_id;
				
			
		}
		$lab_courses = array();
		$courses = array();
		
		$assessment_date = $this->Student_Model->get_student_assessment_date($hist_id);

		if (!$assessment_date){
			$transaction_date2=array();
			$assessment_date2[0]= (object)array('transaction_date' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . " + 1 day") ));
			$assessment_date = $assessment_date2;
		}
		
		
			if(!empty($dcourses)){
				foreach ($dcourses as $course) {
					if ($course->type_description == 'Lab' && empty($course->enrollments_history_id)) {
					   if($laboratory_fees){
					 		$lab_courses[] = array(
							'name'=>$course->course_code,
							'amount'=>(array_key_exists($course->id, $laboratory_fees) ? $laboratory_fees[$course->id] : 0),
							);
						}else{
							$lab_courses[] = array(
							'name'=>$course->course_code,
							'amount'=>0,
							);
						}
					}	
					$courses[] = array(
						'id'=>$course->id,
						'name'=>$course->course_code,
						'units'=>$course->credit_units,
						'pay_units'=>$course->paying_units,
						're_enrollments_id'=>$course->re_enrollments_id,
						'withdrawn_on'=>$course->withdrawn_on,
						'assessment_id'=>$course->assessment_id,
						'post_status'=>$course->post_status,
						'enrollments_history_id'=>$course->enrollments_history_id,
						'transaction_date'=>$course->transaction_date,
								
					);
				}
			}
		
	//	$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
		$ledger_data = array();
		$ledger_data = $this->teller_model->get_ledger_data($result->payers_id);
		$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms($this->session->userdata('idno'));
	
		$grade_level = array(11,12);

		if (is_college_student($this->session->userdata('idno'))) {
			
			if (in_array($result->year_level,$grade_level)) { //student is SHS
					
				$current_history  = $this->shs_student_model->get_StudentHistory_id($this->session->userdata('idno'), $student_inclusive_terms[0]->id); 
				$assess           = $this->shs_student_lib->StudentAssessments($current_history); 
					
				$this->tab_lib->enqueue_tab('Assessment', 'shs/student/student_assessment',
							array('assess'=>$assess,
								'academic_terms'=>$student_inclusive_terms,
								'selected_term'=>$hist_id,
								'idnum'=>$this->session->userdata('idno'),
								), 
								'assessment', $tab=='assessment', FALSE);
				
			} else {
		
				//ADDED: 6/26/15 by Genes
				if (isset($selected_history_id)) {
					$affiliated_fees = $this->fees_schedule_model->ListStudentAffiliated_Fees($selected_history_id);
				} else {
					$affiliated_fees = $this->fees_schedule_model->ListStudentAffiliated_Fees($this->session->userdata('student_histories_id'));
				}
				$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment_view_for_students', 
							array(
									'assessment_date'=>$assessment_date[0]->transaction_date,								
									'other_courses_payments'=>$other_courses_payments,
									'assessment'=> $assessment,
									'courses'=>$courses,
									'affiliated_fees'=>$affiliated_fees,
									'lab_courses'=>$lab_courses,
									'student_inclusive_terms' =>$student_inclusive_terms,
									'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
// 								'current_period'  => $this->balances_lib->current_period(),
//								'due_this_period' => $this->balances_lib->due_this_period(),								
							), 
									'assessment', $tab=='assessment', FALSE);
			}	
		} else {
			$this->load->model('basic_ed/assessment_basic_ed_model');
			$this->load->model('basic_ed/basic_ed_enrollments_model');
				
			$basic_ed = $this->basic_ed_enrollments_model->getLatestHistoryInfo($this->session->userdata('idno'));

			$stud_status = FALSE;
			if ($basic_ed){
				if ($basic_ed->status == 'active') {
					$stud_status = TRUE;
				}			

				$this->load->model('basic_ed/basic_ed_model', 'bed_model');
				$config = array(
						'ledger_data'			=> $ledger_data,
						'unposted_transactions'	=> $unposted_students_payments,
						'periods'				=> $this->bed_model->exam_dates($result->year_level, $result->levels_id),
						'current_academic_year' => $this->bed_model->academic_year(),
						'bed_status' 			=> $this->student_info->bed_status,
						'current_nth_exam'      => $this->bed_model->current_exam($result->year_level, $result->levels_id),
						'total_number_of_exams' => count($this->bed_model->exam_dates($result->year_level, $result->levels_id)),
				);

				$this->load->library('basic_ed_balances', $config);
				if($this->basic_ed_balances->error == TRUE){
					$this->content_lib->set_message($this->basic_ed_balances->error_message, 'alert-error');
					$this->content_lib->content();
					return;
				}

				$this->tab_lib->enqueue_tab ('Assessment', 'teller/assessment_basic_ed',
						array(
								'tuition_basic'=>$this->assessment_basic_ed_model->getTuitionRateBasic($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
								'basic_ed_fees'=>$this->assessment_basic_ed_model->ListBasic_Ed_Fees($basic_ed->levels_id, $basic_ed->academic_years_id, $basic_ed->yr_level),
								'post_status'=>$this->assessment_basic_ed_model->CheckIfPosted($basic_ed->id),
								'basic_ed_sections_id' => $basic_ed->basic_ed_sections_id,
								'stud_status'=>$stud_status,
								'current_nth_exam'    => $this->basic_ed_balances->current_nth_exam(),
								'due_this_exam'       => $this->basic_ed_balances->due_this_exam2(),
						),
						'assessment', $tab=='assessment', FALSE);
			}	
		}
				
		$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$ledger_data,
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date
						), 'ledger', FALSE);
		
		//$this->tab_lib->enqueue_tab('Assessment', 'accounts/assessment_table', array('assessment'=>$assessment), 'assessment', TRUE);
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->content();
	}
	
	
	
	/**
	 * Tools
	 * 
	 * Aside from the dashboard is the most common "grouped action" of any role...
	 * 
	 * @param string $action specified action
	 */
	public function tools(){
		
		$action = $this->input->post('action');		
		if (method_exists($this, $action)) {
			//action is a private method...
			call_user_func_array(array($this, $action), array());
		} else {
			//show tools...
			//print_r(get_defined_vars());die();
			//var_dump($user_data->idno); die();
			//print_r( $this->student_info->idno);die();
			$data = array(
					'name'	=>	$this->userinfo['fname'] . " " . $this->userinfo['lname'],
					'role'	=>	$this->role,
					'phone_number'=>$this->student_info->phone_number,
					'idno'			=> $this->student_info->idno,
					'errors' => array(),
			);
			$this->content_lib->set_title ('Tools | ' . $this->config->item('application_title'));
			
			$this->load->library('tab_lib');
			$this->load->model('common_model');
			
			
			switch ($this->input->post('action')){
				case "Delete":
					$this->content_lib->set_message('Students cannot delete feedbacks!','alert-error');
					break;
				case "Mark as Resolved":
					if ($resolved = $this->common_model->mark_resolved($this->input->post('check'))  AND $resolved>0)
						$this->content_lib->set_message('Successfully marked as Resolved!','alert-success');
					else
						$this->content_lib->set_message('Unable to mark as resolved!','alert-error');
					break;
				case "Unresolve":
					if ($resolved = $this->common_model->unresolve($this->input->post('check'))  AND $resolved>0)
						$this->content_lib->set_message('Successfully unresolved!','alert-success');
					else
						$this->content_lib->set_message('Unable to unresolve!','alert-error');
					break;
				case "Remark":
					if ($dev_remark = $this->common_model->dev_remark($this->input->post('id_to_update'),$this->input->post('dev_remark')) AND $dev_remark>0)
						$this->content_lib->set_message("Successfully posted!","alert-success");
					else
						$this->content_lib->set_message("Unable to post response!","alert-error");
					break;
			}
				
			
			
			if ( ($data['data'] = $this->common_model->get_feedbacks($this->userinfo['idno'], TRUE)) == TRUE ){
				$feedbacks_count = count($data['data']);			
				$passwordtab_active = $feedbacks_count > 0 ? FALSE : TRUE ;			
			}else{
				$passwordtab_active = TRUE;
			}

			$data['role']=$this->userinfo['role'];
			
			$this->load->library('table_lib');
			$this->load->library('form_lib');				
			
			$this->tab_lib->enqueue_tab ('Update Password', 'common/tools', $data, 'password', $passwordtab_active);
			$this->tab_lib->enqueue_tab ('Phone Number', 'common/phone_number_update',array(), 'phone_number', FALSE);
			
			
			if (substr($this->userinfo['ip_address'],0,8) == '10.1.22.'  ||  $this->userinfo['ip_address'] == '10.1.1.168' ){				
				if ($data['phones'] = $this->student_common_model->list_student_numbers($this->userinfo['idno'])){				
					$this->tab_lib->enqueue_tab ('Phone Update Beta', 'common/phone_update_beta',$data, 'phone_beta', FALSE);
				}
			}
				
			$this->tab_lib->enqueue_tab ('Feedbacks', 'feedback_view', $data, 'feedbacks', !$passwordtab_active);
				
				
			$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
			$this->content_lib->enqueue_footer_script ('password_strength');
			$this->content_lib->enqueue_footer_script('data_tables');
				
			$this->content_lib->content();
		}
	}
	
	public function update_phone(){
		$this->load->model('student_common_model');

		if ( ! $this->common->nonce_is_valid($this->input->post('nonce'))){
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');				
		}
		
		$primary_number 	= $this->input->post('primary_number');
		$idno 				= $this->input->post('idno');
		
		if ( !$this->student_common_model->update_student_number($idno,'mobile','student',$primary_number) )
			$this->content_lib->set_message('Failed to save phone number', 'alert-error');
		else
			$this->content_lib->set_message('Phone number successfully saved', 'alert-success');		
		
		$this->content_lib->content();
	}
	
	
	public function update_phone_beta_delete_this_blahblah(){
		$this->load->model('student_common_model');
	
		if ( ! $this->common->nonce_is_valid($this->input->post('nonce'))){
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
		}
	
		$primary_number 	= $this->input->post('primary_number');
		$idno 				= $this->input->post('idno');
	
		if ( !$this->student_common_model->update_student_number($idno,'mobile','student',$primary_number) )
			$this->content_lib->set_message('Failed to save phone number', 'alert-error');
		else
			$this->content_lib->set_message('Phone number successfully saved', 'alert-success');
	
		$this->content_lib->content();
	}
	
	
	private function change_password(){
		
		$this->load->model('student_common_model');
		$data = array(
				'name'	=>	$this->userinfo['fname'] . " " . $this->userinfo['lname'],
				'role'	=>	$this->role,
				'title'	=>	'Tools',
		);		
		$old_password = $this->input->post('password');
		$new_password = $this->input->post('newpassword1', TRUE);
		$new_password2 = $this->input->post('newpassword2', TRUE);
		
		//server side validation...
		$errors = array();
		if ( ! $this->common->nonce_is_valid($this->input->post('nonce')))
			$errors[] = 'Nonce is invalid. ';
		
		if ( ! $this->student_common_model->user_is_valid($this->userinfo['idno'], $old_password))
			$errors[] = 'Old password did not match. ';
		
		if ( $new_password != $new_password2 )
			$errors[] = 'Passwords did not match. ';
		
		//regular expression validation...
		$regex = "#[ ']+?#";
		if (preg_match($regex, $new_password))
			$errors[] = 'Invalid Characters found. ';
		
		if (count($errors) == 0) {
			$result = $this->student_common_model->update_information($this->userinfo['idno'], array('password'=>$new_password));
			if ( ! $result)
				$errors[] = 'Error found while updating password. '; 
		}
		
		if ( count($errors) > 0 )
			$data['errors'] = $errors; else 
			$data['success'] = TRUE;
		
		$this->load->library('tab_lib');
		$this->tab_lib->enqueue_tab ('Password Settings', 'common/tools', $data, 'password', TRUE);
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->enqueue_footer_script ('password_strength');
		$this->content_lib->content();
	}
	


	/**
	 * CheckAllowedCoursesToEnroll
	 * Edited: 01/21/2013
	 * 
	 */
	private function CheckAllowedCoursesToEnroll() {
		//list courses to be taken		
		if ( ($Course_FromProspectus = $this->Prospectus_Model->ListCoursesToBeTaken($this->session->userdata('prospectus_id'))) != TRUE ){
			if ($Course_FromProspectus != FALSE){
				$this->content_lib->set_message('No more courses to be taken or empty prospectus!','alert-error');				
			}else{ //set as null when no result is found but no error in query
				$this->content_lib->set_message('Error on getting courses from prospectus!','alert-error');
			}
			return;
		}
		$grade_array = array('5.0','NG','INC','DR','NC','NA','WD','INE',NULL);
		$d2 = "('')";
		$CoursesFailed = array();
		$CoursesFailed_Elective = array();
		$CoursesNoPrereq = array();
		$NotTakenProspectus = array();
		$NotTakenElective = array();
		$final3 = array();
		$CoursesNotTaken = array();
		//print_r($this->session->userdata); die();
		//print_r($Course_FromProspectus);die();
		//$data2 = $this->Prospectus_Model->checkIfTaken(2324, 5839170);
		//print_r($data2); die();
		if ($Course_FromProspectus) {
			foreach($Course_FromProspectus AS $course) {
				//check if course is taken excluding the current term
				$data2 = $this->Prospectus_Model->checkIfTaken($course->courses_id,$this->session->userdata('idno'));
				if (!$data2) {
					//if not taken, temporarily save to array
					$CoursesNotTaken[$course->courses_id] = $course;
				} elseif (in_array($data2->finals_grade, $grade_array)) {	
					if ($course->course_elective == 'N') {
						$CoursesFailed[] = $course->courses_id;
					} else {
						$CoursesFailed_Elective[] = $course->courses_id;						
					}
				} else {
						if ($course->cutoff_grade AND ($data2->finals_grade > $course->cutoff_grade)) {
							//print("n2 <p>");
							$CoursesFailed[] = $course->courses_id;
						}
				}
			}

			//list all credited courses of particular student
			/*if ( ($data_CreditedCourses = $this->Prospectus_Model->ListCreditedCourses($this->session->userdata('idno'))) !=TRUE ){
				$this->content_lib->set_message('Notice: Got some errors on picking up your credited courses.<br>You may continue or send feedback to concerns.','alert-info');
			}*/
			//print_r($CoursesFailed); die();
			
			$data_CreditedCourses = $this->Prospectus_Model->ListCreditedCourses($this->session->userdata('idno'),$this->session->userdata('prospectus_id'));
			//print_r($data_CreditedCourses); die();
			
			if ($data_CreditedCourses && is_array($data_CreditedCourses) ) { //no error in query and zero or more rows
				foreach($data_CreditedCourses AS $course) {
					//remove courses that are not taken but credited
					foreach($CoursesNotTaken AS $not_taken) {
						if (intval($course->courses_id) == $not_taken->courses_id) {
							unset($CoursesNotTaken[intval($course->courses_id)]);
							//print($not_taken->courses_id."<p>".$course->courses_id."<p>");
						}
					}
					
					/*ADDED: 3/31/15 by genes
						remove coursefailed also if the course is credited
					*/	
					if ($CoursesFailed) {
						foreach($CoursesFailed AS $k=>$v) {
							if (intval($course->courses_id) == $v) {
								unset($CoursesFailed[$k]);
								//print($v."<p>".intval($course->courses_id)."<p>");
							}
						}
					}
					
				}
			}
			//print_r($CoursesFailed); die();
			
			if (isset($CoursesNotTaken)) {
				foreach($CoursesNotTaken AS $course) {
					if ($course->course_elective == 'N') {
						$NotTakenProspectus[] = $course->courses_id;
					} else {
						$NotTakenElective[] = $course->courses_id;					
					}
				}
			} else {
				$NotTakenElective =array();
				$NotTakenProspectus =array();
			}
			//print_r($CoursesNotTaken); die();
			//print_r($NotTakenProspectus); die();
			$d1 = json_encode($NotTakenProspectus);
			$d2 = str_replace(array('"','[',']'),array("'","(",")"),$d1);
			
			
			if ($NotTakenElective) {
				$d3 = json_encode($NotTakenElective);
				$d4 = str_replace(array('"','[',']'),array("'","(",")"),$d3);
			} else {
				$d4 = "('')";
			} 
			
			
			//print_r($d2); die();
			//$d2 are courses NOT TAKEN, NOT CREDITED
			//$d4 are courses NOT TAKEN, ELECTIVE
			//OK NA output of $d2
			//**************************************************************************************************************

			//list all preprequisites of not taken and not credited courses
			if (($Courses_WithPrereq = $this->Prospectus_Model->ListCourseWithPrereq($d2, $d4, $this->session->userdata('prospectus_id'))) !=TRUE){
				$this->content_lib->set_message('Notice: Got some errors on picking up courses with prerequisites. You may continue or send feedback to concerns.','alert-info');						
			}

			if (($Courses_NoPrereq = $this->Prospectus_Model->ListCourseNoPrereq($d2, $d4, $this->session->userdata('prospectus_id'))) != TRUE){
				$this->content_lib->set_message('Notice: Got some errors on picking up courses without prerequisites. You may continue or send feedback to concerns.','alert-info');						
			}
			
			
			
			$NotTaken_WPassedPrereq='';
			$NotTaken_WPassedPrereq_Elective='';
			$NotTaken_WCreditedPrereq = '';		
			$NotTaken_WCreditedPrereq_Elective = '';	
			$MyAdvisedCourses=array();
			$Courses_NoPrereq_Prospectus='';
			$Courses_NoPrereq_Elective='';
			//check courses with prerequisites
			//print_r($Courses_WithPrereq); die();
			$Final_Courses_WithPrereq = '';
			$Final_Courses_WithPrereq_Elective = array();
			//print_r($Courses_NoPrereq);
			//die();
			
			$yr = $this->session->userdata('year_level');
			//$data = $this->Prospectus_Model->PassPrerequisites(9884,6246257,3);
			
			//print_r($data); die();
			if ($Courses_WithPrereq && is_array($Courses_WithPrereq)){ //no error in query and zero or more rows
				foreach ($Courses_WithPrereq AS $course) {
					if ($course->from_prospectus == 'Y') {
						if ($this->Prospectus_Model->PassPrerequisites($course->prospectus_courses_id, $this->session->userdata('idno'), 
																			$this->session->userdata('yr_level'))) {
							$Final_Courses_WithPrereq[] = $course->courses_id;
							//print("Y<p>");
						}
					} else {
						$courses_children = $this->Prospectus_Model->Topic_Elective($course->prospectus_courses_id, $this->session->userdata('idno'),
																			$this->session->userdata('year_level'));
						if ($courses_children) {
							foreach ($courses_children AS $child) {
								if ($this->Prospectus_Model->PassPrereq_Elective_Child($child->topic_courses_id, $this->session->userdata('idno'),
																			$this->session->userdata('year_level'), $this->session->userdata('prospectus_id'))) {
									//print($child->topic_courses_id."<br>");
									$Final_Courses_WithPrereq_Elective[] = $child->topic_courses_id;									
								}
							}
						}
					}
				}
			}
			//die();
			//for courses with no prerequisites
			if ($Courses_NoPrereq && is_array($Courses_NoPrereq)) {
				foreach ($Courses_NoPrereq AS $data5) {
					if ($data5->from_prospectus == 'Y') {
						$Courses_NoPrereq_Prospectus[] = $data5->courses_id;
					} else {
						$Courses_NoPrereq_Elective[] = $data5->courses_id;						
					}
				}
			}
			
			//for advised courses
			//COMMENTED ON: 3/31/15 by genes
			//NO NEED TO CHECK ADvised Courses
			/*if (($AdvisedCourses = $this->Student_Model->ListAdvisedCourses($this->session->userdata('student_histories_id'))) !=TRUE){
				$this->content_lib->set_message('Notice: Got some errors on picking up advised courses. You may continue or send feedback to concerns.','alert-info');						
			}*/
			$AdvisedCourses = $this->Student_Model->ListAdvisedCourses($this->session->userdata('student_histories_id'));
					
			if ($AdvisedCourses && is_array($AdvisedCourses)) {
				foreach ($AdvisedCourses AS $data6) {
					$MyAdvisedCourses[] = $data6->courses_id;
				}
			}
			//$forced_courses = array('1081','1067','1511'); //added by tatskie. will ask genes if this will work
			//$forced_courses = array('00838');
			//print_r($AdvisedCourses);die();		
			//$MyAdvisedCourses = array_merge($forced_courses,$MyAdvisedCourses); //added by tatskie
		
			$final3['CoursesFailed']					= $CoursesFailed;
			$final3['CoursesFailed_Elective']			= $CoursesFailed_Elective;
			$final3['Final_Courses_WithPrereq']			= $Final_Courses_WithPrereq;
			$final3['Final_Courses_WithPrereq_Elective']= $Final_Courses_WithPrereq_Elective;				
			$final3['Courses_NoPrereq_Prospectus'] 		= $Courses_NoPrereq_Prospectus;
			$final3['Courses_NoPrereq_Elective']		= $Courses_NoPrereq_Elective;
			$final3['NotTaken_WPassedPrereq']  	 		= $NotTaken_WPassedPrereq;
			$final3['NotTaken_WPassedPrereq_Elective'] 	= $NotTaken_WPassedPrereq_Elective;
			$final3['NotTaken_WCreditedPrereq'] 		= $NotTaken_WCreditedPrereq;
			$final3['NotTaken_WCreditedPrereq_Elective']= $NotTaken_WCreditedPrereq_Elective;
			$final3['MyAdvisedCourses'] 				= $MyAdvisedCourses;
					
		}

		//print_r($final3); die();
		return $final3;
	}

		
		
/******************************
** StudentOKToEnroll
******************************/
		private function StudentOKToEnroll($course_offerings_id, $courses_id, $prospectus_id, $advised, $elective) {
			
			//NOTE: this method must be UPDATED to include computation of units_enrolled of elective subjects by: genes 5/4/13
			//$data3 = $this->Student_Model->getUnitsEnrolled($this->session->userdata('idno'),$this->session->userdata('academic_terms_id'),
				//												$this->session->userdata('prospectus_id'));


			$grades    = $this->Grades_Model->ListStudentGrades($this->session->userdata('academic_terms_id'),$this->session->userdata('idno'));
			
			$total_credit_units_enrolled = 0;
			$total_bracketed_units_enrolled = 0;
			
			if ($grades) {
				foreach($grades AS $grade) {
					if ($grade->is_bracketed == 'N') {
						$total_credit_units_enrolled += $grade->credit_units;
					} else {
						$total_bracketed_units_enrolled +=  $grade->credit_units;
					}
				}
			}
			
			$data1 = $this->Offerings_Model->getCourseOfferingInfo($course_offerings_id,$prospectus_id,$advised,$elective);
			$credit_units = 0;
			//print_r($data1); die();
			//$total_credit_units_enrolled = 0;
			if ($data1->is_bracketed == 'N') {
				//if ($data3) {
					//$total_credit_units_enrolled = $data3->total_credit_units_enrolled;
				//}
				$credit_units = $total_credit_units_enrolled + $data1->credit_units;
				$max_units = $this->getUnits();
				if ($credit_units > $max_units['max_units_to_enroll']) {
					$this->AlertMsg = "Overload Credit Units!";
					$this->AlertType = "Error";
					return FALSE;
				}
			}
		
//			if (($data3->total_credit_units_enrolled + $data3->total_bracketed_units_enrolled + $data1->credit_units) > $this->session->userdata('max_bracket_units')) {

			// if (($total_credit_units_enrolled + $total_bracketed_units_enrolled  + $data1->credit_units) > $this->session->userdata('max_bracket_units')) {
/*			if (($total_credit_units_enrolled + $data1->credit_units) > $this->session->userdata('max_bracket_units')) {
					$this->AlertMsg = "Overload Total Units!";
					$this->AlertType = "Error";
					return FALSE;			
			}
*/				
				
			//check if student already enrolled on particular course on particular academic term
			$data1 = $this->Student_Model->getEnrolledCourse($this->session->userdata('academic_terms_id'),$courses_id,$this->session->userdata('idno'));
			if (!$data1) {
				$offering = $this->Offerings_Model->ListOfferingSlots($course_offerings_id);
					
				foreach ($offering[0] AS $offer) {
								
							$day_names = $this->Rooms_Model->ListDayNames($offer->days_day_code);
									
							foreach ($day_names AS $day) {
								$data1 = $this->Offerings_Model->getConflictEnrollment($this->session->userdata('idno'),$this->session->userdata('academic_terms_id'),
																							$offer->start_time,$offer->end_time,$day->day_name);
								//check if there is conflict
								if ($data1) {
									//store conflicting Enrollment ID
									$this->ConflictEnrollmentID[] = $data1->id;
								}					
							}
						}
							
				if ($this->ConflictEnrollmentID) {
							$this->AlertMsg = "Conflict offering!";
							$this->AlertType = "Error";
							return FALSE;
						} else {
							return TRUE;
						}
							
			} else {
				$this->AlertMsg = "Course already enrolled!";
				$this->AlertType = "Error";
				return FALSE;
			}
		}
		
		
		//Edited: 01/24/2013
		//retrieves maximum credit units and maximum bracketed units
		private function getUnits() {
			$max_units_to_enroll=0;
			$max_bracket_units=0;
			$data4 = $this->Student_Model->getCheckWithSummmer($this->session->userdata('prospectus_id'), $this->session->userdata('yr_level'));
			$data5 = $this->academic_terms_model->current_academic_term($this->Enrollments_Model->academic_term_for_enrollment_date());
			if($data5->term == "Summer" && !$data4) {
				$max = $this->Student_Model->getMaxInSummmer($this->session->userdata('idno'), $this->session->userdata('academic_terms_id'));
				if ($max) {
					$max_units_to_enroll = $max->max_units; 
					$max_bracket_units = 12; 				
				} else {
					$max_units_to_enroll = 9; 
					$max_bracket_units = 12; 
				}
			} else {						
				$data2 = $this->Student_Model->getMaxUnits($this->session->userdata('student_histories_id'),$this->session->userdata('academic_terms_id'));
				if (!$data2){
					$max_units_to_enroll = $this->Student_Model->get_max_units_from_max_units($this->session->userdata('idno'),$this->session->userdata('academic_terms_id'));						
					$max_units_to_enroll = $max_units_to_enroll ? $max_units_to_enroll : 0; //added by tatskie to make sure all result is numeric 
					$max_bracket_units = 0;
				}else{
					$max_units_to_enroll = $data2->max_units_to_enroll; 
					$max_bracket_units = $data2->max_bracket_units;
				} 
			}
			return array('max_units_to_enroll'=>$max_units_to_enroll,'max_bracket_units'=>$max_bracket_units);
		}


		
		private function DisplayCourses($return=FALSE) {
			$d2=$this->CheckAllowedCoursesToEnroll();
			//print_r($d2);die();
			$CoursesInElective = array_unique(array_merge(
													(array)$d2['CoursesFailed_Elective'],
													(array)$d2['Final_Courses_WithPrereq_Elective'], 
													(array)$d2['Courses_NoPrereq_Elective'], 
													(array)$d2['NotTaken_WPassedPrereq_Elective'], 
													(array)$d2['NotTaken_WCreditedPrereq_Elective']));

			$CoursesInElective = array_values($CoursesInElective);
			$CoursesInElective = json_encode($CoursesInElective);
			$CoursesInElective = str_replace(array('"','[',']'),array("'","(",")"),$CoursesInElective);
			

			$CoursesInProspectus = array_unique(array_merge(
													(array)$d2['CoursesFailed'],
													(array)$d2['Final_Courses_WithPrereq'], 
													(array)$d2['Courses_NoPrereq_Prospectus'], 
													(array)$d2['NotTaken_WPassedPrereq'],
													(array)$d2['NotTaken_WCreditedPrereq']));
			$CoursesInProspectus = array_values($CoursesInProspectus);
			$CoursesInProspectus = json_encode($CoursesInProspectus);
			$CoursesInProspectus = str_replace(array('"','[',']'),array("'","(",")"),$CoursesInProspectus);

			//print_r($d2['NotTaken_WPassedPrereq']); die();
			//print_r($CoursesInProspectus); die();

			
			if (!empty($d2['MyAdvisedCourses'])) {
				$AdvisedCourses = json_encode($d2['MyAdvisedCourses']);
				$AdvisedCourses = str_replace(array('"','[',']'),array("'","(",")"),$AdvisedCourses);
			} else {
				$AdvisedCourses = "('')";
			}
			//print_r($AdvisedCourses);die();			

			//@Oct 21: tatskie, continue editing here 			
			$this->load->model('academic_terms_model');
			$this->load->model('hnumis/enrollments_model');
			$academic_terms = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
			$this->session->set_userdata('academic_terms_id',$academic_terms->id);
			
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			
			
			$data['ConflictEnrollmentID'] = $this->ConflictEnrollmentID;
			
			$block_id = $this->Student_Model->getStudentBlock($this->session->userdata('idno'), $this->session->userdata('academic_terms_id'), $this->session->userdata('prospectus_id'));

			//print_r($AdvisedCourses); die();

			if (isset($block_id) AND $block_id->block_sections_id != NULL) {
				$data['courses'] = $this->Offerings_Model->ListOfferingsThatCanBeTakenByBlock(
						$this->session->userdata('academic_terms_id'),$CoursesInProspectus,$AdvisedCourses,
						$CoursesInElective,$this->session->userdata('prospectus_id'),$block_id->block_sections_id,
						$this->session->userdata('student_histories_id'));
			} else {
				//print("no block"); die();
				//print_r($this->student_info); die();
				$my_info['student_histories_id'] = $this->student_info->student_histories_id;
				$my_info['year_level'] = $this->student_info->year_level;
				$my_info['academic_programs_id'] = $this->student_info->academic_programs_id;
				$my_info['prospectus_id'] = $this->student_info->prospectus_id;
				//print_r($AdvisedCourses); die();
				//print_r($CoursesInProspectus); die();
				$data['courses'] = $this->Offerings_Model->ListOfferingsThatCanBeTaken(
						$this->session->userdata('academic_terms_id'),
						$CoursesInProspectus,$AdvisedCourses,
						$CoursesInElective,$my_info);
				//print_r($data['courses']);die();
			}
			
			//print_r($data['courses']);die();

			
			$data['grades']    = $this->Grades_Model->ListStudentGrades($this->session->userdata('academic_terms_id'),$this->session->userdata('idno'));
			
			$data['AlertMsg'] = $this->AlertMsg;
			$data['AlertType'] = $this->AlertType;
			$data['assessed'] = FALSE;
			
			$current_academic_term = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
			$current_student_history = $this->Student_Model->get_StudentHistory_id($this->userinfo['idno'], $current_academic_term->id);
			if($current_student_history){
				$this->assessment = $this->assessment_model->CheckStudentHasAssessment($current_student_history->id);
				$data['assessed'] = $this->assessment;
			}
			
			//print_r($data['courses'] );die();
			
			//Units
			$load_units = $this->getUnits();
			$data['max_units_to_enroll'] = $load_units['max_units_to_enroll'];
			$data['max_brackete_units'] = $load_units['max_units_to_enroll'];
			if (!$return) {
				$data['title'] = "College Enrollment (" . $current_academic_term->term . " SY " . $current_academic_term->sy . ")";
				$this->content_lib->enqueue_body_content('student/header_title', $data);
				$this->content_lib->enqueue_body_content('student/list_offerings_that_can_be_taken', $data);
				$this->content_lib->content();
			} else {
				//$data['title'] = "Offerings";
				//$this->content_lib->enqueue_body_content('student/header_title', $data);
				$this->content_lib->enqueue_body_content('student/view_offerings', $data);
				$this->content_lib->content();			
			}
	
		}


		public function __destruct(){
			$this->db->close();
		}

		
		/**
		 * Description:
		 * This method generates a pdf file of the list of students in a particular class handled by the faculty member.
		 */
		function generate_class_schedule_pdf($academic_term_id, $max_units) {
			
			$this->load->model("hnumis/Reports_model");

			//print_r($this->userinfo);
			//die();
			$this->userinfo['max_bracket_units'] = $max_units;
			$this->userinfo['student_histories_id'] = $this->input->post('student_histories_id');
			
			$selected_term = $this->AcademicYears_Model->getAcademicTerms($academic_term_id);

			$this->Reports_model->generate_student_class_schedule_pdf($academic_term_id, $this->userinfo, $selected_term);
		
		}

		/*
		private function summer_patch_2014($current_academic_term_id){
			$patch_payer_info = $this->teller_model->get_student($this->session->userdata('idno'));
			if ($patch_payer_info){
				$unposted_payments = $this->teller_model->get_unposted_payments_for_summer_patch2014($patch_payer_info->payers_id);
				$patch_ledger_data =$this->teller_model->get_ledger_data($patch_payer_info->payers_id, TRUE, FALSE, FALSE, FALSE);
				if ($patch_ledger_data){
					$patch_ledger_balance=0;
					foreach ($patch_ledger_data as $row){
						$patch_ledger_balance = $patch_ledger_balance + round($row['credit'],2);
						$patch_ledger_balance = $patch_ledger_balance - round($row['debit'],2) ;
					}
					
					if($unposted_payments){
						foreach ($unposted_payments as $row){
							$patch_ledger_balance = $patch_ledger_balance + round($row['receipt_amount'],2);								
						}						
					}
					
					if ((float)round($patch_ledger_balance,2) >= 800 ){
						//$patch_hist_id_inserted	= $this->teller_model->summer2014_patch_insert_history($this->userinfo['idno'],$this->userinfo['prospectus_id'],$current_academic_term->id, $this->userinfo['year_level'],'Y');
					 	$yr_level = $this->session->userdata('yr_level') == 0 ? 1 : $this->session->userdata('yr_level') ; 
						$patch_hist_id_inserted	= $this->teller_model->summer2014_patch_insert_history($this->session->userdata('idno'), $this->session->userdata('prospectus_id'), $current_academic_term_id, $yr_level ,'Y');
						if ($patch_hist_id_inserted	< 1){
							$this->session->set_userdata('not_allowed_to_enroll_message','Failed to perform summer 2014 patch!');
						}
					}else{
						$this->session->set_userdata('not_allowed_to_enroll_message','Balance still exists!');						
					}
						
				}else{
					$this->session->set_userdata('not_allowed_to_enroll_message','Failed to get payment records!');						
				}
			}
		}*/
		
		public function enrollment_check(){
			/*
			 * Before going to the enroll method... must pass through this "Checkpoint"
			 */
			$data = array();
			$this->load->model('hnumis/enrollments_model');
			$default_cr_amount = $this->enrollments_model->default_cr_amount();
			$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
			$result = $this->teller_model->get_student($this->userinfo['idno']);
			$ledger_data = $this->teller_model->get_ledger_data($result->payers_id);
			$unposted_students_payments = $this->teller_model->get_unposted_students_payment($result->payers_id);
			$this->load->library('balances_lib',
					array(
							'ledger_data'=>$ledger_data,
							'unposted_students_payments'=>$unposted_students_payments,
							'current_academic_terms_obj'=>$current_academic_terms_obj,
							'period'=>$this->academic_terms_model->what_period(),
							'period_dates'=>$this->academic_terms_model->period_dates(),
					));
			if ($this->input->post()){
				if ($this->common->nonce_is_valid($this->input->post('nonce'))){
					switch ($this->input->post('action')){
						case 'insert_scheduled_academic_term':
							//home check...
							if( round($this->balances_lib->credit_balance(),0) >= $default_cr_amount){
								$new_history_id = $this->enrollments_model->insert_scheduled_student_history($this->userinfo['idno'], $this->userinfo['idno']);
								//print_r($new_history_id);die();
								if ($new_history_id){
									//added by tatskie Oct 20, 2014 to fix auto add adjustment
									$this->session->set_userdata('student_histories_id',$new_history_id);  									
									$this->content_lib->set_message('Successfully added scheduled academic term to your academic history.', 'success');
								} else {
									$this->content_lib->set_message('Error found while adding scheduled academic term to your academic history.', 'error');
								}
							} else {
								$this->content_lib->set_message('You are not allowed to perform this action.', 'error');
							}	
							break;
						case 'set_can_enroll_to_yes' :
							//This should happen ONLY when the student's CURRENT academic term matches the SYSTEM's academic term
							$student_current_academic_term = $this->enrollments_model->students_current_academic_term($this->userinfo['idno']);
							if ($this->enrollments_model->academic_term_for_enrollment_date() == $student_current_academic_term->academic_terms_id){
								if($this->enrollments_model->set_can_enroll_to_y_current_term($this->userinfo['idno'], $student_current_academic_term->academic_terms_id))
									$this->content_lib->set_message('Successfully set your CAN ENROLL status to YES.', 'success'); else
									$this->content_lib->set_message('Error found while setting CAN ENROLL status to YES.', 'error');
							} else {
								$this->content_lib->set_message('You are not allowed to perform this action.', 'error');
							}
							break;
						default: break;
					}
				} else {
					$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
					$this->content_lib->content();
					die();
				}
			}

			/*
			    Provision for students BLOCKED for enrollment here.
			    As of 3.28.2018, this feature is hard-coded as per Sir Rey.
			    by: Toyet
			*/
			////log_message("INFO",'before checking IDs');
			$blocked_for_enrollment = array( '03225705', '06513935', '06720947', '06726856' );
			// 1.  03225705 -  DEMATA, John Paulo Panio
			// 2.  06513935 -  LAMAY, Ma. Cresel Darunday
			// 3.  06720947 -  VIRTUDAZO, BERNARD
			// 4.  06726856 -  OPERIANO, JAN COLEEN			
			//

			// implemented new way of blocking student from enrollment			
			$blocked_status = $this->col_students_model->get_blocked_status($this->userinfo['idno']); 
			// however, still check with the old method  :) - Toyet 3.2.2019
			if($blocked_status=='N'){
				if (in_array($this->userinfo['idno'], $blocked_for_enrollment)) {
					$blocked_status = 'Y';
				}
			}

			//if (in_array($this->userinfo['idno'], $blocked_for_enrollment)) {
			if($blocked_status=='Y') { 
				$data['scheduled_academic_term'] = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
				$data['error'] = 'BLOCKED_FOR_ENROLLMENT';
				$this->content_lib->enqueue_body_content('student/add_history', $data);
				$this->content_lib->content(); die();
				////log_message("INFO",'After na gud');				
			} else {


			/*
			 * Rules for student who enroll with history's academic term matching scheduled academic term:
			 * 1. History has academic term equal to the scheduled academic term. 
			 * 2. History's can_enroll flag is equal to Y 
			 * 3. year level's schedule for enrollment matches with History's year level OR date now is less than or equal to History's extension date  
			 */
			$student_current_academic_term = $this->enrollments_model->students_current_academic_term($this->userinfo['idno']);
			//print_r($student_current_academic_term); die();
			if ($this->enrollments_model->academic_term_for_enrollment_date() == $student_current_academic_term->academic_terms_id){
				//This student satisfies #1. History has academic term equal to the scheduled academic term.
				if ($student_current_academic_term->can_enroll=='Y'){
					//Satisfies no. 2
					if ($this->enrollments_model->on_enrollment_schedule($student_current_academic_term->year_level, ($student_current_academic_term->levels_id==7 ? TRUE : FALSE), ($this->enrollments_model->is_new_enrollee($this->userinfo['idno'])))){
						//On enrollment schedule...
						$this->session->set_userdata('enrollment_checked', 'successful');
						redirect(site_url('student/enroll'));
					} else {
						//Today isn't the student's enrollment schedule or he/she needs extension...
						if(!is_null($student_current_academic_term->extension_end) && $student_current_academic_term->extension_end >= date('Y-m-d')){
							//FIXME: If student's scheduled enrollment is sooner than Today and he requested for extension... the student will be able to enroll...
							$this->session->set_userdata('enrollment_checked', 'successful');
							redirect(site_url('student/enroll'));
						}
						$data['scheduled_academic_term'] = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
						//$data['enrollment_schedule'] = $this->enrollments_model->enrollment_schedule($student_current_academic_term->academic_terms_id); wishlist...
						$data['error'] = 'HAS_CURRENT_TERM_BUT_NOT_ON_SCHEDULE';
						$this->content_lib->enqueue_body_content('student/add_history', $data);
						$this->content_lib->content(); die();
					}		
				} else {
					if (round($this->balances_lib->credit_balance(),0) < $default_cr_amount){
						//Student's enroll flag does not allow him/her to enroll.
						//Student needs to pay CR to teller or go to accounts for privileged enroll
						if (round($this->balances_lib->credit_balance(),0) < 0){
							$data['balance'] = abs($this->balances_lib->credit_balance());
							$data['error'] = 'HAS_CURRENT_TERM_BUT_NOT_CAN_ENROLL_AND_WITH_LEDGER_BALANCE';								
						}else{
							$data['error'] = 'HAS_CURRENT_TERM_BUT_NOT_CAN_ENROLL';								
						}
						$data['scheduled_academic_term'] = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
						$this->content_lib->enqueue_body_content('student/add_history', $data);
						$this->content_lib->content(); die();
						
					}
					// balance >= default cr amount...
					
					else{
						$data['scheduled_academic_term'] = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
						$data['error'] = 'SCHEDULED_ACADEMIC_TERM_MATCHING_BUT_ENOUGH_CREDIT';
						$this->content_lib->enqueue_body_content('student/add_history', $data);
						$this->content_lib->content(); die();						
					}
					
				}	
			}
			/*
			 * Today does not have a scheduled enrollment
			 * 1. If user needs to enroll, must go to accounts to have extension.
			 */
			if ($this->enrollments_model->academic_term_for_enrollment_date() == 0){
				//date is not anymore on enrollment schedule.
				if($student_current_academic_term->academic_terms_id == $current_academic_terms_obj->id){
					if($student_current_academic_term->can_enroll=="Y" && (!is_null($student_current_academic_term->extension_end) && $student_current_academic_term->extension_end >= date('Y-m-d'))){
						//student has his enrollment extended and extension end is SOONER than OR is now.
						$this->session->set_userdata('enrollment_checked', 'successful');
						redirect(site_url('student/enroll'));
					} else {
						//echo "We're past the enrollment schedule. You must ask DRIC if you need extension to your enrollment schedule."; die();
						$data['scheduled_academic_term'] = $this->academic_terms_model->current_academic_term();
						$data['error'] = 'NOT_ON_SCHEDULE_BUT_HAS_ACADEMIC_TERM';
						$this->content_lib->enqueue_body_content('student/add_history', $data);
						$this->content_lib->content(); die();
					}				
				} else {
					$data['scheduled_academic_term'] = $this->academic_terms_model->current_academic_term();
					$data['error'] = 'NOT_ON_SCHEDULE_NO_ACADEMIC_TERM';
					$this->content_lib->enqueue_body_content('student/add_history', $data);
					$this->content_lib->content(); die();
				}
			}
			/*
			 * Student history's academic term id does not match the academic_term_id scheduled today. 
			 * 1. If he has 800 or more on credit, then we may update his academic history. 
			 */
			//print($student_current_academic_term->academic_terms_id); die();
			if ($this->enrollments_model->academic_term_for_enrollment_date() != $student_current_academic_term->academic_terms_id){
				//This occurs whenever the system's academic_term does not match the term of the enrollment schedule...
				if ( round($this->balances_lib->credit_balance(),0)  >= $default_cr_amount){
					//he has a credit balance of 800 or more
					$data['scheduled_academic_term'] = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
					$data['error'] = 'SCHEDULED_ACADEMIC_TERM_NOT_MATCHING_BUT_ENOUGH_CREDIT';
					//$data['error'] = 'genes';
					$this->content_lib->enqueue_body_content('student/add_history', $data);
					$this->content_lib->content(); die();
				} else {
					$data['scheduled_academic_term'] = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
					$data['error'] = 'SCHEDULED_ACADEMIC_TERM_NOT_MATCHING_NOT_ENOUGH_CREDIT';
					$this->content_lib->enqueue_body_content('student/add_history', $data);
					$this->content_lib->content(); die();
				}
			}
			}  //for blocked_for_enrollment Toyet 3.28.2018
		}

		
		function view_prospectus() {
			$this->load->library('prospectus_lib');
			$this->load->model('hnumis/college_model');
				
			$student = $this->college_model->getCollegebyProspectusID($this->session->userdata('prospectus_id'));
			//print_r($student);die();

			$this->prospectus_lib->view_prospectus($student->colleges_id,$this->session->userdata('prospectus_id'));
			
		}

	/*
	* @ADDED: 11/7/14
	* @author: genes
	* @description: displays students enrolled from student offerings including students from parallel offerings
	*/
	function list_enrolled_students() {
		$this->load->model('hnumis/Student_Model');
		
		$data['students'] = $this->Student_Model->ListEnrollees_Including_Parallel($this->input->get('offer_id'),$this->input->get('parallel_no'));

		$this->load->view('dean/list_of_students_from_modal', $data);
	
	}


	public function process_student_action($idnum) {
		
		$this->load->library('shs/shs_student_lib');
	
		$this->shs_student_lib->process_student_action($idnum);
		
	}

	
}