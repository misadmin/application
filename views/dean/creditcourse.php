<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>


<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function show_course()
{
	show('course');
	hide('notes');
}
function show_notes()
{
	hide('course');
	show('notes');
}
</script>

<body onLoad="show_course();">

<div style="background:#FFF; width:40%; text-align:center; margin:auto;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  	<tr class="head">
  		<td colspan="3" class="head" style="text-align:left; padding:10px;">  CREDIT COURSE </td>
    </tr>
    <tr>
    	<td colspan="3">
			<form action="<?php echo site_url('dean/creditcourse');?>" method="post">  <?php echo validation_errors(); ?>
			<?php $this->common->hidden_input_nonce(FALSE); ?>
  			<input type="hidden" name="step" value="2" />
    		<table border="0" cellpadding="2" cellspacing="0" class="inside">
   				<tr>
  					<td colspan="3" align="left" valign="middle" style="font-weight:bold; font-size:14px;">
					  <?php 
					  		print($course->course_code. " - ".$course->descriptive_title);
					  		print("<br>".$course->credit_units);  ?> </td>
  				</tr> 
  				<tr>
  					<td colspan="3" class="head" style="text-align:left; padding:10px;">  CREDIT COURSE DETAILS </td>
  				</tr>
  				<?php if($courses_passed) { ?>
				<tr>
  					<td width="97" align="left" valign="middle">Taken at HNU?</td>
  					<td width="3" align="left" valign="middle">:</td>
  					<td width="401" align="left" valign="middle"><input name="taken_type" type="radio" class="form" onClick="show_course();" value="Y" checked="checked"/>  Yes
    					<input name="taken_type" type="radio" class="form" onClick="show_notes();" value="N"/> No </td>
				</tr>
				
			<td colspan="3">
  				<table width="100%" border="0" cellspacing="0" cellpadding="0" id="course">
    				<tr>
      					<td width="19%" align="left">Course</td>
      					<td width="2%" align="left">:</td>
      					<td width="79%" align="left"><select name="enrollments_id" id="enrollments_id" class="form">
        				<?php
							foreach($courses_passed AS $course) {
								print("<option value=".$course->enrollments_id.">".$course->course_code." - ".$course->descriptive_title."</option>");	
							}
						?>  </select></td>
    				</tr>
  				</table>
			<?php } ?>	
  				<table width="100%" border="0" cellspacing="0" cellpadding="0" id="notes">
      				<tr>
        				<td width="20%" align="left">Course Details</td>
        				<td width="2%" align="left">:</td>
        				<td width="78%" align="left"><input type="text" name="course" value=" " /> </td>
      				</tr>
					<tr>
        				<td width="20%" align="left">Grade</td>
        				<td width="2%" align="left">:</td>
        				<td width="78%" align="left"><input type="text" name="outside_hnu_grade" value=" " /> </td>
      				</tr>
   				</table>
			</td>
	</tr>
	<tr>
  		<td>&nbsp;</td>
  		<td>&nbsp;</td>
  		<td align="left"><input type="submit" name="button" id="button" value="Credit" class="btn btn-success" /></td>
    </tr>
  </table>
</form>
  
<div style="width:95%;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-hover" style="margin:10px;">
	   <thead>
   			<tr>
    			<th width="79%" class="header">Credited Subjects</th>
    			<th width="9%" class="header">Grade</th>
			    <th width="9%" class="header">Taken at HNU?</th>
			    <th width="12%" class="header">Delete</th>
			</tr>
	   </thead>
		<?php
			if ($credited) {
				foreach ($credited AS $course) {
		?>
  			<tr>
			    <td><?php print($course->notes); ?></td>
			    <td><?php print($course->grade); ?></td>
			    <td><?php print($course->taken_here); ?></td>
			    <td style="text-align:center; vertical-align:middle;">
			      <a class="delete_course" href="<?php print($course->id); ?>" my_id="<?php print($course->id); ?>">
			        <img src="<?php print(base_url('assets/img/icon-recycle.gif')); ?>" width="20" height="20" align="absmiddle" style="vertical-align:middle; border:none" />
		          </a></td>
		  </tr>
		<?php
				}
			}
		?>
 
  <thead>
 <tr>
    <th colspan="4" class="header" style="text-align:left">&nbsp;
 	
	</th>
    </tr>
  </thead>
</table>
</div>
    </td>
    </tr>
    </table>
    </div>

</body>


<form id="delete_course" action="<?php echo site_url("dean/creditcourse")?>" method="post">
  <input type="hidden" name="step" value="3" />

</form>

<script>
	$('.delete_course').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete the course?');

		if (confirmed){
			var credited_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'credited_id',
		    value: credited_id,
		}).appendTo('#delete_course');
		$('#delete_course').submit();
		} 
		
	});
</script>
  