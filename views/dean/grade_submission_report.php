<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}
</style>

<div style="background:#FFF; width:600px;" align="center">
<div style="width:100%; margin-bottom:40px;" align="center">	

<table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
	 <thead> 
		<tr class="head">
  			<th colspan="5" class="head" style="text-align:center; padding:10px;">  GRADE SUBMISSION REPORT (<?php print($academic_term->term." ".$academic_term->sy); ?>) </th>
  	   	</tr>
	</thead>
  	   	<?php if ($faculty) {
				 foreach($faculty as $f) {  ?>
					<thead> 
						<tr class="head">
				  			<th colspan="5" class="head" style="text-align:left; padding:10px;">  <?php print($f->name); ?> </th>
				  	   	</tr>
					</thead>
				<?php  $submission = $this->offerings_model->get_submission_dates($f->empno, $academic_term->id);
					if ($submission) { ?>
					<thead>
						<tr>
							<th width="10%" class="head" style="vertical-align:middle;">COURSE CODE</th>
							<th width="15%" class="head" style="vertical-align:middle;">SCHEDULE</th>
							<th width="5%" class="head" style="vertical-align:middle;">PRELIMS</th>
							<th width="5%" class="head" style="vertical-align:middle;">MIDTERM</th>
							<th width="5%" class="head" style="vertical-align:middle;">FINALS</th>
						</tr>
					</thead>
				<?php foreach($submission as $s) { ?>
						<tr>
							<td style="text-align:center; vertical-align:middle; "><?php print($s->course_code); ?></td>
							<td style="text-align:left; vertical-align:middle; "><?php print($s->schedule); ?></td>
							<td style="text-align:center; vertical-align:middle; "><?php print($s->prelim_date); ?></td>
							<td style="text-align:center; vertical-align:middle; "><?php print($s->midterm_date); ?></td>
							<td style="text-align:center; vertical-align:middle; "><?php print($s->finals_date); ?></td>
						</tr>
				<?php 	}
					} else { ?>
						<tr>
							<td style="text-align:center; vertical-align:middle; ">This faculty has not submitted any grade yet...</td>
						</tr>
				<?php }
				} 
			}?>
    	
</table>
</div>
</div>

