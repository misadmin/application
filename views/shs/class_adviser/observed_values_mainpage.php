
		<div style="width:<?php 
								if (isset($value_width)) {
									print($value_width); 
								} else {
									print("70%");
								}
							?>;" >
			<?php 
				if($show_pulldown) {
			?>
					<div style="width:99%; margin:0 auto; ">
						<?php 
							$data = array(
										'academic_terms'=>$academic_terms,
										'selected_term'=>$selected_term,
										'select_id'=>'observed_values',
										'change_select'=>'change_observed_values');
							$this->load->view('shs/reports/pulldown_academic_terms',$data);
						?>
					</div>
			<?php
				}
			?>
			
			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; " id="display_observed_values" >
				<?php 
					$this->load->view('shs/class_adviser/observed_values');
				?>
			</div>
		</div>

			
<script>
	$(document).ready(function(){
		$('.change_observed_values').bind('change', function(){
			
			var element2    = $("option:selected", "#observed_values");
			var term_id     = element2.val();
			
			var student_idno = '<?php print($idnum); ?>';

			$('#show_change_icon_observed_values').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+student_idno,
				data: {	"term_id": term_id,
						"action": "change_term_for_obeserved_values" },
				dataType: 'json',
				success: function(response) {													

					$('#display_observed_values').html(response.output); 

					$('#show_change_icon_observed_values').html("")
										
				}
			});

		});
	});
</script> 

