<?php

	class AcademicYears_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListAcademicTerms($not_previous=TRUE){ 
			$result = null;
			
			if ($not_previous) {
				//UPDATED: 3/6/2013 By: Genes
				//show only current and upcoming terms
				$q = "SELECT a.id,
							 b.id AS academic_years_id,
							CASE a.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
							END AS term, 
							CONCAT(b.end_year-1,'-',b.end_year) AS sy
						FROM 
							academic_terms AS a, 
							academic_years AS b 
						WHERE 
							a.academic_years_id=b.id 
							AND a.status != 'previous'
						ORDER BY 
							b.end_year DESC,
							a.term ";
			} else {
				$q = "SELECT a.id,
							 b.id AS academic_years_id,
							CASE a.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
							END AS term, 
							CONCAT(b.end_year-1,'-',b.end_year) AS sy
						FROM 
							academic_terms AS a, 
							academic_years AS b 
						WHERE 
							a.academic_years_id=b.id 
							and a.term>0
						ORDER BY 
							b.end_year DESC,
							a.term desc";			
			}
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		//EDITED: 8/18/13 by genes added my_term
		function getAcademicTerms($id=null)	{
			if($id == null) { return null; }
			
			$result = null;
			
			$q = "SELECT 
						a.id, 
						b.id AS academic_years_id,
						CASE a.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
						END AS term,
						a.term AS my_term, 
						CONCAT(b.end_year-1,'-',b.end_year) AS sy
					FROM 
						academic_terms AS a, 
						academic_years AS b 
					WHERE 
						a.academic_years_id=b.id 
						AND a.id={$this->db->escape($id)} ";
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
			function getCurrentAcademicTerm() {
			$result = null;
			//sorry for the edit... Justing, commented a.sy since this is not on the academic_terms table
			$q1 = "SELECT a.academic_years_id, a.id, 
						CASE a.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
							END AS term, 
						CONCAT(b.end_year-1,'-',b.end_year) AS sy
					FROM 
						academic_terms AS a, 
						academic_years AS b
					WHERE 
						a.academic_years_id=b.id 
						AND b.status='current' 
						AND a.status='current' 
					ORDER BY b.end_year DESC, a.term DESC 
					LIMIT 1";
			
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		
		//ADDED: 02/16/13
		//Edited by Justing...
		//added limit...
		function ListAcademicYears($limit=0) {
			$q = "SELECT 
						a.id,
						a.end_year,
						a.end_year-1 AS start_year
					FROM
						academic_years AS a
					WHERE 
						a.status != 'previous' 
					ORDER BY 
						a.end_year DESC
					";
			
			if ( ! empty ($limit)){
				$q .= " LIMIT {$this->db->escape($limit)}";
			}
			//print_r($q);
			//die();
			$query = $this->db->query($q);
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		//Added: 2/23/2013
		function GetAcademicYear($aca_id) {
			$q = "SELECT 
						a.id,
						a.end_year,
						a.end_year-1 AS start_year,
						CONCAT('SY ',a.end_year-1,'-',a.end_year) AS sy,
						a.start_date						
					FROM
						academic_years AS a
					WHERE 
						a.id = {$this->db->escape($aca_id)}";
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
		
		function current_academic_year(){
			$sql = "
				SELECT
					id, end_year, start_date
				FROM
					academic_years
				WHERE
					status='current'
				";
			$query = $this->db->query($sql);
			if($query && $query->num_rows() > 0){
				return $query->row();
			} else {
				return FALSE;
			}
		}
		
	//Added: 4/11/2013
	function getPeriod($curdate) {
			$q = "SELECT 
						a.period
				  FROM
						periods as a
					WHERE 
						{$this->db->escape($curdate)} >= a.start_date 
						 AND {$this->db->escape($curdate)} <= a.end_date  ";
		//	print($q);
		//	die();			
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
	//Added:  4/12/2013	
	function getRefDate($term) {
		$result = NULL;
			if($term == '1st Semester' OR $term == '2nd Semester'){
				$q = "SELECT *
				  		FROM
							academic_terms 
						WHERE
							academic_terms.status = 'current'
							AND CURDATE() <= DATE_ADD(academic_terms.start_date, INTERVAL 96 DAY) ";
			}else {
				$q = "SELECT *
				  		FROM
							academic_terms 
						WHERE
							academic_terms.status = 'current'
							AND CURDATE() <= DATE_ADD(academic_terms.start_date, INTERVAL 7 DAY) ";
			}		
			
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
	
	//Added: 4/18/2013
	
		function getTermDate($acadYear_id, $term) {
		$result = NULL;
			$q = "SELECT 
						a.start_date
				  FROM
						academic_terms as a
					WHERE
						a.cademic_years_id = {$this->db->escape($acadYear_id)} 
						AND a.term = {$this->db->escape($acadYear_id)} ";
			//print($q);
			//die();			
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
	
		function ListAcadYears() {
			$result = NULL;
			
			$q = "SELECT 
						a.id,
						a.end_year,
						CONCAT('SY ',a.end_year-1,'-',a.end_year) AS sy
				  FROM
						academic_years AS a
				  ORDER BY
				  		a.end_year DESC";
			//print($q);
			//die();			
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
}


//end of AcademicYears_model.php
