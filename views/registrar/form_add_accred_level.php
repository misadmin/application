<h2 class="heading" style="margin-top:60px;">New Accreditation Level</h2>
<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<form id="addTown" method="post" action="" class="form-horizontal">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" name="step" value="2" />
			<div class="control-group formSep">
				<div class="control-label">College:</div>
				<div class="controls">
					<select name="college_id" id="college_id">
							<option value="00007">CAS</option>											
								<?php						
									foreach($colleges AS $college) {
											echo "<option value=".$college->id.">" . $college->college_code ."</option>";	
									}
								?>					
					</select> 
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Program:</div>
				<div class="controls">
						<select name="program_id" id="program_id">						
								<option value="">-- Select Program--</option>										
										<?php 					
											foreach($programs AS $programs) {
												print("<option value=".$programs->id." class=".$programs->colleges_id.">" . $programs->abbreviation ."</option>");
											}
										?>									
						</select> 
								
				</div>
			</div>
			
			<div class="control-group formSep">
				<div class="control-label">Accreditor:</div>
				<div class="controls">
					<select name="accred_id" id="accred_id">
							<option value="">-- Select Accreditor--</option>											
								<?php						
									foreach($accreditors AS $accreditor) {
											echo "<option value=".$accreditor->id.">" . $accreditor->acronym ."</option>";	
									}
								?>					
					</select> 
				</div>
			</div>
			
			<div class="control-group formSep">
				<div class="control-label">Level:</div>
				<div class="controls">
					<input type="text" name="level" style="width:50px;">
				</div>
			</div>
			
			<div class="control-group formSep">
					<label for="date_taken" class="control-label">Date of Approved:</label>
					<div class="controls">
						<div class="input-append date" id="da1" data-date-format="yyyy-mm-dd">
							<input type="text" name="date_approved" id="date_approved" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
			</div>
			
			<div class="control-group formSep">
					<label for="date_taken" class="control-label">Start Date:</label>
					<div class="controls">
						<div class="input-append date" id="da2" data-date-format="yyyy-mm-dd">
							<input type="text" name="start_date" id="start_date" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
			</div>
			
			<div class="control-group formSep">
					<label for="date_taken" class="control-label">End Date:</label>
					<div class="controls">
						<div class="input-append date" id="da3" data-date-format="yyyy-mm-dd">
							<input type="text" name="end_date" id="end_date" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
			</div>
			
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					 <button class="btn btn-success" type="submit">Add New Accreditation Level!</button> 
				</div>
			</div>
		</form>
		</div>
	</div>
</div>

<!--  <script language="JavaScript" type="text/javascript" src=<?php print(base_url('assets/js/chained_select/')."/jquery.min.js"); ?> ></script> -->
<!-- <script language="JavaScript" type="text/javascript" src=<?php print(base_url('assets/js/chained_select/')."/jquery.chained.min.js"); ?> charset="utf-8"></script> -->

<script type="text/javascript" charset="utf-8">
          $(function(){
              $("#program_id").chained("#college_id"); 
             
});
</script>

<script>
	$(document).ready(function(){
		$('#da1').datepicker();
	
	});
</script>

<script>
	$(document).ready(function(){
		$('#da2').datepicker();
	
	});
</script>

<script>
	$(document).ready(function(){
		$('#da3').datepicker();
	
	});
</script>


<script>

<script type="text/javascript">
		function hide(obj)
		  {
		      var obj1 = document.getElementById(obj);
		      obj1.style.display = 'none';
		  }

		function show(obj)
		  {
		      var obj1 = document.getElementById(obj);
		      obj1.style.display = 'block';
		  }
		
		function CheckValue(field) {
			if (field.value >= 101) {
				hide('end_date');
			} else {
				show('end_date');
			}
		}
		
</script>


<script>
	$(document).ready(function(){
		$('#dp2').datepicker();
	});
	
	$(document).ready(function(){
		$('#dp3').datepicker();
	});
	
	$(document).ready(function(){
		$('#new_withdrawal_sked').validate({
			onkeyup: false,
			errorClass: 'error',
			validClass: 'valid',
			rules: {
				amount: { required: true, minlength: 1 },
				type: {required: true},
			},
			highlight: function(element) {
				$(element).closest('div').addClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						unhighlight: function(element) {
							$(element).closest('div').removeClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						errorPlacement: function(error, element) {
							$(element).closest('div').append(error);
						}
		});
	$(".money").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});


	$('#year').change(function(){
		var year_id = $('#year option:selected').val();
		var level_id = $('#levels option:selected').val();

		
		$.ajax({
			url: "<?php echo site_url("ajax/sections"); ?>/?year=" + year_id + "&level=" + level_id ,
			dataType: "json",
			success: function(data){
				var doptions = make_options_section(data);
				$('#section').html(doptions);
			}	
		});	
	});



	$('#levels').change(function(){
		var level_id = $('#levels option:selected').val();
		
		$.ajax({
			url: "<?php echo site_url("ajax/year"); ?>/?level=" + level_id,
			dataType: "json",
			success: function(data){
				var doptions = make_options_year(data);
				$('#year').html(doptions);
			}	
		});	
	});


function make_options_year (data){
	var doptions = '<option value="">-- Select Year --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].yr_level
		 	+ '">'
		 	+ data[i].yr_level
		 	+ '</option>';
	}
	return doptions;
}

function make_options_section (data){
	var doptions = '<option value="">-- Select Section --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].section
		 	+ '</option>';
	}
	return doptions;
}

</script>






  