
			<div class="col1" id="brgy_st"></div>
			<div class="col2a" id="show_select_brgy">
				<select name="select_brgy" id="select_brgy" class="form-control" style="font-style:italic;" >
					<?php
						foreach ($brgys AS $brgy) {
							print("<option value=".$brgy->id.">".$brgy->name."</option>");
						}
					?>
					<option value="new_brgy" style="font-weight:bold;" id="new_brgy_st"></option>
				</select>
			</div>

			
<script>
	$(document).ready(function(){

		$("select#select_brgy").change(function(){

			$('#new_brgy_group').hide();
			
			var country_name = $("select#select_country option:selected").attr('country_name'); 
			var brgy_id      = $("select#select_brgy option:selected").attr('value'); 

			if (brgy_id.length > 0 ) { 

				if (brgy_id == 'new_brgy') {
					if (country_name == 'Philippines') {
						$('#new_brgy_group').show();
						$('#new_brgy_name3').focus();						
					} else {
						$('#new_street_group').show();
						$('#new_street_name4').focus();
					}
				} else {	
					$('#edit_brgy_group').show();					
				}
			}
		});
	});
</script>
			