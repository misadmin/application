<?php

	class Payments_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


    	
	function ListStudentPayments($payers_id) {
		$result = 0;
		$q = "
			SELECT 
				date(p.transaction_date) payment_date, 
				if(p.from_isis='Y',if(substr(p.isis_refno,1,4)='Adj#' or substr(p.isis_refno,1,3)='AR#', td.description2, td.description), tc.description) description,				
				if(p.from_isis='Y',p.isis_status,p.`status`) `status`, 
				p.receipt_amount, 
				if(p.from_isis='Y',p.isis_refno, p.receipt_no) receipt_no,
				p.machine_ip, p.tellers_id, em.empno, s.idno,
				concat_ws(', ',em.lname,em.fname) emp_name,
				concat_ws(', ',s.lname,s.fname) stud_name
			FROM payments p
				LEFT JOIN payment_items it ON it.payments_id = p.id
				LEFT JOIN teller_codes 	tc ON tc.id = it.teller_codes_id
				LEFT JOIN teller_codes 	td ON td.teller_code = p.isis_tran_code
				LEFT JOIN tellers 		t1 ON t1.id = p.tellers_id
				LEFT JOIN tellers       t2 ON t2.id = p.tellers_id
				LEFT JOIN employees 	em ON em.empno = t1.employees_empno
				LEFT JOIN students   	s 	ON s.idno = t2.students_idno
			WHERE 
				!isnull(if(p.from_isis='Y',substr(p.isis_refno,4,60), p.receipt_no))
				and p.receipt_amount > 0 
				and p.payers_id = {$this->db->escape($payers_id)}
			ORDER BY p.transaction_date desc 				
				";

		$q2 = "
			SELECT
				date(p.transaction_date) payment_date,
				if(p.from_isis='Y', if(p.isis_tran_code in ('WS','add more here'),td.description2, td.description), tc.description) description,
				if(p.from_isis='Y', p.isis_status,p.`status`) `status`, p.receipt_amount,
				if(p.from_isis='Y',p.isis_refno, p.receipt_no) receipt_no,
				p.machine_ip, p.tellers_id, em.empno, s.idno,
				concat_ws(', ',em.lname,em.fname) emp_name,
				concat_ws(', ',s.lname,s.fname) stud_name
			FROM payments p
				LEFT JOIN payment_items it ON it.payments_id = p.id
				LEFT JOIN teller_codes 	tc ON tc.id = it.teller_codes_id
				LEFT JOIN teller_codes 	td ON td.teller_code = p.isis_tran_code
				LEFT JOIN tellers 		t1 ON t1.id = p.tellers_id
				LEFT JOIN tellers       t2 ON t2.id = p.tellers_id
				LEFT JOIN employees 	em ON em.empno = t1.employees_empno
				LEFT JOIN students   	s 	ON s.idno = t2.students_idno
			WHERE
				p.payers_id = {$this->db->escape($payers_id)} and p.receipt_amount > 0 and !isnull(p.receipt_no) 
				and !(substr(concat_ws('',p.isis_refno),1,4) = 'Adj#' or substr(concat_ws('',p.isis_refno),1,3)='AR#' or substr(concat_ws('',p.isis_refno),-2,2)='FE' )				
			ORDER BY p.transaction_date desc
		";
		
		
			//print_r($q);die();
			$query = $this->db->query($q2);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}
		

}

//end of payments_model.php