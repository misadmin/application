
<link rel="stylesheet" type="text/css" href="../assets/DataTables-1.10.16/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="../assets/Buttons-1.5.1/css/buttons.dataTables.min.css"/> 

<!--<script type="text/javascript" src="../assets/jquery-1.12.4.js"></script>-->
<script type="text/javascript" src="../assets/DataTables-1.10.16/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="../assets/Buttons-1.5.1/js/dataTables.buttons.min.js"></script>

<script type="text/javascript" src="../assets/Buttons-1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="../assets/JSZip-2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="../assets/pdfmake-0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="../assets/pdfmake-0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="../assets/Buttons-1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/Buttons-1.5.1/js/buttons.print.min.js"></script>
 

<div class="span11 row-fluid">
<div class="heading">
	<?php 
		$displayTerm = ' ';
		if($term==469){
			$displayTerm = "1st Sem 2018-2019";
		} elseif ($term==470) {
			$displayTerm = "2nd Sem 2018-2019";
		} 
	?>

	<h2>List of NSTP Students</h2>
	<h3><?php echo $displayTerm; ?></h3>
</div>

<style>
	.display {
 			font-family:Segoe UI,Verdana,sans-serif;
  			font-size:1em; <font color='red'>/* Never set font sizes in pixels! */</font>
  			color:#00f;
	}
</style>

<table id="nstp_list" class="display cell-border compact" cellspacing="0" width="100%" >

		<thead>
			<th text-align: "right"; width="4%">No</th>
			<th class="left" width="25%">Name</th>
			<th class="left" width="5%">Gender</th>
			<th class="center" width="5%">Birthday</th>
			<th class="center" width="5%">Contact No.</th>
			<th class="left" width="10%">Program/Year</th>
			<th class="left" width="12%">Barangay</th>
			<th class="left" width="12%">Town</th>
			<th class="left" width="5%">Province</th>
			<th class="left" width="9%">Course Enrolled</th>
			<th class="center" width="7%">Final Grade</th>
		</thead>		

		<?php if(isset($data)) {
			$x = 1;
			foreach($data as $row) { ?>
		<tr>
				  <td text-align: "right"><?php echo $x.'.' ?></td>
				  <td class="left"><?php echo $row->name ?></td>
				  <td class="left"><?php echo $row->gender ?></td>
				  <td class="left"><?php echo date_format(date_create($row->birthday),"d/m/Y") ?></td>
				  <td class="left"><?php echo $row->phone_number ?></td>
				  <td class="left"><?php echo $row->program_year ?></td>
				  <td class="left"><?php echo $row->barangay ?></td>
				  <td class="left"><?php echo $row->town ?></td>
				  <td class="left"><?php echo $row->province ?></td>
				  <td class="left"><?php echo $row->course_code ?></td>
				  <td class="center"><?php echo $row->finals_grade ?></td>
		</tr>
		<?php
				$x = $x + 1;
			}
		} ?>

</table>

</div>

<script>
	$(document).ready(function() {
		var fileIdentifier = "<?php echo $displayTerm ?>";
	    $('#nstp_list').DataTable( {
	        dom: 'B<"clear">lfrtip',
	        "pageLength": 100,
	        buttons: { 
	        	name: 'primary',
	        	buttons: [
	            {
	                extend: 'excelHtml5',
		            text: 'Export to EXCEL',
	                title: 'List of NSTP Students '+fileIdentifier
	            },
	            {
	                extend: 'pdfHtml5',
		            text: 'Export to PDF',
		            pageSize: 'LEGAL', 
		            orientation: 'landscape',
					customize : function(doc) {doc.pageMargins = [15, 20, 80, 20 ]; },
	                title: 'List of NSTP Students '+fileIdentifier
	            },
	            {
		            extend: 'print',
		            text: 'View Printable Version',
		            autoPrint: true,
	                title: 'List of NSTP Students '+fileIdentifier
	            },
	        ] }


  	    } );
	} );
</script>

