<div style="float:left; width:auto;">
<form id="search_faculty" action="<?php 
	switch ($this->uri->segment(2)) {
		case 'masterlist':
			print(site_url('dean/masterlist'));
			break;
		case 'deans_list':
			print(site_url('dean/deans_list'));
			break;
	} ?>" method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="2" name="step" />
	
	
<div class="formSep">
	<h3>Choose a Term and a Program</h3>
</div> 
	<fieldset>
		<div class="control-group formSep">
			<label for="deanslist" class="control-label"> <b> Academic Term: </b> </label>
				<div class="controls">
					<select id="academic_terms" name="academic_terms">
							<!--  <option value="">-- Select Academic Term --</option>  -->
          			<?php
						foreach($academic_terms AS $academic_term) {
							print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");	
						}
					?>
					</select>
				</div>
		</div>
		<div class="control-group formSep">
			<label for="deanslist" class="control-label"><b>Program:</b></label>
				<div class="controls">
					<select id="program" name="program">
						<?php if ($program) {
								foreach($program as $p) { ?>
									<option value="<?php print($p->id); ?>"> <?php print($p->abbreviation); ?> </option>
								<?php } 
								} ?>
					</select>
				</div>
		</div>
		<div class="control-group formSep">
			<label for="deanslist" class="control-label"><b>Year Level:</b></label>
				<div class="controls">
					<select id="yearlevel" name="yearlevel">
						<option value="1"> First Year </option>
						<option value="2"> Second Year </option>
						<option value="3"> Third Year </option>
						<option value="4"> Fourth Year </option>
						<option value="5"> Fifth Year </option>
					</select>
				</div>
		</div>
		<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">View Master List Students</button>
					</div>
		</div>	
	</fieldset>
</form>

