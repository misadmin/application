<?php if ( !empty($descriptive_title)): ?>
<?php 
	$userinfo = $this->session->all_userdata();
	//log_message("info", print_r($userinfo,true)); // Toyet 3.22.2019 
	log_message("info",print_r($userinfo['role'],true));
	log_message("info",print_r($userinfo['empno'],true));
	// 00192 - Victoria C. Millanar 
	if($userinfo['role']=='registrar' and 
       $userinfo['empno']==1637){
		log_message("info",'HAVE ACCESS TO GRADES RECTIFICATION!!!!');
	}
	//log_message("info",print_r($results,true)); 
?>
	<h3 class="heading">Course: <?php echo $descriptive_title . "({$course_code}) {$section_code}"; ?></h3>
	<div style="text-align:left; float:left; padding: 3px;">
		<a id="download_grading_sheet" href="" course_description="<?php print($course_code.'/'.$section_code.' '.$descriptive_title); ?>"
			course_offerings_id="<?php print(isset($course_offerings_id)?$course_offerings_id:''); ?>"
			credit_units="<?php print(isset($credit_units)?$credit_units:''); ?>" 
			download_type="with_grades" >
			Download Grading Sheet as PDF<i class="icon-download-alt"></i></a>
	</div>
	<div style="text-align:left; float:left; padding: 3px;">
		<a id="download_class_list" href="" course_description="<?php print($course_code.'/'.$section_code.' '.$descriptive_title); ?>"
			course_offerings_id="<?php print(isset($course_offerings_id)?$course_offerings_id:''); ?>"
			credit_units="<?php print(isset($credit_units)?$credit_units:''); ?>" 
			download_type="with_grades" >
			Download Class List as PDF<i class="icon-download-alt"></i></a>
	</div>
	<div style="text-align:right; float:right; padding: 3px;">
		<a id="download_blank_grading_sheet" href="" course_description="<?php print($course_code.'/'.$section_code.' '.$descriptive_title); ?>"
			course_offerings_id="<?php print(isset($course_offerings_id)?$course_offerings_id:''); ?>"
			credit_units="<?php print(isset($credit_units)?$credit_units:''); ?>"
			download_type="blank" >
			Download Blank Grading Sheet as PDF<i class="icon-download-alt"></i></a>
	</div>
	<?php endif; ?>
<table class="table table-striped table-bordered mediaTable activeMediaTable" id="MediaTable-0">
	<thead>
		<tr>
			<th>No.</th>
			<th>Student ID</th>
			<th>Academic Program</th>
			<th>Student Name</th>
			<th>Prelim</a></th>
			<th>Midterms</th>
			<th>Finals</th>
			<th style="display:none"></th>
			<th style="display:none"></th>
		</tr>
	</thead>
	<tbody>
<?php if( ! empty($results)): $count=0; ?>
<?php foreach($results as $result): $count++; ?>
		<tr>
			<td><?php echo $count; ?></td>
			<td><?php echo $result->idno; ?></td>
			<td style="width:15% !important;"><?php echo $result->year_level . " " . $result->academic_program; ?></td>
			<td style="width:45% !important;"><?php echo $result->lname . ", " . $result->fname ." ". $result->mname; ?></td>

			<!-- This portion is edited to accommodate rectification of grades by class 
			     by the registrar role and exclusively locked to (00192-Victoria C. Millanar)
			     -Toyet 3.22.2019
			-->
			<?php
			if( $userinfo['role']=='registrar' and 
       			($userinfo['empno']==1637 or $userinfo['empno']==192 or $userinfo['empno']==606)) { ?>
				<td class="prelim col-lg-1"><input type='text' id="prelim" style="width:35% !important;" maxlength="4" value="<?php echo trim ($result->prelim_grade); ?>"></td>
				<td class="midterm col-lg-1"><input type='text' id="midterm" style="width:35% !important;" maxlength="4" value="<?php echo trim($result->midterm_grade); ?>"></td>
				<td class="final col-lg-1"><input type='text' id="final" style="width:35% !important;" maxlength="4" value="<?php echo trim($result->finals_grade); ?>"></td>
       		<?php } else { ?>
				<td style="text-align: center" class="prelim"><?php echo $result->prelim_grade; ?></td>
				<td style="text-align: center" class="midterm"><?php echo $result->midterm_grade; ?></td>
				<td style="text-align: center" class="final"><?php echo $result->finals_grade; ?></td>
			<?php } ?>

			<td style="display:none"><?php echo $result->student_history_id; ?></td>
			<td style="display:none"><?php echo $result->course_offerings_id; ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="6">No Record Found</td>
		</tr>
<?php endif; ?>
	</tbody>
</table>

<?php
   if($userinfo['role']=='registrar' and
      ($userinfo['empno']==1637 or $userinfo['empno']==192 or $userinfo['empno']==606)) { ?>
      <div class="text-right">
            <br><button type="button" class="btn btn-primary" name="btn_rectify" id="btn_rectify">Rectify Grades</button>
      </div>
<?php } ?>

<form id="dload_grade_sheet_form" method="post" target="_blank">
		<input type="hidden" name="action" value="download_grading_sheet" />
</form>


<script>
	$(document).ready(function(){

		$('#download_grading_sheet').click(function(event){
			event.preventDefault();
			var course_description = $(this).attr('course_description');
			var course_offerings_id = $(this).attr('course_offerings_id');
			var credit_units = $(this).attr('credit_units');
			var download_type = $(this).attr('download_type');
			
			$('<input>').attr({
				type: 'hidden',
				name: 'course_description',
				value: course_description,
			}).appendTo('#dload_grade_sheet_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'course_offerings_id',
				value: course_offerings_id,
			}).appendTo('#dload_grade_sheet_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'credit_units',
				value: credit_units,
			}).appendTo('#dload_grade_sheet_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'download_type',
				value: download_type,
			}).appendTo('#dload_grade_sheet_form');
			
			$('#dload_grade_sheet_form').submit();

		});
	});
</script>


<form id="dload_blank_grade_sheet_form" method="post" target="_blank">
		<input type="hidden" name="action" value="download_grading_sheet" />
</form>
<script>
	$(document).ready(function(){

		$('#download_blank_grading_sheet').click(function(event){
			event.preventDefault();
			var course_description = $(this).attr('course_description');
			var course_offerings_id = $(this).attr('course_offerings_id');
			var credit_units = $(this).attr('credit_units');
			var download_type = $(this).attr('download_type');
			
			$('<input>').attr({
				type: 'hidden',
				name: 'course_description',
				value: course_description,
			}).appendTo('#dload_blank_grade_sheet_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'course_offerings_id',
				value: course_offerings_id,
			}).appendTo('#dload_blank_grade_sheet_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'credit_units',
				value: credit_units,
			}).appendTo('#dload_blank_grade_sheet_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'download_type',
				value: download_type,
			}).appendTo('#dload_blank_grade_sheet_form');
			
			$('#dload_blank_grade_sheet_form').submit();

		});
	});
</script>



<form id="download_class_list_form" method="post" target="_blank">
		<input type="hidden" name="action" value="download_class_list" />
</form>
<script>
	$(document).ready(function(){

		$('#download_class_list').click(function(event){
			event.preventDefault();
			var course_description = $(this).attr('course_description');
			var course_offerings_id = $(this).attr('course_offerings_id');
			var credit_units = $(this).attr('credit_units');
			var download_type = $(this).attr('download_type');
			
			$('<input>').attr({
				type: 'hidden',
				name: 'course_description',
				value: course_description,
			}).appendTo('#download_class_list_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'course_offerings_id',
				value: course_offerings_id,
			}).appendTo('#download_class_list_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'credit_units',
				value: credit_units,
			}).appendTo('#download_class_list_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'download_type',
				value: download_type,
			}).appendTo('#download_class_list_form');
			
			$('#download_class_list_form').submit();

		});
	});
</script>


<script>
	$( "#btn_rectify" ).click(function() {
		var rectify_values = [];
		var new_row = [];
		$("#MediaTable-0 tr:gt(0)").each(function () {
			var this_row = $(this); 
	       	var stidno = $.trim(this_row.find('td:eq(1)').html());
       		console.log(stidno); 
			var prelim = $.trim(this_row.find('td:eq(4) input').val()); 
			var midterm = $.trim(this_row.find('td:eq(5) input').val()); 
			var final = $.trim(this_row.find('td:eq(6) input').val()); 
			var student_history_id = $.trim(this_row.find('td:eq(7)').html());
			var course_offering_id = $.trim(this_row.find('td:eq(8)').html());
			new_row = [ student_history_id, course_offering_id, prelim, midterm, final ];
			rectify_values.push( new_row );
		}); 
		console.log( 'before ajax' );
		console.log( rectify_values );
		$.ajax({
			url      : 	'<?php echo site_url('ajax/rectify_grades'); ?>',
			type     : 	'post',
			data     : { 'rectify_values': rectify_values },
			dataType : 	'json',
			success  : 	function( response ) {
							console.log(response);
						},
			error    :  function (response){
						    console.log('error');
					    }
		});
	}); 
</script>
