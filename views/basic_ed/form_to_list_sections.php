<h2 class="heading" style="margin-top:20px;">List of Sections</h2>

<div style="text-align:center; width:100%;">
		<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; text-align:right;
						border-color: #D8D8D8; padding: 5px; margin-bottom:20px; margin-top:20px; margin-left:40px; vertical-align:middle;
						width:45%;">
				<a href="#" data-toggle="modal" data-target="#modal_add_section" class="btn btn-success"
								data-project-id='<?php //print($student_tor); ?>'><i class="icon-plus icon-white"></i> Add Section</a>	
		</div>		

		<!-- Modal to Create New Section -->
 		
		      <div class="modal hide fade" id="modal_add_section" >
		 		<form method="post" id="add_section_form">
		   				<input type="hidden" name="action" value="2" />
		   				<?php $this->common->hidden_input_nonce(FALSE); ?>
		   		<div class="modal-header">
		       		<button type="button" class="close" data-dismiss="modal">�</button>
		       		<h3>Create New Section</h3>
		   		</div>
		   		<div class="modal-body">
		   		
		   			<div style="text-align:left;">
						<table border="0" cellpadding="2" cellspacing="0" 
							class="table table-striped table-condensed table-hover" style="width:100%;">
							<tr>
								<td>Level</td>
								<td>:</td>
								<td>
									<select id="levels" name="level" class="form">
										<option value="">-- Select Level --</option>
											<?php foreach ($levels as $level): ?>
												<option value="<?php echo $level->id; ?>"><?php echo $level->level; ?></option>
											<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Section</td>
								<td>:</td>
								<td><input type="text" name="section" style="width:400px;"></td>
							</tr>
							
						</table>
					</div>
		   		
		   		</div>
			   <div class="modal-footer">
		    		<input type="submit" class="btn btn-primary" id="add_section" value="ADD SECTION">
			   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		       </div>
				</form>
		    </div>				
			 
				<!--  End of Modal to Create New Section -->		
						
		<div>
		
		<?php
			if ($sections) {
		?>
			<div style="width:45%; margin-bottom:40px; margin-left:40px;">
			  <table id="section_list" border="0" cellspacing="0" class="table table-condensed table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
			    <thead>
			    <tr>
			      <th width="30%" style="vertical-align:middle; text-align:center;">Section</th>
			      <th width="5%" style="vertical-align:middle; text-align:center;">Department</th>
			   <!-- <th width="5%"  style="vertical-align:middle; text-align:center;">Delete</th>  -->
			       <th width="5%"  style="vertical-align:middle; text-align:center;">Edit</th>
			      </tr>
			    </thead>		
		<?php 
				foreach ($sections AS $section) {
		?>
		    <tr>
		      <td style="text-align:left; vertical-align:middle; "><?php print($section->section_name); ?> </td>
		      <td style="text-align:left; vertical-align:middle; "><?php print($section->level); ?> </td>
		      <td style="text-align:center; vertical-align:middle; ">
			 		<a href="#" data-toggle="modal" data-target="#modal_edit_section_<?php print($section->id); ?>" 
								data-project-id='<?php print($section); ?>'><i class="icon-edit"></i></a>

		      
				  <!-- modal for EDITING sections -->
				      
				      <div class="modal hide fade" id="modal_edit_section_<?php print($section->id); ?>" >
				 		<form method="post" id="edit_section_form_<?php print($section->id); ?>">
				   				<input type="hidden" name="action" value="3" />
				   				 <input type="hidden" name="section_id" value= "<?php print($section->id);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
				   		<div class="modal-header">
				       		<button type="button" class="close" data-dismiss="modal">�</button>
				       		<h3>Edit Section</h3>
				   		</div>
				   		<div class="modal-body">
				 			<div style="text-align:center;">
								<table border="0" cellpadding="2" cellspacing="0" 
									class="table table-striped table-condensed table-hover" style="width:100%;">
									
									<tr>
										<td>Section</td>
										<td>:</td>
										<td><input type="text" name="section" style="width:300px;" value="<?php print($section->section_name); ?>"></td>
									</tr>	
									
								</table>
							</div>
				   		</div>
					   <div class="modal-footer">
				    		<input type="submit" class="btn btn-primary" id="edit_section_<?php print($section->id); ?>" value="EDIT SECTION!">
					   		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				       </div>
						</form>
				    </div>		
				     <!-- End of EDITING a course from other schools -->
								
			</td>
		   </tr>
							
		<?php
				}
		?>
		</table>
		</div>
		<?php 		
			}
		?>
		  </div>
</div>	

<form id="edit_sec" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="3" />
		
</form>			


<script>
	$('.edit_section').click(function(event){
		event.preventDefault(); 
	
		     var section_id = $(this).attr('href');
		
	  		$('<input>').attr({
			    type: 'hidden',
			    name: 'section_id',
			    value: section_id,
			}).appendTo('#edit_sec');
			$('#edit_sec').submit();
		
	});
</script>


<form id="delete_religion_form" method="post" >
	<input type="hidden" name="action" value="delete_religion" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>			

<script>
	$('.delete_religion').bind('click', function(event){
		event.preventDefault();
		var confirmed = confirm("Continue to delete religion?");
							
		if (confirmed){
			var id = $(this).attr('href');
								
			$('<input>').attr({
			    type: 'hidden',
			    name: 'religion_id',
			    value: id,
			}).appendTo('#delete_religion_form');
			$('#delete_religion_form').submit();
		}		
	});
</script>






<script>
$().ready(function() {
	$('#religion_list').dataTable( {
	    "aoColumnDefs": [{ "bSearchable": true, "aTargets": [ 1,2] }],
		"iDisplayLength": 50,
	    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],	    
    });
    $("#religion_list").stickyTableHeaders();
    $('#religion_list').stickyTableHeaders('updateWidth');
	
	
})
</script>
 
