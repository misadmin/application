<style>
	table.balances 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
</style>

<h3 class="heading">List of ISIS & MIS mismatched balances as of 15 Oct 2013:</h3>
<div class="loading"></div>

<div class="row-fluid">
	<?php if(isset($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $message; ?>
	</div>
	<?php endif; ?>
	<form id="frm_mismatch" method="post" action="" class="form-horizontal">
		<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<label for="college" class="control-label">College: </label>
					<div class="controls">
					<select id="colleges" name="college">
						<option value="">-- Select College --</option>	
							<?php foreach ($colleges as $college): ?>
							<option <?php echo ($college->id==$this->input->post('college') ? 'selected="selected"' : ''); ?> value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				<label for="programs" class="control-label">Program: </label>
					<div class="controls">
						<select id="programs" name="program">
						<option value="">-- Select Program --</option>	
						</select>
						<button class="btn btn-primary" type="submit">Submit</button>
					</div>
						
			</fieldset>
	</form>
</div>
<form method="post" action="<?php echo site_url('extras/mismatches_view'); ?>" id="mismatches_form">
	<?php echo $this->common->hidden_input_nonce(); ?>
</form>
<hr>
<div id="table_here" class="span10">
<?php 
	if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {

?>

<table id="balances_t" class="table table-bordered table-striped balances">
<thead>
	<tr>
	<th width="5%"></th>
	<th width="15%">IDNO</th>
	<th width="19%">College</th>
	<th width="20%">Program</th>
	<th width="13">ISIS</th>
	<th width="13%">MIS</th>
	<th width="15%">Remark</th>
	</tr>
</thead>

<?php
	if (count($balances) < 1) {
		echo '<td class="center" colspan=7>No mismatched balances found!</td>';
	}else{
		$row = 0;
		foreach($balances as $balance){
			$row++;
?>
			<tr >
			<td class="center"><?php echo $row .'.'; ?> </td>
			<td class="center"><a href="<?php echo site_url($this->session->userdata('role') . '/student') .'/'. $balance['stud_id'] ?>">     
	
			<?php echo $balance['stud_id']; ?>  </a></td>
	
			<td class="center"><?php echo $balance['college_code']; ?></td>
			<td class="center"><?php echo $balance['program']; ?></td>
			<td class="right"><?php echo number_format($balance['isis_balance'],2); ?></td>
			<td class="right"><?php echo number_format($balance['mis_balance'],2); ?></td>
			<td><?php echo $balance['remark']; ?></td>
			</tr>		

<?php
		} 		
	}
?>

</table>

<?php } 
?>

</div>



<script>
$(document).ready(function(){
	$("#balances_t").stickyTableHeaders();
    $("#balances_t").stickyTableHeaders('updateWidth');

	$('#frm_mismatch').validate({
		submitHandler: function(form) {
			$('.loading').delay(5).fadeIn(100);
			form.submit();
		}
	});
    

    
});
    

$('#colleges').change(function(){
	var college_id = $('#colleges option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
		dataType: "json",
		success: function(data){
			var doptions = make_options(data);
			$('#programs').html(doptions);
		}	
	});	
});


function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}


</script>