<div <?php if ( ! empty($tab_id)) echo "id=\"{$tab_id}\""; ?> class="tabbable"><br>
<?php if(isset($contents) && count($contents) > 0):?>
	<ul class="nav nav-tabs">
<?php foreach($contents as $header): ?>
<?php if ( ! empty($children[$header['alias']]) && count($children[$header['alias']] > 0)): ?>
		<li class="dropdown<?php if($header['active']) echo " active"; ?>">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title']; ?> <b class="caret"></b></a>
			<ul class="dropdown-menu">
<?php foreach ($children[$header['alias']] as $child) : ?>
				<li><a href="#<?php echo $child['alias']; ?>" data-toggle="tab"><?php echo $child['title']; ?></a></li>
<?php endforeach; ?>
			</ul>
		</li>
<?php else: ?>
		<li <?php if($header['active']): ?>class="active" <?php endif; ?>><a href="#<?php echo $header['alias']; ?>" data-toggle="tab"><?php echo $header['title']; ?></a></li>
<?php endif; ?>
<?php endforeach; ?>
	</ul>
<?php endif;?>
	<div <?php if ( ! empty($tab_id)) echo "id=\"{$tab_id}Content\""; ?>class="tab-content">
<?php if(isset($contents) && count($contents) > 0): ?>
<?php foreach($contents as $content): ?>
<?php if ( ! empty($children[$content['alias']]) && count($children[$content['alias']] > 0)): ?>
<?php foreach($children[$content['alias']] as $child): ?>
		<div class="tab-pane <?php if($child['active']) echo "active"; ?>" id="<?php echo $child['alias']; ?>">
<?php echo $child['content']; ?>		
		</div>
<?php endforeach; ?>
<?php else: ?>
<?php if( !empty($content['content'])): ?>
		<div class="tab-pane <?php if($content['active']) echo "active"; ?>" id="<?php echo $content['alias']; ?>">
<?php echo $content['content']; ?>		
		</div>
<?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
	</div>
</div>