<?php

	class Shs_enrollments_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
		
		
		function ListEnrollments($academic_terms_id=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							a.id,
							a.description,
							(SELECT 
									COUNT(n.id) 
								FROM 
									students AS m,
									student_histories AS n,
									prospectus AS o
								WHERE 
									m.idno = n.students_idno
									AND n.prospectus_id = o.id
									AND m.gender = 'M' 
									AND n.year_level = 11
									AND n.academic_terms_id = {$this->db->escape($academic_terms_id)} 
									AND o.academic_programs_id = a.id 
									AND n.can_enroll = 'Y'
							) AS m11,
							(SELECT 
									COUNT(n.id) 
								FROM 
									students AS m,
									student_histories AS n,
									prospectus AS o
								WHERE 
									m.idno = n.students_idno
									AND n.prospectus_id = o.id
									AND m.gender = 'F' 
									AND n.year_level = 11
									AND n.academic_terms_id = {$this->db->escape($academic_terms_id)} 
									AND o.academic_programs_id = a.id 
									AND n.can_enroll = 'Y'
							) AS f11,
							(SELECT 
									COUNT(n.id) 
								FROM 
									students AS m,
									student_histories AS n,
									prospectus AS o
								WHERE 
									m.idno = n.students_idno
									AND n.prospectus_id = o.id
									AND m.gender = 'M' 
									AND n.year_level = 12
									AND n.academic_terms_id = {$this->db->escape($academic_terms_id)} 
									AND o.academic_programs_id = a.id 
									AND n.can_enroll = 'Y'
							) AS m12,
							(SELECT 
									COUNT(n.id) 
								FROM 
									students AS m,
									student_histories AS n,
									prospectus AS o
								WHERE 
									m.idno = n.students_idno
									AND n.prospectus_id = o.id
									AND m.gender = 'F' 
									AND n.year_level = 12
									AND n.academic_terms_id = {$this->db->escape($academic_terms_id)} 
									AND o.academic_programs_id = a.id 
									AND n.can_enroll = 'Y'
							) AS f12							
						FROM 
							academic_programs AS a		
						WHERE 
							a.status = 'O' 
							AND a.colleges_id = 12
						ORDER BY 
							a.description ";
			
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		//added Toyet 8.2.2018
		function IDs_For_EnrolledSHS($academic_terms_id=NULL,$year_level='',$level='',$section_id='') {
		$sql = "
		    select sh.students_idno, concat(st.lname,st.fname) as name
            from student_histories sh
            left join prospectus prs on prs.id=sh.prospectus_id
            left join academic_programs ap on ap.id=prs.academic_programs_id
            left join shs_block_sections bs on bs.block_sections_id=sh.block_sections_id
            left join students st on st.idno=sh.students_idno
            where sh.academic_terms_id={$academic_terms_id}
                and bs.block_sections_id={$section_id}
            order by name;
            ";
        //log_message("INFO", print_r($sql,true)); // Toyet 8.2.2018
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			$result = $query->result();
		} 
		
		return $result;
		}
		
	}

?>	