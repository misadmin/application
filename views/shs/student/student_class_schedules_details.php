			<div style="width:99%; padding:3px; overflow:auto; margin:0 auto; margin-top:5px; ">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:10%; height:30px;">Course<br>Code</td>
								<td class="shs_header" style="width:40%; height:30px;">Descriptive Title</td>
								<td class="shs_header" style="width:25%; height:30px;">Schedule</td>
								<td class="shs_header" style="width:25%; height:30px;">Teacher</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($class_schedules) {
									foreach($class_schedules AS $class_sched) {
							?>			
										<tr>
											<td style="text-align:center;"><?php print($class_sched->course_code); ?></td>
											<td><?php print($class_sched->descriptive_title); ?></td>
											<td>
												<?php 
													if ($class_sched->time_sched) {
														foreach ($class_sched->time_sched AS $sched) {
												?>
														<div>
															<?php print($sched->course_sched); ?>
														</div>
												<?php
														}
													}
												?>
											</td>
											<td><?php print($class_sched->teacher); ?></td>
										</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>
