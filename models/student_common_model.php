<?php
class Student_common_model extends MY_Model {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function insert_student ($data){
		$this->load->helper('student_helper');
		
		$next_number = next_student_number ($this->last_id_number());
		 
		$sql = "
			INSERT into
				students
			";
		$fields = "(`idno`,";
		$values = "VALUES ('{$next_number}',";
		foreach ($data as $key =>$val){
			$fields .= "`{$key}`, ";
			$values .= "{$this->db->escape($val)}, "; 
		}
		$fields = trim($fields, ", ") . ")\n";
		$values = trim($values, ", ") . ")\n";
		$sql .= $fields . $values;
		
		$this->db->trans_start();
		if ($query = $this->db->query($sql)) {
			$insertid = $this->db->insert_id();			
			if ($this->db->query("INSERT into payers (`students_idno`) VALUE('{$insertid}')")){
				$this->db->trans_complete();
				return $insertid;
			}else{
				$this->db->trans_rollback();				
			} 
		} else {
			$this->db->trans_rollback();
		}
		return FALSE;		
	}
	
	public function my_information($userid){
		
		$sql = "
				SELECT
					sh.id, s.idno, s.lname, s.fname, s.mname, s.gender, s.dbirth, DATE_FORMAT(s.dbirth, '{$this->config->item('mysql_date_format')}') as dbirth_formatted, 
					s.civil_status, s.stay_with,
					s.student_type, s.citizenships_id, s.religions_id,
					s.meta,
					r.religion,
					CONCAT(IF(ISNULL(addr.birth_address), '', addr.birth_address), IF(ISNULL(addr.birth_barangays_id), '', CONCAT('\n', pbbarangay.name)), IF(ISNULL(addr.birth_towns_id), '', CONCAT('\n', pbtowns.name)), IF(ISNULL(addr.birth_provinces_id), '', CONCAT('\n', pbprov.name)), '\n', IF(ISNULL(addr.birth_countries_id), '', CONCAT('\n', pbco.name))) as full_birth_address, 
					CONCAT(IF(ISNULL(addr.city_address), '', addr.city_address), IF(ISNULL(addr.city_barangays_id), '', CONCAT('\n', city_add_barangays.name)), IF(ISNULL(addr.city_towns_id), '', CONCAT('\n', city_add_towns.name)), IF(ISNULL(addr.city_provinces_id), '', CONCAT('\n', city_add_provinces.name)), IF(ISNULL(addr.city_countries_id), '', CONCAT('\n', city_add_countries.name))) as full_city_address,
					CONCAT(IF(ISNULL(addr.home_address), '', addr.home_address), IF(ISNULL(addr.home_barangays_id), '', CONCAT('\n', home_add_barangays.name)), IF(ISNULL(addr.home_towns_id), '', CONCAT('\n', home_add_towns.name)), IF(ISNULL(addr.home_provinces_id), '', CONCAT('\n', home_add_provinces.name)), IF(ISNULL(addr.home_countries_id), '', CONCAT('\n', home_add_countries.name))) as full_home_address,
					addr.birth_barangays_id, addr.birth_towns_id, addr.birth_provinces_id, addr.birth_countries_id,
					addr.city_barangays_id, addr.city_towns_id, addr.city_provinces_id, addr.city_countries_id,
					addr.home_barangays_id, addr.home_towns_id, addr.home_provinces_id, addr.home_countries_id,
					cz.name as citizenship,
					sh.id as student_histories_id,
					if(isnull(sh.id), bh.levels_id,'') as levels_id,										
					if(isnull(sh.id), bpt1.year_level,sh.year_level) as year_level,					
					bh.status AS bed_status,					
					if(isnull(sh.id),bbs.section_name,'') as section,
					if(isnull(sh.id), bh.prospectus_id, sh.prospectus_id) AS prospectus_id,
					ap.max_yr_level,
					ap.id AS academic_programs_id,
					c.college_code, c.id as colleges_id,
					ap.id as ap_id, 
					if(isnull(sh.id),
						ap1.description, ap.abbreviation ) as abbreviation,
					r.religion,pn.phone_number,
					CONCAT(home_add_barangays.name,', ',home_add_towns.name,', ',home_add_provinces.name) AS my_home_address,
					CASE s.gender
						WHEN 'F' THEN 'Female'
						WHEN 'M' THEN 'Male'
						ELSE 'No Gender'
					END AS my_gender,
					r.religion AS my_religion,
					cz.name AS my_citizenship,
					ap.description AS my_acad_program,
					ap.acad_program_groups_id,
					CONCAT(IF(ISNULL(addr.home_address),'', CONCAT(addr.home_address,', ')), IF(ISNULL(addr.home_barangays_id), '', CONCAT(home_add_barangays.name,', ')), IF(ISNULL(addr.home_towns_id), '', CONCAT(home_add_towns.name,', ')), IF(ISNULL(addr.home_provinces_id), '', CONCAT(home_add_provinces.name,', ')), IF(ISNULL(addr.home_countries_id), '', home_add_countries.name)) as my_full_home_address,
					CONCAT(IF(ISNULL(addr.birth_address), '', CONCAT(addr.birth_address,', ')), IF(ISNULL(addr.birth_barangays_id), '', CONCAT(pbbarangay.name,', ')), IF(ISNULL(addr.birth_towns_id), '', CONCAT(pbtowns.name,', ')), IF(ISNULL(addr.birth_provinces_id), '', CONCAT(pbprov.name,', ')), '', IF(ISNULL(addr.birth_countries_id), '', pbco.name)) as my_full_birth_address,
					sbs.section_name,
					bes1.students_idno AS bed_id,
					bh.enrollment_status
				FROM
					students as s
				LEFT JOIN student_histories as sh ON s.idno=sh.students_idno 
				LEFT JOIN basic_ed_students AS bes1 ON bes1.students_idno = s.idno 
				left join basic_ed_histories bh on bh.students_idno = bes1.students_idno 
				LEFT JOIN prospectus AS bp1 ON bp1.id = bh.prospectus_id 
				LEFT JOIN prospectus_terms AS bpt1 ON bpt1.prospectus_id = bp1.id
				LEFT JOIN academic_programs AS ap1 ON ap1.id = bp1.academic_programs_id 
				left join basic_ed_sections   bs on bs.id = bh.basic_ed_sections_id				
				LEFT JOIN religions as r ON s.religions_id = r.id
				LEFT JOIN addresses addr ON addr.students_idno=s.idno
				LEFT JOIN barangays as pbbarangay ON addr.birth_barangays_id=pbbarangay.id
				LEFT JOIN towns as pbtowns ON pbtowns.id=addr.birth_towns_id
				LEFT JOIN provinces as pbprov ON pbprov.id=addr.birth_provinces_id
				LEFT JOIN countries as pbco ON pbco.id=addr.birth_countries_id
				LEFT JOIN barangays as city_add_barangays ON city_add_barangays.id=addr.city_barangays_id
				LEFT JOIN towns as city_add_towns ON city_add_towns.id=addr.city_towns_id
				LEFT JOIN provinces as city_add_provinces ON city_add_provinces.id=addr.city_provinces_id
				LEFT JOIN countries as city_add_countries ON city_add_countries.id=addr.city_countries_id
				LEFT JOIN barangays as home_add_barangays ON home_add_barangays.id=addr.home_barangays_id
				LEFT JOIN towns as home_add_towns ON home_add_towns.id=addr.home_towns_id
				LEFT JOIN provinces as home_add_provinces ON home_add_provinces.id=addr.home_provinces_id
				LEFT JOIN countries home_add_countries ON home_add_countries.id=addr.home_countries_id
				LEFT JOIN citizenships cz ON s.citizenships_id=cz.id			
				LEFT JOIN prospectus  as  p ON p.id = sh.prospectus_id
				LEFT JOIN academic_programs ap ON  ap.id = p.academic_programs_id
				LEFT JOIN academic_terms tr ON tr.id = sh.academic_terms_id and sh.prospectus_id = p.id
				LEFT JOIN academic_years ay on ay.id = tr.id   
				LEFT JOIN colleges c ON c.id=ap.colleges_id
				LEFT JOIN phone_numbers pn ON s.idno = pn.students_idno and pn.is_primary='Y'
				LEFT JOIN shs_block_sections AS sbs ON sbs.block_sections_id = sh.block_sections_id 
				LEFT JOIN bed_block_sections AS bbs ON bbs.block_sections_id = bh.bed_block_sections_id 
				WHERE
					s.is_active='Y'
					AND s.idno={$this->db->escape($userid)} 
				ORDER BY
					tr.id desc, bh.academic_years_id desc, pn.id desc
				LIMIT 1
			";
		
		//die($sql);
		$query = $this->db->query($sql);
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
		
		if ($query && is_object($query) && $query->num_rows() > 0){
			return $query->row();
		} else
			return FALSE;
	} 

	public function update_information($userid, $data){
		
		$sql = "UPDATE
					students
				SET
				";
		foreach ($data as $key=>$val) {
			if ( $key == 'password' )
				$sql .= "`{$key}` = PASSWORD({$this->db->escape($val)}),\n"; 
			else				
				if ( $key == 'phone_number'){
					$sql .="";
					if (!empty($val) or $val=0 or $val=''){
						if ( !$this->student_common_model->update_student_number($userid,'mobile','student',$val))
							return false;
					}
				}else{
					$sql .= "`{$key}` = {$this->db->escape($val)},\n";										
				}							
			
		}
		$sql = rtrim($sql, ",\n");
		$sql .= "\nWHERE
		idno={$this->db->escape($userid)}
		LIMIT 1";

		//print_r($sql);die();
		
		$this->db->query($sql);
		
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		} else
			return TRUE;
	}
	
	public function update_student_number($userid,$phone_type,$owner_type,$val){
		$sql = "				
				INSERT INTO `phone_numbers` 
					(`students_idno`, `phone_type`, `owner_type`, `phone_number`, `is_primary`) 
				VALUES 
					('{$userid}', '{$phone_type}', '{$owner_type}', '{$val}', 'Y')
				ON DUPLICATE KEY UPDATE `phone_number`= '{$val}'
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		return $query; 		
// 		if ($this->db->_error_number()) {
// 			$this->set_error();
// 			return FALSE;
// 		}
		
				
	}
	
	function list_student_numbers($idno){
		$sql = "
			select id, students_idno, phone_type, owner_type, phone_number, is_primary 
			from phone_numbers
			where students_idno = {$this->db->escape($idno)}
		";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result();
		else 
			return false;
		
	}
	
	function update_number_via_ajax($id, $field_name, $val){
		$sql = "
			update phone_numbers set `{$field_name}` = {$this->db->escape($val)} 
			where `id` = {$this->db->escape($id)}
		";
		
		$query = $this->db->query($sql);
		if ($query && $this->db->affected_rows()>0)
			return TRUE;
		else 
			return FALSE;
	}

	
	function remove_number_via_ajax($id){
		$sql = "
			delete from phone_numbers where id = {$id}
		";
	
		$query = $this->db->query($sql);
		if ($query && $this->db->affected_rows()>0)
			return TRUE;
			else
			return FALSE;
	}
	
	
	public function search ($str, $count=15, $page=1) {
		
		$start = ($page - 1) * $count;
		$sql = "
			SELECT
				idno,
				CONCAT(lname, ', ', fname) as fullname,
				gender,
				fname,
				lname,
				mname
			FROM
				students
			";
		
		if (is_numeric($str)) {
			$sql .= "
				WHERE
					is_active = 'Y'
				AND 
					idno={$this->db->escape($str)}
				LIMIT 1";
		} else {
			if (strpos($str, ',') === FALSE ) {
				$sql .= "
					WHERE
						is_active = 'Y'
					AND
						lname LIKE '" . trim(addslashes($str)) . "%'
					OR
						fname LIKE '" . trim(addslashes($str)) . "%'
					ORDER BY 
						fname, lname
					LIMIT $start, $count";
			} else {
				$str_arr = explode (',', $str);
				$sql .= "
					WHERE
						is_active = 'Y'
					AND
						lname LIKE '" . trim(addslashes($str_arr[0])) . "%'
					AND
						fname LIKE '" . trim(addslashes($str_arr[1])) . "%'
					ORDER BY 
						fname, lname
					LIMIT $start, $count
					";
			}
		}
		
		$query = $this->db->query($sql);
		//print_r($query);die();
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
		
		if ( $query ){
			return $query->result();
		}
	}
	
	public function total_search_results ($str){
		$sql = "
			SELECT
				count(*) as total
			FROM
				students
			";
		
		if (is_numeric($str)) {
			$sql .= "
			WHERE
				is_active='Y' 
			AND
				idno={$this->db->escape($str)}
			";
		} else {
			if (strpos($str, ',') === FALSE ) {
			$sql .= "
				WHERE
					lname LIKE '{$str}%'
				OR
					fname LIKE '{$str}%'
				";
			} else {
			$str_arr = explode (',', $str);
				$sql .= "
					WHERE
						lname LIKE '{$str_arr[0]}%'
					AND
						fname LIKE '" . trim($str_arr[1]) . "%'
					";
			}
		}
		if ($query = $this->db->query($sql)) {
			$res = $query->row();
			return $res->total;
		} else {
			return FALSE;
		}
	}
	
	public function user_is_valid($userid, $password){
		$sql = "SELECT
					s.idno, s.lname, s.fname, s.mname, s.gender, s.dbirth, DATE_FORMAT(s.dbirth, '{$this->config->item('mysql_date_format')}') as dbirth_formatted, 
					s.civil_status,
					s.student_type, s.citizenships_id, s.religions_id,
					sh.id as student_histories_id, sh.prospectus_id,
					sh.year_level as yr_level,
					ap.abbreviation,
					r.role
				FROM
					students as s
				LEFT JOIN
					roles as r
				ON
					s.idno=r.employees_empno
				LEFT JOIN
					student_histories sh
				ON
					sh.students_idno=s.idno
				LEFT JOIN
					prospectus p
				ON
					sh.prospectus_id=p.id
				LEFT JOIN
					academic_programs ap
				ON
					p.academic_programs_id=ap.id
				WHERE
					s.is_active='Y' 
				AND			
					s.idno={$this->db->escape($userid)}
				AND
					s.password=PASSWORD({$this->db->escape($password)})
				ORDER BY
					sh.academic_terms_id
				DESC
				LIMIT 1
				";
		
		$query = $this->db->query($sql);
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
		
		if ( is_object($query) && $query->num_rows() > 0){
			$role = 'student';
			foreach ($query->result() as $row){
				if (!is_null($row->role))
					$roles[] = $row->role;
			}
			$ret = array (
					'role'=>$role,
					'fname'=>$row->fname,
					'idno' => $row->idno,
    				'lname' => $row->lname,
    				'fname' => $row->fname,
    				'mname' => $row->mname,
    				'dbirth' => $row->dbirth,
					'yr_level'=>$row->yr_level,
					'student_histories_id'=>$row->student_histories_id,
					'prospectus_id'=>$row->prospectus_id,
					'abbreviation'=>$row->abbreviation,
    			);
			return $ret;
		} else 
			return FALSE;
	}
	
	/**
	 * Method: update_address
	 * 
	 * Insert an address for a student. If duplicate student id is found will update the student's 
	 * address data.
	 * 
	 * @param associative array  $data
	 * @return boolean TRUE when the query is successful, else FALSE
	 */
	public function update_address ($data){
		$keys = array(
				'students_idno', 
				'birth_address',	
				'birth_barangays_id',
				'birth_towns_id',
				'birth_provinces_id',
				'birth_countries_id',
				'city_address',
				'city_barangays_id',
				'city_towns_id',
				'city_provinces_id',
				'city_countries_id',
				'home_address',
				'home_barangays_id',
				'home_towns_id',
				'home_provinces_id',
				'home_countries_id',
			);
		
		$sql = "
			INSERT 
				into
			addresses (
			";
		$values = "
			VALUE (";
		$updates = "
			ON DUPLICATE KEY UPDATE
				";
		
		foreach ($data as $key => $val) {
			if (in_array($key, $keys)){
				$sql .= "`{$key}`,\n";
				$values .= ($val ? "{$this->db->escape($val)},\n" : "NULL,\n");
				if ($key != 'students_idno')
					$updates .= ($val ? "`{$key}`={$this->db->escape($val)},\n" : "`{$key}`=NULL,\n");  
			}
		}
		$sql = rtrim($sql, ",\n") . ")\n";
		$values = rtrim($values, ",\n") . ")\n";
		$updates = rtrim($updates, ",\n");

		$sql = $sql . $values . $updates;
		
		if ($query = $this->db->query($sql)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function student_address ($student_id){
		$sql = "SELECT
					*
				FROM
					addresses
				WHERE
					students_idno={$this->db->escape($student_id)}
				LIMIT
					1";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->row(); else
			return FALSE;
	}
	
	public function search_by_program ($program_id, $year_level, $all=FALSE, $page=1, $count=15){
		$start = ($page - 1) * $count;
		$sql = "
			SELECT
			DISTINCT
				idno,
				CAP_FIRST(CONCAT(lname, ', ', fname)) as fullname,
				gender,
				fname,
				lname,
				mname,
				ap.abbreviation as abbr,
				sh.year_level
			FROM
				students s
			LEFT JOIN
				student_histories sh
			ON
				sh.students_idno=s.idno
			LEFT JOIN
				prospectus p
			ON
				p.id=sh.prospectus_id
			LEFT JOIN
				academic_programs ap
			ON
				ap.id=p.academic_programs_id
			LEFT JOIN
				academic_terms at
			ON
				at.id=sh.academic_terms_id
			JOIN enrollments e on e.student_history_id = sh.id				
			WHERE
				s.is_active='Y' 
			AND			
				ap.id={$this->db->escape($program_id)}
			AND
				sh.year_level={$this->db->escape($year_level)}
			AND
				at.status='current'
			ORDER BY
				lname ASC, fname ASC
			";
		
		if (! $all){
			$sql .= "LIMIT {$start}, {$count}\n";
		}
		//print_r($sql);die();
		$query = $this->db->query($sql);
		
		if ($query && count($query) > 0){
			return $query->result();
		} else {
			return FALSE;
		}
	}
	
	function last_id_number (){
		$sql = "SELECT
						idno
					FROM
						students
					ORDER BY
						idno
					DESC
					LIMIT 1";
		$query = $this->db->query($sql);
		if($query && $query->num_rows > 0){
			return $query->row()->idno;
		} else {
			return FALSE;
		}
	}
	
	public function my_roles ($userid){
		$sql = "
			SELECT
			DISTINCT
				r.role
			FROM
				roles r
			WHERE
				r.students_idno={$this->db->escape($userid)}
			";
		$query = $this->db->query($sql);
		
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
		if ( is_object($query)){
			return $query->result();
	}
	}
	
	public function delete_roles ($userid, $roles) {
		$sql = "
			DELETE from
				roles
			WHERE
				students_idno={$this->db->escape($userid)}
			AND
			";
	
		if (is_array($roles)){
			$which_roles = " role IN (";
			foreach($roles as $role){
				$which_roles .= "{$this->db->escape($role)}, ";
			}
			$which_roles = rtrim($which_roles, ", ") . ")";
		} else {
			$sql .= " role={$roles}
				";
		}
			return $this->db->query($sql . $which_roles);
	}
	
	public function add_roles ($userid, $roles, $colleges_id=0, $offices_id=1) {
		$sql = "
			INSERT into
				roles (`students_idno`, `colleges_id`, `offices_id`, `role`)
			VALUES
			";
	
		$values = "";
		
		foreach ($roles as $role){
			if (in_array($role, $this->config->item('roles_with_colleges'))){
				//this role has a college...
				$values .= "({$this->db->escape($userid)}, {$this->db->escape($colleges_id)}, NULL, {$this->db->escape($role)}),\n";
			} else {
				if (in_array($role, $this->config->item('roles_with_offices'))){
					//this role has an office
					$values .= "({$this->db->escape($userid)}, NULL, {$this->db->escape($offices_id)}, {$this->db->escape($role)}),\n";
				} else {
					$values .= "({$this->db->escape($userid)}, NULL, NULL, {$this->db->escape($role)}),\n";
				}
			}
		}
		$sql .= rtrim($values, ",\n");
		return $this->db->query($sql);
	}
	
	public function update_roles ($userid, $roles, $colleges_id){
		$sql = "
			UPDATE
				roles
			SET
				colleges_id={$this->db->escape($colleges_id)}
			WHERE
				role IN (";
		
		foreach ($roles as $role){
			$sql .= "{$this->db->escape($role)},";
		}
		$sql = rtrim($sql, ",") . ")";
		$sql .= " AND students_idno={$this->db->escape($userid)}";
		
		return $this->db->query($sql);
	}
	
	public function get_idno($lastname,$firstname,$middlename){
		$this->db->select('idno');
		$this->db->where("lname like '$lastname%' and fname like '$firstname%' and mname like '$middlename%'");
		if ($query = $this->db->get('students')){
			foreach ($query->result() as $row)
			return $row->idno;			
		}else{
			return FALSE;
		}
	}

	public function last_logins($idno){
		$sql = "
			select '' as role, u.ip_address, u.time_in 
			from user_logs u
			left join students s on s.idno = u.user_id
			where s.idno ='{$idno}'
			order by time_in desc
			limit 5
		";
	
		$query = $this->db->query($sql);
		if ($query)
			return $query->result();
		else
			return FALSE;
	
	}
	
	// ADDED: 2/1/16 by genes
	function get_my_bed_status($history_id) {
		
		$q = "SELECT 
					status
				FROM 
					basic_ed_histories 
				WHERE 
					id = $history_id ";
		//die($q);				
		$query = $this->db->query($q);
			
		if($query->num_rows() > 0){
			$result = $query->row();
		} 
			
		return $result;
						
	}
	
}

?>