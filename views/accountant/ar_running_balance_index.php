<?php if (isset($message) AND !empty($message)): ?>
<div class="alert alert-success alert-error">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message ?>
</div>
<?php endif; ?>

<ul class="nav nav-tabs" id="ar_balance">
  <li class="active"><a href="#basic_ed" data-toggle="tab">Basic ED</a></li>
  <li><a href="#college" data-toggle="tab">College</a></li>
</ul>
<div class="loading"></div>
<div class="tab-content">
  <div class="tab-pane active" id="basic_ed">
  			<div class="">
			<h2 class="lead">A/R Running Balance - Basic ED</h2>
			
			<label>Show running balance:</label>
			<form class="form-horizontal" method="post" id="ar_basic_ed" action="ar_balance_basic">
			<input type="hidden" style="display:none" name="nonce" value="<?php echo $this->common->nonce() ?>">
			<fieldset>
			  <div class="control-group error" id="basic_ed-date-error">
			  	<div class="controls"></div>
			  </div>

			  
			  	<div class="control-group formSep">
					<div class="control-label">Academic Year</div>
					<div class="controls">
						<select name="academic_year_id" id="academic_year_id">
								<option value="0"> Select Academic Year</option>
							<?php
								foreach($academic_years AS $key => $academic_year) {
							?>
								<option value= "<?php echo $key; ?>" >
									<?php  echo $academic_year ; ?> 
								</option>	
							<?php 
								}
							?>					
						</select>
					</div>
				</div>
			  
			  
			  
			  <div class="control-group input-append">
			    <label class="control-label" for="start_date">From:</label>
			    <div class="controls date" id="be-start_date" data-date-format="yyyy-mm-dd">
			      <input type="text" id="basic_ed-start_date" name="start_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-01')?>">
				  <span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			  </div>
			  <div class="control-group input-append">
			    <label class="control-label" for="end_date">To:</label>
			    <div class="controls date" id="be-end_date" data-date-format="yyyy-mm-dd">
			      <input type="text" id="basic_ed-end_date" name="end_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d')?>">
				 <span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			  </div>

			  
			  <div class="form-actions">
				<button type="submit" class="btn btn-success" id="submitbutton">Show</button>
			  </div>
			</fieldset>
			</form>
			</div>
			
			<script>
			$(document).ready(function() {
				$('#be-start_date').datepicker();
				$('#be-end_date').datepicker();
				
				
				$.validator.addMethod("enddate1", function(value, element) {
					var startdatevalue = $('#basic_ed-start_date').val();
					return Date.parse(startdatevalue) <= Date.parse(value);
				},"End Date should be greater than or equal to Start Date.");
				$('#ar_basic_ed').validate({
					errorClass:'help-inline',
					onkeyup: true,
					rules:{
						end_date:{enddate1:1},
					},
					highlight:function(element,errorClass){
						$(element).addClass('error');
					},
					unhighlight:function(element,errorClass){
						$(element).removeClass('error');
					},
					errorPlacement: function(error, element) {
					     error.appendTo(element.closest('form').children().children('#basic_ed-date-error').children('.controls'));
					},
					errorElement:"span",
					submitHandler: function(form) {
						$('.loading').delay(800).fadeIn(400);
						form.submit();
					}
				});

			});
			</script>
  </div>
  <div class="tab-pane" id="college">
		  <div class="">
			<h2 class="lead">A/R Running Balance - College</h2>
			
			<label>Show running balance:</label>
			<form class="form-horizontal" method="post" id="ar_college" action="ar_balance_college">
			<input type="hidden" style="display:none" name="nonce" value="<?php echo $this->common->nonce() ?>">
			<fieldset>
			  <div class="control-group error" id="college-date-error">
			  	<div class="controls"></div>
			  </div>
			  <div class="control-group input-append">
			    <label class="control-label" for="start_date">From:</label>
			    <div class="controls date" id="c-start_date" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-01') ?>">
			      <input type="text" id="college-start_date" name="start_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-01')?>" readonly="">
				  <span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			  </div>
			  <div class="control-group input-append">
			    <label class="control-label" for="end_date">To:</label>
			    <div class="controls date" id="c-end_date" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-d') ?>">
			      <input type="text" id="college-end_date" name="end_date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d')?>">
				 <span class="add-on"><i class="icon-calendar"></i></span>
			    </div>
			  </div>
			  <style>
			  div.btn-group[data-toggle=buttons-radio] input[type=radio] {
				  display:    block;
				  position:   absolute;
				  top:        0;
				  left:       0;
				  width:      107%;
				  height:     115%;
				  opacity:    0;
				  margin:	  -2px;
				}
			  </style>
			  <div class="control-group">
				  <div class="controls">
				  	<div class="btn-group" data-toggle="buttons-radio">
						<select name="show_by" id="show_by">
						<option value="all">All</option>
						<option value="group">By Group</option>
						</select>
					</div>
				  </div>
			  </div>
				<div class="form-actions">
				<button type="submit" class="btn btn-success">Show</button>
				</div>
			</fieldset>
			</form>
			</div>
			
			<script>
			$(document).ready(function() {
				$('#c-start_date').datepicker();
				$('#c-end_date').datepicker();
				$.validator.addMethod("enddate2", function(value, element) {
					var startdatevalue = $('#college-start_date').val();
					//console.log(Date.parse(startdatevalue) < Date.parse(value));
					return Date.parse(startdatevalue) <= Date.parse(value);
				},"End Date should be greater than or equal to Start Date.");
				$('#ar_college').validate({
					errorClass:'help-inline',
					onkeyup: false,
					rules:{
						end_date:{enddate2:1},
					},
					highlight:function(element,errorClass){
						$(element).addClass('error');
					},
					unhighlight:function(element,errorClass){
						$(element).removeClass('error');
					},
					errorPlacement: function(error, element) {
					     error.appendTo(element.closest('form').children().children('#college-date-error').children('.controls'));
					},
					errorElement:"span",
					submitHandler: function(form) {
						$('.loading').delay(800).fadeIn(400);
						form.submit();
					}
				});
			});
			  
			</script>
  </div>
</div>
 