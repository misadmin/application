<div style="margin-top:20px;">
<h2 class="heading">Fees Schedule By Academic Programs</h2>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; 
	border-color: #D8D8D8; height: 35px; margin-bottom:10px; padding:2px;">
<table style="width:100%; padding:0px; height:auto;">
<tr>
<td style="text-align:left; vertical-align:top; font-weight:bold;">
<?php 
	if ($program_items) {
?>
<a href="download" class="btn btn-link" id="download_fees_schedule"><i class="icon-download-alt"></i> Download as CSV</a>
<?php 
	}
?>
</td>
<td style="text-align:right; vertical-align:top;">	
<form id="change_data" method="POST" >
	<?php $this->common->hidden_input_nonce(FALSE); ?>
					<select name="academic_terms_id" class="select_change" id="academic_terms_id">
					<?php 
						$grand_total = array('gtotal1'=>0,'gtotal2'=>0,'gtotal3'=>0,'gtotal4'=>0,'gtotal5'=>0);
						foreach($terms AS $term) {
					?>
							<option value="<?php print($term->id); ?>" <?php if ($term->id == $selected_term) { print("selected"); } ?>>
								<?php print($term->term.' '.$term->sy); ?> </option>
					<?php 	
						}
					?>
					</select>
					
					<select name="academic_programs_id" class="select_change" id="academic_programs_id">
					<?php 
						foreach($acad_programs AS $program) {
					?>
							<option value="<?php print($program->id); ?>" <?php if ($program->id == $selected_program) { print("selected"); } ?> 
								acad_program_groups_id="<?php print($program->acad_program_groups_id); ?>" >
								<?php print($program->abbreviation); ?> </option>
					<?php 	
						}
					?>
					</select>
</form>	
</td>
</tr>
</table>				
</div>
<h3><?php print("Academic Group: ".$my_program['group']); ?></h3>
<h3><?php print($my_program['description']); ?></h3>
<h4><?php print($my_program['sy_semester']); ?></h4><br>
<?php 
	if ($program_items) {
?>
<div>
	<table class="table table-bordered table-striped table-hover" >
		<thead>
			<tr style="background-color: #EAEAEA; font-weight: bold;">
				<td style="width:40%; text-align:center;">
					ITEM
				</td>
				<td style="width:12%; text-align:center;">
					1st Year
				</td>
				<td style="width:12%; text-align:center;">
					2nd Year
				</td>	
				<td style="width:12%; text-align:center;">
					3rd Year
				</td>
				<td style="width:12%; text-align:center;">
					4th Year
				</td>	
				<td style="width:12%; text-align:center;">
					5th Year
				</td>
			</tr>
		</thead>
		<?php
			$fees_group="";
			$flag = 1;
			$total_y1 = 0;
			$total_y2 = 0;
			$total_y3 = 0;
			$total_y4 = 0;
			$total_y5 = 0;
			foreach($program_items AS $item) {
				
				if ($fees_group != $item->fees_group) {
					$prev_group = $fees_group;
					$fees_group = $item->fees_group;
					
					if($flag > 1){ 
 						if($prev_group == "Miscellaneous Fees" 
							OR $prev_group == "Student Support Services" 
							OR $prev_group == "Learning Resources Fee"
							OR $prev_group == "Other School Fees"
							OR $prev_group == "Additional Other Fees"){
					?>
						
						<tr>	
							<td style='font-weight:bold; text-align:right;'>
								<?php 
									print("TOTAL ".$prev_group.":");
								?>
							</td>
						<td style="text-align: right; font-weight: bold; font-size:15px">
							<?php 
								print(number_format($total_y1,2));
							?>
						</td>
							<td style="text-align: right; font-weight: bold; font-size:15px">
							<?php 
								print(number_format($total_y2,2));
							?>
						</td>
							<td style="text-align: right; font-weight: bold; font-size:15px">
							<?php 
								print(number_format($total_y3,2));
							?>
						</td>
							<td style="text-align: right; font-weight: bold; font-size:15px">
							<?php 
							print(number_format($total_y4,2));
							?>
						</td>
							<td style="text-align: right; font-weight: bold; font-size:15px">
							<?php 
								print(number_format($total_y5,2));
							?>
						</td>
					</tr>
				<?php } 
				 }
				 print("<tr>");
				 print("<td style='font-weight: bold;' colspan='6'>".$fees_group."</td>");
				 print("</tr>");
				 $total_y1 = 0;
				 $total_y2 = 0;
				 $total_y3 = 0;
				 $total_y4 = 0;
				 $total_y5 = 0;
			}
			$flag += 1 ;
		?>
			
		<tr>	
			<td style="padding-left: 30px;">
				<?php 
					print($item->description);
				?>
			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y1,2));
					if ($can_update AND $item->y1) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee1."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee1\" >
							<i class=\"icon-trash\"></i></a>");
					}	
					$total_y1 = $total_y1 + $item->y1;
					$grand_total['gtotal1'] += $item->y1; 
				?>
				<!-- Modal to edit Fee Schedule 1st year-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee1); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee1); ?>">
				   				<input type="hidden" name="step" value="7" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee1);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>1st Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y1);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee1); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule 1st year-->		

<script>
$('#update_fee_<?php print($item->fee1); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee1); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y2,2));
					if ($can_update AND $item->y2) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee2."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee2\" >
							<i class=\"icon-trash\"></i></a>");
					}
					$total_y2 = $total_y2 + $item->y2;
					$grand_total['gtotal2'] += $item->y2; 
				?>
				<!-- Modal to edit Fee Schedule 2nd year-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee2); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee2); ?>">
				   				<input type="hidden" name="step" value="7" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee2);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>2nd Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y2);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee2); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule 2nd year-->		

<script>
$('#update_fee_<?php print($item->fee2); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee2); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y3,2));
					if ($can_update AND $item->y3) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee3."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee3\" >
							<i class=\"icon-trash\"></i></a>");
					}
					$total_y3 = $total_y3 + $item->y3;
					$grand_total['gtotal3'] += $item->y3; 
				?>
				<!-- Modal to edit Fee Schedule 3rd year-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee3); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee3); ?>">
				   				<input type="hidden" name="step" value="7" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee3);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>3rd Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y3);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee3); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule 3rd year-->		

<script>
$('#update_fee_<?php print($item->fee3); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee3); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y4,2));
					if ($can_update AND $item->y4) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee4."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee4\" >
							<i class=\"icon-trash\"></i></a>");
					}
					$total_y4 = $total_y4 + $item->y4;
					$grand_total['gtotal4'] += $item->y4; 
				?>
				<!-- Modal to edit Fee Schedule 4th year-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee4); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee4); ?>">
				   				<input type="hidden" name="step" value="7" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee4);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>4th Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y4);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee4); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule 4th year-->		

<script>
$('#update_fee_<?php print($item->fee4); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee4); ?>').submit();
	}		
});
</script>


			</td>
			<td style="text-align: right;">
				<?php 
					print(number_format($item->y5,2));
					if ($can_update AND $item->y5) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_fee_".$item->fee5."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_fee\" href=\"$item->fee5\" >
							<i class=\"icon-trash\"></i></a>");
					}
					$total_y5 = $total_y5 + $item->y5;
					$grand_total['gtotal5'] += $item->y5; 
				?>
				<!-- Modal to edit Fee Schedule 5th year-->
					<div class="modal hide fade" id="modal_edit_fee_<?php print($item->fee5); ?>" >
				 		<form method="post" id="update_fee_form_<?php print($item->fee5); ?>">
				   				<input type="hidden" name="step" value="7" />
				   				 <input type="hidden" name="fees_schedule_id" value= "<?php print($item->fee5);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Fees Schedule</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($item->description);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>5th Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($item->y5);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_fee_<?php print($item->fee5); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Fee Schedule 5th year-->		

<script>
$('#update_fee_<?php print($item->fee5); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Fee Schedule rate?");

	if (confirmed){

		$('#update_fee_form_<?php print($item->fee5); ?>').submit();
	}		
});
</script>


			</td>
		</tr>
		
	<?php 	
		
	}
	?>
	<tr>	
		<td style="font-weight: bold; text-align:right;">
			<?php 
				print("TOTAL ".$fees_group.":");
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y1,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y2,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y3,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y4,2));
			?>
		</td>
		<td style="text-align: right; font-weight: bold; font-size:15px">
			<?php 
				print(number_format($total_y5,2));
			?>
		</td>
		</tr>
		<tr style="font-weight:bold; font-size:15px;">
			<td style="text-align:right; background-color:#EAEAEA;">GRAND TOTAL:</td>
			<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal1'],2)); ?></td>
			<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal2'],2)); ?></td>
			<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal3'],2)); ?></td>
			<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal4'],2)); ?></td>
			<td style="text-align:right; background-color:#EAEAEA;"><?php print(number_format($grand_total['gtotal5'],2)); ?></td>
		</tr>
		
	<?php 
		if ($other_tuition_fees) {
	?>
		<tr>
		<td style="font-weight:bold; padding-top:30px;" colspan="6">Other Tuition Fees</td>
		</tr>
	<?php 
			foreach ($other_tuition_fees AS $other_fee) {
	?>	

		<tr>	
			<td style="padding-left: 30px;">
				<?php 
					print($other_fee->course_code);
				?>
			</td>
			<td style="text-align: right;">
				<?php 
					print($other_fee->y1);
					if ($can_update AND $other_fee->y1) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_otherfee_".$other_fee->fee1."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_tuition_others\" href=\"$other_fee->fee1\" >
							<i class=\"icon-trash\"></i></a>");
					}	
				?>
				<!-- Modal to edit Other Tuition Fees 1st year-->
					<div class="modal hide fade" id="modal_edit_otherfee_<?php print($other_fee->fee1); ?>" >
				 		<form method="post" id="update_otherfee_form_<?php print($other_fee->fee1); ?>">
				   				<input type="hidden" name="step" value="9" />
				   				 <input type="hidden" name="tuition_others_id" value= "<?php print($other_fee->fee1);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Other Tuition Fees</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($other_fee->course_code);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>1st Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($other_fee->y1);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_otherfee_<?php print($other_fee->fee1); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Other Tuition Fees 1st year-->		

<script>
$('#update_otherfee_<?php print($other_fee->fee1); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Other Tuition rate?");

	if (confirmed){

		$('#update_otherfee_form_<?php print($other_fee->fee1); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print($other_fee->y2);
					if ($can_update AND $other_fee->y2) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_otherfee_".$other_fee->fee2."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_tuition_others\" href=\"$other_fee->fee2\" >
							<i class=\"icon-trash\"></i></a>");
					}
				?>
				<!-- Modal to edit Other Tuition Fees 2nd year-->
					<div class="modal hide fade" id="modal_edit_otherfee_<?php print($other_fee->fee2); ?>" >
				 		<form method="post" id="update_otherfee_form_<?php print($other_fee->fee2); ?>">
				   				<input type="hidden" name="step" value="9" />
				   				 <input type="hidden" name="tuition_others_id" value= "<?php print($other_fee->fee2);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Other Tuition Fees</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($other_fee->course_code);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>2nd Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($other_fee->y2);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_otherfee_<?php print($other_fee->fee2); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Other Tuition Fees 2nd year-->		

<script>
$('#update_otherfee_<?php print($other_fee->fee2); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Other Tuition rate?");

	if (confirmed){

		$('#update_otherfee_form_<?php print($other_fee->fee2); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print($other_fee->y3);
					if ($can_update AND $other_fee->y3) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_otherfee_".$other_fee->fee3."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_tuition_others\" href=\"$other_fee->fee3\" >
							<i class=\"icon-trash\"></i></a>");
					}
				?>
				<!-- Modal to edit Other Tuition Fees 3rd year-->
					<div class="modal hide fade" id="modal_edit_otherfee_<?php print($other_fee->fee3); ?>" >
				 		<form method="post" id="update_otherfee_form_<?php print($other_fee->fee3); ?>">
				   				<input type="hidden" name="step" value="9" />
				   				 <input type="hidden" name="tuition_others_id" value= "<?php print($other_fee->fee3);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Other Tuition Fees</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($other_fee->course_code);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>3rd Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($other_fee->y3);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_otherfee_<?php print($other_fee->fee3); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Other Tuition Fees 3rd year-->		

<script>
$('#update_otherfee_<?php print($other_fee->fee3); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Other Tuition rate?");

	if (confirmed){

		$('#update_otherfee_form_<?php print($other_fee->fee3); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print($other_fee->y4);
					if ($can_update AND $other_fee->y4) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_otherfee_".$other_fee->fee4."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_tuition_others\" href=\"$other_fee->fee4\" >
							<i class=\"icon-trash\"></i></a>");
					}
				?>
				<!-- Modal to edit Other Tuition Fees 4th year-->
					<div class="modal hide fade" id="modal_edit_otherfee_<?php print($other_fee->fee4); ?>" >
				 		<form method="post" id="update_otherfee_form_<?php print($other_fee->fee4); ?>">
				   				<input type="hidden" name="step" value="9" />
				   				 <input type="hidden" name="tuition_others_id" value= "<?php print($other_fee->fee4);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Other Tuition Fees</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($other_fee->course_code);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>4th Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($other_fee->y4);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_otherfee_<?php print($other_fee->fee4); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Other Tuition Fees 4th year-->		

<script>
$('#update_otherfee_<?php print($other_fee->fee4); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Other Tuition rate?");

	if (confirmed){

		$('#update_otherfee_form_<?php print($other_fee->fee4); ?>').submit();
	}		
});
</script>

			</td>
			<td style="text-align: right;">
				<?php 
					print($other_fee->y5);
					if ($can_update AND $other_fee->y5) {
			 			print(" <a href=\"#\" data-toggle=\"modal\" data-target=#modal_edit_otherfee_".$other_fee->fee5."  
								data-project-id=\"\"><i class=\"icon-edit\"></i></a>");
						print(" <a class=\"delete_tuition_others\" href=\"$other_fee->fee5\" >
							<i class=\"icon-trash\"></i></a>");
					}
				?>
				<!-- Modal to edit Other Tuition Fees 5th year-->
					<div class="modal hide fade" id="modal_edit_otherfee_<?php print($other_fee->fee5); ?>" >
				 		<form method="post" id="update_otherfee_form_<?php print($other_fee->fee5); ?>">
				   				<input type="hidden" name="step" value="9" />
				   				 <input type="hidden" name="tuition_others_id" value= "<?php print($other_fee->fee5);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Other Tuition Fees</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($other_fee->course_code);
										?>
										</td>		
									</tr>
									<tr>
										<td>Year</td>
										<td>:</td>
										<td>5th Year</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="rate" value="<?php 
												print($other_fee->y5);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_otherfee_<?php print($other_fee->fee5); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>	
					</div>				
				<!--  End of Modal to edit Other Tuition Fees 5th year-->		

<script>
$('#update_otherfee_<?php print($other_fee->fee5); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Other Tuition rate?");

	if (confirmed){

		$('#update_otherfee_form_<?php print($other_fee->fee5); ?>').submit();
	}		
});
</script>

			</td>
		</tr>


	<?php
			} 
		}
	?>
		</table>
</div>
	
	<?php 
		}
		if ($lab_items) {
	?>
<div>
	<div style="width:30%; float:left; overflow:auto; height:280px;">
	<table class="table table-bordered table-striped table-hover" >
		<thead>
			<tr style="background-color: #EAEAEA; font-weight: bold;">
				<td style="width:60%; text-align:center;">
					LABORATORY FEES
				</td>
				<td style="width:40%; text-align:center;">
					RATE
				</td>
				<?php 
					if ($can_update) {
				?>
				<td style="text-align:center;">
					Edit
				</td>
				<td style="text-align:center;">
					Del.
				</td>				
				<?php 
					}
				?>
			</tr>
		</thead>
		
		<?php 						
			foreach ($lab_items AS $lab_item) {
				print("<tr>");
				print("<td style='padding-left: 30px;'>".$lab_item->course_code."</td>");
				print("<td style='text-align: right;'>".$lab_item->rate."</td>");
				if ($can_update) {					
		?>		
			 	<td style="text-align: center;">
			 		<a href="#" data-toggle="modal" 
			 			data-target="#modal_edit_labfee_<?php print($lab_item->laboratory_fees_id); ?>"  
							data-project-id=""><i class="icon-edit"></i></a>
		
			 		<!-- Modal to edit Lab Fees -->
					<div class="modal hide fade" id="modal_edit_labfee_<?php print($lab_item->laboratory_fees_id); ?>" >
				 		<form method="post" id="update_labfee_form_<?php print($lab_item->laboratory_fees_id); ?>">
				   				<input type="hidden" name="step" value="8" />
				   				 <input type="hidden" name="laboratory_fees_id" value= "<?php print($lab_item->laboratory_fees_id);?>"/>
				   				 <input type="hidden" name="academic_programs_id" value= "<?php print($selected_program);?>"/>
				   				 <input type="hidden" name="academic_terms_id" value= "<?php print($selected_term);?>"/>
				   				<?php $this->common->hidden_input_nonce(FALSE); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
							    <h3>Edit Laboratory Fee</h3>
							</div>
							<div class="modal-body">
								<div style="text-align:left; margin-top:0px;">
									<h3><?php print($my_program['group']); ?></h3>
									<h3><?php print($my_program['description']); ?></h3>
									<h4><?php print($my_program['sy_semester']); ?></h4><br>
								<table class="table table-bordered table-striped table-condensed"
										style="font-weight:bold; width:80%;">
									<tr>
										<td style="width:23%;">Item</td>
										<td style="width:2%;">:</td>
										<td style="width:75%;">
										<?php
											print($lab_item->course_code);
										?>
										</td>		
									</tr>
									<tr>
										<td>Rate</td>
										<td>:</td>
										<td><input type="text" name="lab_rate" value="<?php 
												print($lab_item->rate);
											?>" style="width:50px; text-align:right;">
										</td>
									</tr>
								</table>
								</div>
							</div>
							<div class="modal-footer">
				    			<input type="submit" class="btn btn-warning" id="update_labfee_<?php print($lab_item->laboratory_fees_id); ?>" value="EDIT FEE!">
								<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							</div>
						</form>
					</div>				
				<!--  End of Modal to edit Fee Schedule -->		

<script>
$('#update_labfee_<?php print($lab_item->laboratory_fees_id); ?>').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to update Laboratory Fee rate?");

	if (confirmed){

		$('#update_labfee_form_<?php print($lab_item->laboratory_fees_id); ?>').submit();
	}		
});
</script>

		</td>	 		
			<?php  		
			 		print("<td style='text-align: center;'>
						<a class='delete_lab' href='$lab_item->laboratory_fees_id'>
						<i class='icon-trash'></i></a></td>");
				}
				print("</tr>");			
			}
		?>
		</table>
	</div>		
	<?php 
		}
		
		if ($affiliated_items) {
	?>
	<div style="width:40%; float: left; margin-left:80px; overflow:auto; height:280px;">
		<table class="table table-bordered table-striped table-hover" >
		<thead>
			<tr style="background-color: #EAEAEA; font-weight: bold;">
				<td style="width:30%; text-align:center;">
					COURSE CODE
				</td>
			<td style="width:30%; text-align:center;">
					AFFILIATION FEES
				</td>
				<td style="width:20%; text-align:center;">
					RATE
				</td>
				<td style="width:10%; text-align:center;">
					Edit
				</td>
				<td style="width:10%; text-align:center;">
					Del.
				</td>				
			</tr>
		</thead>
		
		<?php 						
			foreach ($affiliated_items AS $a_item) {
		?>
			<tr>
				<td style="padding-left: 30px;"><?php print($a_item->course_code); ?></td>
				<td style="padding-left: 30px;"><?php print($a_item->description); ?></td>
				<td style="text-align: right;"><?php print($a_item->rate); ?></td>
				<td style="text-align: center;"><i class="icon-edit"></i></td>
				<td style="text-align: center;"><i class="icon-trash"></i></td>
			</tr>
		<?php 
			}
		?>		
		</table>					
	</div>
	<?php 
		}
	?>
</div>


<script>
	$(document).ready(function(){
		$('.select_change').bind('change', function(){
			show_loading_msg();
            var element = $("option:selected", "#academic_programs_id");
			var acad_program_groups_id = element.attr('acad_program_groups_id');

			$('<input>').attr({
				type: 'hidden',
				name: 'acad_program_groups_id',
				value: acad_program_groups_id,
			}).appendTo('#change_data');

			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 'change_pulldown',
			}).appendTo('#change_data');

			$('#change_data').submit();
		});
	});
</script>


<script>
$('.delete_fee').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to delete Scheduled Fee?");

	if (confirmed){
		var fees_schedule_id = $(this).attr('href');

		$('<input>').attr({
			type: 'hidden',
			name: 'step',
			value: 'delete_schedule_fee',
		}).appendTo('#change_data');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'fees_schedule_id',
		    value: fees_schedule_id,
		}).appendTo('#change_data');

		$('#change_data').submit();
	}		
});
</script>

<script>
$('.delete_lab').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to delete Laboratory Fee?");

	if (confirmed){
		var laboratory_fees_id = $(this).attr('href');

		$('<input>').attr({
			type: 'hidden',
			name: 'step',
			value: 'delete_lab_fee',
		}).appendTo('#change_data');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'laboratory_fees_id',
		    value: laboratory_fees_id,
		}).appendTo('#change_data');

		$('#change_data').submit();
	}		
});
</script>

<script>
$('.delete_tuition_others').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to delete Other Tuition Fee?");

	if (confirmed){
		var tuition_others_id = $(this).attr('href');

		$('<input>').attr({
			type: 'hidden',
			name: 'step',
			value: 'delete_tuition_others',
		}).appendTo('#change_data');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'tuition_others_id',
		    value: tuition_others_id,
		}).appendTo('#change_data');

		$('#change_data').submit();
	}		
});
</script>

<form method="post" id="download_list_form" action="<?php echo site_url("{$role}/download_fees_schedule_to_csv"); ?>" />
			<input type="hidden" name="academic_terms_id" value="<?php print($selected_term); ?>" />
			<input type="hidden" name="acad_programs_id" value="<?php print($selected_program); ?>" />
			<input type="hidden" name="acad_groups" value="<?php print($my_program['group']); ?>" />
			<input type="hidden" name="description" value="<?php print($my_program['description']); ?>" />
			<input type="hidden" name="sy_semester" value="<?php print($my_program['sy_semester']); ?>" />
</form>

<script>
	$('#download_fees_schedule').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>

