<?php echo (isset($clearance) ? $clearance : '')?>
<?php echo (isset($midterm_invoice) ? $midterm_invoice : ''); ?>
<?php echo (isset($prefinal_invoice) ? $prefinal_invoice : ''); ?>
<?php 
	if(!isset($print_count))
		$print_count = 0;
	if(!isset($print_count_prelim))
		$print_count_prelim = 0;
	if(!isset($print_count_midterm))
		$print_count_midterm = 0;
	if(!isset($print_count_prefinal))
		$print_count_prefinal = 0;
	if(!isset($print_count_final))
		$print_count_final = 0;
	if(!isset($print_count_clearance))
		$print_count_clearance = 0;

if(in_array($this->session->userdata('role'), $this->config->item('roles_allowed_to_print_assessment'))):

$name = $familyname . ", " . $firstname . " " . substr($middlename, 0, 1) . ".";
$course_year = $level . " " . $course;


$table_size = array(
		'width'=>60,
		'height'=>count($tuition_fees_content) < 19 ? 19 : count($tuition_fees_content)
);
$length = array('fee_type'=>12, 'subject'=>20, 'units'=>6, 'hours'=>8, 'amount'=>12);
$numbers = array(0=>'units', 1=>'hours', 2=>'amount');

$tuition_fees_header = array(
		'fee_type'=>'Fee Type',
		'subject'=>'Subject',
		'units'=>'Units',
		'hours'=>'Hrs',
		'amount'=>'Amount'
);

$tuition_fees_content[] = array(
		'subject'=>'',
		'units'=>'____',
		'hours'=>'_____',
		'amount'=>'_________'
);

$tuition_fees_content[] = array(
		'subject'=>'Sub-total:',
		'units'=>0,
		'hours'=>0,
		'amount'=>0
);

$total_assessment = 0;

$tuition_fees = array();
$tuition_fees[] = '+' . str_pad('', $table_size['width'], '-') . '+';
$temp_head = '| ';
foreach($tuition_fees_header as $key=>$val){
	$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
	$temp_head .= str_pad($val, $length[$key], ' ', $str_pad);
}

$tuition_fees[] = str_pad($temp_head, $table_size['width']+1, ' ') . '|';
$tuition_fees[] = $tuition_fees[0];

$totals = array();

////log_message("INFO", print_r($tuition_fees_content, true)); // Toyet 6.6.2018
////log_message("INFO", print_r($miscellaneous_fees_content, true)); // Toyet 7.19.2018

foreach($tuition_fees_content as $row=>$value){
	$output_total = FALSE;
	$i = count($tuition_fees);
	$tuition_fees[$i] = '| ';
	foreach($value as $key=>$val){

		////log_message("INFO",print_r($val,true)); // Toyet 6.28.2018

		if($key == 'subject' && $val == $tuition_fees_content[count($tuition_fees_content)-1][$key]) $output_total = TRUE;
		if($key == 'subject' && $val == ''){
			if(count($tuition_fees) < $table_size['height']-2){
				$tuition_fees[$i] .= str_pad('', $table_size['width']-1, ' ') . '|';
				for($c = count($tuition_fees); $c < $table_size['height']-3; $c++){
					$tuition_fees[] = '|' . str_pad('', $table_size['width'], ' ') . '|';
				}
				$i = $c;
				$tuition_fees[$i] = '| ';
			}
		}
		if($key == 'fee_type'){
			$tuition_fees[$i] .= str_pad($val, $table_size['width']-2, ' ');
		} else {
			$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
			if($key == 'subject') {
				$tuition_fees[$i] .= str_pad('', $length['fee_type'], ' ');
			}
			if(in_array($key, $numbers)){
				if(is_numeric($val)){
					$totals[$key] = isset($totals[$key]) ? $totals[$key] : 0;
					$totals[$key] += $val;
					$val = number_format($val, in_array($key, array('units')) ? 1 : 2);
				}
				if($output_total) {
					$val = number_format($totals[$key], in_array($key, array('units')) ? 1 : 2);
					if($key == 'amount') $total_assessment = $totals[$key];
				}

			}
			$tuition_fees[$i] .= str_pad(substr($val, 0, $length[$key]), $length[$key], ' ', $str_pad);
		}
	}
	$tuition_fees[$i] = str_pad($tuition_fees[$i], $table_size['width']+1, ' ') . '|';
}

$tuition_fees[] = $this->print_lib->condensed();
$tuition_fees[count($tuition_fees)-1] = $tuition_fees[0];

//print_r($tuition_fees);

//////////////////////////////////////////////////////////// TABLE 2 ////////////////////////////////////

//$table_size = array('width'=>60, 'height'=>16);
$length = array('fee_type'=>42, 'description'=>24, 'amount'=>12);
//$numbers = array(0=>'units', 1=>'hours', 2=>'amount');

$miscellaneous_fees_header = array(
		'fee_type'=>'Fee Type',
		'description'=>'Description',
		'amount'=>'Amount'
);

$miscellaneous_fees_content[] = array(
		'description'=>'',
		'amount'=>'_________'
);

$miscellaneous_fees_content[] = array(
		'description'=>'Sub-total:',
		'amount'=>0
);

$miscellaneous_fees = array();
$miscellaneous_fees[] = '+' . str_pad('', $table_size['width'], '-') . '+';
$temp_head = '| ';
foreach($miscellaneous_fees_header as $key=>$val){
	$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
	$temp_head .= str_pad($val, $key == 'fee_type' ? $length[$key]-25 : $length[$key], ' ', $str_pad);
}

$miscellaneous_fees[] = str_pad($temp_head, $table_size['width']+1, ' ') . '|';
$miscellaneous_fees[] = $miscellaneous_fees[0];

$totals = array();
foreach($miscellaneous_fees_content as $row=>$value){
	$output_total = FALSE;
	$i = count($miscellaneous_fees);
	$miscellaneous_fees[$i] = '| ';
	foreach($value as $key=>$val){
		if($key == 'description' && $val == $miscellaneous_fees_content[count($miscellaneous_fees_content)-1][$key]){
			$output_total = TRUE;
		}
		$str_pad = (in_array($key, $numbers)) ? STR_PAD_LEFT : STR_PAD_RIGHT;
		if($key == 'description') {
			$miscellaneous_fees[$i] .= str_pad('', $length['fee_type']-24, ' ');
		}
		//if()
		if(in_array($key, $numbers)){
			if(is_numeric($val)){
				$totals[$key] = isset($totals[$key]) ? $totals[$key] : 0;
				$totals[$key] += $val;
				$val = number_format($val, in_array($key, array('units', 'hours')) ? 1 : 2);
			}
			if($output_total) {
				$val = number_format($totals[$key], in_array($key, array('units', 'hours')) ? 1 : 2);
				if($key == 'amount') $total_assessment += $totals[$key];
			}

		}
		$miscellaneous_fees[$i] .= str_pad(substr($val, 0, $length[$key]), $length[$key], ' ', $str_pad);
	}
	$miscellaneous_fees[$i] = str_pad($miscellaneous_fees[$i], $table_size['width']+1, ' ') . '|';
}
if(count($miscellaneous_fees) < count($tuition_fees)-3){
	for($c = count($miscellaneous_fees); $c < count($tuition_fees)-3; $c++){
		$miscellaneous_fees[] = '|' . str_pad('', $table_size['width'], ' ') . '|';
	}
}
$sub_total[1] = $totals['amount'];
$miscellaneous_fees[] = $miscellaneous_fees[0];
$miscellaneous_fees[] = $this->print_lib->no_condensed();
$miscellaneous_fees[count($miscellaneous_fees)-1] .= str_pad('TOTAL ASSESSMENT:', ($table_size['width']/2)-10, ' '); 
$miscellaneous_fees[count($miscellaneous_fees)-1] .= str_pad(number_format($total_assessment, 2), 12, ' ', STR_PAD_LEFT);
$miscellaneous_fees[count($miscellaneous_fees)-1] .= $this->print_lib->condensed();
$miscellaneous_fees[] = str_pad('', $table_size['width']+2, '-');

//print_r($miscellaneous_fees);

/////////////////////////////////////////////////////// TABLE 1 AND 2 READY FOR PRINTING //////////////////////

$print_content = "";
for($i = 0; $i < $table_size['height']; $i++){
	$print_content .= $tuition_fees[$i] . " " . $miscellaneous_fees[$i] . "\n";
}
$print_content = trim($print_content);

?>

<?php
//this should be placed outside the html tag or body to be accessible
//echo sprintf($this->config->item('jzebra_applet'), base_url()); ?>
<div id="assessment_to_print" style="display:none;"><?php 
echo $this->print_lib->init(); 
if($this->session->userdata('printed') == 'yes')
	echo $this->print_lib->set_page_length_in_lines(33); else {
	echo $this->print_lib->set_page_length_in_lines(34);
	$this->session->set_userdata('printed', 'yes');
}
echo "\n";
echo $this->print_lib->align_right($idnumber) . "\n";?>
<?php echo $this->print_lib->align_right($name) . "\n"; ?>
<?php echo $this->print_lib->align_right($course_year) ; ?>
<?php //echo print_lib::align_right($term) . "\n"; ?>
<?php //echo print_lib::align_right('Assessment Test Print') . "\n"; ?>
<?php echo $this->print_lib->condensed(); ?>

<?php echo $print_content; ?>

<?php echo $this->print_lib->no_condensed(); ?>
<?php echo "\n"; ?>
<?php 
if($term=="Summer"){
	echo str_pad("Due for Midterm:", ($table_size['width']*1.2)-12, ' ') . str_pad(number_format(ceil($payable_midterm), 2), 10, ' ', STR_PAD_LEFT) . " *\n";
} else {

	//edited   Toyet 11.23.2017
	if(!isset($bal_b4_assessment)){
		$bal_b4_assessment = 0;
	}

	$bal_b4_assessment = round($bal_b4_assessment,2);
	//log_message("INFO", "BALANCE B4 ASSESS: ".print_r($bal_b4_assessment,true)); // Toyet 6.28.2018
	//log_message("INFO", "PRELIM DUE: ".print_r($payable_prelim,true)); // Toyet 6.28.2018
	//log_message("INFO", "TRANS AFTER ASSESS: ".print_r($trans_after_assess,true)); // Toyet 6.28.2018
	//log_message("INFO", "PERIOD: ".print_r($period,true)); // Toyet 6.28.2018

	if($bal_b4_assessment<=0){
		if($payable_prelim<=0){
			$payable_prelim = 0;
		} else {
			$payable_prelim = $payable_prelim;
		}
		echo str_pad("Due for {$period}:", ($table_size['width']*1.2)-12, ' ') . str_pad(number_format(ceil($payable_prelim), 2), 10, ' ', STR_PAD_LEFT) . " *\n";

	} else {
		if($trans_after_assess<>0){
			echo str_pad("Due for {$period}:(".number_format($payable_prelim, 2)."+Bal=".number_format(ceil($bal_b4_assessment), 2).'-'.number_format(abs($trans_after_assess), 2).')', ($table_size['width']*1.2)-12, ' ') . str_pad(number_format(ceil($payable_prelim), 2), 10, ' ', STR_PAD_LEFT) . " *\n";
		} else {
			echo str_pad("Due for {$period}:(".number_format($payable_prelim, 2)."+Bal=".number_format(ceil($bal_b4_assessment), 2).')', ($table_size['width']*1.2)-12, ' ') . str_pad(number_format(ceil($payable_prelim), 2), 10, ' ', STR_PAD_LEFT) . " *\n";
		}
	}	
}
?>
<?php echo str_pad('', $table_size['width']*1.2, '-'); ?>

<?php echo $this->print_lib->condensed(); ?>
<?php echo "*Amount may change depending on payments received and other charges incurred during the semester.  For details of your \n";?>
<?php echo "assessment and payments, login to your HNUMIS account or visit the Finance Office.\n"; ?>

<?php 
	$print_count_display = 0;
	switch ($period){
		case "Prelim":
			$print_count_display = $print_count_prelim;
		case "Midterm":
			$print_count_display = $print_count_midterm;
		case "Prefinal":
			$print_count_display = $print_count_prefinal;
		case "Final":
			$print_count_display = $print_count_clearance+1;
	}
?>

<?php echo "Prints:[<span id='{$period}_counter'>".$print_count_display."</span>]\n"; ?>
<?php echo "\nStudent Assessment. Printed using HNUMIS. " . date(DATE_RFC822) . $this->print_lib->no_condensed();
//echo "\n";
//echo "\n";
//echo "\n";
//echo "\n";
//echo "\n"; ?>
</div>
<script>
//function print(content) {
//    var applet = document.jzebra;
//    if (applet != null) {
//    	//applet.append('\x1B\x40');
//		applet.append(content);
//		applet.print();
//	}
//}
    
$(document).ready(function(){

	$('.print_content').on('mousedown', function(){
		$.ajax({
			type: 'POST',
			data: {idnum: <?php echo "'".$idnum."'"; ?>, 
			       hist_id: <?php echo "'".$hist_id."'"; ?>,
			       period: 'Pre-Final' },
			url: "<?php echo site_url('prints_counter/check_print_count'); ?>",
			success: function(result) {	
				//alert('ajax result = '+result);
				$('#Pre-final_counter').html(result); 
				$('#print_prefinal').html("Print Prefinal Statement ["+result.trim()+"]"); 
			},
			error:  function (x, e)
			{
			  alert("The call to the server side failed. " + x.responseText);
			}
		});
	});

	$('.print_content').on('mouseup', function(){
		var id=$(this).data('id');
		var content_to_print = "";
		var hiddenElement = document.createElement('a');
		var count = $('#Pre-final_counter').text();		
		//var countString = parseInt(count)+1;
		var countString = parseInt(count);
		$('#Pre-final_counter').html(countString); 
		content_to_print = $('#'+id).text();
		hiddenElement.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(content_to_print);
		hiddenElement.target = '_blank';
		hiddenElement.download = 'receipt.txt';
		document.getElementById('container').appendChild(hiddenElement);
		hiddenElement.click();

	});


	$('.print_final').on('mousedown', function(){
		$.ajax({
			type: 'POST',
			data: {idnum: <?php echo "'".$idnum."'"; ?>, 
			       hist_id: <?php echo "'".$hist_id."'"; ?>,
			       period: 'Final' },
			url: "<?php echo site_url('prints_counter/check_print_count'); ?>",
			success: function(result) {	
				$('#final_counter').html(result.trim()); 
				$('#print_final').html("Print Student Clearance ["+result.trim()+"]"); 
				$('#view_clearance').html("View Student Clearance ["+result.trim()+"]"); 
			},
			error:  function (x, e)
			{
			  alert("The call to the server side failed. " + x.responseText);
			}
		});
	});

	$('.print_final').on('mouseup', function(){
		var id=$(this).data('id');
		var content_to_print = "";
		var hiddenElement = document.createElement('a');
		var count = $('#final_counter').text();	
		//var countString = parseInt(count)+1;
		var countString = parseInt(count);
		$('#final_counter').html(countString); 
		content_to_print = $('#'+id).text();
		hiddenElement.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(content_to_print);
		hiddenElement.target = '_blank';
		hiddenElement.download = 'receipt.txt';
		document.getElementById('container').appendChild(hiddenElement);
		hiddenElement.click();

	});

});
</script>

<div class="row-fluid" style="margin-top:20px">
	<div class="span12" style="margin-top:20px">
		<?php if(empty($selected_history_id)):?>
		  <?php if($term=='Summer'): ?>
		    <button class="btn btn-large btn-primary print_content" data-id="assessment_to_print">Print Assessment</button>
		    <button class="btn btn-large btn-primary print_content" data-id="print_clearance">Print Student Clearance</button>
		  <?php else: ?>
		  <button class="btn btn-large btn-primary print_content" data-id="assessment_to_print">Print Assessment</button>
		    <button class="btn btn-large btn-primary print_content" name="btn1" data-id="print_midterm_assessment">Print Midterm Statement</button>
		    <button class="btn btn-large btn-primary print_content" name="btn2" data-id="print_prefinal_assessment" id="print_prefinal">Print Prefinal Statement [<?php echo $print_count; ?>]</button>
		    <button class="btn btn-large btn-primary print_final" name="btn3" data-id="print_clearance" id="print_final">Print Student Clearance [<?php echo $print_count_clearance; ?>]</button>
		  <?php endif; ?>
		<?php endif; ?>
		<div id="container" style="display:none;"></div>
	</div>
</div>
<?php endif; ?>

<a name="jumpto"></a> 

<div class="row-fluid" style="margin-top:4px">
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-large btn-info btn-lg" data-toggle="modal" data-target="#viewAssmt">View Assessment</button>
  <button class="btn btn-large btn-info btn-lg" name="btn4" data-toggle="modal" data-target="#viewMidTerm">View Midterm Statement</button>
  <button class="btn btn-large btn-info btn-lg" name="btn5" data-toggle="modal" data-target="#viewPreFinal">View Prefinal Statement [ ]</button>
  <button class="btn btn-large btn-info btn-lg" name="btn6" data-toggle="modal" data-target="#viewClearance" id="view_clearance">View Student Clearance [<?php echo $print_count_clearance; ?>]</button>
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal hide fade" id="viewAssmt" role="dialog" tabindex="-1" style="width:980px; margin-left:-500px;">
    <div class="modal-dialog-centered" role="document" style="font-family:'Courier New';font-size:9px;">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Student Assessment</h4>
        </div>
        <div class="modal-body">
          <pre><?php echo '<script type="text/javascript">document.write(document.getElementById("assessment_to_print").innerHTML);</script>' ?></pre>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal hide fade" id="viewMidTerm" role="dialog" style="width:1050px; margin-left:-500px;" >
    <div class="modal-dialog" role="document" style="font-family:'Courier New';font-size:11px;">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Midterm Statement</h4>
        </div>
        <div class="modal-body">
          <pre><?php echo '<script type="text/javascript">document.write(document.getElementById("print_midterm_assessment").innerHTML);</script>' ?></pre>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>      
    </div>
  </div>  
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal hide fade" id="viewPreFinal" role="dialog" style="width:1050px; margin-left:-500px;" >
    <div class="modal-dialog" role="document" style="font-family:'Courier New';font-size:11px;">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pre-Final Statement</h4>
        </div>
        <div class="modal-body">
          <pre><?php echo '<script type="text/javascript">document.write(document.getElementById("print_prefinal_assessment").innerHTML);</script>' ?></pre>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>      
    </div>
  </div>  
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal hide fade" id="viewClearance" role="dialog" style="width:1070px; margin-left:-500px;" >
    <div class="modal-dialog" role="document" style="font-family:'Courier New';font-size:11px;">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Final Statement</h4>
        </div>
        <div class="modal-body">
          <pre><?php echo '<script type="text/javascript">document.write(document.getElementById("print_clearance").innerHTML);</script>' ?></pre>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>      
    </div>
  </div>  
</div>
