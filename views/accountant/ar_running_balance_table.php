<?php 

/*==================================================================
 * Table Lib Child
 ==================================================================*/


class ar_table extends Table_lib{
	
	private $q_type = 'basic';  
	public $current_group;
	
	public function set_q_type($q_type){
		$this->q_type = $q_type;
	}
	
	
	public function __construct(){
		parent::__construct();
		$current_group = '';
	}
	
	public function format_raw_data_code($row){
		if ($this->q_type == 'basic'){		
				if ($this->current_group != $row['type']){
					$this->current_group = $row['type'];
					if ($row['type'] == 2)
						$type = 'Kinder';
					else if ($row['type'] == 3)
						$type = 'Grade School';
					else if ($row['type'] == 4)
						$type = 'High School (Day)';
					else if ($row['type'] == 5)
						$type = 'High School (Night)';
					else if ($row['type'] == 6)
						$type = 'College';
					else if ($row['type'] == 7)
						$type = 'Graduate School';
					else
						$type = $row['type'];
					return '<td colspan="5" style="background:#eee;font-size:1.1em;font-weight:bold" class="header">'.$type.'</td></tr><tr><td>&nbsp;&nbsp;'.$row['code'].'</td>';
						
					
					//return '<td colspan="5" style="background:#eee;font-size:1.1em;font-weight:bold" class="header"> '.$row['type'].'</td></tr><tr><td>&nbsp;&nbsp;'.$row['code'].'</td>';
				}
				else 
					return '<td>&nbsp;&nbsp;'.$row['code'].'</td>';
		}else{
				if ($this->current_group != $row['type']){
					$this->current_group = $row['type'];
					// 2 = Kinder,3 = Grade School ,4 = High School Day,5 = Night
					if ($row['type'] == 2)
						$type = 'Kinder';
					else if ($row['type'] == 3)
						$type = 'Grade School';
					else if ($row['type'] == 4)
						$type = 'High School (Day)';
					else if ($row['type'] == 5)
						$type = 'High School (Night)';
					else if ($row['type'] == 6)
						$type = 'College';
					else if ($row['type'] == 7)
						$type = 'Graduate School';
					else
						$type = $row['type'];
					return '<td colspan="5" style="background:#eee;font-size:1.1em;font-weight:bold" class="header">'.$type.'</td></tr><tr><td>&nbsp;&nbsp;'.$row['code'].'</td>';
				}
				else 
					return '<td>&nbsp;&nbsp;'.$row['code'].'</td>';				
		}
	}
	
	public function format_data_debit($row){
		return number_format($row['debit'],2);
	}
	
	public function format_cell_debit($row){
		return 'style="text-align:right;margin"';
	}
	
	public function format_data_credit($row){
		return number_format($row['credit'],2);
	}
	
	public function format_cell_credit($row){
		return 'style="text-align:right;margin"';
	}
	
	public function format_data_stud($row){
		return '<a class="ar-details-student-list btn btn-link" data-abbr="'.$row['type'].'" data-trans-type="'.($row['debit'] > 0 ? 'debit' : 'credit').'" data-trans-code="'.$row['code'].'">'.number_format($row['stud']).'</a>';
	}
	
	public function format_cell_stud($row){
		return 'style="text-align:right;margin"';
	}
}

$ar_table = new ar_table();

$ar_table->set_q_type($q_type); 


/*==================================================================
 * Table Header
 ==================================================================*/

$ar_table->set_attributes(array('class'=>'table table-condensed table-hover','id'=>'ar_balance'));
$ar_table->insert_head_row(array(
										array('id'=>'code',
											  'value'=>'Code'
											 ),
		  								array('id'=>'trans',
		  									  'value'=>'Transactions'
		  									 ),
										array('id'=>'stud',
											  'value'=>'Students',
											  'attributes' => array('style'=>'text-align:right')
											 ),
										array('id'=>'debit',
											  'value'=>'Debit',
											  'attributes' => array('style'=>'text-align:right')
											 ),
										array('id'=>'credit',
											  'value'=>'Credit',
											  'attributes' => array('style'=>'text-align:right')
											  )
									));

/*===========================================================
 * if DEBUG
 *===========================================================*/

//$debug = TRUE;

if (isset($debug) AND $debug === TRUE){
	echo '<pre>';
	print_r($data);
	die();
}


/*============================================================
 * @ Media Print Style
 *===========================================================*/

?>

<style>
@media print {
@page {size: portrait}
#maincontainer {
	padding:0 !important;
}
body {
	padding:0 !important;
	margin:20px !important;
}
th {
	text-shadow: none !important;
}
h2 {
	font-size:1.3em;
	margin-top:-20px;
}
.navbar, footer{
	display:none !important;
}
td {
	font-size: 0.8em !important;
	line-height: 8px !important;
}
.breadcrumb,
button,
.add-on,
.btn,
.hint,
#lil-stat {
	display:none;
}

input,
span {
	border:none;
}
</style>

<?php 
/*==================================================================================================
 * DESIGN
 *================================================================================================== */

//print_r ($x_groups); die();
//print selected groups
if (isset($x_groups) AND is_array($x_groups)){
	if ($q_type=='basic'){
/*
		$group_list = array_map(
				function($obj) {
					return $obj->yr_level;
				},$x_groups);
			*/
	}else{
		$group_list = array_map(
				function($obj) {
					return $obj->abbreviation;
				},$x_groups);
		//print_r($group_list);die();
		$group_list = ' <span style="font-size:0.7em;color:#838383">'.implode(", ", $group_list).'</span>';

	}
	
}

?>
<div class="span7">
<div class="heading" >
	<h3>AR Running Balance<?php echo ( isset($type) ? ' - '.$type : '')?></h3>
	<?php 
	if (isset($start_date) AND isset($end_date)){
		$s = strtotime($start_date);
		$e = strtotime($end_date);
		
		if ($e > $s)
			echo '<h3>For Period: '.$start_date.' to '.$end_date.'</h3>';
		else if ($e < $s)
			echo '<h3>For Period: '.$end_date.' to '.$start_date.'</h3>';
		else
			echo '<h3>For Period: '.$start_date.'</h3>';
	}
	if (!isset($x_show_by)) 
		$x_show_by='asasadsds';
	//if (isset($group_list) AND !empty($group_list)){
	//	echo $group_list;
	//}
	?>
	
	<?php 
	/*==================================================================================================
	 * SCRIPT
	 *================================================================================================== */
	?>
	<div id="ar_balance_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="arBalanceModal" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    <h3 id="ar_balance_modal_label">Details</h3>
		  </div>
		  <div class="modal-body">
			    <table id="ar-details-table" class="table table-bordered">
			    <thead>
			    <tr>
			    	<th>#</th>
			    	<th>ID Number</th>
			    	<th>Name</th>
			    	<th>Course/Year</th>
			    	<th id="ar_balance_modal_trans_type">Debit</th>
			    	<th>Reference</th>
			    </tr>
			    </thead>
			    <tbody id="ar_balance_modal_content">
			    
			    </tbody>
			    </table>
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  </div>
	</div>
	<style>
	#ar-details-table > tbody > tr > td:nth-child(5){
		text-align:right;
		padding-right:5px;
	}
	#ar_balance_modal {
		width: 90%;
		left: 5%;
		margin-left: 0;
	}
	</style>
	<script>
	$(document).ready(function() {
		$('.ar-details-student-list').bind('click', function(){
			show_loading_msg();
			$.post('<?php echo site_url('ajax/ar_balance_details/'.$q_type) ?>',
				{
					'abbr' 		: $(this).data('abbr'),
					'trans_type': $(this).data('trans-type'),
					'trans_code': $(this).data('trans-code'),
					'start_date': <?php echo $this->db->escape($start_date)?>,
					'end_date'  : <?php echo $this->db->escape($end_date)?>,
					'x_show_by' : <?php echo $this->db->escape($x_show_by)?>,
					'is_isis'   : <?php echo $this->db->escape($is_isis)?>
				},
			  function(data){
			    if (data.type == 'Success'){
				    hide_loading_msg();
				    $('#ar_balance_modal_label').text(data.heading);
				    $('#ar_balance_modal_trans_type').text(data.trans_type);
					var str = '';
				    $.each(data.data, function (i,item){
						str += '<tr><td>'+(i+1)+'</td><td>'+item.idno+'</td><td>'+item.student+'</td><td>'+item.course_year+'</td><td>'+item.amount+'</td><td>'+item.ref+'</td></tr>';
					});
					$('#ar_balance_modal_content').html(str);
					$('#ar_balance_modal').modal('show');
			    }
			    else {
			    	hide_loading_msg();
			    	show_sticky_msg(data.type,data.message);
			    }
			  }, "json");
		});
	});
	</script>

</div>
<?php

/*==================================================
 * PRINT TABLE
 ==================================================*/
if ($data)
{
	$ar_table->insert_data($data);
	$ar_table->content();
}
else
{
	$ar_table->content();
	echo '<div clas="row-fluid"><div class="span12">No A/R Balance at given time frame</div></div>';
}

?>
</div>

