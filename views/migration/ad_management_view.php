<style>
	table.isis_t 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
	tr.sub_total {font-weight: bold;}
</style>
<h3>List of Classes:</h3>
<hr>

<div id="isis" class="span7">


<form id="frm1" method="post">  
	<input type="hidden" name="nonce" value="<?php echo $this->common->nonce(); ?>">

<table class="table table-condensed table-striped isis_t">
	<thead>
	<h4><?php echo count((array)$rows) < 1 ? '' : $rows[0]->room .' Occupants:'; ?></h4>
	<tr>
		<th></th>
		<th>CO ID</th>
		<th>Subject</th>
		<th>Section</th>
		<th>Room</th>
		<th>Day</th>
		<th>Time</th>
		<th>ID No.</th>
		<th>Teacher</th>					
		<th>
			<?php if (count((array)$rows) > 0): ?> 
				<input class="checkboxes" type="checkbox" id="ckbCheckAll" />
			<?php endif ?>
		</th>
				
	</tr>
	</thead>

<?php 
	//print_r($rows);die();
	$numrow = 0;
	if (count((array)$rows) < 1){ 
		echo '<tr><td class="center" colspan="10"><b>No Record Found!</b></td></tr>';
	}else{
		foreach($rows as $row){
			$numrow++;
			
?>
			<tr>
			<td class="center"><?php echo $numrow .'.'; ?>
			<td class="center"><?php echo $row->co_id; ?></td>
			<td class="left"><?php echo $row->course_code; ?></td>
			<td class="center"><?php echo $row->section_code; ?></td>
			<td class="center"><?php echo $row->room ?></td>
			<td class="center"><?php echo $row->sked ?></td>
			<td class="center"><?php echo $row->start_time . $row->end_time;  ?></td>
			<td class="center"><?php echo $row->teacher_idno; ?></td>
			<td class="left"><?php echo $row->teacher_fname . ' ' . $row->teacher_lname  ; ?></td>			
			<td class="center"><input class="checkboxes" type="checkbox" name="check[]" value="<?= $numrow-1 ?>" /></td></tr>
		
<?php
		 
		} //foreach
	} //count

?>	

</table>
<?php 
	if (count((array)$rows) > 0){ 
?>
	<input type="submit" name="action" value="Create Teacher's OU" class="btn btn-primary">
	<input type="submit" name="action" value="Create Subject's OU" class="btn btn-primary">
	<input type="submit" name="action" value="Create Student Folders" class="btn btn-primary">
	<input type="submit" name="action" value="Create Student Logins" class="btn btn-primary">
	<input type="submit" name="action" value="Share Students Folders" class="btn btn-primary">
	<input type="submit" name="action" value="More Actions" class="btn btn-primary">
	
	
<?php 
	} 
?>
</form>
</div>

<script>
    $(document).ready(function () {
        $("#ckbCheckAll").click(function () {
            $(".checkboxes").attr('checked', this.checked);
        });
    });

    
</script>
