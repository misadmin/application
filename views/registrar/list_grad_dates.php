<h2 class="heading">Graduation Schedule -- <?php print($academic_term->term."  ".$academic_term->sy);?></h2>

<table style = "width:40%;" id="offerings" class="table table-bordered table-condensed">
    <thead>
    <tr>
      <th width="15%" class="header" style="text-align:center; vertical-align:middle;">Date</th>
      <th width="15%" class="header" style="text-align:center; vertical-align:middle;">Academic Program</th>
       <th width="15%" class="header" style="text-align:center; vertical-align:middle;">Delete</th>
    </tr>
    </thead>
    <?php
    if($grad_schedule) { 
		if($grad_schedule) {
			foreach ($grad_schedule AS $gradsked) {
			?>
			<tr>
				 <td style="text-align:center; vertical-align:middle; "><?php print($gradsked->grad_date); ?></td>
				 <td style="text-align:left; vertical-align:middle; "><?php print($gradsked->abbreviation); ?> </td>
				  <td style="text-align:center; vertical-align:middle; ">
					 <a class="delete_schedule" href="<?php print($gradsked->id); ?>"><i class="icon-trash"></i></a>
	              </td>
			</tr>
   	<?php
			}
		}
	 } else {	
	?>
	<tr>
		 <td style="text-align:center; vertical-align:middle; "> No Grudation Schedule</td>
		 
	</tr>
<?php }?>	
</table>
<?php

?>
<script>
$().ready(function() {
	$('#offerings').dataTable( {
	    "aoColumnDefs": [
	    { "bSearchable": false, "aTargets": [ 1,2,3,4,5,6 ] }
	                   ] } );
  }
)
</script>

<form id="edit_priv" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="edit_privilege_form" />
		
</form>			


<script>
	$('.edit_privilege').click(function(event){
		event.preventDefault(); 
	
		     var priv_id = $(this).attr('href');
		
	  		$('<input>').attr({
			    type: 'hidden',
			    name: 'priv_id',
			    value: priv_id,
			}).appendTo('#edit_priv');
			$('#edit_priv').submit();
		
	});
</script>

<form id="delete_sked" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="step" value="3" />
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="academic_term_id" value="<?php print($academic_term->id); ?>" >
		
	</form>			

<script>
$('.delete_schedule').bind('click', function(event){
	event.preventDefault();
	
		var sked_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'sked_id',
		    value: sked_id,
		}).appendTo('#delete_sked');
		$('#delete_sked').submit();
			
});
</script>