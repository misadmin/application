<?php

	class Laboratory_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


	    function Add_Fee($data) 
		{
			$query = "INSERT INTO lab_fees (id, academic_years_id, courses_id, rate, posted) 
   					      VALUES ('',
								  {$this->db->escape($data['academic_years_id'])},
								  {$this->db->escape($data['course_id'])},
								  {$this->db->escape($data['rate'])},
								  'N')";

			$this->db->query($query);
			$id = @mysql_insert_id();
			//print($query);
			//die();
			return $id;
			
		}

		function AddGraduate_Tuition($data) 
		{
			$query = "INSERT INTO tuition_fees (id, academic_programs_id, academic_years_id, rate, yr_level) 
   				      VALUES ('',
						  	  {$this->db->escape($data['programs_id'])},
					  	  	  {$this->db->escape($data['academic_years_id'])},
						      {$this->db->escape($data['rate'])},
						      {$this->db->escape($data['yr_level'])})";
				
			$this->db->query($query);					
				
			$id = @mysql_insert_id();
			
			return $id;
		}
		
//Added: 2/20/2013
		
	function AddNew_OtherTuition_Item($item_name){
	  // print_r($data);
	  // die();
		$query = "INSERT INTO courses_groups (id, group_name) 
   		      		VALUES ('',
				  			{$this->db->escape($item_name)})";
		$this->db->query($query);
		$id = @mysql_insert_id();
		//print($id);
		//die();
		$query = "INSERT INTO other_tuition_fee_items (id, courses_groups_id) 
   		      		VALUES ('',
				  			'".$id."')";		
	
		$this->db->query($query);						  
		$id = @mysql_insert_id();		
		return $id;
	}	

//Added: 2/20/2013
	function AddNew_Misc_Item($item_name){
	
		$query = "INSERT INTO misc_items (id, description) 
   		      		VALUES ('',
				  			{$this->db->escape($item_name)})";
		$this->db->query($query);						  
		$id = @mysql_insert_id();		
		return $id;
	}		  

//Added: 2/20/2013
	function AddNew_OtherFee_Item($item_name){
		$query = "INSERT INTO other_fee_items (id, description) 
   		      		VALUES ('',
				  			{$this->db->escape($item_name)})";
		$this->db->query($query);						  
		$id = @mysql_insert_id();		
		return $id;
	}

//Added: 2/21/2013
	function AddNew_AddOtherFee_Item($item_name){
		$query = "INSERT INTO additional_other_fee_items (id, description) 
   		      		VALUES ('',
				  			{$this->db->escape($item_name)})";
		$this->db->query($query);						  
		$id = @mysql_insert_id();		
		return $id;
	}	
	
//Added: 2/23/2013
    	
	function ListTuitionFees($data) {
			$result = 0;
			$q = "SELECT 
						a.id,
						b.description, 
						b.abbreviation,
						a.yr_level,
						a.rate					
					FROM
						tuition_fees AS a, academic_programs as b
					WHERE 
					    b.colleges_id = {$this->db->escape($data[college_id])}
						AND a.academic_programs_id = b.id
						AND a.academic_years_id = {$this->db->escape($data[academic_years_id])}
					ORDER BY
					    b.description, a.yr_level";
			
			//print($q);
		//	die();
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}
		
		function getFee($fee_id) 
		{
			$result = null;
			
			$q = "SELECT a.rate, a.yr_level, a.academic_years_id, b.description 
					FROM
						tuition_fees as a, academic_programs as b, academic_years as c
					WHERE
						a.academic_programs_id = b.id
						AND a.id = {$this->db->escape($fee_id)} "; 
			
				
			$query = $this->db->query($q);
			
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}	
		
	//Added: 3/1/2013
	function UpdateTuitionFee($id, $tf){
	$q = "UPDATE tuition_fees 
			SET 
				rate={$this->db->escape($tf)} 
			WHERE
				id={$this->db->escape($id)}";
			
		if ($this->db->query($q)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
}

//end of tuition_model.php