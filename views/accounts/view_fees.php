<div style="width:auto;">
<div style="text-align:right">
<a class="download_enroll" href="" academic_terms_id="<?php print($term->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>
 <table width="100%" border="0" cellspacing="0" cellpadding="1" class="table table-bordered">
 	<thead>
  
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td rowspan="2" style="width:22%; text-align:center; vertical-align:middle;">COLLEGES &amp; PROGRAMS </td>
    <td colspan="3" style="width:13%; text-align:center;">1st Year </td>
    <td colspan="3" style="width:13%; text-align:center;">2nd Year </td>
    <td colspan="3" style="width:13%; text-align:center;">3rd Year </td>
    <td colspan="3" style="width:13%; text-align:center;">4th Year </td>
    <td colspan="3" style="width:13%; text-align:center;">5th Year </td>
    <?php //<td colspan="3" style="width:13%; text-align:center;">GRAND TOTAL </td> ?>
    </tr>
  
	</thead>
  <?php 
  		foreach($colleges AS $college) {
  ?>
  <tr style="text-align:center;" class="success">
    <td colspan="19" style="text-align:left; font-weight:bold;"><?php print($college->name); ?></td>
    </tr>
  <?php
			$programs = $this->Enrollments_Model->ListEnrollmentSummary($college->id, $academic_terms_id);
			
			$total1 = 0;
			$total2 = 0;
			$total3 = 0;
			$total4 = 0;
			$total5 = 0;			
			$total_m = 0;			
			$total_f = 0;			
			$col_total = 0;
			$college_total = array('m1'=>0,'m2'=>0,'m3'=>0,'m4'=>0,'m5'=>0,
									'f1'=>0,'f2'=>0,'f3'=>0,'f4'=>0,'f5'=>0,
									'total1'=>0,'total2'=>0,'total3'=>0,'total4'=>0,'total5'=>0,
									'total_m'=>0,'total_f'=>0);
			foreach ($programs AS $program) {
				$college_total['m1'] = $college_total['m1'] + $program->m1; 
				$college_total['f1'] = $college_total['f1'] + $program->f1;
				$total1 = $program->m1 + $program->f1; 
				$college_total['total1'] = $college_total['total1'] + $total1;
				$college_total['m2'] = $college_total['m2'] + $program->m2; 
				$college_total['f2'] = $college_total['f2'] + $program->f2; 
				$total2 = $program->m2 + $program->f2; 
				$college_total['total2'] = $college_total['total2'] + $total2;
				$college_total['m3'] = $college_total['m3'] + $program->m3; 
				$college_total['f3'] = $college_total['f3'] + $program->f3;
				$total3 = $program->m3 + $program->f3; 
				$college_total['total3'] = $college_total['total3'] + $total3;
				$college_total['m4'] = $college_total['m4'] + $program->m4; 
				$college_total['f4'] = $college_total['f4'] + $program->f4; 
				$total4 = $program->m4 + $program->f4; 
				$college_total['total4'] = $college_total['total4'] + $total4;
				$college_total['m5'] = $college_total['m5'] + $program->m5; 
				$college_total['f5'] = $college_total['f5'] + $program->f5; 
				$total5 = $program->m5 + $program->f5; 
				$college_total['total5'] = $college_total['total5'] + $total5;
				$total_m = $program->m1 + $program->m2 + $program->m3 + $program->m4 + $program->m5; 
				$college_total['total_m'] = $college_total['total_m'] + $total_m;
				$total_f = $program->f1 + $program->f2 + $program->f3 + $program->f4 + $program->f5; 
				$college_total['total_f'] = $college_total['total_f'] + $total_f;
				$program_total = $total_m + $total_f; 
				$col_total = $col_total + $program_total;
   ?>
  <tr>
    <td style="text-align:left; padding-left:30px;"><?php print($program->abbreviation); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m1); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f1); ?></td>
    <td style="text-align:center; font-weight:bold;"><?php print($total1); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m2); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f2); ?></td>
    <td style="text-align:center; font-weight:bold;"><?php print($total2); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m3); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f3); ?></td>
    <td style="text-align:center; font-weight:bold;"><?php print($total3); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m4); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f4); ?></td>
    <td style="text-align:center; font-weight:bold;"><?php print($total4); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($program->m5); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($program->f5); ?></td>
    <td style="text-align:center; font-weight:bold;"><?php print($total5); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($total_m); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($total_f); ?></td>
    <td style="text-align:center; font-weight:bold;"><?php print($program_total); ?></td>
  </tr>
  <?php
  			}
			$grand_total['m1'] = $grand_total['m1'] + $college_total['m1'];
			$grand_total['f1'] = $grand_total['f1'] + $college_total['f1'];
			$grand_total['m2'] = $grand_total['m2'] + $college_total['m2'];
			$grand_total['f2'] = $grand_total['f2'] + $college_total['f2'];
			$grand_total['m3'] = $grand_total['m3'] + $college_total['m3'];
			$grand_total['f3'] = $grand_total['f3'] + $college_total['f3'];
			$grand_total['m4'] = $grand_total['m4'] + $college_total['m4'];
			$grand_total['f4'] = $grand_total['f4'] + $college_total['f4'];
			$grand_total['m5'] = $grand_total['m5'] + $college_total['m5'];
			$grand_total['f5'] = $grand_total['f5'] + $college_total['f5'];
			$grand_total['total1'] = $grand_total['total1'] + $college_total['total1'];
			$grand_total['total2'] = $grand_total['total2'] + $college_total['total2'];
			$grand_total['total3'] = $grand_total['total3'] + $college_total['total3'];
			$grand_total['total4'] = $grand_total['total4'] + $college_total['total4'];
			$grand_total['total5'] = $grand_total['total5'] + $college_total['total5'];
			$grand_total['total_m'] = $grand_total['total_m'] + $college_total['total_m'];
			$grand_total['total_f'] = $grand_total['total_f'] + $college_total['total_f'];
			$grand_total['grand_total'] = $grand_total['grand_total'] + $col_total;

   ?>
  <tr style="font-weight:bold;">
    <td style="text-align:right;">COLLEGE TOTAL: </td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m1']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f1']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total1']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m2']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f2']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total2']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m3']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f3']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total3']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m4']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f4']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total4']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['m5']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['f5']); ?></td>
    <td style="text-align:center;"><?php print($college_total['total5']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($college_total['total_m']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($college_total['total_f']); ?></td>
    <td style="text-align:center;"><?php print($col_total); ?></td>
  </tr>
   <?php
  		}
  ?>
  <tr style="font-weight:bold; background:#EAEAEA;">
    <td style="text-align:right;">GRAND TOTAL: </td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m1']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f1']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total1']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m2']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f2']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total2']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m3']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f3']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total3']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m4']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f4']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total4']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['m5']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['f5']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['total5']); ?></td>
    <td style="text-align:center; color:#0000CC;"><?php print($grand_total['total_m']); ?></td>
    <td style="text-align:center; color:#FF0000;"><?php print($grand_total['total_f']); ?></td>
    <td style="text-align:center;"><?php print($grand_total['grand_total']); ?></td>
  </tr>
</table>
<div style="text-align:right">
<a class="download_enroll" href="" academic_terms_id="<?php print($term->id); ?>">Download to PDF<i class="icon-download-alt"></i></a>
</div>

</div>

  <form id="download_enroll_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="3" />
</form>
<script>
	$('.download_enroll').click(function(event){
		event.preventDefault(); 
		var academic_terms_id = $(this).attr('academic_terms_id');
			
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_terms_id',
			value: academic_terms_id,
		}).appendTo('#download_enroll_summary_form');
		$('#download_enroll_summary_form').submit();
		
	});
</script>
