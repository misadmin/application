<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
	vertical-align: middle;
	text-align: center;
	border-color: inherit;
}

#mysked_filter { display: none; }

th.head {
	padding:3px;
	text-align:center;
	vertical-align:middle;
}
@media only screen and (min-width: 768px) {
	#course-offerings {padding-right: 36px;}	
}
.course-table-container h5 {
	padding-bottom: 10px;
}

</style>


<div class="row-fluid">
	<div class="span5 pull-right course-table-container">
		<h5 >My Schedule:</h5>
		<table class="table table-bordered table-hover" id="mysked">
			<thead>
				<tr><th colspan="5" style="text-align:center">Enrolled Courses</th></tr>
				<tr>
					<th width="10%" class="head">Action</th>
					<th width="25%" class="head">Course</th>
					<th width="35%" class="head">Time</th>
					<th width="20%" class="head">Room</th>
					<th width="10%" class="head">Units</th>
				</tr>
			</thead>
			<tbody>
<?php
			$bracketed = 0;
			$credit = 0;
			$record_counter = 0;
			//print_r($grades);die(); 				
			if ($grades != null) {
					foreach ($grades AS $my_grade) {
						$record_counter++;
?>
					<tr>
					    <td style="vertical-align:middle; text-align:center;">
<?php 
					 		if ($assessed) {
								echo "&nbsp;";
							} else {	
								echo "<a class=\"cancel_enroll\" href=\"{$my_grade->id}\" course_code=\"{$my_grade->course_code}\" section_code=\"{$my_grade->section_code}\"><i class=\"icon-trash\"></i></a>";
							} 
?> 
						</td>
				    	<td style="vertical-align:middle; text-align:left;">
							<?php echo $my_grade->course_code." [".$my_grade->section_code."]"; ?>
						</td>
						<?php $my_offering_slots = $this->Offerings_Model->ListOfferingSlots($my_grade->course_offerings_id); ?>						
				     	<td style="text-align:left; vertical-align:middle;">
<?php 			     	
						if ($my_offering_slots){
							foreach($my_offering_slots[0] AS $my_slots) 
								echo $my_slots->tym." ".$my_slots->days_day_code ."<br>";
						}else{ 
							echo "Error! Please send feedback to concerns.";
						} 
?>						  
						<td style="text-align:left; vertical-align:middle;">
<?php 					
						if ($my_offering_slots){
							foreach($my_offering_slots[0] AS $my_slots)
								echo $my_slots->room_no . "<br>";
						}else{
							echo "Error! Please send feedback to concerns.";
						}							
?>	
					    </td>
					    <td class="head" style="vertical-align:middle; text-align:right;">
<?php					 
							if ($my_grade->is_bracketed == 'Y') {
								$bracketed = $bracketed + $my_grade->credit_units;
								echo "(".$my_grade->credit_units.")"; 
							}else{
								$credit = $credit + $my_grade->credit_units;
								echo $my_grade->credit_units; 		
							}					
?>
						</td>
					</tr>
			   
<?php
					} // end of foreach ($grades AS $my_grade)
						
				} // end of else count($grades) < 1. $skeds var is recommended to replace $grades var 
					
			
			if ($record_counter == 0){
?>				
				<tr>
					<td colspan="5" style="vertical-align:middle; text-align:center; height:100px"> You have no courses (subjects) enrolled yet.</td>
				</tr>
<?php 		
			}			
?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4" class="head" style="vertical-align:middle; text-align:right;">Total Units Enrolled:</td>
					<td class="head" style="vertical-align:middle; text-align:right;"><?php echo number_format($credit,1); ?></td>
				</tr>
				<tr>
					<td colspan="4" class="head" style="vertical-align:middle; text-align:right; ">Maximum Credit Units: </td>
					<td class="head" style="vertical-align:middle; text-align:right;"><?php	echo number_format($max_units_to_enroll,1); ?></td>
   				</tr>
			</tfoot>
		</table>
	</div> <!--  end of div for courses enrolled -->
	
	
	<!-- start of div for course offerings -->
	<div class="span6 pull-left course-table-container" id="course-offerings"><h5>To enroll, select from the course offerings below:</h5>
		<table class="table table-bordered table-hover" id="COTable">
			<thead>
				<tr class="head"><th colspan=5 style="text-align:center">Course Offerings</th></tr>
				<tr>
					<th width="26%" class="head">Course</th>
					<th width="10%" class="head">Units</th>
					<th width="28%" class="head">Time</th>
					<th width="16%" class="head">Room</th>
					<th width="20%" class="head">Enrolled</th>
				</tr>
			</thead>
			<tbody>

<?php
				if ($courses) {
					foreach ($courses AS $course) {
						$id = "('".$course->id."')";	
						$parallel = $this->Offerings_Model->getOffering($course->id);						
						if ($parallel->parallel_no) {
							$parallel_no = ltrim($parallel->parallel_no,'0');
						} else {
							$parallel_no = 0;
						}					
						
						/** 
						 * 
						 * i temporarily made $enrollees false. this should be queried only when user click the enrollees link
						 * to minmize overhead cost. otherwise, PHP will get data (the classlist of every course) from mysql
						 * and store this to memory but only clicked classlists will be used. imagine how many courses will be
						 * displayed on one student enrollment panel and how many of them will access this panel during enrollment    
						 * SUGGESTION: do this via ajax.
						 * 
						 * @by: tatskie
						 */
						$enrollees = false;
						//$enrollees = $this->Student_Model->ListEnrollees_Including_Parallel($course->id,$parallel->parallel_no);
						
						
?>
					<tr>
						<td style="text-align:left; vertical-align:middle;" title="<?php echo sanitize_text_field($course->descriptive_title); ?>">
<?php 
			 				$cnt_enrollee = 0;
							if ($course->enrollee_parallel) {
								$cnt_enrollee = $course->enrollee_parallel;
							} else {
								$cnt_enrollee = $course->enrollee;					
							}
							if ($cnt_enrollee < $course->max_students) {
								if ($course->elective == 'Y') {
									echo "<a class=\"enroll_slot\" href=\"{$course->id}\" courses_id=\"{$course->courses_id}\" max_enrolee=\"{$course->max_students}\"  parallel_no=\"{$course->parallel_no}\" advised=\"{$course->advised}\" elective=\"{$course->elective}\" course_code=\"{$course->course_code}\" section_code=\"{$course->section_code}\"  >".$course->course_code." [".$course->section_code."] (<i>".$course->mother_code.")</a>";
								} else {
									echo "<a class=\"enroll_slot\" href=\"{$course->id}\" courses_id=\"{$course->courses_id}\" max_enrolee=\"{$course->max_students}\"  parallel_no=\"{$course->parallel_no}\" advised=\"{$course->advised}\" elective=\"{$course->elective}\" course_code=\"{$course->course_code}\" section_code=\"{$course->section_code}\"  >".$course->course_code." [".$course->section_code."]</a>";
								}								
							} else {
								if ($course->elective == 'Y') {
									echo $course->course_code." [".$course->section_code."] (<i>".$course->mother_code.")</i>";	
								} else {						
									echo $course->course_code." [".$course->section_code."]";	
								}	
							}
	?>
						</td>
				     	<td style="text-align:center; vertical-align:middle; ">
				     		<?php 
							  	if ($course->is_bracketed == 'Y') {
									echo "(".$course->credit_units.")"; 
								} else {
									echo $course->credit_units; 				
								}
							?>
						</td>
					     	<?php
								$offering_slots = $this->Offerings_Model->ListOfferingSlots($course->id);
							?>
						<td style="text-align:left; vertical-align:middle;">
<?php
								if ($offering_slots){ 
									foreach($offering_slots[0] AS $slots) 
										echo $slots->tym." ".$slots->days_day_code."<br>";
								}else{
									echo "Error! Please send feedback to concerns.";
								}
?> 
						</td>
						<td style="text-align:center; vertical-align:middle;">
<?php 
								if ($offering_slots){									
									foreach($offering_slots[0] AS $slots) 
										echo $slots->room_no."<br>";
								}else{
									echo "Error! Please send feedback to concerns.";	
								}
?>
						</td>
					<td style="text-align:center; vertical-align: middle;">
<?php //echo site_url($this->uri->segment(1).'/list_enrolled_students');?>
<?php 					
					if ($course->enrollee_parallel) { 
						if($course->enrollee_parallel > 0) {
?> 
			 				<a href="#" data-toggle="modal" data-target=#modal_classlist1_<?php echo $course->id;?> data-project-id=""><?php echo str_pad($course->enrollee_parallel,2,'0',STR_PAD_LEFT);?></a>/<?php echo $course->max_students;?>
<?php 									
						} else { 
							echo str_pad($course->enrollee_parallel,2,'0',STR_PAD_LEFT);  
							echo "/" . $course->max_students;
						}
						
						
?>
					<div class="modal hide fade" id="modal_classlist1_<?php print($course->id); ?>" offer_id="<?php print($id); ?>" style="width:750px; margin-left: -375px;">
					   <div class="modal-header">
					       <button type="button" class="close" data-dismiss="modal">×</button>
					       <h3>List of Students</h3>
					   </div>
						<div class="modal-body">            
							<div style="text-align:left; margin-top:0px; font-size:13px; font-weight:bold;">
									<?php print($course->course_code." [".$course->section_code."]<br>"); ?>
									<?php print($course->descriptive_title.'<br>'); 
										if ($offering_slots) {
											foreach($offering_slots[0] AS $slots) {
	            								print($slots->tym.' '.$slots->days_day_code.' ['.$slots->room_no.']<br>'); 
			  								}
										}
									?>
							</div>
	
							<div id="modalContent_<?php print($course->id); ?>" >
						    	   text here
							</div>
						</div>
					   <div class="modal-footer">
					       <a href="#" class="btn" data-dismiss="modal">Close</a>
					   </div>
					</div>

					<script>
					$('#modal_classlist1_<?php print($course->id); ?>').on('show', function(){
						  var offer_id = $(this).attr('offer_id');
						  $('#modalContent_<?php print($course->id); ?>').html('loading...')
					
						  $.ajax({
						      cache: false,
						      type: 'GET',
						      url: '<?php echo site_url($this->uri->segment(1).'/list_enrolled_students');?>',
						      data: {offer_id: <?php print($course->id); ?>,parallel_no: <?php print($parallel_no); ?>,num: <?php print("1"); ?>},
						      success: function(data) {
						        $('#modalContent_<?php print($course->id); ?>').html(data); //this part to pass the var
						      }
						  });
						})
					</script>						

<?php 
						
				} else {

					if($course->enrollee > 0){
?> 
			 				<a href="#" data-toggle="modal" data-target=#modal_classlist2_<?php echo $course->id;?> data-project-id=""><?php echo str_pad($course->enrollee,2,'0',STR_PAD_LEFT);?></a>/<?php echo str_pad($course->max_students,2,'0',STR_PAD_LEFT); ?>

					<div class="modal hide fade" id="modal_classlist2_<?php print($course->id); ?>" 
						offer_id="<?php print($id); ?>" style="width:750px; margin-left: -375px;">
						<div class="modal-header">
					       <button type="button" class="close" data-dismiss="modal">×</button>
					       <h3>List of Students</h3>
						</div>
						<div class="modal-body">            
							<div style="text-align:left; margin-top:0px; font-size:13px; font-weight:bold;">
								<?php print($course->course_code." [".$course->section_code."]<br>"); ?>
								<?php print($course->descriptive_title.'<br>'); 
									if ($offering_slots) {
										foreach($offering_slots[0] AS $slots) {
	           								print($slots->tym.' '.$slots->days_day_code.' ['.$slots->room_no.']<br>'); 
		  								}
									}
								?>
							</div>
	
							<div id="modalContent_<?php print($course->id); ?>" >
							       text here
							</div>
						</div>
						<div class="modal-footer">
						    <a href="#" class="btn" data-dismiss="modal">Close</a>
						</div>
					</div>

					<script>
					$('#modal_classlist2_<?php print($course->id); ?>').on('show', function(){
						  var offer_id = $(this).attr('offer_id');
						  $('#modalContent_<?php print($course->id); ?>').html('loading...')
					
						  $.ajax({
						      cache: false,
						      type: 'GET',
						      url: '<?php echo site_url($this->uri->segment(1).'/list_enrolled_students');?>',
						      data: {offer_id: <?php print($course->id); ?>,parallel_no: <?php print($parallel_no); ?>,num: <?php print("2"); ?>},
						      success: function(data) {
						        $('#modalContent_<?php print($course->id); ?>').html(data); //this part to pass the var
						      }
						  });
						})
					</script>						

<?php 	
					} else { 
							echo str_pad($course->enrollee,2,'0',STR_PAD_LEFT) . "/" . str_pad($course->max_students,2,'0',STR_PAD_LEFT);
					}
				} 
?>
					</td>
			</tr>

   
<?php
					} //end of foreach ($courses AS $course)
						
				} // end of if ($courses)
?>
				
			</tbody>
	
			<form id="enroll_form" action="<?php echo site_url("student/{$this->uri->segment(2)}")?>" method="post">
				<input type="hidden" name="step" value="2" />
				<?php $this->common->hidden_input_nonce(FALSE); ?>
			</form>
			
		</table> <!-- end of COTable -->
	
	</div> <!-- end of div id=course-offerings -->

</div> <!-- end of main div -->


<script>
	$('.enroll_slot').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Enroll in ' + $(this).attr('course_code') + ' [' + $(this).attr('section_code') + '] ?');

		if (confirmed) {
			var course_offerings_id = $(this).attr('href');
			var courses_id = $(this).attr('courses_id');
			var max_enrolee = $(this).attr('max_enrolee');
			var parallel_no = $(this).attr('parallel_no');
			var advised = $(this).attr('advised');
			var elective = $(this).attr('elective');
			console.log(courses_id);
			$('<input>').attr({
				type: 'hidden',
				name: 'course_offerings_id',
				value: course_offerings_id,
			}).appendTo('#enroll_form');
			$('<input>').attr({
				type: 'hidden',
				name: 'courses_id',
				value: courses_id,
			}).appendTo('#enroll_form');	
			$('<input>').attr({
				type: 'hidden',
				name: 'max_enrolee',
				value: max_enrolee,
			}).appendTo('#enroll_form');	
			$('<input>').attr({
				type: 'hidden',
				name: 'parallel_no',
				value: parallel_no,
			}).appendTo('#enroll_form');	
			$('<input>').attr({
				type: 'hidden',
				name: 'advised',
				value: advised,
			}).appendTo('#enroll_form');	
			$('<input>').attr({
				type: 'hidden',
				name: 'elective',
				value: elective,
			}).appendTo('#enroll_form');	
			$('#enroll_form').submit();
		}
		
	});
</script>


  <form id="cancel_form" action="<?php echo site_url("student/{$this->uri->segment(2)}")?>" method="post">
  <input type="hidden" name="step" value="3" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>

<script>
	$('.cancel_enroll').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete ' + $(this).attr('course_code') + ' [' + $(this).attr('section_code') + ']?');

		if (confirmed){
			var enrollments_id = $(this).attr('href');
			
			$('<input>').attr({
				type: 'hidden',
				name: 'enrollments_id',
				value: enrollments_id,
			}).appendTo('#cancel_form');
			$('#cancel_form').submit();
		}
		
	});


	$(document).ready(function(){
		
		<?php if ($courses) { ?>		
			var dt = $('#COTable').dataTable({
					"iDisplayLength": -1,
					"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],				
					"aoColumns": [ 
									{"bSearchable": true}, 
									{"bSearchable": true}, 
									{"bSearchable": true},
									{"bSearchable": true}, 
									{"bSearchable": false},
								],
				});
			
		<?php } ?>

		<?php if ($grades != null) { ?>		
			var dt2 = $('#mysked').dataTable({
				"iDisplayLength": -1,
				"aLengthMenu": [[5,10, -1], [5,10, "All"]],
				});
			dt2.fnSort( [ [1,'asc'] ] );
			
		<?php } ?>		
		
	});
</script>
 