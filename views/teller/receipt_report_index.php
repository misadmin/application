<h2 class="heading">Receipt Report</h2>
<form action="" class="form-horizontal" id="rsi" method="post">
	<input type="hidden" name="nonce" value="<?php echo $this->common->nonce() ?>" />
	<input type="hidden" name="submitted" value="1" />
	<fieldset>
		<div class="control-group input-append">
			<label class="control-label"  for="date">Date</label>
			<div class="controls date" id="rsi-date" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-d') ?>">
			      <input type="text" id="rsi-date-input" name="date" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d')?>" readonly="">
				 <span class="add-on"><i class="icon-calendar"></i></span>
			</div>
		</div>
		<div class="control-group formSep">
			<label for="payment_methods" class="control-label">Payment Method: </label>
			<div class="controls">
				<select id="payment_methods" name="payment_methods">
					<option value="All">All</option>
				<?php foreach ($this->config->item('payment_methods') as $key=>$val): ?>
					<option value="<?php echo $val; ?>"><?php echo $val; ?></option>
<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-actions"><button type="submit" class="btn btn-success">Show!</button></div>
	</fieldset>
</form>
<script>
$().ready(function() {
	$('#rsi-date').datepicker();
});
</script>