
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Subject</div>
			<div class="col2a">
				<input type="text" class="form-control" id="descriptive_title_room_offering_modal" style="width:300px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Time</div>
			<div class="col2a">
				<input type="time" id="start_time_change_modal" style="width:auto;" /> - 
				<input type="time" id="end_time_change_modal" style="width:auto;" />
			</div>
		</div>
		<div style="width:100%;">
			<div class="col1">Day</div>
			<div class="col2a" style="font-size:12px;">
				<div style="float:left; padding-top:3px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Mon" class="day_change_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Monday
					</div>
				</div>
				<div style="float:left; padding-top:3px; padding-left:8px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Tue" class="day_change_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Tuesday
					</div>
				</div>
				<div style="float:left; padding-top:3px; padding-left:8px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Wed" class="day_change_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Wednesday
					</div>
				</div>
				<div style="float:left; padding-top:3px; padding-left:8px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Thu" class="day_change_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Thursday
					</div>
				</div>
				<div style="float:left; padding-top:3px; padding-left:8px;">
					<div style="float:left;">
						<input type="checkbox" name="day_code2" value="Fri" class="day_change_modal" />
					</div>
					<div style="float:left; padding-top:3px; padding-left:2px;"> 
						Friday
					</div>
				</div>
			</div>
		</div>
		<div style="width:100%; clear:both;" id="vacant_rooms_for_offering1">
		
		</div>
	</div>

<script>
	$("#start_time_change_modal").blur(function(){
		
		var timeElements = $('#start_time_change_modal').val().split(":");    
		var theHour = parseInt(timeElements[0]);
		var theMintute = timeElements[1];
		var newHour = theHour + 1;
		
		if (newHour < 10) {
			var newHour1 = "0" + newHour;
		} else {
			var newHour1 = newHour;			
		}

		$('#end_time_change_modal').val(newHour1 + ":" + theMintute);
		
	});
</script>