<?php 

/**
 * Form Library v2.0
 * 
 * Creates a form automatically with validations where you save time and effort :)
 * 
 * This Library require you to include this following files:
 *  
 *   Javascripts:
 *        Jquery
 *        Jquery.validate
 *        Twitter Bootstrap
 *        
 *   CSSs:
 *        Twitter Bootstrap
 *        
 * Updates: 
 * 4/15/2013 : 
 * - added id parameter for form elements
 * - removed br tags
 * 
 * @author Kevin Felisilda 6128492
 * @author Aljun Bajao 6247912
 *
 */
class Form_lib implements Form_lib_Interface {
	
	/**
	 * holds the id control
	 * @var String
	 */
	public $form_id;
	
	/**
	 * holds the attribute controls
	 * @var Array
	 */
	private $attributes;
	
	/**
	 * holds the input controls
	 * 
	 * @var Multi Array
	 */
	private $inputs;
	
	/**
	 *  holds the hidden input controls
	 * @var Array
	 */
	private $hidden_inputs;
	
	/**
	 *  holds the rules
	 * @var Array
	 */
	private $rules;
	
	/**
	 * self explanatory
	 * @var String
	 */
	private $reset_label;
	
	/**
	 * self explanatory
	 * @var Boolean
	 */
	private $reset_enabled;
	
	/**
	 * holds the params for submit button
	 * @var String
	 */
	private $submit;
	
	/**
	 * class for every control group
	 * 
	 * @var String
	 */
	private $control_group_class;
	
	/**
	 * Extra scripts by the user
	 * @var Array;
	 */
	private $scripts;
	
	/**
	 * constructor
	 * @param unknown_type $form_attributes
	 */
	public function __construct($form_attributes=NULL){
		if (!is_null($form_attributes))
			$this->set_attributes($form_attributes);
		else
			$this->attributes = array();
		
		$this->clear_form();
	}
	
	/**
	 * Clears the form elements
	 */
	public function clear_form(){
		$this->form_id = '';
		$this->inputs = array();
		$this->hidden_inputs = array();
		$this->rules = array();
		$this->scripts = array();
		
		$this->submit = array('enabled'=>FALSE,'label'=>'Submit','class'=>'');
		$this->reset = array('enabled'=>FALSE,'label'=>'Reset','class'=>'');
		
		$this->form_id .= substr(str_shuffle('qwertyuiopasdfghjklzxcvbnm'),0,7);
	}
	
	/**
	 * sets the id in the form
	 * @param string $form_id
	 */
	public function set_id($form_id){
		$this->form_id = $form_id;
	}
	
	/**
	 * sets all the attributes in the form
	 * @param string $form_attributes
	 */
	public function set_attributes($form_attributes){
		$this->attributes = $form_attributes;
	}
	
	/**
	 * Enqueue Hidden Input to the form
	 * <code>bool enqueue_hidden_input($name [,$value])</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->form_lib->enqueue_hidden_input('nonce',$this->common->nonce());
	 * </pre>
	 * @param  string $name
	 * @param  string $value
	 * @param  string $attributes
	 * @return boolean
	 */
	public function enqueue_hidden_input($name, $value='', $attributes=array(), $id=''){
		if (empty($name)){
			return FALSE;
		}
		$input = array();
		$input['type'] = 'hidden';
		$input['name'] = $name;
		$input['value'] = $value;
		$input['attributes'] = $this->extract_attributes($attributes);
		$input['id'] = (!empty($id) ? $id : $id);
		
		array_push($this->hidden_inputs, $input);
	}
	
	/**
	 * Enqueue Password Input to the form
	 * 
	 * <code>bool enqueue_password_input (mixed $data [, String $label='' [, $placeholder='' [, $value='' [, $required=FALSE [, $class [, $rules='' [,$prepend ='' [, $append='']]]]]])</code>
	 * 
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * //inline method                         //name            //label            //placeholder //value  //is required //class  //rule   //appaned  //prepend
	 * $this->form_lib->enqueue_password_input('password',       'Password',         '*******',    '',      TRUE,        ,''    'minlength:3');
	 * $this->form_lib->enqueue_password_input('passwordconfirm','Confirm Password', '******',     '',      FALSE,       ,''    'equalTo:password');
	 * 
	 * //array method
	 * $password_input = array('name' => 'password',
	 *                         'label' => 'Password',
	 *                         'rules' => array('required','minlength:3'),
	 *                         'placeholder' => '******'
	 *                        );
	 * $password_confirm = array('name' => 'passwordconfirm',
	 *                         'label' => 'Confirm Password'',
	 *                         'rules' => array('equalTo:password'),
	 *                         'placeholder' => '******'
	 *                        );
	 * $this->form_lib->enqueue_password_input($password_input);
	 * $this->form_lib->enqueue_password_input($password_confirm);
	 * </pre>
	 * 
	 * Screenshoot
	 * <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA6EAAABsCAIAAAAUpyOKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAB1OSURBVHhe7Z3dbhzHlcf1BMkbJIAfxNeG7/LhmwCJ+QALR4qRINiLeMGrBbI3SwFWtJdxhL3hjRZiAAEWJOzGCBFs5DCxJMPCSrFixRIpSqRCkSI5nNlTXd1d1V8z/VXd1T2/QUMY9nRVnfrXqepfnz7dOjfjgwIogAKOFTiX/DhujepRAAVQAAVQYHYODVAABVDAtQIwrmuFqR8FUAAFUCClAIyLS6AACjhXAMZ1LjENoAAKoAAKJBWAcfEIFEAB5wrAuM4lpgEUQAEUQAEYFx9AARToWAEYt2PBaQ4FUAAFUIA4Lj6AAijgXAEY17nENIACKIACKEAcd1g+MJ3O2FCgRwVamS8wbisyUgkKoAAKoEB5BYjjltequyNDoJnNzqZsKNC/AtNZeKFVew7AuLWloyAKoAAKoEA9BWDcerq5KqXoNkLbyXR2eqa2k8n05Gx2PJmyoUBnCojLieMFHjgVV9SXWxp2a3xg3BqiUQQFUAAFUKCJAjBuE/VaLhsDrlCF4IXQzNHp9PBk+upkeiDb8Vm46T/ZUMCFApabieOJ+4kTiisGvDutjbkwbsuLBdWhAAqgAAosUgDGXaRQh7/rCK5EzgQphC3+cTzdf3324ujs+eHZM9lesaFARwrsiLMdKscT9xMnlAsqcUhxS3FOjblVPzBuVcU4HgVQAAVQoKECMG5DAVsrLkFcDbivT1XIdu+1goynB2d/fzn52/7kyz21/fUFGwp0oYD2t6/2J+J+4oTiivuv1a0Dcc4QcytyLozb2kpBRSiAAiiAAuUUgHHL6eT+KGEGyXqUUJmQhATPtg/OhDAevph88ez07s7JZ9uynf7l6Ylsf2ZDAWcKaB/7TLbtk3s7p/d3T8UJ5Srr6cFk70ilzRxPlKNWRNwZjOt+CaEFFEABFECBhAIwri8OIUFcSXmUO8ISwRXA/XLv9MaDo1/+fv/968/ZUKBHBcQJf/fo9aMAcyWaK+m5OpRb6QPjVpKLg1EABVAABZorAOM217CFGnSigkTIJIgr94UlgiuAe/EPL+4/3dne22ZDgR4VECcUVxSH/OrlZPdQ5eaKo6qs3CqYC+O2sEx0W8XJy5f3fnXp1rs/uv72W2wo0KMC4oT3f/NRt+5PayNRAMb1YiAFF+T+r0TI5PkeSX+Uu8P/cmvv/57tHpwcHE4O2VCgRwXECcUVJZr78MVpEMo9ez1RbxODcb1YO5wZcffSh5/89CeP7t/p8fqKplFAFBAnFFcUh3Tm7FQ8WgVgXC+GViJicv9XGFcycR+/VDm4cm9a2OJ4dsyGAr0rIK4oDim5ueKc4qI6XaFKGJd8XC/WmUpGfPz97z5+8DmX2T1eXtK0VkCcUFxRormVHJiDUUAUgHG9cAPNuIenU3lbkyQqyLM+ghQyt3uHGwxAAVFAXFEcUtxSHj4TFxVHhXG9WDhcGiH3prnMZvp7ooC4ojikS3+n7nEqAON6Ma6aceWhdcl3lId77mzDuASwPVJAM664pTinuKg4KozrxcLh0ghBCi6zPSE8zBBXhHFdTvfR1g3jejG0MePKA2fyXlJ5cxNxXJZ1fxTQjCtuKc4pLgrjerFqODYCxvVnAmIJjOt4uo+2ehjXi6FNMy5xXBKRfVIgZNztUxjXi/WiEyNgXMjSHwVg3E4m/QgbgXG9GFQY15/FFEuyCsC4XiwT3RoB47IU+KMAjNvt7B9PazCuF2MJ4/qzmGIJjOvFotC3ETAuS4E/CsC4fa8HQ21/gIy7vb7yRuqzsr7t1QDcXn2jmkkwrj+LKZbAuF6tJn0ZA+OyFPijwDzGfXLtpvlvSs5vPZnNZM+Fa7v9zJytzaZNZ2po1J0cex5cDP9Xl82tRRo1arqo8uYSLTLb+n2gjLt62+6jIGVFpqygUJ1DYVyP3gngzzI9XEvIVUgsA4lzqj5bBGdWjz5bm41NgnGHO2HHZ/kCxo2xcmvtunx3QmblpnfzprM1NKkzW1YkuqjZVlaJtQfzu9Wk6aKaXdRZ3ItRMO5MhXaT2FvOHV0dBePCuKNSAMbNMG7y3CCnjcZM2epqBOOOagKOj1mr9qgs42p+iilKTczgKjSkutnuxvnknidbF8Kg5s2N6Do1U8rMzdRP0tDFtU3TRFSbZu7swRfWNlVzidUjaVKyBt1w/e7k1WZ1JodxzQV8YKRqWttcrKE5RhXJKBxUEgba5YDyEuUNTfVVcnSMq2K60SfGXrPTSiFI77RAOUiHCEvHvGpyJKJKZM/K6qrKnAiOjStcXSVXoeoSxvE+KwDjLmDcYOFefOOv+gJdtwSMC+OOSoGyjGvHca14odydV9PT7IlCmNmgZrZUPAlzKwyBNaowPib/4MwNn6xJRXHcxt2xFxNNopkly6wb6gC5MFB4qm3OdNDeE1/hZ7ujDgt7berU1wDzJaoUbx55HFfBpSZS8y34qmHUiqqarzk7DeTeXl2RT1BYcaz6kqo5aM5GYeuAYLerfNw//cc76vPPv/0qeLfU/D99xiZsG5ACMG4Fxo2DN1bkw0R07HCvOVKfAyxQDiIf4RlIn7b1KSEMh0RnymQExYobSWypafoEuQoDmqGjN7VOPq49E99+KwzTpiZR/GcU6LWmqgpemuCuHZcNpqH6yVCaTN5kKnC29aJ79FmTUhm9umC97sxNDAjRPw/iw33ZDmYXolQTJbuzQE9ruYuHptYV/0DjuGWeOYsp1sJTo1HeTsOzK+u311cUx0bcm8iGiGpWOyOWTaQnuMtV+NOlAHHfeefnv30qjDv/z1Fdx49+Efe5gzDuAsZVpx99/1GCFhabmhBIRJwxsFrhDXVaDc5qYZwjWP1vXjivz6+yM/iSqjm6k2iCMeaAIEjjinHv6wdWLlx9Glxmp/702Y2xbbgKlI3j6okaQ2EKjxLz1JogEZapq0oTPswgVfan+Yybaj0XN7MmFcVxs62X6c5cxo3WlqinRU0HmoYQP9/g8t3RjLtQIntololxi5NvE29diIOpJi/BKpndqdlUqpCj5Hv8byIWnAu+Nu6mji4zLuXfq0Acd7jL9HAth3EzjBvm8IW5fflAGd+ks/DUVJS3MzrHPLh4fmtLUv3U7dUwC0Kt9XEaX1SzuZMYnDBM+MddrsL//j6MJb/36RNh3NSfXFejgBMF6jCumR3RJIqhKkqgN1eVs5kENcPQbDidMwlI2QrnMO68g63lJGNSzkshdCu1u5OKCltkmY7jZi+8sx2cb3D21zl1zpUoZ2jKsFTmmIHGcQsYN0GaecHURSFdBbbrcXbCyvp6ALryKYzjRpZ0FMd1snwMl72wvBsFYNwFcVz7Z3Orzn7fgiJaDcRWDlx2p2ZTObkKzuoEOCtx0JyrcsBXnRK6YNx04JY4bjdzcMlbqcO49t1w8yYB/fiUelBMz8T4RVpm+lR65iycdFGYM7jfElaV88xZ9o1m0SJgTLJqsMPSNbuTqc3qciITI27LPB9mp8wmO2hrmIj+ZruTeuYsvNlVRqLM0JR4EcTIGTeVNKsTCVIZBRpac3fK/lQmbpSVW5yPm3iuzfCws3zcJV/p6H4vCsC4ZRnXDqzaAYy4vLmXl4zlRDFaFVnZCN/uqaK5G9HtvMI4bhTc7SiOy2U2CvSgAP8HRK0gJoVm44rj6qfAgk+QUWu9GyHeHf9vEbkvW0i9UcFG1dz3KtivLIsOCMK/rp456wVxaHTJFYBxyzKuhbBBECJ6DCVOZogPSKUZxHkIqUzcKCu3OB/XTmAIQ1NO83GXfC7Q/V4UgHHB1XoKDJBx63XU71Ll83F7WV9odMkVgHHLMm789kf12PWWeaFY+hUKQX25O4NUB/NGBTvTN/XAstSQCO6ae4I3N67xXoUln7Mj6z6M6zfC+GsdjOvF2MC4I1uRR9YdGNeLZaJbI3h32Mhm8aC7A+N2O/vH0xqM68VYwriDXn9HbzyM68Uy0a0RMO7o5/WAOgjjdjv7x9MajOvFWKYZ9+nJ+9efy6we0BqEqSNWIGTcpydf7k2eHZ69Opmens3Eact/ziU/5QtyZF8KwLgjntGD6xqM29c6MPR2YVwvRjBm3N3Ds0f7kzvbpzDu4FbhERusGVfcUpxTXBTG9WLVcGwEjDviGT24rsG4jqf7aKuHcb0YWs24h6fT54dnX+1P7u3AuD28nmZw635nBmvGFbf82/5EXFQclTiuFwuHSyNg3M7mFw0tVADGdTnXx1w3jOvF6GrGPTqdvjg6e/xycn8XxoVxPVJAM664pTjn3tFUHBXG9WLhcGkEjLsQvDigMwVgXJdzfcx1w7hejO50OptMFePuvz57enD28MXkg1vPd49edbaC0BAKzFHg8T9e/fKT/YcvTp8eTF4eT19PpuKu4rTlP+TjltfKkyNhXNYEfxSAcT1ZFgZnBozrxZAJLpxNZ8eT2cHJVPId5Y7w/zw6uviHva8Pj/xZZbBkORUQJxRXFIf86qVKxj04PjueTMVdYVwv1g5nRnz8ve/s7+8sp8/Ta98U2H388Na7P3Tm7FQ8WgVgXF+GVqDhZDI9PJnuvT7bPlBPnt14cCTBM7lHzIYCPSogTiiAK9ddEsTdfx0mKoi7VvoQx60klw8H3//o15s/e3/vySPfcAd7lk0BcUJxRXFIH+YFNgxLARjXl/ESZpD7vxIhk1CuZOUK5gpVSNLCF89O7+6cfLYt2+lfnp7I9mc2FHCmgPaxz2TbPpGHzCQHV1IUNOBKJq68UUHuNkzOKsVw1RSDcX1ZaKrYcffShxI8k6QFNhToUQFxQgC3ysTlWKMAjOuLN+h0BXmU5/WpwlyJ5sqLSCU39+8vJ0IY8l5S2f76gg0FulBA+5u84kMeMhO6FVeUCK64pTinuGjVRAUY15dVBjtQAAVQYJkUgHE9Gm0J5WrMlWiuJC0IUsgjaBLTlbc1CWQ8e8WGAh0psCPOdqgcT9xPnFBcURxS3DIE3OqThjhudc0ogQIogAIo0EgBGLeRfO0WllBuhLlTyc0VpJA3LQhbyA1igQx51ifc9J9sKOBCAcvNxPHE/cQJxRXFIU/PgkfNajk9jFtLNgqhAAqgAArUVwDGra+di5Ix5gpMSHquhM1kE7w4CYK7bCjQmQLicgHXigeqN4WJQ2rArZyKG8wTGNfFckGdKIACKIACcxSAcX10D0W6UUxXswUbCvSogEbbenSrJxiM6+NCg00ogAIoMGoFYFzfhzfkXU29bCjQuQKtzBAYtxUZqQQFUAAFUKC8AjBuea04EgVQoKYCMG5N4SiGAiiAAihQVwEYt65ylEMBFCitAIxbWioORAEUQAEUaEcBGLcdHakFBVBgjgIwLu6BAiiAAijQsQIwbseC0xwKLKMCMO4yjjp9RgEUQIFeFYBxe5WfxlFgORSAcZdjnOklCqAACnikAIzr0WBgCgqMVQEYd6wjS79QAAVQwFsFYFxvhwbDUGA8CsC44xlLeoICKIACA1EAxh3IQGEmCgxZARh3yKOH7SiAAigwSAVg3EEOG0ajwLAUgHGHNV5YiwIogAIjUADGHcEg0gUU8F0BGNf3EcI+FEABFBidAjDu6IaUDqGAfwrAuP6NCRahAAqgwMgVgHFHPsB0DwV8UADG9WEUsAEFUAAFlkoBGNfT4X5+9PzHN3787cvfPvdv59hQoEcFxAk/+OSDhvMExm0oIMVRAAVQAAWqKgDjVlWso+Pfu/Hem1fe/OOjP27vbbOhQI8KiBOKK4pDNnF9GLeJer2UPXn58t6vLt1690fX336LDQV6VECc8P5vPuplFtDo0BWAcT0dwW+uffPTx58enBwcTg7ZUKBHBcQJxRUlmttkqsC4TdTrpezdSx9+8tOfPLp/p8frK5pGAVFAnFBcURyyl4lAo4NWAMb1dPjk3rSwxfHsmA0FeldAXFEcsslUgXGbqNdL2Y+//93HDz7nMrvHy0ua1gqIE4orSjS3l4lAo4NWoNF5a9A999x4QQqZ273DDQaggCggrgjjer5itG6e3JvmMpvp74kC4orikK07ORWOXgEY19MhhnE9WVsxA8b1dI1wbJYgBZfZTH9PFBBXhHEdz/hxVg/jejquMK4naytmwLierhGOzYJxmfv+KADjOp7uo60exvV0aGFcf5ZXLCFXwdNlwqVZMC4T3x8FYFyXc33MdcO4no4ujOvP8oolMK6ny4RLs2BcJr4/CsC4Luf6mOuGcT0dXRjXn+UVS2BcT5cJl2bBuEx8fxSAcV3O9THXXZpxt9dX3og+q7drSHJ7NSj+g5//4I1a5eMmbUtCi1bWt2tY5K6I9LWpScWM+/WtK1fvmHeKtfgn7ylDgXwFYFx3i4W3NcO4/hAelsxj3CfXbpr/puT81pPZTPZcuLbbz9Ta2qzWdHR8nzZXUaozO1tqqBzjKj6NwTRgzMqY2gL2heOg2k82r8xrypRVBnnhsS10Np9xP796+crVq/919eqVy1f+++vjFv/kRbwoUKyAF4xrn8kubi2chNkDHlwM/r+uf/rXG2+vPahRPi6SOKfq/wMsOLN69NnabGxSMeO2eF2dqoqLTBQovMwufK+CDUNba9cFMVvCozoTumrT8fFVC9Yxro0yQ7Ez6msZxk0QriqoKLMiU2bBtLbaOVW1WHtts+yCzhhXKGTn1pXLl69+Hi0ELf4J5KFAgQL9M66cugyYPtm68Nb1ypjbAvaFk1wxbpKSlXleYW4Lnc1n3Bavq1NVMftQYO5ldinG1QQWc5iamMFVaLRc7G6cT+4JFpPgmJsb0XVqppQ5u6d+koYurm2aJqLadCg3e/CFtU3VXLx6WMcrm/Wv1uJWaEmO2emuWRXe3NjS3Qz7WFCt1BCLEH/Pq9a+ihBLoqWvWLeM7LE4a1sb5zeDkEVhQwmFKxNaCcbNIG6iEZM5EFFvAJzrUWqD2mtlF6zeiKKwsnNldVUlQEhQNlkm3BEkImQDxvMZN0yJSJY1Oy00T++06rVD1TGv5vbUdGE2iytcXXWWq3Dn6mVJVJB/r9zaEcxt8U/iByhQqEDfjCvE9pZeBy3KrMiUWTCtvFraraciweqUk7CwduXtFHTGuFxmQ6J9KLAgVyFOD7DjuFbEUe7hqOlp9sgECaawHB/ib7QnW8pedqKGTIUhsGaK57aevRK247jhryUsmWe2VVxXqJY+K4WjVAcjcs0qlrqKmGWPnIXi5OgW2TaLF6hobZ/TUErhiitkOcYtDNpa/Bt/DVgwLJHYGeBqTJKJlId0mbB8bsA4P1dBs3DKHm2FFVU1X3N2mopvr67IJyisUFx9KexpBOHmALszFYfDHM4zZ6Sg+aNAz4ybCOJmppTJHIioN8BZCQ+Y8IyVXbB5K4rCmjiHnO0CSN2IEvvktBcXyQaMc3DZYtw4mJEbj7FPcuZIbblVSdB6CM36tK1+j/MOrZ7akSETR5HYUsVrgIyuBbkKLV5Xp6riIhMF5l1mz4vj5ubj2jMxDmGmJlF2mueW0rMj+5MhswzqzTs4ebkez+4QoKOq5liSuzplu5aqWVs7p9qZ4GlEw/G6l1tttuNzq02vXbmcXbKhilDVjHETuBlRow2mCaLNMm7MznZFRd+jnpV95iym2NxAdN5Ow7Mr67fXV5S9kTELe5pIT3CZq9DHNbQ/sIUlvSjQP+MWPsZhhXhjFA7WSuuuXBCwicE08SUOvgb3/sJWVJ0WVmZgMcu4hsJT9uiyVlQ1Bta8nepunT61bK3dvBDeN4xuGhb2NIofmwOCu36OGBcOQ4EeFCgbx9WYEMNc6gI1MU+tCRLRlZpKJkSagansT/MZN9W6BXamajuOm2Xc+RlZKbNNvDP54F2qiTkdjALbJhCbVSwdx40Wt/m6FdmmqDpYhBc3ZCVFVMHccoxb9IhZguXycHAB4yaeY4v+KMO4xY+8JQg4ZmiTl2CVzO7U/ZEq5Cj5Hv+biAXngm8y4gzj9rAI9gJ/S9Kov4ybwM1otVU7EzHdYsaNz3N2skHR92hltaLCYW5fPlBm7sclluZMAkZ8btbRlC1J9VO3V8MsiIU9NfScpOoq5wP7WN6rsCSzexDdrMO4Zh2IJlHMYYqo1Nw3V5UBbKkL42ypeFZkf5rDuPMOLhfHLbYkx+xM10xiRopx53QwuLzeVLeGolysomoTkYJkRoR9PyqOf0eX7tG1d2ZtnNNQCv0rLmglGNe+TR/WHgVBC6ObGV6dA7uqzqpx3ALGTZBmHmguCukqsJVU4jA7YWV9PQDdlIFx8oNtNXFcAszjVaB/xi16E0IC7PJwMD98m4zsBpEfK6G2DOMWv5khQcAxQwex4WCz0nazO/XSLwZI/Tp3zUocNMHsnJ6q0545wGU+7nj9fBC0t5xG1mFcHZVMPHMWzbjgMSY9E8PXrcS3bnJKWVSVqjDLuGrCRneBCg+2MS06PqeqrP2mYMbsTNeyEeJ4T1qWBDaqmk38uLDa0AAFxAufOcuRPRqatU0dx9W6qfyuaGjSAeM4jhsn9Zai3TKMq58ZS+QVZPJtrYTVXF7thnFTSbPaSht8E/nBUY9s8E1l4kZZucX5uDFtk49L7Ha0CvTMuMHyl3yiy3pSweCvHceNGLRjxrVDyHY2Qrwam1tyqRNnaLC6cydpwQGtqmjuRvRMTGEc14q4wLjw90gV4P+AKAV0wzuoZgZC+Y6WYlxVnbmxn3zXgckNSLxXIfxfIrrOVbCtkYzaKMfCGD/nvQqBaEEFIbeqUtbx83salZUXOgTh34rvVsuMGM+cLWe4ws9e9824+rUyibwCO3fWPJuledfGwY4Z10LYIM6RuotnpZ3ZNGyDbyoTN8rK1XGOeT21DiAf1895hFW1FYBxy1PdEI40t7DMK9vc2F2acd00T61FCsC4tVdDCrauQO+Mq6aJ/dyu/SiGyQ3I5ODavDsHdlXtbeUq2O+tVO+ktKg0879FmB6lH3+xSuU8GWNwPxHcNS9euLlxzdl7FUZ7s6L1WUOFLSoA4wJL9RSAcevp5rwUjNvi+khVDRXwgnGdzzkaSCjAM2cNZw3FW1QAxmV5qqcAjFtPN+elYNwW10eqaqgAjOt8wvvXAIzbcNZQvEUFYFz/VohhWATjejpOMG6L6yNVNVQAxvV0mXBpFozbcNZQvEUFYFyXc33MdcO4no4ujNvi+khVDRWAcT1dJlyaBeM2nDUUb1EBGNflXB9z3TCup6ML47a4PlJVQwVgXE+XCZdmwbgNZw3FW1QAxnU518dcN4zr6ejCuC2uj1TVUAEY19NlwqVZMG7DWUPxFhWAcV3O9THXDeN6Orrf+PdvfP3q6xbXCKpCgdoK3Nu9963L32oyVc4lP02qomw3CsC4tecLBVtXAMbtZtaPrxUY19Mx/cXvfvHmf775xd4XrS8WVIgClRQQJxRXFIdsMlVg3Cbq9VL24+99Z39/p5KrcDAKOFJg9/HDW+/+sJeJQKODVgDG9Xf43rvxngTPJGmBDQV6VECcsCHgyhyDcf1daAosu//Rrzd/9v7ek0eOqIVqUaCkAuKE4orikIObRBjcuwIwbu9DgAEoMH4FYNwhjvHdSx9K8EySFthQoEcFxAkB3CEuID7YDOP6MArYgAIjVwDGHfkA0z0UQAEU8E8BGNe/McEiFBidAjDu6IaUDqEACqCA7wrAuL6PEPahwAgUgHFHMIh0AQVQAAWGpQCMO6zxwloUGKQCMO4ghw2jUQAFUGDICsC4Qx49bEeBgSgA4w5koDATBVAABcajAIw7nrGkJyjgrQIwrrdDg2EogAIoMFYFYNyxjiz9QgGPFIBxPRoMTEEBFECB5VAAxl2OcaaXKNCrAjBur/LTOAqgAAosowIw7jKOOn1GgY4VgHE7FpzmUAAFUAAFYFx8AAVQwLkCMK5ziWkABVAABVAgqQCMi0egAAo4VwDGdS4xDaAACqAACsC4+AAKoEDHCsC4HQtOcyiAAiiAAsRx8QEUQAHnCsC4ziWmARRAARRAAeK4+AAKoEDHCsC4HQtOcyiAAiiAAsRx8QEUQAHnCsC4ziWmARRAARRAgaQC/w8mb68dXifvAgAAAABJRU5ErkJggg==">
	 * <br>
	 * 
	 * @param Assoc Array or String $data
	 * @param String $label
	 * @param String $placeholder
	 * @param String $value
	 * @param Boolean $required
	 * @param String $prepend
	 * @param String $append
	 * @param String $rules
	 * @param String $id the input id
	 * @return Bool returns TRUE if success, FALSE otherwise
	 */
	public function enqueue_password_input($data, $label='', $placeholder='', $value='', $required=FALSE, $class='', $rules='', $prepend='', $append='', $id=''){
		$parameters = get_defined_vars();
		$parameters['type'] = 'password';
		return ($this->enqueue_input($parameters));
	}
	
	/**
	 * Enqueue Text Input to the form
	 * 
	 * <code>bool enqueue_text_input (mixed $data [, String $label='' [, $placeholder='' [, $value='' [, $required=FALSE [, $class='' [, $prepend='' [, $append='' [, $rules=''[, $id=''[, $helper_message='']]]]]]]])</code>
	 * 
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * //inline method                     //id              //label            //placeholder //textbox value  //is required //class  //prepend  //append  //rules //id //helper_message
	 * $this->form_lib->enqueue_text_input('username',       'Username',         'Username',       '',           TRUE           );
	 * $this->form_lib->enqueue_text_input('email',          'Email Address',    'Email Add',      '',           TRUE           );
	 * 
	 * //array method
	 * $password_input = array('name' => 'password',
	 *                         'label' => 'Password',
	 *                         'rules' => array('required','minlength:3'),
	 *                         'placeholder' => '******'
	 *                        );
	 * $this->form_lib->enqueue_password_input($password_input);
	 * </pre>
	 * 
	 * Screenshot:
	 * <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAxIAAACiCAIAAABahGx0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACSDSURBVHhe7Z1Lbx3Hlcf1CexvYAP+FkEAAUE2BiebPLaJuZvBDCBLCZLVRAK4mMwim1CIHW4yQBwii+EsKFALIRZICDbAjWVcmKQke8zYiiUy5EiiKJIyxced06/q6uft7lvdXd33d9EQrlrVVdX/PnXur8853bow5IMCKDBhClyIfibs7DldFEABFKiuwIXqh3IkCqBANxUAm7p53Zg1CqBA+wqATe1fA2aAAg0rADY1LDjDoQAK9EYBsKk3l5ITQYGiCoBNRZWiHQqgAApEFQCbsAgUmDgFwKaJu+ScMAqggCEFwCZDQtINCnRHAbCpO9eKmaIACtilANhk1/VgNijQgAJgUwMiMwQKoEAvFQCbenlZOSkUyFMAbMI+UAAFUKCaAmBTNd04CgU6rADY1OGLx9RRAAVaVQBsalV+BkeBNhQAm9pQnTFRAAX6oADY1IeryDmgQCkFwKZSctEYBVAABZQCYBPGgAITpwDYVO6Sn58P1XZ2NmRDgYYV0C2wnO3S2rwCYJN5TekRBSxXAGwqeoHOz8/Pzs5PT89PTs5evXK347NjNhRoUAExOdf2xAgdUxRiE4ri054CYFN72jMyCrSkANhUSHgXmISTTl++PDk8PHnx4tX+/qvnz9lQoGkF9vfF/MQIxRTFIMUsnZAnn5YUAJtaEp5hUaA9BcCm0doLM52cOMD04sXxs2cvd3ePtrcOHz86fPTN4TdsKNCgAmJyjx8fbW+LEYopnhy8OP32WzFOyGn0Kq6nBdhUj670igIWKwA2jbg4kps7PZUfJwkvvdzdOfj73/e//HLv/v1n6+tP19aefvYZGwo0p8Da2rON9b0H9/c3NwXZBZ7ELB1ykpgT2bo23CzYlKf605dP37397ptzb1747QU2FGhRATHCax9fM+UiwKYRSp6dSSpEbuvlJ2p3ff2v1379Xz+Y+v13v8OGAi0qIEZ45z9/c/D119/u7p4cHEi1EwEnUy6xVD9gU55cl25fujh/8ZOHn+zs7bChQIsKiBGKKYpBllreWY3BpnwZz89OJT0nCRG5uf/r1X//73/9580Hn7V49RkaBUQBMUIxxQ+v/lrMUozTCTidnRpxCHRSSgGwKU+u12df//TRpwevDo5Oj9hQoEUFxAjFFCXmVGp5g03V5JL0h5TfSqhJcnNz3//eV/+7gRNo0fgZ2lNAjFBMUWJOkq2TgJPsc/J0fBpXAGzKk1ySMmKpx8NjNhRoXQExRTFIIy6CaNOIaNPJiSRBpAhXCkokKYMTaN34mYCngJiiGOTegwdH/9gWE3UKw/k0roAZL9z4tBsaUH6lhPFZsShggwJiimBTMytfqkZevXhx9PixlOLKrxROwAb7Zw6igJiiGOSzjQ0xTjFRp7yJT+MKgE0jok14TLyVJQqATY25Rweb9vcPHz16traWik23pt62xCqYxkQp4GPT2pq8C0NMFGxqzCfoA4FNYBMpyG4oADY15iIVNsnrBsCmieISy0/WwyYxS2F6sKkxhxAbCGwCm7oBDZa7swamBzY15iXBpgbsmSEqKAA2NeYEcgYCm8AmsKkbCoBNjXlMsKnCLzqHNKAA2NSYEwCbKkpNSXgDjoAhCioANlVcxuUPA5sK2iTNGlYgE5u2l5an3paSO227PFhbWr6y9ES3f2kW25OyOrYHV5x+VlcyGsc7cdqvDrSOCo1SflmWPhF1QGKG4w1ePtq0szD91sxdfdTknvHmZM/R6di0uzI/t7iuv5UguYd3FqCAaQUswibHR89uxrxYbI89y7j8TIpgE06gYWJgOPUkXU5t05Obl5dvbvsmXw1fRh41EpvKr7iajwCbahZY7x5swlXZowDY1NjSB5vsMXtmoiswMkmXwKbZVTd0dOu6Gw7yicePJ8n+kLH81TVY9UNWs5sKjwazfhAr0slQxnL2X3GGSIs2ZY/i9OxNzL37ivU/HG5ed+d8ZWlw3e05BDXp8/JAsNDbM6qfzBmO50qINpWvbSLaZDqOgmcsogDYNJ6vK3E02FTEIGnTvAKlsWnKhYyhwJALKB5tCKZ4AKT2p+a/QjTxM31CMyHEqJCzmx9MwaacUZxDvInpSOTQktOPHOhlElXPOdiU00/ODEs4g5SmprHp7sxb/md6YUeNp/YG2T3J603PzExL05m7bo5vQf5wP+FhYVdOK7evaFPZ6exwP2HaMDHWGAJViTbdX5zzP/Mru6rWeD3YG2T3hL3mFxfnpeniusthK3ecv8hn/s6WvxrDrubmbqy7O7ecpnfkD/cjO51jg+8+zSTGgnJ6oUBnsEndOyqP5jtor/AiyO7pd4qugxt4N6/6HXDYVXC7PHTj7TeDSg7x/qqqw/8l8H4MomOVdALjYhNOoBcrrnkqGjliaWzyK5kSQRpvgYRLRlshilEUY2klU050yt3/lZYN9HFH9aF4K2sUPc2nr3F37eu9RUDNhaxEtCko3kr0owfe4jMs6RBizc1ikyBLgD3h17D0yfnm/buLOxoLBYc5zOPtDr+5X7XD/BFcPAp3h9+848OxxhCoPDYJsgS0JK5zfmVLsc59B6G2hI28nS7uLLo7ve8+LTkO10Mrh378Bs5Or1sHm4JuXTwKewsbeEeFY+HCeqFAR7BJkCW4j1R3jR7ruPe3Ttg8disZ3Fb6+QLH/Xlo5dCPfxfr7PS6dSP//o+Bi0dhb2GD+FglncB42IQT6MajqSMZxcIGZrDJWw7B/UYkUOTtV2sqEpoKVlFBbMoZJYZNUXrLxabAvSQiYU6MKtpPh7ApVi7uR4iCEvIApnSoiX+P1pv7CBWnomi/AZlFitM1hivpNFXzStgUsI76qY4k9QKX6uwMACv+PVpv7iNUnIr8yJMHXl4USr6njtULaLDQhTU8pe5gUzRi7ztoVUKuOT49wB75Hq039xEqTkUeQgVOP/geKVfXGK6MExgbm3ACkFMtChjBJgkUKcIQRomXN8WwKUyoaUvMqytK3t4oVHLDUZmj6NiU7D+83QpunCLptlhtUxBtSp1n1gzLeINkW7PRJj9KFMmbqTyanrzT+Sbve5Dx08JKWp5PIZqOTeqQSMavmkrlscmPEnl5Mz2YFGTu3ACRxI10vsn7ro4rjk3aUIrMIKfuK9ARbPKjRF66zWcalUfzo/2B49MTdpnf1TPVxbEp+hh28FxRcScwHjbhBGohhobvUuwczgg2SdmQX3OtgrX62ohhk7Ogc0vCpy4vZ7yAIHMUHZuS/YfTm131qp3CInEpJM/AprR+/JLwcIZBjVdxX5DWsjw2OTkzvW7JzadFd/gRIo9qUl9PMBKbIjk2NcCow0y/CiHjvU1aEN77MQ7zcZq/UOm21NcTjMQmPQTlJOyKY1MyWIUX64MCFmGTkz4LMnGeWwlvEDU3o9JtkQiQFurPxya9bjQcMS3C5HSZGm2q6B2LYFOYjscJdP+exE5ISs5qJDZVtHhbDzNbk2TqLCtgk1ZrFPCRT00664S1SRrLqK+j+Cde2lQw2qQzmgmEynrd5foNVWDk31n6lUk664RVSkEGzS9jcrFmJDaFhx87wxXEJpWtiw2BX+u+AjZhk3u3Gr43z4ml+6F+nXXCKiWNdSLxdq08PIlQ4eHezXGxaJOeuUvFtQK+swg24QS6ghp9mifYVGD51t6kCjZFUnH6s2+Rf0h9ki7YORKbwmfkhJjuBm/YHH2YV0seeyavsoo5bwl3USZ4ZE49++ZFnvKepNPrmbSn6tT7M0Occqu//Wfr1v3SJZ2KMr+rJ+n0R/n6EG7pk/urcC5WYVMYNo89+6aHytOfpNMePB6RpNPf+zIIyphGRZvc2FfwJF00JFbYERTBJrmCOIEKZswh4ygwadhUeMk22rAiNjU6x/YG4z9XGWeFc6xZBWzDpvbWZe0jF8Qms9eX3lBgpAJgU+2Lv8AAYFOeSGDTyGVMg8YUAJsKODQzTcCmxqyagUopADaZWeHj9QI2gU3k77qhANg0nq8rcTTYVOq3nMaNKQA2lVjGtTUFm8CmbkBDY47J2oHAptrcYLxjsMnaVTDhEwObGnMCOQOBTWAT2NQNBcCmxjymwqZna2u//+53RPnYr7W8kmrCf785/VYU8LBJzPLw8aNX+/tiqI0tCgZSCoBNYFM3oKEVJ2XVoGBTY47bwaYXL44eP362sZGKTVYZBpOZHAV8bNrYEOMUEwWbGvMJ+kBgE9gENnVDAbCpMRd5fnJycnBwtL299+AB2DQ5UGL/mXrYJGZ59I9tMVEx1MYWBQMRbSpkA6/97rWtwy371xIznAQF7j2598bcG4UMd1SjC9HPqOYT9+/np6cnh4ff7u7ub27Off97z/Z2JsHAOEf7FfjHN1/+8QdTYpZinKdHR2KoE7c4LThhok15F+HqR1cv/uXi53uf27+cmGG/FRAjFFMUgzTiNMCmfBnPz05PX748fvbs8Jtv7vzmP/7n3/7l/7a+6reBcXb2KyBGKKYoBilmeby3d/rtt2KoRhwCnZRSAGwaIdel25fkFl9e4MSGAi0qIEZoipnE4sGmEcv+7EyqRk4OXrzc3X3x9dcfXv31H/9pSpIjbCjQogISZxJmOvj6awk1SYbOKWw6Oyv1e09jIwqATUZkpBMU6JICYNOIq3V+LukPuZuXh5WEnOTmfv/LL/ce3H+2sf50be3pZ5+xoUBzCqytieGJ+UluTkxRmEnM0gk1SYbu/LxLfqcvcwWb+nIlOQ8UKKwA2DRaqrMzqbeVHyeJOUm2TuBJKsQPHz8+fPSN/HSxoUBzCojJPX4s5idGKKYoBukwkxSDE2oavYxraQE21SIrnaKAzQqATYWujpDT6enZq2Opc5IK8ZMXL+Qu/9Xz52wo0LQC+/tifmKEYopikE6cCWYqtIZraQQ21SIrnaKAzQqATUWvjmTrXHiSm3spJXG347NjNhRoUAExOdf2xAgdUxRgIjdXdAHX0g5sqkVWOkUBmxUAm8pdHfmVUpv8aLGhQMMK6BZYznZpbV4BsMm8pvSIApYrADZZfoGYHgqggLUKgE3WXhomhgJ1KQA21aUs/aIACvRdAbCp71eY80OBhAJgE0aBAiiAAtUUAJuq6cZRKNBhBcCmDl88po4CKNCqAmBTq/IzOAq0oQDY1IbqjIkCKNAHBcCmPlxFzgEFSikANpWSi8YogAIooBQAmzAGFJg4BcCmibvknDAKoIAhBcAmQ0LSDQp0RwGwqTvXipmiAArYpQDYZNf1YDYo0IACYFMDIjMECqBALxUAm3p5WTkpFMhTAGzCPlAABVCgmgJgUzXdOAoFOqwA2NThi8fUUQAFWlUAbGpVfgZHgTYUAJvaUJ0xUQAF+qAA2NSHq8g5oEApBcCmUnLRGAVQAAWUAmATxoACE6cA2DRxl5wTRgEUMKQA2JQn5NOXT9+9/e6bc29e+O0FNhRoUQExwmsfXzO06odgU3ElX+3v3/vD+yvv/OzW1NtsKNCiAmKEX/z5g+KmS8uaFACb8oS9dPvSxfmLnzz8ZGdvhw0FWlRAjFBMUQzSiCMAm4rLuPH+ex//8ucPv1hv8eozNAqIAmKEYopikMWtl5Z1KAA25an6+uzrnz769ODVwdHpERsKtKiAGKGYosScjHgBsKm4jB/+5EePNu/jBFo0fob2FBAjFFOUmFNx66VlHQqATXmqSlJGLPV4eMyGAq0rIKYoBmnEC4BNxWWUpAxOoHXjZwKeAmKKYpDFrZeWdShgxgvXMTMb+pRfKWF8ViwK2KCAmCLY1LxbkF8pnIAN9s8cRAExRbCpeScQGxFsGhFtwmPirSxRAGxqxV2CTZbYP9MAm1rxAMlBwSawiRRkNxQAm1pxmmATvGKPAkSbWnECRJtKyE6Szh5/wUzAphJL11xTsImlZ48CYJO5lV29J6JNRJu6EWuxx3O1NROwqbqfG+NIsKktg2fcpAJg0xhL2dihYBPYBDZ1QwGwyZjbK9MR2AS+2KNAJjZtLy3HX8d6ebC2tHxl6Ylu7dIstidlLWwPrjhvdl1dyWgc78RpvzrQOio0SplFmGxbbojEDMcbvG1s2lmYfiv5mblb+aycDt3D1ZfKXQ2H6Um63ZX5ueRncb3yewqcDt3D1ZfUru4vzs3Nr+xm/cavL6b8a+rOblCCPa7Kkpn0FptSPL547dnNyivX6dA9XH2p3NVwmI5NOIHK7o4Dx1BgZLTpyc3Lyze3fXsvxxbBIhl51EhsGmO51XNoD7FpDEjK0bhebBoDknLWTB42bTlsdWNx7sZ6xq842NRzHOw1No0BSTlOoF5swgn0fMVZcr+kT6M8Ns2uuqGjW9fdcJBPPH48SfaHjOWvo8GqH7Wa3VR4NJj1/0uZSCdDQTRn/xVniLRoU/YoTs/exNyFH+t/ONy87s75ytLguttzCGrS5+WBYKG3Z1Q/mTMcj85siDaBTYH3ycEm75/yuAps6rkTB5tK+7qeYRNOYIw4jYUMVGFKpbFpyoWMocBQEH8V2hBM8QBI7U9N5IVo4mf6hGZCiInEdNOwKWcU51hvYjoSObTk9C8HeplENw49Apty+smZYWlXEjnAZmxy40ULQRZP4Epl9BRo3Z0JM3zezoaSdFk3mm5M6E6QxZPIkArmqyiRk2sLPt7OAkm6rTvzXpxp/cbc/J2tcL2p3m4shkm65E4ZYn5x0UkuejMXxvI+2omEE9NSgak7J955VfB34x8ykdjkRtdvBnUb4utVRs/3+9qtqn5L3USSDifQ8xuV8des8R5KY5NfyZQI0nghJbWIcrBJhYLcQ5zolItTX2nZQB93VCeKt7JG0dN8if713iKg5kJWItoUFG8l+tHzlfEZdh+bErVNPhW5lDS9sOOcoctH3ndnt/fN2em3db66OxvCpkRtk09FDjZJidGWQhPvuwNGHos4vLJ433U3qlZpNDZpkSQ5yu8/0pvDVYkhwp0uvfnjDl22c+fgNAhnG9BSOETWuLjLFhToNTa5TlzffIfuxvl91++mD8LbUHUbHdzpOn4zCOA3gU04gRZWgXEQ6VaHZrDJg4bgDiSSX9PDPx7chEGjgDUKYlPOKDFsitJbLjaJE4gl6TRsivbTb2zKStLp1UlZ3xU0Cjc1iU25N5oeFWl0Ev2ufE0AJSOxKQOVHPCKIJTLPak7Q26LFZ4rMNJ4Lgwmpe7EV7ajQK+xKau2Sa/lzPqunIDmUpvAJpxAOwuhW6BjdrZGsEkCRSrOJIwSL29STBMGjbxblGABhvu9ZevczyRrm/JG0bEpTNgF/askneo5km7LwKZkP2GOPjHD7kebxsCmyIN4HcGmyDM4LuiMwCY3ghX7uMEtLVbkRZ6c3lJ3Rp7Riz8EpFJyKnOn4lJ6Ok/fibtsQQGwSXvOWUMolbZzglVNRpvGwCacAIn+SgoYwSYpG/JrrlX4NidJ53BRbkn41OXljBcQZI4SexYvqyR8anbVq3YKi8SlkDwDm5LzHAZF6+EMgxqvycWmMFvnJey6gE161CcAnRHYlCwDV52UizYFXj7/NQd+9jDxk+CkFOt5dKiS+zB7D9eJ3sCmFGzSa0ude9MuYBNOgCVfVYGR2DQeD1h3tNmaJFOnZ31JuP8Gp7QkXay0qRPYpMGHFHf71Ui50SanWeKlA0FheJhEK1DbpKAnrG2KEptW2xS+Ryqxs+qC7wSa2DxJsCkFm5z7VD/B597adgGbcAL4kKoKgE2m0GecfmzApqz3XY6sbQpTdNMLd/3XXLZVEu4XXGtEkl7bFGbc5u+s+6XZediU8b5K5XmDaP/8nZXwSbrkzniESeXjRj00x5N0VR2ccQLrNTYlSsL9aomRtU36u2EGPldFKiHGfSNUmdddeolsnEALKWzjy83ODicNm8aBm/qObRub6jszEz3zX/na6Tsmc1a9xSYTS7W+PvjPVSZzudl51mBTfSu9eM9gU55WYJOdvmMyZwU2FfdrBluCTZO53Ow8a7DJ4NKu3BXYBDYRUe+GAmBTZTc3zoFgk50AMZmzApvGWcumjgWbwKZuQMNkekn9rMEmU16vVD9gE0vPHgXAplKLt6bGYBPYBDZ1QwGwqSYnmN8t2GQPNDATsKkVJxAbFGwCm7oBDXhMsKkVjwk2sfTsUQBsasUJgE0lZKck3B5/wUzAphJL11xTsImlZ48CYJO5lV29J6JNedq99rvXtg637FkzzGSSFbj35N4bc29UX+vakReiHyN99rWTD3/8w+fPdyfZ8Dh3exR48uhvK+/8tK9rrSvnBTblXamrH129+JeLn+99bs+yYSaTqYAYoZiiGKQRzwI2FZfxiw/+tPqrX+xtP5xMw+Os7VFAjFBMUQyyuPXSsg4FwKYRql66fUlu8SVbx4YCLSogRmiKmcTiwaZSznTj/ffkFl+ydWwo0KICYoQwU6mVW1NjsKkmYekWBexVAGyy99owMxRAAbsVAJvsvj7MDgVqUABsqkFUukQBFJgIBcCmibjMnCQK6AqATdgDCqAAClRTAGyqphtHoUCHFQCbOnzxmDoKoECrCoBNrcrP4CjQhgJgUxuqMyYKoEAfFACb+nAVOQcUKKUA2FRKLhqjAAqggFIAbMIYUGDiFACbJu6Sc8IogAKGFACbDAlJNyjQHQXApu5cK2aKAihglwJgk13Xg9mgQAMKgE0NiMwQKIACvVQAbOrlZeWkUCBPAbAJ+0ABFECBagqATdV04ygU6LACYFOHLx5TRwEUaFUBsKlV+RkcBdpQAGxqQ3XGRAEU6IMCYFMfriLngAKlFACbSslFYxRAARRQCoBNGAMKTJwCYNPEXXJOGAVQwJACYJMhIekGBbqjANjUnWvFTFEABexSAGyy63owGxRoQAGwqQGRGQIFUKCXCoBNvbysnBQK5CkANmEfKIACKFBNAbCpmm4chQIdVgBs6vDFY+oogAKtKgA2tSo/g6NAGwqATW2ozpgogAJ9UABsyruKT18+fff2u2/OvXnhtxfYUKBFBcQIr318zZTLAZuKK/lqf//eH95feednt6beZkOBFhUQI/zizx8UN11a1qQA2JQn7KXbly7OX/zk4Sc7eztsKNCiAmKEYopikEYcAdhUXMaN99/7+Jc/f/jFeotXn6FRQBQQIxRTFIMsbr20rEMBsClP1ddnX//00acHrw6OTo/YUKBFBcQIxRQl5mTEC4BNxWX88Cc/erR5HyfQovEztKeAGKGYosScilsvLetQAGzKU1WSMmKpx8NjNhRoXQExRTFII14AbCouoyRlcAKtGz8T8BQQUxSDLG69tKxDATNeuI6Z2dCn/EoJ47NiUcAGBcQUwabm3YL8SuEEbLB/5iAKiCmCTc07gdiIYNOIaBMeE29liQJgUyvuEmyyxP6ZBtjUigdIDgo2gU2kILuhANjUitMEm+AVexQg2tSKEyDaVEJ2knT2+AtmAjaVWLrmmoJNLD17FACbzK3s6j0RbSLa1I1Yiz2eq62ZgE3V/dwYR4JNbRk84yYVAJvGWMrGDgWbwCawqRsKgE3G3F6ZjsAm8MUeBTKxaXtpOf461suDtaXlK0tPdGuXZrE9KWthe3DFebPr6kpG43gnTvvVgdZRoVHKLMJk23JDJGY43uBVsWlnYfqt2Gd6YWe8uRg++u7MW+NOKTtJt7Uyv7gevpjA4F+78RNujx+ZnJlYh02pnnrb8DIer7vB6tTlwXhTysYmg6s+1hVOAAXSFRgZbXpy8/LyzcDiy7FFsNRGHjUSm8ZbtDUcbRE2zdzVz08oZWxMMapXbdh0f3FufnHxxuLi/Nz8na1jg3/lBVEokK2Aldg0u6kv2sHsrbExxagTqA2bDK76WFcsARTIdQL5LyBIYNPsqhs6unXdDQf5xOPHk2R/yFj+wpMl4/0nQrObCo+cdZ3sZChjOfuvOEOkRZuyR3F69ibmOpBY/8Ph5nV3uCtLg+tuzyGoSZ/ujZC3Z1Q/mTMcz8uME22KYtPQCUDFdo03tzGPrg2bxKZ3V+bn5hbvBzcEBv+Ky0CBDAU6gE1Dw3d1Y7oA8cc1RpsMrvpYVywBFMh2AuWwyb+NkYXgAopHG4IpHkU5CyR65xOilY4mfqZPaCaEGCfYHPQpGb1kki5nFOfYIAysxa78/uVAL5PoxrNHYFNOPzkzHM+z1INNTuQp+CiSCndqubP4To293Dygf7RCoDA5GHQie6ZnZpyUodtWdTgzU1uSbn1xTjJ08uf8yq6Qk8G/EppGgUwFOoZN6g5S3enqt5V6UCps6XlSjb2U3/SO1Zype/ure97Um9fZ2rDJ4KqPdcUSQIE8J1AOm/xKpkSQRo8exRhCcYxiLK1qyolOufu/0rKBPu6oflQoKGsUPc2nOwo3+qX3FgE1N1yWiDYFxVuJfvTAW3yGtmCTwyse5ITf3K8e32ixn/Brys6Qm+7OTMvHPdhBI+dLrGd3OJ2utAbu7vpqm1jYKNC0Ah3AJsdzeTevTqjfvwENM3da7EcxkB4QCnY6oXXvbngwu3zFr9UInGCs5+T9btjADdHXV9vUtAFMThkfZ5qlQNXapihteNAQ1CZGAkXJaFMYNApYoyA25YwSwyY/9OX3n4tNgQ8Jk3QaNkX7sRObipSEKzDSiCcEvbSdISJNL9xdmHbQKECpSBow6NnZGeBRJC9XZ5KOGDIKNK6AldjklUGoLZVRFC1pxBM6gbSdgVfdvH55MFhadrxhEIJSgXcfzoJCB4VHIZB59AY2gXf9UcAINkmgKEjSOVVE8fKmWLQpTKhpa9CrK0reIylUcsNRmaPo2JTsP3JPFSTpwoRgrLYpwKbUeWbN0JZokz6PyHN2KuQTJuS0EqjkTg93pAtpJd/Vn5GIVSpL6QQVa11NJV53yW2fPQpYiU2Jwgg9UJ+CU6rgVC+GSO70cEfctPTvlV8ERRgRKkphKSfCFD5iDTb1hxjsWYktzsQINknZkF9z7ZZdR95QkIw2uUHfvJLwqcvLGS8gyBwl9ixeVkn41OyqV00VFolLLj4Dm5LzHAZF6+EM02q5ysOBwdqmYPAIvKSFfEYFnhxWWlBpuemFBZed/HycXirlApkegiLa1HgIpEUPMmlDdwmb9JLP1JBPmM7TnJa20wnU3/RfG+PEnG4GRayZ0aYA4Ig24QT6q8BIbCrPAFYfYbYmydSp1oBNsQIkL4MWS6V5HJS6U/bHqpqCCqfs2qZI2XmIWNQ29dd9TBozef+Lp4Q/jaz8C9FPxT4jBBPtIwpAfoGRzlKqQepO98YxUtUUVDglqqZUbZOKe1HbRISptwqATRWdldHDqnrhvLcN6E+7SXWS9jScXw2V8ySde3KxZ+h0+kl9kk5/70HQwA1SURLeW/cBNo3jB2rHJrcUyQvsL98chC8Rjj80555E6s7YM3R6iVJQyhp5kk5/jjposHxzidqmCVwpPT7lScOmcbxcfcdWxab6ZmRTz9Q29dgBde7UrIs22bRU65sL/7lK51ZKjycMNtW30ov3DDblaQU29dgBde7UwKbifs1gS7CpcyulxxMGmwwu7cpdgU1gE4m8bigANlV2c+McCDb1mEI6d2pg0zhr2dSxYBPY1A1o6JyDMz5hsMmU1yvVD9hk3JLpsLICYFOpxVtTY7AJbAKbuqEA2FSTE8zvFmyq/BvPgcYVAJtacQKxQcEmsKkb0GDcAXWuQ7CpFY8JNnVupfR4wmBTK04AbCohOyXhPXZAnTs1sKnE0jXXFGzq3Erp8YTBJnMru3pPRJvytHvtd69tHW71eBFyah1S4N6Te2/MvVF9rWtHmnlvk5GpWN/Jhz/+4fPnux2yE6baYwWePPrbyjs/tX7R9HyCYFPeBb760dWLf7n4+d7nPV6HnFonFBAjFFMUgzTikMCm4jJ+8cGfVn/1i73th52wEybZYwXECMUUxSCLWy8t61AAbBqh6qXbl+QWX7J1bCjQogJihKaYSSwebCrlTDfef09u8f3/zTT8/4n916CzHwWaUUCMEGYqtXJragw21SQs3aKAvQqATfZeG2aGAihgtwJgk93Xh9mhQA0KgE01iEqXKIACE6EA2DQRl5mTRAFdAbAJe0ABFECBagqATdV04ygU6LACYFOHLx5TRwEUaFUBsKlV+RkcBdpQAGxqQ3XGRAEU6IMCYFMfriLngAKlFACbSslFYxRAARRQCoBNGAMKTJwCYNPEXXJOGAVQwJACYJMhIekGBbqjANjUnWvFTFEABexSAGyy63owGxRoQAGwqQGRGQIFUKCXCoBNvbysnBQK5CkANmEfKIACKFBNAbCpmm4chQIdVgBs6vDFY+oogAKtKgA2tSo/g6NAGwqATW2ozpgogAJ9UABs6sNV5BxQoJQCYFMpuWiMAiiAAkoBsAljQIGJUwBsmrhLzgmjAAoYUgBsMiQk3aBAdxQAm7pzrZgpCqCAXQqATXZdD2aDAg0oADY1IDJDoAAK9FIBsKmXl5WTQoE8BcAm7AMFUAAFqikANlXTjaNQoMMKgE0dvnhMHQVQoFUF/h+3BaSngdHNuwAAAABJRU5ErkJggg==">
	 * <br>
	 * 
	 * @param Assoc Array or String $data
	 * @param String $label
	 * @param String $placeholder
	 * @param String $value
	 * @param Bool $required
	 * @param String $prepend
	 * @param String $append
	 * @param String $rules
	 * @return Bool returns TRUE if success, FALSE otherwise
	 */
	public function enqueue_text_input($data, $label='', $placeholder='', $value='', $required=FALSE, $class='', $rules='', $prepend='', $append='', $id='', $helper_message=''){
		$parameters = get_defined_vars();
		$parameters['type'] = 'text';
		return ($this->enqueue_input($parameters));
	}
	
	/**
	 * Enqueue Text Data to Form
	 * This is not input type it cannot be edited nor transmitted.
	 * 
	 * <code>bool enqueue_text_data ($label[, $data=''[,$helper='']])</code>
	 * 
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->form_lib->enqueue_text_data('Academic Term','1st Semester 2013-2014');
	 * </pre>
	 * @param string $label
	 * @param string $data
	 * @param string $helper
	 * @return boolean
	 */
	public function enqueue_text_data($label, $data='', $helper=''){
		$parameters = get_defined_vars();
		$parameters['type'] = 'textdata';
		return ($this->enqueue_input($parameters));
	}
	
	/**
	 * Enqueue Text Area to the form
	 * <code>bool enqueue_textarea (mixed $data [,$label='' [,$class='' [,$required=FALSE [,$value='' [,$rules='']]]]])</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->form_lib->enqueue_textarea(array('name'=>'comments',
	 *                                         'label'=>'Your Comments', 
	 *                                         'rules'=>array('required','minlength:3'),
	 *                                         'class'=>'input-xlarge'
	 *                                         )
	 *                                  );
	 * </pre>
	 * @param Multi $data
	 * @param String $label
	 * @param String $value
	 * @param Boolean $required
	 */
	public function enqueue_textarea($data, $label='', $class='', $required=FALSE, $value='', $rules='', $id=''){
		$parameters = get_defined_vars();
		$parameters['type'] = 'textarea';
		return ($this->enqueue_input($parameters));
	}
	
	
	/**
	 * Enqueue Checkbox to the form
	 * <code>bool enqueue_checkbox (mixed $data [,$value='' [,$label='' [,$class='' [,$required=FALSE]]]])</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->form_lib->enqueue_checkbox('agreement','accepted','I accept the license agreement','',TRUE);
	 * </pre>
	 * @param Multi $data
	 * @param String $label
	 * @param String $class
	 * @param String $required
	 * @return Bool
	 */
	public function enqueue_checkbox($data, $value='', $label='', $class='', $required=FALSE, $id=''){
		$parameters = get_defined_vars();
		$parameters['type'] = 'checkbox';
		return ($this->enqueue_input($parameters));
	}
	
	/**
	 * Enqueue Radio Button to form
	 * 
	 * <code>bool enqueue_radio_button (mixed $data [, array $options=array() [, $label='']])</code>
	 * 
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * //inline data
	 * $this->form_lib->enqueue_radio_button('contact',
	 *                                        array(//value  //label
	 *                                              '1m'=>'1 Month',
	 *                                              '6m'=>'6 Months',
	 *                                              '1y'=>'1 Year',
	 *                                              '2y'=>'2 Years'
	 *                                              ),
	 *                                        'Contract'
	 *                                       );
	 * 
	 * //array data
	 * $contract = array(
	 *                   'name' => 'contact',
	 *                   'options' => array(
	 *                                      '1m' => '1 Month',
	 *                                      '6m' => '6 Months',
	 *                                      '1y' => '1 Year',
	 *                                      '2y' => '2 Years',
	 *                                      ),
	 *                   'label' => 'Contract'
	 *                   );
	 *                   
	 * $this->form_lib->enqueue_radio_button($contract);
	 * </pre>
	 * 
	 * Screenshot
	 * <img src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAL4AAACACAIAAAA6ZkQXAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAnzSURBVHhe7Z3LbxtVFMbZg+gGibCgVBUSLJEK7Gi7QGpVVqzaBRGWkJD4D9iwMbAAiaYvqQUVITVVaWiEEG2lJmkSmpCHlXoSx3ESErvNo5X8SByHJruicObpO/Y87DNjd6bzRVZkj+fM3PnOb869M8n95oU9/EABlgIvsKIQBAX2gA4gYCoAdJjCIQzogAGmAkCHKRzCgA4YYCoAdJjCIQzogAGmAkCHKRzCgA4YYCoAdJjCIQzogAGmAk2iU+yNHdR/4hJzn4EJe/r06fTMzG83bpw9d45e9IY+0kL3Bso6WB2/pk/NV1Jc1syDXna7c29oC9doBh1ZAeP4FZGaVcO7BN63oIv55MmT7qtX+/r7l5eX/1V+6A19pIX0laPk9ijIzYvFarCi1euWuadUPFL/jtp9vw2v0Tg6JnDk7Ssy9RYb3pUW0ixu5u37JCKVlivd3aOjo5ub5fFE8nb/ML0mp6TNcpkW0ld2tUctK7F43L7qxHt7Y6IuJJy8qNnz7PlBp44cU0qrHZkumnLksmLKj7JUPVn1T7SCkgGtdhlfiqVd6B8V4sxbaAbamnWpY6ICUyiWbt4ZHByZmEzOJqT02OT9oXtjpY0N+opWsNx8UZLkk8Wpw4pL8qEZ55RMjrzEQIenFYXrR+/t5POgmjm04aojV127EiNgZbxVBNIiTAv1Azf1eDVbUOOEhYb0PlWdnp6ebDYrszKamJrOSLMLmcVs9sFKem5hLpOhr2gFJ42d0ZHJ0sVSyBFQ42ulsed8DvsGhvuG/EDHpKOOmNidWSbetr/Tt2AJq0/o0KCYBje3+gaTqfn0/NJy7uGjR48LhUI+X5hJzdJXtIIHdKrsqORU0fGulU8KuKPhtkYz6Nj11qYc60dm2VU79N9C36RWK2u0fBJORWdgaGRhKbeyuk7QbG1tVSoV+rWczdL78xcueEFHZ0cjp4qOd618UsANDPfvG0ZH7D60zeql0/ZMEvsm5b0dOiZM2lF1fr1+PZfLTafSmflFlZudnZ3d3d1yuZzP5+krWsETOrJcsV5J77isO1yxQjelVSBGO42jo+S9Ot6xHMsI4xPbqmMaR2u3hmoGANpeasY6SqBtN+d+lohrSJLUPzCwsbGZlKZX19ao3hA629vbpVKJqhF9RSt4Q0cb0mtHW1XDbqxjiY6VViGsOoqSltdBWjkRrp5sC4zWLZm6f3m74lUHnap61yj0YqbRdfN3lGo4MC7OK5XttfV1eaBTLJbLW3RHx/niXNuOyzDZ0Mou9x60Cis6zZ3bgV6bKKH7N1Rg6E4glRz6oQsr+kgL3W4JBvq42ta4JjqstrWpbTui2kMdE/394XRXF73ogpw+NvSHiLY1McA7ijQ6Ac5LCJoGdEKQpGA2EegEMy8haBXQCUGSgtlEoBPMvISgVUAnBEkKZhOBTjDzEoJWAZ0QJCmYTQQ6wcxLCFoFdEKQpGA2EegEMy8haBXQCUGSgtlEoBPMvISgVUAnBEkKZhOBTjDzEoJWAZ0QJCmYTQQ6wcxLCFoFdEKQpGA2MdLosJwuxP/Ar5twL/zbvpBvdTKAhxkwgflXdhHi6KLDc7qQKVCnOVhO61EWwukimGXSn1aJThf9w2Pfnv+FXnfvjbk5XTjNvJdbppQHOF34k6RgbsVwuvju4pW3jsWOf/bl+x9/fuDoqdM/XXVyunDtONQV6Hd7nC6q0+KadKvxIysR7bBUp4u+ob/f/LDz7J/jE6X/vhiZe+XoJ/uPnEzcT9o6XeizgdUZeBaDF40tgZ0WOl0IJdCtGvqBSu02IoqOalfw1Q+X3vno0zOZx51S7u0rqVe/n+g4dLzr8jVbpwvlLBecX+rOdb0sGey00uniGdulRBqd+JnLHYeOHewaP/BzquNS6uVvxgmdi929tk4Xlj4V4tko2hLIXLXa6aLaY3m4fmOWpIiiozpd/DWWoB5q35HOfV9PvBifeOmDzjeOnPon+8DW6cI01rEa+JhsCdrodPEsClBE0TGcLn7svvH64ZMd75147d0T+w+f/P3OsLPTRbXuWGZLwEktCC10uhDvDgAdZulsPkx0ukhlFq790ddz62724VoDThfVW4L2w2SlQdVbQPXGQj45XeAKq/nU+xABpwuPIka0w1JVg9OFF3oijY4X4RALdMAAUwGgwxQOYUAHDDAVADpM4RAGdMAAUwGgwxQOYUAHDDAVADpM4RAGdMAAUwGgwxQOYUAHDDAVADpM4RAGdMAAUwGgwxQOYUAHDDAVADpM4RAGdMAAU4FIo8NyulCEtp5BbH46qpYR8b/bmUkKZlh00eE5XShZdDA9qQXFp8fcBhGeiKIjOl2MJ5K3+4fpNTkluTldaM+3jcXjxqNta7MqTol6jsHZ24soOobTxc07g4MjE5PJ2YSUHpu8P3RvzMnpgnoqSdLNdWyn6urw1INjzJsSYusf4ixPWJfZ9OTm1Po6FVF0VKcLmZXRxNR0RppdyCxmsw9W0nMLc5mMrdOFkQ8XtxSl2yKHJpOdQTVGYKrmsefGs9y9eIC1nhplDxFFR3W6uNU3mEzNp+eXlnMP5UedFwr5fGEmNWvrdNEoOuapn2qUiTZLUxN9YUi6uUijMzA0srCUW1ldJ2jI3aJSqdCv5WzW1umicXTqy1J1wnH9rGF1iT5F3dUAqk1lxWU3EUVHdbqYTqUz84sqNzs7O7u7u+VyOZ/P2zpdeEWnbnhkKjBi1Wm/50nTPEYUHcPpIilNr66tUb0hdLa3t0ulkrPThSawa2GwWEFYZLytGeqogyPXjTed5ZYERBQd0elibX1dHugUi+XyVgNOFw63BIUMWaffuJoyxs/Vbkxx41FGx0CnJaj7t1E4XXjUMqJVR1UNThde6Ik0Ol6EQyzQAQNMBYAOUziEAR0wwFQA6DCFQxjQAQNMBYAOUziEAR0wwFQA6DCFQxjQAQNMBYAOUziEAR0wwFQA6DCFQxjQAQNMBYAOUziEAR0wwFQA6DCFQxjQAQNMBSKNDsfpQphOZTk1xjzjk7ICpwsmmsENYzldWM3zNR0inC6Cm3F/WsZ0unB9zrlWZfR6FJIpwDxNI9phsZ0uBJVtHxDts9PFM32ssANVEUXHq9OFOs/OZGQhiuyj04VQ6Cw9DngVw4+oiKLj0elCGSs7TguvHxwznS5sa5sf2fe0jUijw3O6sHQMrE2Cn04X1R4rUCYGEUWH7XThXm9UiKzR8eZ0EbACFFF0mE4XjV8x+eV0Ie4R6HjqYH0K5jldiLZ/qpWSbQ/io9MFrrB8Srpvm4HThUcpI9phqarB6cILPZFGx4twiAU6YICpANBhCocwoAMGmAoAHaZwCAM6YICpANBhCocwoAMGmAoAHaZwCAM6YICpANBhCocwoAMGmAoAHaZwCPsfdvL17IjVuvgAAAAASUVORK5CYII=">
	 * <br>
	 * @param Multi $data
	 * @param Array $options
	 * @param String $label
	 * @return Bool
	 */
	public function enqueue_radio_button($data, array $options=array(), $label='', $id=''){
		$parameters = get_defined_vars();
		$parameters['type'] = 'radio';
		return ($this->enqueue_input($parameters));
	}
	
	/**
	 * Enqueue Select Input to form
	 * <code>bool enqueue_select_input (mixed $data [, array $options=array() [, $label='']])</code>
	 *  Example:
	 * <pre class="sunlight-highlight-php">
	 * 	$this->form_lib->enqueue_select_input('currentstatus',
	 *                                         array('single'=>'Single',
	 *                                               'married'=>'Married',
	 *                                               'complicated'=>'It\'s Complicated'
	 *                                               ),
	 *                                        'Current Status'
	 *                                        );
	 * </pre>
	 * @param Multi $data
	 * @param Array $options
	 * @param String $label
	 * @param String $id
	 * @return Bool
	 */
	public function enqueue_select_input($data, array $options=array(), $label='', $id='', $class='',$selected=''){
		$parameters = get_defined_vars();
		$parameters['type'] = 'select';
		return ($this->enqueue_input($parameters));
	}
	
	/**
	 * Enqueue Select Input From DB Array
	 * Example
	 * <pre class="sunlight-highlight-php">
	 * assume that $academic_program variable has data from database
	 * 
	 * //tags inside {} will be replaced with array data
	 * $this->form_lib->enqueue_select_input('academic_program','Academic Program','{id}','{abbreviation} [{effective_year}]',$academic_program);
	 * </pre>
	 * 
	 * Screenshot
	 * <img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYMAAACxCAIAAACpw0ZXAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABLPSURBVHhe7Z3PrlzFEYf9KLxDHoCnyJZdSFaQtRcg73mBWMoTsAGzMhtWbBAismVFDo6EkLhIdogIXhkJcVPV1X+quvvM9D0zc0+fW79PR3F3VXX1RDP95Rzf3PG9awAA2BqYCACwPTARAGB7YCIAwPbARACA7YGJAADbAxMBALYHJgIAbA9MBADYHpgIALA9MBEAYHtgojvFmx+++OWrD3HhmuSiD2T8aB4DJro70Lv+6pO3f/nqg+8/f//bz/78/NM/4cK1yUUfP/oQ0keRPpCDMoKJ7g7hf4U+ePbs2cuXL3/99dcYBeDWoY/f1dXV06dP5TMZoweBie4O9K5///l7pKE4B2BT6KMod0ZxfhCY6O5AJvrXo3d/++23OAdgU+jO6NvP/gITuYNMRI/ocQLABDz/9F2YyB0wETgjf705caUCJvIITATOCJmFnvTHgYlABCYCZ0RM9HoMmAgUYCJwRvZtolcfv/PWW289+CZOT4A7ndDnmwejq+UlG87x+jcAJgJnREz0yxizmSid6jMc5RNNNA5v9M7Hr+KMIInZwF4YM9Gzh3+suP+4/D+QXj6+H6OBh89iPKzKM4IL9UJBmuu6cyLd213BJRAT/W+MyUwk9uCTfLpDtjNRUNEeb4vGTbSkFM6pgx60JLWNBBZNpHrLokAvelQpnUra9ugycBbERD+PMZeJkjzak8yRSDr0XJzIxSX44ONiIlWqIqI8hlrmktSecrlrZ3cFL+2bKOzCr0Py5WWo8tw7v2C7qiy6wYtfyToTBacU3ZhUgVL3Hz5U8jlqIl1QxmqLZjfeo3TsV8JEt4aY6L9jTGUiOWE8SkdZ0DOuSQdUx8I4JeMwrlLLc4FKN2MZ0ao8SMvLek0dDP3CijBSLyhV5ZZ1METrVXrz0jaG63EqXsU6E/VPfA2lyABWCQdNxPnSK82kTRWUjaubn4VKGrW7gksgJvopca9HzP3000wm0gepZ49F0kJ7ElML+rMEUzPTlCracVx2fPdQYkkL1AtSQyJ2tcH0gm20kONxtUS7L34lp/89EZ94G0kkMWRBcGm3rOhHwZtSohglkJsJerpUCRPdGmKi/yiifhIxGpjIRHwQLXIcFw4mhxVckY6yENdVdUyM5tLuYaYBFyzsruGSvgDUYvva4ja2eZqZKE8UIW4Kui9+JSufzjhQCaGQEloFYXQDE4WGobb1y5KYlipholtDTPTCEiV0716cJ+YxUXVUy5zPXXO+mtPKExNMDWwwYaLdw0yD1PPI6V4uUbvYl5GW6J1zydKqPDHR7otfyUoTLUiF4VrJKEtIgzETca3yXOsX3UJPlypholtDTPRtA2kojhTTmIgOUTldgRzhg5dzPKHTFv8IIS4slTEaFoWoLs1nlYN5O32A85gGi7vLJNENBvQuuopfscTLKAzDWK2qF0lct+Vw3lyP13CqiSoxMFkIJscT89fXGcrk3rxRtZNtU9mm3aNTCRPdGmKi52PMYiJ1IjMqxmcvEUPhYAYefBPScgZzWP/srNTqhnk/SreHmQapoLO7htN9AZhddB9VnoPvPHgg1WZVeenlv2Zd0L74lZz8dMaj2g1xbsQQzKDvdTJUFtfXmomUjnoo2D36lTDRrSEm+ucY09wTgZM9cjqn/411dkykZKwlYl3rBCoTZdg+geKSGKiXd/eoKinW7gougZjo2Rgw0abouxu++1E3UFswZqKLkk10KWCiW0NM9HQMmGhj8sOZeWTbCJgInBEx0ZMxYCJQmMNExKVkJN1hottBTPSPMWAiUJjARODuICYaByYCEZgInBEyy02JKxUwkUdgIjAbMJFHyET4V4bAPOBfGXIKmej7z9+7urqKcwA2hT6K+JcXPUImonf9yZMn+NeowbbQx+/HH3+kj6J8JmP0IDDR3eHND1+8+uTtcGf0Pt0V0yM6LlybXPTxC3dDH9IHkj6W8QN6EJjoTvHh3x/jwjXVFT+ax4CJ7hTjbzwAtwBM5BSYCEwFTOSUgTdefmVCo399ovz+eyD/3kZYpX+Lgwvb37vAb3sAA0zklEETLSmFc+qgBy1JbSOBRROp3rIo0IseVUqnkrY9ugzMA0zklBUmCk4pujGpAqXwrwyBGwMTOWWFifonvoZSZACrhIMm4nzplWbSpgrKxtXNz0IljdpdwbTARE4ZNJHFnngbSSQxZEFwabes6EfBm1KiGCWQmwl6ulQJE+0LmMgpK+6JQqASQiEltArC6AYmCg1DbeuXJTEtVcJE+wImcsoaEy1IheFayShLSIMxE3Gt8lzrF91CT5cqYaJ9ARM55SQTVWJgshBMjif4V4bAADcz0e+///7vwIsXLySKyE4ja0zEATndPKrdEOdGDMEM+l4nQ2Vxfa2ZSOmoh4Ldo18JE+0L+kDKJ/Popxf3RHeKQRNZ9NGOjomUjLVErGudQGWiDNsnUFwSA/Xy7h5VJcXaXcG03OyeCNwZxt/4y5BNdClgon0BEzkFJgJTARM5ZQITEZeSkXSHiXYETOSUrU0EgAEmcgpMBKYCJnLKwBsvjzga/bhTfl4VyM9ZYZV+6uLC9jkJT2fAABM5ZdBES0rhnDroQUtS20hg0USqtywK9KJ6eS9YlpcgbdvuCqYFJnLKChMFpxTdmFSBUpf7VpBeUC9XeQq3u4JpgYmcssJEPQ20UIoMYJVw0EScL73STNoMBG370oxG7a5gWmAipwyayGJPvI0kki2yNawqEpQu+lHwptEvKi/NBoJlMxq1u4JpgYmcsuKeKAT06eZ5JiXEDzRIUihy0FCZ6R0IDUOt9UtIyG/1N0HTvzSAiXYGTOSUNSZakArDtZKhkfbCOb8VpBuMI+H+48dKhO2uYFpgIqecZKLigEy2hMnx5IzfCtINGkoFTLQvYCKnrDERB+R088jkSm1xAcO6CPcqtROoLK7vGkV3tM2boFrPw7QVTLQvYCKnDJrIoo92dEykZGiVMUCoa51AZaIP2yegvCKo5b2gahGXEhRsdwXTAhM5ZfyNvwzZRJcCJtoXMJFTYCIwFTCRUyYwkXmYOi/SHSbaETCRU7Y2EQAGmMgpMBGYCpjIKQNvvDziaPTjjvqBFZOfs8Iq/dTFhe1zEp7OgAEmcsqgiZaUwjl10IOWpLaRwKKJVG9ZFOhF9fI6WBmRCS0o3u4KpgUmcsoKE4VDX3RjUgVK3e63ghgoGlvBRPsCJnLKChMd1UBAXKAKjpmI86VXmkmbkaBC5ynd7gqmBSZyyqCJLFYDNpJINshW4NJuWaURgTelRKUZadYNxglh5zDRvoCJnLLinigE9OnmeSYlsg+SgW5gotAw1LbSoWk3GCf1CphoZ8BETlljogWpMFwrGRqlkuKPdhHlTG+uVZ5rpUOpbrBMbEOYaF/ARE45yUTGAUK2RO2H2/lWEG5hO8BE+wImcsoaE3FATjePTK7U0kgZgHWh73UyVBbXG6MUSkfbvA3WEwEm2hcwkVMGTWTRRzs6JlIytMoYINQdMJHtE4hOKRm1fDFYb0GxdlcwLTCRU8bf+MuQTXQpYKJ9ARM5BSYCUwETOWUCExGXkpF0h4l2BEzkFHrj//C3a1y4JrlgIqfARLimumAip4yY6LtYW/j6y5L9+k0MCo/0qp9L2R++vH5tF8olzfOq816Pfubmr6/qOK5pL5jIKYMm+u55mX50dX395vqjMOajnsYxlbQiiinqWTZR0dDzsCagd5RWhF7eDbYXvSSYaEcXTOSUFSYSp2TdmJS6OKU9ddREukCNyxbBU/W+Kpgv/apgon1dMJFTVpiI74PSY1eV0helSCWl4JiJ+H5KPc3RQx8vJNGoe65DQTV9rSIw0b4umMgpgyaqyEKRxzEdyZeYqFjj6D2RvURhlZ5IgqSVbjCPaQvtJphoXxdM5JQV90TyQKSdolWVj300kdykUPAmJuKGwV+tdGjaDdKA4vI6YaL9XjCRU9aYKMtFReKlJJVNJMFHYyZirSidtdKhVDfIu6QgTLTfCyZyykkmyk9eNiXFxUQiC7qOmYjGWjF82S1i816Q/rMm3SjBRDu6YCKnrDFRuMfJNz7GHSElZtEmkkcz4oCJqjudfJXdbfM2mK9sQ7pgon1dMJFTBk1UoYVibkbUrYoxkYjmoInam5pok2QxolUb0faEifZ7wUROGTHRRa9sogtdMNG+LpjIKTARrqkumMgpM5iIuJCMqp/E4Zr/gomcMv7GA3ALwEROgYnAVMBEThl44+WLDzX6SxDLV9sH8rcvhlX6uxh7X3eP72wEFTCRUwZNtKQUzqmDHrQktY0EFk2kesuiQC+ql/eCveW0bbsrmBaYyCkrTBScUnRjUgVK2X9s8aiJdEEZqy3KsBfsL+dhuyuYFpjIKStM1HVDA6XIAKpAm6JABSpfeqWZtBkI9peHEUy0I2AipwyayGI1YCOJZItsDS7tlhV/KHjTxi+xWTcYJ5G4nICJ9gVM5JQV90QhoE83zzMpkf2QDHQDE4WGobaVDk27wThhynICJtoXMJFT1phoQSoM10qGRqlEGoyZiGuV51rpUKobzGO9nICJ9gVM5JSTTKQdEMmWMDmemL++zlAm9+aNqp1sm9S8G+wuh4n2BkzklDUm4oCc7ubwl1oaKQOwLqqbFYHK4vpsFEvpaJvXwYXlMNHOgImcMmgiiz7a0TGRkqFVxgCh7oCJbJ9AFEvJqOV1cHE5JdpdwbTARE4Zf+MvQzbRpYCJ9gVM5BSYCEwFTOSUCUyUH6TOj3SHiXYETOSUrU0EgAEmcgpMBKYCJnLKwBsvjzga/bhT/dAqP2eFVfqpiwvb5yQ8nQEDTOSUQRMtKYVz6qAHLUltI4FFE6nesijQi+rl3WAH2vZIBZgJmMgpK0wUnFJ0Y1IFSt3ut4IkKKC3gYn2BUzklBUmOqSBgghBFRwzEedLrzSTNiNBeTXNbRKl213BtMBEThk0kcVqwEYSyRbZGlzaLSv6UfCmlCiaCUizbjBOmilMtC9gIqesuCcKgersF1IiCyEZ6AYmCg1DbSsdmnaDcaI2FmCifQETOWWNiRakwnCtZJQQpMGYibhWea6VDqW6wThppjDRvoCJnHKSiapDz2RLmBxPLv6tIBGThIn2BkzklDUm4oCcbh7VGohzKwTWhb7XyVBZXF8bJVI62uZtMEIBmGi/wEROGTSRpTrpMcqUTCUEqWudQGXiEdsnEAVTMmp5NxiAiXYNTOSU8Tf+MmQTXQqYaF/ARE6BicBUwEROmcBExKVkJN1hoh0BEzllaxMBYICJnAITgamAiZwy8MbLI46m+tlUjAbyc1ZYpZ+6uLB9TsLTGTDARE4ZNNGSUjinDnrQktQ2Elg00TENSSdGLW+ClRGZ0Jji7a5gWmAip6wwUTj0RTcLIqHUDb8VJMErc51elXfrBg2lB0y0L2Aip6wwkZr3NRAQF6iCARNxdSDX2UU8o+puUCN7y5jS7a5gWmAipwyayGI1YCOJZINsBeuPBKVrlVUeUfnYohuMM0KvD+l2VzAtMJFTVtwThYA+3TzPpET2QTJFY4wAlR0ykVkV9qlb5WCEc7ojzVUWzA5M5JQ1JlqQCsO1klE+kQb9RZQ7aCKClwfuP36cU91gjJuGMNG+gImccpKJamcQnArFJseT498KEul0jXQz9Ubta13oBmYEJnLKGhNxQE43j0yu1NJIGYAFFW5gOh45aKJstuK/hSBTts9QQbsrmBaYyCmDJrLoo80qKBgpGAOEutYJx0xkNiiV3WCI1ltQrN0VTAtM5JTxN/4ydEx0XmCifQETOQUmAlMBEzllAhPZJ6yzIt1hoh0BEzllaxMBYICJnAITgamAiZwy8MbLI45GP+6on2Ix+TkrrNJPXb0fbOHpDFTARE4ZNNGSUjinDnrQktQ2Elg00TENSSdGLe8Ge9C2RyrATMBETllhouCUopsFkVBqm28FMcthor0BEzllhYnUvNFAQYSgCgZMxNX2Pscu4hlVd4NEu5ygdLsrmBaYyCmDJrJYDdhIglaFYPqz8keC0rXK8gqiaIaJLbrBOLPLCUq3u4JpgYmcsuKeKASqs19IiSyEZIraGAKVHTKRWRX2qVvlYMIsh4n2BkzklDUmWpAKw7WSUUKQButMRPDywMi3gjTLYaJ9ARM55SQT1c4gOBWKTY4nt/KtIEQ1h4n2BUzklDUm4oCcbh6ZXKm1QmBBhRuY2glUdtBE2WzFfwvBhN2YC9pdwbTARE4ZNJGlOukxypRMJQSpa51wzERmg1LZDQow0a6BiZwy/sZfho6JzgtMtC9gIqfARGAqYCKnTGCizhPWuZDuMNGOgImcQm88LlxTXfGjeQyYCACwPTARAGB7YCIAwPbARACA7YGJAADbAxMBALYHJgIAbA9MBADYHpgIALA9MBEAYHtgIgDA9sBEAIDtgYkAAFtzff1/Lnk/6rVeXwYAAAAASUVORK5CYII=" />
	 * <br>
	 * @param unknown_type $data
	 * @param unknown_type $label
	 * @param unknown_type $value_key
	 * @param unknown_type $label_key
	 * @param unknown_type $options
	 * @param unknown_type $selected
	 * @param unknown_type $id
	 * @return boolean
	 */
	public function enqueue_select_input2($data, $label, $value_key, $label_key, $options, $selected='', $id='',$class=''){
		$parameters = get_defined_vars();
		$parameters['type'] = 'class_input_select';
		return ($this->enqueue_input($parameters));
	}
	
	/**
	 * Enqueue Free Line on form
	 * <code>bool enqueue_free_line(String $line)</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->form_lib->enqueue_free_line('&lt;div class="controls"&gt;Register &lt;a href="#"&gt;Here&lt;/a&gt;&lt;/div&gt;');
	 * </pre>
	 * @param String $line
	 * @return Bool
	 */
	public function enqueue_free_line($line){
		$parameters = array('type' => 'freeline',
							'data' => 'freeline',
							'content' => $line
							);
		return ($this->enqueue_input($parameters));
	}
	
	/**
	 * Enqueue script as the bottom of the form
	 * <code>bool enqueue_script(mixed $script)</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * //string input
	 * $this->form_lib->enqueue_script("alert('Hello World');");
	 * //array input
	 * $this->form_lib->enqueue_script(array("$('#{form_id}-submit').bind('click', function() { alert('submitting form') } );",
	 *                                       "$('#{form_id}-reset').bind('click', function() { alert('form cleared') } );"
	 *                                        ));
	 * </pre>
	 * Note! You can add Execution Variables like <code>{form_id}</code> and it will replace the form id
	 * Note! You should put semi collon ';' in every line
	 * @param Multi $script
	 * @return Bool
	 */
	public function enqueue_script($script){
		if (empty($script)){
			return FALSE;
		}
		if (is_array($script)){
			foreach($script as $line){
				if (is_string($line)){
					array_push($this->scripts, $line);
				}
			}
		}
		else if (is_string($script)){
			array_push($this->scripts, $script);
		}
		return TRUE;
	}
	
	/**
	 * Process all the input to be enqueued
	 * @param Multi Array $parameters
	 * @return Bool returns TRUE if success, FALSE otherwise
	 */
	private function enqueue_input($parameters){
		if (!isset($parameters['data']) || empty($parameters['data'])){
			//data or name is not set (nothing todo here)
			return FALSE;
		}
	
		//data default values
		$input = array(
				'label' => '',
				'value' => '',
				'placeholder' => '',
				'class' => '',
				'required' => False
		);
	
		if (is_array($parameters['data'])){
			//data is inserted in assoc array format
			$input = array_merge($input,$parameters['data']);
			//$input = $parameters['data'];
			$input['type'] = $parameters['type'];
		}
		else {
			//data is inserted in parameters
			$input = array_merge($input,$parameters);
			$input['name'] = $parameters['data'];
		}
	
		//set rules
		if (isset($input['rules'])){
			if (is_array($input['rules']) AND count($input['rules']) > 0) {
				foreach ($input['rules'] as $rule){
					$rule_array = explode(':',$rule);
					$this->set_rule($input['name'],$rule_array[0],(isset($rule_array[1]) ? $rule_array[1] : ''));
				}
			}
			else if (is_string($input['rules'])){
				$rule = $input['rules'];
				$rule_array = explode(':',$rule);
				$this->set_rule($input['name'],$rule_array[0],(isset($rule_array[1]) ? $rule_array[1] : ''));
			}
		}
	
		if ($input['required']){
			$this->set_rule($input['name'],'required');
		}
	
		array_push($this->inputs, $input);
		return TRUE;
	}
	
	public function enqueue_text_helper($data,$message,$type){
		$i=0;
		foreach($this->inputs as $input){
			if ($input['data'] == $data){
				$this->inputs[$i]['helper_message'] = array('type'=>$type,'message'=>$message); 
				break;
			}
			$i++;
		}
	}
	
	/**
	 * Sets the rule in the form
	 * <code>bool set_rule (string $target, string $name[, $parameter=''])</code>
	 *  Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->form_lib->set_rule('password','required');
	 * $this->form_lib->set_rule('password','minlength', '3');
	 * </pre>
	 * @param String $target
	 * @param String $name
	 * @param String $parameter
	 */
	public function set_rule($target,$name,$parameter=''){
		//check
		if (empty($target) || empty($name)){
			return False;
		}
		
		//process rule
		$to_insert_rule = array();
		
		//if rule is just a require string
		if ($name == 'required'){
			$to_insert_rule['rule'] = $name;
			$to_insert_rule['value'] = 'true';
		}
		else {
			$to_insert_rule['rule'] = $name;
			if ($name == 'equalTo')
				$to_insert_rule['value'] = '"#'.$parameter.'"';
			else
				$to_insert_rule['value'] = $parameter;
		}
		
		$this->rules[$target][] = $to_insert_rule;
	}
	
	/**
	 * Set Submit Button to the form
	 * <code>bool set_submit_button(string $label[, $class=''])</code>
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->form_lib->set_submit_button('Login','btn btn-primary');
	 * </pre>
	 * Submit button will be place at the bottom part of the form
	 * @param String $label
	 * @param Multi $class
	 */
	public function set_submit_button($label, $class=''){
		$this->submit['enabled'] = TRUE;
		$this->submit['label'] = $label;
		$this->submit['class'] = (is_array($class) ? implode(' ',$class) : $class);
	}
	
	/**
	 * Set Reset Button to the form
	 * <code>bool set_reset_button(string $label[, $class=''])</code>
	 *  Example:
	 * <pre class="sunlight-highlight-php">
	 * $this->form_lib->set_reset_button('Reset','btn btn-link');
	 * </pre>
	 * Reset button will be place at the bottom part of the form
	 * @param String $label
	 * @param Multi $class
	 */
	public function set_reset_button($label, $class=''){
		$this->reset['enabled'] = TRUE;
		$this->reset['label'] = $label;
		$this->reset['class'] = (is_array($class) ? implode(' ',$class) : $class);
	}
	
	/**
	 * Adds a class attribute to each control group.
	 * @param Multi $class
	 */
	public function add_control_class ($class=''){
		if (!empty($class)){
			if (is_array($class) && count($class)>0)
				$this->control_group_class = implode(' ', $class); else
				$this->control_group_class = $class;
		}
	}
	
	/**
	 * Process all the form elements
	 * @param Boolean $return
	 * @return String
	 */
	public function content($return=TRUE,$debug=FALSE){
		if($debug){
			echo '<pre>';
			print_r($this);
			exit();
		}
		
		//$form_id = (!empty($this->form_id) ? $this->form_id : str_shuffle('kevinfelisilda')); //haha
		$form_id = $this->form_id;
		
		$form = '<form id="'.$form_id.'"'.$this->extract_attributes($this->attributes).'>';
		if (count($this->hidden_inputs) > 0){
			foreach($this->hidden_inputs as $input){
				$form .= '<div class="hidden"><input type="'.$input['type'].'" name="'.$input['name'].'" value="'.$input['value'].'"'.$input['attributes'].'></div>';
			}
		}
		$form .= '<fieldset>';
		if (count($this->inputs) > 0){
			foreach($this->inputs as $input){
				if ($input['type'] == 'text' || $input['type'] == 'password'){
					$extra = (!empty($input['prepend']) || !empty($input['append']) ? TRUE : FALSE);
					$form .= '<div class="control-group ' . $this->control_group_class;
					if (isset($input['helper_message']['type']) AND !empty($input['helper_message']['type']))
						$form .= ' '.$input['helper_message']['type'];
					$form .= '">';
					$form .= (!empty($input['label']) ? '<label for="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '" class="control-label">'.$input['label'].'</label>' : '');
					$form .= '<div class="controls">';
					if ($extra){
						$form .= sprintf('<div class="%s%s">',(!empty($input['prepend']) ? 'input-prepend' : ''),(!empty($input['append']) ? (!empty($input['prepend']) ? ' ' : '').'input-append' : ''));
					}
					if (!empty($input['prepend'])){
						$form .= sprintf('<span class="add-on" id="%s">%s</span>',substr(base64_encode($input['append'].'-prepend'),0),$input['prepend']);
						//$form .= "<script>$('#fname').width(206-($('#fnameprep').width()+11))</script>";
					}
					$form .= '<input type="'.$input['type'].'" id="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '" name="'.$input['name'].'" value="'.$input['value'].'" placeholder="'.$input['placeholder'] . '"';
					
					//added by justing... this is always set as an array or an empty string... lets test for empty
					if (! empty($input['class'])){
						if(is_array($input['class']) && count($input['class']) > 0){
							$form .= ' class="' . implode(' ', $input['class']) . '"';
						} else {
							//it could be a string...
							$form .= ' class="' . $input['class'] . '"';
						}
					}
					$form .= '>';
					if (!empty($input['append'])){
						$form .= sprintf('<span class="add-on" id="%s">%s</span>',substr(base64_encode($input['append'].'-append'),0),$input['append']);
					}
					if (!empty($input['helper_message'])){
						if (!is_array($input['helper_message']))
							$helper_message = $input['helper_message'];
						else
							$helper_message = $input['helper_message']['message'];
						$form .= '<span for="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '" class="help-inline">'.$helper_message.'</span>';
					}
					if ($extra){
						$form .= '</div>';
					}
					$form .= '</div></div>';
					//$form .= '<div class="formSep"></div>';
				}
				else if ($input['type'] == 'textdata'){
					$form .= '<div class="control-group data ' . $this->control_group_class . '">
							<label class="control-label">'.$input['label'].'</label>
							<div class="controls">
							<p>'.$input['data'].'</p>
							</div>
							</div>';
				}
				else if ($input['type'] == 'textarea'){
					$form .= '<div class="control-group ' . $this->control_group_class . '">';
					$form .= '<label for="'.$form_id.'-'.$input['name'].'" class="control-label">'.$input['label'].'</label>';
					$form .= '<div class="controls">';
					$form .= '<textarea style="resize:vertical"'.(!empty($input['class']) ? (is_array($input['class']) ? ' class="'.explode(' ',$input['class']).'"' : ' class="'.$input['class'].'"' ) : '').' rows="5" id="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '" name="'.$input['name'].'">'.$input['value'].'</textarea>';
					$form .= '</div></div>';
				}
				else if ($input['type'] == 'checkbox'){
					$form .= '<div class="control-group ' . $this->control_group_class . '">';
					$form .= '<div class="controls">';
					$form .= '<label style="display:inline" for="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '">'.$input['label'].'</label>';
					$form .= '<input style="float:left;margin-right:7px"'.(!empty($input['class']) ? (is_array($input['class']) ? ' class="'.explode(' ',$input['class']).'"' : ' class="'.$input['class'].'"' ) : '').' type="checkbox" id="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '" name="'.$input['name'].'" value="'.$input['value'].'">';
					$form .= '</div></div>';
				}
				else if ($input['type'] == 'radio' AND isset($input['options']) AND is_array($input['options'])){
					$form .= '<label class="control-label">'.$input['label'].'</label>';
					foreach ($input['options'] as $value => $label){
						$form .= '<div class="controls">';
						$form .= '<label class="radio">';
						$form .= '<input type="radio"'.(!empty($input['class']) ? (is_array($input['class']) ? ' class="'.explode(' ',$input['class']).'"' : ' class="'.$input['class'].'"' ) : '').' name="'.$input['name'].'" id="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '" value="'.$value.'"'.($label == reset($input['options']) ? ' checked>' : '>').$label;
						$form .= '</label>';
						$form .= '</div>';
					}
				}
				else if ($input['type'] == 'select' AND isset($input['options']) AND is_array($input['options'])){
					$form .= '<div class="control-group ' . $this->control_group_class . '">';
					$form .= '<label class="control-label" for="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '">'.$input['label'].'</label>';
					$form .= '<div class="controls">';
					$form .= '<select name="'.$input['name'].'" id="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '"'.(!empty($input['class']) ?  'class="'.$input['class'].'"' : '').'>';
					foreach ($input['options'] as $value => $label){
						//By Justin: Added trap: isset($input['selected'])
						$form .= '<option value="'.$value.'" '.(isset($input['selected']) && $input['selected'] == $value ? 'selected="selected"' : '').' '.($label == reset($input['options']) ? ' checked>' : '>').$label.'</option>';
					}
					$form .= '</select></div></div>';
				}
				else if ($input['type'] == 'class_input_select' AND isset($input['options']) AND is_array($input['options'])){
					//$input['options'] = get_object_vars($input['options']);
					$options = array();
					foreach ($input['options'] as $option){
						if (is_object($option)){
							array_push($options, get_object_vars($option));
						}
						else{
							array_push($options, $option);
						}
					}
					$form .= '<div class="control-group ' . $this->control_group_class .'">';
					$form .= '<label class="control-label" for="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '">'.$input['label'].'</label>';
					$form .= '<div class="controls">';
					$form .= '<select name="'.$input['name'].'" id="' . (empty($input['id']) ? $form_id.'-'.$input['name'] : $input['id']) . '" class="'.$input['class'].'">';
					$i = 0;
					foreach ($options as $option){
						$i++;
						$label = $this->replace_tags($input['label_key'],$option,$i);
						$value = $this->replace_tags($input['value_key'],$option,$i);
						$form .= '<option value="'.$value.'"'.($value == $input['selected'] ? ' selected="selected">' : '>').$label.'</option>';
					}
					$form .= '</select></div></div>';
				}
				else if ($input['type'] == 'freeline'){
					$form .= $input['content'];
				}
			}
			if ($this->submit['enabled'] || $this->reset['enabled']){
				$form .= '<div class="form-actions">';
				$form .= ($this->submit['enabled'] ? '<button type="submit" id="'.$form_id.'-submit" style="margin-right:10px" class="'.(!empty($this->submit['class']) ? $this->submit['class'] : 'btn btn-success').'">'.$this->submit['label'].'</button>' : '');
				$form .= ($this->reset['enabled'] ? '<button type="reset" id="'.$form_id.'-reset" class="'.(!empty($this->reset['class']) ? $this->reset['class'] : 'btn').'">'.$this->reset['label'].'</button>' : '');
				$form .= '</div>';
			}
		}
			
		$form .= '</fieldset></form>';
		
		//scripts
		if (count($this->rules) > 0){
			
			$rules = "onkeyup:false,errorClass:'help-inline',validClass:'text-success',rules:{";
			foreach($this->rules as $rule => $data){
				$rules .= $rule.':{';
				foreach($data as $parameter){
					$rules .= $parameter['rule'].':'.str_replace('{form_id}',$form_id,$parameter['value']).',';
				}
				$rules .= '},';
			}
			$rules .= '},highlight:function(element,errorClass){$(element).parent().parent().addClass(\'error\');},unhighlight:function(element,errorClass){$(element).parent().parent().removeClass(\'error\');},errorElement:"span"';
			
			$form .= '<script>$(document).ready(function(){$("#';
			$form .= $form_id;
			$form .= '").validate({';
			$form .= $rules;
			$form .= '});';
			
			//put reset button script
			if ($this->reset['enabled']){
				$form .= "$('#".$form_id."-reset').bind('click', function(){ $('#".$form_id."').validate().resetForm(); $('#".$form_id." .control-group.error').removeClass(\"error\")})";
			}
			
			//lets enqueue all the other scripts here...
			if(!empty($this->enqueued_scripts)){
				foreach ($this->enqueues_scripts as $script){
					$form .= $script;
				}
				$form .= ';';
			}
			
			foreach ($this->scripts as $script){
				$form .= str_replace('{form_id}',$form_id,$script);
			}
			$form .= '});</script>';
		}
		
		if ($return)
			return $form;
		else
			echo $form;
	}
	
	private function replace_tags($source, $replace, $order = 0){
		if (is_array($source)){
			$source = implode(' ',$source);
		}
		
		if (is_array($replace)){
			foreach($replace as $key => $value){
				$source = str_replace('{'.$key.'}', $value, $source);
			}
		}
		
		$source = str_replace('{nth}',$order,$source);
		
		return $source;
	}
	
	/**
	 * This function will concatinate all the atrributes
	 * @param Array or String $attributes
	 * @return String
	 */
	private function extract_attributes($attributes){
		if (is_array($attributes)){
			$string = '';
			foreach($attributes as $key => $value){
				switch($key){
					case 'class':
						$concat = ' ';
						break;
					case 'style':
						$concat = ';';
						break;
					default:
						$concat = ',';
						break;
				}
				$value = (is_array($value)) ? implode($concat,$value) : $value;
				$string .= $key.'="'.$value.'" ';
			}
			$attributes = trim($string);
		}
		return (!empty($attributes) ? ' '.$attributes : '');
	}
}

interface Form_lib_Interface {
	public function content($return=TRUE);
}