<style>
	div#msg_register {
		border-radius: 5px; 
		background-color: #D7D4D4; 
		border: solid; 
		border-width: 1px; 
		border-color: #B7B6B6; 
		text-align:left; 
		padding-left: 10px;
		margin:0 auto;
		margin-bottom:20px;
		font-family: arial;
		font-size: 14px;
		width: 98%;
		height: 35px;
		line-height: 35px;
		font-weight:bold;
		color:#B50303;
	}
</style>

<div class="row-fluid" >
	<div id="msg_register" style="display:none;">
	</div>

	<div class="form-horizontal">
		<fieldset>
			<form id="proceed_to_register_old_student_form" class="form-horizontal">
				<div class="control-group formSep">
					<label for="grade_level" class="control-label">Register For</label>
					<div class="controls">
						<select id="academic_terms_id" name="academic_terms_id" class="form-control" >
							<option value="0">--Select Term--</option>
							<?php 
								if ($acad_terms) {
									foreach ($acad_terms AS $term) {
							?>
									<option value="<?php print($term->id); ?>" >
										<?php print($term->term.' '.$term->sy); ?>
									</option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="grade_level" class="control-label">Grade Level</label>
					<div class="controls">
						<select id="grade_level" name="grade_level" class="form-control" >
							<option value="11">Grade 11</option>
							<option value="12">Grade 12</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="strand" class="control-label">Strand:</label>
					<div class="controls">
						<select id="prospectus_id" name="prospectus_id" class="form-control" style="width:auto;" >
							<?php 
								if ($strands) {
									foreach ($strands AS $strand) {
							?>
									<option value="<?php print($strand->id); ?>" <?php if ($current_strand_id == $strand->id) print("selected"); ?>><?php print($strand->description); ?></option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls" style="float:left; padding-right:10px;">
						<input type="submit" class="btn btn-danger" name="proceed_to_register_student" id="proceed_to_register_student" value="Register Student!" disabled />
					</div>
					<div style="float:left; padding-right:8px; margin-top:5px;" id="show_register_icon">
					</div>		
				</div>
			</form>				
		</fieldset>
	</div>
	
</div>


<script>
	$('#proceed_to_register_old_student_form').submit(function(e){
		e.preventDefault();		
		
		var confirmed = confirm('Proceed to register student?');

		if (confirmed){

			var students_idno     = "<?php print($idnum); ?>";
			var academic_terms_id = $('#academic_terms_id').val();
			var grade_level       = $('#grade_level').val();
			var prospectus_id     = $('#prospectus_id').val();
				
			$('#show_register_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+students_idno,
				data: { students_idno: students_idno,
						academic_terms_id: academic_terms_id,
						grade_level: grade_level,
						prospectus_id: prospectus_id,
						action: 'register_old_student'},
				dataType: 'json',
				success: function(response) {				

					$('#show_extracted_items').html(response.output); 	

					$('h4.media-heading').html(response.student_info);

					$('#show_register_icon').html("");
					
					if (response.success) {
						$('#msg_register').show();	
						$('#msg_register').html("Student successfully registered! Proceed to Teller if Registration Fee has not been made yet!");
					}
				}
			});
		}
		
	});
</script>


<script>
$('#academic_terms_id').bind('click', function(event){
	event.preventDefault();

		var element       = $("option:selected", "#academic_terms_id");
		var acad_terms_id = element.val();

		if (acad_terms_id != 0) {
			$('#proceed_to_register_student').prop("disabled", false);
		} else {
			$('#proceed_to_register_student').prop("disabled", true);
		}
});
	
</script>


