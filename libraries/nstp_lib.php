<?php 
class nstp_lib {
	
	protected $CI;
	public function __construct(){
		$this->CI =& get_instance();
	}
	
	public function index($term=NULL){
		$this->CI->load->model('developers/nstp_model');

		$data = $this->CI->nstp_model->fetch_nstp_list($term);
		////log_message('INFO',print_r($data,true)); // Toyet 8.14.2018

		$this->CI->content_lib->enqueue_body_content('developers/nstp_list', 
								array( 'data' => $data,
									   'term' => $term,
								 ) );
		$this->CI->content_lib->content();
	}

}

