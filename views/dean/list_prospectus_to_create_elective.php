<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
</style>
<div style="background:#FFF; width:70%;">
<div style="width:100%;"></div>
<div style="width:100%;">
  <table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
    <thead>
    <tr>
      <th width="37%" class="header" style="vertical-align:middle;">Program</th>
      <th width="12%" class="header" style="vertical-align:middle;">Program Status</th>
      <th width="10%" class="header" style="vertical-align:middle;">Effective<br />
        Year</th>
      <th width="13%" class="header" style="vertical-align:middle;">Prospectus 
        Status</th>
      <th width="9%" class="header"  style="vertical-align:middle;">Details</th>
      </tr>
    </thead>
    <?php
		if ($prospectus) {
			foreach ($prospectus AS $prospectus) {
	?>
    <tr>
      <td style="text-align:left; vertical-align:middle; "><?php print($prospectus->description); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($prospectus->program_status); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($prospectus->effective_year); ?></td>
      <td style="text-align:center; vertical-align:middle; "><?php print($prospectus->prospectus_status); ?></td>
      <td style="text-align:center; vertical-align:middle; ">
      <a class="prospectus_id" href="<?php print($prospectus->id); ?>"><i class="icon-edit"></i></a></td>
      </tr>
    <?php
			}
		}
	?>
</table>
</div>
</div>

  <form id="prospectus2" action="<?php echo site_url("dean/{$this->uri->segment(2)}")?>" method="post">
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="2" />
</form>
<script>
	$('.prospectus_id').click(function(event){
		event.preventDefault(); 
		var prospectus_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'prospectus_id',
		    value: prospectus_id,
		}).appendTo('#prospectus2');
		$('#prospectus2').submit();
	});
</script>