<?php

	class Student_model extends CI_Model {
	
		function __construct() {
        	parent::__construct();
		
   		}


		function getStudent($id=null) {
			if($id == null) { return null; }
			
			$result = null;
			
			$q1 = "SELECT b.students_idno,b.yr_level,b.prospectus_id,d.abbreviation,
											CONCAT(a.lname,', ',a.fname) AS neym
											FROM 
												col_students AS b,
												students AS a,
												prospectus AS c,
												academic_programs AS d
											WHERE
												a.idno=b.students_idno
												AND b.prospectus_id=c.id
												AND	d.id=c.academic_programs_id
												AND b.students_idno = {$this->db->escape($id)} ";
			$query = $this->db->query($q1);

			//print($q1);
			//die();

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}

		
		function UpdateStudentProspectus($academic_terms_id,$students_idno,$prospectus_id,$yr_level) {
			//This needs to be resolved:
			//1. col_student is a redundant table of student_histories. You can actually get everything from col_students out of the student histories table.
			//2. We're updating two tables here when we can update only one.
			/* Not needed anymore...
			$q1 = "UPDATE col_students 
					SET 
						prospectus_id = {$this->db->escape($prospectus_id)}, yr_level={$this->db->escape($yr_level)}
					WHERE
						students_idno = {$this->db->escape($students_idno)} ";
				
			$q2 = "UPDATE student_histories 
					SET 
						prospectus_id = {$this->db->escape($prospectus_id)}, year_level={$this->db->escape($yr_level)}, can_enroll='Y' 
					WHERE
						students_idno = {$this->db->escape($students_idno)} 
					AND 
						academic_terms_id={$this->db->escape($academic_terms_id)} 
					";
			*/	
			
			$q2 = "INSERT INTO 
						student_histories (students_idno, academic_terms_id, prospectus_id, year_level, inserted_by, inserted_on) 
					VALUES ({$this->db->escape($students_idno)}, 
							{$this->db->escape($academic_terms_id)},
							{$this->db->escape($prospectus_id)},
							{$this->db->escape($yr_level)},
							{$this->session->userdata('empno')},
							NOW()) 
					ON DUPLICATE KEY UPDATE 
							prospectus_id={$this->db->escape($prospectus_id)},
							year_level={$this->db->escape($yr_level)},
							block_sections_id=NULL, 
							updated_by={$this->session->userdata('empno')},
							updated_on=NOW()";
			
			// genes (via rey)  added block_sections_id=NULL, on 10.29.15
			 
			//print_r($q2);die();				
			if ($this->db->query($q2)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
		//Edited 6/26/13 by: genes 
		//year level extracted from student histories
		//EDITED 7/24/13 by genes
		//include grades
		function ListEnrollees($course_offerings_id) {
			$result = null;
			
			$q = "SELECT 
						CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1)) AS neym, 
						a.idno,
						f.abbreviation,
						c.year_level,
						a.gender,
						d.prelim_grade,
						d.midterm_grade,
						d.finals_grade
					FROM 
						students AS a, 
						student_histories AS c, 
						enrollments AS d, 
						prospectus AS e, 
						academic_programs AS f
					WHERE
						a.idno=c.students_idno
						AND c.id=d.student_history_id
						AND c.prospectus_id=e.id
						AND e.academic_programs_id=f.id 
						AND d.course_offerings_id={$this->db->escape($course_offerings_id)} 
					ORDER BY
						a.lname, 
						a.fname ";

			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}

		/*
		 * @EDITED: 11/29/2014 by genes
		 * @NOTE: checks for parallel
		 */
		function ListEnrollees_Including_Parallel($course_offerings_id,$parallel_no=NULL) {
			$result = null;
			//print($course_offerings_id); die();
			/*$q = "(SELECT
						a.idno AS idno,
						UPPER(CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1),'.')) AS neym,
						f.abbreviation,
						c.year_level,
						i.course_code,
						a.gender
					FROM
						students AS a,
						student_histories AS c,
						enrollments AS d,
						prospectus AS g,
						academic_programs AS f,
						course_offerings AS h,
						courses AS i
					WHERE
						a.idno=c.students_idno
						AND c.id=d.student_history_id
						AND c.prospectus_id=g.id
						AND g.academic_programs_id=f.id
						AND d.course_offerings_id=h.id
						AND i.id=h.courses_id
						AND d.course_offerings_id={$this->db->escape($course_offerings_id)}
				)
				UNION
				(SELECT
						a.idno AS idno,
						UPPER(CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1),'.')) AS neym,
						f.abbreviation,
						c.year_level,
						i.course_code,
						a.gender
				FROM
						students AS a,
						student_histories AS c,
						enrollments AS d,
						prospectus AS g,
						academic_programs AS f,
						course_offerings AS h,
						courses AS i,
						parallel_offerings AS j
				WHERE
						a.idno=c.students_idno
						AND c.id=d.student_history_id
						AND c.prospectus_id=g.id
						AND g.academic_programs_id=f.id
						AND d.course_offerings_id=h.id
						AND i.id=h.courses_id
						AND j.course_offerings_id=h.id
						AND j.parallel_no={$this->db->escape($parallel_no)}
				)
				
			ORDER BY
			neym";
			*/
			if ($parallel_no) {
				$q = "SELECT
						a.idno AS idno,
						UPPER(CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1),'.')) AS neym,
						f.abbreviation,
						c.year_level,
						i.course_code,
						a.gender
					FROM
						students AS a,
						student_histories AS c,
						enrollments AS d,
						prospectus AS g,
						academic_programs AS f,
						course_offerings AS h,
						courses AS i,
						parallel_offerings AS p
					WHERE
						a.idno=c.students_idno
						AND c.id=d.student_history_id
						AND c.prospectus_id=g.id
						AND g.academic_programs_id=f.id
						AND d.course_offerings_id=h.id
						AND i.id=h.courses_id
						AND p.course_offerings_id = h.id
						AND p.parallel_no={$this->db->escape($parallel_no)}
					ORDER BY
						neym ";
			} else {
				$q = "SELECT
						a.idno AS idno,
						UPPER(CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1),'.')) AS neym,
						f.abbreviation,
						c.year_level,
						i.course_code,
						a.gender
					FROM
						students AS a,
						student_histories AS c,
						enrollments AS d,
						prospectus AS g,
						academic_programs AS f,
						course_offerings AS h,
						courses AS i
					WHERE
						a.idno=c.students_idno
						AND c.id=d.student_history_id
						AND c.prospectus_id=g.id
						AND g.academic_programs_id=f.id
						AND d.course_offerings_id=h.id
						AND i.id=h.courses_id
						AND d.course_offerings_id={$this->db->escape($course_offerings_id)}
					ORDER BY
						neym ";				
			}
			
			
			//print($q);die();
			$query = $this->db->query($q);
				
			if($query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		}
		
		
		/*
		 * @UPDATED: 8/22/14 by genes
		 * @note: include academic_programs_id
		 */
		function getEnrollStatus($id=null,$academic_terms_id=null) {
			if($id == null) { return null; }
			
			$result = FALSE;
			//Updated to include prospectus & academic programs - genes 5/7/13
			$q1 = "
					SELECT 
							a.*,
							c.abbreviation,
							c.id AS academic_programs_id
						FROM 
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c
							
						WHERE 
							a.prospectus_id = b.id
							AND b.academic_programs_id = c.id
							AND a.students_idno = {$this->db->escape($id)} 
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
				";
			//print($q1);die();
			$query = $this->db->query($q1);


			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}


		function EnrollStudentToOffering($enrollments_data, $post_status=NULL) {
			//$enrolled_by = isset($this->session->userdata('empno')) ? $this->session->userdata('empno') : null; 
			if($enrollments_data['parallel_no']){
				$q1 = "SELECT
						COUNT(xx.id) as num_enrolees,
						aa.max_students
						FROM
							course_offerings AS aa LEFT JOIN enrollments AS xx ON aa.id=xx.course_offerings_id
								LEFT JOIN parallel_offerings AS yy ON aa.id=yy.course_offerings_id
						WHERE
							yy.parallel_no = {$this->db->escape($enrollments_data['parallel_no'])}";
			}else{
				$q1 = "SELECT
						COUNT(xx.id) as num_enrolees,
						aa.max_students
						FROM
							course_offerings AS aa,
							enrollments AS xx  
						WHERE
							aa.id=xx.course_offerings_id
							AND aa.id = {$this->db->escape($enrollments_data['course_offerings_id'])}";
			}
			
			$query = $this->db->query($q1);
			
			$student_histories_id = $this->db->select('id')
				->from('student_histories')
				->where('students_idno', $enrollments_data['students_idno'])
				->where('academic_terms_id', $enrollments_data['academic_terms_id'])
				->limit(1)->get()->row()->id;
			
			if($query->row()->num_enrolees < $query->row()->max_students){
				
				$query2 = "
					INSERT INTO enrollments ( 
							student_history_id, 
							course_offerings_id,
							date_enrolled,								
							ip_address,
							post_status
					)
					VALUES (
						'{$student_histories_id}',
						{$this->db->escape($enrollments_data['course_offerings_id'])},
						NOW(),
						{$this->db->escape($enrollments_data['ip_address'])},
						{$this->db->escape($post_status)})";
				
				//print($query2); die();
				$this->db->query($query2);
				$id = @mysql_insert_id();
				
				$value = $id;
			}else {
				$value = 0;
			}
			
			return $value;
		}

		
		function getUnitsEnrolled($students_idno,$academic_terms_id,$prospectus_id) {
			
			$result = null;
			
			$q1 = "SELECT SUM(c.credit_units) AS total_credit_units_enrolled,
						(SELECT SUM(x.credit_units) 
								FROM
									enrollments AS a, 
									course_offerings AS b, 
									courses AS x, 
									student_histories AS d,
									prospectus_courses AS e,
									prospectus_terms AS f
								WHERE
									a.course_offerings_id=b.id 
									AND b.courses_id=x.id 
									AND a.student_history_id=d.id
									AND x.id=e.courses_id 
									AND d.students_idno='$students_idno' 
									AND b.academic_terms_id={$this->db->escape($academic_terms_id)}
									AND e.is_bracketed = 'Y'
									AND e.prospectus_terms_id=f.id
									AND f.prospectus_id={$this->db->escape($prospectus_id)}							
						) AS total_bracketed_units_enrolled
						FROM 
							enrollments AS a, 
							course_offerings AS b, 
							courses AS c, 
							student_histories AS d,
							prospectus_courses AS e,
							prospectus_terms AS f
						WHERE 
							a.course_offerings_id=b.id 
							AND b.courses_id=c.id 
							AND a.student_history_id=d.id
							AND c.id=e.courses_id 
							AND d.students_idno={$this->db->escape($students_idno)} 
							AND b.academic_terms_id={$this->db->escape($academic_terms_id)}
							AND e.is_bracketed = 'N'
							AND e.prospectus_terms_id=f.id
							AND f.prospectus_id={$this->db->escape($prospectus_id)} ";
			//print($q1);
			//die();
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		
		function CheckIfWithMaxUnits($students_idno, $academic_terms_id) {
			$q1 = "SELECT d.max_units AS max_units_to_enroll
						FROM 
							max_units AS d 
						WHERE 
							d.academic_terms_id={$this->db->escape($academic_terms_id)}
							AND d.students_idno={$this->db->escape($students_idno)} ";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}	
		
		}		
		

		/*function ReturnMaxUnit($students_idno, $academic_terms_id) {

			$q1 = "SELECT d.max_units AS max_units_to_enroll
						FROM 
							max_units AS d 
						WHERE d.academic_terms_id='$academic_terms_id'
							AND d.students_idno='$students_idno' ";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
				
			} else {
				$q1 = "SELECT c.max_credit_units AS max_units_to_enroll
						FROM prospectus_terms AS c,
							academic_terms AS e,
							col_students AS a  
						WHERE a.students_idno='$students_idno' 
							AND c.prospectus_id=a.prospectus_id
							AND a.yr_level=c.year_level 
							AND e.id='$academic_terms_id' 
							AND e.term=c.term";
						
				print($q1);
			  	die();
			
				$query = $this->db->query($q1);

				if($query->num_rows() > 0){
					$result = $query->row();
				} 
			}
			
			return $result ; 
		}*/


		function getMaxUnits($student_histories_id,$academic_terms_id) {
			//var_dump($students_idno);
			$result = null;
			//Please check... if ever max units is set on max_units table will return zero results...
			//Note: there is a mysql function IFNULL
			/*$q1 = "SELECT c.max_bracket_units, IF(d.max_units IS NULL,c.max_credit_units,d.max_units) AS max_units_to_enroll,
						IF(d.max_units IS NOT NULL,c.max_bracket_units+(d.max_units-c.max_credit_units),c.max_bracket_units) AS max_bracket_units
						FROM prospectus_terms AS c,
							academic_terms AS e,
							col_students AS a LEFT JOIN max_units AS d ON a.students_idno=d.students_idno 
								AND d.academic_terms_id='$academic_terms_id'
						WHERE c.prospectus_id=a.prospectus_id 
							AND a.students_idno='$students_idno' 
							AND a.yr_level=c.year_level 
							AND e.id='$academic_terms_id' 
							AND e.term=c.term 
						GROUP BY d.id";
			*/
			$q1 = "
					SELECT 
						c.max_bracket_units, 
						IF(d.max_units IS NULL,c.max_credit_units,d.max_units) AS max_units_to_enroll,
						IF(d.max_units IS NOT NULL,c.max_bracket_units+(d.max_units-c.max_credit_units),c.max_bracket_units) AS max_bracket_units
					FROM
						prospectus_terms AS c,
						academic_terms AS e,
						student_histories AS a LEFT JOIN max_units AS d ON a.students_idno=d.students_idno 
							AND d.academic_terms_id={$this->db->escape($academic_terms_id)}
					WHERE 
						c.prospectus_id=a.prospectus_id 
						AND a.id={$this->db->escape($student_histories_id)}
						AND a.year_level=c.year_level 
						AND e.id={$this->db->escape($academic_terms_id)}
						AND e.term=c.term 
					GROUP BY d.id
						";
			
			//print_r($q1);die();

			$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
						
		}

		function get_max_units_from_max_units($students_idno,$academic_terms_id){
			$sql = "
				select max_units 
				from 
					max_units
				where 
					students_idno={$this->db->escape($students_idno)}
					and academic_terms_id = {$this->db->escape($academic_terms_id)}
			";
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				$result = $query->row()->max_units;
			}else{
				return 0;
			}				
			return $result;
				
								
		}
		
		function getEnrolledCourse($academic_terms_id,$courses_id,$students_idno) {
			
			$result = null;
			
			$q1 = "SELECT a.id
						FROM enrollments AS a,
							course_offerings AS b,
							student_histories AS c
 						WHERE 
							a.course_offerings_id=b.id
							AND a.student_history_id=c.id
							AND c.students_idno={$this->db->escape($students_idno)}
							AND b.courses_id={$this->db->escape($courses_id)}
							AND b.academic_terms_id={$this->db->escape($academic_terms_id)} ";
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}

		function getCheckWithSummmer($prospectus_id, $yr_level) {			
			$q1 = "
					SELECT a.id
					FROM 
						prospectus AS a,
						prospectus_terms AS b
 					WHERE 
						a.id=b.prospectus_id
						AND a.id={$this->db->escape($prospectus_id)}
						AND b.term=3
						AND b.year_level={$this->db->escape($yr_level)} 
				";
			
			$query = $this->db->query($q1);
			if($query && $query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		}


		function WithdrawCourse($enrollments_id) {
					
			$q1 = "DELETE FROM enrollments 
						WHERE id={$this->db->escape($enrollments_id)} ";

			//print($q1); die();
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
		function TransferDeletedToHistory($stud_data) {
			
			$q1 = "
					INSERT INTO
						enrollments_history (student_history_id,
											course_offerings_id,
											status,
											enrolled_by,
											date_enrolled,
											ip_address_enrolled,
											inserted_by,
											inserted_on)
					 SELECT 
							student_history_id,
							course_offerings_id,
							'deleted',
							enrolled_by,
							date_enrolled,
							ip_address,
							".$stud_data['students_idno'].",
							NOW()
					FROM
							enrollments
					WHERE id = ".$stud_data['enrollments_id'];
			//echo $q1; die();
			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
				
		}
		
		//ADDED: 5/16/14 by genes
		function TransferDissolvedToHistory($empno, $course_offerings) {
				
			$q1 = "INSERT INTO
						enrollments_history (student_history_id,
											course_offerings_id,
											status,
											enrolled_by,
											date_enrolled,
											ip_address_enrolled,
											inserted_by,
											inserted_on)
					 SELECT
							student_history_id,
							course_offerings_id,
							'dissolved',
							enrolled_by,
							date_enrolled,
							ip_address,
							".$empno.",
							NOW()
					FROM
							enrollments
					WHERE course_offerings_id IN $course_offerings ";
				
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		
		}
		
		
		//Set max units of student....
		public function setMaxUnits($students_idno,$academic_terms_id, $units) {
			$result = null;
			
			//edited by Justing... Dec. 18, 2012
			//1. Added academic_terms ID to the select query.
			//2. Get only the id. (explicit declaration instead of getting everything)
			//Reason: Max units is set for a student at the academic term specified...
			$q2 = "
					SELECT 
						id 
					FROM 
						max_units
					WHERE 
						students_idno = '{$students_idno}'
					AND
						academic_terms_id='{$academic_terms_id}'
					";
			
			$query = $this->db->query($q2);
			if ($query->num_rows() > 0) {
				$result = $query->row();
				
				$q1 = "
					UPDATE 
						max_units 
					SET 
						max_units = '{$units}'
					WHERE 
						id = '{$result->id}'
					";
				//Note: Query was edited to use only the result id. Previous query is vague since student
				//can have more than one row here at different academic_terms... 
				$this->db->query($q1);
				$id = $result->id;
			}
			else {
				$q1 = "INSERT INTO max_units
				VALUES ('', '{$students_idno}', '{$academic_terms_id}', '{$units}')";
				$result = $this->db->query($q1);
				$id = $this->db->insert_id();
			}		
				return $id;
		}

		/**
		 * Method: student_is_enrolled
		 * 
		 * Determines whether student is enrolled at a certain semester or the current semester
		 * 
		 * @param int $student_id	(required) 	
		 * @param int $academic_term	(optional) if not set will mean the current term
		 * @return boolean	True if the student is enrolled, False if the student is NOT enrolled
		 */
		public function student_is_enrolled ($student_id, $academic_term=0){
			
			if ( empty($academic_term) ) {
				//lets set the academic term to the current academic term...
				//$this->load->model('hnumis/AcademicYears_model');
				$this->load->model('hnumis/enrollments_model');
				$this->load->model('academic_terms_model');
				$academic_term = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
				//$academic_term = $this->AcademicYears_model->getCurrentAcademicTerm();
			}
			
			$sql = "
				SELECT
					id
				FROM
					student_histories
				WHERE
					students_idno='{$student_id}'
				AND
					academic_terms_id='{$academic_term->id}'
				LIMIT 1
				";
			//print($sql);
			//die();
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0)
				return TRUE; else
				return FALSE;
		}
		
		public function get_StudentHistory_id ($student_id, $academic_term=0){
			
			//NOTE: 7/18/13 by genes
			//i checked if variable is object or not
			if (is_object($academic_term)) {
				$academic_term_id = $academic_term->id; 
			} else {
				$academic_term_id = $academic_term;
			}
			
			$sql = "
				SELECT
					id, 
					students_idno,
					prospectus_id,
					academic_terms_id,
					year_level,
					can_enroll,
					block_sections_id
				FROM
					student_histories
				WHERE
					students_idno='{$student_id}'
				AND
					academic_terms_id='{$academic_term_id}'
				LIMIT 1
				";
			//return $sql;
			//log_message("INFO", print_r($sql,true)); // Toyet 7.21.2018
			$query = $this->db->query($sql);
			$result=false;
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
		
		public function insert_student_history ($student_id, $prospectus_id, $academic_terms_id=0, $year_level=1, $can_enroll=TRUE, $block_sections_id=FALSE) {
			
			$enroll = ($can_enroll ? 'Y' : 'N');			
			
			if ( empty($academic_terms_id) ) {
				//lets set the academic term to the current academic term...
				$this->load->model('hnumis/AcademicYears_model');
				$academic_term_obj = $this->AcademicYears_model->getCurrentAcademicTerm();
				$academic_terms_id = $academic_term_obj->id;
			} 
			
			if (is_object($academic_terms_id) && isset($academic_terms_id->id)){
				$academic_terms_id = $academic_terms_id->id;
			}
			
			$sql = "
					INSERT IGNORE INTO col_students (`students_idno`) VALUES ('{$student_id}')
			";
			$this->db->query($sql);			
			$sql = "
				INSERT into
					student_histories (students_idno, academic_terms_id, prospectus_id, year_level, can_enroll, inserted_by, inserted_on ";
			$sql .= $block_sections_id===false ? ")" : ", block_sections_id) ";
			$sql .= "
				VALUE 
					(
						'{$student_id}', 
						'469', 
						'{$prospectus_id}', 
						'{$year_level}', 
						'{$enroll}',
						{$this->session->userdata('empno')},
						now() ";
			$sql .= $block_sections_id===false ? ")" : ",'{$block_sections_id}')";
			//print_r($sql);die();
			log_messag("INFO", print_r($sql, true)); // Toyet 10.24.2018
			if($this->db->query($sql))
				return $this->db->insert_id(); else
				return FALSE;
		}
		
		//Added:  January 12, 2013 by Amie
		//EDITED: 5/12/14 by genes
		function assign_block($student_id, $academic_term_id, $section_id, $prospectus_id, $year_level) {
					
			/*$q1 = "UPDATE student_histories
						SET block_sections_id = $section_id
						WHERE
							 students_idno = {$this->db->escape($student_id)}
							AND academic_terms_id = {$this->db->escape($academic_term_id)}";
			*/
			
			$q2 = "INSERT INTO 
						student_histories (students_idno, academic_terms_id, block_sections_id, prospectus_id, year_level, inserted_by, inserted_on) 
					VALUES ({$this->db->escape($student_id)}, 
							{$this->db->escape($academic_term_id)},
							{$this->db->escape($section_id)},
							{$this->db->escape($prospectus_id)},
							{$this->db->escape($year_level)},
							{$this->session->userdata('empno')},
							NOW()) 
					ON DUPLICATE KEY UPDATE 
							block_sections_id = {$this->db->escape($section_id)}, 
							prospectus_id = {$this->db->escape($prospectus_id)},
							year_level = {$this->db->escape($year_level)},
							updated_by={$this->session->userdata('empno')},
							updated_on=NOW()";

			//print($q2); die();
			if ($this->db->query($q2)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		//Added:  February 19, 2013 by Amie
		function unblock_student($student_id, $academic_term_id) {
					
			$q1 = "UPDATE student_histories
						SET block_sections_id = NULL
						WHERE
							 students_idno = {$this->db->escape($student_id)}
							AND academic_terms_id = {$this->db->escape($academic_term_id)}";

			//print($q1);
			//die();
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		//Added:  February 19, 2013 by Amie
		function get_blocksection($idnum, $academicterm) {
					
			$q1 = "SELECT b.section_name, b.yr_level
					FROM block_sections b, student_histories sh, academic_terms ac
						WHERE
							b.id = sh.block_sections_id
							AND sh.academic_terms_id = {$this->db->escape($academicterm)}
							AND sh.students_idno = {$this->db->escape($idnum)}";

			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} else $result = FALSE;
			
			return $result;		
		}
		
		
		//Added:  February 14, 2013 by Amie
		//updated: 3/23/2013
		//UPDATED: 1/10/14 genes
		function creditcourse($student) {
					
			$q1 = "INSERT INTO 
						credited_courses
								(students_idno,
								prospectus_courses_id,
								enrollments_id,
								other_schools_enrollments_id,
								credited_by,
								credited_on)
					
						VALUES(
							".$student['students_idno'].",
							".$student['prospectus_courses_id'].",
							".$student['enrollments_id'].",
							".$student['other_schools_enrollments_id'].",
							".$student['credited_by'].",
							NOW())";
						
			//print($q1);	die();
			
			if($this->db->query($q1))
				return $this->db->insert_id(); else
				return FALSE;
		}
		
		//Added: Feb. 16, 2013 by Amie
		function Remove_credited_course($credited_id) {
			$q1 = "DELETE from credited_courses
					WHERE id={$this->db->escape($credited_id)}";

			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}	 
		}
		
		//Added: Feb. 15, 2013 by Amie
		function view_creditedcourses($idnum, $pc_id)
		{
			$result = null;
			
			$q1 = "SELECT * from credited_courses
					WHERE
						students_idno = {$this->db->escape($idnum)}
							AND prospectus_courses_id = {$this->db->escape($pc_id)}";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;		
		}
		
		//Added:  January 19, 2013 by Amie
		function ListBlockStudents($blockid, $academic_terms_id) {
					
			$q1 = "SELECT CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1)) AS student, a.idno
						FROM 
							students AS a, col_students AS b, student_histories AS c  
						WHERE
							a.idno=b.students_idno 
							AND b.students_idno=c.students_idno 
							AND c.block_sections_id={$this->db->escape($blockid)}
							AND c.academic_terms_id = {$this->db->escape($academic_terms_id)}";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;		
		}

		//Added 01/19/2013 By:Genes
		function getStudentBlock($students_idno,$academic_terms_id,$prospectus_id) {
			$result = null;
			
			$q1 = "SELECT a.block_sections_id
						FROM 
							student_histories AS a
 						WHERE 
							a.students_idno={$this->db->escape($students_idno)}
							AND a.academic_terms_id ={$this->db->escape($academic_terms_id)}
							AND a.prospectus_id = {$this->db->escape($prospectus_id)} ";
			//print($q1);
			//die();

			$query = $this->db->query($q1);

			if($query AND $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
		
		
		//Added: 01/21/2013 By: Genes
		//@EDITED: 3/31/15 by genes
		//NOTE: $result should not return TRUE if no data is extracted
		function ListAdvisedCourses($student_histories_id) {
			$result = null;
			
			$q1 = "SELECT 
						a.id,
						a.courses_id, 
						b.course_code,
						b.credit_units, 
						c.fname,
						c.lname
					FROM 
						advised_courses AS a,
						courses AS b, 
						employees AS c
 					WHERE
						b.id = a.courses_id
						AND a.advised_by = c.empno
						AND a.student_histories_id={$this->db->escape($student_histories_id)} ";
			//print($q1); die();
			$query = $this->db->query($q1);

 			if($query && $query->num_rows() > 0){
 				$result = $query->result();
 			}
			
			/*if ($query){
				if($query->num_rows() > 0) 
					$result = $query->result();
				else
					$result = true;
			}
			*/
			//return null;						
			return $result;	
		
		}
		
		//Added: 1/25/13 By:Genes
		function AddAdvisedCourse($advised_courses_data) {
			$sql="
				select id
				from advised_courses
				where 
					student_histories_id ={$this->db->escape($advised_courses_data['student_histories_id'])}
					and courses_id = {$this->db->escape($advised_courses_data['courses_id'])} 
				";
			$query = $this->db->query($sql);
			if ($query){ 
				 if ($query->num_rows() >= 1){
				 		return false;
				 }
			}else{
				return false;				
			}
			 
				
			$query = "
						INSERT INTO
							advised_courses (courses_id, student_histories_id,advised_by)
						VALUES (
								{$this->db->escape($advised_courses_data['courses_id'])},
								{$this->db->escape($advised_courses_data['student_histories_id'])},
								{$this->session->userdata('empno')}
								)
					";
		
			$result = $this->db->query($query);

			return $this->db->insert_id();
		}
		
		
		function update_student_history ($student_histories_id, $data){
			$sql = "
				UPDATE 
					student_histories
				SET
				";

			foreach ($data as $key=>$val){
				$sql.= " `{$key}` = {$this->db->escape($val)},";
			}
			$sql = rtrim($sql, ',') . " WHERE id={$student_histories_id}";
			//print($sql); die();
			log_message("INFO",print_r($sql,true)); // Toyet 10.24.2018
			return $this->db->query($sql);
		}
		
		/*ADDED: 3/18/13 By: Genes
		 * @UPDATED: 8/28/14 by: genes
		 * @notes: included 'name', 'gender', 'course_yr' 
		*/
		function AllEnrollees($course_offerings_id) {
			$result = null;
			
			$q = "SELECT 
							a.idno,
							CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1)) AS neym, 
							UPPER(CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1),'.')) AS name, 
							a.gender,
							CONCAT(f.abbreviation,'-',c.year_level) AS course_yr,
							f.abbreviation,
							c.year_level,
							i.course_code,
							c.id as stud_histories_id
						FROM 
							students AS a, 
							student_histories AS c, 
							enrollments AS d, 
							prospectus AS g, 
							academic_programs AS f,
							course_offerings AS h,
							courses AS i
						WHERE
							a.idno=c.students_idno
							AND c.id=d.student_history_id
							AND c.prospectus_id=g.id
							AND g.academic_programs_id=f.id
							AND d.course_offerings_id=h.id
							AND i.id=h.courses_id
							AND d.course_offerings_id IN {$course_offerings_id} 
						ORDER BY
							neym";

			//print($q);die();
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}
		
		function getMaxInSummmer($idno, $academic_terms_id) {
			$result = null;

			$q1 = "SELECT 
						a.max_units
					FROM
						max_units AS a					
					WHERE 
						a.students_idno={$this->db->escape($idno)}
						AND a.academic_terms_id={$this->db->escape($academic_terms_id)} ";
			
			//print($q1);
			//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}
		

		function Check_if_with_Privilege($student_history_id, $bed=NULL){
		$result = NULL;
		if(!$bed){
		$q1 = "SELECT 
					a.id, b.scholarship, 
					(c.discount_percentage * 100) as discount_percentage,
					a.posted,
					a.total_amount_availed
				FROM 
				   privileges_availed as a,
				   scholarships as b,
				   privileges_availed_details as c
				WHERE
				   a.scholarships_id = b.id
				   AND a.id = c.privileges_availed_id
				   AND a.student_histories_id = {$this->db->escape($student_history_id)}";
		}else {
			$q1 = "SELECT
			a.id, b.scholarship,
			(c.discount_percentage * 100) as discount_percentage,
			a.posted,
			a.total_amount_availed
			FROM
				privileges_availed as a,
				scholarships as b,
				privileges_availed_details as c
			WHERE
				a.scholarships_id = b.id
				AND a.id = c.privileges_availed_id
				AND a.basic_ed_histories_id = {$this->db->escape($student_history_id)}";
		}
		//print($q1);
		//die();
		$query = $this->db->query($q1);

		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
		return $result;
	}
	
	
			function getPayingUnitsBasic($students_idno, $academic_terms_id){
	
			$result = NULL;	
			$q1 = "SELECT SUM(x.paying_units) AS total_paying_units_enrolled
						FROM
								enrollments AS a, 
								course_offerings AS b, 
								courses AS x, 
								student_histories AS d
						WHERE
								a.course_offerings_id=b.id 
								AND b.courses_id=x.id 
								AND a.student_history_id=d.id
								AND d.students_idno={$this->db->escape($students_idno)}
								AND b.academic_terms_id={$this->db->escape($academic_terms_id)}
								AND x.id NOT IN (SELECT 
													y.courses_id 
													FROM 
													   tuition_others as y 
													WHERE 
													   y.yr_level = d.year_level
													   AND y.academic_terms_id = {$this->db->escape($academic_terms_id)}
													   AND y.levels_id = 6)";
			
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
			
			return $result;
						
		}
		
		
			function getPayingUnitsOthers($students_idno, $academic_terms_id){
	
			$result = NULL;	
			$q1 = "SELECT SUM(x.paying_units) AS total_paying_units_enrolled
						FROM
								enrollments AS a, 
								course_offerings AS b, 
								courses AS x, 
								student_histories AS d
						WHERE
								a.course_offerings_id=b.id 
								AND b.courses_id=x.id 
								AND a.student_history_id=d.id
								AND d.students_idno={$this->db->escape($students_idno)}
								AND b.academic_terms_id={$this->db->escape($academic_terms_id)}
								AND x.id IN (SELECT 
													y.courses_id 
													FROM 
													   tuition_others as y 
													WHERE 
													   y.yr_level = d.year_level
													   AND y.academic_terms_id = {$this->db->escape($academic_terms_id)}
													   AND y.levels_id = 6)";
			//print_r($q1);
			//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
			
			return $result;
						
		}
	
		function ListStudentsEnrolled($academic_terms_id, $yr_level, $colleges_id, $already_assessed = FALSE) {
			
			$q = "SELECT 
						c.id AS student_histories_id,
						c.students_idno,
						s.lname,
						s.fname,
						s.mname,
						f.abbreviation,
						c.year_level,
						f.acad_program_groups_id,
						f.id as academic_programs_id
					FROM 
						student_histories AS c, 
						enrollments AS d,
						prospectus AS e, 
						academic_programs AS f,
						students s					
					WHERE
						c.id=d.student_history_id
						AND s.idno = c.students_idno
						AND c.prospectus_id=e.id
						AND e.academic_programs_id=f.id 
						AND c.academic_terms_id = {$this->db->escape($academic_terms_id)} 
						AND c.year_level = {$this->db->escape($yr_level)}
						AND f.colleges_id = {$this->db->escape($colleges_id)} ";					 
			$q .= $already_assessed ? " AND c.id not in 
								(SELECT student_histories_id 
									FROM assessments ass
									JOIN ledger l ON l.assessments_id = ass.id 
								) ":" "; 
			$q .= " GROUP BY
						c.id ";
			//print_r($q);die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0)
				return $query->result();			
			else
				return null;
		}


		/*
		 * @Updated: 7/9/14
		 * @author: genes
		 */
		function getMyTuitionFee_Basic($student_histories_id, $acad_program_groups_id, $academic_terms_id,$withdraw=FALSE) {
			$result = NULL;	
			if(!$withdraw){
				$q1 = "SELECT 
							SUM(a.rate * n.paying_units) AS tuition_fee
						FROM 
							fees_schedule AS a,
							fees_groups AS b,
							fees_subgroups AS c,
							enrollments AS m,
							courses AS n,
							course_offerings AS o,
							student_histories AS p
						WHERE
							a.fees_subgroups_id = c.id
							AND b.id = c.fees_groups_id
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND b.id = 9
							AND p.id = m.student_history_id
							AND o.courses_id = n.id
							AND o.id=m.course_offerings_id
							AND m.student_history_id = {$this->db->escape($student_histories_id)}
							AND p.year_level = a.yr_level
							AND p.academic_terms_id = a.academic_terms_id
							AND n.id NOT IN (SELECT 
													aa.courses_id
												FROM
													tuition_others AS aa
												WHERE
													aa.academic_terms_id = {$this->db->escape($academic_terms_id)} 
													AND aa.yr_level = p.year_level)	";
			}else{
				$q1 = "SELECT 
							SUM(a.rate * n.paying_units) AS tuition_fee
						FROM 
							fees_schedule AS a,
							fees_groups AS b,
							fees_subgroups AS c,
							enrollments AS m,
							courses AS n,
							course_offerings AS o,
							student_histories AS p
						WHERE
							a.fees_subgroups_id = c.id
							AND b.id = c.fees_groups_id
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND b.id = 9
							AND p.id = m.student_history_id
							AND o.courses_id = n.id
							AND o.id=m.course_offerings_id
							AND m.student_history_id = {$this->db->escape($student_histories_id)}
							AND p.year_level = a.yr_level
							AND p.academic_terms_id = a.academic_terms_id
							AND m.withdraw_status != NULL
							AND n.id NOT IN (SELECT 
													aa.courses_id
												FROM
													tuition_others AS aa
												WHERE
													aa.academic_terms_id = {$this->db->escape($academic_terms_id)} 
													AND aa.yr_level = p.year_level)	";
			}	
			//print($q1);die();
			//log_message("INFO", print_r($q1,true)); // Toyet 7.3.2018
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			//print_r($result);
			//die();
			return $result;
		
		}


		function getMyTuitionFee_Others($student_histories_id, $academic_terms_id) {
			$result = NULL;	
			$q1 = "SELECT 
						SUM(a.rate * n.paying_units) AS tuition_fee
					FROM 
						tuition_others AS a,
						enrollments AS m,
						courses AS n,
						course_offerings AS o,
						student_histories AS p
					WHERE
						a.courses_id = n.id
						AND p.id = m.student_history_id
						AND o.courses_id = n.id
						AND o.id=m.course_offerings_id
						AND m.student_history_id = {$this->db->escape($student_histories_id)}
						AND p.year_level = a.yr_level
						AND p.academic_terms_id = a.academic_terms_id
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
					";
			//GROUP BY a.courses_id
			//print_r($q1); die();
			$query = $this->db->query($q1);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} else {
				$result = 0;
			}
			
			return $result;
		
		}

		/*
		 * ADDED: 6/11/15 by genes
		 */
		function getMyAffiliated_Fees($student_histories_id) {
			$result = NULL;
			$q1 = "SELECT
						a.rate
					FROM
						fees_affiliation AS a,
						fees_affiliation_courses AS b, 
						enrollments AS c,
						course_offerings AS d,
						student_histories AS e
					WHERE
						a.id = b.fees_affiliation_id
						AND b.courses_id=d.courses_id
						AND c.course_offerings_id = d.id
						AND e.academic_terms_id=d.academic_terms_id
						AND a.academic_terms_id=e.academic_terms_id
						AND e.id = c.student_history_id
						AND e.id = {$this->db->escape($student_histories_id)} 
					GROUP BY
						a.description,
						a.rate ";

			//die($q1);
			$query = $this->db->query($q1);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} else {
				$result = 0;
			}
			
			return $result;
		
		}
		
		
		function getMyTuitionFee_Prospectus($student_histories_id, $academic_terms_id) {
			$result = NULL;	
			$q1 = "SELECT 
						SUM(a.rate * n.paying_units) AS tuition_fee
					FROM 
						fees_schedule_prospectus_courses AS a,
						fees_schedule AS b,
						enrollments AS m,
						courses AS n,
						course_offerings AS o,
						student_histories AS p,
						prospectus_terms AS r,
						prospectus_courses AS s
					WHERE
						p.id = m.student_history_id
						AND o.id=m.course_offerings_id
						AND o.courses_id = n.id
						AND p.prospectus_id = r.prospectus_id 
						AND s.prospectus_terms_id = r.id
						AND s.id = a.prospectus_courses_id
						AND m.student_history_id = {$this->db->escape($student_histories_id)}
						AND a.fees_schedule_id = b.id
						AND p.year_level = b.yr_level
						AND p.academic_terms_id = b.academic_terms_id 
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
			
			//print($q1);
			//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}

		
		function getMyOtherFees($yr_level, $academic_terms_id, $acad_program_groups_id) {
			$result = NULL;	
			$q1 = "SELECT 
							SUM(a.rate) AS rate
						FROM 
							fees_schedule AS a,
							fees_groups AS b,
							fees_subgroups AS c
						WHERE
							a.fees_subgroups_id = c.id
							AND b.id = c.fees_groups_id
							AND a.yr_level = {$this->db->escape($yr_level)} 
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND b.id != 9";
			
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}

		function getMyLabFees($academic_years_id, $student_histories_id) {
			$result = NULL;	
			$q1 = "SELECT 
						SUM(rate) AS rate
					FROM
						(SELECT b.rate 
						FROM
							courses AS a,
							laboratory_fees AS b,
							enrollments AS c,
							course_offerings AS d
						WHERE
							a.id = b.courses_id
							AND a.id = d.courses_id
							AND c.course_offerings_id = d.id
							AND c.student_history_id = {$this->db->escape($student_histories_id)}
							AND b.academic_years_id = {$this->db->escape($academic_years_id)}
						GROUP BY
							a.id) AS rate ";
			//print($q1);
			//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}
		
		/*
		 * EDITED: 4/17/15 by genes
		 * included academic_programs
		 */
		function getCurrent_History($student_histories_id) {
			$result = NULL;	
			$q1 = "SELECT 
							a.id,
							a.academic_terms_id,
							c.id AS prospectus_terms_id,
							CONCAT(a.year_level,' ',d.abbreviation) AS year_course
						FROM
							student_histories AS a,
							prospectus AS b,
							prospectus_terms AS c,
							academic_programs AS d
						WHERE
							a.prospectus_id = b.id
							AND c.prospectus_id = b.id
							AND a.year_level = c.year_level
							AND b.academic_programs_id=d.id
							AND a.id = {$this->db->escape($student_histories_id)} 
						GROUP BY
							a.id";
			
			//die($q1);
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}

		
			function AssessYearLevel($student) {
			//$student is an associative array....
			//array('prospectus_id', 'yr_level', 'idno')
			$this->load->model('hnumis/Prospectus_Model'); // added by Justing... inorder for this method to be called by other methods...
			$this->load->model('hnumis/enrollments_model');
			$this->load->model('academic_terms_model');
			$this->load->model('teller/assessment_model');
			$data['can_update'] = 'N';
			//print_r($student); die();
				
			//get the current term
			$aterm = $this->academic_terms_model->current_academic_term($this->enrollments_model->academic_term_for_enrollment_date());
			
			$history = $this->getStudentHistory($student['student_histories_id']);
			
			$isAssessed = $this->assessment_model->CheckStudentHasAssessment($student['student_histories_id']);
			//print($isAssessed->id); die();
				
			if (!$isAssessed) {
				if ($history->academic_terms_id == $aterm->id) {
					$data['can_update'] = 'Y';
				}
			}
			
			$prospectus_terms = $this->Prospectus_Model->ListProspectusTerms($student['prospectus_id']);
			
			//get the total credit units the student must get to be promoted
			$max_credit = $this->Prospectus_Model->getMaxCreditByYear($student['prospectus_id'],$student['yr_level']);
			
			$credit_units=0;
			$credit_units_elective = 0;
			$total_elective = 0;

			//get the total credit units the student has enrolled and passed
			foreach ($prospectus_terms AS $term) {
				$courses = $this->Prospectus_Model->ShowProspectusGrades($term->id,$student['idno']);
				if ($courses) {
					foreach ($courses AS $course) {
						if ($course->elective == 'Y' AND $course->is_bracketed == 'N') {
							$total_elective = $total_elective + $course->credit_units;
						} 
											
						$credited_courses = $this->ListCreditedCourses($student['idno'], $course->prospectus_courses_id);

						if ($course->grade) {
							if ($course->is_bracketed == 'N') {
								$credit_units = $credit_units + $course->credit_units;
							}
						} else {
							$my_code="";
							if ($course->elective == 'Y') {
								$pattern = '/,/';
								$subject = $course->elective_grade;
								$my_code = preg_split($pattern, $subject);
								if (isset($my_code[1]) AND ($my_code[1] != 'HNU*')) {
									foreach($my_code AS $k=>$v) {
										$num=$k/3;
										if (floor($num) == $num) {
											$credit_units_elective = $credit_units_elective + $my_code[$k+2];
										}
									}
								}
							}
						}
					}
				}
				
			}

			if ($student['max_yr_level'] == $student['yr_level']) {
				$year_level = $student['yr_level'];
			} else {
				//yr_level will depend on the no. of passed units taken for the current year
				if (($credit_units + $credit_units_elective) >= $max_credit->max_credit_units) { 
					//do not include Senior High Students from automated +1 in year_level
					// by Toyet 10.23.2018
					if(!in_array($student['yr_level'],array(11,12))) {
						$year_level = $student['yr_level'] + 1;
					} else {
						$year_level = $student['yr_level'];
					}
				} else {
					$year_level = $student['yr_level'];
				}
			}
			
			$data['cnt_credit_taken'] 		= $credit_units + $credit_units_elective;
			$data['total_credits']    		= $max_credit->max_credit_units;
			$data['year_level']       		= $year_level;
			$data['credit_units_elective'] 	= $credit_units_elective;
			$data['total_elective'] 		= $total_elective;

			return $data;		
		}
						
		
		function AddHistory_ByPrivilege($data) {

			$query = "INSERT INTO student_histories 
							(	id, 
								students_idno,
								academic_terms_id, 
								prospectus_id, 
								block_sections_id, 
								year_level,
								can_enroll,
								inserted_by,
								inserted_on)
							VALUES ('',
									{$this->db->escape($data['students_idno'])},
									{$this->db->escape($data['academic_terms_id'])},
									{$this->db->escape($data['prospectus_id'])},
									{$this->db->escape($data['block_sections_id'])},
									{$this->db->escape($data['year_level'])},
									'Y',
									{$this->db->escape($data['inserted_by'])},
									NOW()
									)";

			//print($query);
			//die();
			$this->db->query($query);
			//$id = @mysql_insert_id();
			
			return $this->db->insert_id();
			
		}
		
		function getStudentHistory($student_histories_id = NULL) {
			//print_r($student_histories_id);die();
			if($student_histories_id == null) { return null; }			
			$result = null;			
			$q1 = "
					SELECT 
						a.*
					FROM 
						student_histories AS a
					WHERE 
						a.id = '{$student_histories_id}' 
				";
			//{$this->db->escape($student_histories_id)}
			$query = $this->db->query($q1);
			//print($q1);die();
			log_message("INFO","getStudentHistory  ==> ".print_r($q1,true)); // Toyet 10.23.2018
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 			
			return $result;		
		}
		
		//Added: 4/11/2012
		
		function withdrawCourseEnrolled($enrollment_id, $period, $user_id, $withdraw_per, $withdrawStat) {
			$q1 = NULL;

		//log_message("INFO", print_r($enrollment_id,true));  //toyet 4.16.2018
		//log_message("INFO", print_r($period,true));  //toyet 4.16.2018
		//log_message("INFO", print_r($user_id,true));  //toyet 4.16.2018
		//log_message("INFO", print_r($withdraw_per,true));  //toyet 4.16.2018
		//log_message("INFO", print_r($withdrawStat,true));  //toyet 4.16.2018
        
		if($withdrawStat){
			if($period == 'Prelim'){
				$q1 = "UPDATE enrollments
				 	   SET prelim_grade = 'WD', 
					   	   midterm_grade = 'WD',
						   finals_grade = 'WD',
						   status = 'Withdrawn',
						   withdrawn_on = NOW(),
						   withdrawn_by = {$this->db->escape($user_id)},
						   withdrawn_percent = $withdraw_per,
						   withdraw_status = {$this->db->escape($withdrawStat)}
						WHERE id = {$this->db->escape($enrollment_id)}";
			}elseif($period == 'Midterm'){
				$q1 = "UPDATE enrollments
				 	   SET midterm_grade = 'WD',
						   finals_grade = 'WD',
						   status = 'Withdrawn',
						   withdrawn_on = NOW(),
						   withdrawn_by = {$this->db->escape($user_id)},
						   withdrawn_percent = $withdraw_per,
						   withdraw_status = {$this->db->escape($withdrawStat)}
						WHERE id = {$this->db->escape($enrollment_id)}";
			}elseif(($period == 'Final') or ($period == 'Pre-Final')){
				$q1 = "UPDATE enrollments
				 	   SET finals_grade = 'WD',
							status = 'Withdrawn',
							withdrawn_on = NOW(),
							withdrawn_by = {$this->db->escape($user_id)},
							withdrawn_percent = $withdraw_per,
							withdraw_status = {$this->db->escape($withdrawStat)}
						WHERE id = {$this->db->escape($enrollment_id)}";
			}
		
		}else{
			
			if($period == 'Prelim'){
				$q1 = "UPDATE enrollments
				 	   SET prelim_grade = 'WD', 
					   	   midterm_grade = 'WD',
						   finals_grade = 'WD',
						   status = 'Withdrawn',
						   withdrawn_on = NOW(),
						   withdrawn_by = {$this->db->escape($user_id)},
						   withdrawn_percent = $withdraw_per
						WHERE id = {$this->db->escape($enrollment_id)}";
			}elseif($period == 'Midterm'){
				$q1 = "UPDATE enrollments
				 	   SET midterm_grade = 'WD',
						   finals_grade = 'WD',
						   status = 'Withdrawn',
						   withdrawn_on = NOW(),
						   withdrawn_by = {$this->db->escape($user_id)},
						   withdrawn_percent = $withdraw_per
						WHERE id = {$this->db->escape($enrollment_id)}";
			}elseif(($period == 'Final') or ($period == 'Pre-Final')){
				
				$q1 = "UPDATE enrollments
				 	   SET finals_grade = 'WD',
							status = 'Withdrawn',
							withdrawn_on = NOW(),
							withdrawn_by = {$this->db->escape($user_id)},
							withdrawn_percent = $withdraw_per
						WHERE id = {$this->db->escape($enrollment_id)}";
			}
		
		}	
			
		//print($q1);
		//log_message("INFO", print_r($q1,true));  //toyet 4.16.2018
		//die();
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		//Added: 4/12/2013
		
		function RemoveAllCourses($history_id) {
					
			$q1 = "DELETE FROM enrollments as a
						WHERE a.student_history_id={$this->db->escape($history_id)} ";

			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
			
		function WithdrawAllCourses($history_id, $period, $user) {
			$q1 = NULL;
			if($period->period == 'Prelim'){
				$q1 = "UPDATE enrollments
				 	   SET prelim_grade = 'WD', 
					   	   midterm_grade = 'WD',
						   finals_grade = 'WD',
						   status = 'Withdrawn',
						   withdrawn_on = NOW(),
						   withdrawn_by = {$this->db->escape($user_id)}
						WHERE enrollments.student_history_id = {$this->db->escape($history_id)}";
			}elseif($period->period == 'Midterm'){
				$q1 = "UPDATE enrollments
				 	   SET midterm_grade = 'WD',
						   finals_grade = 'WD',
						   status = 'Withdrawn',
						   withdrawn_on = NOW(),
						   withdrawn_by = {$this->db->escape($user_id)}
						WHERE enrollments.student_history_id = {$this->db->escape($history_id)}";
			}elseif($period->period == 'Final'){
				$q1 = "UPDATE enrollments
				 	   SET finals_grade = 'WD',
					   status = 'Withdrawn',
						   withdrawn_on = NOW(),
						   withdrawn_by = {$this->db->escape($user_id)}
					WHERE enrollments.student_history_id = {$this->db->escape($history_id)}";
			}
			//print($q1);
			//die();
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		 
		/**
		 * Assess student based on student's ID
		 * 
		 * @param int student_id
		 */
		public function assess_student_year_level_by_id ($student_id,$stud_info=NULL){
			//produce this array: array('prospectus_id', 'yr_level', 'idno', block_sections_id)
			//let us extract his latest history:
			$sql = "
				SELECT
					id,
					academic_terms_id,
					prospectus_id,
					year_level,
					block_sections_id
				FROM
					student_histories sh
				WHERE
					sh.students_idno={$this->db->escape($student_id)}
				ORDER BY
					academic_terms_id 
				DESC
				LIMIT 1
				";
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0){
				$row = $query->row();
				$this->load->model('academic_terms_model');
				//print("one: ".$this->academic_terms_model->current_academic_term()->id);
				//print("<p>two: ".$row->academic_terms_id); die();
				
				$ret = $this->AssessYearLevel(array(
													'prospectus_id'=>$row->prospectus_id,
													'yr_level'=>$row->year_level, 
													'idno'=>$student_id, 
													'max_yr_level'=>$stud_info['max_yr_level'],
													'student_histories_id'=>$row->id));
				
				//gets the new block_id with the same section name
				//Added by Amie on March 28, 2014 
				//$new_block = $this->getNewBlock($row->block_sections_id, $row->year_level);
				
				if($this->academic_terms_model->current_academic_term()->id == $row->academic_terms_id){
					//NOTE: get the next academic term
					$return = array('has_history_id'=>TRUE,'academic_terms_id'=>$row->academic_terms_id, 'history_id'=>$row->id, 'year_level' =>$ret['year_level'], 'prospectus_id'=>$row->prospectus_id);						
					//return array('has_history_id'=>TRUE, 'history_id'=>$row->id);
					return $return;
				} else {
					//return array('has_history_id'=>FALSE, 'academic_terms_id'=>$row->academic_terms_id, 'year_level' =>$ret['year_level'], 'prospectus_id'=>$row->prospectus_id);
					return array('has_history_id'=>FALSE, 'academic_terms_id'=>$row->academic_terms_id, 'history_id'=>$row->id, 'year_level' =>$ret['year_level'], 'prospectus_id'=>$row->prospectus_id);					
				}	
			} else {
				return FALSE;
			}
			
		}
		
		
	/*	private function getNewBlock($block_id, $year_level) {
			$resutl = NULL;
			$q = "SELECT n.id
					FROM block_sections n, block_sections o
					WHERE o.section_name = n.section_name
					AND o.academic_programs_id = n.academic_programs_id
					AND n.yr_level = o.yr_level + 1
					AND o.id = {$this->db->escape($block_id)}
					AND o.yr_level = {$this->db->escape($year_level)}";
			
			$query = $this->db->query($q);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}
*/		
		
		/**
		 * Use enrollments_model::is_new_enrollee() instead
		 * NOTE: Deprecated
		 * 
		 * @param unknown_type $student_id
		 * @return boolean|number
		 */
		public function is_new_enrollee($student_id){
			$sql = "
				SELECT count(*) as idcount
				FROM
					student_histories
				WHERE
					students_idno={$this->db->escape($student_id)}
				";
			$query = $this->db->query($sql);
			
			if($query && $query->num_rows()){
				$row = $query->row();
				if($row->idcount == 1){
					return TRUE;
				}else{
					return FALSE;
				}
			} else{
				return 0;
			}
		}
		
		//Added: 4/15/2013
		
			function DeleteAdvisedCourse($advised_course_id) {
					
			$q1 = "DELETE FROM advised_courses 
						WHERE id={$this->db->escape($advised_course_id)} ";

			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
		//UPDATED: 1/14/14 by genes
		//NOTE: prospectus_courses_id IS NEEDED
		function ListCreditedCourses($students_idno, $prospectus_courses_id = NULL) {
			$result = null;
			
			$q1 = "SELECT
						 cc.id,
						 IF(cc.enrollments_id IS NULL,os.catalog_no,c.course_code) AS cat_no,
						 IF(cc.enrollments_id IS NULL,'OTHER SCHOOL','HNU') AS taken_where,
						 IF(cc.enrollments_id IS NULL,os.descriptive_title,c.descriptive_title) 
						 	AS descriptive_title,  
						 IF(cc.enrollments_id IS NULL,os.units,c.credit_units) 
						 	AS units,
						 IF(cc.enrollments_id IS NULL,os.final_grade,e.finals_grade) 
						 	AS final_grade
					FROM 
						credited_courses AS cc
							LEFT JOIN enrollments AS e ON cc.enrollments_id=e.id
							LEFT JOIN course_offerings AS co ON co.id=e.course_offerings_id
							LEFT JOIN student_histories AS sh ON sh.id=e.student_history_id
							LEFT JOIN prospectus AS p ON p.id=sh.prospectus_id
							LEFT JOIN prospectus_terms AS pt ON pt.prospectus_id=p.id
							LEFT JOIN prospectus_courses AS pc ON pc.prospectus_terms_id=pt.id
							LEFT JOIN courses AS c ON c.id=co.courses_id
							LEFT JOIN other_schools_enrollments AS os
								ON os.id=cc.other_schools_enrollments_id  
					WHERE 
						cc.students_idno = {$this->db->escape($students_idno)}
						AND cc.prospectus_courses_id = {$this->db->escape($prospectus_courses_id)} 
					GROUP BY
						cc.id
					";
			//print($q1); die();
			//$q1 .= (is_null($prospectus_courses_id) ? "" : " AND cc.prospectus_courses_id = {$this->db->escape($prospectus_courses_id)}");

			
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}
		
		
			function getCreditedCourse($credited_course_id) {
					
			$q1 = "SELECT 
					      a.id,
						  a.grade, 
						  a.taken_here,
						  a.notes,
						  a.students_idno,
						  c.course_code	
					FROM 
						  credited_courses as a,
						  prospectus_courses as b,
						  courses as c
						WHERE
							a.id = {$this->db->escape($credited_course_id)}
							AND a.prospectus_courses_id = b.id
							AND b.courses_id = c.id";

			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} else $result = FALSE;
			
			return $result;		
		}
		
			function getAcadGroup($history_id) {
					
			$result = null;
			
			$q1 = "SELECT c.acad_program_groups_id
						FROM student_histories AS a,
							 prospectus as b,
							 academic_programs as c
						WHERE
							a.id={$this->db->escape($history_id)}
							AND a.prospectus_id=b.id
							AND	b.academic_programs_id = c.id";
			$query = $this->db->query($q1);

			//print($q1);
			//die();

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
		
		function AllowStudenToEnroll($data) {

			$q1 = "UPDATE 
							student_histories
						SET 
							year_level = {$this->db->escape($data['year_level'])},
							can_enroll = 'Y',
							updated_by = {$this->session->userdata('empno')},
							updated_on = now()
						WHERE
							id = {$this->db->escape($data['id'])} ";

			//print($q1);die();
			if ($this->db->query($q1))
				return TRUE;
			else
				return FALSE;
		
		}
		
	//Added: 5/2/2013
	
	function getStudentInfo($history_id){
			$result = 0;
			$q = "SELECT d.acad_program_groups_id,
						 b.year_level,
						 b.academic_terms_id,
						 b.students_idno
						
					FROM
						student_histories as b,
						prospectus as c,
						academic_programs as d
					WHERE
						 b.id = {$this->db->escape($history_id)}
						 AND b.prospectus_id = c.id
						 AND c.academic_programs_id = d.id";	 
	
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		
	}
	
	
	function getTuitionRateBasic($acad_prog_group_id, $acad_term_id, $yr_level){
			//log_message("INFO", print_r($acad_prog_group_id,true)); // Toyet 7.3.2018 	
			$result = 0;
			$q = "SELECT a.rate 
  					FROM fees_schedule as a,
        				 fees_subgroups as b, 
        				 fees_groups as c
					WHERE 
						a.acad_program_groups_id = {$this->db->escape($acad_prog_group_id)}
						AND a.academic_terms_id = {$this->db->escape($acad_term_id)}
						AND a.yr_level = {$this->db->escape($yr_level)}
						AND a.fees_subgroups_id = b.id 
						AND b.fees_groups_id = c.id
						AND c.id = 9
						AND a.levels_id = 6";	 
		//print($q);
		//die();
			//log_message("INFO", print_r($q,true)); // Toyet 7.3.2018 
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
	}
	
	
	public function getTuitionOthersRate($k, $academic_term_id, $student_year_level){
			$sql = "
				SELECT c.rate, 
      				b.courses_id 
    			FROM 
					 enrollments as a, 
         			 course_offerings as b, 
         			 tuition_others as c 
    			WHERE
					a.id = {$this->db->escape($k)}
					AND a.course_offerings_id = b.id 
					AND b.courses_id = c.courses_id 
					AND c.academic_terms_id = {$this->db->escape($academic_term_id)}
					AND c.yr_level = {$this->db->escape($student_year_level)}
					AND c.levels_id = 6	";
			
		//	print($sql);
		//	die();	
			$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
				return $result;
			} else{
				return FALSE;
			
			}			
		}
		
		public function getLabFee($k, $academic_year_id){
			$sql = "
				SELECT c.rate, 
      				b.courses_id 
    			FROM 
					 enrollments as a, 
         			 course_offerings as b, 
         			 laboratory_fees as c 
    			WHERE
					a.id = {$this->db->escape($k)}
					AND a.course_offerings_id = b.id 
					AND b.courses_id = c.courses_id 
					AND c.academic_years_id = {$this->db->escape($academic_year_id)}
					AND c.levels_id = 6	";
			
		//	print($sql);
		//	die();	
			$query = $this->db->query($sql);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
				return $result;
			} else{
				return FALSE;
			
			}			
		}

/****************************************************************
* The following methods are used to generate student certificate
*****************************************************************/

		//Added: 05/04/13 By genes
		//use to list all courses taken by student with rates
		function ListMyTuitionFees_Basic($student_histories_id) {
			$result = NULL;	
			$q1 = "SELECT 
						f.course_code,
						f.credit_units,
						f.paying_units,
						g.rate,
						if ((SELECT
								x.rate
							FROM
								tuition_others AS x
							WHERE
								x.courses_id = f.id
								AND x.academic_terms_id = g.academic_terms_id 
								AND x.levels_id = g.levels_id
								AND x.yr_level = g.yr_level) IS NULL,
							(f.paying_units*g.rate),f.paying_units*(SELECT
								x.rate
							FROM
								tuition_others AS x
							WHERE
								x.courses_id = f.id
								AND x.academic_terms_id = g.academic_terms_id 
								AND x.levels_id = g.levels_id
								AND x.yr_level = g.yr_level))
						AS tuition_fee
					FROM 
						enrollments AS a,
						course_offerings AS b,
						student_histories AS c,
						prospectus AS d,
						academic_programs AS e,
						courses AS f,
						fees_schedule AS g
					WHERE
						b.id = a.course_offerings_id 
						AND c.id = a.student_history_id
						AND c.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND f.id = b.courses_id
						AND c.id = {$this->db->escape($student_histories_id)} 
						AND g.fees_subgroups_id = 201
						AND g.acad_program_groups_id = e.acad_program_groups_id
						AND g.academic_terms_id = b.academic_terms_id 
						-- AND g.levels_id = 6
						AND g.yr_level = c.year_level 
					ORDER BY f.course_code;";
						
			//print($q1);
		//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->result();
			}
			
			return $result;
		
		}
		
		
		function getMyMatriculationFee($student_histories_id) {
			$result = NULL;	
			$q1 = "SELECT 
							'Matriculation' AS code,
							d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d
						WHERE
							a.prospectus_id = b.id
							AND a.id = {$this->db->escape($student_histories_id)}
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.fees_subgroups_id = 14
							AND d.academic_terms_id = a.academic_terms_id
							-- AND d.levels_id = 6
							AND d.yr_level = a.year_level
							AND d.posted = 'Y'	";
						
			//print($q1);
		//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
			
			return $result;
		
		}


		function ListMyMiscellaneous_Fees($student_histories_id) {
			$result = NULL;	
			$q1 = "SELECT 
							e.description,
							d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							fees_subgroups AS e
						WHERE
							a.prospectus_id = b.id
							AND a.id = {$this->db->escape($student_histories_id)}
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.academic_terms_id = a.academic_terms_id
							-- AND d.levels_id = 6
							AND d.yr_level = a.year_level
							AND d.posted = 'Y'
							AND d.fees_subgroups_id = e.id
							AND e.fees_groups_id = 2 
						ORDER BY
							e.description ";
						
			//print($q1);
		//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->result();
			}
			
			return $result;
		
		}


		function ListMyOther_Fees($student_histories_id) {
			$result = NULL;	
			$q1 = "SELECT 
							e.description,
							d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							fees_subgroups AS e
						WHERE
							a.prospectus_id = b.id
							AND a.id = {$this->db->escape($student_histories_id)}
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.academic_terms_id = a.academic_terms_id
							-- AND d.levels_id = 6
							AND d.yr_level = a.year_level
							AND d.posted = 'Y'
							AND d.fees_subgroups_id = e.id
							AND e.fees_groups_id = 3 
						ORDER BY
							e.description ";
						
			//print($q1);
		//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->result();
			}
			
			return $result;
		
		}

		
		function ListMyAddtlOther_Fees($student_histories_id) {
			$result = NULL;	
			$q1 = "SELECT 
							e.description,
							d.rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							fees_subgroups AS e
						WHERE
							a.prospectus_id = b.id
							AND a.id = {$this->db->escape($student_histories_id)}
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.academic_terms_id = a.academic_terms_id
							-- AND d.levels_id = 6
							AND d.yr_level = a.year_level
							AND d.posted = 'Y'
							AND d.fees_subgroups_id = e.id
							AND e.fees_groups_id = 4 
						ORDER BY
							e.description ";
						
			//print($q1);
		//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		
		}

		/*
		 * ADDED: 6/30/15 by genes
		 * the rate is summed so that students will not be able to see the details
		 */
		function getLearningResources_Fees($student_histories_id) {
			$result = NULL;
			$q1 = "SELECT
							'Learning Resources Fee' AS description,
							SUM(d.rate) AS rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							fees_subgroups AS e
						WHERE
							a.prospectus_id = b.id
							AND a.id = {$this->db->escape($student_histories_id)}
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.academic_terms_id = a.academic_terms_id
							AND d.yr_level = a.year_level
							AND d.posted = 'Y'
							AND d.fees_subgroups_id = e.id
							AND e.fees_groups_id = 10
						GROUP BY
							e.fees_groups_id ";
		
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}
		
		/*
		 * ADDED: 6/30/15 by genes
		* the rate is summed so that students will not be able to see the details
		*/
		function getStudentSupport_Fees($student_histories_id) {
			$result = NULL;
			$q1 = "SELECT
							'Student Support Services' AS description,
							SUM(d.rate) AS rate
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							fees_schedule AS d,
							fees_subgroups AS e
						WHERE
							a.prospectus_id = b.id
							AND a.id = {$this->db->escape($student_histories_id)}
							AND b.academic_programs_id = c.id
							AND c.acad_program_groups_id = d.acad_program_groups_id
							AND d.academic_terms_id = a.academic_terms_id
							AND d.yr_level = a.year_level
							AND d.posted = 'Y'
							AND d.fees_subgroups_id = e.id
							AND e.fees_groups_id = 11
						GROUP BY
							e.fees_groups_id ";
		
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}

		
		function ListMyLaboratory_Fees($student_histories_id) {
			$result = NULL;	
			$q1 = "SELECT 
							f.course_code,
							e.rate
						FROM
							student_histories AS a,
							course_offerings AS b,
							enrollments AS c,
							academic_terms AS d,
							laboratory_fees AS e,
							courses AS f
						WHERE
							a.id = c.student_history_id
							AND b.id = c.course_offerings_id
							AND a.academic_terms_id = d.id
							AND d.academic_years_id = e.academic_years_id
							-- AND e.levels_id = 6
							AND e.courses_id = f.id
							AND b.courses_id = e.courses_id
							AND a.id = {$this->db->escape($student_histories_id)}
						GROUP BY
							f.id
						ORDER BY
							f.course_code ";
						
		//	print($q1);
		//die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->result();
			}
			
			return $result;
		
		}
//Added: 5/4/2013

     function setStudentHistWD($history_id) {
		
			$q1 = "UPDATE student_histories
				 	   SET totally_withdrawn = 'Y'
   					WHERE id = {$this->db->escape($history_id)}";
		
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
	
//Added: 7/9/2013
	function createRemarks($stud_id, $user_id, $remark, $view_rights) {
		$query = "
				INSERT INTO remarks (students_idno, employees_empno, remark_date, remark, status, view_rights)
				VALUES 
					(
						{$this->db->escape($stud_id)},
						{$this->db->escape($user_id)},
						NOW(),
						{$this->db->escape($remark)}, 
						'A',
						'{$view_rights}'
					)
				";
			//print($query);die();
			$this->db->query($query);
			$id = @mysql_insert_id();			
			return $id;
		}

//Added: 7/10/2013
	function ListRemarks($student_id) {
			$result = NULL;
			$q1 = "SELECT
			a.id,
			DATE_FORMAT(a.remark_date, '%c-%d-%Y %h:%i %p') as remark_date, 
			a.remark 
			FROM
				remarks as a
			WHERE
			 a.students_idno = {$this->db->escape($student_id)}
			
			ORDER BY
			a.remark_date DESC";
		
			//print($q1);
			//die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
			
		return $result;
		
		}
		
	//updated: 7/15/2013
	
		function ListNewRemarks($student_id) {
			$result = NULL;
			$q1 = "SELECT
			a.id,
			DATE_FORMAT(a.remark_date, '%c-%d-%Y %h:%i %p') as remark_date, 
			a.remark
			FROM
			remarks as a
			WHERE
			a.students_idno = {$this->db->escape($student_id)}
			AND a.view_rights = 'Public'
				
			ORDER BY
			a.remark_date Desc";
		
			//print($q1);
			//die();
			$query1 = $this->db->query($q1);
		
			
			$q2 = "UPDATE remarks
			SET status = 'I'
			WHERE remarks.students_idno = {$this->db->escape($student_id)}";
			
			$query2 = $this->db->query($q2);
			
			if($query1 && $query1->num_rows() > 0 && $query2){
			   $result = $query1->result();
		}
				
			return $result;
		
		}
		
//Added: 7/10/2013
	function CheckForNewRemarks($student_id) {
			$result = NULL;
			$q1 = "SELECT
			a.id,
			a.remark_date,
			a.remark
			FROM
			remarks as a
			WHERE
			a.students_idno = {$this->db->escape($student_id)}
			AND a.status = 'A' ";
		
			//print($q1);
			//die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
			$result = $query->result();
		}
				
			return $result;
		
		}
		
//Added: 7/15/2013

		function getSutdentCollegeId($student_id) {
			$result = NULL;
			$q1 = "SELECT c.colleges_id
			
			FROM student_histories as a,
			     prospectus as b,
			     academic_programs as c
			WHERE
			  a.students_idno = {$this->db->escape($student_id)}
			  AND a.prospectus_id = b.id
			  AND b.academic_programs_id = c.id 
			
			ORDER BY a.id desc LIMIT 1	
			";
		
			//print($q1);
			//die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
			return $result;
		
		}
		
//Added: 7/23/2013

		function getRemark($remark_id) {
			$result = NULL;
			$q1 = "SELECT a.id, a.remark, DATE_FORMAT(a.remark_date, '%c-%d-%Y %h:%i %p') as remark_date, a.view_rights
			FROM remarks as a
			WHERE
			a.id = {$this->db->escape($remark_id)}
			
			";
		
			//print($q1);
			//die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
				
			return $result;
		
			}
			
	function editRemark($remarks_id, $remark, $view_rights) {			
			$q1 = "
				UPDATE remarks
				SET 
					remark 		= {$this->db->escape($remark)},
			    	view_rights = {$this->db->escape($view_rights)},
			    	updated_by 	= {$this->session->userdata('empno')},
			    	updated_on 	= NOW()
				WHERE 
					id = {$this->db->escape($remarks_id)}
			";
		
			if ($this->db->query($q1)) 
				return TRUE;
			else 
				return FALSE;
	}
					
	//Added: 7/24/2013
	function deleteRemark($remarks_id) {	
		$q1 = "
			DELETE FROM remarks
	 	    WHERE id = {$this->db->escape($remarks_id)}
	 	    LIMIT 1
		";		
		if ($this->db->query($q1))
		   return TRUE;
		else
		   return FALSE;						
	}
			
		
/************************************
* end for methods for student certificate
*************************************/
		
		function is_college_student($student_id){
			$this->db->select('id')->from('student_histories')->where('students_idno', $student_id);
			$query = $this->db->get();
			if ($query && $query->num_rows() > 0)
				return TRUE; else
				return FALSE;  	
		}
		
		
		//ADDED: 7/18/13 by genes
		function getUnits($student, $term) {
			
			$student_units['max_units_to_enroll']=0;
			$student_units['max_bracket_units']=0;
			
			$with_summer = $this->getCheckWithSummmer($student->prospectus_id, $student->year_level);

			if($term == "Summer" AND !$with_summer) {
				$max = $this->getMaxInSummmer($student->students_idno, $student->academic_terms_id);
				
				if ($max) {
					$student_units['max_units_to_enroll'] = $max->max_units;
					$student_units['max_bracket_units']   = 12;
				} else {
					$student_units['max_units_to_enroll'] = 9;
					$student_units['max_bracket_units']   = 12;
				}
			} else {
				$max = $this->getMaxUnits($student->id, $student->academic_terms_id);
				$student_units['max_units_to_enroll'] = $max ? $max->max_units_to_enroll:0 ;
				$student_units['max_bracket_units'] = $max  ? $max->max_bracket_units:0;
					
			}
				
			return $student_units;

		}
		
		/*@Added: 9/9/2013
		 *@updated: 8/22/14 by genes
		 *@notes: include academic_programs_id and year_level 
		*/
		function insert_reenrollment($sked_id, $academic_programs_id, $year_level, $amount_credited, $course_code) {
				
			$q1 = "INSERT INTO 
						re_enrollments 
							(
							enrollments_id,
							transaction_date,
							academic_programs_id,
							year_level,
							amount_credited,
							course_code
							)
						VALUES 
							(
							{$this->db->escape($sked_id)},
							NOW(),
							{$this->db->escape($academic_programs_id)},
							{$this->db->escape($year_level)},
							{$this->db->escape($amount_credited)},
							{$this->db->escape($course_code)}
							)";
				
			//print($q1);	die();
			if($this->db->query($q1))
				return $this->db->insert_id(); else
				return FALSE;
		}
		
	//Added: 9/16/2013

		function getAcadProgram($history_id) {
				
			$result = null;
				
			$q1 = "SELECT b.academic_programs_id,
						  c.max_yr_level 
					FROM student_histories AS a,
						 prospectus as b,
						 academic_programs as c
					WHERE
						a.id={$this->db->escape($history_id)}
						AND a.prospectus_id=b.id
						AND b.academic_programs_id = c.id";
			$query = $this->db->query($q1);
		
			//print($q1);
			//die();
		
			if($query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
				
		}
		
	//Added: 10/2/2013
	
	function getMyTuition_Basic_Withdrawn($student_histories_id, $acad_program_groups_id, $academic_terms_id) {
			$result = NULL;	
				$q1 = "SELECT 
							(a.rate * SUM(n.paying_units)) AS tuition_fee
						FROM 
							fees_schedule AS a,
							fees_groups AS b,
							fees_subgroups AS c,
							enrollments AS m,
							courses AS n,
							course_offerings AS o,
							student_histories AS p
						WHERE
							a.fees_subgroups_id = c.id
							AND b.id = c.fees_groups_id
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND b.id = 9
							AND p.id = m.student_history_id
							AND o.courses_id = n.id
							AND o.id=m.course_offerings_id
							AND m.student_history_id = {$this->db->escape($student_histories_id)}
							AND p.year_level = a.yr_level
							AND p.academic_terms_id = a.academic_terms_id
							AND m.withdraw_status = NULL
							AND n.id NOT IN (SELECT 
													aa.courses_id
												FROM
													tuition_others AS aa
												WHERE
													aa.levels_id = 6
													AND aa.academic_terms_id = {$this->db->escape($academic_terms_id)} 
													AND aa.yr_level = p.year_level)	";
			
		//	print($q1);
		//	die();
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
			
			return $result;
		
		}	
		
	//Added: 10/26/2013

		function GetPostedPrivilege($student_history_id){
			$result = NULL;
			$q1 = "SELECT
			a.id, b.scholarship,
			(c.discount_percentage * 100) as discount_percentage,
			a.posted,
			a.total_amount_availed
			FROM
			privileges_availed as a,
			scholarships as b,
			privileges_availed_details as c
			WHERE
			a.scholarships_id = b.id
			AND a.id = c.privileges_availed_id
			AND student_histories_id = {$this->db->escape($student_history_id)}
			AND a.posted = 'Y'";
		
			
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			return $result;
		}
		//Added: 10/28/2013
		
		function ListPastPrograms($student_no){
			$result = NULL;
			
			$q1 = "SELECT
				CASE b.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
							END AS term, 
				CONCAT(c.end_year-1,'-',c.end_year) AS sy,
				e.abbreviation,
				a.year_level
			FROM
				student_histories as a,
				academic_terms as b,
				academic_years as c,
				prospectus as d,
				academic_programs as e
			WHERE
			a.students_idno = {$this->db->escape($student_no)}
			AND a.academic_terms_id = b.id
			AND b.academic_years_id = c.id
			AND a.prospectus_id = d.id
			AND d.academic_programs_id = e.id
			AND a.id IN(SELECT hist.id 
						FROM enrollments,
							 student_histories as hist
						WHERE enrollments.student_history_id = hist.id )
			";
		
				
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
			$result = $query->result();
			}
			return $result;
		}
	//Added:  10/30/2013

	function getRe_enrolledUnitsBasic($history_id, $academic_terms_id){
		
			$result = NULL;
			$q1 = "SELECT SUM(x.paying_units) AS total_paying_units_enrolled
			FROM
				enrollments AS a,
				course_offerings AS b,
				student_histories as c,
				courses AS x
			WHERE
				a.student_history_id={$this->db->escape($history_id)}
				AND c.id = a.student_history_id
				AND a.course_offerings_id=b.id
				AND b.courses_id=x.id
				AND x.id IN (SELECT coff.courses_id 
							   FROM
							   		course_offerings as coff,
							   		enrollments as enroll,
							   		re_enrollments as re_en
							   	WHERE
							   		enroll.student_history_id = {$this->db->escape($history_id)}
							   		AND re_en.enrollments_id = enroll.id
							   		AND enroll.course_offerings_id = coff.id
									AND x.id NOT IN (SELECT 
														y.courses_id 
													FROM 
							   							tuition_others as y 
													WHERE 
													   y.yr_level = c.year_level
							   							AND y.academic_terms_id = {$this->db->escape($academic_terms_id)}
							   						AND y.levels_id = 6))";

		//	print($q1); die();
			$query = $this->db->query($q1);
		
			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
				
			return $result;
		
			}
			
			function getRe_enrolledUnitsOthers($history_id, $academic_terms_id){
			
				$result = NULL;
				$q1 = "SELECT SUM(x.paying_units) AS total_paying_units_enrolled
				FROM
				enrollments AS a,
				course_offerings AS b,
				student_histories as c,
				courses AS x
				WHERE
				a.student_history_id={$this->db->escape($history_id)}
				AND c.id = a.student_history_id
				AND a.course_offerings_id=b.id
				AND b.courses_id=x.id
				AND x.id IN (SELECT coff.courses_id
							FROM
								course_offerings as coff,
								enrollments as enroll,
								re_enrollments as re_en
							WHERE
								enroll.student_history_id = {$this->db->escape($history_id)}
								AND re_en.enrollments_id = enroll.id
								AND enroll.course_offerings_id = coff.id
							AND x.id IN (SELECT
												y.courses_id
											FROM
												tuition_others as y
											WHERE
												y.yr_level = c.year_level
												AND y.academic_terms_id = {$this->db->escape($academic_terms_id)}
												AND y.levels_id = 6))";
			
				//	print($q1); die();
				$query = $this->db->query($q1);
			
				if($query && $query->num_rows() > 0){
					$result = $query->row();
				}
			
				return $result;
			
		}	

		//Added:  10/31/2013
		//Added:  NSTP to the WHERE condition - Toyet 7.2.2018
		//        the subjects though follow the tuition fee basic rate.
		//        if that's the case and if no rate has been set in the
		//        tuition_others table, the query returns empty.
		//        As of 7.2.2018 (as Mam Cecil/Mam Tina), the cost of 
		//        these subjects shall now be based on the department
		//        tuition fee basic rate.
		//Edited: removed MS% to the WHERE condition, it conflicts with
		//        MS courses in Computer Studies (COECS) department - Toyet 7.3.2018
		//        --OR courses.course_code LIKE 'MS%'
		function checkIfWithCWTS($history_id, $academic_terms_id = NULL){
		
			$result = NULL;
			if( $academic_terms_id>=468) {
				$q1 = "SELECT x.paying_units, 0 as rate
					FROM
						enrollments AS a,
						course_offerings AS b,
						courses AS x
					WHERE
						a.student_history_id = {$this->db->escape($history_id)}
						AND a.course_offerings_id = b.id
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND b.courses_id = x.id
						AND x.id IN (SELECT
									courses.id
										FROM
											courses
										WHERE
										courses.course_code LIKE 'ROTC%'
										OR courses.course_code LIKE 'CWTS%'
									    OR courses.course_code LIKE 'NSTP%')";
			} else {
				if($academic_terms_id){
					$q1 = "SELECT x.paying_units, y.rate
					FROM
						enrollments AS a,
						course_offerings AS b,
						courses AS x
							LEFT JOIN tuition_others as y ON x.id = y.courses_id
					WHERE
						a.student_history_id = {$this->db->escape($history_id)}
						AND a.course_offerings_id = b.id
						AND y.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND b.courses_id = x.id
						AND x.id IN (SELECT
									courses.id
										FROM
											courses
										WHERE
										courses.course_code LIKE 'ROTC%'
										OR courses.course_code LIKE 'CWTS%'
									    OR courses.course_code LIKE 'NSTP%')";
				}else{
				$q1 = "SELECT x.paying_units, y.rate
				FROM
					enrollments AS a,
					course_offerings AS b,
					courses AS x 
						LEFT JOIN tuition_others as y ON x.id = y.courses_id
				WHERE
					a.student_history_id = {$this->db->escape($history_id)}
					AND a.course_offerings_id = b.id
					AND b.courses_id = x.id
					AND x.id IN (SELECT
									courses.id 
								 FROM 
								 	courses
								 WHERE
									 courses.course_code LIKE 'ROTC%'
									 OR courses.course_code LIKE 'CWTS%'
									 OR courses.course_code LIKE 'NSTP%')";
				}
			}
									 //--OR courses.course_code LIKE 'MS%'
			//print($q1); die();
			//log_message("INFO", print_r($q1,true)); // Toyet 7.2.2018
			$query = $this->db->query($q1);

		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
		return $result;
	}
	//justin
 	public function class_list($course_offerings_id){
 		$this->db->select("CONCAT(s.lname,', ',s.fname,' ',LEFT(s.mname,1)) AS neym, s.idno, co.section_code, c.descriptive_title, c.course_code, sh.year_level, ap.abbreviation, s.gender, CASE at.term WHEN 1 THEN '1st Semester' WHEN 2 THEN '2nd Semester' WHEN 3 THEN 'Summer' END AS term, CONCAT(ay.end_year-1,'-',ay.end_year) AS sy, CONCAT(emp.lname, ', ', emp.fname, ' ', LEFT(emp.mname, 1), '(', emp.empno, ')') as teacher, cos.schedule ", FALSE);
 		$this->db->from('enrollments e');
 		$this->db->join('course_offerings co', 'co.id=e.course_offerings_id', 'left');
 		$this->db->join('student_histories sh', 'sh.id=e.student_history_id', 'left');
 		$this->db->join('employees emp', 'co.employees_empno=emp.empno', 'left');
 		$this->db->join('students s', 's.idno=sh.students_idno', 'left');
 		$this->db->join('courses c', 'co.courses_id=c.id', 'left');
 		$this->db->join('prospectus p', 'p.id=sh.prospectus_id', 'left');
 		$this->db->join('academic_programs ap', 'ap.id=p.academic_programs_id', 'left');
 		$this->db->join('academic_terms at', 'at.id=co.academic_terms_id', 'left');
 		$this->db->join('academic_years ay', 'at.academic_years_id=ay.id', 'left');
 		$this->db->join("(SELECT cos.course_offerings_id, GROUP_CONCAT(CONCAT(rooms.room_no, ' ', cos.days_day_code, ' ', cos.start_time, '-', cos.end_time)) as schedule FROM course_offerings_slots cos LEFT JOIN rooms on rooms.id=cos.rooms_id WHERE cos.course_offerings_id={$course_offerings_id} GROUP BY cos.course_offerings_id) as cos", 'cos.course_offerings_id=co.id', 'left');
 		$this->db->order_by('s.lname, s.fname');
 		$this->db->where('co.id', $course_offerings_id);
 		$query = $this->db->get();
 		//echo $this->db->last_query(); die();
 		if($query->num_rows() > 0) {
 			return $query->result();
 		} else {
 			return $this->course_offering_details($course_offerings_id);
 		}
 	}

 	public function course_offering_details($course_offerings_id){
 		$this->db->select("co.section_code, c.descriptive_title, c.course_code, CASE at.term WHEN 1 THEN '1st Semester' WHEN 2 THEN '2nd Semester' WHEN 3 THEN 'Summer' END AS term, CONCAT(ay.end_year-1,'-',ay.end_year) AS sy, CONCAT(emp.lname, ', ', emp.fname, ' ', LEFT(emp.mname, 1), '(', emp.empno, ')') as teacher, cos.schedule ", FALSE);
 		$this->db->from('course_offerings co');
 		$this->db->join('courses c', 'co.courses_id=c.id', 'left');
 		$this->db->join('academic_terms at', 'at.id=co.academic_terms_id', 'left');
 		$this->db->join('employees emp', 'co.employees_empno=emp.empno', 'left');
 		$this->db->join('academic_years ay', 'at.academic_years_id=ay.id', 'left');
 		$this->db->join("(SELECT cos.course_offerings_id, GROUP_CONCAT(CONCAT(rooms.room_no, ' ' ,cos.days_day_code, ' ', cos.start_time, ' - ', cos.end_time)) as schedule FROM course_offerings_slots cos LEFT JOIN rooms on rooms.id=cos.rooms_id WHERE cos.course_offerings_id={$course_offerings_id} GROUP BY cos.course_offerings_id) as cos", 'cos.course_offerings_id=co.id', 'left');
 		$this->db->where('co.id', $course_offerings_id);
 		$query = $this->db->get();
 		
 		if($query->num_rows() > 0)
 			return $query->result(); else
 			return FALSE;
 	}
 	
 	//Added: 12/4/2013
 	
 	function cancelWithdrawnCourse($enrollment_id, $period) {
 		$q1 = NULL;
 	
 			if($period == 'Prelim'){
 				$q1 = "UPDATE enrollments
 				SET prelim_grade = NULL,
 				midterm_grade = NULL,
 				finals_grade = NULL,
 				status = 'Active',
 				withdrawn_on = NULL,
 				withdrawn_by = NULL,
 				withdrawn_percent = NULL,
 				withdraw_status = NULL
 				WHERE id = {$this->db->escape($enrollment_id)}";
 			}elseif($period == 'Midterm'){
 				$q1 = "UPDATE enrollments
 				SET midterm_grade = NULL,
 					finals_grade = NULL,
 					status = 'Active',
 					withdrawn_on = NULL,
 					withdrawn_by = NULL,
 					withdrawn_percent = NULL,
 					withdraw_status = NULL
 					WHERE id = {$this->db->escape($enrollment_id)}";
 			}elseif(($period == 'Final') or ($period == 'Pre-Final')){
 				$q1 = "UPDATE enrollments
 				SET finals_grade = NULL,
 						status = 'Active',
 								withdrawn_on = NULL,
 								withdrawn_by = NULL,
 								withdrawn_percent = NULL,
 								withdraw_status = NULL
 								WHERE id = {$this->db->escape($enrollment_id)}";
 			}
 			
 			if ($this->db->query($q1)) {
 				return TRUE;
 			} else {
 				return FALSE;
 			}
 	}	

 	//Added: 12/4/2013
 	
 	function unsetStudentHistWD($history_id) {
 	
 		$q1 = "UPDATE student_histories
 		SET totally_withdrawn = 'N'
 		WHERE id = {$this->db->escape($history_id)}";
 	
 		if ($this->db->query($q1)) {
 		return TRUE;
 	} else {
 			return FALSE;
 	}
 	}
 	
 	//Added: 12/7/2013
 	function get_basicEdLevel($id, $current_acadYear_id) {

 		$q1 = "SELECT a.level, a.id, b.id as history_id, b.yr_level, c.assessed_amount
 			FROM
 				levels AS a,
 				basic_ed_histories as b,
 				assessments_bed as c
 			WHERE
 				b.students_idno = {$this->db->escape($id)}
 				AND b.levels_id = a.id 
 				AND b.academic_years_id = {$this->db->escape($current_acadYear_id)}
 				AND b.id = c.basic_ed_histories_id
 				";
   // print_r($q1);die();
 		$query = $this->db->query($q1);
 		if($query->num_rows() > 0){
 			return $query->row();
 		} else
 			return FALSE;
 	}
 	//Added: 12/7/2013
 	
 	function isBasicEd($student_id, $current_year){
 		$q1 = "SELECT id
 			FROM
 				basic_ed_histories as b
 			WHERE
 				b.students_idno = {$this->db->escape($student_id)}
 				AND b.academic_years_id = {$this->db->escape($current_year)}	";
 		
 		$query = $this->db->query($q1);
 	
 		$stat = 0;
 		if($query->num_rows() > 0){
 			$result = $query->row();
 			$stat = 1;
 		}
	 	if($stat){
	 		return TRUE;
	 	}else{
	 		return FALSE;
	 	}	
 	
 	}
 	
 	//Added: January 3, 2014 by Isah
 	
 	function listOtherSchools()	{
 			
 		$result = null;
 	
 		$q = "SELECT
					a.id, a.school, a.school_address, b.name as town, c.name as province, d.name as country
				  FROM 
 					schools as a
 					LEFT JOIN towns as b on a.towns_id = b.id
 					LEFT JOIN provinces as c on b.provinces_id = c.id
 					LEFT JOIN countries as d on c.countries_id = d.id
 				 ORDER BY a.school";
 		//print($q); die();
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 	
 		return $result;
 	}
 	
 	
 	//ADDED: 01/16/14 by genes
 	function List_Student_TOR($student) {
 		$result = null;
 		
 		$q = "(SELECT
 					NULL AS towns_id,
 					NULL AS school_add,
 					a.id AS student_histories_id,
 					NULL AS other_schools_id, 
 					CASE b.term
						WHEN 1 THEN '1st Semester' 
						WHEN 2 THEN '2nd Semester'
						WHEN 3 THEN 'Summer'
					END AS term,
					e.description AS program_name,
 					CONCAT('S.Y. ',c.end_year-1,' - ',c.end_year) AS sy,
 					'Holy Name University' AS school_name,
 					a.prospectus_id AS prospectus_id,
 					c.end_year AS sy_year,
 					b.term AS sy_term,a.students_idno,'1' as what_skul
 				FROM
 					student_histories AS a,
 					academic_terms AS b,
 					academic_years AS c,
 					prospectus AS d,
 					academic_programs AS e,
 					acad_program_groups as apg
 				WHERE
 					a.academic_terms_id=b.id
 					AND b.academic_years_id=c.id
 					AND a.prospectus_id=d.id
 					AND d.academic_programs_id=e.id
 					AND a.students_idno = {$this->db->escape($student['idno'])}
 					AND b.term != 0
					AND apg.id=e.acad_program_groups_id
					AND (apg.levels_id is null or apg.levels_id<>12))

 				UNION
 				(SELECT
 					d.towns_id AS towns_id,
 					a.schools_id AS school_add,
 					NULL AS student_histories_id,
 					a.id AS other_schools_id,
 					c.term_description AS term,
 					a.program AS program_name,
 					CONCAT('S.Y. ',e.end_year-1,' - ',e.end_year) AS sy,
 					IF (f.id IS NULL,
 						d.school,
 						CONCAT(d.school,' - ',f.name,', ',g.name)) AS school_name,
 					NULL as prospectus_id,
 					e.end_year AS sy_year,
 					c.id AS sy_term,a.students_idno,'0' as what_skul
 				FROM
 					other_schools_student_histories AS a 
 						LEFT JOIN term AS c ON a.term_id=c.id
 						LEFT JOIN schools AS d ON a.schools_id=d.id
 						LEFT JOIN academic_years AS e ON a.academic_years_id=e.id
 						LEFT JOIN towns AS f ON d.towns_id=f.id
 						LEFT JOIN provinces AS g ON f.provinces_id=g.id
 				WHERE
 					a.students_idno = {$this->db->escape($student['idno'])}
 				)
 					
 				ORDER BY
 					sy_year,sy_term,what_skul ";
 		
 		//die($q);
 		log_message("INFO", print_r($q,true)); // Toyet 7.12.2018
 		$query = $this->db->query($q);
 		
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 		//print_r($result);die();
 		return $result; 		
 	}
 	

 	//ADDED: 01/16/14 by genes
 	function List_Student_TOR_Courses($student_histories_id, $other_schools_id, $prospectus_id) {
 		$result = null;

 		if ($prospectus_id != NULL) {
	 		$q = "SELECT
	 					'hnu' AS what_school, c.course_code, c.descriptive_title, a.finals_grade,
				 		if(!isnull(eo.id), 
				 			if(eo.is_bracketed != 'Y' OR eo.is_bracketed IS NULL, eo.credit_units,CONCAT('(',eo.credit_units,')')), 
				 			if(d.is_bracketed != 'Y' OR d.is_bracketed IS NULL, c.credit_units,CONCAT('(',c.credit_units,')')) 
							) AS credit_units, 				 		
				 		if(!isnull(eo.id), eo.is_bracketed, d.is_bracketed) AS is_bracketed,				 		
				 		e.prospectus_id, concat_ws('_','H',a.id) AS enr_id
			 		FROM
	 					enrollments AS a 
	 						LEFT JOIN enrollments_override eo ON eo.enrollments_id=a.id	 					
	 						LEFT JOIN course_offerings AS b ON a.course_offerings_id=b.id
	 						LEFT JOIN courses AS c ON b.courses_id=c.id
	 						LEFT JOIN prospectus_courses AS d ON c.id=d.courses_id
	 						LEFT JOIN prospectus_terms AS e ON d.prospectus_terms_id=e.id 
	 				WHERE
	 					a.student_history_id = {$this->db->escape($student_histories_id)}
	 				GROUP BY
	 					c.course_code
					ORDER BY
						c.course_code ";
 		} else {
 			$q = "SELECT
 						'other' AS what_school,
 						a.catalog_no AS course_code,
 						a.descriptive_title AS descriptive_title,
 						a.final_grade AS finals_grade,
 						a.units AS credit_units, concat_ws('_','O',a.id) AS enr_id
		 			FROM
			 			other_schools_enrollments AS a
 					WHERE
 						a.other_schools_student_histories_id = {$this->db->escape($other_schools_id)}
		 			ORDER BY
 						a.catalog_no ";
 		}
 			
 		//print($q); die();
 		$query = $this->db->query($q);
 			
 		if($query && $query->num_rows() > 0){
 		$result = $query->result();
 	}
 		
 	return $result;
 	}

 	
 	//ADDED: 01/21/14 by genes
 	function List_Taken_Programs($student) {
 		$result = null;
 			
 		$q = "SELECT
 					e.description AS program_name
 				FROM
 					student_histories AS a,
 					academic_terms AS b,
 					prospectus AS d,
 					academic_programs AS e
 				WHERE
			 		a.prospectus_id=d.id
			 		AND a.academic_terms_id=b.id
 					AND d.academic_programs_id=e.id
 					AND a.students_idno = {$this->db->escape($student['idno'])}
 					AND b.term != 0
 				GROUP BY
 					d.academic_programs_id
 				ORDER BY
 					a.academic_terms_id ";
 			
 		//print($q); die();
 		$query = $this->db->query($q);
 			
 		if($query && $query->num_rows() > 0){
 		$result = $query->result();
 	}
 		
 	return $result;
 	}
 	
 	//Added: 10/31/2013
 	
 	function checkIfWithWithdrawals($history_id){
 	
 		$result = NULL;
 		
 			$q1 = "SELECT a.id
 					FROM
 						enrollments AS a,
 						course_offerings AS b,
 						courses AS c
 		
 					WHERE
 						a.student_history_id = {$this->db->escape($history_id)}
 						AND a.course_offerings_id = b.id
 						AND b.courses_id = c.id
 						AND a.status = 'Withdrawn'";
 		
 			//print($q1); die();
 			$query = $this->db->query($q1);
 	
 			if($query && $query->num_rows() > 0){
 				$result = $query->result();
 			}
 				return $result;
 	}

 	
 	function AddStudentGraduated($student) {
 	
 		if ($student['taken_type'] == 'hnu') {
	 		$query = "INSERT INTO 
 							graduated_students 	(student_histories_id,graduation_statement)
				 		VALUES ({$this->db->escape($student['student_histories_id'])},
									{$this->db->escape($student['graduation_statement'])})";
 		} else {
 			$query = "INSERT INTO
 							graduated_students 	(other_schools_student_histories_id,graduation_statement,grad_type)
 							VALUES ({$this->db->escape($student['other_schools_id'])},
 									{$this->db->escape($student['graduation_statement'])},
									{$this->db->escape($student['grad_type'])})";
 		}
		//print($query); die();
		
 		if($this->db->query($query))
 			return $this->db->insert_id();
 		else
 			return FALSE;
 		
 	}
 	
 	
 	//ADDED: 1/25/2014 by Genes
 	function getGraduatedStatement($student_histories_id=null, $other_schools_id=null) {
 		$result = null;
 			
 		$q = "(SELECT
 						'hnu' AS what_school,
 						a.id,
 						a.graduation_statement,
 						d.description AS program_name,
 						d.max_yr_level AS max_yr_level,
 						d.colleges_id AS colleges_id
			 		FROM
 						graduated_students AS a,
 						student_histories AS b,
				 		prospectus AS c,
 						academic_programs AS d
 					WHERE
						a.student_histories_id=b.id
						AND b.prospectus_id=c.id
 						AND c.academic_programs_id=d.id
 						AND a.student_histories_id = {$this->db->escape($student_histories_id)}
 				)
 				UNION
 				(SELECT
 						'other' AS what_school,
 						a.id,
 						a.graduation_statement,
 						b.program AS program_name,
 						a.grad_type AS max_yr_level,
 						'NULL' AS colleges_id
			 		FROM
			 			graduated_students AS a,
 						other_schools_student_histories AS b
			 		WHERE
 						a.other_schools_student_histories_id=b.id
				 		AND a.other_schools_student_histories_id = {$this->db->escape($other_schools_id)}
 				) ";
 			
 		//print($q); die();
 		$query = $this->db->query($q);
 			
 		if($query && $query->num_rows() > 0){
 			$result = $query->row();
 		}
 			
 		return $result;
 			
 	}
 	
 	//ADDED: 01/31/14 by genes
 	function List_Graduated_Programs($student_idno,$with_others=FALSE) {
 		$result = null;
 	
 		if ($with_others) {
 			$q = "SELECT
			 			b.id,
			 			b.student_histories_id,
			 			b.other_schools_student_histories_id,
			 			b.graduation_statement,
			 			IF(b.student_histories_id IS NULL,'OTHER SCHOOL','HNU') AS what_school,
			 			IF(b.student_histories_id IS NULL,os.program,e.description) AS program_name,
			 			IF(b.student_histories_id IS NULL,
			 				te.term_description,
			 				CASE f.term
								WHEN 1 THEN '1st Semester' 
								WHEN 2 THEN '2nd Semester'
								WHEN 3 THEN 'Summer'
							END) AS term,
 						IF(b.student_histories_id IS NULL,
 							CONCAT(ay.end_year-1,'-',ay.end_year),						
 							CONCAT(g.end_year-1,'-',g.end_year)
 							) AS sy,
 						b.grad_type,
 						os.schools_id,
 						e.id AS academic_programs_id
			 		FROM
			 			graduated_students AS b LEFT JOIN student_histories AS a ON a.id=b.student_histories_id
			 				LEFT JOIN prospectus AS d ON a.prospectus_id=d.id
			 				LEFT JOIN academic_programs AS e ON d.academic_programs_id=e.id
			 				LEFT JOIN academic_terms AS f ON a.academic_terms_id=f.id
			 				LEFT JOIN academic_years AS g ON f.academic_years_id=g.id
			 				LEFT JOIN other_schools_student_histories AS os ON b.other_schools_student_histories_id=os.id
			 				LEFT JOIN term AS te ON te.id=os.term_id
			 				LEFT JOIN academic_years AS ay ON os.academic_years_id=ay.id
			 		WHERE
			 			a.students_idno = {$this->db->escape($student_idno)}
 						OR os.students_idno = {$this->db->escape($student_idno)}
 			 ";
			 			
 		} else {
	 		$q = "SELECT
				 		b.id,
				 		e.description AS program_name,
				 		b.graduation_statement,
				 		e.id AS academic_programs_id
				 	FROM
				 		student_histories AS a,
				 		graduated_students AS b,
				 		prospectus AS d,
				 		academic_programs AS e,
				 		academic_terms AS f,
				 		academic_years AS g
				 	WHERE
				 		a.id=b.student_histories_id
				 		AND a.prospectus_id=d.id
				 		AND d.academic_programs_id=e.id
				 		AND a.academic_terms_id=f.id
				 		AND f.academic_years_id=g.id
				 		AND a.students_idno = {$this->db->escape($student_idno)}
				 	ORDER BY
				 		a.academic_terms_id ";
 		}
 		
 		//print($q); die();
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 		$result = $query->result();
 	}
 		
 		return $result;
 	}
 	
 	
 	function List_Student_Programs($student) {
 		$result = null;

		$q = "SELECT
					program_name,
					a_term,
					b_term 
					
				FROM	
					((SELECT
							c.description AS program_name,
							a.academic_terms_id AS a_term,
							'S' AS b_term
						FROM
							student_histories AS a,
							prospectus AS b,
							academic_programs AS c,
							enrollments AS d
						WHERE
							a.prospectus_id = b.id
							AND b.academic_programs_id=c.id
							AND d.student_history_id=a.id
							AND a.students_idno = {$this->db->escape($student['idno'])}
						ORDER BY
							a.academic_terms_id DESC
						LIMIT 1)
					UNION
		 				(SELECT
					 			IF(b.student_histories_id IS NULL,os.program,e.description) AS program_name,
		 						f.term,te.id AS b_term
					 		FROM
					 			graduated_students AS b LEFT JOIN student_histories AS a ON a.id=b.student_histories_id
					 				LEFT JOIN prospectus AS d ON a.prospectus_id=d.id
					 				LEFT JOIN academic_programs AS e ON d.academic_programs_id=e.id
					 				LEFT JOIN academic_terms AS f ON a.academic_terms_id=f.id
					 				LEFT JOIN academic_years AS g ON f.academic_years_id=g.id
					 				LEFT JOIN other_schools_student_histories AS os ON b.other_schools_student_histories_id=os.id
					 				LEFT JOIN term AS te ON te.id=os.term_id
					 				LEFT JOIN academic_years AS ay ON os.academic_years_id=ay.id
					 		WHERE
					 			a.students_idno = {$this->db->escape($student['idno'])}
		 						OR os.students_idno = {$this->db->escape($student['idno'])}
		 				)) AS n 
			GROUP BY 
				program_name
			ORDER BY
				a_term DESC ";
 				
 		//print($q); die();
 		$query = $this->db->query($q);
 		
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 				
 		return $result;			
 		
 	}
 	
 	
 	//ADDED: 02/01/1014 by genes
 	function DeleteGraduated($id) {
 		
 		$query = "DELETE FROM
 						graduated_students
 					WHERE
 						id = {$this->db->escape($id)} ";
 		
 		if ($this->db->query($query)) {
	 		return TRUE;
 		} else {
 			return FALSE;
 		}
 		 		
 	}
 	
 	public function student_is_basic_ed ($idnum){
 		$this->db->select('id')
 			->from('basic_ed_histories')
 			->where('students_idno', $idnum);
 		$query = $this->db->get();
 		if($query->num_rows() > 0)
 			return TRUE; else
 			return FALSE;	
 	}
 	
 	//Added: 2/17/2014
 	
 	function ListReligions()	{
 	
 		$result = null;
 	
 		$q = "SELECT
		 		id,
 				religion,
		 		status
 			FROM
 				religions
 			ORDER BY id";
 		//print($q); die();
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 		$result = $query->result();
 	}
 	
 	return $result;
 	}
 	
 	function addReligion($data) {
 	
 		$q1 = "INSERT INTO
 					religions
 						(religion, status)
 				VALUES ({$this->db->escape($data['religion'])},
 						{$this->db->escape($data['status'])})";
 	
 		//die($q1);
 		if($this->db->query($q1))
 		return $this->db->insert_id();
 		else
 			return FALSE;
 	}
 	
 	function deleteReligion($id) {
 	
 		$query = "DELETE FROM
 					religions
 				WHERE
 					id = {$this->db->escape($id)} ";
 	
 		//print($query); die();
 	
 		if ($this->db->query($query)) {
 		return TRUE;
 	} else {
 	return FALSE;
 	}
 	 
 	}
 	
 	function editReligion($data) {
 	
 		$q1 = "UPDATE religions
 				SET religion = {$this->db->escape($data['religion'])},
 					status = {$this->db->escape($data['status'])}
 				WHERE id = {$this->db->escape($data['id'])}";
 	
 		if ($this->db->query($q1)) {
 		return TRUE;
 		} else {
 		return FALSE;
 		}
 		}

		//@EDITED: 2/13/15
		//@NOTES: added academic_programs_id
		//@EDITED: 3/18/15 added student_histories_id
 		function getStudent_SO($graduated_students_id) {
 			//print_r($student_histories_id);die();

 			if($graduated_students_id == null) { return null; }
 			
 			$result = null;
 			
 			$q1 = "SELECT
		 					if(g.grad_date IS NOT NULL,DATE_FORMAT(g.grad_date,'%M %e, %Y'),
		 						'[Graduation Date NOT Set]') AS grad_date,
		 					d.description,
		 					e.level,
		 					f.accreditor,
		 					f.acronym AS accreditor_acronym,
		 					d.colleges_id,
		 					n.name AS college_name,
		 					DATE_FORMAT(g.grad_date,'%c') AS month_diploma_numeric,	
		 					DATE_FORMAT(g.grad_date,'%M') AS month_diploma1,	
							CASE MONTH(g.grad_date)
								WHEN 1 THEN 'Enero' 
								WHEN 2 THEN 'Pebrero'
								WHEN 3 THEN 'Marso'
								WHEN 4 THEN 'Abril'
								WHEN 5 THEN 'Mayo'
								WHEN 6 THEN 'Hunyo'
								WHEN 7 THEN 'Hulyo'
								WHEN 8 THEN 'Agosto'
								WHEN 9 THEN 'Setyembre'
								WHEN 10 THEN 'Oktobre'
								WHEN 11 THEN 'Nobyembre'
								WHEN 10 THEN 'Disyembre'
							END AS month_diploma,
							DAYOFMONTH(g.grad_date) AS day_diploma,
							YEAR(g.grad_date) AS year_diploma,
							b.academic_terms_id,
							g.academic_terms_id,
							a.grad_type,
							d.id AS academic_programs_id,
							a.student_histories_id 
		 				FROM
		 					graduated_students AS a,
		 					student_histories AS b LEFT JOIN prospectus AS c ON b.prospectus_id = c.id
		 						LEFT JOIN academic_programs AS d ON c.academic_programs_id = d.id
		 						LEFT JOIN graduation_dates AS g ON (g.academic_programs_id = d.id
		 							AND b.academic_terms_id = g.academic_terms_id) 
		 						LEFT JOIN colleges AS n ON n.id = d.colleges_id
		 						LEFT JOIN accreditations AS e ON d.id=e.academic_programs_id
		 						LEFT JOIN accreditors AS f ON f.id=e.accreditors_id	
		 				WHERE
		 					a.student_histories_id = b.id
		 					AND a.id = {$this->db->escape($graduated_students_id)} ";
 		
								// edited by ra via genes 10/14/2015
		 						// LEFT JOIN graduation_dates AS g ON (g.colleges_id = d.colleges_id
		 						//	AND b.academic_terms_id = g.academic_terms_id) 
	
 			//print_r($q1); die();
 			$query = $this->db->query($q1);

 			if($query->num_rows() > 0){
 				$result = $query->row();
 			}
 		
 			return $result;
 		}

 		 			
 		//Added: 4/11/2014 by Isah
 		
 		function ListCitizenship()	{
 		
 			$result = null;
 		
 			$q = "SELECT
		 		id,
 				name as citizenship
 			FROM
 				citizenships
 			ORDER BY id";
 			//print($q); die();
 			$query = $this->db->query($q);
 		
 			if($query && $query->num_rows() > 0){
 				$result = $query->result();
 			}
 		
 			return $result;
 		}
 		
 		
 		//Added: 4/11/2014 by Isah
 		
 		function updatetBasicEdStudentInfo($data) {
 		
 			$q1 = "UPDATE students
 					SET
 						fname = {$this->db->escape($data['first'])},
 						lname = {$this->db->escape($data['last'])},
 						mname = {$this->db->escape($data['middle'])},
 						gender = {$this->db->escape($data['gender'])},
 						dbirth = {$this->db->escape($data['birthdate'])},
 						citizenships_id = {$this->db->escape($data['citizenship'])},
 						religions_id = {$this->db->escape($data['religion'])}
 					WHERE
 						idno = {$this->db->escape($data['students_idno'])}";
 			//print($q1); die();
 					if ($this->db->query($q1)) {
 						return TRUE;
 					} else {
 						return FALSE;
 					}
 		}		
 		
 		//Added: 4/12/2014 by Isah
 		
 		function ListBasicStudentsEnrolled($data) {
 			$result = NULL;
 				
 			$q = "SELECT
		 			a.id AS student_histories_id,
		 			a.students_idno,
		 			a.inserted_on,
		 			CONCAT(UPPER(b.lname),', ',CONCAT(UPPER(substring(b.fname,1,1)), LOWER(substring(b.fname,2,LENGTH(b.fname)))),' ',LEFT(b.mname,1)) AS neym, 
		 			c.section_name
		 			FROM
			 			basic_ed_histories AS a,
			 			students AS b,
			 			basic_ed_sections AS c
		 			WHERE
			 			a.academic_years_id = {$this->db->escape($data['cur_yr_id'])}
			 			AND a.yr_level = {$this->db->escape($data['yr_level'])}
			 			AND a.levels_id = {$this->db->escape($data['basic_level'])}
			 			AND a.students_idno = b.idno
			 			AND a.basic_ed_sections_id = c.id
			 			GROUP BY a.students_idno
			 			ORDER BY
			 			b.lname ";
 		
 			//print($q);
 			//die();
 			$query = $this->db->query($q);
 				
 			if($query->num_rows() > 0){
 			$result = $query->result();
 		}
 			
 		return $result;
 		}

 		/*
 		 * Added Aug 27, 2014
 		 * 
 		 */
 		function get_student_assessment_date($student_histories_id){
 			$sql = "
 				select ass.transaction_date
				from assessments ass
				where 
 					ass.student_histories_id={$this->db->escape($student_histories_id)}
 			";
 			$query = $this->db->query($sql); 		
 				
 			if($query && $query->num_rows() > 0) {
 				return $query->result();
 			}
			return false; 			
 		}
 		

 	/*
 	 * @ADDED: 3/21/15 by genes 
 	 */
 	function ListCollegeGraduates($college_id,$acad_prog_id,$acad_term) {
 		$result = null;

 		if ($acad_prog_id) {
			$q = "SELECT 
						CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1)) AS neym, 
						a.idno,
						f.abbreviation,
						c.year_level,
						a.gender
					FROM 
						students AS a, 
						student_histories AS c, 
						graduated_students AS d, 
						prospectus AS e, 
						academic_programs AS f
					WHERE
						a.idno=c.students_idno
						AND c.id=d.student_histories_id
						AND c.prospectus_id=e.id
						AND e.academic_programs_id=f.id  
						AND f.colleges_id = {$this->db->escape($college_id)}
						AND f.id = {$this->db->escape($acad_prog_id)}
						AND c.academic_terms_id = {$this->db->escape($acad_term)}
					ORDER BY
						a.lname, a.fname ";
 		} else {
			$q = "SELECT 
						CONCAT(a.lname,', ',a.fname,' ',LEFT(a.mname,1)) AS neym, 
						a.idno,
						f.abbreviation,
						c.year_level,
						a.gender
					FROM 
						students AS a, 
						student_histories AS c, 
						graduated_students AS d, 
						prospectus AS e, 
						academic_programs AS f
					WHERE
						a.idno=c.students_idno
						AND c.id=d.student_histories_id
						AND c.prospectus_id=e.id
						AND e.academic_programs_id=f.id  
						AND f.colleges_id = {$this->db->escape($college_id)}
						AND c.academic_terms_id = {$this->db->escape($acad_term)}
					ORDER BY
						a.lname, a.fname ";			
 		}
 		 				
 		//print($q); die();
 		$query = $this->db->query($q);
 		
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 				
 		return $result;			
 			
 	}
	
	
	/*	ADDED: 11/20/15 by genes
	*	For forced enrollment initially done by developers controller
	*/
	function Forced_Enroll_Student($data) {
		$id = NULL;
		
		$query = "INSERT INTO enrollments ( 
							student_history_id, 
							course_offerings_id,
							enrolled_by,
							date_enrolled,								
							ip_address,
							post_status)	
					VALUES (
							{$this->db->escape($data['student_history_id'])},
							{$this->db->escape($data['course_offerings_id'])},
							{$this->db->escape($data['enrolled_by'])},
							NOW(),
							{$this->db->escape($data['ip_address'])},
							{$this->db->escape($data['post_status'])}
							) ";
		//die($query);
		if ($this->db->query($query)) {
			$id = @mysql_insert_id();
		}
		
		return $id;
		
	}


	function getMyTotalUnpostedAssessment($student_histories_id, $acad_program_groups_id, $academic_terms_id, $yr_level, $academic_years_id) {
			$result = NULL;	
			
			$q1 = "SELECT sum(rate) AS rate 

					FROM 
						(
						(SELECT 
							SUM(a.rate * n.paying_units) AS rate
						FROM 
							fees_schedule AS a,
							fees_groups AS b,
							fees_subgroups AS c,
							enrollments AS m,
							courses AS n,
							course_offerings AS o,
							student_histories AS p
						WHERE
							a.fees_subgroups_id = c.id
							AND b.id = c.fees_groups_id
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND b.id = 9
							AND p.id = m.student_history_id
							AND o.courses_id = n.id
							AND o.id=m.course_offerings_id
							AND m.student_history_id = {$this->db->escape($student_histories_id)}
							AND p.year_level = a.yr_level
							AND p.academic_terms_id = a.academic_terms_id
							AND n.id NOT IN (SELECT 
													aa.courses_id
												FROM
													tuition_others AS aa
												WHERE
													aa.academic_terms_id = {$this->db->escape($academic_terms_id)} 
													AND aa.yr_level = p.year_level)
					) 
					
					UNION ALL
					
					(SELECT 
						SUM(a.rate * n.paying_units) AS rate
					FROM 
						tuition_others AS a,
						enrollments AS m,
						courses AS n,
						course_offerings AS o,
						student_histories AS p
					WHERE
						a.courses_id = n.id
						AND p.id = m.student_history_id
						AND o.courses_id = n.id
						AND o.id=m.course_offerings_id
						AND m.student_history_id = {$this->db->escape($student_histories_id)}
						AND p.year_level = a.yr_level
						AND p.academic_terms_id = a.academic_terms_id
						AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
					)
					
					UNION ALL

					(SELECT
						a.rate AS rate
					FROM
						fees_affiliation AS a,
						fees_affiliation_courses AS b, 
						enrollments AS c,
						course_offerings AS d,
						student_histories AS e
					WHERE
						a.id = b.fees_affiliation_id
						AND b.courses_id=d.courses_id
						AND c.course_offerings_id = d.id
						AND e.academic_terms_id=d.academic_terms_id
						AND a.academic_terms_id=e.academic_terms_id
						AND e.id = c.student_history_id
						AND e.id = {$this->db->escape($student_histories_id)} 
					GROUP BY
						a.description,
						a.rate
					)
					
					UNION ALL
					
					(SELECT 
						SUM(a.rate * n.paying_units) AS rate
					FROM 
						fees_schedule_prospectus_courses AS a,
						fees_schedule AS b,
						enrollments AS m,
						courses AS n,
						course_offerings AS o,
						student_histories AS p,
						prospectus_terms AS r,
						prospectus_courses AS s
					WHERE
						p.id = m.student_history_id
						AND o.id=m.course_offerings_id
						AND o.courses_id = n.id
						AND p.prospectus_id = r.prospectus_id 
						AND s.prospectus_terms_id = r.id
						AND s.id = a.prospectus_courses_id
						AND m.student_history_id = {$this->db->escape($student_histories_id)}
						AND a.fees_schedule_id = b.id
						AND p.year_level = b.yr_level
						AND p.academic_terms_id = b.academic_terms_id 
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} 
					)
					
					UNION ALL
					
					(SELECT 
							SUM(a.rate) AS rate
						FROM 
							fees_schedule AS a,
							fees_groups AS b,
							fees_subgroups AS c
						WHERE
							a.fees_subgroups_id = c.id
							AND b.id = c.fees_groups_id
							AND a.yr_level = {$this->db->escape($yr_level)} 
							AND a.academic_terms_id = {$this->db->escape($academic_terms_id)} 
							AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)}
							AND b.id != 9
					)
					
					UNION ALL
					
					(SELECT 
						SUM(rate) AS rate
					FROM
						(SELECT b.rate 
						FROM
							courses AS a,
							laboratory_fees AS b,
							enrollments AS c,
							course_offerings AS d
						WHERE
							a.id = b.courses_id
							AND a.id = d.courses_id
							AND c.course_offerings_id = d.id
							AND c.student_history_id = {$this->db->escape($student_histories_id)}
							AND b.academic_years_id = {$this->db->escape($academic_years_id)}
						GROUP BY
							a.id) AS rate
					)
					) s
													";
			//die($q1);										
			$query = $this->db->query($q1);

			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
													
		
	}
	

}
//end of student_model.php
