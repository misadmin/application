<?php
/*
$groups = array(
		'1'=>array(
			'name'		=>'tuition',
			'children' 	=> array(
					'100'=>'tuition'
			)
		),
		'2'=>array(
			'name'=>'miscellaneous', 
			'children' => array(
					'101'=>'misc1',
					'102'=>'misc2',
					'103'=>'misc3'
			)
		),
		'3'=>array(
			'name'=>'other fees',
			'children' => array(
					'101'=>'misc1',
					'102'=>'misc2',
					'103'=>'misc3'
			)	
		),
	);
	*/

//print_r($groups);
?>
<style>
.c
</style>
<script>
$( document ).ready(function() {
	$("li.groups").each(function() {
		$(this).next("ul").hide();
	});
	
    $("li.groups").bind('click', function() {
    	$(this).next("ul").toggle("slow");
    });
});
</script>

<ul>
<?php foreach($groups as $key=>$val): ?>
	<li class='groups' group-id="<?php echo $key ?>"><?php echo $val['name'] ?></li>
	<ul>
	<?php foreach($val['children'] as $child_key => $child): ?>
	<?php if( ! empty($child)): ?>
		<li child-id="<?php echo $child_key;?>"><?php echo $child; ?></li>
	<?php endif; ?>
		<?php endforeach; ?>
		<li><a class="add-item" parent_id="<?php echo $key;?>" label="Add <?php echo $val['name']?> Subgroup" type="subgroup" subgroup_id="<?php echo $child_key;?>">Add New Subgroup</a></li>
	</ul>
<?php endforeach; ?>
	<li><a id="add-group" label="Add New Group" type="group" class="add-item">Add New Group</a></li>
</ul>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="myModalLabel"></h3>
	</div>
	<div class="modal-body">
		<form method="post" class="form-horizontal" id="update_item_form">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" name="action" id="hidden_action" value="" />
			<fieldset>
				<div class="control-group formSep">
				</div>
				<div class="control-group formSep">
					<label class="control-label" for="description">Description</label>
					<div class="controls"><input type="text" id="description" name="description" placeholder="Description" /></div>
				</div>
			</fieldset>
		</form>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" id="update_item">Add Item</button>
	</div>
</div>

<script>

$('.add-item').bind('click', function(){
	$('#myModal').modal('show');
	$("#myModalLabel").html($(this).attr("label"));
	$("#update_item").html("Add "+$(this).attr("type"));
	$("#grouptype_id").remove();
	$("#hidden_action").val("new_"+$(this).attr("type"));

	var hiddenInput = $('<input/>',{type:'hidden',id:"grouptype_id",name:"type",value:$(this).attr("type")});
	hiddenInput.appendTo($("#update_item_form"));

	$("#parent_id").remove();
	if($(this).attr("type") == "subgroup"){
		hiddenInput = $('<input/>',{type:'hidden',id:"parent_id",name:"parent_id",value:$(this).attr("parent_id")});
		hiddenInput.appendTo($("#update_item_form"));
	}	
});

$("#update_item").bind("click",function(){
	$("#update_item_form").submit();
});

$('#update_item_form').validate({
	onkeyup: false,
	errorClass: 'error',
	validClass: 'valid',
	rules: {
		description: { required: true, minlength: 3 },
	},
	highlight: function(element) {
		$(element).closest('div').addClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	unhighlight: function(element) {
		$(element).closest('div').removeClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	
	errorPlacement: function(error, element) {
		$(element).closest('div').append(error);
	}
});
</script>