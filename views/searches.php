<div id="maincontainer" class="clearfix">
	<div class="row-fluid">
		<div class="span9">
			<div class="tabable">
				 <ul id="myTab" class="nav nav-tabs">
					<li <?php if($active=='searches') echo 'class="active"'; ?>><a href="#searches" data-toggle="tab">Twitter Search Terms</a></li>
					<li <?php if($active=='new_search') echo 'class="active"'; ?>><a href="#new_search" data-toggle="tab">New Search</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade <?php if($active=='searches') echo 'in active'; ?>" id="searches">
						<div class="row-fluid">
							<div class="span12">
								<?php if ($active=='searches' && !empty($message)) : ?>
									<div class="alert <?php echo $severity; ?>"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $message; ?></div>
								<?php endif; ?>
								<?php if ( count($searches) > 0 ) : ?>
								<table  class="table table-striped table-bordered table-condensed">
									 <thead><tr><th>Search Term</th><th>Last Processed</th><th>Status</th><th>Actions</th></tr></thead>
								<?php foreach ($searches as $search): ?>
									<tr>
										<td><a href="<?php echo site_url("/{$search->id}/1"); ?>"><?php echo $search->string; ?></a></td>
										<td><?php echo $search->last_processed; ?></td>
										<td><?php echo ucfirst($search->status); ?></td>
										<td><a href="<?php echo site_url("/{$search->id}/1"); ?>" title="view"><i class="icon-eye-open"></i></a>&nbsp;&nbsp;<a href="<?php echo site_url("/searches/delete/{$search->id}/{$this->common_functions->nonce()}"); ?>" title="Delete" onclick="if ( confirm( 'Are you sure you want to Delete this search term?' ) ) { return true;}return false;" ><i class="icon-trash"></i></a>&nbsp;&nbsp;<a href="<?php echo site_url("/searches/toggle/{$search->id}/{$this->common_functions->nonce()}"); ?>" title="Toggle"><i class="icon-off"></i></a></td>
									</tr>
								<?php endforeach; ?>
								</table>
								<?php else: ?>
								<h4></h4>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="tab-pane fade <?php if($active=='new_search') echo 'in active'; ?>" id="new_search">
						<div class="row-fluid">
							<div class="span12">
								<form id="search_form" method="post" action="<?php echo site_url('/searches/new_search'); ?>" class="form-horizontal">
									<?php $this->common_functions->insert_hidden_nonce(); ?>
									<?php if ($active=='new_search' && !empty($message)) : ?>
									<div class="alert <?php echo $severity; ?>"><button type="button" class="close" data-dismiss="alert">×</button><?php echo $message; ?></div>
									<?php endif; ?>
									<fieldset>
										<div class="control-group formSep">
											<label for="new_search" class="control-label">New Search Terms</label>
											<div class="controls">
												<input type="text" id="new_search" name="new_search" class="input-xlarge" value="" />
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button class="btn btn-primary" type="submit">Insert New Search</button>
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#search_form').validate({
				onkeyup: false,
				errorClass: 'error',
				validClass: 'valid',
				rules: {
					new_search: { required: true, minlength: 3, maxlength: 64 }
				},
				highlight: function(element) {
					$(element).closest('div').addClass("f_error");
								setTimeout(function() {
									boxHeight()
								}, 200)
							},
							unhighlight: function(element) {
								$(element).closest('div').removeClass("f_error");
								setTimeout(function() {
									boxHeight()
								}, 200)
							},
							errorPlacement: function(error, element) {
								$(element).closest('div').append(error);
							}
		});
	});

</script>
