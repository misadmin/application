
			<div style="padding:3px; overflow:auto; width:40%;">
					<table class="table table-hover table-bordered table-striped table-condensed" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:20%;">Detail</td>
								<td class="shs_header" style="width:70%;">Assigned</td>
								<td class="shs_header" style="width:10%;">Update</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Section Adviser</td>
								<td class="show_class_adviser">
									<?php print($section->class_adviser); ?>
								</td>
								<td style="text-align:center;">
									<a href="<?php print($section->id); ?>" 
										description="<?php print($section->description." - ".$section->section_name); ?>"
										academic_programs_id="<?php print($section->academic_programs_id); ?>"
										block_adviser="<?php print($section->block_adviser); ?>"
										class="assign_section_adviser" >
										<div id="show_assign_adviser_icon" >
											<i class="icon-edit"></i>
										</div>
									</a>
								</td>
							</tr>
							<tr>
								<td>Classroom</td>
								<td><?php print($section->room_no); ?></td>
								<td style="text-align:center;">
									<a href="<?php print($section->id); ?>" 
										description="<?php print($section->description." - ".$section->section_name); ?>"
										class="assign_room_no" >
										<div id="show_update_room_id_icon" >
											<i class="icon-edit"></i>
										</div>
									</a>
								</td>
							</tr>
							<tr>
								<td>Section Name</td>
								<td><?php print($section->section_name); ?></td>
								<td style="text-align:center;">
									<a href="<?php print($section->id); ?>" 
										description="<?php print($section->description." - ".$section->section_name); ?>"
										section_name="<?php print($section->section_name); ?>"
										class="update_section_name" >
										<div id="show_update_sectionname_icon" >
											<i class="icon-edit"></i>
										</div>
									</a>
								</td>
							</tr>
						</tbody>
					</table>
			</div>

			<?php 
				if ($assessment) {
			?>		
					<div style="font-size:14px; font-weight:bold; color:#E40808; margin-top:5px;">
						Strand cannot be updated anymore! Assessment already posted!
					</div>
			<?php 
				}
			?>
<!-- JQ for Assign Adviser -->
<script>
	$('.assign_section_adviser').click(function(event){
		event.preventDefault();

		var description 	= $(this).attr('description');
		var block_adviser 	= $(this).attr('block_adviser');
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id); ?>",
			data: {"action": "extract_teachers",
					"block_adviser": block_adviser
				},
			dataType: 'json',
			success: function(response) {													

				$('#modal_assign_section_adviser').modal('show');
					
				$('#section_text').val(description);
				$('#select_teachers').html(response.output); 
															
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
					
		});
						
	});
</script>				

	<div id="modal_assign_section_adviser" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="assign_adviser_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Assign Section Adviser</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="section_id_modal"></div>
						<?php
							$this->load->view('bed/sections/modal_view/assign_section_adviser_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Assign Adviser!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script>
		$('#assign_adviser_form').submit(function(e) {
			e.preventDefault();
			
			var block_adviser = $('#teacher_id').val();

			$('#modal_assign_section_adviser').modal('hide');

			$('#show_assign_adviser_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id)); ?>",
				data: {"block_adviser": block_adviser,
						"action": "assign_section_adviser" },
				dataType: 'json',
				success: function(response) {													
																			
					$('#show_section_management_page').html(response.output); 
					
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
				
			});
			
		});
	</script>				
	<!-- End for JQ Assign Adviser -->



<!-- JQ for Update Section Name -->
<script>
	$('.update_section_name').click(function(event){
		event.preventDefault();

		$('#modal_update_section_name').modal('show');
		
		$('#description_section_modal').val($(this).attr('description'));
		$('#section_name_modal').val($(this).attr('section_name'));
						
	});
</script>				

	<div id="modal_update_section_name" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_section_name_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Section Name</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="section_id_modal"></div>
						<?php
							$this->load->view('bed/sections/modal_view/update_section_name_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update Section Name!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script>
	$('#update_section_name_form').submit(function(e) {
		e.preventDefault();
		
		var section_name = $('#section_name_modal').val();

		$('#modal_update_section_name').modal('hide');

		$('#show_update_sectionname_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;'/>")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id)); ?>",
			data: {"section_name": section_name,
					"action": "update_section_name" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_section_management_page').html(response.output); 
				
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
			
		});
		
	});
</script>				
<!-- End for JQ Update Section Name -->


<!-- JQ for Assign Room No. -->
	<script>
		$('.assign_room_no').click(function(event){
			event.preventDefault();

			var description = $(this).attr('description');
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id)); ?>",
				data: {"action": "extract_bed_rooms" },
				dataType: 'json',
				success: function(response) {
					
					$('#modal_assign_room_no').modal('show');
					
					$('#description_assign_room').val(description);
					$('#vacant_rooms').html(response.bed_buildings);														

				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
				
			});
							
		});
	</script>

	<div id="modal_assign_room_no" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="assign_room_no_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Assign Classroom</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="section_id_modal"></div>
						<?php
							$this->load->view('bed/sections/modal_view/assign_room_no_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Assign Room!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<script>
		$('#assign_room_no_form').submit(function(e) {
			e.preventDefault();
			
			var rooms_id = $('input[name=room_id]:checked').val();

			$('#modal_assign_room_no').modal('hide');

			$('#show_update_room_id_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;'/>")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/detailed_section_management/'.$section_id));?>",
				data: {"rooms_id": rooms_id,
						"action": "assign_room_no" },
				dataType: 'json',
				success: function(response) {													
					
					$('#show_section_management_page').html(response.output); 
					
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
				
			});
			
		});
	</script>				
	
	