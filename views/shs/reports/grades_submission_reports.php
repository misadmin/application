

			<div style="width:100%; overflow:auto; padding-top:5px;" >
				<?php 
					if ($teachers) {
						foreach($teachers AS $teacher) {
				?>			
						<div style="font-size:16px; font-weight:bold; color:#030B3D; ">
							<?php 
								print("[".$teacher->employees_empno."] ".$teacher->name); 
							?>
						</div>	
						
						<div style="margin-top:5px; margin-bottom:30px;">
							<table class="table table-hover table-bordered table-striped" >
								<thead>
									<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
										<td class="shs_header" style="width:8%;">Strand-<br>Grade Level</td>
										<td class="shs_header" style="width:7%;">Section</td>
										<td class="shs_header" style="width:7%;">Course<br>Code</td>
										<td class="shs_header" style="width:24%;">Descriptive Title</td>
										<td class="shs_header" style="width:17%;">Class<br>Adviser</td>
										<td class="shs_header" style="width:5%;">No. of<br>Enrollees</td>
										<td class="shs_header" style="width:8%;">Midterm<br>Submitted</td>
										<td class="shs_header" style="width:8%;">Finals<br>Submitted</td>
										<td class="shs_header" style="width:8%;">Midterm<br>Confirmed</td>
										<td class="shs_header" style="width:8%;">Finals<br>Confirmed</td>
									</tr>
								</thead>
								<tbody>
									<?php
										if ($teacher->grades) {
											foreach($teacher->grades AS $grade) {
									?>	
												<tr>
													<td style="vertical-align:middle;"><?php print($grade->abbreviation.'-Grade '.$grade->grade_level); ?></td>
													<td style="vertical-align:middle; text-align:center;">
														<?php print($grade->section_name); ?>
													</td>
													<td style="vertical-align:middle; text-align:center;"><?php print($grade->course_code); ?></td>
													<td style="vertical-align:middle;"><?php print($grade->descriptive_title); ?></td>
													<td style="vertical-align:middle;"><?php print($grade->block_adviser); ?></td>
													<td style="vertical-align:middle; text-align:center;"><?php print($grade->num_enrollees); ?></td>
													<td style="vertical-align:middle; text-align:center;"><?php print($grade->midterm_date); ?></td>
													<td style="vertical-align:middle; text-align:center;"><?php print($grade->finals_date); ?></td>
													<td style="vertical-align:middle; text-align:center;"><?php print($grade->midterm_confirm_date); ?></td>
													<td style="vertical-align:middle; text-align:center;"><?php print($grade->finals_confirm_date); ?></td>
												</tr>

									<?php 
											}
										}
									?>
								</tbody>
							</table>	
						</div>
				<?php 
						}
					}
				?>
			</div>

			
