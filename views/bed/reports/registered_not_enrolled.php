
	<div id="rounded_header" style="height:32px;">

		<div style="float:right;">
			<div style="float:left;">
				<select name="prospectus_id" class="levels_id" id="prospectus_id" style="margin:0px; margin-right: 10px;">
					<?php
						foreach($prospectuses AS $prospectus) {
					?>
							<option value="<?php print($prospectus->id); ?>" >
									<?php print($prospectus->description); ?> </option>	
					<?php 
						}
					?>					
				</select>
			
			</div>
			
			<div style="float:left;">
				<select name="academic_year_id" class="levels_id" id="academic_year_id" style="margin:0px;">
					<?php
						foreach($school_years AS $school_year) {
					?>
							<option value="<?php print($school_year->id); ?>" >
									<?php print($school_year->sy); ?> </option>	
					<?php 
						}
					?>					
				</select>
			
			</div>
			
		</div>

	</div>

	<div id="show_extracted_students">
		<?php 
			$this->load->view('bed/reports/students_enrolments');
		?>
	</div>
	
	
	<script>
		$(document).ready(function(){
			$('.levels_id').bind('change', function(){
				
				var prospectus_id 		= $("select#prospectus_id option:selected").attr('value');
				var description 		= $("select#prospectus_id option:selected").text();
				var academic_year_id 	= $("select#academic_year_id option:selected").attr('value');
				var display_sy 			= $("select#academic_year_id option:selected").text();
				
				show_loading_msg();
				
				$.ajax({
					cache: false,
					type: 'POST',
					url: "<?php echo site_url($this->uri->segment(1).'/registered_not_enrolled'); ?>",
					data: {"prospectus_id": prospectus_id,
							"description": description,
							"academic_year_id": academic_year_id,
							"display_sy": display_sy,
							"action": "list_selected" 
						},
					dataType: 'json',
					success: function(response) {													

						$('#show_extracted_students').html(response.output); 
						
						$(".loading").hide();
						
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
				});
				
			});
		});
	</script>

