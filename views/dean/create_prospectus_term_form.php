<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

<form action="<?php echo site_url('dean/prospectus');?>" method="post">
<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="step" value="7" />
  <?php echo validation_errors(); 
  ?><div style="background:#FFF; ">
  <table align="center" cellpadding="0" cellspacing="0" style="width:40%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head">
  NEW PROSPECTUS TERM
  </td>
  </tr>
  <tr>
    <td colspan="3">
  
  <table border="0" cellpadding="2" cellspacing="0" class="inside">
<tr>
  <td colspan="3" align="left" valign="middle" style="font-weight:bold; font-size:14px;"><?php print($prospectus->description." - ".$prospectus->effective_year); ?></td>
  </tr>
<tr>
  <td width="107" align="left" valign="middle">Year Level</td>
  <td width="6" align="left" valign="middle">:</td>
  <td width="401" align="left" valign="middle">
  <select name="year_level" id="select" class="form">
		<option value="1" selected="selected">1st Year</option>
		<option value="2">2nd Year</option>
		<option value="3">3rd Year</option>
		<option value="4">4th Year</option>
		<option value="5">5th Year</option>	
    </select></td>
</tr>
<tr>
  <td align="left" valign="middle">Term</td>
  <td align="left" valign="middle">:</td>
  <td align="left" valign="middle"><select name="term" id="year_level" class="form">
    <option value="1" selected="selected">1st Semester</option>
    <option value="2">2nd Semester</option>
    <option value="3">Summer</option>
  </select></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><input type="submit" name="button" id="button" value="Continue!"  class="btn btn-success" /></td>
</tr>
    </table>
    </td>
    </tr>
    </table>
    </div>

</form>