<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>

<div id="show_track_management_page" >
	<h2 class="heading">Track Management</h2>
	<div id="rounded_header" style="width:99%; font-size:12px; margin:0 auto; margin-top:15px; padding:0px;">
		<div id="container">
			<div id="left" >
				<div id="show_error_msg" style="padding-left:10px; padding-top:10px; color:#F92424; font-weight:bold; font-size:14px;">
					
				</div>
			</div>
			<div id="right">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<div style="float:left; padding-top:4px; padding-right:4px; font-size:14px;" >
					<a href="#" class="btn btn-success add_track" >
						<i class="icon-plus-sign" style="margin-top:2px;"></i> New Track
					</a>
				</div>
			</div>
		</div>
	</div>

	
	<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_items" >
		<?php
			$data = array(
					"tracks"=>$tracks,
			);
			
			$this->load->view('shs/tracks/list_all_tracks',$data);					
		?>
	</div>
</div>



<script>
	$('.add_track').click(function(event){
		
		$('#modal_new_track').modal('show');
		
		$('#abbreviation_modal').focus();

		$('#abbreviation_modal').val('');
		$('#description_modal').val('');
		$('#num_year_modal').val(2);
		
	});
</script>				


	<div id="modal_new_track" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_track_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">New Track</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<?php														
							$this->load->view('shs/tracks/modal_view/new_track_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Track!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<!-- JQ to submit an update to a volunteer -->
<script>
	$('#add_track_form').submit(function(e) {
		e.preventDefault();
		
		var abbreviation = $('#abbreviation_modal').val();
		var group_name   = $('#description_modal').val();
		var num_year     = $('#num_year_modal').val();

		$('#modal_new_track').modal('hide');
		
		$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/track_management'); ?>",
			data: {"abbreviation": abbreviation,
					"group_name": group_name,
					"num_year": num_year,
					"action": "add_track" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_items').html(response.output); 
				
				$('#show_add_icon').html('');
				
			}
		});
		
	});
</script>				

