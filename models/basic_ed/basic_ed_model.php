<?php

class Basic_ed_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function exam_dates ($year_level, $levels_id, $academic_years_id=null){
		
		$this->db->select ('periods_bed.exam_number, periods_bed.start_date, periods_bed.end_date', FALSE)
			->from('periods_bed');
		if(is_null($academic_years_id)){
			$this->db->join('academic_years', 'academic_years.id=periods_bed.academic_years_id', 'left');
			$this->db->where('academic_years.status', 'current');
		} else {
			$this->db->where('periods_bed.academic_years_id', $academic_years_id);
		}
		$this->db->where('periods_bed.levels_id', $levels_id);
		$this->db->like('periods_bed.year_level', $year_level, 'none');
		$this->db->order_by('periods_bed.exam_number');  //added by Toyet 10/22/2018
		
		if($query = $this->db->get()){
			return $query->result();
		} else 
			return array();
	}
	
	//returns integer
	public function current_exam ($year_level, $levels_id){
/**
 * why the hell this gets error?
 * Fatal error: Call to a member function row() on a non-object
 * Usting please explain! hahahahah 
 *		$start_date = (int) str_replace('-', '', $this->db->select('start_date')
 *				->from('academic_years')
 *				->where('status', 'current')
 *				->get()->row()->start_date
 *			);
**/	
		// so i replace it with traditional ninja move and it works
		//$start_date = (int) str_replace('-', '', $this->get_start_date()[0]->start_date);
		$start_date = (int) str_replace('-', '', $this->get_start_date()->start_date);
		//print("START DATE: ".$start_date); die();
		$this->db->select ('periods_bed.exam_number, periods_bed.start_date, periods_bed.end_date', FALSE)
			->from ('periods_bed')
			->join ('academic_years', 'academic_years.id=periods_bed.academic_years_id', 'left')
			->where('periods_bed.levels_id', $levels_id)
			->like('periods_bed.year_level', $year_level, 'none')
			->where('academic_years.status', 'current')
			->order_by('periods_bed.exam_number');
		$now = (int)date('Ymd');
		if($now==$start_date)
			return 1;
		if($query = $this->db->get()){
			//echo $this->db->last_query(); die('basic_ed/Basic_ed_model 55');
			$sql = $this->db->last_query();
			//log_message("INFO", print_r($sql,true)); // Toyet 7.23.2018
			foreach($query->result() as $row){
				if ($now > $start_date && $now <= (int)str_replace('-', '', $row->end_date))
					return $row->exam_number;
				$start_date = (int)str_replace('-', '', $row->end_date);
			}
			return isset($row->exam_number) ? $row->exam_number : 1;	
		} else
			return 1; //default on assumption that if dates not yet set, it will point to first exam...
	}
	
	public function get_start_date(){		
		$sql = "
				select start_date
				from academic_years 
				where status='current'			
		";
		$query = $this->db->query($sql);		
		if($query && $query->num_rows() > 0){
			return $query->row();	
		}
		return FALSE;
	}
	
	public function academic_year($id=null){
		$this->db->select('id, end_year, start_date')
			->from('academic_years');
		if(is_null($id))
			$this->db->where('status', 'current');	
		else
			$this->db->where('id', $id);

		$this->db->limit(1);
		return $this->db->get()->row();
	}
}