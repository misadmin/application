<script type="text/javascript">
function hide(obj)
  {
	return false;
    var obj1 = document.getElementById(obj);
     // console.log("style: "/*+typeof(obj1.style)*/);
      if(typeof(obj1)=='object')
    	  obj1.style.display = 'none'; else
          return false;
      
  }
function show(obj)
  {
	  return false;
      var obj1 = document.getElementById(obj);
      //console.log("style: "+typeof(obj1.style));
      if(typeof(obj1)=='object')
    	  obj1.style.display = 'table';
          return false;
      
  }

function show_permission()
{
	show('permission');
}
function hide_permission()
{
	hide('permission');
}
</script>

<body onLoad="hide_permission();">
<h2 class="heading" style="margin-top:60px;">List Graduation Schedule</h2>
<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<form id="addTown" method="post" action="" class="form-horizontal">
				<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" name="step" value="2" />
			
			<div class="control-group formSep">
				<div class="control-label">Academic Term:</div>
				<div class="controls">
					<select name="acad_term_id" id="acad_term_id">
							<option value="">---Select Academic Term---</option>											
								<?php						
									foreach($academic_terms AS $academic_term) {
											echo "<option value=".$academic_term->id.">" . $academic_term->term . " ".  $academic_term->sy ."</option>";	
									}
								?>					
					</select> 
				</div>
			</div>
		
			
		<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					 <button class="btn btn-success" type="submit">List Graduation Schedule!</button> 
				</div>
		</div>
		</form>
		</div>
	</div>
</div>
</body>


<script>
	$(document).ready(function(){
		$('#da1').datepicker();
	
	});
</script>

<script>
	$(document).ready(function(){
		$('#da2').datepicker();
	
	});
</script>

<script>
	$(document).ready(function(){
		$('#da3').datepicker();
	
	});
</script>

<script>
$(function () {
    $('.checkall').on('click', function () {
        $(this).closest('fieldset').find(':checkbox').prop('checked', this.checked);
		show_permission();
    });
});
</script>

<script>
$('.uncheck').click(function(){
      $('.checkall').attr('checked', false);
	  hide_permission();
})
</script>





  