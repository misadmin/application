<?php 

class Ajax extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library ('session');
/**		$names = [['Justin', 'Villocido'], ['Tata', 'Respecia']];
		foreach($names as list($firstname, $lastname)){
			echo "{$firstname}, {$lastname}";
		} **/
		if ( $this->session->userdata('logged_in') == FALSE  ) {
			echo ("Direct Access Denied...");//this should be on a view...
			die();
		}
		
		if ( $this->config->item('ajax_strict')) {
			if ( ! $this->input->is_ajax_request()) {
				echo ("Direct Access Denied...");//this should be on a view...
				die();
			}
		}
		
	}
	
	public function search_student(){
		$str = $this->input->get('query');
		//print($str);
		//die();
		$this->load->model('student_common_model');
		$result = $this->student_common_model->search($str);
	
		$res = array();
		if ($result && count($result) > 0) {
			foreach ($result as $row){
				$res[] = $row->fullname;
			}
		}
		$return = array(
				'query'			=> $str,
				'suggestions'	=> $res,
		);
		echo json_encode($return);
	}
	
	public function search_faculty(){
		$str = $this->input->get('query');
	
		$this->load->model('employee_common_model');
		$result = $this->employee_common_model->search($str);
	
		$res = array();
		if ($result && count($result) > 0) {
			foreach ($result as $row){
				$res[] = $row->fullname;
			}
		}
		$return = array(
				'query'			=> $str,
				'suggestions'	=> $res,
		);
		echo json_encode($return);
	}
	
	public function programs (){
		$colleges_id = $this->input->get('college');
		$status = $this->input->get('status');
		
		if (! $status) {
			$status = '';
		}
		$this->load->model('hnumis/College_model');
		$programs = $this->College_model->programs_from_college($colleges_id, $status);
		echo json_encode ($programs);
	}

	
	public function list_sub_levels (){
		$top_levels = $this->input->get('top_levels');
		$this->load->model('extras_model');
		$sub_levels = $this->extras_model->list_sub_levels($top_levels);
		echo json_encode ($sub_levels);
	}
	
	public function sections() {
		$year_id = $this->input->get('year');
		$level_id = $this->input->get('level');
		
		if($level_id<>12){
			$this->load->model('basic_ed/basic_ed_sections_model');
			$sections = $this->basic_ed_sections_model->ListAllSections($year_id, $level_id);
		} else {
			$this->load->model('academic_terms_model');
			$this->load->model('hnumis/shs/section_model');
			$academic_terms = $this->academic_terms_model->current_academic_term ($academic_terms_id=0);
			////log_message("INFO", print_r($academic_terms,true)); // Toyet 11.19.2018
			$academic_terms_id = $academic_terms->id;
			//$section = $this->section_model->ListSections($year_id,$academic_terms_id=NULL,$strand_id=NULL);
			$section = $this->section_model->ListSections($year_id,$academic_terms_id,$strand_id=NULL); //changed Toyet 11.19.2018

			$sections = array();
			foreach($section as $row){
				$__section = array('id'=>$row->id,
			                       'section'=>$row->section_name);
				$sections[] = $__section;
			}
		}
		////log_message("INFO",print_r($sections,true)); // Toyet 8.2.2018
		echo json_encode ($sections);
	}
	
	public function year() {
		$level_id = $this->input->get('level');
		
		$this->load->model('basic_ed/basic_ed_sections_model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/enrollments_model','college_enrollments_model');
		
		//$current_term = $this->academic_terms_model->getCurrentAcademicTerm();
		$current_term = $this->academic_terms_model->current_academic_term($this->college_enrollments_model->academic_term_for_enrollment_date());
		
		$year = $this->basic_ed_sections_model->Get_distinct_year($level_id, $current_term->sy_id);
		
		echo json_encode ($year);
	}
	
	public function academic_term() {
		$term = $this->input->get('academic_term');
		$college_id = $this->input->get('college_id');
		
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/faculty_model');

		$academic_term = $this->academic_terms_model->current_academic_term($term);
		$faculty = $this->faculty_model->list_faculty_by_college_term($term, $college_id);
		echo json_encode($faculty);
	}
	
	public function countries (){
		$this->load->model('Places_model');
		$countries = $this->Places_model->fetch_results(0, 'countries');
		echo json_encode ($countries);
	}
	
	public function towns (){
		$this->load->model('Places_model');
		$id = $this->input->get('id');
		
		$towns = $this->Places_model->fetch_results($id, 'towns');
		echo json_encode ($towns);
	}
	
	public function barangays (){
		$this->load->model('Places_model');
		$id = $this->input->get('id');
	
		$barangays = $this->Places_model->fetch_results($id, 'barangays');
		echo json_encode ($barangays);
	}
	
	/**
	 * Returns names of student within AR Balance Time Frame, Teller Code, And (Basic ED or College)
	 * @author Kevin
	 */
	public function ar_balance_details($type=''){
		$output = array('type'=>'Error','message'=>'Unknown error');
		//print_r("here");die();
		$this->load->model('accountant/accountant_model');
		
		if (empty($type) || !in_array($type,array('basic','college'))){
			$output['message'] = 'Request Error';
			$this->output->set_output(json_encode($output));
			return;
		}

		
		if($type == 'college'){					
			$trans_code = $this->input->get_post('trans_code');
			$levels_id 	= $this->input->get_post('abbr');
			$start_date = $this->input->get_post('start_date');
			$end_date 	= $this->input->get_post('end_date');
			$trans_type = $this->input->get_post('trans_type');
			$x_show_by	= $this->input->get_post('x_show_by');
			$is_isis 	= $this->input->get_post('is_isis');
			
			if (!$trans_code){
				$output['message'] = 'Missing Transcode';
				$this->output->set_output(json_encode($output));
				return;
			}
			
			if (!$levels_id){
				$output['message'] = 'Missing Abbr';
				$this->output->set_output(json_encode($output));
				return;
			}
			
			if (!$start_date || !strtotime($start_date)){
				$output['message'] = 'Missing or Invalid Start Date';
				$this->output->set_output(json_encode($output));
				return;
			}
			
			if (!$end_date || !strtotime($end_date)){
				$output['message'] = 'Missing or Invalid End Date';
				$this->output->set_output(json_encode($output));
				return;
			}
			
			if (!$trans_type || !in_array($trans_type, array('credit','debit'))){
				$output['message'] = 'Missing or Invalid Trans Type';
				$this->output->set_output(json_encode($output));
				return;
			}

			//$output = array('type'=>'Empty','message'=> $trans_code." levels_id=" . $levels_id . " " .$start_date." " .$end_date. " " .$trans_type);
			//$output = array('type'=>'Empty','message'=>$this->db->last_query());
			//$this->output->set_output(json_encode($output));
			//return;
				
			
			if ($x_show_by == 'all'){
				if ($is_isis=='Y')
					$query = $this->accountant_model->ar_balance_college_detail_students_all_isis($trans_code,$levels_id,$start_date,$end_date,$trans_type);
				else
					$query = $this->accountant_model->ar_balance_college_detail_students_all_mis($trans_code,$levels_id,$start_date,$end_date,$trans_type);						
			}else{ 
				if ($is_isis=='Y'){
					$query = $this->accountant_model->ar_balance_college_detail_students_bygroup_isis($trans_code,$levels_id,$start_date,$end_date,$trans_type);
				}else{
						
					
					$query = $this->accountant_model->ar_balance_college_detail_students_bygroup_mis($trans_code,$start_date,$end_date,$trans_type);
				}
			}				
			
			if (!$query){
				$output['message'] = 'Bad Query';
				$this->output->set_output(json_encode($output));
				//echo $this->db->last_query();
				return;
			}
			
			if ($query->num_rows() == 0){
				$output = array('type'=>'Empty','message'=>'No Result Found');
				//$output = array('type'=>'Empty','message'=>$this->db->last_query());
				$this->output->set_output(json_encode($output));
				return;
			}

			//$output = array('type'=>'Empty','message'=>$this->db->last_query());
			//$this->output->set_output(json_encode($output));
			//return;
					
 			$output = array(
						'type'=>'Success',
						'trans_type'=>humanize($trans_type),
						'heading'=>$query->row(0)->description,
						'data'=>$query->result());
		}
				
		elseif ($type=='basic'){
			$levels_id  = $this->input->get_post('levels_id');
			$yr_level 	= $this->input->get_post('yr_level');
			$trans_code = $this->input->get_post('trans_code');
			$start_date = $this->input->get_post('start_date');
			$end_date 	= $this->input->get_post('end_date');
			$is_isis 	= $this->input->get_post('is_isis');
			$academic_year_id = $this->input->get_post('academic_year_id'); 
				
			if (!$start_date || !strtotime($start_date)){
				$output['message'] = 'Missing or Invalid Start Date';
				$this->output->set_output(json_encode($output));
				return;
			}
			
			if (!$end_date || !strtotime($end_date)){
				$output['message'] = 'Missing or Invalid End Date';
				$this->output->set_output(json_encode($output));
				return;
			}
			if ($is_isis=='N')
				$query = $this->accountant_model->ar_balance_basic_detail_students_mis($trans_code,$start_date,$end_date,$abbr);
			else 
				$query = $this->accountant_model->ar_balance_basic_detail_students_isis($academic_year_id,$levels_id,$yr_level,$trans_code,$start_date,$end_date);
				
			if (!$query){
				$output['message'] = 'Bad Query';
				$this->output->set_output(json_encode($output));
				//echo $this->db->last_query();
				return;
			}
			
			if ($query->num_rows() == 0){
				$output = array('type'=>'Empty','message'=>'No Result Found');
				//$output = array('type'=>'Empty','message'=>$this->db->last_query());
				$this->output->set_output(json_encode($output));
				return;
			}

			//$output = array('type'=>'Empty','message'=>$this->db->last_query());
			//$this->output->set_output(json_encode($output));
			//return;				
			
			$output = array(
							'type'=>'Success',
							'heading'=>$query->row(0)->description,
							'data'=>$query->result());
		}
		
		$this->output->set_output(json_encode($output));
	} 


	public function ar_details(){
		//die("here...");
		$output = array('type'=>'Error','message'=>'Unknown error');
		$this->load->model('accountant/acct_reports_model');	

		$levels_id 	 = $this->input->get_post('levels_id');
		//$college 	= $this->input->get_post('college');
		$group_name  = $this->input->get_post('group_name');
		$year_level  = $this->input->get_post('year_level');
		$tran_code 	 = $this->input->get_post('tran_code');
		$tran_type 	 = $this->input->get_post('tran_type');
		$trangroup	 = $this->input->get_post('trangroup');
		$deskription = $this->input->get_post('deskription');
		$start_date  = $this->input->get_post('start_date');
		$end_date 	 = $this->input->get_post('end_date');
		$summary 	 = $this->input->get_post('summary');
		$l99 	     = $this->input->get_post('l99');

		if (empty($levels_id) || !in_array($levels_id,array('0','1','2','3','4','5','6','7','11','12','99'))){
			$output['message'] = 'Unidentified Levels ID!';
			$this->output->set_output(json_encode($output));
			return;
		}
			
		if (!$start_date || !strtotime($start_date)){
			$output['message'] = 'Missing or Invalid Start Date';
			$this->output->set_output(json_encode($output));
			return;
		}
			
		if (!$end_date || !strtotime($end_date)){
			$output['message'] = 'Missing or Invalid End Date';
			$this->output->set_output(json_encode($output));
			return;
		}
			
		if (!$tran_type || !in_array($tran_type, array('credit','debit'))){
			$output['message'] = 'Missing or Invalid Trans Type';
			$this->output->set_output(json_encode($output));
			return;
		}

		//$output = array('type'=>'Empty','message'=> $trans_code." levels_id=" . $levels_id . " " .$start_date." " .$end_date. " " .$trans_type);
		//	$output['message'] = $tran_code;
		//	$this->output->set_output(json_encode($output));
		//	return;

		log_message('info','TOYET: levels_id   = '.$levels_id);
		log_message('info','TOYET: group_name  = '.$group_name);
		log_message('info','TOYET: year_level  = '.$year_level);
		log_message('info','TOYET: tran_code   = '.$tran_code);
		log_message('info','TOYET: tran_type   = '.$tran_type);
		log_message('info','TOYET: trangroup   = '.$trangroup);
		log_message('info','TOYET: start_date  = '.$start_date);
		log_message('info','TOYET: end_date    = '.$end_date);
		log_message('info','TOYET: description = '.$deskription);
		log_message('info','TOYET: summary = '.$summary);
		log_message('info','TOYET: l99 = '.$l99);

		if ((in_array($levels_id,array('6','7','12') )) or 
			($levels_id=='99' and ($trangroup=='ADJC')))
			$query = $this->acct_reports_model->ar_details_college_ver2($levels_id,$group_name,$year_level,$tran_code,$tran_type,$trangroup,$start_date,$end_date,$deskription,$summary,$l99);
		else 
			$query = $this->acct_reports_model->ar_details_bed_ver2($levels_id,$group_name,$year_level,$tran_code,$tran_type,$trangroup,$start_date,$end_date,$summary,$l99);

		//echo 'window.open("message_vessel.php","_blank")';
		//var_dump($query); exit();
		//$output = array('type'=>'Empty','message'=>$this->db->last_query());
		//$this->output->set_output(json_encode($output));
		//return;
		//log_message('info',print_r($query,TRUE));
		
		if (!$query){
			$output['message'] = 'Bad Query';
			//$output = array('type'=>'Bad Query','message'=>$this->db->last_query());
			$this->output->set_output(json_encode($output));
			return;
		}
			
		if ($query->num_rows() == 0){
			$output = array('type'=>'Empty','message'=>'No Result Found');
			//$output = array('type'=>'Empty','message'=>$this->db->last_query());
			$this->output->set_output(json_encode($output));
			return;
		}

		//$output = array('type'=>'Nice one! :)','message'=>$this->db->last_query());
		//$this->output->set_output(json_encode($output));
		//return;

		$dumb_section=array('Grade 1','Grade 2','Grade 3','Grade 4','Grade 5','Grade 6','Grade 7','Grade 8', '(',')');
		//$trimed_abbreviation = str_replace($dumb_section,"",$abbreviation);
		
		$output = array(
				'type'=>'Success',
				'tran_type'=>humanize($tran_type),
				'heading'=>$query->row(0)->description,
				'data'=>$query->result());
	    ////log_message('info',print_r($output,true));
		$this->output->set_output(json_encode($output));
	}


	public function ar_trans_details(){
		//echo "here";die();
		$output = array('type'=>'Error','message'=>'Unknown error');
		$this->load->model('accountant/acct_reports_model');
	
		$levels_id 	= $this->input->get_post('levels_id');
		//$college 	= $this->input->get_post('college');
		$year_level = $this->input->get_post('year_level');
		$group_name = $this->input->get_post('group_name');
		$tran_code 	= $this->input->get_post('tran_code');
		$tran_type 	= $this->input->get_post('tran_type');
		$start_date = $this->input->get_post('start_date');
		$end_date 	= $this->input->get_post('end_date');
	
	
		if (empty($levels_id) || !in_array($levels_id,array('0','1','2','3','4','5','6','7','11','12'))){
			$output['message'] = 'Unknown Levels ID!';
			$this->output->set_output(json_encode($output));
			return;
		}
			
		if (!$start_date || !strtotime($start_date)){
			$output['message'] = 'Missing or Invalid Start Date';
			$this->output->set_output(json_encode($output));
			return;
		}
			
		if (!$end_date || !strtotime($end_date)){
			$output['message'] = 'Missing or Invalid End Date';
			$this->output->set_output(json_encode($output));
			return;
		}
			
		if (!$tran_type || !in_array($tran_type, array('credit','debit'))){
			$output['message'] = 'Missing or Invalid Trans Type';
			$this->output->set_output(json_encode($output));
			return;
		}
	
		//$output = array('type'=>'Empty','message'=> $trans_code." levels_id=" . $levels_id . " " .$start_date." " .$end_date. " " .$trans_type);
		//$output = array('type'=>'Empty','message'=>$this->db->last_query());
		//$this->output->set_output(json_encode($output));
		//return;
	
		if (in_array($levels_id,array('6','7') ))
			$query = $this->acct_reports_model->ar_transactions_details_college($levels_id,$group_name, $year_level,  $tran_code,$tran_type,$start_date,$end_date);
		else
			$query = $this->acct_reports_model->ar_transactions_details_bed($levels_id, $group_name, $year_level, $tran_code,$tran_type,$start_date,$end_date);
	
		if (!$query){
			$output['message'] = 'Bad Query';
			//$output = array('type'=>'Bad Query','message'=>$this->db->last_query());
			$this->output->set_output(json_encode($output));
			return;
		}
	
		if ($query->num_rows() == 0){
			$output = array('type'=>'Empty','message'=>'No Result Found');
			//$output = array('type'=>'Empty','message'=>$this->db->last_query());
			$this->output->set_output(json_encode($output));
			return;
		}
	
		//$output = array('type'=>'Nice one! :)','message'=>$this->db->last_query());
		//$this->output->set_output(json_encode($output));
		//return;
	
		$dumb_section=array('Grade 1','Grade 2','Grade 3','Grade 4','Grade 5','Grade 6','Grade 7','Grade 8', '(',')');
		//$trimed_abbreviation = str_replace($dumb_section,"",$abbreviation);
	
		$output = array(
				'type'=>'Success',
				'tran_type'=>humanize($tran_type),
				'heading'=>$query->row(0)->description,
				'data'=>$query->result());
	
		$this->output->set_output(json_encode($output));
	}	
	
	
	public function academic_groups(){
		$this->load->model('mass_printing_model', 'print_model');
		$academic_groups = $this->print_model->academic_groups();
		
		$this->output->set_output(json_encode($academic_groups));
	}

	public function schools($level=3){
		$this->load->model('common_model');
		$query = $this->input->get('query');
		$schools = $this->common_model->schools($level, $query);
		$school_res = array();
		foreach ($schools as $school){
			$school_res[] = $school->school;
		}
		$return = array(
				'query'			=> $query,
				'suggestions'	=> $school_res,
		);
	
		$this->output->set_content_type('application/json')->set_output(json_encode($return));
	}


	public function update_phone(){
		$this->load->model('student_common_model');		
		if(!empty($_POST)){						
			foreach($_POST as $field_name => $val) {
				//clean post values
				$field_userid = strip_tags(trim($field_name));
				$val = strip_tags(trim(mysql_real_escape_string($val)));
		
				//from the fieldname:user_id we need to get user_id
				$split_data = explode(':', $field_userid);
				$id = $split_data[1];
				$field_name = $split_data[0];
				if(!empty($id) && !empty($field_name) && !empty($val)) {
					if ($update = $this->student_common_model->update_number_via_ajax($id, $field_name, $val)){
						echo "Saved!";	
					}			
				}else{
					echo "Invalid Requests";
				}
			}
		} else {
			echo "Invalid Requests";
		}		
	}

	
	public function remove_phone(){
		$this->load->model('student_common_model');
		
		if(!empty($_POST)){
			foreach($_POST as $id => $val) {
				
					
				$id_to_delete = $val;
				if(!empty($id_to_delete)) {
					if ($update = $this->student_common_model->remove_number_via_ajax($id_to_delete)){
						echo "Deleted!";
					}
				}else{
					echo "Invalid Requests";
				}
				
			}
			
			
		} else {
			echo "Empty or invalid value!" ;
		}
	}
	
	public function receipt_detail(){
		$this->load->library('print_lib');
		$this->load->library('extras_lib');
		$this->load->helper('number_words_helper');

		$or_number = $this->input->get_post('or_number');
		$or_details = $this->extras_lib->Get_ORDetails2($or_number);

		////log_message("INFO", print_r($or_details,true)); // Toyet 11.8.2018

		$teller_items = array($or_details->description);
		$payments = array();
		$payments[] = $or_details->receipt_amount;
		$payment_methods = array();
		$payment_methods[] = array('type'=>$or_details->payment_method);

		////log_message("INFO",print_r($this->session->userdata,true)); // Toyet 11.8.2018
		$datestamp = new DateTime();
		$trace_strings = array("//This is a reprint,#".
			             trim($or_number).",".
			             date_format($datestamp,'Y-m-d').",".
			             $this->session->userdata['role'].','.
			             $this->session->userdata['lname'].','.
			             $this->session->userdata['fname'].
			             "/");
		$receipt_data = array(
						'id_number'=>$or_details->idno, 
						'or_number'=>$or_number,
						'date'=>$or_details->date,
						'names'=>array($or_details->payor), 
						'course_year'=>$or_details->course,
						'item_codes'=>$or_details->description,
						'items'=>$teller_items,
						'payments'=>$payments,
						'remarks'=>array($or_details->remark),
						'trace_strings'=>$trace_strings,
						'payment_methods'=>$payment_methods,
						'password'=>" ",
						);
		////log_message("INFO", print_r($receipt_data,true)); // Toyet 11.8.2018		
		$this->load->view('print_templates/receipt2_reprint', $receipt_data );
	}	

    // Added by Toyet 12.11.2018
	public function recompute_tuition_byprivilege(){
		$this->load->model('financials/Scholarship_Model');
		$return_value = 0;
		$return_array = array();
		$privilege_info = array();
		$selected_privilege_id = $this->input->post('selected_privilege_id');
		$paying_units = $this->input->post('paying_units');
		$scholarship_units = $this->input->post('scholarship_units');
		$tuition_fee_basic_rate = $this->input->post('tuition_fee_basic_rate');
		$year_level = $this->input->post('year_level');
		$withcwts_units = $this->input->post('withcwts_units');

		$privilege_info = $this->Scholarship_Model->getScholarshipInfo($selected_privilege_id);		
		//log_message("INFO",print_r($privilege_info,true));  // Toyet 12.12.2018
		//log_message("INFO",print_r($year_level,true));  // Toyet 12.12.2018

		if($year_level<4){
			if($privilege_info->max_unit_undergrad>0){
				if($paying_units-$withcwts_units>$privilege_info->max_unit_undergrad){
					$scholarship_units = $privilege_info->max_unit_undergrad;
				} else {
					$scholarship_units = $paying_units-$withcwts_units;
				}
			} else {
				$scholarship_units = 18;
			}
		} else {
			if($privilege_info->max_unit_grad>0){
				if($paying_units-$withcwts_units>$privilege_info->max_unit_grad){
					$scholarship_units = $privilege_info->max_unit_grad;
				} else {
					$scholarship_units = $paying_units-$withcwts_units;
				}
			} else {
				$scholarship_units = 18;
			}
		}

		// scholarships related details
		$priv_array = array(183,189,364);
		if(in_array($selected_privilege_id,$priv_array)){
			// 183 - WORKING SCHOLAR
			// 364 - HNU CHORALE
			// 189 - PRIEST & RELIGIOUS PRIV
			//       add back the $withcwts_units  -As Per Sir Charlie by Toyet 4.29.2019
			$scholarship_units += $withcwts_units;
		}

		$tuition_fee = $scholarship_units*$tuition_fee_basic_rate; 
		$return_array = array( 'tuition_fee'=>$tuition_fee, 
			                   'selected_privilege_id'=>$selected_privilege_id, );
		$return_array = json_encode($return_array);

		echo $return_array;
		return $return_array;
	}

   // added by Toyet 2.27.2019
	public function fetch_comments(){ 
		$this->load->model('col_students_model');
		$comments = "";

		$student_idno = $this->input->post('student_idno');
		log_message("info", print_r($student_idno,true));
		$history_id = $this->input->post('history_id');
		$this_sem_only = $this->input->post('this_sem_only');

		log_message("info", "THIS SEM FLAG =>>".print_r($this_sem_only,true)); 

		$comments_array = $this->col_students_model->get_student_comments($student_idno, $history_id, $this_sem_only);
		$comments = json_encode($comments_array);
		echo $comments;
		return $comments;

	}

   // added by Toyet 2.27.2019
	public function update_comments(){ 
		$this->load->model('col_students_model');
		$comments = "";

		$student_idno = $this->input->post('student_idno');
		$history_id = $this->input->post('history_id');
		$comments = $this->input->post('comments');

		$comments_array = $this->col_students_model->update_student_comments($student_idno, $history_id, $comments);

		$comments = json_encode($comments_array);
		echo $comments;
		return $comments;

	}

   // added by Toyet 2.27.2019
	public function check_if_exist_comments(){ 
		$this->load->model('col_students_model');
		$return = "";

		$student_idno = $this->input->post('student_idno');
		$history_id = $this->input->post('history_id');

		$comments_array = $this->col_students_model->check_student_comments($student_idno, $history_id);

		if(count($comments_array)>0){
			$return = array( 'itExists'=>true );
		}

		$return = json_encode($return);

		//log_message('info', print_r($return,true)); 

		echo $return;
		return $return;

	}

   // added by Toyet 2.27.2019
	public function add_comments(){ 
		$this->load->model('col_students_model');
		$comments = "";

		$student_idno = $this->input->post('student_idno');
		$history_id = $this->input->post('history_id');
		$comments = $this->input->post('comments');

		$comments_array = $this->col_students_model->add_student_comments($student_idno, $history_id, $comments);
		
		$comments = json_encode($comments_array);

		log_message("info", print_r($comments,true)); 

		echo $comments;
		return $comments;

	}
	
    // added by Toyet 2.28.2019
	public function get_blocked_status(){ 
		$this->load->model('col_students_model');
		$comments = "";

		$student_idno = $this->input->post('student_idno');
		$history_id = $this->input->post('history_id');

		$blocked = $this->col_students_model->get_blocked_status($student_idno, $history_id);
		$blocked = json_encode($blocked);

		//log_message("info", print_r($blocked,true)); 

		echo $blocked;
		return $blocked;

	}

    // added by Toyet 2.28.2019
	public function block_unblockEnrollment(){ 
		$this->load->model('col_students_model');
		$status = "";

		$student_idno = $this->input->post('student_idno');
		$history_id = $this->input->post('history_id');
		$blocked_status = $this->input->post('blocked_status');

		//log_message("info", print_r($blocked_status,true));

		$blocked = $this->col_students_model->update_blocked_status($student_idno, $history_id, $blocked_status);

		//log_message("info", "-------NEW BLOCKED STATUS------"); 
		//log_message("info", print_r($blocked,true)); 

		$status = array('status'=>$blocked_status);
		$status = json_encode($status);
		//log_message("info", print_r($status,true)); 

		echo $status;
		return $status;

	}

	// added by Toyet 3.25.2019
	public function rectify_grades() {
		$this->load->model('registrar_model');

		$return = json_encode('Done.');

		$rectify_values = $this->input->post('rectify_values');

		foreach ($rectify_values as $student){
			$this->registrar_model->rectify_grades($student['0'], $student['1'], 
				                    $student['2'], 
				                    $student['3'], 
				                    $student['4']);
		}

		echo $return;
		return $return;
	}


	public function listTownsByProvince(){
		$province_id = $this->input->get('province_id');

		//log_message("INFO", print_r($province_id,true)); 

		$this->load->model('hnumis/Otherschools_Model');

		$towns = $this->Otherschools_Model->listTownsByProvince($province_id);

		//log_message("info", print_r($towns,true));

		echo json_encode ($towns);
	}


	public function listBrgyByTown(){
		$town_id = $this->input->get('town_id');

		//log_message("INFO", print_r($town_id,true)); 

		$this->load->model('hnumis/Otherschools_Model');

		$towns = $this->Otherschools_Model->listBrgyByTown($town_id);

		//log_message("info", print_r($towns,true));

		echo json_encode ($towns);
	}


}