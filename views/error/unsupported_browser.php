<html>
<head>
	<meta charset="UTF-8" />
	<title>Browser Error | HNU MIS</title>
</head>
<body>
	<h2>Error!</h2>
	<p>Unsupported Browser, Please use the latest browser available inorder to use MIS. Thank you!</p>
	<p>Your browser: <?php echo $this->agent->browser() ?> Version: <?php echo $this->agent->version() ?></p>
	<?php if (isset($minimum_browser)): ?>
	<p>You need atleast Version <?php echo $minimum_browser['version'] ?> of <?php echo $minimum_browser['name'] ?></p>
	<?php endif ?>
</body>
</html>