<?php

class Teller_model extends MY_Model {

	/**
	 * Method: get_last_receipt
	 * 
	 * provides the last receipt No. give the tax type...
	 * 
	 * @param string tax_type either vat or nonvat. Defaults to 'nonvat'
	 * @return object last receipt in string or False when fails or no receipt found.
	 */
	public function get_last_receipts () {
		$machine_ip = $this->input->ip_address();
		$sql = "
			SELECT vat_receipt_no, non_vat_receipt_no, suffix 
			FROM machines 
			WHERE machine_ip={$this->db->escape($machine_ip)}
			LIMIT 1	
		";		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0) {
			return $query->row();
		} else
			return FALSE;
	}
	
	
	/**
	 * All teller codes
	 * 
	 * @param string $tax_type either vat or nonvat. Defaults to nonvat
	 * @return boolean
	 */
	public function get_teller_codes ($tax_type='nonvat', $teller_visible=FALSE, $level=NULL) {
		$type = ($tax_type=='nonvat' ? 'NV' : 'V'); // type is NV when tax_type is nonvat, else type is V.
		$sql = "
			SELECT distinct t.id, t.teller_code, t.description, default_amount
			FROM teller_codes t
			WHERE t.tax_type= '{$type}'
			";
		if(!is_null($level)){
			$sql .= "AND (FIND_IN_SET('{$level}',applicable_levels)>0)
				";
		}
		if($teller_visible){
			$sql .= "AND teller_visible='Y'\n";
		}
			$sql .= "ORDER BY t.teller_code ASC 	";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){ 
			//added by justing...
			$return = array();
			foreach ($query->result() as $row){
		 		$return[(int)$row->id] = $row->teller_code . " | " . $row->description . " | " . $row->id . " | " . $row->default_amount;
			}
			return $return;
		}	
		else{
			$this->set_error();
			//return $this->db->_error_number() .":". $this->db->_error_mmessage();			
			return FALSE;
		}
	}
	
	public function all_teller_codes () {
		$sql = "
		SELECT 
			distinct t.id, t.teller_code, 
				if(t.teller_visible='Y',t.description,t.description2) description,  
				is_ledger, default_amount, t.teller_visible, 
				t.tax_type, t.status, t.applicable_levels,
				if(t.description2!='' or t.description2 is not null,'Y','N') as posting_visible
		FROM 
			teller_codes t
		ORDER BY 
			t.teller_code 
		ASC
		";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			$this->set_error();
			return FALSE;
		}
	}
	
	public function teller_codes () {
		$sql = "
		SELECT 
			distinct t.id, t.teller_code, t.description, is_ledger, 
			default_amount, t.tax_type, t.status
		FROM 
			teller_codes t
		WHERE 
			t.status='Active' 
			AND t.tax_type='NV' or t.tax_type='V'
		ORDER BY 
			t.teller_code ASC";
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0){
			return $query->result();
		} else {
			$this->set_error();
			return FALSE;
		}
	}
	
	public function update_teller_code($data){
		$data['updated_by'] = $this->session->userdata('empno');
		$data['updated_on'] = date('Y-m-d H:i:s');		
		$sql = "
			UPDATE
				teller_codes
			SET
			";

		$columns = array('id', 'teller_code', 'description', 'default_amount','teller_visible', 'is_ledger', 'tax_type', 'applicable_levels', 'status','updated_by','updated_on');

		$posting_visible = $data['posting_visible'];
		
		foreach ($data as $key=>$val){
			if ( in_array($key, $columns)) {				
				if($key=='applicable_levels')
					$val = implode(',', $val);
	 			$sql .= "`{$key}`={$this->db->escape(sanitize_text_field($val))},\n";
	 			//Determine if Posting_Visible has been changed  Toyet 6.26.2018	 			
	 			if($key=='description' and $posting_visible=='Y'){
	 				$sql .= "`description2`={$this->db->escape(sanitize_text_field($val))},\n";
	 			} elseif ($key=='description' and $posting_visible=='N') {
	 				$sql .= "`description2`=NULL,\n";
	 			}

			}
		}
		$sql = rtrim($sql, ",\n") . " WHERE id={$this->db->escape($data['item_id'])}
		";

		//log_message("INFO", print_r($posting_visible,true)); // Toyet 6.26.2018 
		//log_message("INFO", print_r($sql,true)); // Toyet 6.26.2018 

		return $this->db->query($sql);
	}
	
	public function insert_teller_code ($data){
		$data['inserted_by'] = $this->session->userdata('empno');
		$data['inserted_on'] = date('Y-m-d H:i:s'); 
		$sql = "
			INSERT into
				teller_codes
			";
		
		$columns = array('teller_code', 'description', 'default_amount','teller_visible','is_ledger', 'tax_type', 'status','inserted_by','inserted_on');

		$fields = "(";
		$value = " VALUE (";

		//log_message("INFO",print_r($data,true)); // Toyet 6.25.2018

		$posting_visible = $data['posting_visible'];

		foreach ($data as $key => $val){
			if ( in_array($key, $columns)) {
				if($key=='description' and $posting_visible=='Y'){
					$fields .= "`{$key}`,";
					$value .= "{$this->db->escape(sanitize_text_field($val))},\n"; 
					$fields .= "`description2`,";
					$value .= "{$this->db->escape(sanitize_text_field($val))},\n"; 
				} else {
					$fields .= "`{$key}`,";
					$value .= "{$this->db->escape(sanitize_text_field($val))},\n"; 
				}
			}
		}
		$fields = rtrim ($fields, ",") . ")\n";
		$value = rtrim ($value, ",\n") . ")\n";
		
		$sql = $sql . $fields . $value;

		//log_message("INFO",print_r($sql,true)); // Toyet 6.25.2018
		//die();

		if ($this->db->query($sql))
			return $this->db->insert_id(); else
			return FALSE;
	}

	public function insert_bank ($data){
		//NOTE: Justin... get rid of limit on tries...
		$keep_doing = true;
		$code = empty($data['code']) ? accronymize($data['name']) : $data['code'] ;
		$tries = 0;
		while ($keep_doing){			
			$coden =  $code . ($tries > 0 ? $tries: '') ;			
			$sql = "
				INSERT INTO banks (code,name)
				VALUES (
					'{$coden}',
					" . $this->db->escape($data['name']) . "
				)
			";
			file_put_contents('test.txt', date('[Y-m-d H:i:s] - ') . "{$sql}\n", FILE_APPEND);
			$query = $this->db->query($sql);
			if ($this->db->insert_id() > 0){
				$keep_doing = false;				
				return $this->db->insert_id();			
			} elseif ($tries == 5){
				$keep_doing = FALSE;
			}
			$tries++;
		}
	}
	
	
	public function get_bank_codes () {
		$sql = "
			SELECT b.id, b.`code`, b.`name`
			FROM banks b
			ORDER BY `name` 
		";
		$query = $this->db->query($sql);
		
		if ($query && $query->num_rows() > 0){
			$return = array();
			foreach ($query->result() as $row){
				$return[(int)$row->id] = $row->code . " | " . $row->name . " | " . $row->id . "" ;
			}
			return $return;
		}
		else{
			$this->set_error();
			return FALSE;
		}
	}
	
	
	public function insert_payments ($data){
		$sql = "
			INSERT INTO payments p (payers_id, is_vatable, transaction_date, machine_ip, tellers_id, receipt_no, receipt_amount, amount_tendered, remark, is_ledger)
				VALUES (
				{$data['payers_id']}, 
				NOW(), 
				{$data['machine_ip']}, 
				{$data['tellers_id']}, 
				{$data['receipt_no']}, 
				{$data['receipt_amount']}, 
				{$data['amount_tendered']}, 
				{$data['remark']}, 
				{$data['is_ledger']})
				";
						
		if ($query = $this->db->query($sql)){
			return $this->db->insert_id();
		} else{
			$this->set_error();
			return FALSE;
		}
	}
	
	public function get_last_inserted_id_of_payments(){
		/*
		 * returned value will be used as payments_id in payment_details 
		 */
		
	}

	public function insert_payment_details ($data){			
		$sql = "
			INSERT into payment_details(payments_id, teller_codes_id, amount)
				VALUES	";			
				foreach ($data as $key=>$val){
					$sql .= "(	$payments_id,  
						{$this->db->escape($key)}, 
						{$this->db->escape($val)}),\n";
				}
			$sql = rtrim($sql, ",\n") . "\n";		
			return $this->db->query($sql);		
	}
	
	//Added: April 11, 2013 by Amie
	public function get_payertype($or) {
		$result = NULL;
		$q = "SELECT pay.students_idno, pay.employees_empno, pay.other_payers_id, pay.inhouse_id
				FROM payers pay, payments p
				WHERE p.payers_id = pay.id
					AND p.receipt_no = {$this->db->escape($or)}";

		$query = $this->db->query($q);
			
			if ($query && $query->num_rows() > 0) 
				$result = $query->row();
			return $result;
	}

	//Added: April 11, 2013 by Amie
	public function get_ORdetails($or, $payertype) {
		
		$result = NULL;
		
		if ($payertype == 'S') {
			$q = "SELECT p.id, p.receipt_no, date(p.transaction_date) as date, p.receipt_amount, p.is_ledger, t.description, pm.payment_method, 
			         CONCAT(s.lname,', ',s.fname,' ',s.mname,'.') as name, p.status
						FROM payments AS p
						LEFT JOIN payers AS pay
						ON p.payers_id = pay.id
						LEFT JOIN students s ON s.idno = pay.students_idno
						LEFT JOIN payments_payment_methods ppm ON ppm.payments_id = p.id
						LEFT JOIN payment_methods pm ON ppm.payment_methods_id = pm.id
						LEFT JOIN payment_items pi ON pi.payments_id = p.id
						LEFT JOIN teller_codes t ON t.id = pi.teller_codes_id
					WHERE p.receipt_no = {$this->db->escape($or)}";
		}  else if ($payertype == 'E') {
			$q = "SELECT p.id, p.receipt_no, date(p.transaction_date) as date, p.receipt_amount, p.is_ledger, t.description, pm.payment_method, 
			         CONCAT(e.lname,', ',e.fname,' ',e.mname,'.') as name, p.status
						FROM payments AS p
						LEFT JOIN payers AS pay
						ON p.payers_id = pay.id
						LEFT JOIN employees e ON e.empno = pay.employees_empno
						LEFT JOIN payments_payment_methods ppm ON ppm.payments_id = p.id
						LEFT JOIN payment_methods pm ON ppm.payment_methods_id = pm.id
						LEFT JOIN payment_items pi ON pi.payments_id = p.id
						LEFT JOIN teller_codes t ON t.id = pi.teller_codes_id
					WHERE p.receipt_no = {$this->db->escape($or)}";
		} else if ($payertype == 'O') {
				$q = "SELECT p.id, p.receipt_no, date(p.transaction_date) as date, p.receipt_amount, p.is_ledger, t.description, pm.payment_method, o.name, p.status
						FROM payments AS p
						LEFT JOIN payers AS pay
						ON p.payers_id = pay.id
						LEFT JOIN other_payers o ON o.id = pay.other_payers_id
						LEFT JOIN payments_payment_methods ppm ON ppm.payments_id = p.id
						LEFT JOIN payment_methods pm ON ppm.payment_methods_id = pm.id
						LEFT JOIN payment_items pi ON pi.payments_id = p.id
						LEFT JOIN teller_codes t ON t.id = pi.teller_codes_id
					WHERE p.receipt_no = {$this->db->escape($or)}"; 
		} else if ($payertype == 'I') {
			$q = "SELECT p.id, p.receipt_no, date(p.transaction_date) as date, p.receipt_amount, p.is_ledger, t.description, pm.payment_method, i.office as name, p.status
				FROM payments AS p
				LEFT JOIN payers AS pay
				ON p.payers_id = pay.id
				LEFT JOIN offices i ON i.id = pay.inhouse_id
				LEFT JOIN payments_payment_methods ppm ON ppm.payments_id = p.id
				LEFT JOIN payment_methods pm ON ppm.payment_methods_id = pm.id
				LEFT JOIN payment_items pi ON pi.payments_id = p.id
				LEFT JOIN teller_codes t ON t.id = pi.teller_codes_id
				WHERE p.receipt_no = {$this->db->escape($or)}";
		}

	
		$query = $this->db->query($q);
			
			if ($query && $query->num_rows() > 0) 
				$result = $query->row();
			return $result;
		
	}
	
	
	//Added: April 10, 2013 by Amie
	public function void_receipts($data) {
		$q = "
				UPDATE payments
				SET 
					voided_by 	= {$data['empno']}, 
					voided_on 	= NOW(), 
					status 		= 'void'
				WHERE 
					id = {$this->db->escape($data['or'])}
					AND status = 'unposted'
			";				
		 return $this->db->query($q) ? true : false;
	}
	
	public function get_payments_for_posting($date) {
		$result = NULL;
		$q = "		
			SELECT trim(LEADING '0' FROM p.id) as id, p.receipt_no, p.receipt_amount, p.transaction_date, p.machine_ip, t.description, 
				if(!isnull(tl3.empno),tl3.empno,
						if(!isnull(tl2.idno),tl2.idno,'Not Found')
				  ) as teller_id,
				if(!isnull(r.students_idno),s.idno,
			 			if(!isnull(e.empno),e.empno,
			 					if(!isnull(op.id),op.id,
			 							if(!isnull(o.id),o.id,'Not Found')
								  )
						  )
				  ) as idno,
				if(!isnull(r.students_idno),CONCAT_ws(', ',s.lname,s.fname), 
						if(!isnull(r.employees_empno),concat_ws(', ',e.lname,e.fname),
								if(!isnull(op.id),op.name, 
										if(!isnull(o.id),o.office,'Not Found')
								 )
							)
				   ) as name				
			FROM payments 				p
				JOIN payers   			r  on r.id = p.payers_id
				LEFT JOIN students 		s  on s.idno = r.students_idno
				LEFT JOIN employees 	e  on e.empno = r.employees_empno
				LEFT JOIN other_payers 	op on op.id = r.other_payers_id
				LEFT JOIN offices 		o  on o.id = r.inhouse_id
				JOIN payment_items 		i  on i.payments_id = p.id
				JOIN teller_codes 		t  on t.id = i.teller_codes_id
				LEFT JOIN tellers 		tl on tl.id = p.tellers_id
				LEFT JOIN students 		tl2 on tl2.idno = tl.students_idno
				LEFT JOIN employees 	tl3 on tl3.empno = tl.employees_empno
			WHERE p.`status` = 'unposted'
				and date(transaction_date) = '{$date}' 
			ORDER BY p.transaction_date,p.machine_ip,p.receipt_no,`name`
			";		
		//print_r($q);die();
		$query = $this->db->query($q);
		if ($query && $query->num_rows() > 0)
			$result = $query->result();	
		return $result;
	}
	
	//Added: April 11, 2013 by Amie
	public function get_data_for_ledger($payment_id) {
		$result = NULL;
		
		$q = "SELECT p.id, p.receipt_no, p.receipt_amount, p.transaction_date
				FROM payments p
				WHERE p.id = {$this->db->escape($payment_id)}";
				
		$query = $this->db->query($q);
		if ($query && $query->num_rows() > 0)
			$result = $query->row();
			
		return $result;
	}
	
	//added: April 11, 2013 by Amie
	public function post_receipts($payment_id, $empno) {
		$q = " 
			UPDATE payments
					SET 
						posted_by 	= {$this->db->escape($empno)}, 
						posted_on 	= NOW(), 
						status 		= 'posted'
					WHERE 
						id = {$this->db->escape($payment_id)}
					AND 
						status='unposted'
			";
		//print_r($q);die();
		
		return $this->db->query($q) ? true : false;
	}

	
	public function post_receipts_ver2_old($data, $empno) {
		if (!(is_array($data) AND count($data)>0))
			return 0;
		$imploded_data = implode(',',$data);
		$sql="
			update payments
				set posted_by 	= {$this->db->escape($empno)},
					posted_on 	= NOW(), 
					status 	 	= 'posted'
				where id in ({$imploded_data})
		";
		$this->db->query($sql);
		return $this->db->affected_rows();		
		}

		
	public function post_receipts_ver2($data, $empno) {
		if (!(is_array($data) AND count($data)>0))
			return 0;		
		$chunked_data = array_chunk($data, 500);		
		$length_of_chuncked = count($chunked_data);
		$i=0;
		$insertions = 0;
		do {
			$imploded_data = implode(',',$chunked_data[$i]);
			$sql="
					update payments
					set posted_by 	= {$this->db->escape($empno)},
					posted_on 	= NOW(),
					status 	 	= 'posted'
					where id in ({$imploded_data})
				";
			$this->db->query($sql);
			$insertions  += (int)$this->db->affected_rows();			
			$i++;
		} while ($i < $length_of_chuncked );		
		return $insertions;			
	}
		
	
	
	public function get_student ($userid){
		$sql="
			SELECT
				py.id as payers_id,s.idno, s.lname, s.fname, s.mname, s.gender,
				bh.status AS bed_status,				
				if(isnull(sh.id), bap1.year_level,sh.year_level) as year_level,
				if(isnull(c.id),'',bs.section_name) as section,							
				if(isnull(sh.year_level), apg1.levels_id, ag.levels_id) as levels_id,
				if(isnull(c.id),bs.section_name,'') as section,			
				if(isnull(sh.id), bh.academic_years_id, sh.academic_terms_id) as current_yr_or_term_id,				
				sh.prospectus_id,
				if(isnull(sh.id), bh.id, sh.id) as student_histories_id,
				c.college_code,c.id as colleges_id,ap.id as ap_id, 
				if(isnull(sh.id),
					 ap1.description, ap.abbreviation ) as abbreviation,
				concat_ws(', ',a.home_address,br.name, t.name, pr.name, co.name) as full_home_address,
				concat_ws(', ',a.city_address,br2.name, t2.name, pr2.name, co2.name) as full_city_address,
				ph.phone_number,
				bh.enrollment_status
			FROM students as 					s
				LEFT JOIN payers 				py ON py.students_idno = s.idno
				LEFT JOIN student_histories 	sh ON	s.idno = sh.students_idno
				LEFT JOIN basic_ed_students     AS bes1 ON bes1.students_idno = s.idno
				left join basic_ed_histories 	bh on bh.students_idno = bes1.students_idno
				left join basic_ed_sections 	bs on bs.id = bh.basic_ed_sections_id
				LEFT JOIN prospectus            AS bp1 ON bp1.id = bh.prospectus_id
				LEFT JOIN prospectus_terms 		AS bpt1 ON bpt1.prospectus_id = bp1.id 
				LEFT JOIN academic_programs     AS ap1 ON ap1.id = bp1.academic_programs_id 
				LEFT JOIN bed_academic_programs AS bap1 ON bap1.academic_programs_id = ap1.id
				LEFT JOIN acad_program_groups   AS apg1 ON apg1.id = ap1.acad_program_groups_id
				LEFT JOIN prospectus 			p  ON	p.id = sh.prospectus_id
				LEFT JOIN academic_programs 	ap ON ap.id = p.academic_programs_id
				left join acad_program_groups 	ag on ag.id = ap.acad_program_groups_id
				LEFT JOIN colleges 			  	c  ON c.id = ap.colleges_id
				LEFT JOIN addresses 			a  ON a.students_idno = s.idno
				left join barangays 			br on br.id = a.home_barangays_id
				left join towns 				t  on t.id = a.home_towns_id
				left join provinces 			pr on pr.id = a.home_provinces_id
				left join countries 			co on co.id = a.home_countries_id
				left join barangays 			br2 on br2.id = a.city_barangays_id
				left join towns 				t2  on t2.id = a.city_towns_id
				left join provinces 			pr2 on pr2.id = a.city_provinces_id
				left join countries 			co2 on co2.id = a.city_countries_id
				left join phone_numbers 		ph on ph.students_idno = s.idno and ph.is_primary='Y'				
			WHERE
				s.is_active='Y'
			AND 
				idno = {$this->db->escape($userid)} 
			ORDER BY	sh.academic_terms_id DESC, bh.academic_years_id desc
			LIMIT 1
		";
		
		//print($sql); die();
		//log_message("INFO",print_r($sql,true));

		$query = $this->db->query($sql);
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
	
		if ( is_object($query) && $query->num_rows() > 0){
			return $query->row();
		} else
			return FALSE;
	}
	
	public function get_employee ($empno){
		$sql = "
			SELECT
				e.";
	}
	
	function privilege_enroll_student($student_id){
		$this->load->model('hnumis/Student_model');
		
		if ($this->Student_model->student_is_enrolled($student_id)){
			//student came from the dric as a first year student or a transferee... so we'll just update it...
			//todo: these queries must include calculation for year level...
			$this->load->model('hnumis/AcademicYears_model');
			$academic_term_obj = $this->AcademicYears_model->getCurrentAcademicTerm();
			$sql = "
			SELECT
				id
			FROM
				student_histories
			WHERE
				students_idno='{$student_id}'
			AND
				academic_terms_id='{$academic_term_obj->id}'
			ORDER BY
				id DESC
			LIMIT 1
			";
			$query = $this->db->query($sql);
			
			if ($query && $query->num_rows() > 0) {
				$result_obj = $query->row();
				return $this->Student_model->update_student_history($result_obj->id, array('can_enroll'=>'Y'));	
			}	
		} else {
			//prospectus model
			$this->load->model('hnumis/Prospectus_model');
			
			if ($prospectus_id = $this->Prospectus_model->student_prospectus_ids($student_id)){
				
				//$year_level = calculate_year_level($prospectus, $subjects_taken)...
				return $this->Student_model->insert_student_history ($student_id, $prospectus_id); 
			} else
				return FALSE;
		}
	}

	
	function get_ledger_data ($payers_id, $balance_only = FALSE, $before_linog = FALSE, $cut_off = FALSE, $exclude_written_off = FALSE){
		//October 3, 2013. Justin: added field academic_terms_id to results... 
		$sql = "SELECT debit, credit, date(transaction_date) as transaction_date "; 
		if (!$balance_only)
			$sql = $sql . ",ledger_id, academic_terms_id, transaction_detail, reference_number, remark,t.`status`, ay.end_year, at.term";
		$sql =  $sql . "
		FROM 
		(
			SELECT l.id as ledger_id, l.transaction_date, h.academic_terms_id, 'ASSESSMENT' as transaction_detail, 
				concat('TF#: ',l.reference_number,'-',s.year_level,'-',apr.abbreviation) as reference_number, l.debit, l.credit,l.remark,l.`status`
			FROM ledger l
			JOIN assessments s on s.id = l.assessments_id 
			JOIN student_histories h on h.id = s.student_histories_id
			-- JOIN prospectus pr on pr.id = h.prospectus_id
			-- JOIN academic_programs apr on apr.id = pr.academic_programs_id
			LEFT JOIN academic_programs apr on apr.id = s.academic_programs_id
			JOIN payers  r on r.students_idno = h.students_idno
			WHERE r.id = {$payers_id} "; 
		$sql .=  !$cut_off ? "":" AND l.transaction_date <= date_add('{$cut_off}', INTERVAL '23:59:59' HOUR_SECOND) ";
		
		
		$sql .= " UNION 
			SELECT l.id as ledger_id, l.transaction_date,   tre.id as academic_terms_id,'ASSESSMENT' as transaction_detail, concat('TF#: ',l.reference_number,'-',h.yr_level,'-', ifnull(lve.`level`,'none')) as reference_number, l.debit, l.credit,l.remark,l.`status`
			FROM ledger l
			JOIN assessments_bed s on s.id = l.assessments_bed_id
			JOIN basic_ed_histories h on h.id = s.basic_ed_histories_id
			JOIN academic_years aye on aye.id = h.academic_years_id
			JOIN academic_terms tre on tre.academic_years_id = aye.id and tre.`term`='1'
			LEFT JOIN levels lve on lve.id = h.levels_id
			JOIN payers r on r.students_idno =  h.students_idno and !isnull(r.students_idno)
			WHERE r.id = {$payers_id} ";
		$sql .=  !$cut_off ? "":" AND l.transaction_date <= date_add('{$cut_off}', INTERVAL '23:59:59' HOUR_SECOND) ";
		
		
		$sql .=" UNION 
			SELECT l.id as ledger_id, l.transaction_date, p.academic_terms_id,
				if(l.debit>0,c.description2, if(c.is_ledger='Y',c.description,c.description2)) as transaction_detail, 
				if(p.from_isis='Y',p.isis_refno,l.reference_number) as reference_number, l.debit, l.credit,l.remark,
				-- if(p.from_isis='Y',p.isis_status, l.status) as status
				l.`status`
			FROM ledger l
			JOIN payments     p on p.id = l.payments_id
			JOIN teller_codes c on c.teller_code = p.isis_tran_code and !isnull(p.isis_tran_code)
			WHERE p.payers_id  = {$payers_id} ";
		$sql .= !$cut_off ? "":" AND l.transaction_date <= date_add('{$cut_off}', INTERVAL '23:59:59' HOUR_SECOND) ";

		
		$sql .=" UNION  
			SELECT l.id as ledger_id, l.transaction_date, p.academic_terms_id, tc.description as transaction_detail, 
				if(p.from_isis='Y',p.isis_refno,l.reference_number) as reference_number, l.debit, l.credit,l.remark,
				-- if(p.from_isis='Y',p.isis_status,l.`status`) as status
				l.`status`
			FROM ledger l
			JOIN payments     p on p.id = l.payments_id
			JOIN payment_items im on im.payments_id = p.id
			join teller_codes tc on tc.id = im.teller_codes_id
			WHERE isnull(p.isis_tran_code) and p.payers_id = {$payers_id} "; 
		$sql .= !$cut_off ? "":" AND l.transaction_date <= date_add('{$cut_off}', INTERVAL '23:59:59' HOUR_SECOND) ";

		
		$sql .=" UNION
			select l.id as ledger_id, a.transaction_date, a.academic_terms_id,concat('Adjustment: ',a.description) as transaction_detail, 
				IF(LOCATE('WITH',a.description)> 0, concat(l.adjustments_id,' - ',l.remark),concat('Adj#: ',l.adjustments_id) ) AS reference_number,
				l.debit, l.credit,
				if(LOCATE('WITH',a.description)> 0,'',l.remark) as remark,l.`status`
			FROM ledger l
			JOIN adjustments a on a.id = l.adjustments_id 
			WHERE a.payers_id = {$payers_id} ";
		$sql .= !$cut_off ? "":" AND l.transaction_date <= date_add('{$cut_off}', INTERVAL '23:59:59' HOUR_SECOND) ";

		
		$sql .=" UNION
			SELECT l.id as ledger_id, l.transaction_date, h.academic_terms_id, s.scholarship as  transaction_detail,
				l.reference_number, l.debit, l.credit,l.remark,l.`status`
			FROM ledger l
			JOIN privileges_availed v  on v.id = l.privileges_availed_id and !isnull(v.student_histories_id) 
			join scholarships       s  on s.id = v.scholarships_id
			JOIN student_histories  h  on h.id = v.student_histories_id
			JOIN payers 		    r on r.students_idno = h.students_idno and !isnull(r.students_idno)
			WHERE r.id = {$payers_id} ";
		$sql .= !$cut_off ? "":" AND l.transaction_date <= date_add('{$cut_off}', INTERVAL '23:59:59' HOUR_SECOND) ";

		
		$sql .=" UNION
			SELECT l.id as ledger_id, l.transaction_date, tre.id as academic_terms_id, s.scholarship as  transaction_detail,
				l.reference_number, l.debit, l.credit,l.remark,l.`status`
			FROM ledger l						
			JOIN (select v.id, v.scholarships_id, h.academic_years_id
					from privileges_availed v 
					join basic_ed_histories h on h.id = v.basic_ed_histories_id
					join payers r on r.students_idno = h.students_idno 
					where r.id = {$payers_id} and v.student_histories_id is null ) v  on v.id = l.privileges_availed_id 
			join scholarships s  on s.id = v.scholarships_id
			JOIN academic_years aye on aye.id = v.academic_years_id
			JOIN academic_terms tre on tre.academic_years_id = aye.id and tre.`term`='1' ";
		$sql .= !$cut_off ? "":" AND l.transaction_date <= date_add('{$cut_off}', INTERVAL '23:59:59' HOUR_SECOND) ";
		
		
		$sql .=" UNION 
			SELECT l.id as ledger_id, l.transaction_date, sh.academic_terms_id, 'Re-enroll' as  transaction_detail,
				concat('Re-enroll#:',l.reference_number) as reference_number, l.debit, l.credit,l.remark,l.`status`
			FROM ledger l
			JOIN re_enrollments re  on re.id  = l.re_enrollments_id 
			JOIN enrollments enr on enr.id = re.enrollments_id
			JOIN student_histories sh on sh.id  = enr.student_history_id
			JOIN payers r on r.students_idno = sh.students_idno and !isnull(r.students_idno) 
			where r.id = {$payers_id} ";				
		$sql .=  !$cut_off ? "":" AND l.transaction_date <= date_add('{$cut_off}', INTERVAL '23:59:59' HOUR_SECOND) ";
		$sql .=") as t 
		LEFT JOIN academic_terms `at` ON `at`.id=t.academic_terms_id
		LEFT JOIN academic_years  ay ON ay.id=at.academic_years_id 
		WHERE t.`status` != 'abrakadabra54321' "
			. ($before_linog ? " AND t.transaction_date < '2013-10-15 23:59:59' " : "")
			. ($exclude_written_off ? " AND t.status != 'written_off' " : "")
			. " ORDER BY t.transaction_date,ledger_id 				
		";
		//if ($payers_id == '00026450') { print_r($sql);die(); }
		//print_r($sql);die();exit();  //debugline: by toyet 9.13.2017

		//log_message("INFO", print_r($sql,true));  // Toyet 5.19.2018
		//die();

		$query = $this->db->query($sql);
		if ($query && $balance_only && $query->num_rows() >= 0)
			return $query->result_array();
		if ($query && $query->num_rows() > 0)
			return $query->result_array(); else
			return array();
		
	}
	
	/**
	 * @author: Tatskie on 2015-03-02
	 * @description: an attempt to speed-up A/R processings  
	 * 
	 */
	public function get_ledger_data_balance_only($idno, $balance_only = TRUE, $before_linog = FALSE, $cut_off = FALSE, $exclude_written_off = FALSE){
		//print_r($cut_off);die();
		if (!$balance_only) return array(); //for ledger_data callers compatibility 
		if ($before_linog) return array(); //for ledger_data callers compatibility
		$sql = "
				SELECT l.debit, l.credit, date(l.transaction_date) as transaction_date
				FROM ledger l
				WHERE idno =  {$this->db->escape($idno)} ";						
		$sql .= !$cut_off ? "":" AND l.transaction_date <= date_add({$this->db->escape($cut_off)}, INTERVAL '23:59:59' HOUR_SECOND) "			 				
			. ($exclude_written_off ? " AND l.status != 'written_off' " : "")
			. " ORDER BY l.transaction_date, l.id "						
		;
		
		//var_dump($sql);die();
		
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() >= 0)
			return $query->result_array(); 
		else
			return array();
		
		
	}
	
	
	public function get_tuition_others($academic_year, $academic_term, $idno){
		$sql="
			select c.course_code,c.paying_units, tu.rate 
			from hnumis.enrollments en
			join hnumis.student_histories sh on sh.id = en.student_history_id
			join hnumis.academic_terms    tr on tr.id = sh.academic_terms_id 
			join hnumis.academic_years    ay on ay.id = tr.academic_years_id
			join hnumis.course_offerings  co on co.id = en.course_offerings_id
			join hnumis.tuition_others    tu on tu.courses_id = co.courses_id and tu.academic_terms_id = tr.id 
			join hnumis.courses           c  on c.id  = tu.courses_id
			where sh.students_idno = 5849410 /*6234004*/ and tr.term=2 and ay.end_year=2013 and
				find_in_set(sh.year_level,tu.yr_level) 				
			";
	}
	
	public function get_fees($academic_year, $academic_term, $idno){
		$sql="
			select lv.`level`, ap.abbreviation, fs.yr_level,fg.fees_group, sg.description, fs.rate
			from hnumis.fees_schedule       fs
			join hnumis.acad_program_groups pg on pg.id = fs.acad_program_groups_id
			join hnumis.academic_programs   ap on ap.acad_program_groups_id = pg.id
			join hnumis.academic_terms      tr on tr.id = fs.academic_terms_id
			join hnumis.academic_years      ay on ay.id = tr.academic_years_id
			join hnumis.fees_subgroups      sg on sg.id = fs.fees_subgroups_id
			join hnumis.fees_groups         fg on fg.id = sg.fees_groups_id
			join hnumis.levels              lv on lv.id = fs.levels_id
			join hnumis.prospectus  		  pr on pr.academic_programs_id = ap.id
			join hnumis.student_histories   sh on sh.prospectus_id = pr.id and 
															sh.academic_terms_id = tr.id and 
															find_in_set(sh.year_level,fs.yr_level)
			join hnumis.students            st on st.idno = sh.students_idno
			where ay.end_year=2013 and tr.term=2 and st.idno =  05512849
			order by ay.end_year,tr.term,fs.yr_level,fg.id, sg.description				
		";
	}	
		
	public function get_lab_fees($academic_year, $academic_term, $idno){
		$sql ="
			select c.course_code, c.paying_units, lf.rate
			from hnumis.enrollments en
			join hnumis.student_histories sh on sh.id = en.student_history_id
			join hnumis.academic_terms    tr on tr.id = sh.academic_terms_id
			join hnumis.academic_years    ay on ay.id = tr.academic_years_id
			join hnumis.course_offerings  co on co.id = en.course_offerings_id
			join hnumis.laboratory_fees   lf on lf.courses_id = co.courses_id and lf.academic_years_id = ay.id
			join hnumis.courses           c  on c.id  = lf.courses_id
			where ay.end_year=2013 and tr.term=2 and sh.students_idno = 5849410
		";
	}
	
		
	
	public function update_receipt_numbers ($data){
		$non_vat = empty($data['non_vat_receipt_no'])  ? "null" : $data['non_vat_receipt_no'];
		$vat  = empty($data['vat_receipt_no']) ? "null" : $data['vat_receipt_no'];   
		$sql = "
			INSERT into machines (`machine_ip`, `vat_receipt_no`, `non_vat_receipt_no`,inserted_by,inserted_on,name)
				VALUES (
					{$this->db->escape($data['machine_ip'])}, 
					$vat, 
					$non_vat,
					{$this->session->userdata('empno')},
					now(),
					''
				)
			ON DUPLICATE KEY UPDATE
				`vat_receipt_no` 		= VALUES(`vat_receipt_no`), 
				`non_vat_receipt_no`	= VALUES(`non_vat_receipt_no`),
				`updated_by` 			= {$this->session->userdata('empno')},
				`updated_on` 			= now() 
			";
		//die($sql);
		return $this->db->query($sql);
	}
	
	public function checkout ($academic_programs_id, $year_level, $levels_id, $payers_id, $empno, $payments, $items, $receipt_no, $machine_ip, $tax_type, $payment_methods_ids=null){
		//payment_methods_ids can come from the session... if a carryover is done...
		$method_keys = $this->config->item('payment_method_keys');
		//print_r($payments);die();
		$methods = $this->config->item('payment_methods');
		$current_academic_term_id = $this->academic_terms_model->getCurrentAcademicTermID();
		//lets calculate first the total amount tendered:
		$total_payments = 0;
		//print_r($items);die();
		
		foreach ($items as $item){
			$total_payments += $item->amount;
		}
		
		$payment_ids = array();
		foreach ($items as $item){
			$is_ledger = $this->item_is_ledger($item->teller_code);
			
			$sql = "
				INSERT into
				payments (`academic_programs_id`, `year_level`, `levels_id`,`academic_terms_id`, `transaction_date`, `machine_ip`, `tellers_id`, `receipt_no`, `payers_id`, `receipt_amount`, `amount_tendered`, `remark`, `is_ledger`, `status`)
				VALUE
				({$this->db->escape($academic_programs_id)}, {$this->db->escape($year_level)}, {$this->db->escape($levels_id)},{$current_academic_term_id->current_academic_term_id}, NOW(), {$this->db->escape($machine_ip)}, '{$this->teller_id($empno)}', {$this->db->escape($receipt_no)}, {$this->db->escape($payers_id)}, {$this->db->escape($item->amount)}, {$this->db->escape($total_payments)}, {$this->db->escape($item->remarks)}, '{$is_ledger}', 'unposted')
				";
			//file_put_contents('test.txt', date('[Y-m-d H:i:s] - ') . "{$sql}\n", FILE_APPEND);
			if ($query = $this->db->query($sql)){
				$payment_id = $this->db->insert_id();
				if ($payment_id < 1){
					return (object)array('error_found'=>TRUE,'error'=>'Error while retrieving payments inserted_id');
				}
				$payment_ids[] = $payment_id;
				$item_sql = "
					INSERT into
						payment_items (`teller_codes_id`, `payments_id`, `amount`)
					VALUE
						({$this->db->escape($item->teller_code)}, '{$payment_id}', {$this->db->escape($item->amount)})
					";
					//file_put_contents('test.txt', date('[Y-m-d H:i:s] - ') . "{$item_sql}\n", FILE_APPEND);
				if (!$this->db->query($item_sql)){
					return (object)array('error_found'=>TRUE,'error'=>'Failed to save payment items!');						
				}
			}else{
				return (object)array('error_found'=>TRUE,'error'=>$this->db->_error_message());
			}
			$receipt_no++;
		}
		
		if (is_null($payment_methods_ids))
			$payment_methods_ids = array();
		
		foreach ($payments as $payment){
			
			if (isset($methods[$payment->type])){
				$payment_method_sql = "
					INSERT into
						payment_methods (`payment_method`, `amount`)
					VALUE
						({$this->db->escape($methods[$payment->type])}, {$this->db->escape($payment->amount)})
					";
				//file_put_contents('test.txt', date('[Y-m-d H:i:s] - ') . "{$payment_method_sql}\n", FILE_APPEND);
				if($this->db->query($payment_method_sql)){
					$payment_methods_id = $this->db->insert_id();
					$payment_methods_ids[] = $payment_methods_id;					
					$payment_method_details_sql = "
						INSERT into 
							payment_method_details(`payment_methods_id`, `key`, `value`)
						VALUE
						";
					foreach($method_keys[$payment->type] as $key){
						$val = isset($payment->$key) ? $payment->$key : "";
						$payment_method_details_sql .= "('{$payment_methods_id}', '{$key}', '{$val}'),\n";
					}
					$payment_method_details_sql = rtrim($payment_method_details_sql, ",\n");
					IF (!$this->db->query($payment_method_details_sql)){
						return (object)array('error_found'=>TRUE,'error'=>'Query error on payment_method_details_sql');						
					}
				}else{
					//return (object)array('error_found'=>TRUE,'error'=>'Query error on payment_method_sql');
				}
			}else{
				//return (object)array('error_found'=>TRUE,'error'=>'Method is not set!');
			}
		}
		
		foreach ($payment_ids as $payment_id){
			$payments_payment_method_sql = "
				INSERT into
					payments_payment_methods (`payments_id`, `payment_methods_id`)
				VALUES
				";
		
			foreach ($payment_methods_ids as $payment_methods_id){
				$payments_payment_method_sql .= "('{$payment_id}', '{$payment_methods_id}'),\n";
			}
			$payments_payment_method_sql = rtrim($payments_payment_method_sql, ",\n");
			//file_put_contents('test.txt', date('[Y-m-d H:i:s] - ') . "{$payments_payment_method_sql}\n", FILE_APPEND);
			IF (!$this->db->query($payments_payment_method_sql)){
				return (object)array('error_found'=>TRUE,'error'=>'Failed to insert data to payments payment methods!');
			}
		}
		
		//We place the payment_methods_ids to the session... 
		$this->session->set_userdata('payment_methods_ids', $payment_methods_ids);
		$field = ($tax_type == 'nonvat' ? 'non_vat_receipt_no' : 'vat_receipt_no');
		
		IF (!$this->db->query("UPDATE machines set `{$field}`='{$receipt_no}' WHERE machine_ip='{$machine_ip}'")){
			return (object)array('error_found'=>TRUE,'error'=>'Failed to update receipts table!');
		}		
		return (object)array('error_found'=>FALSE);
	}
	
	public function other_courses_payments ($academic_terms_id, $year_level) {
		$sql = "
			SELECT
				courses_id,rate
			FROM
				tuition_others
			WHERE
				academic_terms_id={$this->db->escape($academic_terms_id)}
			AND
				yr_level={$this->db->escape($year_level)}
			";
		
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			$return = array();
			foreach($query->result() as $result){
				$return[$result->courses_id] = $result->rate;
			}
			return $return;
		} else{
			return FALSE;
		}
	}
	
	//Edited by: Tatskie on 7th May 2013 [and t.yr_level = sh.year_level]
	public function other_courses_payments_histories_id ($student_histories_id) {
		$sql = "
		SELECT
			courses_id,rate
		FROM
			tuition_others t
		LEFT JOIN
			academic_terms at
		ON
			at.id=t.academic_terms_id
		LEFT JOIN
			student_histories sh
		ON
			sh.academic_terms_id=at.id and t.yr_level = sh.year_level
		WHERE
			sh.id={$this->db->escape($student_histories_id)}
		";
		
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			$return = array();
			foreach($query->result() as $result){
				$return[$result->courses_id] = $result->rate;
			}
			return $return;
		} else{
			return FALSE;
		}
	}
	
	public function all_laboratory_fees ($academic_terms_id){
		$sql = "
			SELECT
				courses_id,rate
			FROM
				laboratory_fees lf
			LEFT JOIN
				academic_terms at
			ON
				at.academic_years_id=lf.academic_years_id
			WHERE
				at.id={$this->db->escape($academic_terms_id)}
			";
		
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			$return = array();
			foreach($query->result() as $result){
				$return[$result->courses_id] = $result->rate;
			}
			return $return;
		} else{
			return FALSE;
		}
	}
	
	public function teller_code_info ($teller_code){
		$sql = "
			SELECT
				t.id, t.teller_code, t.description, is_ledger, default_amount, t.tax_type, t.status
			FROM
				teller_codes t
			WHERE
				t.id={$this->db->escape($teller_code)}
			LIMIT 1";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->row(); else
			return FALSE;
	}
	
	public function bank_info ($bank_id){
		$sql = "
			SELECT
				code, name
			FROM
				banks
			WHERE
				id={$this->db->escape($bank_id)}
			LIMIT 1
			";
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0)
			return $query->row(); else
			return FALSE;
	}
	
	/**
	 * 
	 * 
	 * @param unknown_type $empno
	 * @return FALSE when no result, int tellers id when a result is found
	 */
	public function teller_id ($empno){
		$sql = "
			SELECT
				id
			FROM
				tellers
			WHERE
			";
		if ($empno < 20000){
			$sql .= "employees_empno = {$this->db->escape($empno)}";
		} else {
			$sql .= "students_idno = {$this->db->escape($empno)}";
		}
		
		$query = $this->db->query($sql);
		
		if ($query AND $query->num_rows() > 0){
			$row = $query->row();
			return $row->id;
		} else {
			return FALSE;
		}
	}
	
	public function item_is_ledger ($item_id){
		$sql = "
			SELECT
				is_ledger
			FROM
				teller_codes
			WHERE
				id={$this->db->escape($item_id)}
			";
		if ($query = $this->db->query($sql)){
			$row = $query->row();
			return ($row->is_ledger);
		}  
			return FALSE;
	}
	
	/**
	 * Returns Summary of Receipts with a given Date
	 * @param String (Date) $date
	 */
	public function daily_receipt_summary ($date, $machine_ip=''){
		$sql="
			select * from (
			select p.machine_ip, concat(e.lname,' ','(',e.empno,')' ) as teller, t.teller_code, t.description,
					p.id as payment_id, s.idno, concat(s.lname,', ', s.fname) as stud, 
					p.transaction_date as transaction_date, p.receipt_no, p.receipt_amount as amount,p.`status`, pm.payment_method
				from payments        p
				join payers          r on r.id = p.payers_id
				join students        s on s.idno = r.students_idno
				join tellers         tl on tl.id = p.tellers_id
				join employees       e  on e.empno =  tl.employees_empno
				join payment_items   i on i.payments_id = p.id
				join teller_codes    t on t.id = i.teller_codes_id
				join payments_payment_methods ppm on ppm.payments_id = p.id
				join payment_methods pm on pm.id = ppm.payment_methods_id
			UNION 
			select p2.machine_ip, concat(s2b.lname,' ','(',s2b.idno,')' ) as teller, t2.teller_code, t2.description,
					p2.id as payment_id, s2.idno, concat(s2.lname,', ', s2.fname) as stud, 
					p2.transaction_date, p2.receipt_no, p2.receipt_amount as amount,p2.`status`, pm.payment_method
				from payments        p2
				join payers          r2 on r2.id = p2.payers_id
				join students        s2 on s2.idno = r2.students_idno
				join tellers         tl2 on tl2.id = p2.tellers_id
				join students        s2b  on s2b.idno =  tl2.students_idno
				join payment_items   i2 on i2.payments_id = p2.id
				join teller_codes    t2 on t2.id = i2.teller_codes_id
				join payments_payment_methods ppm on ppm.payments_id = p2.id
				join payment_methods pm on pm.id = ppm.payment_methods_id
			) as test
			where date(transaction_date) = {$this->db->escape($date)}
		";
		if(!empty($machine_ip))
			$sql .= " AND machine_ip='{$machine_ip} ";
		  
		$sql .= " order by machine_ip, teller_code, transaction_date, receipt_no 
			";
		//print_r($sql);	die();
		$query = $this->db->query($sql);
		
		//echo '<pre>';
		//print_r($this->db->last_query());
		//die();
		
		if ($query)
		{
			return $query->result();
		}
		else
		{
			return FALSE;
		}
	}

	public function receipts ($date, $payment_methods){
		$sql="
		select * from 
			(
			select p.machine_ip, concat(e.lname,', ',e.fname, '(',e.empno,')' ) as teller, t.description, 
			p.id as payment_id, s.idno, concat(s.lname,', ', s.fname) as payor,
			p.transaction_date as transaction_date, p.receipt_no, p.receipt_amount as amount, p.`status`, pm.payment_method
			from payments        p
			join payers          r on r.id = p.payers_id
			join students        s on s.idno = r.students_idno
			join tellers         tl on tl.id = p.tellers_id
			join employees       e  on e.empno =  tl.employees_empno
			join payment_items   i on i.payments_id = p.id
			join teller_codes    t on t.id = i.teller_codes_id
			join payments_payment_methods ppm on ppm.payments_id = p.id
			join payment_methods pm on pm.id = ppm.payment_methods_id
		UNION
			select p2.machine_ip, concat(s2b.lname,', ',s2b.fname,'(',s2b.idno,')' ) as teller, t2.description,
			p2.id as payment_id, s2.idno, concat(s2.lname,', ', s2.fname) as payor,
			p2.transaction_date, p2.receipt_no, p2.receipt_amount as amount,p2.`status`, pm.payment_method
			from payments        p2
			join payers          r2 on r2.id = p2.payers_id
			join students        s2 on s2.idno = r2.students_idno
			join tellers         tl2 on tl2.id = p2.tellers_id
			join students        s2b  on s2b.idno =  tl2.students_idno
			join payment_items   i2 on i2.payments_id = p2.id
			join teller_codes    t2 on t2.id = i2.teller_codes_id
			join payments_payment_methods ppm on ppm.payments_id = p2.id
			join payment_methods pm on pm.id = ppm.payment_methods_id
			) as student_payments				
		where date(transaction_date) = {$this->db->escape($date)} 
		";		
		$sql = ($payment_methods=='All' ? $sql. " " : $sql . " and payment_method = {$this->db->escape($payment_methods)} " );
		$sql = $sql . " order by machine_ip, receipt_no, transaction_date, description";
		//print_r($sql);
		//die();
		$query = $this->db->query($sql);
		return ($query ?  $query->result() : FALSE);
	}
	
	//Justin...
	public function employee_payers_id ($empno){
		$this->db->select('id')->from('payers as p')->where('employees_empno', $empno);
		$query = $this->db->get();
	
		if( $query && $query->num_rows() > 0)
			return $query->row()->id; else
			return FALSE;
	}
	
	public function payers_basic_information ($payers_id){
		//this will make me forget how to make a proper MYSQL query...
		$this->db->select("payers.id as number,
				IF(NOT ISNULL(payers.employees_empno), CONCAT(employees.lname, ', ', employees.fname),
				IF(NOT ISNULL(payers.other_payers_id), other_payers.name,
				IF(NOT ISNULL(payers.inhouse_id), offices.office, ''))) as fullname", FALSE);
		$this->db->from('payers');
		$this->db->join('employees', 'employees.empno=payers.employees_empno', 'left');
		$this->db->join('other_payers', 'other_payers.id=payers.other_payers_id', 'left');
		$this->db->join('offices', 'offices.id=payers.inhouse_id', 'left');
		$this->db->where('payers.id', $payers_id);
		$query = $this->db->get();
		if ($query && $query->num_rows()>0)
			return $query->row(); else
			return FALSE;
	}

	public function new_daily_receipt_summary ($date, $machine=''){
		$sql = "
			select distinct p.machine_ip, 
				if(!isnull(e.empno),concat(e.lname,' ','(',e.empno,')'),if(!isnull(s2.idno),concat(s2.lname,'(',s2.idno,')'),'Not Found'))  as teller, 
				c.teller_code, c.description, c.tax_type,
				p.id as payment_id,  p.transaction_date as transaction_date, p.receipt_no,
				p.receipt_amount as whole_amount, 
				if(p.receipt_amount <  pm.amount, p.receipt_amount, pm.amount) as amount,
				pm.payment_method,
				p.`status`, 
				if(!isnull(s.idno),concat(s.lname,', ', s.fname), if(!isnull(e2.empno),concat_ws(', ',e2.lname,e2.fname), if(!isnull(of.id),of.office, if(!isnull(op.id),op.name,'Not Found')))) as payor, 
				if(!isnull(s.idno),s.idno, if(!isnull(e2.empno),e2.empno,if(!isnull(of.id),of.id,  if(!isnull(op.id),op.id,'Not Found')))) as payor_id
			from payments_payment_methods ppm
				left join payments        p  on p.id = ppm.payments_id
				left join payment_methods pm on pm.id = ppm.payment_methods_id
				left join payers          r  on r.id    = p.payers_id
				left join students        s  on s.idno  = r.students_idno -- student payor
				left join employees 	  e2 on e2.empno = r.employees_empno -- employee payor
				left join offices 		  of on of.id = r.inhouse_id  -- office payor
				left join other_payers    op on op.id = r.other_payers_id -- other payors
				left join tellers         t  on t.id    = p.tellers_id
				left join employees       e  on e.empno =  t.employees_empno -- employee teller
				left join students 		  s2 on s2.idno = t.students_idno -- student teller
				left join payment_items   i  on i.payments_id = p.id 
				left join teller_codes    c  on c.id    = i.teller_codes_id
			 where date(p.transaction_date) = '{$date}'				
			";		
		
		if (!empty($machine)){
			$sql .= " AND machine_ip='{$machine}' "; 
		}
		$sql .= " order by machine_ip, teller_code, transaction_date, receipt_no ";
				
		$query = $this->db->query($sql);
		if ($query)
			return $query->result(); 
		else
			return FALSE;
	}
	
	
	function ListStudentLedgerEntries($payer_id){
		$result = null;
			
		$q = " SELECT
					a.id,
					a.transaction_date,
					a.debit,
					a.credit
				FROM
					ledger AS a LEFT JOIN payments AS b ON a.payments_id=b.id 
					LEFT JOIN adjustments AS c ON a.adjustments_id=c.id
					LEFT JOIN assessments AS d ON a.assessments_id=d.id
					LEFT JOIN privileges_availed AS e ON a.privileges_availed_id=e.id
					LEFT JOIN payers AS f ON f.id=b.payers_id
				WHERE
					f.id = {$this->db->escape($payer_id)} 
				ORDER BY a.transaction_date DESC";
		
		print($q);
		die();
		$query = $this->db->query($q);
			
		if($query->num_rows() > 0){
		$result = $query->result();
		}
			
		return $result;
		
	}


	public function update_student($what,$idnum, $levels_id, $yrlevel, $prospectus_id=0,$academic_term_and_year){
		//echo "herer";die();
		switch ($what) {
			case 'College':
				$sql = "
					INSERT INTO `student_histories` (`students_idno`, `academic_terms_id`, `prospectus_id`, `year_level`, `can_enroll`, `inserted_by`, `inserted_on`) VALUES 
						(
						'{$idnum}',
						'{$academic_term_and_year->current_academic_term_id}',
						'{$prospectus_id}',
						'{$yrlevel}',
						'N',
						'{$this->session->userdata('empno')}',
						now()
						)
					";
				break;
			case 'Basic Ed':
				$sql = "
					INSERT INTO `basic_ed_histories` (`students_idno`, `academic_years_id`, `levels_id`, `yr_level`) VALUES
					(
						'{$idnum}',
						'{$academic_term_and_year->current_academic_year_id}',
						'{$levels_id}',
						'{$yrlevel}'						
					)
				
				";
				
				break;			
		}
		//echo $sql; die(); 		
		$query = $this->db->query($sql);
		return $this->db->insert_id() ? $this->db->insert_id() : false; 
	} 

	public function daily_payment_method_summary($date, $machine_ip=''){
		$sql = "
				select machine_ip, payment_method, sum(amount) amount_sum
				from 
					(
					SELECT pm.id, p.machine_ip, pm.payment_method, pm.amount
					FROM (select * from payment_methods group by id) pm 
					LEFT JOIN payments_payment_methods ppm on ppm.payment_methods_id = pm.id
					LEFT JOIN payments p ON p.id = ppm.payments_id
					WHERE 
						p.`status` NOT IN ('void','deleted')
						and pm.payment_method <> 'Cash'
						and date(p.transaction_date) = '{$date}'					
				";
		$sql = (empty($machine_ip) ? $sql : $sql . " and p.machine_ip='{$machine_ip}'") ;
		$sql .= " group by pm.id ) t
				group by machine_ip, payment_method
				";
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->result();
		} else
			return array();
	}
	
	public function get_unposted_students_payment($payers_id){
		$sql = "
			SELECT if(p.receipt_amount<0, abs(p.receipt_amount), 0) debit, if(p.receipt_amount >= 0, p.receipt_amount,0) credit, 
				null ledger_id,date(p.transaction_date) transaction_date, p.academic_terms_id,
				c.description2 transaction_detail, p.receipt_no AS reference_number, p.remark, p.`status`, ay.end_year, at.term 
			FROM payments p
				JOIN payment_items i on i.payments_id = p.id
				JOIN teller_codes c on c.id = i.teller_codes_id
				LEFT JOIN academic_terms at ON at.id=p.academic_terms_id
				LEFT JOIN academic_years ay ON ay.id=at.academic_years_id
			WHERE p.from_isis='N' AND c.is_ledger='Y' AND p.`status`='unposted' AND p.payers_id = '{$payers_id}'				
		";
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->result_array(); 
		else
			return array();
	}

	
	function summer2014_patch_insert_history($students_idno, $prospectus_id, $academic_terms_id, $year_level, $can_enroll){
		$sql ="
			insert ignore into student_histories (students_idno,prospectus_id,academic_terms_id,year_level,can_enroll,inserted_by,inserted_on) values
			($students_idno, $prospectus_id, $academic_terms_id, $year_level, '$can_enroll', '$students_idno', now() )
		";
		//print_r($sql);die();
		if($this->db->query($sql))
			return $this->db->insert_id(); else
			return FALSE;
	
	}
	
	function get_unposted_payments_for_summer_patch2014($payers_id){
		$sql = "
			select p.receipt_amount
			from payments p
			where p.`status`='unposted' and p.from_isis='N'
			and p.payers_id = '{$payers_id}'
		";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return false;				
	}
	
	function student_has_current_academic_term ($idno, $academic_terms_id=0){
		$this->db->select('academic_terms.id')
			->from('student_histories')
			->join('academic_terms', 'student_histories.academic_terms_id=academic_terms.id', 'left')
			->where('students_idno', $idno)
			->limit(1);
		if($academic_terms_id==0)
			$this->db->where('academic_terms.status', 'current'); else
			$this->db->where('academic_terms.id', $academic_terms_id);
			
		$query = $this->db->get();
		if($query->num_rows() > 0)
			return TRUE; else
			return FALSE;
	}

	// Found! -Toyet 10.24.2018
	// Changed:  order by condition is changed
	//             from:  order_by('id', 'desc')
	//             to  :  order_by('academic_terms_id', 'desc')
	//           this will ensure that the last term is selected
	//                                     - Toyet 10.24.2018
	function match_history_to_current ($student_idno){
		$previous_history = (array)$this->db->select('*')
			->from('student_histories')
			->where('students_idno', $student_idno)
			->order_by('academic_terms_id', 'desc')
			->limit(1)->get()->row();
		
		$academic_terms_id = $this->db->select('id')->from('academic_terms')->where('status','current')->get()->row()->id; //current academic term
		if(!$this->student_has_current_academic_term($student_idno)){
			unset($previous_history['id']);
			unset($previous_history['extension_end']);
			unset($previous_history['updated_by']);
			unset($previous_history['updated_on']);
			unset($previous_history['inserted_on']);
			$previous_history['inserted_by'] = $this->session->userdata('empno');
			$previous_history['academic_terms_id'] = $academic_terms_id;
			$previous_history['can_enroll'] = 'N';
			$this->db->set('inserted_on', 'NOW()', FALSE);
			$this->db->insert('student_histories', $previous_history);
			if($this->db->insert_id() > 0)
				return TRUE; else
				return FALSE;
		} else {
			return TRUE;
		}
	}
	
	//
	// There is a similarly named function located in \models\hnumis\student_model.php
	// Comment made by Toyet 10.24.2018 for guidance purposes
	function insert_student_history ($student_idno, $academic_terms_id=0, $data=array()){
		log_message("INFO", print_r("THE PROGRAM PASSES HERE...")); // Toyet 10.24.2018
		$previous_history = (array)$this->db->select('*')
			->from('student_histories')
			->where('students_idno', $student_idno)
			->order_by('academic_terms_id', 'desc')
			->limit(1)->get()->row();
		if($academic_terms_id==0)
			$academic_terms_id = $this->db->select('id')->from('academic_terms')->where('status','current')->get()->row()->id; //current academic term
		
		foreach($data as $key=>$val){
			if(array_key_exists($key, $previous_history)){
				$previous_history[$key] = $val;
			}
		}
		
		unset($previous_history['id']);
		unset($previous_history['extension_end']);
		unset($previous_history['updated_by']);
		unset($previous_history['updated_on']);
		unset($previous_history['inserted_on']);
		$previous_history['inserted_by'] = $this->session->userdata('empno');
		$previous_history['academic_terms_id'] = $academic_terms_id;
		$this->db->set('inserted_on', 'NOW()', FALSE);
		$this->db->insert('student_histories', $previous_history);
		if($this->db->insert_id() > 0)
			return TRUE; else
			return FALSE;
	}
	
	function update_student_history ($history_id, $data){
		$this->db->set('updated_on', 'NOW()', FALSE);
		$this->db->set('updated_by', $this->session->userdata('empno'));			
		$this->db->where('id', $history_id);			
		$this->db->update('student_histories', $data);			
		return $this->db->affected_rows();											
	}
	
	function is_assessed($academic_years_id,$academic_terms_id,$payers_id){
		
		$sql = "
			select sum(assessment_count) as assessment_count 
				from
				(
						select count(*) as assessment_count
						from student_histories h
						join payers r on r.students_idno = h.students_idno
						join assessments a on a.student_histories_id = h.id
						join ledger l on l.assessments_id = a.id
						where h.academic_terms_id = {$this->db->escape($academic_terms_id)} and r.id = {$this->db->escape($payers_id)}
					union all
						select count(*)  as assessment_count
						from basic_ed_histories bh
						join assessments_bed ab on ab.basic_ed_histories_id = bh.id
						join payers r on r.students_idno = bh.students_idno
						join ledger l on l.assessments_bed_id = ab.id
						where bh.academic_years_id = {$this->db->escape($academic_years_id)} and r.id = {$this->db->escape($payers_id)}
				) t 
			";
		//print_r($sql);die();
		$query = $this->db->query($sql);
		if ($query && $query->num_rows() > 0)
			return $query->row()->assessment_count;
		else
			return FALSE;
		
	}
}

?>