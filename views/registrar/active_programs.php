<div class="row-fluid span12">
	<form method="post" id="active_programs_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
		<fieldset>
			<div class="control-group">
				<div class="controls">
					<select id="active_colleges" name="college">
						<option value="">-- Select College --</option>
<?php foreach ($colleges as $college): ?>
						<option value="<?php echo $college->id; ?>" <?php if(isset($college_id) && $college_id==$college->id) echo ' selected="selected" '?>><?php echo $college->name; ?></option>
<?php endforeach; ?>
					</select>
					<span id="active_loading_img" style="margin-left:10px;"><img style="width:30px;height:30px" src="<?php echo base_url($this->config->item('loading_image')); ?>" /></span>
				</div>
			</div>
			<div>
				<table class="table table-striped table-bordered mediaTable activeMediaTable" id="active_programs_table">
					<thead>
						<tr>
							<th width="3%"><input type="checkbox" id="active_select_all" /></th>
							<th width="25%">Program Abbreviation</th>
							<th width="72%">Program Name</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="control-group">
				<div class="controls">
					<button class="disabled btn btn-danger submit" id="deactivate_active">Freeze Program(s)</button>
					<button class="disabled btn btn-warning submit" id="unoffer_active">Do not Offer Program(s) for First Year</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script>
$(document).ready(function(){
<?php if(!empty($college_id)): ?>
	$('#active_loading_img').show();
	$('#inactive_loading_img').show();
	$('#unoffered_loading_img').show();
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=<?php echo $college_id; ?>&status=O",
		dataType: "json",
		success: function(data){
			var rows = make_rows(data);
			$('#active_programs_table > tbody').html(rows);
			$('#active_loading_img').hide();
			$('#deactivate_active').removeClass('disabled');
			$('#unoffer_active').removeClass('disabled');
		}	
	});
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=<?php echo $college_id; ?>&status=F",
		dataType: "json",
		success: function(data){
			var rows = make_rows(data);
			$('#inactive_programs_table > tbody').html(rows);
			$('#inactive_loading_img').hide();
			$('#activate_inactive').removeClass('disabled');
		}	
	});
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=<?php echo $college_id; ?>&status=U",
		dataType: "json",
		success: function(data){
			var rows = make_rows(data);
			$('#unoffered_programs_table > tbody').html(rows);
			$('#unoffered_loading_img').hide();
			$('#activate_unoffered').removeClass('disabled');
			$('#deactivate_unoffered').removeClass('disabled');
		}	
	});
<?php else: ?>
	$('#active_loading_img').hide();
	$('#inactive_loading_img').hide();
	$('#unoffered_loading_img').hide();
<?php endif; ?>
});

$('#active_colleges').bind('change', function(){
	var college_id = $('#active_colleges option:selected').val();

	$('#active_loading_img').show();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
		dataType: "json",
		success: function(data){
			var rows = make_rows(data, 'active');
			$('#active_programs_table > tbody').html(rows);
			$('#active_loading_img').hide();
		}	
	});

	$('#deactivate_active').removeClass('disabled');
	$('#unoffer_active').removeClass('disabled');
});


function make_rows(data, status){
	var rows='';
	
	
	for (var i = 0; i < data.length; i++) {
		rows = rows + '<tr><td><input type="checkbox" class="' + status + '_selector" name="program_ids[]" value="' + data[i].id + '"/></td><td><a title="click here to view prospectus" href="<?php echo site_url('/registrar/prospectus'); ?>/'+ data[i].id + '">' + data[i].abbreviation + '</a></td><td>'+ data[i].description + '</td></tr>';
	}
	return rows;
}

$('#active_select_all').bind('change', function(){
	
	if ($(this).is(':checked')){
		$('.active_selector').each(function(){
			$(this).attr('checked', 'checked');
		});
	} else {
		$('.active_selector').each(function(){
			$(this).removeAttr('checked');
		});
	}
});

</script>