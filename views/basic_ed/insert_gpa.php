<?php 
	if ($stud_status != 'active') {
		print("<h3>Student NOT Active! GPA cannot be inputted!</h3>");
	} else {
?>

<?php if (isset($message) && $tab=='14') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
<div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:5px; ">
 <form method="post" id="gpaform">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "insert_gpa"/>
	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" > 
	<input type="hidden" name="acadYear_id" value="<?php print($acadYear_id); ?>" > 
	<table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
	   <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    <strong><font face="Verdana, Arial, Helvetica, sans-serif"> INPUT GPA in <?php print($acadYear); ?></font></strong>
		   </div>
		   </td>
 	</table>	

		  
	 <div style="margin-left:20px; width:90%; margin-bottom:20px; ">
	     
	      <table width="18%" align="left" cellpadding="0" cellspacing="0" style="width:50%;">
			  <tr>
				 				
				  <td> 
				 	 <div style="margin-left:35px;">
				    <label class="control-label" for="gpa"><b> GPA:</b></label>
						 <input type="text" name="gpa" class="gpa input-xlarge" style="width:20px; text-align:left">
					</div>
				  </td>
		 	 </tr>
		 	 <tr>
		 	     <div style="margin-left:25px;">
		 		 <td colspan = "2">
		 		 </div>
		 			 <button class="btn btn-primary" type="submit" style="margin-left:10px;" >Assign GPA</button>
		 		</div>	 
		 		 </td>
		 		 
		 	 </tr>
		  </table>
 	 </div>
 	 
  
</form>
 </div>
 
 <?php }?>
 <script>
$(document).ready(function(){
	$('#gpaform').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			amount: { required: true, minlength: 1 },
			type: {required: true},
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	$(".gpa").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});
</script>
 