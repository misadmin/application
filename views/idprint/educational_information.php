<?php //print_r($educational_background);die(); ?>
<div class="row-fluid">
	<div class="span12">
<?php if (isset($message) && $tab=='educ_info') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
		<form id="student_educational_information_form" method="post" action="" class="form-horizontal">
			<input type="hidden" name="action" value="update_student_educational_information" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<div class="control-group formSep">
					<label for="primary_school" class="control-label">Primary School:</label>
					<div class="controls" id="primary_school_controls">
						<input type="text" name="Primary" id="primary" class="input-xlarge" 
							value="<?php echo (isset($educational_background['Primary']) ? $educational_background['Primary'] : "");?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="primary_school_year" class="control-label">School Year:</label>
					<div class="controls" id="primary_school_year_controls">
						<input type="text" name="Primary_sy" id="primary_school_year" class="input-xlarge" 
							value="<?php echo (isset($educational_background['Primary_sy']) ? $educational_background['Primary_sy'] : "");?>" placeholder="yyyy-yyyy" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="intermediate_school" class="control-label">Intermediate School:</label>
					<div class="controls" id="intermediate_school_controls">
						<input type="text" name="Intermediate" id="intermediate_school" class="input-xlarge" 
							value="<?php echo (isset($educational_background['Intermediate']) ? $educational_background['Intermediate'] : "");?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="intermediate_school_year" class="control-label">School Year:</label>
					<div class="controls" id="intermediate_school_year_controls">
						<input type="text" name="Intermediate_sy" id="intermediate_school_year" class="input-xlarge" 
							value="<?php echo (isset($educational_background['Intermediate_sy']) ? $educational_background['Intermediate_sy'] : "");?>" placeholder="yyyy-yyyy" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="secondary_school" class="control-label">Secondary (HS) School:</label>
					<div class="controls" id="secondary_school_controls">
						<input type="text" name="Secondary" id="Secondary" class="input-xlarge" 
							value="<?php echo (isset($educational_background['Secondary']) ? $educational_background['Secondary'] : "");?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="secondary_school_year" class="control-label">School Year:</label>
					<div class="controls" id="secondary_school_year_controls">
						<input type="text" name="Secondary_sy" id="Secondary_sy" class="input-xlarge" 
							value="<?php echo (isset($educational_background['Secondary_sy']) ? $educational_background['Secondary_sy'] : "");?>" placeholder="yyyy-yyyy" />
					</div>
				</div> 
				<div class="control-group formSep">
					<label for="secondary_school_address" class="control-label">Secondary School Address:</label>
					<div class="controls" id="secondary_school_address_controls">					
						<div style="margin-top:6px" id="secondary_school_address_control_text"><strong><?php echo isset($educational_background['Secondary_address']) ? $educational_background['Secondary_address'] : '' ;?></strong>&nbsp;<small><a href="javascript://" id="secondary_school_address_control"  class="edit_toggle" >Edit</a></small></div>
						<div id="secondary_school_address_control_controls"></div>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="last_school" class="control-label">(If Transferee) Last School Attended:</label>
					<div class="controls" id="last_school_controls">
						<input type="text" name="last_school" id="last_school" class="input-xlarge" value="<?php if(isset($last_school)) echo $last_school; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="last_school_year" class="control-label">School Year:</label>
					<div class="controls" id="last_school_year_controls">
						<input type="text" name="last_school_year" id="last_school_year" class="input-xlarge" value="<?php if(isset($last_school_year)) echo $last_school_year; ?>" placeholder="yyyy-yyyy" />
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button id="update_educational_information" class="btn btn-primary" type="submit">Update Educational Information</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>