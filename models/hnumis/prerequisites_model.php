<?php

	class Prerequisites_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
			
   		}

		
		/*NOTE: Added on 01/04/2013
		//
		*/
		function AddPrerequisites($prereq_data) {
				
			$q1 = "INSERT INTO
						prerequisites (id, prospectus_courses_id, prereq_type)
					VALUES (
						'',
						{$this->db->escape($prereq_data['prospectus_courses_id'])},
						{$this->db->escape($prereq_data['prereq_type'])})";
			
			$this->db->query($q1);
			$id = @mysql_insert_id();
			
			switch ($prereq_data['prereq_type']) {
				case 'C':
					$this->AddPrereq_Courses($id,$prereq_data['courses_id'],$prereq_data['prereqs']);
					break;
				case 'Y':
					$this->AddPrereq_YrLevel($id,$prereq_data['yr_level']);
					break;
			}						

			return;
		}
		
		
		function AddPrereq_Courses($id, $courses_id, $prereqs)	{
				
			$query = "INSERT INTO 
							prereq_courses (id, prerequisites_id, prospectus_courses_id, prereq_type)
						VALUES ('',
								{$this->db->escape($id)},
								{$this->db->escape($courses_id)},
								'M')";
					
			$this->db->query($query);
			$id2 = @mysql_insert_id();
			
			//Add prerequisites of the given prospectus_courses_id
			if ($prereqs) {
				foreach ($prereqs AS $prereq) {
					$query = "INSERT INTO 
								prereq_courses (id, prerequisites_id, prospectus_courses_id, prereq_type)
								VALUES ('',
									{$this->db->escape($id)},
									{$this->db->escape($prereq->prospectus_courses_id)},
									'C')";
	
					$this->db->query($query);
				}
			}

			return $id2;
		}
				

		function AddPrereq_YrLevel($id, $yr_level)	{
				
			$query = "INSERT INTO 
							prereq_yrlevel (id, prerequisites_id, yr_level)
						VALUES ('',
								{$this->db->escape($id)},
								{$this->db->escape($yr_level)})";
					
			$this->db->query($query);
			$id2 = @mysql_insert_id();
					
			return $id2;
					
		}


		function AddPrereq_CutOff($id, $grade)	{
				
			$query = "INSERT INTO 
							prereq_cutoff (id, prerequisites_id, grade)
						VALUES ('',
								{$this->db->escape($id)},
								{$this->db->escape($grade)})";
					
			$this->db->query($query);
			$id2 = @mysql_insert_id();
					
			return $id2;
					
		}

		function DeletePrerequisite($id) {
				
			$query = "DELETE 
						FROM 
							prerequisites 
						WHERE
							id = {$this->db->escape($id)} ";
			
			//print($query);
			//die();
			if ($this->db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
						
		}

		
	}