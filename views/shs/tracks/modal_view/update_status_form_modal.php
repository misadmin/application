
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Description</div>
			<div class="col2a">
				<input type="text" class="form-control" id="description_status" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Status</div>
			<div class="col2a">
				<select id="status_status" class="form-control" >
					<option value="Frozen" shortcut="F">Frozen</option>
					<option value="Offered" shortcut="O">Offered</option>
				</select>
			</div>
		</div>
	</div>
