
<!-- 
	All Tuition Reports will now based on Posted Assessments! No more selection for Unposted to simplify query in Tuition Report Model. 
-->

		<h2 class="heading">Assessment Report - By Group</h2>
		<form id="batch_posting_form" method="post" action="" class="form-horizontal">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<input type="hidden" name="step" value="2" />
			
			<div class="control-group">
					<label class="control-label" for="academic_terms_id">Academic Term&nbsp;&nbsp;:</label>
					<div class="controls ">
						 <select name="academic_terms_id" id="academic_terms_id">
								<?php
									foreach($academic_terms AS $academic_term)
										{
											if ($current_term->id == $academic_term->id) {
												print("<option value=".$academic_term->id." selected>".$academic_term->term." ".$academic_term->sy."</option>");	
											} else {
												print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");	
											}
										}
								?>
						  </select>
					</div>
			</div>	
			
			<div class="control-group">
					<label class="control-label" for="program_groups_id">Group&nbsp;&nbsp;:</label>
					<div class="controls ">
						 <select name="program_groups_id" id="program_groups_id">
								<?php
									foreach($program_groups AS $group) {
										print("<option value=\"$group->id\">$group->abbreviation</option>");
									}
								?>
						 </select>
					</div>
			</div>
			
			<div class="control-group">
					<label class="control-label" for="yr_level">Year Level&nbsp;&nbsp;:</label>
					<div class="controls">
						<select name="yr_level" id="yr_level">
							  <option value="0" selected="selected">All Year Levels</option>
							  <option value="1">1st Year</option>
							  <option value="2">2nd Year</option>
							  <option value="3">3rd Year</option>
							  <option value="4">4th Year</option>
							  <option value="5">5th Year</option>
						</select>
					</div>
			</div>		
			
			<div class="control-group">
				 <div class="controls">
					<button class="btn btn-primary" name="generate_report_button" type="submit" id="batch_post">Generate Report!</button>
				</div>
			</div>
			
		</form>

<script>
$('#batch_post').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to generate Tuition Report?");

	if (confirmed){
		$('#batch_posting_form').submit();
	}		
});
</script>





  