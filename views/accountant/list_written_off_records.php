<style>
	.table th {text-align:center; vertical-align:middle;}
	td.right {text-align:right}
	td.center {text-align:center}
</style>
<div class="row-fluid span10">
<h3> List of Written Off Records:</h3>
<hr> 
<form action="" class="form-horizontal" id="rsi" method="post">
	<input type="hidden" name="nonce" value="<?php echo $this->common->nonce() ?>" />
	<input type="text"  name="stud_start" placeholder="From Start Name" value="" >
	<input type="text"  name="stud_end" placeholder="To End Name" value="" >
	<button type="submit" class="btn btn-success">Search</button>
</form>
<hr>



<table id="writeoffs" class="table table-bordered table-striped">
    <thead>
		<tr>
			<th width="8%" >Ledger ID</th>
			<th width="9%" >ID No</th>
			<th width="17%">Student</th>
			<th width="10%">Date</th>
			<th width="20%">Description</th>      
			<th width="16%">Ref No</th>
			<th width="10%">Debit</th>
			<th width="10%">Credit</th>
		</tr>
    </thead>

<?php 
	if (isset($wos)){
		foreach ($wos AS $row) {
?>
<tr>
 <td class="center"><?php  echo $row->ledger_id; ?></td> 
 <td class="center"><?php  echo $row->idno; ?></td> 
 <td><?php  echo $row->student; ?></td> 
 <td><?php  echo $row->transaction_date; ?></td> 
 <td><?php  echo $row->description; ?></td> 
 <td><?php  echo $row->refno; ?></td> 
 <td class="right"><?php  echo $row->debit !=0 ? number_format($row->debit,2) : '' ; ?></td> 
 <td class="right"><?php  echo $row->credit!=0 ? number_format($row->credit,2) :'' ; ?></td> 
 
<?php 
		}
	}else{
?>
	<tr><td class="center" colspan="8">No Record found!</td>
<?php 		
	}
?>    
    

</table>

</div>
<script>
$(document).ready(function(){
	var oTable = $("#writeoffs").dataTable({
		"iDisplayLength": 50,
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
	});

	oTable.fnSort( [ [2,'asc'], ] );
	

});	

</script>
