<?php 

class Basic_ed_statements{

	public function __contruct(){
	
	}
	
	function payable($period, $basic_ed_info, $debit_balance){
		
		//$this->load->model('hnumis/Student_Model');
	//	print_r($basic_ed_info); die();		
		if($basic_ed_info){
			if(($basic_ed_info->level == 'Kinder' AND $basic_ed_info->yr_level == 2) OR
					($basic_ed_info->level == 'GS' AND $basic_ed_info->yr_level == 6) OR
					(($basic_ed_info->level == 'HS-Day' OR $basic_ed_info->level == 'HS-Night') AND $basic_ed_info->yr_level == 4) ){
					$exams = 5;
			}else{
					$exams = 6;
			}
			$show_invoice = TRUE;
		} else {
			$show_invoice = FALSE;
		}
			
		// start computation of payable
				$payablePerExam = ($basic_ed_info->assessed_amount - 500) / $exams;
				$due['Ex1'] = $payablePerExam;
				$due['Ex2'] = $payablePerExam * 2;
				$due['Ex3'] = $payablePerExam * 3;
				$due['Ex4'] = $payablePerExam * 4;
				$due['Ex5'] = $payablePerExam * 5;
				$due['Ex6'] = $payablePerExam * 6;
				$other_charges = 0;
				switch($period){
					case 1:	$expected_balance_before_exam =  ($basic_ed_info->assessed_amount - 500);
							break;
					case 2:	$expected_balance_before_exam =  ($basic_ed_info->assessed_amount - 500) - $due['Ex1']; 
							break;
					case 3:	$expected_balance_before_exam =  ($basic_ed_info->assessed_amount - 500) - $due['Ex2']; 
							break;
					case 4:	$expected_balance_before_exam =  ($basic_ed_info->assessed_amount - 500) - $due['Ex3']; 
							break;	
					case 5:	$expected_balance_before_exam =  ($basic_ed_info->assessed_amount - 500) - $due['Ex4']; 
							break;	
					case 6: $expected_balance_before_exam =  ($basic_ed_info->assessed_amount - 500) - $due['Ex5']; 
							break;							
				}
				
			$result[2]  = "Exam #".$period;
			$numExam = $exams - $period + 1;					
			if ($debit_balance > $expected_balance_before_exam) {
				$other_charges = $debit_balance - $expected_balance_before_exam;
				$payable_exam = $payablePerExam + ($other_charges/$numExam);
			}
			else {
				$excess = $expected_balance_before_exam - $debit_balance;
				$payable_exam = ($expected_balance_before_exam/$numExam) - $excess;
			}
			$result[0] = $payable_exam;
			$result[1] = $show_invoice;
			
			return($result); 
			
	}
}
