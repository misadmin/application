<?php

	class Track_strand_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListTracks() {
			$result = null;
			
			$q1 = "SELECT 
							a.id,
							a.group_name,
							a.abbreviation,
							a.num_year,
							(SELECT 
									COUNT(x.id) 
								FROM 
									academic_programs AS x 
								WHERE 
									x.acad_program_groups_id = a.id 
							) AS num_strand,
							(SELECT 
									GROUP_CONCAT(x.abbreviation SEPARATOR ', ') 
								FROM 
									academic_programs AS x 
								WHERE 
									x.acad_program_groups_id = a.id
								ORDER BY 
									x.abbreviation
							) AS strand_description
						FROM 
							acad_program_groups AS a 
						WHERE 
							a.levels_id = 12 
						ORDER BY 
							a.abbreviation ";
			
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function ListStrands($acad_program_groups_id=NULL,$status=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							a.id,
							a.acad_program_groups_id,
							a.abbreviation,
							a.description,
							CASE a.status
								WHEN 'F' THEN 'Frozen'
								WHEN 'O' THEN 'Offered'
							END AS status,
							a.max_yr_level,
							b.group_name AS track_name
						FROM 
							academic_programs AS a,
							acad_program_groups AS b
						WHERE 
							a.acad_program_groups_id = b.id
							AND a.colleges_id = 12 ";
			
			if ($acad_program_groups_id) {
				$q1 .= " AND a.acad_program_groups_id = {$this->db->escape($acad_program_groups_id)} ";
			}
			
			if ($status) {
				$q1 .= " AND a.status = {$this->db->escape($status)} ";
			}
					
			$q1 .= " ORDER BY 
						b.abbreviation,
						a.abbreviation ";
			
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		

		function ListStrandsByLatestProspectus() {
			$result = null;
			
			$q1 = "SELECT 
							b.id, 
							a.description, 
							b.effective_year,
							c.abbreviation AS track_abbrev,
							a.abbreviation,
							a.id AS academic_programs_id
						FROM 
							academic_programs AS a,
							prospectus AS b,
							acad_program_groups AS c
						WHERE 
							a.id = b.academic_programs_id
							AND a.acad_program_groups_id = c.id
							AND a.colleges_id = 12 
							AND a.status = 'O' 
							AND b.status ='A' 
							AND b.id = (SELECT 
												x.id 
											FROM 
												prospectus AS x 
											WHERE 
												x.academic_programs_id = a.id 
											ORDER BY 
												x.effective_year DESC 
											LIMIT 1)
						ORDER BY 
							a.description,
							b.effective_year DESC";
			
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function AddAcadProgramGroups($data) {
				
			$q1 = "INSERT INTO
							acad_program_groups (group_name,abbreviation,levels_id,num_year)
						VALUES (
							{$this->db->escape($data['group_name'])},
							{$this->db->escape($data['abbreviation'])},
							{$this->db->escape($data['levels_id'])},
							{$this->db->escape($data['num_year'])} ) ";
			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
		function AddStrand($data) {
				
			$q1 = "INSERT INTO
							academic_programs (colleges_id,acad_program_groups_id,abbreviation,description,status,max_yr_level,inserted_by)
						VALUES (
							{$this->db->escape($data['colleges_id'])},
							{$this->db->escape($data['acad_program_groups_id'])},
							{$this->db->escape($data['abbreviation'])},
							{$this->db->escape($data['description'])},
							{$this->db->escape($data['status'])},
							{$this->db->escape($data['max_yr_level'])},
							{$this->db->escape($data['inserted_by'])}) ";
			
			if ($this->db->query($q1)) {
				return $this->db->insert_id();
			} else {
				return FALSE;
			}
		}


		function UpdateAcadProgramGroups($data){

			$q1 = "UPDATE acad_program_groups
					SET group_name = {$this->db->escape($data['group_name'])},
					    abbreviation = {$this->db->escape($data['abbreviation'])},
						num_year = {$this->db->escape($data['num_year'])}
		 			WHERE
						id = {$this->db->escape($data['id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

		
		function UpdateStrandStatus($data){

			$q1 = "UPDATE academic_programs
					SET status = {$this->db->escape($data['status'])},
						update_by = {$this->db->escape($data['update_by'])}
		 			WHERE
						id = {$this->db->escape($data['id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}


		function UpdateStrand($data){

			$q1 = "UPDATE academic_programs
					SET acad_program_groups_id = {$this->db->escape($data['acad_program_groups_id'])},
						abbreviation = {$this->db->escape($data['abbreviation'])},
						description = {$this->db->escape($data['description'])},
						max_yr_level = {$this->db->escape($data['max_yr_level'])},
						update_by = {$this->db->escape($data['update_by'])}
		 			WHERE
						id = {$this->db->escape($data['id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

		
	    function DeleteTrack($track_id) {
			
			$q = "DELETE FROM
						acad_program_groups 
					WHERE
						id = {$this->db->escape($track_id)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
	    function DeleteStrand($strand_id) {
			
			$q = "DELETE FROM
						academic_programs 
					WHERE
						id = {$this->db->escape($strand_id)} ";

			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


		function ListTracksByArray($array_academic_programs_groups_id=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							a.id AS acad_program_groups_id,
							a.abbreviation,
							a.group_name,
							(SELECT 
									GROUP_CONCAT(x.abbreviation SEPARATOR ', ') 
								FROM 
									academic_programs AS x 
								WHERE 
									x.acad_program_groups_id = a.id
								ORDER BY 
									x.abbreviation
							) AS strand_description
						FROM 
							acad_program_groups AS a
						WHERE 
							a.id IN (".$array_academic_programs_groups_id.")
						ORDER BY 
							a.abbreviation ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
	}
	
?>
