<?php 
$debug = TRUE;
/***********************************************************************************
 * TABLE LIB CHILD CLASS
 ***********************************************************************************/

class rs_table extends Table_lib{
	
	public $cur_code;
	public $cur_machine_ip;
	public $cur_sub_total;
	public $total_method=array();
	public $voided_total;
	//public $valid_total;
	public $grand_total;
	public $min_or_number;
	public $max_or_number;
	
	private $is_first_entry;
	
	public function __construct(){
		parent::__construct();
		$this->cur_code = '';
		$this->cur_machine_ip = '';
		$this->cur_sub_total = 0;
		$this->grand_total = 0;
		$this->voided_total = 0;
		$this->min_or_number = 0;
		$this->max_or_number = 0;
		$this->valid_total = 0;
		$this->is_first_entry = true;
	}
	
	public function set_payment_methods($payment_methods){
		foreach($payment_methods as $method){
			$this->total_method[$method] = 0;
		}
	}
	
	public function format_raw_data_transaction_date($row){
		
		$print_sub = false;
		$print_ip = false;
		$print_group = false;
		$str = '';
		
		//if first data
		if ($this->is_first_entry){
			$this->min_or_number = $row['receipt_no'];
			$this->max_or_number = $row['receipt_no'];
			$this->cur_sub_total = $row['amount'];
			
			$this->cur_machine_ip = $row['machine_ip'];
			$print_ip = true;
			
			$this->cur_code = $row['teller_code'];
			$print_group = true;
			
			$this->is_first_entry = false;
		}
		else {
			//check for min / max or number
			if ($row['receipt_no'] < $this->min_or_number)
				$this->min_or_number = $row['receipt_no'];
			if ($row['receipt_no'] > $this->max_or_number)
				$this->max_or_number = $row['receipt_no'];
			
			//check for next machine ip
			if ($this->cur_machine_ip != $row['machine_ip']){
				$print_ip = true;
				$print_sub = true;
				$this->cur_machine_ip = $row['machine_ip'];
			}
			
			//check for next group
			if ($this->cur_code != $row['teller_code']){
				$print_sub = true;
				$print_group = true;
			}
			$this->cur_sub_total += $row['amount'];

		}

		
		if ($print_sub){
			$str .= '<td colspan="3" style="text-align:right">'.$this->cur_code.' Sub Total</td><td style="text-align:right"><strong>'.number_format($this->cur_sub_total,2).'</strong></td><td colspan="4"></td></tr><tr>';
			$this->cur_sub_total = 0;
		}
		if ($print_ip){
			$str .= '<td colspan="8" style="border-bottom:solid 3px #000;padding-top:10px;font-size:1.2em;font-weight:bold">Machine No: '.$row['machine_ip'].'</td></tr><tr>';
		}
		if ($print_group){
			$str .= '<td colspan="8" style="background:#eee;font-size:1.1em;font-weight:bold" class="header">&nbsp;&nbsp;'.$row['teller_code'].' <span style="font-weight:normal">('.$row['description'].')</span></td></tr><tr>';
			$this->cur_code = $row['teller_code'];
		}

		//$this->total_method[$row['payment_method']] += $row['amount'];
				
		$str .= '<td>&nbsp;&nbsp;&nbsp;&nbsp;'.$row['transaction_date'].'</td>';
		
		return $str;
		
		//check for max and min O.R Number
		if ($this->is_first_entry){
			$this->min_or_number = $row['receipt_no'];
			$this->max_or_number = $row['receipt_no'];
		}
		else {
			if ($row['receipt_no'] < $this->min_or_number)
				$this->min_or_number = $row['receipt_no'];
			if ($row['receipt_no'] > $this->max_or_number)
				$this->max_or_number = $row['receipt_no'];
		}
		
		//check for current teller code
		if ($this->cur_code != $row['teller_code']){
			if ($this->is_first_entry){
				$this->is_first_entry = false;
			}
			else {
				$print_sub = true;
			}
			$this->cur_sub_total = 0;
		}
		
		if ($row['status'] == 'void'){
			$this->voided_total += $row['amount'];
		}
		
		//print machine ip header
		if ($this->cur_machine_ip != $row['machine_ip']){
			$print_ip = true;
			$this->cur_machine_ip = $row['machine_ip'];
		}
		
		//print teller code header
		if ($this->cur_code != $row['teller_code']){
			$print_group = true;
			$this->cur_code = $row['teller_code'];
		}
		
		//update total and subtotals
		$this->cur_sub_total += $row['amount'];
		$this->grand_total += $row['amount'];

		
		if ($print_sub)
			$str .= '<td colspan="3" style="text-align:right">'.$this->cur_code.' Sub Total</td><td style="text-align:right"><strong>'.number_format($this->cur_sub_total,2).'</strong></td><td colspan="4"></td></tr><tr>';
		if ($print_ip)
			$str .= '<td colspan="8" style="border-bottom:solid 3px #000;padding-top:10px;font-size:1.2em;font-weight:bold">Machine No: '.$row['machine_ip'].'</td></tr><tr>';
		if ($print_group)
			$str .= '<td colspan="8" style="background:#eee;font-size:1.1em;font-weight:bold" class="header">&nbsp;&nbsp;'.$row['teller_code'].' <span style="font-weight:normal">('.$row['description'].')</span></td></tr><tr>';
		
		
		$str .= '<td>&nbsp;&nbsp;&nbsp;&nbsp;'.$row['transaction_date'].'</td>';
		
		return $str;
	}
	
	public function trigger_on_last(){
		$this->is_first_entry = true;
	}
	
	public function format_data_amount($row){
		$str = '';
		if ($row['status'] == 'void'){
			$str .= '(VOID) ';
		}
		$str .= number_format($row['amount'],2);
		return $str.'-'.$this->cur_sub_total;
	}
	
	public function format_cell_amount($row){
		return 'style="text-align:right;margin"';
	}
}

/***********************************************************************************
 * TABLE LIB CHILD CLASS DECLARATION
 ***********************************************************************************/

$rs_table = new rs_table();
$rs_table->set_attributes(array('class'=>'table table-condensed table-hover','id'=>'rs_table'));
$rs_table->set_payment_methods($payment_methods);

/***********************************************************************************
 * HEADERS
 ***********************************************************************************/

$rs_table->insert_head_row(
array(
	array("id"=>"transaction_date",
		  "value"=>"O.R. Date - Time"
		 ),
	array("id"=>"receipt_no",
		  "value"=>"O.R. No."
		 ),
	array("id"=>"teller",
		  "value"=>"Teller"
		 ),
	array("id"=>"amount",
		  "value"=>"Amount",
		  "attributes"=>array("style"=>"text-align:right")
		 ),
	array("id"=>"teller_code",
		  "value"=>"Code"
		 ),
	array("id"=>"payor_id",
		  "value"=>"I.D. #"
		 ),
	array("id"=>"payor",
		  "value"=>"Name"
		 ),
	)
);

/*===============================================================================
 * Display Header
 ================================================================================*/

echo '<h2 class="heading">Receipt Summary <small> as of '.$date.'</small></h2>';

$debug = !true;

if (isset($records) AND is_array($records) AND count($records) > 0){
	if (isset($debug) AND $debug == TRUE){
		echo '<table class="table"><tbody>';
		foreach ($records as $r){
			echo '<tr><td>'.$r->machine_ip.'</td><td>'.$r->teller_code.'</td><td>'.$r->transaction_date.'</td><td>'.$r->receipt_no.'</td><td>'.$r->teller.'</td><td>'.$r->amount.'</td><td>&nbsp;</td><td>'.$r->idno.'</td><td>'.$r->stud.'</td></tr>';
		}
		echo '</tbody></table>';
		die();
	}
	else {
		$rs_table->insert_data($records);
		$rs_table->content(TRUE);
		$rs_table->insert_free_row('<td colspan="3" style="text-align:right">'.$rs_table->cur_code.' Sub Total</td><td style="text-align:right"><strong>'.number_format($rs_table->cur_sub_total,2).'</strong></td><td colspan="4"></td></tr><tr>');
		$rs_table->insert_free_row('<td colspan="3" style="text-align:right;padding-top:10px;border-top:solid 3px #000;margin-top:20px">GRAND TOTAL</td><td style="text-align:right;padding-top:10px;border-top:solid 3px #000;"><strong>'.number_format($rs_table->grand_total,2).'</strong></td><td style="text-align:right;padding-top:10px;border-top:solid 3px #000;" colspan="4"></td></tr><tr>');
		echo '<div class="row-fluid"><div class="pull-left" style="width:250px;font-size:15px;font-weight:bold;line-height: 20px;">Grand Total: <span class="pull-right">'.number_format($rs_table->grand_total,2).'</span></div><div class="pull-left" style="width:350px;margin-left:77px;font-size:15px;font-weight:bold;">OR#s: &nbsp;&nbsp;&nbsp;&nbsp;'.$rs_table->min_or_number.'-'.$rs_table->max_or_number.'</div></div>';
		echo '<div class="row-fluid"><div style="width:250px;font-size:15px;font-weight:bold;line-height: 20px;">Voided Total: <span class="pull-right">'.number_format($rs_table->voided_total,2).'</span></div></div>';
		echo '<div class="row-fluid"><div style="width:250px;font-size:15px;font-weight:bold;line-height: 20px;">Valid Total: <span class="pull-right">'.number_format($rs_table->grand_total - $rs_table->voided_total,2).'</span></div></div>';
		
		foreach($rs_table->total_method as $method => $amount)
			echo '<div class="row-fluid"><div style="width:250px;font-size:15px;font-weight:bold;line-height: 20px;">'.$method. ' Total: <span class="pull-right">'.number_format($amount,2).'</span></div></div>';
		
		echo '<br>';
		$rs_table->content();
	}
}
else{
	$rs_table->content();
	echo '<p>No records</p>';
}