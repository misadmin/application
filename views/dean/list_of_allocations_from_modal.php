<div style="text-align:center;">
<?php 
	if ($block_names OR $allocations) {
?>
	<div style="font-weight: bold; color: red; text-align: left;">Note: Only students belonging to the following blocks/allocations can enroll to this course 
	even if they are advised courses.</div>
<?php		
	}
?>
<table cellpadding="2" cellspacing="0" class="table table-striped table-condensed table-hover" style="width:100%;">
 	<thead>
  	<tr style="font-weight:bold; background:#EAEAEA;">
    	<th width="10%" style="text-align:right; vertical-align:middle;">No.</th>
    	<th width="90%" style="text-align:center; vertical-align:middle;">Allocations</th>
    </tr>
	</thead>
	<tbody>
  <?php
		$cnt = 0;
		if ($block_names) {
			foreach($block_names AS $bname) {
				$cnt++;
  ?>
  
  <tr>
    <td style="text-align:right;"><?php print($cnt); ?>.</td>
    <td style="text-align:left; padding-left:10px;"><?php print($bname->block_name); ?></td>
    </tr>
	<?php
			}
		}
	?> 
  <?php
		if ($allocations) {
			foreach($allocations AS $alloc) {
				$cnt++;
  ?>
  
  <tr>
    <td style="text-align:right;"><?php print($cnt); ?>.</td>
    <td style="text-align:left; padding-left:10px;"><?php print($alloc->allocation_name); ?></td>
    </tr>
	<?php
			}
		}
	?> 
  </tbody>   
</table>
</div>
