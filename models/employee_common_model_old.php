<?php

class Employee_common_model extends MY_Model {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function my_information($empno){
		
		if ($empno < 20000){
			$sql = "SELECT
						e.empno, e.lname, e.fname, e.mname, DATE_FORMAT('%M %e, %Y', e.dbirth) as dbirth,'' as street,'' as town ,'' province,
						r.role,
						c.id as college_id,
						c.college_code,
						c.name as college_name
					FROM
						employees as e
					LEFT JOIN
						roles as r
					ON
						e.empno=r.employees_empno
					LEFT JOIN
						colleges as c
					ON
						r.colleges_id=c.id
					WHERE
						e.empno={$this->db->escape($empno)}
					";
		} else {
			$sql = "
			SELECT
				s.idno as empno, s.lname, s.fname, s.mname, DATE_FORMAT('%M %e, %Y', s.dbirth) as dbirth,'' as street,'' as town ,'' province,
				r.role,
				c.id as college_id,
				c.college_code,
				c.name as college_name
			FROM
				students as s
			LEFT JOIN
				roles as r
			ON
				s.idno=r.students_idno
			LEFT JOIN
				colleges as c
			ON
				r.colleges_id=c.id
			WHERE
				s.idno={$this->db->escape($empno)}
			";
		}
		$query = $this->db->query($sql);
		//ADDED BY KEVIN on APRIL 24 check if A Result was found else return false 'AND $query->num_rows() > 0'
		if ($query !== FALSE AND $query->num_rows() > 0){
			foreach ($query->result() as $res) {
				if ($res->role == 'faculty' || $res->role == 'dean')
					$role[] = ucfirst($res->role) . "({$res->college_name})"; else
					$role[] = ucfirst($res->role);
			}
			$fname = explode(",", $res->fname);
			$res->fname = $fname[0];
			$res->suffix = (isset($fname[1]) ? ucfirst(trim($fname[1])) : "");
			return array('userinfo'=>$res, 'roles'=>$role);
		} else {
			return FALSE;
		}
		
	} 
	
	public function my_roles ($userid){
		$sql = "
			SELECT
			DISTINCT
				r.role
			FROM
				roles r
			LEFT JOIN
				employees e
			ON
				r.employees_empno = e.empno
			WHERE
				e.empno={$this->db->escape($userid)}
			";
		$query = $this->db->query($sql);
		
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
		if ( is_object($query)){
			return $query->result();
		}
	}
	
	public function delete_roles ($userid, $roles) {
		$sql = "
			DELETE from
				roles
			WHERE
				employees_empno={$this->db->escape($userid)}
			AND
			";
		
		if (is_array($roles)){
			$which_roles = " role IN (";
			foreach($roles as $role){
				$which_roles .= "{$this->db->escape($role)}, ";
			}
			$which_roles = rtrim($which_roles, ", ") . ")";
		} else {
			$sql .= " role={$roles}
			";
		}
		return $this->db->query($sql . $which_roles);	
	}
	
	public function add_roles ($userid, $roles, $colleges_id=0, $offices_id=1) {
		$sql = "
			INSERT into
				roles (`employees_empno`, `colleges_id`, `offices_id`, `role`)
			VALUES
			";
		
		$values = "";
		foreach ($roles as $role){
			
			if (in_array($role, $this->config->item('roles_with_colleges'))){
				//this role has a college...
				$values .= "({$this->db->escape($userid)}, {$this->db->escape($colleges_id)}, NULL, {$this->db->escape($role)}),\n";
			} else {
				if (in_array($role, $this->config->item('roles_with_offices'))){
					//this role has an office
					$values .= "({$this->db->escape($userid)}, NULL, {$this->db->escape($offices_id)}, {$this->db->escape($role)}),\n";
				} else {
					$values .= "({$this->db->escape($userid)}, NULL, NULL, {$this->db->escape($role)}),\n";
				}
			}
		}
		$sql .= rtrim($values, ",\n");
		return $this->db->query($sql);
	}
	
	public function update_roles ($userid, $roles, $colleges_id){
		$sql = "
				UPDATE
					roles
				SET
					colleges_id={$this->db->escape($colleges_id)}
				WHERE
					role IN (";
		foreach ($roles as $role){
			$sql .= "{$this->db->escape($role)},";
		}
		$sql = rtrim($sql, ",") . ")";
		$sql .= " AND employees_empno={$this->db->escape($userid)}";
		
		return $this->db->query($sql);
	}
	/**
	 * Method update_information
	 * 
	 * Updates user information
	 * 
	 * @param integer $userid
	 * @param array $data associative array of key value pairs
	 * @return boolean
	 */
	public function update_information($userid, $data){
		$sql = "UPDATE
					employees
				SET
				";
		foreach ($data as $key=>$val) {
			if ( $key == 'password' )
				$sql .= "{$key} = PASSWORD({$this->db->escape($val)})\n"; else
				$sql .= "{$key} = {$this->db->escape($val)}\n";
		}
		$sql .= "WHERE
					empno={$this->db->escape($userid)}
				LIMIT 1";
		$this->db->query($sql);
		
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		} else
			return TRUE;
	}
	/**
	 * Method user_is_valid
	 * 
	 * Returns an associative array of user information if a successful validation occurs. Else,
	 * returns FALSE.
	 * 
	 * @param integer $userid
	 * @param string $password
	 * @return multitype:array | boolean
	 */
	public function user_is_valid($userid, $password, $role){
		if ($userid < 20000){
			$sql = "SELECT
						e.*,
						r.role,
						c.id as college_id,
						c.college_code,
						c.name as college_name
					FROM
						employees as e
					LEFT JOIN
						roles as r
					ON
						e.empno=r.employees_empno
					LEFT JOIN
						colleges as c
					ON
						r.colleges_id=c.id
					WHERE
						e.empno={$this->db->escape($userid)}
					AND
						e.password=PASSWORD({$this->db->escape($password)})
					AND
						r.role='{$role}'
					LIMIT 1
					";
		} else {
			$sql = "
					SELECT
						s.idno as empno,
						s.*,
						r.role,
						c.id as college_id,
						c.college_code,
						c.name as college_name
					FROM
						students as s
					LEFT JOIN
						roles as r
					ON
						s.idno=r.students_idno
					LEFT JOIN
						colleges as c
					ON
						r.colleges_id=c.id
					WHERE
						s.idno={$this->db->escape($userid)}
					AND
						s.password=PASSWORD({$this->db->escape($password)})
					AND
						r.role='{$role}'
					LIMIT 1
			";
		}
		
		$query = $this->db->query($sql);
		
		if ($query && $query->num_rows() > 0){
			
			$row = $query->row();
			$ret = array (
					'role'=>$row->role,
					'fname'=>$row->fname,
					'empno' => $row->empno,
    				'lname' => $row->lname,
    				'fname' => $row->fname,
    				'mname' => $row->mname,
    				'dbirth' => $row->dbirth,
					'college_code'	=> $row->college_code,
					'college_id'	=> $row->college_id,
					'college_name'	=> $row->college_name,			
    			);
			
			return $ret;
		} else 
			return FALSE;
	}
	
	public function search ($str, $count=15, $page=1) {
		
		$start = ($page - 1) * $count;
		$sql = "
			SELECT
				empno,
				CONCAT (lname, ', ', fname) as fullname,
				mname
			FROM
				employees
			";
		
		if (is_numeric($str)) {
			$sql .= "
				WHERE
					empno={$this->db->escape($str)}
				LIMIT 1";
		} else {
			//query is NOT numeric...
			if (strpos($str, ',') === FALSE ) {
				$sql .= "
					WHERE
						lname LIKE '{$str}%'
					OR
						fname LIKE '{$str}%'
					ORDER BY lname, fname
					LIMIT 15";
			} else {
				$str_arr = explode (',', $str);
				$sql .= "
					WHERE
						lname LIKE '{$str_arr[0]}%'
					AND
						fname LIKE '" . trim($str_arr[1]) . "%'
					ORDER BY lname, fname
					LIMIT 15
				";
			}
		}
		
		$query = $this->db->query($sql);
	
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
	
		if ( is_object($query)){
			return $query->result();
		}
	}
	
	public function total_search_results ($str){
		$sql = "
			SELECT
				count(*) as total
			FROM
				employees
			";
	
		if (is_numeric($str)) {
			$sql .= "
				WHERE
					idno={$this->db->escape($str)}
			";
		} else {
			if (strpos($str, ',') === FALSE ) {
			$sql .= "
				WHERE
					lname LIKE '{$str}%'
				OR
					fname LIKE '{$str}%'
				";
			} else {
				$str_arr = explode (',', $str);
				$sql .= "
						WHERE
				lname LIKE '{$str_arr[0]}%'
					AND
				fname LIKE '" . trim($str_arr[1]) . "%'
				";
			}
		}
		
		if ($query = $this->db->query($sql)) {
			$res = $query->row();
			return $res->total;
		} else {
			return FALSE;
		}
	}
	
	public function upcoming_birthdays(){
		$sql = "SELECT 
					empno,
					concat(lname,', ', fname) as fullname,
					concat(monthname(dbirth),' ',dayofmonth(dbirth)) as birthday
				from 
					employees 
				where 
					(dayofyear(dbirth) - dayofyear(curdate())) between 0 and 3
				ORDER by 
					date_format( dbirth, '%m/%d' )
				";
		$query = $this->db->query($sql);
		
		if ($this->db->_error_number()) {
			$this->set_error();
			return FALSE;
		}
		
		if ( is_object($query)){
			return $query->result();
		}
	}
	
	public function search_by_starting_letter($starting_letter){
		$starting_letter = substr ($starting_letter, 0, 1);
		$sql = "
			SELECT
				empno,
				CONCAT (lname, ', ', fname) as fullname,
				mname,
				gender
			FROM
				employees
			WHERE
				lname LIKE '{$starting_letter}%'
			ORDER BY
				lname, fname
			";
		$query = $this->db->query($sql);
		
		if ($query) {
			return $query->result(); 
		} else {
			return FALSE;
		}
	}
	
	public function new_employee($data){
		$sql = "
			INSERT into
				employees (`lname`, `fname`, `mname`, `gender`, `dbirth`, `street`, `town`, `province`, `blood_type`)
			VALUE
				(";
		$content_data = array('familyname', 'firstname', 'middlename', 'gender', 'birthdate', 'home_address_barangay_id', 'home_address_town', 'home_address_province', 'bloodtype');

		foreach ($content_data as $dat){
			if (isset($data[$dat]))
				$sql .= "{$this->db->escape($data[$dat])},"; else
				$sql .= "'',";
		}
		$sql = rtrim($sql, ',') . ")";
		
		if ($this->db->query($sql))
			return $this->db->insert_id(); else
			return FALSE;
	}
}