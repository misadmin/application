
		<!-- NOTE: academic_terms_id is used as id to match with jquery being used -->
		<select name="program_id" id="academic_terms_id" class="form-control" >
			<option value="">-- Select Program --</option>
			<?php
				foreach ($programs AS $program) {
					print("<option value=".$program->id.">".$program->abbreviation."</option>");
				}
			?>
		</select>


<script>

	$('#academic_terms_id').change(function(event){
		event.preventDefault();

		var lastname   = $('#lastname').val();
		var firstname  = $('#firstname').val();
		var middlename = $('#middlename').val();
		var dbirth     = $('#dbirth').val();
		var academic_terms_id = $("select#academic_terms_id option:selected").attr('value'); 
		var e_person   = $('#emergency_notify').val();
		var e_contact  = $('#emergency_telephone').val();
		var e_address  = $("select#brgy_id option:selected").attr('value'); 

		var req_array  = [lastname,firstname,middlename,dbirth,e_person,e_contact,e_address,academic_terms_id];

			if (required_isEmpty(req_array)) {
				$('#register_student').prop("disabled", true);
			} else {	
				$('#register_student').prop("disabled", false);
			}
	});

		
	function required_isEmpty(req_array) {
		
		var aLen; 
		aLen = req_array.length;
		
		for (i = 0; i < aLen; i++) {	

			if (!req_array[i]) {
				return true;
			}
		}
		return false;
	}

</script>	