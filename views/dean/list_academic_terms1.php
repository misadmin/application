<form id="academic_terms_form" action="" method="post">
<?php $this->common->hidden_input_nonce(FALSE); ?>
<select name="academic_terms_id" id="academic_terms_id" class="form">
<?php foreach($academic_terms AS $academic_term): ?>
	<option value="<?php echo $academic_term->id; ?>" <?php if($academic_term->id==$selected_term) echo 'selected="selected"'?>><?php echo $academic_term->term . " " . $academic_term->sy; ?></option>	
<?php endforeach; ?>
</select>
</form>
<script>
	$('#academic_terms_id').change(function(){
		$('#academic_terms_form').submit();
	});
</script>