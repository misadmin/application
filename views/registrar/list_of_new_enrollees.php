<div style="margin-top:20px;">
<h2 class="heading">List of New Enrollees</h2>
<div class="span10">
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; 
	border-color: #D8D8D8; height: 35px; margin-bottom:10px; padding:2px; ">
	<table style="height:auto; width:100%;">
		<tr>
				<td style="text-align:left; vertical-align:top; font-weight:bold; width:20%; padding:3px;">
					<a href="<?php print(base_url('downloads/New Enrollees '.$s_term.'.xls')); ?>" class="btn btn-link" id="download_summary">
					<i class="icon-download-alt"></i> Download as Excel</a>
				</td>
				
				<td style="width:50%; text-align:right; vertical-align:top;">	
				<form id="change_term" method="POST" >
					<?php $this->common->hidden_input_nonce(FALSE); ?>
									<select name="academic_terms_id" id="academic_terms_id">
									<?php 
										foreach($terms AS $term) {
									?>
											<option value="<?php print($term->id); ?>" <?php if ($term->id == $selected_term) { print("selected"); } ?>>
												<?php print($term->term.' '.$term->sy); ?> </option>
									<?php 	
										}
									?>
									</select>
									
				</form>	
			</td>
		</tr>
	</table>				
</div>
<?php 
	if ($enrollees) {
?>
<div style="padding-top:10px; padding-bottom:10px;">
	<h3>New enrollees during: <?php print($s_term); ?></h3>
</div>

<div>
	<table id="enrollees2" class="table table-bordered table-striped table-hover table-condensed" >
		<thead>
			<tr style="background-color: #EAEAEA; font-weight: bold;">
				<td style="width:10%; text-align:center;">
					ID No.
				</td>
				<td style="width:65%; text-align:center;">
					Name
				</td>
				<td style="width:15%; text-align:center;">
					Course/Year
				</td>	
				<td style="width:10%; text-align:center;">
					Gender
				</td>
			</tr>
		</thead>
		<?php 
			foreach ($enrollees AS $enrol) {
		?>
			<tr>
				<td style="text-align:center;"><?php print($enrol->idno); ?></td>
				<td><?php print($enrol->name); ?></td>
				<td><?php print($enrol->abbreviation.'-'.$enrol->year_level); ?></td>
				<td style="text-align:center;"><?php print($enrol->gender); ?></td>
			</tr>
			
		<?php 
			}
		?>
		
		</table>
</div>

<?php 
	}
?>
</div>

<script>
	$(document).ready(function(){
		$('#academic_terms_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'display_selected_term',
			}).appendTo('#change_term');
			$('#change_term').submit();
		});
	});
</script>


<script>
$().ready(function() {
	var oTable = $('#enrollees2').dataTable({
					"iDisplayLength": 100,
			   		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
	});
	oTable.fnSort( [ [1,'asc'] ] );
    $("#enrollees2").stickyTableHeaders();
    $('#enrollees2').stickyTableHeaders('updateWidth');
	
  })
</script>
