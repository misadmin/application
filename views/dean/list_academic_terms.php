<h2 class="heading">Select Academic Term</h2>

<form action="<?php 
	switch ($this->uri->segment(2)) {
		case 'offer_course_schedules':
			print(site_url('dean/offer_course_schedules'));
			break;
		case 'assign_faculty_to_schedule':
			print(site_url('dean/assign_faculty_to_schedule'));
			break;
		case 'dissolve_section':
			print(site_url('dean/dissolve_section'));
			break;
		case 'offer_schedule_slots':
			print(site_url('dean/offer_schedule_slots'));
			break;
		case 'view_course_offerings':
			print(site_url('dean/view_course_offerings'));
			break;
		case 'view_class_list':
			print(site_url('dean/view_class_list'));
			break;
		case 'masterlist':
			print(site_url('dean/masterlist'));
			break;
		case 'grade_submission':
			print(site_url('dean/grade_submission'));
			break;
			
	}
	?>" method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="2" name="step" />
	<?php echo validation_errors();   ?>

		<fieldset>
			<div class="control-group formSep">
				<label for="" class="control-label">Academic Term: </label>
					<div class="controls">
						<select id="academic_terms_id" name="academic_terms_id">
							<!--  <option value="">-- Select Academic Term --</option>  -->
				          	<?php
								foreach($academic_terms AS $academic_term) {
									print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");	
								}
							?>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">Continue</button>
					</div>
				</div>	
		</fieldset>
	</form>
	