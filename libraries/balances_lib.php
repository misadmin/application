<?php 

class Balances_lib {
	public $ledger_data;
	public $current; //array('year', 'term', 'period');
	public $current_period = '';
	public $periods = array(1=>4, 2=>4, 3=>2);//array(1=>array('Prelim','Midterm','Pre-Final','Final'),2=>array('Prelim','Midterm','Pre-Final','Final'),3=>array('Midterm','Final'));
	public $unposted_payments=array();
	public $all_transactions=array();
	public $total_previous_debit=0; //previous semesters...
	public $total_previous_credit=0; //
	public $total_current_debit=0; //current semesters...
	public $total_current_credit=0; //
	public $assessment=0;
	public $bad_debt=0;
	public $current_term_debit_transactions=array();
	public $current_term_transaction=array();
	public $period_dates=array();
	public $current_registration=0;
	public $current_registration_transaction;
	public $credits_from_CR;
	public $current_credit_transactions_after_assessment=array();
	public $has_assessment = FALSE;
	public $invoices_this_term = array();
	public $current_credit_without_CR = array();
	public $assumed_CR_this_term=0;
	public $balance_before_assessment=0;   //added Toyet 7.4.2018
	public $current_debit_div4 = 0;
	public $current_debit_div1 = 0;
	public $payable_for_current_period =0;  //added Toyet 10.2.2018


	public function __construct($config){
		$this->ledger_data = $config['ledger_data'];
		$this->unposted_payments = $config['unposted_students_payments'];
		$this->current = $config['current_academic_terms_obj'];
		$this->current_period = $config['period'];
		foreach($config['period_dates'] as $period_date){
			$this->period_dates[$period_date->period] = $period_date->start_date;
		}
		$this->all_transactions = array_merge($this->ledger_data, $this->unposted_payments);
		usort($this->all_transactions(), array($this, 'compare_array_by_date_key'));
		$this->total_previous_debit = 0;
		$this->total_previous_credit = 0;
		$has_college_registration = FALSE;
		$current_debit_div4 = 0;
		$current_debit_div1 = 0;

		foreach($this->all_transactions as $transaction){
			$transaction = (object)$transaction;
			if($transaction->end_year==$this->current->end_year && $transaction->term==$this->current->num_term){
				//Current Term:
				$this->total_current_debit += $transaction->debit;  // Toyet
				$this->total_current_credit += $transaction->credit;  // Toyet
				if($transaction->transaction_detail=='ASSESSMENT'){
					$this->assessment += $transaction->debit;
				}
				if($transaction->transaction_detail=='COLLEGE REGISTRATION'){
					$this->current_registration += $transaction->credit;
					$this->current_registration_transaction = $transaction;
					$has_college_registration = TRUE;
				}
				$this->current_term_transaction[] = $transaction;
				if(!empty($transaction->debit))
					$this->current_term_debit_transactions[] = $transaction; else
					$this->current_term_credit_transactions[] = $transaction;

				//added Toyet 8.9.2018
				switch ($transaction->transaction_detail){
					case "REFUND":
						$current_debit_div1 += $transaction->debit;
						break;
					case "Adjustment: TUITION TRANSFER - DEBIT":
						$current_debit_div1 += $transaction->debit;
						break;
					default:
						if($transaction->transaction_detail<>"ASSESSMENT")
							$current_debit_div4 += $transaction->debit;
				}
				$this->current_debit_div4 = $current_debit_div4;
				$this->current_debit_div1 = $current_debit_div1;

			} else {
				//Not current
				if($transaction->transaction_detail=='COLLEGE REGISTRATION' && $transaction->status=='active'){
					//Fixes multiple CR transaction problem of student...
					if(isset($latest_CR_transaction) && $latest_CR_transaction->end_year==$transaction->end_year && $latest_CR_transaction->term==$transaction->term){
						$latest_CR_transaction->credit += $transaction->credit;
					} else {
						$latest_CR_transaction = $transaction;
					}
				}
				if($transaction->transaction_detail=='ASSESSMENT' && $transaction->status=='active'){
					//since he's assessed so we can't use his CR transaction... weakness: what about if he's assessed first and then he registers... assume assume assume... sigh
					if(isset($latest_CR_transaction) && $latest_CR_transaction->end_year == $transaction->end_year && $latest_CR_transaction->term == $transaction->term){
						unset($latest_CR_transaction);
					}
				}
				$this->total_previous_debit += $transaction->debit;
				$this->total_previous_credit += $transaction->credit;
				
			}
			if($transaction->status=='written_off'){
				$this->bad_debt += $transaction->debit - $transaction->credit;
			}
			if($transaction->transaction_detail == 'BAD DEBTS'){
				$this->bad_debt += $transaction->debit - $transaction->credit;
			}
		}
		//This is where the decision whether to use the latest CR transaction or NOT is used. 
		if( ! $has_college_registration){
			$this->current_registration = isset($latest_CR_transaction) ? $latest_CR_transaction->credit : 0;
			$this->current_registration_transaction = isset($latest_CR_transaction) ? $latest_CR_transaction : array();
		}
		//echo "COLLEGE REGISTRATION: " . $this->current_registration . " term: " . $this->current_registration_transaction->term . " end year: " . $this->current_registration_transaction->end_year; die();
		//INVOICES THIS TERM:
		$invoices_this_term = array();
		$current_credit_without_CR = array();

		//this is a forced solution - should refactor sooner  Toyet 9.18.2018
		//this is a forced solution - should refactor sooner  Toyet 9.18.2018
		//this is a forced solution - should refactor sooner  Toyet 9.18.2018
		$this->current_registration = 0;
		$this->balance_before_assessment = $has_college_registration ? ($this->total_previous_debit - $this->total_previous_credit):($this->total_previous_debit - $this->total_previous_credit + $this->current_registration); //if positive this will be considered as previous balance...

		$current_credit_transactions_after_assessment = array();
		$this->total_current_credit = 0;  //added Toyet 7.10.2018
		$this->total_current_debit = 0;  //added Toyet 7.10.2018

		foreach ($this->current_term_transaction as $current_transaction){

			if($current_transaction->transaction_detail=='ASSESSMENT'){
				$assessment_date = $current_transaction->transaction_date;
			}

			if(!isset($assessment_date)){
				if($current_transaction->credit!=0 && $current_transaction->transaction_detail!=='COLLEGE REGISTRATION')
					$this->balance_before_assessment = $this->balance_before_assessment - $current_transaction->credit;// + $current_transaction->debit;
			} else {
				//after assessment:
				if($current_transaction->credit!=0) {
					$current_credit_transactions_after_assessment[] = $current_transaction; 			
					$this->total_current_credit += $current_transaction->credit;  //added Toyet 7.10.2018
				} elseif($current_transaction->debit!=0 and $current_transaction->transaction_detail!='ASSESSMENT') {
					$this->total_current_debit += $current_transaction->debit;  //added Toyet 7.10.2018
				}
			}
		}

		//log_message("INFO", print_r($current_credit_transactions_after_assessment,true)); // Toyet 7.20.2018
		//log_message("INFO", print_r($this->total_current_debit,true)); // Toyet 7.10.2018
		//log_message("INFO", print_r($this->total_current_credit,true)); // Toyet 7.10.2018

		$this->current_credit_transactions_after_assessment = $current_credit_transactions_after_assessment;
		if(isset($assessment_date)){
			$this->has_assessment = TRUE;
			//credit transaction that does not include college registration...
			foreach ($this->current_term_transaction as $current_transaction){
				if($current_transaction->debit==0 && $current_transaction->transaction_detail!=='COLLEGE REGISTRATION' && (int)str_replace("-", "", $current_transaction->transaction_date) >= (int)str_replace("-", "", $assessment_date)){
					$this->current_credit_without_CR[] = (object)$current_transaction;
				}
			}
			//if positive balance before assessment... then this should also be divided by the remaining periods and added to invoices for each period... else
			//considered as advanced payment...
			if($this->balance_before_assessment > 0 && $this->balance_before_assessment < $this->current_registration){
				//student has arrears...
				$count = 0;
				foreach($this->period_dates as $period=>$dperiod_date){
					if((int)str_replace("-", "", $dperiod_date) > (int)str_replace("-", "", $assessment_date)){
						$invoices_this_term['Inv for Previous Balance - ' . $period] = (object)array(
								'transaction_detail'=>'Inv for Previous Balance - '.$period,
								'credit'=>0,
								'debit'=>($this->balance_before_assessment/(count($this->period_dates)-$count)),
								'period'=>$period,
								'transaction_date'=>$dperiod_date);
					} else {
						$count++;
					}
				}
			} elseif($this->balance_before_assessment <= 0){
				//student has no arrears or has credit balance....
				$invoices_this_term['Forwarded Balance'] = (object)array(
						'transaction_detail'=>'Forwarded Balance',
						'credit'=>sprintf('%01.2f', abs($this->balance_before_assessment)),
						'debit'=>0,
						'period'=>'Prelim',
						'transaction_date'=>isset($assessment_date) ? $assessment_date : '',
				);
			}

			//CT after assessment...
			//print_r($this->current_term_debit_transactions); die();
			foreach($this->current_term_debit_transactions as $debit_transaction){
				$count = 0;
				foreach($this->period_dates as $period=>$dperiod_date){
					if((int)str_replace("-", "", $dperiod_date) > (int)str_replace("-", "", $debit_transaction->transaction_date)){
						$invoices_this_term[$debit_transaction->transaction_detail . ' - '.$period . ' - ' . $debit_transaction->ledger_id] = (object)array(
								'transaction_detail'=>$debit_transaction->transaction_detail . ' - '.$period,
								'credit'=>0,
								'debit'=>$debit_transaction->debit/(count($this->period_dates)-$count),
								'period'=>$period,
								'original_transaction_date'=>$debit_transaction->transaction_date,
								'transaction_date'=>($period == 'Prelim' ? $debit_transaction->transaction_date : $dperiod_date),
						);
					} else {
						$count++;
					}
				}
			}
			//lets break down the CR into parts...
			$credit_from_CR_per_period = $this->current_registration / (count($this->period_dates));
			foreach($this->period_dates as $period=>$dperiod_date){
				$credits_from_CR["CREDIT FROM CR - " . $period] = (object)array(
								'transaction_detail'=>"CREDIT FROM CR" . ' - '.$period,
								'credit'=>$credit_from_CR_per_period,
								'debit'=>0,
								'period'=>$period,
								'original_transaction_date'=>$debit_transaction->transaction_date,
								'transaction_date'=>($period == 'Prelim' ? $debit_transaction->transaction_date : $dperiod_date),
						);
			}
			//print_r($credits_from_CR); die();
			//$invoices_this_term = array_merge($current_credit_without_CR, $invoices_this_term);
			usort($invoices_this_term, array($this, 'compare_object_by_date_property'));
			$this->invoices_this_term = $invoices_this_term;
			
			//credits from CR...
			usort($credits_from_CR, array($this, 'compare_object_by_date_property'));
			$this->credits_from_CR = $credits_from_CR;

		}//end has isset($assessment_date)...
		/*
		//DEBUGGING:
		echo "LEDGER BALANCE: ".$this->ledger_balance() . "\n\n";
		$assessment_content = array_merge($this->invoices_this_term, $this->current_credit_transactions_after_assessment);
		$assessment_content = array_merge($assessment_content, $this->credits_from_CR);
		usort($assessment_content,array($this, 'compare_object_by_date_property'));
		$balance = 0;
		foreach($this->invoice_data('Pre-Final') as $transaction){
			$balance += $transaction->credit - $transaction->debit;
			echo "TRANS: " . $transaction->transaction_detail . " CREDIT: " . $transaction->credit . " DEBIT: " . $transaction->debit . " BALANCE: " . $balance . "\n";	
		}
		die();*/
		//log_message("INFO", "INVOICES THIS TERM"); // Toyet 7.4.2018
		//log_message("INFO", print_r($this->balance_before_assessment,true)); // Toyet 7.4.2018
		//log_message("INFO", print_r($this->balanceBeforeAssessment(),true)); // Toyet 7.4.2018
		//$this->payable_for_current_period = ceil($this->ledger_balance()/2); // for Pre-Final
		$this->payable_for_current_period = $this->due_this_period($this->current_period,
                                                                           $this->assessment,
                                                                           $this->current->num_term);

	}
	
	public function assessment(){
		return $this->assessment;
	}
	
	public function current_term_transactions(){
		
		$previous_debit_balance = $this->total_previous_debit - $this->total_previous_credit;
		$current_term_transactions = array_merge(
				array((object)array(
						'transaction_date'=>'',
						'credit'=>$previous_debit_balance < 0 ? -$previous_debit_balance : "",
						'debit'=>$previous_debit_balance > 0 ? $previous_debit_balance : "",
						'transaction_detail'=>'FORWARDED BALANCE>>>>')),
				$this->current_term_transaction);
		return $current_term_transactions;
	}
	public function over_payment(){
		//$over_payment = ($this->total_previous_credit + $this->total_current_credit - $this->total_current_debit - $this->total_previous_debit);
		$over_payment = 0;
		return ($over_payment > 0 ? $over_payment : 0);
	}
	public function all_transactions(){
		return $this->all_transactions; // includes all posted and unposted payments...
	}
	public function invoice_data($period='Prelim'){
		$invoices = array_merge($this->invoices_this_term, $this->current_credit_transactions_after_assessment);
		if(is_array($this->credits_from_CR) && count($this->credits_from_CR) > 0)
			$invoices = array_merge($invoices, $this->credits_from_CR);
		usort($invoices, array($this, 'compare_object_by_date_property'));
		$periods_covered = array();
		foreach($this->period_dates as $key=>$dperiod){
			if($key==$period){
				$periods_covered[] = $key;
				break;
			}
			$periods_covered[] = $key;
		}
		$total_credits=0;
		$last_credit_date = date('Y-m-d');
		$invoice_items=array();
		foreach($invoices as $invoice_item){
			if(isset($invoice_item->period) && in_array($invoice_item->period, $periods_covered)){
				$invoice_items[] = $invoice_item;
			}
			if(!isset($invoice_item->period))
				$invoice_items[] = $invoice_item;
		}
		return $invoice_items;
	}
	public function balance_for_period($period, $round=TRUE){
		$invoices = $this->invoice_data($period);
		$debit_balance = 0;
		foreach($invoices as $invoice){
			$debit_balance += $invoice->debit - $invoice->credit;
		}
		$debit_balance = $debit_balance > 0 ? $debit_balance : 0;
		$debit_balance = ($round) ? ceil($debit_balance) : $debit_balance;
		return $debit_balance;
	}
	public function ledger_balance(){
		$debit_balance = 0;
		foreach($this->all_transactions as $transaction){
			$transaction = (object)$transaction;
			$debit_balance += $transaction->debit - $transaction->credit;
		}
		return ($debit_balance > 0 ? $debit_balance : 0);
	}	
	
	/*public function due_this_period($round=TRUE){
		$period = $this->current_period;
		$due_this_period = 0;
		print_r($this->invoice_data($period)); die();
		foreach ($this->invoice_data($period) as $transaction){
			$transaction = (object)$transaction;
			$due_this_period += $transaction->debit - $transaction->credit;
			//print($due_this_period."<p>");
		}
		//die($this->ledger_balance());
		$due_this_period = ($period!=='Final' ? ceil($due_this_period) : $this->ledger_balance()); 
		return ($due_this_period > 0 ? $due_this_period : 0);
	}*/
	
//	function due_this_period() {
//		$period = $this->current_period;
//		$due_this_period = 0;

/*	function due_this_period( $period = NULL) {
		$period = empty($period) ? $this->current_period : $period;
		$due_this_period = 0;
		switch ($period) {
			case 'Prelim':
				$due_this_period = $this->ledger_balance()/4;
				
			break;
			case 'Midterm':
				$due_this_period = $this->ledger_balance()/3;
				break;
			case 'Pre-Final':
				$due_this_period = $this->ledger_balance()/2;
				
			break;
			case 'Final':
				$due_this_period = $this->ledger_balance();
				
				break;
			
		}
		$due_this_period = $due_this_period < 1 ? 0 : $due_this_period;
		$due_this_period =  $period == 'Final' ? $due_this_period : ceil($due_this_period);
		return $due_this_period;
		
	}*/


	//Toyet Suggestion 11.23.2017
	//                  8.1.2018 - ongoing...
	//                 9.18.2018 - error on prefinal due
	function due_this_period( $period = NULL, $assessed_value = 0.00, $term = NULL ) {

		$period = empty($period) ? $this->current_period : $period;
		$assessed_value = empty($assessed_value) ? $this->assessment : $assessed_value;
		$due_this_period = 0;
		$ledger_balance = 0;
		$debit_adj_after_assess = 0;

		switch ($period) {
			case 'Prelim':
				$due_this_period = $assessed_value/4;
				$ledger_balance = $this->ledger_balance();
				if($ledger_balance<0){ 
					if($due_this_period > abs($ledger_balance)) {
						$due_this_period = $due_this_period - abs($ledger_balance); 
					} else { 
						$due_this_period = 0;
					} 
				} 
				$debit_adj_after_assess = $this->current_debit_div4 / 4;
				break;

			case 'Midterm':
				if($term='Summer'){
					$due_this_period = $assessed_value/2;   //changed Toyet 4.17.2018
				} else {
					$due_this_period = ($assessed_value/4)*2;
				}
				$debit_adj_after_assess =  $this->current_debit_div4 / 3;
				$ledger_balance = $this->ledger_balance();
				break;

			case 'Pre-Final':
				//$debit_adj_after_assess =  ($this->current_debit_div4/4) * 3;
				//$due_this_period = ($assessed_value/4)*3;
				$due_this_period = $this->ledger_balance()/2;
				break;

			case 'Final':
				$due_this_period = $this->ledger_balance();
				break;
			
		}

		//log_message("INFO", "INVOICE => period      : ".print_r($period,true)); //Toyet 8.8.2018
		//log_message("INFO", "INVOICE => due SA WALA PA: ".print_r($due_this_period,true)); //Toyet 8.8.2018

		//$due_this_period += $debit_adj_after_assess;
		//$due_this_period = $due_this_period - $this->total_current_credit + $this->current_debit_div1 + $this->balance_before_assessment;

		//log_message("INFO", "INVOICE => due SA HAPIT NA: ".print_r($due_this_period,true)); //Toyet 8.8.2018

		$due_this_period = $due_this_period < 1 ? 0 : $due_this_period;
		$due_this_period =  $period == 'Final' ? $due_this_period : ceil($due_this_period);

		//log_message("INFO", "INVOICE => bal b4      : ".print_r($this->balance_before_assessment,true)); //Toyet 8.8.2018
		//log_message("INFO", "INVOICE => due         : ".print_r($due_this_period,true)); //Toyet 8.8.2018
		//log_message("INFO", "INVOICE => debits b4   : ".print_r($this->total_current_debit,true)); //Toyet 8.8.2018
		//log_message("INFO", "INVOICE => debits after: ".print_r($debit_adj_after_assess,true)); //Toyet 8.8.2018
		//log_message("INFO", "INVOICE => credit      : ".print_r($this->total_current_credit,true)); //Toyet 8.8.2018
		//log_message("INFO", "INVOICE => debits div4 : ".print_r($this->current_debit_div4,true)); //Toyet 8.8.2018
		//log_message("INFO", "INVOICE => debits div1 : ".print_r($this->current_debit_div1,true)); //Toyet 8.8.2018

		return $due_this_period;
		
	}
		
	public function bad_debt(){
		return ($this->bad_debt > 0 ? $this->bad_debt : 0);
	}
	public function balance_previous_term(){
		$balance_previous_term = $this->total_previous_debit - $this->total_previous_credit;
		return $balance_previous_term > 0 ? $balance_previous_term : 0;
	}
	public function current_period(){
		return $this->current_period;
	}
	public function credit_balance(){
		$ledger_balance = $this->total_current_debit + $this->total_previous_debit - ($this->total_previous_credit + $this->total_current_credit);
		return -$ledger_balance;
	}
	private function compare_object_by_date_property($a, $b){
		if($a->transaction_detail=='Forwarded Balance'){
			return -1;
		}
		if($b->transaction_detail=='Forwarded Balance'){
			return 1;
		}
		if($a->transaction_date == $b->transaction_date)
			return 0;
		return $a->transaction_date < $b->transaction_date ? -1 : 1;
	}
	
	private function compare_array_by_date_key($a, $b){
		if($a['transaction_detail']=='Forwarded Balance'){
			return -1;
		}
		if($b['transaction_detail']=='Forwarded Balance'){
			return 1;
		}
		if($a['transaction_date'] == $b['transaction_date'])
			return 0;
		return $a['transaction_date'] < $b['transaction_date'] ? -1 : 1;
	}

	//added Toyet 7.4.2018
	public function balanceBeforeAssessment() {
		return $this->balance_before_assessment;
	}

	//added Toyet 7.10.2018
	public function transAfterAssessment() {
		return $this->total_current_debit-$this->total_current_credit;
	}

}
