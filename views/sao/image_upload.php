<div class="span12">
	<div id="scanned_tor"></div>
	<div id="tor_upload_form">
		<form id="upload_tor_form" method="post" action="" class="form-horizontal" enctype="multipart/form-data" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<input type="hidden" name="action" value="upload" />
			<input type="hidden" name="idx1" value=""/>
			<input type="hidden" name="idy1" value=""/>
			<input type="hidden" name="idx2" value=""/>
			<input type="hidden" name="idy2" value=""/>
			<input type="hidden" name="sigx1" value=""/>
			<input type="hidden" name="sigy1" value=""/>
			<input type="hidden" name="sigx2" value=""/>
			<input type="hidden" name="sigy2" value=""/>
			<fieldset>
				<div class="control-group formSep">
					<div style="max-width:500px">
						<img class="thumbnail" id="id-image" for="id_picture" class="preview-image" src="<?php echo $image; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="tor" class="control-label">ID Picture</label>
					<div class="controls">
						<input type="file" id="id_picture" name="id_picture" for="id-image" class="input-xlarge image_input" placeholder="ID Picture" />
						<input id="id-image-submit" type="submit" name="submit" class="btn btn-primary" value="Crop Picture" /> 
					</div>
				</div>
				<div class="control-group formSep">
					<div style="max-width:500px">
						<img class="thumbnail" id="sig-image" for="sig_picture" class="preview-image" src="<?php echo $sign_image; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="tor" class="control-label">Signature</label>
					<div class="controls">
						<input type="file" id="sig_picture" name="sig_picture" for="sig-image" class="input-xlarge image_input" placeholder="Signature" />
						<input type="submit" name="submit" class="btn btn-primary" value="Submit Signature" />
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script>
$('.image_input').bind('change', function(){
	
	var reader = new FileReader();
	var f = $(this).get(0).files;
	var preview = $(this).attr('for');

	if (preview=='id-image'){
		$('#' + preview + '-submit').val('Submit Picture');	
	} else {
		$('#' + preview + '-submit').val('Submit Signature');
	}
	
	
	reader.onload = function (e) {
		$('#' + preview).attr('src', e.target.result);
	};
	reader.readAsDataURL(f[0]);
});

$(document).ready(function(){

	$('#id-image').imgAreaSelect({
		onSelectEnd: function (img, selection) {
			console.log(selection.x1 + ', ' + selection.y1 + ', ' + selection.x2 + ', ' + selection.y2);
			$('input[name="idx1"]').val(selection.x1);
			$('input[name="idy1"]').val(selection.y1);
			$('input[name="idx2"]').val(selection.x2);
			$('input[name="idy2"]').val(selection.y2);
		}
	});
	
	$('#sig-image').imgAreaSelect({
		onSelectEnd: function (img, selection) {
			console.log(selection.x1 + ', ' + selection.y1 + ', ' + selection.x2 + ', ' + selection.y2);
			$('input[name="sigx1"]').val(selection.x1);
			$('input[name="sigy1"]').val(selection.y1);
			$('input[name="sigx2"]').val(selection.x2);
			$('input[name="sigy2"]').val(selection.y2);
		}
	});
});
</script>