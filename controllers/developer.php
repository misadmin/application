


<?php 
/**
 * The GOD controller
 * @author Kevin Felisilda
 *
 */


class Test_class {
	public $gender = 'Male';
	public $name = 'Kevin';
	public $id = '06128492';
	public $sy = '4th';
	private $password = 'ahcaksunfuear';
	protected $girlfriend = 'void';
	
	public function __construct(){
		
	}
}

class Developer extends MY_Controller {
	
	/**
	 * This specify which folders to be excluded on searching for classes
	 * 
	 * Example:
	 * <pre class="sunlight-highlight-php">
	 * $excluded_folders = array('font','..','.');
	 * </pre>
	 */
	private $excluded_folders = array('font','..','.');
	
	/**
	 * Constructor
	 * Nothing else
	 * 
	 */
	public function __construct(){
		parent::__construct();
		
		$this->navbar_data['menu'] =
		array(
			'Documentation'=>'documentation',
			'Users\' Feedback'=>'feedback',
			'App Settings' => 
				array('Tellers Terminals'=>'terminal',
					  'Edit Config.php'=>'edit_config',
					  'List Students After Posting'=>'list_after_posting',	
		),
			'Schools' =>
				array('Other Schools Towns'=>'other_Schools',
						'Schools'=>'schools',
						"SHS Fix"=>'shs_fix',
						"Basic Ed Fix" => 'bed_fix',
					),
			'Students' =>'student',	
			'Various Reports' =>
				array( 'Random IDs' => 'random_ids',
					   'Undersized Class - 15 and below [1st Sem 2017-2018]' => 'undersized_class_466',
					   'Undersized Class - 16 - 20 [1st Sem 2017-2018]' => 'undersized_class20_466',
					   'Undersized Class - 21 - 30 [1st Sem 2017-2018]' => 'undersized_class30_466',
					   'Undersized Class - 31 - 40 [1st Sem 2017-2018]' => 'undersized_class40_466',
					   'All Class Offerings [1st Sem 2017-2018]' => 'undersized_classAll_466',
					   'Undersized Class - 15 and below [2nd Sem 2017-2018]' => 'undersized_class_467',
					   'Undersized Class - 16 - 20 [2nd Sem 2017-2018]' => 'undersized_class20_467',
					   'Undersized Class - 21 - 30 [2nd Sem 2017-2018]' => 'undersized_class30_467',
					   'Undersized Class - 31 - 40 [2nd Sem 2017-2018]' => 'undersized_class40_467',
					   'All Class Offerings [2nd Sem 2017-2018]' => 'undersized_classAll_467',				   
					    ),
			'Misc Queries' =>
				array( 'List of Mismatched Assessments - College' => 'mistmatched_assessments_college',
					   'List of Mismatched Assessments - Basic Ed ' => 'mistmatched_assessments_bed'
					    )


		);
		$this->content_lib->set_navbar_content('', $this->navbar_data);
	}
	
	public function student2($idnum){
		$this->load->library('../controllers/dean', array(), 'dean_lib');
		//$this->dean_lib->tools();
		$this->dean_lib->student($idnum);		
	}
	
	public function edit_config(){
		$this->content_lib->set_message('wish list!', 'alert-error');
		$this->content_lib->content();
		
	}
	
	public function num_to_wrd($num = 3){
		$this->load->helper(array('number_words'));
		$this->content_lib->enqueue_body_content('',convert_number_to_words($num));
		$this->content_lib->content();
	}
	
	/**
	 * This method shows the documentation to the user...
	 * 
	 */
	public function documentation(){
		$this->load->library('tab_lib');
		
		//$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$post_requests = $this->input->post();
		
		if (isset($post_requests['type']) AND isset($post_requests['name'])){
			$this->content_lib->enqueue_footer_script('sunlight');
			$this->content_lib->enqueue_footer_script('sunlight_php');
			$this->content_lib->enqueue_header_style('sunlight_dark');
			$this->load->library('document_lib', $post_requests);
			$classes = $this->document_lib->process_class();
			$this->content_lib->enqueue_body_content('admin/class_definitions', array('classes'=>$classes,'page'=>$post_requests['type']));
		} else {
			$en_lib = FALSE;
			$en_con = FALSE;
			$en_mod = FALSE;
			if (isset($post_requests['page'])){
				switch($post_requests['page']){
					case 'controllers':
						$en_con = TRUE;
						break;
					case 'libraries':
						$en_lib = TRUE;
						break;
					case 'models':
						$en_mod = TRUE;
						break;
					default:
						$en_lib = TRUE;
						break;
				}
			} else $en_lib = TRUE;
			$files = array();
			$files['controllers'] = $this->_get_dir_php_contents('controllers');
			$files['libraries'] = $this->_get_dir_php_contents('libraries');
			$files['models'] = $this->_get_dir_php_contents('models');
			
			$this->tab_lib->enqueue_tab('Libraries', 'admin/classes', array('type'=>'libraries', 'files'=>$files['libraries']), 'libraries', $en_lib);
			$this->tab_lib->enqueue_tab('Controllers', 'admin/classes', array('type'=>'controllers', 'files'=>$files['controllers']), 'controllers', $en_con);
			$this->tab_lib->enqueue_tab('Models', 'admin/classes', array('type'=>'models', 'files'=>$files['models']), 'models', $en_mod);
			
			$this->content_lib->enqueue_body_content('', '<h2 class="heading">Documentation</h3>'.$this->tab_lib->content());
			$this->content_lib->enqueue_body_content('admin/class_form');
		}
		$this->content_lib->content();
	}
	
	public function debug(){
		$this->role = 'Debugger';
	}
	
	/**
	 * This function is used to test the functionality of the table_lib
	 */
	public function table(){
		$this->load->model('student_common_model');

		$this->load->library('table_lib', array('class'=>array('table','table-bordered','table-condensed')));
		$result = array();
		$search = array('tiro,jas','bajao,aljun','mig,char','felisilda,kevin');
		foreach($search as $name){
			$result[] = $this->student_common_model->search($name,50);
		}
		//die();
		$persons = array();
		foreach($result as $res1){
			foreach($res1 as $res){
				$this->table_lib->add_tbody_row(array('<img style="height:50px" src="http://mis3/hnumis/assets/img/students/'.substr($res->idno,0,3).'/'.$res->idno.'.jpg"/>', ($res->gender =='F' ? 'Female': 'Male'), $res->fullname),($res->gender =='F' ? 'class="error"': ''));
			}

		}
		$this->table_lib->add_thead_row(array(array('value' => 'JACK TEAM','attributes'=>array('colspan'=>'3', 'style'=>'text-align:center;font-size:2.5em;padding:20px'))),'', 'thead');
		$this->table_lib->add_thead_row(array(array('value' => 'Image','attributes'=>array('width'=>'30%')), 'Gender', 'Full Name'),'', 'thead');
		
		//$this->table_lib->set_tbody($persons);
		$this->content_lib->enqueue_body_content('', $this->table_lib->content());
		$this->content_lib->content();
	}
	
	/**
	 * This function is used to test the table_lib with formatting
	 */
	public function table_with_formatting(){
		$student = new Test_class();
		$this->load->library('table_lib',array('style'=>'background:#eee','class'=>'table'));
		
		//$this->table_lib->add_thead_row(array(array('value'=>'developers','attributes'=>'colspan="3"')));
		//$this->table_lib->add_thead_row(array('ID','NAME','GENDER','NAME','ID'),'');
		
		$attributes = array('class' => 'table table-condensed',
							'id' => 'students',
							'width' => '100%'
							);
		
		$this->table_lib->set_table_attributes($attributes);
		$this->table_lib->set_column_names(array('id','name','gender'));

		$this->table_lib->add_tbody_row(array('06247912','Jason','Male'));
		$this->table_lib->add_tbody_row($student);
		$this->table_lib->add_tbody_row(array('id'=>'06247912','name'=>'Aljun','gender'=>'Male'));
		
		
		$this->content_lib->enqueue_body_content('',$this->table_lib->content());
		$this->content_lib->content();
	}
	
	/**
	 * This function searches for php files in a certain folder
	 * @param unknown_type $dir_name
	 * @return multitype:string |multitype:string NULL
	 */
	private function _get_dir_php_contents($dir_name){
		$directory = '';
		$files = array();
		
		if (!isset($dir_name) || empty($dir_name)) {
			return array(
					'result' => 'failure',
					'reason' => 'empty dir name'
			);
		}
		
		$directory = APPPATH.$dir_name;
		
		if ($handle = opendir($directory)) {
			while (false !== ($entry = readdir($handle))) {
				if (!in_array($entry, $this->excluded_folders) AND is_dir($directory.'/'.$entry)){
					if ($handle2 = opendir($directory.'/'.$entry)) {
						while (false !== ($entry2 = readdir($handle2))) {
							if (preg_match('!.php!i', $entry2)) {
								$files[] = $entry.'/'.preg_replace('!.php!i','',$entry2);
							}
						}
						closedir($handle2);
					}
				}
				else if (preg_match('!.php!i', $entry)) {
					$files[] = preg_replace('!.php!i','',$entry);
				}
			}
			closedir($handle);
		}
		return $files;
	}
	
	public function feedback(){
		//$this->content_lib->set_message('On this site will rise the Users\' Feedback list, harhar!', 'alert-error');												
		$this->load->model('common_model');		
		if ($this->input->post()){
			switch ($this->input->post('action')){
				case "Delete":
					//print_r($this->input->post());die();	
					if ($deleted = $this->common_model->delete_feedbacks($this->input->post('check')) AND $deleted > 0)
						$this->content_lib->set_message("Successfully deleted $deleted rows!","alert-success");				
					else 
						$this->content_lib->set_message('Unable to delete!','alert-error');
					break;
				case "Mark as Resolved":
					if ($resolved = $this->common_model->mark_resolved($this->input->post('check'))  AND $resolved>0)
						$this->content_lib->set_message('Successfully marked as Resolved!','alert-success');
					else 
						$this->content_lib->set_message('Unable to mark as resolved!','alert-error');
					break;
				case "Unresolve":
					if ($resolved = $this->common_model->unresolve($this->input->post('check'))  AND $resolved>0)
						$this->content_lib->set_message('Successfully unresolved!','alert-success');
					else 
						$this->content_lib->set_message('Unable to unresolve!','alert-error');
					break;
				case "Remark":
					//print_r($this->input->post());die();								
					if ($dev_remark = $this->common_model->dev_remark($this->input->post('id_to_update'),$this->input->post('dev_remark')) AND $dev_remark>0)
						$this->content_lib->set_message("Successfully posted!","alert-success");
					else
						$this->content_lib->set_message("Unable to post Developer's Remark!","alert-error");
					break;
			}
		}		
		$this->load->library('table_lib');
		$this->load->library('form_lib');		
		$data['data'] = $this->common_model->get_feedbacks();
		$this->content_lib->enqueue_body_content('feedback_view', $data);
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->content_lib->content();
	}

	public function student(){
		if ($this->session->userdata('empno')>20000)
			return;

		// choose only what is needed...
		$this->load->model('hnumis/College_Model');
		$this->load->model('hnumis/Programs_Model');
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('hnumis/Rooms_Model');
		$this->load->model('hnumis/Offerings_Model');
		//$this->load->model('hnumis/Faculty_Model');
		$this->load->model('hnumis/Student_Model');
		$this->load->model('hnumis/Enrollments_model');
		$this->load->model('hnumis/Prospectus_Model');
		//$this->load->model('hnumis/BlockSection_Model');
		$this->load->model('financials/Payments_Model');		
		$this->load->library('form_validation');
		$this->load->library('my_pdf');
		$this->load->helper('student_helper');
		
		
		
		$this->content_lib->set_title('Dean | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
	
		if ( ! empty($query)){
				
			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);
				
			$this->load->library('pagination_lib');
				
			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);
	
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
	
				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//var_dump ($results);
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->set_message("No result found for that query", 'alert-error');
			}
		}
	
		if (is_numeric($idnum)){
				
			//after the search... when results are found....
			$this->load->model('student_common_model');
			$this->load->model('Academic_terms_model');
			$this->load->model('hnumis/BlockSection_Model');
			$this->load->model('hnumis/Prospectus_Model');
			$this->load->model('teller/teller_model');
			$this->load->model('teller/assessment_model');
				
			$this->content_lib->set_title ('Dean | ' . $idnum);
			$result = $this->student_common_model->my_information($idnum);
			$result2 = $this->teller_model->get_student($idnum);
				
			//$tab = 'assessment';
			$tab = "student_info";
			
			switch ($action = $this->input->post('action')){	
				case 'create_remarks':		
					if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
						$remarks = $this->input->post('remarks');
						$view_rights = $this->input->post('view_rights');
						$empno = $this->session->userdata('empno');							
						$vrights  = $view_rights? "Private":"Public";	
						$status  = $remarks != NULL ? $this->Student_Model->createRemarks($idnum, $empno, $remarks, $vrights) : 0;
						if($status){
							$this->content_lib->set_message("Remarks created!", "alert-success");
						}else{
							$this->content_lib->set_message("Error in creating remarks!", "alert-error");
						}
					}else {
						$this->content_lib->set_message("Error in submitting form", 'alert-error');
							
					}	
					break;							

				case 'edit_remarks':						
					if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
						$remarks_id = $this->input->post('remark_id');
						$remark = $this->input->post('remark');							
						$view_rights = $this->input->post('view_rights') == 1 ? 'Private' : 'Public' ;
						$status = $this->Student_Model->editRemark($remarks_id, $remark, $view_rights);							
						if($status){
							$this->content_lib->set_message("Successful in editing the remark!", "alert-success");
						}else{
							$this->content_lib->set_message("Error in editing the remark!", "alert-error");
						}
					}else {
						$this->content_lib->set_message("Error in submitting form", 'alert-error');
	
					}						
					break;
	
				case 'delete_remark':
	
					if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
						$remarks_id = $this->input->post('remark_id');	
						$status = $this->Student_Model->deleteRemark($remarks_id);
						if($status){
							$this->content_lib->set_message("Successful in deleting the remark!", "alert-success");
						}else{
							$this->content_lib->set_message("Error in deleting the remark!", "alert-error");
						}
					}else {
						$this->content_lib->set_message("Error in submitting form", 'alert-error');
					}	
					break;
				case 'generate_class_schedule':
					$this->load->model("hnumis/Reports_model");	
					$student['idno']= $result->idno;
					$student['fname']= $result->fname;
					$student['lname']= $result->lname;
					$student['mname']= $result->mname;
					$student['yr_level']= $result->year_level;
					$student['abbreviation']= $result->abbreviation;
					$student['max_bracket_units']= $this->input->post('max_units');
					$student['student_histories_id']= $this->input->post('student_histories_id');	
					$selected_term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));	
					$this->Reports_model->generate_student_class_schedule_pdf($this->input->post('academic_terms_id'), $student, $selected_term);	
					break;
					
				case 'delete_enrolled_course':
					//print($this->input->post('enrollments_id')); die();
					if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
						if ($this->Student_Model->WithdrawCourse($this->input->post('enrollments_id'))) {
							$this->content_lib->set_message("Enrolled course successfully deleted!", "alert-success");
						} else {
							$this->content_lib->set_message("Error in deleting the course!", "alert-error");
						}
					}else {
						$this->content_lib->set_message("Error in submitting form", 'alert-error');
					}	
					break;
				
				case 'change_forced_college':

					$tab = "forced_enrollment";
					
					$current_term = $this->Academic_terms_model->current_academic_term();
					$selected_college = $this->input->post('colleges_id');
					
					// Code below is hardcoded with $academic_terms_id as 472 to 
					// force 1st Sem 2019-2020.  This should be removed when
					// 1st Sem actually starts    -Toyet 5.6.2019
					if ($this->input->post('colleges_id') != 0) {
						$offerings = $this->Offerings_Model->ListOfferings('471',$this->input->post('colleges_id'));
					} else {
						$offerings = $this->Offerings_Model->ListOfferings('471');
					}
				
					break;
					
				case 'forced_enroll_student':
					
					$this->load->model('hnumis/Courses_Model');
					$this->load->model('accounts/accounts_model');
					$this->load->model('ledger_model');
					//print_r($result2); die();
					$data['course_offerings_id'] = $this->input->post('course_offerings_id');
					$data['student_history_id'] = $result->student_histories_id;
					$data['enrolled_by'] = $this->session->userdata('empno');
					$data['ip_address'] = $this->session->userdata('ip_address');
					$data['post_status'] = "";

					$isAssessed = $this->assessment_model->CheckStudentHasAssessment($result->student_histories_id);

					if(!$isAssessed){
						$enrollments_id = $this->Student_Model->Forced_Enroll_Student($data);
						if ($enrollments_id) {
							$this->content_lib->set_message("Enrolled course successfully added!", "alert-success");
						} else {
							$this->content_lib->set_message("Error in adding the course!", "alert-error");
						}
					} else {
						$data['post_status'] = "added";
						$enrollments_id = $this->Student_Model->Forced_Enroll_Student($data);
						//$enrollments_id = TRUE;
						if ($enrollments_id) {
							/*	ADDED: 11/25/15 by genes 
							*	NOTE: codes copied from student controller
							*/
							$student_info = $this->Student_Model->getStudentInfo($result->student_histories_id);

							$adj['date'] = DATE("Y/m/d");
							$payer_info = $this->teller_model->get_student($student_info->students_idno);
							$adj['payers_id'] = $payer_info->payers_id;
							$adj['academic_programs_id'] = $payer_info->ap_id; //added by tatskie Aug 23, 2014
							$adj['year_level'] = $payer_info->year_level; //added by tatskie Aug 23, 2014								
							$adj['type'] = "Debit";
							$tuition_basic_rate = $this->Student_Model->getTuitionRateBasic($student_info->acad_program_groups_id, $student_info->academic_terms_id, $student_info->year_level);
							$tuition_others_rate = $this->Student_Model->getTuitionOthersRate($enrollments_id, $student_info->academic_terms_id, $student_info->year_level);
							$course_info = $this->Courses_Model->getCourseInfo($enrollments_id);
							$adj['description'] = "ADD SUBJECT: " . $course_info->course_code;
							$cur_aca_yr = $this->AcademicYears_Model->current_academic_year();
							$lab_amount = $this->accounts_model->get_lab_fee($course_info->id, $cur_aca_yr->id);

							$lab_fee = 0;
							if($lab_amount){
								$lab_fee = $lab_amount->rate;
							}
							if($tuition_others_rate){
								$adj['amount'] = $tuition_others_rate->rate * $course_info->paying_units + $lab_fee;
							}else{
								$adj['amount'] = $tuition_basic_rate->rate * $course_info->paying_units + $lab_fee;
							}
									
							//print_r($adj); die();
							$adj['remark'] = 'Added by Student: ID no. '. $student_info->students_idno;
							$adj['empno'] = $student_info->students_idno;
							$term = $this->academic_terms_model->getCurrentAcademicTerm();
							$adj['term'] = $term->id;
							//print_r($adj); die();

							$postAdded = $this->ledger_model->add_adjustment($adj, 'Y', $student_info->students_idno, DATE("Y/m/d H:i:s"), FALSE);

							/*
							*  end here....
							*/
							
							$this->content_lib->set_message("Enrolled course successfully added!", "alert-success");

						} else {
							$this->content_lib->set_message("Error in adding the course!", "alert-error");
						}
						
					
					}
					/*
					//check if student is assessed
					if ($this->assessment_model->CheckStudentHasAssessment($result->student_histories_id)) {
						$data['post_status'] = "added";
					} else {
						$data['post_status'] = "";
					}
					
					if ($this->Student_Model->Forced_Enroll_Student($data)) {
						$this->content_lib->set_message("Enrolled course successfully added!", "alert-success");
					} else {
						$this->content_lib->set_message("Error in adding the course!", "alert-error");
					}
					*/
					
					$tab = "financials";
					$selected_college = $result->colleges_id;
					$current_term = $this->Academic_terms_model->current_academic_term();
					$offerings = $this->Offerings_Model->ListOfferings($current_term->id,$result->colleges_id);
					
					break;
					
				default:
					$selected_college = $result->colleges_id;
					$current_term = $this->Academic_terms_model->current_academic_term();
					$offerings = $this->Offerings_Model->ListOfferings($current_term->id,$result->colleges_id);
				 
					break;
	
			}
			$result = $this->student_common_model->my_information($idnum);
				
			if ($result != FALSE) {
				//print_r(json_decode($result->meta));die();
				//a user with that id number is seen...
				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> $result->fname. " " . ucfirst(substr($result->mname,0,1)) . ". " . $result->lname,
						'course'	=> $result->abbreviation,
						'college_id'=> $result->colleges_id,
						'college'	=> $result->college_code,
						'level'		=> $result->year_level,
						'full_home_address'	=> $result->full_home_address,
						'full_city_address'	=> $result->full_city_address,
						'phone_number'		=> $result->phone_number,
						'section'			=> $result->section,
				);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
				else {
					if ($result->gender == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}
	
				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...
				$data['prospectus']= $this->Programs_Model->ListPrograms($this->session->userdata('college_id'),'O');
				$data['year_level'] = $result->year_level;
				$data['academic_program'] =$result->abbreviation;
				$data['idnum'] = $idnum;
				$data['academic_program_id'] = $result->ap_id;
	
				$this->load->model('hnumis/Student_model');
				
				$current_term = $this->Academic_terms_model->current_academic_term();
				$grades = $this->Enrollments_model->student_grades($idnum);
	
				$blocksections = $this->BlockSection_Model->ListSectionsProgram($data['year_level'],$data['academic_program_id']);
				$current = $this->Academic_terms_model->getCurrentAcademicTerm();
				$history_id = $this->Student_Model->getEnrollStatus($idnum, $current_term->id);
				$data4 = $this->Student_Model->getCheckWithSummmer($result->prospectus_id, $data['year_level']);
				$data5 = $this->AcademicYears_Model->getCurrentAcademicTerm();
					
				if($data5->term == "Summer" AND !$data4) {
					$max = $this->Student_Model->getMaxInSummmer($idnum, $current_term->id);
					if ($max) {
						$max_units = $max->max_units;
					} else {
						$max_units = 9.0;
					}
				} else {
					if ($history_id) {
						$data2 = $this->Student_Model->getMaxUnits($history_id->id,$current_term->id);
						if ($data2) {
							$max_units = $data2->max_units_to_enroll;
						} else {
							$max_units = 0;
						}
					} else {
						$max_units = 0; //student is not enrolled and max_units cannot be retrieved
					}
				}
	
				//Added 1/25/2013 By: Genes
				$CourseFromProspectus = $this->Prospectus_Model->ListCoursesToBeAdvised($idnum, $result->prospectus_id,$result->year_level);
				//print_r($CourseFromProspectus);
				//die();
				$CourseFromCourses    = $this->Courses_Model->ListAllCoursesToBeAdvised();
				if($history_id){
					$AdvisedCourses = $this->Student_Model->ListAdvisedCourses($history_id->id);
				}else{
					$AdvisedCourses = array();
				}
				//Added: Feb. 13, 2013 by Amie
				$prospectus_id = $this->Prospectus_Model->student_prospectus_ids($idnum);
				$prospectus = $this->Prospectus_Model->student_prospectus($prospectus_id);
	
				//Added: Feb. 19, 2013 by Amie
				$block = $this->Student_Model->get_blocksection($idnum,$current->id);
					
				//added January 4, 2013 (psychometrician exams results)
				$this->load->model('psychometrician_model');
				$student_exams = $this->psychometrician_model->student_exams($idnum);
				$this->load->library('tab_lib');
	
				$this->load->library('print_lib');
	
				
				//This looks dirty... but this will work... make schedule be set using a pull down menu
				//added by: justing 3/15/2012
				$academic_terms = $this->academic_terms_model->student_inclusive_academic_terms($idnum); //terms this student is
				//enrolled...
				if ($action=='schedule_current_term') {
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $this->input->post('academic_term'));
					$tab = 'schedule';
					$selected_term = $this->input->post('academic_term');
				} else {
					$schedule_courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $current_term->id);
					//$tab = 'student_info';
					$selected_term = '';
				}
	
				//display assessment of student
				$result = $this->student_common_model->my_information($idnum);
				$result2 = $this->teller_model->get_student($idnum);
				$assessment_values = array();
	
				$ledger_data = array();
				$ledger_data = $this->teller_model->get_ledger_data($result2->payers_id);
				$this->load->model('academic_terms_model');
	
				//$this->tab_lib->enqueue_tab('Teller', 'teller/teller', $teller_values, 'teller', TRUE);
				$this->load->model('accounts/accounts_model');
				$this->load->model('hnumis/enrollments_model');
				$this->load->model('hnumis/AcademicYears_model');
				$this->load->model('accounts/fees_schedule_model');
				
				$period_now = $this->academic_terms_model->what_period();
	
	
				//assessment details
				$acontent = array(
						'idnumber'	=>$result->idno,
						'familyname'=>$result->lname,
						'firstname'	=>$result->fname,
						'middlename'=>$result->mname,
						'level'		=>$result->year_level,
						'course'	=>$result->abbreviation,
						'due_now' 	=> '',
						'succeeding_dues'	=> '7,756.87'
				);
	
				//assessment form
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
	
				$unposted_students_payments = $this->teller_model->get_unposted_students_payment($result2->payers_id);
				$this->load->library('balances_lib',
						array(
								'ledger_data'=>$ledger_data,
								'unposted_students_payments'=>$unposted_students_payments,
								'current_academic_terms_obj'=>$current_academic_terms_obj,
								'period'=>$this->academic_terms_model->what_period(),
								'period_dates'=>$this->academic_terms_model->period_dates(),
						));

				//assessment form
				if(isset($selected_history_id)){
					$assessment = $this->accounts_model->student_assessment($selected_history_id);
					$other_courses_payments = $this->teller_model->other_courses_payments_histories_id($selected_history_id);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($selected_term_id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($selected_history_id); //ok
					$tab = 'financials';
				} else {
					$assessment = $this->accounts_model->student_assessment($result->student_histories_id);
					$other_courses_payments = $this->teller_model->other_courses_payments($current_academic_terms_obj->id, $result->year_level);
					$laboratory_fees = $this->teller_model->all_laboratory_fees($current_academic_terms_obj->id); //
					$dcourses = $this->enrollments_model->get_enrolled_courses($result->student_histories_id); //ok
				}
	
				$current_academic_terms_obj = $this->academic_terms_model->getCurrentAcademicTerm();
				$lab_courses = array();
				$courses = array();
				$student_inclusive_terms = $this->academic_terms_model->student_inclusive_academic_terms ($idnum);
				
				if(!empty($dcourses)){
					foreach ($dcourses as $course) {
						if ($course->type_description == 'Lab' && empty($course->enrollments_history_id)) {
							$lab_courses[] = array(
									'name'=>$course->course_code,
									'amount'=>(isset($laboratory_fees[$course->id]) ? $laboratory_fees[$course->id] : 0),
							);
						}
						$courses[] = array(
								'id'=>$course->id,
								'name'=>$course->course_code,
								'units'=>$course->credit_units,
								'pay_units'=>$course->paying_units,
								're_enrollments_id'=>$course->re_enrollments_id,
								'withdrawn_on'=>$course->withdrawn_on,
								'assessment_id'=>$course->assessment_id,
								'post_status'=>$course->post_status,
								'enrollments_history_id'=>$course->enrollments_history_id,
								'transaction_date'=>$course->transaction_date,								
						);
					}
				}
				//end of code to display assessment of student
				
				$this->tab_lib->enqueue_tab ('Student Information', 'common/student_information_full', $result, 'student_info',
						($tab=='student_info'));
	
				if (is_college_student($idnum)) {
					if ($this->input->post('academic_term')) {
						$selected_term = $this->input->post('academic_term');
						$terms=$this->AcademicYears_model->getAcademicTerms($this->input->post('academic_term'));
						$acad_term=$terms->term;
					} else {
						$selected_term = $current_academic_terms_obj->id;
						$acad_term = $current_academic_terms_obj->term;
					}
	
					$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $selected_term);
					if ($selected_history) {
						$student_units = $this->Student_model->getUnits($selected_history, $acad_term);
						$student_history_id=$selected_history->id;
						
					} else {
						$student_units['max_bracket_units']=0;
						$student_history_id=0;
					}
						
					$assessment_date = $this->Student_Model->get_student_assessment_date($student_history_id);						

					if (!$assessment_date){
						$transaction_date2=array();
						$assessment_date2[0]= (object)array('transaction_date' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . " + 1 day") ));
						$assessment_date = $assessment_date2;
					}
						
					$acad_term = $this->AcademicYears_Model->getAcademicTerms($selected_term);
					//print_r($schedule_courses); die();	
					$this->tab_lib->enqueue_tab ('Schedule', 'developers/schedule', array('schedules'=>$schedule_courses,
												'assessment_date'=>$assessment_date,
												'academic_terms'=>$academic_terms,
												'academic_terms_id'=>$selected_term,
												'max_units'=>$student_units['max_bracket_units'],					
												'student_histories_id'=>$student_history_id), 'schedule');
				}
	
				$this->tab_lib->enqueue_tab('Financials',
						'',
						array(),
						'financials',
						$tab=='financials');
	
						//ADDED: 6/11/15 by Genes
						$affiliated_fees = $this->fees_schedule_model->ListStudentAffiliated_Fees($student_history_id);

				$this->tab_lib->enqueue_tab('Assessment', 'teller/assessment',
						array(
								'assessment_date'=>$assessment_date[0]->transaction_date,								
								'other_courses_payments'=>$other_courses_payments,
								'assessment'=> $assessment,
								'courses'=>$courses,
								'affiliated_fees'=>$affiliated_fees,
								'lab_courses'=>$lab_courses,
								'student_inclusive_terms' =>$student_inclusive_terms,
								'selected_history_id'=>isset($selected_history_id) ? $selected_history_id : "",
								'student_details'=>$acontent
						),
						'assessment', $tab=='financials', 'financials');
				// Edit by Justing... until here...
	
				//enqueue ledger
				$this->tab_lib->enqueue_tab('Ledger', 'teller/ledger',
						array(
								'ledger_data'=>$ledger_data,
								'semester_start_date'=>$current_academic_terms_obj->semester_start_date,
								'year_start_date'=>$current_academic_terms_obj->year_start_date
						),
						'ledger', FALSE, 'financials');
	
				$payments = $this->Payments_Model->ListStudentPayments($result2->payers_id);
					
				
				$this->tab_lib->enqueue_tab ('Payments', 'student/list_student_payments', array('payments_info'=>$payments), 'payments',
						FALSE,'financials');
	
				if (is_college_student($idnum)){
					$this->tab_lib->enqueue_tab ('Grades', 'student/grades', array('terms'=>$grades), 'grades', FALSE);
				
					/* ADDED: 11/19/15 by genes
 					*  NOTE: For Forced Enrollment
					*/
					$colleges = $this->College_Model->ListColleges();

					$this->content_lib->enqueue_footer_script('data_tables');
					$this->content_lib->enqueue_footer_script('data_tables_tools');		
					
					$this->tab_lib->enqueue_tab ('Forced Enrollment', 'developers/forced_enrollment', 
													array(
													"selected_college"=>$selected_college,
													"colleges"=>$colleges,
													"offerings"=>$offerings,
													), 'forced_enrollment', $tab=='forced_enrollment');
					/*
					*  END: for forced enrollment
					*/
					
					$this->tab_lib->enqueue_tab ('Exam Results', 'student/psych_exam_results_with_form', array('exams_taken'=>$student_exams), 'exam', FALSE);
						
					$prospectus_history = $this->Prospectus_Model->ListProspectusTerms($prospectus_id);						
	
					$this->tab_lib->enqueue_tab('Prospectus',
							'',
							array(),
							'prospectus',
							FALSE);
						
					//ADDED: 3/25/13 
					//EDITED: 1/10/14 by genes
					//only students of the college can be credited by dean
					if ($this->userinfo['college_id'] == $result->colleges_id) {
						$can_credit = TRUE;
					} else {
						$can_credit = FALSE;
					}
					$this->tab_lib->enqueue_tab ('Prospectus History', 'dean/prospectus_history_to_credit',
													array(
															'can_credit'=>$can_credit,
															'prospectus_terms'=>$prospectus_history, 
															'student'=>$dcontent), 
												'prospectus_history',FALSE,'prospectus');

						
					$credited_courses =  $this->Student_Model->ListCreditedCourses($idnum);

					$this->tab_lib->enqueue_tab ('Credited Courses', 'student/list_credited_courses',
							array('credited_courses'=>$credited_courses), 'list_credited_courses',
							FALSE,'prospectus');
	
						
					//Updated: July 15, 2013
					//allows the dean to input a remark for a specific student
					$student_college_Id = $this->Student_Model->getSutdentCollegeId($idnum);
					if ($this->session->userdata('college_id')==$student_college_Id->colleges_id) {
						$this->tab_lib->enqueue_tab('Remarks',
								'',
								array(),
								'remarks',
								FALSE);
						$this->tab_lib->enqueue_tab ("Create", "dean/create_remarks_form", array('current_term'=>$current_term->id),
								"create_remarks", FALSE, 'remarks');
						$remarks_list = $this->Student_Model->ListRemarks($idnum);
						//print_r($remarks_list);
						//die();
						$this->tab_lib->enqueue_tab ("View", "dean/view_remarks", array('all_remarks'=>$remarks_list),
								"view_remarks", FALSE, 'remarks');
					}
						
				}

				/*
				 * shows computation for assessment
				 */
				//print($result->student_histories_id.'<p>'.$result->acad_program_groups_id.'<p>'.$result->year_level.'<p>'.$data5->id.'<p>'.$data5->academic_years_id); die();
				$tuition_basic  =  $this->Student_Model->getMyTuitionFee_Basic($result->student_histories_id, $result->acad_program_groups_id,
						464);
				
				$other_fees     = $this->Student_Model->getMyOtherFees($result->year_level, 464, $result->acad_program_groups_id);

				$tuition_others =  $this->Student_Model->getMyTuitionFee_Others($result->student_histories_id, 464);

				$tuition_byprospectus =  $this->Student_Model->getMyTuitionFee_Prospectus($result->student_histories_id, 464);
				
				$lab_fees       = $this->Student_Model->getMyLabFees($data5->academic_years_id, $result->student_histories_id);
				$total          = $tuition_basic->tuition_fee  + $tuition_byprospectus->tuition_fee + $other_fees->rate + $lab_fees->rate ;

				$total_unposted = $this->Student_Model->getMyTotalUnpostedAssessment($result->student_histories_id, $result->acad_program_groups_id,
						464, $result->year_level, $data5->academic_years_id);
				
				$this->tab_lib->enqueue_tab ('Computed Assessments', 'student/computed_assessments', 
										array('tuition_basic'=>$tuition_basic->tuition_fee,
												'tuition_others'=>$tuition_others->tuition_fee,
												'other_fees'=>$other_fees->rate,
												'lab_fees'=>$lab_fees->rate,
												'total'=>$total,
												'total_unposted'=>$total_unposted->rate), 'computed_assessments', FALSE);
				
											
				$tab_content = $this->tab_lib->content();
				$this->content_lib->flush_sidebar();
				$this->content_lib->enqueue_body_content("", $tab_content);
	
			} else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
				$this->content_lib->set_message('Student does not exist', 'alert-error');
			}
		}
	
		//what will happen if there is no query... and the result is not numeric?
		if ( empty($query) && ! is_numeric($idnum)) {
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$data = array();
			$this->load->model('hnumis/College_model');
			$data['colleges'] = $this->College_model->all_colleges();
			$this->load->model('student_common_model');
	
			if ($this->input->post('what')=='search_by_program'){
				$total = count ($this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), TRUE));
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				$results = $this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), FALSE, $page, $this->config->item('results_to_show_per_page'));
				$this->load->library('pagination_lib');
				$data['pagination'] = $this->pagination_lib->pagination('', $total, $page);
				$data['results'] = $results;
				$this->load->model('hnumis/Programs_model');
				$this->load->model('hnumis/College_model');
				$data['query'] = $this->Programs_model->getProgram($this->input->post('program'))->abbreviation . " " . $this->input->post('year_level');
				$data['programs'] = $this->College_model->programs_from_college($this->input->post('college'));
				$data['college'] = $this->input->post('college');
				$data['program'] = $this->input->post('program');
				$data['year_level'] = $this->input->post('year_level');
				$data['end'] = $end;
				$data['total'] = $total;
				$data['start'] = $start;
			}
			$this->content_lib->enqueue_body_content('common/search_result_by_program', $data);
		}
			
		$this->content_lib->content();
	}
	

	public function faculty(){
		$this->load->library('faculty_lib');
		$this->faculty_lib->faculty(FALSE);
	}

	//Added October 4, 2013
	public function terminal(){
		$this->load->model('teller/terminal_model');
		$this->content_lib->enqueue_header_style('gritter');
		$this->content_lib->enqueue_footer_script('gritter');
		$terminals = $this->terminal_model->retrieve();
		$this_terminal = $this->terminal_model->retrieve($this->input->ip_address());

		$data = array(
				'terminal_table'=>$this->load->view('developers/terminal_table', array(
						'terminals'=>$terminals, 
						'this_terminal'=>$this_terminal), 
						TRUE),
				'terminal_marked'=>(count($this_terminal) > 0 ? TRUE : FALSE),
				'terminal_id'=>(count($this_terminal) > 0 ? $this_terminal->id : 0),
		);
		//register this terminal
		$this->content_lib->enqueue_body_content('developers/terminal', $data);
		$this->content_lib->content();
	}
	
	public function handler(){
		//AJAX handler...
		if ( $this->config->item('ajax_strict')) {
			if ( ! $this->input->is_ajax_request()) {
				echo ("Direct Access Denied...");//this should be on a view...
				die();
			}
		}
		$this->load->model('teller/terminal_model');
		$action = $this->input->post('action');
		//nonce
		if($this->common->nonce_is_valid($this->input->post('nonce'))){
			switch ($action){
				case 'delete' :
					$id = $this->input->post('id');
					if($this->terminal_model->delete($id))
						$return = array('success'=>TRUE, 'message'=>'The terminal was deleted.', 'nonce'=>$this->common->nonce()); else
						$return = array('success'=>FALSE, 'message'=>'Error found while deleting terminal.', 'nonce'=>$this->common->nonce());	
					break;
				case 'edit' :
					$id = $this->input->post('id');
					$terminal_name = sanitize_text_field($this->input->post('terminal_name'));
					$terminal_description = sanitize_text_field($this->input->post('terminal_details'));
					if($result=$this->terminal_model->update($id, array('name'=>$terminal_name, 'description'=>$terminal_description))){
						$terminals = $this->terminal_model->retrieve();
						$return = array('success'=>TRUE, 'message'=>'Terminal specified is updated', 'terminals'=>$terminals, 'nonce'=>$this->common->nonce(), 'id'=>$id);
					} else {
						$return = array('success'=>FALSE, 'message'=>'Error encountered while updating terminal', 'nonce'=>$this->common->nonce());
					}	 
					break;
				case 'mark_terminal' :
					$ip = $this->input->ip_address();
					$terminal_name = sanitize_text_field($this->input->post('terminal_name'));
					if (! empty($terminal_name)){						
						$terminal_description = sanitize_text_field($this->input->post('terminal_details'));
						if($result = $this->terminal_model->create($terminal_name, $terminal_description, $ip)) {
							$mic = $result->mic;
							$id = $result->id;
							$this->input->set_cookie(array('name'=>'terminal', 'value'=>$mic, 'expire'=>365*10*24*60*60));//lets place there a COOKIE...
							$terminals = $this->terminal_model->retrieve();
							$return = array('success'=>TRUE, 'message'=>"This terminal is added to the Teller's list", 'terminals'=>$terminals, 'id'=>$id, 'nonce'=>$this->common->nonce()); 
						} else
							$return = array('success'=>FALSE, 'message'=>"Terminal was not ADDED to teller");
					}else{
						$return  = array('success'=>FALSE, 'message'=>"Required field misssing!");
					}
					break;
			}
		} else {
			$return = array('success'=>FALSE, 'message'=>$this->config->item('nonce_error_message'), 'nonce'=>$this->common->nonce());
		}
		//echo json_encode($return); die();
		$this->output->set_content_type('application/json')->set_output(json_encode((object)$return));
	}
	
	/*
	 * this is a temporary module
	 * by Genes 12/11/13
	 */
	public function list_after_posting() {
		$this->load->library('accounts_lib');
		
		$this->accounts_lib->list_after_posting();
		
	}
	
	//Added: 2/3/2014
	
	public function other_Schools(){
		
		
		if ($this->common->nonce_is_valid($this->input->post('nonce'))){
		
			$this->load->model('hnumis/OtherSchools_Model');
		
				
			$step = ($this->input->post('step') ?  $this->input->post('step') : 1);
				
			switch ($step) {
				case 1:
		
									
					$this->content_lib->enqueue_header_style('gritter');
					$this->content_lib->enqueue_footer_script('gritter');
					
					$data['schools_list'] = $this->OtherSchools_Model->listMasterOtherSchools();
					$data['town_groups'] = $this->OtherSchools_Model->listTownsOfPhilippines();
					//print_r($town_groups); die();
					$this->content_lib->enqueue_body_content('developers/form_add_town_to_Otherschools', $data);
					$this->content_lib->content();
		
					break;
		
				case 2:
		
					$school_id = $this->input->post('school_id');
					$town_id = $this->input->post('town_id');
					//print($town_id);die();
					$status = $this->OtherSchools_Model->insertTownToOtherSchool($school_id,$town_id);
					if($status){
						$this->content_lib->set_message('Successful in inserting town to school!', 'alert-success');
						$this->index();
					}else{
						$this->content_lib->set_message('Error in inserting town to school!', 'alert-error');
						$this->index();
					}
					break;
		
			}
		} else {
			$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			$this->index();
		}
		
	}		
	
	
	public function schools() {
		
			$this->load->model('hnumis/OtherSchools_Model');
			$this->content_lib->enqueue_footer_script('chained');
			$this->content_lib->enqueue_footer_script('data_tables');
					
			if ($this->common->nonce_is_valid($this->input->post('nonce'))) {	
			
				switch ($this->input->post('action')) {
					case 'delete_school': //deletes school
						
							if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
									if ($this->OtherSchools_Model->DeleteSchool($this->input->post('school_id'))) {
										$this->content_lib->set_message("School successfully removed!", "alert-success");
									} else {
										$this->content_lib->set_message("Error found while deleting school!", "alert-error");
									}
							} else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
									$this->index();
							}

							break;

					case 'add_school': //add school
							
								if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
									$data['school_name'] = $this->input->post('school');
									$data['school_abbrev'] = $this->input->post('school_abbreviation');
									$data['towns_id'] = $this->input->post('towns_id');
									$data['levels_id'] = 6; //for college schools
									$data['status'] = 'active'; 
														
									if ($this->OtherSchools_Model->AddSchool($data)) {
										$this->content_lib->set_message("School successfully added!", "alert-success");
									} else {
										$this->content_lib->set_message("Error found while adding school!", "alert-error");
									}
								} else {
									$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
									$this->index();
								}
							
								break;
					default:
						//$this->session->unset_userdata('academic_terms_id');
			
						//$this->list_terms_courses();
						break;
				}
			} else {
						$this->content_lib->set_message("Error in submitting form", 'alert-error');	
		   	}
		   	$data['country_groups'] = $this->OtherSchools_Model->listCountries();
		   	$data['province_groups'] = $this->OtherSchools_Model->listProvinces();
		   	$data['town_groups'] = $this->OtherSchools_Model->listTowns();
		   	$data['schools'] = $this->OtherSchools_Model->ListSchools();
		   	//print_r($data['schools']); die();
		   	$this->content_lib->enqueue_body_content('developers/form_to_list_schools', $data);
		   	$this->content_lib->content();
		   	
		}
	
		public function sms (){
			$message='';
			if ($this->input->post() && $this->common->nonce_is_valid($this->input->post('nonce'))){
				$this->load->model('sms_model');
				$this->load->library('sms_client', $this->config->item('sms_api_config'));
				$messages = $this->sms_model->college_mass_sending($this->input->post('message'), $this->input->post('program'), $this->input->post('year_level'), $this->input->post('academic_term_id'));
				$return = json_decode($this->sms_client->send_multiple_messages($messages));
				if(isset($return->success) && $return->success===TRUE){
					$severity = 'success';
					$message = $return->message;
				} else {
					$severity = 'error';
					$message = (isset($return->message) ? $return->message : 'Error found while sending messages.');
				}
			}
			$this->load->model('hnumis/college_model');
			$this->load->model('academic_terms_model');
			$this->content_lib->enqueue_body_content('developers/sms_sending', array(
				'academic_terms'=>$this->academic_terms_model->academic_terms(10),
				'colleges'=>$this->college_model->all_colleges(),
				'message'=>$message,
				'severity'=>isset($severity) ? $severity : ''));
				$this->content_lib->content();
		}
		public function grades_sms ($period){
			//if ($this->input->post() && $this->common->nonce_is_valid($this->input->post('nonce'))){
				$this->load->model('hnumis/grades_model');
				$this->load->model('sms_model');
				$template = "HNUSMS: [[idno]], Your [period] grade for the subject: [course] is [grade].";
				$this->load->library('sms_client', $this->config->item('sms_api_config'));
				$result = $this->grades_model->grades_for_period(455);
				$messages = array();
				foreach ($result as $row){
					$message = str_replace(array('[idno]', '[period]', '[course]', '[grade]'), array($row->students_idno, 'Final', $row->course_code, $row->finals_grade), $template);
					$messages[] = (object)array('number'=>$this->sms_model->ten_digit_sms_number($row->phone_number), 'message'=>$message);
				}
				//print_r($messages); die();
				$return = json_decode($this->sms_client->send_multiple_messages($messages));
				if(isset($return->success) && $return->success===TRUE){
					$severity = 'success';
					$message = $return->message;
				} else {
					$severity = 'error';
					$message = (isset($return->message) ? $return->message : 'Error found while sending messages.');
				}
				print_r($return);
			//}
			//foreach ($result)
		}
		public function sms_assessment ($academic_term=NULL){
			if (is_null($academic_term)){
				die("Yo, something wrong wid your face. Let me kick it.");
			}
			$this->load->model('sms_model');
			$template = "HNUSMS: [[idno]], your assessment for [term] is computed at P [assessment]. Login to your mis account for details.";
			$messages = $this->sms_model->assessment_messages ($template, $academic_term);
			$this->load->library('sms_client', $this->config->item('sms_api_config'));
			
			$return = json_decode($this->sms_client->send_multiple_messages($messages));
			if(isset($return->success) && $return->success===TRUE){
				echo "Successfully sent Assessment for this term";
			} else {
				echo isset($return->message) ? $return->message : 'Error found while sending messages.';
			}
		}
		
		/*
		 * temporary method
		 */
		function basic_ed_fix() {
				
			$this->load->model('basic_ed/Reports_Model');
				
			$this->Reports_Model->basic_ed_fix();
		
		}	

		/*
		 * temporary method
		 */
		function fix_histories() {
		
			$this->load->model('basic_ed/Reports_Model');
		
			$this->Reports_Model->fix_histories();
		
		}
		
		/*
		 * temporary method
		 */
		function shs_fix() {
		
			$this->load->library('shs/shs_fix_lib');
		
			$this->shs_fix_lib->fix_course_offerings();
		
		}

		/*
		 * temporary method
		 */
		function bed_fix() {
		
			$this->load->library('shs/shs_fix_lib');
		
			$this->shs_fix_lib->fix_bed_histories();
		
		}

		function random_ids(){
			$this->load->library('random_ids');
			$this->random_ids->index();
		}
		
		function undersized_class_466() {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size=15, $term=466);
		}

		function undersized_class20_466( $size=20, $term=466) {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size );
		}

		function undersized_class30_466( $size=30, $term=466) {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size );
		}

		function undersized_class40_466( $size=40, $term=466) {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size );
		}

		function undersized_class_467() {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size=15, $term=467);
		}

		function undersized_class20_467( $size=20, $term=467) {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size, $term );
		}

		function undersized_class30_467( $size=30, $term=467) {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size, $term );
		}

		function undersized_class40_467( $size=40, $term=467) {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size, $term );
		}

		function undersized_classAll_466( $size=0, $term=466) {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size, $term );
		}

		function undersized_classAll_467( $size=0, $term=467) {
			$this->load->library('undersized_class');
			$this->undersized_class->index( $size, $term );
		}

		/* Added by Toyet 7.27.2018 */
		function mistmatched_assessments_college($term=469) {
			$this->load->model('student_common_model');
			$this->load->model('teller/assessment_model');
			$this->load->model('hnumis/student_model');
			$this->load->model('accounts/accounts_model');
			$this->load->model('posting/posting_model');

			$academic_terms_id = 469;
			$program = 161; 
			$acad_program_groups_id = 12;
			$academic_years_id = 119;
			$yearlevel = 2;
			$history_data = array();

			$students_for_term = $this->student_common_model->search_by_program($program,$yearlevel);

			$eval_student = array();
			$mismatched = array();

			foreach($students_for_term as $enrollee){
				$history_data = $this->student_model->get_StudentHistory_id($enrollee->idno, $academic_terms_id);

				$tuition_fee = $this->student_model->getMyTuitionFee_Basic($history_data->id, $acad_program_groups_id, $academic_terms_id,$withdraw=FALSE);
				$paying_units = $this->student_model->getPayingUnitsBasic($enrollee->idno,$academic_terms_id);
				$lab_fees = $this->student_model->getMyLabFees($academic_years_id, $history_data->id);

				$supposed_assessment = $this->accounts_model->student_assessment($history_data->id);		
				$supposed_assessment_total = 0.00;
				foreach($supposed_assessment as $key){
					foreach($key as $value){
						if(!isset($key['Tuition Basic']))
							$supposed_assessment_total += $value;
					}
				}

				$supposed_assessment_total += $tuition_fee->tuition_fee + $lab_fees->rate;;

				$posted_assessment = $this->posting_model->getPostedAssessment($enrollee->idno,$history_data->id);

				$eval_student[] = array(
					'students_idno'=>$enrollee->idno,
					'name'=>$enrollee->fullname,
					'section'=>$enrollee->abbr,
					'posted_assessment'=>round($posted_assessment[0]->assessed_amount,2),
					'supposed_assessment'=>round($supposed_assessment_total,2),
				);

			}

			$this->content_lib->enqueue_body_content('developers/list_mismatched_assessments',
		                        array('students'=>$eval_student));
		   	$this->content_lib->content();
		}

		/* Added by Toyet 7.27.2018 */
		function mistmatched_assessments_bed($term=469) {
			$this->load->model('student_common_model');
			$this->load->model('financials/Payments_Model');
			$this->load->model('Academic_terms_model');
			$this->load->model('basic_ed/Basic_Ed_Enrollments_Model');
			$this->load->model('financials/Scholarship_Model');
			$this->load->model('accounts/Accounts_model');
			$this->load->model('hnumis/Student_model');
			$this->load->model('basic_ed/assessment_basic_ed_model');
			$this->load->model('basic_ed/basic_ed_sections_model');
			$this->load->model('hnumis/academicyears_model');
			$this->load->model('hnumis/faculty_model');
			$this->load->model('basic_ed/Reports_Model');

			$academic_terms_id = 469;
			$academic_years_id = 119;
			$levels_array = array( 1, 2, 3, 4, 5, 11 );

			$eval_student = array();
			$mismatched = array();
			$course_prefix = "";

			foreach($levels_array as $row => $level):

				switch ($level) {
					case 1:
						$year_array = array( 1 );
						$course_prefix = "Kinder ";
						break;
					case 2:
						$year_array = array( 1 );
						$course_prefix = "Kinder ";
						break;
					case 3:
						$year_array = array( 1, 2, 3, 4, 5, 6 );
						$course_prefix = "Grade ";
						break;
					case 4:
						$year_array = array( 7, 8, 9, 10 );
						$course_prefix = "HS ";
						break;
					case 5:
						$year_array = array( 7, 8, 9, 10 );
						$course_prefix = "HS ";
						break;
					case 11:
						$year_array = array( 1 );
						$course_prefix = "Pre-School ";
						break;
				}

				foreach($year_array as $row => $year):

					$students_for_term = $this->Basic_Ed_Enrollments_Model->list_enrollees($level, $year, $section=NULL, $academic_years_id);
					log_message("INFO",print_r($course_prefix,true));
					if(count($students_for_term)>0):
						foreach($students_for_term as $enrollee){
							$basic_ed_fees = $this->assessment_basic_ed_model->ListBasic_Ed_Fees($level, $academic_years_id, $year);
							$tuition_basic = $this->assessment_basic_ed_model->getTuitionRateBasic($level, $academic_years_id, $year);
							$post_status   = $this->assessment_basic_ed_model->CheckIfPosted($enrollee->hist_id);
							if($post_status) {
								$posted_assessment = $this->assessment_basic_ed_model->getPostedAssessment_BEd($enrollee->hist_id);
							} else {
								$posted_assessment[0]->assessed_amount = 0.00;
							}
							//log_message("INFO","".print_r($tuition_basic,true));
							//log_message("INFO","BASIC ED FEES => ".print_r($basic_ed_fees,true));
							$misc_fees = 0.00;
							foreach($basic_ed_fees as $key => $val){
								//log_message("INFO",print_r($key,true));
								//log_message("INFO",print_r($val,true));
								$misc_fees += $val->rate;
							}

							if(round($posted_assessment[0]->assessed_amount,2)<>round($tuition_basic->rate+$misc_fees,2)){
								$eval_student[] = array(
									'students_idno'=>$enrollee->students_idno,
									'name'=>$enrollee->name,
									'course_year'=>$course_prefix.(string)$year,
									'section'=>$enrollee->section,
									'posted_assessment'=>round($posted_assessment[0]->assessed_amount,2),
									'supposed_assessment'=>round($tuition_basic->rate+$misc_fees,2),
								);
							}
						}
					endif;
				endforeach;
			endforeach;

			$this->content_lib->enqueue_body_content('developers/list_mismatched_assessments',
		                        array('students'=>$eval_student,
		                              'list_title'=>"BASIC EDUCATION" )
		                    );
		   	$this->content_lib->content();
		}

}

?>
