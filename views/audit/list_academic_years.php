<div style="float:left; width:auto;">
<form method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="2" name="step" />
	
	
<?php echo validation_errors();   ?>

	<div class="formSep">
		<h3>View Withdrawal Schedules </h3>
	</div> 

		<fieldset>
			<div class="control-group formSep">
				<label for="" class="control-label">Academic Year: </label>
					<div class="controls">
						<select id="academic_yr_id" name="academic_yr_id">
							<!--  <option value="">-- Select Academic Term --</option>  -->
          	<?php
				foreach($academic_years AS $academic_year) {
					print("<option value=".$academic_year->id.">".$academic_year->sy."</option>");	
				}
			?>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">Continue</button>
					</div>
				</div>	
		</fieldset>
	</form>           
</div>