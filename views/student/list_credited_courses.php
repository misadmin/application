 
<table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class = "head">
  <tr class="head">
  <td colspan="3" class="head" style="text-align:left; padding:10px;">
 CREDITED COURSES</td>
  </tr>
</table>	

<table class = "table table-striped table-bordered">
	<thead>
		<tr>
			<th width="5%">Catalog No.</th>
			<th width="5%">Credit Units</th>
			<th width="5%">Grade</th>
			<th width="5%">Taken in HNU</th>
			<th width="15%">Notes</th>
		</tr>
	</thead>
	<tbody> 
<?php if($credited_courses){ ?>
<?php foreach ($credited_courses as $creditedCourse): ?>
		<tr>
			<td>  <a class="edit_credited_course" href="<?php print($creditedCourse->id);?>" > 
			       <img src="<?php print(base_url('assets/img/icon-recycle.gif'));?>" /> </a> <?php echo $creditedCourse->course_code; ?>
        		  
		     </td>
			<td><?php echo $creditedCourse->credit_units; ?></td>
			<td><?php echo $creditedCourse->grade; ?></td>
			<?php if($creditedCourse->taken_here == 'Y'){?>
				<td><?php print('YES'); ?></td>
			<?php }else{ ?>
			 	<td><?php print('NO'); ?></td>
			<?php } ?>	
			<td><?php echo $creditedCourse->notes; ?></td>
		</tr>
<?php endforeach; ?>
<?php }else{ ?>
		<tr>
			<td colspan="6">No credited courses</td>
		</tr>
<?php } ?>
	</tbody>
</table>

<form id="editC" method="post" >
	<?php echo $this->common->hidden_input_nonce(); ?>
		<input type="hidden" name="action" value="remove_credited_course" />
		
	</form>			

<script>
$('.edit_credited_course').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to remove credited course?");

	if (confirmed){
		var credit_course_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'credit_course_id',
		    value: credit_course_id,
		}).appendTo('#editC');
		$('#editC').submit();
	}		
});
</script>
