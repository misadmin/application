<?php

	class year_level_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		
		/*
		 * @added: 30/01/2018
		 * @author: genes
		 */
		public function level_management() {
		
			$this->CI->load->model('hnumis/bed/year_level_model');
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_levels');
			
			switch ($action) {
				case 'list_levels':
		
					$levels = $this->CI->year_level_model->ListLevels();
					
					$data = array(
									"levels"=>$levels,
								);
					
					break;
					
			}
				
			$this->CI->content_lib->enqueue_body_content('bed/levels_management',$data);
			$this->CI->content_lib->content();
				
		}
		
		
		public function year_level_management() {
		
			$this->CI->load->model('hnumis/bed/year_level_model');
			
			$data['levels']   = $this->CI->year_level_model->ListLevels();

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_yr_levels');
			
			switch ($action) {
				case 'list_yr_levels':
		
					$data['yr_levels']  = $this->CI->year_level_model->ListYearLevels('All');
					$data['level_id'] = 0;

					break;
					
				case 'add_yr_level':

					$data['acad_program_groups_id'] = $this->CI->input->post('level_id');
					$data['abbreviation'] 			= $this->CI->input->post('abbreviation').$this->CI->input->post('year_level');
					$data['description']  			= $this->CI->input->post('description');
					$data['year_level'] 			= $this->CI->input->post('year_level');
					$data['max_yr_level'] 			= $this->CI->input->post('num_year');
					$data['colleges_id']  			= 14;
					$data['departments_id']  		= 0;
					$data['status']      			= 'O';
					$data['inserted_by']  			= $this->CI->session->userdata('empno');

					if ($this->CI->year_level_model->AddYearLevel($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['yr_levels']  = $this->CI->year_level_model->ListYearLevels($this->CI->input->post('nav_level_id'));
					$data['level_id'] 	= $this->CI->input->post('nav_level_id');
					
					$my_return['output'] = $this->CI->load->view('bed/levels/year_level_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_yr_level':

					$data['id']  			        = $this->CI->input->post('yr_level_id');
					$data['year_level']  			= $this->CI->input->post('year_level');
					$data['abbreviation'] 			= $this->CI->input->post('abbreviation').$this->CI->input->post('year_level');
					$data['description']  			= $this->CI->input->post('description');
					$data['update_by'] 				= $this->CI->session->userdata('empno');

					if ($this->CI->year_level_model->UpdateYearLevel($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['yr_levels']  = $this->CI->year_level_model->ListYearLevels($this->CI->input->post('nav_level_id'));
					$data['level_id'] 	= $this->CI->input->post('nav_level_id');
					
					$my_return['output'] = $this->CI->load->view('bed/levels/year_level_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_status':

					$data['id']        = $this->CI->input->post('yr_level_id');
					$data['status']    = $this->CI->input->post('status');
					$data['level_id']  = $this->CI->input->post('level_id');
					$data['update_by'] = $this->CI->session->userdata('empno');
					
					if ($this->CI->year_level_model->UpdateYrLevelStatus($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['yr_levels']  = $this->CI->year_level_model->ListYearLevels($this->CI->input->post('level_id'));
					$data['level_id'] 	= $this->CI->input->post('level_id');
					
					$my_return['output'] = $this->CI->load->view('bed/levels/year_level_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'delete_yr_level':

					$this->CI->year_level_model->DeleteYearLevel($this->CI->input->post('yr_level_id'));
					
					$data['level_id'] = $this->CI->input->post('level_id');

					if (!$this->CI->input->post('level_id')) {
						$data['yr_levels']  = $this->CI->year_level_model->ListYearLevels('All');
					} else {
						$data['yr_levels']  = $this->CI->year_level_model->ListYearLevels($this->CI->input->post('level_id'));
					}
					
					
					$my_return['output'] = $this->CI->load->view('bed/levels/year_level_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'change_level':

					$data['level_id'] = $this->CI->input->post('level_id');
					
					if (!$this->CI->input->post('level_id')) {
						$data['yr_levels']  = $this->CI->year_level_model->ListYearLevels('All');
					} else {
						$data['yr_levels']  = $this->CI->year_level_model->ListYearLevels($this->CI->input->post('level_id'));
					}

					$my_return['output'] = $this->CI->load->view('bed/levels/year_level_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
			}
				
			$this->CI->content_lib->enqueue_body_content('bed/year_level_management',$data);
			$this->CI->content_lib->content();
				
		}

				
	}
	
?>