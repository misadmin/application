<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

<form action="<?php echo site_url('accounts/add_new_other_tuition_fee_item');?>" method="post">
  <?php echo validation_errors(); 
  ?>
  <input type="hidden" name="step" value="2" />
  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" style="width:50%; margin-top:35px;" class="head1">
      <tr class="head">
        <td colspan="3" class="head"> ADD NEW OTHER TUITION FEE ITEM </td>
      </tr>
      
	  <tr>
        <td colspan="3"><table border="0" cellpadding="2" cellspacing="0" class="inside">
          <tr>
            <td width="199" align="left" valign="middle">Existing Items </td>
            <td width="15" align="left" valign="middle">:</td>
            <td width="438" align="left" valign="middle"><select name="other_tuition_fee_item_id" size="1" class="form" id="other_tuition_fee_item_id">
             <?php
			 		foreach($other_tuition_fee_items AS $other_tuition) {
							print("<option value=".$other_tuition->other_tuition_fee_item_id.">".$other_tuition->group_name."</option>");	
					}
						
				?>
                        </select></td>
          </tr>
		  <tr>
            <td>New Item Name </td>
            <td>:</td>
            <td><input name="item_name" type="text" id="item_name" size="10" maxlength="50" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="button2" id="button2" value="Save New Other Tuition Fee Item!"  class="btn btn-success" /></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</form>