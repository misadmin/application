<style>
	#container{width:100%; }
	#left{float:left;}
	#center{text-align:center;}
	#right{float:right;}
</style>

<div id="show_school_days_management_page" >
	<h2 class="heading">School Days Management</h2>
	<div id="rounded_header" style="width:37%; font-size:12px; margin-top:15px; padding:0px;">
		<div id="container">
			<div id="left" >
				<div id="show_error_msg" style="padding-left:10px; padding-top:10px; color:#F92424; font-weight:bold; font-size:14px;">
					
				</div>
			</div>
			<div id="right">
				<div id="show_add_icon" style="float:left; padding-right:8px; margin-top:10px;">
				</div>
				<div style="float:left; padding-top:4px; padding-right:4px; font-size:14px;" >
					<a href="#" class="btn btn-success add_school_day" >
						<i class="icon-plus-sign" style="margin-top:2px;"></i> New School Day
					</a>
				</div>
					<div style="float:left; padding-top:5px;" class="b2">
						<select name="term_id" class="form-control change_select" style="width:auto; height:auto;" id="select2">
							<?php 
								if ($terms) {
									foreach ($terms AS $term) {
							?>		
										<option value="<?php print($term->id); ?>" 
											end_year="<?php print($term->end_year); ?>"
											term_text="<?php print($term->term." ".$term->sy); ?>"
											term="<?php print($term->term); ?>"
											<?php if ($term_id == $term->id) print("selected"); ?>>
											<?php print($term->term." ".$term->sy); ?>
										</option>
							<?php
									}
								}
							?>
						</select>
					</div>
				
			</div>
		</div>
	</div>

	
	<div style="width:38%; overflow:auto; margin-top:15px; padding-left:5px;" id="show_extracted_items" >
		<?php
			$data = array(
					"school_days"=>$school_days,
			);
			
			$this->load->view('shs/attendance/list_school_days',$data);			
		?>
	</div>
</div>



<script>
	$('.add_school_day').click(function(event){
		
		var element2 = $("option:selected", "#select2");
		$('#term_id_modal').val(element2.val());
		$('#term_text_modal').val(element2.attr('term_text'));

		var school_term = element2.attr('term');
		var end_year    = element2.attr('end_year');

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/school_days'); ?>",
			data: {"school_term": school_term,
					"end_year": end_year,
					"action": "list_months" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#month_modal').html(response.output); 
				
				$('#modal_new_school_day').modal('show');
				
			}
		});

		
	});
</script>				


	<div id="modal_new_school_day" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="add_school_day_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">New School Day</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="term_id_modal"></div>
						<?php
							$this->load->view('shs/attendance/modal_view/new_school_day_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add School Day!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<script>
	$('#add_school_day_form').submit(function(e) {
		e.preventDefault();
		
		var term_id      = $('#term_id_modal').val();
		var num_days     = $('#num_days_modal').val();
		var school_month = $('#school_month').val();

		$('#modal_new_school_day').modal('hide');
		
		$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/school_days'); ?>",
			data: {"term_id": term_id,
					"num_days": num_days,
					"school_month": school_month,
					"action": "add_school_day" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_items').html(response.output); 
				
				$('#show_add_icon').html('');
				
			}
		});
		
	});
</script>				


<script>
	$(document).ready(function(){
		
		$('.change_select').bind('change', function(){
			
			var element2    = $("option:selected", "#select2");
			var term_id     = element2.val();

			$('#show_add_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/school_days'); ?>",
				data: {"term_id": term_id,
						"action": "change_select" },
				dataType: 'json',
				success: function(response) {													

					$('#show_extracted_items').html(response.output); 

					$('#show_add_icon').html("")
										
				}
			});

		});
	});
</script> 

<!--This script forces the select to go back to default once Back button of browser is pressed-->
<script>
$(document).ready(function () {
    $("select").each(function () {
        $(this).val($(this).find('option[selected]').val());
    });
})
</script>