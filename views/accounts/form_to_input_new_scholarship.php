<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

<form action="<?php echo site_url('accounts/new_scholarship');?>" method="post">
  <?php echo validation_errors(); 
  ?>
  <input type="hidden" name="step" value="2" />
  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" style="width:50%; margin-top:35px;" class="head1">
      <tr class="head">
        <td colspan="3" class="head"> ADD NEW SCHOLARSHIP ITEM </td>
      </tr>
      
	  <tr>
        <td colspan="3"><table border="0" cellpadding="2" cellspacing="0" class="inside">
		    
		   <tr>
            <td width="102">Scholarship Code </td>
            <td width="5">:</td>
            <td width="254"><input name="scholarship_code" type="text" id="scholarship_code" size="10" maxlength="50" /></td>
          </tr>
		  <tr>
            <td>Scholarship</td>
            <td>:</td>
            <td><input name="scholarship" type="text" id="scholarship" size="10" maxlength="50" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="button2" id="button2" value="Save New Scholarship Item!"  class="btn btn-success" /></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</form>