<?php

class accts_prospectus_lib {
	
	/**
	 * Codeigniter Instance
	 * @var unknown_type
	 */
	protected $CI;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	
	
	/*
	 * @added: 9/26/14
	 * @author: genes
	 */
	public function view_prospectus($college_id, $prospectus_id=NULL) {
	
		$this->CI->load->model('hnumis/college_model');
		$this->CI->load->model('hnumis/prospectus_model');
		$this->CI->load->model('hnumis/programs_model');
		
		$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_prospectus');
		$colleges = $this->CI->college_model->ListColleges();
	
		switch ($action) {
			case 'list_prospectus':
	
				$prospectus = $this->CI->prospectus_model->ListProspectus($college_id);
				
				if ($prospectus_id) {
					$prospectusterms = $this->CI->prospectus_model->ListProspectusTermsWithCourses($prospectus_id);
				} else {
					$prospectusterms = $this->CI->prospectus_model->ListProspectusTermsWithCourses($prospectus[0]->id);
				}
				
				if ($prospectusterms) {
					foreach($prospectusterms AS $k=>$pterm) {
						$prereqs = $this->CI->prospectus_model->ListPrereq_Courses($pterm->id);
						$str_prereq = "";
						if ($prereqs) {
							$str_prereq = "";
							foreach ($prereqs AS $prereq) {
								$str_prereq = $str_prereq.$prereq->course_code.", ";
							}
							$str_prereq = substr_replace($str_prereq ,"",-2);
						}
						$prereqs = $this->CI->prospectus_model->ListPrereq_YrLevel($pterm->id);
						if ($prereqs) {
							$str_prereq = "";
							foreach ($prereqs AS $prereq) {
								$str_prereq = $str_prereq.$prereq->pre_req.", ";
							}
							$str_prereq = substr_replace($str_prereq ,"",-2);
						}
						$prospectusterms[$k]->pre_req = $str_prereq;
					}	
				}
				
				//print_r($prospectusterms); die();
				
				
				if ($prospectus_id) {
					$data = array(
							"colleges"=>$colleges,
							"selected_college"=>$college_id,
							"prospectuses"=>$prospectus,
							"selected_prospectus"=>$prospectus_id,
							"prospectusterms"=>$prospectusterms,
					);
				} else {
					$data = array(
							"colleges"=>$colleges,
							"selected_college"=>$college_id,
							"prospectuses"=>$prospectus,
							"selected_prospectus"=>$prospectus[0]->id,
							"prospectusterms"=>$prospectusterms,
					);
				}
					
				break;
	
			case 'display_selected_program':
				
				$prospectus = $this->CI->prospectus_model->ListProspectus($this->CI->input->post('college_id'));
				
				if ($this->CI->input->post('type') == 'prospectus') {
					$prospectusterms = $this->CI->prospectus_model->ListProspectusTermsWithCourses($this->CI->input->post('prospectus_id'));
				} else {
					$prospectusterms = $this->CI->prospectus_model->ListProspectusTermsWithCourses($prospectus[0]->id);
				}

				if ($prospectusterms) {
					foreach($prospectusterms AS $k=>$pterm) {
						$prereqs = $this->CI->prospectus_model->ListPrereq_Courses($pterm->id);
						$str_prereq = "";
						if ($prereqs) {
							$str_prereq = "";
							foreach ($prereqs AS $prereq) {
								$str_prereq = $str_prereq.$prereq->course_code.", ";
							}
							$str_prereq = substr_replace($str_prereq ,"",-2);
						}
						$prereqs = $this->CI->prospectus_model->ListPrereq_YrLevel($pterm->id);
						if ($prereqs) {
							$str_prereq = "";
							foreach ($prereqs AS $prereq) {
								$str_prereq = $str_prereq.$prereq->pre_req.", ";
							}
							$str_prereq = substr_replace($str_prereq ,"",-2);
						}
						$prospectusterms[$k]->pre_req = $str_prereq;
					}
				}
				//print_r($prospectusterms); die();
				
				$data = array(
						"colleges"=>$colleges,
						"selected_college"=>$this->CI->input->post('college_id'),
						"prospectuses"=>$prospectus,
						"selected_prospectus"=>$this->CI->input->post('prospectus_id'),
						"prospectusterms"=>$prospectusterms,
				);
					
				break;
				
			case 'download_to_pdf':
				
				$program = $this->CI->programs_model->getProgramByProspectusID($this->CI->input->post('prospectus_id'));
				$terms = json_decode($this->CI->input->post('prospectusterms'));

				$this->Generate_prospectus_pdf($terms,$program);
				
				break;
	
		}
			
		$this->CI->content_lib->enqueue_body_content('accounts/display_prospectus_payingunits',$data);
		$this->CI->content_lib->content();
			
	}
	
	
	/*
	 * @ADDED: 10/2/14
	 * @author: genes
	 */
	private function Generate_prospectus_pdf($terms,$program) {
		
		$this->CI->load->library('my_pdf');
		$this->CI->load->library('hnumis_pdf');
		$l = 4.5;

		//print(strlen('Field Study 2: Experiencing the Teaching- Learning Process')); die();
		$this->CI->hnumis_pdf->AliasNbPages();
		$this->CI->hnumis_pdf->set_HeaderTitle(STRTOUPPER($program->name));
		$this->CI->hnumis_pdf->AddPage('P','folio');
		$this->CI->hnumis_pdf->SetAutoPageBreak(TRUE,12);
		$this->CI->hnumis_pdf->set_with_prospectus_header(TRUE);
		
		$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->CI->hnumis_pdf->SetDrawColor(165,165,165);
			
		$this->CI->hnumis_pdf->SetFont('times','B',11);
		$this->CI->hnumis_pdf->SetTextColor(10,10,10);
		$this->CI->hnumis_pdf->SetFillColor(190,190,190);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->Ln();
		
		$this->CI->hnumis_pdf->Cell(200,$l,STRTOUPPER($program->description),0,1,'C',false);
		$this->CI->hnumis_pdf->Cell(200,$l,'Effective Year - '.$program->effective_year,0,1,'C',false);
		$this->CI->hnumis_pdf->Ln();
		
		if ($terms) {
			$pros_term = 0;
			$max_bracket = "";
			$with_CWTS11   = FALSE;
			$with_CWTS12   = FALSE;
			$with_EngPlus  = FALSE;
			$notes_CWTS11  = "Students may take MS 11 (Military Science 11) instead of CWTS 11";
			$notes_CWTS12  = "Students who took up MS 11 may take MS 12(Military Science 12) instead of CWTS 12";
			$notes_EngPlus = "Students who did not pass the Placement Test are required to take first ENGL PLUS instead of English  1B";
				
			foreach($terms AS $pterm) {

				$this->CI->hnumis_pdf->set_prospectus_sy($pterm->term.' '.$pterm->y_level);
				
				if ((strlen($pterm->course_code) > 14) OR (strlen($pterm->descriptive_title) > 59) OR (strlen($pterm->pre_req) > 27))  {
					$two_liner = TRUE;
				} else {
					$two_liner = FALSE;
				}
				
				if ($pros_term != $pterm->prospectus_terms_id) {
					
					$pros_term = $pterm->prospectus_terms_id;
						
					$this->CI->hnumis_pdf->set_next_term(TRUE);
						
					if ($max_bracket) {
						$this->CI->hnumis_pdf->SetFont('times','B',9.5);
						$this->CI->hnumis_pdf->Cell(23,$l,'',1,0,'L',TRUE);
						$this->CI->hnumis_pdf->Cell(85,$l,'TOTAL:',1,0,'R',TRUE);
						$this->CI->hnumis_pdf->Cell(22,$l,$max_bracket,1,0,'L',TRUE);
						$this->CI->hnumis_pdf->Cell(13,$l,'',1,0,'C',TRUE);
						$this->CI->hnumis_pdf->Cell(50,$l,'',1,1,'L',TRUE);
						$this->CI->hnumis_pdf->SetFont('times','BI',9.5);
						$max_bracket="";
					}
						
					if ($with_CWTS11) {
						$this->CI->hnumis_pdf->SetX(20);
						$this->CI->hnumis_pdf->Cell(50,4,$notes_CWTS11,0,1,'L',FALSE);
						$with_CWTS11 = FALSE;
					}
					if ($with_CWTS12) {
						$this->CI->hnumis_pdf->SetX(20);
						$this->CI->hnumis_pdf->Cell(50,4,$notes_CWTS12,0,1,'L',FALSE);
						$with_CWTS12 = FALSE;
					}
					if ($with_EngPlus) {
						$this->CI->hnumis_pdf->SetX(20);
						$this->CI->hnumis_pdf->Cell(50,4,$notes_EngPlus,0,1,'L',FALSE);
						$with_EngPlus = FALSE;
					}
					
					$this->CI->hnumis_pdf->Ln();
						
					if ($this->CI->hnumis_pdf->GetY() > 307) { //check if Y is already at the bottom line 
						$this->CI->hnumis_pdf->AddPage('P','folio');
					}
						
					$this->CI->hnumis_pdf->SetFont('times','B',10);
					$this->CI->hnumis_pdf->Cell(20,$l,$pterm->term.' '.$pterm->y_level,0,1,'L',false);
					$this->CI->hnumis_pdf->SetFont('times','',9.5);
	
					$this->Prospectus_header();
				
					if ($two_liner)  {
						$this->Prospectus_content($pterm);
					} else {
						$this->CI->hnumis_pdf->Cell(23,$l,$pterm->course_code,1,0,'L',false);
						$this->CI->hnumis_pdf->Cell(85,$l,$pterm->descriptive_title,1,0,'L',false);
						$this->CI->hnumis_pdf->Cell(10,$l,$pterm->credit_units,1,0,'C',false);
						$this->CI->hnumis_pdf->Cell(12,$l,$pterm->cutoff_grade,1,0,'C',false);
						$this->CI->hnumis_pdf->Cell(13,$l,$pterm->num_retakes,1,0,'C',false);
						$this->CI->hnumis_pdf->Cell(50,$l,$pterm->pre_req,1,1,'L',false);
					}
					
				} else {
					
					$max_bracket = $pterm->max_credit_units.'/'.$pterm->max_bracket_units;
					$this->CI->hnumis_pdf->set_next_term(FALSE);

					if ($two_liner)  {
						$this->Prospectus_content($pterm);
					} else {
						$this->CI->hnumis_pdf->Cell(23,$l,$pterm->course_code,1,0,'L',false);
						$this->CI->hnumis_pdf->Cell(85,$l,$pterm->descriptive_title,1,0,'L',false);
						$this->CI->hnumis_pdf->Cell(10,$l,$pterm->credit_units,1,0,'C',false);
						$this->CI->hnumis_pdf->Cell(12,$l,$pterm->cutoff_grade,1,0,'C',false);
						$this->CI->hnumis_pdf->Cell(13,$l,$pterm->num_retakes,1,0,'C',false);
						$this->CI->hnumis_pdf->Cell(50,$l,$pterm->pre_req,1,1,'L',false);
					}

				}

				if ($pterm->courses_id == 1497) {
						$with_CWTS11 = TRUE;
				}
				if ($pterm->courses_id == 1506) {
					$with_CWTS12 = TRUE;
				}
				if ($pterm->courses_id == 1411) {
					$with_EngPlus = TRUE;
				}
					
						
				if ($this->CI->hnumis_pdf->GetY() > 308) {
					$this->CI->hnumis_pdf->AddPage('P','folio');
				}
				
			}
			
			$this->CI->hnumis_pdf->SetFont('times','B',9.5);
			$this->CI->hnumis_pdf->Cell(23,$l,'',1,0,'L',TRUE);
			$this->CI->hnumis_pdf->Cell(85,$l,'TOTAL:',1,0,'R',TRUE);
			$this->CI->hnumis_pdf->Cell(22,$l,$max_bracket,1,0,'L',TRUE);
			$this->CI->hnumis_pdf->Cell(13,$l,'',1,0,'C',TRUE);
			$this->CI->hnumis_pdf->Cell(50,$l,'',1,1,'L',TRUE);
			$this->CI->hnumis_pdf->SetFont('times','',9.5);
		}
			
		$this->CI->hnumis_pdf->Output();
		
	}
	
	
	
	private function Prospectus_header() {

		$this->CI->hnumis_pdf->MultiCell(23,8,'Course Code',1,'C',true);
		$this->CI->hnumis_pdf->SetXY(33,$this->CI->hnumis_pdf->GetY()-8);
		$this->CI->hnumis_pdf->MultiCell(85,8,'Descriptive Title',1,'C',true);
		$this->CI->hnumis_pdf->SetXY(118,$this->CI->hnumis_pdf->GetY()-8);
		$this->CI->hnumis_pdf->SetFont('times','',9);
		$this->CI->hnumis_pdf->MultiCell(10,4,'Credit Units',1,'C',true);
		$this->CI->hnumis_pdf->SetXY(128,$this->CI->hnumis_pdf->GetY()-8);
		$this->CI->hnumis_pdf->MultiCell(12,4,'Cut-off Grade',1,'C',true);
		$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
		$this->CI->hnumis_pdf->MultiCell(13,4,'No. of Retakes',1,'C',true);
		$this->CI->hnumis_pdf->SetFont('times','',9.5);
		$this->CI->hnumis_pdf->SetXY(153,$this->CI->hnumis_pdf->GetY()-8);
		$this->CI->hnumis_pdf->MultiCell(50,8,'Pre-requisites',1,'C',true);
		
		return;
	}
	
	
	private function Prospectus_content($pterm) {
		if ((strlen($pterm->course_code) > 14) AND (strlen($pterm->descriptive_title) > 59) AND (strlen($pterm->pre_req) > 27)) {
			$this->CI->hnumis_pdf->MultiCell(23,4,$pterm->course_code,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(33,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(85,4,$pterm->descriptive_title,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(118,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(10,8,$pterm->credit_units,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(128,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(12,8,$pterm->cutoff_grade,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(13,8,$pterm->num_retakes,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(153,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(50,4,$pterm->pre_req,1,'L',FALSE);
		} elseif ((strlen($pterm->course_code) > 14) AND (strlen($pterm->descriptive_title) > 59) AND (strlen($pterm->pre_req) <= 27)) {
			$this->CI->hnumis_pdf->MultiCell(23,4,$pterm->course_code,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(33,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(85,4,$pterm->descriptive_title,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(118,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(10,8,$pterm->credit_units,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(128,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(12,8,$pterm->cutoff_grade,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(13,8,$pterm->num_retakes,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(153,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(50,8,$pterm->pre_req,1,'L',FALSE);
		} elseif ((strlen($pterm->course_code) > 14) AND (strlen($pterm->descriptive_title) <= 59) AND (strlen($pterm->pre_req) > 27)) {
			$this->CI->hnumis_pdf->MultiCell(23,4,$pterm->course_code,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(33,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(85,8,$pterm->descriptive_title,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(118,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(10,8,$pterm->credit_units,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(128,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(12,8,$pterm->cutoff_grade,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(13,8,$pterm->num_retakes,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(153,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(50,4,$pterm->pre_req,1,'L',FALSE);
		} elseif ((strlen($pterm->course_code) > 14) AND (strlen($pterm->descriptive_title) <= 59) AND (strlen($pterm->pre_req) <= 27)) {
			$this->CI->hnumis_pdf->MultiCell(23,4,$pterm->course_code,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(33,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(85,8,$pterm->descriptive_title,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(118,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(10,8,$pterm->credit_units,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(128,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(12,8,$pterm->cutoff_grade,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(13,8,$pterm->num_retakes,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(153,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(50,8,$pterm->pre_req,1,'L',FALSE);
		} elseif ((strlen($pterm->course_code) <= 14) AND (strlen($pterm->descriptive_title) > 59) AND (strlen($pterm->pre_req) > 27)) {
			$this->CI->hnumis_pdf->MultiCell(23,8,$pterm->course_code,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(33,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(85,4,$pterm->descriptive_title,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(118,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(10,8,$pterm->credit_units,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(128,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(12,8,$pterm->cutoff_grade,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(13,8,$pterm->num_retakes,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(153,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(50,4,$pterm->pre_req,1,'L',FALSE);
		} elseif ((strlen($pterm->course_code) <= 14) AND (strlen($pterm->descriptive_title) > 59) AND (strlen($pterm->pre_req) <= 27)) {
			$this->CI->hnumis_pdf->MultiCell(23,8,$pterm->course_code,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(33,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(85,4,$pterm->descriptive_title,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(118,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(10,8,$pterm->credit_units,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(128,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(12,8,$pterm->cutoff_grade,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(13,8,$pterm->num_retakes,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(153,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(50,8,$pterm->pre_req,1,'L',FALSE);
		} elseif ((strlen($pterm->course_code) <= 14) AND (strlen($pterm->descriptive_title) <= 59) AND (strlen($pterm->pre_req) > 27)) {
			$this->CI->hnumis_pdf->MultiCell(23,8,$pterm->course_code,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(33,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(85,8,$pterm->descriptive_title,1,'L',FALSE);
			$this->CI->hnumis_pdf->SetXY(118,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(10,8,$pterm->credit_units,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(128,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(12,8,$pterm->cutoff_grade,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(140,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(13,8,$pterm->num_retakes,1,'C',FALSE);
			$this->CI->hnumis_pdf->SetXY(153,$this->CI->hnumis_pdf->GetY()-8);
			$this->CI->hnumis_pdf->MultiCell(50,4,$pterm->pre_req,1,'L',FALSE);
		}
		
		return;
	}
	
	
}