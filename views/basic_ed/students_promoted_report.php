<div style="margin-top:10px">
<h2 class="heading"> <?php print($basic_level." ".$yr_level);?> MASTERLIST (ENROLLED) <?php print("for SY ".$cur_yr."-".$cur_yr_end); ?> 

<a href="download" class="btn btn-link " id="download_list"><i class="icon-download-alt"></i> <strong>Download as an Excel File</strong></a> </h2>
  	

<table class="table table-striped table-bordered"; style="width:50%">
	<thead >
		<tr align="center">
			<th width="5%" class="head" style=" vertical-align:middle; text-align:center;">No.</th>
			<th width="6%" class="head" style="vertical-align:middle; text-align:center;">Student ID</th>
			<th width="30%" class="head" style="vertical-align:middle; text-align:center;">Name</th>
			<th width="20%" class="head" style="vertical-align:middle; text-align:center;">Section</th>
			<th width="20%" class="head" style="vertical-align:middle; text-align:center;">Gender</th>
		</tr>
	</thead>
	<tbody> 

	
	<?php 
		if($students) {
			$count = 0;
			foreach ($students as $student): ?>
			<tr>
				<?php $count++;?>
				<td><?php print($count); ?></td>
				<td><?php print($student->students_idno); ?></td>
				<td><?php print($student->neym); ?></td>
				<td><?php print($student->section_name); ?></td>
				<td><?php print($student->gender); ?></td>
			</tr>
<?php
			endforeach;
		} ?>
</tbody>
</table>

<form method="post" action="<?php echo site_url("{$role}/download_basicEd_enrolled_csv"); ?>" id="download_list_form">
			<input type="hidden" name="sy_id" value="<?php echo $cur_yr_id; ?>" />
			<input type="hidden" name="basic_level" value="<?php echo $basic_level; ?>" />
			<input type="hidden" name="yr_level" value="<?php echo $yr_level; ?>" />
			<input type="hidden" name="cur_yr" value="<?php echo $cur_yr; ?>" />
			<input type="hidden" name="level_id" value="<?php echo $level_id; ?>" />
			
</form>

<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
		
</div> 
