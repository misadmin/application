<div class="row-fluid">
	<div class="span12">
		<?php if(isset($message)):?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo urldecode($message); ?>
		</div>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid">
	<div class="span8"></div>
	<div class="span4">
		<form method="post" action="" id="grades_history_form">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<?php action_form('view_grades', FALSE); ?>
			<select name="history_id"  class="pull-right">
				<?php foreach($histories as $history): ?>
				<option value="<?php echo $history->id ?>" <?php if(isset($selected_history_id) && $history->id==$selected_history_id){echo " selected";} ?>><?php echo $history->term ?> - <?php echo $history->sy; ?></option>
				<?php endforeach; ?>
			</select>
		</form>
	</div>
</div>
<div class="row-fluid">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Catalog #</th>
				<th>Section</th>
				<th>Title</th>
				<th>Units</th>
				<th>Prelim</th>
				<th>Midterm</th>
				<th>Finals</th>
			</tr>
		</thead>
		<tbody>
		<?php if(is_array($grades) && count($grades) > 0): ?>
		<?php foreach($grades as $grade): ?>
			<tr>
				<td><?php echo $grade->course_code; ?></td>
				<td style="text-align:center"><?php echo $grade->section_code; ?></td>
				<td><?php echo $grade->descriptive_title; ?></td>
				<td style="text-align:center"><?php echo $grade->credit_units; ?></td>
				<td style="text-align:center"><?php echo $grade->prelim_grade; ?></td>
				<td style="text-align:center"><?php echo $grade->midterm_grade; ?></td>
				<td style="text-align:center"><?php echo $grade->finals_grade; ?></td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="4">No Courses Taken for this Semester</td>
			</tr>
		<?php endif; ?>
		</tbody>
	</table>
	
</div>
<div class="row-fluid" style="margin-top:20px">
	<div class="span12">
		<input type="button" class="btn btn-success" id="print_grade" value="Print Grades for this Semester" />
	</div>
</div>
<?php 
if(isset($print_student_grade)){
	echo $print_student_grade; 
}
?>
<script>
$().ready(function(){
	$('select[name="history_id"]').on('change', function(){
		$('#grades_history_form').submit();
	});
});
</script>