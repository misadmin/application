<style>
	table.balances 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
</style>


<h3 class="heading">List of Students with Total Withdrawals</h3>
<div class=span7>

<table id="withdrawals_t" class="table table-bordered table-striped balances">
<thead>
	<tr>
	<th width="10%"></th>
	<th width="15%">ID No.</th>
	<th width="30%">Student</th>
	<th width="30%">Program</th>
	<th width="15%">No. of Subjects</th>
	</tr>
</thead>

<?php
	if (count($rows) < 1) {
		echo '<td class="center" colspan=5>No record found!</td>';
	}else{
		$row_num = 0;
		foreach($rows as $row){
			$row_num++;
?>
			<tr >
			<td class="center"><?php echo $row_num .'.'; ?> </td>
			<td class="center"><a href="<?php echo site_url($this->session->userdata('role') . '/student') .'/'. $row->students_idno ?>">     	
				<?php echo $row->students_idno; ?>  </a></td>	
			<td class="left"><?php echo $row->student; ?></td>
			<td class="left"><?php echo $row->year_level ." " .  $row->abbreviation; ?></td>
			<td class="center"><?php echo $row->withdrawns; ?></td>
			</tr>		

<?php
		} 		
	}
?>

</table>

</div>



<script>
$(document).ready(function(){
	$("#withdrawals_t").stickyTableHeaders();
    $("#withdrawals_t").stickyTableHeaders('updateWidth');

	
});

</script>