<div style="text-align:center;">
<table cellpadding="2" cellspacing="0" class="table table-striped table-condensed table-hover" style="width:100%;">
 	<thead>
  	<tr style="font-weight:bold; background:#EAEAEA;">
    	<td width="8%" style="text-align:center; vertical-align:middle;">No.</td>
    	<td width="16%" style="text-align:center; vertical-align:middle;">ID No.</td>
    	<td width="48%" style="text-align:center;">Name</td>
    	<td width="6%" style="text-align:center;">Gender</td>
    	<td width="22%" style="text-align:center;">Course/Year</td>
    </tr>
	</thead>
	<tbody>
  <?php
		$cnt = 0;
		if ($students) {
			foreach($students AS $student) {
				$cnt++;
  ?>
  
  <tr>
    <td style="text-align:center;"><?php print($cnt); ?></td>
    <td style="text-align:center;"><?php print($student->idno); ?></td>
    <td style="text-align:left;"><?php print($student->neym); ?></td>
    <td style="text-align:center;"><?php print($student->gender); ?></td>
    <td style="text-align:left;"><?php print($student->abbreviation.' - '.$student->year_level); ?></td>
    </tr>
	<?php
			}
		}
	?> 
  </tbody>   
</table>
</div>
