
			<div style="padding:3px; overflow:auto;">
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:7%;">Yr Level ID</td>
								<td class="shs_header" style="width:20%;">Level</td>
								<td class="shs_header" style="width:13%;">Year Level</td>
								<td class="shs_header" style="width:40%;">Description</td>
								<td class="shs_header" style="width:10%;">Status</td>
								<td class="shs_header" style="width:5%;">Update</td>
								<td class="shs_header" style="width:5%;">Del.</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($yr_levels) {
									foreach($yr_levels AS $y_level) {
										if ($y_level->status == 'Offered') {
											$bg = "#FFFFFF";
										} else {
											$bg = "#FF9C9C";
										}
							?>
							<tr>
									<td style="text-align:center; background-color:<?php print($bg); ?>; "><?php print($y_level->id); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($y_level->track_name); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($y_level->abbreviation); ?></td>
									<td style="background-color:<?php print($bg); ?>; "><?php print($y_level->description); ?></td>
									<td style="text-align:center; background-color:<?php print($bg); ?>; ">
										<div style="float:left; padding-left:5px;">
											<?php print($y_level->status); ?>
										</div>
										<div>
											<a href="<?php print($y_level->id); ?>" 
												status="<?php print($y_level->status); ?>"
												description="<?php print($y_level->description); ?>"
												class="update_status" >
												<div id="show_edit_icon_<?php print($y_level->id); ?>" >
													<i class="icon-edit"></i>
												</div>
											</a>
										</div>
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>; ">
										<a href="<?php print($y_level->id); ?>" 
											acad_program_groups_id="<?php print($y_level->acad_program_groups_id); ?>"
											abbreviation="<?php print($y_level->abbreviation); ?>"
											description="<?php print($y_level->description); ?>"
											year_level="<?php print($y_level->year_level); ?>"											
											class="update_strand" >
											<div id="show_pencil_icon_<?php print($y_level->id); ?>" >
												<i class="icon-pencil"></i>
											</div>
										</a>
									</td>
									<td style="text-align:center; background-color:<?php print($bg); ?>; ">
										<a href="<?php print($y_level->id); ?>" class="delete_strand" >
											<div id="show_trash_icon_<?php print($y_level->id); ?>" >
												<i class="icon-trash"></i>
											</div>
										</a>
									</td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>

			

<!-- JQ to delete strand-->			
<script>
	$('.delete_strand').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to delete this year level?');

		if (confirmed){
			
			var level_id = $("select#level_id option:selected").attr('value');
			var yr_level_id = $(this).attr('href');

			$('#show_trash_icon_'+yr_level_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: '<?php echo site_url($this->uri->segment(1).'/year_level_management');?>',
				data: { level_id: level_id,
						yr_level_id: yr_level_id,
						action: 'delete_yr_level'},
				dataType: 'json',
				success: function(response) {
					$('#show_extracted_items').html(response.output); 

				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
		}

	});
</script>


<!-- JQ to update status-->
<script>
	$('.update_status').click(function(event){
		event.preventDefault();

		var level_id   	= $(this).attr('href');
		var description = $(this).attr('description');
		var status      = $(this).attr('status');
		
		$('#strand_id_status').val(level_id);
		$('#description_status').val(description);
		$('#status_status').val(status);
		
		$('#modal_update_status').modal('show');
	});
</script>				


	<div id="modal_update_status" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_status_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Status</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="strand_id_status"></div>
						<?php														
							$this->load->view('bed/levels/modal_view/update_status_form_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<!-- JQ to submit an update to a status -->
<script>
	$('#update_status_form').submit(function(e) {
		e.preventDefault();
		
		var yr_level_id = $('#strand_id_status').val();
		var level_id  	= "<?php print($level_id); ?>";

		var status 		= $("select#status_status option:selected").attr('shortcut');
	
		$('#modal_update_status').modal('hide');

		$('#show_edit_icon_'+yr_level_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/year_level_management'); ?>",
			data: {"yr_level_id": yr_level_id,
					"level_id": level_id,
					"status": status,
					"action": "update_status" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_items').html(response.output); 
				
				$('#show_edit_icon_'+yr_level_id).html("<i class='icon-edit'></i>")
				
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
			
		});
		
	});
</script>				



<script>
	$('.update_strand').click(function(event){
		event.preventDefault();

		var yr_level_id    	= $(this).attr('href');
		var level_id		= $(this).attr('acad_program_groups_id');
		var description  	= $(this).attr('description');
		var year_level	 	= $(this).attr('year_level');
		
		$('#track_id_update_modal').val(level_id);
		$('#strand_id_update').val(yr_level_id);
		$('#description_update_modal').val(description);
		$('#year_level_update').val(year_level);

		$('#modal_update_strand').modal('show');
				
	});
</script>				


	<div id="modal_update_strand" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="update_strand_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Status</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="strand_id_update"></div>
						<?php
							$data['levels'] = $levels;
							$this->load->view('bed/levels/modal_view/update_strand_form_modal', $data);
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<script>
	$('#update_strand_form').submit(function(e) {
		e.preventDefault();
		
		var nav_level_id 	= $("select#level_id option:selected").attr('value');
		var abbreviation	= $("select#track_id_update_modal option:selected").attr('level_abbreviation');
		var yr_level_id    	= $('#strand_id_update').val();
		var year_level 		= $('#year_level_update').val();
		var description  	= $('#description_update_modal').val();
	
		$('#modal_update_strand').modal('hide');

		$('#show_pencil_icon_'+yr_level_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/year_level_management'); ?>",
			data: {"yr_level_id": yr_level_id,
					"abbreviation": abbreviation,
					"description": description,
					"nav_level_id": nav_level_id,	
					"year_level": year_level,						
					"action": "update_yr_level" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_extracted_items').html(response.output); 
				
				$('#show_pencil_icon_'+yr_level_id).html("<i class='icon-pencil'></i>")
				
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
			
		});
		
	});
</script>				


