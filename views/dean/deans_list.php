<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}
</style>

<div style="background:#FFF; width:600px;" align="center">
<div style="width:100%; margin-bottom:40px;" align="center">	

<table border="0" cellspacing="0" class="table table-bordered table-hover" style="width:100%; padding:0px; margin:0px;">
	 <thead> 
		<tr class="head">
  			<th colspan="3" class="head" style="text-align:center; padding:10px;">  DEAN'S LIST (<?php print($term->term." ".$term->sy); ?>) </th>
  	   	</tr>
		<tr class="head">
  			<th colspan="3" class="head" style="text-align:center; padding:10px;"> <?php print($program->abbreviation." - ".$level); ?> </th>
  	   	</tr>
		<tr>
			<th width="2%" class="head" style="vertical-align:middle;">ID No.</th>
			<th width="20%" class="head" style="vertical-align:middle;">NAME</th>
			<th width="2%" class="head" style="vertical-align:middle;">AVERAGE</th>
		</tr>
	</thead>
			<?php 
				if ($candidates) {
					foreach($candidates as $candidate) { ?>
					<tr>
						<td style="text-align:center; vertical-align:middle; "><?php print($candidate['idno']); ?></td>
						<td style="text-align:left; vertical-align:middle; "><?php print($candidate['name']); ?></td>
						<td style="text-align:center; vertical-align:middle; "><?php print(number_format($candidate['average'],4,'.',',')); ?></td>
					</tr>
			<?php 	}
				} else { ?>
					<tr class="head">
  						<th colspan="3" class="head" style="text-align:center; padding:10px;"> No Dean's List Students.... </th>
  	   				</tr>
				<?php } ?>
</table>
</div>
</div>

