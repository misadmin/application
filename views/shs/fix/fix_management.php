
		<div id="show_list_of_offerings">
			<div style="float:left; padding:3px; overflow:auto; width:50%; height:550px;" >
					<table class="table table-hover table-bordered table-striped" >
						<thead>
							<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:15%;">Course<br>Offerings<br>ID</td>
								<td class="shs_header" style="width:10%;">Block<br>Sections<br>ID</td>
								<td class="shs_header" style="width:25%;">Schedule</td>
								<td class="shs_header" style="width:5%;">Schedule<br>Count</td>
								<td class="shs_header" style="width:15%;">Course<br>Code</td>
								<td class="shs_header" style="width:15%;">Abbreviation</td>
								<td class="shs_header" style="width:15%;">Section Name</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($course_offerings) {
									foreach($course_offerings AS $offering) {
							?>
							<tr>
									<td style="text-align:center;">
										<a href="<?php print($offering->block_sections_id); ?>" 
											academic_terms_id="<?php print($offering->academic_terms_id); ?>"
											courses_id="<?php print($offering->courses_id); ?>"
											course_offerings_id="<?php print($offering->course_offerings_id); ?>"
											class="show_offerings" >
											<div style="float:left; width:80%;">
												<?php print($offering->course_offerings_id); ?>
											</div>
											<div style="float:left; width:20%;" id="show_pencil_icon_<?php print($offering->block_sections_id); ?>" >
												
											</div>
										</a>
									</td>
									<td style="text-align:center;"><?php print($offering->block_sections_id); ?></td>
									<td><?php print(nl2br($offering->sched)); ?></td>
									<td style="text-align:center;"><?php print($offering->cnt); ?></td>
									<td style="text-align:center;"><?php print($offering->course_code); ?></td>
									<td style="text-align:center;"><?php print($offering->abbreviation); ?></td>
									<td style="text-align:center;"><?php print($offering->section_name); ?></td>
							</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
			</div>

			<div style="float:right; width:48%;" id="show_same_offerings">
			
			
			</div>
		</div>
		
			
<script>
	$('.show_offerings').click(function(event){
		event.preventDefault();

		var block_sections_id   = $(this).attr('href');
		var academic_terms_id   = $(this).attr('academic_terms_id');
		var courses_id          = $(this).attr('courses_id');
		var course_offerings_id = $(this).attr('course_offerings_id');

		$('#show_pencil_icon_'+block_sections_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/shs_fix'); ?>",
			data: {"block_sections_id": block_sections_id,
					"academic_terms_id": academic_terms_id,
					"courses_id": courses_id,
					"course_offerings_id": course_offerings_id,
					"action": "extract_same_offerings" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_same_offerings').html(response.output); 
				
				$('#show_pencil_icon_'+block_sections_id).html("")
				
			}
		});
		
	});
</script>					