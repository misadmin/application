  <?php echo validation_errors(); 
  ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; background:#FFF; ">
  <table align="left" cellpadding="2" cellspacing="0" style="width:35%; border:solid; border-width:1px; border-color:#060; margin-top:10px; margin-left:20px;">
<tr>
        	<td width="199">Academic Terms</td>
        	<td width="15">:</td>
            <td width="438"><?php print($academic_terms->term." ".$academic_terms->sy); ?></td>
        </tr>
<tr>
  <td>Course [Section]</td>
  <td>:</td>
  <td><?php print($offerings->course_code."[".$offerings->section_code."]"); ?></td>
</tr>
<tr>
  <td align="left" valign="top">Schedule</td>
  <td align="left" valign="top">:</td>
  <td><?php
  		foreach($offering_slot AS $slot) {
			print($slot->tym." ".$slot->days_day_code." [".$slot->room_no."]");
			print("<br>");
		}
  
  ?></td>
</tr>
<tr>
  <td align="left" valign="top">Faculty</td>
  <td align="left" valign="top">:</td>
  <td><?php print("[".$offerings->employees_empno."] ".$offerings->emp_name); ?></td>
</tr>
    </table>
  <br />
  <br />
<br />
  <table border="0" cellspacing="0" style="border:solid; border-width:1px; border-color:#666; width:70%; padding:20px; margin-left:20px;">
    <tr style="height:30px; background:#060; color:#FFF; font-weight:bold;">
      <td width="27%" align="center">ID No.</td>
      <td width="30%">Name</td>
      <td width="18%">Course</td>
      <td width="25%">Year</td>
    </tr>
    <?php
		if ($enrollees) {
			foreach ($enrollees AS $enrollee) {
	?>
    <tr>
      <td align="left" valign="top"><?php print($enrollee->idno); ?></td>
      <td><?php print($enrollee->neym); ?></td>
      <td align="left" valign="top"><?php print($enrollee->abbreviation); ?></td>
      <td align="left" valign="top"><?php print($enrollee->year_level); ?></td>
    </tr>
    <?php
			}
		}
	?>
  </table>
  </div>
