<?php $this->load->helper('inflector') ?>
<ul>
<?php foreach ($files as $file): ?>
<li>
	<a href="" class="crh" data-type="<?php echo $type ?>" data-name="<?php echo $file ?>">
		<?php echo humanize($file) ?>
	</a>
</li>
<?php endforeach; ?>
</ul>