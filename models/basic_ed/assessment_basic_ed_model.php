<?php

	class Assessment_Basic_Ed_Model extends MY_Model {
	
  		function __construct() {
        	parent::__construct();
			
   		}

		/*
		function CheckStudentHasAssessment($student_histories_id) {
			$result = NULL;
				
			$q1 = "SELECT
						a.id
					FROM
						assessments AS a
					WHERE
						a.student_histories_id = {$this->db->escape($student_histories_id)} ";		
		
			$query = $this->db->query($q1);
				
			if($query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
		
		}*/

   		
   		/**
   		 * This function adds a new assessment to the assessment table for Basic Ed
   		 * @param array $assess_data (contains assessment detials for basic ed)
   		 * @return unknown
   		 * modified by Tatskie to align with static a/r on Aug 26, 2014
   		 */
		function AddAssessment($assess_data) {
			$query = "
				INSERT INTO assessments_bed (	
						basic_ed_histories_id, 
						assessed_amount, 
						transaction_date,
						employees_empno,
						academic_years_id,
						levels_id,
						year_level)
					VALUES (
						{$this->db->escape($assess_data['basic_ed_histories_id'])},
						{$this->db->escape($assess_data['assessed_amount'])},
						NOW(),
						{$this->db->escape($assess_data['employees_empno'])},
						{$this->db->escape($assess_data['academic_years_id'])},
						{$this->db->escape($assess_data['levels_id'])},
						{$this->db->escape($assess_data['year_level'])}			
					)
			";
							
			//print($query); die();
			$this->db->query($query);
			$id = @mysql_insert_id();			
			return $id;
		
		}
		

	//Added: 4/13/2013 by genes
	/**
	 * This function retrieves a summary of assessment for Basic Ed
	 * @param smallint $academic_years_id (represents the academic to which the assessment applies.)
	 * @return NULL
	 * 
	 */
	function ListAssessmentSummary($academic_years_id, $post_status=NULL) {
			
		$result = null;
		$this->load->model('academic_terms_model');
			
	//	if ($post_status == 'Y') {    //   commented out 8/25/2016 RA 
			
			/*$q1 = "SELECT
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=1
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.status='active'
								AND bb.basic_ed_sections_id NOT IN (166,175)
								AND dd.id = d.id)
						AS assess1, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=2
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess2, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=3
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess3, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=4
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess4, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=5
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess5, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=6
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess6, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=7
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess7, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=8
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess8, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=9
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess9, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=10
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess10, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=11
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess11, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								basic_ed_histories AS bb,
								assessments_bed AS cc,
								levels AS dd
							WHERE
								bb.id = cc.basic_ed_histories_id
								AND bb.levels_id = dd.id
								AND bb.yr_level=12
								AND bb.academic_years_id = {$this->db->escape($academic_years_id)}
								AND bb.levels_id = d.id)
						AS assess12, 
						d.level
					FROM
						basic_ed_histories AS b,
						levels AS d
					WHERE 
						b.levels_id = d.id
						AND b.academic_years_id = {$this->db->escape($academic_years_id)}
						AND d.id IN (2,3,4,5)
					GROUP BY
						d.id";
					*/

			$q1 = "SELECT
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=1
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
							) AS assess1, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=2
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
							) AS assess2, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=3
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
							) AS assess3, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=4
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
							) AS assess4, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=5
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
															) AS assess5, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=6
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
															) AS assess6, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=7
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
															) AS assess7, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=8
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
															) AS assess8, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=9
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
															) AS assess9, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=10
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
															) AS assess10, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=11
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
															) AS assess11, 
						(SELECT 
							if (SUM(cc.assessed_amount)IS NULL,0,SUM(cc.assessed_amount))
							FROM
								assessments_bed AS cc
							WHERE
								cc.basic_ed_histories_id IN 
								(SELECT x.id
										FROM
											basic_ed_histories AS x
										WHERE
											x.yr_level=12
											AND x.levels_id=d.id
											AND x.status='active'
											AND x.basic_ed_sections_id NOT IN (166,175)	
											AND x.academic_years_id = {$this->db->escape($academic_years_id)})
														) AS assess12, 
						d.level
					FROM
						levels AS d
					WHERE 
						d.id IN (11,1,3,4,12)	
					ORDER BY	
						d.rank";
		// added level id = 12 - 8/25/2016 RA
		//print($q1); die();
/*		} else {

			$acad_terms = $this->academic_terms_model->ListAcademicTerms($academic_years_id);
		
			$q1 = "SELECT 
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=1
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess1,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=2
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess2,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=3
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess3,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=4
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess4,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=5
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess5,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=6
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess6,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=7
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess7,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=8
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess8,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=9
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess9,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=10
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess10,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=11
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess11,
					(SELECT 
						SUM(bb.rate)
					FROM
						basic_ed_histories AS aa,
						fees_schedule AS bb
					WHERE
						aa.levels_id = bb.levels_id
						AND aa.yr_level = bb.yr_level
						AND bb.yr_level=12
						AND bb.academic_terms_id = {$this->db->escape($acad_terms[0]->id)}
						AND bb.levels_id = d.id 
						AND aa.id NOT IN (
								SELECT basic_ed_histories_id
									FROM 
										assessments_bed )
						AND aa.status='active'
						AND aa.basic_ed_sections_id NOT IN (166,175)
						AND aa.academic_years_id = {$this->db->escape($academic_years_id)}
						)
					AS assess12,
					d.level
				FROM
					basic_ed_histories AS b,
					levels AS d
				WHERE 
					b.levels_id = d.id
					AND b.academic_years_id = {$this->db->escape($academic_years_id)}
					AND d.id IN (11,1,3,4,12)  											
				GROUP BY
					d.id 
				ORDER BY
					d.rank ";
			} 

			// added level id = 12 8/25/2016 RA	
*/
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->result();
			}  

			return $result;
		}


		function getTuitionRateBasic($levels_id, $acad_years_id, $yr_level){
		
			$result = 0;

			$q = "SELECT 
						a.rate, a.academic_terms_id
					FROM 
						fees_schedule as a, 
						academic_terms AS b 
					WHERE
						a.levels_id = {$this->db->escape($levels_id)}
						AND a.academic_terms_id = b.id
						AND b.academic_years_id = {$this->db->escape($acad_years_id)}
						AND a.yr_level = {$this->db->escape($yr_level)}
						AND a.fees_subgroups_id = 201
					ORDER BY
						a.academic_terms_id DESC
					";

			//print($q); die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
		
		}
		
		/*
		 * @author: genes
		 * @date: 7/11/14
		 */
		function ListBasic_Ed_Fees($levels_id, $acad_years_id, $yr_level) {
			
			$result = NULL;
			
			$q = "SELECT
							c.fees_group,
							b.description,
							a.rate
						FROM
							fees_schedule AS a,
							fees_subgroups AS b,
							fees_groups AS c,
							academic_terms AS d
						WHERE
							a.fees_subgroups_id = b.id
							AND b.fees_groups_id = c.id
							AND a.levels_id = {$this->db->escape($levels_id)}
							AND a.academic_terms_id = d.id
							AND d.academic_years_id = {$this->db->escape($acad_years_id)}
							AND a.yr_level = {$this->db->escape($yr_level)}
							AND a.fees_subgroups_id != 201
						ORDER BY
							c.weight,b.description";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
				
		}
		
		
		function CheckIfPosted($basic_ed_histories_id) {
			$result = NULL;
			$q = "SELECT
						DATE_FORMAT(a.transaction_date,'%W, %M %e, %Y @ %h:%i%p') AS transaction_date
					FROM
						assessments_bed AS a
					WHERE
						a.basic_ed_histories_id = {$this->db->escape($basic_ed_histories_id)} ";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
				
			return $result;
				
		}
		
		
		/*
		 * @ADDED: 9/4/14
		 * @author: genes
		 */
		function getNumberofExams($basic_ed_histories_id) {
			$result = NULL;
			
			$q = "SELECT
						COUNT(*) AS num_exams
					FROM
						periods_bed AS a,
						basic_ed_histories AS b
					WHERE
						a.academic_years_id=b.academic_years_id
						AND a.levels_id=b.levels_id
						AND b.yr_level LIKE a.year_level
						AND b.id = {$this->db->escape($basic_ed_histories_id)} 
					GROUP BY
						a.academic_years_id,
						a.levels_id,
						b.yr_level";

			//print($q); die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
				
		}
		
		
		/*
		 * @ADDED: 9/4/14
		 * @author: genes
		 */
		function getCurrentDates_forExam($basic_ed_histories_id) {
			$result = NULL;
				
			$q = "SELECT
						a.id,
						a.exam_number,
						a.start_date,
						a.end_date
					FROM
						periods_bed AS a,
						basic_ed_histories AS b
					WHERE
						a.academic_years_id=b.academic_years_id
						AND a.levels_id=b.levels_id
						AND FIND_IN_SET(b.yr_level,a.year_level) > 0
						AND b.id = {$this->db->escape($basic_ed_histories_id)}
						AND CURRENT_DATE() <= a.end_date
					ORDER BY
						a.end_date
					LIMIT 1	";
			
			//print($q); die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
			$result = $query->row();
			}
				
			return $result;
			
				
		} 

		
		/*
		 * @Added: 9/12/14
		 * @author: genes
		 */
		function getStudentsEnrolled($academic_years_id, $levels_id, $grade_level, $post_status){
		
			$result = NULL;
			
			if ($post_status == 'Y') {
					$q = "SELECT
								COUNT(a.students_idno) AS numEnroll,
								c.level
							FROM 
								assessments_bed AS b,
								basic_ed_histories AS a,
								levels AS c
							WHERE
								b.basic_ed_histories_id = a.id
								AND a.levels_id = c.id
								AND a.academic_years_id = {$this->db->escape($academic_years_id)} 
								AND a.status='active' 
								AND a.basic_ed_sections_id NOT IN (166,175)";

					if ($levels_id != 0) {
						$q = $q . " AND a.levels_id = {$this->db->escape($levels_id)}";
					}
					
					if ($grade_level != 0) {
						$q = $q . " AND a.yr_level = {$this->db->escape($grade_level)} ";
					}
			} else { //excludes also students in self-contained section
				$q = "SELECT
								COUNT(a.students_idno) AS numEnroll,
								c.level
							FROM
								basic_ed_histories AS a,
								levels AS c
							WHERE
								a.levels_id = c.id
								AND a.academic_years_id = {$this->db->escape($academic_years_id)}
								AND a.status='active' 
								AND a.basic_ed_sections_id NOT IN (166,175)
								AND a.id NOT IN
									(SELECT
											a.basic_ed_histories_id
										FROM
											assessments_bed AS a) ";
				
				if ($levels_id != 0) {
					$q = $q . " AND a.levels_id = {$this->db->escape($levels_id)}";
				}
					
				if ($grade_level != 0) {
					$q = $q . " AND a.yr_level = {$this->db->escape($grade_level)} ";
				}
			}				
		
			//print($q); die();
			$query = $this->db->query($q);
		
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		
		}
		
		
		/*
		 * @Added: 9/12/14
		 * @author: genes
		 * @EDITED: 3/12/15: LEFT JOIN is removed
		 */
		function ListTuitionFees($academic_years_id, $academic_terms_id, $levels_id, $grade_level, $post_status) {
			$result = null;
				
			if ($post_status == 'Y') {
				/*$q = "SELECT
						d.level,
						IF (b.year_level IS NULL,a.yr_level,b.year_level) AS yr_level,
						c.rate,
						COUNT(DISTINCT(a.students_idno)) AS numEnroll
					FROM
						levels AS d,
						fees_schedule AS c 
							LEFT JOIN assessments_bed AS b ON b.year_level=c.yr_level AND b.levels_id=c.levels_id,
						basic_ed_histories AS a 
					WHERE
						c.fees_subgroups_id = 201
						AND a.id=b.basic_ed_histories_id
						AND a.status = 'active'
						AND a.levels_id = d.id
						AND a.basic_ed_sections_id NOT IN (166,175)
						AND a.academic_years_id = {$this->db->escape($academic_years_id)} 
						AND c.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
				*/
				$q = "SELECT
						a.level,
						d.yr_level AS yr_level,
						c.rate,
						COUNT(d.id) AS numEnroll,
						a.rank AS rank
					FROM
						levels AS a,
						assessments_bed AS b,
						fees_schedule AS c,
						basic_ed_histories AS d
					WHERE
						b.basic_ed_histories_id=d.id
						AND a.id=d.levels_id 
						AND d.academic_years_id={$this->db->escape($academic_years_id)} 
						AND d.status='active'
						AND c.academic_terms_id={$this->db->escape($academic_terms_id)} 
						AND c.fees_subgroups_id=201
						AND c.levels_id=d.levels_id
						AND c.yr_level=d.yr_level ";
					
				if ($levels_id != 0) {
					$q = $q . " AND d.levels_id = {$this->db->escape($levels_id)} ";
				}
					
				if ($grade_level != 0) {
					$q = $q . " AND d.yr_level = {$this->db->escape($grade_level)} ";
				}

			} else {
				$q = "SELECT
						d.level,
						a.yr_level,
						c.rate,
						COUNT(DISTINCT(a.students_idno)) AS numEnroll,
						d.rank AS rank
					FROM
						basic_ed_histories AS a,
						fees_schedule AS c,
						levels AS d
					WHERE
						a.yr_level = c.yr_level
						AND a.levels_id = c.levels_id
						AND a.levels_id = d.id
						AND a.status = 'active'
						AND c.fees_subgroups_id = 201
						AND a.basic_ed_sections_id NOT IN (166,175)
						AND a.id NOT IN (SELECT
											basic_ed_histories_id
										FROM
											assessments_bed)		
						AND c.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
				
				if ($levels_id != 0) {
					$q = $q . " AND c.levels_id = {$this->db->escape($levels_id)}";
				}
					
				if ($grade_level != 0) {
						$q = $q . " AND a.yr_level = c.yr_level
									AND c.yr_level = {$this->db->escape($grade_level)} ";
				}
				
			}

			$q = $q . " GROUP BY
							c.levels_id, c.yr_level
						ORDER BY
							rank, c.yr_level ";
				
			
			//print($q); die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		}
		
		/*
		 * @EDITED: 3/12/15: LEFT JOIN is removed
		 */
		function ListMiscellaneous($academic_years_id, $academic_terms_id, $levels_id, $grade_level, $post_status) {
		
			$result = NULL;
			
			if ($post_status == 'Y') {
				/*$q = "SELECT
						e.description,
						c.rate,
						COUNT(DISTINCT(a.students_idno)) AS enrollees
					FROM
						levels AS d,
						fees_subgroups AS e,
						fees_schedule AS c 
							LEFT JOIN assessments_bed AS b ON b.year_level=c.yr_level AND b.levels_id=c.levels_id
							LEFT JOIN basic_ed_histories AS a ON a.yr_level=c.yr_level AND a.levels_id=c.levels_id
					WHERE
						e.fees_groups_id = 2
						AND c.fees_subgroups_id = e.id
						AND a.status = 'active'
						AND a.levels_id = d.id
						AND a.basic_ed_sections_id NOT IN (166,175)
						AND a.academic_years_id = {$this->db->escape($academic_years_id)} 
						AND c.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
				*/
				$q = "SELECT
						e.description,
						c.rate,
						COUNT(DISTINCT(a.students_idno)) AS enrollees
					FROM
						levels AS d,
						fees_subgroups AS e,
						fees_schedule AS c,
						assessments_bed AS b,
						basic_ed_histories AS a 
					WHERE
						b.basic_ed_histories_id=a.id
						AND c.levels_id=a.levels_id
						AND c.yr_level=a.yr_level 						
						AND c.fees_subgroups_id = e.id
						AND d.id=a.levels_id
						AND a.academic_years_id={$this->db->escape($academic_years_id)} 
						AND a.status='active'
						AND c.academic_terms_id={$this->db->escape($academic_terms_id)} 
						AND e.fees_groups_id = 2 ";
				
				if ($levels_id != 0) {
					$q = $q . " AND c.levels_id = {$this->db->escape($levels_id)}";
				}
					
				if ($grade_level != 0) {
					$q = $q . " AND c.yr_level = {$this->db->escape($grade_level)} ";
				}
			
			} else { //excludes also students in self-contained section
				$q = "SELECT
						e.description,
						c.rate,
						COUNT(DISTINCT(a.students_idno)) AS enrollees
					FROM
						basic_ed_histories AS a,
						fees_schedule AS c,
						levels AS d,
						fees_subgroups AS e
					WHERE
						a.yr_level = c.yr_level
						AND a.levels_id = c.levels_id
						AND a.levels_id = d.id
						AND a.status = 'active'
						AND c.fees_subgroups_id = e.id
						AND e.fees_groups_id = 2
						AND a.basic_ed_sections_id NOT IN (166,175)
						AND a.id NOT IN (SELECT
											basic_ed_histories_id
										FROM
											assessments_bed)
						AND c.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
				
				if ($levels_id != 0) {
					$q = $q . " AND c.levels_id = {$this->db->escape($levels_id)} ";
				}
					
				if ($grade_level != 0) {
					$q = $q . " AND a.yr_level = c.yr_level
								AND c.yr_level = {$this->db->escape($grade_level)} ";
				}
				
			}				
		
			$q = $q . " GROUP BY
							c.rate,e.id
						ORDER BY
							e.description,d.id,c.yr_level ";
				
			
			//print($q); die();
			$query = $this->db->query($q);
		
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}

		/*
		 * @EDITED: 3/12/15: LEFT JOIN is removed
		 */
		function ListOtherSchoolFees($academic_years_id, $academic_terms_id, $levels_id, $grade_level, $post_status) {
		
			$result = NULL;
				
			if ($post_status == 'Y') {
				$q = "SELECT
							e.description,
							c.rate,
							COUNT(DISTINCT(a.students_idno)) AS enrollees
						FROM
							levels AS d,
							fees_subgroups AS e,
							fees_schedule AS c,
							assessments_bed AS b,
							basic_ed_histories AS a 
						WHERE
							b.basic_ed_histories_id=a.id
							AND c.levels_id=a.levels_id
							AND c.yr_level=a.yr_level 						
							AND c.fees_subgroups_id = e.id
							AND d.id=a.levels_id
							AND a.academic_years_id={$this->db->escape($academic_years_id)} 
							AND a.status='active'
							AND c.academic_terms_id={$this->db->escape($academic_terms_id)} 
							AND e.fees_groups_id = 3 ";
									
				if ($levels_id != 0) {
					$q = $q . " AND c.levels_id = {$this->db->escape($levels_id)}";
				}
					
				if ($grade_level != 0) {
					$q = $q . " AND c.yr_level = {$this->db->escape($grade_level)} ";
				}
				
			} else { //excludes also students in self-contained section
				$q = "SELECT
							e.description,
							c.rate,
							COUNT(DISTINCT(a.students_idno)) AS enrollees
						FROM
							basic_ed_histories AS a,
							fees_schedule AS c,
							levels AS d,
							fees_subgroups AS e
						WHERE
							a.yr_level = c.yr_level
							AND a.levels_id = c.levels_id
							AND a.levels_id = d.id
							AND a.status = 'active'
							AND c.fees_subgroups_id = e.id
							AND e.fees_groups_id = 3
							AND a.basic_ed_sections_id NOT IN (166,175)
							AND a.id NOT IN (SELECT
												basic_ed_histories_id
											FROM
												assessments_bed)
							AND c.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
		
				if ($levels_id != 0) {
					$q = $q . " AND c.levels_id = {$this->db->escape($levels_id)}";
				}
					
				if ($grade_level != 0) {
					$q = $q . " AND a.yr_level = c.yr_level
					AND c.yr_level = {$this->db->escape($grade_level)} ";
				}
					
			}
		
			$q = $q . " GROUP BY
							c.rate,e.id
						ORDER BY
							e.description,d.id,c.yr_level ";
						
			//print($q); die();
			$query = $this->db->query($q);
		
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		
			return $result;
		
		}
		
		/*
		 * @EDITED: 3/12/15: LEFT JOIN is removed
		 */
		function ListAdditionalOtherFees($academic_years_id, $academic_terms_id, $levels_id, $grade_level, $post_status) {
		
			$result = NULL;
		
			if ($post_status == 'Y') {
				$q = "SELECT
							CONCAT(e.description,'[',d.level,'-',a.yr_level,']') AS description,
							c.rate,
							COUNT(DISTINCT(a.students_idno)) AS enrollees
						FROM
							levels AS d,
							fees_subgroups AS e,
							fees_schedule AS c,
							assessments_bed AS b,
							basic_ed_histories AS a 
						WHERE
							b.basic_ed_histories_id=a.id
							AND c.levels_id=a.levels_id
							AND c.yr_level=a.yr_level 						
							AND c.fees_subgroups_id = e.id
							AND d.id=a.levels_id
							AND a.academic_years_id={$this->db->escape($academic_years_id)} 
							AND a.status='active'
							AND c.academic_terms_id={$this->db->escape($academic_terms_id)} 
							AND e.fees_groups_id = 4 ";
									
				if ($levels_id != 0) {
					$q = $q . " AND c.levels_id = {$this->db->escape($levels_id)}";
				}
					
				if ($grade_level != 0) {
					$q = $q . " AND c.yr_level = {$this->db->escape($grade_level)} ";
				}
		
			} else { //excludes also students in self-contained section
				$q = "SELECT
							CONCAT(e.description,'[',d.level,'-',a.yr_level,']') AS description,
							c.rate,
							COUNT(DISTINCT(a.students_idno)) AS enrollees
						FROM
							basic_ed_histories AS a,
							fees_schedule AS c,
							levels AS d,
							fees_subgroups AS e
						WHERE
							a.yr_level = c.yr_level
							AND a.levels_id = c.levels_id
							AND a.levels_id = d.id
							AND a.status = 'active'
							AND c.fees_subgroups_id = e.id
							AND e.fees_groups_id = 4
							AND a.basic_ed_sections_id NOT IN (166,175)
							AND a.id NOT IN (SELECT
												basic_ed_histories_id
											FROM
												assessments_bed)
							AND c.academic_terms_id = {$this->db->escape($academic_terms_id)} ";
		
				if ($levels_id != 0) {
					$q = $q . " AND c.levels_id = {$this->db->escape($levels_id)}";
				}
					
				if ($grade_level != 0) {
					$q = $q . " AND a.yr_level = c.yr_level
								AND c.yr_level = {$this->db->escape($grade_level)} ";
				}
				
			}
		
			$q = $q . " GROUP BY
							d.id,c.rate,e.id
						ORDER BY
							e.description,d.id,c.yr_level";
		
			//print($q); die();
			$query = $this->db->query($q);
		
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
		
			return $result;
		
		}

		public function getPostedAssessment_BEd($basic_ed_histories_id){
			$sql = "select assessed_amount 
                    from assessments_bed 
                    where basic_ed_histories_id={$basic_ed_histories_id}";

            //log_message("INFO",print_r($sql,true));

			$query = $this->db->query($sql);
		
			if ($query && $query->num_rows() > 0){
				return $query->result();
			} else {
				return array( 'assessed_amount' => 0.00 );
			}
		}
		
	}
	
//end of assessment_basic_ed_model	