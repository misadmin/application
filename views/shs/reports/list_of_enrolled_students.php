		
			<div style="width:100%; overflow:auto; margin-top:15px; ">
					<table class="table table-bordered table-striped table-condense" >
						<thead>
							<tr style="text-align:center; font-size:12px; font-weight:bold;" class="shs_header" >
								<td class="shs_header" style="width:8%;">Count</td>
								<td class="shs_header" style="width:14%;">ID<br>No.</td>
								<td class="shs_header" style="width:25%;">Lastname</td>
								<td class="shs_header" style="width:25%;">Firstname</td>
								<td class="shs_header" style="width:20%;">Middlename</td>
								<td class="shs_header" style="width:8%;">Gender</td>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($students) {
									$cnt=1;
									foreach($students  AS $student) {
							?>			
										<tr style="font-size:12px;">
											<td style="text-align:right;"><?php print($cnt); ?>.</td>
											<td style="text-align:center;">
												<?php 
													if (isset($link_student)) {
												?>
														<a href="<?php print(site_url($this->uri->segment(1).'/student')."/".$student->idno);?>" >
															<?php print($student->idno); ?>
														</a>													
												<?php 
													} else {
														print($student->idno); 
													}
												?>
												</td>
											<td><?php print($student->lname); ?></td>
											<td><?php print($student->fname); ?></td>
											<td><?php print($student->mname); ?></td>
											<td style="text-align:center;"><?php print($student->gender); ?></td>
										</tr>
							<?php
										$cnt++;
									}
								}
							?>
						</tbody>
					</table>
			</div>
