<div class="span12">
	<div id="scanned_tor"></div>
	<div id="tor_upload_form">
		<form id="upload_tor_form" method="post" action="" class="form-horizontal" enctype="multipart/form-data" />
			<?php echo $this->common->hidden_input_nonce(); ?>
			<input type="hidden" name="action" value="upload_tor" />
			<fieldset>
				<div class="control-group formSep">
					<label for="tor" class="control-label">TOR Image</label>
					<div class="controls">
						<input type="file" id="tor" name="tor" class="input-xlarge" placeholder="TOR Image" />
					</div>
				</div>
				<div class="control-group formSep">
					<div class="control-group">
						<div class="controls">
							<button id="submit_tor" class="btn btn-primary" disabled="disabled" type="submit">Post TOR</button>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<script>
	$('#tor').on('change', function(){
		$('#submit_tor').removeAttr('disabled');
	});
</script>