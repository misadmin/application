
<div id="show_section_management_page" >
	<h2 class="heading">Section Management</h2>

	<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px;">
		<div style="font-size:18px; font-weight:bold; margin-bottom:10px; color:#030B3D;">
			<?php print($section->term." ".$section->sy); ?>
		</div>
		<div style="font-size:25px; font-weight:bold; margin-bottom:10px; color:#030B3D;">
			<?php print("Grade ".$section->grade_level." - ".$section->section_name); ?>
		</div>
		<div style="font-size:20px; font-weight:bold; font-style:italic; margin-bottom:10px; color:#025E05;">
			<?php print($section->strand); ?>
		</div>
		<div style="font-size:14px; font-weight:bold;">
			<div style="float:left; width:120px; clear:both;">
				Class Adviser
			</div>
			<div style="float:left; width:15px;">
				:
			</div>
			<div style="float:left;" class="show_class_adviser">
				<?php print($section->class_adviser); ?>
			</div>
			<div style="float:left; width:120px; clear:both;">
				Classroom
			</div>
			<div style="float:left; width:15px;">
				:
			</div>
			<div style="float:left;">
				<?php print($section->room_no); ?>
			</div>
		</div>
	</div>
	<?php
		if ($section->strand_status == "O") {
	?>
	<div style="width:99%; overflow:auto; margin:0 auto; margin-top:15px;" id="show_extracted_items" >
		<ul class="nav nav-tabs" style="font-weight:bold; color:#08055E;">
			<li <?php if($active_tab == 'tab3') print("class='active'"); ?> ><a href="#section_details" data-toggle="tab">Section Details</a></li>
			<li <?php if($active_tab == 'tab1') print("class='active'"); ?> ><a href="#class_schedule" data-toggle="tab">Class Schedule</a></li>
			<li <?php if($active_tab == 'tab2') print("class='active'"); ?> ><a href="#list_of_students" data-toggle="tab">List of Students</a></li>
		</ul>
		<div class="tab-content" >
			<div class="tab-pane <?php if($active_tab == 'tab2') print("in active"); ?>" id="list_of_students">
				<?php
					$data['percent'] = "50%";
					$this->load->view('shs/student/list_of_students', $data);
				?>
			</div>
			<div class="tab-pane <?php if($active_tab == 'tab1') print("in active"); ?>" id="class_schedule">
				<?php
					$this->load->view('shs/sections/class_schedule');
				?>
			</div>
			<div class="tab-pane <?php if($active_tab == 'tab3') print("in active"); ?>" id="section_details">
				<?php
					$data=array(
								"section"=>$section,
								"faculty"=>$faculty,
								"strands"=>$strands,
							);
					$this->load->view('shs/sections/section_details', $data);
				?>
			</div>
		</div>

	</div>
	<?php
		} else {
	?>
	<div style="font-size:20px; font-weight:bold; color:#E40808; margin-top:20px; padding-left:20px;" >
		Strand is currently not offered!
	</div>
	<?php
		}
	?>
</div>

