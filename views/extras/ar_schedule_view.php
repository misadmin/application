<style>
	table.balances 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
	tr.sub_total {font-weight: bold;}
</style>

<?php 

$code_levels= array(
		2=>'Kinder',
		3=>'Grade School',
		4=>'HS-Day',
		5=>'HS-Night',
		6=>'College',
		7=>'Graduate School',
		8=>'K-12 Day',
		9=>'K-12 Night',
		99=>'Other Payors',
	);
?>


<h3 class="heading">
<?php 	
	echo $title;	
	if ($this->input->post('sub_level')){
		if ($this->input->post('top_levels') == 'basic_ed')
			echo ' for ' . $code_levels[$this->input->post('sub_level')] ;
		elseif ($this->input->post('top_levels') == 'college') 
			echo ' for ' . $colleges[(float)$this->input->post('sub_level')];
	}
?> as of <?php  echo (isset($date)? $date:"") . ":"; ?></h3>
<div class="loading"></div>
<?php 
	//$date = isset($date) ? $this->input->post('end_date') : date('Y-m-d');
?>
 
<div class="row-fluid">
	<?php if(isset($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $message; ?>
	</div>
	<?php endif; ?>
	<form id="frm_sked" method="post" action="" class="form-horizontal">
		<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
			
			    <label class="control-label" for="end_date">On or Before:</label>
			    <div class="controls date" id="cut_date" data-date-format="yyyy-mm-dd" data-date="<?php echo date('Y-m-d'); ?>">
			      <input type="text" id="end_date" name="end_date" placeholder="YYYY-MM-DD" value="<?php echo $this->input->post('end_date') ? $this->input->post('end_date') : date('Y-m-d')?>">
				 <span class="add-on"><i class="icon-calendar"></i></span>
			    </div>


				<label for="top_levels" class="control-label">Department: </label>
				<div class="controls">
					<select id="top_levels" name="top_levels">
						<option value="">-- Select Top Level --</option>						
						<option <?php echo ('basic_ed'==$this->input->post('top_levels') ? 'selected="selected"' : ''); ?> value="basic_ed">Basic Ed</option>	
						<option <?php echo ('college'==$this->input->post('top_levels') ? 'selected="selected"' : ''); ?> value="college">College</option>	
					</select>
				</div>				
			    
				<label id="l_sub_level" for="sub_levels" class="control-label">Sub Level: </label>
				<div class="controls">
					<select id="sub_levels" name="sub_level">
						<option value="">-- Select Sub Level --</option>	
					</select>
				</div>
				
				<label id="print_to" for="print_to" class="control-label">Target: </label>
				<div class="controls">
					<select id="print_to" name="print_to">
						<option value="Screen">Screen</option>	
						<option value="CSV">CSV (Can be opened by MS Excel)</option>	
						<option value="DOC" disabled>Word</option>						
						<option value="PDF" disabled>PDF</option>
					</select>
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>	
							
			</fieldset>
	</form>
</div>
<form method="post" action="<?php echo site_url('extras/ar_schedule_view'); ?>" id="mismatches_form">
	<?php echo $this->common->hidden_input_nonce(); ?>
</form>
<div id="table_here" class="span7">
<?php 
	if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {

?>

<table id="balances_t" class="table table-condensed table-bordered table-striped balances">
<thead>
	<tr>
	<th></th>
	<th>SY</th>
	<th>ID No.</th>
	<th>Student</th>
	<th>Program/Section</th>
	<th>Balance</th>
	</tr>
</thead>

<?php
	if (count($rows) < 1) {
		echo '<td class="center" colspan=5>No record found!</td>';
	}else{
		$row_num = 0;
		//print_r($rows);die();
		foreach($rows as $row){
			if ($row['abbreviation'] !== 'Sub Total'){
				$row_num++;
			}
			
?>
			<tr <?php echo $row['abbreviation']=='Sub Total' ? 'class="sub_total"' : '';  ?>>
			<td class="center"><?php echo $row['abbreviation'] == 'Sub Total' ? '': $row_num .'.'; ?> </td>
			<td class="center"><?php echo $row['sy']; ?></td>
			<td class="center"><a href="<?php echo site_url($this->session->userdata('role') . '/student') .'/'. $row['students_idno'] ?>"><?php echo $row['students_idno']; ?>  </a></td>	
			<td class="left"><?php echo $row['student']; ?></td>
			<td class="left"><?php echo $row['year_level'] ." " .  $row['abbreviation']; ?></td>
			<td class="right"><?php echo number_format($row['balance'],2); ?></td>
			</tr>		

<?php
		} 		
	}
?>

</table>

<?php } 
?>

</div>



<script>
$(document).ready(function(){
	$("#balances_t").stickyTableHeaders();
    $("#balances_t").stickyTableHeaders('updateWidth');
	$('#cut_date').datepicker();

	$('#frm_sked').validate({
		submitHandler: function(form) {
			$('.loading').delay(5).fadeIn(100);
			form.submit();
		}
	});

	
	$('#top_levels').change(function(){
		var top_level_id = $('#top_levels option:selected').val();
		
		$.ajax({
			url: "<?php echo site_url("ajax/list_sub_levels"); ?>/?top_levels=" + top_level_id,
			dataType: "json",
			success: function(data){
				var doptions = make_options(data);
				$('#sub_levels').html(doptions);
				if (top_level_id =='basic_ed')
					$('#l_sub_level').html("Year Level:")
				else
					$('#l_sub_level').html("College:");
			}	
		});	
	});


	function make_options (data){
		var doptions = '<option value="">-- Select Sub Level --</option>';
		for (var i = 0; i < data.length; i++) {
			doptions = doptions 
			 	+ '<option value="'
			 	+ data[i].id
			 	+ '">'
			 	+ data[i].sublevel
			 	+ '</option>';
		}
		return doptions;
	}
	
	
	
});
/**    
function make_options (data){
	var doptions = '<option value="">-- Select Sub Level --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].sublevel
		 	+ '</option>';
	}
	return doptions;
}
**/

</script>