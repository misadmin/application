<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
	text-align:left;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
select.form1 {	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

 <div style="background:#FFF; width:40%; text-align:center; margin:auto;">
  <table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head" style="text-align:center; padding:10px;">
  Block/Unblock Student </td>
  </tr>
  <tr>
    <td colspan="3">
	<form action="<?php echo site_url("dean/student/{$student['idnum']}"); ?>" method="post">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
      <input type="hidden" name="action" value="assign_student_to_blocksection" />
        <table align="center" cellpadding="2" cellspacing="0" class="inside">
          <?php /*<tr>
            <td width="199">Academic Term</td>
            <td width="15">:</td>
            <td width="438"><?php print($current_term->term." ".$current_term->sy); ?></td>
          </tr> */ ?>
          <tr>
            <td width="199">Current Block Assignment</td>
            <td width="15">:</td>
            <td width="438"><?php 
					if ($block) {
						print($block->yr_level." - ".$block->section_name); 
					} else {
						print ("No assigned block");
					}
				?>            </td>
          </tr>
          <tr>
            <td>Block Section </td>
            <td>:</td>
            <td><select name="blocksection" id="blocksections_id" class="form">
                <?php 
  		foreach($blocksection as $block_sect) {
			print("<option value= $block_sect->id> ".$block_sect->abbreviation. " - " .$block_sect->yr_level." [ ".$block_sect->section_name." ]</option>");
		} ?>
            </select></td>
          </tr>
          <tr>
            <td colspan="3" align="center"><input type="submit" name="button" id="button" value="Assign Block Section!" class="btn btn-success"/></td>
            </tr>
        </table>
    </form>    </td>
  </tr>
  <?php
		if ($block) {
  ?>
  <tr>
    <td colspan="3" align="center">
	<form action="<?php echo site_url("dean/student/{$student['idnum']}"); ?>" method="post">
      <input type="hidden" name="action" value="unblock_student" />
			<input type="submit" name="button" id="button" value="Unblock Student!" class="btn btn-success" />
	</form>	
	</td>
  </tr>
  <?php
  		}
  ?>
   </table>
 </div>

