<style>
	td.grade_header {
		background-color:#E7E8E7;
		font-size:bold;
		text-align:center;
		font-size:12px;
		vertical-align:middle;
		height:auto;
		padding:2px;
	}

</style>

	<div style="width:90%;">
		<?php 
			$term_quarter1 = '1';
			$term_quarter2 = '2';
			if ($grades) {
				foreach($grades AS $term) {
		?>
					<div style="font-size:15px; font-weight:bold; color:#025A0E; border:solid 1px #D6D8D6; background-color:#C3C3C3;
						height:24px; padding-left:5px; padding-top:7px;" >
						<?php print($term->term.' - '.$term->sy); 
							//added by Toyet 7.30.2018
							if(substr($term->term,0,3)=='1st') {
								$term_quarter1 = '1'; 
								$term_quarter2 = '2'; 
							} else {
								$term_quarter1 = '3'; 
								$term_quarter2 = '4'; 
							} ?>
					</div>

					<div style="width:100%; overflow:auto; margin-top:2px; margin-bottom:25px;">
						<table class="table table-hover table-bordered table-striped" >
							<thead>
								<tr style="text-align:center; font-size:11px; font-weight:bold;" >
									<td class="grade_header" rowspan="2" style="width:10%;">Course<br>Code</td>
									<td class="grade_header" rowspan="2" style="width:37%;">Descriptive Title</td>
									<td class="grade_header" rowspan="2" style="width:25%;">Teacher</td>
									<td class="grade_header" colspan="2" style="width:12%;">Quarter</td>
									<td class="grade_header" rowspan="2" style="width:6%;">Final<br>Grade</td>
									<td class="grade_header" rowspan="2" style="width:10%;">Remarks</td>
								</tr>
								<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
									<td class="grade_header" style="width:6%;"><?php echo $term_quarter1; ?></td>
									<td class="grade_header" style="width:6%;"><?php echo $term_quarter2; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php
									$midterm_total      = 0;
									$finals_total       = 0;
									$show_ave_midterm   = TRUE;
									$show_ave_finals    = TRUE;
									$num_subj           = 0;
									$total_final_grades = NULL;
									
									if (isset($term->grades)) {

										foreach($term->grades AS $grade) {
								?>			
											<tr>
												<td style="text-align:center;"><?php print($grade->course_code); ?></td>
												<td><?php print($grade->descriptive_title); ?></td>
												<td><?php print($grade->teacher); ?></td>
												<td style="text-align:center; font-weight:bold;"><?php print($grade->midterm_grade); ?></td>
												<td style="text-align:center; font-weight:bold;"><?php print($grade->finals_grade); ?></td>
												<td style="text-align:center; font-weight:bold;">
													<?php 
														if ($grade->final_grade) {
															print(number_format(round($grade->final_grade,0),0)); 
															$total_final_grades += round($grade->final_grade);
														}
													?>
												</td>
												<td style="text-align:center; font-weight:bold;">
													<?php 

														if ($grade->midterm_grade) {
															$midterm_total += $grade->midterm_grade;
														} else {
															$show_ave_midterm = FALSE;
														}

														if ($grade->finals_grade) {
															$finals_total += $grade->finals_grade;
														} else {
															$show_ave_finals = FALSE;
														}

														if ($grade->final_grade) {
															if (round($grade->final_grade,0) >= 75) {
																print("Promoted");
															} else {
																print("Failed");
															}
														}

														$num_subj++;
														//print($grade->remark); 
													?>
												</td>
											</tr>
								<?php 
										}
									}
								?>
							</tbody>
							<tfoot>
								<tr style="text-align:center; font-weight:bold;" >
									<td colspan="3" class="grade_header" style="text-align:right; font-size:14px; " >GENERAL AVE:</td>
									<td class="grade_header" style="font-size:14px;" >
									<?php 
									
										if ($show_ave_midterm AND $num_subj) {
											$ave_midterm = number_format(($midterm_total/$num_subj),2);
											print($ave_midterm);
										} 
									
									?>
									</td>
									<td class="grade_header" style="font-size:14px;">
									<?php 
									
										if ($show_ave_finals AND $num_subj) {
											$ave_finals = number_format(($finals_total/$num_subj),2);
											print($ave_finals);
										} 
									
									?>
									
									</td>
									<td class="grade_header" style="font-size:14px;">
									<?php
									
										if ($show_ave_midterm AND $show_ave_finals AND $num_subj) {
											print(number_format(($total_final_grades/$num_subj),2));
										} else {
											$gen_ave = NULL;
										}
										
									?>
									</td>
									<td class="grade_header" style="font-size:14px;">
									</td>
								</tr>
							</tfoot>
						</table>
					</div>

		<?php 
				}
			}
		?>
	</div>
	
	
	
	