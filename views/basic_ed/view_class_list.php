<h2 class="heading" style="margin-top:20px;"> 
Section/Level: <?php print($section_name." - ".$yr_level); ?>
<br />
Class Adviser: <?php print($class_adviser); ?>
	       
</h2>	       

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height:30px; 
	padding: 2px; margin-bottom:20px; margin-left:40px; width:60%; ">

	<table>
	<tr>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:13%;">
	 <a href="<?php print(base_url('downloads/Class List - '.$section_name.' '.$yr_level.'.xls')); ?>" class="btn btn-link">
	<i class="icon-download-alt"></i> Download as Excel</a> 
	</td>
	</tr>
	</table>						
</div>

<div style="margin-top:10px">
	 <!--       <a href="download" class="btn btn-link " id="download_list"><i class="icon-download-alt"></i> <strong>Download as an Excel File</strong></a> </h2>  -->
		<?php if ($list) { 	
					$count = 0;?>
			<div style="width:60%; margin-bottom:40px; margin-left:40px;">
			  <table id="advisers_list" border="0" cellspacing="0" class="table table-condensed table-bordered table-hover" 
			  		style="width:100%; padding:0px; margin:0px; ">
			    <thead>
				    <tr style="height:45px;">
				      <th width="8%" style="vertical-align:middle; text-align:center;">No.</th>
				      <th width="28%" style="vertical-align:middle; text-align:center;">Last Name</th>
				   	  <th width="29%"  style="vertical-align:middle; text-align:center;">First Name</th>
				      <th width="30%"  style="vertical-align:middle; text-align:center;">Middle Name</th>
				      <th width="5%"  style="vertical-align:middle; text-align:center;">Gender</th>
				    </tr>
			    </thead>		
				<?php foreach ($list AS $l) { 
							$count++;?>
					    <tr>
					      <td style="text-align:center; vertical-align:middle; "><?php print($count); ?> </td>
					      <td style="text-align:left; vertical-align:middle; "><?php print($l->lname); ?> </td>
					      <td style="text-align:left; vertical-align:middle; "><?php print($l->fname); ?> </td>
					      <td style="text-align:left; vertical-align:middle; "><?php print($l->mname); ?> </td>
					      <td style="text-align:center; vertical-align:middle; "><?php print($l->gender); ?> </td>
					   	</tr>
				<?php } ?>
			  </table>
			</div>
		<?php } ?>
		
<form method="post" action="<?php echo site_url("{$role}/download_section_csv"); ?>" id="download_list_form">
<!-- 			<input type="hidden" name="year" value="<?php echo $section->year_level; ?>" />
			<input type="hidden" name="level" value="<?php echo $section->levels_id; ?>" />
			<input type="hidden" name="adviser" value="<?php echo $section->id; ?>" />
			<input type="hidden" name="section" value="<?php echo $section->bed_id; ?>" />
			<input type="hidden" name="academicyear" value="<?php echo $section->academic_years_id; ?>" />
 -->			
</form>

<script>
	$('#download_list').click(function(event){
		event.preventDefault(); 
		$('#download_list_form').submit();
	});
</script>
</div>
		
  <form id="download_enroll_summary_form" method="post" target="_blank">
  <input type="hidden" name="step" value="3" />
</form>

<script>
	$('.download_enroll').click(function(event){
		event.preventDefault(); 
		var academic_years_id = $(this).attr('academic_years_id');
			
		$('<input>').attr({
			type: 'hidden',
			name: 'academic_years_id',
			value: academic_years_id,
		}).appendTo('#download_enroll_summary_form');
		$('#download_enroll_summary_form').submit();
		
	});
</script>




