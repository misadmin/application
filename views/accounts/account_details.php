<?php
	
?><h2 class="heading"><?php echo $account->account_name; ?></h2>
<h5>Account Number: <?php echo $account->account_number; ?></h5>
<h5>Account Type: <?php echo $account->type; ?></h5>
<br /><br />
<h3>Statement of Account</h3>
<table class="table table-bordered table-condensed" id="account_details_table">
	<thead>
		<tr>
			<th>Date</th>
			<th>Item</th>
			<th>Debit</th>
			<th>Credit</th>
			<th>Running Balance</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
		<tr>
			<td>2013-10-20</td>
			<td>Payment For Rentals</td>
			<td>10000</td>
			<td></td>
			<td>1000</td>
		</tr>
	</tbody>
</table>
<script>
$().ready(function(){
	$('#account_details_table').dataTable();
});
</script>