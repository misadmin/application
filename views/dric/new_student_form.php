<h2 class="heading">Create a New Student Account</h2>

<div class="row-fluid">
	<?php if(isset($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $message; ?>
	</div>
	<?php endif; ?>
	<form id="new_student" method="post" action="" class="form-horizontal">
		<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
				<div class="control-group formSep">
					<label for="firstname" class="control-label">First Name: </label>
					<div class="controls">
						<input type="text" name="firstname" id="firstname" placeholder="First Name" class="input-xlarge" value="<?php if(isset($firstname)) echo $firstname; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="middlename" class="control-label">Middle Name: </label>
					<div class="controls">
						<input type="text" name="middlename" id="middlename"  placeholder="Middle Name" class="input-xlarge"  value="<?php if(isset($middlename)) echo $middlename; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="familyname" class="control-label">Family Name: </label>
					<div class="controls">
						<input type="text" name="familyname" id="familyname" placeholder="Family Name" class="input-xlarge"  value="<?php if(isset($familyname)) echo $familyname; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="gender" class="control-label">Gender: </label>
					<div class="controls">
						<select id="gender" name="gender">
							<?php foreach ($this->config->item('genders') as $key=>$val): ?>
							<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="college" class="control-label">College: </label>
					<div class="controls">
						<select id="colleges" name="college">
							<option value="">-- Select College --</option>
							<?php foreach ($colleges as $college): ?>
							<option value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="programs" class="control-label">Program: </label>
					<div class="controls">
						<select id="programs" name="program">
							<option value="">-- Select Program --</option>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="address" class="control-label">Home Address: </label>
					<div class="controls" style="padding-bottom: 5px;">
						<select id="country" name="country">
							<option value="137">Philippines</option>
							<?php foreach ($countries as $country): ?>
								<option value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="controls" style="padding-bottom: 5px;">
						<select id="province" name="province">
							<option value="49">Bohol</option>
							<?php foreach ($provinces as $province): ?>
								<option value="<?php echo $province->id; ?>"><?php echo $province->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="controls" style="padding-bottom: 5px;">
						<select id="town" name="town">
							<option value="">-- Select Town --</option>
							<?php foreach ($towns as $town): ?>
								<option value="<?php echo $town->id; ?>"><?php echo $town->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="controls" style="padding-bottom: 5px;">
						<select id="barangay" name="barangay">
							<option value="">-- Select Barangay --</option>
						</select>,&nbsp
					</div>
					<div class="controls" style="padding-bottom: 5px;">
						<input type="text" name="address" id="address" placeholder="Sitio/Street/House No" class="input-xlarge"  value="<?php if(isset($address)) echo $address; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="primaryphone" class="control-label">Student's Primary Mobile Phone No: </label>
					<div class="controls">
						<input type="text" name="primaryphone" id="primaryphone" placeholder="Student's Primary Phone" class="input-xlarge"  value="<?php if(isset($primaryphone)) echo $primaryphone; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<div class="container"><h3>EMERGENCY INFO</h3></div>
					<label for="persontocontact" class="control-label">Person to Contact: </label>
					<div class="controls" style="padding-bottom: 5px;">
						<input type="text" name="persontocontact" id="persontocontact" placeholder="Person to contact in case of emergency" class="input-xlarge"  value="<?php if(isset($persontocontact)) echo $persontocontact; ?>" />
					</div>
					<label for="college" class="control-label">Relation: </label>
					<div class="controls" style="padding-bottom: 5px;">
						<input type="text" name="relation" id="relation" placeholder="Relation" class="input-xlarge"  value="<?php if(isset($relation)) echo $relation; ?>" />
					</div>
					<label for="college" class="control-label">Emergency Phone: </label>
					<div class="controls" style="padding-bottom: 5px;">
						<input type="text" name="emergency_phone" id="emergency_phone" placeholder="Emergency Phone" class="input-xlarge"  value="<?php if(isset($emergency_phone)) echo $emergency_phone; ?>" />
					</div>
					<label for="college" class="control-label">Emergency Address: </label>
					<div class="controls" style="padding-bottom: 5px;">
						<input type="text" name="emergency_address" id="emergency_address" placeholder="Emergency Address" class="input-xlarge"  value="<?php if(isset($emergency_address)) echo $emergency_address; ?>" />
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Add New Student</button>
					</div>
				</div>	
			</fieldset>
	</form>
</div>
<form method="post" action="<?php echo site_url('dric/student'); ?>" id="search_duplicate_form">
	<?php echo $this->common->hidden_input_nonce(); ?>
</form>
<script>
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Unacceptable values found"
);

$('#new_student').validate({
	onkeyup: false,
	errorClass: 'error',
	validClass: 'valid',
	rules: {
		firstname: { required: true },
		familyname: { required: true },
		college: { required: true },
		program: { required: true },
		country: { required: true },
		province: { required: true },
		town: { required: true },
		barangay: { required: true },
		address: { required: true },
		primaryphone: { required: true },
		persontocontact: { required: true },
		relation: { required: true },
		emergency_address: { required: true },
		emergency_phone: { required: true }
	},
	highlight: function(element) {
		$(element).closest('div').addClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	unhighlight: function(element) {
		$(element).closest('div').removeClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	errorPlacement: function(error, element) {
		$(element).closest('div').append(error);
	}
});
/*
$("#firstname").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
$("#familyname").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
$("#middlename").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
*/
$('#colleges').change(function(){
	var college_id = $('#colleges option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
		dataType: "json",
		success: function(data){
			var doptions = make_options(data);
			$('#programs').html(doptions);
		}	
	});	
});

$('#familyname').on('blur', function(){
	var firstname = $('#firstname').val();
	var lastname = $('#familyname').val();

	if(firstname == "" || lastname == ""){
		return false;
	}	
	$.ajax({
		url: "<?php echo site_url("ajax/search_student"); ?>/?query=" + lastname + "," + firstname,
		dataType: "json",
		success: function(data){
			if(data.suggestions.length > 0){
				var url = "<?php echo site_url('dric/student'); ?>";
				$.gritter.add({
					time : 10000,
					title : 'Duplicate Warning',
					text : 'A search on this name produced ' + data.suggestions.length + ' result(s). Hence, this warning. Possible duplicates can be viewed <a id="search_duplicate" search_string="' + data.query +'" href="javascript://">Here</a>',
				});
				$('#search_duplicate').on('click', function(){
					var search_item = $(this).attr('search_string');
					console.log("here");
					$('<input>').attr({
					    type: 'hidden',
					    name: 'q',
					    value: search_item,
					}).appendTo('#search_duplicate_form');
						$('#search_duplicate_form').submit();	
				});
			} else {
				$.gritter.add({
					time : 10000,
					title : 'No Duplicate',
					text : 'There are no possible duplicates of this name.',
				});
			}
		}	
	});	
});

$('#firstname').on('blur', function(){
	var firstname = $('#firstname').val();
	var lastname = $('#familyname').val();

	if(firstname == "" || lastname == ""){
		return false;
	}	
	$.ajax({
		url: "<?php echo site_url("ajax/search_student"); ?>/?query=" + lastname + "," + firstname,
		dataType: "json",
		success: function(data){
			if(data.suggestions.length > 0){
				var url = "<?php echo site_url('dric/student'); ?>";
				$.gritter.add({
					time : 10000,
					title : 'Duplicate Warning',
					text : 'A search on this name produced ' + data.suggestions.length + ' result(s). Hence, this warning. Possible duplicates can be viewed <a id="search_duplicate" search_string="' + data.query +'" href="javascript://">Here</a>',
				});
				$('#search_duplicate').on('click', function(){
					var search_item = $(this).attr('search_string');
					console.log("here");
					$('<input>').attr({
					    type: 'hidden',
					    name: 'q',
					    value: search_item,
					}).appendTo('#search_duplicate_form');
						$('#search_duplicate_form').submit();	
				});
			} else {
				$.gritter.add({
					time : 10000,
					title : 'No Duplicate',
					text : 'There are no possible duplicates of this name.',
				});
			}
		}	
	});	
});


$('#primaryphone').on('focusout', function(){
	var primaryphone = $('#primaryphone').val();
	var sp_chars = '.\+*?[^]$(){}=!<>|:-';
	primaryphone = primaryphone.trim();
	if(primaryphone.length==11){
		for(var i=0; i<=sp_chars.length; i++){
			console.log(sp_chars[i]);
			if(sp_chars.indexOf(primaryphone[i]) !== -1){
				$.gritter.add({
					time : 10000,
					title : 'Invalid Primary Mobile Phone',
					text : 'Phone number may contain a specials characters like =>  '+sp_chars,
				});
			}
		}
	} else {
		$.gritter.add({
			time : 10000,
			title : 'Invalid Primary Mobile Phone',
			text : 'Phone number should be 11 digits long.',
		});
	}
})


$('#emergency_phone').on('focusout', function(){
	var emergency_phone = $('#emergency_phone').val();
	var sp_chars = '.\+*?[^]$(){}=!<>|:-';
	emergency_phone = emergency_phone.trim();
	if(emergency_phone.length==11){
		for(var i=0; i<=sp_chars.length; i++){
			console.log(sp_chars[i]);
			if(sp_chars.indexOf(primaryphone[i]) !== -1){
				$.gritter.add({
					time : 10000,
					title : 'Invalid Primary Mobile Phone',
					text : 'Phone number may contain a specials characters like =>  '+sp_chars,
				});
			}
		}
	} else {
		$.gritter.add({
			time : 10000,
			title : 'Invalid Emergency Phone',
			text : 'Phone number should be 11 digits long.',
		});
	}
})


$('#province').change(function(){
	var province_id = $('#province option:selected').val();
	//alert(province_id);
	$.ajax({
		url: "<?php echo site_url("ajax/listTownsByProvince"); ?>/?province_id="+province_id,
		dataType: "json",
		success: function(data){
			//console.log(data);
			var doptions = make_options_towns(data);
			//console.log(doptions);
			$('#town').html(doptions);
			$('#barangay').html('<option value="">-- Select Barangay --</option>');
		}	
	});	
});


$('#town').change(function(){
	var town_id = $('#town option:selected').val();
	//alert(town_id);
	$.ajax({
		url: "<?php echo site_url("ajax/listBrgyByTown"); ?>/?town_id="+town_id,
		dataType: "json",
		success: function(data){
			console.log(data);
			var doptions = make_options_barangay(data);
			//console.log(doptions);
			$('#barangay').html(doptions);
		}	
	});	
});

// This is for PROGRAMS
function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}


function make_options_towns(data){
	var doptions = '<option value="">-- Select Town --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].name
		 	+ '</option>';
	}
	return doptions;
}


function make_options_barangay(data){
	var doptions = '<option value="">-- Select Barangay --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].name
		 	+ '</option>';
	}
	return doptions;
}


</script>