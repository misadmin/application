<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Use to trap old browsers
 * @author Kevin Felisilda
 *
 */
class Browser_trap {

	private $browser_name;
	private $browser_version;
	
	function check_browser(){
		//$this->output->append_output('');
		//echo $this->uri->segment(1);
		$CI =& get_instance();
		$allowed_browsers = $CI->config->item('allowed_browsers');
		$this->browser_name = strtolower($CI->agent->browser());
		$this->browser_version = $CI->agent->version();
		
		if (array_key_exists($this->browser_name, $allowed_browsers) AND $this->browser_version < $allowed_browsers[$this->browser_name]){
			$CI->load->view('error/unsupported_browser',array('minimum_browser'=>array('name'=>$this->browser_name, 'version'=>$allowed_browsers[$this->browser_name])));
			$CI->output->_display();
			exit();
		}
	}

}

/* End of file hooks.php */
/* Location: ./application/hooks/browser_trap.php */