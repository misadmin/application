<style>
	.table th {text-align:center; vertical-align:middle;}
	td.right {text-align:right}
	td.center {text-align:center}
</style>

<?php
	//$errors = ['idno'=>'1234','student'=>'Juan dela cruz','program'=>'ab philo','yr_level'=>'4'];
	//$errors = [];
?>
<div class="row-fluid span7">

	<h3> Assessment posting cannot continue, zero paying units courses found!</h3>
	<hr>
	<table id="post_result" class="table table-bordered table-striped table-condensed">
	    <thead>
			<tr>
				<th width="30%">College</th>
				<th width="20%">Course Code</th>
				<th width="45%">Descriptive Title</th>
				<th width="5%">Paying Unit</th>      
			</tr>
	    </thead>
	
	<?php
		$cnt=0; 
		foreach ($errors AS  $error) {
	?>
 		<tr> 
			<td class="center"><?php echo $error->name; ?></td> 
			<td><?php echo $error->course_code; ?></td> 
			<td><?php echo $error->descriptive_title; ?></td> 
			<td class="center"><?php echo $error->paying_units; ?></td> 
		</tr>		
	<?php
		}
	?>
	</table>
<?php
$this->content_lib->set_message('Please refer to the dean offering the subject!','alert-error');
?>
</div>

<script>
$(document).ready(function(){
	var oTable = $("#post_result").dataTable({
		"iDisplayLength": 100,
		"aLengthMenu": [[50, 100, 1000, -1], [50, 100, 1000, "All"]],
	});

	oTable.fnSort( [ [1,'asc'], ] );
	

});	

</script>
