<h3 class="heading">Update Student to:</h3>
<form method="post" action="" id="update_student">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value="update_student" />	
	<div class="btn-group">
	  <input type="submit" class="btn btn-success" id="update_basic_ed_student" name="what" value="Basic Ed" />
	  <input type="submit" class="btn btn-success" id="update_college_student" name="what" value="College" />
	</div>
</form>

