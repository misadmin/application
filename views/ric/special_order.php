<h2 class="heading">Special Order</h2>
<?php 
	if ($graduated_programs) {
?>
<table class="table table-condensed" style="width:50%; border: solid; border-width: 1px; 
				border-color: #D8D8D8;">
	<form method="POST" target="_blank">
  		<input type="hidden" name="action" value="generate_special_order_pdf" />
 	<thead>
		<tr>
			<th colspan="3">Generate Special Order to PDF</th>
		</tr>
	</thead>
	<tr>
		<td colspan="3">
			<input type="checkbox" name="grant_status" value="Y" checked="checked" style="margin-right:10px;">Granted Automous Status?
		</td>
	</tr>
	<tr style="vertical-align:middle">
		<td style="width:auto;">Graduated Program</td>
		<td style="width:2%">:</td>
		<td>
			<select name="graduated_students_id" style="width:auto;">
				<?php 
					foreach($graduated_programs AS $g_program) {
						print("<option value=\"".$g_program->id."\" >".$g_program->program_name."</option>");
					}
				?>
			</select>	
		</td>
	</tr>
	<?php 
		if ($with_dwct) {
	?>	
	<tr style="vertical-align:middle">
		<td>Accredition Level</td>
		<td>:</td>
		<td>
		<select name="level" style="width:auto;">
			<option value="0">None</option>
			<option value="I">Level I</option>
			<option value="II">Level II</option>
			<option value="III">Level III</option>
			<option value="IV">Level IV</option>
		</select>
		</td>
	</tr>	
	<tr style="vertical-align:middle">
		<td>Graduate Type</td>
		<td>:</td>
		<td>
		<select name="grad_type" style="width:auto;">
			<option value="G">Graduated</option>
			<option value="C">Completed</option>
		</select>
		</td>
	</tr>	
	<tr style="vertical-align:middle">
		<td>College</td>
		<td>:</td>
		<td>
		<select name="college" style="width:auto;">
			<option value="College of Business and Accountancy">College of Business and Accountancy</option>
			<option value="College of Arts and Sciences">College of Arts and Sciences</option>
			<option value="College of Education">College of Education</option>
			<option value="College of Engineering">College of Engineering</option>
			<option value="College of Computer Studies">College of Computer Studies</option>
			<option value="College of Nursing">College of Nursing</option>
			<option value="College of Law">College of Law</option>
		</select>
		</td>
	</tr>	
	<tr style="vertical-align:middle">
		<td>Graduation Date</td>
		<td>:</td>
		<td><input type="date" name="grad_date" style="width:130px;" value="<?php	print(DATE('Y-m-d')); ?>">
		</td>
	</tr>	
	<tr style="vertical-align:middle">
		<td>Accreditor</td>
		<td>:</td>
		<td><input type="text" name="accreditor" style="width:300px;"></td>
	</tr>	
	<tr style="vertical-align:middle">
		<td>Accreditor Acronym</td>
		<td>:</td>
		<td><input type="text" name="accreditor_acronym" style="width:80px;"></td>
	</tr>	
	
	
	
	<?php 
		}
	?>
	<tr style="vertical-align:middle">
		<td style="width:auto;">Date Issued</td>
		<td style="width:2%">:</td>
		<td><input type="date" name="date_issued" style="width:130px;" value="<?php	print(DATE('Y-m-d')); ?>"></td>
	</tr>	
	<tr>
		<td colspan="3">
			<input type="submit" value="Proceed >>" class="btn btn-primary">
		</td>
	</tr>	
	</form>
</table>
<?php 
	} else {
?>
	<span style="font-weight: bold; margin-left: 20px; font-size: 16px; color: #FF0000;">SET FIRST THE GRADUATED PROGRAM!</span>
<?php 
	}
?>

