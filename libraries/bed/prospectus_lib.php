<?php

	class prospectus_lib {
		
		/**
		 * Codeigniter Instance
		 * @var unknown_type
		 */
		protected $CI;
		
		/**
		 * Constructor
		 */
		public function __construct(){
			$this->CI =& get_instance();
		}

		
		
		/*
		 * @added: 30/01/2019
		 * @author: genes
		 */
		public function prospectus_management() {
			
			$this->CI->load->model('hnumis/bed/prospectus_model');
			$this->CI->load->model('hnumis/bed/year_level_model');
			
			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_prospectus');
			
			switch ($action) {
				case 'list_prospectus':
		
					$data['prospectuses'] 	= $this->CI->prospectus_model->ListProspectuses();
					$data['yr_levels']      = $this->CI->year_level_model->ListYearLevels('All');
					
					break;
					
				case 'add_prospectus':

					$data['academic_programs_id'] = $this->CI->input->post('yr_level_id');
					$data['effective_year']       = $this->CI->input->post('effective_year');
					$data['inserted_by']  		  = $this->CI->session->userdata('empno');
					
					if ($this->CI->prospectus_model->AddProspectus($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}

					$data['prospectuses']	= $this->CI->prospectus_model->ListProspectuses();
					$data['yr_levels']      = $this->CI->year_level_model->ListYearLevels('All');

					$my_return['output'] = $this->CI->load->view('bed/prospectus/prospectus_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'delete_prospectus':

					if ($this->CI->prospectus_model->DeleteProspectus($this->CI->input->post('prospectus_id'))) {
						$my_return['msg'] = TRUE;
					} else {
						$my_return['msg'] = FALSE;
					}

					$data['prospectuses'] 	= $this->CI->prospectus_model->ListProspectuses();
					$data['yr_levels']    	= $this->CI->year_level_model->ListYearLevels('All');

					$my_return['output'] = $this->CI->load->view('bed/prospectus/prospectus_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'update_prospectus':

					$data['id']             = $this->CI->input->post('prospectus_id');
					$data['effective_year'] = $this->CI->input->post('effective_year');
					$data['status']         = $this->CI->input->post('status');
					
					if ($this->CI->prospectus_model->UpdateProspectus($data)) {
						$my_return['msg'] = TRUE;
					} else {
						$my_return['msg'] = FALSE;
					}

					$data['prospectuses'] 	= $this->CI->prospectus_model->ListProspectuses();
					$data['yr_levels']    	= $this->CI->year_level_model->ListYearLevels('All');

					$my_return['output'] = $this->CI->load->view('bed/prospectus/prospectus_details', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
			}

			$this->CI->content_lib->enqueue_body_content('bed/prospectus_management',$data);
			$this->CI->content_lib->content();
				
		}
		
		
		public function prospectus_courses_management($prospectus_id=NULL) {

			$this->CI->load->model('hnumis/bed/prospectus_model');
			$this->CI->load->model('hnumis/bed/subject_model');
			//$this->CI->load->model('hnumis/shs/prerequisites_model'); 
			$this->CI->load->model('hnumis/bed/course_offerings_model'); 
					
			
			$data['subjects']  		= $this->CI->subject_model->ListSubjects(14);
			$data['prospectuses'] 	= $this->CI->prospectus_model->ListProspectuses();
			$data['s_types']      	= $this->CI->subject_model->ListSubjectTypes();

			$data['yr_level']		= $this->CI->prospectus_model->getYrLevelByProspectusID($prospectus_id);

			$action = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'proceed_to_prospectus_courses');
			
			switch ($action) {
				case 'proceed_to_prospectus_courses':
				
					$data['prospectus_courses'] = $this->CI->prospectus_model->ListProspectusTerms_Courses($prospectus_id);
					$data['prospectus_id']      = $prospectus_id;

					break;
					
				case 'add_prospectus_term':

					$data['prospectus_id'] = $prospectus_id;
					$data['year_level']    = $this->CI->input->post('year_level');
					$data['term']          = 1;
					$data['inserted_by']   = $this->CI->session->userdata('empno');
					
					if ($this->CI->prospectus_model->AddProspectusTerm($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}

					$data['prospectus_courses'] = $this->CI->prospectus_model->ListProspectusTerms_Courses($prospectus_id);

					$my_return['output'] = $this->CI->load->view('bed/prospectus/list_all_prospectus_courses', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'change_yr_level':
					
					$data['prospectus_courses'] = $this->CI->prospectus_model->ListProspectusTerms_Courses($prospectus_id);
					$data['prospectus_id']      = $prospectus_id;

					$my_return['output'] = $this->CI->load->view('bed/prospectus_courses_management', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'add_prospectus_term_course':
				
					$data['prospectus_id']       = $prospectus_id;
					$data['prospectus_terms_id'] = $this->CI->input->post('term_id');
					$data['courses_id']          = $this->CI->input->post('course_id');
					$data['subject_types_id']    = $this->CI->input->post('stype_id');
					$data['inserted_by']         = $this->CI->session->userdata('empno');

					if ($this->CI->prospectus_model->AddProspectusTermCourse($data)) {
						$my_return['msg']    = "OK";
					} else {
						$my_return['msg']    = "ERROR";
					}
					
					$data['prospectus_courses'] = $this->CI->prospectus_model->ListProspectusTerms_Courses($prospectus_id);

					$my_return['output'] = $this->CI->load->view('bed/prospectus/list_all_prospectus_courses', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;

				case 'delete_prospectus_course':
				
					$data['prospectus_id']        = $prospectus_id;
					$data['courses_id']           = $this->CI->input->post('courses_id');
					$data['prospectus_course_id'] = $this->CI->input->post('prospectus_course_id');
					
					$offered = $this->CI->course_offerings_model->CheckIfCourseOffered($data);
					
					if (!$offered) {
						if ($this->CI->prospectus_model->DeleteProspectusTermCourse($this->CI->input->post('prospectus_course_id'))) {
							$my_return['success'] = TRUE;
							$my_return['msg_error'] = "Course deleted!";
						} else {
							$my_return['success'] = FALSE;
							$my_return['msg_error'] = "Course not deleted!";
						}
					} else {
						$my_return['success']   = FALSE;
						$my_return['msg_error'] = "Course has already offerings!";
					}
					
					$data['prospectus_courses'] = $this->CI->prospectus_model->ListProspectusTerms_Courses($prospectus_id);

					$my_return['output'] = $this->CI->load->view('bed/prospectus/list_all_prospectus_courses', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
				
				case 'delete_prospectus_term':
				
					$data['prospectus_id'] = $prospectus_id;

					if ($this->CI->prospectus_model->DeleteProspectusTerm($this->CI->input->post('prospectus_term_id'))) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}
					
					$data['prospectus_courses'] = $this->CI->prospectus_model->ListProspectusTerms_Courses($prospectus_id);

					$my_return['output'] = $this->CI->load->view('bed/prospectus/list_all_prospectus_courses', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
					
				case 'update_prospectus_course_type':
				
					$data['prospectus_id']         = $prospectus_id;
					$data['subject_types_id']      = $this->CI->input->post('stype_id');
					$data['prospectus_courses_id'] = $this->CI->input->post('prospectus_courses_id');					

					if ($this->CI->prospectus_model->UpdateCourseType($data)) {
						$my_return['success'] = TRUE;
					} else {
						$my_return['success'] = FALSE;
					}
					
					$data['prospectus_courses'] = $this->CI->prospectus_model->ListProspectusTerms_Courses($prospectus_id);

					$my_return['output'] = $this->CI->load->view('bed/prospectus/list_all_prospectus_courses', $data, TRUE);
					
					echo json_encode($my_return,JSON_HEX_APOS);
				
					return;
															
			}

			$this->CI->content_lib->enqueue_body_content('bed/prospectus_courses_management',$data);
			$this->CI->content_lib->content();
				
		}
		
	}
	
?>