<style>
	td.grade_header {
		background-color:#E7E8E7;
		font-size:bold;
		text-align:center;
		font-size:12px;
		vertical-align:middle;
		height:auto;
		padding:2px;
	}

</style>

	<div style="width:100%;" id="show_student_grades">
		<?php 
			if ($grades) {
				foreach($grades AS $term) {
		?>
					<div style="font-size:15px; font-weight:bold; color:#025A0E; border:solid 1px #D6D8D6; background-color:#C3C3C3;
						height:24px; padding-left:5px; padding-top:7px;" >
						<?php print($term->term.' - '.$term->sy); ?>
					</div>

					<div style="width:100%; overflow:auto; margin-top:2px; margin-bottom:25px;">
						<table class="table table-hover table-bordered table-striped" >
							<thead>
								<tr style="text-align:center; font-size:11px; font-weight:bold;" >
									<td class="grade_header" rowspan="2" style="width:10%;">Course<br>Code</td>
									<td class="grade_header" rowspan="2" style="width:42%;">Descriptive Title</td>
									<td class="grade_header" rowspan="2" style="width:30%;">Teacher</td>
									<td class="grade_header" colspan="2" style="width:12%;">Quarter</td>
									<td class="grade_header" rowspan="2" style="width:6%;">Final<br>Grade</td>
								</tr>
								<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
									<td class="grade_header" style="width:6%;">1</td>
									<td class="grade_header" style="width:6%;">2</td>
								</tr>
							</thead>
							<tbody>
								<?php
									if (isset($term->grades)) {
										foreach($term->grades AS $grade) {
								?>			
											<tr>
												<td style="text-align:center;"><?php print($grade->course_code); ?></td>
												<td><?php print($grade->descriptive_title); ?></td>
												<td><?php print($grade->teacher); ?></td>
												<td style="text-align:center; font-weight:bold;">
													<?php 
														if (isset($can_edit)) {
															if ($grade->midterm_confirm_date AND $grade->midterm_grade) {
													?>			
																<div id="show_icon_Midterm_<?php print($grade->enrollments_id); ?>" >
																	<a href="#" 
																		enrollments_id="<?php print($grade->enrollments_id); ?>"
																		period="Midterm"
																		old_grade="<?php print($grade->midterm_grade); ?>"
																		course="<?php print($grade->descriptive_title); ?>"
																		class="change_grade" ><?php print($grade->midterm_grade); ?>
																	</a>
																</div>
													<?php 
															} elseif ($grade->midterm_confirm_date) {
													?>
																<div id="show_icon_Midterm_<?php print($grade->enrollments_id); ?>" >
																	<a href="#" 
																		enrollments_id="<?php print($grade->enrollments_id); ?>"
																		period="Midterm"
																		old_grade="<?php print($grade->midterm_grade); ?>"
																		course="<?php print($grade->descriptive_title); ?>"
																		class="change_grade" ><i class="icon-edit"></i>
																	</a>
																</div>
													<?php 
															}
														} else {
															print($grade->midterm_grade);
														}
													?>
												</td>
												<td style="text-align:center; font-weight:bold;">
													<?php 
														if (isset($can_edit)) {
															if ($grade->finals_confirm_date AND $grade->finals_grade) {
													?>			
																<div id="show_icon_Finals_<?php print($grade->enrollments_id); ?>" >
																	<a href="#" 
																		enrollments_id="<?php print($grade->enrollments_id); ?>"
																		period="Finals"
																		old_grade="<?php print($grade->finals_grade); ?>"
																		course="<?php print($grade->descriptive_title); ?>"
																		class="change_grade" ><?php print($grade->finals_grade); ?>
																	</a>
																</div>
													<?php 
															} elseif ($grade->finals_confirm_date) {
													?>
																<div id="show_icon_Finals_<?php print($grade->enrollments_id); ?>" >
																	<a href="#" 
																		enrollments_id="<?php print($grade->enrollments_id); ?>"
																		period="Finals"
																		old_grade="<?php print($grade->finals_grade); ?>"
																		course="<?php print($grade->descriptive_title); ?>"
																		class="change_grade" ><i class="icon-edit"></i>
																	</a>
																</div>
													<?php 
															}
														} else {
															print($grade->finals_grade);
														}
													?>
												</td>
												<td style="text-align:center; font-weight:bold;">
													<?php //print($grade->finals_grade); ?>
												</td>
											</tr>
								<?php 
										}
									}
								?>
							</tbody>
						</table>
					</div>

		<?php 
				}
			}
		?>
	</div>
	
	
<script>
	$('.change_grade').click(function(event){
		event.preventDefault();

		var enrollments_id = $(this).attr('enrollments_id');
		var period         = $(this).attr('period');
		var course         = $(this).attr('course');
		
		$('#enrollments_id').val(enrollments_id);
		$('#period').val(period);
		$('#old_grade').val($(this).attr('old_grade'));
		$('#course').html($(this).attr('course'));
		
		$('#modal_change_grade').modal('show');
	});
</script>				


	<div id="modal_change_grade" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="change_grade_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Change Grade</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="enrollments_id"></div>
						<div id="course" style="font-weight:bold; margin-bottom:5px;"></div>						
						<?php														
							$this->load->view('shs/registrar/modal_view/shs_change_grade_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Change Grade!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<!-- JQ to submit an update to a status -->
<script>
	$('#change_grade_form').submit(function(e) {
		e.preventDefault();
		
		var enrollments_id = $('#enrollments_id').val();
		var period         = $('#period').val();
		var new_grade      = $('#new_grade').val();
		var student_idno   = '<?php print($idnum); ?>';

		$('#modal_change_grade').modal('hide');

		$('#show_icon_'+period+'_'+enrollments_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+student_idno,
			data: {"enrollments_id": enrollments_id,
					"period": period,
					"new_grade": new_grade,
					"action": "change_student_grade" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_student_grades').html(response.output); 
			}
		});
		
	});
</script>				

	