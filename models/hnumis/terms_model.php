<?php

	class Terms_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}
	function ListTerms() {
			$result = 0;
			$q = "
					SELECT a.terms_name, a.id
					FROM
						terms as a
					ORDER BY
					    a.id
				";
			
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}

    	
	function ListAcademicTerms($current_aca_year_id) {
			$result = 0;
			$q = "SELECT a.term,
					CASE a.term
							WHEN 1 THEN '1st Semester' 
							WHEN 2 THEN '2nd Semester'
							WHEN 3 THEN 'Summer'
					END AS term_word 
					FROM
						academic_terms as a
					WHERE
						a.academic_years_id = {$this->db->escape($current_aca_year_id)}	
					ORDER BY
					    a.term";
			
			//print_r($q);
			//die();
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}
		
		   	
	function ListLevels() {
			$result = 0;
			$q = "
					SELECT 
						a.id,
						a.level
					FROM
						levels as a
					WHERE 
						status='active'
					AND
						rank > 0
					ORDER BY
					    a.id
				";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}
		
		function ListBasicLevels($rank) {
			$result = 0;
			$q = "SELECT * 
					FROM levels 
					WHERE status = 'active' 
					AND type = 'basic_ed' 
					AND rank >= {$this->db->escape($rank->rank)}	
					ORDER BY
					    rank";
				
			//print($q);
			//die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
		
		}

}

//end of payments_model.php