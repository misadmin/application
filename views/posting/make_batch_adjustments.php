<h2 class="heading">Batch Adjustments </h2>
<body onLoad="show_offering();">

<form id="new_adjustment_form" method="post" action="" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value="batch" />
	
	<label class="control-label">Batch Type&nbsp;&nbsp;:</label>
	<div class="controls">
		<label class="radio">
			<input name="batch_type" type="radio" class="form" onClick="show_offering();" value="O" checked="checked"/> Course Offering
		</label>
		<label class="radio">
			<input name="batch_type" type="radio" class="form" onClick="show_program();" value="B"/> Basic Education
		</label>
	</div>
	
	
	<div class="control-group" id="offering">
	    <label class="control-label" for="offering">Course Offerings&nbsp;&nbsp;:</label>
	    <div class="controls ">
		     <select name="offering" class="span5">
				 <?php
					foreach($offerings AS $course) {
													print("<option value=".$course->id.">". $course->course_code. " [".$course->section_code."] ".$course->timeslot." - " .$course->emp_name. "</option>");	
													}  
				?> 
			</select>
    	</div>
    </div>
 	
 	<div id="program">
	    <div class="control-group">
		    <label class="control-label" for="level">Level&nbsp;&nbsp;:</label>
		    <div class="controls">
			    <select id="levels" name="level" class="form">
					<option value="">-- Select Level --</option>
						<?php foreach ($levels as $level): ?>
						<option value="<?php echo $level->id; ?>"><?php echo $level->level; ?></option>
						<?php endforeach; ?>
				</select>
	    	</div>
	    </div>
	    
	     <div class="control-group">
		    <label class="control-label" for="grade_level">Grade/Year Level&nbsp;&nbsp;:</label>
		    <div class="controls">
			    <select id="year" name="year" class="form">
					<option value="">Select Year</option>
				</select>
	    	</div>
	    </div>
	    
	    <div class="control-group">
		    <label class="control-label" for="section">Section&nbsp;&nbsp;:</label>
		    <div class="controls">
			    <select id="section" name="section" class="form">
					<option value="">-- Select Section --</option>						
				</select>
	    	</div>
	    </div>
    </div>
 	
 	
    
    <div class="control-group">
	    <label class="control-label" for="date_adjustment">Date of Adjustment&nbsp;&nbsp;:</label>
	     <div class="controls">
		     <div class="input-append date" id="dp2" data-date-format="yyyy-mm-dd">
				 <input type="text" name="date_taken" id="date_taken" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
				 <span class="add-on">
				 	<i class="icon-calendar"></i>
				 </span>
			 </div>
		 </div>
	</div>
	
	<div class="control-group">
	    <label class="control-label" for="description">Adjustment Description&nbsp;&nbsp;:</label>
	    <div class="controls">
		    <select id="description" name="description">
				 <?php 
						foreach ($trans_types as $type): ?>
						<option value="<?php echo $type->description2; ?>"> 
						<?php echo $type->description2; ?></option>
						<?php endforeach; ?>
			</select>
    	</div>
    </div>
    
    <div class="control-group">
	    <label class="control-label" for="description">Amount&nbsp;&nbsp;:</label>
	    <div class="controls">
		   <input type="text" style="text-align:right" class="span2" id="amount" name="amount" placeholder="Amount"/>
    	</div>
    </div>
	
	<div class="control-group radio_buttons required">
  		<label class="radio_buttons required control-label">Adjustment Type&nbsp;&nbsp;:</label>
    	<div class="controls">
      		<label class="radio inline">
        		<input name="type" type="radio" class="form" value="Debit"/>Debit</label>
      		<label class="radio inline">
       			<input name="type" type="radio" class="form" value="Credit"/>Credit</label>
    	</div>
	</div>
	
	
	<div class="control-group">
	    <label class="control-label" for="remarks">Remarks&nbsp;&nbsp;:</label>
	    <div class="controls">
	     	<textarea id="remarks" name="remarks" class="input-xlarge field span10" rows="8" placeholder="Please limit your remark to a maximum of 200 characters :)" maxlength="200"></textarea>
		</div>
    </div>
    
    <div class="control-group">
    	 <div class="controls">
    		<button class="btn btn-primary" type="submit">Adjust Student Ledger</button>
    	</div>
	</div>
</form>

</body>


<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = '';
  }

function show_offering()
{
	show('offering');
	hide('program');
}
function show_program()
{
	hide('offering');
	show('program');
}
</script>

<!-- 
<body onLoad="show_offering();">

<div style="background:#FFF; width:50%; text-align:center; margin:auto;" align="center">
	<form id="new_adjustment_form" method="post" action="" class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value="batch" />
	<table height="354" align="left" cellpadding="0" cellspacing="0" class="head1" style="width:100%; margin-top:10px;">
  		<tr class="head">
			<td colspan="3" class="head" style="text-align:center; padding:10px;">  Make Batch Adjustments </td>
		</tr>
  		<tr>
    		<td colspan="3">  
  				<table border="0" cellpadding="2" cellspacing="0" class="inside" align="left">
					<tr>
						<td width="97" align="left" valign="middle">Batch Type</td>
						<td width="3" align="left" valign="middle">:</td>
						<td width="401" style="text-align:left; vertical-align:middle;">
							<input name="batch_type" type="radio" class="form" onClick="show_offering();" value="O" checked="checked"/> Course Offering
							<input name="batch_type" type="radio" class="form" onClick="show_program();" value="B"/> Basic Education  </td>
					</tr>
					<tr>
				  		<td colspan="3">
							<table style="width:100%;" border="0" cellspacing="0" cellpadding="0" id="offering">
    							<tr>
									<td width="30%" align="left">Course Offerings</td>
									<td width="2%" align="left">:</td>
									<td width="68%" align="left">
									<select name="offering" class="form">
								  <?php
											foreach($offerings AS $course) {
												print("<option value=".$course->id.">". $course->course_code. " [".$course->section_code."] ".$course->timeslot." - " .$course->emp_name. "</option>");	
											}  ?> </select> </td>
   	 							</tr>
  							</table>
    						<table style="width:100%;" border="0" cellspacing="0" cellpadding="0" id="program">
								<tr>
									<td width="30%" align="left">Level:</td>
									<td width="2%" align="left">:</td>
									<td width="68%" style="text-align:left;">
										<select id="levels" name="level" class="form">
											<option value="">-- Select Level --</option>
											<?php foreach ($levels as $level): ?>
												<option value="<?php echo $level->id; ?>"><?php echo $level->level; ?></option>
											<?php endforeach; ?>
										</select>  </td>
								</tr>
								<tr>
									<td width="30%" align="left">Grade/Year Level:</td>
									<td width="2%" align="left">:</td>
									<td width="68%" style="text-align:left;">
										<select id="year" name="year" class="form">
											<option value="">Select Year</option>
																					
										</select>  </td>
								</tr>
								<tr>
									<td width="30%" align="left">Section:</td>
									<td width="2%" align="left">:</td>
									<td width="68%" style="text-align:left;">
										<select id="section" name="section" class="form">
												<option value="">-- Select Section --</option>						
										</select>  </td>
								</tr>
								
			  				</table>
    					</td>
  					</tr>
  					<tr align="left">
						<td width="30%"> Date of Adjustment</td>
						<td width="2%"> : </td>
						<td width="68%" class="input-append date" id="dp2" data-date-format="yyyy-mm-dd">
							<input type="text" name="date_taken" id="date_taken" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
							<span class="add-on"><i class="icon-calendar"></i></span></td>
					</tr>
					<tr align="left">
						<td width="30%"> Adjustment Description </td>
						<td width="2%"> : </td>
						<td width="68%"> <select id="description" name="description">
									<?php 
										foreach ($trans_types as $type): ?>
												<option value="<?php echo $type->description; ?>"> <?php echo $type->description; ?></option>
									<?php endforeach; ?>
									</select>  </td>
					</tr>
					
					<tr align="left">
						<td width="30%"> Amount </td>
						<td width="2%"> : </td>
						<td width="68%"> 
							<input type="text" style="text-align:right" class="money input-xlarge wide span8" id="amount" name="amount" placeholder="Amount"/> 	</td>								
					</tr>
					<tr align="left">
						<td width="30%">Adjustment Type </td>
						<td width="2%"> : </td>
						<td width="68%"> 
									<input name="type" type="radio" class="form" value="Debit"/>  Debit
									<input name="type" type="radio" class="form" value="Credit"/> Credit 
					  </td>
					</tr>
					<tr align="left">		
						<td width="30%"> Remarks </td>
						<td width="2%"> : </td>
						<td width="68%">
									<textarea id="remarks" name="remarks" class="input-xlarge"></textarea>
					  </td>
					</tr>

					<tr align="center"> 
						<td colspan="3"> <button class="adjust" type="submit">Adjust Student Ledger</button> </td>
					</tr>
    			</table>
			</td>
 		</tr>
 	</table>
 </form>
</div>
</body>
 -->
<script>
	$(document).ready(function(){
		$('#dp2').datepicker();
	});
	
	$(document).ready(function(){
		$('#new_adjustment_form').validate({
			onkeyup: false,
			errorClass: 'error',
			validClass: 'valid',
			rules: {
				amount: { required: true, minlength: 1 },
				type: {required: true},
			},
			highlight: function(element) {
				$(element).closest('div').addClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						unhighlight: function(element) {
							$(element).closest('div').removeClass("f_error");
							setTimeout(function() {
								boxHeight()
							}, 200)
						},
						errorPlacement: function(error, element) {
							$(element).closest('div').append(error);
						}
		});
	$(".money").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });
});


	$('#year').change(function(){
		var year_id = $('#year option:selected').val();
		var level_id = $('#levels option:selected').val();

		
		$.ajax({
			url: "<?php echo site_url("ajax/sections"); ?>/?year=" + year_id + "&level=" + level_id ,
			dataType: "json",
			success: function(data){
				var doptions = make_options_section(data);
				$('#section').html(doptions);
			}	
		});	
	});



	$('#levels').change(function(){
		var level_id = $('#levels option:selected').val();
		
		$.ajax({
			url: "<?php echo site_url("ajax/year"); ?>/?level=" + level_id,
			dataType: "json",
			success: function(data){
				var doptions = make_options_year(data);
				$('#year').html(doptions);
			}	
		});	
	});


function make_options_year (data){
	var doptions = '<option value="">-- Select Year --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].yr_level
		 	+ '">'
		 	+ data[i].yr_level
		 	+ '</option>';
	}
	return doptions;
}

function make_options_section (data){
	var doptions = '<option value="">-- Select Section --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].section
		 	+ '</option>';
	}
	return doptions;
}

</script>






  