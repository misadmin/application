<?php

	class OtherSchools_Model extends CI_Model {
	
		function __construct() {
        	parent::__construct();
		
   		}


 	//Added: January 3, 2014 by Isah
 	
 	function listOtherSchools($towns_id=NULL)	{
 			
 		$result = null;
 	
 		if ($towns_id == NULL) {
	 		$q = "SELECT
						a.id, a.school, a.school_address, a.towns_id
					FROM 
	 					schools as a
	 				ORDER BY a.school";
 		} else {
	 		$q = "SELECT
						a.id, a.school, a.school_address, a.towns_id
					FROM 
	 					schools as a
	 				WHERE
	 					a.towns_id = {$this->db->escape($towns_id)}
	 				ORDER BY 
	 					a.school ";
 		}
		//die($q);
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 	
 		return $result;
 	}
 	
 	function ListCourses($other_id)	{
 			
 		$result = null;
 	
 		$q = "SELECT
					a.id,
					a.catalog_no,
 				    a.descriptive_title, 
 				    a.units,
 				    a.final_grade,
 				    a.remarks 
			  FROM
 				 other_schools_enrollments as a
 				
 			  WHERE 
 				 a.other_schools_student_histories_id = {$this->db->escape($other_id)} ";
 	
 		$query = $this->db->query($q);

 		////log_message("INFO",print_r($q,TRUE));  //toyet 4.5.2018
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 	
 		return $result;
 	}
 	
 	function ListStudentOtherSchools($idno)	{
 	
 		$result = null;
 	
 		$q = "SELECT
		 		a.id,
		 		CONCAT(c.term_description, ' ', b.end_year-1,'-',b.end_year) AS term,
		 		d.school,
		 		if(d.towns_id, CONCAT(e.name, ', ', f.name, ', ' ,g.name), d.school_address) as school_address,
		 		a.program,
		 		a.academic_years_id,
		 		a.term_id,
		 		a.schools_id,
		 		CONCAT(e.name,', ',f.name,', ',g.name) AS school_address,
		 		d.towns_id
		 	 FROM
 				other_schools_student_histories as a,
 				academic_years as b,
 				term as c,
 				schools as d
 				LEFT JOIN towns as e on d.towns_id = e.id
 					LEFT JOIN provinces as f on e.provinces_id = f.id
 					LEFT JOIN countries as g on f.countries_id = g.id
 			WHERE
 				a.students_idno = {$this->db->escape($idno)}
 				AND a.academic_years_id = b.id
 				AND a.term_id = c.id
 				AND a.schools_id = d.id
 			ORDER BY b.end_year, c.id";
		
 		//die($q); 	
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 		$result = $query->result();
 	}
 	
 	return $result;
 	}
 	
 	
  	function listTermsOther()	{
 			
 		$result = null;
 	
 		$q = "SELECT
					id, terms_id, term_description
				  FROM term
				  ORDER BY terms_id, id ";
 	
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 	
 		return $result;
 	}
 	
 	
 	function listCountries()	{
 	
 		$result = null;
 	
 		$q = "SELECT
					id, name
				  FROM countries
 				  WHERE id != 137
				  ORDER BY name ";
 	
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 	
 		return $result;
 	}
 	
 	
 	function listProvinces()	{
 	
 		$result = null;
 	
 		$q = "SELECT
					id, name, countries_id
				  FROM
 					 provinces
 				  WHERE 
 				    id != 49
 				  ORDER BY name ";
 	
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 	
 		return $result;
 	}
 	
 	function listTowns()	{
 	
 		$result = null;
 	
 		$q = "SELECT
 				id, name, provinces_id
 			FROM
 				towns
 			WHERE 
 				id != 960	
 			ORDER BY name ";
 	
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 		$result = $query->result();
 	}
 	
 		return $result;
 	}


 	//added by Toyet 4.10.2019
 	function listTownsByProvince($province_id)	{
 		$result = null;
 	
 		$q = "SELECT
 				id, name
 			FROM
 				towns t
 			WHERE 
 				t.provinces_id = {$province_id}
 			ORDER BY name ";
 	
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 		$result = $query->result();
 	} 	
 		return $result;
 	}
 	

 	//added by Toyet 4.10.2019
 	function listBrgyByTown($town_id){
 		$result = null;
 	
 		$q = "SELECT
 				id, name
	 		  FROM
	 			barangays b
	 		  WHERE 
	 			b.towns_id = {$town_id}
	 		  ORDER BY name ";
 	
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 		$result = $query->result();
 	} 	
 		return $result;
 	}


 	function insertOtherSchoolsInfo($academicYrId, $students_idno, $terms_id, $school_id, $program) {
 			
 		$q1 = "INSERT INTO
 					 other_schools_student_histories (`students_idno`, `academic_years_id`, `term_id`, `schools_id`, `program`)  					 
 				VALUES('{$students_idno}','{$academicYrId}','{$terms_id}','{$school_id}',{$this->db->escape($program)})";
 		 		
 		if($this->db->query($q1))
 			return $this->db->insert_id();
 		 else
 			return FALSE;
 	}
 	
 	function insertOtherSchoolsCourse($otherSchoolenrollId, $catalogNo, $descriptiveTitle, $units, $finalGrade, $remarks) {
 	
 		$q1 = "INSERT INTO 		 	
 					other_schools_enrollments
						(`other_schools_student_histories_id`, `other_schools_courses_id`, `catalog_no`, `descriptive_title`, `units`, `final_grade`, `remarks`)  					
 				VALUES('{$otherSchoolenrollId}',null,{$this->db->escape($catalogNo)},{$this->db->escape($descriptiveTitle)},{$this->db->escape($units)}, {$this->db->escape($finalGrade)}, {$this->db->escape($remarks)})";
 		//die($q1);
 		if($this->db->query($q1))
 		return $this->db->insert_id();
 		else
 			return FALSE;
 	}
 	
 	//Added: January 6, 2014
 	
 	function getOtherProgram($other_enrollments_id) {
 		
 		$result = null;
 			
 		$q1 = "SELECT 
 					CONCAT(c.term_description, ' ', b.end_year-1,'-',b.end_year) AS term,
 					d.school, a.program
 				FROM
 					other_schools_student_histories as a,
 					academic_years as b,
 					term as c,
 					schools as d
 		 		WHERE
			 		a.academic_years_id = b.id
			 		AND a.term_id = c.id
			 		AND	a.schools id = d.id
			 		AND a.id = {$this->db->escape($other_enrollments_id)} ";
 		
 		$query = $this->db->query($q1);
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->row();
 		}
 		
 		return $result;
 		
 	}
 	
 	function getOtherCourse($other_course_id) {
 			
 		$result = null;
 	
 		$q1 = "SELECT
			 		a.catalog_no, 
			 		a.descriptive_title,
			 		a.units,
			 		a.final_grade,
			 		a.remarks
 				FROM
			 		other_schools_enrollments as a
 				WHERE
	 		 		a.id = {$this->db->escape($other_course_id)} ";
 		
 		$query = $this->db->query($q1);
 	
 		if($query->num_rows() > 0){
 		$result = $query->row();
 	}
 		
 	return $result;
 		
 	}

 	
 	function DeleteOtherSchoolTOR($id) {
 	
 		$query = "DELETE FROM
 						other_schools_student_histories
 					WHERE
 						id = {$this->db->escape($id)} ";
 			
 		//print($query); die();
 		
 		if ($this->db->query($query)) {
 			return TRUE;
 		} else {
 			return FALSE;
 		}
 	
 	}

 	//Added: January 7, 2014 
 	//EDITED: 2/2/14
 	function updatetOtherSchoolsCourse($data) {
 			
 		$q1 = "UPDATE 
 					other_schools_enrollments
 				SET catalog_no = {$this->db->escape($data['catalog_no'])}, 
 					descriptive_title = {$this->db->escape($data['descriptive_title'])},
 					units = {$this->db->escape($data['units'])},
 					final_grade = {$this->db->escape($data['final_grade'])},
 					remarks = {$this->db->escape($data['remarks'])}
 				WHERE
 					id = {$this->db->escape($data['course_id'])}";
 	
 			
 		if ($this->db->query($q1)) {
 			return TRUE;
 		} else {
 			return FALSE;
 		}
 	}
 	
 	//ADDED: 2/2/14
 	function updatetOtherSchoolsHistory($data) {
 			
 		$q1 = "UPDATE 
 					other_schools_student_histories
 				SET 
 					academic_years_id = {$this->db->escape($data['academic_years_id'])}, 
 					term_id = {$this->db->escape($data['term_id'])},
 					schools_id = {$this->db->escape($data['schools_id'])},
 					program = {$this->db->escape($data['program'])}
 				WHERE
 					id = {$this->db->escape($data['other_id'])}";
 	
 		//die($q1);	
 		if ($this->db->query($q1)) {
 			return TRUE;
 		} else {
 			return FALSE;
 		}
 	}

 	
 	//ADDED: 2/2/14
 	function updatetGraduatedProgram($data) {
 			
 		if ($data['taken_type'] == 'hnu') {
 		 	$q1 = "UPDATE 
 					graduated_students
 				SET 
 					student_histories_id = {$this->db->escape($data['histories_id'])}, 
  					other_schools_student_histories_id = NULL,
  					graduation_statement = {$this->db->escape($data['statement'])}
 				WHERE
 					id = {$this->db->escape($data['graduated_id'])}";
 		} else {
 		 	$q1 = "UPDATE 
 					graduated_students
 				SET 
 					student_histories_id = NULL,
 					other_schools_student_histories_id = {$this->db->escape($data['other_schools_id'])}, 
  					graduation_statement = {$this->db->escape($data['statement'])}
 				WHERE
 					id = {$this->db->escape($data['graduated_id'])}"; 		
 		}

 		if ($this->db->query($q1)) {
 			return TRUE;
 		} else {
 			return FALSE;
 		}
 	}
 	
 	//Added: January 8, 2014
 	
 	function DeleteOtherSchoolCourse($id) {
 	
 		$query = "DELETE FROM
 					other_schools_enrollments
 				WHERE
 					id = {$this->db->escape($id)} ";
 	
 		//print($query); die();
 			
 		if ($this->db->query($query)) {
	 		return TRUE;
	 	} else {
	 		return FALSE;
	 	}
 	
 	}

 	function DeleteSchool($id) {
 	
 		$query = "DELETE FROM
				 			schools
				 		WHERE
				 			id = {$this->db->escape($id)} ";
				 	
 		//print($query); die();
 	
 		if ($this->db->query($query)) {
	 		return TRUE;
	 	} else {
	 		return FALSE;
	 	}
	 	
 	}
 	
 	
 	function ListStudentSY($student_id) {
 			
 		$result = null;
 			
 		$q1 = "SELECT
 					a.id AS other_schools_id,
 					c.term_description AS term,
 					a.program AS program_name,
 					CONCAT('S.Y. ',e.end_year-1,' - ',e.end_year) AS sy,
 					CONCAT(d.school,' - ',f.name,', ',g.name) AS school_name,
 					e.end_year AS sy_year,
 					c.id AS sy_term
 				FROM
 					other_schools_student_histories AS a,
 					term AS c,
 					schools AS d,
 					academic_years AS e,
 					towns AS f,
 					provinces AS g
 				WHERE
 					a.term_id=c.id
 					AND a.schools_id=d.id
 					AND a.academic_years_id=e.id
 					AND d.towns_id=f.id
 					AND f.provinces_id=g.id
 					AND a.students_idno = {$this->db->escape($student_id)}
 				ORDER BY
 					c.id DESC, e.end_year DESC ";

 		//print_r($q1); die();
        ////log_message('INFO','OTHER GRADUATED');  //Toyet 4.11.2018
		////log_message('INFO',print_r($q1,TRUE));  //toyet 9.20.2017

 		$query = $this->db->query($q1);
 			
 		if($query->num_rows() > 0){
 			$result = $query->result();
 		}
 	
 	return $result;
 	}
 	
 	//Added: Feb. 4, 2014 by Isah
 	
 	function listMasterOtherSchools()	{
 	
 		$result = null;
 	
 		$q = "SELECT * 
 					FROM 
 						schools
 					WHERE 
 						school IS NOT NULL
 						AND towns_id IS NULL
 						AND levels_id = 6	 
 			 		ORDER BY 
 						school";
 		
 		$query = $this->db->query($q);
 	
 			if($query && $query->num_rows() > 0){
 			$result = $query->result();
 	}
 	
 	return $result;
 	}
 	
 	
 	function ListSchools() {
 		$result = null;
 		
 		$q = "SELECT 
 					a.id, 
 					a.abbreviation,	
 					a.school,
 					CONCAT(b.name,', ', c.name,', ',d.name) AS school_address,
 					UPPER(a.status) AS status,
 				    b.id AS town_id, 
 				    c.id AS prov_id,
 				    b.name AS town,
 				    c.name AS prov,
 					d.name AS country
 			   FROM 
 					schools as a
 						LEFT JOIN towns as b ON a.towns_id = b.id
 						LEFT JOIN provinces as c ON b.provinces_id = c.id	
 						LEFT JOIN countries as d ON c.countries_id = d.id								 					 
 				WHERE
 					a.levels_id = 6
 					
 				ORDER BY 
 					a.school
 				";
		//print($q); die();
 		$query = $this->db->query($q);
 		
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();		
 		}
 		
 		return $result;
 			
 	}

 	
 	function listTownsOfPhilippines()	{
 	
 		$result = null;
 	
 		$q = "SELECT
 				a.id, a.name, b.name as province
 			FROM
 				towns as a, provinces as b
 			WHERE
 				a.provinces_id = b.id
 				AND b.countries_id = 137
 			ORDER BY a.name ";
 	
 		$query = $this->db->query($q);
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->result();
 		}
 	
 		return $result;
 	}
 	

 	function insertTownToOtherSchool($school_id, $town_id) {
 	
 		$q1 = "UPDATE 
 					schools
 				SET 
 					towns_id = {$this->db->escape($town_id)} 
 				WHERE
 					id = {$this->db->escape($school_id)}";
 	
 		//die($q1);	
 		if ($this->db->query($q1)) {
 			return TRUE;
 		} else {
 			return FALSE;
 		}
 	}

 	function AddSchool($data) {
 	
 		$q1 = "INSERT INTO
 					schools
 						(abbreviation, school, towns_id, levels_id, status)
 					VALUES ({$this->db->escape($data['school_abbrev'])},
 						{$this->db->escape($data['school_name'])},
 						{$this->db->escape($data['towns_id'])},
 						{$this->db->escape($data['levels_id'])},
 						{$this->db->escape($data['status'])})";
  						
 		//die($q1);
 		if($this->db->query($q1))
 			return $this->db->insert_id();
 		else
 			return FALSE;
 	}
 	
 	//Added: 4/3/2014 by Isah
 	
 	function updatetSchools($data) {
 	
 		$q1 = "UPDATE schools
 				SET
 					school = {$this->db->escape($data['school_name'])},
 					abbreviation = {$this->db->escape($data['school_abbrev'])},
 					towns_id = {$this->db->escape($data['towns_id'])},
 					status = {$this->db->escape($data['status'])}
 	 			WHERE
 	 				id = {$this->db->escape($data['school_id'])}";
 	
 	
 	 	if ($this->db->query($q1)) {
 	 		return TRUE;
 	 	} else {
 	 		return FALSE;
 	 	}
 	 }
 	 
 	 function getOtherSchoolProgram($graduated_students_id) {
 		$result = null;

 		
 		$q1 = "SELECT 
 					b.id AS other_schools_id,
 					b.program
 				FROM
 					graduated_students AS a,
 					other_schools_student_histories AS b
 		 		WHERE
			 		a.other_schools_student_histories_id=b.id
			 		AND a.id = {$this->db->escape($graduated_students_id)} ";
 		
 		//print($q1); die();
 		$query = $this->db->query($q1);
 	
 		if($query && $query->num_rows() > 0){
 			$result = $query->row();
 		}
 		
 		return $result;
 	 	
 	 }
 	
 	 
 	 function getOtherSchoolInfo($school_id) {
 	 	$result = null;
 	 
 	 	$q1 = "SELECT
 	 			
 	 			a.abbreviation, 
 	 			a.school,
 	 			a.status,
 	 			if(a.towns_id, a.towns_id, NULL) as town_id
 	 		FROM
		 	 	schools as a
		 	 		
 					
 	 		WHERE
 	 			a.id = {$this->db->escape($school_id)}
 	 			";
 	 		
 		//print($q1); die();
 	 	$query = $this->db->query($q1);
 	 
 	 	if($query && $query->num_rows() > 0){
 	 	$result = $query->row();
 	 }
 	 	
 	 return $result;
 	 
  }
 	 
 	function getSchoolAddress($town_id) {
 	 		$result = null;
 	 			
 	 		$q1 = "SELECT
 	 	 	 		a.id as town_id,
 	 				a.name as town,
 	 				b.id as province_id,
 	 				b.name as prov,
 	 				c.id as country_id,
 	 				c.name as country
  	 			FROM
 	 				towns as a,
 	 				provinces as b,
 	 				countries as c
 	 			WHERE
 	 				a.id = {$this->db->escape($town_id)}
 	 				AND a.provinces_id = b.id
 	 				AND b.countries_id = c.id
 	 		";
 	 		 
 	 	//print($q1); die();
 	 		$query = $this->db->query($q1);
 	 			
 	 		if($query && $query->num_rows() > 0){
 	 		$result = $query->row();
 	 	}
 	 	
 	 	return $result;
 	 		
 	 	}

 	//Added:  6/23/2014 by isah

 	function listAllProvinces()	{
 	 	
 	 		$result = null;
 	 	
 	 		$q = "SELECT
					id, name, countries_id
				  FROM
 					 provinces
 				  ORDER BY name ";
 	 	
 	 		$query = $this->db->query($q);
 	 	
 	 		if($query && $query->num_rows() > 0){
 	 			$result = $query->result();
 	 		}
 	 	
 	 		return $result;
 	 	}
 	 	
 	 	function listAllTowns()	{
 	 	
 	 		$result = null;
 	 	
 	 		$q = "SELECT
 				id, name, provinces_id
 			FROM
 				towns
 			ORDER BY name ";
 	 	
 	 		$query = $this->db->query($q);
 	 	
 	 		if($query && $query->num_rows() > 0){
 	 			$result = $query->result();
 	 		}
 	 	
 	 		return $result;
 	 	}
 	 	
 	 	function listAllCountries()	{
 	 	
 	 		$result = null;
 	 	
 	 		$q = "SELECT
					id, name
				  FROM countries
 				 
				  ORDER BY name ";
 	 	
 	 		$query = $this->db->query($q);
 	 	
 	 		if($query && $query->num_rows() > 0){
 	 			$result = $query->result();
 	 		}
 	 	
 	 		return $result;
 	 	}
 	 	
}
//end of student_model.php