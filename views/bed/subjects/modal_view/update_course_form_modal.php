	
	<div id="original_course_code"></div>
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Subject Code</div>
			<div class="col2a">
				<div style="float:left;">
					<input type="text" class="form-control search_update_code" id="course_code_update_modal" style="width:100px;" />
				</div>
				<div style="float:left; padding-top:6px; padding-left:4px; font-size:12px; color:#FF0000;" id="show_update_search_code_icon"></div>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Descriptive Title</div>
			<div class="col2a">
				<input type="text" class="form-control" id="descriptive_title_update_modal" style="width:350px;" />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Paying Units</div>
			<div class="col2a">
				<input type="text" class="form-control" id="paying_units_update_modal" style="width:30px;" />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Credit Units</div>
			<div class="col2a">
				<input type="text" class="form-control" id="credit_units_update_modal" style="width:30px;" />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Type</div>
			<div class="col2a">
				<select id="course_types_id_update_modal" >
					<option value="008">Lecture</option>
					<option value="011">Laboratory</option>
					<option value="013">Elective</option>
				</select>
			</div>
		</div>
	</div>

	
<script>
	//$(document).ready(function(){
		$(".search_update_code").blur(function(){
			
			var search_code = $('#course_code_update_modal').val();
			var original_course_code = $('#original_course_code').val();
			
			$('#show_update_search_code_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/subject_management'); ?>",
				data: {"search_code": search_code,
						"original_course_code": original_course_code,
						"action": "search_course_code" },
				dataType: 'json',
				success: function(response) {													
					if (response.found) {
						$('#show_update_search_code_icon').html('Subject Code already exists!'); 
					} else {
						$('#show_update_search_code_icon').html(''); 						
					}
				}
			});
						
		});
	//});
</script>	