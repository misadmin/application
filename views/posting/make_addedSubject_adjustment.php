<div class="fluid span12">
<form id="new_adjustment_form" method="post" action="" class="form-horizontal">
  <input type="hidden" name="action" value="make_adjustments_addedSubject" />
  <input name="tab" value="adjustments" type="hidden" />
  <input name="description" value="<?php print("Add Subject");?>" type="hidden" />
   
  
	<?php $this->common->hidden_input_nonce(FALSE); ?>	
			
			<fieldset>
				
				<div class="control-group formSep">
					<label for="date_taken" class="control-label">Date of Adjustment</label>
					<div class="controls">
						<div class="input-append date" id="dp3" data-date-format="yyyy-mm-dd">
							<input type="text" name="date_taken" id="date_taken" class="span6" value="<?php echo date('Y-m-d'); ?>" readonly/>
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="adjustment" class="control-label">Adjustment Description</label>
					<div class="controls">
						Add Subject
					</div>
				</div>
				
				<div class="control-group formSep">
					<label for="adjustment" class="control-label">Subject</label>
					<div class="controls">
						<select id="tuitionSel" name="tuitionSel">
						<?php 
							foreach ($addedSubjects as $type){?>
							
								<option value="<?php echo $type['tuition']."   |   ".$type['id'];?>"> <?php echo $type['course_code']; ?></option>
				
							<?php }?>
						</select>
					</div>
				</div>
						
				<div class="control-group formSep">
					<label for="reference" class="control-label">Amount</label>
					<div class="controls">
						<input type="text" style="text-align:right" class="money input-large" id="get_id_val" name="amount" placeholder="Amount" /> 				
					</div>									
				</div>
				
							
				<div class="control-group formSep">
					<label for="adjustment_type" class="control-label">Adjustment Type</label>
					<div class="controls">
						<input name="type" type="radio" class="form" value="Debit" checked = "checked"/>  Debit
		    		 	<input name="type" type="radio" class="form" value="Credit"/> Credit 
					</div>
				</div>
				
				

				<div class="control-group">
					<div class="controls">
						<button class="adjust" type="submit">Adjust Student Ledger</button>
					</div>
				</div>
			</fieldset>
</form>
</div>


<script>
$(document).ready(function(){
	$('#new_adjustment_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			amount: { required: true, minlength: 1 },
			type: {required: true},
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	$(".money").keydown(function(event) {
		
		// Allow: backspace, delete, tab, escape, period, enter...
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 110 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 || event.keyCode == 13 )) {
                event.preventDefault(); 
            }   
        }
    });

	$(".text_input").keydown(function(event) {
		//we block all double quotes... "
		if ( event.keyCode == 222 ) {
        	event.preventDefault();
        }
    });

</script>


<script>
	$(document).ready(function(){
		$('#dp3').datepicker();
	
	});
</script>

<script>
jQuery(function($){
    var $idval = $('#get_id_val');
    $('select[name="tuitionSel"').change(function(){
        $idval.val($(this).val())
    }).triggerHandler('change')
   
})
</script>
