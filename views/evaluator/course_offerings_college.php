<h2 class="heading">Course Offerings<span class="pull-right"><button id="print_offerings" class="btn btn-success">Print Offerings</button></span></h2>
<div class="row-fluid">
	<div id="search_meta" class="well clearfix">	
		<?php if (isset($start)): ?>
			<span class="pull-left">Showing <?php echo $start; ?>-<?php echo $end; ?> of <?php echo $total; ?></span>
			<span class="pull-right">
				<form method="post">
					<?php $this->common->hidden_input_nonce(FALSE); ?>
					<?php action_form('limit_results', FALSE); ?>
					<select name="college" id="college">
						<option value="">-- Select College --</option>
					</select>
					<select name="programs" id="programs">
						<option value="">-- Select Program --</option>
					</select>
					<select name="offering_status" id="offering_status">
						<option value="">-- Select status --</option>
						<option value="active" <?php if($this->session->userdata('show_only')=='active') echo ' selected="selected" '; ?>>Active</option>
						<option value="dissolved" <?php if($this->session->userdata('show_only')=='dissolved') echo ' selected="selected" '; ?>>Dissolved</option>
					</select>
					<button type="submit" class="btn">Go</button>
				</form>
			</span>
		<?php endif; ?>
		
		<form method="post" id="academic_term_form" class="form-inline pull-right" style="margin-right:5px">
			<?php $this->common->hidden_input_nonce(FALSE); ?>
			<?php action_form('set_academic_term', FALSE); ?>
			<?php //Select Academic Term: ?>
			<select name="academic_term" id="academic_term">
			
				<?php foreach ($academic_terms as $term): ?>
						<option value="<?php echo $term->id; ?>" <?php if($current_academic_term == $term->id) {$academic_term_description = $term->term . " " . $term->sy; echo ' selected="selected" '; }?>><?php echo $term->term . " " . $term->sy; ?></option>
				<?php endforeach; ?>
			</select>
		</form>
		</div>
		
		<!-- a href="#" id="download" title="Download CSV File"><i class="icon-download-alt"></i> Download CSV File</a -->
	</div>

<?php //echo $pagination; ?>
	<table id="course_offerings_table" class="table table-bordered">
		<thead>
			<tr>
				<th>Catalog ID</th>
				<th style="text-align:center" >Section</th>
				<th>Descriptive Title</th>
				<th>Units</th>
				<th>Schedule</th>
				<th>Room</th>
				<th>Enrolled/Max</th>
				<th>Teacher</th>
				<th>Status</th>
			</tr>
		</thead>
<?php if ( isset($courses_offered) && !empty($courses_offered)): ?>		
		<tbody>
<?php foreach($courses_offered as $course): ?>
			<tr>
				<td><?php echo $course->course_code; ?></td>					
				<td style="text-align:center"><a class="course_offerings_id" descriptive_title="<?php echo $course->descriptive_title; ?>" schedule="<?php echo $course->schedule; ?>" room="<?php echo $course->room; ?>" teacher="<?php echo $course->teacher; ?>" status="<?php echo $course->status; ?>" term="<?php echo $academic_term_description; ?>" href="<?php echo $course->course_offerings_id; ?>" course_code="<?php echo $course->course_code; ?>" section="<?php echo $course->section_code; ?>" title="Click to open class list"><?php echo $course->section_code; ?></a></td>
				<td><?php echo $course->descriptive_title; ?></td>
				<td><?php echo $course->credit_units; ?></td>
				<td><?php echo $course->schedule; ?></td>
				<td><?php $room_assignment = explode(" ", $course->room); echo $room_assignment[0]; ?></td>
				<td><?php echo $course->enrolled_count . "/" . $course->max_enrollment_count; ?></td>
				<td><?php echo $course->teacher; ?></td>
				<td><?php echo $course->status; ?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
<?php endif; ?>
	</table>
</div>
<form id="srcform2" method="post">
</form>
<form id="download_form" method="post" action="<?php echo site_url('evaluator/download_offerings'); ?>">
	<?php echo $this->common->hidden_input_nonce(); ?>
	<input type="hidden" name="academic_term" value="<?php echo $current_academic_term; ?>" />
	<input type="hidden" name="college" value="<?php echo $show_college; ?>" />
	<input type="hidden" name="programs" value="<?php echo $show_program; ?>" />
	<input type="hidden" name="offering_status" value="<?php echo $this->session->userdata('show_only'); ?>" />
</form>
<form id="open_class_list" method="post" action="<?php echo site_url('evaluator/class_list'); ?>">
	<?php echo $this->common->hidden_input_nonce(); ?>
</form>

<style>
	form{
	margin:0;
	}
</style>
<script>
	var colleges=<?php echo json_encode($colleges); ?>;
	var show_college = '<?php echo $show_college; ?>';
	var show_program = '<?php echo $show_program; ?>';
	
	$(document).ready(function(){
		
		$('#course_offerings_table').dataTable({
			"iDisplayLength": 25,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		});
		var colleges_as_option = '';
		$.each(colleges, function(key, val){
			colleges_as_option += '<option value="' + val.id + '"';
			if ( val.id == show_college ) {
				colleges_as_option += ' selected="selected"';
			}
			colleges_as_option += '>' + val.name + '</option>'; 
		});
		$('#college').append(colleges_as_option);

		var college_id = show_college;
		if(college_id!=''){
			$.ajax({
				url: "<?php echo site_url('ajax/programs/college='); ?>" + college_id,
				dataType: "json",
				success: function(data){
					var doptions = make_options(data);
					$('#programs').html(doptions);
				}	
			});
		}
	});

	$('#college').change(function(){
		
		var college_id = $('#college option:selected').val();
		$.ajax({
			url: "<?php echo site_url('ajax/programs/?college='); ?>" + college_id,
			dataType: "json",
			success: function(data){
				var doptions = make_options(data);
				$('#programs').html(doptions);
			}	
		});	
	});
		
	
	$('.pagination a').click(function(event){
		event.preventDefault(); 
		var page = $(this).attr('href').substring(1);
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'page',
		    value: page,
		}).appendTo('#srcform2');
		
		$('#srcform2').submit();
	});

	$('#download').bind('click', function(event){
		event.preventDefault(); 
		
		$('#download_form').submit();
	});
	
	$('#academic_term').bind('change', function(event){
		event.preventDefault(); 
		
		$('#academic_term_form').submit();
	});

	$('.course_offerings_id').bind('click', function(event){
		event.preventDefault();
		var course_offerings_id = $(this).attr('href');
		var schedule = $(this).attr('schedule');
		var room = $(this).attr('room');
		var status = $(this).attr('status');
		var teacher = $(this).attr('teacher');
		var section = $(this).attr('section');
		var term = $(this).attr('term');
		var course_code = $(this).attr('course_code');
		var descriptive_title = $(this).attr('descriptive_title');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_offerings_id',
		    value: course_offerings_id,
		}).appendTo('#open_class_list');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'schedule',
		    value: schedule,
		}).appendTo('#open_class_list');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'room',
		    value: room,
		}).appendTo('#open_class_list');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'status',
		    value: status,
		}).appendTo('#open_class_list');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'teacher',
		    value: teacher,
		}).appendTo('#open_class_list');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'section',
		    value: section,
		}).appendTo('#open_class_list');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'term',
		    value: term,
		}).appendTo('#open_class_list');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'course_code',
		    value: course_code,
		}).appendTo('#open_class_list');

		$('<input>').attr({
		    type: 'hidden',
		    name: 'descriptive_title',
		    value: descriptive_title,
		}).appendTo('#open_class_list');
		
		$('#open_class_list').submit();
	});
	
	function make_options (data){
		var doptions = '<option value="">-- Select Program --</option>';
		for (var i = 0; i < data.length; i++) {
			doptions = doptions 
			 	+ '<option value="' + data[i].id + '"';

		 	if (data[i].id==show_program) {
				doptions = doptions + ' selected="selected" ';
		 	}
			doptions = doptions 
				+ '>'
			 	+ data[i].abbreviation
			 	+ '</option>';
		}
		return doptions;
	}	
</script>