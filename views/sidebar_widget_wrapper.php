<div id="accordion<?php echo $id; ?>">
	<div class="accordion-heading">
		<a href="#collapse<?php echo $id; ?>" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle"><?php echo $title; ?></a>
	</div>
	<div class="accordion-body collapse <?php echo $in; ?>" id="collapse<?php echo $id; ?>">
		<div class="accordion-inner">
			<?php echo $content; ?>
		</div>
	</div>
</div>