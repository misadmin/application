
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Grade/Section</div>
			<div class="col2a">
				<input type="text" class="form-control" id="description_update_strand_modal" style="width:210px;" disabled />
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Strand</div>
			<div class="col2a">
				<select id="update_strand_id_modal" class="form-control" style="width:auto;" >
					<?php
						if ($strands) {
							foreach($strands AS $strand) {
					?>
								<option value="<?php print($strand->id); ?>"><?php print($strand->description); ?></option>
					<?php 
							}
						}
					?>
				</select>
			</div>
		</div>
		<div style="width:100%; font-size:12px; font-weight:bold; color:#575555; margin-top:10px;">
			NOTE: You can only select Strands that are <span style="font-weight:bold; color:#E40808;">Offered</span>!
		</div>
	</div>
