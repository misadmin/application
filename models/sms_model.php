<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SMS_Model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }
    
    public function college_mass_sending ($message, $program, $year_level, $academic_term = NULL){
    	$this->db->select('s.idno,s.lname,s.fname,pn.phone_number')
    		->from('students s')
    		->join('student_histories sh', 'sh.students_idno=s.idno', 'left')
    		->join('prospectus p', 'p.id=sh.prospectus_id', 'left')
    		->join('academic_programs ap', 'ap.id=p.academic_programs_id', 'left')
    		->join('academic_terms at', 'at.id=sh.academic_terms_id', 'left')
    		->join('enrollments e', 'e.student_history_id = sh.id', 'inner')
    		->join('phone_numbers pn', 'pn.students_idno=s.idno', 'left')
    		->where('ap.id', $program)
    		->where('sh.year_level', $year_level)
    		->where('pn.is_primary', 'Y')
    		->where('pn.phone_type', 'mobile')
    		->group_by('s.idno')
    		->order_by('lname ASC, fname ASC');
    	
    	if (is_null($academic_term)){
    		$this->db->where('at.status', 'current');
    	} else {
    		$this->db->where('at.id', $academic_term);
    	}
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$messages = array();
			foreach($query->result() as $row){
				if(!is_null($row->phone_number)){
					$messages[] = (object)array('number'=>$this->ten_digit_sms_number($row->phone_number), 'message'=>$message);	
				}	
			}
			return $messages;
		} else {
			return FALSE;
		}
    }
    public function assessment_messages ($template, $academic_terms_id){
    	$this->db->select("DISTINCT
		    	sh.students_idno,
		    	a.id as assessment_id,
		    	a.assessed_amount,
		    	RIGHT(pn.phone_number,10) as phone_number,
		    	CONCAT('SY ', (ay.end_year-1), '-', ay.end_year) as sy,
    			CASE at.term
		    		WHEN 1 THEN '1st Sem'
		    		WHEN 2 THEN '2nd Sem'
		    		WHEN 3 THEN 'Summer'
		    	END AS term
    			", FALSE)
    			->from("student_histories sh")
    			->join("assessments a", "sh.id=a.student_histories_id", "left")
    			->join("phone_numbers pn", "pn.students_idno=sh.students_idno", "left")
    			->join("academic_terms at", "sh.academic_terms_id=at.id", "left")
    			->join("academic_years ay", "ay.id=at.academic_years_id", "left")
    			->where("sh.academic_terms_id", $academic_terms_id)
    			->where("NOT isnull(a.assessed_amount)", null, FALSE)
    			->where("pn.is_primary", "Y")
    			->where("NOT isnull(pn.phone_number)", null, FALSE);
    	$query = $this->db->get();
    	if ($query->num_rows() > 0){
    		$messages = array();
    		foreach ($query->result() as $row){
    			$message = str_replace(array('[term]', "[idno]", "[assessment]"), array($row->term . " " . $row->sy, $row->students_idno, number_format($row->assessed_amount, 2)), $template);
    			$messages[] = (object)array('number'=>$this->ten_digit_sms_number($row->phone_number), 'message'=>$message);
    		}
    		return $messages;	
    	} else {
    		return FALSE;
    	}
    	
    }
    public function ten_digit_sms_number ($sms_number){
		$sms_number = strval($sms_number);
		return substr($sms_number, -10, 10);
	}
}