<div class="row-fluid">
	<?php if(!empty($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
		<a class="close" data-dismiss="alert">&times;</a>
		<?php echo $message; ?>
	</div>
	<?php endif; ?>
	<h2 class="heading">SMS Mass Sending</h2>
	<form method="post" class="form-horizontal">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<div class="row-fluid">
		<div class="span6">
			<h3 class="heading">Recepients</h3>
			<div class="control-group formSep">
				<label class="control-label" for="academic_term_id">Academic Term</label>
				<div class="controls">
					<select name="academic_term_id">
						<?php foreach ($academic_terms as $term): ?>
						<?php if(!is_null($term->term)): ?>
						<option value="<?php echo $term->id; ?>"><?php echo $term->term . " " . $term->sy; ?></option>
						<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="colleges">College</label>
				<div class="controls">
					<select name="college" class="print_selector">
						<option value="">-- Select College --</option>
							<?php foreach ($colleges as $college): ?>
							<option value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
							<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="programs">Program</label>
				<div class="controls">
					<select name="program"  class="print_selector">
						<option value="">-- Select Program --</option>
					</select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" for="year_level">Year Level</label>
				<div class="controls">
					<select name="year_level" class="print_selector">
						<option value="">-- Select Year Level --</option>
						<?php for($count = 1; $count<=12; $count++ ): ?>
						<option value="<?php echo $count; ?>"><?php echo $count; ?></option>
						<?php endfor; ?>
					</select>
				</div>
			</div>
			<h3 class="heading">Message</h3>
			<div class="row-fluid">
				<textarea class="span12 sms_message" name="message"></textarea>
				<span class="label label-warning" style="margin-top:5px;">Maximum: 160 Chars</span><span class="label character_count_label" style="margin-left:5px;">Character Count: <span class="character_count">0</span></span>
			</div>
			<div class="row-fluid" style="margin-top:20px">
				<input class="btn btn-success" type="submit" value="Send Message" />
			</div>
		</div>
		<div class="span6">
			
		</div>
	</div>
</div>
</form>
<script>
$().ready(function(){
	var max_string_count = 160;
	$('#student_levels').hide();
	$('textarea.sms_message').each(function(){
		$(this).parents('div').find('span.character_count').html($(this).val().length);
		$(this).parents('div').find('span.character_count_label').removeClass(function(index, css) {
		    return (css.match (/\label-\S+/g) || []).join(' ');
		});
		if($(this).val().length < (0.8*max_string_count)) {
			$(this).parents('div').find('span.character_count_label').addClass('label-success');
		}
		if($(this).val().length >= (0.8*max_string_count) && $(this).val().length < (0.9*max_string_count)) {
			$(this).parents('div').find('span.character_count_label').addClass('label-warning');
		}
		if($(this).val().length >= (0.9*max_string_count)) {
			$(this).parents('div').find('span.character_count_label').addClass('label-danger');
		}
	});
	$('textarea.sms_message').on('keyup', function(){
		$(this).val($(this).val().substring(0, max_string_count));
		$(this).parents('div').find('span.character_count').html($(this).val().length);
		$(this).parents('div').find('span.character_count_label').removeClass(function(index, css) {
		    return (css.match (/\label-\S+/g) || []).join(' ');
		});
		if($(this).val().length < (0.8*max_string_count)) {
			$(this).parents('div').find('span.character_count_label').addClass('label-success');
		}
		if($(this).val().length >= (0.8*max_string_count) && $(this).val().length < (0.9*max_string_count)) {
			$(this).parents('div').find('span.character_count_label').addClass('label-warning');
		}
		if($(this).val().length >= (0.9*max_string_count)) {
			$(this).parents('div').find('span.character_count_label').addClass('label-important');
		}
	});
	$('select[name="college"]').on('change', function(){
		var college_id = $(this).val();
		var that = $(this);
		$.ajax({
			url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
			dataType: "json",
			success: function(data){
				var doptions = make_options(data);
				that.parents('form').find('select[name="program"]').html(doptions);
			}	
		});	
	});
});
function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}
</script>
