<?php

	class Section_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListSections($grade_level=NULL,$academic_terms_id=NULL,$strand_id=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							a.id,
							a.yr_level,
							e.abbreviation,
							e.description,
							b.section_name,
							CONCAT(c.lname,', ',c.fname) AS class_adviser,
							IF (b.rooms_id,g.room_no,'') AS room_no,
							(SELECT 
									COUNT(DISTINCT(x.students_idno)) 
								FROM 
									student_histories AS x 
								WHERE 
									x.block_sections_id = a.id 
									
							) AS num_students,
							e.status AS strand_status
						FROM 
							block_sections AS a,
							shs_block_sections AS b 
								LEFT JOIN employees AS c ON b.block_adviser=c.empno
								LEFT JOIN rooms AS g ON g.id = b.rooms_id,
							academic_programs AS e
						WHERE 
							a.id = b.block_sections_id
							AND a.academic_programs_id = e.id
							AND a.block_sections_type = 'shs' ";
			
			if ($grade_level) {
				$q1 .= " AND a.yr_level = {$this->db->escape($grade_level)} ";			
			}

			if ($academic_terms_id) {
				$q1 .= " AND b.academic_terms_id = {$this->db->escape($academic_terms_id)} ";					
			}
			
			if ($strand_id) {
				$q1 .= " AND a.academic_programs_id = {$this->db->escape($strand_id)} ";
			}
			
			$q1 .= " ORDER BY 
						a.yr_level,
						e.abbreviation,
						a.section_name ";
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function ListSectionSchedules($section_id=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							e.id AS course_offerings_slots_id,
							e.course_offerings_id,
							g.course_code,
							g.descriptive_title,
							TIME_FORMAT(e.start_time,'%l:%i%p') AS stime,
							TIME_FORMAT(e.end_time,'%l:%i%p') AS etime,
							e.start_time,
							e.end_time,
							f.room_no,
							e.rooms_id AS offerings_slots_rooms_id,
							d.employees_empno,
							CONCAT(h.lname,', ',h.fname) AS teacher,
							(SELECT 
									CONCAT('(',GROUP_CONCAT(QUOTE(x.day_name)),')')
								FROM 
									room_occupancy AS x,
									room_occupancy_course_offerings_slots AS y
								WHERE 
									x.id = y.room_occupancy_id 
									AND y.course_offerings_slots_id = e.id
							) AS days_names,
							(SELECT 
									GROUP_CONCAT(x.day_name)						
								FROM 
									room_occupancy AS x,
									room_occupancy_course_offerings_slots AS y
								WHERE 
									x.id = y.room_occupancy_id 
									AND y.course_offerings_slots_id = e.id
							) AS day_name
						FROM 
							shs_block_sections AS a,
							block_sections AS b,
							block_course_offerings AS c,
							course_offerings AS d 
								LEFT JOIN employees AS h ON d.employees_empno=h.empno,
							course_offerings_slots AS e,
							rooms AS f,
							courses AS g
						WHERE 
							 a.block_sections_id = b.id 
							 AND b.id = c.block_sections_id 
							 AND c.course_offerings_id = d.id 
							 AND d.id = e.course_offerings_id 
							 AND e.rooms_id = f.id 
							 AND d.courses_id = g.id 
							 AND a.block_sections_id = {$this->db->escape($section_id)} 
						ORDER BY 
							e.start_time ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function ListStudentsEnrolled($section_id=NULL, $order_by=NULL) {
			$result = null;
			
			$q1 = "SELECT 
							a.idno,
							a.lname,
							a.fname,
							a.mname,
							a.gender,
							b.year_level,
							CONCAT(a.lname,', ',a.fname,' ',UPPER(LEFT(a.mname,1)),'.') AS name,
							a.dbirth
						FROM 
							students AS a,
							student_histories AS b,
							block_sections AS c,
							shs_block_sections AS d 
						WHERE 
							 a.idno = b.students_idno
							 AND b.block_sections_id = c.id 
							 AND c.id = d.block_sections_id 
							 AND b.block_sections_id = {$this->db->escape($section_id)} 
						GROUP BY 
							a.idno ";
							 
			if ($order_by == 'lname') {
				$q1 .= " ORDER BY 
							a.lname,
							a.fname,
							a.mname ";
			} else {
				$q1 .= " ORDER BY 
							a.gender,
							a.lname,
							a.fname,
							a.mname ";
			}
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}

		
		function AddBlockSection($data) {
				
			$q1 = "INSERT INTO
							block_sections (yr_level,academic_programs_id,block_sections_type,inserted_by)
						VALUES (
							{$this->db->escape($data['yr_level'])},
							{$this->db->escape($data['academic_programs_id'])},
							{$this->db->escape($data['block_sections_type'])},
							{$this->db->escape($data['inserted_by'])} ) ";
			//return $q1;
			if ($this->db->query($q1)) {
				$data['block_sections_id'] = $this->db->insert_id();
				
				if ($this->AddSHSBlockSection($data)) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		}

		
		private function AddSHSBlockSection($data) {
				
			$q1 = "INSERT INTO
							shs_block_sections (block_sections_id,academic_terms_id,section_name,encoded_by)
						VALUES (
							{$this->db->escape($data['block_sections_id'])},
							{$this->db->escape($data['academic_terms_id'])},
							{$this->db->escape($data['section_name'])},
							{$this->db->escape($data['inserted_by'])}) ";

			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
	    function DeleteSection($section_id) {
			
			//Remove rooms assigned
			$q2 = "DELETE FROM 
							room_occupancy 
						WHERE 
							id IN (SELECT 
										a.room_occupancy_id 
									FROM 
										room_occupancy_block_section AS a
									WHERE 
										a.block_sections_id = {$this->db->escape($section_id)}) ";
			//return $q2;
			if ($this->db->query($q2)) {
				$q = "DELETE FROM
							block_sections 
						WHERE
							id = {$this->db->escape($section_id)} ";

				if ($this->db->query($q)) {
					return TRUE;
				} else {
					return FALSE;
				}				
			} else {
				return FALSE;
			}

		}


		function getSection($section_id) {
			$result = null;
			//return $section_id;
			$q1 = "SELECT 
							a.id,
							a.yr_level AS grade_level,
							e.abbreviation,
							e.description AS strand,
							e.id AS strand_id,
							b.section_name,
							b.block_adviser,
							b.academic_terms_id,
							b.rooms_id,
							CONCAT(c.lname,', ',c.fname) AS class_adviser,
							IF (b.rooms_id,g.room_no,'') AS room_no,
							CASE l.term
									WHEN 1 THEN '1st Semester' 
									WHEN 2 THEN '2nd Semester'
									WHEN 3 THEN 'Summer'
							END AS term, 
							CONCAT(m.end_year-1,'-',m.end_year) AS sy,
							e.status AS strand_status,
							l.term AS term_level,
							h.id AS prospectus_id
						FROM 
							block_sections AS a,
							shs_block_sections AS b 
								LEFT JOIN employees AS c ON b.block_adviser=c.empno
								LEFT JOIN rooms AS g ON g.id = b.rooms_id,
							academic_programs AS e 
								LEFT JOIN prospectus AS h ON h.academic_programs_id = e.id,
							academic_terms AS l, 
							academic_years AS m
						WHERE 
							a.id = b.block_sections_id
							AND a.academic_programs_id = e.id
							AND b.academic_terms_id = l.id 
							AND l.academic_years_id = m.id
							AND a.id = {$this->db->escape($section_id)} ";
			
			//return $q1;
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}


		function AssignBlockAdviser($data) {
			$q1 = "UPDATE shs_block_sections
					SET block_adviser = {$this->db->escape($data['block_adviser'])},
						updated_by = {$this->db->escape($data['updated_by'])}
		 			WHERE
						block_sections_id = {$this->db->escape($data['block_sections_id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
			
		}

		function UpdateAssignedStrand($data) {
			$q1 = "UPDATE block_sections
					SET academic_programs_id = {$this->db->escape($data['academic_programs_id'])},
						updated_by = {$this->db->escape($data['updated_by'])}
		 			WHERE
						id = {$this->db->escape($data['block_sections_id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
			
		}

		function UpdateSectionName($data) {
			$q1 = "UPDATE shs_block_sections
					SET section_name = {$this->db->escape($data['section_name'])},
						updated_by = {$this->db->escape($data['updated_by'])}
		 			WHERE
						block_sections_id = {$this->db->escape($data['block_sections_id'])} ";
						
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
			
		}

		
		function AssignRoom_RoomOccupancy($data) {
			
			foreach($data['day_names'] AS $k=>$v) {
				
				$q1 = "INSERT INTO
								room_occupancy (rooms_id,
												academic_terms_id,
												from_time,
												to_time,
												day_name,
												occupancy_type,
												encoded_by)
								VALUES (
									{$this->db->escape($data['rooms_id'])},
									{$this->db->escape($data['academic_terms_id'])},
									{$this->db->escape($data['start_time'])},
									{$this->db->escape($data['end_time'])},
									'".$v."',
									{$this->db->escape($data['occupancy_type'])},
									{$this->db->escape($data['encoded_by'])} ) ";

				$this->db->query($q1);
					
				$data['room_occupancy_id'] = $this->db->insert_id();
						
				if ($data['occupancy_type'] == 'shs_section') {
					$this->AddRoomOccupancyBlockSection($data);
				} else {
					$this->AddRoomOccupancyCourseOfferingsSlots($data);
				}			
			}
			
			return TRUE;
				
		}


		function AssignRoomTo_SHSBlockSection($data) {
			$q1 = "UPDATE shs_block_sections
					SET rooms_id = {$this->db->escape($data['rooms_id'])},
						updated_by = {$this->db->escape($data['encoded_by'])}
		 			WHERE
						block_sections_id = {$this->db->escape($data['block_sections_id'])} ";
					
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
			
		}
		

		private function AddRoomOccupancyBlockSection($data) {
				
			$q1 = "INSERT INTO
							room_occupancy_block_section (room_occupancy_id,
															block_sections_id,
															encoded_by)
						VALUES (
							{$this->db->escape($data['room_occupancy_id'])},
							{$this->db->escape($data['block_sections_id'])},
							{$this->db->escape($data['encoded_by'])}) ";

			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


		private function AddRoomOccupancyCourseOfferingsSlots($data) {
				
			$q1 = "INSERT INTO
							room_occupancy_course_offerings_slots (room_occupancy_id,
																	course_offerings_slots_id,
																	encoded_by)
						VALUES (
							{$this->db->escape($data['room_occupancy_id'])},
							{$this->db->escape($data['course_offerings_slots_id'])},
							{$this->db->escape($data['encoded_by'])}) ";

			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		
		function CheckIfRoomOccupied($data, $section_rooms_id=NULL) {
					
			$q1 = "SELECT 
						d.rooms_id 
					FROM 
						room_occupancy AS d 
					WHERE
						d.academic_terms_id={$this->db->escape($data['academic_terms_id'])}
						AND ({$this->db->escape($data['start_time'])} BETWEEN ADDTIME(d.from_time,'00:01') AND SUBTIME(d.to_time,'00:01') 
							OR {$this->db->escape($data['end_time'])} BETWEEN ADDTIME(d.from_time,'00:01') AND ADDTIME(d.to_time,'00:01')) 
						AND d.day_name IN ".$data['day_names1']."
						AND d.rooms_id = {$this->db->escape($data['rooms_id'])} 
						AND d.rooms_id != {$this->db->escape($section_rooms_id)} ";
			//return $q1;
			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
			
		}


		function DeleteRoomOccupancy($course_offerings_slots_id=NULL) {
			//Remove rooms assigned
			$q2 = "DELETE FROM 
							room_occupancy 
						WHERE 
							id IN (SELECT 
										a.room_occupancy_id 
									FROM 
										room_occupancy_course_offerings_slots AS a
									WHERE 
										a.course_offerings_slots_id = {$this->db->escape($course_offerings_slots_id)}) ";
			//return $q2;
			if ($this->db->query($q2)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
		
		
		function UpdateRoom_RoomOccupancy($data) {
			$q1 = "UPDATE room_occupancy
					SET rooms_id = {$this->db->escape($data['rooms_id'])},
						updated_by = {$this->db->escape($data['updated_by'])}
		 			WHERE
						id IN (SELECT 
									a.room_occupancy_id 
								FROM 
									room_occupancy_block_section AS a
								WHERE 
									a.block_sections_id = {$this->db->escape($data['block_sections_id'])} 
							) ";
			//return $q1;			
			if ($this->db->query($q1)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		
		function DeleteInsertRoom_RoomOccupancy($data) {

			//delete first the old records 
			$q1 = "DELETE FROM 
								room_occupancy 
							WHERE 
								id IN (SELECT 
												a.room_occupancy_id 
											FROM 
												room_occupancy_course_offerings_slots AS a 
											WHERE 
												a.course_offerings_slots_id = {$this->db->escape($data['course_offerings_slots_id'])}
										) ";

			if ($this->db->query($q1)) {
				if ($this->AssignRoom_RoomOccupancy($data)) {  //then insert the new records
					return TRUE;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}

		}


		function ListCoursesAssigned($section_id=NULL) {

			$result = null;
			
			$q1 = "SELECT 
							d.id AS course_offerings_id,
							g.course_code,
							g.descriptive_title,
							g.paying_units,
							g.credit_units,
							a.encoded_by AS enrolled_by,
							a.encoded_on AS date_enrolled,
							CONCAT(h.lname,', ',h.fname) AS teacher,
							a.block_sections_id,
							g.id AS courses_id,
							DATE_FORMAT(i.midterm_date,'%b/%e/%Y @%h:%i%p') AS midterm_date,
							DATE_FORMAT(i.finals_date,'%b/%e/%Y @%h:%i%p') AS finals_date,
							DATE_FORMAT(j.midterm_confirm_date,'%b/%e/%Y @%h:%i%p') AS midterm_confirm_date,
							DATE_FORMAT(j.finals_confirm_date,'%b/%e/%Y @%h:%i%p') AS finals_confirm_date							
						FROM 
							shs_block_sections AS a,
							block_sections AS b,
							block_course_offerings AS c,
							course_offerings AS d 
								LEFT JOIN employees AS h ON d.employees_empno = h.empno
								LEFT JOIN grade_submission_dates AS i ON i.course_offerings_id = d.id
								LEFT JOIN shs_course_offerings_confirmation AS j ON j.course_offerings_id = d.id,
							courses AS g
						WHERE 
							 a.block_sections_id = b.id 
							 AND b.id = c.block_sections_id 
							 AND c.course_offerings_id = d.id 
							 AND d.courses_id = g.id 
							 AND a.block_sections_id = {$this->db->escape($section_id)} 
						GROUP BY  
							g.id
						ORDER BY 
							g.course_code ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		

		function ListLabFees($data) {

			$result = null;
			
			$q1 = "SELECT 
							d.id AS course_offerings_id,
							g.course_code,
							e.rate
						FROM 
							shs_block_sections AS a,
							block_sections AS b,
							block_course_offerings AS c,
							course_offerings AS d,
							laboratory_fees AS e,
							courses AS g
						WHERE 
							 a.block_sections_id = b.id 
							 AND b.id = c.block_sections_id 
							 AND c.course_offerings_id = d.id 
							 AND e.courses_id = d.courses_id
							 AND d.courses_id = g.id 
							 AND a.block_sections_id = {$this->db->escape($data['block_sections_id'])} 
							 AND e.academic_years_id = {$this->db->escape($data['academic_years_id'])} 
							 AND e.levels_id = 12
						GROUP BY 
							g.id
						ORDER BY 
							g.course_code ";
			//die($q1);
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}


		function getBlockSectionAssessment($section_id=NULL) {

			$result = null;

			$q1 = "SELECT 
						a.id,
						DATE_FORMAT(a.transaction_date,'%W, %M %e, %Y @ %h:%i%p') AS date_assessed
					FROM 
						assessments AS a,
						student_histories AS b
					WHERE
						a.student_histories_id = b.id 
						AND b.block_sections_id = {$this->db->escape($section_id)} 
					LIMIT 1 ";

			$query = $this->db->query($q1);
			
			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;		
			
		}
		
		
	}
	
?>
