<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
th.header {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px; 
	color:#000; 
	text-align:center; 
	height:25px;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
th.head {
	padding:3px;
	text-align:center;
}

</style>
<div style="background:#FFF; width:920px;" align="center">
<div style="width:100%; margin-bottom:40px;" align="center">	

<table border="0" cellspacing="0" class="table table-bordered table-hover table-condensed" style="width:100%; padding:0px; margin:0px;">
	 <thead> 
	 	
		<tr class="head">
  			<th colspan="7" class="head" style="text-align:center; padding:10px;">  SUMMARY OF UNPOSTED ADJUSTMENTS </th>
  	   </tr>
    
    <tr>
      <th width="5%" class="head" style="vertical-align:middle;">Remove</th>
      <th width="10%" class="head" style="vertical-align:middle;">Transaction<br>Date</th>
       <th width="28%" class="head" style="vertical-align:middle;">Description</th>
	   <th width="22%" class="head" style="vertical-align:middle;">Transacted By</th>
	   <th width="8%" class="head" style="vertical-align:middle;">Debit</th>
	  <th width="8%" class="head" style="vertical-align:middle;">Credit</th>
	 
	  <th width="40%" class="head" style="vertical-align:middle;">Remarks</th>
	</tr>
    </thead>
    <?php
		if ($adjustments) {
			$debit = 0;
			$credit = 0;
			foreach ($adjustments AS $adj) {
				//print_r($adj); die();
				
	?>
    <tr>
      <!--  <td style="text-align:left; vertical-align:middle; "> <a class="delete_adjustment" href="" adj="<?php print($adj['adjustment_id']); ?>"><i class="icon-remove "> <?php //print($adj['adjustment_id']); ?> </a> </td>
       -->
	  <td style="text-align:center; vertical-align:middle; ">
	  <?php 
	  		if ($adj['transact_by_user'] == $login_user) {
	  ?>
	  	<a class="delete_adjustment" href="" adj="<?php print($adj['adjustment_id']); ?>"><i class="icon-trash"></i></a>
	  <?php 
	  		} else {
	  ?>
	  	<i class="icon-asterisk"></i>
	  <?php 
	  		}
	  ?>
	  </td>
	  <td style="text-align:left; vertical-align:middle; "><?php print($adj['transaction_date']); ?></td>
	   <td style="text-align:left; vertical-align:middle; "><?php print($adj['description']); ?></td>
	   <td style="text-align:left; vertical-align:middle; "><?php print($adj['transact_by']); ?></td>
	   <?php if ($adj['adjustment_type'] == 'Debit') { ?>
			  <td style="text-align:right; vertical-align:middle; "> <?php print(number_format($adj['amount'],2,'.',',')); ?> </td>
			  <td style="text-align:left; vertical-align:middle; "> </td>
	  <?php   $debit += $adj['amount']; 
	  		} else if ($adj['adjustment_type'] == 'Credit') { ?>
			  <td style="text-align:left; vertical-align:middle; "> </td>
	  	      <td style="text-align:right; vertical-align:middle; "> <?php print(number_format($adj['amount'],2,'.',',')); ?> </td>
	  <?php   $credit += $adj['amount']; 
	  		} ?>
	 
	  <td style="text-align:left; vertical-align:middle; "><?php print($adj['remark']); ?></td>
	</tr>
    <?php } ?>
		<th  colspan="3" style="text-align:right; vertical-align:middle; ">TOTAL</th>
		<td style="text-align:right; vertical-align:right; "><?php print(number_format($debit,2,'.',',')); ?></td>
		<td style="text-align:right; vertical-align:right; "><?php print(number_format($credit,2,'.',',')); ?></td>
		<td style="text-align:left; vertical-align:middle; "></td>
	<?php } ?>
	
		
</table>
</div>
</div>

<form action="" method="post" id="adjust_form">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" value="delete_adjustments" name="action" />
</form>

<script>
	$('.delete_adjustment').click(function(event){
		event.preventDefault(); 
		var confirmed = confirm('Are you sure you want to remove the adjustment?');

		if (confirmed){
			var adjustment_id = $(this).attr('adj');
			

			$('<input>').attr({
				type: 'hidden',
				name: 'adjustment_id',
				value: adjustment_id,
			}).appendTo('#adjust_form');		
			$('#adjust_form').submit();
		} 
		
	});
</script>
