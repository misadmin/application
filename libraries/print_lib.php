<?php 

class Print_lib extends EpsonStatic {
	private $content = "";
	public $headers = array();
	public $data = array();
	private $ledger_title = "";
	
	public function char_form_feed (){
		return chr(12);
	}
	
	public function line_feed () {
		return chr(10);
	}

	public function set_page_length_in_lines ($lines){
		return chr(27).chr(67).chr($lines);
	}
	
	public function reverse_feed (){
		return chr(27) . chr(74);
	}
	
	public function bold(){
		return chr(27) . chr(69);
	}
	
	public function init (){
		return chr(27) . chr(64);
	}
	
	public function carriage_return_new_line(){
		return '\r\n';
	}
	
	public function regular(){
		
		return chr(27) . chr(70);
	}
	
	public function condensed(){
		return chr(15);
	}
	
	public function no_condensed(){
		return chr(18);
	}
	
	public function big_font (){
		return chr(27) . 'w1' . chr(27) + 'W1'; 
	}
	/*
	init 			: chr(27) + chr(64),
	bold 			: chr(27) + chr(69),
	regular 		: chr(27) + chr(70),
	condensed 		: chr(15),
	noCondensed 	: chr(18),
	bigFont 		: chr(27) + 'w1' + chr(27) + 'W1',
	noBigFont 		: chr(27) + 'w0' + chr(27) + 'W0',
	vSpacing6LPI 	: chr(27) + chr(50),
	vSpacing8LPI 	: chr(27) + chr(48),
	Box drawing component
	drawLine 		: chr(196),
	drawDLine 		: chr(205),
	topLeft 		: chr(218),
	topRight 		: chr(191),
	vLine 			: chr(33),
	vIntersectR 	: chr(180),
	vIntersectL 	: chr(195),
	vIntersectTC 	: chr(194),
	intersect	 	: chr(197),
	vIntersectBC 	: chr(193),
	bottomLeft 		: chr(192),
	bottomRight 	: chr(217),
	*/
	public function draw_box ($what){
		switch($what) {
			case 'drawLine':	return chr(196); break;
			case 'drawDLine':	return chr(205); break;
			case 'topLeft':		return chr(218); break;
			case 'topRight': 	return chr(191); break;
			case 'vLine':		return chr(33); break;
			case 'vIntersectR': return chr(180); break;
			case 'vIntersectL':	return chr(195); break;
			case 'vIntersectTC':return chr(194); break;
			case 'intersect':	return chr(197); break;
			case 'vIntersectBC':return chr(193); break;
			case 'bottomLeft':  return chr(192); break;
			case 'bottomRight': return chr(217); break;
		}
	}
	public function align_right ($string, $length=78){
		return str_pad($string, $length, ' ', STR_PAD_LEFT);
	}
	
	public function align_center ($string, $length=78){
		return str_pad($string, $length, ' ', STR_PAD_BOTH);
	}
	
	public function write_receipt ($string, $padding=40){
		for($count = 1; $count<=$padding; $count++){
			$string = " " . $string; 
		}
		return $string;
	}
	
	public function set_table_header ($headers){
		$this->headers = $headers;
	}
	
	public function set_data ($data){
		$this->data = $data;
	}
	
	public function clear_table(){
		$this->headers = array();
		$this->data = array();
	}
	
	public function set_ledger_title($title = ""){
		$this->ledger_title = $title;
	}
	
	public function draw_table ($length=78, $table_border='-', $condensed=FALSE){
		
		$content = "";
		$border = "";
		$dcontent = "";
		$data_content = "";
		foreach ($this->headers as $header){
			$border .= str_pad('', floor($header['width'] * $length/100), $table_border, STR_PAD_RIGHT) . " ";
			$dcontent .= str_pad($header['name'],floor($header['width'] * $length/100), ' ', STR_PAD_RIGHT) . " "; 
		}
		
		$data_content = "";
		if (count($this->data) > 0) {
			foreach ($this->data as $row){
				foreach ($this->headers as $header){
					$exploded = explode("\n", (is_object($row) ? $row->$header['id'] : $row[$header['id']]));
					$dat = $exploded[0];
					if (strlen($dat) > floor($header['width'] * $length/100)){
						$dat = substr($dat, 0, floor($header['width']*$length/100));
					}
					$data_content .=  str_pad($dat,floor($header['width'] * $length/100), ' ', STR_PAD_RIGHT) . " ";
				}
				$data_content .= "\n";
			}	
		}
		
		if (empty($data_content)){
			$content = $dcontent;	//return only the header without border
		} 
		elseif ($condensed){
			$content = $dcontent . "\n" . $data_content; 
		} else {
			$content = $content . "\n" . $border . "\n" . $dcontent . "\n" . $border . "\n" . $data_content ."\n" . $border;
		}
		
		return $content;	
	}
	
	public function draw_ledger ($length=78, $term="Finals", $payable=0, $table_border='-', $condensed=FALSE){
	
		$content = "";
		$border = "";
		$dcontent = "";
		$data_content = "";
		$balance = 0;
		$temp_len = $length - strlen($this->ledger_title);
		$ledger_title = str_pad($this->ledger_title, $length - $temp_len/2, ' ', STR_PAD_LEFT);
		$ledger_title = $this->bold() . str_pad($ledger_title, ($length/2), ' ') . $this->regular() . "\n";
		foreach ($this->headers as $header){
			$border .= str_pad('', floor($header['width'] * $length/100), $table_border, STR_PAD_RIGHT) . " ";
			$width = floor($header['width'] * $length/100) - strlen($header['name']);
			$dcontent .= str_pad(str_pad($header['name'], strlen($header['name']) + ($width/2), ' ', STR_PAD_LEFT), strlen($header['name']) + $width, ' ', STR_PAD_RIGHT) . ' ';
		}
	
		$data_content = "";
		if (count($this->data) > 0) {
			foreach ($this->data as $row){
				foreach ($this->headers as $header){
					$dat = (is_object($row) ? trim($row->$header['id']) : trim($row[$header['id']]));
					if (strlen($dat) > floor($header['width'] * $length/100) - 1){
						if(!is_numeric($dat))
							$dat = substr($dat, 0, floor($header['width']*$length/100) - 1);
					}
					if(is_numeric($dat)){
						$balance = $dat;
						$value = (float)sprintf("%.2f", $dat);
						if ($value < 0) {
							$value = "(" . number_format(abs($value), 2);
							$data_content .=  str_pad($value, floor($header['width'] * $length/100) - 1, ' ', STR_PAD_LEFT) . ") ";
						} else {
							$data_content .=  str_pad(number_format($value, 2), floor($header['width'] * $length/100) - 1, ' ', STR_PAD_LEFT) . "  ";
						}
					} else {
						$data_content .= str_pad($dat,floor($header['width'] * $length/100) - 1, ' ', STR_PAD_RIGHT) . "  ";
					}
				}
				$data_content .= "\n";
			}
			$balance = number_format(($balance > 0 ? $balance : 0), 2);
			$table_width = 0;
			foreach ($this->headers as $header){
				$table_width += $header['width'] ;
			}
			$balance_length = strlen(number_format($payable, 2));
			$remaining_bal = str_pad("Please pay this amount for " . $term . " : ",  $table_width * $length/100 - strlen(number_format($payable, 2)) - 10, ' ', STR_PAD_LEFT);
			$data_content .= str_pad('', $table_width * $length/100 + 3, '-') . "\n";
			$data_content .= $remaining_bal  . " " . str_pad(number_format($payable, 2), $balance_length + 11, " ", STR_PAD_LEFT) . "\n";
			$data_content .= str_pad('', $table_width * $length/100 - $balance_length, ' ', STR_PAD_LEFT) . " ";
			$data_content .= str_pad('', $balance_length + 2, "=") . "\n\n";
		} else {
			$data_content .= "Error! No Ledger Data\n";
			$bor = explode("\n", $border, 0);
			$data_content .= str_pad('', strlen($bor[0])-1, '-') . "\n";
		}
	
		if ($condensed){
			$content = $this->condensed(). $border . "\n" . $dcontent . "\n" . $border . "\n" . $data_content . $this->no_condensed() . "\n";
		} else {
			$content = $content . "\n" . $border . "\n" . $dcontent . "\n" . $border . "\n" . $data_content ."\n";
		}
		return $ledger_title . $content;
	}
	
	public function draw_clearance($data = array(), $length=78, $spacing=1, $condensed=FALSE){
		$title = $this->bold() . "CLEARANCE" . $this->regular();
		$content = "";
		$data_content = "";
		$max_width = array('office'=>20, 'laboratory'=>10);
		$space_between_col = 10;
		$offices = array();
		$laboratories = array();
		
		foreach ($data['office'] as $office){
			$max_width['office'] = max($max_width['office'], strlen($office));
		}
		foreach ($data['office'] as $office){
			$offices[] = str_pad($office, $max_width['office'], ' ') . "   " . str_pad('', 20, '_');
			//$data_content .= str_pad('', $spacing, "\n");
		}
		
		foreach ($data['laboratory'] as $lab){
			$max_width['laboratory'] = max($max_width['laboratory'], strlen($lab));
		}
		if(!empty($data['laboratory']) && count($data['laboratory']) > 0){
			$laboratories[] = 'LABORATORY:';
			foreach ($data['laboratory'] as $lab){
				$laboratories[] = str_pad('LAB:' . $lab, $max_width['laboratory'] + 4, ' ') . "   " . str_pad('', 20, '_');
				//$data_content .= str_pad('', $spacing, "\n");
			}
		}
		
		for ($i = 0; $i < max(count($offices), count($laboratories)); $i++){
			$data_content .= isset($offices[$i]) ? $offices[$i] : str_pad('', strlen($offices[0]), ' ');
			$data_content .= str_pad('', $space_between_col, ' ');
			if(!empty($laboratories) && count($laboratories) > 0) {
				$data_content .= isset($laboratories[$i]) ? $laboratories[$i] : "";
				$data_content .= str_pad('', $spacing, "\n");
			} else {
				$data_content .= "\n";
				$data_content .= str_pad('', $spacing - 1, "\n");
			}
			
		}
		
		$data_content .= str_pad('', 3, "\n");
		if(!empty($laboratories) && count($laboratories) > 0) {
			$data_content .= str_pad('', strlen($offices[0]), ' ');
			$data_content .= str_pad('', $space_between_col, ' ');
			$data_content .= str_pad('Accounts Clerk', $max_width['laboratory'], ' ') . "   " . str_pad('', 20, '_');
		} else {
			$data_content .= str_pad('Accounts Clerk', $max_width['office'], ' ') . "   " . str_pad('', 20, '_');
		}
			
		if($condensed){
			$content = $this->condensed() . $data_content;
		} else {
			$content = $this->regular() . $data_content;
		}
		return $title . "\n" . $content;
	}
	public function basic_ed_clearance($offices=array(), $width=136){
		$title = $this->bold() . "CLEARANCE" . $this->regular();
		$content = "";
		$width_of_one = floor($width/4);
		$clearance_val = "";
		if(is_array($offices) && count($offices) > 0){
			$remaining = count($offices);
			$start = 0;
			do{
				$current_offices = 	array_slice($offices, $start, 4);
				$lines = "";
				$names = "";
				foreach ($current_offices as $count=>$office){
					$lines .= str_pad("__________________________", $width_of_one, " ", STR_PAD_BOTH);
					$names .= str_pad($office, $width_of_one, " ", STR_PAD_BOTH);
				}
				$clearance_val = $clearance_val . $lines . "\n" . $names . "\n\n"; 			
				$remaining = $remaining - 4;
				$start = $start + 4;
			} while($remaining > 0);
			$return = $title . "\n\n" . $this->condensed(). $clearance_val . $this->no_condensed();
		}
		return $return;
	}
	public function cut(){
		return EpsonStatic::Cut();
	}
	public function table($column_def, $rows){
		$content = '';
		$alignment = array('left'=>STR_PAD_RIGHT, 'right'=>STR_PAD_LEFT, 'center'=>STR_PAD_BOTH);
		foreach ($rows as $row){
			$next_line = array();
			foreach($column_def as $key=>$def){
				if (strlen($row[$key]->content) <= $def){
					if(isset($row[$key]->weight) && $row[$key]->weight=='bold'){
						$content .= $this->bold().str_pad($row[$key]->content, $def, ' ', isset($row[$key]->align) ? $alignment[$row[$key]->align] : STR_PAD_RIGHT) . $this->regular();
					} else {
						$content.=str_pad($row[$key]->content, $def, ' ', isset($row[$key]->align) ? $alignment[$row[$key]->align] : STR_PAD_RIGHT);
					}
				} else {
					$row_content_exploded = explode(" ", $row[$key]->content);
					$current_cont = "";
					$previous_cont = "";
					foreach ($row_content_exploded as $row_count=>$cont){
						$current_cont .= $cont . " "; 
						if (strlen(rtrim($current_cont)) > $def){
							if(isset($row[$key]->weight) && $row[$key]->weight=='bold'){
								$content.=$this->bold().str_pad(rtrim($previous_cont), $def, ' ', isset($row[$key]->align) ? $alignment[$row[$key]->align] : STR_PAD_RIGHT).$this->regular();
							} else {
								$content.=str_pad(rtrim($previous_cont), $def, ' ', isset($row[$key]->align) ? $alignment[$row[$key]->align] : STR_PAD_RIGHT);
							}
							$next_line[$key] = "";
							foreach ($row_content_exploded as $row_count2=>$cont1){
								if ($row_count2 >= $row_count)
									$next_line[$key] = $next_line[$key] . " " . $cont1;
							}							
							break(1); 
						}
						$previous_cont = $current_cont;
					}
				} 
			}
			$content.="\n";
			if(count($next_line) > 0){
				//has content...
				foreach ($column_def as $key=>$def){
					$content .= str_pad((isset($next_line[$key]) ? substr(trim($next_line[$key]), 0, $def) : ''), $def, ' ', STR_PAD_RIGHT);
				}
				$content .= "\n";
			}
		}
		return $content;
	}
	public function prelim_education_table ($elementary, $highschool){
		$content = str_pad('ELEMENTARY COURSE COMPLETED', 68, ' ', STR_PAD_BOTH) . str_pad('HIGH SCHOOL COMPLETED', 68, ' ', STR_PAD_BOTH) . "\n";
		$content .= str_pad('+---------+--------------------------------------+-------------+', 68, ' ', STR_PAD_BOTH) . str_pad('+---------+--------------------------------------+-------------+', 68, ' ', STR_PAD_BOTH) . "\n";
		$content .= str_pad('|  Grade  | Name of School                       | School Year |', 68, ' ', STR_PAD_BOTH) . str_pad('|   Year  | Name of School                       | School Year |', 68, ' ', STR_PAD_BOTH) . "\n";
		$content .= str_pad('+---------+--------------------------------------+-------------+', 68, ' ', STR_PAD_BOTH) . str_pad('+---------+--------------------------------------+-------------+', 68, ' ', STR_PAD_BOTH) . "\n";
		if (count($elementary) > count($highschool))
			$total_count = count($elementary); else
			$total_count = count($highschool);
		
		$grade_bottommed = FALSE;
		$hs_bottommed = FALSE;
		for ($count = 0; $count <= $total_count; $count++){
			if(isset($elementary[$count])){
				$to_be_replaced = array('Intermediate', 'Elementary');
				$replaced_with = array('Interm.', 'Elem.');
				$grade_content = str_pad('|' . str_pad(substr(str_replace($to_be_replaced, $replaced_with, $elementary[$count]->level), 0, 9), 9, ' ', STR_PAD_RIGHT) . '| ' .  str_pad(substr($elementary[$count]->school, 0, 36), 36, ' ', STR_PAD_RIGHT) . ' | ' . str_pad(substr($elementary[$count]->sy, 0, 11), 11, ' ', STR_PAD_RIGHT) . ' |', 68, ' ', STR_PAD_BOTH);
			} else {
				if(!$grade_bottommed){
					$grade_content = str_pad('+---------+--------------------------------------+-------------+', 68, ' ', STR_PAD_BOTH);
					$grade_bottommed = TRUE;  
				} else {
					$grade_content = str_pad('', 68, ' ', STR_PAD_BOTH);
				}
			}
			if(isset($highschool[$count])){
				$hs_content = str_pad('|' . str_pad(substr($highschool[$count]->level, 0, 9), 9, ' ', STR_PAD_BOTH) . '| ' .  str_pad(substr($highschool[$count]->school, 0, 36), 36, ' ', STR_PAD_RIGHT) . ' | ' . str_pad(substr($highschool[$count]->sy, 0, 11), 11, ' ', STR_PAD_RIGHT) . ' |', 68, ' ', STR_PAD_BOTH);
			}else{
				if(!$hs_bottommed){
					$hs_content = str_pad('+---------+--------------------------------------+-------------+', 68, ' ', STR_PAD_BOTH);
					$hs_bottommed = TRUE;
				} else {
					$hs_content = str_pad('', 68, ' ', STR_PAD_BOTH);
				}
			}
			$content .= $grade_content . $hs_content . "\n";
		}
		return $content;
	}
	public function form9_table ($courses, $page, $which = 'first', $forwarded_units=0, $max_lines=60, $fullname='', $last_sem){
		$units_this_page = 0;
		$content = str_pad('', 136, '=')."\n";
		$columns = array(60,9,9,4,4,4,4,4,4,4,4,4,4,4);
		$romans = array('I','II', 'III', 'IV', 'V','VI','VII','VIII','IX','X','XI');
		$content .= str_pad('', $columns[0], ' ', STR_PAD_BOTH). "|".str_pad('',$columns[1], " ", STR_PAD_BOTH)."|".str_pad('', $columns[2])."|".str_pad('CREDIT BY GROUP',55,' ',STR_PAD_BOTH)."\n";
		$content .= str_pad('COURSE NUMBER AND DESCRIPTIVE TITLE', $columns[0], ' ', STR_PAD_BOTH). "|".str_pad('FINAL',$columns[1], " ", STR_PAD_BOTH)."|".str_pad('', $columns[2])."|".str_pad('',55,'-',STR_PAD_RIGHT)."\n";
		$content .= str_pad('', $columns[0], ' ', STR_PAD_BOTH). "|".str_pad('RATING',$columns[1], " ", STR_PAD_BOTH)."|".str_pad('CREDIT',$columns[2], " ", STR_PAD_BOTH)."|";	
		for ($count = 3; $count <=13; $count++){
			$content .= str_pad($romans[$count-3],4,' ',STR_PAD_BOTH) ."|";
		}
		$content = rtrim($content, "|")."\n";
		for ($count = 0; $count <= 13; $count++){
			$content .= str_pad('',$columns[$count],'-',STR_PAD_BOTH) ."+";
		}
		$content = rtrim($content, "|")."\n";
		if($forwarded_units!=0){
			$temp = array();
			$temp[] = (object)array('content'=>str_pad("CREDIT UNITS EARNED FORWARDED", 60, " ", STR_PAD_RIGHT), 'rating'=>'', 'credit'=>$forwarded_units, 'underlined'=>TRUE,'from_hnu'=>'');
			$temp[] = (object)array('content'=>str_pad("", 60, " ", STR_PAD_RIGHT), 'rating'=>'', 'credit'=>'', 'underlined'=>TRUE,'from_hnu'=>'');
			$courses = array_merge($temp, $courses);
		}
		foreach($courses as $line){
			$credit_units = '';
			if($line->from_hnu == '1'){
				//course FROM HNU
				if(is_numeric($line->rating) && (float)$line->rating > 3){
					$credit_units = "---";
				}
				if (is_numeric($line->rating) && (float)$line->rating <= 3){
					if((float)$line->credit < 0){
						$credit_units = "(" . number_format(abs($line->credit), 1) . ")";
					} else {
						$credit_units = number_format($line->credit, 1);
					}
				}
				if(!is_numeric($line->rating)){
					$credit_units = "---";
				}
				if($last_sem == $line->academic_term){
					if((float)$line->credit < 0){
						$credit_units = "(" . number_format(abs($line->credit), 1) . ")";
					} else {
						$credit_units = number_format($line->credit, 1);
					}
				}
				if(is_numeric($credit_units)){
					$units_this_page += $credit_units;
				}
			} else {
				//course not from HNU
				$credit_units = $line->credit;
				if(is_numeric($line->credit)){
					if((float)$line->credit < 0){
						$credit_units = "(" . number_format(abs($line->credit), 1) . ")";
					} else {
						$credit_units = number_format($line->credit, 1);
						$units_this_page += $line->credit;
					}
				}
			}
			$content .= str_pad($line->content, $columns[0], ' ', STR_PAD_BOTH). "|".str_pad(substr($line->rating, 0, $columns[1]),$columns[1], " ", STR_PAD_BOTH)."|".str_pad(substr($credit_units, 0, $columns[2]),$columns[2], " ", STR_PAD_BOTH)."|";
			for ($count = 3; $count <=13; $count++){
				$content .= str_pad('',4,' ',STR_PAD_BOTH) ."|";
			}
			$content = rtrim($content, "|")."\n";
		}
		$content .= str_pad('', 136, '-')."\n";
		$content .= str_pad('TOTAL CREDITS EARNED', ($columns[0]+$columns[1])) . " |" . str_pad(number_format(($units_this_page), 1), $columns[2], " ", STR_PAD_BOTH) . "|";
		for ($count = 3; $count <=13; $count++){
			$content .= str_pad('',4,' ',STR_PAD_BOTH) ."|";
		}
		$content = rtrim($content, "|"). "\n";
		$content .= str_pad('', 136, '-')."\n";
		$content .= "Remarks: \n\n";
		if((count(explode("\n",$content))+4) >= $max_lines - 6 && $which=='last'){
			$content .= "\n\n" . str_pad('Page: ' . $page, 136, " ", STR_PAD_BOTH)."\n".$this->char_form_feed();
			$page++;
		}
		if($which=="last"){
			$content .= str_pad('CERTIFICATE OF THE REGISTRATION', 136, " ", STR_PAD_BOTH)."\n\n";
			$certification = "I hereby certify that the foregoing records of {$fullname} a candidate for graduation have been verified by me and that true copies of the official records substantiating the same are kept in the files of our school.";
			$cert_exploded = explode(" ", $certification);
			$previous_line = "";
			$current_line = "      ";
			foreach ($cert_exploded as $cert_word){
				$current_line .= $cert_word . " ";
				if(strlen($current_line) > 136){
					$current_line = $cert_word . " ";
					$content .= rtrim($previous_line)."\n"; 
				}
				$previous_line = $current_line;
			}
			$content .= trim($previous_line)."\n\n";
			//die();
		}
		// $content .= str_pad('', 104) . str_pad('MRS. PRUDENCIA A. MONDRAGON', 32, " ", STR_PAD_BOTH) . "\n";
		$content .= str_pad('', 104) . str_pad('PROF. VICTORIA C. MILLANAR', 32, " ", STR_PAD_BOTH) . "\n";
		$content .= str_pad('', 104) . str_pad('', 32, "-", STR_PAD_BOTH) . "\n";
		$content .= str_pad('', 104) . str_pad('REGISTRAR', 32, " ", STR_PAD_BOTH) . "\n";
		$content .= str_pad('Page: ' . $page, 136, " ", STR_PAD_BOTH)."\n";
		return (object)array('content'=>$content, 'total_credits'=>($units_this_page));	
	}
}

class EpsonStatic {
	public function Init() {
		return '\x1B\x40';
	}
	static function ResetStyles() {
		return '\x1B\x21\x00';
	}
	static function MakeStyle($font, $bold = false, $underline = false, $double_width = false, $double_height = false) {
		$hex = 0;
		if ($font) {
			$hex += $font;
		}
		if ($bold) {
			$hex += 8;
		}
		if ($underline) {
			$hex += 80;
		}
		if ($double_width) {
			$hex += 20;
		}
		if ($double_height) {
			$hex += 10;
		}
		if ($hex < 10) {
			$hex = '0'.$hex; }
			return '\x1B\x21\x'.$hex;
	}
	public function Cut() {
		return '\x1D\x56\x41';
	}
}
