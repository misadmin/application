<style>
	td.values_header {
		background-color:#E7E8E7;
		font-size:bold;
		text-align:center;
		font-size:12px;
		vertical-align:middle;
		height:auto;
		padding:2px;
	}

</style>
	
		<div style="width:100%; margin-top:10px;"  id="show_behavior_values_here">
			<div style="width:100%; overflow:auto; margin-top:2px; margin-bottom:25px;">
			<?php 
				$term_quarter1 = '1'; 
				$term_quarter2 = '2';
				$term_to_check = '1st';
			    //added by Toyet 7.30.2018
			    if(isset($term->term)){
			    	if(substr($term->term,0,3)=='1st'){
			    		$term_to_check = '1st';
			    	} else {
			    		$term_to_check = '2nd';
			    	}
			    }
			    if(isset($selected_history->term)){
			    	if(substr($selected_history->term,0,3)=='1st'){
			    		$term_to_check = '1st';
			    	} else {
			    		$term_to_check = '2nd';
			    	}
			    }

					if($term_to_check=='1st') {
						$term_quarter1 = '1'; 
						$term_quarter2 = '2'; 
					} else {
						$term_quarter1 = '3'; 
						$term_quarter2 = '4'; 
					} 

			?>
				<table class="table table-hover table-bordered table-striped" >
					<thead>
						<tr style="font-size:16px; font-weight:bold;" >
							<td colspan="4" style="text-align:center; background-color:#C3C3C3;">
								REPORT ON LEARNER'S OBSERVED VALUES
							</td>
						</tr>
						<tr style="text-align:center; font-size:11px; font-weight:bold;" >
							<td class="values_header" rowspan="2" style="width:20%;">Course Values</td>
							<td class="values_header" rowspan="2" style="width:50%;">Behavior Statements</td>
							<td class="values_header" colspan="2" style="width:30%;">Quarter</td>
						</tr>
						<tr style="text-align:center; font-size:11px; font-weight:bold;" >
							<td class="values_header" style="width:15%;"><?php echo $term_quarter1; ?></td>
							<td class="values_header" style="width:15%;"><?php echo $term_quarter2; ?></td>
						</tr>
					</thead>
					
					<tbody>
						<?php 
							if ($observed_values) {
								$num =1;
								$icon_id=1;
								foreach($observed_values AS $ovalue) {
						?>
									<tr style="text-align:center; font-size:12px; font-weight:bold;" >
										<td rowspan="<?php print($ovalue->cnt); ?>" style="vertical-align:middle; color:#04368D;"><?php print($num.". ".$ovalue->description); ?></td>
										<?php 
											if ($ovalue->behaviors) {
												$x=0;
												foreach($ovalue->behaviors AS $behavior) {
													if ($x > 0)
														print("</tr><tr style=\"text-align:center; font-size:12px; font-weight:bold;\">");
										?>
													<td><?php print($behavior->behavior_statement); ?></td>
													<td style="vertical-align:middle;">
														<div id="icon_<?php print($icon_id); ?>" style="float:left;">
														</div> 
														<div style="text-align:center;">
															<?php 
																if ($can_update_values) {
															?>
																<a href="#" 
																	student_histories_id="<?php print($student_histories_id); ?>" 
																	behavior_statements_id="<?php print($behavior->behavior_statements_id); ?>" 
																	marking="<?php print($behavior->midterm_marking); ?>"
																	behavior_statement="<?php print($behavior->behavior_statement); ?>"
																	value_description="<?php print($ovalue->description); ?>"
																	period="Midterm"
																	icon_id="<?php print($icon_id); ?>"
																	class="show_modal_marking" >
																	<?php 
																		if ($behavior->midterm_marking) {
																			print($behavior->midterm_marking);
																		} else {
																			print("<i class=\"icon-edit\"></i>");	
																		}
																		$icon_id++;
																	?>	
																</a>
															<?php 
																} else {
																	print($behavior->midterm_marking);
																}
															?>
														</div>		
													</td>
													<td style="vertical-align:middle;" >
														<div id="icon_<?php print($icon_id); ?>" style="float:left;">
														</div> 
														<div style="text-align:center;">
															<?php 
																if ($can_update_values) {
															?>
																<a href="#" 
																	student_histories_id="<?php print($student_histories_id); ?>" 
																	behavior_statements_id="<?php print($behavior->behavior_statements_id); ?>" 
																	marking="<?php print($behavior->finals_marking); ?>"
																	behavior_statement="<?php print($behavior->behavior_statement); ?>"
																	value_description="<?php print($ovalue->description); ?>"
																	period="Finals"
																	icon_id="<?php print($icon_id); ?>"
																	class="show_modal_marking" >
																	<?php 
																		if ($behavior->finals_marking) {
																			print($behavior->finals_marking);
																		} else {
																			print("<i class=\"icon-edit\"></i>");	
																		}
																		$icon_id++;
																	?>	
																</a>
															<?php 
																} else {
																	print($behavior->finals_marking);
																}
															?>
														</div>		
													</td>										
										<?php
													$x++;
												}
											}
										?>
									</tr>
						<?php 
									$num++;
								}
							}
						?>
					</tbody> 
				</table>
			</div>
		</div>	

			

<script>
	$('.show_modal_marking').click(function(event){
		event.preventDefault();

		var student_histories_id   = $(this).attr('student_histories_id');
		var behavior_statements_id = $(this).attr('behavior_statements_id');
		var marking                = $(this).attr('marking');
		var period                 = $(this).attr('period');
		var icon_id                = $(this).attr('icon_id');
	
		$('#student_histories_id').val(student_histories_id);
		$('#behavior_statements_id').val(behavior_statements_id);
		$('#icon_id').val(icon_id);
		$('#marking').val(marking);
		
		$('#period').html($(this).attr('period'));
		$('#value_description').html($(this).attr('value_description'));
		$('#behavior_statement').html($(this).attr('behavior_statement'));
		
		$('#modal_change_marking').modal('show');
	});
</script>				


	<div id="modal_change_marking" class="modal hide fade" >
		<div class="modal-dialog" >
			<div class="modal-content">
				<form id="change_marking_form" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="text-align:left;">Update Marking</h3>
					</div>
					<div class="modal-body" style="text-align:left; font-size:15px;">
						<div id="student_histories_id"></div>
						<div id="behavior_statements_id"></div>
						<div id="icon_id"></div>
						<?php														
							$this->load->view('shs/class_adviser/modal_view/markings_modal');
						?>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Update Marking!" />
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close!</button>
					</div>
				</form>
			</div>
		</div>
	</div>



<script>
	$('#change_marking_form').submit(function(e) {
		e.preventDefault();
		
		var student_histories_id   = $('#student_histories_id').val();
		var behavior_statements_id = $('#behavior_statements_id').val();
		var marking                = $('#marking').val();
		var icon_id                = $('#icon_id').val();

		var period                 = $('#period').html();

		$('#modal_change_marking').modal('hide');

		$('#icon_'+icon_id).html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php print(site_url($this->uri->segment(1).'/my_section'));?>",
			data: {"student_histories_id": student_histories_id,
					"behavior_statements_id": behavior_statements_id,
					"period": period,
					"marking": marking,
					"action": "update_marking" },
			dataType: 'json',
			success: function(response) {													
																		
				$('#show_behavior_values_here').html(response.output); 
			}
		});

		$('#icon_'+icon_id).html("");
		
	});
</script>				

