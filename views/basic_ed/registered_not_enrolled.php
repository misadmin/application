
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; 
	border-width: 1px; border-color: #D8D8D8; height: 35px; 
	padding: 2px; margin-bottom:20px;">

	<table style="width:100%; height:auto;">
	
	<tr>
	<td style="text-align:left; vertical-align:top; font-weight:bold; width:20%; padding:3px;">
	<?php
		if ($students) { 
	?>
		<a href="<?php print(base_url('downloads/Registered But Not Enrolled '.$display_level.'-'.$display_sy.'.xls')); ?>" class="btn btn-link">
		<i class="icon-download-alt"></i> Download as Excel</a>
	<?php
		} 
	?>	
	</td>
	<td style="text-align:right; vertical-align:top;">
		<form id="change_data" method="POST">

			<select name="levels_id" class="levels_id" id="select1">
				<?php
					foreach($levels AS $level) {
				?>
						<option value="<?php print($level->levels_id); ?>"
								row_id="<?php print($level->row_id); ?>"
								yr_level="<?php print($level->yr_level); ?>" 
								display_level="<?php print($level->display_yr_level); ?>"
								<?php if ($level->row_id == $selected_level) { print("selected"); } ?>>
								<?php print($level->display_yr_level); ?> </option>	
				<?php 
					}
				?>					
			</select>

			<select name="academic_year_id" class="levels_id" id="select2">
				<?php
					foreach($school_years AS $school_year) {
				?>
						<option value="<?php print($school_year->id); ?>" 
								display_sy="<?php print($school_year->sy); ?>"
								<?php if ($school_year->id == $selected_year) { print("selected"); } ?>>
								<?php print($school_year->sy); ?> </option>	
				<?php 
					}
				?>					
			</select>
							
	</form>
	</td>
	</tr>
	</table>						
</div>


<div style="margin-top:10px">
<h2 class="heading"><?php print($display_level);?> MASTERLIST (REGISTERED BUT NOT ENROLLED) <?php print("for: ".$display_sy); ?></h2>
<table class="table table-striped table-bordered"; style="width:50%">
	<thead >
		<tr align="center">
			<th width="5%" class="head" style=" vertical-align:middle; text-align:center;">No.</th>
			<th width="15%" class="head" style="vertical-align:middle; text-align:center;">Student ID</th>
			<th width="75%" class="head" style="vertical-align:middle; text-align:center;">Name</th>
			<th width="5%" class="head" style="vertical-align:middle; text-align:center;">Gender</th>
			
		</tr>
	</thead>
	<tbody> 

	
	<?php 
		if($students) {
			$count = 0;
			foreach ($students as $student): ?>
			<tr>
				<?php $count++;?>
				<td style="text-align:right;"><?php print($count); ?>.</td>
				<td style="text-align:center;"><?php print($student->students_idno); ?></td>
				<td><?php print($student->neym); ?></td>
				<td style="text-align:center;"><?php print($student->gender); ?></td>
				
			</tr>
<?php
			endforeach;
		} ?>
</tbody>
</table>
</div> 


<script>
	$(document).ready(function(){
		$('.levels_id').bind('change', function(){
			show_loading_msg();
            var element = $("option:selected", "#select1");
			var row_id = element.attr('row_id');
			var yr_level = element.attr('yr_level');
			var display_level = element.attr('display_level');

			var element2 = $("option:selected", "#select2");
			var display_sy = element2.attr('display_sy');

			
			$('<input>').attr({
				type: 'hidden',
				name: 'row_id',
				value: row_id,
			}).appendTo('#change_data');

			$('<input>').attr({
				type: 'hidden',
				name: 'yr_level',
				value: yr_level,
			}).appendTo('#change_data');

			$('<input>').attr({
				type: 'hidden',
				name: 'display_level',
				value: display_level,
			}).appendTo('#change_data');

			$('<input>').attr({
				type: 'hidden',
				name: 'display_sy',
				value: display_sy,
			}).appendTo('#change_data');

			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'list_selected',
			}).appendTo('#change_data');
			
			$('#change_data').submit();
		});
	});
</script>

