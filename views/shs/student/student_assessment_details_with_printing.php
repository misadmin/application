

				<div style="padding-left:20px; font-weight:bold; font-size:15px; margin-bottom:15px; color:#026105; margin-top:10px;">
					<?php 
						$gtotal = 0;
						if ($assess['assess_record']) {
					?>
						ASSESSMENT POSTED ON: <?php print($assess['assess_record']->date_assessed); ?>
					<?php 
						} else {
					?>
						ASSESSMENT UNPOSTED!
					<?php 
						}
					?>
				</div>


				<div style="padding-left:20px;">
					<div style="float:left; font-weight:bold; margin-bottom:25px; border-bottom:solid 1px #CBCBCB; width:40%;">
						<?php 
							if ($assess['tuition_basic']) {
								print("Tuition Fees (@ P ".number_format($assess['tuition_basic']->rate,2).")/unit"); 
							} else {
								print("<div style='color:#A40404; margin-bottom:10px;'>No Basic Rate assigned by Accounts!</div>");  
							}
						?>
					</div>
					
					<div style="clear:both;">
						<table class="table table-hover table-bordered table-striped table-condensed" style="width:50%;" >
							<thead>
								<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
									<td class="shs_header" style="width:56%;">Course Code - Description</td>
									<td class="shs_header" style="width:12%;">Load<br>Units</td>
									<td class="shs_header" style="width:12%;">Paying<br>Units</td>
									<td class="shs_header" style="width:20%;">Total</td>
								</tr>
							</thead>
							<tbody>
							<?php
								if ($assess['courses']) {
									$total=0;
									$total_pay_unit=0;
									foreach($assess['courses'] AS $course) {
										$total = $course->paying_units*$assess['tuition_basic']->rate;
										$gtotal = $gtotal + $total;
							?>
										<tr>
											<td>
												<?php 
													print("<div style='width:85px; float:left;'>".$course->course_code." - </div>".substr($course->descriptive_title,0,20)); 
														
													if (strlen($course->descriptive_title) > 20) {
														print('...');
													}
												?>
											</td>
											<td style="text-align:center;"><?php print($course->credit_units); ?></td>
											<td style="text-align:center;"><?php print($course->paying_units); $total_pay_unit += $course->paying_units; ?></td>
											<td style="text-align:right;"><?php print(number_format($total,2)); ?></td>
										</tr>
							<?php
									}
							?>
							</tbody>
							<tfoot>
								<tr style="font-weight:bold; background-color:#B5B2B2;">
									<td colspan="2" style="text-align:right;">TOTAL:</td>
									<td style="text-align:center;"><?php print(number_format($total_pay_unit,2)); ?></td>
									<td style="text-align:right;"><?php print(number_format($gtotal,2)); ?></td>
								</tr>
							</tfoot>
							<?php	
								}
							?>
						</table>
					</div>
					
					<div style="width:55%; text-align:right; padding-top:15px;">
						<?php print("P ".number_format($gtotal,2)); ?>
					</div>
					
					<div style="float:left; font-weight:bold; margin-top:25px; border-bottom:solid 1px #CBCBCB; width:40%; margin-bottom:20px;">
						Miscellaneous and Other Fees:
					</div>
					
					<?php 
						$misc_subtotal = array();
						if ($assess['misc_fees']) {
							foreach($assess['misc_fees'] AS $misc_fee) {
								$total=0;
					?>

								<div style="padding-top:20px;">
									<div style="clear:both; font-weight:bold; font-size:14px; ">
										<?php print($misc_fee->fees_group); ?>
									</div>
									<?php 
										if ($misc_fee->fees_subgroups) {
											foreach($misc_fee->fees_subgroups AS $fee_subgroup) {
									?>
												<div style="clear:both; float:left; padding:5px; padding-left:30px; width:30%;"> 
													<?php print($fee_subgroup->description); ?>
												</div>
												<div style="float:left; width:10%; padding:5px; text-align:right;">
														<?php print($fee_subgroup->rate); $total=$total + $fee_subgroup->rate; $misc_subtotal[$misc_fee->fees_group] = $total; ?>
												</div>
									<?php
											}
										}
									?>		
								</div>
								<div style="clear:both; width:55%; text-align:right;">
									<?php print("P ".number_format($total,2)); $gtotal=$gtotal + $total; ?>
								</div>
					<?php
							}
						}
					?>
					
					<div style="float:left; font-weight:bold; margin-top:25px; border-bottom:solid 1px #CBCBCB; width:40%;">
						Laboratory Fees:
					</div>

					<div style="clear:both;">
						<?php 
							$total = 0;
							$total_lab_fees = 0;
							$lab_courses = array();
							if ($assess['lab_fees']) {
								foreach($assess['lab_fees'] AS $lab_fee) {
						?>
									<div>
										<div style="clear:both; float:left; padding:5px; padding-left:30px; width:30%;">
											<?php print($lab_fee->course_code); $lab_courses['name'] = $lab_fee->course_code; ?>
										</div>
										<div style="float:left; width:10%; padding:5px; text-align:right;">
											<?php print(number_format($lab_fee->rate,2)); $total = $total +  $lab_fee->rate; $lab_courses['amount'] = $lab_fee->rate; ?>
										</div>
									</div>
						<?php
								}
								
								$total_lab_fees = $total;
							}
						?>
							<div style="clear:both; width:55%; text-align:right;">
								<?php print("P ".number_format($total,2)); $gtotal=$gtotal + $total; ?>
							</div>		
					</div>

					<div style="clear:both; margin-top:30px; font-weight:bold; font-size:15px;">
						<div style="float:left; width:40%;">
							TOTAL Assessment:
						</div>
						<div style="float:left; width:15%; text-align:right;">
							<?php print("P ".number_format($gtotal,2)); ?>
						</div>
					</div>
				</div>


<form id="change_assessment_term_form" method="post" >
  <input type="hidden" name="action" value="schedule_current_term" />
  <input type="hidden" name="shs_assess" value="shs_assessment" />  
</form>

<script>
	$(document).ready(function(){
		$('.change_assessment').bind('change', function(){
			
			var element2      = $("option:selected", "#assess");
			var academic_term = element2.val();
			
			$('<input>').attr({
				type: 'hidden',
				name: 'academic_term',
				value: academic_term,
			}).appendTo('#change_assessment_term_form');
			
			$('#change_assessment_term_form').submit();
		});
	});
			
</script>

				
		
<?php

	if(isset($ledger_data)){
		foreach($ledger_data as $item){
			//if($item['transaction_detail']=='COLLEGE REGISTRATION' &&)
		}
		//$balance_before_assessment = $ledger_data[0]->debit;   //added Toyet 11.23.2017
		//log_message('INFO',"LEDGER DATA  ==>".print_r($ledger_data,true));
	}

	//if(in_array($this->session->userdata('role'), $this->config->item('roles_allowed_to_print_assessment'))):
		if (isset($student_inclusive_terms)) {
			foreach($student_inclusive_terms as $val){
				if($selected_history_id == ''){
					$term = $val->term;
					$sy = $val->sy;
					break;
				}
				if($selected_history_id == $val->student_histories_id){
					$term = $val->term;
					$sy = $val->sy;
				}		
			}
		}

		$cost_per_unit = isset($assessment['Tuition Basic']['Tuition Basic']) ? $assessment['Tuition Basic']['Tuition Basic'] : 0 ;

		$tuition_fees_content[0] = array(
				'fee_type'=>'TUITION ' . $term . ' SY ' . $sy . ' @ ' . number_format($cost_per_unit, 2),
		);

			if (is_array($courses) && !empty($courses)){
				foreach ($courses as $course){ //$total_tuition += ($course['pay_units'] * $cost_per_unit); $total_pay_units += $course['pay_units']; $total_units += $course['units'];
						if (!$course['withdrawn_on']) {
							$tuition_fees_content[count($tuition_fees_content)] = array(
									'subject' => $course['name'],
									'units' => $course['units'],
									'hours' => $course['pay_units'],
									'amount' => (isset($other_courses_payments[$course['id']]) && array_key_exists($course['id'], $other_courses_payments) ? $other_courses_payments[$course['id']] : $cost_per_unit) * $course['pay_units'],
							);
						}
				}
			}

			
		if (!empty($assessment) && count($assessment) > 0) {
			foreach($assessment as $key=>$val){
				if($key != "Tuition Basic"){
					$miscellaneous_fees_content[] = array(
						'fee_type'=>$key,
						'amount'=>$misc_subtotal[$key]
					);
				}
			}
		}

		if (isset($lab_courses)) {
			if (is_array($lab_courses) && count($lab_courses) > 0){
				$miscellaneous_fees_content[] = array(
					'fee_type'=>'Laboratory Fees',
					'amount'=>$total_lab_fees
				);
			}
		}

		//added by Tatskie on Jun 30, 2015
		$affiliated_total = 0;

		$miscellaneous_fees_content[] = array('fee_type'=>'Other Additional School Fees','amount'=>$affiliated_total);

		//log_message('INFO',"current academic term  ==>".print_r($current_academic_term_obj->term,true));  //Toyet 4.17.2018

		if( $current_academic_term_obj->term == 'Summer'){

			$payable_midterm = $this->balances_lib->due_this_period('midterm', TRUE, $current_academic_term_obj->term);

		} else {

			$payable_prelim = $this->balances_lib->due_this_period('prelim', TRUE,  $current_academic_term_obj->term);

		}

		//added Toyet 11.23.2017
		//  attempt to extract balance before assessment
		$temp = $this->balances_lib->current_term_transactions();
		if( $temp[0]->transaction_detail = 'FORWARDED BALANCE>>>>' ){
			//$balance_before_assessment = $temp[0]->debit;
			$balance_before_assessment = $this->balances_lib->balanceBeforeAssessment();
		}

		//log_message('INFO',"printing.... BAL B4 ASSESS ==>".print_r($balance_before_assessment,true));
		//log_message('INFO',"printing.... ASSESS RECORD ==>".print_r($assess,true));
		//log_message("INFO", "INVOICES THIS TERM"); // Toyet 7.5.2018
		//log_message("INFO", print_r($invoices_this_term,true)); // Toyet 7.5.2018

		$assessed_value = $assess['assessed_amount'];                                     //added Toyet 11.23.2017
		//log_message('INFO',"printing.... ASSESSED VALUE ==>".print_r($assessed_value,true));
		$payable_prelim = $this->balances_lib->due_this_period( TRUE, $assessed_value);   //added Toyet 11.23.2017
		//log_message('INFO',"printing.... PRELIM DUE  ==>".print_r($payable_prelim,true));
		$trans_after_assess = $this->balances_lib->transAfterAssessment();
		//log_message('INFO',"trans after assess ==> ".print_r($trans_after_assess,true));

			if (!empty($assessment) && count($assessment) > 0) {
					$this->load->view('print_templates/student_assessment',
							array(
									'selected_history_id'=>$selected_history_id,
									'current_academic_term_obj'=>$current_academic_term_obj,
									'term'=>$current_academic_term_obj->term,
									'tuition_fees_content'=>isset($tuition_fees_content) ? $tuition_fees_content : array(),
									'miscellaneous_fees_content'=>isset($miscellaneous_fees_content) ? $miscellaneous_fees_content : array(),
									'idnumber'=>$student_details['idnumber'],
									'familyname'=>$student_details['familyname'],
									'firstname'=>$student_details['firstname'],
									'middlename'=>$student_details['middlename'],
									'level'=>$student_details['level'],
									'course'=>$student_details['course'],
									'payable_midterm'=>(isset($payable_midterm) ? $payable_midterm : 0),
									'payable_prelim'=>(isset($payable_prelim) ? $payable_prelim : 0),
									'bal_b4_assessment'=>$balance_before_assessment,
									'assessed_value'=>$assessed_value,
									'trans_after_assess'=>$trans_after_assess,
							)
					);
			}
	
	//endif;

?>


