<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
	width:100%;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>
  <div style="background:#FFF; ">
<?php
	if (!$assessment) {
?>
<div>ASSESSMENT NOT POSTED YET!</div>
<?php
	} else {
?>

<form method="post" id="display_withdrawal computation">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
 
 <table width="44%" align="center" cellpadding="1" cellspacing="1" class="head1" style="width:40%;">
    <tr class="head">
       <td colspan="3" class="head"> TOTAL WITHDRAWAL COMPUTATION </td>
    </tr>
       <td colspan="2">
		  <tr>
            <td width="61%" valign="middle">Tuition Fee</td>
            <td width="3%" valign="middle">:</td>
            <td width="36%" valign="middle">
			<?php
					print(number_format($tuition_fee,2));
			?></td>
          </tr>
		  <tr>
            <td valign="middle">Percentage of Withdrawal Charges </td>
            <td valign="middle">:</td>
            <td valign="middle">
			<?php
				$withdraw_per = $withdraw_percentage * 100;
				print(number_format($withdraw_per));
			?>
			%			 </td>
           </tr>
		   <tr>
				<td valign="middle">Tuition Fee Charge</td>
				<td valign="middle">:</td>
				
				<td valign="middle">
				  <div align="right">
					<?php
					$tuition_charge = $tuition_fee * $withdraw_percentage;
					print(number_format($tuition_charge,2));
				?>
				   </div></td>
          </tr>
       	  <tr>
            <td valign="middle">Matriculation Fee</td>
            <td valign="middle">:</td>
            <td valign="middle">
			  <div align="right">
			    <?php
				print(number_format($matriculation,2));
			?>
		       </div></td>
          </tr>
		  <tr>
            <td valign="middle">Miscellaneous Fee</td>
            <td valign="middle">:</td>
            <td valign="middle">
			  <div align="right">
			    <?php
				print(number_format($miscellaneous,2));
			?>
		       </div></td>
         </tr>
		 <tr>
            <td valign="middle">Other Fee</td>
            <td valign="middle">:</td>
            <td valign="right">
			  <div align="right">
			    <?php
				print(number_format($other_fee,2));
			?>
		       </div></td>
          </tr>
		 <tr>
            <td valign="middle">Learning Resources</td>
            <td valign="middle">:</td>
            <td valign="right">
			  <div align="right">
			    <?php
				print(number_format($learning_resources,2));
			?>
		       </div></td>
          </tr>
		 <tr>
            <td valign="middle">Student Support Services</td>
            <td valign="middle">:</td>
            <td valign="right">
			  <div align="right">
			    <?php
				print(number_format($student_support,2));
			?>
		       </div></td>
          </tr>
		  <?php
				if ($other_add_fees) {
		  ?>
		 <tr>
            <td valign="middle">Other Additional School Fees</td>
            <td valign="middle">:</td>
            <td valign="right">
			  <div align="right">
			    <?php
				print(number_format($other_add_fees,2));
			?>
		       </div></td>
          </tr>
		  <?php
				}
			?>
		  <tr>
            <td valign="middle">Laboratory Fee ( <?php print(number_format($withdraw_per));
			?> %) </td>
            <td valign="middle">:</td>
            <td valign="middle" style="text-align:right; border-bottom:solid; border-width:1px;">
			    <?php
				$lab_charge = $lab_fee * $withdraw_percentage;
				if($lab_fee){
					print(number_format($lab_charge,2));
				}else{	
					$lab_charge = 0;
					print(number_format($lab_charge,2));
				}	
			?>		      </td>
         </tr>
		 <tr >
            <td valign="middle" style="padding-top: 5px;">Total Charges</td>
            <td valign="middle" style="padding-top: 5px;">:</td>
            <td valign="right" style="padding-top:5px;">
			  <div align="right" >
			    <?php
				$total_tuition = $tuition_charge + $matriculation + $miscellaneous + $other_fee + $lab_charge + $learning_resources 
									+ $student_support + $other_add_fees;
				if($total_tuition >= 0){
					print(number_format($total_tuition,2));
				}else{
					print("(".number_format(abs($total_tuition),2).")");
				}	
			?>
		       </div></td>
          </tr>
		  <tr>
            <td valign="middle">Other Charges</td>
            <td valign="middle">:</td>
            <td valign="right">
			  <div align="right">
			    <?php
				if($other_payable >= 0){
					print(number_format($other_payable,2));
				}else{
					print("(".number_format(abs($other_payable),2).")");
				}	
			?>
		       </div></td>
          </tr>
		 <!-- 
		 <tr>
            <td valign="middle"><strong>TOTAL Charges </strong></td>
            <td valign="middle"><strong>:</strong></td>
            <td valign="middle">
			  <div align="right"><strong>
			    <?php
				/*$total = $tuition_charge + $matriculation + $miscellaneous + $other_fee + $lab_charge + $other_payable;
				if($total >= 0){
					print(number_format($total,2));
				}else{
					print("(".number_format($total,2).")");
				}	*/
			?>			 
		      </strong></div></td>
		 </tr>	  -->
		  <tr>
            <td valign="middle">Less: Payments/CR adjustments</td>
            <td valign="middle">:</td>
            <td valign="right" style="text-align:right; border-bottom:solid; border-width:1px;">
			  
			    <?php
				print("- ".number_format($amount_paid,2));
			?>
		       </td>
          </tr> 
		   <tr>
            <td valign="middle" style="padding-top: 5px; font-weight:bold;">Cash to be paid</td>
            <td valign="middle" style="padding-top: 5px; font-weight:bold;">:</td>
            <td valign="right"  style="padding-top: 5px; font-weight:bold;">
			  <div align="right">
		      <?php
				$cash_payable = $total_tuition + $other_payable - $amount_paid;
				//print($total_tuition."<br>".$other_payable."<br>".$amount_paid."<p>");
				if($cash_payable > 0){
					print(number_format($cash_payable,2));
				}else{
					//print("(".abs(number_format($cash_payable,2)).")");
					print(number_format($cash_payable,2));
				}	
			?>
              </div></td>
          </tr> 
    </td>
    </table>
</form>
<?php			
		}
?>
 </div>

