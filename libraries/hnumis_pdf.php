<?php
	class hnumis_pdf extends my_pdf {
		
		
		private $HeaderTitle="";
		private $history_id="";
		private $myFooter=-10;
		private $with_header=TRUE;
		private $with_footer=TRUE;
		private $with_TOR_header=FALSE;
		private $with_TOR_footer=FALSE;
		private $student_name;
		private $student_idno;
		private $student_program;
		private $graduated_programs;
		private $prepared_employee;
		private $checked_employee;
		private $date_tor;
		private $granted_status;
		private $remarks;		
		private $remarks_linenum=0;		
		private $last_page = FALSE;
		private $with_prospectus_header = FALSE;
		private $new_term = FALSE;
		private $prospectus_sy="";
		
		var $wLine; // Maximum width of the line
		var $hLine; // Height of the line
		var $Text; // Text to display
		var $border;
		var $align; // Justification of the text
		var $fill;
		var $Padding;
		var $lPadding;
		var $tPadding;
		var $bPadding;
		var $rPadding;
		var $TagStyle; // Style for each tag
		var $Indent;
		var $Space; // Minimum space between words
		var $PileStyle; 
		var $Line2Print; // Line to display
		var $NextLineBegin; // Buffer between lines 
		var $TagName;
		var $Delta; // Maximum width minus width
		var $StringLength; 
		var $LineLength;
		var $wTextLine; // Width minus paddings
		var $nbSpace; // Number of spaces in the line
		var $Xini; // Initial position
		var $href; // Current URL
		var $TagHref; // URL for a cell
		
		function set_HeaderTitle($text, $student_histories_id=NULL) {
			$this->HeaderTitle=$text;
			$this->history_id=$student_histories_id;
		}
		
		function get_HeaderTitle() {
			return $this->HeaderTitle;
		}
		
		function set_Footer($footer) {
			$this->myFooter=$footer;
		}
		
		function get_Footer() {
			return $this->myFooter;
		}
		
		function WithHeader($my_head) {
			$this->with_header= $my_head;
		}
		
		function WithFooter($my_footer) {
			$this->with_footer = $my_footer;
		}
		
		function set_TOR_header($head) {
			$this->with_TOR_header=$head;		
		}
		
		function set_TOR_footer($footer) {
			$this->with_TOR_footer=$footer;
		}
		
		function set_student_name($name) {
			$this->student_name=$name;
		}
		
		function set_student_program($program) {
			$this->student_program=$program;
		}

		function set_prepared_employee($employee) {
			$this->prepared_employee=$employee;
		}
		
		function set_checked_employee($employee) {
			$this->checked_employee=$employee;
		}
		
		function set_date_tor($deyt) {
			$this->date_tor=$deyt;
		}
		
		function set_granted_status($status) {
			$this->granted_status=$status;
		}

		function set_remarks($remarks) {
			$this->remarks=$remarks;
		}

		function set_remarks_linenum($remarks_num) {
			$this->remarks_linenum=$remarks_num;
		}
		
		function set_graduated_programs($programs) {
			$this->graduated_programs=$programs;
		}
		
		function set_student_idno($idno) {
			$this->student_idno=$idno;
		}
		
		function set_last_page($page) {
			$this->last_page=$page;
		}
		
		function set_with_prospectus_header($prospectus) {
			$this->with_prospectus_header=$prospectus;
		}
		
		function set_prospectus_sy($sy) {
			$this->prospectus_sy=$sy;
		}
		
		function set_next_term($next) {
			$this->new_term=$next;
		}
		
		
		function Header() {
			
			if ($this->with_TOR_header) {
				$this->Image(base_url('assets/img/hnu_logo_tor.jpg'),13,11,30,36,'JPG');
				$this->SetX(150);
				$this->SetFont('helvetica','B',9);
				$this->Cell(10,4,'07040');
				$this->SetFont('helvetica','',8);
				$this->Cell(10,4, $this->student_idno.' 201401111401');
				$this->SetFont('helvetica','B',9);
				$this->Ln();
				$this->SetX(150);
				$this->Cell(10,4,'Re-Accredited:');
				$this->Ln();
				$this->SetFont('helvetica','',9);
				$this->SetX(160);
				$this->MultiCell(45,4,'Philippine Accrediting Association of Schools, Colleges and Universities (PAASCU - 1987) as certified to by the Federation of Accrediting Agencies of The Philippines (FAAP)',0,'L',false);
				if ($this->granted_status) {
					$this->Ln();
					$this->SetX(150);
					$this->SetFont('helvetica','B',9);
					$this->Cell(10,4,'Granted:');
					$this->Ln();
					$this->SetFont('helvetica','',9);
					$this->SetX(160);
					$this->MultiCell(45,4,'Autonomy Status per CHED Memo Order No. 32, Series of 2001',0,'L',false);
				}
				$this->Image(base_url('assets/img/hnu.png'),60,23,85,10,'PNG');
				$this->SetXY(0,35);
				$this->SetFont('helvetica','B',14);
				$this->Cell(0,0,'Office of the Registrar',0,0,'C');
				$this->SetXY(0,40);
				$this->SetFont('helvetica','',11);
				$this->Cell(0,0,'City of Tagbilaran',0,0,'C');
				$this->SetXY(0,50);
				//$this->SetFont('helvetica','B',16);
				$this->SetFont('bookosb','',15);
				$this->Cell(0,10,'Official Transcript of Records',0,0,'C');
				$this->Ln();
				
				if ($this->PageNo() > 1) {
					if ($this->granted_status) {
						$this->Ln();
					}
					$this->SetFont('bookosb','',11);
					$this->Cell(0,5,$this->student_name,0,0,'L');						
					$this->Ln();
					$this->Ln();
					$this->SetFont('bookosb','',11);
					$first_course=TRUE;
		
					$l1 = 5;
					foreach($this->graduated_programs AS $program) {
						if (strlen($program->program_name) > 70) {
							$l1 = $l1-1;
							if ($first_course) {
								$this->Cell(30,$l1,'Course:   ');
								$first_course = FALSE;
								$this->SetX(30);
								$this->MultiCell(160,$l1,$program->program_name,0,'L');
							} else {
								$this->SetX(30);
								$this->MultiCell(160,$l1,$program->program_name,0,'L');
							}	
						} else {
							if ($first_course) {
								$this->Cell(30,$l1,'Course:   ');
								$first_course = FALSE;
								$this->SetX(30);
								$this->MultiCell(160,$l1,$program->program_name,0,'L');
							} else {
								$this->SetX(30);
								$this->MultiCell(160,$l1,$program->program_name,0,'L');
							}	
						}
					}
					
					//$this->Ln();
					$this->SetY($this->GetY()+1);
					$this->SetLineWidth(0.5);
					$this->SetFont('bookosb','',12);
					$this->Cell(0,8,'C O L L E G I A T E    R E C O R D','TB',0,'C');
					$this->SetFont('helvetica','',9);
					$this->SetXY(13,$this->GetY()+10);
					$this->MultiCell(25,3.5,'School Term & Course No.',0,'C');
					$this->SetXY(38,$this->GetY()-7);
					$this->MultiCell(120,7,'Descriptive Title',0,'C');
					$this->SetXY(172,$this->GetY()-7);
					$this->MultiCell(20,7,'Final Rating',0,'L');
					$this->SetXY(188,$this->GetY()-7);
					$this->MultiCell(15,7,'Units',0,'C');
					$this->Ln();
					$this->Line(10,$this->GetY()-5,206,$this->GetY()-5);
						
					$this->SetY($this->GetY()-4); //start for the content for 2nd page			
				} 
				
			} else {
				if ($this->with_header) {
					$this->Image(base_url('assets/img/hnulogo100px.png'),11,7,13,17,'PNG');
					$this->SetX(25);
					$this->SetFont('times','B',20);
					$this->SetTextColor(19,99,3);
					$this->Cell(10,4,'Holy Name University');
					$this->SetTextColor(43,43,43);
					$this->SetFont('times','I',11);
					$this->Ln();
					$this->SetX(25);
					// RA 03.15.2018 - change of address
					// $this->Cell(10,8,'Lesage St., Tagbilaran City, Bohol');
					$this->Cell(10,8,'Janssen Heights, J. A. Clarin St., Tagbilaran City, Bohol');
					
					$this->MyHeader();
					
				} else {
					$this->SetY(25);
					$this->Ln();
				}
			}
		}

		
		function MyHeader() {
			$this->Ln();
			$this->SetX(25);
			$this->SetFont('times','B',10);
			$this->Cell(10,1,$this->get_HeaderTitle());		
			$this->Ln(4);
			
			if ($this->with_prospectus_header AND ($this->PageNo() > 1) AND !$this->new_term) {
				$this->Prospectus_header();
			}
		}

		
		private function Prospectus_header() {
			$this->Ln();
			$this->SetDrawColor(165,165,165);
			$this->SetTextColor(10,10,10);
			$this->SetFillColor(190,190,190);
			$this->SetFont('times','B',10);
			$this->Cell(20,4,$this->prospectus_sy,0,1,'L',false);
			$this->SetFont('times','',9.5);
				
			$this->MultiCell(23,8,'Course Code',1,'C',true);
			$this->SetXY(33,$this->GetY()-8);
			$this->MultiCell(85,8,'Descriptive Title',1,'C',true);
			$this->SetXY(118,$this->GetY()-8);
			$this->SetFont('times','',9);
			$this->MultiCell(10,4,'Credit Units',1,'C',true);
			$this->SetXY(128,$this->GetY()-8);
			$this->MultiCell(12,4,'Cut-off Grade',1,'C',true);
			$this->SetXY(140,$this->GetY()-8);
			$this->MultiCell(13,4,'No. of Retakes',1,'C',true);
			$this->SetFont('times','',9.5);
			$this->SetXY(153,$this->GetY()-8);
			$this->MultiCell(50,8,'Pre-requisites',1,'C',true);
				
			return;
		}
		
		
		//Page footer
		function Footer() {
			//$this->Cell(30,4,'Graasdasdasdasd a sdasdas asdasm',0,0,'L',false);
				
			if ($this->with_TOR_footer) {
	   			 // Go to 12 cm from bottom
				$ln = -95;
				$l = 3;
				if (!$this->last_page) {
					//print("PAGE:".$this->last_page); 
					$this->SetY($ln);
					$this->SetLineWidth(0.1);
					if ($this->PageNo() == 1) {
						$this->Line(10,237,205,237);
					} else {
						//$this->SetY(235.2);
						$this->Line(10,238,205,238);
					}
					//$this->Cell(30,$l,$this->GetY());
					$this->Ln();
						
					$this->SetFont('helvetica','',8);
					$this->Cell(30,$l,'Grading System',0,0,'L',false);
					$this->SetX(35);
					$this->Cell(150,$l,'1.0-1.5 (90%-100%) Excellent; 1.6-2.0 (89%-85%) Very Good; 2.1-2.5 (84%-80%) Good; 2.6-3.0 (79%-75%) Passed;',0,0,'L',false);
	
					$this->Ln();
					$this->Cell(30,$l,' ');
					$this->SetX(35);
					$this->Cell(150,$l,'5.0 (Below 75%) Failure: must repeat; WD - Withdrawn Subject; DR - Dropped; INE - Incomplete Examination;',0,0,'L',false);
	
					$this->Ln();
					$this->Cell(30,$l,' ');
					$this->SetX(35);
					$this->Cell(150,$l,'INC - Incomplete Requirement; NC - No Credit; NA - Never Attended',0,0,'L',false);
	
					$this->SetXY(35,$ln+15);			
					$this->Cell(150,$l,'One collegiate unit of credit is one hour lecture or recitation each week for a period of complete semester of 18 units');
	
					$this->SetXY(35,$ln+21);
					$this->Cell(150,$l,'Three hours of laboratory work each week or a total of 54 hours a semester are regarded as equivalent also to one unit of credit.');
	
					$this->SetXY(35,$ln+27);
					$this->MultiCell(150,$l,'The semestral average grade of a student is computed by multiplying the number of units assigned to a course by the grade earned and the product is divided by the total units earned for the semester.');

				}
				//$l=4;
				//$ln = -95;


				// $this->SetXY(145,$ln+37);
				$this->SetXY(145,$ln+42);  // modified by RA to fix overlapping remarks line
				$this->SetFont('helvetica','',10);
				$this->Cell(150,$l,'Date:');
				//$this->SetXY(155,$ln+37);
				$this->SetXY(155,$ln+42); // modified by RA
				$this->SetFont('bookosb','',10);
				$mm = substr($this->date_tor,0,2);
				$dd = substr($this->date_tor,3,2);
				$yy = substr($this->date_tor,6,4);
				$deyt = date('d F Y',mktime(0,0,0,$mm,$dd,$yy));
				$this->Cell(150,$l,$deyt);
				
				if ($this->remarks_linenum >= 272) {
					$this->SetXY(45,-50);
					$this->SetFont('helvetica','',10);
					$this->Cell(150,16,'Prepared by:');  // 5
					$this->SetXY(67,-50);
					$this->SetFont('bookosb','',10);
					$this->Cell(10,16,strtoupper(utf8_decode($this->prepared_employee)));  // 5
				
					$this->SetXY(140,-43);
					$this->SetFont('bookosb','',10);
					$this->Cell(40,10,'PROF. VICTORIA C. MILLANAR',0,0,'C');  // 5
					$this->SetXY(140,-38);
					$this->SetFont('helvetica','',10);
					$this->Cell(40,8,'University Registrar',0,0,'C');  // 5
				} else {
					$this->SetXY(45,$ln+48);
					$this->SetFont('helvetica','',10);
					$this->Cell(150,$l,'Prepared by:');
					$this->SetXY(67,$ln+48);
					$this->SetFont('bookosb','',10);
					$this->Cell(10,$l,strtoupper(utf8_decode($this->prepared_employee)));

					$this->SetXY(140,$ln+52);
					$this->SetFont('bookosb','',10);
					$this->Cell(40,$l,'PROF. VICTORIA C. MILLANAR',0,0,'C');
					$this->SetXY(140,$ln+56);
					$this->SetFont('helvetica','',10);
					$this->Cell(40,$l,'University Registrar',0,0,'C');
				}


				$this->SetXY(45,$ln+63);
				$this->SetFont('helvetica','',10);
				$this->Cell(150,$l,'Checked by:');
				$this->SetXY(67,$ln+63);
				$this->SetFont('bookosb','',10);
				$this->Cell(10,$l,strtoupper(utf8_decode($this->checked_employee)));
				
				$this->SetXY(15,$ln+73);
				$this->SetFont('helvetica','',10);
				$this->Cell(30,$l,'NOT VALID WITHOUT',0,0,'C');
				$this->SetXY(15,$ln+77);
				$this->Cell(30,$l,'SEAL',0,0,'C');
				
				$this->SetXY(0,$ln+79);
				$this->SetFont('helvetica','',10);
				$this->Cell(0,5,' - '.$this->PageNo().'/{nb}'.' - ',0,0,'C');
			} else {
				if ($this->with_footer) {
					date_default_timezone_set('Asia/Manila');
			 		$deyt = date('D. M. j, Y g:iA');
		   			//Position at 1.0 cm from bottom
		    		$this->SetY($this->get_Footer());
		    		//Arial italic 8
		    		$this->SetFont('times','I',9);
		    		//Page number
					$this->Cell(30,5,'Run Date:'.$deyt.'  Page: '.$this->PageNo().'/{nb}');
					if ($this->history_id) {
						$this->SetX(175);
		    			$this->SetFont('times','B',9);
						$this->Cell(30,5,'*'.$this->history_id.'*',0,0,'R');
					}
				}
			}
		}

	function WriteTag($w, $h, $txt, $border=0, $align="J", $fill=false, $padding=0)
	{
		$this->wLine=$w;
		$this->hLine=$h;
		$this->Text=trim($txt);
		$this->Text=preg_replace("/\n|\r|\t/","",$this->Text);
		$this->border=$border;
		$this->align=$align;
		$this->fill=$fill;
		$this->Padding=$padding;

		$this->Xini=$this->GetX();
		$this->href="";
		$this->PileStyle=array();		
		$this->TagHref=array();
		$this->LastLine=false;

		$this->SetSpace();
		$this->Padding();
		$this->LineLength();
		$this->BorderTop();

		while($this->Text!="")
		{
			$this->MakeLine();
			$this->PrintLine();
		}

		$this->BorderBottom();
	}


	function SetStyle($tag, $family, $style, $size, $color, $indent=-1)
	{
		 $tag=trim($tag);
		 $this->TagStyle[$tag]['family']=trim($family);
		 $this->TagStyle[$tag]['style']=trim($style);
		 $this->TagStyle[$tag]['size']=trim($size);
		 $this->TagStyle[$tag]['color']=trim($color);
		 $this->TagStyle[$tag]['indent']=$indent;
	}


	// Private Functions

	function SetSpace() // Minimal space between words
	{
		$tag=$this->Parser($this->Text);
		$this->FindStyle($tag[2],0);
		$this->DoStyle(0);
		$this->Space=$this->GetStringWidth(" ");
	}


	function Padding()
	{
		if(preg_match("/^.+,/",$this->Padding)) {
			$tab=explode(",",$this->Padding);
			$this->lPadding=$tab[0];
			$this->tPadding=$tab[1];
			if(isset($tab[2]))
				$this->bPadding=$tab[2];
			else
				$this->bPadding=$this->tPadding;
			if(isset($tab[3]))
				$this->rPadding=$tab[3];
			else
				$this->rPadding=$this->lPadding;
		}
		else
		{
			$this->lPadding=$this->Padding;
			$this->tPadding=$this->Padding;
			$this->bPadding=$this->Padding;
			$this->rPadding=$this->Padding;
		}
		if($this->tPadding<$this->LineWidth)
			$this->tPadding=$this->LineWidth;
	}


	function LineLength()
	{
		if($this->wLine==0)
			$this->wLine=$this->w - $this->Xini - $this->rMargin;

		$this->wTextLine = $this->wLine - $this->lPadding - $this->rPadding;
	}


	function BorderTop()
	{
		$border=0;
		if($this->border==1)
			$border="TLR";
		$this->Cell($this->wLine,$this->tPadding,"",$border,0,'C',$this->fill);
		$y=$this->GetY()+$this->tPadding;
		$this->SetXY($this->Xini,$y);
	}


	function BorderBottom()
	{
		$border=0;
		if($this->border==1)
			$border="BLR";
		$this->Cell($this->wLine,$this->bPadding,"",$border,0,'C',$this->fill);
	}


	function DoStyle($tag) // Applies a style
	{
		$tag=trim($tag);
		$this->SetFont($this->TagStyle[$tag]['family'],
			$this->TagStyle[$tag]['style'],
			$this->TagStyle[$tag]['size']);

		$tab=explode(",",$this->TagStyle[$tag]['color']);
		if(count($tab)==1)
			$this->SetTextColor($tab[0]);
		else
			$this->SetTextColor($tab[0],$tab[1],$tab[2]);
	}


	function FindStyle($tag, $ind) // Inheritance from parent elements
	{
		$tag=trim($tag);

		// Family
		if($this->TagStyle[$tag]['family']!="")
			$family=$this->TagStyle[$tag]['family'];
		else
		{
			reset($this->PileStyle);
			while(list($k,$val)=each($this->PileStyle))
			{
				$val=trim($val);
				if($this->TagStyle[$val]['family']!="") {
					$family=$this->TagStyle[$val]['family'];
					break;
				}
			}
		}

		// Style
		$style="";
		$style1=strtoupper($this->TagStyle[$tag]['style']);
		if($style1!="N")
		{
			$bold=false;
			$italic=false;
			$underline=false;
			reset($this->PileStyle);
			while(list($k,$val)=each($this->PileStyle))
			{
				$val=trim($val);
				$style1=strtoupper($this->TagStyle[$val]['style']);
				if($style1=="N")
					break;
				else
				{
					if(strpos($style1,"B")!==false)
						$bold=true;
					if(strpos($style1,"I")!==false)
						$italic=true;
					if(strpos($style1,"U")!==false)
						$underline=true;
				} 
			}
			if($bold)
				$style.="B";
			if($italic)
				$style.="I";
			if($underline)
				$style.="U";
		}

		// Size
		if($this->TagStyle[$tag]['size']!=0)
			$size=$this->TagStyle[$tag]['size'];
		else
		{
			reset($this->PileStyle);
			while(list($k,$val)=each($this->PileStyle))
			{
				$val=trim($val);
				if($this->TagStyle[$val]['size']!=0) {
					$size=$this->TagStyle[$val]['size'];
					break;
				}
			}
		}

		// Color
		if($this->TagStyle[$tag]['color']!="")
			$color=$this->TagStyle[$tag]['color'];
		else
		{
			reset($this->PileStyle);
			while(list($k,$val)=each($this->PileStyle))
			{
				$val=trim($val);
				if($this->TagStyle[$val]['color']!="") {
					$color=$this->TagStyle[$val]['color'];
					break;
				}
			}
		}
		 
		// Result
		$this->TagStyle[$ind]['family']=$family;
		$this->TagStyle[$ind]['style']=$style;
		$this->TagStyle[$ind]['size']=$size;
		$this->TagStyle[$ind]['color']=$color;
		$this->TagStyle[$ind]['indent']=$this->TagStyle[$tag]['indent'];
	}


	function Parser($text)
	{
		$tab=array();
		// Closing tag
		if(preg_match("|^(</([^>]+)>)|",$text,$regs)) {
			$tab[1]="c";
			$tab[2]=trim($regs[2]);
		}
		// Opening tag
		else if(preg_match("|^(<([^>]+)>)|",$text,$regs)) {
			$regs[2]=preg_replace("/^a/","a ",$regs[2]);
			$tab[1]="o";
			$tab[2]=trim($regs[2]);

			// Presence of attributes
			if(preg_match("/(.+) (.+)='(.+)'/",$regs[2])) {
				$tab1=preg_split("/ +/",$regs[2]);
				$tab[2]=trim($tab1[0]);
				while(list($i,$couple)=each($tab1))
				{
					if($i>0) {
						$tab2=explode("=",$couple);
						$tab2[0]=trim($tab2[0]);
						$tab2[1]=trim($tab2[1]);
						$end=strlen($tab2[1])-2;
						$tab[$tab2[0]]=substr($tab2[1],1,$end);
					}
				}
			}
		}
	 	// Space
	 	else if(preg_match("/^( )/",$text,$regs)) {
			$tab[1]="s";
			$tab[2]=' ';
		}
		// Text
		else if(preg_match("/^([^< ]+)/",$text,$regs)) {
			$tab[1]="t";
			$tab[2]=trim($regs[1]);
		}

		$begin=strlen($regs[1]);
 		$end=strlen($text);
 		$text=substr($text, $begin, $end);
		$tab[0]=$text;

		return $tab;
	}


	function MakeLine()
	{
		$this->Text.=" ";
		$this->LineLength=array();
		$this->TagHref=array();
		$Length=0;
		$this->nbSpace=0;

		$i=$this->BeginLine();
		$this->TagName=array();

		if($i==0) {
			$Length=$this->StringLength[0];
			$this->TagName[0]=1;
			$this->TagHref[0]=$this->href;
		}

		while($Length<$this->wTextLine)
		{
			$tab=$this->Parser($this->Text);
			$this->Text=$tab[0];
			if($this->Text=="") {
				$this->LastLine=true;
				break;
			}

			if($tab[1]=="o") {
				array_unshift($this->PileStyle,$tab[2]);
				$this->FindStyle($this->PileStyle[0],$i+1);

				$this->DoStyle($i+1);
				$this->TagName[$i+1]=1;
				if($this->TagStyle[$tab[2]]['indent']!=-1) {
					$Length+=$this->TagStyle[$tab[2]]['indent'];
					$this->Indent=$this->TagStyle[$tab[2]]['indent'];
				}
				if($tab[2]=="a")
					$this->href=$tab['href'];
			}

			if($tab[1]=="c") {
				array_shift($this->PileStyle);
				if(isset($this->PileStyle[0]))
				{
					$this->FindStyle($this->PileStyle[0],$i+1);
					$this->DoStyle($i+1);
				}
				$this->TagName[$i+1]=1;
				if($this->TagStyle[$tab[2]]['indent']!=-1) {
					$this->LastLine=true;
					$this->Text=trim($this->Text);
					break;
				}
				if($tab[2]=="a")
					$this->href="";
			}

			if($tab[1]=="s") {
				$i++;
				$Length+=$this->Space;
				$this->Line2Print[$i]="";
				if($this->href!="")
					$this->TagHref[$i]=$this->href;
			}

			if($tab[1]=="t") {
				$i++;
				$this->StringLength[$i]=$this->GetStringWidth($tab[2]);
				$Length+=$this->StringLength[$i];
				$this->LineLength[$i]=$Length;
				$this->Line2Print[$i]=$tab[2];
				if($this->href!="")
					$this->TagHref[$i]=$this->href;
			 }

		}

		trim($this->Text);
		if($Length>$this->wTextLine || $this->LastLine==true)
			$this->EndLine();
	}


	function BeginLine()
	{
		$this->Line2Print=array();
		$this->StringLength=array();

		if(isset($this->PileStyle[0]))
		{
			$this->FindStyle($this->PileStyle[0],0);
			$this->DoStyle(0);
		}

		if(count($this->NextLineBegin)>0) {
			$this->Line2Print[0]=$this->NextLineBegin['text'];
			$this->StringLength[0]=$this->NextLineBegin['length'];
			$this->NextLineBegin=array();
			$i=0;
		}
		else {
			preg_match("/^(( *(<([^>]+)>)* *)*)(.*)/",$this->Text,$regs);
			$regs[1]=str_replace(" ", "", $regs[1]);
			$this->Text=$regs[1].$regs[5];
			$i=-1;
		}

		return $i;
	}


	function EndLine()
	{
		if(end($this->Line2Print)!="" && $this->LastLine==false) {
			$this->NextLineBegin['text']=array_pop($this->Line2Print);
			$this->NextLineBegin['length']=end($this->StringLength);
			array_pop($this->LineLength);
		}

		while(end($this->Line2Print)==="")
			array_pop($this->Line2Print);

		$this->Delta=$this->wTextLine-end($this->LineLength);

		$this->nbSpace=0;
		for($i=0; $i<count($this->Line2Print); $i++) {
			if($this->Line2Print[$i]=="")
				$this->nbSpace++;
		}
	}


	function PrintLine()
	{
		$border=0;
		if($this->border==1)
			$border="LR";
		$this->Cell($this->wLine,$this->hLine,"",$border,0,'C',$this->fill);
		$y=$this->GetY();
		$this->SetXY($this->Xini+$this->lPadding,$y);

		if($this->Indent!=-1) {
			if($this->Indent!=0)
				$this->Cell($this->Indent,$this->hLine);
			$this->Indent=-1;
		}

		$space=$this->LineAlign();
		$this->DoStyle(0);
		for($i=0; $i<count($this->Line2Print); $i++)
		{
			if(isset($this->TagName[$i]))
				$this->DoStyle($i);
			if(isset($this->TagHref[$i]))
				$href=$this->TagHref[$i];
			else
				$href='';
			if($this->Line2Print[$i]=="")
				$this->Cell($space,$this->hLine,"         ",0,0,'C',false,$href);
			else
				$this->Cell($this->StringLength[$i],$this->hLine,$this->Line2Print[$i],0,0,'C',false,$href);
		}

		$this->LineBreak();
		if($this->LastLine && $this->Text!="")
			$this->EndParagraph();
		$this->LastLine=false;
	}


	function LineAlign()
	{
		$space=$this->Space;
		if($this->align=="J") {
			if($this->nbSpace!=0)
				$space=$this->Space + ($this->Delta/$this->nbSpace);
			if($this->LastLine)
				$space=$this->Space;
		}

		if($this->align=="R")
			$this->Cell($this->Delta,$this->hLine);

		if($this->align=="C")
			$this->Cell($this->Delta/2,$this->hLine);

		return $space;
	}


	function LineBreak()
	{
		$x=$this->Xini;
		$y=$this->GetY()+$this->hLine;
		$this->SetXY($x,$y);
	}


	function EndParagraph()
	{
		$border=0;
		if($this->border==1)
			$border="LR";
		$this->Cell($this->wLine,$this->hLine/2,"",$border,0,'C',$this->fill);
		$x=$this->Xini;
		$y=$this->GetY()+$this->hLine/2;
		$this->SetXY($x,$y);
	}
		
	}
		
?>
