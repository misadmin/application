<style>
	table.tibolclass 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
</style>
<div class="row-fluid span7">
<h3 class="heading">Paying Units:</h3>
<div class="loading"></div>

<div class="row-fluid">
	<?php if(isset($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $message; ?>
	</div>
	<?php endif; ?>
	<form id="frm_entry" method="post" action="" class="form-horizontal">
		<?php echo $this->common->hidden_input_nonce(); ?>
			<fieldset>
		    	<label for="end_yr" class="control-label">SY: </label>					    	
		    		<div class="controls">		    	
					<select id="end_yr" name="end_yr">
						<?php foreach ($end_yr as $key=>$value): ?>
							<option <?php echo ($value==$this->input->post('end_yr') ? 'selected="selected"' : ''); ?> value="<?php echo $value; ?>"><?php echo $value-1 .'-' . $value; ?></option>						
						<?php endforeach; ?>
					</select>
					</div>
		    	<label for="term" class="control-label">Term: </label>					    	
		    		<div class="controls">		    	
					<select id="term" name="term">
						<option value="">-- Select Term --</option>												
						<option value="1">1st Sem</option>						
						<option value="2">2nd Sem</option>						
						<option value="3">Summer</option>						
					</select>
					</div>		    
				<label for="college" class="control-label">College: </label>
					<div class="controls">
					<select id="colleges" name="college">
						<option value="">-- Select College --</option>	
							<?php foreach ($colleges as $college): ?>
						<option <?php echo ($college->id==$this->input->post('college') ? 'selected="selected"' : ''); ?> value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
							<?php endforeach; ?>
					</select>
					</div>
				<label for="programs" class="control-label">Program: </label>
					<div class="controls">
						<select id="programs" name="program">
						<option value="">-- Select Program --</option>	
						</select>
					</div>
				<label for="yr_level" class="control-label">Year Level: </label>
				<div class="controls">
					<select id="yr_level" name="yr_level">
						<option value="">-- Select Level --</option>	
						<option value="1">1st Year</option>	
						<option value="2">2nd Year</option>	
						<option value="3">3rd Year</option>	
						<option value="4">4th Year</option>	
						<option value="5">5th Year</option>	
					</select>
				</div>
				<label id="print_to" for="print_to" class="control-label">Target: </label>
				<div class="controls">
					<select id="print_to" name="print_to">
						<option value="Screen">Screen</option>	
						<option value="CSV">CSV (Can be opened by MS Excel)</option>	
						<option value="DOC" disabled>Word</option>						
						<option value="PDF" disabled>PDF</option>
					</select>
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>	
					
					
					
			</fieldset>
	</form>
</div>
<form method="post" action="<?php echo site_url('extras/paying_units_view'); ?>" id="frm_entry2">
	<?php echo $this->common->hidden_input_nonce(); ?>
</form>
<div id="table_here">
<hr>
<?php 
	if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {

?>

<table id="tibolid" class="table table-bordered table-striped table-condensed tibolclass">
<thead>
	<tr>
		<th></th>
		<th>I.D. No.</th>
		<th>Student</th>
		<th>Program</th>
		<th>Units</th>
	</tr>
</thead>

<?php
	if (count($rows) < 1) {
		echo '<td class="center" colspan=5>No record found!</td>';
	}else{
		$row_num = 0;
		$sum_units = 0.00;
		foreach($rows as $row){
			$row_num++;
			$sum_units +=$row->pay_units;
?>
			<tr >
			<td class="center"><?php echo $row_num .'.'; ?> </td>
			<td class="center"><a href="<?php echo site_url($this->session->userdata('role') . '/student') .'/'. $row->idno ?>">     	
				<?= $row->idno ?>  </a></td>	
			<td class="left"><?= $row->student ?></td>
			<td class="left"><?= $row->program ?></td>
			<td class="center"><?= number_format($row->pay_units,2) ?></td>
			</tr>		

<?php
		} 		
		if ($row_num > 1)
			echo '<tr><td colspan=4 class="right"><b>Average Paying Units...</b></td><td class="center"><b>'. number_format($sum_units/$row_num,2) . '</b></td></tr>';
	}
?>

</table>

<?php } 
?>

</div>

</div>

<script>
$(document).ready(function(){
	$("#tibolid").stickyTableHeaders();
    $("#tibolid").stickyTableHeaders('updateWidth');

	$('#frm_entry').validate({
		submitHandler: function(form) {
			$('.loading').delay(5).fadeIn(100);
			form.submit();
		}
	});
    

    
});
    

$('#colleges').change(function(){
	var college_id = $('#colleges option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id + "&status=O",
		dataType: "json",
		success: function(data){
			var doptions = make_options(data);
			$('#programs').html(doptions);
		}	
	});	
});


function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}


</script>