<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>


<body onLoad="show_blocks();">

<div style="background:#FFF; width:40%; text-align:center; margin:auto;">
	<table align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top:10px;" class="head1">
  		<tr class="head">
  			<td colspan="3" class="head" style="text-align:center; padding:10px;">ALLOCATE OFFERING </td>
    	</tr>
    	<tr>
    		<td colspan="3">
				<form action=" <?php echo site_url('dean/offerings'); ?>" method="post">  <?php echo validation_errors(); ?>
				<?php $this->common->hidden_input_nonce(FALSE); ?>
				<input type="hidden" name="step" value="12" />
			  	<table border="0" cellpadding="4" cellspacing="0" class="inside" style="width:100%">
					<tr>
				  		<td width="76" align="left">Allocation</td>
				  		<td width="5" align="left">:</td>
				  		<td width="419" align="left" valign="middle">
							<input name="allocation_type" type="radio" class="form" onClick="show_blocks();" value="blocks" checked="checked"/>  Block Section
							<input name="allocation_type" type="radio" class="form" onClick="show_groups();" value="program_groups"/>
							Program Groups
							<input name="allocation_type" type="radio" class="form" onClick="show_programs();" value="programs"/> Program/Year Level </td>
					</tr>
					<tr>
					  <td align="left">&nbsp;</td>
					  <td align="left">&nbsp;</td>
					  <td align="left" valign="middle">
					  
					  <table border="0" cellspacing="0" cellpadding="0" id="blocks" style="width:100%;">
			    				<tr>
      								<td width="24%" align="left">Block Section</td>
							    	<td width="2%" align="left">:</td>
						        	<td width="74%" style="text-align:left;"> <select name="blocksection" id="blocksections_id" class="form">
						          		<?php  foreach($blocksection as $block) {
										print("<option value= $block->id> ".$block->abbreviation. " - " .$block->yr_level." [ ".$block->section_name." ]</option>");
										} ?>
			      				  </select></td>
    							</tr>
  							</table>
							
						<table border="0" cellspacing="0" cellpadding="0" id="groups" style="width:100%;">
      							<tr>
        							<td width="30%" align="left">Program Group</td>
			        				<td width="2%" align="left">:</td>
			        				<td width="68%" style="text-align:left;">
										 <select id="groups" name="groups" class="form">
											<?php  foreach($program_groups as $group) {
													print("<option value=$group->id>".$group->abbreviation."</option>");
										} ?></select>
								  </td>
      							</tr>
								<tr>
        							<td width="30%" align="left">Year Level</td>
        							<td width="2%" align="left">:</td>
		        					<td width="68%" style="text-align:left;"><select name="yearlevel1" id="yearlevel1" class="form">
										<option value="0" selected="selected">All Year Levels</option>
										<option value="1">First Year</option>
										<option value="2">Second Year</option>
										<option value="3">Third Year</option>
										<option value="4">Fourth Year</option>
										<option value="5">Fifth Year</option>
							  	  </select></td>
      							</tr>
			   			</table>	
						
						<table border="0" cellspacing="0" cellpadding="0" id="programs" style="width:100%;">
      							<tr>
        							<td width="30%" align="left">Academic Program</td>
			        				<td width="2%" align="left">:</td>
			        				<td width="68%" style="text-align:left;">
										 <select id="programs" name="id" class="form">
												<?php foreach ($programs as $program): ?>
														<option value="<?php echo $program->id; ?>"><?php echo $program->abbreviation; ?></option>
													<?php endforeach; ?>
												</select>
								   </td>
      							</tr>
								<tr>
        							<td width="30%" align="left">Year Level</td>
        							<td width="2%" align="left">:</td>
		        					<td width="68%" style="text-align:left;"><select name="yearlevel" id="yearlevel" class="form">
								    <option value="0" selected="selected">All Year Levels</option>
		   							<option value="1">First Year</option>
								    <option value="2">Second Year</option>
								    <option value="3">Third Year</option>
								    <option value="4">Fourth Year</option>
								    <option value="5">Fifth Year</option>
							  	  </select></td>
      							</tr>
			   			</table>	
												
					  </td>
				  </tr> 
					<tr>
						<td colspan="3">
  							
</td>
					</tr>
			     	<tr>
      					<td>&nbsp;</td>
					    <td>&nbsp;</td>
      					<td style="text-align:left;"><input type="submit" name="button" id="button" value="Allocate!" class="btn btn-success" /></td>
			    	</tr>
				</table>
			</form>
			</td>
		</tr>
  </table>
</div>    

</td>
</tr>
</table>
</div>
</body>


<form id="delete_course" action="<?php echo site_url("dean/offerings")?>" method="post">
  <input type="hidden" name="step" value="12" />

</form>

<script>
	$('.delete_course').click(function(event){
		event.preventDefault(); 
		var allocation_id = $(this).attr('href');
		var allocation_type = $(this).attr('name');
		
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'allocation_type',
		   	value: allocation_id,
		}).appendTo('#delete_course');
		$('#delete_course').submit();
	});
</script>

<script type="text/javascript">
function hide(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'none';
  }
function show(obj)
  {
      var obj1 = document.getElementById(obj);
      obj1.style.display = 'table';
  }

function show_blocks()
{
	show('blocks');
	hide('groups');
	hide('programs');
	
}

function show_groups()
{
	hide('blocks');
	show('groups');
	hide('programs');
}

function show_programs()
{
	hide('blocks');
	hide('groups');
	show('programs');
}

</script>

