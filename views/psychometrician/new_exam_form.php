<?php 
//Add a new examination type...
?>
<div class="fluid span6">
<form id="new_exam_form" method="post" action="" class="form-horizontal">
<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value="new_exam" />
		<fieldset>
			<div class="control-group formSep">
				<label for="exam_abbreviation" class="control-label">Exam Abbreviation</label>
				<div class="controls">
					<input type="text" id="exam_abbreviation" name="exam_abbreviation" class="input-xlarge" value=""/>
				</div>
			</div>
			<div class="control-group formSep">
				<label for="exam_description" class="control-label">Exam Description</label>
				<div class="controls">
					<textarea id="exam_description" name="exam_description" class="input-xlarge"></textarea>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-primary" type="submit">Insert New Exam</button>
				</div>
			</div>
		</fieldset>
</form>
</div>
<script>
$(document).ready(function(){
	$('#new_exam_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			exam_abbreviation: { required: true, minlength: 2},
			exam_description: { required: true, minlength: 6 },
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
	$('#edit_exam_form').validate({
		onkeyup: false,
		errorClass: 'error',
		validClass: 'valid',
		rules: {
			exam_abbreviation: { required: true, minlength: 2},
			exam_description: { required: true, minlength: 6 },
		},
		highlight: function(element) {
			$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
	});
});
</script>