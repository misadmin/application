
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Year Level</div>
			<div class="col2a">
				<select class="form-control" style="width:auto; height:auto;" id="strand_id_add_modal" >
					<?php 
						if ($yr_levels) {
							foreach ($yr_levels AS $yr_level) {
					?>		
								<option value="<?php print($yr_level->id); ?>" ><?php print($yr_level->description); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Effective Year</div>
			<div class="col2a">
				<input type="text" class="form-control" id="effective_year_modal" style="width:100px;" />
			</div>
		</div>
	</div>
