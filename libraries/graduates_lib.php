<?php

class graduates_lib {
	
	/**
	 * Codeigniter Instance
	 * @var unknown_type
	 */
	protected $CI;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	
	
	/*
	 * @added: 3/21/15
	 * @author: genes
	 */
	public function view_candidates($college_id=NULL) {
	
		$this->CI->load->model('hnumis/student_model');
		$this->CI->load->model('hnumis/programs_model');
		$this->CI->load->model('hnumis/AcademicYears_Model');
		
		$action     = ($this->CI->input->post('action') ?  $this->CI->input->post('action') : 'list_graduates');
		$acad_terms = $this->CI->AcademicYears_Model->ListAcademicTerms(FALSE);
		
		switch ($action) {
			case 'list_graduates':
	
				$current_term = $this->CI->academic_terms_model->getCurrentAcademicTerm();
				$students   = $this->CI->student_model->ListCollegeGraduates($college_id,NULL,$current_term->id);
				$programs   = $this->CI->programs_model->ListProgramsWithGraduates($college_id,$current_term->id);
				
				$data = array(
						"terms"=>$acad_terms,
						"selected_term" => $current_term->id,
						"students" => $students,
						"programs" => $programs,
						"selected_program" => 0,
						"show_current_term" => $current_term->term.' '.$current_term->sy,
				);
				
				break;
	
			case 'display_selected':
				
				$current_term = $this->CI->AcademicYears_Model->getAcademicTerms($this->CI->input->post('academic_terms_id'));
				
				$students   = $this->CI->student_model->ListCollegeGraduates($college_id,
																		$this->CI->input->post('academic_programs_id'),
																		$current_term->id);
				if (!$students) {
					$students   = $this->CI->student_model->ListCollegeGraduates($college_id,NULL,$current_term->id);
				}
				$programs   = $this->CI->programs_model->ListProgramsWithGraduates($college_id,$current_term->id);
																			
				$data = array(
						"terms"=>$acad_terms,
						"selected_term" => $current_term->id,
						"students" => $students,
						"programs" => $programs,
						"selected_program" => $this->CI->input->post('academic_programs_id'),
						"show_current_term" => $current_term->term.' '.$current_term->sy,
				);
			
				break;
				
			case 'download_to_pdf':
				
				$data['students'] = json_decode($this->CI->input->post('students'));
				$data['selected_term'] = $this->CI->input->post('selected_term');
				
				$this->Generate_candidates_pdf($data);
				
				break;
	
		}
			
		$this->CI->content_lib->enqueue_body_content('dean/display_candidates_for_graduation',$data);
		$this->CI->content_lib->content();
			
	}
	
	
	/*
	 * @ADDED: 3/24/15
	 * @author: genes
	 */
	private function Generate_candidates_pdf($data) {
		
		$this->CI->load->library('my_pdf');
		$this->CI->load->library('hnumis_pdf');
		$l = 6;
		
		$this->CI->hnumis_pdf->AliasNbPages();
		$this->CI->hnumis_pdf->set_HeaderTitle("CANDIDATES FOR GRADUATION: ".$data['selected_term']);
		$this->CI->hnumis_pdf->AddPage('P','folio');
		
		$this->CI->hnumis_pdf->SetDisplayMode('fullwidth');
		$this->CI->hnumis_pdf->SetDrawColor(165,165,165);
			
		$this->CI->hnumis_pdf->SetFont('times','B',11);
		$this->CI->hnumis_pdf->SetTextColor(10,10,10);
		$this->CI->hnumis_pdf->SetFillColor(190,190,190);
		$this->CI->hnumis_pdf->Ln();
		$this->CI->hnumis_pdf->SetX(15);
		
		$header1 = array('Count','ID No.','Name','Gender','Course');

		$w1 = array(11,22,100,20,30);

		for($i=0;$i<count($w1);$i++)
			$this->CI->hnumis_pdf->Cell($w1[$i],8,$header1[$i],1,0,'C',true);

		$this->CI->hnumis_pdf->SetFont('Arial','',10);
		$this->CI->hnumis_pdf->SetTextColor(0,0,0);
		$this->CI->hnumis_pdf->SetFillColor(255,255,255);
		$cnt=0;		
		foreach($data['students'] AS $student) {
			$cnt++;
			$this->CI->hnumis_pdf->Ln();
			$this->CI->hnumis_pdf->SetX(15);
			$this->CI->hnumis_pdf->Cell($w1[0],$l,$cnt.'.',1,0,'R',true);
			$this->CI->hnumis_pdf->Cell($w1[1],$l,$student->idno,1,0,'C',true);
			$this->CI->hnumis_pdf->Cell($w1[2],$l,utf8_decode($student->neym),1,0,'L',true);
			$this->CI->hnumis_pdf->Cell($w1[3],$l,$student->gender,1,0,'C',true);
			$this->CI->hnumis_pdf->Cell($w1[4],$l,$student->abbreviation,1,0,'C',true);
		}
			
		$this->CI->hnumis_pdf->Output();
		
	}	
	
}