
	<div style="width:100%; padding:3px; font-size:11px; overflow: auto;">
		<div style="width:100%">
			<div class="col1">Strand</div>
			<div class="col2a">
				<select class="form-control" style="width:auto; height:auto;" id="strand_id_add_modal" >
					<?php 
						if ($strands) {
							foreach ($strands AS $strand) {
					?>		
								<option value="<?php print($strand->id); ?>" ><?php print($strand->description); ?></option>
					<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div style="width:100%">
			<div class="col1">Effective Year</div>
			<div class="col2a">
				<input type="text" class="form-control" id="effective_year_modal" style="width:100px;" />
			</div>
		</div>
	</div>
