<?php

	class Deanr extends MY_Controller {
		
		public function __construct() {
			parent::__construct();
	
			$role = $this->session->userdata('role');
			if($role != 'dean') {
				redirect('dean/welcome_dean');	
			}
			
			//for data
			$this->load->model('hnumis/College_Model');
			$this->load->model('hnumis/Programs_Model');
			$this->load->model('hnumis/AcademicYears_Model');
			$this->load->model('hnumis/Courses_Model');
			$this->load->model('hnumis/Rooms_Model');
			$this->load->model('hnumis/Offerings_Model');
			$this->load->model('hnumis/Faculty_Model');
			$this->load->model('hnumis/Student_Model');
	
			//for validation
			$this->load->library('form_validation');
			$this->load->view('dean/html_head');
			$this->load->view('dean/dean_dashboard');
		}
	
	
		public function index() {
	
			$role = $this->session->userdata('role');
			if($role != 'dean') {
				redirect('dean/welcome_dean');	
			}
	
			$this->load->view('dean/footer');
		}
		
		/*
		** method to create academic programs
		**
		*/
		public function create_program() {

			$data['groups'] = $this->Programs_Model->ListProgramGroups();

			if ($this->input->post('departments_id')) {

				$this->session->set_userdata('departments_id',$this->input->post('departments_id'));

				$data['department_name'] = $this->College_Model->getDepartment($this->input->post('departments_id'));
				
				$this->load->view('dean/create_program', $data);
			
			} elseif (!$this->input->post('abbreviations')) { //going to create a program
				
				$data['departments'] = $this->College_Model->ListCollegeDepartments($this->session->userdata('colleges_id'));
					
				if ($data['departments'] != NULL) {	
					$this->load->view('dean/list_department', $data);
				} else {
					$this->session->set_userdata('departments_id',0);				
					$this->load->view('dean/create_program', $data);
				}
								
			} else {
				$data = array();
						
				$data['departments_id'] 		= $this->session->userdata('departments_id');
				$data['colleges_id']    		= $this->session->userdata('colleges_id');
				$data['abbreviations'] 			= $this->input->post('abbreviations');
				$data['description']    		= $this->input->post('description');
				$data['acad_program_groups_id']	= $this->input->post('acad_program_groups_id');
				$data['status']         		= "I";
				
				$this->Programs_Model->AddProgram($data);
				
				redirect('dean/dean');
			}

			$this->load->view('dean/footer');

		}
		
		public function offer_course_schedules() {
			
			if (!$this->input->post('academic_terms_id') && !$this->session->userdata('academic_terms_id')) {
				$this->list_terms_courses();
			} elseif($this->input->post('academic_terms_id'))  {
				$this->session->set_userdata('academic_terms_id',$this->input->post('academic_terms_id'));
				$this->input_courses();
			} elseif ($this->input->post('courses_id')) {
				$this->select_vacant_room($this->input->post('courses_id'));
			} elseif ($this->input->post('room_id')) {
				$data = array();

				$s_code = $this->Offerings_Model->getLastSection($this->session->userdata('courses_id'),$this->session->userdata('academic_terms_id'));
				
				if ($s_code == NULL) {
					$data['section_code']  	= 'A';
				} else {
					$data['section_code']  	= CHR(ORD($s_code->section_code) + 1);
				}

				$data['academic_terms_id']	= $this->session->userdata('academic_terms_id');
				$data['courses_id']        	= $this->session->userdata('courses_id');
				$data['rooms_id']   		= $this->input->post('room_id');			
				$data['start_time'] 		= $this->session->userdata('start_time');				
				$data['end_time'] 			= $this->session->userdata('end_time');				
				$data['day_code'] 			= $this->session->userdata('day_code');				
				$data['max_students'] 		= $this->session->userdata('max_students');				
				$data['additional_charge'] 	= $this->session->userdata('additional_charge');				
				
				$data['course_offerings_id']=$this->Offerings_Model->AddOffering($data);

				$this->Rooms_Model->AddRoomOccupancy($data);
				
				$this->Offerings_Model->AddOfferingSlot($data);				
				
				$this->session->unset_userdata('academic_terms_id');
				$this->session->unset_userdata('courses_id');
				$this->session->unset_userdata('start_time');
				$this->session->unset_userdata('end_time');
				$this->session->unset_userdata('day_code');
				$this->session->unset_userdata('max_students');
				$this->session->unset_userdata('additional_charge');
				$this->session->unset_userdata('academic_terms_id');
				
				redirect('dean/dean');				
				
			} else {
				$this->session->unset_userdata('academic_terms_id');
				$this->session->unset_userdata('courses_id');
				$this->session->unset_userdata('start_time');
				$this->session->unset_userdata('end_time');
				$this->session->unset_userdata('day_code');
				$this->session->unset_userdata('max_students');
				$this->session->unset_userdata('additional_charge');
				$this->session->unset_userdata('academic_terms_id');
				
				$this->list_terms_courses();
				
			}
			//$this->load->view('dean/footer');
			
		}
		
		public function offer_schedule_slots($id=null,$offering_id=null) {
			switch ($id) {
				case 1:
					$this->list_terms_courses();
					break;
				case 2:
					$this->session->set_userdata('academic_terms_id',$this->input->post('academic_terms_id'));
					$this->list_schedule_to_select();
					break;
				case 3:
					$this->session->set_userdata('course_offerings_id',$offering_id);
					$this->input_new_slot();
					break;
				case 4:
					$this->select_vacant_room($this->session->userdata('courses_id')); 
					break;
				case 5:
					$data['course_offerings_id'] = $this->session->userdata('course_offerings_id');
					$data['start_time'] 		= $this->session->userdata('start_time');				
					$data['end_time'] 			= $this->session->userdata('end_time');				
					$data['day_code'] 			= $this->session->userdata('day_code');				
					$data['rooms_id']   		= $this->input->post('room_id');			
					
					$this->Rooms_Model->AddRoomOccupancy($data);					
					$this->Offerings_Model->AddOfferingSlot($data);				

					$this->session->unset_userdata('academic_terms_id');
					$this->session->unset_userdata('courses_id');
					$this->session->unset_userdata('start_time');
					$this->session->unset_userdata('end_time');
					$this->session->unset_userdata('day_code');
					$this->session->unset_userdata('academic_terms_id');
					break;
									
				default:
					$this->session->unset_userdata('academic_terms_id');
					$this->session->unset_userdata('courses_id');
					$this->session->unset_userdata('start_time');
					$this->session->unset_userdata('end_time');
					$this->session->unset_userdata('day_code');
					$this->session->unset_userdata('academic_terms_id');

					$this->list_terms_courses();
					break;
			}	

			$this->load->view('dean/footer');
					
		}

	
		public function assign_faculty_to_schedule($step=null,$offering_id=null) {
			switch ($step) {
				case 1:
					$this->list_terms_courses();
					break;
				case 2:				
					$this->session->set_userdata('academic_terms_id',$this->input->post('academic_terms_id'));
					$this->list_schedule_to_select();
					break;
				case 3:
					$this->session->set_userdata('course_offerings_id',$offering_id);				
					$this->select_offering_faculty();
					break;
				case 4:
					$this->Offerings_Model->AssignFaculty($this->input->post('employee_empno'),$this->session->userdata('course_offerings_id'));
					$this->session->unset_userdata('academic_terms_id');
					break;
					
				default:
					$this->session->unset_userdata('academic_terms_id');
	
					$this->list_terms_courses();
					break;
			}
			
			$this->load->view('dean/footer');

		}


		public function dissolve_section() {
			
			if (!$this->input->post('academic_terms_id') && !$this->session->userdata('academic_terms_id')) {
				$this->list_terms_courses();
			} elseif ($this->input->post('academic_terms_id'))  {
				$this->session->set_userdata('academic_terms_id',$this->input->post('academic_terms_id'));
				$this->confirm_dissolve_section();
								
			} elseif ($this->input->post('course_offerings_id')) {
				$this->Offerings_Model->DissolveOffering($this->input->post('course_offerings_id'));
				$this->Rooms_Model->ClearRoomOccupancy($this->input->post('course_offerings_id'));
				$this->session->unset_userdata('academic_terms_id');

				redirect('dean/dean');				
			} else {
				$this->session->unset_userdata('academic_terms_id');

				$this->list_terms_courses();
			}

			$this->load->view('dean/footer');

		}
		
		
		public function advise_academic_program($status=null) {

			$data = $this->search_student();		

			if ($status == "ok") {
				$aterm = $this->AcademicYears_Model->getCurrentAcademicTerm();
				//print_r($aterm);
				//print($this->session->userdata('students_idno'));
				//die();
				$this->Student_Model->UpdateStudentProspectus($aterm->id, 
						$this->session->userdata('students_idno'), $this->input->post('prospectus_id'), $this->input->post('yr_level'));

				redirect('dean/dean');				
			} elseif ($data) {
				$data['prospectus']= $this->Programs_Model->ListPrograms($this->session->userdata('colleges_id'),'A');					
				$this->load->view('dean/advise_academic_program',$data);
			}
			
			$this->load->view('dean/footer');
						
		}
		

		public function view_course_offerings($step=null) {
			switch ($step) {
				case 1:
					$this->list_terms_courses();
					break;
				case 2:				
					$this->session->set_userdata('academic_terms_id',$this->input->post('academic_terms_id'));
					$this->list_schedule_to_select();
					break;
					
				default:
					$this->session->unset_userdata('academic_terms_id');
	
					$this->list_terms_courses();
					break;
			}
			$this->load->view('dean/footer');
		}


		public function view_class_list($step=null,$offering_id=null) {
			switch ($step) {
				case 1:
					$this->list_terms_courses();
					break;
				case 2:				
					$this->session->set_userdata('academic_terms_id',$this->input->post('academic_terms_id'));
					$this->list_schedule_to_select();
					break;
				case 3:				
					$this->session->set_userdata('course_offerings_id',$offering_id);				
					$this->list_students_enrolled();
					break;
					
				default:
					$this->session->unset_userdata('course_offerings_id');
					$this->session->unset_userdata('academic_terms_id');
	
					$this->list_terms_courses();
					break;
			}
			$this->load->view('dean/footer');
		}
		
/************************
** The following methods are private
**
*************************/

		private function list_terms_courses() {
			
			$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms();
			//$data['courses'] = $this->Courses_Model->ListCourses($this->session->userdata('colleges_id'),'A');
			$this->content_lib->enqueue_sidebar_widget('dean/list_academic_terms',$data);
		}

		private function input_courses() {
			
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			$data['courses'] = $this->Courses_Model->ListCourses($this->session->userdata('colleges_id'),'A');
			$data['rooms'] = $this->Rooms_Model->ListRooms();
			$data['days'] = $this->Offerings_Model->ListDays();
			
			$this->load->view('dean/input_offering_schedules',$data);
			
		}

		private function select_offering_faculty() {
			
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			//$data['offerings'] = $this->Offerings_Model->ListOfferings($this->session->userdata('academic_terms_id'),$this->session->userdata('colleges_id'));
			$data['faculty'] 		= $this->Faculty_Model->ListFaculty($this->session->userdata('colleges_id'));
			$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
			$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
			
			$this->load->view('dean/assign_faculty_to_schedule',$data);
			
		}


		private function list_schedule_to_select() {
			
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			$data['offerings'] = $this->Offerings_Model->ListOfferings($this->session->userdata('academic_terms_id'),$this->session->userdata('colleges_id'));
			
			$this->load->view('dean/list_schedule_to_select',$data);
			
		}

		private function input_new_slot() {
			
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
			$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
			$data['days']           = $this->Offerings_Model->ListDays();

			//print_r($data['offerings']->id);
			//die();
			$this->session->set_userdata('courses_id',$data['offerings']->courses_id);
			
			$this->load->view('dean/input_new_slot',$data);
			
		}
	
		
		private function confirm_dissolve_section() {
				
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			$data['offerings'] = $this->Offerings_Model->ListOfferings($this->session->userdata('academic_terms_id'),$this->session->userdata('colleges_id'));
				
			$this->load->view('dean/dissolve_section',$data);
		}


		private function list_students_enrolled() {
				
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			$data['offerings']      = $this->Offerings_Model->getOffering($this->session->userdata('course_offerings_id'));
			$data['enrollees']      = $this->Student_Model->ListEnrollees($this->session->userdata('course_offerings_id'));
			$data['offering_slot']  = $this->Offerings_Model->ListOfferingSlots($this->session->userdata('course_offerings_id'));
				
			$this->load->view('dean/list_students_enrolled',$data);
		}

		private function select_vacant_room($courses_id) {

			$hr1  = substr($this->input->post('start_time'),0,2);
			$min1 = substr($this->input->post('start_time'),3,2);
			$hr2  = substr($this->input->post('num_hr'),0,1);
			$min2 = substr($this->input->post('num_hr'),2,2);

			if ($this->input->post('am_pm') == 'PM') {
				$start_time = date('H:i', mktime($hr1+12,$min1,0,0,0,0));				
				$end_time	= date('H:i', mktime($hr1+12+$hr2,$min1+$min2,0,0,0,0));
			} else {
				$start_time = date('H:i', mktime($hr1,$min1,0,0,0,0));				
				$end_time	= date('H:i', mktime($hr1+0+$hr2,$min1+$min2,0,0,0,0));					
			}
			

			$this->session->set_userdata('start_time',$start_time);
			$this->session->set_userdata('end_time',$end_time);
			$this->session->set_userdata('courses_id',$courses_id);
			$this->session->set_userdata('day_code',$this->input->post('day_code'));
			$this->session->set_userdata('max_students',$this->input->post('max_students'));
			$this->session->set_userdata('additional_charge',$this->input->post('additional_charge'));

			
			$data['academic_terms'] = $this->AcademicYears_Model->getAcademicTerms($this->session->userdata('academic_terms_id'));
			$data['course']         = $this->Courses_Model->RetrieveCourse($courses_id);
			$data['schedule']       = $start_time." - ".$end_time." ".$this->input->post('day_code');
			
			$day_names = $this->Rooms_Model->ListDayNames($this->input->post('day_code'));
			
			//	print_r($day_names);
			foreach ($day_names AS $day) {
				$days[] = $day->day_name;
			}
			$d1 = json_encode($days);
			$d2 = str_replace(array('"','[',']'),array("'","(",")"),$d1);
			
			$data['bates_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
											$start_time, $end_time, $d2,3);
			$data['main_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
											$start_time, $end_time, $d2,1);
			$data['scanlon_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
											$start_time, $end_time, $d2,2);
			$data['freina_building'] = $this->Rooms_Model->ListVacantRooms($this->session->userdata('academic_terms_id'),
											$start_time, $end_time, $d2,7);
				
			$this->load->view('dean/select_vacant_room',$data);
		}


		private function search_student() {
			
			$this->form_validation->set_rules('student_idno', 'ID No.', 'required|numeric');
			
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('dean/search_student');
			} else {
	
				$data['student']=$this->Student_Model->getStudent($this->input->post('student_idno'));
			//print_r($data);
			//die();

				if ($data['student'] == null) {
					$msg['error'] = "ID No. not found!";	
					$this->load->view('dean/search_student',$msg);
				} else {
					foreach($data['student'] AS $k=>$v) {
						//print("$k == $v<br>");
						$this->session->set_userdata($k,$v);
					}
					return $data;
				}
			}
			
		}
		
	}


//End of dean.php