<?php

	class BlockSection_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function ListSections($year_level) 
		{
			$result = null;
			
			$q = "SELECT a.* 
					FROM 
						block_sections AS a 
					WHERE
						a.yr_level={$this->db->escape($year_level)} ";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		function GetLastSection($program_id,$yearlevel) 
		{
			$result = null;
			
			$q = "SELECT a.section_name 
					FROM 
						block_sections AS a
					WHERE 
						a.academic_programs_id = {$this->db->escape($program_id)} 
						AND a.yr_level = {$this->db->escape($yearlevel)} 
					ORDER BY
						 a.section_name DESC LIMIT 1";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}
		
		
		function ListSectionsProgram($year_level, $program) 
		{
			$result = null;
			
			$q = "SELECT a.* 
					FROM 
						block_sections AS a 
					WHERE
						a.yr_level={$this->db->escape($year_level)}
						AND a.academic_programs_id = {$this->db->escape($program)} ";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		function ListBlocks($program, $year_level, $term) 
		{
			$result = null;
			
			$q = "SELECT 
						c.id,
						c.section_name,
						COUNT(a.id) AS size
					FROM 
						block_sections as c LEFT JOIN student_histories AS a ON c.id = a.block_sections_id 
							AND a.academic_terms_id =  {$this->db->escape($term)}
					WHERE 
						c.academic_programs_id = {$this->db->escape($program)}
					 	AND c.yr_level = {$this->db->escape($year_level)} 
					GROUP BY
						c.id ";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}
		
		function ListAllSections() 
		{
			$result = null;
			
			$q = "SELECT a.id, b.abbreviation, a.yr_level, a.section_name 
					FROM 
						block_sections AS a, academic_programs AS b
					WHERE
						a.academic_programs_id = b.id"; 
			
				
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
			
		}	
		
		function getSection($block_id) 
		{
			$result = null;
			
			$q = "SELECT b.abbreviation, a.yr_level, a.section_name
					FROM
						block_sections AS a, academic_programs AS b
					WHERE
						a.academic_programs_id = b.id
						AND a.id = {$this->db->escape($block_id)} "; 
			
				
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}	

	

		function add_blocksection($program, $yearlevel, $section){

			$query = "
					INSERT into block_sections (section_name,yr_level,academic_programs_id,inserted_by,inserted_on)
					VALUES
						(
							{$this->db->escape($section)},
							{$this->db->escape($yearlevel)},
							{$this->db->escape($program)},
							{$this->session->userdata('empno')},
							now()
						)
					";
				$this->db->query($query);
				$id = @mysql_insert_id();			
				return $id;
		}

		function ListBlockByCollege($college_id, $courses_id) {
			$result = null;
			
			/*$q = "SELECT 
						a.id, 
						b.abbreviation, 
						a.yr_level, 
						a.section_name 
					FROM 
						block_sections AS a, 
						academic_programs AS b
					WHERE 
						a.academic_programs_id = b.id
						AND b.colleges_id={$this->db->escape($college_id)} "; 
			*/
			$q = "SELECT 
						a.id, 
						e.abbreviation, 
						a.yr_level, 
						a.section_name 
					FROM 
						block_sections AS a, 
						prospectus AS b,
						prospectus_terms AS c,
						prospectus_courses AS d,
						academic_programs AS e
					WHERE 
						a.academic_programs_id = b.academic_programs_id
						AND a.academic_programs_id = e.id
						AND b.id=c.prospectus_id
						AND c.id=d.prospectus_terms_id
						AND e.colleges_id={$this->db->escape($college_id)}
						AND d.courses_id =  {$this->db->escape($courses_id)}"; 
			
			//print($q);
			//die();	
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		}

		
		function UnblockOffering($block_id) {
			$q = "DELETE FROM 
						block_course_offerings 
					WHERE
						id={$this->db->escape($block_id)} ";
			
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}

	}


//end of AcademicYears_model.php