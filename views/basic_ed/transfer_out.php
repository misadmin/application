<?php 
	if ($stud_status != 'active') {
		print("<h3>Student NOT Active! Cannot be withdrawn!</h3>");
	} else {
?>

<?php if (isset($message) && $tab=='17') : ?>
		<div class="alert <?php echo $severity; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
		</div>
<?php endif; ?>
  <div style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#666; margin-top:10px; "> 
 <form method="post" id="assignSection">
	<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value= "transfer_out"/>
	<input type="hidden" name="stud_id" value="<?php print($student_id); ?>" > 
	    <table width="18%" align="center" cellpadding="0" cellspacing="0" style="width:100%;"> 
	  <tr>
		  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:20px;">
		    <div class="control-group formSep">
		    	<strong><font face="Verdana, Arial, Helvetica, sans-serif">Withdraw Student/Pupil</font></strong>
		   </div>
		   </td>
 	 </tr>
 	 
	 </table>	

	<div style="margin-left:20px; width:90%; margin-bottom:20px; ">
	     
	   <!--    <table width="18%" align="left" cellpadding="0" cellspacing="0" style="width:50%;">
			  <tr>
				  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">  -->
				   <label class="control-label"> <b> Academic Year: </b> </label>
					<div class="controls">
						<select id="academic_year_id" name="academic_year_id">
						<?php
							foreach($academic_years AS $academic_year) {
								print("<option value=".$academic_year->id.">".$academic_year->start_year."-".$academic_year->end_year."</option>");	
							}
						?>
						</select>
					</div>
			<!-- 	   </td>
				</tr>   
			 
		 	 <tr>
				  <td colspan="1" class="head" style="text-align:left; padding:10px; font-size:15px;">  -->
				   <label class="control-label" for="reason"><b> Reason:</b></label>
						 <input type="text" name="reason" style="width:700px; text-align:left">
						 
					<label for="date_taken" class="control-label">Date Withdrawn <i>(mm/dd/yyyy)</i>:</label>
					<input type="date" name="dateW" value="<?php echo date('Y-m-d'); ?>" >
				<!--  	<div class="controls">
						<div class="input-append date" id="dateF" data-date-format="yyyy-mm-dd">
							<input type="text" name="date_filed" id="date_filed" class="span6" value="<?php //echo date('Y-m-d'); ?>" readonly/>
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div> -->
			<!--  	   </td>
		 	 </tr>
		 	 <tr>
		 	     <div style="margin-left:25px;">
		 		 <td colspan = "2"> -->
			</div>
			 <div class="controls">
		 			 <button class="btn btn-primary" type="submit" style="margin-left:20px;" >Withdraw</button>
		 	</div>		 
		</div>	 
		 	<!-- 	 </td>
		 		 
		 	 </tr>
		  </table> 
 	 </div>  -->
 	 
  
	</form>
  <!--   </div>  --> 
  <?php }?>
  
  <script>
	$(document).ready(function(){
		$('#dateF').datepicker();
	
	});
	</script>
  