
<style>
	div#msg_header_transfer_strand {
		border-radius: 5px; 
		background-color: #D7D4D4; 
		border: solid; 
		border-width: 1px; 
		border-color: #B7B6B6; 
		text-align:left; 
		padding-left: 10px;
		margin:0 auto;
		margin-bottom:20px;
		font-family: arial;
		font-size: 14px;
		width: 98%;
		height: 35px;
		line-height: 35px;
		font-weight:bold;
		color:#B50303;
</style>

	<div class="row-fluid">
		<?php 
			if (!$is_registered) {
		?>
				<div id="msg_header_transfer_strand"> 
					Student not registered yet for the current semester! He/she needs to Register and Enroll first!
				</div>
		<?php		
			} elseif(!$is_enrolled) {
		?>	
				<div id="msg_header_transfer_strand"> 
					Student is not enrolled yet for current semester!
				</div>
		<?php		
			} else {
		?>	
			<div id="msg_header_transfer_strand"> </div>
			<div class="form-horizontal">
				<fieldset>
					<div class="control-group formSep">
						<label for="strand_id" class="control-label">Strand to Transfer</label>
						<div style="float:left; padding-left:20px;" >
							<select id="prospectus_id_to_transfer" name="strand_id_to_transfer" class="form-control" style="width:auto;">
								<?php 
									if ($strands) {
										foreach ($strands AS $strand) {
								?>
										<option value="<?php print($strand->id); ?>" 
											strand_id="<?php print($strand->academic_programs_id); ?>"
											<?php if ($strand->id == $current_prospectus_id) print("disabled"); ?> ><?php print($strand->description); ?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
						<div style="float:left; padding-left:10px; padding-top:5px;" id="show_search_icon">
						</div>
					</div>
					<div class="control-group formSep">
						<label for="block_id" class="control-label">Block Section</label>
						<div style="float:left; padding-left:20px;">
							<select id="block_sections_id" name="block_sections_id" class="form-control" >
								<?php 
									if ($to_sections) {
										foreach ($to_sections AS $section) {
								?>
										<option value="<?php print($section->id); ?>" ><?php print("Grade ".$section->yr_level.'-'.$section->section_name.' '.$section->abbreviation); ?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
					</div>

					<div class="control-group">
						<label for="strand" class="control-label" id="show_transfer_to_strand_icon">
						</label>
						<div class="controls">
							<a href="<?php print($idnum); ?>" 
								student_history_id="<?php print($student_histories_id); ?>" 
								class="btn btn-warning transfer_to_strand" >Transfer to Strand!
							</a>
						</div>
					</div>	
				</fieldset>

			</div>
		<?php
			}
		?>	

	</div>

<script>
	$(document).ready(function(){
		$('#prospectus_id_to_transfer').bind('change', function(){

			var element2  = $("option:selected", "#prospectus_id_to_transfer");
			var strand_id = element2.attr('strand_id');

			var student_idno      = '<?php print($idnum); ?>';
			var year_level        = '<?php print($year_level); ?>';
			var academic_terms_id = '<?php print($academic_terms_id); ?>';
		
			$('#show_search_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
			
			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+'/'+student_idno,
				data: {"strand_id": strand_id,
						"grade_level": year_level,
						"academic_terms_id": academic_terms_id,
						"action": "search_block_sections" },
				dataType: 'json',
				success: function(response) {													

					$('#block_sections_id').html(response.output);
					
					$('#show_search_icon').html("");
				
				}
			});
			
		});
	});
</script>


	
<script>
	$('.transfer_to_strand').click(function(e){
		e.preventDefault();		
		
		var prospectus_id     = $('#prospectus_id_to_transfer').val();
		var block_sections_id = $('#block_sections_id').val();

		if (isEmpty(block_sections_id)) {
			alert('Select a section to transfer the student!');
		} else {
		
			var confirmed = confirm('Transfer student to strand?');

			if (confirmed){
			
				var students_idno      = $(this).attr('href');
				var student_history_id = $(this).attr('student_history_id');

				$('#show_transfer_to_strand_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")

				$.ajax({
					cache: false,
					type: 'POST',
					url: "<?php print(site_url($this->uri->segment(1).'/process_student_action'));?>"+"/"+students_idno,
					data: { "student_history_id": student_history_id,
							"block_sections_id": block_sections_id,
							"prospectus_id": prospectus_id,
							"action": 'transfer_to_strand'},
					dataType: 'json',
					success: function(response) {				
						$('#show_extracted_items').html(response.output); 	

						$('h4.media-heading').html(response.student_info);

						if (response.success) {
							$('#msg_header_transfer_strand').html("Student successfully transferred to strand!");
						}
					}
				});
			}
		}
		
	});

</script>
	
	
	