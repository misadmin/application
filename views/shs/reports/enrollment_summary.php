
	<h2 class="heading">Enrollment Summary - Senior High School</h2>

	<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 35px; margin-bottom:10px; padding:2px;">
			<div style="float:right; overflow:auto;">
				<div id="show_change_data_icon" style="float:left; padding-right:8px; margin-top:7px;">
				</div>
				<div style="float:left; padding-top:8px; padding-right:10px;" class="b2">
					<a href="#" style="text-decoration: none; color:#4E4D4D; font-size:12px; "
						class="download_enrollment_summary_pdf" >
						<div style="float:right; padding-left:3px; padding-top:2px;">Download to PDF</div>
						<div id="excel_labels" style="float:right;">
							<img src="<?php print(base_url('assets/images/pdf_icon.png')); ?>" style="height:20px;" /> 
						</div>
					</a>			
				</div>
				<div style="float:left; padding-top:3px;" class="b2">
					<select name="academic_terms_id" class="select_change" id="academic_terms_id">
						<?php 
							foreach($terms AS $term) {
						?>
								<option value="<?php print($term->id); ?>" 
									sy="<?php print($term->term.' '.$term->sy); ?>" <?php if($selected_term == $term->id) print("selected"); ?> >
									<?php print($term->term.' '.$term->sy); ?> 
								</option>
						<?php 	
							}
						?>
					</select>
				</div>
			</div>
	</div>

	<div id="show_list_of_students">
		<?php 
			$this->load->view('shs/reports/enrollment_summary_details');
		?>
	</div>

			
		
<script>
	//$(document).ready(function(){
		$('.select_change').bind('change', function(){
			
			var element1          = $("option:selected", "#academic_terms_id");
			var academic_terms_id = element1.val();
			var term_sy           = element1.attr('sy');
			
			$('#show_change_data_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />");

			$.ajax({
				cache: false,
				type: 'POST',
				url: "<?php echo site_url($this->uri->segment(1).'/shs_enrollment_summary'); ?>",
				data: {"academic_terms_id": academic_terms_id,
						"term_sy": term_sy,
						"action": "change_selection" },
				dataType: 'json',
				success: function(response) {													
					$('#show_list_of_students').html(response.output); 
					
					$('#show_change_data_icon').html('');
				}
				
			});

		});
	//});
</script> 

