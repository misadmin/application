<?php
 
class Dric extends MY_Controller {

	function __construct(){
		parent::__construct();

		if ($this->session->userdata('role')=='dric'){
			$this->navbar_data['menu'] = array(
					'Student'	=>array(
							'Students'=>'student',
							'New Student'=>'new_student'),
					'Faculty'	=>'faculty',
					'Reports'	=>array(
							'Offerings'	=>array('Offerings' => 'offerings_current_and_previous',
												'Incoming Offerings' => 'offerings_incoming',
												),
							'Alpha List' => 'alphalist',
							'Enrollment Summary' =>  'enrollment_summary',
							'Mass Printing'		=> 'printer',
				  			'Withdrawals'=>'list_withdrawals',
							'Teachers Load'=>'faculty_load',
							'CWTS/MS'=>'cwts_graduates',
							'Courses Masterlist'=>'courses_masterlist',
							'Student Masterlist'=>'masterlist_withaddresses',
							'Room Utilization'=>'room_utilization',
						),
			);
		}
		$this->content_lib->set_navbar_content('', $this->navbar_data);
		$this->load->model('hnumis/Grades_Model');
		$this->load->model('hnumis/Offerings_Model');
		$this->load->model('hnumis/AcademicYears_model');
		$this->load->model('hnumis/Student_model');
		$this->load->model('Academic_terms_model');
		$this->load->model('hnumis/Withdrawal_Model');
		$this->load->model('hnumis/Courses_Model');
		$this->load->model('Ledger_model');
		$this->load->model('teller/teller_model');
		$this->load->model('financials/Scholarship_Model');
		$this->load->model('teller/assessment_model');
		$this->load->model('financials/Scholarship_Model');
		$this->load->model('accounts/Accounts_model');
		$this->load->model('hnumis/Enrollments_Model');
		
		$this->load->helper('student_helper');

	}

	public function student(){
		$this->content_lib->set_title('DRIC | Student | ' . $this->config->item('application_title'));
		$this->content_lib->enqueue_sidebar_widget('common/search', array('role'=>$this->role, 'what'=>'student'), 'Search Students', 'in');
		$query = $this->input->post('q');
		$idnum = $this->uri->segment(3);
		$this->load->library('print_lib');
		$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
		//$this->content_lib->enqueue_body_content('print_templates/nothing', array());
		if ( ! empty($query)){

			//A search query occurs... so lets query the student_common_model
			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$this->load->model('student_common_model');
			$results = $this->student_common_model->search($query, $this->config->item('results_to_show_per_page'), $page);
			$total = $this->student_common_model->total_search_results($query);

			$this->load->library('pagination_lib');

			if ($results !== FALSE) {
				$pagination = $this->pagination_lib->pagination('', $total, $page);

				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);

				//A result or several results were found
				if (count($results) > 1){
					//when several results are found... lets show the search result page...
					$res=array();
					foreach ($results as $result){
						$port = substr($result->idno, 0, 3);
						if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg"))
							$image = base_url($this->config->item('student_images_folder') . "{$port}/{$result->idno}.jpg");
						else {
							if ($result->gender=='F')
								$image = base_url($this->config->item('no_image_placeholder_female')); else
								$image = base_url($this->config->item('no_image_placeholder_male'));
						}
						$res[] = array('image'=>$image, 'idnum'=>$result->idno, 'fullname'=>$result->fullname, 'mname'=>$result->mname);
					}
					$data = array(
							'start'		=>$start,
							'end'		=>$end,
							'total'		=>$total,
							'pagination'=>$pagination,
							'results'	=>$res,
							'query'		=>$query
					);
					$this->content_lib->enqueue_body_content ('common/search_result', $data);
				} else {
					//Only one result is seen... lets show his profile instead.
					//redirect to the user's profile...
					redirect(site_url("{$this->role}/student/{$results[0]->idno}"));
				}
			} else {
				//A result is NOT found...
				$this->content_lib->enqueue_body_content('common/error', array('message'=>'Result not found'));
			}
		}

		if (is_numeric($idnum)){
			
			$this->load->model('student_common_model');
			
			//Let us process first the action specificed ....
			$result = $this->student_common_model->my_information($idnum);	
			
			
				switch ($this->input->post('action')){
						case 'generate_class_schedule':
							$this->load->model("hnumis/Reports_model");
							$this->load->model("hnumis/AcademicYears_Model");

							$current = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));

							$student['idno']= $result->idno;
							$student['fname']= $result->fname;
							$student['lname']= $result->lname;
							$student['mname']= $result->mname;
							$student['yr_level']= $result->year_level;
							$student['abbreviation']= $result->abbreviation;
							$student['max_bracket_units']= $this->input->post('max_units');
							$student['student_histories_id']= $this->input->post('student_histories_id');
							
							$selected_term = $this->AcademicYears_Model->getAcademicTerms($this->input->post('academic_terms_id'));
							//print_r($student);die();
								
							$this->Reports_model->generate_student_class_schedule_pdf($this->input->post('academic_terms_id'),$student, $selected_term);
							return;
							
						case 'generate_grade':
							$this->load->model("hnumis/Reports_model");
							$this->load->model('student_common_model');
							
							$result = $this->student_common_model->my_information($idnum);

							$student['idno'] = $result->idno;
							$student['lname'] = $result->lname;
							$student['fname'] = $result->fname;
							$student['mname'] = $result->mname;
							$student['yr_level'] = $result->year_level;
							$student['abbreviation'] = $result->abbreviation;
								
							$grades = json_decode($this->input->post('grade_term'));
							
							//print_r($grades); die();	
							$student['school_year'] = $grades->school_year;
							$student['term'] = $grades->term;
							//print_r($grades->courses); die();						
							//foreach ($grades AS $grade) {
							//	$grade_term = $grade['courses'];
							//}
							//print_r($grade_term);die();
							
							$this->Reports_model->generate_student_grades_pdf($grades->courses,$student);	
							return;
						case 'generate_diploma':
							if ($this->input->post('leb_no')) {
								$leb_no = $this->input->post('leb_no');
								$serial_no = $this->input->post('serial_no');
							} else {
								$leb_no = $this->input->post('leb_no2');
								$serial_no = $this->input->post('serial_no2');
							}
							
							$this->print_diploma($idnum, $this->input->post('graduated_students_id'),$leb_no,$serial_no);
							return;
				}
					
			$tab = $this->process_student_activities($idnum);
			//print_r($tab);die();	
			//after the search... when results are found....
			$this->load->model('student_common_model');
			//$result = $this->student_common_model->my_information($idnum);
			$this->content_lib->enqueue_footer_script('date_picker');
			$this->content_lib->enqueue_header_style('date_picker');

			if ($tab !== FALSE) {
				//a user with that id number is seen...

				$port = substr($idnum, 0, 3);
				$dcontent = array(
						'image'		=> base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"),
						'idnum'		=> $idnum,
						'name'		=> strtoupper($tab['familyname']) . ', '. ucfirst($tab['firstname']) . " " . ucfirst($tab['middlename']),

						/*'phone_number' 	=> $result->phone_number,
						'home_street'	=> $result->home_street,
						'home_town'		=> $result->home_town,
						'home_province' => $result->home_province,
						*/
				);

				$dcontent = array_merge ($tab, $dcontent);
				if (is_file(FCPATH . $this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg"))
					$dcontent['image'] = base_url($this->config->item('student_images_folder') . "{$port}/{$idnum}.jpg");
				else {
					if ($tab['gender'] == 'F')
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_female')); else
						$dcontent['image'] = base_url($this->config->item('no_image_placeholder_male'));
				}

				$this->content_lib->enqueue_body_content('common/student_profile', $dcontent);
				//enqueue here what other activities are for this actor... this should be tabbed...

				//This looks dirty... but this will work... make schedule be set using a pull down menu
				//added by: justing 3/15/2012
				$this->load->model('Academic_terms_model');
				$current_term = $this->Academic_terms_model->current_academic_term();

				//$tab['term'] = $current_term->term . " SY. " . $current_term->sy;
				$tab['term'] = "1st Semester 2018-2019";
				
				//$tab['academic_terms'] = $academic_terms;
				$academic_terms = $this->Academic_terms_model->student_inclusive_academic_terms($idnum); //terms this student is enrolled...
				$this->load->model('hnumis/Courses_Model');
				$this->load->model('hnumis/AcademicYears_model');
				$this->load->model('financials/Tuition_Model');

				$action = $this->input->post('action');

				if (is_college_student ($idnum)){ 
					//on May 28, 2015: temp solution Tata added this IF(is_college...) condition 
					//so $courses variable will not get error if student is a grades schooler
				
					if ($action=='schedule_current_term') {
						$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $this->input->post('academic_term'));
						
						$current_term = $this->Academic_terms_model->current_academic_term($this->input->post('academic_term'));
						$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $this->input->post('academic_term'));
						$tab['term'] = $current_term->term . " SY " . $current_term->sy;
						$tab['tab'] = 'schedule';
						$selected_term = $this->input->post('academic_term');
					} else {
							$courses = $this->Courses_Model->student_courses_from_academic_terms ($idnum, $academic_terms[0]->id);
							$selected_term = '';
							$assessment_date = $this->Tuition_Model->getAssessmentDate($idnum, $current_term->id);
					}
									
					$tab['idnumber'] = $idnum;
					$tab['courses'] = $courses;
				
				}
				
				
				$this->load->library('print_lib');
				$this->load->library('tab_lib');
				$this->tab_lib->set_tab_container_name('student_tab');
				if ($this->session->userdata('role')=='dric'){				
					$this->tab_lib->enqueue_tab(
								'Basic Info',
								'dric/student_information_form',
								$tab,
								'sif',
								($tab['tab'] =='sif' || empty($tab['tab']) ? TRUE : FALSE)
					);
					$this->tab_lib->enqueue_tab('Other Info',
										'',
										array(),
										'information',
										($tab['tab'] =='sia' || $tab['tab'] =='family_info' || $tab['tab'] =='educ_info' || $tab['tab'] =='suporting_persons' || $tab['tab'] =='emergency_info' || empty($tab['tab']) ? TRUE : FALSE)
										);
	
					$this->tab_lib->enqueue_tab(
										'Address Info',
										'dric/student_info_addresses',
										$tab,
										'sia',
										($tab['tab'] =='sia' ? TRUE : FALSE),
										'information'
					);
				
					$this->tab_lib->enqueue_tab(
										'Family Info',
										'dric/family_information',
										$tab,
										'family_info',
										($tab['tab'] =='family_info' ? true : false),
										'information'
					);
					$this->tab_lib->enqueue_tab(
										'Educational Info',
										'dric/educational_information',
										$tab,
										'educ_info',
										($tab['tab'] =='educ_info' ? TRUE : FALSE),
										'information'
					);
					$this->tab_lib->enqueue_tab(
							'Support of Education',
							'dric/supporting_persons_information',
							$tab,
							'supporting_persons',
							($tab['tab'] =='supporting_persons' ? TRUE : FALSE),
										'information'
					);

					//log_message("info", print_r($tab,true)); 

					$this->tab_lib->enqueue_tab(
							'Emergency Contact Info',
							'dric/emergency_contact',
							$tab,
							'emergency_info',
							($tab['tab'] =='emergency_info' ? TRUE : FALSE),
										'information'
					);
				}

				if (is_college_student ($idnum)){
					if ($this->input->post('academic_term')) {
						$selected_term = $this->input->post('academic_term');
			   			$terms=$this->AcademicYears_model->getAcademicTerms($this->input->post('academic_term'));
			   			$acad_term=$terms->term;
			   			 
					} else {
						$selected_term = $academic_terms[0]->id;
						$acad_term = $current_term->term;
					}

					$selected_history = $this->Student_model->get_StudentHistory_id($idnum, $selected_term);

					if ($selected_history) {
						$student_units = $this->Student_model->getUnits($selected_history, $acad_term);
						$student_history_id=$selected_history->id;
					} else {
						$student_units['max_bracket_units']=0;
						$student_history_id=0;
					}

					$acad_term = $this->AcademicYears_model->getAcademicTerms($selected_term);
					
					if ($this->session->userdata('role')=='dric'){							
						$this->tab_lib->enqueue_tab ('Schedule', 'student/schedule', 
								array('schedules'=>$courses,
									'academic_terms'=>$academic_terms,
									'assessment_date'=>$assessment_date,
									'academic_terms_id'=>$selected_term,
									'max_units'=>$student_units['max_bracket_units'],
									'student_histories_id'=>$student_history_id,
									'lname' => $tab['familyname'],
									'fname' => $tab['firstname'],									
									'mname' => $tab['middlename'],
									'course'=>$tab['course'],
									'yr_level'=>$tab['level'],
									), 'schedule', $tab['tab']=='schedule'
						);
					}
					$this->load->model('hnumis/enrollments_model');
					$this->load->model('col_students_model');
					$tab['histories'] = $this->col_students_model->student_inclusive_terms($idnum);
					
					if(!isset($tab['grades'])){
						//grades not yet set... lets fetch this from most recent history...
						$tab['grades'] = $this->col_students_model->student_grades_given_history_id($idnum, $tab['histories'][0]->id);
					}
					//print_r($tab);die();
					$print_student_grade = $this->load->view('print_templates/student_grades', $tab, TRUE); 
					$tab['print_student_grade'] = $print_student_grade;
					if ($this->session->userdata('role')=='dric'){
						$this->tab_lib->enqueue_tab ('Grades', 'dric/grades', $tab, 'grades', ($tab['tab'] =='grades' ? TRUE : FALSE));

						if($result->year_level > 10){
							//Student does not have a college history yet... probably a senior high student...
							//give option to DRIC to give this student a college history...
							// added Toyet 5.3.2018
							$this->load->model('hnumis/College_model');
							$data['colleges'] = $this->College_model->all_colleges();
							$this->tab_lib->enqueue_tab ('College Enrollment', 'dric/enroll_basic_ed_to_college', $data, 'college_enrollment', $this->session->userdata('role')=='dric' ? FALSE : TRUE);
						}
						
						$this->tab_lib->enqueue_tab(
								'Enrollment Extension',
								'dric/enrollment_extension',
								$tab,
								'enrollment_extension',
								($tab['tab'] =='enrollment_extension' ? TRUE : FALSE)
						);
	
						$this->tab_lib->enqueue_tab(
								'Password',
								'student/password',
								$tab,
								'password',
								($tab['tab'] =='password' ? TRUE : FALSE)
						);
						
						$this->tab_lib->enqueue_tab('Print',
								'',
								array(),
								'printing',
								($tab['tab'] =='print_schedule' || $tab['tab'] =='print_diploma' || $tab['tab'] =='print_id' || empty($tab['tab']) ? TRUE : FALSE)
						);
						
						
						
						
						$this->tab_lib->enqueue_tab(
								'Class Schedule',
								'print_templates/class_schedule',
								$tab,
								'print_shortcut',
								($tab['tab'] =='print_schedule' ? TRUE : FALSE),
								'printing'
						);
						

						$graduated_programs = $this->Student_model->List_Graduated_Programs($idnum);
						$with_dwct = FALSE;
						$grad_hnu = TRUE;
						
						//if student has no records for graduated program in HNU, try other schools
						if (!$graduated_programs) {
							$grad_hnu = FALSE;
							$graduated_programs = $this->Student_model->List_Graduated_Programs($idnum, TRUE);
							//print_r($graduated_programs); die();
							if ($graduated_programs) {
								//check if graduated from DWCT for old students
								$dwct_school_id = array(1951,7760,7839);
								$dwct = array(new stdClass());
								foreach ($graduated_programs AS $prog) {
									if (in_array($prog->schools_id, $dwct_school_id)) {
										//print($prog->schools_id.'<br>');
										$dwct[0] = $prog;
										$with_dwct = TRUE;
										$grad_hnu = TRUE;
									}
								}
								$graduated_programs = $dwct;
							}
						}
						
						$this->tab_lib->enqueue_tab ('Diploma', 'dric/print_diploma',
								array('graduated_programs'=>$graduated_programs,
										'with_dwct'=>$with_dwct,
										'grad_hnu'=>$grad_hnu),
								'print_diploma', ($tab['tab'] =='print_diploma' ? TRUE : FALSE),
								'printing'
						);
						
						
						$this->tab_lib->enqueue_tab(
								'I.D.',
								'dric/print_id',
								$tab,
								'print_id',	
								($tab['tab'] =='print_id' ? TRUE : FALSE),
								'printing'
						);
						//print_r($current_term); die();
						//$coursestaken = $this->Grades_Model->ListStudentGrades($current_term->id, $idnum);
						$coursestaken = $this->Grades_Model->ListStudentGrades($current_term->id, $idnum);
						//print_r($coursestaken); die();
						$history = $this->Student_model->getEnrollStatus($idnum, $current_term->id);
						if($history){
	
							//deletes ALL enrolled courses from enrollments table -- TOTAL withdrawal or by course within the enrollment period
							$current_t = $this->Academic_terms_model->current_academic_term();
							//print_r($current_t); die();
							//print_r($coursestaken); die();
							$history_id = $history->id;
							
							$assessment = $this->assessment_model->CheckStudentHasAssessment($history_id);
							
							$MyPrivilege = $this->Student_model->Check_if_with_Privilege($history_id);
							
							$cur_academic_term = $this->AcademicYears_model->getCurrentAcademicTerm();
							//print_r($cur_academic_term); die();
							$RefDate = $this->AcademicYears_model->getRefDate($cur_academic_term->term);
						
							// die('not ok');
	
							//$acad_term = $this->Academic_terms_model->current_academic_term($this->Enrollments_Model->academic_term_for_enrollment_date());
							//print_r($acad_term); die();
							$academic_term = $this->AcademicYears_model->getAcademicTerms($cur_academic_term->id);
							$coursestaken =  $this->Grades_Model->ListStudentGrades($cur_academic_term->id, $idnum);
								
							$withdraw_percentage = $this->Withdrawal_Model->getWithdrawalPercentage($cur_academic_term->id);
							//print_r($assessment); die();
							//if( /* $assessment AND*/ $RefDate == NULL){
							//if( $RefDate != NULL){
							/*EDITED BY: genes 12/9/15
							*
							*/
							// print_r ($RefDate);
							// die();
							if ($RefDate) {
								//if($MyPrivilege == NULL OR ($MyPrivilege != NULL AND $MyPrivilege->posted == 'Y')){
					 		   		$this->tab_lib->enqueue_tab('WITHDRAW(Beyond adjustment period)','dric/withdraw_within_enrollment_period',
														array('schedules'=>$coursestaken, 
																'academic_terms'=>$academic_terms, 
																'history_id' =>$history_id,
																'what_period'=>'beyond',
																'withdraw_per'=>$withdraw_percentage),
													'withdraw_within_enrollment_period', FALSE);
									//	}
							}else{
									$this->tab_lib->enqueue_tab('WITHDRAW(Within adjustment period)', 'dric/withdraw_within_enrollment_period2',
														array('schedules'=>$coursestaken, 
																'academic_terms'=>$academic_terms, 
																'history_id' =>$history_id,
																'what_period'=>'within', 
																'withdraw_per'=>$withdraw_percentage), 
													'withdraw_within_enrollment_period2', FALSE);
							}
							
						}
						
					}
					
				} else {
					//Student does not have a college history yet... probably a basic ed student...
					//give option to DRIC to give this student a college history...
					$this->load->model('hnumis/College_model');
					$data['colleges'] = $this->College_model->all_colleges();
					$this->tab_lib->enqueue_tab ('College Enrollment', 'dric/enroll_basic_ed_to_college', $data, 'college_enrollment', $this->session->userdata('role')=='dric' ? FALSE : TRUE);
				}
			$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
	}else {
				//Has a Numeric ID Number but is not found in database...
				//todo: Create a view to be placed here...
		$this->content_lib->enqueue_body_content('common/error', array('message'=>"No student found with that ID Number..."));
	}
 }

		//what will happen if there is no query... and the result is not numeric?
		// We will present the search_result_by_program form
		if ( empty($query) && ! is_numeric($idnum)) {

			$page = $this->input->post('page') ? $this->input->post('page') : 1;
			$data = array();
			$this->load->model('hnumis/College_model');
			$data['colleges'] = $this->College_model->all_colleges();
			$this->load->model('student_common_model');

			if ($this->input->post('what')=='search_by_program'){
				$total = count ($this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), TRUE));
				$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
				$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
						? $total
						: ($start + (int)$this->config->item('results_to_show_per_page') - 1)
				);
				$results = $this->student_common_model->search_by_program ($this->input->post('program'), $this->input->post('year_level'), FALSE, $page, $this->config->item('results_to_show_per_page'));
				$this->load->library('pagination_lib');
				$data['pagination'] = $this->pagination_lib->pagination('', $total, $page);
				$data['results'] = $results;
				$this->load->model('hnumis/Programs_model');
				$this->load->model('hnumis/College_model');
				$data['query'] = $this->Programs_model->getProgram($this->input->post('program'))->abbreviation . " " . $this->input->post('year_level');
				$data['programs'] = $this->College_model->programs_from_college($this->input->post('college'));
				$data['college'] = $this->input->post('college');
				$data['program'] = $this->input->post('program');
				$data['year_level'] = $this->input->post('year_level');
				$data['end'] = $end;
				$data['total'] = $total;
				$data['start'] = $start;
			}
			$this->content_lib->enqueue_body_content('common/search_result_by_program', $data);
		}
		$this->content_lib->content();
	}

	public function new_student(){
		$this->load->model('hnumis/Otherschools_Model');
		$this->content_lib->set_title('DRIC | New Student | ' . $this->config->item('application_title'));
		$data = array(); //a placeholder of the data to pass to the form...
		$this->content_lib->enqueue_header_style('gritter');
		$this->content_lib->enqueue_footer_script('gritter');

		//lets check whether an input is found...
		if ($this->input->post('nonce') && $this->common->nonce_is_valid($this->input->post('nonce'))) {
			//an input is found...
			//nonce is valid...
			//todo: call model to add student...
			$lastname = sanitize_text_field($this->input->post('familyname', TRUE));
			$firstname = sanitize_text_field($this->input->post('firstname', TRUE));
			$middlename = sanitize_text_field($this->input->post('middlename', TRUE));
			$gender = sanitize_text_field($this->input->post('gender', TRUE));
			$program_id = $this->input->post('program');
			$country_id = $this->input->post('country');
			$province_id = $this->input->post('province');
			$town_id = $this->input->post('town');
			$barangay_id = $this->input->post('barangay');
			$address = $this->input->post('address');
			$primaryphone = $this->input->post('primaryphone');
			$persontocontact = $this->input->post('persontocontact');
			$relation = $this->input->post('relation');
			$emergency_address = $this->input->post('emergency_address');
			$emergency_phone = $this->input->post('emergency_phone');

			$this->load->model('student_common_model');
			$this->load->model('hnumis/Student_model');
			$this->load->model('hnumis/prospectus_model');

			//var_dump($country_id);
			//var_dump($province_id);
			//var_dump($town_id);
			//var_dump($barangay_id);
			//var_dump($address);
			//var_dump($primaryphone);
			//var_dump($persontocontact);
			//var_dump($relation);
			//var_dump($emergency_address);
			//var_dump($emergency_phone);
			//die();

			//Duplicate Entry Restriction Added by Tatskie @
			if ($student_id=$this->student_common_model->get_idno($lastname,$firstname,$middlename)){
				$data['message'] = 'Duplicate Entry Error: ID Number: <a href="' . site_url('dric/student/') . "/{$student_id}\">{$student_id}</a>";
				$data['severity'] = "error";
				$data['student_id'] = $student_id;
			}else{

				$new_meta = array();
				$new_meta = array('emergency_address'=>$emergency_address,
								  'emergency_notify'=>$persontocontact,
								  'emergency_telephone'=>$emergency_phone,
								  'emergency_email_address'=>' ');

				$data  = array('lname'=>$lastname, 
							   'fname'=>$firstname, 
							   'mname'=>$middlename, 
							   'gender'=>$gender,
							   'meta'=>json_encode($new_meta),
			        		  );

				$data1 = array('country_id'=>$country_id,
							   'province_id'=>$province_id,
							   'town_id'=>$town_id,
							   'barangay_id'=>$barangay_id,
							   'address'=>$address,
			        		  );

				$data2 = array('emergency_notify'=>$persontocontact,
					           'relation'=>$relation,
					           'emergency_phone'=>$emergency_phone,
					           'street_name'=>$address,
					           'barangay_id'=>$barangay_id,
				              );

				$data3 = array('phone_type'=>'mobile',
					           'owner_type'=>'student',
					           'is_primary'=>'Y',
					           'phone_number'=>$primaryphone,
				              );

				$student_id = $this->student_common_model->insert_student($data,$data1,$data2,$data3);				
				//this should NOT be here but to be in the teller's activities...
				$prospectus_id = $this->prospectus_model->prospectus_from_program($program_id) ? $this->prospectus_model->prospectus_from_program($program_id) : NULL;
				//var_dump($prospectus_id);die();
				if ($student_id){
					$history = $this->Student_model->insert_student_history($student_id, $prospectus_id, 0, 1, FALSE); //last should be FALSE and will only be TRUE once paid...

					$data['message'] = '<h2>Successfully added the student with ID: <a href="' . site_url('dric/student/') . "/0{$student_id}\">{$student_id}</a></h2>";
					$data['severity'] = "success";
					$data['student_id'] = $student_id;
						
				}else{
					$data['message'] = '<h2>Failed to save new student!</h2>';
					$data['severity'] = "warning";
					$data['student_id'] = '';						
				}
			}
		}

		$this->load->model('hnumis/college_model');
		$data['colleges'] = $this->college_model->all_colleges();
		$data['countries'] = $this->Otherschools_Model->listCountries();
		$data['provinces'] = $this->Otherschools_Model->listProvinces();
		$data['towns'] = $this->Otherschools_Model->listTownsByProvince(49);
		$this->content_lib->enqueue_body_content('dric/new_student_form', $data);
		$this->content_lib->content();
	}

	private function process_student_activities($idnum){

		//added: 01/07/2013:
		$this->load->model('common_model');
		$this->load->model('Places_model');
		$religions = $this->common_model->religions();
		$citizenships = $this->common_model->citizenships();
		$countries = $this->Places_model->countries();
		$philippine_id = $this->Places_model->country_id('Philippines');
		$provinces = $this->Places_model->fetch_results($philippine_id, 'provinces');

		$this->load->model('student_common_model');
		
		switch ($this->input->post('action')){
			case 'add_college_history'		:
								if ($this->common->nonce_is_valid($this->input->post('nonce'))){
									$this->load->model('hnumis/Student_model');
									$this->load->model('hnumis/Prospectus_model');
									$program_id = $this->input->post('program');
									$prospectus_id = ($this->Prospectus_model->prospectus_from_program($program_id) ? $this->Prospectus_model->prospectus_from_program($program_id) : NULL);

									$history = $this->Student_model->insert_student_history($idnum, $prospectus_id, 0, 1, FALSE, FALSE); //last should be FALSE and will only be TRUE once paid...
									if ($history){
										$return1 = array('tab'=>'sif', 'message'=>"Successfully moved student to college.", 'severity'=>'alert-success');
									} else {
										$return1 = array('tab'=>'sif', 'message'=>"Error found while moving student to college.", 'severity'=>'alert-error');
									}
								} else {
									$return1 = array('tab'=>'sif', 'message'=>$this->config->item('nonce_error_message'), 'severity'=>'alert-error');
								}
								break;
			case 'extend_enrollment_period' :
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									$this->load->model('hnumis/Enrollments_model');
									if ($ret = $this->Enrollments_model->extend_student_enrollment_schedule ($idnum))
										$return1 = array('tab'=>'enrollment_extension', 'message'=>'This student\'s Enrollment schedule is
														extended for ONE day. ', 'severity'=>'success');
									else
										//print_r($ret); die();
										$return1 = array('tab'=>'enrollment_extension', 'message'=>'Error extending student\'s Enrollment schedule.
													Possible reason: student is not enrolled. ', 'severity'=>'error');
								} else {
									$return1 = array('tab'=>'enrollment_extension', 'message'=>$this->config->item('nonce_error_message'), 'severity
												'=>'error');
								}
						  		break;
			case 'change_password'	:
								//lets check if this is a valid input from the user...
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									//nonce is valid
									$this->load->model('student_common_model');
									$idno = $this->input->post('student_id');
									$newpassword = $this->input->post('password');

									if ( $this->student_common_model->update_information($idno, array('password'=>$newpassword))) {
										$return1 = array('tab'=>'password', 'message'=>'Password Changed', 'severity'=>'alert-success', 'password'=>
										$newpassword);
									} else {
										$return1 = array('tab'=>'password', 'message'=>'Password NOT Changed', 'severity'=>'alert-error');
									}
								} else {
									//nonce is invalid...
									$return1 = array('tab'=>'password', 'message'=>$this->config->item('nonce_error_message'), 'severity'=>
												'alert-error');
								}
								break;
			case 'update_student_information' :

								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									//todo: server side validation...
									$data['fname'] = strip_tags($this->input->post('firstname', TRUE));
									$data['lname'] = strip_tags($this->input->post('familyname', TRUE));
									$data['mname'] = strip_tags($this->input->post('middlename', TRUE));
									$data['dbirth'] = $this->input->post('birthdate');
									$data['gender'] = $this->input->post('gender');
									$data['civil_status'] = $this->input->post('civil_status');
									$data['citizenships_id'] = $this->input->post('citizenship');
									$data['religions_id'] = $this->input->post('religion');
									$data['phone_number'] = $this->input->post('phone_number');
										
									if ($this->student_common_model->update_information ($idnum, $data))
										$return1 = array('tab'=>'sif', 'message'=>'Student information updated.', 'severity'=>'alert-success'); else
										$return1 = array('tab'=>'sif', 'message'=>'Error Found. Student information not updated.', 'severity'=>
										'alert-error');
								} else {
									$return1 = array('tab'=>'sif', 'message'=>'Nonce is invalid. Student information not updated', 'severity'=>
									'alert-error');
								}
								break;
			case 'update_student_address_information' :
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									$student_address = $this->student_common_model->student_address($idnum);
									//print_r($student_address);die();
									$data = array('students_idno' =>$idnum,);

									// Note: for addresses we're going to update only when the country is set...
									// Home Address...
									if ($this->input->post('home_address_country')){
										$data['home_address']		= sanitize_text_field($this->input->post('home_address'));
										$data['home_barangays_id']	= $this->input->post('home_address_barangay_id');
										$data['home_towns_id']		= $this->input->post('home_address_town');
										$data['home_provinces_id']	= $this->input->post('home_address_province');
										$data['home_countries_id']	= $this->input->post('home_address_country');
									}

									// Birth Place...
									if ($this->input->post('place_of_birth_country')){
										$data['birth_address']		= sanitize_text_field($this->input->post('place_of_birth_address'));
										$data['birth_barangays_id']	= 0;
										$data['birth_towns_id']		= $this->input->post('place_of_birth_town_id');
										$data['birth_provinces_id']	= $this->input->post('place_of_birth_province');
										$data['birth_countries_id']	= $this->input->post('place_of_birth_country');
									}
									// City Address...
									if ($this->input->post('city_address_barangay_id')){
										switch ($this->input->post('city_address_barangay_id')) {
											case 'none'	:
											case '0'	: 	$city_address = '';
															$city_address_country = '';
															$city_address_province = '';
															$city_address_town = '';
															$city_address_barangay = '';
															break;
											case 'same_as_home' :
															$city_address = ($this->input->post('home_address') ? $this->input->post('home_address') : isset($student_address->home_address)?$student_address->home_address:'' );
															$city_address_country = ($this->input->post('home_address_country') ? $this->input->post('home_address_country') : $student_address->home_countries_id);
															$city_address_province = ($this->input->post('home_address_province') ? $this->input->post('home_address_province') : $student_address->home_provinces_id);
															$city_address_town = ($this->input->post('home_address_town') ? $this->input->post('home_address_town') : $student_address->home_towns_id);
															$city_address_barangay = ($this->input->post('home_address_barangay_id') ? $this->input->post('home_address_barangay_id') : ( isset($student_address->home_barangays_id) ? $student_address->home_barangays_id : ""));
															break;
											default:
															$city_address = sanitize_text_field($this->input->post('city_address'));
															$city_address_country = $this->input->post('city_address_country');
															$city_address_province = $this->input->post('city_address_province');
															$city_address_town = $this->input->post('city_address_town');
															$city_address_barangay = $this->input->post('city_address_barangay_id');
															break;
										}
										$data['city_address']		= $city_address;
										$data['city_barangays_id']	= $city_address_barangay;
										$data['city_towns_id']		= $city_address_town;
										$data['city_provinces_id']	= $city_address_province;
										$data['city_countries_id']	= $city_address_country;
									}

									//update stay in city field...
									$stay_with_update = $this->student_common_model->update_information ($idnum, array('stay_with'=>$this->input->post('stay_in_city')));

									if ($this->student_common_model->update_address($data) OR $stay_with_update)
										$return1 = array('tab'=>'sia', 'message'=>'Student Address information updated.', 'severity'=>'alert-success'); else
										$return1 = array('tab'=>'sia', 'message'=>'Student Address information NOT updated.', 'severity'=>'alert-error');
								} else {
									$return1 = array('tab'=>'sia', 'message'=>'Nonce is invalid. Address not updated', 'severity'=>'alert-error');
								}
								break;
			case 'update_student_educational_information' :
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									//print_r($this->input->post());die();
									//print_r($data);die();
									$my_information = $this->student_common_model->my_information($idnum);
									$meta = json_decode($my_information->meta, TRUE);
									
									
									//$data = array('students_idno' =>$idnum,);
									$meta['educational_background']=array(
											'Primary'		=>$this->input->post('Primary'),
											'Primary_sy'	=>$this->input->post('Primary_sy'),
											'Intermediate'  =>$this->input->post('Intermediate'),
											'Intermediate_sy'  =>$this->input->post('Intermediate_sy'),
											'Secondary'  =>$this->input->post('Secondary'),
											'Secondary_sy'  =>$this->input->post('Secondary_sy'),
									);
/* 									[primary_school_year] => prim sy
									[intermediate_school] => intermed
									[intermediate_school_year] => intermed sy
									[secondary_school] => seconda
									[secondary_school_year] => second sy
									[secondary_school_address_country] => 137
									[secondary_school_address_province] => 59
									[secondary_school_address_town] => 1199
									[secondary_school_address_barangay_id] => 32087
									[secondary_school_address] => 123
									[last_school] => if transferee
									[last_school_year] => transterrr sy
									)									
 */									//print_r($meta);die();	
									$data['meta'] = json_encode($meta);	
									//print_r($data);die();
									if ($this->student_common_model->update_information ($idnum, $data))
										$return1 = array('tab'=>'educ_info', 'message'=>'Educational Information Updated', 'severity'=>'alert-success');
									else
										$return1 = array('tab'=>'educ_info', 'message'=>'Error Updating Educational Information.', 'severity'=>'alert-error');
								} else {
									$return1 = array('tab'=>'grades', 'message'=>'Nonce Error. Updating Educational Information not done.', 'severity'=>'alert-error');
								}										
								break;
			case 'update_supporting_information'			:
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									$supporting_information_name = $this->input->post('supporting_information_name');
									$supporting_information_occupation = $this->input->post('supporting_information_occupation');
									$supporting_information_relation = $this->input->post('supporting_information_relation');
									$supporting_information_address = $this->input->post('supporting_information_address');

									$my_information = $this->student_common_model->my_information($idnum);
									$meta = json_decode($my_information->meta, TRUE);

									$support_information = (isset($meta['support_information']) ? $meta['support_information'] : "");

									foreach ($supporting_information_name as $key => $val) {
										$support_information[$key] = array(
												'name' 			=> $supporting_information_name[$key],
												'occupation'	=> $supporting_information_occupation[$key],
												'relation'		=> $supporting_information_relation[$key],
												'address'		=> $supporting_information_address[$key],
												);
									}

									$meta['support_information'] = $support_information;

									$data = array('meta'=>json_encode($meta));
									if ($this->student_common_model->update_information ($idnum, $data))
										$return1 = array('tab'=>'supporting_persons', 'message'=>'Student Support information updated.', 'severity'=>'alert-success'); 
									else
										$return1 = array('tab'=>'supporting_persons', 'message'=>'Error Updating Student Support information.', 'severity'=>'alert-error');
								} else {
									$return1 = array('tab'=>'supporting_persons', 'message'=>'Nonce Error. Updating Student Support information not done.', 'severity'=>'alert-error');
								}
								break;
			case 'delete_support_information'		:
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									$id = (int)$this->input->post('id');
									$my_information = $this->student_common_model->my_information($idnum);
									
									$meta = json_decode($my_information->meta, TRUE);
									$support_information = $meta['support_information'];

									unset($support_information[$id]);
									$meta['support_information'] = $support_information;

									$data = array('meta'=>json_encode($meta));
									if ($this->student_common_model->update_information ($idnum, $data))
										$return1 = array('tab'=>'supporting_persons', 'message'=>'Student Support information deleted.' , 'severity'=>'alert-success'); else
										$return1 = array('tab'=>'supporting_persons', 'message'=>'Error Deleting Student Support information.' , 'severity'=>'alert-error');
								} else {
									$return1 = array('tab'=>'supporting_persons', 'message'=>'Nonce Error. Deleting Student Support information not done.' , 'severity'=>'alert-error');
								}
								break;
			case 'update_student_family_information' :
									if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									$fathers_name = strip_tags($this->input->post('fathers_name'));
									$fathers_number = strip_tags($this->input->post('fathers_number'));
									$mothers_name = strip_tags($this->input->post('mothers_name'));
									$mothers_number = strip_tags($this->input->post('mothers_number'));
									$num_brothers = (int)$this->input->post('num_brothers');
									$num_sisters = (int)$this->input->post('num_sisters');

									$student_address = $this->student_common_model->student_address($idnum);

									if ($this->input->post('fathers_address_country') == 'same_as_home') {
										$fathers_address_country = $student_address->home_countries_id;
										$fathers_address_province = $student_address->home_provinces_id;
										$fathers_address_town = $student_address->home_towns_id;
										$fathers_address_barangay = $student_address->home_barangays_id;
										$fathers_address = $student_address->home_address;
									} else {
										$fathers_address_country = (is_numeric($this->input->post('fathers_address_country')) ? $this->input->post('fathers_address_country') : FALSE);
										$fathers_address_province = (is_numeric($this->input->post('fathers_address_province')) ? $this->input->post('fathers_address_province') : FALSE);
										$fathers_address_town = (is_numeric($this->input->post('fathers_address_town')) ? $this->input->post('fathers_address_town') : FALSE);
										$fathers_address_barangay = (is_numeric($this->input->post('fathers_address_barangay_id')) ? $this->input->post('fathers_address_barangay_id') : FALSE);
										$fathers_address = $this->input->post('fathers_address');
									}

									if ($this->input->post('mothers_address_country') == 'same_as_home') {
										$mothers_address_country = $student_address->home_countries_id;
										$mothers_address_province = $student_address->home_provinces_id;
										$mothers_address_town = $student_address->home_towns_id;
										$mothers_address_barangay = $student_address->home_barangays_id;
										$mothers_address = $student_address->home_address;
									} else {
										$mothers_address_country = (is_numeric($this->input->post('mothers_address_country')) ? $this->input->post('mothers_address_country') : FALSE);
										$mothers_address_province = (is_numeric($this->input->post('mothers_address_province')) ? $this->input->post('mothers_address_province') : FALSE);
										$mothers_address_town = (is_numeric($this->input->post('mothers_address_town')) ? $this->input->post('mothers_address_town') : FALSE);
										$mothers_address_barangay = (is_numeric($this->input->post('mothers_address_barangay_id')) ? $this->input->post('mothers_address_barangay_id') : FALSE);
										$mothers_address = $this->input->post('mothers_address');
									}

									$this->load->model('places_model');
									$this->load->model('student_common_model');

									$my_information = $this->student_common_model->my_information($idnum);
									$meta = json_decode($my_information->meta, TRUE);

									//$meta['fathers_address'] = (isset($meta['fathers_address']) ? $meta['fathers_address'] : "");
									//$meta['mothers_address'] = (isset($meta['mothers_address']) ? $meta['mothers_address'] : "");

									$fathers_address = trim($fathers_address . "\n" . $this->places_model->parse_address($fathers_address_country, $fathers_address_province, $fathers_address_town, $fathers_address_barangay));
									$fathers_address = (! empty($fathers_address) ? $fathers_address : $meta['family_info']['fathers_address'] );

									$mothers_address = trim($mothers_address . "\n" . $this->places_model->parse_address($mothers_address_country, $mothers_address_province, $mothers_address_town, $mothers_address_barangay));
									$mothers_address = (! empty($mothers_address) ? $mothers_address : $meta['family_info']['mothers_address'] );

									$meta['family_info']['fathers_name'] = $fathers_name;
									$meta['family_info']['fathers_number'] = $fathers_number;										
									$meta['family_info']['mothers_name'] = $mothers_name;
									$meta['family_info']['mothers_number'] = $mothers_number;										
									$meta['family_info']['fathers_address'] =  $fathers_address;
									$meta['family_info']['mothers_address'] =  $mothers_address;
									$meta['family_info']['num_brothers'] = $num_brothers;
									$meta['family_info']['num_sisters'] = $num_sisters;
									
									//print_r($meta);die();

									$data = array('meta'=>json_encode($meta));
									//print_r($data);die();
									if ($this->student_common_model->update_information ($idnum, $data))
										$return1 = array('tab'=>'family_info', 'message'=>'Student Family information updated.', 'severity'=>'alert-success'); else
										$return1 = array('tab'=>'family_info', 'message'=>'Error Updating Student Family information.', 'severity'=>'alert-error');

								} else {
									$return1 = array('tab'=>'family_info', 'message'=>'Nonce error. Student family information NOT updated.', 'severity'=>'alert-error');
								}
								break;
			case 'update_student_emergency_information' :
								if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
									$emergency_address_country = (is_numeric($this->input->post('emergency_address_country')) ? $this->input->post('emergency_address_country') : FALSE);
									$emergency_address_province = (is_numeric($this->input->post('emergency_address_province')) ? $this->input->post('emergency_address_province') : FALSE);
									$emergency_address_town = (is_numeric($this->input->post('emergency_address_town')) ? $this->input->post('emergency_address_town') : FALSE);
									$emergency_address_barangay = (is_numeric($this->input->post('emergency_address_barangay_id')) ? $this->input->post('emergency_address_barangay_id') : FALSE);
									$emergency_address = $this->input->post('emergency_address');

									$this->load->model('places_model');
									$this->load->model('student_common_model');
									$my_information = $this->student_common_model->my_information($idnum);
									$meta = json_decode($my_information->meta, TRUE);
									$emergency_address = trim($emergency_address . "\n" . $this->places_model->parse_address($emergency_address_country, $emergency_address_province, $emergency_address_town, $emergency_address_barangay));
									$emergency_address = (! empty($emergency_address) ? $emergency_address : $meta['emergency_address'] );

									$meta['emergency_address'] =  $emergency_address;
									$meta['emergency_notify'] = $this->input->post('emergency_notify', TRUE);
									$meta['emergency_telephone'] = $this->input->post('emergency_telephone', TRUE);
									$meta['emergency_email_address'] = $this->input->post('emergency_email_address', TRUE);

									$data = array('meta'=>json_encode($meta));
									if ($this->student_common_model->update_information ($idnum, $data))
										$return1 = array('tab'=>'emergency_info', 'message'=>'Student Emergency information updated.', 'severity'=>'alert-success'); else
										$return1 = array('tab'=>'emergency_info', 'message'=>'Error Updating Student Emergency information.', 'severity'=>'alert-error');
								} else {
									$return1 = array('tab'=>'emergency_info', 'message'=>'Nonce error. Student Emergency information NOT updated.', 'severity'=>'alert-error');
								}
								break;
			case 'edit_sif'			:
								$return1 = array('tab'=>'sif', 'message'=>'Student Information Updated', 'severity'=>'alert-success');
								break;

			case 'withdraw_enrolled_courses':
								if($this->common->nonce_is_valid($this->input->post('nonce'))) {

									if ($this->input->post('course')) {
										$student_info = $this->Student_model->getStudentInfo($this->input->post('history_id'));
										$academic_term = $this->AcademicYears_model->getCurrentAcademicTerm();
										$academic_year = $this->AcademicYears_model->current_academic_year();
										$withdraw_percentage = $this->Withdrawal_Model->getWithdrawalPercentage($academic_term->id);
										
										$tuition_rate_basic = $this->Student_model->getTuitionRateBasic($student_info->acad_program_groups_id,
																	$student_info->academic_terms_id, $student_info->year_level);
										
										$payers = $this->teller_model->get_student($student_info->students_idno);
										$user_id = $this->session->userdata('empno');
										$period = $this->Academic_terms_model->what_period();
										
										$data['payer_id'] = $payers->payers_id;
										$data['empno'] = $this->userinfo['empno'];
										$status1 = 1;
										$status2 = 0;
										$status3 = 0;
										if ($this->input->post('what_period') == 'within') { 
											// the following code is for withrawal within the enrollment period
											$this->db->trans_start();
											$stud_data['students_idno'] = $this->userinfo['empno'];
											foreach($this->input->post('course') AS $k => $v) {
												$stud_data['enrollments_id']  = $k;
												if (!$this->Student_model->TransferDeletedToHistory($stud_data)) {
													$this->db->trans_rollback();
													$status1 = 0;
													//break 4;
													break;   // PHP7 no longer need a 4 Toyet 4.10.2019
												} else {
													if (!$this->Student_model->WithdrawCourse($k)) {
														$this->db->trans_rollback();
														$status1 = 0;
														//break 5;
														break;  // PHP7 no longer need a 5 toyet 4.10.2019
													}
												}
											}
											
											$this->db->trans_complete();
											$status2 = $status1;
											$status3 = $status1;

										}else{
										  	// the following code is for withdrawal, outside the enrollment period

											//financial computation of total withdrawal
											  $withdraw_percentage = $this->Withdrawal_Model->getWithdrawalPercentage($academic_term->id); 
											  $tuition_rate_basic = $this->Student_model->getTuitionRateBasic($student_info->acad_program_groups_id,
																	$student_info->academic_terms_id, $student_info->year_level);
											  $assessment = $this->Accounts_model->student_assessment($this->input->post('history_id'));
											
											  // start of computation of other misc and other fees 	
											  $other_charges = 0;
											  $total_misc = 0;
											  if($assessment){
											  	foreach($assessment as $key=>$val){
											  		if($key != "Tuition Basic"){
											  			$misc_subtotal[$key] = 0;
											  			foreach($val as $desc=>$rate){
											  				$misc_subtotal[$key] += $rate;
											  			}
											  		}
											  		
											  	}
											  }
											  foreach($misc_subtotal as $misc){
											  	$total_misc += $misc;
											  }
											  
											  //end of the computation of other misc and other fees
//==========================											  
											  if($this->input->post('TotalWithdraw')){
												//for financial adjustments 
												$ledger_data = $this->teller_model->get_ledger_data($payers->payers_id);
												$running_balance = 0;
												$amount_paid = 0;
												$total_debit = 0;
												foreach($ledger_data as $ledge){
													$running_balance +=  $ledge['debit'] - $ledge['credit'];
													$amount_paid += $ledge['credit'];
													$total_debit += $ledge['debit'];
													$running_balance = round($running_balance,5);
													if($running_balance == 0){
														$amount_paid = 0;
														$total_debit = 0;
													}
												}
												/*foreach($ledger_data as $ledge){
												   print("<div style='width:300px;float:left;'>Debit: ".$ledge['debit']."</div><div>Credit: ".$ledge['credit']."</div>");
												}  
												die();
												*/
												$withdraw_status = $this->input->post('withdraw_stat');
												
												// start of computation of tuition
												$academic_program_group_id = $this->Student_model->getAcadGroup($this->input->
														post('history_id'));
												$tuition_fee_basic = $this->Student_model->getMyTuitionFee_Basic($this->input->
														post('history_id'), $academic_program_group_id->
														acad_program_groups_id, $academic_term->id);
													
												$tuition_fee_others = $this->Student_model->getMyTuitionFee_Others($this->input->
														post('history_id'), $academic_term->id);
												if($tuition_fee_others ){
													$tuition_others = $tuition_fee_others->tuition_fee;
												}else{
													$tuition_others = 0;
												}
													
												$history_id = $this->input->post('history_id');
												$tuition = $tuition_fee_basic->tuition_fee + $tuition_others;
												//end of the computation of tuition
												/*$fee = $this->Accounts_model->getFees($history_id, 1);
												$matriculation = $fee->total_fee;
												$fee = $this->Accounts_model->getFees($history_id, 2);
												$miscellaneous = $fee->total_fee;
												$fee = $this->Accounts_model->getFees($history_id, 3);
												$other_fees = $fee->total_fee;
												$fee = $this->Accounts_model->getFees($history_id, 5);
												*/
												
												//start of computation of lab fee
												$dcourses = $this->Enrollments_Model->get_enrolled_courses($history_id);
												$cur_aca_yr = $this->AcademicYears_model->current_academic_year();
												
												if(!empty($dcourses)){
													$lab_courses = array();
													foreach ($dcourses as $course) {
														if ($course->type_description == 'Lab' AND $course->enrolled_status == 'current') {
															$lab_amount = $this->Accounts_model->get_lab_fee($course->id, $cur_aca_yr->id);
															
															if($lab_amount){
																$lab_courses[] = array(
																		'name'=>$course->course_code,
																		'amount'=> $lab_amount->rate,
																);
															}else{
																$lab_courses[] = array(
																		'name'=>$course->course_code,
																		'amount'=> 0.00,
																);		
															}	
														}
														
													}
												}
												$total_lab_fees = 0;
												if($lab_courses){
													foreach ($lab_courses as $lab_course) {
														$total_lab_fees += (float)$lab_course['amount'];
													}
												}
												//end of computation of lab fee
												
												$total_assessment = $total_misc + $tuition + $total_lab_fees;
												
												if($withdraw_status == "MED"){
													//status == 1 means total withdrawal is permitted, with medical certificate
													$data['remark'] = 'Total WD w/ Med. Cert.';
													$payers = $this->teller_model->get_student($idnum);
													$data['payer_id'] = $payers->payers_id;
													$data['empno'] = $this->userinfo['empno'];
													$other_payable = $total_debit - $total_assessment;
													$data['amount'] = $running_balance - $other_payable;
												}else{
													//status == 2 means total withdrawal, no medical certificate
													//compute student payable
													//computation of tuition charge
													
													/*  NOTE: the following codes are updated below
													*         by genes 12/10/15
													$tuition_charge = $tuition * $withdraw_percentage->percentage;
													
													//computation of laboratory charge
													$lab_charge = $total_lab_fees * $withdraw_percentage->percentage;
													
													//computaion of total charges
													//$total_charges = $tuition_charge + $lab_charge + $total_misc;
													$total_charges = $tuition_charge + $lab_charge;
																								
													$other_payable = $total_debit - $total_assessment; 
													
													$total_payable = $total_charges + $other_payable - $amount_paid;
													* end of codes....
													*/
													
													// ADDED: 12/10/15 by genes
													$tuition_charge = $tuition * (1 - $withdraw_percentage->percentage);
													
													$lab_charge = $total_lab_fees * (1 - $withdraw_percentage->percentage);
													
													$total_payable = $tuition_charge + $lab_charge - $amount_paid;
													
													$data['amount'] = $running_balance;
													// end of added codes...
													
													$data['remark'] = 'Total WD.';
													
												}
												/*print($data['amount']."<br>");
												print($tuition_charge."<br>");
												print($lab_charge."<br>");
												print($total_charges."<br>");
												print($running_balance."<br>");
												print($total_payable."<br>");
												
												//print($total_payable."<br>");
												
												die();
												*/
												$data['adjustment_type'] = 'credit';
												$data['description'] = 'TOTAL WITHDRAWAL';
												$data['current_term_id'] = $academic_term->id;
												
												//withdraws each course, places WD on the affected period
												$this->db->trans_start(); 
												foreach($this->input->post('course') AS $k => $v){
													
													$result = $this->Student_model->withdrawCourseEnrolled($k, $period, $user_id,
																							  $withdraw_percentage->percentage,
																						 	  $this->input->post('withdraw_stat'));
													if (!$result){
														$this->db->trans_rollback();
														$status1 = 0;
													}
												}		
												//the following is performed if all the subjects are successfully withdrawn
												if($status1){
													if ($status2 = $this->Student_model->setStudentHistWD($this->input->post('history_id'))){
														if ($status3 = $this->Ledger_model->insertWithdrawalFees($data)){
																$this->db->trans_complete();
														}else{
															$this->db->trans_rollback();
															$status2 = 0;
														}
													}else{
														$this->db->trans_rollback();
														//die("not ok");
													}
												}
											
//print("STATUS: ".$status3); die();											
											//******** End of Total withdraw computation ********
											} else {
//=============================												//individual course
												$withdrawStat = NULL;
											  
												
												//financial computation of withdrawals, individual course
											  	$date = date('Y-m-d');
											  	
												$totalTuitionFee = 0;
												$totalLabFee = 0;
												//$period = $this->Academic_terms_model->what_period();
												//$user_id = $this->session->userdata('empno');
												$ref_no ='';
												$tuitionFeeOthers = 0;
												$tuitionFeeBasic = 0;
												$totalTuitionFee = 0;
												$total_paying_units = 0;
												$total_paying_units_others = 0;
												$tuitionFeeBasic = 0;
												$tuitionFeeOthers = 0;
												$labFee = 0;
												//print_r($this->input->post('course')); die();
												$this->db->trans_start();
												foreach($this->input->post('course') AS $k => $v) {
													//the following are performed on each course withdrawn
													$course_info = $this->Courses_Model->getCourseInfo($k);
													$tuition_rate_others = $this->Student_model->getTuitionOthersRate($k, $academic_term->id,
																	    $student_info->year_level);
													if($course_info->type_description == 'Lab'){
														$LabFeeInfo = $this->Student_model->getLabFee($k, $academic_year->id);
														$labFee = $LabFeeInfo->rate;
													}else{
														$labFee = 0;
													}
												
													if($tuition_rate_others){
														$tuitionfee= $tuition_rate_others->rate * $course_info->paying_units;
														$totalTuitionFee = $totalTuitionFee + $tuitionfee;
														$total_paying_units = $total_paying_units + $course_info->paying_units;
													}else{
														$tuitionfee = $tuition_rate_basic->rate * $course_info->paying_units;
														$totalTuitionFee = $totalTuitionFee + $tuitionfee;
														$total_paying_units = $total_paying_units + $course_info->paying_units;
													}
													
													$ref_no = $ref_no." ".$course_info->course_code;
												
													//accummulates the lab fee of all lab courses
													
													$totalLabFee = $totalLabFee + $labFee;

													$result = $this->Student_model->withdrawCourseEnrolled($k, $period, $user_id, $withdraw_percentage->percentage, $withdrawStat);
													//print($withdraw_percentage->percentage);
													//die();
													if(!$result){
														$this->db->trans_rollback();
														$status1 = 0;
													} 
												}
												
												//print($totalTuitionFee."".$totalLabFee); die();
												
												if($status1){														
													//calculates the total tuition fee + total lab fee 
													$TuitionPlusLabFees = $totalTuitionFee + $totalLabFee;
													$privilege = $this->Student_model->Check_if_with_Privilege($this->input->post('history_id'));
													$amount_debit_lab = 0;
													$amount_debit_others = 0;
													$amount_debit_basic = 0;
													$amount_credit_others = 0;
													
													$credit_percentage = 1 - $withdraw_percentage->percentage;
													$amount_credit = $TuitionPlusLabFees * $credit_percentage;
													/*else{
														//if the student has availed of a privilge and has been posted already
														$paying_units_basic = $this->Student_model->getPayingUnitsBasic($idnum, $student_info->
																														academic_terms_id);
														$paying_units_others = $this->Student_model->getPayingUnitsOthers($idnum, $student_info->
																														academic_terms_id);
														$total_units_enrolled = $paying_units_basic->total_paying_units_enrolled + $paying_units_others->
																													total_paying_units_enrolled;
														$tuition_discount_info = $this->Scholarship_Model->getTuitionPrivilege($privilege->id);
														
														$max_units_info = $this->Scholarship_Model->getMaxUnitsDiscount($privilege->id);
														if($max_units_info){
															$max_unit_undergrad = $max_units_info->max_unit_undergrad;
															$max_unit_grad = $max_units_info->max_unit_grad;
														}else{
															$max_unit_undergrad = 50;
															$max_unit_grad = 50;
														}
														$academic_program_info = $this->Student_model->getAcadProgram($this->input->post('history_id'));
													
														if($student_info->year_level < $academic_program_info->max_yr_level){
														//for non-graduating
																	if($total_units_enrolled <= $max_unit_undergrad){
																	$tuition_discount = $totalTuitionFee * ($tuition_discount_info->discount_percentage/100);
																	$tuition_without_discount = $totalTuitionFee - $tuition_discount;
																	$tuition_fee = $tuition_discount * $withdraw_percentage->percentage;
																	$lab_fee = $totalLabFee * $withdraw_percentage->percentage;
																	$amount_debit = $tuition_fee + 	$lab_fee;
																	$credit_percentage = 1 - $withdraw_percentage->percentage;
																	$amount_credit = $tuition_without_discount * $credit_percentage;
																}else{
																	if($tuitionFeeOthers){
																 		  $tuition_discount = $tuitionFeeOthers * ($tuition_discount_info->
																   										discount_percentage/100);
																	   $amount_debit_others = $tuition_discount * $withdraw_percentage->percentage;
																	   $tuition_without_discount = $tuitionFeeOthers - $tuition_discount;
																	   $credit_percentage = 1 - $withdraw_percentage->percentage;
																	   $amount_credit_others = $tuition_without_discount * $credit_percentage;
																	}
																	if($totalLabFee){
																		$amount_debit_lab = $totalLabFee * $withdraw_percentage->percentage;
																	}
																//	print($total_paying_units); die();
																	if($total_paying_units){
																		$units_without_discount = $total_units_enrolled - ($max_unit_undergrad + $total_paying_units_others);
																		if($total_paying_units <= $units_without_discount){
																			$credit_percentage = 1 - $withdraw_percentage->percentage;
																			$amount_credit_tuition_basic = $TuitionPlusLabFees * $credit_percentage;
																			
																		}else{
																			$tuition_without_discount = $units_without_discount * $tuition_rate_basic->rate;
																			$credit_percentage = 1 - $withdraw_percentage->percentage;
																			$amount_credit_without_discount = $tuition_without_discount * $credit_percentage;
																			$units_with_discount = $total_paying_units - $units_without_discount;
																			$tuition_with_discount = $units_with_discount * $tuition_rate_basic->rate;
																			$tuition_discount = $tuition_with_discount * ($tuition_discount_info->
																													   	    discount_percentage/100);
																			$amount_debit_basic = $tuition_discount * $withdraw_percentage->percentage;
																			$tuition_without_discount_privilege = $tuition_with_discount - $tuition_discount;
																			$credit_percentage = 1 - $withdraw_percentage->percentage;
																	  		$amount_credit_privilege = $tuition_without_discount_privilege * $credit_percentage;
																			$amount_credit_tuition_basic = 	$amount_credit_without_discount +
																											$amount_credit_privilege;
																		}
																	}
																
																	$amount_debit = $amount_debit_others  +  $amount_debit_basic + $amount_debit_lab;
																	$amount_credit = $amount_credit_others +  $amount_credit_tuition_basic;
																}
															
														}else{
														//for graduating
															if($total_units_enrolled <= $max_unit_grad){
																$tuition_discount = $totalTuitionFee * ($tuition_discount_info->discount_percentage/100);
																$tuition_without_discount = $totalTuitionFee - $tuition_discount;
																$tuition_fee = $tuition_discount * $withdraw_percentage->percentage;
																$lab_fee = $totalLabFee * $withdraw_percentage->percentage;
																$amount_debit = $tuition_fee + 	$lab_fee;
																$credit_percentage = 1 - $withdraw_percentage->percentage;
																$amount_credit = $tuition_without_discount * $credit_percentage;
															}else{
																$amount_debit_others = 0;
																$amount_debit_basic = 0;
																if($tuitionFeeOthers){
																   $tuition_discount = $tuitionFeeOthers * ($tuition_discount_info->
																										discount_percentage/100);
																   $amount_debit_others = $tuition_discount * $withdraw_percentage->percentage;
																   $tuition_without_discount = $tuitionFeeOthers - $tuition_discount;
																   $credit_percentage = 1 - $withdraw_percentage->percentage;
																   $amount_credit_others = $tuition_without_discount * $credit_percentage;
																}
																if($totalLabFee){
																	$amount_debit_lab = $totalLabFee * $withdraw_percentage->percentage;
																}
																if($total_paying_units){
																	$units_without_discount = $total_units_enrolled - ($max_unit_grad + $total_paying_units_others);
																	if($total_paying_units <= $units_without_discount){
																		$credit_percentage = 1 - $withdraw_percentage->percentage;
																		$amount_credit_tuition_basic = $TuitionPlusLabFees * $credit_percentage;
																	}else{
																		$tuition_without_discount = $units_without_discount * $tuition_rate_basic->rate;
																		$credit_percentage = 1 - $withdraw_percentage->percentage;
																		$amount_credit_without_discount = $tuition_without_discount * $credit_percentage;
																		$units_with_discount = $total_paying_units - $units_without_discount;
																		$tuition_with_discount = $units_with_discount * $tuition_rate_basic->rate;
																		$tuition_discount = $tuition_with_discount * ($tuition_discount_info->
																														discount_percentage/100);
																		$amount_debit_basic = $tuition_discount * $withdraw_percentage->percentage;
																		$tuition_without_discount_privilege = $tuition_with_discount - $tuition_discount;
																		$credit_percentage = 1 - $withdraw_percentage->percentage;
																		$amount_credit_privilege = $tuition_without_discount_privilege * $credit_percentage;
																		$amount_credit_tuition_basic = 	$amount_credit_without_discount +
																										$amount_credit_privilege;
																	}
																}
																$amount_debit = $amount_debit_others  +  $amount_debit_basic + $amount_debit_lab;
																$amount_credit = $amount_credit_others +  $amount_credit_tuition_basic;
															}
														}
														$studtype = 'WP';
													}*/
													
													$payers = $this->teller_model->get_student($idnum);
													$data['payer_id'] = $payers->payers_id;
													$data['remark'] = $ref_no ;
													$data['empno'] = $this->userinfo['empno'];
													//$data['studtype'] = $studtype;
													$data['current_term_id'] = $academic_term->id;
													$data['amount'] = $amount_credit;
													$data['adjustment_type'] = 'credit';
													$data['description'] = 'WITHDRAW SUBJECT';
													//print_r($data); die();
													if($status2 = $this->Ledger_model->insertWithdrawalFees($data)){
														$this->db->trans_complete();
														$status3 = $status2;
													}else{
														$this->db->trans_rollback();
													}
													/*}else {
														if($amount_credit){
															$data['amount'] = $amount_credit;
															$data['adjustment_type'] = 'credit';
															$data['description'] = 'WITHDRAW SUBJECT';
															if($status2 = $this->Ledger_model->insertWithdrawalFees($data)){
																$this->db->trans_complete();
																$status3 = $status2;
															}else{
																$this->db->trans_rollback();
															}
														}
														if($amount_debit){
															$data['amount'] = $amount_debit;
															$data['adjustment_type'] = 'debit';
															$data['description'] = 'WITHDRAW SUBJECT - PRIVILEGE ADJUSTMENT';
															if($status2 = $this->Ledger_model->insertWithdrawalFees($data)){
																$this->db->trans_complete();
																$status3 = $status2;
															}else{
																$this->db->trans_rollback();
															}
														}
													}*/
												}	
											}//***  end of individual withdrawal
									}	//****end of outside adjustment period
									//print_r($status1); die();

									log_message("INFO", "STATUS 1".print_r($status1,true));  //toyet 4.16.2018
									log_message("INFO", "STATUS 2".print_r($status2,true));  //toyet 4.16.2018
									log_message("INFO", "STATUS 3".print_r($status3,true));  //toyet 4.16.2018

									if($status1 AND $status2 AND $status3){
												$return1 = array('tab'=>'sif', 'message'=>'Course successfully withdrawn/removed',
																			'severity'=>'alert-success');
									}else{
												$return1 = array('tab'=>'sif', 'message'=>'Error in withdrawing the course.',
																				'severity'=>'alert-error');
									}		
						//**** end of if with course selected
                              
						}else{
							$return1 = array('tab'=>'sif', 'message'=>'Error! No course selected!', 'severity'=>'alert-error');
						}//** end if no course is selected
					//*** end if no error in submitting the form
					}else {
							$return1 = array('tab'=>'sif', 'message'=>'Nonce error. Course is not Withdrawn.', 'severity'=>'alert-error');
					}
					break;

			case 'withdrawCourse'			:
								//within the enrollment period
								if($this->common->nonce_is_valid($this->input->post('nonce'))) {
									$date = date('Y-m-d');
									$period = $this->Academic_terms_model->what_period();
									$user_id = $this->session->userdata('empno');
									$status = $this->Student_model->withdrawCourseEnrolled($this->input->post('enrollment_id'), $period, $user_id);
									if($status){
												$return1 = array('tab'=>'sif', 'message'=>'Course successfully withdrawn',
																'severity'=>'alert-success');
											}else {
												$return1 = array('tab'=>'sif', 'message'=>'Error in withdrawing a course.',
																'severity'=>'alert-error');
											}
								}else {
									   $return1 = array('tab'=>'sif', 'message'=>'Nonce error. Course is not Withdrawn.',
														 'severity'=>'alert-error');
							 	}

								break;

			case 'view_grades'				:
								if($this->common->nonce_is_valid($this->input->post('nonce'))){
									$this->load->model('col_students_model');
									$return1 = array('tab'=>'grades', 
											'selected_history_id'=>$this->input->post('history_id'), 
											'grades'=>$this->col_students_model->student_grades_given_history_id($idnum, $this->input->post('history_id')));
								} else {
									$return1 = array('tab'=>'grades', 'message'=>'Nonce error. Please do not resubmit forms.',
											'severity'=>'alert-error');
								}
								break;

			//todo: what else can he do...
			default			:
				
								$return1 = array('tab'=>'sif');
								break;
		}
		$my_information = $this->student_common_model->my_information($idnum);
		//print_r($my_information);die();

		if ($my_information) {
			$meta = json_decode($my_information->meta, TRUE);
			//print_r($meta);die();
			$return = array(
					'firstname'		=>$my_information->fname,
					'familyname'	=>$my_information->lname,
					'middlename'	=>$my_information->mname,
					'course'		=>$my_information->abbreviation,
					'religions'		=>$religions,
					'citizenships'	=>$citizenships,
					'religions_id'	=>$my_information->religions_id,
					'citizenships_id'=>$my_information->citizenships_id,
					'birthdate'		=>$my_information->dbirth,
					'gender'		=>$my_information->gender,
					'civil_status'	=>$my_information->civil_status,
					'countries'		=>$countries,
					'place_of_birth'=>$my_information->full_birth_address,
					'philippine_id'	=>$philippine_id,
					'provinces'		=>$provinces,
					'home_address'	=>$my_information->full_home_address,
					'city_address'	=>$my_information->full_city_address,
					'level'			=>$my_information->year_level,
					'full_birth_address'	=> $my_information->full_birth_address,
					'full_city_address'		=> $my_information->full_city_address,
					'full_home_address'		=> $my_information->full_home_address,
					'stay_with'		=> $my_information->stay_with,
					'phone_number'	=> $my_information->phone_number,
					'section'		=> $my_information->section,
					'bed_status' 	=> $my_information->bed_status,

			);
			$return = array_merge ($return, $return1);
			if (is_array($meta)){								
				$return = array_merge($return, $meta);
				//print_r($return);die();
			}
			//print_r($return);die();
			return $return;

		} else {
			return FALSE;
		}

	}

	
	public function faculty(){
		$this->load->library('faculty_lib');
		$this->faculty_lib->faculty(TRUE);
	}
	
	
	public function download_student_list_csv (){
		$year_level = $this->input->post('year_level');
		$program = $this->input->post('program');

		$this->load->model('student_common_model');
		$records = $this->student_common_model->search_by_program ($program, $year_level, TRUE);
		//print_r($records); die();
		$filename = $records[0]->abbr . "_" . $year_level . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings
		fputcsv($output, array('No.', 'Name', 'Gender', 'Course and Year Level'));
		$count = 0;
		foreach ($records as $record){
			$count++;
			fputcsv ($output, array($count, $record->fullname, $record->gender, $record->abbr . " ". $record->year_level));
		}
	}

	
	//Added: March 1, 2014 by Amie
	public function download_faculty_load_csv (){
		$term_id = $this->input->post('term_id');
		$term = $this->input->post('term');
		$sy = $this->input->post('sy');
		
		$this->load->model('hnumis/faculty_model');
		$records = $this->faculty_model->List_faculty_load($term_id);
				
		$filename = "FacultyLoad_" . $term . "_" . $sy . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
		
		fputcsv($output, array('Emp ID', 'Name', 'Subject', 'Section', 'Credit Units', 'Paying Units', 'Schedule Day', 'Schedule Time', 'Classroom', '# of Students'));
		
		foreach ($records as $record){
			fputcsv ($output, array($record->empno, $record->name, $record->course_code, $record->section_code, $record->credit_units, $record->paying_units, $record->days_day_code, $record->start_time."-".$record->end_time, $record->room_no, $record->num_enrollees));
		}
	}
	
	//Added: March 8, 2014 by Isah
	public function download_CWTS_grads_csv (){
		$term_id = $this->input->post('term_id');
		$sy = $this->input->post('sy');
		$term = $this->input->post('term');
		$gender = $this->input->post('gender');
		if($gender == "Female")
			$gender = 'F';
		else 
			$gender = 'M';
		$this->load->model('hnumis/Enrollments_model');
		
		$records = $this->Enrollments_model->list_CWTS_enrollees($term_id, $gender);
		//print_r($records); die();
		$filename = "CWTSGraduates_" . $term . "_" . $sy . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
	
		fputcsv($output, array('Name', 'Course', 'Birth Date', 'Age', 'Address', 'Tel. No.', 'Grade'));
	
		foreach ($records as $record){
			fputcsv ($output, array($record->sname, $record->abbreviation, $record->dbirth, $record->age, $record->home_address, $record->phone_number, $record->finals_grade));
		}
	}
	
	//Added: March 11, 2014 by Amie
	public function download_courses_masterlist_csv (){
			
		$this->load->model('hnumis/courses_model');
		$records = $this->courses_model->course_masterlist();
	
		$filename = "CouresMasterlist.csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
	
		fputcsv($output, array('Catalog No.', 'Description', 'Pay Units', 'Load Units', 'Contact Hours', 'College', 'Flags'));
	
		foreach ($records as $record){
			fputcsv ($output, array($record->course_code, $record->descriptive_title, $record->paying_units, $record->credit_units, $record->contact_hours, $record->college_code, $record->course_type));
		}
	}
	
	//Added: March 12, 2014 by Amie
	public function download_masterlist_address_csv (){
		$term_id = $this->input->post('term_id');
		$sy = $this->input->post('sy');
		$term = $this->input->post('term');
		
		$this->load->model('hnumis/Enrollments_model');
	
		$records = $this->Enrollments_model->masterlist_report($term_id);
		$filename = "Student_Masterlist_" . $term . "_" . $sy . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
	
		fputcsv($output, array('Student ID', 'Name', 'Course', 'Address'));
	
		foreach ($records as $record){
			fputcsv ($output, array($record->idno, $record->sname, $record->course, $record->home_address));
		}
	}
	
	//Added: 4/3/2014 by Isah
	public function download_room_utilization_csv (){
		$term_id = $this->input->post('term_id');
		$term = $this->input->post('term');
		$sy = $this->input->post('sy');
	
		$this->load->model('hnumis/Enrollments_Model');
		$records = $this->Enrollments_Model->room_utilization($term_id);
	
		$filename = "RoomUtilization_" . $term . "_" . $sy . ".csv";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);
	
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
	
		// output the column headings
	
		fputcsv($output, array('Building Name', 'Room No', 'Day', 'Schedule', 'Teacher', 'Course Code', 'Section Code', '# of Students'));
	
		
		foreach ($records as $record){
			fputcsv ($output, array($record->bldg_name, $record->room_no, $record->day_name, $record->schedule_time, $record->name, $record->course_code, $record->section_code, $record->num_enrollees));
		}
	}
	
	public function offerings_current_and_previous(){
		$this->offerings(FALSE);
	}

	public function offerings_incoming(){
		$this->offerings(TRUE);
	}
	
	
	public function offerings($incoming = FALSE){
		$this->content_lib->enqueue_footer_script('data_tables');
		$this->content_lib->set_title ('DRIC | Offerings | ' . $this->config->item('application_title'));
		$limit = array();
		$show_only = ($this->session->userdata('show_only') ? $this->session->userdata('show_only') : 'all');
		$show_college = ($this->session->userdata('show_college') ? $this->session->userdata('show_college') : '');
		$show_program = ($this->session->userdata('show_program') ? $this->session->userdata('show_program') : '');
		$current_academic_term = ($this->session->userdata('show_academic_term') ? $this->session->userdata('show_academic_term') : '');
		$limit = array('college'=>$show_college, 'program'=>$show_program);
		$this->load->library('print_lib');
		$this->content_lib->enqueue_after_html(sprintf($this->config->item('jzebra_applet'), base_url()));
		//process actions here...
		if ($action = $this->input->post('action')) {
			if ($this->common->nonce_is_valid($this->input->post('nonce'))){
				switch ($action){
					case 'limit_results' :
								$limit = array(
									'college'=>$this->input->post('college'),
									'program'=>$this->input->post('programs'),
								);
								$show_college = $this->input->post('college');
								$show_program = $this->input->post('programs');
								$show_only = $this->input->post('offering_status');
								$show_only = ( ! empty($show_only) ? $show_only : 'all' );
								$this->session->set_userdata(array('show_only'=>$show_only));
								$this->session->set_userdata(array('show_college'=>$this->input->post('college')));
								$this->session->set_userdata(array('show_program'=>$this->input->post('programs')));
								break;
					case 'set_academic_term' :
								$current_academic_term = $this->input->post('academic_term');
								$this->session->set_userdata(array('show_academic_term'=>$current_academic_term));
								break;
				}
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}
		}
		$page = $this->input->post('page') ? $this->input->post('page') : 1;
		$this->load->model('hnumis/Courses_model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/College_model');
		$this->load->model('hnumis/AcademicYears_model');

		$academic_terms = $incoming === TRUE ? $this->academic_terms_model->academic_terms_incoming() : $this->academic_terms_model->academic_terms(6);
		$colleges = $this->College_model->all_colleges();

		$current_term = ( ! empty($current_academic_term) ? $current_academic_term : $this->academic_terms_model->getCurrentAcademicTerm()->id);

		$courses_offered = $this->Courses_model->list_courses_with_limit($limit, $current_term, $show_only, $page, $this->config->item('results_to_show_per_page'));
		
		//$courses_offered = $this->Courses_model->list_courses_with_limit($limit, 163, $show_only, $page, $this->config->item('results_to_show_per_page'));
		$this->load->library('pagination_lib');
		$pagination = "";
		$total = $courses_offered['total_rows'];
		if (! empty($courses_offered)){
			$start = ($page - 1)*(int)$this->config->item('results_to_show_per_page') + 1;
			$end = ($start + (int)$this->config->item('results_to_show_per_page') > $total
					? $total
					: ($start + (int)$this->config->item('results_to_show_per_page') - 1));
			$pagination = $this->pagination_lib->pagination('', $courses_offered['total_rows'], $page);
		} else {
			$start = 0;
			$page = 1;
			$end = 0;
		}

		$content = array(
				'courses_offered'=>$courses_offered['result'],
				'page'=>$page,
				'start'=>$start,
				'end'=>$end,
				'total'=>$courses_offered['total_rows'],
				'pagination'=> $pagination,
				'colleges'=>$colleges,
				'show_college'=>$show_college,
				'show_program'=>$show_program,
				'academic_terms'=>$academic_terms,
				'current_academic_term'=>$current_term,
				);
		$this->load->library('print_lib');
		$this->content_lib->enqueue_body_content('dric/course_offerings', $content);
		$this->content_lib->enqueue_body_content('print_templates/class_offerings', array('courses_offered'=>$courses_offered));
		$this->content_lib->content();
	}

	public function download_offerings (){

		if ($this->common->nonce_is_valid($this->input->post('nonce'))){
			$limit = array(
					'college'=>$this->input->post('college'),
					'program'=>$this->input->post('programs'),
			);
			$show_only = $this->input->post('offering_status');
			$show_only = ( ! empty($show_only) ? $show_only : 'all' );

			$this->load->model('hnumis/Courses_model');
			$this->load->model('academic_terms_model');

			$current_term = ($this->input->post('academic_term') ? $this->input->post('academic_term') : $this->academic_terms_model->getCurrentAcademicTerm()->id);
			$records = $this->Courses_model->list_courses_with_limit($limit, $current_term, $show_only, 1, 100000);
			$filename = "course_offerings.csv";
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=' . $filename);

			//create a file pointer connected to the output stream
			$output = fopen('php://output', 'w');

			// output the column headings
			fputcsv($output, array('Catalog ID', 'Section', 'Descriptive Title', 'Schedule', 'Room', 'Enrolled | Max', 'Teacher', 'Status'));
			//print_r($records);
			foreach ($records['result'] as $record){
				fputcsv ($output, array($record->course_code, $record->section_code, $record->descriptive_title, str_replace('<br />', ' ', $record->schedule), $record->room, $record->enrolled_count . ' | ' . $record->max_enrollment_count, $record->teacher, $record->status));
			}
		}
	}

	public function class_list(){
		$this->load->model('hnumis/student_model');
		//$this->student_model->class_list($this->input->post('course_offerings_id')); die();
		if ( $this->input->post()){
			if ($this->common->nonce_is_valid($this->input->post('nonce'))) {
				$this->load->model('hnumis/Enrollments_model');
				$students = $this->Enrollments_model->class_record($this->input->post('course_offerings_id'));

				$class = array(
							'catalog_id'=>($students ? $students[0]->course_code : $this->input->post('course_code')),
							'description'=>($students ? $students[0]->descriptive_title : $this->input->post('descriptive_title')),
							'section_code'=>($students ? $students[0]->section_code : $this->input->post('section')),
							'term'=>$this->input->post('term'),
							'teacher'=>$this->input->post('teacher'),
							'schedule'=>$this->input->post('schedule'),
							'students'=>$students,
						);
				$this->content_lib->enqueue_body_content('dric/class_list', array('course_offering'=>$class));
			} else {
				$this->content_lib->set_message($this->config->item('nonce_error_message'), 'alert-error');
			}
		} else {
			$this->content_lib->set_message('You are not allowed to go directly to this link.', 'alert-error');
		}
		$this->content_lib->content();
	}

	public function alphalist(){
		$this->content_lib->set_title ('DRIC | Alpha List | ' . $this->config->item('application_title'));
		$this->content_lib->content();
	}

	/****** *****/

	public function enrollment_summary() {
		$this->content_lib->set_title ('DRIC | Enrollment Summary | ' . $this->config->item('application_title'));
		$this->load->model('hnumis/Reports_Model');

		$this->Reports_Model->enrollment_summary();

	}
	
	//Justing Nov. 22, 2013
	public function pdf_class_list($course_offerings_id=''){
		if (empty($course_offerings_id)){
			die();
		}
		$this->load->library('class_list_pdf_lib');
		$this->class_list_pdf_lib->generate_class_list_pdf($course_offerings_id);
	}
	
	public function printer (){
		$this->load->model('hnumis/college_model');
		$this->load->model('academic_terms_model');
		$this->load->model('mass_printing_model', 'mass_printing');
		
		$this->content_lib->set_title ('DRIC | Print Shortcuts | ' . $this->config->item('application_title'));
		$this->load->library('tab_lib', 'print_tabs');
		$this->content_lib->enqueue_body_content('dric/print');
		$this->tab_lib->enqueue_tab('Grades', 'dric/print/grades', array(
				'academic_terms'=>$this->academic_terms_model->academic_terms(10), 
				'colleges'=>$this->college_model->all_colleges()
				), 'grades', TRUE);
		$this->tab_lib->enqueue_tab('Enrollment List', 'dric/print/enrollment_list', array(
				'academic_terms'=>$this->academic_terms_model->academic_terms(10),
				'colleges'=>$this->college_model->all_colleges()
		), 'enrollment_list', FALSE);
		$this->tab_lib->enqueue_tab('Master List', 'dric/print/master_list', array(
				'academic_terms'=>$this->academic_terms_model->academic_terms(10), 
				'colleges'=>$this->college_model->all_colleges(),
				'academic_groups'=>$this->mass_printing->academic_groups(),
				), 'master_list', FALSE);
		$this->tab_lib->enqueue_tab('FORM XIX', 'dric/print/form19', array(
				'academic_terms'=>$this->academic_terms_model->academic_terms(10),
				'colleges'=>$this->college_model->all_colleges(),
				), 'form19', FALSE);
		$this->tab_lib->enqueue_tab('Offerings', 'dric/print/offerings', array(), 'offerings', FALSE);
		$this->tab_lib->enqueue_tab('Class List', 'dric/print/class_list', array(), 'class_list', FALSE);
		$this->content_lib->enqueue_body_content('', $this->tab_lib->content());
		$this->content_lib->content();
	}
	
	public function print_id(){
		$this->load->view('dric/print_id/id.html', array());
	}
	
	
	// added by ra 3/21/14
	//EDITED: 6/3/14 by genes
	private function print_diploma($idnum, $graduated_students_id, $leb_no=NULL,$serial_no=NULL){
		$this->load->model('student_common_model');
		$this->load->model('hnumis/otherschools_model');
		
		//print_r($this->input->post); die();
		
		$data['my_student'] = $this->student_common_model->my_information($idnum);
		$data['so_text']    = $this->input->post('so_text');
		if ($this->input->post('grad_hnu') == 'Y' AND $this->input->post('with_dwct') == 'N') {
			$data['my_diploma'] = $this->Student_model->getStudent_SO($graduated_students_id);
		} else {
					
			$data['my_diploma'] = new stdClass();

			$other_school = $this->otherschools_model->getOtherSchoolProgram($graduated_students_id);
			$data['my_diploma']->description = $other_school->program;
			
			$m1 = substr($this->input->post('grad_date'),5,2);
			$d1 = substr($this->input->post('grad_date'),8,2);
			$y1 = substr($this->input->post('grad_date'),0,4);
			
			switch ($m1) {
				case 1:
					$data['my_diploma']->month_diploma = 'Enero';
					break;
				case 2:
					$data['my_diploma']->month_diploma = 'Pebrero';
					break;
				case 3:
					$data['my_diploma']->month_diploma = 'Marso';
					break;
				case 4:
					$data['my_diploma']->month_diploma = 'Abril';
					break;
				case 5:
					$data['my_diploma']->month_diploma = 'Mayo';
					break;
				case 6:
					$data['my_diploma']->month_diploma = 'Hunyo';
					break;
				case 7:
					$data['my_diploma']->month_diploma = 'Hulyo';
					break;
				case 8:
					$data['my_diploma']->month_diploma = 'Agosto';
					break;
				case 9:
					$data['my_diploma']->month_diploma = 'Setyembre';
					break;
				case 10:
					$data['my_diploma']->month_diploma = 'Oktobre';
					break;
				case 11:
					$data['my_diploma']->month_diploma = 'Nobyembre';
					break;
				case 11:
					$data['my_diploma']->month_diploma = 'Disyembre';
					break;
			}
				
			$data['my_diploma']->grad_date = DATE('F j, Y',mktime(0,0,0,$m1,$d1,$y1));
			$data['my_diploma']->month_diploma_numeric = DATE('n',mktime(0,0,0,$m1,$d1,$y1));
			$data['my_diploma']->month_diploma1 = DATE('F',mktime(0,0,0,$m1,$d1,$y1));
			$data['my_diploma']->day_diploma = DATE('j',mktime(0,0,0,$m1,$d1,$y1));
			$data['my_diploma']->year_diploma = DATE('Y',mktime(0,0,0,$m1,$d1,$y1));
			
		}
		
		//print($data['my_diploma']->academic_programs_id); die();
		
		if ($data['my_diploma']->academic_programs_id == 17) {
			//if student graduated as LI.B or Bachelor of Laws
			$data['my_diploma']->leb_no = $leb_no;
			$data['my_diploma']->serial_no = $serial_no;
			$this->load->view('dric/print_diploma/col_diploma.html', $data);						
		} else {
			$this->load->view('dric/print_diploma/diploma.html', $data);			
		}

	}
			
	public function barcode($idnumber){
		$idnumber = str_pad ($idnumber, '0', 8, STR_PAD_LEFT);
		$this->load->library('barcode');
		$this->barcode->render($idnumber);
	}
	
	public function pdf($what='enrollment_list', $academic_term, $academic_program, $year_level){
		$this->load->model('mass_printing_model', 'printer');
		$students = $this->printer->enrollment_list ($academic_term, $academic_program, $year_level);
		$student_courses = array();
		foreach($students as $key => $student){
			if($key==0){
				$sy = $student->sy;
				$term = $student->academic_term;
				$academic_program = $student->program;
				$year_level = $student->year_level;
			}
			if(isset($student_courses[$student->idno])){
				$student_courses[$student->idno]['courses'][] = (object)array(
						'course'=>$student->course_code,
						'credit_units'=>$student->credit_units,
						'is_bracketed'=>$student->is_bracketed,
				);
			} else {
				$student_courses[$student->idno]['courses'] = array(
						(object)array(
								'course'=>$student->course_code,
								'credit_units'=>$student->credit_units,
								'is_bracketed'=>($student->is_bracketed == 'Y' ? TRUE : FALSE )
						)
				);
				$student_courses[$student->idno]['info'] = (object)array(
						'fullname'=>$student->fullname,
						'idno'=>$student->idno,
						'gender'=> $student->gender,
				);
			}
		}
		$this->load->view('dric/pdf/enrollment_list', array(
				'student_courses'=>$student_courses,
				'school_year' => $sy,
				'academic_term'=>$term,
				'year_level'=>$year_level,
				'academic_program'=>$academic_program));
	}

	public function list_withdrawals() {
		$this->load->library('../controllers/accountant/');
		$this->accountant->Withdrawals();
	}
	
	//Added 3/1/2014 by Isah
	
	public function faculty_load() {
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/faculty_model');
		
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of academic terms
				$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
				$this->content_lib->enqueue_body_content('dric/search_academic_term', $data);
				$this->content_lib->content();
				break;
			case 2:
				$data['term'] = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms'));
				//print_r($data['term'] ); die();
				$data['loads'] = $this->faculty_model->List_faculty_load($data['term']->id);
				
				$this->content_lib->enqueue_body_content('dric/faculty_load',$data);
				$this->content_lib->content();
				break;
		}
	}
	//Added 3/1/2014 by Isah
	
	public function cwts_graduates() {
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/Enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of academic terms and gender choices
				$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
				$this->content_lib->enqueue_body_content('dric/search_term_gender', $data);
				$this->content_lib->content();
				break;
			case 2:
				$data['term'] = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms'));
				$gender = $this->input->post('gender');
				$course = $this->input->post('course');
			
				if($course == 'C'){
					$course_id = 1506;
					$data['course_nm'] = "NSTP-CWTS GRADUATES";
				}	
				else {
					$course_id = 1067;
					$data['course_nm'] = "MS 12 GRADUATES";
				}
				
				$data['graduates'] = $this->Enrollments_model->list_CWTS_enrollees($data['term']->id, $gender, $course_id);
				//print_r($data); die();
				if ($gender == 'M') {
						$data['gender'] = "Male";
				} else {
					if ($gender == 'F') {
						$data['gender'] = 'Female';
					} else {
						$data['gender'] = 'Masterlist';
					}
				}
				
				//print($data['gender']); die();
				$this->content_lib->enqueue_body_content('dric/CWTS_graduates',$data);
				$this->content_lib->content();
				break;
		}
	}
	
	//Added: March 11, 2014 by Amie
	public function courses_masterlist() {
		$this->load->model('hnumis/courses_model');

		$data['courses'] = $this->courses_model->course_masterlist();
		$this->content_lib->enqueue_body_content('dric/courses_masterlist',$data);
		$this->content_lib->content();
	}
	
	//Added: March 12, 2014 by Amie
	public function masterlist_withaddresses() {
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/Enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of academic terms
				$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
				$this->content_lib->enqueue_body_content('dric/search_academic_term', $data);
				$this->content_lib->content();
				break;
			case 2:
				$data['term'] = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms'));
				$data['students'] = $this->Enrollments_model->masterlist_report($data['term']->id);
				
				$this->content_lib->enqueue_body_content('dric/masterlist_report',$data);
				$this->content_lib->content();
				break;
		}
	}
	
	//Added: March 18, 2014 by Amie
	public function room_utilization() {
		$this->load->model('hnumis/AcademicYears_Model');
		$this->load->model('academic_terms_model');
		$this->load->model('hnumis/Enrollments_model');
	
		$action = ($this->input->post('action') ?  $this->input->post('action') : 1);
			
		switch ($action) {
	
			case 1: //display a list of academic terms 
				$data['academic_terms'] = $this->AcademicYears_Model->ListAcademicTerms(FALSE);
				$this->content_lib->enqueue_body_content('dric/search_academic_term', $data);
				$this->content_lib->content();
				break;
			case 2:
				$data['term'] = $this->academic_terms_model->current_academic_term($this->input->post('academic_terms'));
				$data['offerings'] = $this->Enrollments_model->room_utilization($data['term']->id);
	
				$this->content_lib->enqueue_body_content('dric/room_utilization',$data);
				$this->content_lib->content();
				break;
		}
	}


	//ADDED: 5/31/14 genes
	function download_enrollment_summary_to_csv() {
		$this->load->model('hnumis/Reports_Model');
	
		$this->Reports_Model->download_enrollment_summary_to_csv();
	
	}

	function get_students() {
		$this->load->model('hnumis/Enrollments_Model');
		
		$data['students'] = $this->Enrollments_Model->masterlist($this->input->get('selected_term'), $this->input->get('prog_id'), $this->input->get('yr_level'), TRUE);
		
		$this->load->view('dean/list_of_students_from_modal', $data);
		
	}
	
}
?>
