

		<h2 class="heading">Assessment Report - Posted Against Running Balance 
				<?php 
					switch ($report_type) {
						case 'college':
							print("By College");
							break;
						
						case 'col_student':
							print("By College Student List");
							break;
					}
				?>
		</h2>

		<div>
			<form id="generate_report_form" method="post" class="form-horizontal">
				<input type="hidden" name="action" value="generate_assessment" />
				
				<div class="control-group acad_term" style="display:block;">
					<label class="control-label" for="academic_term">Academic Term&nbsp;&nbsp;:</label>
					<div class="controls ">
						 <select name="academic_terms_id" id="academic_terms_id">
							<?php
								foreach($academic_terms AS $academic_term) {
							?>			
									<option value="<?php print($academic_term->id); ?>" 
										<?php if ($current_term->id == $academic_term->id) print("selected"); ?> 
										term_sy="<?php print($academic_term->term." ".$academic_term->sy); ?>" >
										<?php 
											print($academic_term->term." ".$academic_term->sy);
										?>
									</option>
							<?php
								}
							?>
						</select>
					</div>
				</div>
								
				<div class="control-group">
					<label class="control-label" for="college">College&nbsp;&nbsp;:</label>
					<div class="controls ">
						<select name="colleges_id" id="colleges_id">
							<option value="0">--Select College--</option> 
							<?php
								foreach($colleges AS $college) {
							?>			
									<option value="<?php print($college->id); ?>" 
										college_name="<?php print($college->name); ?>"
										college_code="<?php print($college->college_code); ?>" >
										<?php print($college->college_code); ?>
									</option>
							<?php
								}
							?>
						</select>
					</div>
				</div>

				<div class="control-group">
					 <div class="controls">
						<button class="btn btn-primary" name="generate_report_button" type="submit" id="generate_report" disabled>Generate Report!</button>
					</div>
				</div>
				
			</form>
		</div>

		<div style="margin-top:20px; text-align:center;" id="show_extract_icon">
			
		</div>
		

<script>
$('#colleges_id').bind('click', function(event){
	event.preventDefault();

		var element     = $("option:selected", "#colleges_id");
		var colleges_id = element.val();

		if (colleges_id != 0) {

			$('#generate_report').prop("disabled", false);
			$('.has_college').show();

		} else {
			$('#generate_report').prop("disabled", true);
			$('.has_college').hide();
		}
});
	
</script>


<script>
$('#generate_report').bind('click', function(event){
	event.preventDefault();
	var confirmed = confirm("Continue to generate Tuition Report?");

	if (confirmed){
		
		var element      = $("option:selected", "#academic_terms_id");
		var term_sy      = element.attr('term_sy');
		var element      = $("option:selected", "#colleges_id");
		var college_name = element.attr('college_name');
		var college_code = element.attr('college_code');

		$('#show_extract_icon').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' />"); 

		$('<input>').attr({
			type: 'hidden',
			name: 'term_sy',
			value: term_sy,
		}).appendTo('#generate_report_form');

		$('<input>').attr({
			type: 'hidden',
			name: 'college_name',
			value: college_name,
		}).appendTo('#generate_report_form');
				
		$('<input>').attr({
			type: 'hidden',
			name: 'college_code',
			value: college_code,
		}).appendTo('#generate_report_form');
				
		$('#generate_report_form').submit();
	}		
});
</script>





  