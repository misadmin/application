		<div style="width:100%; float:right; overflow:auto; padding:7px;" >
			<a href="#" style="text-decoration: none; color:#4E4D4D; font-size:12px; "
						college_name="<?php print($college_name); ?>"
						abbreviation="<?php print($abbreviation); ?>"
						description="<?php print($description); ?>"
						term_sy="<?php print($term_sy); ?>"
						year_level="<?php print($year_level); ?>"
						reports='<?php print(json_encode($reports,JSON_HEX_APOS)); ?>'
						class="download_excel_report" >
						<div style="float:right; padding-left:3px; padding-top:2px;">Download to Excel</div>
						<div id="excel_labels" style="float:right;">
							<img src="<?php print(base_url('assets/images/excel_icon.png')); ?>" style="height:20px;" /> 
						</div>
			</a>
		</div>

		
<script>
	$('.download_excel_report').click(function(event){
		event.preventDefault(); 
		
		var college_name = $(this).attr('college_name');
		var abbreviation = $(this).attr('abbreviation');
		var description  = $(this).attr('description');
		var term_sy      = $(this).attr('term_sy');
		var year_level   = $(this).attr('year_level');
		var reports      = $(this).attr('reports');
		
		$('#excel_labels').html("<img src='<?php echo base_url('assets/img/loading.gif') ?>' style='height:20px;' />")
		
		$.ajax({
			cache: false,
			type: 'POST',
			url: "<?php echo site_url($this->uri->segment(1).'/posted_running_student'); ?>",
			data: { 
					"college_name": college_name,
					"abbreviation": abbreviation,
					"description": description,
					"term_sy": term_sy,
					"year_level": year_level,
					"reports": reports,
					"action": 'create_posted_running_excel'},
			//dataType: 'json',
			success: function(response) {
				//$('#show_extracted_students').html(response.students); 
				window.location.href = '<?php print(base_url("downloads/assessment_".$abbreviation."_".$year_level.".xls")); ?>';

				$('#excel_labels').html("<img src='<?php print(base_url('assets/images/excel_icon.png')); ?>' style='height:20px;'> ")
			}
	
		});
			
	});
		
</script>	

		