<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:25px;
}
input.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:12px;
}
</style>

<form action="<?php echo site_url('accounts/list_privileges');?>" method="post">
  <?php echo validation_errors(); 
  ?>
  <input type="hidden" name="step" value="4" />
  <div style="background:#FFF; ">
    <table align="center" cellpadding="0" cellspacing="0" style="width:40%; margin-top:35px;" class="head1">
      <tr class="head">
        <td colspan="3" class="head"> UPDATE PRIVILEGE  </td>
			
      </tr>
      <tr>
          <td colspan="3"><table border="0" cellpadding="2" cellspacing="0" class="inside">
		 <td align="left" valign="middle">Privilege</td>
         <td align="left" valign="middle">:</td>
         <td align="left" valign="middle"><select name="privilege_id" id="privilege_id" class="form" value = "<?php print($privilege_info->scholarship)?>;">
          <?php
				foreach($privileges AS $privilege) {
					print("<option value=".$privilege->id.">".$privilege->scholarship."</option>");	
				}
		 ?>
         </select></td>
     </tr>
     <tr>
          <td align="left" valign="middle">Name</td>
          <td align="left" valign="middle">:</td>
           <td align="left" valign="middle"><?php print($privilege_info->lname.",".$privilege_info->fname); ?></td>
     </tr>
          <tr>
            <td>Course/Year </td>
            <td>:</td>
            <td><?php print($privilege_info->abbreviation."-".$privilege_info->year_level); ?></td>
          </tr>
          <tr>
            <td>Privilege Discount</td>
            <td>:</td>
            <td><input name="priv_disc" type="text" id="priv_disc" value="<?php print($privilege_info->discount_percentage); ?>" size="10" maxlength="10" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="button2" id="button2" value="Update Student Privilege!"  class="btn btn-success" /></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</form>