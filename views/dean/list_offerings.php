<h2 class="heading">List of Offerings per College</h2>
<div class="span10" style="width:95%; margin-left:0px;">

<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px;">

<form id="change_sy" method="POST">
		<select name="academic_terms_id" id="academic_terms_id" style="width:auto;">
				<?php 
					foreach($terms AS $term) {
				?>
						<option value="<?php print($term->id); ?>" <?php if ($term->id == $selected_term) { print("selected"); } ?>>
							<?php print($term->term.' '.$term->sy); ?> </option>
				<?php 	
					}
				?>
		</select>

	    <select name="colleges_id" id="colleges_id" style="width:auto;">
				<option value="0" >All Colleges</option>
	    		<?php
					foreach($colleges AS $college) {
						if ($college->id == $selected_college) {
							print("<option value=\"$college->id\" selected>$college->college_code</option>");
						} else {
							print("<option value=\"$college->id\">$college->college_code</option>");
						}
					}
				?>
		</select>

</form>	
</div>


<table id="offerings" class="table table-bordered table-condensed">
    <thead>
    <tr>
      <th width="8%" style="text-align:center; vertical-align:middle;">Catalog ID</th>
      <th width="5%" style="text-align:center; vertical-align:middle;">Section</th>
      <th width="26%" style="text-align:center; vertical-align:middle;">Descriptive Title</th>
      <th width="5%" style="text-align:center; vertical-align:middle;">Credit Units</th>      
      <th width="5%" style="text-align:center; vertical-align:middle;">Paying Units</th>      
      <th width="15%" style="text-align:center; vertical-align:middle;" >Time</th>
      <th width="5%" style="text-align:center; vertical-align:middle;" >Room</th>
      <th width="10%" style="text-align:center; vertical-align:middle;" >Enrolled</th>
      <th width="17%" style="text-align:center; vertical-align:middle;" >Faculty</th>
      <th width="3%" style="text-align:center; vertical-align:middle;">Allocations</th>
      <th width="6%" style="text-align:center; vertical-align:middle;" >Status</th>
    </tr>
    </thead>
<?php
		if ($offerings) {
			foreach ($offerings AS $offer) {
				echo '<tr>';
				echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->course_code;
				echo '<td style="text-align:center; vertical-align:middle; ">' . trim($offer->section_code);
				echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->descriptive_title;
				echo '<td style="text-align:center; vertical-align:middle; ">' . $offer->credit_units;       
				echo '<td style="text-align:center; vertical-align:middle; ">' . number_format($offer->paying_units,1);
				
				$offering_slots = $this->Offerings_Model->ListOfferingSlots($offer->id);
				echo '<td style="text-align:left; vertical-align:middle; ">';
				if ($offering_slots) {
					foreach($offering_slots[0] AS $slots) {
						echo $slots->tym .' ' . $slots->days_day_code . '<br>';
					}
					echo '<td style="text-align:center; vertical-align:middle; ">';
					foreach($offering_slots[0] AS $slots) {
						echo $slots->bldg_name !== 'Main' ? str_pad($slots->room_no, 4, "0", STR_PAD_LEFT): $slots->room_no ;
						echo '<br>';
					}
				}else{
					echo '<td style="text-align:center; vertical-align:middle; ">';
				}    
      			echo '<td style="text-align:center; vertical-align:middle; ">';
				if ($offer->enrollee_parallel) { 
				 	 print($offer->enrollee_parallel."/".$offer->max_students); 
				} else { 
					print($offer->enrollee."/".$offer->max_students);  	  
				} 
				echo '<td style="text-align:left; vertical-align:middle; ">' . $offer->employees_empno . ' ' . $offer->emp_name;
?>
					<td class="middle-td" style="text-align:center;">
						<a href="#" data-toggle="modal" data-target=#modal_allocation_<?php echo $offer->id;?> data-project-id=""><i class="icon-zoom-in"></i></a>

					<div class="modal hide fade" id="modal_allocation_<?php print($offer->id); ?>" 
						offer_id="<?php print($offer->id); ?>" >
						<div class="modal-header">
					       <button type="button" class="close" data-dismiss="modal">�</button>
					       <h3>List of Allocations</h3>
						</div>
						<div class="modal-body">            
							<div style="text-align:left; margin-top:0px; font-size:13px; font-weight:bold;">
								<?php print($offer->course_code." [".$offer->section_code."]<br>"); ?>
								<?php print($offer->descriptive_title.'<br>'); 
									if ($offering_slots) {
										foreach($offering_slots[0] AS $slots) {
	           								print($slots->tym.' '.$slots->days_day_code.' ['.$slots->room_no.']<br>'); 
		  								}
									}
								?>
							</div>
	
							<div id="modalContentAlloc_<?php print($offer->id); ?>" >
							       text here
							</div>
						</div>
						<div class="modal-footer">
						    <a href="#" class="btn" data-dismiss="modal">Close</a>
						</div>
					</div>

					<script>
					$('#modal_allocation_<?php print($offer->id); ?>').on('show', function(){
						  var offer_id = $(this).attr('offer_id');
						  $('#modalContentAlloc_<?php print($offer->id); ?>').html('loading...')
					
						  $.ajax({
						      cache: false,
						      type: 'GET',
						      url: '<?php echo site_url($this->uri->segment(1).'/list_allocations');?>',
						      data: {offer_id: <?php print($offer->id); ?>},
						      success: function(data) {
						        $('#modalContentAlloc_<?php print($offer->id); ?>').html(data); //this part to pass the var
						      }
						  });
						})
					</script>						
</td>

<?php 				echo '<td style="text-align:center; vertical-align:middle; ">' . ucfirst(strtolower($offer->status)); 
			}
		}
?>
</table>
</div>

<script>
	$(document).ready(function(){
		$('#colleges_id').bind('change', function(){
			show_loading_msg();
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 2,
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>

<script>
	$(document).ready(function(){
		$('#academic_terms_id').bind('change', function(){
			show_loading_msg();
			$('<input>').attr({
				type: 'hidden',
				name: 'step',
				value: 2,
			}).appendTo('#change_sy');
			$('#change_sy').submit();
		});
	});
</script>


<script>
$().ready(function() {
	$('#offerings').dataTable( {
	    "aoColumnDefs": [{ "bSearchable": false, "aTargets": [ 1,2,3,4,5,6 ] }],
		"iDisplayLength": 50,
	    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],	    
    });
    $("#offerings").stickyTableHeaders();
    $('#offerings').stickyTableHeaders('updateWidth');
	
	
})
</script>