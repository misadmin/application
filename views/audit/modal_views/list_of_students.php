		<div style="width:100%; overflow:auto; padding-top:10px; ">
			<table class="table table-hover table-bordered table-striped" >
				<thead>
					<tr style="text-align:center; font-size:11px; font-weight:bold;" class="shs_header" >
						<td class="shs_header" style="width:5%;">Count</td>
						<td class="shs_header" style="width:15%;">ID<br>Number</td>
						<td class="shs_header" style="width:23%;">Lastname</td>
						<td class="shs_header" style="width:22%;">Firstname</td>
						<td class="shs_header" style="width:20%;">Middlename</td>
						<td class="shs_header" style="width:15%;">Course-Year</td>
					</tr>
				</thead>
				<tbody style="font-size:11px;">
					<?php
						if ($students) {
							$cnt=1;
							foreach($students AS $student) {
					?>			
								<tr>
									<td style="text-align:right;">
										<?php print($cnt); ?>.
									</td>
									<td style="text-align:center;">
										<a href="<?php print(site_url($this->uri->segment(1).'/student')."/".$student->idno);?>" >
											<?php print($student->idno); ?>
										</a>
									</td>
									<td><?php print($student->lname); ?></td>
									<td><?php print($student->fname); ?></td>
									<td><?php print($student->mname); ?></td>
									<td style="text-align:center;"><?php print($student->program); ?></td>
								</tr>
					<?php 
								$cnt++;
							}
						}
					?>
				</tbody>
			</table>
		</div>
