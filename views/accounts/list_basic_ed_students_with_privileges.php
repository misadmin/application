<?php 
	$with_priv_pdf=json_encode($with_privileges);
?>
<h2 class="heading">Basic Education Students With Privileges</h2>
<div style="border-radius: 5px; background-color: #EAEAEA; border: solid; border-width: 1px; 
	border-color: #D8D8D8; height: 30px; padding: 5px; text-align:right; margin-bottom:20px; vertical-align:middle;">
<div style="float:left; margin-top:7px; margin-left:20px;"> 
<?php if ($with_privileges) { ?>
	  <a class="download_scholar_list" href="" 
		  with_priv='<?php print($with_priv_pdf); ?>'><i class="icon-download-alt"></i>Download to PDF</a>
<?php } ?>	 
  </div>
<div style="float:right;">   
	<form id="change_data" method="POST">
				
					<select name="privilege_items_id" id="privilege_items_id" style="width: auto;">
					<?php 
						foreach($privilege_items AS $item) {
					?>
							<option value="<?php print($item->id); ?>" <?php if ($item->id == $selected_item) { print("selected"); } ?>  >
								<?php print($item->scholarship_code." - ".$item->scholarship); ?> </option>
					<?php 	
						}
					?>
					</select>
					
					<span style="padding-left:40px; ">Posted:
					<select name="posted" id="posted_id" style="width: auto;">
						<option value="All" <?php if ($posted == 'All') { print("selected"); } ?>>
								All</option>
						<option value="N" <?php if ($posted == 'N') { print("selected"); } ?>>
								No</option>
						<option value="Y" <?php if ($posted == 'Y') { print("selected"); } ?>>
								Yes</option>
					</select></span>
					
	</form>	
</div>					
</div>

<h3><?php print($privilege->scholarship); ?></h3>
<h4><?php print($academic_yr->sy)?></h4>

<div>
<?php
	if ($with_privileges) {
?>
  <table class="table table-bordered table-striped table-hover">
	<thead>
	<tr style="background-color: #EAEAEA; font-weight: bold;">
        <td style="text-align: center;"></td>
        <td style="text-align: center;">ID No.</td>
        <td style="text-align: center;">Name</td>
        <td style="text-align: center;">Level-Year</td>
        <td style="text-align: center;">Tuition Fee Discount %</td>
        <td style="text-align: center;">Total Discount</td>
        <td style="text-align: center;">Remarks</td>
        <td style="text-align: center;">POSTED</td>
        </tr>
    </thead>
    <?php
    	$x = 1;
		foreach ($with_privileges AS $priv) {
	?>
    <tr>
    	<td style="text-align: center;"><?php print($x); ?></td>
        <td style="text-align: center;"><?php print($priv->students_idno); ?></td>
        <td style="text-align: left;"><?php print($priv->lname.", ".$priv->fname); ?></td>
    	<td style="text-align: left;"><?php print($priv->stud_level); ?></td>
        <td style="text-align: right;"><?php print(($priv->discount_percentage * 100)."%"); ?></td>
        <td style="text-align: right;"><?php print(number_format($priv->discount_amount,2)); ?></td>
        <td style="text-align: left;"><?php print($priv->remark); ?></td>
        <td style="text-align: center;"><?php print($priv->posted); ?></td>
        </tr>
	<?php $x++;
	 } ?>

  </table>
<?php 
	} else {
?>
	<div style="font-size: 15px; font-weight: bold; margin-top: 10px;"><?php print("No Students availed on this scholarship!"); ?> </div>
<?php 	
	} 
?> 

</div>
  
 <form id="download_scholar_list_pdf" method="post" target="_blank">
    <input type="hidden" name="action" value="download_to_pdf"/>
    <input type="hidden" name="scholar_desc" value="<?php print($privilege->scholarship); ?>" />
    <input type="hidden" name="acad_yr" value="<?php print($academic_yr->sy); ?>" />
	<?php $this->common->hidden_input_nonce(FALSE); ?>
</form>

<script>
	$('.download_scholar_list').click(function(event){
		event.preventDefault(); 
		var with_priv = $(this).attr('with_priv');
			
		$('<input>').attr({
			type: 'hidden',
			name: 'with_priv',
			value: with_priv,
		}).appendTo('#download_scholar_list_pdf');
		$('#download_scholar_list_pdf').submit();
		
	});
</script>


<script>
	$('.privilege').click(function(event){
		event.preventDefault(); 
		var  privilege = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'privilege',
		    value: privilege,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>


<script>
	$(document).ready(function(){
		$('#privilege_items_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'change_data',
			}).appendTo('#change_data');
			$('#change_data').submit();
		});
	});
</script>


<script>
	$(document).ready(function(){
		$('#posted_id').bind('change', function(){
			$('<input>').attr({
				type: 'hidden',
				name: 'action',
				value: 'change_data',
			}).appendTo('#change_data');
			$('#change_data').submit();
		});
	});
</script>
