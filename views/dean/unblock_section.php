<style type="text/css">
table.head1 {
	border:solid;
	border-color:#e3e4e3;
	border-width:1px;
	font-family:Verdana, Geneva, sans-serif; 
	font-size:12px; 
	color:#666;
}
tr.head {
	font-size:14px;
	font-weight:bold;
	background:#ddffdd;
	color:#666666;
}
td.head {
	padding:8px;
}
table.inside {
	margin:5px;
	font-family:inherit;
	font-size:inherit;
}
select.form {
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:auto;
	height:auto;
}
</style>

<form action="<?php echo site_url('dean/assign_blocksection');?>" method="post">
  <?php echo validation_errors(); 
  ?>
  <?php $this->common->hidden_input_nonce(FALSE); ?>
  <input type="hidden" name="step" value="6" />
  <div style=" background:#FFF; ">
  <table align="center" cellpadding="0" cellspacing="0" style="width:35%; margin-top:10px;" class="head1">
  <tr class="head">
  <td colspan="3" class="head">
  UNBLOCK  SECTION
  </td>
  </tr>
  <tr>
    <td colspan="3">
  <table border="0" cellpadding="4" cellspacing="0" class="inside">
    <tr>
      <td width="118" align="left" valign="top">Academic Terms</td>
      <td width="5" align="left" valign="top">:</td>
      <td width="299"><?php print($academic_terms->term." ".$academic_terms->sy); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top">Course [Section]</td>
      <td align="left" valign="top">:</td>
      <td><?php print($offerings->course_code."[".$offerings->section_code."]"); ?></td>
    </tr>
    <tr>
      <td align="left" valign="top">Schedule</td>
      <td align="left" valign="top">:</td>
      <td><?php
  		foreach($offering_slot AS $slot) {
			print($slot->tym." ".$slot->days_day_code." [".$slot->room_no."]");
			print("<br>");
		}
  
  ?></td>
    </tr>
    <tr>
      <td valign="top">Assigned Block/s </td>
      <td valign="top">:</td>
      <td><span style="text-align:left; vertical-align:top; ">
        <?php 
				$block_names = $this->Offerings_Model->ListBlockOfferings($offerings->id);
				foreach($block_names AS $bname) {
					print($bname->block_name."<br>");
				}
			 ?>
      </span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="submit" name="button" id="button" value="Unblock Section!" class="btn btn-success" /></td>
    </tr>
  </table>
    </td>
  </tr>
    </table>
  <br />
  <br />
  <br />
  </div>    

</form>