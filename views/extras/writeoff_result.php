<style>
	table.tibolclass 
	th {vertical-align:middle; text-align : center; color:green; }
	td.center {text-align:center;} 
	td.right {text-align:right;} 
	tr.mismatched {color: #D66E6E;}
</style>

<div class="row-fluid span5">
<h4>Write-Off Result:</h4>
<table id="tibolid" class="table table-condensed table-striped table-bordered">
	<thead>
		<tr>
			<th colspan="2" class="center">ID No.</th>
			<th class="center">Student</th>
			<th class="center">Remark</th>
		</tr>
	</thead>
	<tbody>
<?php if (isset($result_message) && count($result_message)>0): 
	$num=0; 
?>
	<?php foreach ($result_message as $row): $num++ ?>  
	<tr>
		<td class="center"><?php echo $num; ?></td>
		<td class="center"><a href="<?php echo site_url($this->session->userdata('role') .  "/student/{$row['idno']}");?> "><?php echo $row['idno']; ?></a></td>
		<td><?php echo $row['student']; ?></td>		
		<td><?php echo $row['remark']; ?></td>		
	</tr>
	<?php endforeach; ?>
<?php else: ?>
	<tr><td class="center" colspan="4">You have not chosen any account to write-off.</td></tr>
<?php endif; ?>
	</tbody>
</table>
</div>
<script>
$(document).ready(function(){
	$("#tibolid").stickyTableHeaders();
    $("#tibolid").stickyTableHeaders('updateWidth');
});
</script>