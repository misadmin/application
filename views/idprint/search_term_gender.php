<div style="margin-top:10px">
	<h2 class="heading">Term and Gender</h2>
</div>

<form action="" method="post"  class="form-horizontal">
		<?php $this->common->hidden_input_nonce(FALSE); ?>
		<input type="hidden" name="action" value="2" />
	
	

	<fieldset>
		<div class="control-group formSep">
			<label class="control-label"> <b> Academic Term: </b> </label>
				<div class="controls">
					<select id="academic_terms" name="academic_terms">
					<?php
						foreach($academic_terms AS $academic_term) {
							print("<option value=".$academic_term->id.">".$academic_term->term." ".$academic_term->sy."</option>");	
						}
					?>
					</select>
				</div>
		</div>
		
		<div class="control-group formSep">
				<label class="control-label"> <b> Course: </b> </label>
					<div class="controls">
						<input type="radio" name="course" id="radio" placeholder="C" class="input-xlarge" value="C" checked="checked" /> CWTS
						<input type="radio" name="course" id="radio2" placeholder="M" class="input-xlarge" value="M" /> MS 12
					</div>
		</div>
		
		<div class="control-group formSep">
				<label class="control-label"> <b> Gender: </b> </label>
					<div class="controls">
						<input type="radio" name="gender" id="radio" placeholder="F" class="input-xlarge" value="F" checked="checked" /> F
						<input type="radio" name="gender" id="radio2" placeholder="M" class="input-xlarge" value="M" /> M
						<input type="radio" name="gender" id="radio3" placeholder="all" class="input-xlarge" value="" /> All
					</div>
		</div>
		<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit">View CWTS Graduates</button>
					</div>
		</div>	
	</fieldset>
</form>

