<?php

	class Tuition_Model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


	    function AddCollege_Tuition($data) 
		{
			foreach($data['academic_programs'] as $program)
			{
				$query = "INSERT INTO tuition_fees (id, academic_programs_id, academic_years_id, rate, yr_level) 
   						      VALUES ('',
									  {$this->db->escape($program->id)},
									  {$this->db->escape($data['academic_years_id'])},
									  {$this->db->escape($data['rate'])},
									  {$this->db->escape($data['yr_level'])})";
				$this->db->query($query);
									
			}						
			
			$id = @mysql_insert_id();
			//print($query);
			//die();
			return $id;
			
		}

		function AddGraduate_Tuition($data) 
		{
			$query = "INSERT INTO tuition_fees (id, academic_programs_id, academic_years_id, rate, yr_level) 
   				      VALUES ('',
						  	  {$this->db->escape($data['programs_id'])},
					  	  	  {$this->db->escape($data['academic_years_id'])},
						      {$this->db->escape($data['rate'])},
						      {$this->db->escape($data['yr_level'])})";
				
			$this->db->query($query);					
				
			$id = @mysql_insert_id();
			
			return $id;
		}
		
//Added: 2/20/2013
		
	function AddNew_OtherTuition_Item($item_name){
	  // print_r($data);
	  // die();
		$query = "INSERT INTO courses_groups (id, group_name) 
   		      		VALUES ('',
				  			{$this->db->escape($item_name)})";
		$this->db->query($query);
		$id = @mysql_insert_id();
		//print($id);
		//die();
		$query = "INSERT INTO other_tuition_fee_items (id, courses_groups_id) 
   		      		VALUES ('',
				  			'".$id."')";		
	
		$this->db->query($query);						  
		$id = @mysql_insert_id();		
		return $id;
	}	

//Added: 2/20/2013
	function AddNew_Misc_Item($item_name){
	
		$query = "INSERT INTO misc_items (id, description) 
   		      		VALUES ('',
				  			{$this->db->escape($item_name)})";
		$this->db->query($query);						  
		$id = @mysql_insert_id();		
		return $id;
	}		  

//Added: 2/20/2013
	function AddNew_OtherFee_Item($item_name){
		$query = "INSERT INTO other_fee_items (id, description) 
   		      		VALUES ('',
				  			{$this->db->escape($item_name)})";
		$this->db->query($query);						  
		$id = @mysql_insert_id();		
		return $id;
	}

//Added: 2/21/2013
	function AddNew_AddOtherFee_Item($item_name){
		$query = "INSERT INTO additional_other_fee_items (id, description) 
   		      		VALUES ('',
				  			{$this->db->escape($item_name)})";
		$this->db->query($query);						  
		$id = @mysql_insert_id();		
		return $id;
	}	
	
//Added: 2/23/2013
    	
	function ListTuitionFees($data) {
			$result = 0;
			$q = "SELECT 
						a.id,
						b.description, 
						b.abbreviation,
						a.yr_level,
						a.rate, 
						a.posted					
					FROM
						tuition_fees AS a, academic_programs as b
					WHERE 
					    b.colleges_id = {$this->db->escape($data[college_id])}
						AND a.academic_programs_id = b.id
						AND a.academic_years_id = {$this->db->escape($data[academic_years_id])}
					ORDER BY
					    b.description, a.yr_level";
			
			//print($q);
		//	die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
		
		}
		
		function getFee($fee_id) 
		{
			$result = null;
			
			$q = "SELECT a.rate, a.yr_level, a.academic_years_id, b.description 
					FROM
						tuition_fees as a, academic_programs as b, academic_years as c
					WHERE
						a.academic_programs_id = b.id
						AND a.id = {$this->db->escape($fee_id)} "; 
			
				
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}	
		
	//Added: 3/1/2013
	function UpdateTuitionFee($id, $tf){
	$q = "UPDATE tuition_fees 
			SET 
				rate={$this->db->escape($tf)} 
			WHERE
				id={$this->db->escape($id)}";
			
		if ($this->db->query($q)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
	
	
	function get_tuition_fee($history_id) 
		{
			$result = null;
			
			$q = "SELECT a.rate
					FROM
						fees_schedule as a,
						fees_subgroups as b,
						fees_groups as c, 
						student_histories as d, 
						prospectus as e, 
						academic_programs as f
					WHERE
						d.id = {$this->db->escape($history_id)} 
						AND d.academic_terms_id = a.academic_terms_id
						AND d.year_level = a.yr_level
						AND a.fees_subgroups_id = b.id
						AND b.fees_groups_id = c.id
						AND c.fees_group = 'Tuition Basic'
						AND d.prospectus_id = e.id
						AND e.academic_programs_id = f.id
						AND f.acad_program_groups_id = a.acad_program_groups_id"; 
			
				
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
			
		}	
		
		//Added: Feb. 8, 2014 by Amie
		function getAssessmentDate($idno, $acadterm)
		{
			$result = null;
				
			$q = "SELECT a.transaction_date, sh.academic_terms_id
					FROM assessments a, student_histories sh
					WHERE a.student_histories_id = sh.id
  						AND sh.students_idno = {$this->db->escape($idno)}
  						AND sh.academic_terms_id = {$this->db->escape($acadterm)} ";

			$query = $this->db->query($q);
								
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			}
			
			return $result;
			
		}
	
}

//end of tuition_model.php