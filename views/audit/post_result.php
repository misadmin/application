<style>
	.table th {text-align:center; vertical-align:middle;}
	td.right {text-align:right}
	td.center {text-align:center}
</style>

<?php
	//$errors = ['idno'=>'1234','student'=>'Juan dela cruz','program'=>'ab philo','yr_level'=>'4'];
	//$errors = [];
?>
<div class="row-fluid span7">

<?php 
	//print_r($errors);die();
	
	if ( is_array($errors) && count($errors) > 0){
?>

	<h3> List of Students with errors in posting:</h3>
	<hr>
	<table id="post_result" class="table table-bordered table-striped table-condensed">
	    <thead>
			<tr>
				<th width="20%">ID No</th>
				<th width="40%">Student</th>
				<th width="25%">Program</th>
				<th width="15%">Yr Level</th>      
			</tr>
	    </thead>
	
	<?php
		$cnt=0; 
		foreach ($errors AS  $error) {
	?>
 		<tr> 
			<td class="center"><a href="<?php echo site_url("audit/student/{$error['idno']}"); ?> "> <?php echo $error['idno']; ?></a></td> 
			<td><?php echo $error['student']; ?></td> 
			<td><?php echo $error['program']; ?></td> 
			<td class="center"><?php echo $error['yr_level']; ?></td> 
		
<?php 		
		}		
?>
	</table>

<?php 
	}else{
		if ($students_exist===FALSE)
			$this->content_lib->set_message('No student to assess or all have already been assessed!','alert-error');
		elseif (isset($assessed_count))
			$this->content_lib->set_message($assessed_count . ' student' .  ($assessed_count > 1 ? 's':''.'') . ' successfully posted!','alert-success');
	}
?>    

</div>
<script>
$(document).ready(function(){
	var oTable = $("#post_result").dataTable({
		"iDisplayLength": 100,
		"aLengthMenu": [[50, 100, 1000, -1], [50, 100, 1000, "All"]],
	});

	oTable.fnSort( [ [1,'asc'], ] );
	

});	

</script>
