<div class="row-fluid">
<?php if(isset($message)): ?>
	<div class="alert alert-<?php echo $severity; ?>">
		<a class="close" data-dismiss="alert">&times;</a>
		<?php echo $message; ?>
	</div>
<?php endif; ?>
	<form id="new_student" method="post" action="" class="form-horizontal">
		<div class="formSep">
			<h3>Create a New Student Account</h3>
		</div>
	
	<?php echo $this->common->hidden_input_nonce(); ?>
		<fieldset>
			<div class="control-group formSep">
				<label for="firstname" class="control-label">First Name: </label>
				<div class="controls">
					<input type="text" name="firstname" id="firstname" placeholder="First Name" class="input-xlarge" value="<?php if(isset($firstname)) echo $firstname; ?>" />
				</div>
			</div>
			<div class="control-group formSep">
				<label for="familyname" class="control-label">Family Name: </label>
				<div class="controls">
					<input type="text" name="familyname" id="familyname" placeholder="Family Name" class="input-xlarge"  value="<?php if(isset($familyname)) echo $familyname; ?>" />
				</div>
			</div>
				<div class="control-group formSep">
					<label for="middlename" class="control-label">Middle Name: </label>
					<div class="controls">
						<input type="text" name="middlename" id="middlename"  placeholder="Middle Name" class="input-xlarge"  value="<?php if(isset($middlename)) echo $middlename; ?>" />
					</div>
				</div>
				<div class="control-group formSep">
					<label for="college" class="control-label">College: </label>
					<div class="controls">
						<select id="colleges" name="college">
							<option value="">-- Select College --</option>
<?php foreach ($colleges as $college): ?>
							<option value="<?php echo $college->id; ?>"><?php echo $college->name; ?></option>
<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="control-group formSep">
					<label for="programs" class="control-label">Program: </label>
					<div class="controls">
						<select id="programs" name="program">
							<option value="">-- Select Program --</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Add New Student</button>
					</div>
				</div>	
		</fieldset>
	</form>
</div>
<script>
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Unacceptable values found"
);

$('#new_student').validate({
	onkeyup: false,
	errorClass: 'error',
	validClass: 'valid',
	rules: {
		firstname: { required: true },
		familyname: { required: true },
		college: { required: true },
		program: { required: true }
	},
	highlight: function(element) {
		$(element).closest('div').addClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	unhighlight: function(element) {
		$(element).closest('div').removeClass("f_error");
		setTimeout(function() {
			boxHeight()
		}, 200)
	},
	
	errorPlacement: function(error, element) {
		$(element).closest('div').append(error);
	}
});

$("#firstname").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
$("#familyname").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });
$("#middlename").rules("add", { regex: "^[\u00F1\u00D1A-Za-z ']*$" });

$('#colleges').change(function(){
	var college_id = $('#colleges option:selected').val();
	
	$.ajax({
		url: "<?php echo site_url("ajax/programs"); ?>/?college=" + college_id,
		dataType: "json",
		success: function(data){
			var doptions = make_options(data);
			$('#programs').html(doptions);
		}	
	});	
});

function make_options (data){
	var doptions = '<option value="">-- Select Program --</option>';
	for (var i = 0; i < data.length; i++) {
		doptions = doptions 
		 	+ '<option value="'
		 	+ data[i].id
		 	+ '">'
		 	+ data[i].abbreviation
		 	+ '</option>';
	}
	return doptions;
}
</script>