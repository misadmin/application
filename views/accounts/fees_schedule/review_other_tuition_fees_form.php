
<div class="row-fluid">
	<div class="span12">
		<div class="form-horizontal">
			<input type="hidden" name="step" value="2">
			<div class="control-group formSep">
				<div class="control-label">School Year</div>
				<div class="controls">
					<?php 
						print($academic_year);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Semester Term</div>
				<div class="controls">
					<?php 
						$academic_terms = json_encode($terms);
						foreach($terms AS $k => $v) {
							$term = $this->Academicyears_model->getAcademicTerms($v);
							print($term->term."<br>");
						}
						
					?>													
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Course Code</div>
				<div class="controls">
					<?php
						$courses = $this->courses_model->getCourseDetails($courses_id);
						print($courses->course_code." ".$courses->descriptive_title);
					?>
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label">Year Level</div>
				<div class="controls">
					<?php 
						$y_levels = json_encode($year_levels);
						foreach($year_levels AS $k => $v) {
						$ordinal =date( 'S' , mktime( 1 , 1 , 1 , 1 , $v ) );
						print($v.$ordinal." Year <br>");
						}
					?>										
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label" >Amount</div>
				<div class="controls" ><?php print(number_format($rate,2));?>			
				</div>
			</div>
			<div class="control-group formSep">
				<div class="control-label"></div>
				<div class="controls">
					<button class="btn btn-danger" id="back_to_fees" ><< Back to Other Tuition Fees</button>
					<button class="btn btn-warning" id="save_fees" courses_id="<?php print($courses_id); ?>" 
						year_levels='<?php print($y_levels); ?>' 
						academic_terms='<?php print($academic_terms); ?>' 
						rate="<?php print($rate); ?>">Save Other Tuition Fees >></button>
				</div>
			</div>
			</div>				
	</div>
</div>

 <form id="back_to_other_tuition_fees_form" method="post">
  <input type="hidden" name="step" value="1" />
</form>

<script>
	$('#back_to_fees').click(function(event){
		event.preventDefault(); 
		
		$('#back_to_other_tuition_fees_form').submit();
	});
</script>


 <form id="save_fees_sched_form" method="post">
  <input type="hidden" name="step" value="3" />
</form>

<script>
	$('#save_fees').click(function(event){
		event.preventDefault(); 
		var courses_id = $(this).attr('courses_id');
		var year_levels = $(this).attr('year_levels');
		var academic_terms = $(this).attr('academic_terms');
		var rate = $(this).attr('rate');
		
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'courses_id',
		   	value: courses_id,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'year_levels',
		   	value: year_levels,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'academic_terms',
		   	value: academic_terms,
		}).appendTo('#save_fees_sched_form');
		$('<input>').attr({
		   	type: 'hidden',
		   	name: 'rate',
		   	value: rate,
		}).appendTo('#save_fees_sched_form');
		$('#save_fees_sched_form').submit();
	});
</script>



