<div class="fluid span6">
<table class="table table-bordered table-striped mediaTable activeMediaTable">
	<thead>
		<tr>
			<th>Exam</th>
			<th>Exam Description</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
<?php 
if (is_array($exams) && count($exams) > 0): ?>
	<?php foreach ($exams as $exam): ?>
		<tr>
			<td id="abbr<?php echo $exam->id; ?>"><a title="Click to edit this examination type" id="<?php echo $exam->id; ?>" href="javascript://" class="exam_edit"><?php echo $exam->abbreviation; ?></a></td>
			<td id="description<?php echo $exam->id; ?>"><?php echo $exam->description; ?></td>
			<td id="status<?php echo $exam->id; ?>"><?php echo $exam->status; ?></td>
		</tr>
	<?php endforeach; ?>
<?php else: ?>
	<tr><td colspan="3">No results Found</td></tr>
<?php endif;?>
	</tbody>
</table>
<div style="margin-top: 20px;" id="edit_exam_div">
	<h4 class="heading">Examination Edit Form <small class="pull-right" id="close_form"><a href="javascript://">&times;</a></small></h4>
	<form id="edit_exam_form" method="post" action="" class="form-horizontal">
<?php $this->common->hidden_input_nonce(FALSE); ?>
	<input type="hidden" name="action" value="edit_exam" />
	<input type="hidden" id="exam_id" name="id" value="" />
		<fieldset>
			<div class="control-group formSep">
				<label for="edit_exam_abbreviation" class="control-label">Exam Abbreviation</label>
				<div class="controls">
					<input type="text" id="edit_exam_abbreviation" name="edit_exam_abbreviation" class="input-xlarge" value=""/>
				</div>
			</div>
			<div class="control-group formSep">
				<label for="edit_exam_description" class="control-label">Exam Description</label>
				<div class="controls">
					<textarea id="edit_exam_description" name="edit_exam_description" class="input-xlarge"></textarea>
				</div>
			</div>
			<div class="control-group formSep">
				<label for="edit_exam_description" class="control-label">Status</label>
				<div class="controls">
					<select name="status" id="edit_exam_status">
						<option value="active">Active</option>
						<option value="inactive">Inactive</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-primary" type="submit">Update This Exam</button>
				</div>
			</div>
		</fieldset>
</form>
</div>
</div>
<script>
	$(document).ready(function(){
		$('#edit_exam_div').hide();
		$('.exam_edit').click(function(){
			var id = $(this).attr('id');
			var abbreviation = $('#abbr'+id).text();
			var description = $('#description'+id).text();
			var status = $('#status'+id).text();
			
			$('#edit_exam_abbreviation').val(abbreviation);
			$('#exam_id').val(id);
			$('#edit_exam_description').val(description);
			$('#edit_exam_status').val(status);
			$('#edit_exam_div').show();
		});
		$('#close_form').click(function(){
			$('#edit_exam_div').hide();
		});
	});
</script>