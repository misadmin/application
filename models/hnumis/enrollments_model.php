<?php 

	class Enrollments_model extends CI_Model {
	
  		function __construct() {
        	parent::__construct();
		
   		}


		function getEnrollmentSchedule($academic_terms_id,$yr_level) {
			
			$result = null;
			
			$q1 = "SELECT 
					a.start_date,   
					a.end_date,
					YEAR(a.end_date) AS yy,
					MONTH(a.end_date) AS mm,
					DAYOFMONTH(a.end_date) AS dd					
						FROM
							enrollment_schedules AS a
						WHERE
							a.academic_terms_id={$this->db->escape($academic_terms_id)}
							AND	a.yr_level = {$this->db->escape($yr_level)} ";
			//print_r($q1);die();
			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			return $result;
		}
		
		/**
		 * Fetch the class record given the course_offering_id
		 * 
		 * Added by Justin 
		 * @param int $course_offering_id
		 *
		 *query modified by tatskie 2013-02-02
		 *EDITED: 7/25/13 by genes
		 *added credit_units
		 */
		
		function class_record ($course_offering_id){
			$sql = "
					SELECT
						e.*, 
						upper(s.lname) as lname, cap_first(s.fname) as fname, cap_first(s.mname) as mname,
						s.idno,
						sh.year_level as year_level,
						c.descriptive_title,
						c.course_code,
						if(!isnull(eo.id), eo.credit_units, c.credit_units) as credit_units,						
						co.section_code,
						ap.abbreviation as academic_program
					FROM
						enrollments e
					LEFT JOIN
						student_histories sh
					ON
						e.student_history_id=sh.id
					LEFT JOIN enrollments_override eo ON eo.enrollments_id=e.id						
					LEFT JOIN
						students s
					ON
						s.idno=sh.students_idno
					LEFT JOIN
						course_offerings co
					ON
						co.id=e.course_offerings_id
					LEFT JOIN
						courses c
					ON
						c.id=co.courses_id
					LEFT JOIN
						prospectus p
					ON
						p.id=sh.prospectus_id
					LEFT JOIN
						academic_programs ap
					ON
						ap.id=p.academic_programs_id
					WHERE
						e.course_offerings_id='{$course_offering_id}'
					GROUP BY
						s.idno
					ORDER BY 
						s.lname, s.fname
			";
			//print_r($sql);die();
			$query = $this->db->query($sql);
			if ($query and $query->num_rows() > 0)
				return $query->result(); else
				return FALSE;
		}
		
		//Added: April 3, 2013 by Amie
		//EDITED: 8/13/14 by genes 
		//Added data for dissolution after posting
		function list_enrollees($offering_id) {
			
			$result = null;
			
			$q = "
					SELECT 
						p.id, 
						s.lname,
						sh.id AS student_histories_id,
						b.acad_program_groups_id,
						sh.academic_terms_id,
						sh.year_level,
						e.id AS enrollments_id,
						b.id as academic_programs_id
					FROM
						enrollments e 
							LEFT JOIN student_histories sh ON e.student_history_id=sh.id
							LEFT JOIN students s ON	s.idno=sh.students_idno
							LEFT JOIN payers p ON p.students_idno = s.idno
							LEFT JOIN prospectus a ON a.id=sh.prospectus_id
							LEFT JOIN academic_programs AS b ON b.id=a.academic_programs_id
					WHERE
						e.course_offerings_id={$this->db->escape($offering_id)}
						AND e.status = 'Active'
					ORDER BY s.lname
				";
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 

		//$this->db->trans_rollback();print_r($result); die();
			return $result;
			
		}

		//Added: May 2, 2013 by Amie
		function list_failures($offering_id) {
			$result = null;
			
			$q = "SELECT s.idno, CONCAT(s.lname, ', ', s.fname) as name, e.finals_grade
					FROM enrollments e
					LEFT JOIN student_histories sh ON e.student_history_id=sh.id
					LEFT JOIN students s ON s.idno=sh.students_idno
					LEFT JOIN payers p ON p.students_idno = s.idno
					WHERE
						e.course_offerings_id={$this->db->escape($offering_id)}
						AND e.finals_grade IN ('WD', 'INC', 'INE', 'NC', 5.0,'IP')
					ORDER BY s.lname";

			$query = $this->db->query($q);
			if ($query and $query->num_rows() > 0)
				return $query->result(); else
				return FALSE;
		}
		/**
		 * Returns the grades of the student for all terms
		 * 
		 * @param unknown_type $student_id
		 */
		function student_grades ($student_id){
			//if ((pc.is_bracketed = 'N') OR (pc.is_bracketed IS NULL),'N','Y') AS is_bracketed,
				
			$sql = "
				SELECT
					e.id AS enrollments_id, sh.id, sh.year_level,
					CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term,
					e.prelim_grade, e.midterm_grade, e.finals_grade,
					c.course_code, co.section_code, c.id as courseid, c.descriptive_title, 
					if(!isnull(eo.id),eo.credit_units,if(!(e.finals_grade REGEXP '^(NA|DR|INC|WD|NC|null|IP|[1-2].[0-9]|3.0|5.0)$') or e.finals_grade is null, '0.0', c.credit_units)) as credit_units,
					ap.abbreviation,
					if(!isnull(eo.id), eo.is_bracketed, ifnull(pc.is_bracketed,'N')) AS is_bracketed,
					CONCAT(emp.lname, ', ', emp.fname) AS instructor
				FROM
					enrollments e
				LEFT JOIN
					student_histories sh
				ON
					sh.id=e.student_history_id
				LEFT JOIN
					course_offerings co
				ON
					co.id=e.course_offerings_id
				LEFT JOIN
					courses c
				ON
					c.id=co.courses_id
				LEFT JOIN
				    prospectus_courses as pc
				ON
				    c.id = pc.courses_id    	
				LEFT JOIN
					academic_terms at
				ON
					at.id=sh.academic_terms_id
				LEFT JOIN
					academic_years ay
				ON
					ay.id=at.academic_years_id
				LEFT JOIN
					employees emp
				ON
					emp.empno=co.employees_empno
				LEFT JOIN
					prospectus p
				ON
					p.id=sh.prospectus_id
				LEFT JOIN
					academic_programs ap
				ON
					p.academic_programs_id=ap.id
				LEFT JOIN enrollments_override eo ON eo.enrollments_id=e.id
				WHERE
					sh.students_idno='{$student_id}'
				GROUP BY 
					at.id,co.courses_id,co.section_code
				ORDER BY "
				.($this->session->userdata('empno') =='404' ? "ay.end_year, at.term, course_code" : "ay.end_year DESC, at.term DESC, course_code")
			;															
			
			//print_r($sql);die();
			
			$query = $this->db->query($sql);
			if ($query && count($query->num_rows() > 0)){
				
				$academic_terms = array();
				foreach ($query->result() as $row) {
					
					if (array_key_exists($row->id, $academic_terms)){
						$academic_terms[$row->id]['courses'][] = array("course"=>$row);
					} else {
						$academic_terms[$row->id] = array(
											"school_year"	=>$row->sy,
											"term"			=>$row->term,
											"program"		=>$row->abbreviation,
											"year_level"	=>$row->year_level,
											"courses"		=>array(array("course"=>$row,)),
								);
					}
				}
				return $academic_terms;
			} else {
				return FALSE;
			}
		}
		
		function CountNumTaken($students_idno, $courses_id, $prospectus_id) {
			
			$q1 = "SELECT 
						a.id 
					FROM
						enrollments AS a,
						student_histories AS b,
						course_offerings AS c
					WHERE
						a.student_history_id=b.id
						AND a.course_offerings_id=c.id
						AND b.students_idno={$this->db->escape($students_idno)}
						AND c.courses_id={$this->db->escape($courses_id)}
						AND b.prospectus_id={$this->db->escape($prospectus_id)}
						AND a.finals_grade IN ('5.0','DR','WD','NA','NC','IP') ";
			$query = $this->db->query($q1);

			//print($q1);
			//die();

			return $query->num_rows();
		}
		
		//ADDED: 2/15/13
		function ElectiveEnrolled($students_idno, $prospectus_courses_id) {
			$result = null;
			
			$q = "SELECT 
						a.id
					FROM 
						enrollments AS a,
						course_offerings AS b,
						elective_courses AS c,
						student_histories AS d
					WHERE 
						a.course_offerings_id=b.id
						AND b.courses_id=c.topic_courses_id
						AND d.id=a.student_history_id
						AND d.students_idno = {$this->db->escape($students_idno)}
						AND c.prospectus_courses_id = {$this->db->escape($prospectus_courses_id)} ";

			//print($q);
			//die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		}
		
	
	//Added: FEb. 16, 2013 by Amie
		function getPassedCourses($students_idno, $prospectus_id) {
			$result = null;
			$grade = "('5.0','NG','INC','DR','NA','WD','INE','IP')";
			//print ($prospectus_id);
			//die();
			$q = "SELECT 
						c.course_code, 
						c.descriptive_title,
						c.credit_units,
						a.finals_grade,
						a.id as enrollments_id,
						g.prospectus_id,
						co.courses_id
				FROM
						enrollments a,
						course_offerings co,
						courses c,
						student_histories as g
				 WHERE 
						a.course_offerings_id = co.id
						AND co.courses_id = c.id
						AND a.student_history_id = g.id
						AND g.students_idno = {$this->db->escape($students_idno)}
						AND a.student_history_id IN (SELECT 
														MAX(ff.student_history_id)
													FROM
														course_offerings AS ee,
														enrollments AS ff,
														student_histories AS gg
													WHERE
														ee.id=ff.course_offerings_id
														AND gg.id=ff.student_history_id
														AND gg.students_idno = {$this->db->escape($students_idno)}
														AND ee.courses_id=co.courses_id
													GROUP BY
														ee.courses_id, 
														gg.students_idno, 
														gg.prospectus_id)
						AND a.finals_grade NOT IN $grade 
						AND a.finals_grade	IS NOT NULL
						AND c.id NOT IN ( SELECT cc.id
												FROM
													courses cc,
													prospectus_courses pc,
													prospectus_terms pt
												WHERE
													pc.courses_id = cc.id
													AND pc.prospectus_terms_id = pt.id
													AND pt.prospectus_id = {$this->db->escape($prospectus_id)}) 
				ORDER BY
					c.course_code ";
			//print($q); die();		
			$query = $this->db->query($q);
			return ($query  ? $query->result() : FALSE );
		}
		
		function extend_student_enrollment_schedule ($student_id){
			
			$this->load->model('hnumis/academic_terms_model');
			$academic_term = $this->academic_terms_model->current_academic_term($this->academic_term_for_enrollment_date());
			$sql = "
				SELECT
					id
				FROM
					student_histories
				WHERE
					students_idno='{$student_id}'
				AND
					academic_terms_id='{$academic_term->id}'
				LIMIT 1
			";
			
			$query = $this->db->query($sql);
			if ($query && $query->num_rows() > 0) {
				$histories_id_obj = $query->row();
				$histories_id = $histories_id_obj->id; 
			} else
				return FALSE;
			$sql = "
				UPDATE 
					student_histories
				SET
					extension_end= DATE_ADD(CURDATE(), INTERVAL 1 DAY)
				WHERE
					id={$this->db->escape($histories_id)}
				";
			
			return $this->db->query($sql);
		}

		
		function DeleteStudents($course_offerings) {
			$q = "DELETE FROM 
						enrollments 
					WHERE
						course_offerings_id IN $course_offerings ";
			
			//print($q); die();
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}			
		}
		
		/*
		 * @ADDED: 8/13/14 
		 * @author: genes
		 */
		/*function DeleteEnrollments($enrollments_id) {
			$q = "DELETE FROM
						enrollments
					WHERE
						id={$this->db->escape($enrollments_id)}";

			//print($q); die();
			if ($this->db->query($q)) {
				return TRUE;
			} else {
				return FALSE;
			}
				
		}*/

		
	function getCourseInfo($enrollment_id) {
			
			$result = null;
			
			$q1 = "SELECT a.finals_grade, 
					b.descriptive_title,
					b.course_code
					FROM
				 		enrollments AS a,
						course_offerings AS c,
						courses AS b
					WHERE
						a.course_offerings_id = c.id 
						AND c.courses_id = b.id
						AND a.id = {$this->db->escape($enrollment_id)} ";
		
			$query = $this->db->query($q1);

			//print($q1);
			//die();

			if($query->num_rows() > 0){
				$result = $query->row();
			} 
			return $result;
		}
		
	function get_enrolled_courses ($student_histories_id){
		$sql = "
			SELECT
				lpad(c.`id`,5,'0') as id, c.`course_code`, c.`courses_groups_id`, c.`course_types_id`, c.`owner_colleges_id`, c.`descriptive_title`, c.`paying_units`, c.`credit_units`, c.`is_service_course`, c.`inserted_by`, c.`inserted_on`, c.`updated_by`, c.`updated_on`, c.`remark`,				
				TRIM(ct.type_description) as type_description,
				reen.id AS re_enrollments_id,
				e.withdrawn_on, 
				l.assessments_id as assessment_id,
				'' as enrollments_history_id,				 
				e.post_status,
				asses.transaction_date,'current' as enrolled_status 
			FROM courses c
			LEFT JOIN course_offerings co ON c.id=co.courses_id
			LEFT JOIN enrollments e ON e.course_offerings_id=co.id
			LEFT JOIN course_types ct ON ct.id=c.course_types_id
			LEFT JOIN re_enrollments AS reen ON reen.enrollments_id = e.id
			LEFT JOIN assessments AS asses ON e.student_history_id = asses.student_histories_id
			LEFT JOIN ledger l ON l.assessments_id = asses.id
			WHERE
				e.student_history_id={$this->db->escape($student_histories_id)}

			UNION
						
			select 
				lpad(c.`id`,5,'0') as id, c.`course_code`, c.`courses_groups_id`, c.`course_types_id`, c.`owner_colleges_id`, c.`descriptive_title`, 0.00 as `paying_units`, 0.00 as  `credit_units`, c.`is_service_course`, c.`inserted_by`, c.`inserted_on`, c.`updated_by`, c.`updated_on`, c.`remark`,
				TRIM(ct.type_description) as type_description,
				'' AS re_enrollments_id,
				'' as withdrawn_on, 
				'' as assessment_id,
				eh.status as enrollments_history_id,				 
				eh.status as post_status,
				eh.inserted_on as transaction_date,'deleted' as enrolled_status
			from enrollments_history eh 
			join course_offerings co2 on co2.id = eh.course_offerings_id and eh.student_history_id = {$this->db->escape($student_histories_id)}
			join courses c on c.id = co2.courses_id
			join course_types ct ON ct.id = c.course_types_id
		
			";
		
		//print($sql); die();
		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->result();
		} else{
			return FALSE;
		}
	}
	
	function student_grades_given_academic_term ($student_id, $academic_term){
		/*sh.id, sh.year_level,
			CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term,
			e.prelim_grade, e.midterm_grade, ,
			c.course_code, c.id as courseid, c.descriptive_title, ,
			ap.abbreviation,
			CONCAT(emp.lname, ', ', emp.fname) AS instructor
		*/
			
		$sql = "
		SELECT c.course_code, e.finals_grade, if(!isnull(eo.id), eo.credit_units, c.credit_units) as credit_units
		FROM
			enrollments e
		LEFT JOIN enrollments_override eo ON eo.enrollments_id=e.id
		LEFT JOIN
			student_histories sh
		ON
			sh.id=e.student_history_id
		LEFT JOIN
			course_offerings co
		ON
			co.id=e.course_offerings_id
		LEFT JOIN
			courses c
		ON
			c.id=co.courses_id
		LEFT JOIN
			academic_terms at
		ON
			at.id=sh.academic_terms_id
		LEFT JOIN
			academic_years ay
		ON
			ay.id=at.academic_years_id
		LEFT JOIN
			employees emp
		ON
			emp.empno=co.employees_empno
		LEFT JOIN
			prospectus p
		ON
			p.id=sh.prospectus_id
		LEFT JOIN
			academic_programs ap
		ON
			p.academic_programs_id=ap.id
		WHERE
			sh.students_idno='{$student_id}'
		AND
			sh.academic_terms_id = '{$academic_term}'";

//		AND (c.course_code NOT LIKE ('CWTS%') OR c.course_code NOT LIKE ('ROTC%'))";
		

		$query = $this->db->query($sql);
		if($query && $query->num_rows() > 0){
			return $query->result();
		} else{
			return FALSE;
		}
	}


	//Added: 4/10/2013 by genes
	//EDITED: 10/14/13 by genes
	/*Enrollments Summary counts all students 
	 * with 1 subject enrolled even if 
	 * the subject is already withdrawn
	 */ 
	function ListEnrollmentSummary($colleges_id, $academic_terms_id) {
			
			$result = null;
			
			$q1 = "SELECT
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='M'
								AND bb.year_level=1
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS m1, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='F'
								AND bb.year_level=1
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS f1, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='M'
								AND bb.year_level=2
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS m2, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='F'
								AND bb.year_level=2
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS f2, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='M'
								AND bb.year_level=3
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS m3, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='F'
								AND bb.year_level=3
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS f3, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='M'
								AND bb.year_level=4
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS m4, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='F'
								AND bb.year_level=4
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS f4, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='M'
								AND bb.year_level=5
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS m5, 
						(SELECT count(DISTINCT(aa.idno))
							FROM
								students AS aa,
								student_histories AS bb,
								enrollments AS cc,
								prospectus AS dd,
								academic_programs AS ee
							WHERE
								aa.idno = bb.students_idno
								AND bb.id = cc.student_history_id
								AND bb.prospectus_id = dd.id
								AND dd.academic_programs_id = ee.id
								AND aa.gender='F'
								AND bb.year_level=5
								AND bb.academic_terms_id = {$this->db->escape($academic_terms_id)}
								AND ee.id = e.id)
						AS f5, 
						e.abbreviation,
						e.id
					FROM
						students AS a,
						student_histories AS b,
						enrollments AS c,
						prospectus AS d,
						academic_programs AS e
					WHERE 
						a.idno = b.students_idno
						AND b.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND b.id=c.student_history_id
						AND e.colleges_id = {$this->db->escape($colleges_id)}
					GROUP BY
						e.id";
		
			//print($q1);die();

			$query = $this->db->query($q1);

			if($query->num_rows() > 0){
				$result = $query->result();
			} 
			return $result;
	}
			
		
	//EDITED: 5/3/2013 by tata
	function ListEnrollmentSummary_tata($colleges_id, $academic_terms_id) {
		
		$q1 = "
				SELECT 
						SUM(st.gender='M' && sh.year_level = 1) as m1, 
						SUM(st.gender='F' && sh.year_level = 1) as f1, 
						SUM(st.gender='M' && sh.year_level = 2) as m2, 
						SUM(st.gender='F' && sh.year_level = 2) as f2, 
						SUM(st.gender='M' && sh.year_level = 3) as m3, 
						SUM(st.gender='F' && sh.year_level = 3) as f3, 
						SUM(st.gender='M' && sh.year_level = 4) as m4, 
						SUM(st.gender='F' && sh.year_level = 4) as f4, 
						SUM(st.gender='M' && sh.year_level = 5) as m5, 
						SUM(st.gender='F' && sh.year_level = 5) as f5,
						ap.abbreviation,ap.id
				FROM 
						academic_programs ap
						join prospectus        pr on pr.academic_programs_id = ap.id 
						join student_histories sh on sh.prospectus_id = pr.id 
						join students          st on st.idno = sh.students_idno
				WHERE 
						sh.academic_terms_id = {$this->db->escape($academic_terms_id)}
						and ap.colleges_id = {$this->db->escape($colleges_id)}
						-- and sh.totally_withdrawn != 'Y'
						and sh.id in (select distinct en.student_history_id from enrollments en) 
				GROUP BY 
					ap.id 
			";					
			$query = $this->db->query($q1);
			return ($query ? $query->result() : FALSE);
		}
		
		//Added: May 7, 2013 by Amie
		//used to generate masterlist for a given term and by program per year level
		//edited to contain course-year
		//EDITED: 10/10/13 by genes
		//checks if total withdraw is included
		//EDITED: 11/23/14 for conformity to modal data
		function masterlist($academic_terms_id, $program, $yearlevel=NULL,$include_total_wdraw=FALSE) {
			$result = null;
			
			if (!$include_total_wdraw) {
				$q1 = "SELECT s.idno, CONCAT(s.lname,', ',s.fname,' ',s.mname) as name, gender, 
							CONCAT(ap.abbreviation,'-',sh.year_level) AS course_yr,
							CONCAT(s.lname,', ',s.fname,' ',s.mname) as neym,
							ap.abbreviation,
							sh.year_level
						FROM academic_programs ap
							join prospectus        pr on pr.academic_programs_id = ap.id 
							join student_histories sh on sh.prospectus_id = pr.id 
							join students          s on s.idno = sh.students_idno
						WHERE 
							sh.academic_terms_id = {$this->db->escape($academic_terms_id)}
							and ap.id = {$this->db->escape($program)}
							and sh.totally_withdrawn = 'N'
							and sh.id in (select distinct en.student_history_id from enrollments en)";
			} else {
				$q1 = "SELECT 
							s.idno, 
							CONCAT(s.lname,', ',s.fname,' ',s.mname) as name, 
							gender,
							CONCAT(ap.abbreviation,'-',sh.year_level) AS course_yr,
							CONCAT(s.lname,', ',s.fname,' ',s.mname) as neym,
							ap.abbreviation,
							sh.year_level
						FROM 
							academic_programs ap,
							prospectus pr,
							student_histories sh,
							students s
						WHERE	 
							pr.academic_programs_id = ap.id
							AND sh.prospectus_id = pr.id
							AND s.idno = sh.students_idno
							AND sh.academic_terms_id = {$this->db->escape($academic_terms_id)}
							AND ap.id = {$this->db->escape($program)}
							AND sh.id in (select distinct en.student_history_id from enrollments en)";
			}
			
			if ($yearlevel) { 
				$q1 .= "and sh.year_level = {$this->db->escape($yearlevel)}
						ORDER by s.lname, s.fname";
			} else {
				$q1 .= "ORDER by s.lname, s.fname";
			}

			$query = $this->db->query($q1);
			return ($query ? $query->result() : FALSE);
		}
					
		//Added: 04/10/2013
	
		function student_withgrades($student_id){
			$sql = "
				SELECT
					e.id as enrollment_id, sh.id, sh.year_level,
					CONCAT((ay.end_year-1), '-', (ay.end_year)) as sy, if(at.term=1,'First Sem',if(at.term=2,'Second Sem','Summer')) as term,
					e.prelim_grade, e.midterm_grade, e.finals_grade,
					c.course_code, c.id as courseid, c.descriptive_title, 
					if(!isnull(eo.id), eo.credit_units, c.credit_units) as credit_units,					
					ap.abbreviation,e.update_history,
					CONCAT(emp.lname, ', ', emp.fname) AS instructor
				FROM
					enrollments e
				LEFT JOIN enrollments_override eo ON eo.enrollments_id=e.id
				LEFT JOIN
					student_histories sh
				ON
					sh.id=e.student_history_id
				LEFT JOIN
					course_offerings co
				ON
					co.id=e.course_offerings_id
				LEFT JOIN
					courses c
				ON
					c.id=co.courses_id
				LEFT JOIN
					academic_terms at
				ON
					at.id=sh.academic_terms_id
				LEFT JOIN
					academic_years ay
				ON
					ay.id=at.academic_years_id
				LEFT JOIN
					employees emp
				ON
					emp.empno=co.employees_empno
				LEFT JOIN
					prospectus p
				ON
					p.id=sh.prospectus_id
				LEFT JOIN
					academic_programs ap
				ON
					p.academic_programs_id=ap.id
				WHERE
					sh.students_idno='{$student_id}'
				ORDER BY
					ay.end_year DESC, at.term DESC, c.course_code ASC
			";

/**			AND ((e.prelim_grade NOT IN('WD', 'NC', 'DR') AND e.prelim_grade IS NOT NULL)
					AND(e.midterm_grade NOT IN('WD', 'NC', 'DR') AND e.midterm_grade IS NOT NULL)
					AND (e.finals_grade NOT IN('WD', 'NC', 'DR') AND e.finals_grade IS NOT NULL) ) **/
			
			//print($sql);die();
			$query = $this->db->query($sql);
			if ($query && count($query->num_rows() > 0)){
				
				$academic_terms = array();
				foreach ($query->result() as $row) {
					
					if (array_key_exists($row->id, $academic_terms)){
						$academic_terms[$row->id]['courses'][] = array("course"=>$row);
					} else {
						$academic_terms[$row->id] = array(
											"school_year"	=>$row->sy,
											"term"			=>$row->term,
											"program"		=>$row->abbreviation,
											"year_level"	=>$row->year_level,
											"courses"		=>array(array("course"=>$row,)),
								);
					}
				}
				return $academic_terms;
			} else {
				return FALSE;
			}
		}
		
		//Added: 4/10/2013
		
		function GetEnrollmentInfo($enrollment_id) 
		{
			$result = null;
			
			$q = "SELECT e.id, 
						 c.course_code,
						 cos.start_time,
						 cos.end_time,
						 cos.days_day_code
					FROM
						enrollments as e,
						course_offerings as co,
						course_offerings_slots as cos,
						courses as c
						
					WHERE
						e.id = {$this->db->escape($enrollment_id)} 
						AND e.course_offerings_id = co.id
						AND co.courses_id = c.id
						AND co.id = cos.course_offerings_id"; 
			
	
			$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}	
		
	function EditGrade($enrollment_id, $new_grade, $period,$prev_grade,$update_history){
		
		if($new_grade == NULL){
		   return FALSE;
		 }  
		 $text = array(
		 		'date_updated'	=> date('Y-m-d'),		 				 	
		 		'empno' 		=> $this->session->userdata('empno'),
		 		'period'		=> $period,
		 		'prev_grade' 	=> $prev_grade,
		 		'new_grade' 	=> $new_grade,
		 	);

		 $text  = json_encode($text);
		 $text_old = str_replace("\\","",$update_history) ;		 			 
		 $text_new=array($text_old,$text);
		 		
		 
		if($period == "Prelim"){
			$q = "
					UPDATE enrollments 
					SET 
						prelim_grade ={$this->db->escape($new_grade)}
				";
		}elseif($period == "Midterm"){
			$q = "
					UPDATE enrollments 
					SET 
						midterm_grade ={$this->db->escape($new_grade)}
				";
		 }else{
			 $q = "
			 		UPDATE enrollments 
					SET 
						finals_grade = {$this->db->escape($new_grade)}
			 	";
		 }
		$q .= 	", update_history = " . $this->db->escape($text);
		$q .= 	" WHERE id = {$this->db->escape($enrollment_id)} ";
		   
		//print_r($q);die();
		if ($this->db->query($q)) {
			return TRUE;
		} else {
			return FALSE;
		}			
	}
	
	
	/*
	 * MODIFIED: 6/10/14
	 * @author: genes
	 * @description: used for college enrollment summary to get total load and paying units
	 */
	function getTotal_Units($academic_terms_id) {
		$result = null;
		
		$q = "SELECT
						SUM(c.paying_units) AS total_paying,
						SUM(c.credit_units) AS total_load
					FROM
						enrollments AS a,
						course_offerings AS b,
						courses AS c
					WHERE
						a.course_offerings_id = b.id
						AND c.id=b.courses_id
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND b.status='A'
						AND a.status='Active' ";
		
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
			$result = $query->row();
		}
			
		return $result;
		
	}
	
	/*
	 * ADDED: 6/11/14
	 * @author: genes
	 * @description: get total numer of enrollees excluding 'withdraw'
	 */
	function getTotalEnrollments($academic_terms_id) {
		$result = null;
		
		$q = "SELECT
						COUNT(*) AS total_enrollees
					FROM
						enrollments AS a,
						course_offerings AS b
					WHERE
						a.course_offerings_id = b.id
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND b.status='A' 
						AND a.status='Active' ";
		
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
		$result = $query->row();
		}
			
		return $result;
		
	}
	
	function get_grades($idno, $term) {
		$result = null;
		
		$q = "SELECT e.finals_grade, if(!isnull(eo.id), eo.credit_units, c.credit_units) as credit_units		
				FROM enrollments e
					LEFT JOIN enrollments_override eo ON eo.enrollments_id=e.id				
					LEFT JOIN student_histories sh ON sh.id=e.student_history_id
					LEFT JOIN students s on s.idno = sh.students_idno
					LEFT JOIN academic_terms at ON at.id=sh.academic_terms_id
					LEFT JOIN course_offerings co ON co.id=e.course_offerings_id
					LEFT JOIN courses c ON c.id=co.courses_id
				WHERE
					s.idno='{$idno}'
					AND sh.academic_terms_id = {$this->db->escape($term)}
					AND c.course_code NOT LIKE ('CWTS%') OR c.course_code NOT LIKE ('ROTC%')
				GROUP BY e.student_history_id";
				
		//print($q);
		//die();
			
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			} 
			
			return $result;
	}

	//Added: 10/28/2013
	
	function listCoursesWithdrawn($history_id) {
		$result = null;
	
		$q = "SELECT 
				c.course_code,
				DATE_FORMAT(a.withdrawn_on, '%b. %d, %Y')  as withdrawn_on
			FROM enrollments as a,
				 course_offerings as b,
				 courses as c
			WHERE
				 a.student_history_id = {$this->db->escape($history_id)}
				 AND a.course_offerings_id = b.id
				 AND b.courses_id = c.id
				 AND a.withdrawn_on IS NOT NULL";
	
			
		$query = $this->db->query($q);
			
		if($query && $query->num_rows() > 0){
		$result = $query->result();
	}
		
	return $result;
		}
	
	function list_offerings($course_id, $acad_term_id, $offering_id) {
			$q = "SELECT a.id as new_offering_id, 
					a.section_code,
				    b.*,
				    c.course_code
				  
				  FROM
						courses as c
						   LEFT JOIN 
								course_offerings a ON a.courses_id = c.id
									LEFT JOIN 
										course_offerings_slots b ON a.id = b.course_offerings_id
			      WHERE
						a.courses_id = {$this->db->escape($course_id)}
						AND a.academic_terms_id = {$this->db->escape($acad_term_id)}
						AND a.status = 'A'	
						AND b.course_offerings_id !=  {$this->db->escape($offering_id)}
				ORDER BY a.section_code						
				  ";
				
			//print($q); die();
			$query = $this->db->query($q);
			if ($query and $query->num_rows() > 0)
			return $query->result(); else
				return FALSE;
		}
		
	//Added: 12/5/2013

		function updateCourseOffering($studentHistNo,$prevOffering_id,$newOffering_id, $empno) {
			$q = "UPDATE enrollments
					SET
						course_offerings_id = {$this->db->escape($newOffering_id)},
						enrolled_by = {$this->db->escape($empno)},
						date_enrolled = NOW()
				  WHERE
						course_offerings_id = {$this->db->escape($prevOffering_id)}
				  		AND student_history_id = {$this->db->escape($studentHistNo)}
						";
			$this->db->query($q);
			return $this->db->affected_rows();
			
		}
		
	//Added:12/5/2013

	function getCourse_id($offering_id){
		$result = null;
		
		$q = "SELECT a.id
					FROM
						courses as a,
						course_offerings as b
					WHERE
						b.id = {$this->db->escape($offering_id)} 
						AND a.id = courses_id"; 
				$query = $this->db->query($q);
			
			
			if($query && $query->num_rows() > 0){
				$result = $query->row();
			} 
			
			return $result;
		}	
		
	//Added: January 20, 2014 by Isah
	
		function listAddedSubjects($history_id, $assessedDate) {
			$q = "SELECT c.course_code, a.id, a.date_enrolled, a.post_status
					FROM
						enrollments a, 
						course_offerings as b,
						courses as c
					WHERE
						a.date_enrolled > {$this->db->escape($assessedDate)}
						AND a.student_history_id = {$this->db->escape($history_id)}
						AND a.post_status IS NULL
						AND a.course_offerings_id = b.id
						AND b.courses_id = c.id	";
			//print_r($q); die();	
			$query = $this->db->query($q);
			if ($query and $query->num_rows() > 0)
			return $query->result(); else
				return FALSE;
		}
		
		
	//Added:  2/1/2014 by isah
	
		function list_student_withdrawals($cur_term_id) {
			$q = "SELECT count(c.idno) as count_subjects, c.idno, c.lname, c.fname, a.year_level, e.abbreviation  
					FROM
						student_histories a
							LEFT JOIN
								enrollments b ON b.student_history_id = a.id
							LEFT JOIN
								students c ON c.idno = a.students_idno,
						prospectus as d,
						academic_programs as e		
					WHERE
						a.prospectus_id = d.id
						AND d.academic_programs_id = e.id
						AND b.withdrawn_on IS NOT NULL
						AND a.academic_terms_id = {$this->db->escape($cur_term_id)}
					GROUP BY c.idno
					ORDER BY c.lname
					";
				
		
			$query = $this->db->query($q);
			if ($query and $query->num_rows() > 0)
			return $query->result(); else
				return FALSE;
		}
		
		//Added: 3/6/2014 by Isah
		
		function list_CWTS_enrollees($academic_terms_id, $gender, $course_id) {
			$q = "SELECT s.idno,
				  CONCAT(s.lname,', ',s.fname) as sname,
				  DATE_FORMAT(s.dbirth,'%m/%e/%Y') as dbirth,
				  ROUND((DATEDIFF(NOW(),s.dbirth)/365),0) as age, 
				  e.finals_grade, 
				  concat(ad.home_address,', ', tn.name,', ', prv.name) as home_address,
				  ph.phone_number,
				  ap.abbreviation
					FROM
						course_offerings as co 
						   LEFT JOIN 
								enrollments e ON co.id = e.course_offerings_id
									LEFT JOIN
										student_histories sh ON	e.student_history_id=sh.id
											LEFT JOIN
												students s ON s.idno=sh.students_idno
													 LEFT JOIN addresses ad ON ad.students_idno=sh.students_idno
													 	  LEFT JOIN towns tn ON ad.home_towns_id = tn.id 
													 	  	  LEFT JOIN provinces prv ON ad.home_provinces_id = prv.id
													 	   			LEFT JOIN phone_numbers ph ON ph.students_idno=sh.students_idno
													 	     			  LEFT JOIN prospectus pr ON pr.id=sh.prospectus_id,
						academic_programs as ap
					WHERE
						co.courses_id = {$this->db->escape($course_id)}
						AND co.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND e.status = 'Active'
						AND e.finals_grade NOT IN ('NG', 'INE', 'INC', 'NA', 'WD', 5.0, 'DR','IP')";
			 
			if ($gender) {
				$q = $q . "AND s.gender = {$this->db->escape($gender)}";
			};
							
			$q = $q . " AND pr.academic_programs_id = ap.id
						GROUP BY s.idno
						ORDER BY sname";
			//print($q); die();		
			$query = $this->db->query($q);
			if ($query and $query->num_rows() > 0)
			return $query->result(); else
				return FALSE;
		}
		
		//Added: 3/10/2014 by Isah
		
		function room_utilization($acad_term_id) {
			$result = null;
				
		/*	$q2  = "SELECT 
					bld.bldg_name,
					rm.room_no,
					cos.days_day_code,
					rc.day_name,
					cos.start_time,
					cos.end_time,
					CONCAT(e.lname, ', ', e.fname) as name,
					count(en.id) as num_enrollees
				FROM
					course_offerings o, 
					employees e,
					enrollments en,
					courses c,
					course_offerings_slots cos,
					rooms rm, 
					room_occupancy rc, 
					buildings bld
					
				WHERE
					o.academic_terms_id = {$this->db->escape($acad_term_id)}
				    AND o.id = en.course_offerings_id 
				    AND e.empno = o.employees_empno
				    AND o.id = cos.course_offerings_id
				    AND c.id = o.courses_id
				    AND cos.rooms_id = rm.id
				    AND cos.id = rc.course_offerings_slots_id
				    AND rm.buildings_id = bld.id
				
				ORDER by rm.room_no, rc.day_name, cos.start_time"; */
			
			$q2 = "SELECT b.bldg_name, rm.room_no, ro.day_name, CONCAT(cos.start_time,'-',cos.end_time) as schedule_time, 
						CONCAT(e.lname, ', ', e.fname) as name, c.course_code, o.section_code, count(en.id) as num_enrollees 
				FROM 
					employees e
						LEFT JOIN course_offerings o ON e.empno = o.employees_empno
							LEFT JOIN enrollments en ON o.id = en.course_offerings_id
								LEFT JOIN courses c ON c.id = o.courses_id
									LEFT JOIN course_offerings_slots cos ON o.id = cos.course_offerings_id 
										LEFT JOIN room_occupancy ro ON cos.id = ro.course_offerings_slots_id
											LEFT JOIN rooms rm ON cos.rooms_id = rm.id LEFT JOIN buildings b ON rm.buildings_id = b.id
				WHERE
					o.academic_terms_id = {$this->db->escape($acad_term_id)}
				GROUP BY o.id
				ORDER by b.bldg_name, rm.room_no ASC, ro.day_name, cos.start_time ASC";
			
			$query = $this->db->query($q2);
			if( $query && $query->num_rows() > 0){
			return $query->result();
		} else {
		return FALSE;
		}
		}
		
		function masterlist_report($academic_term) {
			$result = null;
			
			$q = "SELECT s.idno,CONCAT(s.lname,', ',s.fname) as sname, 
					CONCAT(ap.abbreviation,'-',sh.year_level) as course, 
					CONCAT(ad.home_address,', ', tn.name,', ', prv.name) as home_address 
				  FROM course_offerings as co 
					LEFT JOIN enrollments e ON co.id = e.course_offerings_id 
					LEFT JOIN student_histories sh ON e.student_history_id=sh.id 
					LEFT JOIN students s ON s.idno=sh.students_idno 
					LEFT JOIN addresses ad ON ad.students_idno=sh.students_idno 
					LEFT JOIN towns tn ON ad.home_towns_id = tn.id 
					LEFT JOIN provinces prv ON ad.home_provinces_id = prv.id 
					LEFT JOIN prospectus pr ON pr.id=sh.prospectus_id, academic_programs as ap 
				 WHERE sh.academic_terms_id = {$this->db->escape($academic_term)} 
					AND pr.academic_programs_id = ap.id 
				GROUP BY s.idno 
				ORDER BY ap.abbreviation, sh.year_level, sname";
			
			$query = $this->db->query($q);
			if( $query && $query->num_rows() > 0){
				return $query->result();
			} else {
				return FALSE;
			}
		}
		
		//method to extract academic term for the current date:
		public function academic_term_for_enrollment_date ($date=NULL){
			$this->db->select('academic_terms_id')->from('enrollment_schedules')
				->limit(1);
			if(is_null($date)){
				$this->db->where('date(NOW()) BETWEEN start_date AND end_date', null, FALSE);
			} else {
				$this->db->where("{$date} BETWEEN start_date AND end_date", null, FALSE);
			}
			$query = $this->db->get();
			
			if($query->num_rows() > 0)
				return $query->row()->academic_terms_id; else
				return 0;
		}
		
		public function students_current_academic_term($idno){
			$this->db->select('student_histories.academic_terms_id,student_histories.year_level,student_histories.extension_end,student_histories.can_enroll,acad_program_groups.levels_id',FALSE)
				->from('student_histories')
				->join('prospectus','prospectus.id=student_histories.prospectus_id', 'left')
				->join('academic_programs', 'academic_programs.id=prospectus.academic_programs_id','left')
				->join('acad_program_groups', 'acad_program_groups.id=academic_programs.acad_program_groups_id','left')
				->where('student_histories.students_idno', $idno)
				->order_by('student_histories.academic_terms_id', 'desc')
				->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
				return $query->row(); else
				return 0;
		}
		
		public function on_enrollment_schedule ($year_level, $graduate_level=FALSE, $new_enrollee=FALSE){
			$this->db->select('academic_terms_id')
				->from('enrollment_schedules')
				->where('(date(NOW()) BETWEEN start_date AND end_date)', null, FALSE);
			if($new_enrollee){
				//NEW ENROLLEE
				$this->db->where('yr_level', 0);
			} else {
				//OLD ENROLLEE
				if($graduate_level)
					$this->db->where('yr_level', 6); else
					$this->db->where('yr_level', $year_level);
			}
			$query = $this->db->get();
			if($query->num_rows() > 0)
				return TRUE; else
				return FALSE;
		}
		
		public function system_academic_term(){
			$query = $this->db->select('id,term,start_date,status')
				->from('academic_terms')
				->where('status', 'current')
				->order_by('id', 'desc')
				->limit(1)->get();
			if($query->num_rows() > 0)
				return $query->row(); else
				return FALSE;
		}
		
		public function default_cr_amount(){
			$query = $this->db->select('default_amount')
				->from('teller_codes')
				->where('teller_code', 'CR')->limit(1)->get();
			if($query->num_rows() > 0)
				return $query->row()->default_amount; else
				return 0;
			
		}
		
		public function insert_scheduled_student_history($students_idno, $inserted_by){
			$query = $this->db->select('prospectus_id,block_sections_id,year_level')
				->from('student_histories')->where('students_idno', $students_idno)->order_by('academic_terms_id', 'desc')
				->limit(1)->get();
			$latest_history = $query->row();
			$insert_data = array(
					'students_idno'=>$students_idno,
					'academic_terms_id'=>$this->academic_term_for_enrollment_date(),
					'prospectus_id'=>$latest_history->prospectus_id,
					'block_sections_id'=>$latest_history->block_sections_id,
					'year_level'=>$latest_history->year_level,
					'inserted_by'=>$inserted_by,
					'can_enroll'=>'Y',
					);
			
			$this->db->set('inserted_on', 'NOW()', FALSE);
			$this->db->insert('student_histories', $insert_data);
			return $this->db->insert_id();
		}
		
		public function set_can_enroll_to_y_current_term($student_id, $academic_terms_id){
			$this->db->query("UPDATE student_histories SET can_enroll='Y' where academic_terms_id=".$this->db->escape($academic_terms_id) . " AND students_idno=". $this->db->escape($student_id));
			return $this->db->affected_rows();
		}
		
		
		public function is_new_enrollee ($students_idno){
			$academic_term_for_enrollment_date = $this->academic_term_for_enrollment_date();
			$this->load->model('academic_terms_model');
			$current_academic_term = $this->academic_terms_model->current_academic_term();
			$this->db->select ('enrollments.id', FALSE)
				->from('enrollments')
				->join('student_histories', 'enrollments.student_history_id=student_histories.id', 'left')
				->join('academic_terms', 'student_histories.academic_terms_id=academic_terms.id', 'left')
				->where('student_histories.students_idno', $students_idno);
			if($academic_term_for_enrollment_date==0){
				$this->db->where("academic_terms.id <= {$current_academic_term->id}", null, FALSE);
			} else {
				$this->db->where("academic_terms.id < " . $academic_term_for_enrollment_date, null, FALSE);
			}
			$query = $this->db->get();
			if($query->num_rows() > 0) {
				return FALSE;
			} else {
				return TRUE;
			}				
		}
		
		/*
		 * @author: gene
		 * @date: 7/18/2014
		 */
		function Count_Enrollees_by_program_course($course_offerings_id) {
			$result = null;
				
			$q = "
					SELECT
						f.abbreviation,
						h.employees_empno, cap_first(concat_ws(', ',e.lname,e.fname)) as teacher,
						COUNT(c.students_idno) AS stud
					FROM
						student_histories AS c,
						enrollments AS d,
						prospectus AS g,
						academic_programs AS f,
						course_offerings AS h
					left join employees e on e.empno = h.employees_empno 
					WHERE
						c.id=d.student_history_id
						AND c.prospectus_id=g.id
						AND g.academic_programs_id=f.id
						AND d.course_offerings_id=h.id						
						AND d.course_offerings_id = {$this->db->escape($course_offerings_id)}
					GROUP BY
						f.id
					ORDER BY
						f.abbreviation
				";
		
			//print($q);die();
			$query = $this->db->query($q);
				
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
			
			return $result;
		}
		
		
		/*
		 * @author: genes
		 * @added: 8/8/14
		 */
		function List_New_Enrollees($academic_terms_id) {
			$result = null;
			
			/*$q = "SELECT
							a.idno,
							CONCAT(TRIM(a.lname),', ',TRIM(a.fname),' ',a.mname) AS name,
							e.abbreviation,
							b.year_level,
							a.gender,
							b.academic_terms_id,
							b.id
						FROM
							students AS a,
							student_histories AS b,
							enrollments AS c,
							prospectus AS d,
							academic_programs AS e
						WHERE
							a.idno=b.students_idno
							AND b.id=c.student_history_id
							AND d.id=b.prospectus_id
							AND d.academic_programs_id=e.id
							AND
							(SELECT
									y.academic_terms_id
								FROM
									enrollments AS x,
									student_histories AS y
								WHERE
									b.id = y.id
									AND x.student_history_id = y.id
								ORDER BY
									y.academic_terms_id
								LIMIT 1
							) = {$this->db->escape($academic_terms_id)}
						GROUP BY
							c.student_history_id
						ORDER BY
							b.academic_terms_id,name";
			*/
			
			$q = "SELECT
							a.idno,
							CONCAT(TRIM(a.lname),', ',TRIM(a.fname),' ',a.mname) AS name,
							e.abbreviation,
							b.year_level,
							a.gender
						FROM
							students AS a,
							student_histories AS b,
							prospectus AS d,
							academic_programs AS e
						WHERE
							b.students_idno=a.idno
							AND b.prospectus_id = d.id
							AND d.academic_programs_id=e.id
							AND
								(SELECT
									x.academic_terms_id
								FROM
									enrollments AS y,
									student_histories AS x
								WHERE
									y.student_history_id = x.id
									AND x.students_idno =a.idno
								ORDER BY
									x.students_idno,
									y.student_history_id,
									x.academic_terms_id
								LIMIT 1) = {$this->db->escape($academic_terms_id)} 
						GROUP BY
							a.idno
						ORDER BY
							name";
			
			//print($q);die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
				
		}
		
		/*
		 * @ADDED: 8/27/14 by Genes
		 * @NOTE: update re_enrollments table
		 */
		function UpdateRe_enrollments($enrollments_id) {
			
			$sql = "UPDATE
						re_enrollments 
					SET
						status='dissolved'
					WHERE
						enrollments_id = {$this->db->escape($enrollments_id)} ";
			
			if ($this->db->query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			}
				
		}
		
		/*
		 * @added: 9/19/14
		 * @author: genes
		 */
		function ListStudentsEnrolled_WithConflict($offering_data) {
			$result = null;

			//list equivalent days for days_day_code
			$day_names = $this->Rooms_Model->ListDayNames($offering_data['days_day_code']);
				
			foreach ($day_names AS $day) {
				$days[] = $day->day_name;
			}
			$d1 = json_encode($days);
			$offering_data['days'] = str_replace(array('"','[',']'),array("'","(",")"),$d1);
				
			//print_r($offering_data);die();
			$q = "SELECT 
						a.idno,
						CONCAT(TRIM(a.lname),', ',TRIM(a.fname),' ',a.mname) AS name,
						j.abbreviation,
						g.year_level,
						b.course_code,
						c.section_code,
						TIME_FORMAT(e.start_time,'%l:%i%p') AS start_time,
						TIME_FORMAT(e.end_time,'%l:%i%p') AS end_time,
						e.days_day_code,
						f.room_no
					FROM 
						students AS a,
						student_histories AS g,
						enrollments AS h,
						course_offerings AS c, 
						room_occupancy AS d, 
						course_offerings_slots AS e,
						courses AS b,
						rooms AS f,
						prospectus AS i,
						academic_programs AS j
					WHERE
						a.idno=g.students_idno
						AND g.id=h.student_history_id
						AND h.course_offerings_id=c.id
						AND e.id=d.course_offerings_slots_id
						AND c.id=e.course_offerings_id 
						AND b.id=c.courses_id
						AND f.id = e.rooms_id
						AND g.prospectus_id=i.id
						AND i.academic_programs_id=j.id
						AND c.academic_terms_id={$this->db->escape($offering_data['academic_terms_id'])}
						AND (({$this->db->escape($offering_data['start_time'])} BETWEEN ADDTIME(e.start_time,'00:01') 
						AND SUBTIME(e.end_time,'00:01') 
						OR {$this->db->escape($offering_data['end_time'])} BETWEEN ADDTIME(e.start_time,'00:01') 
						AND ADDTIME(e.end_time,'00:01')) 
						AND d.day_name IN ".$offering_data['days'].")
						AND f.status='active'
						AND c.id != {$this->db->escape($offering_data['course_offerings_id'])}
						AND a.idno IN (SELECT
											x.students_idno
										FROM
											student_histories AS x,
											course_offerings AS y,
											enrollments AS z
										WHERE
											x.id=z.student_history_id
											AND y.id=z.course_offerings_id
											AND y.id IN {$offering_data['parallel_offerings_id']})	
					GROUP BY
						a.lname,a.fname,a.mname";
						
			//print($q);die();
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
			$result = $query->result();
			}
				
			return $result;
				
		}


		function ListEnrolled_Posted($academic_terms_id, $academic_programs_id, $year_level) {
			$result = null;

			$q = "SELECT
						a.idno,
						a.lname,
						a.fname,
						a.mname,
						CONCAT(e.abbreviation,'-',c.year_level) AS program
					FROM
						students AS a,
						student_histories AS b,
						assessments AS c,
						academic_programs AS e
					WHERE
						b.students_idno = a.idno
						AND b.id = c.student_histories_id
						AND c.academic_programs_id = e.id
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND c.academic_programs_id = {$this->db->escape($academic_programs_id)}
						AND c.year_level = {$this->db->escape($year_level)}
					ORDER BY
						a.lname,
						a.fname,
						a.mname ";
			
			//return $q;
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
				
		}


		function ListEnrolled_Running($academic_terms_id, $academic_programs_id, $year_level) {
			$result = null;

			$q = "SELECT
						a.idno,
						a.lname,
						a.fname,
						a.mname,
						CONCAT(e.abbreviation,'-',b.year_level) AS program
					FROM
						students AS a,
						student_histories AS b,
						prospectus AS c,
						academic_programs AS e,
						course_offerings AS f,
						enrollments AS g
					WHERE
						b.students_idno = a.idno
						AND b.prospectus_id = c.id
						AND c.academic_programs_id = e.id
						AND b.id = g.student_history_id 
						AND f.id = g.course_offerings_id
						AND f.academic_terms_id = b.academic_terms_id 
						AND g.status = 'Active'
						AND b.academic_terms_id = {$this->db->escape($academic_terms_id)}
						AND e.id = {$this->db->escape($academic_programs_id)}
						AND b.year_level = {$this->db->escape($year_level)}
					GROUP BY 
						b.id
					ORDER BY
						a.lname,
						a.fname,
						a.mname ";
			
			//return $q;
			$query = $this->db->query($q);
			
			if($query && $query->num_rows() > 0){
				$result = $query->result();
			}
				
			return $result;
				
		}

		
}
//end of student_model.php
