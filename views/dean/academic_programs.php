<h2 class="heading">Academic Programs (<?php echo $college; ?>)</h2>
<table class="table table-striped table-bordered mediaTable activeMediaTable" id="MediaTable-0">
	<thead>
		<tr>
			<th>Abbreviation</th>
			<th>Descriptive Title</th>
			<th>Latest Prospectus Year</th>
			<th>Program Status</th>
		</tr>
	</thead>
<?php if(isset($programs) && count($programs)>0):?>	
	<tbody>
<?php foreach ($programs as $program): ?>
  <tr>	
	<?php
	if($program->program_status=='O' ){ 	
	?>	
		<td><?php print("<a class=\"prog\" href=\"{$program->id}\"> {$program->abbreviation}</a>");?></td>
		<td><?php echo $program->description; ?></td>
		<td><?php echo $program->effective_year; ?></td>
	 	<td><?php echo ($program->program_status=='F' ? 'Frozen' : 'Offered'); ?></td>
	
	<?php
	 }else{
	?>
		<td><?php echo $program->abbreviation; ?></td>
	    <td><?php echo $program->description; ?></td>
		<td><?php echo $program->effective_year; ?></td>
		<td><?php echo ($program->program_status=='F' ? 'Frozen' : 'Offered'); ?></td>
	
	<?php	
		}
	?>		 
	</tr>	
<?php endforeach; ?>
</tbody>
<?php endif;?>
</table>

 <form id="srcform2" action="<?php echo site_url("dean/academics")?>" method="post">
  <input type="hidden" name="step" value="2" />
</form>

<script>
	$('.prog').click(function(event){
		event.preventDefault(); 
		var  prog_id = $(this).attr('href');
		
		$('<input>').attr({
		    type: 'hidden',
		    name: 'id',
		    value: prog_id,
		}).appendTo('#srcform2');
		$('#srcform2').submit();
	});
</script>